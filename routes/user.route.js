const userRoute = {
  "path": "/api/user",
  "whitelist": ["user.*"],
  "use": [],
  "aliases": {
    // "POST /validateLoginUser": "user.validateLoginUser",
    "POST /login": "user.login",
    "POST /verifyOtp": "user.verifyOtp",
    "POST /verifyEmail": "user.verifyEmail",
    "POST /profile": "user.profile",
    "POST /updateProfile": "user.updateProfile",
    "POST /activateAndInactivateAccount": "user.activateAndInactivateAccount",
    "POST /changePhoneNumber": "user.changePhoneNumber",
    "POST /:action/address": "user.manageAddress",
    "POST /:action/card": "user.manageCard",
    "POST /setDefaultPaymentOption": "user.setDefaultPaymentOption",
    "POST /setAppliedCoupons": "user.setAppliedCoupons",
    "POST /logout": "user.logout",
    "POST /updateLiveLocation": "user.updateLiveLocation",
    "POST /trackUserLocation": "user.trackUserLocation",
    "POST /addBankDetails": "user.addBankDetails",
    "POST /removeBankDetails": "user.removeBankDetails",
    "POST /getBankDetails": "user.getBankDetails",
    "POST /checkUserAndProfessionalAvailableUsingPhoneNo":
      "user.checkUserAndProfessionalAvailableUsingPhoneNo",
    "POST /checkUserAvail": "user.checkUserAvail",
    "POST /adminCheckUserAvail": "user.adminCheckUserAvail",
    "POST /registerUserByBooking": "user.registerUserByBooking",
    "POST /viewDetails": "user.viewDetails",
    "POST /getRatings": "user.getRatings",
    "POST /checkContacts": "user.checkContacts",
    "POST /adminViewDetails": "user.adminViewDetails",
    "POST /addWalletAmount": "user.addWalletAmount",
    "POST /searchUserByName": "user.searchUserByName",

    //DASHBOARD
    "POST /common/dashboard/getUserData": "user.getUserData",
    "POST /getInviteAndEarnList": "user.getInviteAndEarnList",
    "POST /getUsersJoinerList/:id": "user.getUsersJoinerList",
    "POST /downloadInviteAndEarnList": "user.downloadInviteAndEarnList",
    "POST /downloadUserJoiningList": "user.downloadUserJoiningList",
    //PERMISSION DATA
    "POST /updatePermissionData": "user.updatePermissionData",
    //INVITE AND EARN
    "POST /getInviteAndEarnDetail": "user.getInviteAndEarnDetail",
    "POST /getInviteAndEarnHistory": "user.getInviteAndEarnHistory",
    // UPDATE PASSWORD BYPASS
    "POST /updatePasswordBypass": "user.updatePasswordBypass",
    // GET USER ID
    "POST /getUserId": "user.getUserId",
    // GET QR CODE
    "POST /getQrcode": "user.getQrcode",
    // PAYMENT AND CARD AND BANK RELATED
    "POST /verifyCard": "user.verifyCard",

    // db insert
    // "POST /dbmigration": "user.dbMigration",
    // Redeem Rewards Point
    "POST /redeemRewardsPoint": "user.redeemRewardsPoint",

    // FOR RENTAL

    "POST /ride/:rideType/location/categories":
      "user.getBookingLocationCategories",

    // UsersActiveInactive......
    "POST /getUsersActiveInactive": "user.getUsersActiveInactive",
    "POST /manageBlockedProfessionalList": "user.manageBlockedProfessionalList",
    //
    "POST  /manageTrustedContact": "user.manageTrustedContact",
    //
    "POST  /updateProfileNotes": "user.updateProfileNotes",
  },
};
//------------------------
module.exports = userRoute;
