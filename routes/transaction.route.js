const transactionRoute = {
  "path": "/api/transaction",
  "whitelist": ["transaction.*"],
  "use": [],
  "aliases": {
    "POST /newRechargeWalletTransaction":
      "transaction.newRechargeWalletTransaction",
    "POST /newWithdrawalWalletTransaction":
      "transaction.newWithdrawalWalletTransaction",
    "POST /sendMoneyToFriends": "transaction.sendMoneyToFriends",
    "POST /getTransactionDetails": "transaction.getTransactionDetails",
    "POST /getTransactionDetailsById": "transaction.getTransactionDetailsById",

    // ADMIN PANEL WALLET HISTORY
    "POST /getWalletTransactionList": "transaction.getWalletTransactionList",
    "POST /:userType/getParticularTransactionList/:id/:paymentGateWay":
      "transaction.getParticularTransactionList",
    "POST /getTransactionHistoryByType/:id/:userType/:transactionType":
      "transaction.getTransactionHistoryByType",

    "GET /redirectPayment/:transaction": "transaction.redirectPayment",
    // banking adding supporting api
    "POST /verifyBankAccount": "transaction.verifyBankAccount",

    // FLUTTERWAVE GATEWAY
    // AVS 3 SECURE REDIRECT

    "POST /validateCharge": "transaction.validateCharge",

    // wehooks for payment gateways

    "POST /:paymentgateway/webhook": "transaction.webhook",
    "POST /refund": "transaction.refund",
    "GET /:paymentgateway/webhook": "transaction.webhook",
    "POST /:paymentgateway/webhookingStripePixKeyPayment":
      "transaction.webhookingStripePixKeyPayment",
    "POST /:paymentgateway/webhookingMercadopagoPixKeyPayment":
      "transaction.webhookingMercadopagoPixKeyPayment",
    "POST /:paymentgateway/webhookingDelbankPixKeyPayout":
      "transaction.webhookingDelbankPixKeyPayout",

    // Get Bank Input Field Configuration
    "POST /getbankinputdata": "transaction.getBankInputData",
    // developertesting routes
    "POST /ENDBOOKINGCHARGE": "transaction.ENDBOOKINGCHARGE",

    //#region  Payment Gatway

    "POST /verifyCardByPaymentGatway": "transaction.verifyCardByPaymentGatway",
    "POST /newBookingCardPayment": "transaction.newBookingCardPayment",
    "POST /newRechargeWalletCardPayment":
      "transaction.newRechargeWalletCardPayment",
    "POST /newWithdrawalWalletCardPayment":
      "transaction.newWithdrawalWalletCardPayment",
    "POST /checkCardTransactionStatus":
      "transaction.checkCardTransactionStatus",
    "POST /getTransactionReport": "transaction.getTransactionReport",
    "POST /validateCardRegistrationStatus":
      "transaction.validateCardRegistrationStatus",
    "POST /verifyCardByPaymentGatway": "transaction.verifyCardByPaymentGatway",
    "POST /verifyCVVByPaymentGatway": "transaction.verifyCVVByPaymentGatway",
    //#endregion  Payment Gatway
  },
};

module.exports = transactionRoute;
