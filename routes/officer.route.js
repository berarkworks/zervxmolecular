const officerRoute = {
  "path": "/api/officer",
  "whitelist": ["officer.*"],
  "use": [],
  "aliases": {
    //#region Response Officer

    "POST /login": "officer.login",
    "POST /verifyOtp": "officer.verifyOtp",
    "POST /getProfile": "officer.getProfile",
    "POST /updateProfile": "officer.updateProfile",
    "POST /updateLiveLocation": "officer.updateLiveLocation",
    "POST /trackOfficerLocation": "officer.trackOfficerLocation",
    "POST /updateOnlineStatus": "officer.updateOnlineStatus",
    "POST /logout": "officer.logout",

    "POST /getRatings": "officer.getRatings",
    // DASHBOARD
    "POST /common/dashboard/getOfficerData": "officer.getOfficerData",
    //officer PERMISSION DATA
    "POST /updatePermissionData": "officer.updatePermissionData",

    //#endregion Response Officer

    //#region Corporate Officer
    //Corporate Ride
    "POST /getCorporateDetailsByUserId": "officer.getCorporateDetailsByUserId",
    "POST /getCorporateOfficerList": "officer.getCorporateOfficerList",
    //manageCorporateOfficer
    "POST /manageCorporateOfficer": "officer.manageCorporateOfficer",
    "POST /getCorporateOfficerById": "officer.getCorporateOfficerById",
    "POST /checkCorporateOfficerByPhoneNumber":
      "officer.checkCorporateOfficerByPhoneNumber",
    "POST /changeCorporateOfficerData": "officer.changeCorporateOfficerData",
    "POST /searchCorporateOfficerByName":
      "officer.searchCorporateOfficerByName",

    //#endregion Corporate Officer
  },
};

module.exports = officerRoute;
