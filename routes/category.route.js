const categoryRoute = {
  "path": "/api/category",
  "whitelist": ["category.*"],
  "use": [],
  "aliases": {
    "POST /user/:categoryName/getLocationBasedCategory":
      "category.userGetLocationBasedCategory",
    "POST /user/:categoryName/userGetLocationBasedCategoryForGustUser":
      "category.userGetLocationBasedCategoryForGustUser",
  },
};

module.exports = categoryRoute;
