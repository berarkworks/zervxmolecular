const subscriptionRoute = {
  "path": "/api/subscription",
  "whitelist": ["subscription.*"],
  "use": [],
  "aliases": {
    "POST  /:userType/getAllSubscription": "subscription.getAllSubscription",
    "POST  /:userType/addSubscriptionPlan": "subscription.addSubscriptionPlan",
    "POST  /:userType/getSubscriptionPlanDetail":
      "subscription.getSubscriptionPlanDetail",
    "POST  /getAllServiceList": "subscription.getAllServiceList",
  },
};

module.exports = subscriptionRoute;
