const adminRoute = {
  "path": "/api/admin",
  "whitelist": ["admin.*"],
  "use": [],
  "aliases": {
    // COMMON
    "GET /reactConfig": "admin.reactConfig",
    "GET /reactConfigWebbooking/:secretKey": "admin.reactConfigWebbooking",
    "POST /getLanguageKeys": "admin.getLanguageKeys",
    "POST /setUserBasedLanguage": "admin.setUserBasedLanguage",
    "POST /common/loginValidation": "admin.loginValidation",
    "POST /get/data/:id": "admin.getDataById",
    // COMMON CONFIG API
    "POST /common/config": "admin.getCommonConfig",
    // ADMIN
    "POST /loginWithOtp": "admin.loginWithOtp",
    "POST /verifyOtp": "admin.verifyOtp",
    "POST /getProfile": "admin.getProfile",
    "POST /updateProfile": "admin.updateProfile",
    "POST /changePassword": "admin.changePassword",
    // CONFIG GENERAL SETTINGS
    "POST /config/getGenerelConfig": "admin.getGenerelConfig",
    "POST /config/uploadGeneralImages": "admin.uploadGeneralImages",
    "POST /config/updateGenerelConfig": "admin.updateGenerelConfig",
    "POST /config/updateAppConfig": "admin.updateAppConfig",
    "POST /config/updatePackageConfig": "admin.updatePackageConfig",
    "POST /config/updateFirebaseConfig": "admin.updateFirebaseConfig",
    "POST /config/updateMapConfig": "admin.updateMapConfig",
    "POST /config/updateBlockProfessionalConfig":
      "admin.updateBlockProfessionalConfig",
    //  SERVICE AREA CONFIG
    "POST /config/getServiceAreaConfig": "admin.getServiceAreaConfig",
    // MAP CONFIG
    "POST /config/getMapConfig": "admin.getMapConfig",
    // SERVICE TYPE CONFIG
    "POST /config/getZervxServices": "admin.getZervxServices",
    "POST /config/getServiceTypeById": "admin.getServiceTypeById",
    "POST /config/manageClientZervxServices": "admin.manageClientZervxServices",
    // CONFIG SMS SETTINGS
    "POST /config/getSMSConfigList": "admin.getSMSConfigList",
    "POST /config/getSMSConfig": "admin.getSMSConfig",
    "POST /config/updateSMSConfig": "admin.updateSMSConfig",
    "POST /config/changeSMSConfigActiveStatus":
      "admin.changeSMSConfigActiveStatus",
    // CONFIG SMTP SETTINGS
    "POST /config/getSMTPConfig": "admin.getSMTPConfig",
    "POST /config/updateSMTPConfig": "admin.updateSMTPConfig",
    // SETUP VEHICLE CATEGORY
    "POST /setup/getVehicleCategoryList": "admin.getVehicleCategoryList",
    "POST /setup/updateVehicleCategory": "admin.updateVehicleCategory",
    "POST /setup/editVehicleCategory": "admin.editVehicleCategory",
    "POST /setup/changeVehicleCategoryStatus":
      "admin.changeVehicleCategoryStatus",
    "POST /setup/getAllVehicleCategory": "admin.getAllVehicleCategory",
    "POST /setup/getEagleVehicleCategory": "admin.getEagleVehicleCategory",
    "POST /getCityBasedVehicleCategory": "admin.getCityBasedVehicleCategory",
    "POST /getActiveServiceCategoryBasedOnServiceAreaId":
      "admin.getActiveServiceCategoryBasedOnServiceAreaId",
    // SERVICE CATEGORIES
    "POST /getAllServiceCategory": "admin.getAllServiceCategory",
    "POST /getAllServiceActiveInactiveCategory":
      "admin.getAllServiceActiveInactiveCategory",
    "POST /getServiceCategoryList/:requestFrom": "admin.getServiceCategoryList",
    "POST /updateServiceCategory": "admin.updateServiceCategory",
    "POST /manageSpecialDays": "admin.manageSpecialDays",
    "POST /getServiceCategoryById": "admin.getServiceCategoryById",
    "POST /getAllServiceMultiPoint": "admin.getAllServiceMultiPoint",
    "POST /changeServiceCategoryStatus": "admin.changeServiceCategoryStatus",
    "POST /getServiceBasedLocation": "admin.getServiceBasedLocation",
    "POST /getEagleServiceCategory": "admin.getEagleServiceCategory",
    "POST /updateCategoryFareBreakup": "admin.updateCategoryFareBreakup",
    "POST /getCategoryFareBreakup": "admin.getCategoryFareBreakup",
    "POST /getCategoryFareBreakupImages": "admin.getCategoryFareBreakupImages",
    // SETUP PAYMENT GATEWAY
    "POST /setup/getAllPaymentGateway": "admin.getAllPaymentGateway",
    "POST /setup/updatePaymentGateway": "admin.updatePaymentGateway",
    "POST /setup/getEditPaymentGateway": "admin.getEditPaymentGateway",
    "POST /setup/changePaymentGatewayStatus":
      "admin.changePaymentGatewayStatus",
    "POST /userGetAvailableCurrencies": "admin.userGetAvailableCurrencies",
    // OPETRATORS ROLE
    "POST /getOperatorsRoleList": "admin.getOperatorsRoleList",
    "POST /getAllOperatorsRoles": "admin.getAllOperatorsRoles",
    "POST /updateOperatorsRole": "admin.updateOperatorsRole",
    "POST /getOperatorRole": "admin.getOperatorRole",
    "POST /changeOperatorsRoleStatus": "admin.changeOperatorsRoleStatus",
    // OPETRATORS
    "POST /getOperatorsList": "admin.getOperatorsList",
    "POST /updateOperator": "admin.updateOperator",
    "POST /getOperator": "admin.getOperator",
    "POST /changeOperatorStatus": "admin.changeOperatorStatus",
    // RESPONSE OFFICE
    "POST /updateResponseOfficePrivileges":
      "admin.updateResponseOfficePrivileges",
    "POST /getResponseOfficePrivileges": "admin.getResponseOfficePrivileges",
    "POST /getResponseOfficeList": "admin.getResponseOfficeList",
    "POST /updateResponseOffice": "admin.updateResponseOffice",
    "POST /getResponseOffice": "admin.getResponseOffice",
    "POST /changeResponseOfficeStatus": "admin.changeResponseOfficeStatus",
    "POST /getAllResponseOffices": "admin.getAllResponseOffices",
    // SETUP DOCUMENTS
    "POST /setup/getDocumentsList": "admin.getDocumentsList",
    "POST /setup/getEditDocument": "admin.getEditDocument",
    "POST /setup/updateDocument": "admin.updateDocument",
    "POST /setup/changeDocumentStatus": "admin.changeDocumentStatus",
    // SETUP CANCELLATION REASONS
    "POST /setup/getCancellationReasonsList":
      "admin.getCancellationReasonsList",
    // "POST /setup/getEditCancellationReason": "admin.getEditCancellationReason",
    "POST /setup/updateCancellationReason": "admin.updateCancellationReason",
    "POST /setup/changeCancellationReasonStatus":
      "admin.changeCancellationReasonStatus",
    // HUBS
    "POST /updateHubsPrivileges": "admin.updateHubsPrivileges",
    "POST /getHubsPrivileges": "admin.getHubsPrivileges",
    "POST /getHubsList": "admin.getHubsList",
    "POST /updateHubs": "admin.updateHubs",
    "POST /getEditHub": "admin.getEditHub",
    "POST /changeHubsStatus": "admin.changeHubsStatus",
    "POST /getHubEmployeeList/:hubsId": "admin.getHubEmployeeList",
    "POST /updateHubsEmployee": "admin.updateHubsEmployee",
    "POST /getEditHubEmployees": "admin.getEditHubEmployees",
    "POST /changeHubEmployeesStatus": "admin.changeHubEmployeesStatus",
    "POST /addWalletAmount": "admin.addWalletAmount",
    // VERIFICATION DOCUMENTS
    "POST /driver/getVerificationDocumentList":
      "admin.getVerificationDocumentList",
    "POST /driver/updateVerificationDocument":
      "admin.updateVerificationDocument",
    "POST /driver/getEditVerificationDocument":
      "admin.getEditVerificationDocument",
    "POST /driver/changeVerificationDocumentStatus":
      "admin.changeVerificationDocumentStatus",
    // USER
    "POST /user/getUsersList": "admin.getUsersList",
    "POST /user/getEditUser": "admin.getEditUser",
    "POST /user/updateUser": "admin.updateUser",
    "POST /user/getUnregisteredUserList": "admin.getUnregisteredUserList",
    "POST /user/closeUnregisteredUser": "admin.closeUnregisteredUser",
    "POST /user/addUnregisteredUserNotes": "admin.addUnregisteredUserNotes",
    "POST /user/changeUnregisteredUsersStatus":
      "admin.changeUnregisteredUsersStatus",
    "POST  /user/changeUsersStatus": "admin.changeUsersStatus",
    // PROFESSIONALS
    "POST /professional/getUnregisteredProfessionalList":
      "admin.getUnregisteredProfessionalList",
    "POST /professional/closeUnregisteredProfessional":
      "admin.closeUnregisteredProfessional",
    "POST /professional/addUnregisteredProfessionalNotes":
      "admin.addUnregisteredProfessionalNotes",
    "POST /professional/addUnregisteredProfessionalStatusChange":
      "admin.addUnregisteredProfessionalStatusChange",
    "POST /professional/getProfessionalList": "admin.getProfessionalList",
    "POST /professional/getEditProfessional": "admin.getEditProfessional",
    "POST /professional/updateProfessionalStep1":
      "admin.updateProfessionalStep1",
    "POST /professional/getAllDocumentForUpload":
      "admin.getAllDocumentForUpload",
    "POST /professional/updateProfessionalStep2":
      "admin.updateProfessionalStep2",
    "POST /professional/getHubListByLocation": "admin.getHubListByLocation",
    "POST /professional/updateProfessionalStep3":
      "admin.updateProfessionalStep3",
    "POST /professional/updateProfessionalStep4":
      "admin.updateProfessionalStep4",
    "POST /professional/changeProfessionalsStatus":
      "admin.changeProfessionalsStatus",
    "POST /professional/updateBankDetailsProfessional":
      "admin.updateBankDetailsProfessional",
    "POST /professional/changeVehicleStatus": "admin.changeVehicleStatus",
    "POST /professional/professionalProfileDocumentVerification":
      "admin.professionalProfileDocumentVerification",
    "POST /professional/deleteProfessionalDocument":
      "admin.deleteProfessionalDocument",
    "POST /professional/professionalVehicleDocumentVerification":
      "admin.professionalVehicleDocumentVerification",
    "POST /professional/addProfessionalNotes": "admin.addProfessionalNotes",
    "POST /professional/addVehicleNotes": "admin.addVehicleNotes",
    "POST /professional/professionalDocumentExpiryNotify":
      "admin.professionalDocumentExpiryNotify",
    "POST /professional/getVehicleList": "admin.getVehicleList",
    "POST /professional/editVehicleDetails": "admin.editVehicleDetails",
    "POST /professional/professionalVerification":
      "admin.professionalVerification",
    "POST /professional/getUnverifiedProfessionalList":
      "admin.getUnverifiedProfessionalList",
    "POST /professional/getExpiredProfessionalList":
      "admin.getExpiredProfessionalList",
    // professional Verifications
    "POST /professional/expireDocumentsProfessionlUnverified":
      "admin.expireDocumentsProfessionlUnverified",
    // LANGUAGES
    "POST /getLanguagesList": "admin.getLanguagesList",
    "POST /getAllLanguages": "admin.getAllLanguages",
    "POST /getEditLanguage": "admin.getEditLanguage",
    "POST /updateLanguage": "admin.updateLanguage",
    "POST /changeLanguageStatus": "admin.changeLanguageStatus",
    "POST /getDefaultLanguage": "admin.getDefaultLanguage",
    "POST /updateTranslateKeys": "admin.updateTranslateKeys",
    "POST /setDefaultLanguage": "admin.setDefaultLanguage",
    // SECURTTY SERVICES
    "POST /getSecurityServiceList": "admin.getSecurityServiceList",
    "POST /updateSecurityServiceList": "admin.updateSecurityServiceList",
    // RIDES
    "POST /viewRideDetails": "admin.viewRideDetails",
    "POST /:tripType/getNewRidesList": "admin.getNewRidesList",
    "POST /:tripType/getScheduleRidesList": "admin.getScheduleRidesList",
    "POST /:tripType/getOngoingRidesList": "admin.getOngoingRidesList",
    "POST /:tripType/getEndedRidesList": "admin.getEndedRidesList",
    "POST /:tripType/getExpiredRidesList": "admin.getExpiredRidesList",
    "POST /:tripType/getUserCancelledRidesList":
      "admin.getUserCancelledRidesList",
    "POST /:tripType/getProfessionalCancelledRidesList":
      "admin.getProfessionalCancelledRidesList",
    "POST /:tripType/paymentFailedIssueList": "admin.paymentFailedIssueList",
    "POST /getSearchRidesList": "admin.getSearchRidesList",
    "POST /getAssignProfessionalList": "admin.getAssignProfessionalList",
    "POST /sendRequstToProfessional": "admin.sendRequstToProfessional",
    "POST /getGuestRidesList": "admin.getGuestRidesList",
    "POST /getManualMeterRidesList": "admin.getManualMeterRidesList",
    "POST /getCoorperateRidesList": "admin.getCoorperateRidesList",

    // manage rides it consist all data of previous each apis

    // RESPONSE OFFICER
    "POST /officer/getOfficerList": "admin.getOfficerList",
    "POST /officer/getEditOfficer": "admin.getEditOfficer",
    "POST /officer/changeOfficerStatus": "admin.changeOfficerStatus",
    "POST /officer/updateOfficer": "admin.updateOfficer",
    // SUBSCRIPTION
    "POST /subscription/getSubscriptionList": "admin.getSubscriptionList",
    "POST /subscription/getEditSubscription": "admin.getEditSubscription",
    "POST /subscription/changeSubscriptionStatus":
      "admin.changeSubscriptionStatus",
    "POST /subscription/updateSubscription": "admin.updateSubscription",
    // DASHBOARD
    "POST /common/dashboard/getOperatorData": "admin.getOperatorData",
    "POST /common/dashboard/getHubsData": "admin.getHubsData",
    "POST /common/dashboard/getHubsBasedCityData": "admin.getHubsBasedCityData",
    "POST /common/dashboard/getNotificationData": "admin.getNotificationData",
    // BOOK RIDE AND GOD VIEW AND EAGLE VIEW
    "POST /gods/location/getProfessionalList":
      "admin.getLocationProfessionalList",
    "POST /eagle/getAllRidesList": "admin.getAllRidesList",
    "POST /bookRide/getProfessionalsAndServiceArea":
      "admin.getProfessionalsAndServiceArea",
    // EARNINGS API
    "POST /fetchBillingCycle": "admin.fetchBillingCycle",
    "POST /fetchCoorperateBillingCycle": "admin.fetchCoorperateBillingCycle",
    // TRANSACTION API
    "POST /transaction/get/:transactionId": "admin.adminGetTransaction",
    "POST /transaction/changeStatus": "admin.changeTransactionStatus",

    // WALLET AMOUNT LIST
    "POST /getWalletAmountList": "admin.getWalletAmountList",
    "POST /wallet/refund": "admin.getRefundDetails",
    "POST /wallet/refund/:action/:transactionId": "admin.refundAction",

    // payout
    // wallet withdrawal
    "POST /wallet/withdraw": "admin.getWithDrawDetails",

    "POST /wallet/withdraw/:action/:transactionId":
      "admin.walletWithDrawManualTransfer",

    // REPORTS MANAGEMENTS
    "POST  /reports/:userType/addReports": "admin.addReports",
    "POST  /reports/getList": "admin.getReportsList",
    "POST  /changeReportStatus": "admin.reportChangeStatus",
    "POST  /addReportsNotes": "admin.addReportsNotes",
    "POST  /viewReports": "admin.viewReports",

    // SECURITY MANAGEMENT
    "POST  /security/getList": "admin.getSecurityList",
    "POST  /security/getClosedList": "admin.getSecurityClosedList",
    "POST  /security/getOperatorList": "admin.getSecurityOperatorList",
    "POST  /changeSecurityStatus": "admin.changeSecurityStatus",
    "POST  /addSecurityNotes": "admin.addSecurityNotes",
    "POST  /operatorAcceptSecurity": "admin.operatorAcceptSecurity",
    // INVITE AND EARN
    "POST /getInviteAndEarnConfig": "admin.getInviteAndEarnConfig",
    "POST /updateInviteAndEarn": "admin.updateInviteAndEarn",
    "POST /updateInviteAndEarnImage": "admin.updateInviteAndEarnImage",
    // WALLET AMOUNT LIST
    "POST /saveErrorData": "admin.saveErrorData",
    // COORPERATE OFFICE
    "POST  /getCoorperateOfficeList": "admin.getCoorperateOfficeList",
    "POST  /updateCoorperateOffice": "admin.updateCoorperateOffice",
    "POST  /getCoorperateOffice": "admin.getCoorperateOffice",
    "POST  /changeCoorperateOfficeStatus": "admin.changeCoorperateOfficeStatus",
    "POST  /getAllCoorperateOffices": "admin.getAllCoorperateOffices",
    "POST  /updateCoorperateOfficePrivileges":
      "admin.updateCoorperateOfficePrivileges",
    "POST  /getCoorperateOfficePrivileges":
      "admin.getCoorperateOfficePrivileges",
    // COORPERATES OFFICE BILLING LIST
    "POST /getCoorperateOfficeBillingList/:id":
      "admin.getCoorperateOfficeBillingList",
    "POST /changeCoorperatePaidStatus": "admin.changeCoorperatePaidStatus",
    //EMAIL TEMPLATE MANAGEMENT
    "POST  /getEmailTemplateList": "admin.getEmailTemplateList",
    "POST  /updateEmailTemplate": "admin.updateEmailTemplate",
    "POST  /editEmailTemplate": "admin.editEmailTemplate",
    "POST  /changeEmailTemplateStatus": "admin.changeEmailTemplateStatus",
    // POPULAR PLACES MANAGEMENT
    "POST  /getPopularPlacesList": "admin.getPopularPlacesList",
    "POST  /uploadPopularPlaceImage": "admin.uploadPopularPlaceImage",
    "POST  /getEditPopularPlace": "admin.getEditPopularPlace",
    "POST  /changePopularPlaceStatus": "admin.changePopularPlaceStatus",
    "POST  /updatePopularPlace": "admin.updatePopularPlace",
    "POST  /getPopularPlaces": "admin.getPopularPlaces",
    "POST  /getAllPopularPlacesData": "admin.getAllPopularPlacesData",
    // AIRPORT MANAGEMENT
    "POST  /getAirportList": "admin.getAirportList",
    "POST  /uploadAirportImage": "admin.uploadAirportImage",
    "POST  /getEditAirport": "admin.getEditAirport",
    "POST  /changeAirportStatus": "admin.changeAirportStatus",
    "POST  /updateAirport": "admin.updateAirport",

    "POST  /getAirportStopList/:airportId": "admin.getAirportStopList",
    "POST  /updateAirportStops": "admin.updateAirportStops",
    "POST  /getEditAirportStop": "admin.getEditAirportStop",
    "POST  /changeAirportStopStatus/:airportId":
      "admin.changeAirportStopStatus",

    "POST  /getAirports": "admin.getAirports",
    "POST  /getAllAirportData": "admin.getAllAirportData",
    "GET  /getAllAirportDataForWebBookingApp":
      "admin.getAllAirportDataForWebBookingApp",
    // AIRPORT TRANSFER SETTINGS
    "GET /getAirportTransferConfig": "admin.getAirportTransferConfig",
    "POST /config/updateAirportTransferConfig":
      "admin.updateAirportTransferConfig",
    //TOLL GATE MANAGEMENT
    "POST /getTollList": "admin.getTollList",
    "POST /updateToll": "admin.updateToll",
    "POST /getEditToll": "admin.getEditToll",
    "POST /changeTollStatus": "admin.changeTollStatus",
    "POST /getAllTollData": "admin.getAllTollData",
    //COUPON CODE MANAGEMENT
    "POST /getCouponList": "admin.getCouponList",
    "POST /updateCoupon": "admin.updateCoupon",
    "POST /getEditCoupon": "admin.getEditCoupon",
    "POST /changeCouponStatus": "admin.changeCouponStatus",
    "POST /getUserBasedCouponList": "admin.getUserBasedCouponList",
    "POST /checkAndApplyManualCoupon": "admin.checkAndApplyManualCoupon",
    // Check Unique Referral Code
    "POST /checkUniqueReferralCode": "admin.checkUniqueReferralCode",

    //NOTIFICATION MANAGEMENT
    "POST /getNotificationTemplatesList": "admin.getNotificationTemplatesList",
    "POST /updateNotificationTemplates": "admin.updateNotificationTemplates",
    "POST /getNotificationTemplates": "admin.getNotificationTemplates",
    "POST /changeNotificationTemplateStatus":
      "admin.changeNotificationTemplateStatus",
    "POST /deleteNotificationTemplateById":
      "admin.deleteNotificationTemplateById",
    "POST /sendPromotion": "admin.sendPromotion",
    "POST /notification/:userType/getUserList":
      "admin.getUserListForNotification",
    // OUTSTATION POPULAR DESTINATION MANAGEMENT
    "POST /getPopularDestinationList": "admin.getPopularDestinationList",
    "POST /getEditPopularDestination": "admin.getEditPopularDestination",
    "POST /changePopularDestinationStatus":
      "admin.changePopularDestinationStatus",
    "POST /updatePopularDestination": "admin.updatePopularDestination",

    // Booking Sercive management
    "POST /service/getVehicleTypes":
      "admin.getServiceCategoryForThirdPartyBooking",
    "POST /service/bookRide": "admin.newRideBookingForThirdParty",
    "POST /bookingservice/create/management":
      "admin.createBookingServiceManagement",
    "POST /bookingservice/list/management":
      "admin.bookingServiceManagementsList",
    "POST /bookingservice/get/management": "admin.getBookingServiceMangement",
    "POST /bookingservice/edit/management":
      "admin.editBookingServiceManagement",
    "POST /bookingservice/status/change":
      "admin.changeBookingServiceManagementStatus",

    // DB ACCESS
    // "POST /remove0": "admin.remove0",
    "POST /forceLogout": "admin.forceLogout",
    // promotion notifications
    "POST /getUpdateEmailEditableFields": "admin.getUpdateEmailEditableFields",

    // DEVELOPER TESTING APIS
    "POST /fcmTesting": "admin.fcmTesting",
    "POST /sendMessageToFCM": "admin.sendMessageToFCM",
    // "POST /getFCMAccessTokenUsingSecretKey":
    //   "admin.getFCMAccessTokenUsingSecretKey",
    "POST /oauth/token": "admin.getFCMAccessTokenUsingSecretKey",
    "POST /oauth/fcm-token": "admin.getFCMAccessTokenUsingSecretKey",

    // PROMOTION CAMPAIGN
    "POST /promotion/campaign/create": "admin.createPromotionCampaign",

    // CLIENTS MANAGEMENT (PAYMENT & PLANS) RALATED APIS
    "POST /client/create": "admin.createClient",
    "POST /client/edit/:id": "admin.editClient",
    "POST /clients": "admin.getclients",
    "POST /client/getAllActiveInActiveClients":
      "admin.getAllActiveInActiveClients",

    // clients plans management
    "POST /plans/add": "admin.createAndUpdatePlans",
    "POST /plans/get/:id": "admin.getPlans",
    "POST /plans/getallplans": "admin.getAllPlans",
    "POST /plans": "admin.getPlansList",

    // pending payment show message for client tests apis => only for zervx
    "POST /client/paymentDetails": "admin.clientPaymentDetails",
    "POST /client/changeClientActiveStatus": "admin.changeClientActiveStatus",
    "POST /client/clientPayBill": "admin.clientPayBill",

    // BOTH APIS
    "POST /:userType/getCategoryList": "admin.getCategoryList",

    //LOYALTY COUPONS
    "POST /loyaltyCoupon/:requestType": "admin.addEditUpdateLoyaltyCoupon", // =>two types create and (update or edit)
    "POST /loyaltyCoupons": "admin.getLoyaltyCouponsList",
    // CONFIG CALL SETTINGS
    "POST /config/getCALLConfig": "admin.getCALLConfig",
    "POST /config/updateCALLConfig": "admin.updateCALLConfig",
    "POST /config/getCallCenterList": "admin.getCallCenterList",
    "POST /config/manageCallCenter/:action": "admin.manageCallCenter",
    //SETUP Heat Map
    "POST /config/getHeatMapList": "admin.getHeatMapList",
    "POST /config/manageHeatMap/:action": "admin.manageHeatMap",
    //SETUP Service Type
    "POST /config/getServiceTypeList": "admin.getServiceTypeList",
    "POST /config/manageCommonCategory/:action": "admin.manageCommonCategory",
    //SETUP Service Type
    "POST /config/getCommonCategoryDropDownList":
      "admin.getCommonCategoryDropDownList",
    "POST /config/getCommonCategoryList": "admin.getCommonCategoryList",
    "POST /config/manageServiceType/:action": "admin.manageServiceType",
    //SETUP Landing Page
    "POST /config/getLandingPageList": "admin.getLandingPageList",
    "POST /config/manageLandingPage/:action": "admin.manageLandingPage",
    "POST /config/getLandingPageTrendingSliderList":
      "admin.getLandingPageTrendingSliderList",
    // Membership Plan
    "POST /getMembershipPlanList": "admin.getMembershipPlanList",
    // "POST  /getAllLanguages": "admin.getAllLanguages",
    "POST /getMembershipPlanById": "admin.getMembershipPlanById",
    "POST /updateMembershipPlan": "admin.updateMembershipPlan",
    // "POST  /changeLanguageStatus": "admin.changeLanguageStatus",
    // "POST  /getDefaultLanguage": "admin.getDefaultLanguage",
    // "POST  /updateTranslateKeys": "admin.updateTranslateKeys",
    // "POST  /setDefaultLanguage": "admin.setDefaultLanguage",
    "POST /webhook/phoneCall/telecmi": "admin.telecmiCallWebhook",
    "POST /membership/:userType/list": "admin.getMembershipListByUserType",
    // Rewards
    "POST /rewards/partnerDeals/list/:userType":
      "admin.getPartnerDealsRewardsList",
    "POST /rewards/points/list": "admin.getPointsRewardsList",
    // manage
    "POST /rewards/partnerDeals/:requestType":
      "admin.managePartnerDealsRewards",
    "POST /rewards/points/:requestType": "admin.managePointsRewards",
    "POST /reward/points/get": "admin.getRewardPointOfCity",

    "POST /NEW": "admin.NEW",

    "POST /external/getAllRideDetails": "admin.getAllRideDetailsExternal",

    // "POST /NEW": "admin.NEW",
    "POST /redeemRewardsPointConfig": "admin.redeemRewardsPointConfig",

    //#region Admin Common API's
    "POST /getAllList/:name": "admin.getAllList",
    "POST /changeStatus/:name": "admin.changeStatus",
    //#endregion
    //Administrators
    "POST /administratorsCreateAndUpdate/:action":
      "admin.administratorsCreateAndUpdate",
    // RegistrationS etup
    "POST /getRegistrationSetupList/:userType":
      "admin.getRegistrationSetupList",
    "POST /getRegistrationSetupByServiceAreaId":
      "admin.getRegistrationSetupByServiceAreaId",
    "POST /manageRegistrationSetup/:userType": "admin.manageRegistrationSetup",
    "POST /getRegistrationDocumentIdUsingServiceAreaIdAndServiceType":
      "admin.getRegistrationDocumentIdUsingServiceAreaIdAndServiceType",
    // "POST /updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId":
    //   "admin.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId",
    "POST /getEndRideIssueList/:type": "admin.getEndRideIssueList",
    "POST /updateBookingInfo": "admin.updateBookingInfo",
    // Incentive Setup
    "POST /setup/getIncentiveList": "admin.getIncentiveList",
    "POST /setup/manageIncentiveSetup/:action": "admin.manageIncentiveSetup",
    "POST /getProfessionalIncentiveDashboard":
      "admin.getProfessionalIncentiveDashboard",
    "POST /getUserIncentiveDashboard": "admin.getUserIncentiveDashboard",
    // ThirdParty Authorization
    "POST /getThirdPartyAPIAuthorization/:gatewayName/:secretKey":
      "admin.getThirdPartyAPIAuthorization",
  },
};
//-----------------
module.exports = adminRoute;
