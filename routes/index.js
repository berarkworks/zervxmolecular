const UAParser = require("ua-parser-js");
const multiparty = require("multiparty");

const route = {
  "authentication": true,
  "authorization": true,
  "mergeParams": true,
  "autoAliases": false,
  "callingOptions": {
    "timeout": 500,
    "retryCount": 0,
  },
  "bodyParsers": {
    "json": {
      "strict": false,
      "limit": "2MB",
    },
    "urlencoded": {
      "extended": true,
      "limit": "2MB",
    },
  },
  "mappingPolicy": "restrict",
  "logging": true,
};

route.onBeforeCall = async (context, _route, request) => {
  /* GET USER AGENT */
  const ua = UAParser(request.headers["user-agent"]);

  context.meta.userAgent = {
    "deviceType": ua.device.model
      ? `${ua.device.type}/${ua.device.model}`
      : `${ua.browser.name}/${ua.browser.version}`,
    "platform": `${ua.os.name}/${ua.cpu.architecture}`,
    "deviceId": `${request.headers.deviceid}`,
  };
  /* PARSE FORM DATA */
  if (
    request.headers["content-type"] &&
    request.headers["content-type"].includes("multipart/form-data")
  ) {
    const formDataPromise = new Promise((resolve, reject) => {
      const form = new multiparty.Form();
      form.parse(request, function (err, fields, files) {
        const formData = {
          "fields": {},
          "files": [],
        };
        if (err) {
          reject(formData);
        }
        Object.keys(fields).forEach((key) => {
          formData.fields[key] = fields[key][0];
        });
        Object.keys(files).forEach((key) => {
          formData.files.push(files[key][0]);
        });
        resolve(formData);
      });
    });
    context.meta.formData = formDataPromise;
    const parsedFormData = await formDataPromise;
    context.params.req._body = true;
    context.params.req.$params = {
      ...context.params.req.$params,
      ...parsedFormData.fields,
      "files": parsedFormData.files,
    };
    context.params.req.body = {
      ...context.params.req.$params,
      ...parsedFormData.fields,
      "files": parsedFormData.files,
    };
  }
};

const routes = [];

routes.push(Object.assign({ ...route }, require("./admin.route")));
routes.push(Object.assign({ ...route }, require("./booking.route")));
routes.push(Object.assign({ ...route }, require("./bookingV2.route")));
routes.push(Object.assign({ ...route }, require("./category.route")));
routes.push(Object.assign({ ...route }, require("./professional.route")));
routes.push(Object.assign({ ...route }, require("./user.route")));
routes.push(Object.assign({ ...route }, require("./officer.route")));
routes.push(Object.assign({ ...route }, require("./subscription.route")));
routes.push(Object.assign({ ...route }, require("./securityEscort.route")));
routes.push(Object.assign({ ...route }, require("./transaction.route")));
routes.push(Object.assign({ ...route }, require("./eventWatch.route")));
routes.push(Object.assign({ ...route }, require("./gaurdWatch.route")));
routes.push(Object.assign({ ...route }, require("./api.routes")));
routes.push(Object.assign({ ...route }, require("./rewards.route")));
routes.push(Object.assign({ ...route }, require("./log.route")));
routes.push(Object.assign({ ...route }, require("./zmap.route")));
routes.push(Object.assign({ ...route }, require("./heatmap.route")));

module.exports = routes;
