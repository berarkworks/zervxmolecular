const eventWatchRoute = {
  "path": "/api/eventWatch",
  "whitelist": ["eventWatch.*"],
  "use": [],
  "aliases": {
    "POST /createEvent": "eventWatch.createEvent",
    "POST /addPraticipants": "eventWatch.addPraticipants",
    "POST /praticipantsStatusChange": "eventWatch.praticipantsStatusChange",
    "POST /eventStatusChange": "eventWatch.eventStatusChange",
    "POST /editEvent": "eventWatch.editEvent",
    "POST /viewDetails": "eventWatch.viewDetails",
    "POST /getYourEventList": "eventWatch.getYourEventList",
    "POST /getParticipatedEventList": "eventWatch.getParticipatedEventList",
  },
};

module.exports = eventWatchRoute;
