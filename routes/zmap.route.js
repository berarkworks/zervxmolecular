const logRoute = {
  "path": "/api/zmap",
  "whitelist": ["zmap.*"],
  "use": [],
  "aliases": {
    "POST /autocomplete": "zmap.getAutocomplete",
    "POST /directions": "zmap.getDirections",
    "POST /geocode": "zmap.getGeocode",
    "POST /distancematrix": "zmap.getDistanceMatrix",
    "POST /snapway": "zmap.getSnapway",
  },
};

module.exports = logRoute;
