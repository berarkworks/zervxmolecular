const bookingV2 = {
  "path": "/api/bookingV2",
  "whitelist": ["bookingV2.*"],
  "use": [],
  "aliases": {
    "POST /professional/ride/:professionalStatus":
      "bookingV2.updateProfessionalBookingStatus",

    // BOOK VIEW STATUS CHANGE
    // "POST /bookingStatusChange/:professionalStatus":
    //   "bookingV2.bookingStatusChange",
    // "POST /bookingStatuschange/Ended": "bookingV2.bookingStatusChangeEnded",
    "POST /checking": "bookingV2.checking",
  },
};
//---------------------------
module.exports = bookingV2;
