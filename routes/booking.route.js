const bookingRoute = {
  "path": "/api/booking",
  "whitelist": ["booking.*"],
  "use": [],
  "aliases": {
    "POST /user/getPaymentOptions": "booking.userGetPaymentOptions",
    "POST /user/ride/:type": "booking.userRideBooking",
    "POST /user/ride/retryBooking": "booking.userRetryBooking",
    "POST /:userType/trackBooking": "booking.trackBooking",
    "POST /:userType/bookingExpiryOrCancel": "booking.bookingExpiryOrCancel",
    "POST /:userType/getCancellationReason": "booking.getCancellationReason",
    "POST /:userType/cancelBooking": "booking.cancelBooking",
    "POST /professional/ride/accept": "booking.professionalAcceptBooking",
    "POST /professional/ride/:professionalStatus":
      "booking.updateProfessionalBookingStatus",
    "POST /professional/ride/stop/:stopStatus":
      "booking.updatedProfessionalBookingStopStatus",
    "POST /:userType/submitRating": "booking.submitRating",
    "POST /professionalGetScheduleList": "booking.professionalGetScheduleList",
    "POST /professional/ride/wishlist/:action":
      "booking.professionalAddWishList",
    "POST /professional/ride/getWishlist": "booking.professionalGetWishList",
    "POST /userGetScheduleList": "booking.userGetScheduleList",
    "POST /:userType/getOrderAndBookingHistory":
      "booking.getOrderAndBookingHistory",
    "POST /changeRideLocation": "booking.changeRideLocation",
    "POST /scheduleBookingCategoryChange":
      "booking.scheduleBookingCategoryChange",
    "POST /:userType/viewRecipt": "booking.viewRecipt",
    "POST /getBookingDetail": "booking.getBookingDetail",
    "POST /getBookingDetailById": "booking.getBookingDetailById",
    "POST /trackBookingDetailsByIdFromWebBookingApp":
      "booking.trackBookingDetailsByIdFromWebBookingApp",
    "POST /trackBookingDetailsByIdForDebug":
      "booking.trackBookingDetailsByIdForDebug",
    "POST /trackRideCoordinatesUsingRideId":
      "booking.trackRideCoordinatesUsingRideId",

    "POST /giveTips": "booking.giveTips",
    "POST /user/ride/expiredRetryBooking": "booking.expiredRetryBooking",
    // TRUSTED CONTACT
    "POST /getTrustedContactsReasons": "booking.getTrustedContactsReasons",
    "POST /:userType/getTrustedContactsList": "booking.getTrustedContactsList",
    "POST /:userType/:action/TrustedContacts":
      "booking.addAndRemoveTrustedContacts",
    "POST /:userType/shareTripToTrustedContact":
      "booking.shareTripToTrustedContact",
    //DASHBOARD
    "POST /common/dashboard/getCityBasedRidesData":
      "booking.getCityBasedRidesData",
    "POST /common/dashboard/getGraphBookingData": "booking.getGraphBookingData",
    "POST /getAvailableProfessionals": "booking.getAvailableProfessionals",
    "POST /getAvailableProfessionalsForGuestUser":
      "booking.getAvailableProfessionalsForGuestUser",
    "POST /getOnlineProfessionals": "booking.getOnlineProfessionals",
    "POST /newRideBookingFromAdmin/:type": "booking.newRideBookingFromAdmin",
    "POST /getOperatorBasedBooking": "booking.getOperatorBasedBooking",
    "POST /getTotalFareData": "booking.getTotalFareData",
    "POST /getOperatorBasedCategoryBooking":
      "booking.getOperatorBasedCategoryBooking",
    // BOOK VIEW STATUS CHANGE
    "POST /bookingStatusChange/:professionalStatus":
      "booking.bookingStatusChange",
    "POST /bookingStatuschange/Ended": "booking.bookingStatusChangeEnded",
    "POST /adminCancelBooking": "booking.adminCancelBooking",
    "POST /adminBookingExpiryOrCancel": "booking.adminBookingExpiryOrCancel",
    // EARNINGS MANAGEMENT IN ADMIN PANEL
    "POST /getSiteEarnings": "booking.getSiteEarnings",
    "POST /getSiteEarningsSummary": "booking.getSiteEarningsSummary",
    // COMMON CONFIG API
    "POST /common/config": "booking.getCommonConfig",
    // PROFESSIONAL EARNINGS
    "POST /getDriversBillingEarnings": "booking.getDriversBillingEarnings",
    "POST /getProfessionalInstantPaymentEarnings":
      "booking.getProfessionalInstantPaymentEarnings",
    "POST /getDayEarnings": "booking.getDayEarnings",
    "POST /getProfessionalDashboard": "booking.getProfessionalDashboard",
    // BOOKING LIST
    "POST /adminRideList": "booking.adminRideList", //  FOR ADMIN
    "POST /cooprateRideList/:id": "booking.cooprateRideList", //  FOR COORPERATE
    "POST /getUserRideList/:id": "booking.getUserRideList", //  FOR USER
    "POST /getUserRideReportList/:id": "booking.getUserRideReportList", //  For User Ride Report

    "POST /getProfessionalRideList/:id": "booking.getProfessionalRideList", //  FOR PROFESSIONAL
    "POST /getProfessionalRideReportList/:id":
      "booking.getProfessionalRideReportList", //  For Professional Ride Report

    "POST /getCouponBasedRideList/:id": "booking.getCouponBasedRideList", // FOR COUPON CODE
    "POST /getAppUsageList": "booking.getAppUsageList",
    "POST /downloadRideHistoryData": "booking.downloadRideHistoryData",
    "POST /getRidesReport": "booking.getRidesReport",
    "POST /getProfessionalCommissionReport":
      "booking.getProfessionalCommissionReport",
    "POST /getProfessionalRankingReport":
      "booking.getProfessionalRankingReport",
    "POST /getRideReviewRatingReport": "booking.getRideReviewRatingReport",

    //
    "POST /getHubsRideList/:id": "booking.getHubsRideList", //  FOR  HUBS
    // outside booking tracking
    "GET /track/:trackId": "booking.trackBookingUsingLink",
    "GET /track/location/:trackId": "booking.trackCurrentLocationOfRide",

    // MANUAL METER
    "POST /manualMeterBooking": "booking.manualMeterBooking",
    // UPDATE TRIP WAY DATA
    "POST /updateTripWayData": "booking.updateTripWayData",
    // COORPERATE BOOKING
    "POST /coorperateBooking/:type": "booking.coorperateBooking",
    // UPDATE WAITING TIME
    "POST /updateWaitingTimes": "booking.updateWaitingTimes",
    // GET BILLING FARE CALCULATION
    "POST /getFareCalculation": "booking.getFareCalculation",
    // QUICK A RIDE
    "POST /quickRideBooking": "booking.quickRideBooking",
    "POST /professionalSelectAndRideBooking":
      "booking.professionalSelectAndRideBooking",
    "POST /makeCallUsingCallMasking": "booking.makeCallUsingCallMasking",
    // upload Security Image
    "POST /uploadSecurityImage": "booking.uploadSecurityImage",
    "POST /uploadSingleImage": "booking.uploadSingleImage",
    //Live Meter
    "POST /rideLiveMeter": "booking.rideLiveMeter",
    //#region Dashboard

    // Ride Graphs..........

    "POST /getAllRides": "booking.getAllRides",
    "POST /getIncomeExpenceRides": "booking.getIncomeExpenceRides",
    "POST /getProfessionalsRides": "booking.getProfessionalsRides",
    "POST /getUsersRides": "booking.getUsersRides",
    "POST /getCorporateRides": "booking.getCorporateRides",
    "POST /getAdminRides": "booking.getAdminRides",

    // Admin..........

    "POST /getTopDashboardAdminRides": "booking.getTopDashboardAdminRides",
    // "POST  /getTopDashboardAdminRides/:type":
    //   "booking.getTopDashboardAdminRides",

    "POST /getTopDashboardAdminCategoryRides":
      "booking.getTopDashboardAdminCategoryRides",

    // Professionals........

    // "POST  /getTopProfessionalsPerformance/:type":
    //   "booking.getTopProfessionalsPerformance",
    "POST /getTopProfessionalsRides": "booking.getTopProfessionalsRides",
    "POST /getTopProfessionalsVechileCategory":
      "booking.getTopProfessionalsVechileCategory",
    "POST /getTopProfessionalsCategoryRides":
      "booking.getTopProfessionalsCategoryRides",
    // Users...........

    // "POST /getTopUsersPerformance/:type": "booking.getTopUsersPerformance",
    "POST /getTopUsersRides": "booking.getTopUsersRides",

    "POST /getTopUsersCategoryRides": "booking.getTopUsersCategoryRides",

    // Corporate User.........
    // "POST  /getTopDashboardCorporateUserRides/:type":
    //   "booking.getTopDashboardCorporateUserRides",
    "POST /getTopDashboardCorporateUserRides":
      "booking.getTopDashboardCorporateUserRides",
    "POST /getTopDashboardCorporateUserCategoryRides":
      "booking.getTopDashboardCorporateUserCategoryRides",

    // Corporate Officer......
    // "POST /getTopDashboardCorporateOfficerRides/:type":
    //   "booking.getTopDashboardCorporateOfficerRides",
    "POST /getTopDashboardCorporateOfficerRides":
      "booking.getTopDashboardCorporateOfficerRides",

    //#endregion Dashboard
    //#region start PopUp  ............

    // Admin........
    "POST /getPopUpAdminRidesDeatils": "booking.getPopUpAdminRidesDeatils",
    "POST /getPopUpAdminDeatils": "booking.getPopUpAdminDeatils",
    "POST /getPopUpAdminGraph": "booking.getPopUpAdminGraph",

    // Professionals popup........
    "POST /getPopUpProfessionalRidesDeatils":
      "booking.getPopUpProfessionalRidesDeatils",
    "POST /getPopUpProfessionalDeatils": "booking.getPopUpProfessionalDeatils",
    "POST /getPopUpProfessionalGraph": "booking.getPopUpProfessionalGraph",

    // Users popup...........
    "POST /getPopUpUsersRidesDeatils": "booking.getPopUpUsersRidesDeatils",
    "POST /getPopUpUsersDeatils": "booking.getPopUpUsersDeatils",
    "POST /getPopUpUsersGraph": "booking.getPopUpUsersGraph",

    // Corporate User popup.................
    "POST /getPopUpCorporateUsersRidesDeatils":
      "booking.getPopUpCorporateUsersRidesDeatils",
    "POST /getPopUpCorporateUsersDeatils":
      "booking.getPopUpCorporateUsersDeatils",
    "POST /getPopUpCorporateUsersGraph": "booking.getPopUpCorporateUsersGraph",

    // Corporate Officer popup...........,
    "POST /getPopUpCorporateOfficersRidesDeatils":
      "booking.getPopUpCorporateOfficersRidesDeatils",
    "POST /getPopUpCorporateOfficersDeatils":
      "booking.getPopUpCorporateOfficersDeatils",
    "POST /getPopUpCorporateOfficersGraph":
      "booking.getPopUpCorporateOfficersGraph",

    //#endregion popup.......

    "POST /trackProfessionalOngoingRide":
      "booking.trackProfessionalOngoingRide",

    "POST /bookingDataMigrationScript": "booking.bookingDataMigrationScript",
    "POST /radiusBasedGetData": "booking.radiusBasedGetData",
    "POST /totalRideReportData": "booking.totalRideReportData",
    "POST /changeBookingDate": "booking.changeBookingDate",
    // Send Notification to Emergency Contact
    "POST /sendPromotion": "booking.sendPromotion",
  },
};

module.exports = bookingRoute;
