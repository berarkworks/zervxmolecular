const rewardsRoute = {
  "path": "/api/rewards",
  "whitelist": ["rewards.*"],
  "use": [],
  "aliases": {
    "POST /getRewardsListByUserType": "rewards.getRewardsListByUserType",
    "POST /getRewardsForRideByUserType": "rewards.getRewardsForRideByUserType",
    "POST /scratchRewardsById": "rewards.scratchRewardsById",
    "POST /getRewordHistory": "rewards.getRewordHistory",
    "POST /getRewordHistoryPopUp": "rewards.getRewordHistoryPopUp",
    "POST /getRewardHistoryByUserId/:userId/:userType":
      "rewards.getRewardHistoryByUserId",
  },
};

module.exports = rewardsRoute;
