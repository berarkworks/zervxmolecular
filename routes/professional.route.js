module.exports = {
  "path": "/api/professional",
  "whitelist": ["professional.*"],
  "use": [],
  "aliases": {
    // "POST /validateLoginProfessional": "professional.validateLoginProfessional",
    "POST /login": "professional.login",
    "POST /loginWithPhoneNo": "professional.loginWithPhoneNo",
    "POST /verifyOtp": "professional.verifyOtp",
    "POST /verifyEmail": "professional.verifyEmail",
    "POST /profile": "professional.profile",
    "POST /updateProfile": "professional.updateProfile",
    "POST /updateProfileImage": "professional.updateProfileImage",
    "POST /updateProfileAndVehicleDetails":
      "professional.updateProfileAndVehicleDetails",
    "POST /updateProfileCommonDetails":
      "professional.updateProfileCommonDetails",
    "POST /manageServiceType": "professional.manageServiceType",
    "POST /activateAndInactivateAccount":
      "professional.activateAndInactivateAccount",
    "POST /changePhoneNumber": "professional.changePhoneNumber",
    "POST /updateOnlineStatus": "professional.updateOnlineStatus",
    "POST /updatePaymentMode": "professional.updatePaymentMode",
    "POST /profileVerification/getRequiredDocumentList":
      "professional.getDocumentListForProfileVerification",
    "POST /profileVerification/updateDocument":
      "professional.updateProfileVerification",
    "POST /profileVerification/removeDocument":
      "professional.removeProfileVerification",
    "POST /vehicleVerification/getRequiredDocumentList":
      "professional.getDocumentListForVehicleVerification",
    "POST /vehicleVerification/getServiceLocationList":
      "professional.getServiceLocationList",
    "POST /vehicleVerification/getHubList": "professional.getHubList",
    "POST /vehicleVerification/updateDetails":
      "professional.updateVehicleDetails",
    "POST /vehicleVerification/updateDocuments":
      "professional.updateVehicleDocuments",
    "POST /vehicleVerification/removeDetails":
      "professional.removeVehicleDetails",
    "POST /vehicleVerification/setDefaultVehicle/:professionalId":
      "professional.setDefaultVehicle",
    "POST /vehicleVerification/updateHubDetails":
      "professional.updateVehicleHubDetails",
    "POST /vehicleVerification/getDocumentListForHub":
      "professional.getDocumentListForHub",
    "POST /logout": "professional.logout",
    "POST /updateLiveLocation": "professional.updateLiveLocation",
    "POST /trackProfessionalLocation": "professional.trackProfessionalLocation",
    "POST /deleteProfessionalUsingProfessionalId":
      "professional.deleteProfessionalUsingProfessionalId",

    "POST /getProfessionalViewDetails":
      "professional.getProfessionalViewDetails",
    "POST /getRatings": "professional.getRatings",
    "POST /checkUserAndProfessionalAvailableUsingPhoneNo":
      "professional.checkUserAndProfessionalAvailableUsingPhoneNo",
    "POST /checkProfessionalAvail": "professional.checkProfessionalAvail",
    "POST /checkContacts": "professional.checkContacts",
    // DASHBOARD
    "POST /common/dashboard/getProfessionalData":
      "professional.getProfessionalData",
    "POST /common/dashboard/getVehiclesData": "professional.getVehiclesData",
    "POST /common/dashboard/getVehiclesDataBasedVehicleCategory":
      "professional.getVehiclesDataBasedVehicleCategory",
    //PROFESSIONAL UPLOADED LIST
    "POST /getUploadedList": "professional.getUploadedList",
    "POST /getExpiredProfessionalList":
      "professional.getExpiredProfessionalList",
    //PROFESSIONAL EARNINGS
    "POST /fetchBillingCycle": "professional.fetchBillingCycle",
    "POST /getDriverBillingList": "professional.getDriverBillingList",

    "POST /updateSubCategoryOption": "professional.updateSubCategoryOption",
    "POST /addWalletAmount": "professional.addWalletAmount",
    //PROFESSIONAL PERMISSION DATA
    "POST /updatePermissionData": "professional.updatePermissionData",
    // INVITE AND EARN
    "POST /getInviteAndEarnDetail": "professional.getInviteAndEarnDetail",
    "POST /getInviteAndEarnHistory": "professional.getInviteAndEarnHistory",
    "POST /getInviteAndEarnList": "professional.getInviteAndEarnList",
    "POST /getUsersJoinerList/:id": "professional.getUsersJoinerList",
    "POST /downloadInviteAndEarnList": "professional.downloadInviteAndEarnList",
    "POST /downloadProfessionalJoiningList":
      "professional.downloadProfessionalJoiningList",
    // UPDATE PASSWORD BYPASS
    "POST /updatePasswordBypass": "professional.updatePasswordBypass",
    // UPDATE LAST PRIORITY RIDE
    "POST /updateLastPriorityStatus": "professional.updateLastPriorityStatus",
    "POST /updateLastPriorityAddress": "professional.updateLastPriorityAddress",
    "POST /deleteLastPriorityAddress": "professional.deleteLastPriorityAddress",
    "POST /getLastPriorityDetails": "professional.getLastPriorityDetails",
    //GET QR CODE
    "POST /getQrcode": "professional.getQrcode",
    "POST /getProfessionalDetails": "professional.getProfessionalDetails",
    // PAYMENT AND CARD AND BANK RELATED
    "POST /:action/manageCard": "professional.manageCard",
    "POST /getCardsList": "professional.getCardsList",
    "POST /addBankDetails": "professional.addBankDetails",
    "POST /getBankDetails": "professional.getBankDetails",
    "POST /verifyCard": "professional.verifyCard",
    "POST /membership/renew/:membershipPlanId":
      "professional.activateMembershipPlan",
    // Redeem Rewards Point
    "POST /redeemRewardsPoint": "professional.redeemRewardsPoint",
    // Professional Active Inactive..........
    "POST /getProfessionalActiveInactive":
      "professional.getProfessionalActiveInactive",
    "POST /manageBlockedUserList": "professional.manageBlockedUserList",
    "POST /manageBlockedProfessional": "professional.manageBlockedProfessional",
    // REPORTS
    "POST  /reports/getOnlineOfflineList": "professional.getOnlineOfflineList",
    //
    "POST  /manageTrustedContact": "professional.manageTrustedContact",
  },
};
