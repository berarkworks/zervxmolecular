const securityEscortRoute = {
  "path": "/api/securityEscort",
  "whitelist": ["securityEscort.*"],
  "use": [],
  "aliases": {
    "POST /:userType/getSosHelp": "securityEscort.getSosHelp",
    "POST /:userType/getSecurityHelpWithoutFile":
      "securityEscort.getSecurityHelpWithoutFile",
    "POST /getSecurityHelpWithFile": "securityEscort.getSecurityHelpWithFile",
    "POST /acceptEscortBooking": "securityEscort.acceptEscortBooking",
    "POST /escortStatusChange": "securityEscort.escortStatusChange",
    "POST /:userType/acknowledgeEscortStatus":
      "securityEscort.acknowledgeEscortStatus",
    "POST /viewSecurityRequest": "securityEscort.viewSecurityRequest",
    "POST /viewClosedSecurityRequest":
      "securityEscort.viewClosedSecurityRequest",
    "POST /viewSecurityRequestBasedOperator":
      "securityEscort.viewSecurityRequestBasedOperator",
    "POST /getAvaliableOfficer": "securityEscort.getAvaliableOfficer",
    "POST /sendOfficerRequest": "securityEscort.sendOfficerRequest",
    "POST /assignOfficer": "securityEscort.assignOfficer",
    "POST /priorityChange": "securityEscort.priorityChange",
    "POST /:userType/sendTripWatch": "securityEscort.sendTripWatch",
    "POST /:userType/sendPersonalWatch": "securityEscort.sendPersonalWatch",
    "POST /cancelEscortBooking": "securityEscort.cancelEscortBooking",
    "POST /:userType/escortBookingCancel": "securityEscort.escortBookingCancel",
    "POST /escortAdminStatusChange": "securityEscort.escortAdminStatusChange",
    "POST /trackEscortDetails": "securityEscort.trackEscortDetails",
    "POST /viewAddressSave": "securityEscort.viewAddressSave",
    "POST /:userType/submitEscortRating": "securityEscort.submitEscortRating",
    "POST /sendEmergencyToTrustedContact":
      "securityEscort.sendEmergencyToTrustedContact",
    "POST /:userType/emergencyExpiryOrDeny":
      "securityEscort.emergencyExpiryOrDeny",
    "POST /escortBookingMediaReport": "securityEscort.escortBookingMediaReport",
    "POST /escortBookingMediaReportImage":
      "securityEscort.escortBookingMediaReportImage",
    "POST /getOfficerHistory": "securityEscort.getOfficerHistory",
    "POST /getUserEscortHistory": "securityEscort.getUserEscortHistory",
    "POST /viewEscortDetails/:id": "securityEscort.viewEscortDetails",
  },
};

module.exports = securityEscortRoute;
