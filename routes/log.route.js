const logRoute = {
  "path": "/api/log",
  "whitelist": ["log.*"],
  "use": [],
  "aliases": {
    "POST /getNotificationBasedOnUserType":
      "log.getNotificationBasedOnUserType",
    "POST /manageLog": "log.manageLog",
    "POST /errorlogReportData": "log.errorlogReportData",
    "POST /:userType/getLoginHistoryByloginId/:loginId":
      "log.getLoginHistoryByloginId",
  },
};
//------------------------------
module.exports = logRoute;
