const gaurdWatchRoute = {
  "path": "/api/gaurdWatch",
  "whitelist": ["gaurdWatch.*"],
  "use": [],
  "aliases": {
    "POST /createSiteLocation": "gaurdWatch.createSiteLocation",
    "POST /changeSiteLocationStatus": "gaurdWatch.changeSiteLocationStatus",
    "POST /deleteSiteLocation": "gaurdWatch.deleteSiteLocation",
    "POST /getSiteLocationList": "gaurdWatch.getSiteLocationList",
    "POST /sendQrToMail": "gaurdWatch.sendQrToMail",
    "POST /addGaurd": "gaurdWatch.addGaurd",
    "POST /removeGaurd": "gaurdWatch.removeGaurd",
    "POST /getGaurdList": "gaurdWatch.getGaurdList",
    "POST /viewSiteLocation": "gaurdWatch.viewSiteLocation",
    "POST /gaurd/loginAndLogout": "gaurdWatch.loginAndLogout",
    "POST /breakStatusChange": "gaurdWatch.breakStatusChange",
    "POST /getGaurdDetails": "gaurdWatch.getGaurdDetails",
    "POST /scanAndUpdate": "gaurdWatch.scanAndUpdate",
    "POST /owner/getGaurdHistory": "gaurdWatch.getGaurdHistory",
    "POST /gaurd/getGaurdHistory": "gaurdWatch.getMyHistory",
    "POST /getSiteLocationBasedGuard": "gaurdWatch.getSiteLocationBasedGuard",
  },
};

module.exports = gaurdWatchRoute;
