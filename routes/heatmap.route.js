const heatmapRoute = {
  "path": "/api/heatmap",
  "whitelist": ["heatmap.*"],
  "use": [],
  "aliases": {
    "POST /manageHeatmap": "heatmap.manageHeatmap",
    "POST /manageManualPointHeatmap": "heatmap.manageManualPointHeatmap",
    "POST /getHeatmapDataByLocation": "heatmap.getHeatmapDataByLocation",
    "POST /heatmapReportData": "heatmap.heatmapReportData",
  },
};
//---------------------------------
module.exports = heatmapRoute;
