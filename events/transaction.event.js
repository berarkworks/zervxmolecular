const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const Mongoose = require("mongoose");
const { MoleculerError } = require("moleculer").Errors;

const peachPaymentUtils = require("../utils/peachpayment.util");
const stripePaymentUtils = require("../utils/stripePayment.util");
const { customAlphabet } = require("nanoid");

// payment

// const transactionFlutterwavePayment = require("../utils/flutterwavePayment.utils");
// const { ALL } = require("../utils/constant.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");
const { CONST_MERCADOPAGO, SUCCESS } = require("../utils/constant.util");

const transactionEvent = {};

transactionEvent.newBookingTransaction = async function (context) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${"New " + constantUtil.BOOKINGCHARGE}`;
  userData = await this.broker.emit("user.getById", {
    "id": context.params.userId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params.clientId,
    "transactionType": constantUtil.BOOKINGCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": constantUtil.DEBIT,
    "from": {
      "userType": constantUtil.USER,
      "userId": context.params.userId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.professionalBookingEndTransaction = async function (context) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.PROFESSIONALEARNINGS + " " + context.params?.refBookingId
  }`;
  userData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "bookingId": context.params?.bookingId,
    "transactionType": constantUtil.PROFESSIONALEARNINGS,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": constantUtil.CREDIT,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
    "to": {
      "userType": constantUtil.PROFESSIONAL,
      "userId": context.params.professionalId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.professionalSiteCommissionTransaction = async function (
  context
) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionType =
    context.params?.paymentType === constantUtil.DEBIT
      ? constantUtil.BOOKINGCHARGE
      : constantUtil.PROFESSIONALEARNINGS;
  const transactionReason = `${
    transactionType + " " + context.params?.refBookingId
  }`;
  userData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "bookingId": context.params?.bookingId,
    "transactionType": transactionType,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionStatus": constantUtil.SUCCESS,
    "transactionId": transId,
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionDate": new Date(),
    "paymentType": context.params?.paymentType, // CREDIT or DEBIT
    "from": {
      "userType": constantUtil.PROFESSIONAL,
      "userId": context.params.professionalId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};
transactionEvent.hubSiteCommissionTransaction = async function (context) {
  // let userData;
  // const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.HUBSEARNINGS + " " + context.params?.refBookingId
  }`;
  // userData = await this.broker.emit("professional.getById", {
  //   "id": context.params.professionalId.toString(),
  // });
  // userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "bookingId": context.params?.bookingId,
    "transactionType": constantUtil.HUBSEARNINGS,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": Date.now(),
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": constantUtil.CREDIT,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
    "to": {
      "userType": constantUtil.HUBS,
      "userId": context.params.hubId,
      "name": context.params.hubData.hubsName || null,
      "avatar": null,
      "phoneNumber": context.params.hubData.phone.number || null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.corporateRideWalletPayTransaction = async function (context) {
  // let userData;
  // const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.BOOKINGCHARGE + " - " + context.params?.refBookingId
  }`;
  // userData = await this.broker.emit("professional.getById", {
  //   "id": context.params.professionalId.toString(),
  // });
  // userData = userData && userData[0];
  let fromData = {},
    toData = {};
  if (context.params.paymentType === constantUtil.DEBIT) {
    fromData = {
      "userType": constantUtil.COORPERATEOFFICE,
      "userId": context.params.corporateId,
      "name": context.params.corporateData.officeName || null,
      "avatar": null,
      "phoneNumber": context.params.corporateData.phone.number || null,
    };
    toData = {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    };
  } else if (context.params.paymentType === constantUtil.CREDIT) {
    fromData = {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    };
    toData = {
      "userType": constantUtil.COORPERATEOFFICE,
      "userId": context.params.corporateId,
      "name": context.params.corporateData.officeName || null,
      "avatar": null,
      "phoneNumber": context.params.corporateData.phone.number || null,
    };
  }
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "bookingId": context.params?.bookingId,
    "transactionType":
      context.params.transactionType || constantUtil.BOOKINGCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": Date.now(),
    "transactionDate": new Date(),
    "transactionReason": context.params.transactionReason || transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": context.params.paymentType,
    "from": fromData,
    "to": toData,
  });
  if (!updateTrasactionData) {
    return { "result": { "code": 0 } };
  } else {
    return { "result": { "code": 1 } };
  }
};
transactionEvent.cancelCharge = async function (context) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.CANCELLATIONCHARGE + " / " + context.params?.refBookingId
  }`;
  userData = await this.broker.emit("user.getById", {
    "id": context.params.userId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.CANCELLATIONCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": constantUtil.DEBIT,
    "bookingId": context.params.bookingId
      ? Mongoose.Types.ObjectId(context.params.bookingId)
      : undefined,
    "from": {
      "userType": constantUtil.USER,
      "userId": context.params.userId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData) {
    return { "result": { "code": 0 } };
  } else {
    return { "result": { "code": 1 } };
  }
};

transactionEvent.profesionalCancelCharge = async function (context) {
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.CANCELLATIONCHARGE + " / " + context.params?.refBookingId
  }`;
  let userData = await this.broker.emit("professional.getById", {
    "id": context.params.id.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.CANCELLATIONCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "bookingId": context.params.bookingId
      ? Mongoose.Types.ObjectId(context.params.bookingId)
      : undefined,
    "paymentType": constantUtil.DEBIT,
    "from": {
      "userType": constantUtil.PROFESSIONAL,
      "userId": context.params.id,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData) {
    return { "result": { "code": 0 } };
  } else {
    return { "result": { "code": 1 } };
  }
};

transactionEvent.professionalPayoutTransaction = async function (context) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${constantUtil.PROFESSIONALEARNINGS}`;
  userData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.PROFESSIONALEARNINGS,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "transactionReason": transactionReason,
    "transactionStatus": constantUtil.SUCCESS,
    "paymentType": constantUtil.CREDIT,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
    "to": {
      "userType": constantUtil.PROFESSIONAL,
      "userId": context.params.professionalId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.checkCardVaild = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const { INFO_PAYMENT_OPTIONS, SOMETHING_WENT_WRONG } =
    await notifyMessage.setNotifyLanguage(languageCode);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
  let minimumAmount;
  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      // SOME TIME NOT FOUND SO WE NEED TO ADD
      if (!!paymentOptionList[prop]?.availableCurrencies === false) {
        throw new MoleculerError(
          "PLEASE ADD AVILABLECURRENCIES FILED IN  ACTIVE PARMANT GATEWAY",
          500
        );
      }
      minimumAmount = paymentOptionList[prop]?.availableCurrencies.find(
        (currency) =>
          currency.currencyCode === generalSettings.data.currencyCode
      );
      minimumAmount = minimumAmount?.minimumAmount; //value is object please console for better understand
      break;
    }
  }

  let userData;
  if (context.params.userType === constantUtil.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  }
  if (context.params.userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError("cant userdata", 500);
  }
  let gatewayResponse;
  const stringTransactionReason = `card adding into ${generalSettings.data.siteTitle.toLowerCase()} ${Date.now()}/${
    context.params.userType
  }/${userData?._id?.toString()}`;

  if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
    // all function working inside this event
    if (paymentData.version === constantUtil.CONST_V3) {
      gatewayResponse = await this.broker.emit(
        "transaction.flutterwave.checkValidCardV3",
        {
          ...context.params,
        }
      );
    } else {
      gatewayResponse = await this.broker.emit(
        "transaction.flutterwave.checkValidCard",
        {
          ...context.params,
        }
      );
    }
    gatewayResponse = gatewayResponse && gatewayResponse[0];
    return gatewayResponse;
  }
  let updateCardToken;
  if (
    paymentData.gateWay === constantUtil.STRIPE ||
    paymentData.gateWay === constantUtil.RAZORPAY ||
    paymentData.gateWay === constantUtil.PEACH ||
    paymentData.gateWay === constantUtil.CONST_CYBERSOURCE ||
    paymentData.gateWay === constantUtil.CONST_ZCREDITAPIARY ||
    paymentData.gateWay === constantUtil.CONST_MERCADOPAGO ||
    paymentData.gateWay === constantUtil.CONST_TEYA
  ) {
    // gatewayResponse = await this.broker.emit(
    //   "transaction.stripe.checkValidCard",
    //   {
    //     ...context.params,
    //     "paymentData": paymentData,
    //     "userData": userData,
    //     "generalSettings": generalSettings,
    //     "minimumChargeAmount": minimumAmount,
    //     //'transactionId': transaction._id.toString(),
    //     "transactionReason": stringTransactionReason,
    //   }
    // );
    if (
      paymentData.gateWay === constantUtil.CONST_ZCREDITAPIARY ||
      paymentData.gateWay === constantUtil.PEACH
    ) {
      paymentData["isCVVRequired"] = true; // Need to Change Dynamically
    } else if (paymentData.gateWay === constantUtil.CONST_MERCADOPAGO) {
      updateCardToken = await this.broker.emit(
        "transaction.mercadopago.updateCardToken",
        {
          ...context.params,
          "userData": userData,
          "paymentData": paymentData,
          "paymentMethodId": context.params.type,
        }
      );
      updateCardToken = updateCardToken && updateCardToken[0];
      if (!updateCardToken || updateCardToken?.code === 400) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      } else {
        context.params["paymentGateWayCustomerId"] =
          updateCardToken?.data?.paymentGateWayCustomerId;
      }
    }
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "paymentData": paymentData,
        "cardId": updateCardToken?.data?.cardId || null,
        "transactionId": context.params.transactionId,
        "transactionAmount": context.params.transactionAmount,
        "paymentGateWayCustomerId": context.params.paymentGateWayCustomerId,
        "paymentGateWay": paymentData.gateWay,
        "isCVVRequired": paymentData.isCVVRequired,
        "isDefault": true,
        "securityCode": context.params.securityCode,
        "successResponse": context.params.gatewayResponse,
      },
    };
  }
  gatewayResponse = gatewayResponse && gatewayResponse[0];
  if (!gatewayResponse) {
    throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
  }
  if (gatewayResponse.code === 200) {
    if (gatewayResponse.data.authType.toUpperCase() === "NOAUTH") {
      const transaction = await this.adapter.model.create({
        "clientId": context.params.clientId,
        "paymentType": constantUtil.CREDIT,
        "gatewayName": paymentData.gateWay,
        "paymentGateWay": paymentData.gateWay,
        "transactionType": constantUtil.VERIFYCARD,
        "gatewayDocId": paymentData._id,
        "transactionReason": stringTransactionReason,
        "from": {
          "phoneNumber": userData?.phone?.number,
          "userType": context.params.userType,
          "userId": userData._id.toString(),
        },
        "to": {
          "userType": constantUtil.ADMIN,
          "userId": paymentData.accountId,
        },
        "transactionStatus": constantUtil.PROCESSING,
        "cardDetailes": {
          "cardNumber": context.params.cardNumber,
          "expiryMonth": context.params.expiryMonth,
          "expiryYear": context.params.expiryYear,
          "holderName": context.params.holderName,
        },
        "gatewayResponse": gatewayResponse.data.successResponse,
        "stripeCustomerId": gatewayResponse.data.stripeCustomerId,
        "transactionId": gatewayResponse.data.transactionId,
        "transactionDate": Date.now(),
        "transactionAmount": gatewayResponse.data.transactionAmount, // change dynamically
        "previousBalance": null,
        "currentBalance": null,
      });
      if (!transaction) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
    }
    gatewayResponse.data.successResponse = undefined;
    // gatewayResponse.data.cardDetailes = undefined
    // gatewayResponse.data.cardDetailes.isCVVRequired = true
    // gatewayResponse.data.transactionId = transaction._id
    gatewayResponse.data["paymentGateWayCustomerId"] =
      context.params.paymentGateWayCustomerId;
    gatewayResponse.data.paymentData = paymentData;
    gatewayResponse.data["paymentGateWay"] = paymentData.gateWay;
  }
  // return details need to alredy parse from payment gatway
  return gatewayResponse;

  // // no conditions matches return
  // return {
  //   'code': 500,
  //   'data': '',
  //   'message': 'NOT HAVE ANY ACTIVE PAYMENT GATWAYS',
  // }
};

// transactionEvent.newBookingCharges = async function (context) {
//   let languageCode;
//   if (context.meta.professionalId) {
//     languageCode = context.meta?.professionalDetails?.languageCode;
//   } else if (context.meta.userId) {
//     languageCode = context.meta?.userDetails?.languageCode;
//   } else {
//     languageCode = context.meta?.adminDetails?.languageCode;
//   }
//   const {
//     ERROR_PAYMENT_OPTION,
//     ERROR_IN_TRANSACTIONS,
//     INFO_TRANSACTIONS_SUCCESS,
//   } = await notifyMessage.setNotifyLanguage(languageCode);
//   const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
//   if (!paymentOptionList) return ERROR_PAYMENT_OPTION;
//   let paymentData = {};
//   let finalPaymentData = {};
//   Object.values(paymentOptionList)
//     .filter((option) => option.status === constantUtil.ACTIVE)
//     .forEach((option) => {
//       if (option.paymentType === constantUtil.PAYMENTCARD) {
//         paymentData = option;
//       }
//     });
//   if (paymentData.gateWay === "PEACH") {
//     const paymentJson = {
//       "data": {},
//     };
//     if (paymentData["mode"] === constantUtil.SANDBOX) {
//       paymentJson["baseUrl"] = paymentData["testUrl"];
//       paymentJson["token"] = paymentData["testAuthToken"];
//       paymentJson["data"]["entityId"] = paymentData["testEntityId"];
//     } else {
//       paymentJson["baseUrl"] = paymentData["liveUrl"];
//       paymentJson["token"] = paymentData["authToken"];
//       paymentJson["data"]["entityId"] = paymentData["entityId"];
//     }
//     paymentJson["data"]["amount"] = context.params.amount.toString();
//     paymentJson["data"]["currency"] = context.params.currencyCode.toUpperCase();
//     paymentJson["data"]["paymentBrand"] =
//       context.params.card.type.toUpperCase();
//     paymentJson["data"]["paymentType"] = "DB";
//     paymentJson["data"]["card.number"] = context.params.card.cardNumber;
//     paymentJson["data"]["card.holder"] = context.params.card.holderName;
//     paymentJson["data"]["card.expiryMonth"] = context.params.card.expiryMonth;
//     paymentJson["data"]["card.expiryYear"] = context.params.card.expiryYear;
//     paymentJson["data"]["card.cvv"] = context.params.card.cvv;
//     finalPaymentData = await peachPaymentUtils.debitTransactions(paymentJson);
//     if (
//       finalPaymentData &&
//       finalPaymentData["result"] &&
//       finalPaymentData["result"]["code"] === "000.100.110"
//     ) {
//       return {
//         "code": 1,
//         "data": finalPaymentData,
//         "message": INFO_TRANSACTIONS_SUCCESS,
//       };
//     } else {
//       return {
//         "code": 0,
//         "data": {},
//         "message": ERROR_IN_TRANSACTIONS,
//       };
//     }
//   }
//   if (paymentData.gateWay === constantUtil.STRIPE) {
//     let tokenJson = {};
//     let paymentJson = {};
//     const keyData = {};
//     if (paymentData["mode"] === constantUtil.SANDBOX) {
//       keyData["secretKey"] = paymentData["testSecretKey"];
//       keyData["publishableKey"] = paymentData["testPublishableKey"];
//     } else {
//       keyData["secretKey"] = paymentData["secretKey"];
//       keyData["publishableKey"] = paymentData["publishableKey"];
//     }
//     tokenJson = {
//       ...keyData,
//       "data": {},
//     };
//     tokenJson["data"]["card"] = {};
//     tokenJson["data"]["card"]["number"] = context.params.card.cardNumber;
//     tokenJson["data"]["card"]["exp_month"] = context.params.card.expiryMonth;
//     tokenJson["data"]["card"]["exp_year"] = context.params.card.expiryYear;
//     tokenJson["data"]["card"]["cvc"] = context.params.card.cvv;
//     finalPaymentData = await stripePaymentUtils.createTokenization(tokenJson);
//     if (finalPaymentData) {
//       paymentJson = {
//         ...keyData,
//         "data": {},
//       };
//       paymentJson["data"]["amount"] = parseInt(
//         parseFloat(context.params.amount) * 100
//       );
//       paymentJson["data"]["currency"] =
//         context.params.currencyCode.toLowerCase();
//       paymentJson["data"]["source"] = finalPaymentData["id"];
//       paymentJson["data"]["description"] = "Wallet Recharge";
//       finalPaymentData = await stripePaymentUtils.createCharges(paymentJson);
//       if (finalPaymentData && finalPaymentData["status"] === "succeeded") {
//         return {
//           "code": 1,
//           "data": finalPaymentData,
//           "message": INFO_TRANSACTIONS_SUCCESS,
//         };
//       } else {
//         return {
//           "code": 0,
//           "data": {},
//           "message": ERROR_IN_TRANSACTIONS,
//         };
//       }
//     } else {
//       return {
//         "code": 0,
//         "data": {},
//         "message": ERROR_IN_TRANSACTIONS,
//       };
//     }
//   }
//   if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
//     if (paymentData.version === constantUtil.CONST_V3) {
//       const respose = await this.broker.emit(
//         "transaction.flutterwave.instantBookingV3",
//         context.params
//       );
//     } else {
//       const respose = await this.broker.emit(
//         "transaction.flutterwave.instantBooking",
//         context.params
//       );
//     }
//   }
// };

transactionEvent.newTipsCharge = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "";
  const userId = context.params.userId;
  const professionalId = context.params.professionalId;
  const languageCode = context.meta?.userDetails?.languageCode;
  const cardId = context.params.cardId;

  const {
    USER_NOT_FOUND,
    ERROR_PAYMENT_OPTION,
    INFO_TRANSACTIONS_SUCCESS,
    ERROR_IN_TRANSACTIONS,
    INFO_PAYMENT_GATEWAY_EVENT,
    TIPS_AMOUNT,
    WALLET_AMOUNT_LOADED,
    INFO_CHECK_CREDENTIAL,
    SOMETHING_WENT_WRONG,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      return ERROR_PAYMENT_OPTION;
    }
    let paymentData = {};
    Object.values(paymentOptionList)
      .filter((option) => option.status === constantUtil.ACTIVE)
      .forEach((option) => {
        if (option.paymentType === constantUtil.PAYMENTCARD) {
          paymentData = option;
        }
      });
    // if (paymentData.gateWay === "PEACH") {
    //   const paymentJson = {
    //     "data": {},
    //   };
    //   if (paymentData["mode"] === constantUtil.SANDBOX) {
    //     paymentJson["baseUrl"] = paymentData["testUrl"];
    //     paymentJson["token"] = paymentData["testAuthToken"];
    //     paymentJson["data"]["entityId"] = paymentData["testEntityId"];
    //   } else {
    //     paymentJson["baseUrl"] = paymentData["liveUrl"];
    //     paymentJson["token"] = paymentData["authToken"];
    //     paymentJson["data"]["entityId"] = paymentData["entityId"];
    //   }
    //   paymentJson["data"]["amount"] = context.params.amount.toString();
    //   paymentJson["data"]["currency"] = context.params.currencyCode.toUpperCase();
    //   paymentJson["data"]["paymentBrand"] =
    //     context.params.card.type.toUpperCase();
    //   paymentJson["data"]["paymentType"] = "DB";
    //   paymentJson["data"]["card.number"] = context.params.card.cardNumber;
    //   paymentJson["data"]["card.holder"] = context.params.card.holderName;
    //   paymentJson["data"]["card.expiryMonth"] = context.params.card.expiryMonth;
    //   paymentJson["data"]["card.expiryYear"] = context.params.card.expiryYear;
    //   paymentJson["data"]["card.cvv"] = context.params.card.cvv;

    //   finalPaymentData = await peachPaymentUtils.debitTransactions(paymentJson);
    //   if (
    //     finalPaymentData &&
    //     finalPaymentData["result"] &&
    //     finalPaymentData["result"]["code"] === "000.100.110"
    //   ) {
    //     return { "code": 1, "message": INFO_TRANSACTIONS_SUCCESS };
    //   } else {
    //     return { "code": 0, "message": ERROR_IN_TRANSACTIONS };
    //   }
    // } else if (paymentData["gateWay"] === constantUtil.STRIPE) {
    //   let tokenJson = {};
    //   let paymentJson = {};
    //   const keyData = {};
    //   if (paymentData["mode"] === constantUtil.SANDBOX) {
    //     keyData["secretKey"] = paymentData["testSecretKey"];
    //     keyData["publishableKey"] = paymentData["testPublishableKey"];
    //   } else {
    //     keyData["secretKey"] = paymentData["secretKey"];
    //     keyData["publishableKey"] = paymentData["publishableKey"];
    //   }
    //   tokenJson = {
    //     ...keyData,
    //     "data": {},
    //   };
    //   tokenJson["data"]["card"] = {};
    //   tokenJson["data"]["card"]["number"] = context.params.card.cardNumber;
    //   tokenJson["data"]["card"]["exp_month"] = context.params.card.expiryMonth;
    //   tokenJson["data"]["card"]["exp_year"] = context.params.card.expiryYear;
    //   tokenJson["data"]["card"]["cvc"] = context.params.card.cvv;
    //   finalPaymentData = await stripePaymentUtils.createTokenization(tokenJson);
    //   if (finalPaymentData) {
    //     paymentJson = {
    //       ...keyData,
    //       "data": {},
    //     };
    //     paymentJson["data"]["amount"] = parseInt(
    //       parseFloat(context.params.amount) * 100
    //     );
    //     paymentJson["data"]["currency"] =
    //       context.params.currencyCode.toLowerCase();
    //     paymentJson["data"]["source"] = finalPaymentData["id"];
    //     paymentJson["data"]["description"] = "Tips";
    //     finalPaymentData = await stripePaymentUtils.createCharges(paymentJson);
    //     if (finalPaymentData["status"] === "succeeded") {
    //       return { "code": 1, "message": INFO_TRANSACTIONS_SUCCESS };
    //     } else {
    //       return { "code": 0, "message": ERROR_IN_TRANSACTIONS };
    //     }
    //   }
    // }
    //--------------------
    // GET USERDATA
    let userData = {},
      cardData = {};
    //Get User Details
    userData = await this.broker.emit("professional.getById", {
      "id": professionalId,
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(USER_NOT_FOUND, 500);
    }
    // //Get Card Details
    // if (paymentData.gateWay === constantUtil.STRIPE && cardId === CONST_PIX) {
    //   cardData = {};
    // } else {
    //   cardData = await this.broker.emit("user.getCardDetailsById", {
    //     "userId": userId,
    //     "cardId": cardId,
    //   });

    //   cardData = cardData && cardData[0];
    //   if (!cardData) {
    //     throw new MoleculerError(INFO_CARD, 500);
    //   }
    //   cardData["cvv"] = context.params.cvv;
    // }
    // it means here not work excuted all error management is gateway eventsI

    // const stringTransactionReason = `${generalSettings.data.siteTitle}/${
    //   constantUtil.WALLETRECHARGE
    // }/${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}/`;

    // const stringTransactionReason = `${
    //   generalSettings.data.siteTitle
    // }/${INFO_WALLET_RECHARGE}/${Date.now()}/${userType}/${userData?._id?.toString()}`;

    switch (paymentData.gateWay) {
      case constantUtil.FLUTTERWAVE:
        if (paymentData.version === constantUtil.CONST_V3) {
          responseData = await this.broker.emit(
            "transaction.flutterwave.walletRechargeV3",
            {
              ...context.params,
              "userType": constantUtil.USER,
              "userId": userId,
            }
          );
        } else {
          responseData = await this.broker.emit(
            "transaction.flutterwave.walletRecharge",
            {
              ...context.params,
              "userType": constantUtil.USER,
              "userId": userId,
            }
          );
        }
        break;

      case constantUtil.STRIPE:
      case constantUtil.PEACH:
      case constantUtil.CONST_ZCREDITAPIARY:
      case constantUtil.CONST_MERCADOPAGO:
      case constantUtil.CONST_TEYA:
        responseData = [
          {
            "code": 200,
            "data": {
              "authType": "NOAUTH",
              "transactionId": context.params.transactionId,
              "transactionStatus": constantUtil.SUCCESS,
            },
            "message": "",
            ...context.params.gatewayResponse,
          },
        ];
        break;

      case constantUtil.RAZORPAY:
        responseData = [
          {
            "code": 200,
            "data": {
              "authType": "NOAUTH",
              "transactionId": context.params.transactionId,
              "transactionStatus": constantUtil.SUCCESS,
              ...context.params.gatewayResponse.nameValuePairs,
            },
            "message": "",
          },
        ];
        break;

      // case constantUtil.CONST_ZCREDITAPIARY:
      //   responseData = [
      //     {
      //       "code": 200,
      //       "data": {
      //         "authType": "NOAUTH",
      //         "transactionId": context.params.transactionId,
      //         "transactionStatus": constantUtil.SUCCESS,
      //       },
      //       "message": "",
      //       ...context.params.gatewayResponse,
      //     },
      //   ];
      //   break;

      // case constantUtil.PEACH:
      //   responseData = [
      //     {
      //       "code": 200,
      //       "data": {
      //         "authType": "NOAUTH",
      //         "transactionId": context.params.transactionId,
      //         "transactionStatus": constantUtil.SUCCESS,
      //       },
      //       "message": "",
      //       ...context.params.gatewayResponse,
      //     },
      //   ];
      //   break;
    }
    responseData = responseData && responseData[0];
    if (!responseData) {
      throw new MoleculerError(INFO_PAYMENT_GATEWAY_EVENT, 500);
    }
    //--------------- Common  Start -----------------------------
    if (responseData.code === 200) {
      if (responseData.data.authType.toUpperCase() === "NOAUTH") {
        const tipsAmount = parseFloat(context.params.amount || 0);
        // DEBIT History For User
        await this.adapter.model.create({
          "clientId": context.params.clientId,
          "transactionAmount": tipsAmount,
          "previousBalance": 0,
          "currentBalance": 0,
          "transactionReason": TIPS_AMOUNT + " / " + context.params.bookingId,
          "from": {
            "userType": constantUtil.USER,
            "userId": context.params.userId || null,
          },
          "to": { "userType": constantUtil.ADMIN, "userId": null },
          "paymentType": constantUtil.DEBIT,
          "gatewayName": paymentData.gateWay,
          "paymentGateWay": paymentData.gateWay,
          "transactionType": constantUtil.TIPSCHARGE,
          "transactionId": responseData.data.transactionId,
          "transactionDate": Date.now(),
          "gatewayDocId": paymentData?._id,
          "transactionStatus": responseData.data.transactionStatus,
          // 'cardDetailes': context.params.cardDetailes,
          "gatewayResponse": responseData,
        });
        // Credit History for Professional
        const currentBalance =
          parseFloat(userData.wallet.availableAmount || 0) + tipsAmount;
        const transaction = await this.adapter.model.create({
          "clientId": context.params.clientId,
          "transactionAmount": tipsAmount,
          "previousBalance": userData.wallet.availableAmount || 0,
          "currentBalance": currentBalance,
          "transactionReason": TIPS_AMOUNT + " / " + context.params.bookingId,
          "from": {
            "userType": constantUtil.ADMIN,
            "userId": null,
          },
          "to": {
            "userType": constantUtil.PROFESSIONAL,
            "userId": userData._id.toString(),
            "name": userData.firstName + userData.lastName,
            "phoneNumber": userData.phone.code + " " + userData.phone.number,
          },
          "paymentType": constantUtil.CREDIT,
          "gatewayName": constantUtil.NONE, // paymentData.gateWay,
          "paymentGateWay": constantUtil.NONE, // paymentData.gateWay,
          "transactionType": constantUtil.TIPSCHARGE,
          "transactionId": responseData.data.transactionId,
          "transactionDate": Date.now(),
          "gatewayDocId": paymentData?._id,
          "transactionStatus": responseData.data.transactionStatus,
          // 'cardDetailes': context.params.cardDetailes,
          "gatewayResponse": {}, // responseData,
        });
        if (!transaction) {
          throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
        }
        // update professional Wallet Amount
        this.broker.emit("professional.updateWalletAmount", {
          "professionalId": professionalId,
          "availableAmount": userData.wallet.availableAmount + tipsAmount,
          "freezedAmount": userData.wallet.freezedAmount,
        });
        // // add amount to userData
        // let amountUpdateInUser;
        // if (userType === constantUtil.USER) {
        //   if (cardId !== CONST_PIX) {
        //     const updateDefaultCardStatus = await this.broker.emit(
        //       "user.updateDefaultCardStatusInActiveActive",
        //       {
        //         "userId": userId.toString(),
        //         "cardId": cardId.toString(),
        //       }
        //     );
        //   }

        //   amountUpdateInUser = await this.broker.emit(
        //     "user.updateWalletAmount",
        //     {
        //       "userId": userData._id.toString(),
        //       "availableAmount":
        //         userData.wallet.availableAmount + context.params.amount,
        //       "freezedAmount": userData.wallet.freezedAmount,
        //       "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        //     }
        //   );
        // }
        // amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
        // if (!amountUpdateInUser) {
        //   throw new MoleculerError(WALLET_NOTIFICATION, 500);
        // }
        return {
          "code": 200,
          "message": WALLET_AMOUNT_LOADED,
          "data": {
            "wallet": {
              ...userData.wallet,
              "availableAmount": userData.wallet.availableAmount + tipsAmount,
            },
          },
        };
      } else {
        return {
          "code": 200,
          "message": INFO_CHECK_CREDENTIAL,
          "data": responseData.data,
        };
      }
    } else {
      return {
        "code": 400,
        "message": ERROR_IN_TRANSACTIONS,
        "data": {},
      };
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "response": responseData,
  };
};

transactionEvent.updateWalletAdminRecharge = async function (context) {
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const { WALLET_LOADED } = await notifyMessage.setNotifyLanguage(languageCode);
  // const transId = `${helperUtil.randomString(8, "a")}`;
  const transId = `${helperUtil.randomString(8, "#")}`;
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    // "transactionType": constantUtil.ADMINRECHARGE,
    "transactionType":
      context.params.transactionType || constantUtil.ADMINRECHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "transactionReason": context.params.reason,
    "transactionStatus": constantUtil.SUCCESS,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "isReimbursement": context.params.isReimbursement || false,
    "paymentType": constantUtil.CREDIT,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": context.params?.admin?.id,
      "name":
        context.params?.admin?.firstName +
          " " +
          context.params?.admin?.lastName || null,
      "avatar": context.params?.admin?.avatar || null,
      "phoneNumber": context.params?.admin?.phone?.number || null,
    },
    "to": {
      "userType": context.params.userType,
      "userId": context.params?.data?.id,
      "name":
        context.params?.data?.firstName +
          " " +
          context.params?.data?.lastName || null,
      "avatar": context.params.data?.avatar || null,
      "phoneNumber": context.params?.data?.phone?.number || null,
    },
  });
  if (!updateTrasactionData) return { "code": 0 };

  return {
    "code": 1,
    "data": {},
    "message": WALLET_LOADED,
  };
};

transactionEvent.updateWalletAdminDebit = async function (context) {
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const { WALLET_LOADED } = await notifyMessage.setNotifyLanguage(languageCode);
  // const transId = `${helperUtil.randomString(8, "a")}`;
  const transId = `${helperUtil.randomString(8, "#")}`;
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.ADMINDEBIT,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "transactionReason": context.params.reason,
    "transactionStatus": constantUtil.SUCCESS,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "paymentType": constantUtil.DEBIT,
    "from": {
      "userType": context.params.userType,
      "userId": context.params?.data?.id,
      "name":
        context.params?.data?.firstName +
          " " +
          context.params?.data?.lastName || null,
      "avatar": context.params?.data?.avatar || null,
      "phoneNumber": context.params?.data?.phone?.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": context.params?.admin?.id,
      "name":
        context.params?.admin?.firstName +
          " " +
          context.params?.admin?.lastName || null,
      "avatar": context.params?.admin?.avatar || null,
      "phoneNumber": context.params?.admin?.phone?.number || null,
    },
  });
  if (!updateTrasactionData) return { "code": 0 };

  return {
    "code": 1,
    "data": {},
    "message": WALLET_LOADED,
  };
};
transactionEvent.subscriptionTransaction = async function (context) {
  const transId = `${helperUtil.randomString(8, "a")}`;
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.SUBSCRIPTIONCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": null,
    "currentBalance": null,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "paymentType": constantUtil.DEBIT,
    "transactionStatus": constantUtil.SUCCESS,
    "from": {
      "userType": context.params.userType,
      "userId": context.params.data.id,
      "name":
        context.params.data.firstName + " " + context.params.data.lastName ||
        null,
      "avatar": context.params.data.avatar || null,
      "phoneNumber": context.params.data.phone.number || null,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData) return { "code": 0 };

  return {
    "code": 1,
    "data": {},
    "message": "SUBCRIPTION UPDATED SUCCESSFULLY",
  };
};

transactionEvent.inviteCharge = async function (context) {
  const transId = `${helperUtil.randomString(8, "a")}`;
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.INVITECHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "paymentType": constantUtil.CREDIT,
    "transactionStatus": constantUtil.SUCCESS,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
    "to": {
      "userType": context.params.userType,
      "userId": context.params.data.id,
      "name":
        context.params.data.firstName + " " + context.params.data.lastName ||
        null,
      "avatar": context.params.data.avatar || null,
      "phoneNumber": context.params.data.phone.number || null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.joinCharge = async function (context) {
  const transId = `${helperUtil.randomString(8, "a")}`;
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "transactionType": constantUtil.JOININGCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionDate": new Date(),
    "paymentType": constantUtil.CREDIT,
    "transactionStatus": constantUtil.SUCCESS,
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
    "to": {
      "userType": context.params.userType,
      "userId": context.params.data.id,
      "name":
        context.params.data.firstName + " " + context.params.data.lastName ||
        null,
      "avatar": context.params.data.avatar || null,
      "phoneNumber": context.params.data.phone.number || null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

transactionEvent.newBookingCancelRefund = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let languageCode,
    transactionStatus = constantUtil.PROCESSING;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const {
    ERROR_PAYMENT_OPTION,
    USER_NOT_FOUND,
    INFO_CARD,
    SOMETHING_WENT_WRONG,
    INFO_CARD_DECLINED,
    INFO_TRANSACTIONS_SUCCESS,
    ERROR_IN_TRANSACTIONS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);

  if (!paymentOptionList) return ERROR_PAYMENT_OPTION;
  let paymentData = {};
  let finalPaymentData = {};
  Object.values(paymentOptionList)
    .filter((option) => option.status === constantUtil.ACTIVE)
    .forEach((option) => {
      if (option.paymentType === constantUtil.PAYMENTCARD) {
        paymentData = option;
      }
    });
  let userData = {};
  const stringTransactionReason =
    "Ride Cancelled/Trip Number : " + context.params?.refBookingId;
  let cardDetails = {};
  let finalResponse;
  const userId = context.params?.userId;
  const cardId = context.params?.cardId;
  const paymentInitId = context.params?.paymentInitId;
  const transactionType = context.params?.transactionType?.toString();
  const bookingId = context.params?.bookingId;
  const amount = parseFloat(context.params?.amount || 0);
  //Get User Details
  userData = await this.broker.emit("user.getById", {
    "id": userId.toString(),
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND, 500);
  }
  // //Get Card Details
  cardDetails = await this.broker.emit("user.getCardDetailsById", {
    "userId": userId,
    "cardId": cardId,
  });
  cardDetails = cardDetails && cardDetails[0];
  if (!cardDetails) {
    throw new MoleculerError(INFO_CARD, 500);
  }
  // if (paymentData["gateWay"] === "PEACH") {
  //   const paymentJson = {
  //     "data": {},
  //   };
  //   if (paymentData["mode"] === constantUtil.SANDBOX) {
  //     paymentJson["baseUrl"] = paymentData["testUrl"];
  //     paymentJson["token"] = paymentData["testAuthToken"];
  //     paymentJson["data"]["entityId"] = paymentData["testEntityId"];
  //   } else {
  //     paymentJson["baseUrl"] = paymentData["liveUrl"];
  //     paymentJson["token"] = paymentData["authToken"];
  //     paymentJson["data"]["entityId"] = paymentData["entityId"];
  //   }
  //   paymentJson["data"]["amount"] = context.params.amount.toString();
  //   paymentJson["data"]["currency"] = context.params.currencyCode.toUpperCase();
  //   paymentJson["data"]["paymentBrand"] =
  //     context.params.card.type.toUpperCase();
  //   paymentJson["data"]["paymentType"] = "RF";
  //   paymentJson["data"]["card.number"] = context.params.card.cardNumber;
  //   paymentJson["data"]["card.holder"] = context.params.card.holderName;
  //   paymentJson["data"]["card.expiryMonth"] = context.params.card.expiryMonth;
  //   paymentJson["data"]["card.expiryYear"] = context.params.card.expiryYear;
  //   paymentJson["data"]["card.cvv"] = context.params.card.cvv;

  //   finalPaymentData = await peachPaymentUtils.debitTransactions(paymentJson);
  //   if (
  //     finalPaymentData &&
  //     finalPaymentData["result"] &&
  //     finalPaymentData["result"]["code"] === "000.100.110"
  //   ) {
  //     return { "code": 1, "message": INFO_TRANSACTIONS_SUCCESS };
  //   } else {
  //     return { "code": 0, "message": ERROR_IN_TRANSACTIONS };
  //   }
  // }
  let paymentGateWay = paymentData.gateWay;
  let inputParams,
    transactionId = context.params?.paymentInitId;
  //
  if (paymentData.gateWay !== constantUtil.FLUTTERWAVE) {
    inputParams = {
      ...context.params,
      "paymentData": paymentData,
      "userData": userData,
      "cardData": cardDetails,
      "paymentInitId": context.params?.paymentInitId,
      "transactionReason": stringTransactionReason,
      "generalSettings": generalSettings,
    };
  }
  switch (paymentData.gateWay) {
    case constantUtil.FLUTTERWAVE:
      // no refund
      if (transactionType === constantUtil.PENALITY) {
        // take penality amount from user wallet because in flutterwave ride amount only taken from end trip so we cant take amount from cancel trip so we take amount at wallet
        if (parseFloat(context?.params?.amount || 0)) {
          this.broker.emit("user.updateWalletAmount", {
            "clientId": context.params.clientId,
            "userId": userData._id.toString(),
            "availableAmount":
              userData.wallet.availableAmount -
              parseFloat(context?.params?.amount || 0),
            "freezedAmount": userData.wallet.freezedAmount,
            "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
          });
        }
        // then create transaction
        this.broker.emit("transaction.cancelCharge", {
          "clientId": context.params.clientId,
          "userId": userData._id.toString(),
          "bookingId": context.params.bookingId.toString(),
          "amount": parseFloat(context?.params?.amount || 0),
          "previousBalance": userData.wallet.availableAmount,
          "currentBalance":
            userData.wallet.availableAmount -
            parseFloat(context?.params?.amount || 0),
          "refBookingId": context.params.refBookingId.toString(),
        });
      }
      break;

    case constantUtil.STRIPE:
      //----------Refund-Ajith Cancel Capture------
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        if (transactionType === constantUtil.PENALITY && amount > 0) {
          //Cancel the Capture Payment and create and take Penality Charge
          finalResponse = await this.broker.emit(
            "transaction.stripe.capturePaymentIntentsTransaction",
            {
              ...context.params,
              "paymentData": paymentData,
              "userData": userData,
              "paymentInitId": context.params?.paymentInitId,
              "captureAmount": amount,
              "transactionReason": stringTransactionReason,
            }
          );
          // finalResponse = await this.broker.emit(
          //   // "transaction.stripe.cancelCaptureAndCreatePaymentTransaction",
          //   "transaction.stripe.updateCaptureAmountAndCapturePaymentTransaction",
          //   {
          //     ...context.params,
          //     "paymentData": paymentData,
          //     "userData": userData,
          //     "cardData": cardDetails,
          //     "generalSettings": generalSettings,
          //     "transactionReason": stringTransactionReason,
          //     "paymentInitId": context.params?.paymentInitId,
          //     "paymentInitAmount": context.params?.amount,
          //   }
          // );
          // } else if (transactionType === constantUtil.CANCELCAPTURE) {
        } else {
          finalResponse = await this.broker.emit(
            "transaction.stripe.cancelCapturePaymentIntents",
            {
              ...context.params,
              "paymentData": paymentData,
              "userData": userData,
              "paymentInitId": paymentInitId,
            }
          );
        }
        finalResponse = finalResponse && finalResponse[0];
      }
      break;

    case constantUtil.RAZORPAY:
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        // if (transactionType === constantUtil.PENALITY && amount > 0) {
        finalResponse = await this.broker.emit(
          "transaction.razorpay.capturePaymentIntentsAndRefund",
          {
            ...context.params,
            "paymentData": paymentData,
            "userData": userData,
            "paymentInitId": context.params?.paymentInitId,
            "captureAmount": context.params.paymentInitAmount,
            "refundAmount": context.params.paymentInitAmount - amount,
          }
        );
        // }
        finalResponse = finalResponse && finalResponse[0];
      }
      break;

    case constantUtil.CONST_ZCREDITAPIARY:
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        if (transactionType === constantUtil.PENALITY && amount > 0) {
          //Cancel the Capture Payment and create and take Penality Charge
          finalResponse = await this.broker.emit(
            "transaction.zcreditapiary.capturePaymentIntentsTransaction",
            {
              ...context.params,
              "paymentData": paymentData,
              "userData": userData,
              "cardData": cardDetails,
              "paymentInitId": context.params?.paymentInitId,
              "captureAmount": amount,
              "transactionReason": stringTransactionReason,
            }
          );
        } else {
          finalResponse = await this.broker.emit(
            "transaction.zcreditapiary.cancelCapturePaymentIntents",
            {
              ...context.params,
              "paymentData": paymentData,
              "userData": userData,
              "cardData": cardDetails,
              "paymentInitId": paymentInitId,
            }
          );
        }
        finalResponse = finalResponse && finalResponse[0];
        transactionStatus = constantUtil.SUCCESS;
      }
      break;

    case constantUtil.PEACH:
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        if (transactionType === constantUtil.PENALITY && amount > 0) {
          //Cancel the Capture Payment and create and take Penality Charge
          finalResponse = await this.broker.emit(
            "transaction.peach.capturePaymentIntentsAndRefund",
            {
              // ...context.params,
              // "paymentData": paymentData,
              // "userData": userData,
              // "cardData": cardDetails,
              // "paymentInitId": context.params?.paymentInitId,
              ...inputParams,
              "captureAmount": amount,
              "transactionReason": stringTransactionReason,
              "currencyCode": generalSettings.data.currencyCode?.toUpperCase(),
            }
          );
        } else {
          finalResponse = await this.broker.emit(
            "transaction.peach.cancelCapturePaymentIntents",
            {
              // ...context.params,
              // "paymentData": paymentData,
              // "userData": userData,
              // "cardData": cardDetails,
              // "paymentInitId": paymentInitId,
              ...inputParams,
            }
          );
        }
        finalResponse = finalResponse && finalResponse[0];
        transactionStatus = constantUtil.SUCCESS;
      }
      break;

    case constantUtil.CONST_MERCADOPAGO:
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        if (transactionType === constantUtil.PENALITY && amount > 0) {
          //Cancel the Capture Payment and create and take Penality Charge
          finalResponse = await this.broker.emit(
            "transaction.mercadopago.capturePaymentIntentsTransaction",
            {
              ...inputParams,
              "captureAmount": amount,
            }
          );
        } else {
          finalResponse = await this.broker.emit(
            "transaction.mercadopago.cancelCapturePaymentIntents",
            {
              ...inputParams,
            }
          );
        }
        finalResponse = finalResponse && finalResponse[0];
      }
      break;

    case constantUtil.CONST_TEYA:
      //If isPreAuthRequired is True
      if (paymentData.isPreAuthRequired === true) {
        if (transactionType === constantUtil.PENALITY && amount > 0) {
          //Cancel the Capture Payment and create and take Penality Charge
          finalResponse = await this.broker.emit(
            "transaction.teya.captureAndRefundPaymentIntentsTransaction",
            {
              ...inputParams,
              "paymentInitAmount": context.params.paymentInitAmount,
              "captureAmount": amount,
            }
          );
        } else {
          finalResponse = await this.broker.emit(
            "transaction.teya.cancelCapturePaymentIntents",
            {
              ...inputParams,
            }
          );
        }
        finalResponse = finalResponse && finalResponse[0];
      }
      break;
  }
  if (!finalResponse) {
    finalResponse["code"] = 400;
    paymentGateWay = constantUtil.WALLET;
  }
  //--------------- db Insert Start------------
  const transaction = await this.adapter.model.create({
    "clientId": context.params.clientId,
    "transactionAmount": amount,
    "previousBalance": null,
    "currentBalance": null,
    "transactionReason": stringTransactionReason,
    "refBookingId": context.params?.refBookingId,
    "from": {
      "userType": constantUtil.USER,
      "userId": context.params?.userId,
      "name": userData.firstName + userData.lastName,
      "phoneNumber": userData.phoneNumber,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
    },
    "paymentType": constantUtil.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
    // "transactionType": amount > 0 ? transactionType : "",
    "transactionType": transactionType,
    "transactionId": transactionId,
    "bookingId": bookingId,
    "gatewayName": paymentGateWay,
    "paymentGateWay": paymentGateWay,
    "gatewayDocId": paymentData?._id,
    "transactionStatus": constantUtil.PROCESSING, //* Must Not Assign Dynamic Status
    "cardDetailes": cardDetails,
  });
  if (!transaction) {
    throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
  }
  //--------------- db Insert Start------------
  let responseMessage = {};
  let availableAmount = userData.wallet.availableAmount;
  // ---------------- Need to Remove Gatway Condition  End -----------------
  if (finalResponse.code === 200) {
    responseMessage = finalResponse?.message;
    transactionId = finalResponse?.data?.transactionId?.toString();
    // updateDb
    const updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "transactionStatus": transactionStatus,
          "transactionId": transactionId,
          "gatewayResponse": finalResponse?.data?.successResponse || {},
        },
      }
    );
    if (!updateTransaction.nModified) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    } else {
      return {
        "code": 200,
        "message": "AMOUNT PAID SUCCESSFULLY",
        "data": {},
      };
    }
  } else {
    // responseMessage = 'CARD PAYMENT FAILED'
    responseMessage = INFO_CARD_DECLINED;
    availableAmount = userData.wallet.availableAmount - amount;
    // updateDb
    const updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "transactionStatus": constantUtil.SUCCESS,
          "gatewayName": constantUtil.WALLET, //If Card payment Failed
          "paymentGateWay": constantUtil.WALLET,
          "previousBalance": userData.wallet.availableAmount,
          "currentBalance": availableAmount,
        },
      }
    );
    if (!updateTransaction.nModified) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    //---------------- Payment Failed Start ------------------
    // substract amount from user avilable amount
    let amountUpdateInUser = await this.broker.emit("user.updateWalletAmount", {
      "userId": userData._id.toString(),
      "availableAmount": availableAmount,
      "freezedAmount": userData.wallet.freezedAmount,
      "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
      "isSendNotification": true,
      "notificationMessage": INFO_CARD_DECLINED, //Need to Change,
    });
    amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
    if (!amountUpdateInUser) {
      throw new MoleculerError(
        SOMETHING_WENT_WRONG + "/WALLET TRANSACTION",
        500
      );
    }
    //---------------- Payment Failed END ------------------
  }
  return {
    "code": 200,
    "message": responseMessage,
    "data": {
      "availableAmount": availableAmount,
    },
  };
};

transactionEvent.userLessAndGreaterEndTransaction = async function (context) {
  const transId = `${helperUtil.randomString(8, "a")}`;
  const insertData =
    context.params.transType === constantUtil.LESSTHENPAID
      ? {
          "clientId": context.params?.clientId,
          "transactionType": context.params.transactionType,
          "transactionAmount": context.params.amount,
          "previousBalance": context.params.previousBalance,
          "currentBalance": context.params.currentBalance,
          "paidTransactionId": null,
          "transactionId": transId,
          "transactionDate": new Date(),
          "paymentType": context.params.paymentType,
          "from": {
            "userType": constantUtil.ADMIN,
            "userId": null,
            "name": null,
            "avatar": null,
            "phoneNumber": null,
          },
          "to": {
            "userType": context.params.userType,
            "userId": context.params.data.id,
            "name":
              context.params.data.firstName +
                " " +
                context.params.data.lastName || null,
            "avatar": context.params.data.avatar || null,
            "phoneNumber": context.params.data.phone.number || null,
          },
        }
      : {
          "clientId": context.params?.clientId,
          "transactionType": context.params.transactionType,
          "transactionAmount": context.params.amount,
          "previousBalance": context.params.previousBalance,
          "currentBalance": context.params.currentBalance,
          "paidTransactionId": null,
          "transactionId": transId,
          "transactionDate": new Date(),
          "paymentType": context.params.paymentType,
          "from": {
            "userType": context.params.userType,
            "userId": context.params.data.id,
            "name":
              context.params.data.firstName +
                " " +
                context.params.data.lastName || null,
            "avatar": context.params.data.avatar || null,
            "phoneNumber": context.params.data.phone.number || null,
          },
          "to": {
            "userType": constantUtil.ADMIN,
            "userId": null,
            "name": null,
            "avatar": null,
            "phoneNumber": null,
          },
        };
  const updateTrasactionData = await this.adapter.insert(insertData);
  if (!updateTrasactionData) return { "code": 0 };

  return {
    "code": 1,
    "data": {},
    "message": "WALLET TRANSACTION UPDATED SUCCESSFULLY",
  };
};

transactionEvent.professionalTolerenceAmountTransaction = async function (
  context
) {
  let userData;
  const transId = `${helperUtil.randomString(8, "a")}`;
  const transactionReason = `${
    constantUtil.PROFESSIONALTOLERENCEAMOUNT +
    " " +
    context.params?.refBookingId
  }`;
  userData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  userData = userData && userData[0];
  const updateTrasactionData = await this.adapter.insert({
    "clientId": context.params?.clientId,
    "bookingId": context.params?.bookingId,
    "transactionType": constantUtil.PROFESSIONALTOLERENCEAMOUNT,
    "transactionAmount": context.params.amount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transId,
    "transactionReason": transactionReason,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": constantUtil.SUCCESS,
    "transactionDate": new Date(),
    "paymentType": constantUtil.CREDIT,
    "to": {
      "userType": constantUtil.PROFESSIONAL,
      "userId": context.params.professionalId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "from": {
      "userType": constantUtil.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData)
    return {
      "result": {
        "code": 0,
      },
    };
  return {
    "result": {
      "code": 1,
    },
  };
};

// transactionEvent.addBankDetailsInStripeAccount = async function (context) {
//   const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
//   if (!paymentOptionList) return "ERROR IN PAYMENT OPTION LIST";
//   let paymentData = {};
//   let finalTokenData = {};
//   let finalAccountData = {};
//   let finalBankAccountData = {};
//   Object.values(paymentOptionList)
//     .filter((option) => option.status === constantUtil.ACTIVE)
//     .forEach((option) => {
//       if (option.paymentType === constantUtil.PAYMENTCARD) {
//         paymentData = option;
//       }
//     });
//   if (paymentData.gateWay === constantUtil.STRIPE) {
//     let tokenJson = {};
//     const keyData = {};
//     if (paymentData["mode"] === constantUtil.SANDBOX) {
//       keyData["secretKey"] = paymentData["testSecretKey"];
//       keyData["publishableKey"] = paymentData["testPublishableKey"];
//     } else {
//       keyData["secretKey"] = paymentData["secretKey"];
//       keyData["publishableKey"] = paymentData["publishableKey"];
//     }
//     tokenJson = {
//       ...keyData,
//       "data": {},
//     };
//     tokenJson["data"]["bank_account"] = {};
//     tokenJson["data"]["bank_account"]["country"] = context.params.country
//       ? context.params.country
//       : "IN";
//     tokenJson["data"]["bank_account"]["currency"] = context.params.currency
//       ? context.params.currency
//       : "inr";
//     tokenJson["data"]["bank_account"]["account_holder_name"] =
//       context.params.accountName;
//     tokenJson["data"]["bank_account"]["account_holder_type"] = "individual";
//     tokenJson["data"]["bank_account"]["routing_number"] =
//       context.params.routingNumber;
//     tokenJson["data"]["bank_account"]["account_number"] =
//       context.params.accountNumber;
//     finalTokenData = await stripePaymentUtils.createTokenization(tokenJson);
//     if (finalTokenData) {
//       const accountJson = {
//         ...keyData,
//         "data": {
//           "type": "custom",
//           "country": "IN",
//           "business_type": "individual",
//           "capabilities": {
//             "card_payments": { "requested": true },
//             "transfers": { "requested": true },
//           },
//           "external_account": {
//             "country": context.params.country ? context.params.country : "IN",
//             "currency": context.params.currency
//               ? context.params.currency
//               : "inr",
//             "object": "bank_account",
//             "account_holder_type": "individual",
//             "account_number": context.params.accountNumber,
//             "routing_number": context.params.routingNumber,
//             "account_holder_name": context.params.accountName,
//           },
//           "tos_acceptance": {
//             "date": parseInt(Date.now() / 1000),
//             "ip": "60.243.36.44",
//             "user_agent":
//               "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
//           },
//           "individual": {
//             // 'address': {
//             //   'line1': '2000 Remington Pl',
//             //   'line2': 'Oklahoma City',
//             //   'city': 'Oklahoma City',
//             //   'country': 'US',
//             //   'postal_code': '73111',
//             //   'state': 'OK',
//             // },
//             "address": {
//               "line1": "3k, vantage plaza",
//               "line2": "near zaitoon",
//               "city": "chennai",
//               "country": "IN",
//               "postal_code": "600020",
//               "state": "TN",
//             },
//             "dob": {
//               "day": "02",
//               "month": "11",
//               "year": "1987",
//             },
//             "email": "derrick12e34@gmail.com",
//             "first_name": "derrick",
//             "last_name": "rose",
//             "phone": "+91 9898989898",
//           },
//         },
//       };
//       finalAccountData = await stripePaymentUtils.createCustomerStripeAccount(
//         accountJson
//       );
//       // console.log('finalAccountData', finalAccountData)
//       if (finalAccountData) {
//         const extAcountJson = {
//           ...keyData,
//           "customer": finalAccountData.id,
//           "data": { "external_account": finalTokenData.id },
//         };
//         finalBankAccountData = await stripePaymentUtils.createExternalAccount(
//           extAcountJson
//         );

//         // console.log('finalBankAccountData', finalBankAccountData)
//         return {
//           "bankAccountToken": finalTokenData.id,
//           "stripeConnectAccountId": finalAccountData.id,
//           "bankAccountId": finalBankAccountData.id,
//         };
//       }
//     }
//   } else {
//     return null;
//   }
// };

transactionEvent.verifyCard = async function (context) {
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let paymentData;
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
    }
  }
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction) {
    return {
      "code": 404,
      "data": "",
      "message": "TRANSACTION CANNOT PROCESS SOMETHING WENT WRONG",
    };
  }
  if (transaction.gatewayName === constantUtil.FLUTTERWAVE) {
    // validate

    const Flutterwave = require("flutterwave-node-v3");
    const flw = new Flutterwave(
      paymentData.testPublicKey,
      paymentData.testSecretKey
    );
    // two type otp and pin
    if (context.params.otp) {
      let cardValidate;
      try {
        cardValidate = await flw.Charge.validate({
          "otp": context.params.otp,
          "flw_ref": transaction.gatewayResponse.data.flw_ref,
        });
      } catch (error) {
        console.log(error);
        return error;
      }

      if (cardValidate.status === "error") {
        return { "code": 500, "data": null, "message": cardValidate.message };
      }

      this.broker.emit("transaction.refund", {
        "transactionId": transaction._id.toString(),
      });
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "gatewayResponse": cardValidate,
        }
      );
      if (!updateTransaction.nModified) {
        throw new MoleculerError(
          `SOMETHING WENT WRONG IN CARD VALIDATE SUCESS REPONSE UPDATING IN DB AND ID IS ${transaction._id}`,
          500
        );
      } else {
        return {
          "code": 200,
          "data": [],
          "message": `${transaction.gatewayResponse.data.currency} ${transaction.gatewayResponse.data.amount} WILL BE REFUNDED TO YOUR CARD WITHIN [5-15] BUSINESS DAYS`,
        };
      }
    }

    // if (context.params.pin) {

    // }
  }
  return {
    "code": 500,
    "data": "",
    "message":
      "NO CONDITION IS MATCHED SOMETHING WENT WRONG CHECK PAYEMENT REALTED DRATILES",
  };
};
// transaction refund
transactionEvent.refund = async function (context) {
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList)
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN PAYMENTDATA TAKING FROM REDDIS IN PAYEMNTOPTIONS LIST",
      500
    );
  let paymentData;
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
    }
  }
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction)
    throw new MoleculerError(
      `CANT FIND DATA FROM Transaction DETAILES MONGO ID IS ${context.params.transactionId}`,
      500
    );
  if (transaction.gatewayName === constantUtil.FLUTTERWAVE) {
    // validate

    const Flutterwave = require("flutterwave-node-v3");
    const flw = new Flutterwave(
      paymentData.testPublicKey,
      paymentData.testSecretKey
    );

    const refund = await flw.Transaction.refund({
      "id": transaction.gatewayResponse.data.id, //This is the transaction unique identifier. It is returned in the initiate transaction call as data.id
      "amount": transaction.gatewayResponse.data.amount,
    });
    if (refund.status === "error") {
      const res = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "$set": {
            "refundResponse": {
              "error": refund.message,
            },
          },
        }
      );
      if (!res)
        throw new MoleculerError(
          `SOMETHING WENT WRONG IN ${transaction.gatewayName} REFUND ERROR UPDATE `,
          500
        );
      return { "code": 500, "data": "", "message": refund.message };
    }
    // if its not getting error update the doucument
    const res = await this.adapter.mode.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "refundResponse": refund,
        },
      }
    );
    if (!res)
      throw new MoleculerError(
        `SOMETHING WENT WRONG IN ${transaction.gatewayName} REFUND SUCCESS UPDATE`,
        500
      );
    return {
      "code": 200,
      "data": refund,
      "message":
        "YOUR CARD VALID CHECKING AMOUNT WAS REFUNDED WITH 2-3 BUSINESSDAYS",
    };
  }
};
// trip end card caharge
transactionEvent.tripEndCardCharge = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let gatewayData;
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }

  const {
    USER_NOT_FOUND,
    SOMETHING_WENT_WRONG,
    INFO_CARD,
    INVALID_BOOKING,
    INFO_AMOUNT_PAID,
    ERROR_CARD_PAYMENT_FAILED,
    INFO_CARD_DECLINED,
    BANK_CREDET_MONEY,
    INFO_SUCCESS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);

  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      gatewayData = paymentOptionList[prop];

      break;
    }
  }
  // only user is use this event
  let userData = await this.broker.emit("user.getById", {
    "id": context.params.userId,
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      USER_NOT_FOUND + " " + SOMETHING_WENT_WRONG + " / WALLET RECHARGE",
      500
    );
  }
  // card detailes
  // const cardDetailes = userData.cards.find(
  //   (card) => card._id.toString() === context.params.cardId
  // )
  let cardDetails = await this.broker.emit("user.getCardDetailsById", {
    "userId": context.params.userId,
    "cardId": context.params.cardId,
  });

  cardDetails = cardDetails && cardDetails[0];
  if (!cardDetails) {
    throw new MoleculerError(INFO_CARD);
  }
  let bookingDetailes = await this.broker.emit(
    "booking.getBookingDetailsById",
    {
      "bookingId": context.params?.bookingId,
    }
  );
  bookingDetailes = bookingDetailes && bookingDetailes[0];
  if (!bookingDetailes) {
    throw new MoleculerError(INVALID_BOOKING, 500);
  }

  let finalResponse = {};
  let transactionType = constantUtil.BOOKINGCHARGE;
  let transactionStatus = constantUtil.PENDING;
  const stringTransactionReason = `${
    generalSettings.data.siteTitle
  }/${"Trip Charge"}/${"Trip Number : " + context.params?.refBookingId}`;
  let transactionId = `${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}-${context.params.reason || ""}`;
  const paymentType = bookingDetailes.payment.option;
  let paymentGateWay = gatewayData.gateWay;
  let captureAmount = 0;

  if (gatewayData.gateWay === constantUtil.FLUTTERWAVE) {
    let data;
    if (gatewayData.version === constantUtil.CONST_V3) {
      data = await this.broker.emit(
        "transaction.flutterwave.tripEndCardChargeV3",
        context.params
      );
    } else {
      data = await this.broker.emit(
        "transaction.flutterwave.tripEndCardCharge",
        context.params
      );
    }
    captureAmount = context.params.amount;
    return data && data[0];
  }
  const inputParams = {
    ...context.params,
    "paymentData": gatewayData,
    "userData": userData,
    "cardData": cardDetails,
    "paymentInitId": context.params?.paymentInitId,
    "generalSettings": generalSettings,
    "transactionReason": stringTransactionReason,
  };
  switch (gatewayData.gateWay) {
    case constantUtil.STRIPE:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        // if (context.params.amount <= context.params?.paymentInitAmount) {
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;
        finalResponse = await this.broker.emit(
          "transaction.stripe.capturePaymentIntentsTransaction",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "paymentInitId": context.params?.paymentInitId,
            "captureAmount": captureAmount,
          }
        );
        // } else {
        //   finalResponse = await this.broker.emit(
        //     // "transaction.stripe.cancelCaptureAndCreatePaymentTransaction",
        //     "transaction.stripe.updateCaptureAmountAndCapturePaymentTransaction",
        //     {
        //       ...context.params,
        //       "paymentData": gatewayData,
        //       "userData": userData,
        //       "cardData": cardDetails,
        //       "generalSettings": generalSettings,
        //       "transactionReason": stringTransactionReason,
        //       "paymentInitId": context.params?.paymentInitId,
        //       "paymentInitAmount": context.params?.amount,
        //     }
        //   );
        // }
      } else {
        finalResponse = await this.broker.emit(
          "transaction.stripe.tripEndCardCharge",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "cardData": cardDetails,
            "generalSettings": generalSettings,
            "transactionReason": stringTransactionReason,
          }
        );
      }
      break;

    case constantUtil.RAZORPAY:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        // if (context.params.amount <= context.params?.paymentInitAmount) {
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;
        if (context.params.amount >= context.params?.paymentInitAmount) {
          finalResponse = await this.broker.emit(
            "transaction.razorpay.capturePaymentIntentsTransaction",
            {
              ...context.params,
              "paymentData": gatewayData,
              "userData": userData,
              "paymentInitId": context.params?.paymentInitId,
              "captureAmount": context.params.paymentInitAmount,
            }
          );
        } else {
          finalResponse = await this.broker.emit(
            "transaction.razorpay.capturePaymentIntentsAndRefund",
            {
              ...context.params,
              "paymentData": gatewayData,
              "userData": userData,
              "paymentInitId": context.params?.paymentInitId,
              "captureAmount": context.params.paymentInitAmount,
              "refundAmount": parseFloat(
                (
                  context.params.paymentInitAmount - context.params.amount
                ).toFixed(2)
              ),
            }
          );
        }
      }
      break;

    case constantUtil.CONST_ZCREDITAPIARY:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;

        finalResponse = await this.broker.emit(
          "transaction.zcreditapiary.capturePaymentIntentsTransaction",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "cardData": cardDetails,
            "paymentInitId": context.params?.paymentInitId,
            "captureAmount": captureAmount,
            "transactionReason": stringTransactionReason,
          }
        );
        // // Collect Extra charge
        // if (context.params.amount > context.params?.paymentInitAmount) {
        //   const capturePaymentResponse = finalResponse && finalResponse[0];
        //   if (capturePaymentResponse.code === 200) {
        //     captureAmount =
        //       parseFloat(context.params.amount) -
        //       parseFloat(context.params?.paymentInitAmount);
        //     // const remainCaptureAmount = 10;
        //     finalResponse = await this.broker.emit(
        //       "transaction.zcreditapiary.tripEndCardCharge",
        //       {
        //         ...context.params,
        //         "paymentData": gatewayData,
        //         "userData": userData,
        //         "cardData": cardDetails,
        //         "generalSettings": generalSettings,
        //         "transactionReason": stringTransactionReason + " - Added Cost",
        //         "amount": captureAmount,
        //       }
        //     );
        //   }
        // }
      } else {
        finalResponse = await this.broker.emit(
          "transaction.zcreditapiary.tripEndCardCharge",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "cardData": cardDetails,
            "generalSettings": generalSettings,
            "transactionReason": stringTransactionReason,
          }
        );
      }
      break;

    case constantUtil.PEACH:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      transactionStatus = constantUtil.SUCCESS;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;
        // const reversalAmount =
        //   parseFloat(context.params.paymentInitAmount) -
        //   parseFloat(context.params.amount);

        finalResponse = await this.broker.emit(
          "transaction.peach.capturePaymentIntentsAndRefund",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "cardData": cardDetails,
            "paymentInitId": context.params?.paymentInitId,
            "paymentInitAmount": context.params.paymentInitAmount,
            "captureAmount": captureAmount,
            "transactionReason": stringTransactionReason,
            "currencyCode": generalSettings.data.currencyCode?.toUpperCase(),
          }
        );
        // if (reversalAmount > 0) {
        //   const notificationObject = {
        //     "clientId": userData?.clientId,
        //     "data": {
        //       "type": constantUtil.NOTIFICATIONTYPE,
        //       "userType": constantUtil.USER,
        //       "action": constantUtil.ACTION_WALLETTRANSACTIONUPDATE,
        //       "timestamp": Date.now(),
        //       "message": BANK_CREDET_MONEY,
        //       "details": lzStringEncode({}),
        //     },
        //     "registrationTokens": [
        //       {
        //         "token": userData.deviceInfo[0].deviceId,
        //         "id": userData._id.toString(),
        //         "deviceType": userData.deviceInfo[0].deviceType,
        //         "platform": userData.deviceInfo[0].platform,
        //         "socketId": userData.deviceInfo[0].socketId,
        //       },
        //     ],
        //   };
        //   /* SOCKET PUSH NOTIFICATION */
        //   this.broker.emit("socket.sendNotification", notificationObject);
        //   // this.broker.emit('socket.statusChangeEvent', notificationObject)
        //   /* FCM */
        //   this.broker.emit("admin.sendFCM", notificationObject);
        // }
        // // Collect Extra charge
        // if (context.params.amount > context.params?.paymentInitAmount) {
        //   const capturePaymentResponse = finalResponse && finalResponse[0];
        //   if (capturePaymentResponse.code === 200) {
        //     captureAmount =
        //       parseFloat(context.params.amount) -
        //       parseFloat(context.params?.paymentInitAmount);
        //     // const remainCaptureAmount = 10;
        //     finalResponse = await this.broker.emit(
        //       "transaction.zcreditapiary.tripEndCardCharge",
        //       {
        //         ...context.params,
        //         "paymentData": gatewayData,
        //         "userData": userData,
        //         "cardData": cardDetails,
        //         "generalSettings": generalSettings,
        //         "transactionReason": stringTransactionReason + " - Added Cost",
        //         "amount": captureAmount,
        //       }
        //     );
        //   }
        // }
      } else {
        cardDetails["data"]["cvv"] = bookingDetailes.payment.card.cvv;
        finalResponse = await this.broker.emit(
          "transaction.peach.tripEndCardCharge",
          {
            ...context.params,
            "paymentData": gatewayData,
            "userData": userData,
            "cardData": cardDetails,
            // "generalSettings": generalSettings,
            "transactionReason": stringTransactionReason,
            "currencyCode": generalSettings.data.currencyCode?.toUpperCase(),
          }
        );
      }
      break;

    case constantUtil.CONST_MERCADOPAGO:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        // if (context.params.amount <= context.params?.paymentInitAmount) {
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;
        finalResponse = await this.broker.emit(
          "transaction.mercadopago.capturePaymentIntentsTransaction",
          {
            ...inputParams,
            "captureAmount": captureAmount,
          }
        );
      } else {
        finalResponse = await this.broker.emit(
          "transaction.mercadopago.tripEndCardCharge",
          {
            ...inputParams,
          }
        );
      }
      break;

    case constantUtil.CONST_TEYA:
      transactionType = constantUtil.BOOKINGCHARGECARD;
      //If isPreAuthRequired is True
      if (gatewayData.isPreAuthRequired === true) {
        transactionId = context.params?.paymentInitId || transactionId;
        // if (context.params.amount <= context.params?.paymentInitAmount) {
        captureAmount =
          context.params.amount <= context.params?.paymentInitAmount
            ? context.params.amount
            : context.params.paymentInitAmount;
        // if (context.params.amount === context.params?.paymentInitAmount) {
        //   finalResponse = await this.broker.emit(
        //     "transaction.teya.capturePaymentIntentsTransaction",
        //     {
        //       ...inputParams,
        //       "amount": context.params.amount,
        //     }
        //   );
        // } else {
        finalResponse = await this.broker.emit(
          "transaction.teya.captureAndRefundPaymentIntentsTransaction",
          {
            ...inputParams,
            "paymentInitAmount": context.params.paymentInitAmount,
            "captureAmount": context.params.amount,
          }
        );
        // }
      } else {
        finalResponse = await this.broker.emit(
          "transaction.teya.tripEndCardCharge",
          {
            ...inputParams,
          }
        );
      }
      break;
  }
  finalResponse = finalResponse && finalResponse[0];
  if (!finalResponse) {
    finalResponse["code"] = 400;
    paymentGateWay = constantUtil.WALLET;
  }
  if (context.params?.requestFrom === "bookingV2") {
    // finalResponse["code"] = 201;
    finalResponse["transactionReason"] = stringTransactionReason;
    finalResponse["paymentGateWay"] = paymentGateWay;
    finalResponse["transactionId"] = transactionId;
    finalResponse["transactionAmount"] = captureAmount;
    finalResponse["transactionStatus"] = transactionStatus;
    finalResponse["cardDetails"] = cardDetails;
    finalResponse["gatewayDocId"] = gatewayData?._id;
    //
    return {
      "code": 201, // Success (Remaining Wallet Logic written in bookingV2 API )
      "message": INFO_AMOUNT_PAID,
      "data": finalResponse,
    };
    // throw new MoleculerError(INFO_SUCCESS, 201, "", finalResponse);
  }
  //--------------- db Insert Start------------
  const transaction = await this.adapter.model.create({
    "clientId": context.params.clientId,
    "transactionAmount": captureAmount,
    "previousBalance": null,
    "currentBalance": null,
    "transactionDate": new Date(),
    "transactionReason": stringTransactionReason,
    "refBookingId": context.params?.refBookingId,
    "from": {
      "userType": constantUtil.USER,
      "userId": context.params?.userId,
      "name": userData.firstName + userData.lastName,
      "phoneNumber": userData.phoneNumber,
    },
    "to": {
      "userType": constantUtil.ADMIN,
      "userId": null,
    },
    "paymentType": constantUtil.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
    "transactionType": transactionType,
    "transactionId": transactionId,
    "bookingId": context.params?.bookingId?.toString(),
    "gatewayName": paymentGateWay,
    "paymentGateWay": paymentGateWay,
    "gatewayDocId": gatewayData?._id,
    "transactionStatus": transactionStatus, // constantUtil.PROCESSING,
    "cardDetailes": cardDetails,
  });
  if (!transaction) {
    throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
  }
  //--------------- db Insert Start------------
  let responseMessage = {},
    isSendNotification = false,
    notificationMessage = null;
  let availableAmount = userData.wallet.availableAmount;
  // ---------------- Need to Remove Gatway Condition  End -----------------
  if (finalResponse.code === 200) {
    responseMessage = finalResponse?.message;
    const paymentTransactionId =
      finalResponse?.data?.transactionId?.toString() || transactionId;
    // updateDb
    const updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "transactionStatus": transactionStatus, // constantUtil.PROCESSING,
          "transactionId": paymentTransactionId,
          "gatewayResponse": finalResponse?.data?.successResponse || {},
        },
      }
    );
    if (!updateTransaction.nModified) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }

    //---------------- Payment Failed Start ------------------
    if (context.params.amount >= context.params?.paymentInitAmount) {
      // substract amount from user avilable amount
      const remainCaptureAmount = parseFloat(
        (context.params.amount - context.params?.paymentInitAmount).toFixed(2)
      );
      isSendNotification = true;
      notificationMessage = INFO_CARD_DECLINED; //Need to Change
      availableAmount = userData.wallet.availableAmount - remainCaptureAmount;

      let amountUpdateInUser = await this.broker.emit(
        "user.updateWalletAmount",
        {
          "userId": userData._id.toString(),
          "availableAmount": availableAmount,
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
          "isSendNotification": isSendNotification,
          "notificationMessage": notificationMessage,
        }
      );
      amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
      if (!amountUpdateInUser) {
        throw new MoleculerError(
          SOMETHING_WENT_WRONG + " IN WALLET TRANSACTION",
          500
        );
      }
      //--------------- db Insert Start------------
      transactionType = constantUtil.BOOKINGCHARGE;
      const transaction = await this.adapter.model.create({
        "clientId": context.params.clientId,
        "transactionAmount": remainCaptureAmount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": userData.wallet.availableAmount - remainCaptureAmount,
        "transactionReason": stringTransactionReason + " / ",
        "refBookingId": context.params?.refBookingId,
        "from": {
          "userType": constantUtil.USER,
          "userId": context.params?.userId,
          "name": userData.firstName + userData.lastName,
          "phoneNumber": userData.phoneNumber,
        },
        "to": {
          "userType": constantUtil.ADMIN,
          "userId": null,
        },
        "paymentType": constantUtil.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
        "transactionType": transactionType,
        "transactionId": transactionId,
        "bookingId": context.params?.bookingId?.toString(),
        "gatewayName": constantUtil.WALLET,
        "paymentGateWay": constantUtil.WALLET,
        "transactionStatus": constantUtil.SUCCESS,
      });
      if (!transaction) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      //--------------- db Insert Start------------
    }
    //---------------- Payment Failed END ------------------
    return {
      "code": 200,
      "message": INFO_AMOUNT_PAID,
      "data": {},
    };
  } else {
    responseMessage = ERROR_CARD_PAYMENT_FAILED;
    if (
      paymentType === constantUtil.CASH ||
      paymentType === constantUtil.CARD
    ) {
      responseMessage = INFO_CARD_DECLINED;
      isSendNotification = true;
      notificationMessage = INFO_CARD_DECLINED; //Need to Change
      availableAmount = userData.wallet.availableAmount - context.params.amount;
      // updateDb
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "$set": {
            "transactionAmount": context.params.amount,
            "previousBalance": userData.wallet.availableAmount,
            "currentBalance": availableAmount,
            "transactionStatus": constantUtil.SUCCESS,
            "gatewayName": constantUtil.WALLET, //If Card payment Failed
            "paymentGateWay": constantUtil.WALLET,
            "transactionType": constantUtil.BOOKINGCHARGE,
          },
        }
      );
      if (!updateTransaction.nModified) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      //---------------- Payment Failed Start ------------------
      // substract amount from user avilable amount
      let amountUpdateInUser = await this.broker.emit(
        "user.updateWalletAmount",
        {
          "userId": userData._id.toString(),
          "availableAmount": availableAmount,
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
          "isSendNotification": isSendNotification,
          "notificationMessage": notificationMessage,
        }
      );
      amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
      if (!amountUpdateInUser) {
        throw new MoleculerError(
          SOMETHING_WENT_WRONG + " IN WALLET TRANSACTION",
          500
        );
      }
      if (paymentType === constantUtil.CARD) {
        const inputParams = {
          ...context.params,
          "paymentData": gatewayData,
          "userData": userData,
          "paymentInitId": context.params?.paymentInitId,
        };
        switch (gatewayData.gateWay) {
          case constantUtil.STRIPE:
            // if (gatewayData.gateWay === constantUtil.STRIPE) {
            //If isPreAuthRequired is True
            if (gatewayData.isPreAuthRequired === true) {
              await this.broker.emit(
                "transaction.stripe.cancelCapturePaymentIntents",
                {
                  ...inputParams,
                }
              );
            }
            break;

          case CONST_MERCADOPAGO:
            await this.broker.emit(
              "transaction.mercadopago.cancelCapturePaymentIntents",
              {
                ...inputParams,
              }
            );
            break;
        }
      }
      //---------------- Payment Failed END ------------------
    }
    return {
      "code": 200,
      "message": responseMessage,
      "data": {
        "availableAmount": availableAmount,
      },
    };
  }
  //----------------------------------
};

// TO ADMIN

// refund
transactionEvent.getRefundDetails = async function (context) {
  const offset = context.params?.skip ?? 0;
  const limit = context.params?.limit ?? 20;
  const search = context.params?.search ?? "";

  const searchFields = [
    "from.phoneNumber",
    "from.userType",
    "transactionStatus",
    "cardDetailes.cardNumber",
    "gatewayName",
  ];
  const query = {
    "transactionType": constantUtil.VERIFYCARD,
    //"clientId": Mongoose.Types.ObjectId(context.params.clientId),
  };
  // based on userType
  if (context.params.userType !== "") {
    query["from.userType"] = context.params.userType.toUpperCase();
  }
  if (!!context.params?.filter === true) {
    // fileter option
    query["transactionStatus"] = context.params.filter;
  }
  // filter by time
  if (!!context.params.fromDate === true && !!context.params.toDate === true) {
    query["createdAt"] = {
      "$gte": new Date(new Date(context.params.fromDate).setHours(0, 0, 0, 0)),
      "$lt": new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      ),
    };
  }
  //
  const count = await this.adapter.count({
    "search": search,
    "searchFields": searchFields,
    "query": query,
  });
  const response = await this.adapter.find({
    "offset": offset,
    "limit": limit,
    "sort": "-_id",
    "search": search,
    "searchFields": searchFields,
    "query": query,
  });

  // take user name
  const getName = async (data) => {
    let userData;
    if (data.from.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": data.from.userId.toString(),
      });
      userData = userData && userData[0];
    }
    if (data.from.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": data.from.userId.toString(),
      });
    }
    userData = userData && userData[0];
    return `${userData?.firstName || ""} ${userData?.lastName || ""}`;
  };

  //
  const output = [];

  for (const data of response) {
    output.push({
      "_id": data._id,
      "name": data.from.name ? data.from.name : await getName(data),
      "cardNumber": data.cardDetailes.cardNumber,
      "time": data.createdAt,
      "amount": data.transactionAmount,
      "status": data.transactionStatus,
    });
  }

  return { "count": count, "response": output };
};
transactionEvent.adminGetWithDrawDetails = async function (context) {
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList)
    throw new MoleculerError(
      "CANT FIND PAYMENT OPTIONS LIST FROM DB REDIS PROBLEM",
      500
    );

  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      // SOME TIME NOT FOUND SO WE NEED TO ADD
      break;
    }
  }

  const offset = context.params?.skip ?? 0;
  const limit = context.params?.limit ?? 100;
  const search = context.params?.search ?? "";
  const searchFields = [
    "to.phoneNumber",
    "to.userType",
    "transactionStatus",
    `accountDetail.${paymentData?.isDefaultBankDisplayKeyInApp}`,
    "to.phoneNumber",
    "gatewayName",
  ];
  const query = {
    "transactionType": constantUtil.WALLETWITHDRAWAL,
    //"clientId": Mongoose.Types.ObjectId(context.params.clientId),
  };
  // based on userType
  if (context.params.userType !== "") {
    query["to.userType"] = context.params.userType.toUpperCase();
  }
  if (!!context.params?.filter === true) {
    // fileter option
    query["transactionStatus"] = context.params.filter;
  }
  // filter by time
  if (!!context.params.fromDate === true && !!context.params.toDate === true) {
    query["createdAt"] = {
      "$gte": new Date(new Date(context.params.fromDate).setHours(0, 0, 0, 0)),
      "$lt": new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      ),
    };
  }
  const count = await this.adapter.count({
    "search": search,
    "searchFields": searchFields,
    "query": query,
  });
  const response = await this.adapter.find({
    "offset": offset,
    "limit": limit,
    "sort": "-_id",
    "search": search,
    "searchFields": searchFields,
    "query": query,
  });
  // take user name
  const getDetails = async (data) => {
    let userData;
    if (data.to.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": data.to.userId.toString(),
      });
    } else if (data.to.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": data.to.userId.toString(),
      });
    }
    userData = userData && userData[0];
    return userData;
  };

  const output = [];
  for (let data of response) {
    data = data.toJSON();
    const userData = await getDetails(data);
    let bankDetails;
    if (data.to.userType === constantUtil.PROFESSIONAL) {
      // userData.bankDetails[""] = undefined;
      bankDetails = userData?.bankDetails || {};
    }
    if (data.to.userType === constantUtil.USER) {
      userData.bankDetails[0][""] = undefined;
      bankDetails = userData.bankDetails[0]
        ? userData.bankDetails[0]
        : userData.bankDetails;
    }

    // if (!!data?.to?.name === false) {
    //   userData = await getDetails(data)
    // }

    // userData = !!userData === true ? userData : await getDetails(data)
    // console.log(userData)

    // if (!!data?.to?.phoneNumber === false) {
    //   userData = !!userData === true ? userData : await getDetails(data)
    // }
    output.push({
      "_id": data._id,
      "name": data.to.name ? data.to.name : userData.firstName,
      "phoneNumber": data?.to?.phoneNumber ?? userData.phone.number,
      "userId": data.to.userId,
      "userType": data.to.userType,
      "time": data?.createdAt,
      "amount": data.transactionAmount,
      "status": data.transactionStatus,
      "payoutOption": data?.payoutOption ?? constantUtil.AUTOMATED,
      "bankDetails": bankDetails,
    });
  }

  return { "count": count, "response": output };
};

transactionEvent.getTransactionById = async function (context) {
  const data = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });

  return data?.toJSON() || null;
};
transactionEvent.changeTransactionStatus = async function (context) {
  console.log("sondij");
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": {
        "$in": context.params.ids.map((id) => Mongoose.Types.ObjectId(id)),
      },
    },
    { "transactionStatus": context.params.status }
  );
  console.log(responseJson);
  if (!responseJson.nModified) {
    return {
      "code": 400,
      "message": "SOMETHING WENT WRONG IN TRANASCTION STATUS CHANGE IN ADMIN",
      "data": {},
    };
  } else {
    return {
      "code": 200,
      "message": "UPDATE STATUS SUCCESSFULLY",
      "data": {},
    };
  }
};
// booking Events
transactionEvent.newBookingCardPayment = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let paymentOption,
    message = "";
  const finalResponse = {};

  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentOption = paymentOptionList[prop];
      break;
    }
  }

  //If isPreAuthRequired is True
  if (paymentOption.isPreAuthRequired === true) {
    let cardDetails = await this.broker.emit("user.getCardDetailsById", {
      "userId": context.params.userData._id.toString(),
      "cardId": context.params.card.cardId,
    });
    cardDetails = cardDetails && cardDetails[0];
    if (!cardDetails) {
      throw new MoleculerError("CANT FIND THE CARD DETAILS", 500);
    }

    // if (paymentOption.gateWay === constantUtil.STRIPE) {
    //   // finalResponse = await this.broker.emit(
    //   //   "transaction.stripe.createPaymentIntents",
    //   //   {
    //   //     ...context.params,
    //   //     "paymentData": paymentOption,
    //   //     "userData": context.params.userData,
    //   //     "cardData": cardDetailes,
    //   //     "generalSettings": generalSettings,
    //   //     "transactionReason":
    //   //       "User Ride Booking/Trip Number : " +
    //   //       context.params.bookingId.toString(),
    //   //   }
    //   // );
    //   return {
    //     "code": 200,
    //     "message": "IN STRIPE, AMOUNT TAKE BEFORE RIDE START",
    //     "data": {},
    //   };
    // } else if (paymentOption.gateWay === constantUtil.FLUTTERWAVE) {
    //   return {
    //     "code": 200,
    //     "message": "IN FLUTTER ONLY AMOUNT TAKED FROM END",
    //     "data": {},
    //   };
    // } else if (paymentOption.gateWay === constantUtil.RAZORPAY) {
    //   return {
    //     "code": 200,
    //     "message": "AMOUNT TAKE BEFORE RIDE START",
    //     "data": {},
    //   };
    // }
    // finalResponse = finalResponse && finalResponse[0];

    switch (paymentOption.gateWay) {
      case constantUtil.STRIPE:
        message = "IN STRIPE, AMOUNT TAKE BEFORE RIDE START";
        break;

      case constantUtil.FLUTTERWAVE:
        message = "IN FLUTTER ONLY AMOUNT TAKED FROM END";
        break;

      case constantUtil.RAZORPAY:
      case constantUtil.CONST_MERCADOPAGO:
      case constantUtil.CONST_TEYA:
        message = "AMOUNT TAKE BEFORE RIDE START";
        break;

      case constantUtil.CONST_ZCREDITAPIARY:
        message = "IN ZCREDITAPIARY, AMOUNT TAKE BEFORE RIDE START";
        break;
    }
    finalResponse["code"] = 200;
    finalResponse["data"] = {};
    finalResponse["data"]["paymentInitId"] = null;
    finalResponse["message"] = message;
  } else {
    finalResponse["code"] = 200;
    finalResponse["data"] = {};
    finalResponse["data"]["paymentInitId"] = null;
    finalResponse["message"] = "";
  }
  return finalResponse;
};
transactionEvent.membershipAddingTransaction = async function (context) {
  const transactionStatus = () => {
    if (
      [constantUtil.WALLET, constantUtil.CASH].includes(
        context.params.paymentType
      )
    ) {
      return constantUtil.SUCCESS;
    }
    if (context.transactionType === constantUtil.PAYMENTGATEWAY) {
      return constantUtil.PENDING;
    }
  };

  const tranasction = await this.adapter.model.create({
    "clientId": context.params.clientId,
    "previousBalance": null,
    "currentBalance": null,
    "transactionId": helperUtil.randomString(8, "a"),
    "transactionStatus": transactionStatus(),
    ...context.params,
  });
  if (!tranasction) {
    return {
      "code": 500,
      "message": "SOMETHING WENT WRONG",
    };
  }
  return tranasction;
};
transactionEvent.insertTransactionBasedOnUserType = async function (context) {
  let userData = {},
    toUserData = {},
    userId,
    errorCode = 200;

  const userType = (context.params?.userType ?? "").toUpperCase();
  const transactionId =
    context.params?.transactionId || `${helperUtil.randomString(8, "a")}`;
  const paymentToUserType =
    context.params?.paymentToUserType || constantUtil.ADMIN;

  if (userType === constantUtil.USER) {
    userId = context.params.userId.toString();
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId.toString(),
    });
    // userData = userData && userData[0];
    // userDetails = {
    //   "userType": userType,
    //   "userId": context.params.userId.toString(),
    //   "name": userData.firstName + " " + userData.lastName || null,
    //   "avatar": userData.avatar || null,
    //   "phoneNumber": userData.phone.number || null,
    // };
  } else if (userType === constantUtil.PROFESSIONAL) {
    userId = context.params.professionalId.toString();
    userData = await this.broker.emit("professional.getProfessionalDetails", {
      "phoneNumber": "00", //For Skip Required Field
      "professionalId": context.params.professionalId.toString(),
    });
    // userData = userData && userData[0];
    // userDetails = {
    //   "userType": userType,
    //   "professionalId": context.params.professionalId.toString(),
    //   "name": userData.firstName + " " + userData.lastName || null,
    //   "avatar": userData.avatar || null,
    //   "phoneNumber": userData.phone.number || null,
    // };
  } else if (userType === constantUtil.ADMIN) {
    if (context.params.userId) {
      userId = context.params.userId.toString();
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId.toString(),
      });
      // userData = userData && userData[0];
    } else if (context.params.professionalId) {
      userId = context.params.professionalId.toString();
      userData = await this.broker.emit("professional.getProfessionalDetails", {
        "phoneNumber": "00", //For Skip Required Field
        "professionalId": context.params.professionalId.toString(),
      });
      // userData = userData && userData[0];
    }
  }
  userData = userData && userData[0];
  if (paymentToUserType === constantUtil.USER) {
    toUserData = await this.broker.emit("user.getById", {
      "id": context.params.paymentToUserId.toString(),
    });
    toUserData = toUserData && toUserData[0];
  } else if (paymentToUserType === constantUtil.PROFESSIONAL) {
    toUserData = await this.broker.emit("professional.getProfessionalDetails", {
      "phoneNumber": "00", //For Skip Required Field
      "professionalId": context.params.paymentToUserId.toString(),
    });
    toUserData = toUserData && toUserData[0];
  }

  const insertTrasactionData = await this.adapter.model.create({
    "clientId": context.params.clientId,
    "transactionType": context.params.transactionType,
    "transactionAmount": context.params.transactionAmount,
    "previousBalance": context.params.previousBalance,
    "currentBalance": context.params.currentBalance,
    "paidTransactionId": null,
    "transactionId": transactionId,
    "transactionDate": new Date(),
    "transactionReason": context.params.transactionReason,
    "bookingId": context.params?.bookingId,
    "refBookingId": context.params?.refBookingId,
    "transactionStatus": context.params.transactionStatus,
    "paymentType": context.params.paymentType,
    "paymentGateWay": context.params.paymentGateWay,
    "gatewayName": context.params.paymentGateWay,
    "from": {
      "userType": userType,
      "userId": userType === constantUtil.ADMIN ? null : userId,
      "name":
        userType === constantUtil.ADMIN
          ? "Admin"
          : userData?.firstName + " " + userData?.lastName,
      "avatar": userData?.avatar || null,
      "phoneNumber": userData?.phone?.number || null,
    },
    "to": {
      "userType": paymentToUserType,
      "userId":
        paymentToUserType === constantUtil.ADMIN
          ? null
          : context.params.paymentToUserId,
      "name":
        paymentToUserType === constantUtil.ADMIN
          ? "Admin"
          : toUserData?.firstName + " " + toUserData?.lastName,
      "avatar": toUserData?.avatar || null,
      "phoneNumber": toUserData?.phone?.number || null,
    },
    "gatewayResponse": context.params.gatewayResponse || {},
    // "currentBalance": userData.wallet.availableAmount,
    "adminId": context.params.adminId, // For All Admin Model Ref Id
  });
  if (!insertTrasactionData) {
    errorCode = 400;
  }
  return {
    "result": {
      "code": errorCode,
    },
  };
};

transactionEvent.tigoWebhooking = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    userData = {};
  const {
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    WALLET_AMOUNT_LOADED,
    SOMETHING_WENT_WRONG,
    WALLET_NOTIFICATION,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);

  const transactionAmount = context.params?.transactionAmount || 0;
  const transactionStatus = (
    context.params?.transactionStatus || ""
  ).toUpperCase();
  console.log("webhooking : " + JSON.stringify(context.params));
  try {
    //#region user Details
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.professionalId,
      });
      // userData = userData && userData[0];
      // if (!userData) throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    } else if (context.params.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      // userData = userData && userData[0];
      // if (!userData) throw new MoleculerError(USER_NOT_FOUND, 500);
    }
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(
        context.params.userType === constantUtil.USER
          ? USER_NOT_FOUND
          : PROFESSIONAL_NOT_FOUND,
        500
      );
    }
    //#endregion user Details
    // add amount to userData
    // let amountUpdateInUser;
    if (
      context.params.userType === constantUtil.PROFESSIONAL &&
      transactionStatus === constantUtil.SUCCESS
    ) {
      await this.broker.emit("professional.updateWalletAmount", {
        "professionalId": context.params.professionalId.toString(),
        "availableAmount": userData.wallet.availableAmount + transactionAmount,
        "freezedAmount": userData.wallet.freezedAmount,
      });
    } else if (
      context.params.userType === constantUtil.USER &&
      transactionStatus === constantUtil.SUCCESS
    ) {
      await this.broker.emit("user.updateWalletAmount", {
        "userId": context.params.userId.toString(),
        "availableAmount": userData.wallet.availableAmount + transactionAmount,
        "freezedAmount": userData.wallet.freezedAmount,
        "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
      });
    }
    const transactionReason = constantUtil.MSG_WALLETRECHARGE;
    const transaction = await this.adapter.model.create({
      "clientId": context.params.clientId,
      "paymentType": constantUtil.CREDIT,
      "paymentGateWay": constantUtil.CONST_TIGO,
      "gatewayName": constantUtil.CONST_TIGO,
      "gatewayDocId": null,
      "transactionType": constantUtil.WALLETRECHARGE,
      "transactionReason": transactionReason,
      "from": {
        "phoneNumber": userData?.phone?.number,
        "userType": context.params.userType,
        "userId": userData._id.toString(),
      },
      "to": {
        "userType": constantUtil.ADMIN,
        "userId": null,
      },
      "transactionStatus":
        transactionStatus === constantUtil.SUCCESS
          ? constantUtil.SUCCESS
          : constantUtil.FAILED,
      // "cardDetailes": {
      //   "cardNumber": context.params.cardNumber,
      //   "expiryMonth": context.params.expiryMonth,
      //   "expiryYear": context.params.expiryYear,
      //   "holderName": context.params.holderName,
      // },
      "gatewayResponse": context.params.gatewayResponse,
      "transactionId": context.params.transactionId,
      "transactionDate": Date.now(),
      "transactionAmount": transactionAmount, // change dynamically
      "previousBalance": userData.wallet.availableAmount,
      "currentBalance": userData.wallet.availableAmount + transactionAmount,
    });
    if (!transaction) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    message = WALLET_AMOUNT_LOADED;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": {
      "availableAmount": userData.wallet.availableAmount + transactionAmount,
    },
  };
};

module.exports = transactionEvent;
