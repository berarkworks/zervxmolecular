const { MoleculerError, MoleculerClientError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { toRegionalUTC } = require("../utils/helper.util");

const logEvent = {};

logEvent.getNotificationBasedOnUserType = async function (context) {
  try {
    const userType = (context.params?.userType ?? "").toUpperCase();
    let matchQuery = {},
      groupQuery = {};
    if (userType === constantUtil.USER) {
      matchQuery = {
        "name": constantUtil.NOTIFICATIONTEMPLATES,
        "userType": userType,
        "userId": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      };
      groupQuery = {
        "_id": "$couponId",
        "couponId": { "$min": "$couponId" },
        "userId": { "$min": "$userId" },
        "count": { "$sum": 1 },
      };
    } else if (userType === constantUtil.PROFESSIONAL) {
      matchQuery = {
        "name": constantUtil.NOTIFICATIONTEMPLATES,
        "userType": userType,
        "professionalId": mongoose.Types.ObjectId(
          context.params?.professionalId?.toString()
        ),
      };
      groupQuery = {
        "_id": "$couponId",
        "couponId": { "$min": "$couponId" },
        "professionalId": { "$min": "$professionalId" },
        "count": { "$sum": 1 },
      };
    }
    // Join with Coupons(admins Collection) and and get Availed Coupons between Date
    const responseData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...matchQuery,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "notificationId",
          "foreignField": "_id",
          "as": "notificationList",
        },
      },
      {
        "$group": groupQuery,
      },
    ]);
    //---------------- For notification Log Start -------
    // const responseData = await this.adapter.model.aggregate([
    //   {
    //     "$lookup": {
    //       "from": "admins",
    //       "localField": "notificationId",
    //       "foreignField": "_id",
    //       "as": "notificationList",
    //     },
    //   },
    //   {
    //     "$project": {
    //       "notificationData": "$notificationList.data.pushNotifications",
    //     },
    //   },
    //   {
    //     "$unwind": "$notificationData",
    //   },
    //   {
    //     "$match": {
    //       "notificationData.status": {
    //         "$eq": "ACTIVE",
    //       },
    //     },
    //   },
    //   {
    //     "$unwind": "$notificationData",
    //   },
    //   {
    //     "$project": {
    //       "notificationId": "$_id",
    //       "_id": "$notificationData._id",
    //       "title": "$notificationData.title",
    //       "content": "$notificationData.content",
    //       "image": "$notificationData.image",
    //       "subject": "$notificationData.subject",
    //       "status": "$notificationData.status",
    //       "date": "$createdAt",
    //     },
    //   },
    // ]);
    //---------------- For notification Log End -------
    return responseData;
  } catch (e) {
    const x = e;
  }
};

logEvent.insertSignupLog = async function (context) {
  try {
    const userType = (context.params?.userType ?? "").toUpperCase();
    const todayDateTime = new Date();
    let isAllowInsertRecord = false;
    //-------------------------
    const matchData = {
      // "type": constantUtil.CONST_LOG,
      "name": constantUtil.LOGIN,
      "userType": userType,
      "logOutTime": null,
      "logDate": {
        "$gte": new Date(todayDateTime.setHours(0, 0, 0, 0)),
        "$lt": new Date(todayDateTime.setHours(23, 59, 59, 999)),
      },
    };
    if (userType === constantUtil.PROFESSIONAL) {
      matchData["professional"] = context.params?.professionalId?.toString();
    } else if (userType === constantUtil.USER) {
      matchData["user"] = context.params?.userId?.toString();
    } else {
      matchData["admin"] = context.params?.adminId?.toString();
      isAllowInsertRecord = true;
    }
    //-------------------------
    const checkRecord = await this.adapter.model
      .findOne(matchData)
      .sort({ "_id": -1 })
      .lean();
    if (checkRecord === null || isAllowInsertRecord) {
      const newManagement = await this.adapter.insert({
        "clientId": context.params.clientId || null,
        "name": constantUtil.LOGIN,
        "type": context.params?.type || constantUtil.CONST_LOG,
        "userType": userType,
        "logInTime": new Date(),
        "logDate": new Date(),
        "regionalLogDate": toRegionalUTC(new Date()),
        "admin": context.params?.adminId?.toString() || null,
        "user": context.params?.userId?.toString() || null,
        "professional": context.params?.professionalId?.toString() || null,
      });
      if (!newManagement) {
        throw new MoleculerError("SOMETHING WENT WORONG", 500);
      }
    }
    //Delete Log History (30 Days older Records)
    await this.adapter.model.deleteMany({
      "type": { "$in": [constantUtil.CONST_LOG, constantUtil.CONST_INCENTIVE] },
      // "name": { "$in": [constantUtil.LOGIN, constantUtil.LOGOUT] },
      "logDate": { "$lt": new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) }, //30 Days older Records
      "admin": context.params?.adminId?.toString() || null,
    });
  } catch (e) {
    const x = e;
  }
};

logEvent.updateSignupLog = async function (context) {
  try {
    const userType = (context.params?.userType ?? "").toUpperCase();
    const currentDateTime = new Date();
    const startDateTime = new Date();
    const endDateTime = new Date();
    //-------------------------
    const matchData = {
      // "type": constantUtil.CONST_LOG,
      "name": constantUtil.LOGIN,
      "userType": userType,
      "logOutTime": null,
      "logDate": {
        "$gte": new Date(startDateTime.setHours(0, 0, 0, 0)),
        "$lt": new Date(endDateTime.setHours(23, 59, 59, 999)),
      },
    };
    if (userType === constantUtil.PROFESSIONAL) {
      matchData["professional"] = context.params?.professionalId?.toString();
    } else if (userType === constantUtil.USER) {
      matchData["user"] = context.params?.userId?.toString();
    } else {
      matchData["admin"] = context.params?.adminId?.toString();
    }
    //-------------------------
    const selectedRecord = await this.adapter.model
      .findOne(matchData)
      .sort({ "_id": -1 })
      .lean();

    if (selectedRecord) {
      const diff = Math.abs(
        new Date(selectedRecord.logInTime) - currentDateTime
      );
      const minutes = parseInt(Math.floor(diff / 1000 / 60));
      const newManagement = await this.adapter.model
        .findOneAndUpdate(
          { "_id": selectedRecord._id },
          // matchData,
          {
            "$set": {
              "logOutTime": currentDateTime,
              "onlineMinutes": minutes,
            },
          },
          { "new": true }
        )
        .lean();
      if (!newManagement) {
        throw new MoleculerError("SOMETHING WENT WORONG", 500);
      }
    }
  } catch (e) {
    const x = e;
  }
};

logEvent.updateSignupLogByCronjob = async function () {
  try {
    const currentDateTime = new Date();
    const logList = await this.adapter.model.aggregate([
      {
        "$match": {
          // "type": constantUtil.CONST_LOG,
          "name": constantUtil.LOGIN,
          "logOutTime": null,
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$project": {
          "_id": "$_id",
          "userType": "$userType",
          "adminId": "$admin",
          "userId": "$user",
          "professionalId": "$professional._id", //"$professional",
          "professionalOnlineStatus": "$professional.onlineStatus",
          "onlineMinutes": {
            "$divide": [
              { "$subtract": [currentDateTime, "$logInTime"] },
              60000,
            ],
          },
        },
      },
    ]);
    //------------------------
    if (logList.length > 0) {
      logList.map(async (item) => {
        try {
          await this.adapter.model.updateOne(
            { "_id": item._id },
            {
              "$set": {
                "onlineMinutes": item.onlineMinutes,
                "logOutTime": currentDateTime,
              },
            }
          );
          //---------------- Incentive List Log Entry Start -------
          if (item.professionalOnlineStatus) {
            await this.broker.emit("log.insertSignupLog", {
              "type": constantUtil.CONST_INCENTIVE,
              "name": constantUtil.LOGIN,
              "userType": item.userType,
              "adminId": item.adminId?.toString() || null,
              "userId": item.userId?.toString() || null,
              "professionalId": item.professionalId?.toString() || null,
            });
          }
          //---------------- Incentive List Log Entry End -------
        } catch (e) {}
      });
    }
  } catch (e) {
    const x = e;
  }
};

logEvent.manageLog = async function (context) {
  try {
    const userType = (context.params?.userType ?? "").toUpperCase();
    const newManagement = await this.adapter.insert({
      "clientId": context.params.clientId || null,
      "name": context.params.name,
      "type": context.params.type || null,
      "userType": userType,
      "logDate": new Date(),
      "regionalLogDate": toRegionalUTC(new Date()),
      "admin": context.params?.adminId?.toString() || null,
      "userId": context.params?.userId?.toString() || null,
      "professionalId": context.params?.professionalId?.toString() || null,
      "reason": context.params.reason,
      "trackData": context.params.trackData,
    });

    if (!newManagement) {
      throw new MoleculerError("SOMETHING WENT WORONG", 500);
    }
    //Delete Log History (30 Days older Records)
    await this.adapter.model.deleteMany({
      "name": context.params.name,
      "type": context.params.type || null,
      "logDate": { "$lt": new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) }, //30 Days older Records
      "userType": userType,
    });
  } catch (e) {
    const x = e;
  }
};
logEvent.calculateOnlineMinutes = async function (context) {
  let loginData = [],
    userDetails = null;
  if (context.params.userType === constantUtil.USER) {
    userDetails = {
      "user": mongoose.Types.ObjectId(context.params.userId),
    };
  } else {
    userDetails = {
      "professional": mongoose.Types.ObjectId(context.params.professionalId),
    };
  }
  try {
    loginData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...userDetails,
          "logDate": {
            "$gte": new Date(context.params.startDate.setHours(0, 0, 0, 0)),
            "$lte": new Date(context.params.endDate.setHours(23, 59, 59, 999)),
          },
        },
      },
      {
        "$group": {
          "_id": null,
          "onlineMinutes": { "$sum": "$onlineMinutes" },
        },
      },
    ]);
  } catch (e) {
    const x = e;
  }
  return {
    "userType": context.params.userType,
    "userId": context.params.userId || null,
    "professionalId": context.params.professionalId || null,
    "onlineMinutes": loginData?.[0]?.onlineMinutes || 0,
  };
};
//---------------------
module.exports = logEvent;
