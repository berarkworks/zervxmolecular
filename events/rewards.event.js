const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");
//Utils
const constantUtil = require("../utils/constant.util");
const { convertToInt } = require("../utils/common.util");
//Mixin
const notifyMessage = require("../mixins/notifyMessage.mixin");

const rewardsEvent = {};

// rewardsEvent.getRewardListCountByUserType_Old_Working = async function (context) {
//   try {
//     const userType = (context.params?.userType ?? "").toUpperCase();
//     let matchQuery = {},
//       groupQuery = {};
//     if (userType === constantUtil.USER) {
//       matchQuery = {
//         "userType": userType,
//         "userId": mongoose.Types.ObjectId(context.params?.userId?.toString()),
//       };
//       groupQuery = {
//         "_id": "$couponId",
//         "couponId": { "$min": "$couponId" },
//         "userId": { "$min": "$userId" },
//         "count": { "$sum": 1 },
//       };
//     } else if (userType === constantUtil.PROFESSIONAL) {
//       matchQuery = {
//         "userType": userType,
//         "professionalId": mongoose.Types.ObjectId(
//           context.params?.professionalId?.toString()
//         ),
//       };
//       groupQuery = {
//         "_id": "$couponId",
//         "couponId": { "$min": "$couponId" },
//         "professionalId": { "$min": "$professionalId" },
//         "count": { "$sum": 1 },
//       };
//     }

//     const responseData = await this.adapter.model.aggregate([
//       {
//         "$match": matchQuery,
//       },
//       {
//         "$group": groupQuery,
//       },
//       {
//         "$sort": { "_id": -1 },
//       },
//     ]);
//     return responseData;
//   } catch (e) {
//     const x = e;
//   }
// };

rewardsEvent.getRewardListCountByUserType = async function (context) {
  try {
    const userType = (context.params?.userType ?? "").toUpperCase();
    let matchQuery = {},
      groupQuery = {};
    if (userType === constantUtil.USER) {
      matchQuery = {
        "userType": userType,
        "userId": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      };
      groupQuery = {
        "_id": "$couponId",
        "couponId": { "$min": "$couponId" },
        "userId": { "$min": "$userId" },
        "count": { "$sum": 1 },
      };
    } else if (userType === constantUtil.PROFESSIONAL) {
      matchQuery = {
        "userType": userType,
        "professionalId": mongoose.Types.ObjectId(
          context.params?.professionalId?.toString()
        ),
      };
      groupQuery = {
        "_id": "$couponId",
        "couponId": { "$min": "$couponId" },
        "professionalId": { "$min": "$professionalId" },
        "count": { "$sum": 1 },
      };
    }
    // Join with Coupons(admins Collection) and and get Availed Coupons between Date
    const responseData = await this.adapter.model.aggregate([
      {
        "$lookup": {
          "from": "admins",
          "localField": "couponId",
          "foreignField": "_id",
          "as": "couponList",
        },
      },
      {
        "$match": {
          ...matchQuery,
          "couponList.data.validFrom": { "$lte": new Date() },
          "couponList.data.validTo": { "$gte": new Date() },
        },
      },
      {
        "$group": groupQuery,
      },
    ]);
    return responseData;
  } catch (e) {
    const x = e;
  }
};

rewardsEvent.insertRewardByUserType = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let userId = "",
    professionalId = "",
    languageCode = "",
    userDetails = {};
  if (userType === constantUtil.USER) {
    userId = context.params?.userId?.toString();
    languageCode = context.meta?.userDetails?.languageCode;
    userDetails = { "userId": userId };
  } else if (userType === constantUtil.PROFESSIONAL) {
    professionalId = context.params?.professionalId?.toString();
    languageCode = context.meta?.professionalDetails?.languageCode;
    userDetails = { "professionalId": professionalId };
  }
  const { SOMETHING_WENT_WRONG } = await notifyMessage.setNotifyLanguage(
    languageCode
  );

  let rewardsData = await this.broker.emit("admin.getById", {
    "clientId": context.params?.clientId,
    "id": context.params?.couponId?.toString(),
  });
  rewardsData = rewardsData && rewardsData[0];

  const responseJson = await this.adapter.model.create({
    // "name": constantUtil.ERRORDATA,
    "clientId": context.params?.clientId,
    "userType": userType,
    "rideId": context.params?.rideId?.toString(),
    ...userDetails,
    "couponCode": rewardsData.data.couponCode,
    "rewardType": rewardsData.data.rewardType,
    "transactionType": constantUtil.CREDIT,
    "couponId": rewardsData?._id?.toString(),
    "pointsType": rewardsData.data.pointsType,
    "rewardPoints": 0, // Update After Coupon Scratch
    "expiredDate": rewardsData?.data?.expiryDate || new Date(),
    "isCouponScratched": false,
  });
  if (!responseJson) throw new MoleculerError(SOMETHING_WENT_WRONG);
  //--------- Update Reward Used Count Start ---------
  await this.broker.emit("admin.updateRewardUsedCount", {
    "clientId": context.params?.clientId,
    "couponId": context.params?.couponId?.toString(),
  });
  //--------- Update Reward Used Count End ---------
  return responseJson; // return first record only
};

rewardsEvent.insertRewardByUserTypeFromAdmin = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let userId = "",
    professionalId = "",
    languageCode = "",
    userDetails = {};
  if (userType === constantUtil.USER) {
    userId = context.params?.userId?.toString();
    languageCode = context.meta?.userDetails?.languageCode;
    userDetails = { "userId": userId };
  } else if (userType === constantUtil.PROFESSIONAL) {
    professionalId = context.params?.professionalId?.toString();
    languageCode = context.meta?.professionalDetails?.languageCode;
    userDetails = { "professionalId": professionalId };
  }
  const { SOMETHING_WENT_WRONG } = await notifyMessage.setNotifyLanguage(
    languageCode
  );
  const responseJson = await this.adapter.model.create({
    // "name": constantUtil.ERRORDATA,
    "clientId": context.params?.clientId,
    "userType": userType,
    ...userDetails,
    "rewardType": constantUtil.POINTS,
    "pointsType": constantUtil.FLAT,
    "transactionType": context.params?.transactionType,
    "rewardPoints": context.params?.rewardPoints,
    "expiredDate": new Date(),
    "isExpired": false,
    "isCouponScratched": true,
    "couponScratchedDate": new Date(),
    "createdId": context.params?.createdId,
    "reason": context.params?.reason,
  });
  if (!responseJson) {
    throw new MoleculerError(SOMETHING_WENT_WRONG);
  } else {
    return responseJson; // return first record only
  }
};

rewardsEvent.getRewardsForRideByUserType = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const rewardType = context.params?.rewardType;
  // const finalRewardsList = [];
  let languageCode,
    userId,
    professionalId,
    responseData,
    finalRewardsList = [];
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
    userId = context.params?.userId?.toString() || "";
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
    professionalId = context.params?.professionalId?.toString() || "";
  }
  const { INFO_NOT_FOUND, INVALID_BOOKING } =
    await notifyMessage.setNotifyLanguage(languageCode);

  //-------- Active Reward List -----------
  let activeRewardsList = await this.broker.emit(
    "admin.getActiveRewardsListBasedOnCityAndUserType",
    {
      "clientId": context.params?.clientId,
      "lng": context.params?.lng,
      "lat": context.params?.lat,
      "userType": userType,
      "rewardType": rewardType,
    }
  );
  activeRewardsList = activeRewardsList && activeRewardsList[0];
  if (!activeRewardsList) {
    throw new MoleculerError(INFO_NOT_FOUND);
  }
  //-------------------
  if (rewardType === constantUtil.PARTNERDEALS) {
    // get rewards count for User / Professional
    let usersRewardsList = await this.broker.emit(
      "rewards.getRewardListCountByUserType",
      {
        "clientId": context.params?.clientId,
        "userType": userType,
        "userId": userId,
        "professionalId": professionalId,
      }
    );
    usersRewardsList = usersRewardsList && usersRewardsList[0];
    if (!usersRewardsList) {
      throw new MoleculerError(INFO_NOT_FOUND);
    }
    // ----------- get ride Details Start -----------
    let bookingDetails = await this.broker.emit(
      "booking.getBookingDetailsById",
      {
        "clientId": context.params?.clientId,
        "bookingId": context.params?.rideId?.toString(),
      }
    );
    bookingDetails = bookingDetails && bookingDetails[0];
    if (!bookingDetails) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    // ----------- get ride Details End -----------
    //---------- Remove Used Rewards Start -----------------
    activeRewardsList.filter((rewards) => {
      const alreadyAppliedRewards = usersRewardsList.find(
        (data) => data.couponId.toString() === rewards._id.toString()
      );
      if (!alreadyAppliedRewards) {
        // Check usedCount & minimumEligible Ride Amount
        if (
          (rewards?.data?.usedCount ?? 0) < rewards.data.totalUsageCount &&
          rewards?.data?.minimumEligibleAmount <=
            bookingDetails.invoice.payableAmount
        ) {
          finalRewardsList.push(rewards);
        }
      }
    });
  } else {
    finalRewardsList = activeRewardsList || [];
  }
  //---------- Remove Used Rewards End -----------------
  // Insert Rewards
  if (finalRewardsList.length !== 0) {
    responseData = await this.broker.emit("rewards.insertRewardByUserType", {
      "clientId": context.params?.clientId,
      "userType": userType,
      "couponId": finalRewardsList[0]?._id?.toString(),
      "rideId": context.params?.rideId?.toString(),
      "userId": userId,
      "professionalId": professionalId,
    });
    responseData = responseData && responseData[0];
  }
  return responseData;
};

rewardsEvent.scratchRewardsById = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let languageCode,
    professionalId,
    userId,
    ridePoint = 0;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
    userId = context.params?.userId?.toString() || "";
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
    professionalId = context.params?.professionalId?.toString() || "";
  }
  const { ERROR_WALLET_POINT_ADDED } = await notifyMessage.setNotifyLanguage(
    languageCode
  );

  const rewardDetails = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params?.rewardId?.toString()) },
      {
        "isCouponScratched": 1,
        "couponCode": 1,
        "isExpired": 1,
        "expiredDate": 1,
        "pointsType": 1,
        "rewardType": 1,
      }
    )
    .populate("couponId", {
      "data.userType": 1,
      "data.rewardPoints": 1,
    })
    .populate("rideId", {
      "invoice.payableAmount": 1,
    })
    .lean();

  switch (rewardDetails?.pointsType?.toUpperCase()) {
    case constantUtil.FLAT:
      ridePoint = convertToInt(rewardDetails.couponId.data.rewardPoints);
      break;

    case constantUtil.PERCENTAGE:
      ridePoint = convertToInt(
        (rewardDetails.rideId.invoice.payableAmount / 100) *
          rewardDetails.couponId.data.rewardPoints
      );
      break;
  }
  if (rewardDetails.rewardType === constantUtil.POINTS) {
    //-------- Update Wallet Point Start ----------------------
    if (userType === constantUtil.USER) {
      await this.broker.emit("user.updateWalletRewardPoint", {
        "clientId": context.params?.clientId,
        "userId": userId,
        "rewardPoints": ridePoint,
      });
    } else if (userType === constantUtil.PROFESSIONAL) {
      await this.broker.emit("professional.updateWalletRewardPoint", {
        "clientId": context.params?.clientId,
        "professionalId": professionalId,
        "rewardPoints": ridePoint,
      });
    }
    //-------- Update Wallet Point End ----------------------
  }
  // ------------- Response Data Start ----------
  const responseData = await this.adapter.model.findOneAndUpdate(
    { "_id": mongoose.Types.ObjectId(context.params?.rewardId?.toString()) },
    {
      "isCouponScratched": true,
      "couponScratchedDate": new Date(),
      "rewardPoints": ridePoint,
    },
    { "new": true }
  );
  if (!responseData) {
    throw new MoleculerError(ERROR_WALLET_POINT_ADDED);
  } else {
    return responseData;
  }
};

module.exports = rewardsEvent;
