const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const subscriptionEvent = {};

// ADMIN MODULES APIS
subscriptionEvent.getSubscriptionList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {
    //"clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  query["status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["name"],
  });
  const subscriptionData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-_id",
    "search": search,
    "searchFields": ["name"],
  });
  responseJson["count"] = count;
  responseJson["response"] = subscriptionData.map((subscrip) => {
    return {
      "_id": subscrip._id,
      "name": subscrip.name,
      "price": subscrip.price,
      "expireDays": subscrip.expireDays,
      "expireInText": subscrip.expireInText,
      "markAsPopular": subscrip.markAsPopular,
      "upgradeOption": subscrip.upgradeOption,
      "currencyCode": subscrip.currencyCode,
      "currencySymbol": subscrip.currencySymbol,
      "status": subscrip.status,
    };
  });
  return { "code": 1, "data": responseJson, "message": "" };
};

subscriptionEvent.getEditSubscription = async function (context) {
  const jsonData = await this.adapter.model
    .findOne({
      "_id": context.params.id,
    })
    .lean();
  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "NO RECORD FOUND" };
  } else {
    return { "code": 1, "data": jsonData, "message": "" };
  }
};

subscriptionEvent.changeSubscriptionStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "status": context.params.status }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

subscriptionEvent.updateSubscription = async function (context) {
  const jsonData = await this.adapter.model
    .findOne({
      "name": context.params.name,
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  if (context.params.id) {
    if (jsonData && jsonData._id.toString() !== context.params.id.toString()) {
      return { "code": 0, "data": {}, "message": "SUBSCRIPTION ALREADY EXIST" };
    } else {
      const responseJson = await this.adapter.updateById(
        mongoose.Types.ObjectId(context.params.id.toString()),
        context.params
      );
      return { "code": 1, "data": responseJson, "message": "" };
    }
  } else {
    if (jsonData) {
      return { "code": 0, "data": {}, "message": "SUBSCRIPTION ALREADY EXIST" };
    } else {
      const responseJson = await this.adapter.insert(context.params);
      return { "code": 1, "data": responseJson, "message": "" };
    }
  }
};
//----------------------------
module.exports = subscriptionEvent;
