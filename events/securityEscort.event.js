const { customAlphabet } = require("nanoid");
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const securityEscortEvent = {};

securityEscortEvent.getUnViewedData = async function (context) {
  const responseJson = await this.adapter.model
    .find(
      {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "isviewed": false,
        "status": constantUtil.NEW,
      },
      {
        "_id": 1,
        "userType": 1,
        "createdAt": 1,
        "alertType": 1,
        "status": 1,
        "priority": 1,
      }
    )
    .sort({ "createdAt": -1 })
    .limit(5)
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    });

  return (
    responseJson.map((data) => {
      return {
        "_id": data._id,
        //"clientId": context.params.clientId,
        "createdAt": data.createdAt,
        "alertType": data.alertType,
        "status": data.status,
        "firstName":
          data.userType === constantUtil.USER
            ? data.user?.firstName
            : data.professional?.firstName,
        "lastName":
          data.userType === constantUtil.USER
            ? data.user?.lastName
            : data.professional?.lastName,
        "phone":
          data.userType === constantUtil.USER
            ? data.user?.phone
            : data.professional?.phone,
      };
    }) || []
  );
};

securityEscortEvent.getSecurityList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const userTypefilterData =
    (context.params?.userTypefilter || "") !== ""
      ? {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$eq": context.params.userTypefilter },
        }
      : {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$in": [constantUtil.USER, constantUtil.PROFESSIONAL] },
        };
  const priorityFilterData =
    (context.params?.priorityFilter || "") !== ""
      ? { "priority": { "$eq": context.params.priorityFilter } }
      : {
          "priority": {
            "$in": [
              constantUtil.NOTASSIGNED,
              constantUtil.CRITICAL,
              constantUtil.MEDIUM,
              constantUtil.MINIMAL,
            ],
          },
        };
  const statusfilterData =
    (context.params?.filter || "") !== ""
      ? { "status": { "$eq": context.params.filter } }
      : {
          "status": {
            "$in": [constantUtil.NEW, constantUtil.ATTENDED],
          },
        };

  const searchQuery = {
    "$match": {
      "$or": [
        {
          "booking.bookingId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  const query = [];
  query.push({
    "$match": {
      ...statusfilterData,
      ...userTypefilterData,
      ...priorityFilterData,
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "userType": 1,
            "_id": 1,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isOperatorAssign": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "userType": 1,
            "priority": 1,
            "ticketId": 1,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "alertDate": 1,
            "status": 1,
            "isResponseOfficerAssign": "$isEscortApply",
            "threadMessage": "$threadMessage",
            "user": "$user",
            "professional": "$professional",
            "operator": "$operator",
            "officer": "$officer",
          },
        },
      ],
      "count": [
        { "$sort": { "_id": -1 } },
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "userType": 1,
            "_id": 1,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isOperatorAssign": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        {
          "$project": {
            "userType": 1,
            "_id": 1,
            "createdAt": 1,
          },
        },
        { "$count": "all" },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData?.[0]?.count?.[0]?.all || 0;
  responseJson["response"] =
    jsonData[0].response.map((data) => {
      return {
        "_id": data._id,
        "createdAt": data.createdAt,
        "alertType": data.alertType,
        "priority": data.priority,
        "status": data.status,
        "adminNotes": data.adminNotes,
        "threadMessage": data.threadMessage,
        "securityServiceName": data.securityServiceName,
        "alertDate": data.alertDate,
        "securityThreadId": data.securityThreadId,
        "isResponseOfficerAssign": data.isEscortApply,
        "isOperatorAssign":
          data.operator && data.operator !== null ? true : false,
        "operator":
          data.operator && data.operator !== null
            ? {
                "firstName": data.operator.data.firstName,
                "lastName": data.operator.data.lastName,
                "email": data.operator.data.email,
              }
            : {},
        "responseOfficer":
          data.officer && data.officer !== null
            ? {
                "firstName": data.officer.firstName,
                "lastName": data.officer.lastName,
                "email": data.officer.email,
              }
            : {},
        "userType": data.userType,
        "firstName":
          data.userType === constantUtil.USER
            ? data.user.firstName
            : data.professional.firstName,
        "lastName":
          data.userType === constantUtil.USER
            ? data.user.lastName
            : data.professional.lastName,
        "phone":
          data.userType === constantUtil.USER
            ? data.user.phone
            : data.professional.phone,
        "booking":
          data.booking && data.booking !== null ? data.booking.bookingId : null,
      };
    }) || [];

  return responseJson;
};

securityEscortEvent.getSecurityClosedList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const userTypefilterData =
    (context.params?.userTypefilter || "") !== ""
      ? {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$eq": context.params.userTypefilter },
        }
      : {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$in": [constantUtil.USER, constantUtil.PROFESSIONAL] },
        };
  const priorityFilterData =
    (context.params?.priorityFilter || "") !== ""
      ? { "priority": { "$eq": context.params.priorityFilter } }
      : {
          "priority": {
            "$in": [
              constantUtil.NOTASSIGNED,
              constantUtil.CRITICAL,
              constantUtil.MINIMAL,
              constantUtil.MEDIUM,
            ],
          },
        };
  const query = [];
  query.push({
    "$match": {
      "status": { "$eq": constantUtil.CLOSED },
      ...userTypefilterData,
      ...priorityFilterData,
    },
  });
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "ticketId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "response": [
        {
          "$group": {
            "_id": "$ticketId",
            "userType": { "$last": "$userType" },
            "createdAt": { "$last": "$createdAt" },
            "alertType": { "$last": "$alertType" },
            "adminNotes": { "$last": "$adminNotes" },
            "priority": { "$last": "$priority" },
            "alertDate": { "$last": "$alertDate" },
            "status": { "$last": "$status" },
            "isEscortApply": { "$last": "$isEscortApply" },
            "officer": { "$last": "$officer" },
            "operator": { "$last": "$operator" },
            "professional": { "$last": "$professional" },
            "user": { "$last": "$user" },
            "booking": { "$last": "$booking" },
            "threadMessage": { "$push": "$threadMessage" },
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "ticketId": "$_id",
            "userType": 1,
            "_id": 0,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        { "$sort": { "createdAt": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "userType": 1,
            "priority": 1,
            "ticketId": 1,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": 1,
            "user.firstName": 1,
            "user.lastName": 1,
            "user.email": 1,
            "user.phone.code": 1,
            "user.phone.number": 1,
            "professional.firstName": 1,
            "professional.lastName": 1,
            "professional.email": 1,
            "professional.phone.code": 1,
            "professional.phone.number": 1,
            "officer.firstName": 1,
            "officer.lastName": 1,
            "officer.email": 1,
            "officer.phone.code": 1,
            "officer.phone.number": 1,
            "operator.firstName": "$operator.data.firstName",
            "operator.lastName": "$operator.data.lastName",
            "operator.email": "$operator.data.email",
            "operator.phone.code": "$operator.data.phone.code",
            "operator.phone.number": "$operator.data.phone.number",
          },
        },
      ],
      "count": [
        { "$sort": { "_id": -1 } },
        {
          "$group": {
            "_id": "$ticketId",
            "userType": { "$last": "$userType" },
            "createdAt": { "$last": "$createdAt" },
            "alertType": { "$last": "$alertType" },
            "adminNotes": { "$last": "$adminNotes" },
            "priority": { "$last": "$priority" },
            "alertDate": { "$last": "$alertDate" },
            "status": { "$last": "$status" },
            "isEscortApply": { "$last": "$isEscortApply" },
            "officer": { "$last": "$officer" },
            "operator": { "$last": "$operator" },
            "professional": { "$last": "$professional" },
            "user": { "$last": "$user" },
            "booking": { "$last": "$booking" },
            "threadMessage": { "$push": "$threadMessage" },
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "ticketId": "$_id",
            "userType": 1,
            "_id": 0,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        { "$count": "all" },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData?.[0]?.count?.[0]?.all || 0;
  responseJson["response"] = jsonData?.[0]?.response || [];

  return responseJson;
};

securityEscortEvent.getSecurityOperatorList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const userTypefilterData =
    (context.params?.userTypefilter || "") !== ""
      ? {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$eq": context.params.userTypefilter },
        }
      : {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "userType": { "$in": [constantUtil.USER, constantUtil.PROFESSIONAL] },
        };
  const statusfilterData =
    (context.params?.statusfilter || "") !== ""
      ? { "status": { "$eq": context.params.statusfilter } }
      : {
          "status": { "$in": [constantUtil.CLOSED, constantUtil.ATTENDED] },
        };
  const priorityFilterData =
    context.params.priorityFilter && context.params.priorityFilter != ""
      ? { "priority": { "$eq": context.params.priorityFilter } }
      : {
          "priority": {
            "$in": [
              constantUtil.NOTASSIGNED,
              constantUtil.CRITICAL,
              constantUtil.MINIMAL,
              constantUtil.MEDIUM,
            ],
          },
        };
  const query = [];
  query.push({
    "$match": {
      "operator": mongoose.Types.ObjectId(context.params.adminId.toString()),
      ...statusfilterData,
      ...userTypefilterData,
      ...priorityFilterData,
    },
  });
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "ticketId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "user.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "operator.data.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "officer.email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "response": [
        {
          "$group": {
            "_id": "$ticketId",
            "userType": { "$last": "$userType" },
            "createdAt": { "$last": "$createdAt" },
            "alertType": { "$last": "$alertType" },
            "adminNotes": { "$last": "$adminNotes" },
            "priority": { "$last": "$priority" },
            "alertDate": { "$last": "$alertDate" },
            "status": { "$last": "$status" },
            "isEscortApply": { "$last": "$isEscortApply" },
            "officer": { "$last": "$officer" },
            "operator": { "$last": "$operator" },
            "professional": { "$last": "$professional" },
            "user": { "$last": "$user" },
            "booking": { "$last": "$booking" },
            "threadMessage": { "$push": "$threadMessage" },
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "ticketId": "$_id",
            "userType": 1,
            "_id": 0,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        { "$sort": { "createdAt": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "userType": 1,
            "priority": 1,
            "ticketId": 1,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": 1,
            "user.firstName": 1,
            "user.lastName": 1,
            "user.email": 1,
            "user.phone.code": 1,
            "user.phone.number": 1,
            "professional.firstName": 1,
            "professional.lastName": 1,
            "professional.email": 1,
            "professional.phone.code": 1,
            "professional.phone.number": 1,
            "officer.firstName": 1,
            "officer.lastName": 1,
            "officer.email": 1,
            "officer.phone.code": 1,
            "officer.phone.number": 1,
            "operator.firstName": "$operator.data.firstName",
            "operator.lastName": "$operator.data.lastName",
            "operator.email": "$operator.data.email",
            "operator.phone.code": "$operator.data.phone.code",
            "operator.phone.number": "$operator.data.phone.number",
          },
        },
      ],
      "count": [
        { "$sort": { "_id": -1 } },
        {
          "$group": {
            "_id": "$ticketId",
            "userType": { "$last": "$userType" },
            "createdAt": { "$last": "$createdAt" },
            "alertType": { "$last": "$alertType" },
            "adminNotes": { "$last": "$adminNotes" },
            "priority": { "$last": "$priority" },
            "alertDate": { "$last": "$alertDate" },
            "status": { "$last": "$status" },
            "isEscortApply": { "$last": "$isEscortApply" },
            "officer": { "$last": "$officer" },
            "operator": { "$last": "$operator" },
            "professional": { "$last": "$professional" },
            "user": { "$last": "$user" },
            "booking": { "$last": "$booking" },
            "threadMessage": { "$push": "$threadMessage" },
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "operator",
            "foreignField": "_id",
            "as": "operator",
          },
        },
        {
          "$lookup": {
            "from": "bookings",
            "localField": "booking",
            "foreignField": "_id",
            "as": "booking",
          },
        },
        {
          "$project": {
            "ticketId": "$_id",
            "userType": 1,
            "_id": 0,
            "createdAt": 1,
            "alertType": 1,
            "adminNotes": 1,
            "priority": 1,
            "alertDate": 1,
            "status": 1,
            "isEscortApply": 1,
            "threadMessage": "$threadMessage",
            "user": { "$arrayElemAt": ["$user", 0] },
            "professional": { "$arrayElemAt": ["$professional", 0] },
            "officer": { "$arrayElemAt": ["$officer", 0] },
            "booking": { "$arrayElemAt": ["$booking", 0] },
            "operator": { "$arrayElemAt": ["$operator", 0] },
          },
        },
        searchQuery,
        { "$count": "all" },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData?.[0]?.count?.[0]?.all || 0;
  responseJson["response"] = jsonData?.[0]?.response || [];

  return responseJson;
};

securityEscortEvent.operatorAcceptSecurity = async function (context) {
  const ticketId = `${customAlphabet(
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    8
  )()}`;
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");

  if (responseJson.user !== null) {
    await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": constantUtil.NEW,
      },
      {
        "operator": context.params.adminId,
        "status": constantUtil.ATTENDED,
        "ticketId": ticketId,
      }
    );

    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.user._id,
      });
  } else if (responseJson.professional !== null) {
    await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": constantUtil.NEW,
      },
      {
        "operator": context.params.adminId,
        "status": constantUtil.ATTENDED,
        "ticketId": ticketId,
      }
    );

    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.professional._id,
      });
  }
  if (responseJson.nModified === 0) return { "code": 0 };

  this.broker.emit("socket.sendOperatorAcceptedNotification", {
    "id": context.params.id,
  });

  return { "code": 1, "response": "Updated Successfully" };
};
securityEscortEvent.changeSecurityStatus = async function (context) {
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 });
  if (!responseJson) return { "code": 0 };
  if (responseJson.userType === constantUtil.USER) {
    await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": constantUtil.ATTENDED,
      },
      {
        "status": constantUtil.CLOSED,
      }
    );
    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.user._id.toString(),
      });

    if (responseJson.officer && responseJson.officer._id !== null)
      await this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson.officer._id.toString(),
        "type": constantUtil.AWAITING,
      });

    await this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": responseJson.user._id.toString(),
      "isEscortClosed": true,
    });
  } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
    await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": constantUtil.ATTENDED,
      },
      {
        "status": constantUtil.CLOSED,
      }
    );
    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.professional._id.toString(),
      });
    if (responseJson.officer && responseJson.officer._id !== null)
      await this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson.officer._id.toString(),
        "type": constantUtil.AWAITING,
      });
    await this.broker.emit("professional.updateOnGoingSecurityBooking", {
      "id": responseJson.professional._id.toString(),
      "isEscortClosed": true,
    });
  }
  if (responseJson.nModified === 0) return { "code": 0 };

  return { "code": 1, "response": "Updated Successfully" };
};
securityEscortEvent.addSecurityNotes = async function (context) {
  let responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!responseJson) return { "code": 0 };
  if (responseJson.userType === constantUtil.USER) {
    responseJson = await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
      },
      {
        "adminNotes": context.params.notes,
      }
    );
    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.user._id,
      });
  } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
    responseJson = await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
      },
      {
        "adminNotes": context.params.notes,
      }
    );
    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.professional._id,
      });
  }
  if (responseJson.nModified === 0) return { "code": 0 };

  return { "code": 1, "response": "Updated Successfully" };
};

securityEscortEvent.getOfficerRatingList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);
  const query = [];
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "escortStatus": constantUtil.ENDED,
      "officer": mongoose.Types.ObjectId(context.params.officerId),
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "acknowledge": "$acknowledge",
            "escortStatus": "$escortStatus",
            "priority": "$priority",
            "alertDate": "$alertDate",
            "alertType": "$alertType",
            "alertLocation": "$alertLocation",
            "fromLocation": "$fromLocation",
            "toLocation": "$toLocation",
            "activity": "$activity",
            "escortReview": "$escortReview",
            "isAdminForceAssign": "$isAdminForceAssign",
            "securityThreadId": "$securityThreadId",
            "ticketId": "$ticketId",
            "userType": "$userType",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
              "avatar": "$user.avatar",
            },
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return jsonData?.[0]?.response || [];
};
module.exports = securityEscortEvent;
