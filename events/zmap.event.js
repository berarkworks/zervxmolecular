const { MoleculerError, MoleculerClientError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { toRegionalUTC } = require("../utils/helper.util");

const zmapEvent = {};

zmapEvent.manageData = async function (context) {
  try {
    this.adapter.model.create({
      "data": context.params.data,
      "countryCode": context.params.countryCode,
      "mapType": context.params.mapType,
      "mapServiceType": context.params.mapServiceType,
      "location": context.params.location,
    });
  } catch (e) {
    const x = e;
  }
};

module.exports = zmapEvent;
