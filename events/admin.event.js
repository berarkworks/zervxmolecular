const FCM = require("fcm-node");
const fs = require("fs");
const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const nodeMailer = require("nodemailer");
const axios = require("axios");
const { trim } = require("lodash");
const { customAlphabet } = require("nanoid");
const { google } = require("googleapis");
const moment = require("moment");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
// const { _valueFinder } = require("../utils/helper.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");
const { getDbQueryString } = require("../utils/common.util");

const adminEvent = {};

// TOKEN EVENT
adminEvent.verifyToken = async function (context) {
  const adminData = await this.adapter.model
    .findOne({
      "_id": context.params.adminId,
      "data.accessToken": context.params.accessToken,
    })
    .lean();

  if (context.params?.deviceId && context.params.socketId !== "") {
    await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.adminId.toString()),
      { "data.socketId": context.params.socketId }
    );
  }

  return adminData;
};
// OTHER EVENTS
adminEvent.sendSMS = async function (context) {
  // const generalSetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const { data } = await storageUtil.read(constantUtil.SMSSETTING);
  const whatsupData = await storageUtil.read(constantUtil.WHATSUPSETTING);
  const clientId = context.params.clientId;

  //  #region Get Config Settings
  //  #endregion Get Config Settings

  let url = "",
    headers = {},
    config = {},
    jsonInputData = {},
    responseData = {};
  if (data.mode === constantUtil.PRODUCTION) {
    const formUrlEncoded = (inputData) =>
      Object.keys(inputData).reduce(
        (queryParmaData, item) =>
          queryParmaData + `&${item}=${encodeURIComponent(inputData[item])}`,
        ""
      );
    if (context.params.notifyType === constantUtil.OTP) {
      const otpReceiverAppName =
        context.params.otpReceiverAppName === constantUtil.WHATSUP
          ? constantUtil.WHATSUP
          : data.smsType;
      let otpMessage =
        context.params?.template?.content || context.params.message;
      // switch (data.smsType) {
      switch (otpReceiverAppName) {
        case constantUtil.TWILIO:
          //#region Send SMS Using Gatway
          if (data.isEnableSMSGatwayOTP) {
            if (!context.params.from) {
              context.params.from = data.accountNumber || data.twilioNumber; //Need to remove data.twilioNumber
            }
            // `we send message from ${generalSetting.data.siteTitle} message is ${context.params.message} be safe this message is confidential `
            try {
              const smsInfo = await this.twilio.messages.create({
                "to": context.params.phoneNumber,
                "from": context.params.from,
                // "body":
                //   context.params?.template?.content ??
                //   `Seu codigo OTP Starmov é: ${context.params.message}`,
                // "body":
                //   context.params?.template?.content || context.params.message,
                "body": otpMessage,
              });
              responseData = smsInfo;
            } catch (error) {
              console.log("error from twilo message", error);
            }
          }
          //#endregion Send SMS Using Gatway
          //#region Send SMS Using WhatsUp
          if (data.isEnableWhatsupOTP) {
            this.broker.emit("booking.sendWhatsupAlert", {
              "clientId": context.params?.clientId,
              "rideId": null,
              "bookingFrom": constantUtil.ADMIN,
              "notifyType": constantUtil.NONE,
              "notifyFor": constantUtil.ALL,
              "alertType": constantUtil.OTP,
              // "message":
              //   context.params?.template?.content || context.params.message,
              "message": otpMessage,
            });
          }
          //#endregion Send SMS Using WhatsUp
          break;

        case constantUtil.TUNISIESMS:
          //#region Send SMS Using Gatway
          if (data.isEnableSMSGatwayOTP) {
            jsonInputData = {
              "fct": "sms",
              "key": data.authToken,
              "mobile": context.params.phoneNumber,
              "sms": otpMessage,
              "sender": data.senderName,
              "date": moment(new Date()).format("DD/MM/YYYY"),
              "heure": moment(new Date()).format("h:mm"),
              "expire": "300",
            };
            const urlLink =
              "https://api.l2t.io/tn/v0/api/api.aspx?fct=sms&key=" +
              jsonInputData.key +
              "&mobile=" +
              jsonInputData.mobile +
              "&sms=" +
              jsonInputData.sms +
              "&sender=" +
              jsonInputData.sender +
              "&date=" +
              jsonInputData.date +
              "&heure=" +
              jsonInputData.heure +
              "";
            config = {
              "method": "GET",
              "url": urlLink,
              // "port": 443,
              "headers": {},
            };
            try {
              const response = await axios(config);
              responseData = response?.data;
            } catch (e) {
              responseData = null;
            }
          }
          //#endregion Send SMS Using Gatway
          break;

        case constantUtil.WHATSUP:
          this.broker.emit("booking.sendWhatsupAlert", {
            "clientId": context.params?.clientId,
            "rideId": null,
            "bookingFrom": constantUtil.ADMIN,
            "notifyType": constantUtil.NONE,
            "notifyFor": constantUtil.ALL,
            "alertType": constantUtil.OTP,
            // "message":
            //   context.params?.template?.content || context.params.message,
            "message": otpMessage,
          });
          // // `we send message from ${generalSetting.data.siteTitle} message is ${context.params.message} be safe this message is confidential `
          // try {
          //   jsonInputData = {
          //     // "from": smsSetting.data.senderID,
          //     // "sender": smsSetting.data.senderName,
          //     // "to":
          //     //   bookingDetails.bookingFor.phoneCode.toString() +
          //     //   bookingDetails.bookingFor.phoneNumber.toString(),
          //     // // "+251911237975",
          //     // "message": message,

          //     "messaging_product": "whatsapp",
          //     "recipient_type": "individual",
          //     "to": context.params.phoneNumber,
          //     // "to": "+917373598575",
          //     // // "+251911237975",
          //     "type": "text",
          //     "text": {
          //       "preview_url": false,
          //       "body":
          //         context.params?.template?.content || context.params.message,
          //     },
          //   };
          //   url =
          //     "https://graph.facebook.com/v16.0/" +
          //     whatsupData.data.accountNumber + // Phone Account id
          //     "/messages"; //Original
          //   // url = "https://graph.facebook.com/v16.0/115441128234449/messages"; //need to Change url
          //   headers = {
          //     "Content-Type": "application/json",
          //     "Authorization": "Bearer " + whatsupData.data.authToken,
          //     // "Authorization":
          //     //   "Bearer EAAKzCDUdLqsBAFHQmYkYLhM02QJoaELL6CtrppqqpOp8Vs9qLnDXNkoBbeUi97Jcb1Vg2G7tkWOYtw6NsBDCZCZBN7cyy5MtDKVW5dpzQHJsz2cDWrvxFVpTxJ0DuUJW4X5uZCH9ZBLwHvw7OuzoFJZCXzTKZAGE8aggZBzZB4pUvINFpf8h0ZC6nuvcrgtm2z7ZBA9TbGpDQ169xAl6V31M1I",
          //   };
          //   config = {
          //     "method": "post",
          //     "url": url,
          //     "headers": headers,
          //     // "data": jsonInputData,
          //     "data": {
          //       "messaging_product": "whatsapp",
          //       "recipient_type": "individual",
          //       "to": context.params.phoneNumber,
          //       // "to": "+917373598575",
          //       // // "+251911237975",
          //       "type": "text",
          //       "text": {
          //         "preview_url": false,
          //         "body":
          //           context.params?.template?.content || context.params.message,
          //       },
          //     },
          //   };
          //   const response = await axios(config);
          //   responseData = response?.data;
          // } catch (error) {
          //   console.log("error from whatsup message", error);
          // }
          break;

        case constantUtil.SENDIFY:
          headers = {
            "Content-Type": "application/x-www-form-urlencoded",
          };
          //#region Send SMS Using WhatsUp
          if (data.isEnableWhatsupOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "whatsapp",
              // "message":
              //   context.params?.template?.content || context.params.message,
              "message": otpMessage,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "account": data.accountSid,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/otp", // OTP
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
              responseData = response?.data;
            } catch (e) {
              responseData = null;
            }
          }
          //#endregion Send SMS Using WhatsUp
          //#region Send SMS Using Device Sim Network
          if (data.isEnableDeviceNetworkOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "sms",
              // "message":
              //   context.params?.template?.content || context.params.message,
              "message": otpMessage,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "mode": "devices",
              "priority": 1,
              "sim": data.senderID,
              "device": data.accountNumber,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/sms", // SMS
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
              responseData = response?.data;
            } catch (e) {
              responseData = null;
            }
          }
          //#endregion Send SMS Using Device Sim Network
          //#region Send SMS Using Gatway
          if (data.isEnableSMSGatwayOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "sms",
              // "message":
              //   context.params?.template?.content || context.params.message,
              "message": otpMessage,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "mode": "credits",
              "gateway": data.accessToken,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/sms", // SMS
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
              responseData = response?.data;
            } catch (e) {
              responseData = null;
            }
          }
          //#endregion Send SMS Using Gatway
          break;
      }
      // switch (otpReceiverAppName) {
      //   case constantUtil.AFRO:
      //     config = {
      //       "method": "post",
      //       "url": url,
      //       // "port": 443,
      //       "headers": headers,
      //       "data": jsonInputData,
      //     };
      //     try {
      //       const response = await axios(config);
      //       responseData = response?.data;
      //     } catch (e) {
      //       return "error";
      //     }
      //     break;
      // }
      return responseData;
    } else {
      const notifySMSSetting = await storageUtil.read(
        constantUtil.NOTIFYSMSSETTING
      );
      const promotionSMSSetting = await storageUtil.read(
        constantUtil.PROMOTIONSMSSETTING
      );
      const emergencySMSSetting = await storageUtil.read(
        constantUtil.EMERGENCYSMSSETTING
      );
      let smsType = null;
      switch (context.params.notifyType) {
        case constantUtil.OTP:
          smsType = data.smsType;
          break;

        case constantUtil.PROMOTION:
          smsType = promotionSMSSetting.data.smsType;
          break;

        case constantUtil.EMERGENCY:
          smsType = emergencySMSSetting.data.smsType;
          break;

        case constantUtil.WHATSUP:
          smsType = whatsupData.data.smsType;
          break;

        default:
          smsType = notifySMSSetting.data.smsType;
          break;
      }
      //
      switch (smsType) {
        case constantUtil.TWILIO:
          {
            try {
              const smsInfo = await this.twilio.messages.create({
                "to": context.params.phoneNumber,
                "from": data.accountNumber || data.twilioNumber, // Need to remove data.twilioNumber
                "body": context.params.message,
              });
              return smsInfo;
            } catch (error) {
              console.log("error from twilo message", error);
            }
          }
          break;

        case constantUtil.TUNISIESMS:
          //#region Send SMS Using Gatway
          if (data.isEnableSMSGatwayOTP) {
            jsonInputData = {
              "fct": "sms",
              "key": data.authToken,
              "mobile": context.params.phoneNumber,
              "sms": otpMessage,
              "sender": data.senderName,
              "date": moment(new Date()).format("DD/MM/YYYY"),
              "heure": moment(new Date()).format("h:mm"),
              "expire": "300",
            };
            const urlLink =
              "https://api.l2t.io/tn/v0/api/api.aspx?fct=sms&key=" +
              jsonInputData.key +
              "&mobile=" +
              jsonInputData.mobile +
              "&sms=" +
              jsonInputData.sms +
              "&sender=" +
              jsonInputData.sender +
              "&date=" +
              jsonInputData.date +
              "&heure=" +
              jsonInputData.heure +
              "";
            config = {
              "method": "GET",
              "url": urlLink,
              // "port": 443,
              "headers": {},
            };
            try {
              const response = await axios(config);
              responseData = response?.data;
            } catch (e) {
              responseData = null;
            }
          }
          //#endregion Send SMS Using Gatway
          break;

        case constantUtil.SENDIFY:
          headers = {
            "Content-Type": "application/x-www-form-urlencoded",
          };
          //#region Send SMS Using WhatsUp
          if (data.isEnableWhatsupOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "whatsapp",
              "message": context.params.message,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "account": data.accountSid,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/otp", // OTP
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
            } catch (e) {
              console.log(e.message);
            }
          }
          //#endregion Send SMS Using WhatsUp
          //#region Send SMS Using Device Sim Network
          if (data.isEnableDeviceNetworkOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "sms",
              "message": context.params.message,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "mode": "devices",
              "priority": 1,
              "sim": data.senderID,
              "device": data.accountNumber,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/sms", // SMS
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
            } catch (e) {
              console.log(e.message);
            }
          }
          //#endregion Send SMS Using Device Sim Network
          //#region Send SMS Using Gatway
          if (data.isEnableSMSGatwayOTP) {
            jsonInputData = {
              "secret": data.authToken,
              "type": "sms",
              "message": context.params.message,
              "phone": context.params.phoneNumber,
              "expire": "300",
              "mode": "credits",
              "gateway": data.accessToken,
            };
            config = {
              "method": "post",
              "url": "https://sendify.com.br/api/send/sms", // SMS
              // "port": 443,
              "headers": headers,
              "data": formUrlEncoded(jsonInputData),
            };
            try {
              const response = await axios(config);
            } catch (e) {
              console.log(e.message);
            }
          }
          //#endregion Send SMS Using Gatway
          break;
      }
    }
  }
};

adminEvent.sendMail = async function (context) {
  // const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #region Get Config Settings
  //  #endregion Get Config Settings
  // console.log('yes i am here')
  const { data } = await storageUtil.read(constantUtil.SMTPSETTING);

  if (!data || !data.host || !data.port || !data.client || !data.secret) {
    return this.logger.error("INVALID SMTP CONFIG");
  } else {
    const nodeMailerSetup = nodeMailer.createTransport({
      "host": data.host,
      "port": data.port,
      "secure": false,
      "auth": {
        "user": data.client,
        "pass": data.secret,
      },
      "tls": {
        "rejectUnauthorized": false,
      },
    });
    nodeMailerSetup.sendMail(context.params, (err, info) => {
      console.log(err);
      return { "err": err, "info": info };
    });
    return true;
  }
};

// adminEvent.sendFCM_Old = async function (context) {
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   const keyData = generalSettings.data.firebaseAdmin;
//   await context.params.registrationTokens.forEach((data) => {
//     const agentType = data.platform.split("/")[0].toUpperCase();
//     // let serverKey;
//     // if (context.params.data.userType === constantUtil.PROFESSIONAL) {
//     //   if (agentType === constantUtil.ANDROID) {
//     //     serverKey = keyData.androidDriver;
//     //   } else {
//     //     serverKey = keyData.iosDriver;
//     //   }
//     // }
//     // if (context.params.data.userType === constantUtil.USER) {
//     //   if (agentType === constantUtil.ANDROID) {
//     //     serverKey = keyData.androidUser;
//     //   } else {
//     //     serverKey = keyData.iosUser;
//     //   }
//     // }

//     const serverKey =
//       context.params.data.userType === constantUtil.PROFESSIONAL
//         ? agentType === constantUtil.ANDROID
//           ? keyData.androidDriver
//           : keyData.iosDriver
//         : agentType === constantUtil.ANDROID
//         ? keyData.androidUser
//         : keyData.iosUser;

//     // console.log("this data from push", context.params.data);

//     // console.log("this fcm id ", data.token);
//     let message;

//     try {
//       // message = {
//       //   "to": data.token,
//       //   "direct_boot_ok": true,
//       //   "priority": "high",
//       //   "content_available": true,
//       //   "sound": "default",
//       //   "notification": {
//       //     "title": generalSettings.data.siteTitle || "title",
//       //     "body": context.params.data.message
//       //       ? context.params.data.message.toString()
//       //       : "",
//       //     "image": context.params?.data?.image || "",
//       //     "sound": "default",
//       //     "priority": "high",
//       //     "android_channel_id": "default_1234567",
//       //   },
//       //   "data": {
//       //     "data": context.params.data,
//       //     "priority": "high",
//       //     "sound": "default",
//       //   },
//       // };

//       const body = (context?.params?.data?.message || "").toString("utf8");
//       const image = (context?.params?.data?.image || "").toString("utf8");
//       const title =
//         generalSettings?.data?.siteTitle?.toString("utf8") || "title";

//       message = {
//         "to": data.token,
//         "priority": "high",
//         "content_available": true,
//         "sound": "default",
//         "data": {
//           "data": context.params.data,
//           "title": title,
//           "body": body,
//           "image": image,
//           "sound": "default",
//           "color": "#FF0000",
//           "priority": "high",
//           "android_channel_id": "default_1234567",

//           //
//           "notification_priority": "PRIORITY_MAX",
//           "default_sound": true,
//           "default_vibrate_timings": true,
//           "default_light_settings": true,
//           "vibrate_timings": ["3.5s"],
//           "visibility": "PUBLIC",

//           "notification_count": 1,
//           "notification": {
//             "title": title,
//             "body": body,
//             "image": image,
//             "sound": "default",
//             "color": "#FF0000",
//             "priority": "high",
//             "android_channel_id": "default_1234567",

//             //
//             "notification_priority": "PRIORITY_MAX",
//             "default_sound": true,
//             "default_vibrate_timings": true,
//             "default_light_settings": true,
//             "vibrate_timings": ["3.5s"],
//             "visibility": "PUBLIC",

//             "notification_count": 1,
//           },
//         },
//         // "notification": {
//         //   "title": title,
//         //   "body": body,
//         //   "image": image,
//         //   "sound": "default",
//         //   "color": "#FF0000",
//         //   "priority": "high",
//         //   "android_channel_id": "default_1234567",
//         //   "click_action": "/",
//         //   //
//         //   "notification_priority": "PRIORITY_MAX",
//         //   "default_sound": true,
//         //   "default_vibrate_timings": true,
//         //   "default_light_settings": true,
//         //   "vibrate_timings": ["3.5s"],
//         //   "visibility": "PUBLIC",

//         //   "notification_count": 1,
//         // },
//         // // new
//         "android": {
//           // "priority": "HIGH",
//           "priority": "high",
//           "data": {
//             "data": context.params.data,
//           },
//           "notification": {
//             "title": title,
//             "body": body,
//             "icon": "",
//             "color": "#FF0000",
//             "sound": "default",
//             "tag": "",

//             "body_loc_key": "",
//             "body_loc_args": [""],
//             "title_loc_key": "",
//             "title_loc_args": [""],
//             "channel_id": "default_1234567",
//             "ticker": "",
//             "sticky": "",
//             "event_time": "",
//             "local_only": "",
//             "notification_priority": "PRIORITY_MAX",
//             "default_sound": true,
//             "default_vibrate_timings": true,
//             "default_light_settings": true,
//             "vibrate_timings": ["3.5s"],
//             "visibility": "PUBLIC",

//             "notification_count": 1,

//             "image": image,
//           },
//           "fcm_options": {},
//           "direct_boot_ok": true,
//         },
//         "webpush": {},
//         "apns": {
//           "to": data.token,
//           "priority": "high",
//           // "content_available": true,
//           "sound": "default",
//           "payload": {
//             "aps": {
//               "sound": "default",
//             },
//           },
//         },
//         "fcm_options": {},
//       };

//       if (
//         context?.params?.temprorySound === true &&
//         agentType !== constantUtil.ANDROID
//       ) {
//         message.sound = "AppRideRequest.caf";
//         message.notification.sound = "AppRideRequest.caf";
//         message.data.sound = "AppRideRequest.caf";
//         message.apns.payload.aps.sound = "AppRideRequest.caf";
//       }

//       //
//     } catch (error) {
//       console.log("error from fcm parsing", error);
//     }
//     console.log("this is server key", serverKey);
//     //set your server key here
//     const fcm = new FCM(serverKey);

//     // "fcm-node": "^1.3.0",
//     fcm.send(message, function (err, response) {
//       if (err) {
//         console.log("Something has gone wrong!", err);
//       }
//       // if (response) console.log("this is fcm response", response);
//     });
//   });
// };

adminEvent.sendFCM = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const keyData = generalSettings.data.firebaseAdmin;
  //--------------------------
  const fcmSettings = await storageUtil.read(constantUtil.FIREBASESETTING);
  //  #region Get Config Settings
  const fcmAccessTokenDetails = await storageUtil.read(
    constantUtil.FCMACCESSTOKEN
  );
  //  #endregion Get Config Settings
  //--------------------------
  const getAccessToken = () => {
    return new Promise(function (resolve, reject) {
      // const key = require("../placeholders/service-account.json");
      const jwtClient = new google.auth.JWT(
        fcmSettings.data.firebaseNotifyConfig.client_email,
        null,
        fcmSettings.data.firebaseNotifyConfig.private_key,
        ["https://www.googleapis.com/auth/firebase.messaging"], // SCOPES
        null
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
  };
  let accessToken = null;
  if (context?.params?.accessToken || fcmAccessTokenDetails?.[0]?.accessToken) {
    accessToken =
      fcmAccessTokenDetails?.[0]?.accessToken || context?.params?.accessToken;
  } else {
    accessToken = await getAccessToken();
  }
  const projectId = fcmSettings.data.firebaseNotifyConfig.project_id;
  //-----------------------------
  await context.params.registrationTokens.forEach(async (data) => {
    const agentType = data.platform.split("/")[0].toUpperCase();

    // console.log("this fcm id ", data.token);
    let message;
    try {
      const body = (context?.params?.data?.message || "").toString("utf8");
      const image = (context?.params?.data?.image || "").toString("utf8");
      const title =
        generalSettings?.data?.siteTitle?.toString("utf8") || "title";

      message = {
        "message": {
          // "to": data.token,
          "token": data.token,
          // "priority": "high",
          // "content_available": true,
          // "sound": "default",
          // -----------------
          // "notification": {
          //   "title": title,
          //   "body": body,
          //   "image": image,

          //   // "sound": "default",
          //   // "color": "#FF0000",
          //   // "priority": "high",
          //   // "android_channel_id": "default_1234567",
          //   //
          //   // "notification_priority": "PRIORITY_MAX",
          //   // "default_sound": true,
          //   // "default_vibrate_timings": true,
          //   // "default_light_settings": true,
          //   // "vibrate_timings": ["3.5s"],
          //   // "visibility": "PUBLIC",
          //   // "notification_count": 1,
          //   // "story_id": "notify_1234567", // "story_12345",
          //   // "channel_id": "notify_1234567", // "default_1234567",
          // },
          // -----------------
          "data": {
            ...context.params.data,
            "timestamp": context.params.data.timestamp?.toString(),
            "title": title,
            "body": body,
            "image": image,
            "sound": "default",
            "priority": "HIGH",
            "story_id": "notify_1234567", // "story_12345",
          },
          "android": {
            "priority": "HIGH", // "HIGH",
            "ttl": "10s",
            // "notification": {
            //   "title": title,
            //   "body": body,
            //   "image": image,
            //   "channel_id": "notify_1234567", // "default_1234567",
            //   // "click_action": ".SplashActivity",
            //   // "click_action": getIntent().getExtras(),
            //   "icon": "",
            //   "color": "#FF0000",
            //   "sound": "default",
            //   // "tag": "",
            //   // "body_loc_key": "",
            //   // "body_loc_args": [""],
            //   // "title_loc_key": "",
            //   // "title_loc_args": [""],
            //   "ticker": "",
            //   // "sticky": "",
            //   // "event_time": "",
            //   // "event_time": new Date().toISOString(),
            //   // "local_only": "",
            //   "notification_priority": "PRIORITY_MAX",
            //   "default_sound": true,
            //   "default_vibrate_timings": true,
            //   "default_light_settings": true,
            //   "vibrate_timings": ["3.5s"],
            //   "visibility": "PUBLIC",
            //   "notification_count": 1,
            // },
            // "fcm_options": {},
            // "direct_boot_ok": true,
          },
          // "webpush": {},
          "apns": {
            // "to": data.token,
            // "priority": "high",
            // "content_available": true,
            // "sound": "default",
            "payload": {
              "aps": {
                "sound": "default",
                "priority": "HIGH",
              },
            },
          },
          // "fcm_options": {},
        },
      };
      // if (
      //   // context?.params?.temprorySound === true &&
      //   agentType !== constantUtil.ANDROID
      // ) {
      //   // message.sound = "AppRideRequest.caf";
      //   // message.message.android["sound"] = "AppRideRequest.caf";
      //   message.message.data["sound"] = "AppRideRequest.caf";
      //   message.message.apns.payload.aps.sound = "AppRideRequest.caf";
      // }
      if (
        agentType !== constantUtil.ANDROID &&
        agentType !== constantUtil.CONST_ANDROID_OS
      ) {
        message.message["notification"] = {
          "title": title,
          "body": body,
          "image": image,

          // "sound": "default",
          // "color": "#FF0000",
          // "priority": "high",
          // "android_channel_id": "default_1234567",
          //
          // "notification_priority": "PRIORITY_MAX",
          // "default_sound": true,
          // "default_vibrate_timings": true,
          // "default_light_settings": true,
          // "vibrate_timings": ["3.5s"],
          // "visibility": "PUBLIC",
          // "notification_count": 1,
          // "story_id": "notify_1234567", // "story_12345",
          // "channel_id": "notify_1234567", // "default_1234567",
        };
      }
    } catch (error) {
      console.log("error from fcm parsing", error);
    }
    // //set your server key here
    // this.fcmNotify.send(message, function (err, response) {
    //   if (err) {
    //     console.log("Something has gone wrong!", err);
    //   }
    // });
    const headers = {
      "Authorization": "Bearer " + accessToken, // data.token,
      "Content-Type": "application/json",
    };
    config = {
      "method": "post",
      "url":
        "https://fcm.googleapis.com/v1/projects/" +
        projectId +
        "/messages:send/",
      "headers": headers,
      "data": message,
    };
    try {
      const response = await axios(config);
      const responseData = response.data;
      //   return {
      //     "code": 200,
      //     "data": { "projectId": projectId, "accessToken": accessToken },
      //     "message": "",
      //   };
    } catch (e) {
      const xxx = e.message;
      console.log("FCM Error_" + xxx);
      // return {
      //   "code": 200,
      //   "data": { "projectId": null, "accessToken": null },
      //   "message": e.message,
      // };
    }
  });
  return {
    "code": 200,
    "data": { "projectId": projectId, "accessToken": accessToken },
    "message": "",
  };
};

// adminEvent.fcmBulkSend = async function (context) {
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   const keyData = generalSettings.data.firebaseAdmin;

//   const agentType = data.platform.split("/")[0].toUpperCase();
//   let serverKey;
//   if (context.params.data.userType === constantUtil.PROFESSIONAL) {
//     if (agentType === constantUtil.ANDROID) {
//       serverKey = keyData.androidDriver;
//     } else {
//       serverKey = keyData.iosDriver;
//     }
//   }
//   if (context.params.data.userType === constantUtil.USER) {
//     if (agentType === constantUtil.ANDROID) {
//       serverKey = keyData.androidUser;
//     } else {
//       serverKey = keyData.iosUser;
//     }
//   }
//   const message = {
//     "registration_ids": [context.params.egistrationIds],
//     "priority": "high",
//     "content_available": true,
//     "notification": {
//       "title": generalSettings.data.siteTitle || "title",
//       "body": context.params.data.message
//         ? context.params.data.message.toString()
//         : "",
//       "image": context.params?.data?.image || "",
//       "sound": "default",
//     },
//     "data": {
//       "data": context.params.data,
//     },
//   };
//   const fcm = new FCM(serverKey);
//   try {
//     const response = fcm.send(message);
//     console.log(response);
//   } catch (error) {
//     console.log("error", error);
//   }
//   return {
//     "code": 200,
//     "message": "SUCCESS",
//     "data": "",
//   };
// };

adminEvent.getProfassionalDocument = async function (context) {
  const documentsList = await this.adapter.model
    .find({
      "sort": "-updatedAt",
      "query": { "data.docsFor": context.params.types },
    })
    .lean();
  return { "code": 1, "data": documentsList };
};

adminEvent.getHubListByLocation = async function (context) {
  const hubList = await this.adapter.model
    .find({
      "name": constantUtil.HUBS,
      "data.serviceArea": context.params.serviceArea,
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  return hubList.map((hub) => ({
    "_id": hub._id,
    "clientId": hub.data.clientId,
    "hubsName": hub.data.hubsName,
    "email": hub.data.email,
    "phone": hub.data.phone,
    "showAddress": hub.data.showAddress,
    "location": hub.data.location,
    "timeSettings": hub.data.timeSettings,
    "holidayDates": hub.data.arrayData.holidayDates,
  }));
};

adminEvent.getDocumentListForHub = async function () {
  const hubVerificationDocumentList = await this.adapter.model
    .find({
      "name": constantUtil.VERIFICATIONDOCUMENT,
      //"data.clientId": context.params.clientId,
      "data.status": constantUtil.ACTIVE,
    })
    .lean();

  return hubVerificationDocumentList.map((hub) => ({
    ...hub.data,
    "_id": hub._id,
  }));
};

// ROLES UPDATES FOR OPERATORS
adminEvent.rolesForceUpdate = async function (context) {
  const clientId = context.params.clientId;
  const updateParams =
    context.params.ForceUpdate === true
      ? {
          "data.extraPrivileges": {},
          "data.accessToken": "",
        }
      : { "data.accessToken": "" };
  await this.adapter.model.updateMany(
    {
      "name": constantUtil.OPERATORS,
      "data.operatorRole": context.params._id,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    updateParams
  );
};
// OPERATORS CHANGES TO INACTIVE WHEN ROLE ACTIVE
adminEvent.rolesChangeInactive = async function (context) {
  await this.adapter.model.updateMany(
    {
      "name": constantUtil.OPERATORS,
      "data.operatorRole": { "$in": context.params._id },
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    {
      "data.status": constantUtil.INACTIVE,
      "data.accessToken": "",
    }
  );
};

// TRUSTED CONTACTS
adminEvent.getTrustedContactsReasons = async function (context) {
  const responseJson = await this.adapter.model
    .find({
      "name": constantUtil.TRUSTEDREASONS,
      // "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      "data.status": constantUtil.ACTIVE,
    })
    .sort({ "rewardType": 1, "_id": -1 })
    .lean();
  if (responseJson.length === 0) {
    return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };
  } else {
    return {
      "code": 200,
      "data": responseJson.map((office) => ({
        "_id": office._id,
        "clientId": office.data.clientId,
        "reason": office.data.reason,
        "isDefaultReason": office.data.isDefaultReason,
        "action": office.data.action,
        "isTimings": office.data.isTimings,
        "fromTime": office.data.fromTime,
        "toTime": office.data.toTime,
        "status": office.data.status,
      })),
      "message": "",
    };
  }
};

adminEvent.getReasonData = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.TRUSTEDREASONS,
      "_id": context.params.reasonId,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  if (!responseJson) {
    return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };
  } else {
    return { "code": 1, "data": responseJson, "message": "" };
  }
};

adminEvent.updateSettingBillingTime = async function (context) {
  // let responseJson = await this.adapter.model.updateOne(
  //   { "name": constantUtil.GENERALSETTING },
  //   {
  //     "data.lastBilledDate": context.params.lastBilledDate,
  //     "data.lastBilledTime": context.params.lastBilledTime,
  //   }
  // );
  // if (!responseJson?.nModified)
  //   return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };

  // responseJson = await this.adapter.model.findOne({
  //   "name": constantUtil.GENERALSETTING,
  // });
  // await storageUtil.write(constantUtil.GENERALSETTING, responseJson);

  // return { "code": 1, "data": responseJson, "message": "" };
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "name": constantUtil.GENERALSETTING,
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
      {
        "data.lastBilledDate": context.params.lastBilledDate,
        "data.lastBilledTime": context.params.lastBilledTime,
      },
      { "new": true }
    )
    .lean();
  if (!responseJson) {
    return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };
  } else {
    await storageUtil.write(constantUtil.GENERALSETTING, responseJson);

    return { "code": 1, "data": responseJson, "message": "" };
  }
};

adminEvent.updateSettingCoorperateBillingTime = async function (context) {
  // let responseJson = await this.adapter.model.updateOne(
  //   {
  //     "name": constantUtil.GENERALSETTING,
  //   },
  //   {
  //     "data.lastCoorperateBilledDate": context.params.lastCoorperateBilledDate,
  //     "data.lastCoorperateBilledTime": context.params.lastCoorperateBilledTime,
  //   }
  // );
  // if (!responseJson?.nModified)
  //   return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };

  // responseJson = await this.adapter.model.findOne({
  //   "name": constantUtil.GENERALSETTING,
  // });
  // await storageUtil.write(constantUtil.GENERALSETTING, responseJson);

  // return { "code": 1, "data": responseJson, "message": "" };
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "name": constantUtil.GENERALSETTING,
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
      {
        "data.lastCoorperateBilledDate":
          context.params.lastCoorperateBilledDate,
        "data.lastCoorperateBilledTime":
          context.params.lastCoorperateBilledTime,
      },
      { "new": true }
    )
    .lean();
  if (!responseJson) {
    return { "code": 0, "data": [], "message": "NO REASONS AVAILABLE" };
  } else {
    await storageUtil.write(constantUtil.GENERALSETTING, responseJson);

    return { "code": 1, "data": responseJson, "message": "" };
  }
};

adminEvent.uploadSingleImageInSpaces = async function (context) {
  const uuid = Date.now().toString(36);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let requestFrom = "";
  switch (context?.params?.requestFrom) {
    case constantUtil.BOOKING:
    case constantUtil.CONST_PACKAGES:
      requestFrom = context?.params?.requestFrom + "/";
      break;
  }
  if (context.params.action == constantUtil.REMOVE) {
    const objectDeleteParams = {
      "Bucket": generalSettings.data.spaces.spacesBucketName,
      "Key":
        generalSettings.data.spaces.spacesObjectName +
        "/" +
        requestFrom +
        context.params.oldFileName,
      // "ACL": "public-read",
    };
    if (context.params.oldFileName) {
      this.s3.deleteObject(objectDeleteParams, function (err, data) {});
    }
    return (
      generalSettings.data.spaces.spacesBaseUrl + "/" + objectDeleteParams.Key
    );
  } else {
    const uploadName = context.params.files[0].originalFilename.split(".");
    let extension = `.${uploadName[uploadName.length - 1]}`;
    extension = extension === ".blob" ? ".jpg" : extension;
    const _fileName = `${context.params.files[0].fieldName
      .toLowerCase()
      .split(" ")
      .join("")}.${context.params.fileName
      .toLowerCase()
      .split(" ")
      .join("")}${uuid}${extension}`;
    // .join("")}${extension}`;
    const objectParams = {
      "Bucket": generalSettings.data.spaces.spacesBucketName,
      "Key":
        generalSettings.data.spaces.spacesObjectName +
        "/" +
        requestFrom +
        _fileName,
      "Body": fs.readFileSync(context.params.files[0].path),
      "ACL": "public-read",
    };
    const responseData = await this.s3.putObject(objectParams, function () {});
    if (responseData) {
      const objectDeleteParams = {
        "Bucket": generalSettings.data.spaces.spacesBucketName,
        "Key":
          generalSettings.data.spaces.spacesObjectName +
          "/" +
          requestFrom +
          context.params.oldFileName,
        // "ACL": "public-read",
      };
      if (context.params.oldFileName) {
        this.s3.deleteObject(objectDeleteParams, function (err, data) {});
      }
    }
    // if (requestFrom !== "") return objectParams.Key;
    // else
    return generalSettings.data.spaces.spacesBaseUrl + "/" + objectParams.Key;
  }
};

adminEvent.uploadMultipleImageInSpaces = async function (context) {
  const uuid = Date.now().toString(36);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const uploadName = context.params.files[0].originalFilename.split(".");
  let extension = `.${uploadName[uploadName.length - 1]}`;
  extension = extension === ".blob" ? ".jpg" : extension;
  return context.params.files.map((file, index) => {
    const _fileName = `${file.fieldName
      .toLowerCase()
      .split(" ")
      .join("")}.[${index}].${context.params.fileName
      .toLowerCase()
      .split(" ")
      .join("")}${uuid}${extension}`;
    // .join("")}${extension}`;

    const objectParams = {
      "Bucket": generalSettings.data.spaces.spacesBucketName,
      "Key": generalSettings.data.spaces.spacesObjectName + "/" + _fileName,
      "Body": fs.readFileSync(file.path),
      "ACL": "public-read",
    };

    this.s3.putObject(objectParams, function () {});
    return generalSettings.data.spaces.spacesBaseUrl + "/" + objectParams.Key;
  });
};

adminEvent.uploadStaticMapImageInSpaces = async function (context) {
  const uuid = Date.now().toString(36);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const _fileName = `${context.params.fileName
    .toLowerCase()
    .split(" ")
    .join("")}${uuid}.jpg`;
  //----------- Delete Old File Start --------
  const objectDeleteParams = {
    "Bucket": generalSettings.data.spaces.spacesBucketName,
    "Key":
      generalSettings.data.spaces.spacesObjectName +
      "/" +
      context.params.oldFileName,
    // "ACL": "public-read",
  };
  if (context.params.oldFileName) {
    await this.s3.deleteObject(objectDeleteParams, function (err, data) {});
  }
  //----------- Delete Old File End --------
  const objectParams = {
    "Bucket": generalSettings.data.spaces.spacesBucketName,
    "Key": generalSettings.data.spaces.spacesObjectName + "/" + _fileName,
    "Body": context.params.files,
    "ACL": "public-read",
  };
  const responseData = await this.s3.putObject(objectParams, function () {});
  // if (responseData) {
  //   const objectDeleteParams = {
  //     "Bucket": generalSettings.data.spaces.spacesBucketName,
  //     "Key":
  //       generalSettings.data.spaces.spacesObjectName +
  //       "/" +
  //       context.params.oldFileName,
  //     // "ACL": "public-read",
  //   };
  //   if (context.params.oldFileName) {
  //     this.s3.deleteObject(objectDeleteParams, function (err, data) {});
  //   }
  // }

  return generalSettings.data.spaces.spacesBaseUrl + "/" + objectParams.Key;
};
adminEvent.uploadPdfInSpaces = async function (context) {
  const uuid = Date.now().toString(36);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const _fileName = `${context.params.fileName
    .toLowerCase()
    .split(" ")
    .join("")}${uuid}.pdf`;
  const objectParams = {
    "Bucket": generalSettings.data.spaces.spacesBucketName,
    "Key": generalSettings.data.spaces.spacesObjectName + "/" + _fileName,
    "Body": context.params.files,
    "ACL": "public-read",
  };
  const responseData = await this.s3.putObject(objectParams, function () {});
  if (responseData) {
    const objectDeleteParams = {
      "Bucket": generalSettings.data.spaces.spacesBucketName,
      "Key":
        generalSettings.data.spaces.spacesObjectName +
        "/" +
        context.params.oldFileName,
      // "ACL": "public-read",
    };
    if (context.params.oldFileName) {
      this.s3.deleteObject(objectDeleteParams, function (err, data) {});
    }
  }
  return generalSettings.data.spaces.spacesBaseUrl + "/" + objectParams.Key;
};
adminEvent.getAllCoorperateOffices = async function () {
  const responseOffice = await this.adapter.model
    .find({
      "name": constantUtil.COORPERATEOFFICE,
      "data.status": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
    })
    .lean();
  if (!responseOffice && responseOffice.length === 0) {
    return [];
  } else {
    return responseOffice;
  }
};
adminEvent.updateCoorperateBillData = async function (context) {
  const updateData = {
    "billingId": mongoose.Types.ObjectId(context.params.billingId.toString()),
    "paidStatus": context.params.paidStatus,
  };
  const responseOffice = await this.adapter.model.updateOne(
    {
      "name": constantUtil.COORPERATEOFFICE,
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    },
    { "$push": { "data.billingData": updateData } }
  );
  if (!responseOffice && responseOffice.length === 0) {
    return [];
  } else {
    return responseOffice;
  }
};
adminEvent.mailContentLink = async function (context) {
  const { data } = await storageUtil.read(constantUtil.GENERALSETTING);
  const generalSettings = data;
  let template = await this.adapter.model.findOne({
    // "name": constantUtil.EMAILTEMPLATE,
    "type": constantUtil.EMAILTEMPLATE,
    "data.name": context.params.template,
  });
  template = template?.data || "";
  let html = "";
  if (typeof template != "undefined" && typeof generalSettings != "undefined") {
    html = html + template.emailContent;
    for (let i = 0; i < context.params.html.length; i++) {
      const regExp = new RegExp("{{" + context.params.html[i].name + "}}", "g");
      html = html.replace(regExp, context.params.html[i].value);
    }
    html = html.replace(
      /{{privacy}}/g,
      `${
        generalSettings.redirectUrls.privacyUrl
          ? generalSettings.redirectUrls.privacyUrl
          : ""
      }`
    );
    html = html.replace(
      /{{terms}}/g,
      `${
        generalSettings.redirectUrls.termsAndConditionUrl
          ? generalSettings.redirectUrls.termsAndConditionUrl
          : ""
      }`
    );
    html = html.replace(
      /{{aboutus}}/g,
      `${
        generalSettings.redirectUrls.aboutUrl
          ? generalSettings.redirectUrls.aboutUrl
          : ""
      }`
    );
    html = html.replace(/{{logo}}/g, generalSettings.mobileLogo);
    html = html.replace(/{{siteUrl}}/g, generalSettings.siteUrl);
    html = html.replace(/{{siteTitle}}/g, generalSettings.siteTitle);
    html = html.replace(/{{currencyCode}}/g, generalSettings.currencyCode);
    html = html.replace(/{{currencySymbol}}/g, generalSettings.currencySymbol);
    // html = html.replace(/{{copyRights}}/g, generalSettings.copyRights)
    html = html.replace(/{{templateSubject}}/g, template.emailSubject);
    let tomail;
    if (Array.isArray(context.params.to)) {
      tomail = [...context.params.to];
      tomail = tomail.filter((e) => {
        return e;
      });
    } else if (context.params.to) {
      tomail = context.params.to;
    }
    let emailSubject = template.emailSubject;
    emailSubject = emailSubject.replace(
      /{{privacy}}/g,
      `${
        generalSettings.redirectUrls.privacyUrl
          ? generalSettings.redirectUrls.privacyUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{terms}}/g,
      `${
        generalSettings.redirectUrls.termsAndConditionUrl
          ? generalSettings.redirectUrls.termsAndConditionUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{aboutus}}/g,
      `${
        generalSettings.redirectUrls.aboutUrl
          ? generalSettings.redirectUrls.aboutUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{currencyCode}}/g,
      generalSettings.currencyCode
    );
    emailSubject = emailSubject.replace(
      /{{logo}}/g,
      generalSettings.mobileLogo
    );
    emailSubject = emailSubject.replace(
      /{{currencySymbol}}/g,
      generalSettings.currencySymbol
    );
    emailSubject = emailSubject.replace(
      /{{siteTitle}}/g,
      generalSettings.siteTitle
    );
    emailSubject = emailSubject.replace(
      /{{templateSubject}}/g,
      template.emailSubject
    );
    // emailSubject = emailSubject.replace(
    //   /{{copyRights}}/g,
    //   generalSettings.copyRights
    // )
    for (let i = 0; i < context.params.html.length; i++) {
      const regExp = new RegExp("{{" + context.params.html[i].name + "}}", "g");
      emailSubject = emailSubject.replace(regExp, context.params.html[i].value);
    }
    const mailOptions = {
      "from": context.params.from,
      "to": tomail,
      "subject": emailSubject,
      "text": html,
      "html": html,
    };
    return mailOptions;
  } else {
    return null;
  }
};
adminEvent.promationMailContentLink = async function (context) {
  const { data } = await storageUtil.read(constantUtil.GENERALSETTING);
  const generalSettings = data;
  const template = context.params.template;
  let html = "";
  if (typeof template != "undefined" && typeof generalSettings != "undefined") {
    html = html + template.content;
    for (let i = 0; i < context.params.html.length; i++) {
      const regExp = new RegExp("{{" + context.params.html[i].name + "}}", "g");
      html = html.replace(regExp, context.params.html[i].value);
    }
    html = html.replace(
      /{{privacy}}/g,
      `${
        generalSettings.redirectUrls.privacyUrl
          ? generalSettings.redirectUrls.privacyUrl
          : ""
      }`
    );
    html = html.replace(
      /{{terms}}/g,
      `${
        generalSettings.redirectUrls.termsAndConditionUrl
          ? generalSettings.redirectUrls.termsAndConditionUrl
          : ""
      }`
    );
    html = html.replace(
      /{{aboutus}}/g,
      `${
        generalSettings.redirectUrls.aboutUrl
          ? generalSettings.redirectUrls.aboutUrl
          : ""
      }`
    );
    html = html.replace(/{{logo}}/g, generalSettings.mobileLogo);
    html = html.replace(/{{siteUrl}}/g, generalSettings.siteUrl);
    html = html.replace(/{{siteTitle}}/g, generalSettings.siteTitle);
    html = html.replace(/{{currencyCode}}/g, generalSettings.currencyCode);
    html = html.replace(/{{currencySymbol}}/g, generalSettings.currencySymbol);
    // html = html.replace(/{{copyRights}}/g, generalSettings.copyRights)
    html = html.replace(/{{templateSubject}}/g, template.subject);
    let tomail;
    if (Array.isArray(context.params.to)) {
      tomail = [...context.params.to];
      tomail = tomail.filter((e) => {
        return e;
      });
    } else if (context.params.to) {
      tomail = context.params.to;
    }
    let emailSubject = template.subject;
    emailSubject = emailSubject.replace(
      /{{privacy}}/g,
      `${
        generalSettings.redirectUrls.privacyUrl
          ? generalSettings.redirectUrls.privacyUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{terms}}/g,
      `${
        generalSettings.redirectUrls.termsAndConditionUrl
          ? generalSettings.redirectUrls.termsAndConditionUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{aboutus}}/g,
      `${
        generalSettings.redirectUrls.aboutUrl
          ? generalSettings.redirectUrls.aboutUrl
          : ""
      }`
    );
    emailSubject = emailSubject.replace(
      /{{currencyCode}}/g,
      generalSettings.currencyCode
    );
    emailSubject = emailSubject.replace(
      /{{logo}}/g,
      generalSettings.mobileLogo
    );
    emailSubject = emailSubject.replace(
      /{{currencySymbol}}/g,
      generalSettings.currencySymbol
    );
    emailSubject = emailSubject.replace(
      /{{siteTitle}}/g,
      generalSettings.siteTitle
    );
    emailSubject = emailSubject.replace(
      /{{templateSubject}}/g,
      template.subject
    );
    // emailSubject = emailSubject.replace(
    //   /{{copyRights}}/g,
    //   generalSettings.copyRights
    // )
    for (let i = 0; i < context.params.html.length; i++) {
      const regExp = new RegExp("{{" + context.params.html[i].name + "}}", "g");
      emailSubject = emailSubject.replace(regExp, context.params.html[i].value);
    }
    const mailOptions = {
      "from": context.params.from,
      "to": tomail,
      "subject": emailSubject,
      "text": html,
      "html": html,
    };
    return mailOptions;
  } else {
    return null;
  }
};

adminEvent.updateCouponCount = async function (context) {
  let coupon = await this.adapter.model
    .findOne({
      "name": constantUtil.COUPON,
      "_id": mongoose.Types.ObjectId(context.params.couponId.toString()),
    })
    .lean();

  // coupon = coupon.toJSON();
  if (coupon.data.appliedCount < coupon.data.totalUsageCount) {
    let appliedCount;
    if (coupon.data?.appliedCount) {
      appliedCount = coupon.data?.appliedCount + 1;
    } else {
      appliedCount = 1;
    }

    let appliedDetails = [];
    // if (!coupon.data?.appliedDetails) {
    //   appliedDetails = [
    //     {
    //       "userId": context.params.userId.toString(),
    //       "count": 1,
    //     },
    //   ];
    // }
    let userFound = false;
    if (coupon.data?.appliedDetails) {
      appliedDetails = coupon.data.appliedDetails.map((userData) => {
        if (userData.userId.toString() === context.params.userId.toString()) {
          userData.count += 1;
          userFound = true;
        }
        return userData;
      });
    }
    if (!userFound) {
      // if (!coupon.data?.appliedDetails || !userFound) {
      // appliedDetails = coupon.data?.appliedDetails.push({
      if (appliedDetails.length === 0) {
        appliedDetails = [
          {
            "userId": context.params.userId.toString(),
            "count": 1,
          },
        ];
      } else {
        appliedDetails.push({
          "userId": context.params.userId.toString(),
          "count": 1,
        });
      }
    }
    let appliedCountData = {};
    if (context.params.actionType === constantUtil.STARTED) {
      appliedCountData = {
        "data.appliedCount": appliedCount,
      };
    }
    return await this.adapter.model.updateOne(
      {
        "name": constantUtil.COUPON,
        "_id": mongoose.Types.ObjectId(context.params.couponId.toString()),
      },
      {
        "$set": {
          // "data.appliedCount": appliedCount,
          ...appliedCountData,
          "data.appliedDetails": appliedDetails,
        },
      }
    );
  }
};

// EMAILS BY SEPARATE
adminEvent.sendServiceMail = async function (context) {
  const clientId = context.params.clientId;
  //  #region Get Config Settings
  const { "data": generalSettings } = await storageUtil.read(
    constantUtil.GENERALSETTING
  );
  //  #endregion Get Config Settings
  //---------------------------
  if (!generalSettings?.emailConfigurationEnable) {
    throw new MoleculerError(
      "GENERALSETTING EMAIL CONFIGURATION DISABLED SO CANT SEND EMAIL OR PLEASE SWITCH ON"
    );
  }
  // params verification is here
  if (!context.params.templateName) {
    throw new MoleculerError(
      "templateName is required",
      422,
      "parameter validation Error"
    );
  }
  // for sending message email is required
  if (!context.params.email) {
    throw new MoleculerError(
      "email is required",
      422,
      "Parameter Validation Error"
    );
  }

  const mailData = await this.adapter.model
    .findOne({
      // "name": constantUtil.EMAILTEMPLATE,
      "type": constantUtil.EMAILTEMPLATE,
      //"data.clientId": clientId,
      "data.name": context.params.templateName,
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  if (!mailData) {
    throw new MoleculerError("CANNOT FIND EMAIL");
  }
  if (mailData.data.status === constantUtil.INACTIVE) {
    return {
      "code": 400,
      "message": "WE CANT SEND MAIL BECAUSE TEMPLATE IS INACTIVE",
      "data": {},
    };
  }
  if (!mailData.data?.emailTemplateEditableFields?.[0]) {
    throw new MoleculerError(
      "INSERT TEMPLATE AND EDITABLE CONTENT FROM CODE BASE"
    );
  }
  context.params = {
    ...context.params,
    "privacy": generalSettings.redirectUrls.privacyUrl
      ? generalSettings.redirectUrls.privacyUrl
      : "",
    "terms": generalSettings.redirectUrls.termsAndConditionUrl
      ? generalSettings.redirectUrls.termsAndConditionUrl
      : "",
    "aboutus": generalSettings.redirectUrls.aboutUrl
      ? generalSettings.redirectUrls.aboutUrl
      : "",
    "logo": generalSettings.mobileLogo,
    "siteUrl": generalSettings.siteUrl,
    "oraganization": generalSettings.siteTitle,
    "adminUrl": "", // "https://admin.zervx.com", //"https://admin.zervx.com?clientId=" + context.params.clientId,
  };
  mailData.data.emailTemplateEditableFields.map((key) => {
    if (context.params[key] === undefined) {
      throw new MoleculerError(
        `${key} is required`,
        422,
        "parameter validation error"
      );
    }
    mailData.data.emailContent = mailData.data.emailContent.replace(
      new RegExp(`{{${key}}}`, "g"),
      context.params[key]
    );
  });
  //
  let fromMailId = `{{SenderName}} <{{mailId}}>`; // "Sender New <ajithn@berarkrays.com>"
  fromMailId = fromMailId.replace(/{{mailId}}/g, mailData.data.senderEmail);
  fromMailId = fromMailId.replace(/{{SenderName}}/g, mailData.data.senderName);
  //
  await this.broker.emit("admin.sendMail", {
    "clientId": context.params?.clientId,
    "from": fromMailId, // generalSettings.emailAddress,
    "to": context.params.email,
    "subject": mailData.data.emailSubject,
    "html": mailData.data.emailContent,
  });
};
adminEvent.getEmailNotificationTemplate = async function (context) {
  let mailData = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "name": constantUtil.NOTIFICATIONTEMPLATES,
        },
      },
      { "$unwind": "$data.emailNotifications" },
      {
        "$match": {
          "data.emailNotifications.title": context.params.templateName,
        },
      },
      {
        "$project": {
          "id": "$data.emailNotifications._id",
          "title": "$data.emailNotifications.title",
          "content": "$data.emailNotifications.content",
          "subject": "$data.emailNotifications.subject",
          "emailTemplateEditableFields": "$data.emailTemplateEditableFields",
        },
      },
    ])
    .allowDiskUse(true);

  mailData = mailData && mailData[0];
  if (!mailData) {
    throw new MoleculerError(
      "CANNOT FIND EMAIL Data CHECK EMAIL DATA IS EXIST"
    );
  } else {
    return mailData;
  }
};

adminEvent.sendPromotionMail = async function (context) {
  console.log("context", context.params);
  const { data } = await storageUtil.read(constantUtil.GENERALSETTING);
  if (!data.emailConfigurationEnable)
    throw new MoleculerError(
      "GENERALSETTING EMAIL CONFIGURATION DISABLED SO CANT SEND EMAIL OR PLEASE SWITCH ON"
    );
  // params verification is here
  if (!context.params.templateName) {
    throw new MoleculerError(
      "templateName is required",
      422,
      "parameter validation Error"
    );
  }
  // for sending message email is required
  if (!context.params.email) {
    throw new MoleculerError(
      "email is required",
      422,
      "Parameter Validation Error"
    );
  }
  // //----------------------------
  // let mailData = await this.adapter.model.aggregate([
  //   {
  //     "$match": {
  //       "name": constantUtil.NOTIFICATIONTEMPLATES,
  //     },
  //   },
  //   { "$unwind": "$data.emailNotifications" },
  //   {
  //     "$match": {
  //       "data.emailNotifications.title": context.params.templateName,
  //     },
  //   },
  //   {
  //     "$project": {
  //       "id": "$data.emailNotifications._id",
  //       "title": "$data.emailNotifications.title",
  //       "content": "$data.emailNotifications.content",
  //       "subject": "$data.emailNotifications.subject",
  //       "emailTemplateEditableFields": "$data.emailTemplateEditableFields",
  //     },
  //   },
  // ]);

  // mailData = mailData && mailData[0];

  if (!context.params.mailData)
    throw new MoleculerError(
      "CANNOT FIND EMAIL Data CHECK EMAIL DATA IS EXIST"
    );
  // //----------------------------
  // if (
  //   !mailData.emailTemplateEditableFields ||
  //   !mailData.emailTemplateEditableFields[0]
  // ) {
  //   throw new MoleculerError(
  //     'THERE IS NO EDITABLE CONTENTS PLEASE ADD FIRST FOR SEND PROMOTION EMAILS'
  //   )
  // }
  // context.params = {
  //   ...context.params,
  //   'privacy': data.redirectUrls.privacyUrl ? data.redirectUrls.privacyUrl : '',
  //   'terms': data.redirectUrls.termsAndConditionUrl
  //     ? data.redirectUrls.termsAndConditionUrl
  //     : '',
  //   'aboutus': data.redirectUrls.aboutUrl ? data.redirectUrls.aboutUrl : '',
  //   'logo': data.mobileLogo,
  //   'siteUrl': data.siteUrl,
  //   'organization': data.siteTitle,
  // }
  // //
  // mailData.emailTemplateEditableFields.map((key) => {
  //   if (context.params[key] === undefined) {
  //     throw new MoleculerError(
  //       `${key} is required`,
  //       422,
  //       'parameter validation error'
  //     )
  //   }

  //   mailData.content = mailData.content.replace(
  //     new RegExp(`{{${key}}}`, 'g'),
  //     context.params[key]
  //   )
  // })
  //
  let fromMailId = `{{SenderName}} <{{mailId}}>`; // "Sender New <ajithn@berarkrays.com>"
  fromMailId = fromMailId.replace(/{{mailId}}/g, data.emailAddress);
  fromMailId = fromMailId.replace(/{{SenderName}}/g, data.siteTitle);
  //
  this.broker.emit("admin.sendMail", {
    "clientId": context.params?.clientId,
    "from": fromMailId, // data.emailAddress
    "to": context.params.email,
    "subject": context.params.mailData.subject,
    "html": context.params.mailData.content,
  });
};
adminEvent.removePrefix0 = async function () {
  let adminData = await this.adapter.model.find(
    { "data.phone.number": { "$regex": /^0/ } },
    { "_id": 1, "data.phone": 1 }
  );
  if (!adminData[0]) {
    return {
      "code": 404,
      "data": {},
      "message": "IN ADMIN PHONE NUMBER START WITH O IS NOT FOUND",
    };
  }
  const duplicateDoc = [];
  adminData = await adminData.map(async (data) => {
    try {
      return await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(data._id.toString()),
        },
        {
          "$set": {
            "data.phone.number": data.data.phone.number.substring(1),
          },
        },
        { "fields": { "data.phone": 1, "_id": 1 }, "new": true }
      );
    } catch (error) {
      if (error.code === 11000) {
        duplicateDoc.push(data.data.phone);
      }
    }
  });
  adminData = await Promise.all(adminData);
  adminData = adminData.filter((data) => !!data !== false);
  if (!adminData[0]) {
    return {
      "code": 500,
      "data": {
        "duplicates": {
          "count": duplicateDoc.length,
          "duplicates": duplicateDoc,
        },
        "data": adminData,
      },
      "message":
        "IN ADMIN PHONE NUMBER START WITH O CANT UPDATE PLEASE CONTACT DEVELOPER",
    };
  }
  return {
    "code": 200,
    "data": {
      "duplicates": {
        "count": duplicateDoc.length,
        "duplicates": duplicateDoc,
      },
      "data": adminData,
    },
    "message": "IN ADMIN UPDATE SUCESSFULLY",
  };
};

adminEvent.getBankInputData = async function (context) {
  const bankInputDetails = await this.adapter.model
    .findOne(
      {
        "name": context.params.paymentType,
        "data.gateWay": context.params.paymentGatway,
      },
      { "data.bankDetails": 1 }
    )
    .lean();
  return bankInputDetails;
};

adminEvent.testData = async function (context) {
  // //---------------------------
  // let testResult = {}
  // testResult = await this.broker.emit('admin.testData', {
  //   'paymentType': constantUtil.PAYMENTGATEWAY,
  //   'paymentGatway': paymentData.gateWay,
  // })
  // if (!testResult) return 'ERROR IN PAYMENT OPTION LIST'
  // testResult = testResult[0]
  // console.log(testResult)
  // //---------------------------
  const bankInputDetails = await this.adapter.model
    .find({
      "$or": [
        { "name": { "$elemMatch": { "name": context.params.paymentType } } },
        {
          "data": { "$elemMatch": { "gateWay": context.params.paymentGatway } },
        },
        { "data.bankDetails": { "$elemMatch": { "name": "Math" } } },
      ],
    })
    .lean();

  return bankInputDetails;
};
adminEvent.getSubscriptionPlansData = async function (context) {
  return await this.adapter.model.find({
    "name": constantUtil.SUBSCRIPTIONPLAN,
    "data.userType": context.params.userType,
    "data.isDefault": true,
  });
};

adminEvent.sendBulkServiceMail = async function ({ params }) {
  const { "data": generalSettings } = await storageUtil.read(
    constantUtil.GENERALSETTING
    //  #region Get Config Settings
  );
  //  #endregion Get Config Settings
  if (!generalSettings.emailConfigurationEnable) {
    throw new MoleculerError(
      "GENERALSETTING EMAIL CONFIGURATION DISABLED SO CANT SEND EMAIL OR PLEASE SWITCH ON"
    );
  }
  const { "data": mailData } = await this.adapter.model.findOne({
    // "name": constantUtil.EMAILTEMPLATE,
    "type": constantUtil.EMAILTEMPLATE,
    "data.name": params.templateName,
    "data.status": constantUtil.ACTIVE,
  });
  if (!mailData) {
    throw new MoleculerError(`${params.templateName} NOT FOUND`);
  }

  const { "data": smtp } = await storageUtil.read(constantUtil.SMTPSETTING);

  if (!smtp.host || !smtp.port || !smtp.client || !smtp.secret)
    throw new MoleculerError("INVALID SMTP");

  params.receiversData.forEach((each) => {
    each = {
      ...each,
      "privacy": generalSettings.redirectUrls.privacyUrl ?? "",
      "terms": generalSettings.redirectUrls.termsAndConditionUrl ?? "",
      "aboutus": generalSettings.redirectUrls.aboutUrl ?? "",
      "logo": generalSettings.mobileLogo,
      "siteUrl": generalSettings.siteUrl,
      "organization": generalSettings.siteTitle,
    };
    mailData.emailTemplateEditableFields.forEach((key) => {
      mailData.data.emailContent = mailData.data.emailContent.replace(
        new RegExp(`{{${key}}}`, "g"),
        each[key]
      );
    });
    //
    let fromMailId = `{{SenderName}} <{{mailId}}>`; // "Sender New <ajithn@berarkrays.com>"
    fromMailId = fromMailId.replace(/{{mailId}}/g, mailData.data.senderEmail);
    fromMailId = fromMailId.replace(
      /{{SenderName}}/g,
      mailData.data.senderName
    );
    //
    this.broker.emit("admin.sendMail", {
      "clientId": context.params?.clientId,
      "from": fromMailId, // generalSettings.emailAddress,
      "to": each.email,
      "subject": mailData.data.emailSubject,
      "html": mailData.data.emailContent,
    });
  });
};
// ----------- Loyalty Coupon / Rewards Start --------
// adminEvent.getLoyaltyCouponById = async function (context) {
//   return await this.adapter.model.find({
//     "_id": context.params.couponId,
//     "name": constantUtil.LOYALTYCOUPON,
//     "data.userType": context.params.userType,
//   });
// };

// adminEvent.updateLoyaltyCouponAppliedCount = async function (context) {
//   let coupon = await this.adapter.model.findOne({
//     "_id": mongoose.Types.ObjectId(context.params.couponId.toString()),
//     "name": constantUtil.LOYALTYCOUPON,
//   });

//   coupon = coupon.toJSON();

//   let appliedCount;
//   if (coupon.data?.appliedCount) {
//     appliedCount = coupon.data?.appliedCount + 1;
//   } else {
//     appliedCount = 1;
//   }

//   let appliedDetails;
//   if (!coupon.data?.appliedDetails) {
//     appliedDetails = [
//       {
//         "userId": context.params.userId.toString(),
//         "count": 1,
//       },
//     ];
//   }
//   let userFound = false;
//   if (coupon.data?.appliedDetails[0]) {
//     appliedDetails = coupon.data.appliedDetails.map((userData) => {
//       if (userData.userId.toString() === context.params.userId.toString()) {
//         userData.count += 1;
//         userFound = true;
//       }
//       return userData;
//     });
//   }
//   if (!coupon.data?.appliedDetails[0] || !userFound) {
//     appliedDetails = coupon.data?.appliedDetails.push({
//       "userId": context.params.userId.toString(),
//       "count": 1,
//     });
//   }

//   const couponUpdate = await this.adapter.model.findOneAndUpdate(
//     {
//       "name": constantUtil.LOYALTYCOUPON,
//       "_id": mongoose.Types.ObjectId(context.params.couponId.toString()),
//     },
//     {
//       "$set": {
//         "data.appliedCount": appliedCount,
//         "data.appliedDetails": appliedDetails,
//       },
//     },
//     { "new": true }
//   );

//   return couponUpdate;
// };

adminEvent.getActiveRewardsListBasedOnCityAndUserType = async function (
  context
) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const rewardType = context.params?.rewardType;
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { SOMETHING_WENT_WRONG, INFO_SERVICE_NOT_PROVIDE } =
    await notifyMessage.setNotifyLanguage(languageCode);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  if (!generalSettings) {
    throw new MoleculerError(SOMETHING_WENT_WRONG);
  }
  // find category because coupon works location based
  let serviceCategory = await this.broker.emit(
    "category.getPopularServiceCategory",
    { ...context.params, "clientId": context.params.clientId }
  );
  serviceCategory = serviceCategory && serviceCategory[0];
  if (!serviceCategory) {
    throw new MoleculerError(INFO_SERVICE_NOT_PROVIDE);
  }
  // let queryCondition = {};

  //   queryCondition = {
  //     "data.validFrom": { "$lte": new Date() },
  //     "data.validTo": { "$gte": new Date() },
  //   };
  // }
  // Active Coupon / rewards List
  let activeLoyaltyCouponsList = [];
  if (rewardType === constantUtil.PARTNERDEALS) {
    activeLoyaltyCouponsList = await this.adapter.model
      .find({
        "name": constantUtil.REWARDS,
        "isActive": true,
        "data.rewardType": rewardType,
        "data.serviceArea": serviceCategory._id.toString(),
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        "data.userType": userType,
        "data.validFrom": { "$lte": new Date() },
        "data.validTo": { "$gte": new Date() },
      })
      .sort({ "rewardType": 1, "updatedAt": -1 });
  } else if (rewardType === constantUtil.POINTS) {
    activeLoyaltyCouponsList = await this.adapter.model
      .find({
        "name": constantUtil.REWARDS,
        "isActive": true,
        "data.rewardType": rewardType,
        "data.serviceArea": serviceCategory._id.toString(),
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        "data.userType": userType,
      })
      .sort({ "rewardType": 1, "updatedAt": -1 });
  }
  return activeLoyaltyCouponsList;
};

adminEvent.updateRewardUsedCount = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.couponId.toString()) },
    { "$inc": { "data.usedCount": 1 } }
  );
};

adminEvent.redeemRewardsPointConfig = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const languageCode = (context.params?.languageCode ?? "").toUpperCase();
  let activeLoyaltyCouponsList = {},
    serviceCategory = {};
  const { INFO_SERVICE_NOT_PROVIDE } = await notifyMessage.setNotifyLanguage(
    languageCode
  );
  //--------------find category because Reward works location based Start ----------
  if (context.params?.serviceAreaId) {
    serviceCategory = { "_id": context.params?.serviceAreaId };
  } else {
    serviceCategory = await this.broker.emit(
      "category.getPopularServiceCategory",
      { ...context.params, "clientId": context.params?.clientId }
    );
    serviceCategory = serviceCategory && serviceCategory[0];
  }
  if (!serviceCategory) {
    throw new MoleculerError(INFO_SERVICE_NOT_PROVIDE);
  }
  //--------------find category because Reward works location based End ----------
  activeLoyaltyCouponsList = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "name": constantUtil.REWARDS,
          "isActive": true,
          "data.rewardType": constantUtil.POINTS,
          "data.serviceArea": serviceCategory._id.toString(),
          //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
          "data.userType": userType,
        },
      },
      {
        "$project": {
          "_id": 0,
          "redeemPointValue": "$data.redeemPointValue",
          "redeemLimitMin": "$data.redeemLimitMin",
          "redeemLimitMax": "$data.redeemLimitMax",
        },
      },
    ])
    .allowDiskUse(true);
  // Return only one row
  activeLoyaltyCouponsList =
    activeLoyaltyCouponsList && activeLoyaltyCouponsList[0];

  return activeLoyaltyCouponsList;
};

adminEvent.getNotificationBasedOnUserType = async function (context) {
  // const userType = (context.params?.userType ?? "").toUpperCase();
  // const languageCode = (context.params?.languageCode ?? "").toUpperCase();
  const activeNotificationList = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "name": "NOTIFICATIONTEMPLATES",
          //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        },
      },
      { "$unwind": "$data.pushNotifications" },
      {
        "$match": {
          "data.pushNotifications.status": "ACTIVE",
          "data.pushNotifications.userType": context.params.userType,
        },
      },
      {
        "$project": {
          "notificationId": "$_id",
          "_id": "$data.pushNotifications._id",
          "title": "$data.pushNotifications.title",
          "content": "$data.pushNotifications.content",
          "image": "$data.pushNotifications.image",
          "subject": "$data.pushNotifications.subject",
          // "status": "$data.pushNotifications.status",
          // "date": "$data.pushNotifications.date",
          "date": {
            "$ifNull": ["$data.pushNotifications.date", "$updatedAt"],
          },
        },
      },
      { "$sort": { "date": -1 } },
    ])
    .allowDiskUse(true);

  return activeNotificationList;
};
// ----------- Loyalty Coupon / Rewards Start --------
adminEvent.getUserBasedCouponList = async function (context) {
  // find category because coupon works location based
  let serviceCategory = await this.broker.emit(
    "category.getPopularServiceCategory",
    { ...context.params, "clientId": context.params?.clientId }
  );
  serviceCategory = serviceCategory && serviceCategory[0];

  if (!serviceCategory) {
    throw new MoleculerError("SORRY WE DONT PROVIDE SERVICE HERE");
  }
  // get booking count of user
  let userBookingsCount = await this.broker.emit(
    "booking.checkBookingCountforCoupon",
    {
      "userId": context.params.userId?.toString(),
      "clientId": context.params.clientId?.toString(),
    }
  );
  userBookingsCount = userBookingsCount && userBookingsCount[0];

  // find coupons list

  // query for which kind of coupon is applicable for this user
  // const firstUserCouponsList1 = await this.adapter.model //Need to Change FIRSTUSER & EVERYONE in Sigle Group Query
  //   .find({
  //     "name": constantUtil.COUPON,
  //     "data.status": constantUtil.ACTIVE,
  //     "data.serviceArea": mongoose.Types.ObjectId(
  //       serviceCategory._id.toString()
  //     ),
  //     "data.consumersType": constantUtil.FIRSTUSER,
  //   })
  //   .lean()
  //   .sort({ "updatedAt": -1 })
  //   .limit(1);
  const firstUserCouponsList = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "name": constantUtil.COUPON,
          "data.status": constantUtil.ACTIVE,
          "data.serviceArea": serviceCategory._id.toString(),
          //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
          "data.consumersType": constantUtil.FIRSTUSER,
        },
      },
      // {
      //   "$match": {
      //     "$or": [
      //       {
      //         "data.vaildTo": { "$gte": new Date() },
      //       },
      //       {
      //         "data.vaildTo": null,
      //       },
      //     ],
      //   },
      // },
      {
        "$project": {
          "_id": "$_id",
          "data": "$data",
        },
      },
    ])
    .allowDiskUse(true);
  // const everyUserCouponsList1 = await this.adapter.model //Need to Change FIRSTUSER & EVERYONE in Sigle Group Query
  //   .find({
  //     "name": constantUtil.COUPON,
  //     "data.status": constantUtil.ACTIVE,
  //     "data.serviceArea": mongoose.Types.ObjectId(
  //       serviceCategory._id.toString()
  //     ),
  //     "data.consumersType": constantUtil.EVERYONE,
  //   })
  //   .lean()
  //   .sort({ "updatedAt": -1 })
  //   .limit(1);
  const everyUserCouponsList = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "name": constantUtil.COUPON,
          "data.status": constantUtil.ACTIVE,
          "data.serviceArea": serviceCategory._id.toString(),
          //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
          "data.consumersType": constantUtil.EVERYONE,
        },
      },
      // {
      //   "$match": {
      //     "$or": [
      //       {
      //         "data.vaildTo": { "$gte": new Date() },
      //       },
      //       {
      //         "data.vaildTo": null,
      //       },
      //     ],
      //   },
      // },
      {
        "$project": {
          "_id": "$_id",
          "data": "$data",
        },
      },
    ])
    .allowDiskUse(true);
  const usageCountPerPerson =
    firstUserCouponsList[0]?.data?.usageCountPerPerson || 0;
  const isAutomaticCoupon = firstUserCouponsList[0]?.data?.isAutomatic || false;
  const usageCountPerPersonEvery =
    everyUserCouponsList[0]?.data?.usageCountPerPerson || 0;

  let queryData = {},
    firstUserBookingsCount = 0,
    everyUserBookingsCount = 0,
    promotionCouponBookingsCount = 0;
  //----------------------------------------
  await userBookingsCount.find((e) => {
    if (e.consumersType === constantUtil.FIRSTUSER)
      firstUserBookingsCount = e.couponUsedCount;
    if (e.consumersType === constantUtil.EVERYONE)
      everyUserBookingsCount = e.couponUsedCount;
    if (e.consumersType === constantUtil.PROMOTION)
      promotionCouponBookingsCount = e.couponUsedCount;
    // console.log("userBookingsCountTest_" + JSON.stringify(e))
  });
  //-----------------
  // if (
  //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  //   isAutomaticCoupon === true
  // ) {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.FIRSTUSER],
  //     },
  //   };
  // } else if (
  //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  //   isAutomaticCoupon === false
  // ) {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
  //     },
  //   };
  // } else if (everyUserBookingsCount < parseInt(usageCountPerPersonEvery)) {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.EVERYONE],
  //     },
  //   };
  // } else {
  //   //For Not Apply Any Coupon
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.NONE],
  //     },
  //   };
  // }
  //----------------------------------------
  // if (firstUserCouponsList.length > 0) {
  if (
    firstUserBookingsCount < parseInt(usageCountPerPerson) &&
    isAutomaticCoupon === true
  ) {
    queryData = {
      "data.consumersType": {
        "$in": [constantUtil.FIRSTUSER],
      },
    };
  } else if (
    firstUserBookingsCount < parseInt(usageCountPerPerson) &&
    isAutomaticCoupon === false
  ) {
    queryData = {
      "data.consumersType": {
        "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
      },
    };
  } else {
    queryData = {
      "data.consumersType": {
        "$in": [constantUtil.EVERYONE],
      },
    };
  }
  // } else {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.EVERYONE],
  //     },
  //   };
  // }
  //----------------------------------------
  // const queryData =
  // userBookingsCount <= parseInt(usageCountPerPerson || 0)
  //   ? {
  //       "data.consumersType": {
  //         "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
  //       },
  //     }
  //   : {
  //       "data.consumersType": {
  //         "$in": [constantUtil.EVERYONE],
  //       },
  //     };

  const couponsList = await this.adapter.model
    .find({
      "name": constantUtil.COUPON,
      "data.status": constantUtil.ACTIVE,
      "data.serviceArea": mongoose.Types.ObjectId(
        serviceCategory._id.toString()
      ),
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      ...queryData,
    })
    .sort({ "data.consumersType": 1 });
  // filter detailes
  const responseData = [];

  // below means if sigle coupon is exist
  if (couponsList[0]) {
    for (let coupon of couponsList) {
      coupon = coupon.toJSON();

      //below condition applies when coupon has in between valid date  or not have valid date
      if (
        (!coupon.data?.vaildFrom && !coupon.data?.vaildTo) || // it means this coupon doest not validation date
        (coupon.data.vaildFrom &&
          coupon.data.vaildTo &&
          new Date(coupon.data.vaildFrom) <= new Date() &&
          new Date(coupon.data.vaildTo) >= new Date())
      ) {
        // below condition applies when coupon has apply count
        if (
          !coupon.data?.appliedCount ||
          (!coupon.data?.appliedCount && !coupon.data?.totalUsageCount) || //this means this coupon does not have limit
          (coupon.data.appliedCount &&
            coupon.data.totalUsageCount &&
            coupon.data.appliedCount < coupon.data.totalUsageCount)
        ) {
          const alredyUsed = coupon.data?.appliedDetails?.find(
            (e) => e.userId?.toString() === context.params?.userId?.toString()
          );
          // below condition applies when requested user does not used yet or person usage count does not exeed usage count of usageCountPerPerson
          if (
            !alredyUsed ||
            alredyUsed?.count < coupon.data.usageCountPerPerson
          ) {
            responseData.push({
              "_id": coupon._id,
              "city": coupon.data.serviceArea,
              "categoryIds": coupon.data.categoryIds,
              "categoryType": coupon.data.categoryType,
              "couponTitle": coupon.data.couponTitle,
              "couponDescription": coupon.data.couponDescription,
              "couponCode": coupon.data.couponCode,
              "couponType": coupon.data.couponType,
              "couponAmount": coupon.data.couponAmount,
              "couponMaxAmount": coupon.data?.couponMaxAmount || 0,
              "totalUsageCount": coupon.data.totalUsageCount,
              "usageCountPerPerson": coupon.data.usageCountPerPerson,
              "vaildFrom": coupon.data.vaildFrom,
              "vaildTo": coupon.data.vaildTo,
              "consumersType": coupon.data.consumersType,
              "isAutomatic": coupon.data.isAutomatic,
              "isProfessionalTolerance": coupon.data.isProfessionalTolerance,
              "isDisplayInApp": coupon.data.isDisplayInApp,
              "couponCodeMessage": coupon.data.couponCodeMessage,
              "currencyCode": serviceCategory.currencyCode,
              "currencySymbol": serviceCategory.currencySymbol,
              "couponGuideLines": coupon.data?.couponGuideLines || [],
              "couponPaymentType": coupon.data?.couponPaymentType || [],
              "isCouponCheckWithMaxTripAmount":
                coupon.data?.isCouponCheckWithMaxTripAmount || false,
            });
            // this coupon retuns successfully so next coupon
            continue;
          } // bleow else means this user exceed the usage count
          else {
            // so skip this and go next

            continue;
          }
        }
      }

      // this is else => above condition does not match ,have inactive this  coupon
      this.adapter.model.updateOne(
        { "_id": mongoose.Types.ObjectId(coupon._id.toString()) },
        {
          "data.status": constantUtil.INACTIVE,
          "data.appliedDetails": [],
          "data.appliedCount": 0,
        }
      );

      // Setting Update for Coupon Code
      // let settingsUpdate = await this.adapter.model.updateOne(
      //   { "name": constantUtil.GENERALSETTING },
      //   {
      //     "data.couponUpdateTime": Date.now(),
      //   }
      // );
      // if (!settingsUpdate?.nModified)
      //   throw new MoleculerError("Error in Update");

      // settingsUpdate = await this.adapter.model.findOne({
      //   "name": constantUtil.GENERALSETTING,
      // });

      // await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
      await this.broker.emit(
        "admin.updateGeneralSettingTimestampInDBAndRedis",
        {
          "type": constantUtil.COUPON,
          "clientId": context.params.clientId,
        }
      );
      // Setting Update for Coupon Code

      continue;
    }
  }
  return responseData || [];
};

adminEvent.updateHubSiteCommission = async function (context) {
  let siteCommission = 0,
    siteCommissionPercentage = 0;
  const hubData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params?.hubId?.toString()),
      "name": constantUtil.HUBS,
      // "data.status": constantUtil.ACTIVE,
    })
    .lean();
  if (hubData) {
    siteCommissionPercentage = hubData.data?.commissionPercentage || 0;
    siteCommission =
      (parseFloat(context.params?.siteCommissionWithoutTax || 0) / 100) *
      parseFloat(siteCommissionPercentage);

    const responseJson = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params?.hubId?.toString()),
        "name": constantUtil.HUBS,
        // "data.status": constantUtil.ACTIVE,
      },
      {
        "$inc": {
          "data.amount": siteCommission,
        },
      }
    );
    if (!responseJson.nModified) {
      siteCommission = 0;
      siteCommissionPercentage = 0;
    } else {
      //-------- Transaction Entry Start ---------------------
      this.broker.emit("transaction.hubSiteCommissionTransaction", {
        "clientId": context.params.clientId?.toString(),
        "bookingId": context.params.bookingId?.toString(),
        "refBookingId": context.params.refBookingId,
        "hubId": context.params?.hubId?.toString(),
        "hubData": hubData.data,
        "amount": siteCommission,
        "previousBalance": hubData?.data?.amount || 0,
        "currentBalance":
          parseFloat(hubData?.data?.amount || 0) + parseFloat(siteCommission),
      });
      //-------- Transaction Entry End ---------------------
    }
  }
  return {
    "hubSiteCommission": siteCommission,
    "hubSiteCommissionPercentage": siteCommissionPercentage,
  };
};

adminEvent.updateRegistrationDocuments = async function (context) {
  let documentsJson = null,
    message = constantUtil.SUCCESS;
  const userType = context.params.userType;
  const documentsList = context.params.documentsList;
  try {
    //#region For Professional & Vehicle Document
    await documentsList?.map(async (eachItem) => {
      const newDocument = {};
      newDocument["status"] = constantUtil.ACTIVE;
      newDocument["docsFor"] = trim(context.params.docsFor);
      newDocument["docsServiceType"] = trim(context.params.docsServiceType);
      newDocument["docsServiceTypeId"] = context.params.docsServiceTypeId;
      newDocument["serviceAreaId"] = context.params.serviceAreaId;
      newDocument["clientId"] = context.params.clientId;
      await eachItem.map((eachSubItem) => {
        switch (eachSubItem.key) {
          case "documentName":
            newDocument["docsName"] = trim(eachSubItem.title || "");
            break;

          case "documentDetail":
            newDocument["docsDetail"] = eachSubItem.title || "";
            break;

          case "documents":
            // newDocument["docsReferImage"] = null; // eachSubItem.title || "";
            newDocument["docsReferImage"] =
              "https://zervx.sgp1.digitaloceanspaces.com/zervx-storage/categorymapimage.[0].suvvehiclecategoryll24x73d.jpg"; // Default
            break;

          case "documentMandatory":
            newDocument["docsMandatory"] = eachSubItem.isMandatory || false;
            break;

          case "expiryDate":
            newDocument["docsExpiry"] = eachSubItem.isMandatory || false;
            break;
        }
      });
      //--------------
      documentsJson = await this.adapter.model
        .findOne({
          //"data.clientId": newDocument.clientId,
          "data.docsName": newDocument.docsName,
          "data.docsFor": newDocument.docsFor,
          "data.docsServiceType": newDocument.docsServiceType,
          "data.docsServiceTypeId": newDocument.docsServiceTypeId,
          "name": constantUtil.DOCUMENTS,
        })
        .lean();
      const updateData = {
        "clientId": newDocument.clientId,
        "docsName": newDocument.docsName,
        "docsFor": newDocument.docsFor,
        "docsServiceType": newDocument.docsServiceType,
        "docsServiceTypeId": newDocument.docsServiceTypeId,
        "serviceAreaId": newDocument.serviceAreaId,
        "docsDetail": newDocument.docsDetail,
        "docsReferImage": newDocument.docsReferImage,
        "docsMandatory": newDocument.docsMandatory,
        "docsExpiry": newDocument.docsExpiry,
        "status": newDocument.status,
      };
      // if (context.params.action === "edit" && documentsJson?._id?.toString()) {
      if (documentsJson?._id?.toString()) {
        // if (
        //   documentsJson &&
        //   documentsJson._id.toString() != context.params.id.toString()
        // ) {
        //   return "documents already exist";
        // } else {
        // updateData.status = documentsJson.data.status; // Status
        // updateData.status = constantUtil.ACTIVE; // Status
        await this.adapter.updateById(
          mongoose.Types.ObjectId(documentsJson._id.toString()),
          { "data": updateData }
        );
        // }
      } else {
        // if (documentsJson) {
        //   return "documents already exist";
        // } else {
        await this.adapter.insert({
          "dataType": constantUtil.CLIENTDATA,
          "type": constantUtil.DOCUMENTS,
          "name": constantUtil.DOCUMENTS,
          "isActive": true,
          "data": updateData,
        });
        // }
      }
    });
    //#endregion For Professional & Vehicle Document

    // //#region LOCAL STORAGE:- DOCUMENT
    // const documentJson = await this.adapter.model
    //   .find({
    //     "name": constantUtil.DOCUMENTS,
    //   })
    //   .lean();

    // const documentsObjectFn = (data) => {
    //   const documentsObject = {};
    //   documentJson.forEach((documents) => {
    //     if (documents.data.docsFor === data) {
    //       documentsObject[documents._id] = {
    //         "_id": documents._id,
    //         "id": documents._id,
    //         ...documents.data,
    //       };
    //     }
    //   });
    //   return documentsObject;
    // };
    // await storageUtil.write(
    //   constantUtil.PROFILEDOCUMENTS,
    //   documentJson ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS) : {}
    // );
    // await storageUtil.write(
    //   constantUtil.DRIVERDOCUMENTS,
    //   documentJson ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS) : {}
    // );
    // //#endregion LOCAL STORAGE:- DOCUMENT

    // await this.broker.emit(
    //   "professional.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId",
    //   {
    //     // "serviceType": context.params.docsServiceType,
    //     "serviceTypeId": context.params.docsServiceTypeId,
    //   }
    // ); // UPDATE ALL PROFESSIONALS
  } catch (e) {
    message = constantUtil.FAILED;
  }
  return message;
};

adminEvent.updateGeneralSettingTimestampInDBAndRedis = async function (
  context
) {
  let timestampData = null,
    // registrationSetupData = null,
    message = constantUtil.SUCCESS;
  const timeStamp = Date.now();
  try {
    // if (context.params.type === constantUtil.CONST_REGISTRATIONSETTING_REDIS) {
    //   registrationSetupData = await this.adapter.model
    //     .findOne(
    //       {
    //         "name": constantUtil.GENERALSETTING,
    //       },
    //       { "data.registrationSetupUpdateTime": 1 }
    //     )
    //     .lean();
    // }
    switch (context.params.type) {
      case constantUtil.CONST_REGISTRATIONSETTING:
        timestampData = {
          "data.registrationSetupUpdateTime": timeStamp,
          "data.registrationSetupRedisUpdateTime": timeStamp,
        };
        break;

      case constantUtil.CONST_REGISTRATIONSETTING_REDIS:
        timestampData = {
          "data.registrationSetupRedisUpdateTime": timeStamp,
        };
        break;

      case constantUtil.MAPSETTING:
        timestampData = { "data.mapKeyUpdateTime": timeStamp };
        break;

      case constantUtil.PACKAGESETTING:
        timestampData = { "data.packageUpdateTime": timeStamp };
        break;

      case constantUtil.AIRPORT:
        timestampData = { "data.airportUpdateTime": timeStamp };
        break;

      case constantUtil.POPULARPLACE:
        timestampData = { "data.popularplaceUpdateTime": timeStamp };
        break;

      case constantUtil.POPULARDESTINATION:
        timestampData = { "data.popularDestinationUpdateTime": timeStamp };
        break;

      case constantUtil.TOLL:
        timestampData = { "data.tollUpdateTime": timeStamp };
        break;

      case constantUtil.COUPON:
        timestampData = { "data.couponUpdateTime": timeStamp };
        break;

      case constantUtil.CALLCENTER:
        timestampData = { "data.callCenterUpdateTime": timeStamp };
        break;
    }
    const settingsUpdate = await this.adapter.model
      .findOneAndUpdate(
        {
          "name": constantUtil.GENERALSETTING,
          //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        },
        {
          ...timestampData,
        },
        { "new": true }
      )
      .lean();
    //
    settingsUpdate["id"] = settingsUpdate._id;
    await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  } catch (e) {
    message = constantUtil.FAILED;
  }
  return message;
};

adminEvent.updateDataInRedis = async function (context) {
  let message = constantUtil.SUCCESS;
  const timeStamp = Date.now();
  try {
    const type = context.params.type?.toUpperCase();
    const category = context.params.category?.toUpperCase();
    switch (type) {
      case constantUtil.DOCUMENTS:
        {
          /* LOCAL STORAGE:- DOCUMENT */
          const documentJson = await this.adapter.model
            .find({ "name": constantUtil.DOCUMENTS })
            .lean();

          const documentsObjectFn = (data) => {
            const documentsObject = {};
            documentJson.forEach((documents) => {
              if (documents.data.docsFor === data) {
                documentsObject[documents._id] = {
                  "id": documents._id,
                  "_id": documents._id,
                  "type": type,
                  "name": documents.data.docsFor,
                  "data": {
                    "status": documents.data.status,
                    "clientId": documents.data.clientId,
                  },
                  ...documents.data,
                  "createdAt": documents.createdAt,
                  "updatedAt": documents.updatedAt,
                };
              }
            });
            return documentsObject;
          };
          await storageUtil.write(
            constantUtil.PROFILEDOCUMENTS,
            documentJson ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS) : []
          );
          await storageUtil.write(
            constantUtil.DRIVERDOCUMENTS,
            documentJson ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS) : []
          );
          /* LOCAL STORAGE:- DOCUMENT */
        }
        break;

      case constantUtil.CALLCENTER:
        {
          /* LOCAL STORAGE:- DOCUMENT */
          const documentJson = await this.adapter.model
            .find({ "name": constantUtil.CALLCENTER })
            .lean();
          await storageUtil.write(constantUtil.CALLCENTER, documentJson);
          /* LOCAL STORAGE:- DOCUMENT */
        }
        break;

      case constantUtil.CANCELLATIONREASON:
        /* LOCAL STORAGE:- DOCUMENT */
        const documentJson = await this.adapter.model
          .find({
            "name": type,
          })
          .lean();
        const documentJsonObject = [];
        documentJson.forEach((reason) => {
          documentJsonObject.push({
            "_id": reason._id,
            "id": reason._id,
            ...reason.data,
            "name": type,
            "data": {
              "status": reason.data.status,
              "clientId": reason.data.clientId,
            },
          });
        });
        await storageUtil.write(type, documentJsonObject);
        /* LOCAL STORAGE:- DOCUMENT */
        break;

      case constantUtil.COMMON:
        {
          /* LOCAL STORAGE:- DOCUMENT */
          const documentJson = await this.adapter.model
            .find({
              "type": constantUtil.COMMON,
              "name": category,
              "isActive": true,
            })
            .lean();
          await storageUtil.write(category, documentJson);
          /* LOCAL STORAGE:- DOCUMENT */
        }
        break;

      default:
        {
          /* LOCAL STORAGE:- DOCUMENT */
          const documentJson = await this.adapter.model
            .find({ "name": type })
            .lean();
          await storageUtil.write(type, documentJson);
          /* LOCAL STORAGE:- DOCUMENT */
        }
        break;
    }
  } catch (e) {
    message = constantUtil.FAILED;
  }
  return message;
};

adminEvent.getRegistrationDocumentUsingServiceTypeId = async function (
  context
) {
  //#region Profile Documents
  const documentJson = await this.adapter.model
    .find({
      "data.status": constantUtil.ACTIVE,
      "data.docsServiceTypeId": mongoose.Types.ObjectId(
        context.params.serviceTypeId
      ),
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  // console.log(
  //   "cronCheckDocumentsExpireCheck___" + JSON.stringify(documentJson)
  // );
  const documentsObjectFn = (docsFor) => {
    const documentsObject = [];
    documentJson.forEach((documents) => {
      if (documents.data.docsFor === docsFor) {
        documentsObject.push({
          "id": documents._id,
          "_id": documents._id,
          ...documents.data,
          "name": documents.data.docsFor,
          "data": {
            "status": documents.data.status,
            "clientId": documents.data.clientId,
          },
        });
      }
    });
    return documentsObject;
  };
  //#endregion Profile Documents
  const profileDocumentRideList = documentsObjectFn(
    constantUtil.PROFILEDOCUMENTS
  );
  //#region Vehicle & Driver Documents
  const driverDocumentRideList = documentsObjectFn(
    constantUtil.DRIVERDOCUMENTS
  );
  //#endregion Vehicle & Driver Documents
  return {
    "profileDocumentRideList": profileDocumentRideList,
    "driverDocumentRideList": driverDocumentRideList,
  };
};

adminEvent.updateWalletAmountTripEnded = async function (context) {
  // const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const corporateId = mongoose.Types.ObjectId(context.params.corporateId);
  const userData = await this.adapter.model
    .findOne(
      { "_id": corporateId },
      {
        "_id": 1,
        "data.clientId": 1,
        "data.officeName": 1,
        "data.amount": 1,
        "data.freezedAmount": 1,
      }
    )
    .lean();

  let availableAmount = parseFloat(userData?.data?.amount || 0);
  if (context.params.paymentType === constantUtil.DEBIT) {
    availableAmount = availableAmount - parseFloat(context.params.amount);
  } else if (context.params.paymentType === constantUtil.CREDIT) {
    availableAmount = availableAmount + parseFloat(context.params.amount);
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      { "_id": corporateId },
      { "data.amount": availableAmount },
      { "new": true }
    )
    .lean();
  // if (!responseJson.nModified) {
  //   return null;
  // } else {
  //-------- Transaction Entry Start ---------------------
  await this.broker.emit("transaction.corporateRideWalletPayTransaction", {
    "clientId": userData?.data?.clientId?.toString(),
    "bookingId": context.params.bookingId?.toString(),
    "refBookingId": context.params.refBookingId?.toString(),
    "corporateId": responseJson?._id?.toString(),
    "corporateData": responseJson.data,
    "paymentType": context.params.paymentType,
    "amount": context.params.amount,
    "previousBalance": userData?.data?.amount || 0,
    "currentBalance": availableAmount,
    "transactionType": context.params.transactionType,
    "transactionReason": context.params.transactionReason,
  });
  // //-------- Transaction Entry End ---------------------
  return responseJson;
  // }
};
// /#region Incentive
adminEvent.runIncentiveProgramToProfessionalByCronjob = async function (
  context
) {
  const validFrom = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000); // Last 30 Day Before
  const validTo = new Date(); // Today
  try {
    const incentiveSetupList = await this.adapter.model
      .find({
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "isActive": true,
        // "data.validFrom": {
        //   "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
        // },
        // "data.validTo": {
        //   "$lte": new Date(validTo.setHours(23, 59, 59, 999)),
        // },
        "data.userType": constantUtil.PROFESSIONAL,
        "data.validFrom": {
          "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        },
        "data.validTo": {
          "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        },
        // "$or": [
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        // ],
      })
      .sort({ "_id": -1 })
      .lean();
    if (incentiveSetupList?.length > 0) {
      await incentiveSetupList?.map(async (incentive) => {
        const clientId = incentive.data.clientId?.toString();
        //--------- incentive StartDate & EndDate --> CAMPAIGN / DAILY ------
        const incentiveStartDate =
          incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
            ? new Date(incentive.data.validFrom.setHours(0, 0, 0, 0))
            : new Date(new Date().setHours(0, 0, 0, 0));
        const incentiveEndDate =
          incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
            ? new Date(incentive.data.validTo.setHours(23, 59, 59, 999))
            : new Date(new Date().setHours(23, 59, 59, 999));
        //------------------------------------
        const todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)); // Today night 23:59:59
        const tomorrowStartTime = new Date(
          new Date(Date.now() + 1 * 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
        ); // Tomorrow Morning 00:00:00
        //------------------------------------
        const incentiveEndTime = incentive.data.endTime;
        const incentiveStartTime = incentive.data.startTime;
        const incentiveOnlineMinutes = incentive.data.onlineTime * 60; // Original --> Hours to Minutes
        // const incentiveOnlineMinutes = incentive.data.onlineTime; // For Testing
        // incentive.data.endTime.getHours();
        // If the Incentive ValidTo is Expired and start next Day(new Day) then Incentive Run Automatically
        if (
          incentiveEndDate >= todayEndTime &&
          incentiveEndDate <= tomorrowStartTime
        ) {
          //---------------- Professional List -----------------------
          let professionalList = await this.broker.emit(
            "professional.getProfessionalListByServiceAreaId",
            {
              "clientId": clientId,
              "serviceAreaId": incentive.data?.serviceAreaId?.toString(),
            }
          );
          professionalList = professionalList && professionalList[0];
          //---------------- Professional List -----------------------
          if (professionalList?.length > 0) {
            await professionalList?.map(async (professional) => {
              //---------------- Calculate Online Minitues -----------------
              let onlineMinutesData = await this.broker.emit(
                "log.calculateOnlineMinutes",
                {
                  "clientId": clientId,
                  "userType": constantUtil.PROFESSIONAL,
                  "professionalId": professional._id?.toString(),
                  "startDate": incentiveStartDate,
                  "endDate": incentiveEndDate,
                }
              );
              onlineMinutesData = onlineMinutesData && onlineMinutesData[0];
              //---------------- Calculate Online Minitues -----------------
              //---------------- Count Accepted Ride & Ended Ride -----------
              let professionalRideData = await this.broker.emit(
                "booking.getRideListByProfessionalId",
                {
                  "clientId": clientId,
                  "professionalId": professional._id?.toString(),
                  "startDate": incentiveStartDate,
                  "endDate": incentiveEndDate,
                  "minimumReviewRating": incentive.data.minimumReviewRating,
                  "rideType": incentive.data.serviceType, // Need to Check
                }
              );
              professionalRideData =
                professionalRideData && professionalRideData[0];
              //---------------- Count Accepted Ride & Ended Ride -----------
              //---------------- Update Professional Incentive Amount -------
              if (
                onlineMinutesData?.onlineMinutes > 0 &&
                professionalRideData?.allRideCount > 0
              ) {
                // const rideAcceptanceRate =
                //   professionalrideData?.allRideCount /
                //   professionalrideData?.endedRideCount;
                let incentiveAmount = incentive.data?.amount || 0;
                const campaignType = incentive.data?.campaignType; // --> GUARANTEE / BONUS
                if (campaignType === constantUtil.CONST_GUARANTEE) {
                  incentiveAmount =
                    incentiveAmount -
                    professionalRideData.professionalCommision;
                }
                if (
                  onlineMinutesData?.onlineMinutes >= incentiveOnlineMinutes &&
                  professionalRideData?.allRideCount >=
                    incentive.data.totalNoOfRides &&
                  professionalRideData?.endedRideCount >=
                    incentive.data.rideAcceptanceRate &&
                  incentiveAmount > 0
                ) {
                  this.broker.emit(
                    "professional.calculateAndUpdateIncentiveAmount",
                    {
                      "clientId": clientId,
                      "professionalId": professional._id?.toString(),
                      "onlineMinutes": onlineMinutesData?.onlineMinutes || 0,
                      "allRideCount": professionalRideData?.allRideCount || 0,
                      "availableAmount":
                        professional.wallet.availableAmount || 0,
                      "amount": incentiveAmount,
                      "incentiveId": incentive._id?.toString(),
                    }
                  );
                }
              }
              //---------------- Update Professional Incentive Amount -------
            });
          }
        }
      });
    }
  } catch (e) {}
};

adminEvent.runIncentiveProgramToUserByCronjob = async function (context) {
  const validFrom = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000); // Last 30 Day Before
  const validTo = new Date(); // Today
  try {
    const incentiveSetupList = await this.adapter.model
      .find({
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "isActive": true,
        // "data.validFrom": {
        //   "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
        // },
        // "data.validTo": {
        //   "$lte": new Date(validTo.setHours(23, 59, 59, 999)),
        // },
        "data.userType": constantUtil.USER,
        "data.validFrom": {
          "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        },
        "data.validTo": {
          "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        },
        // "$or": [
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        // ],
      })
      .sort({ "_id": -1 })
      .lean();
    if (incentiveSetupList?.length > 0) {
      await incentiveSetupList?.map(async (incentive) => {
        const clientId = incentive.data.clientId?.toString();
        //--------- incentive StartDate & EndDate --> CAMPAIGN / DAILY ------
        const incentiveStartDate =
          incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
            ? new Date(incentive.data.validFrom.setHours(0, 0, 0, 0))
            : new Date(new Date().setHours(0, 0, 0, 0));
        const incentiveEndDate =
          incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
            ? new Date(incentive.data.validTo.setHours(23, 59, 59, 999))
            : new Date(new Date().setHours(23, 59, 59, 999));
        //------------------------------------
        const todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)); // Today night 23:59:59
        const tomorrowStartTime = new Date(
          new Date(Date.now() + 1 * 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
        ); // Tomorrow Morning 00:00:00
        //------------------------------------
        const incentiveEndTime = incentive.data.endTime;
        const incentiveStartTime = incentive.data.startTime;
        const incentiveOnlineMinutes = incentive.data.onlineTime * 60; // Original --> Hours to Minutes
        // const incentiveOnlineMinutes = incentive.data.onlineTime; // For Testing
        // incentive.data.endTime.getHours();
        // If the Incentive ValidTo is Expired and start next Day(new Day) then Incentive Run Automatically
        if (
          incentiveEndDate >= todayEndTime &&
          incentiveEndDate <= tomorrowStartTime
        ) {
          //---------------- User List -----------------------
          let userList = await this.broker.emit(
            "user.getUserListByServiceAreaId",
            {
              "clientId": clientId,
              "serviceAreaId": incentive.data?.serviceAreaId?.toString(),
            }
          );
          userList = userList && userList[0];
          //---------------- Professional List -----------------------
          if (userList?.length > 0) {
            await userList?.map(async (user) => {
              // //---------------- Calculate Online Minitues -----------------
              // let onlineMinutesData = await this.broker.emit(
              //   "log.calculateOnlineMinutes",
              //   {
              //     "clientId": clientId,
              //     "userId": user._id?.toString(),
              //     "startDate": incentiveStartDate,
              //     "endDate": incentiveEndDate,
              //   }
              // );
              // onlineMinutesData = onlineMinutesData && onlineMinutesData[0];
              // //---------------- Calculate Online Minitues -----------------
              //---------------- Count Accepted Ride & Ended Ride -----------
              let userRideData = await this.broker.emit(
                "booking.getRideListByUserId",
                {
                  "clientId": clientId,
                  "userId": user._id?.toString(),
                  "startDate": incentiveStartDate,
                  "endDate": incentiveEndDate,
                  "minimumReviewRating": incentive.data.minimumReviewRating,
                  "rideType": incentive.data.serviceType, // Need to Check
                  "minimumEligibleAmount": parseFloat(
                    incentive?.data?.minimumEligibleAmount || 0
                  ),
                }
              );
              userRideData = userRideData && userRideData[0];
              //---------------- Count Accepted Ride & Ended Ride -----------
              //---------------- Update Professional Incentive Amount -------
              if (
                // onlineMinutesData?.onlineMinutes > 0 &&
                userRideData?.allRideCount > 0
              ) {
                // const rideAcceptanceRate =
                //   professionalrideData?.allRideCount /
                //   professionalrideData?.endedRideCount;
                let incentiveAmount = incentive.data?.amount || 0;
                const campaignType = incentive.data?.campaignType; // --> GUARANTEE / BONUS
                // if (campaignType === constantUtil.CONST_GUARANTEE) {
                //   incentiveAmount =
                //     incentiveAmount - userRideData.professionalCommision;
                // }
                if (
                  userRideData?.allRideCount >= incentive.data.totalNoOfRides &&
                  userRideData?.totalNoOfEligibleRides >=
                    incentive.data.rideAcceptanceRate &&
                  incentiveAmount > 0
                ) {
                  this.broker.emit("user.calculateAndUpdateIncentiveAmount", {
                    "clientId": clientId,
                    "userId": user._id?.toString(),
                    "onlineMinutes": 0, //onlineMinutesData?.onlineMinutes || 0,
                    "allRideCount": userRideData?.allRideCount || 0,
                    "availableAmount": user.wallet.availableAmount || 0,
                    "amount": incentiveAmount,
                    "incentiveId": incentive._id?.toString(),
                  });
                }
              }
              //---------------- Update Professional Incentive Amount -------
            });
          }
        }
      });
    }
  } catch (e) {}
};

adminEvent.checkAndValidateIncentiveRuleByServiceAreaId = async function (
  context
) {
  // const validFrom = new Date(); // Today
  // const validTo = new Date(); // Today
  let incentiveSetup = {},
    isValid = false;
  const currentDateTime = new Date();
  try {
    incentiveSetup = await this.adapter.model
      .findOne({
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "isActive": true,
        "data.userType": context.params.userType, // USER / PROFESSIONAL
        "data.serviceAreaId": mongoose.Types.ObjectId(
          context.params.serviceAreaId
        ),
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        "data.validFrom": {
          // "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
          "$lte": currentDateTime,
          // "$gte": new Date(),
        },
        "data.validTo": {
          // "$lte": new Date(new Date().setHours(23, 59, 59, 999)),
          // "$lte": new Date(),
          "$gte": currentDateTime,
        },
      })
      .sort({ "_id": 1 })
      .lean();
  } catch (e) {
    isValid = false;
  }
  // console.log("---------- INCENTIVE -------------------");
  console.log({
    "type": constantUtil.COMMON,
    "name": constantUtil.CONST_INCENTIVE,
    "isActive": true,
    "data.userType": context.params.userType,
    "data.serviceAreaId": mongoose.Types.ObjectId(context.params.serviceAreaId),
    "data.validFrom": {
      // "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
      "$lte": currentDateTime,
      // "$gte": new Date(),
    },
    "data.validTo": {
      // "$lte": new Date(new Date().setHours(23, 59, 59, 999)),
      // "$lte": new Date(),
      "$gte": currentDateTime,
    },
  });
  //--------- incentive StartDate & EndDate ------
  // const incentiveStartDate =
  //   incentive.data.incentiveType == CONST_CAMPAIGN
  //     ? new Date(incentive.data.validFrom.setHours(0, 0, 0, 0))
  //     : new Date(new Date().setHours(0, 0, 0, 0));
  // const incentiveEndDate =
  //   incentive.data.incentiveType == CONST_CAMPAIGN
  //     ? new Date(incentive.data.validTo.setHours(23, 59, 59, 999))
  //     : new Date(new Date().setHours(23, 59, 59, 999));
  // //------------------------------------
  // let todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)); // Today night 23:59:59
  // let tomorrowStartTime = new Date(
  //   new Date(Date.now() + 1 * 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
  // ); // Tomorrow Morning 00:00:00
  //------------------------------------
  const daysList = [
    "SUNDAY",
    "MONDAY",
    "TUESDAY",
    "WEDNESDAY",
    "THURSDAY",
    "FRIDAY",
    "SATURDAY",
  ];
  var dayName = daysList[currentDateTime.getDay()];
  if (incentiveSetup) {
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      //#region Time Period
      const incentiveStartTime = new Date(
        new Date(incentiveSetup.data.validFrom).setHours(
          incentiveSetup.data.startTime.getHours(),
          incentiveSetup.data.startTime.getMinutes(),
          incentiveSetup.data.startTime.getSeconds(),
          incentiveSetup.data.startTime.getMilliseconds()
        )
      );
      const incentiveEndTime = new Date(
        new Date(incentiveSetup.data.validTo).setHours(
          incentiveSetup.data.endTime.getHours(),
          incentiveSetup.data.endTime.getMinutes(),
          incentiveSetup.data.endTime.getSeconds(),
          incentiveSetup.data.endTime.getMilliseconds()
        )
      );
      const incentiveStartTime1 = new Date(
        new Date(incentiveSetup.data.validFrom).setHours(
          incentiveSetup.data.startTime1?.getHours(),
          incentiveSetup.data.startTime1?.getMinutes(),
          incentiveSetup.data.startTime1?.getSeconds(),
          incentiveSetup.data.startTime1?.getMilliseconds()
        )
      );
      const incentiveEndTime1 = new Date(
        new Date(incentiveSetup.data.validTo).setHours(
          incentiveSetup.data.endTime1?.getHours(),
          incentiveSetup.data.endTime1?.getMinutes(),
          incentiveSetup.data.endTime1?.getSeconds(),
          incentiveSetup.data.endTime1?.getMilliseconds()
        )
      );
      //
      const incentiveStartTime2 = new Date(
        new Date(incentiveSetup.data.validFrom).setHours(
          incentiveSetup.data.startTime2?.getHours(),
          incentiveSetup.data.startTime2?.getMinutes(),
          incentiveSetup.data.startTime2?.getSeconds(),
          incentiveSetup.data.startTime2?.getMilliseconds()
        )
      );
      const incentiveEndTime2 = new Date(
        new Date(incentiveSetup.data.validTo).setHours(
          incentiveSetup.data.endTime2?.getHours(),
          incentiveSetup.data.endTime2?.getMinutes(),
          incentiveSetup.data.endTime2?.getSeconds(),
          incentiveSetup.data.endTime2?.getMilliseconds()
        )
      );
      //#endregion Time Period
      if (
        ((currentDateTime >= incentiveStartTime &&
          currentDateTime <= incentiveEndTime) ||
          (currentDateTime >= incentiveStartTime1 &&
            currentDateTime <= incentiveEndTime1) ||
          (currentDateTime >= incentiveStartTime2 &&
            currentDateTime <= incentiveEndTime2)) &&
        incentiveSetup?.data?.days?.indexOf(dayName) !== -1 // Day Exists In
      ) {
        isValid = true;
      }
    } else if (context.params.userType === constantUtil.USER) {
      // Day Exists In
      if (incentiveSetup?.data?.days?.indexOf(dayName) !== -1) {
        isValid = true;
      }
    }
    // console.log(JSON.stringify(incentiveSetup));
    // console.log(
    //   "---------- dayName : " +
    //     dayName +
    //     ", isvalid : " +
    //     isValid +
    //     ",currentDateTime: " +
    //     currentDateTime +
    //     " incentiveStartTime : " +
    //     incentiveStartTime +
    //     " incentiveEndTime : " +
    //     incentiveEndTime +
    //     "-------------------"
    // );
    // console.log("---------- INCENTIVE -------------------");
  }
  return isValid;
  // return {
  //   "validFrom": incentiveSetupList?.data?.validFrom,
  //   "validTo": incentiveSetupList?.data?.validTo,
  //   "startTime": incentiveSetupList?.data?.startTime,
  //   "endTime": incentiveSetupList?.data?.endTime,
  //   "days": incentiveSetupList?.data?.days,
  // };
};
// /#endregion Incentive
// /#region Clients Base Data
adminEvent.manageClientsBaseData = async function (context) {
  try {
    const baseDataList = await this.adapter.model
      .find({
        "dataType": constantUtil.BASEDATA,
        // "name": constantUtil.GENERALSETTING,
      })
      .lean();
    const privilegesData = await this.adapter.model
      .findOne(
        {
          "dataType": constantUtil.BASEDATA,
          "name": constantUtil.PLANS,
          "data.role": constantUtil.BASEDATA,
        },
        { "data.privileges": 1 }
      )
      .lean();
    if (baseDataList?.length > 0) {
      await baseDataList?.map(async (baseData) => {
        try {
          //---------------------
          baseData["dataType"] = constantUtil.CLIENTDATA;
          baseData["data"]["clientId"] = context.params.clientId;
          baseData["data"]["bookingPrefix"] = `${customAlphabet(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            2
          )()}`;
          switch (baseData.name) {
            case constantUtil.GENERALSETTING:
              baseData["data"]["privileges"] =
                privilegesData?.data?.privileges || {};
              break;

            // case constantUtil.DEVELOPER:
            //   baseData["data"]["phone"]["code"] = context.params.phoneCode;
            //   baseData["data"]["phone"]["code"] = context.params.phoneCode;
            //   baseData["data"]["phone"]["code"] = context.params.phoneNumber;
            //   break;

            // case "":
            //   break;
          }
          //---------------------
          // const clientBaseDataCount = await this.adapter.model.count({
          //   "dataType": constantUtil.CLIENTDATA,
          //   "name": baseData.name,
          //   "type": baseData.type,
          //   "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
          // });
          // if (clientBaseDataCount === 0) {
          await this.adapter.insert({
            "dataType": constantUtil.CLIENTDATA,
            "name": baseData.name,
            "type": baseData.type,
            "isActive": baseData.isActive || true,
            "data": baseData.data,
          });
          // }
          //Update Redis Data
          if (baseData.type === constantUtil.COMMON) {
            await this.broker.emit("admin.updateDataInRedis", {
              "clientId": context.params?.clientId,
              "type": baseData.type,
              "category": baseData.name,
            });
          } else {
            await this.broker.emit("admin.updateDataInRedis", {
              "clientId": context.params?.clientId,
              "type": baseData.type,
            });
          }
        } catch (e) {
          console.log(e.message);
        }
      });
    }
  } catch (e) {
    const x = e.message;
  }
};
adminEvent.getClientsBaseDataFromRedisUsingTypeAndClientId = async function (
  context
) {
  let config;
  try {
    const configList = await storageUtil.read(context.params.type);
    if (configList.length > 0) {
      switch (context.params.type) {
        case constantUtil.GENERALSETTING:
        case constantUtil.MAPSETTING:
        case constantUtil.SMSSETTING:
        case constantUtil.CALLSETTING:
        case constantUtil.INVITEANDEARN:
        case constantUtil.SMTPSETTING:
        case constantUtil.PACKAGESETTING:
          // case constantUtil.FIREBASESETTING:
          {
            Object.values(configList)
              .filter(
                (each) =>
                  each.name === context.params.type &&
                  each.data.clientId === context.params.clientId &&
                  (each.data.status === constantUtil.ACTIVE ||
                    each.isActive === true)
              )
              .forEach((item) => {
                config = item;
              });
          }
          break;

        case constantUtil.FIREBASESETTING:
          config =
            configList?.filter((each) => {
              each.clientId === context.params.clientId;
            })?.[0] || {};
          break;

        case constantUtil.PAYMENTGATEWAY:
          {
            config = [];
            Object.values(configList)
              .filter(
                (each) =>
                  each.type === context.params.type &&
                  each.data.clientId === context.params.clientId &&
                  (each.data.status === constantUtil.ACTIVE ||
                    each.isActive === true)
              )
              .forEach((item) => {
                config.push(item);
              });
          }
          break;

        case constantUtil.VEHICLECATEGORY:
          {
            config = [];
            Object.values(configList)
              .filter(
                (each) =>
                  each.name === context.params.type &&
                  each.data.clientId === context.params.clientId // &&
                // (each.data.status === constantUtil.ACTIVE ||
                //   each.isActive === true)
              )
              .forEach((item) => {
                config.push(item);
              });
          }
          break;

        default:
          {
            config = [];
            Object.values(configList)
              .filter(
                (each) =>
                  each.name === context.params.type &&
                  each.data.clientId === context.params.clientId &&
                  (each.data.status === constantUtil.ACTIVE ||
                    each.isActive === true)
              )
              .forEach((item) => {
                config.push(item);
              });
          }
          break;
      }
    }
  } catch (e) {
    const x = e.message;
  }
  return config;
};

adminEvent.updateClientsBaseDataFromRedisUsingType = async function (context) {
  let responseJson,
    updateFieldData = {},
    status = constantUtil.SUCCESS;
  try {
    if (
      context.params.action === constantUtil.UPDATE &&
      context.params.updateKeyName &&
      context.params.clientId
    ) {
      switch (context.params.updateKeyName) {
        case constantUtil.MAPSETTING:
          updateFieldData = { "data.mapKeyUpdateTime": Date.now() };
          break;

        case constantUtil.PACKAGESETTING:
          updateFieldData = { "data.packageUpdateTime": Date.now() };
          break;

        case constantUtil.AIRPORT:
          updateFieldData = { "data.airportUpdateTime": Date.now() };
          break;

        case constantUtil.TOLL:
          updateFieldData = { "data.tollUpdateTime": Date.now() };
          break;

        case constantUtil.POPULARPLACE:
          updateFieldData = { "data.popularplaceUpdateTime": Date.now() };
          break;

        case constantUtil.COUPON:
          updateFieldData = { "data.couponUpdateTime": Date.now() };
          break;
      }
      await this.adapter.model.updateOne(
        {
          "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
          "name": constantUtil.GENERALSETTING,
          "type": constantUtil.GENERALSETTING,
        },
        {
          ...updateFieldData,
        }
      );
      //
      const updateKeyJson = await this.adapter.model
        .find({
          "name": context.params.updateKeyName,
          "type": context.params.updateKeyName,
        })
        .lean();
      await updateKeyJson.map((item) => {
        item["id"] = item._id;
      });
      // Update Data in Redis
      await storageUtil.write(context.params.updateKeyName, updateKeyJson);
    }
    // Get Data
    responseJson = await this.adapter.model
      .find({
        "name": context.params.name,
        "type": context.params.name,
      })
      .lean();
    await responseJson.map((item) => {
      item["id"] = item._id;
    });
    // Update Data in Redis
    await storageUtil.write(context.params.name, responseJson);
  } catch (e) {
    status = constantUtil.FAILED;
  }
  return status;
};
// /#endregion Clients Base Data
// #region Airport
adminEvent.getAllAirportData = async function (context) {
  let responseJson = [];
  try {
    responseJson = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "type": constantUtil.AIRPORT,
            "name": constantUtil.AIRPORT,
            // "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
            "data.status": constantUtil.ACTIVE,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "city": "$data.serviceArea",
            "airportShortName": "$data.airportShortName",
            "airportLongName": "$data.airportLongName",
            "airportDescription": "$data.airportDescription",
            "airportImage": "$data.airportImage",
            "location": "$data.location",
            "locationAddress": "$data.locationAddress",
            "locationCoordinates": "$data.locationCoordinates",
            // "stops": "$data.stops",
            // "stops": {
            //   "$cond": {
            //     "if": {
            //       "$ne": ["$data.stops", undefined],
            //     },
            //     "then": "$data.stops",
            //     "else": [],
            //   },
            // },
            "stops": { "$ifNull": ["$data.stops", []] },
            "status": "$data.status",
            "fareCategories": { "$ifNull": ["$data.tollCategorys", []] },
          },
        },
      ])
      .allowDiskUse(true);
  } catch (e) {
    responseJson = [];
  }
  return responseJson;
};
// #endregion Airport
//-------------------------------------
module.exports = adminEvent;
