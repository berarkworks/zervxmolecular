const turf = require("@turf/turf");
const mongoose = require("mongoose");
const { MoleculerError } = require("moleculer").Errors;
const each = require("sync-each");
const pdf = require("html-pdf");
const converter = require("number-to-words");
const { customAlphabet } = require("nanoid");
const { google } = require("googleapis");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtils = require("../utils/helper.util");

const { bookingObject } = require("../utils/mapping.util");
const mappingUtils = require("../utils/mapping.util");
const invoiceUtils = require("../utils/invoice.utils");
const helperUtil = require("../utils/helper.util");
const googleApiUtil = require("../utils/googleApi.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const {
  setDayStartHours,
  setDayEndHours,
  getDbQueryString,
} = require("../utils/common.util");

const bookingEvent = {};

bookingEvent.notifyChecklistEnabledProfessional = async function (context) {
  let notificationObject = {},
    professionalData = {},
    temprorySoundData = {};
  const clientId = context.params.clientId;

  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  const { PROFESSIONAL_NEW_RIDE } = notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("category")
    .lean();
  if (bookingData.bookingStatus === constantUtil.AWAITING) {
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    if (context.params.isWish === true) {
      if (bookingData.wishList.length > 0) {
        professionalData = await this.broker.emit(
          "professional.getWishedProfessionalList",
          {
            "clientId": context.params?.clientId,
            "professionalsList": bookingData.wishList,
          }
        );
      }
    } else {
      const radius =
        parseInt(generalSettings.data.requestDistance) +
        parseInt(generalSettings.data.retryRequestDistance);
      temprorySoundData = { "temprorySound": true };
      professionalData = await this.getProfessionalByLocation(
        //clientId,
        bookingData.origin.lat,
        bookingData.origin.lng,
        bookingData.destination.lat,
        bookingData.destination.lng,
        bookingData.vehicle.vehicleCategoryId.toString(),
        constantUtil.BOOKING, // actionType
        context.params.rideType,
        context.params.radius /* @TODO FROM GENERAL SETTINGS */,
        // context.params?.maxRadius || radius, // maxRadius
        true,
        true,
        generalSettings.data.isTailRideNeeded,
        true,
        true,
        bookingData.isGenderAvailable,
        bookingData.childseatAvailable,
        bookingData.handicapAvailable,
        !!(
          bookingData.user?.gender === constantUtil.MALE &&
          bookingData?.category?.isRestrictMaleBookingToFemaleProfessional
        ),
        bookingData.tripType === constantUtil.RENTAL, //isRentalSupported
        context.params?.serviceAreaId,
        bookingData.isShareRide,
        bookingData?.payment.option || constantUtil.CASH,
        bookingData?.languageCode || null,
        bookingData?.isPetAllowed || false
      );
    }
    professionalData = professionalData && professionalData[0];
    //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    // userData = await this.broker.emit("user.getById", {
    //   "id": context.meta.userId.toString(),
    // });
    professionalData =
      await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
        professionalData,
        bookingData,
        bookingData.user
      );
    //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    if ((professionalData?.length || 0) > 0) {
      bookingData["retryTime"] = generalSettings.data.driverRequestTimeout;
      notificationObject = {
        ...temprorySoundData,
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_BOOKINGREQUEST,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_NEW_RIDE,
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": professionalData?.map((professional) => {
          if (
            // parseFloat(professional.wallet.availableAmount) <
            // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
            bookingData.payment.option === constantUtil.WALLET ||
            bookingData.payment.option === constantUtil.PAYMENTCARD ||
            // bookingData.payment.option !== constantUtil.CASH &&
            // bookingData.payment.option !== constantUtil.CREDIT) ||
            parseFloat(professional.wallet.availableAmount) >=
              parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
          ) {
            return {
              "token": professional?.deviceInfo[0]?.deviceId || "",
              "id": professional?._id?.toString(),
              "deviceType": professional?.deviceInfo[0]?.deviceType || "",
              "platform": professional?.deviceInfo[0]?.platform || "",
              "socketId": professional?.deviceInfo[0]?.socketId || "",
              "duration": professional?.duration,
              "distance": professional?.distance,
            };
          }
        }),
      };
      this.broker.emit("admin.sendFCM", notificationObject);
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
    } else {
      return null;
    }
  }
};

bookingEvent.notifyScheduleRideToAdmin = async function (context) {
  const notificationObject = {
    "clientId": context.params.clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": constantUtil.PROFESSIONAL,
      "action": constantUtil.ACTION_BOOKINGREQUEST,
      "timestamp": Date.now(),
      "message": "",
      "details": context.params?.detailsData || {},
    },
    "registrationTokens": [],
  };
  this.broker.emit(
    "socket.sendSecurityNotificationToAdmin",
    notificationObject
  );
};

// FOR ADMIN PANEL RIDES
bookingEvent.viewRideDetails = async function (context) {
  const { INVALID_BOOKING } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "deviceInfo": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "deviceInfo": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("vehicle.vehicleCategoryId")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("admin", { "data.accessToken": 0, "data.password": 0 })
    .populate("coorperate", { "data.password": 0, "data.accessToken": 0 })
    .populate("couponId")
    .populate("denyProfessionals.professionalId", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
    })
    .populate("cancelledProfessionalList.professionalId", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
    })
    .populate("requestSendProfessionals.professionalId", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
      // "location": 1,
    })
    .populate("adminAssignedProfessional", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
      "gender": 1,
    })
    .populate("trustedContactsLog.userId", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
      // "location": 1,
    })
    .populate("trustedContactsLog.professionalId", {
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
      // "location": 1,
    })
    .lean();

  if (!responseJson) {
    return { "code": 0, "data": {}, "message": INVALID_BOOKING };
  }
  // responseJson = responseJson.toJSON();
  // -------- Assign Distance Value Start---------------
  const distanceTypeUnitValue =
    (responseJson?.category?.distanceType ?? constantUtil.KM).toUpperCase() ===
    constantUtil.KM
      ? constantUtil.KM_VALUE
      : constantUtil.MILES_VALUE;
  responseJson["category"]["distanceTypeUnitValue"] = distanceTypeUnitValue;
  // -------- Assign Distance Value End---------------
  return { "code": 1, "data": responseJson, "message": "" };
};

bookingEvent.getNewRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        // "professional.vehicles.hub": mongoose.Types.ObjectId(
        //   context.params.loginUserId
        // ),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  const query = [
    {
      "$match": {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "guestType": constantUtil.USER,
        "bookingStatus": constantUtil.AWAITING,
        "bookingType": constantUtil.INSTANT,
      },
    },
  ];
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // {
        //   "$lookup": {
        //     "from": "professionals",
        //     "localField": "professional",
        //     "foreignField": "_id",
        //     "as": "professional",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$professional",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getScheduleRidesList = async function (context) {
  const wishedTime = new Date();
  const wishedTimeStamp = 25 * 60 * 1000;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      // professionalVehicleHubQuery = {
      //   "professional.vehicles.hub": mongoose.Types.ObjectId(
      //     context.params.loginUserId
      //   ),
      professionalVehicleHubQuery = {
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      // "guestType": constantUtil.USER,  // Need to Check -----
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.SCHEDULE,
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // {
        //   "$lookup": {
        //     "from": "professionals",
        //     "localField": "professional",
        //     "foreignField": "_id",
        //     "as": "professional",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$professional",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
            "isAssignAllowed": {
              "$cond": {
                "if": {
                  "$lte": [
                    { "$subtract": ["$bookingDate", wishedTime] },
                    wishedTimeStamp,
                  ],
                },
                "then": false,
                "else": true,
              },
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getOngoingRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      // professionalVehicleHubQuery = {
      //   "professional.vehicles.hub": mongoose.Types.ObjectId(
      //     context.params.loginUserId
      //   ),
      professionalVehicleHubQuery = {
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingStatus": {
        "$in": [
          constantUtil.ACCEPTED,
          constantUtil.ARRIVED,
          constantUtil.STARTED,
        ],
      },
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getEndedRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      // professionalVehicleHubQuery = {
      //   "professional.vehicles.hub": mongoose.Types.ObjectId(
      //     context.params.loginUserId
      //   ),
      professionalVehicleHubQuery = {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }
  if ((context.params.search || "") !== "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingStatus": {
        "$in": [constantUtil.ENDED],
      },
    },
  });
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "bookingStatus": "$bookingStatus",
            "guestType": "$guestType",
            "invoice": "$invoice",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
              "phoneNumber": {
                "$concat": ["$user.phone.code", "$user.phone.number"],
              },
            },
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phoneNumber": {
                "$concat": [
                  "$professional.phone.code",
                  "$professional.phone.number",
                ],
              },
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getExpiredRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        // "professional.vehicles.hub": mongoose.Types.ObjectId(
        //   context.params.loginUserId
        // ),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "guestType": constantUtil.USER,
      "bookingStatus": {
        "$in": [constantUtil.EXPIRED, constantUtil.USERDENY],
      },
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // {
        //   "$lookup": {
        //     "from": "professionals",
        //     "localField": "professional",
        //     "foreignField": "_id",
        //     "as": "professional",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$professional",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};

bookingEvent.getProfessionalCancelledRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        // "professional.vehicles.hub": mongoose.Types.ObjectId(
        //   context.params.loginUserId
        // ),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingStatus": {
        "$in": [constantUtil.PROFESSIONALCANCELLED],
      },
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "guestType": "$guestType",
            "invoice": "$invoice",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getUserCancelledRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        // "professional.vehicles.hub": mongoose.Types.ObjectId(
        //   context.params.loginUserId
        // ),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "guestType": constantUtil.USER,
      "bookingStatus": {
        "$in": [constantUtil.USERCANCELLED],
      },
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [
        { "$sort": { "_id": -1 } },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        { "$count": "all" },
      ],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // {
        //   "$lookup": {
        //     "from": "professionals",
        //     "localField": "professional",
        //     "foreignField": "_id",
        //     "as": "professional",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$professional",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.paymentFailedIssueList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingStatus": constantUtil.ENDED,
      "payment.paid": false,
    },
  });
  // if (
  //   !!context.params.tripType === true &&
  //   [constantUtil.RENTAL, constantUtil.DAILYTRIP].includes(
  //     context.params.tripType.toUpperCase()
  //   )
  // ) {
  //   query[0].$match.tripType = context.params.tripType.toUpperCase();
  // }
  if (context.params.tripType === constantUtil.CONST_RIDE.toLowerCase()) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [
            constantUtil.CONST_RIDE.toLowerCase(),
            constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
          ],
        },
      },
    });
  } else if (
    context.params.tripType === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    query.push({
      "$match": {
        "serviceCategory": {
          "$in": [constantUtil.CONST_PACKAGES.toLowerCase()],
        },
      },
    });
  }

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "bookingId": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingDate": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "bookingType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "bookingBy": "$bookingBy",
            "invoice": "$invoice",
            "guestType": "$guestType",
            "serviceCategory": "$serviceCategory",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
            },
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getSearchRidesList = async function (context) {
  const { INVALID_BOOKING } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const responseJson = await this.adapter.model
    .findOne({
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingId": context.params.bookingId,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();

  if (!responseJson)
    return { "code": 0, "data": {}, "message": INVALID_BOOKING };

  return { "code": 1, "data": responseJson, "message": "" };
};

bookingEvent.getAssignProfessionalList = async function (context) {
  // try {
  const professionalJSON = [];
  const { INVALID_BOOKING } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  const clientId = context.params.clientId;
  const generalSettingsJson = await storageUtil.read(
    constantUtil.GENERALSETTING
  );
  const bookingData = await this.adapter.model
    .findOne({
      "_id": context.params.bookingId,
    })
    .lean();
  if (!bookingData) {
    return { "code": 0, "data": {}, "message": INVALID_BOOKING };
  }
  const radius =
    parseInt(generalSettingsJson.data.requestDistance) +
    parseInt(generalSettingsJson.data.retryRequestDistance);
  const professionalByLocation = await this.broker.emit(
    "professional.getProfessionalByLocation",
    {
      "clientId": clientId,
      "lat": bookingData.origin.lat,
      "lng": bookingData.origin.lng,
      "droplat": bookingData.destination.lat,
      "droplng": bookingData.destination.lng,
      "vehicleCategoryId":
        bookingData.isAcceptOtherCategory === true
          ? null
          : bookingData.vehicle.vehicleCategoryId.toString(),
      "professionalOnlineStatus": context.params.professionalOnlineStatus,
      "actionType": constantUtil.BOOKING,
      "rideType": context.params.rideType,
      "radius":
        context.params.checkRadius === 0
          ? radius
          : parseInt(context.params.checkRadius),
      // "maxRadius": context.params.maxRadius,
      "checkOngoingBooking": true,
      "checkLastPriority": true,
      "forTailRide": false,
      "isSubCategoryAvailable": true,
      "isForceAppliedToProfessional": true,
      "isAcceptOtherCategory": bookingData.isAcceptOtherCategory,
      "isGenderAvailable": bookingData.isGenderAvailable,
      "childseatAvailable": bookingData.childseatAvailable,
      "handicapAvailable": bookingData.handicapAvailable,
      "isRestrictMaleBookingToFemaleProfessional": false,
      "serviceAreaId": bookingData.category,
      "isShareRide": bookingData.isShareRide,
      "paymentOption": bookingData?.payment.option || constantUtil.CASH,
      "languageCode": bookingData?.languageCode || null,
      "isPetAllowed": bookingData?.isPetAllowed || false,
      "isEnableLuggage": bookingData?.isEnableLuggage || false,
    }
  );
  if (professionalByLocation.length === 0) {
    return null;
  }
  const vehicleCategoryJson = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  const to = turf.point([
    parseFloat(bookingData.origin.lat),
    parseFloat(bookingData.origin.lng),
  ]);
  // const professionalJSON = professionalByLocation[0]?.map((professional) => {
  professionalByLocation[0]?.forEach((professional) => {
    const defaultVehicle = professional?.vehicles?.filter((vehicle) => {
      vehicle = JSON.parse(JSON.stringify(vehicle));
      return (
        vehicle.defaultVehicle === true &&
        vehicle.status === constantUtil.ACTIVE
      );
    });
    if (defaultVehicle?.[0]?.vehicleCategoryId) {
      if (
        // parseFloat(professional.wallet.availableAmount) <
        // parseFloat(generalSettingsJson.data?.minimumWalletAmountToOnline) &&
        bookingData.payment.option === constantUtil.WALLET ||
        bookingData.payment.option === constantUtil.PAYMENTCARD ||
        // bookingData.payment.option !== constantUtil.CASH &&
        // bookingData.payment.option !== constantUtil.CREDIT) ||
        parseFloat(professional.wallet.availableAmount) >=
          parseFloat(generalSettingsJson.data?.minimumWalletAmountToOnline)
      ) {
        const vehicalCategoryMetaData =
          vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];

        const from = turf.point([
          parseFloat(professional.location.coordinates[1]),
          parseFloat(professional.location.coordinates[0]),
        ]);
        //--------- Distance -----------
        const distanceType = (
          professional.vehicles[0]?.serviceCategory?.distanceType ||
          constantUtil.KM
        ).toUpperCase();
        const distanceTypeUnits =
          distanceType === constantUtil.KM ? "kilometers" : "miles";
        const distanceTypeUnitValue =
          distanceType === constantUtil.KM
            ? constantUtil.KM_VALUE
            : constantUtil.MILES_VALUE;
        //--------- Distance -----------
        professionalJSON.push({
          "distance": turf
            .distance(from, to, { "units": distanceTypeUnits })
            .toFixed(2),
          "distanceType": distanceType,
          "distanceTypeUnits": distanceTypeUnits,
          "distanceTypeUnitValue": distanceTypeUnitValue,
          "bookingId": context.params.bookingId,
          "professionalId": professional._id,
          "firstName": professional.firstName,
          "lastName": professional.lastName,
          "email": professional.email,
          "phone": professional.phone,
          "wallet": professional.wallet,
          "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
          "vehicleCategoryName": vehicalCategoryMetaData.vehicleCategory,
          "avgRating": professional.review.avgRating,
          "onlineStatus": professional.onlineStatus
            ? constantUtil.CONST_ONLINE
            : constantUtil.CONST_OFFLINE,
        });
      }
    }
  });
  const responseJson = {};
  responseJson["count"] = professionalJSON.length || 0;
  responseJson["response"] = professionalJSON || {};
  return { "code": 1, "data": responseJson, "message": "" };
  // } catch (e) {
  //   const xxxx = e.message;
  // }
};

bookingEvent.sendRequstToProfessional = async function (context) {
  let bookingData,
    queryData = {};
  const clientId = context.params.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //------------- Notification Msg Start --------
  const {
    INVALID_BOOKING,
    INVALID_PROFESSIONAL,
    VEHICLE_NOT_AVAILABLE,
    BOOKING_UPDATE_FAILED,
    VEHICLE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_ACCEPTED_BOOKING,
    BOOKING_ASSIGNED_BY_ADMIN,
    BOOKING_REQUEST_ASSIGNED,
    PROFESSIONAL_NEW_RIDE,
    BOOKING_CANCELLED_BY,
    INFO_ADMIN,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  //------------- Notification Msg End --------
  // let bookingData = await this.adapter.model
  //   .findOne({
  //     "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //     "bookingStatus": constantUtil.AWAITING,
  //   })
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "trustedContacts": 0,
  //     "cards": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();

  // let bookingStatusQuery = {};
  // if (context.params.actionType === constantUtil.ACTION_REASSIGN) {
  //   bookingStatusQuery = { "bookingStatus": constantUtil.ACCEPTED };
  //   // bookingData = await this.adapter.model
  //   //   .findOne(
  //   //     {
  //   //       "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //   //       "bookingStatus": constantUtil.ACCEPTED,
  //   //     },
  //   //     {
  //   //       "bookingType": 1,
  //   //       "vehicle": 1,
  //   //       "pickupLocation": 1,
  //   //       "requestSendProfessionals": 1,
  //   //     }
  //   //   )
  //   //   .lean();
  // } else {
  //   bookingStatusQuery = { "bookingStatus": constantUtil.AWAITING };
  //   //   bookingData = await this.adapter.model
  //   //     .findOne(
  //   //       {
  //   //         "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //   //         "bookingStatus": constantUtil.AWAITING,
  //   //       },
  //   //       {
  //   //         "bookingType": 1,
  //   //         "vehicle": 1,
  //   //         "pickupLocation": 1,
  //   //         "requestSendProfessionals": 1,
  //   //       }
  //   //     )
  //   //     .lean();
  // }
  bookingData = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "bookingStatus": {
          "$in": [constantUtil.AWAITING, constantUtil.ACCEPTED],
        },
        // ...bookingStatusQuery,
      },
      {
        "_id": 1,
        "bookingType": 1,
        "bookingStatus": 1,
        "vehicle": 1,
        "pickupLocation": 1,
        "requestSendProfessionals": 1,
      }
    )
    .lean();
  if (!bookingData)
    return { "code": 0, "data": {}, "message": INVALID_BOOKING };
  //-----------------------------------
  let professionalData = await this.broker.emit("professional.getById", {
    "clientId": context.params?.clientId,
    "id": context.params.professionalId.toString(),
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData)
    return {
      "code": 0,
      "data": {},
      "message": INVALID_PROFESSIONAL,
    };

  const vehicleDetails = {};
  professionalData.vehicles.forEach((vehicle) => {
    if (
      vehicle.status === constantUtil.ACTIVE &&
      vehicle.defaultVehicle === true
    ) {
      vehicleDetails.vehicleCategoryId = bookingData.vehicle.vehicleCategoryId;
      vehicleDetails.vinNumber = vehicle.vinNumber;
      vehicleDetails.type = vehicle.type;
      vehicleDetails.plateNumber = vehicle.plateNumber;
      vehicleDetails.model = vehicle.model;
      vehicleDetails.year = vehicle.year;
      vehicleDetails.color = vehicle.color;
      vehicleDetails.noOfDoors = vehicle.noOfDoors;
      vehicleDetails.noOfSeats = vehicle.noOfSeats;
    }
  });
  if (Object.keys(vehicleDetails).length === 0)
    return {
      "code": 0,
      "data": {},
      "message": VEHICLE_NOT_AVAILABLE,
    };
  const latLng = {
    "lat": bookingData?.pickupLocation?.coordinates[0][1],
    "lng": bookingData?.pickupLocation?.coordinates[0][0],
  };
  //-----------------------------------
  let isRequestAlreadySendToProfessional = false;
  let requestSendProfessionals = bookingData.requestSendProfessionals || [];
  await requestSendProfessionals?.map((item) => {
    if (item?.professionalId?.toString() === context.params.professionalId) {
      item.count = item.count + 1;
      isRequestAlreadySendToProfessional = true;
    }
  });
  if (isRequestAlreadySendToProfessional === false) {
    requestSendProfessionals.push({
      "professionalId": professionalData?._id?.toString(),
      "requestReceiveDateTime": new Date().toISOString(),
      "count": 1,
      "location.coordinates": professionalData?.location?.coordinates,
      "duration": professionalData?.duration || 0,
      "distance": professionalData?.distance || 0,
    });
  }
  if (bookingData.bookingType === constantUtil.INSTANT) {
    // // const updatedBookingData = await this.adapter.model.updateOne(
    // //   {
    // //     "_id": context.params.bookingId,
    // //     "bookingStatus": constantUtil.AWAITING,
    // //   },
    // //   {
    // //     "professional": context.params.professionalId,
    // //     "vehicle": vehicleDetails,
    // //     "bookingStatus": constantUtil.ACCEPTED,
    // //     "activity.acceptLatLng": context.params.latLng || latLng,
    // //     "isAdminForceAssign": true,
    // //     "activity.acceptTime": new Date(),
    // //     "regionalData.acceptTime": helperUtil.toRegionalUTC(new Date()),
    // //   }
    // // );
    // // if (!updatedBookingData?.nModified)
    // //   return {
    // //     "code": 0,
    // //     "data": {},
    // //     "message": BOOKING_UPDATE_FAILED,
    // //   };

    // // bookingData = await this.adapter.model
    // //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    // //   .populate("user", {
    // //     "password": 0,
    // //     "bankDetails": 0,
    // //     "trustedContacts": 0,
    // //     "cards": 0,
    // //   })
    // //   .populate("professional", {
    // //     "password": 0,
    // //     "bankDetails": 0,
    // //     "cards": 0,
    // //     "trustedContacts": 0,
    // //   })
    // //   .populate("category")
    // //   .populate("security")
    // //   .populate("officer", { "password": 0, "preferences": 0 })
    // //   .lean();

    // bookingData = await this.adapter.model
    //   .findOneAndUpdate(
    //     {
    //       "_id": mongoose.Types.ObjectId(context.params.bookingId),
    //       "bookingStatus": constantUtil.AWAITING,
    //     },
    //     {
    //       "professional": context.params.professionalId,
    //       "vehicle": vehicleDetails,
    //       "bookingStatus": constantUtil.ACCEPTED,
    //       "activity.acceptLatLng": context.params.latLng || latLng,
    //       "isAdminForceAssign": true,
    //       "activity.bookingTime": new Date(),
    //       "activity.acceptTime": new Date(),
    //       "regionalData.acceptTime": helperUtil.toRegionalUTC(new Date()),
    //     },
    //     { "new": true }
    //   )
    //   .populate("user", {
    //     "password": 0,
    //     "bankDetailes": 0,
    //     "trustedContacts": 0,
    //     "cards": 0,
    //   })
    //   .populate("professional", {
    //     "password": 0,
    //     "cards": 0,
    //     "bankDetails": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("category")
    //   .populate("security")
    //   .populate("officer", { "password": 0, "preferences": 0 })
    //   .lean();

    // if (!bookingData)
    //   return { "code": 0, "data": {}, "message": INVALID_BOOKING };
    queryData = {
      "professional": context.params.professionalId,
      "vehicle": vehicleDetails,
      "bookingStatus": constantUtil.ACCEPTED,
      "activity.acceptLatLng": context.params.latLng || latLng,
      "isAdminForceAssign": true,
      "activity.bookingTime": new Date(),
      "activity.acceptTime": new Date(),
      "regionalData.acceptTime": helperUtil.toRegionalUTC(new Date()),
      "requestSendProfessionals": bookingData.requestSendProfessionals,
    };
    // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
    //   // For update and wallet and card payment
    //   if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
    //     const finalAmount =
    //       parseFloat(bookingData.user.wallet.availableAmount) -
    //       parseFloat(bookingData.invoice.estimationAmount);
    //     this.broker.emit("user.updateWalletAmountInBooking", {
    //       "userId": bookingData.user._id.toString(),
    //       "amount": parseFloat(bookingData.invoice.estimationAmount.toFixed(2)),
    //       "finalAmount": parseFloat(finalAmount.toFixed(2)),
    //     });
    //   }
    //   // if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
    //   //   // let paymentData = await this.broker.emit(
    //   //   //   'transaction.newBookingCharges',
    //   //   //   {
    //   //   //     'card': bookingData.card,
    //   //   //     'userId': bookingData.user._id.toString(),
    //   //   //     'amount': bookingData.invoice.estimationAmount.toFixed(2),
    //   //   //     'currencyCode': bookingData.currencyCode,
    //   //   //   }
    //   //   // )
    //   //   // paymentData = paymentData[0]

    //   //   // if (
    //   //   //   !paymentData ||
    //   //   //   paymentData === undefined ||
    //   //   //   paymentData === 'undefined' ||
    //   //   //   paymentData.code === 0
    //   //   // )
    //   //   //   return {
    //   //   //     'code': 0,
    //   //   //     'data': {},
    //   //   //     'message': 'ERROR IN CARD PAYMENT, KINDLY TRY AFTER SOMETIME',
    //   //   //   }
    //   //   this.broker.emit('user.updateWalletAmountInBooking', {
    //   //     'userId': bookingData.user._id.toString(),
    //   //     'amount': parseFloat(bookingData.invoice.estimationAmount.toFixed(2)),
    //   //     'finalAmount': parseFloat(
    //   //       bookingData.user.wallet.availableAmount.toFixed(2)
    //   //     ),
    //   //   })
    //   // }
    //   this.broker.emit("user.updateOnGoingBooking", {
    //     "userId": bookingData.user._id.toString(),
    //     "bookingId": context.params.bookingId,
    //     "bookingType": bookingData.bookingType,
    //   });
    // }
    // // if (bookingData.invoice.couponApplied === true) {
    // //   this.broker.emit("admin.updateCouponCount", {
    // //     "userId": bookingData.user._id.toString(),
    // //     "couponId": bookingData.couponId.toString(),
    // //   });
    // // }
    // this.broker.emit("professional.updateOnGoingBooking", {
    //   "professionalId": context.params.professionalId,
    //   "bookingId": context.params.bookingId,
    // });
  } else {
    if (context.params.actionType === constantUtil.ACTION_FORCEASSIGN) {
      queryData = {
        "professional": context.params.professionalId,
        "vehicle": vehicleDetails,
        "bookingStatus": constantUtil.ACCEPTED,
        "activity.acceptLatLng": context.params.latLng || latLng,
        "isAdminForceAssign": true,
        "bookingDate": new Date(),
        "activity.acceptTime": new Date(),
        "regionalData.acceptTime": helperUtil.toRegionalUTC(new Date()),
        "requestSendProfessionals": bookingData.requestSendProfessionals,
      };
    } else if (context.params.actionType === constantUtil.ACTION_REASSIGN) {
      if (bookingData.bookingStatus === constantUtil.AWAITING) {
        queryData = {
          "wishList": context.params.professionalId,
          "isAdminForceAssign": true,
          "adminAssignedProfessional": context.params.professionalId,
        };
      } else {
        queryData = {
          "professional": context.params.professionalId,
          "isAdminForceAssign": true,
          "requestSendProfessionals": bookingData.requestSendProfessionals,
        };
      }
    } else {
      queryData = {
        "wishList": context.params.professionalId,
        "isAdminForceAssign": true,
        "adminAssignedProfessional": context.params.professionalId,
      };
    }
  }
  bookingData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        // "bookingStatus": constantUtil.AWAITING,
      },
      queryData,
      { "new": true }
    )
    .populate("user", {
      "password": 0,
      "bankDetailes": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "cards": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();

  // if (!bookingData)
  //   return { "code": 0, "data": {}, "message": INVALID_BOOKING };

  //#region Vehicle Details
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData)
    return {
      "code": 0,
      "data": {},
      "message": VEHICLE_CATEGORY_NOT_FOUND,
    };
  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  // bookingData.distanceUnit = bookingData.category.distanceType;
  bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;
  //#endregion Vehicle Details

  //#region Notification
  if (context.params.actionType === constantUtil.ACTION_FORCEASSIGN) {
    if (bookingData?.user?._id) {
      // For update and wallet and card payment
      if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
        const finalAmount =
          parseFloat(bookingData.user.wallet.availableAmount) -
          parseFloat(bookingData.invoice.estimationAmount);
        this.broker.emit("user.updateWalletAmountInBooking", {
          "clientId": context.params?.clientId,
          "userId": bookingData.user._id.toString(),
          "amount": parseFloat(bookingData.invoice.estimationAmount.toFixed(2)),
          "finalAmount": parseFloat(finalAmount.toFixed(2)),
        });
      }
      this.broker.emit("user.updateOnGoingBooking", {
        "clientId": context.params?.clientId,
        "userId": bookingData.user._id.toString(),
        "bookingId": context.params.bookingId,
        "bookingType": bookingData.bookingType,
      });
    }
    this.broker.emit("professional.updateOnGoingBooking", {
      "clientId": context.params?.clientId,
      "professionalId": context.params.professionalId,
      "bookingId": context.params.bookingId,
    });
    //--------------
    const notificationObject1 = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN,
        "timestamp": Date.now(),
        "message": BOOKING_ASSIGNED_BY_ADMIN,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": [
        {
          "token": professionalData?.deviceInfo[0]?.deviceId,
          "id": professionalData?._id?.toString(),
          "deviceType": professionalData?.deviceInfo[0]?.deviceType,
          "platform": professionalData?.deviceInfo[0]?.platform,
          "socketId": professionalData?.deviceInfo[0]?.socketId,
        },
      ],
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject1);
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject1);
    //--------
    // if (bookingData.bookingBy === constantUtil.USER) {
    if (bookingData?.user?._id) {
      const notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          // "action": constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN,
          "action": constantUtil.ACTION_ACCEPTBOOKING,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_ACCEPTED_BOOKING,
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": [
          {
            "token": bookingData.user?.deviceInfo[0]?.deviceId,
            "id": bookingData.user?._id?.toString(),
            "deviceType": bookingData.user?.deviceInfo[0]?.deviceType,
            "platform": bookingData.user?.deviceInfo[0]?.platform,
            "socketId": bookingData.user?.deviceInfo[0]?.socketId,
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
    //-------- Send SMS Alert Start --------------------------
    // if (
    //   bookingData.bookingBy === constantUtil.ADMIN ||
    //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE
    // )
    {
      this.broker.emit("booking.sendSMSAlertByRideId", {
        "clientId": context.params?.clientId,
        "rideId": context.params.bookingId.toString(),
        "bookingFrom": bookingData.bookingFrom,
        "notifyType": constantUtil.ACCEPTED,
        "notifyFor": constantUtil.USER,
      });
    }
    //-------- Send SMS Alert End --------------------------
    //-------- Send SMS Alert Start --------------------------
    // if (
    //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
    //   bookingData.bookingBy === constantUtil.ADMIN ||
    //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
    //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
    // )
    {
      this.broker.emit("booking.sendSMSAlertByRideId", {
        "clientId": context.params?.clientId,
        "rideId": context.params.bookingId.toString(),
        "bookingFrom": bookingData.bookingFrom,
        "notifyType": constantUtil.FORCEASSIGNBYADMIN,
        "notifyFor": constantUtil.PROFESSIONAL,
      });
    }
    //-------- Send SMS Alert End --------------------------
  } else if (context.params.actionType === constantUtil.ACTION_REASSIGN) {
    if (bookingData.bookingStatus === constantUtil.ACCEPTED) {
      // Remove From Old Professionals Onging Ride
      const oldProfessional = await this.schema.professionalModel.model
        .findOneAndUpdate(
          {
            "bookingInfo.ongoingBooking": mongoose.Types.ObjectId(
              context.params.bookingId
            ),
          },
          {
            "bookingInfo.ongoingBooking": null,
          },
          { "new": true }
        )
        .lean();
      //--------------
      const notificationObjectOldDriver = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_HOME,
          "timestamp": Date.now(),
          "message": BOOKING_CANCELLED_BY + " " + INFO_ADMIN,
          "details": lzStringEncode({}),
        },
        "registrationTokens": [
          {
            "token": oldProfessional?.deviceInfo[0]?.deviceId,
            "id": oldProfessional?._id?.toString(),
            "deviceType": oldProfessional?.deviceInfo[0]?.deviceType,
            "platform": oldProfessional?.deviceInfo[0]?.platform,
            "socketId": oldProfessional?.deviceInfo[0]?.socketId,
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      await this.broker.emit(
        "socket.sendNotification",
        notificationObjectOldDriver
      );
      /* FCM */
      await this.broker.emit("admin.sendFCM", notificationObjectOldDriver);
      //--------
      // Update To New Professionals Onging Ride
      await this.broker.emit("professional.updateOnGoingBooking", {
        "clientId": context.params?.clientId,
        "professionalId": context.params.professionalId,
        "bookingId": context.params.bookingId,
      });
      // //--------------
      // notificationObject1 = {
      //   "data": {
      //     "type": constantUtil.NOTIFICATIONTYPE,
      //     "userType": constantUtil.PROFESSIONAL,
      //     "action": constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN,
      //     "timestamp": Date.now(),
      //     "message": BOOKING_ASSIGNED_BY_ADMIN,
      //     "details": lzStringEncode(bookingObject(bookingData)),
      //   },
      //   "registrationTokens": [
      //     {
      //       "token": professionalData?.deviceInfo[0]?.deviceId,
      //       "id": professionalData?._id?.toString(),
      //       "deviceType": professionalData?.deviceInfo[0]?.deviceType,
      //       "platform": professionalData?.deviceInfo[0]?.platform,
      //       "socketId": professionalData?.deviceInfo[0]?.socketId,
      //     },
      //   ],
      // };
      //  /* SOCKET PUSH NOTIFICATION */
      // await this.broker.emit("socket.sendNotification", notificationObject1);
      // /* FCM */
      // await this.broker.emit("admin.sendFCM", notificationObject1);
      // //--------
    }
    //--------------
    const notificationObject1 = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action":
          bookingData.bookingStatus === constantUtil.ACCEPTED
            ? constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN
            : constantUtil.ACTION_ASSIGN,
        "timestamp": Date.now(),
        "message": BOOKING_ASSIGNED_BY_ADMIN,
        "details":
          bookingData.bookingStatus === constantUtil.ACCEPTED
            ? lzStringEncode(bookingObject(bookingData))
            : lzStringEncode({}),
      },
      "registrationTokens": [
        {
          "token": professionalData?.deviceInfo[0]?.deviceId,
          "id": professionalData?._id?.toString(),
          "deviceType": professionalData?.deviceInfo[0]?.deviceType,
          "platform": professionalData?.deviceInfo[0]?.platform,
          "socketId": professionalData?.deviceInfo[0]?.socketId,
        },
      ],
    };
    /* SOCKET PUSH NOTIFICATION */
    await this.broker.emit("socket.sendNotification", notificationObject1);
    /* FCM */
    await this.broker.emit("admin.sendFCM", notificationObject1);
    //--------
  } else {
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_ASSIGN,
        "timestamp": Date.now(),
        "message": BOOKING_ASSIGNED_BY_ADMIN,
        "details": lzStringEncode({}),
      },
      "registrationTokens": [
        {
          "token": professionalData?.deviceInfo[0]?.deviceId,
          "id": professionalData?._id?.toString(),
          "deviceType": professionalData?.deviceInfo[0]?.deviceType,
          "platform": professionalData?.deviceInfo[0]?.platform,
          "socketId": professionalData?.deviceInfo[0]?.socketId,
        },
      ],
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  //#endregion Notification

  return {
    "code": 1,
    "data": bookingObject(bookingData),
    "message": BOOKING_REQUEST_ASSIGNED,
  };
};

bookingEvent.updateSecurityInfo = async function (context) {
  const { ERROR_SECURITY_UPDATE, UPDATED } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.professionalDetails?.languageCode
    );
  const updatedBookingData = await this.adapter.model.updateOne(
    {
      "_id": context.params.bookingId,
    },
    {
      "security": context.params.securityId,
      "isSecurityAvail": true,
    }
  );
  if (!updatedBookingData?.nModified) {
    return {
      "code": 0,
      "data": {},
      "message": ERROR_SECURITY_UPDATE,
    };
  } else {
    return { "code": 1, "data": {}, "message": UPDATED };
  }
};

bookingEvent.updateOfficerInfo = async function (context) {
  const { ERROR_SECURITY_UPDATE, UPDATED } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.professionalDetails?.languageCode
    );
  const updatedBookingData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.bookingId),
    },
    {
      "officer": context.params.officer,
    }
  );
  if (!updatedBookingData?.nModified) {
    return {
      "code": 0,
      "data": {},
      "message": ERROR_SECURITY_UPDATE,
    };
  } else {
    return { "code": 1, "data": {}, "message": UPDATED };
  }
};
// bookingEvent.getAllRidesList = async function (context) {
//   const notifyMsg = await notifyMessage.setNotifyLanguage(
//     context.meta?.professionalDetails?.languageCode
//   );
//   const checkTime = Date.now() - parseInt(context.params.time) * 60 * 60 * 1000;
//   const findQuery =
//     context.params.cityId && context.params.cityId !== ""
//       ? {
//           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
//           "category": mongoose.Types.ObjectId(context.params.cityId.toString()),
//         }
//       : {
//           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
//         };
//   const responseJson = await this.adapter.model
//     .find(findQuery)
//     .populate("category", { "_id": 1, "distanceType": 1, "locationName": 1 })
//     .sort({ "bookingDate": 1, "createdAt": -1 })
//     .lean();
//   if (!responseJson)
//     return { "code": 0, "data": {}, "message": INVALID_BOOKING };
//   // // -------- Assign Distance Value Start---------------
//   // const distanceTypeUnitValue =
//   //   (responseJson?.category?.distanceType ?? constantUtil.KM).toUpperCase() ===
//   //   constantUtil.KM
//   //     ? constantUtil.KM_VALUE
//   //     : constantUtil.MILES_VALUE;
//   // responseJson["category"]["distanceTypeUnitValue"] = distanceTypeUnitValue;
//   // // -------- Assign Distance Value End---------------
//   return {
//     "code": 1,
//     "data": responseJson.map((data) => {
//       return {
//         "_id": data._id,
//         "bookingId": data.bookingId,
//         "bookingDate": data.bookingDate,
//         "bookingStatus": data.bookingStatus,
//         "bookingType": data.bookingType,
//         "currencySymbol": data.currencySymbol,
//         "estimation": data.estimation,
//         "payment": data.payment,
//         "cityId": data.category,
//         "cityName": data.category.locationName,
//         "amount": data.invoice.payableAmount,
//         "categoryId": data.vehicle.vehicleCategoryId,
//         "distanceType": data.category.distanceType,
//         "distanceTypeUnitValue":
//           data.category.distanceType === constantUtil.KM
//             ? constantUtil.KM_VALUE
//             : constantUtil.MILES_VALUE,
//       };
//     }),
//     "message": "",
//   };
// };
bookingEvent.getAllRidesList = async function (context) {
  const { INVALID_BOOKING } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  const checkTime = Date.now() - parseInt(context.params.time) * 60 * 60 * 1000;
  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      professionalVehicleHubQuery = {
        // "professional.vehicles.hub": mongoose.Types.ObjectId(
        //   context.params.loginUserId
        // ),
        "hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  // if (context.params.cityId && context.params.cityId !== "") {
  //   query.push({
  //     "$match": {
  //       //"clientId": mongoose.Types.ObjectId(context.params.clientId),
  //       //"bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //       "category": mongoose.Types.ObjectId(context.params.cityId.toString()),
  //       "$or": [
  //         {
  //           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //           "activity.cancelTime": null,
  //           "activity.denyOrExpireTime": null,
  //         },
  //         {
  //           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //           "activity.cancelTime":
  //             context.params.time === 1 || context.params.time === 3
  //               ? {
  //                   "$and": [
  //                     { "$lte": new Date() },
  //                     { "$gte": new Date(parseInt(checkTime)) },
  //                   ],
  //                 }
  //               : { "$gte": new Date(parseInt(checkTime)) },
  //         },
  //       ],
  //     },
  //   });
  // } else {
  //   query.push({
  //     "$match": {
  //       //"clientId": mongoose.Types.ObjectId(context.params.clientId),
  //       // "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //       "$or": [
  //         {
  //           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //           "activity.cancelTime": null,
  //           "activity.denyOrExpireTime": null,
  //         },
  //         {
  //           "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //           "activity.cancelTime":
  //             context.params.time === 1 || context.params.time === 3
  //               ? {
  //                   "$and": [
  //                     { "$1te": new Date() },
  //                     { "$gte": new Date(parseInt(checkTime)) },
  //                   ],
  //                 } //{ "$lte": new Date(parseInt(checkTime)) }
  //               : { "$gte": new Date(parseInt(checkTime)) },
  //         },
  //       ],
  //     },
  //   });
  // }
  if ((context.params.cityId || "") !== "") {
    query.push({
      "$match": {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        //"bookingDate": { "$gte": new Date(parseInt(checkTime)) },
        "category": mongoose.Types.ObjectId(context.params.cityId.toString()),
      },
    });
  }
  // if (context.params.time === 1 || context.params.time === 3) {
  query.push({
    "$match": {
      "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
      "$or": [
        {
          "activity.cancelTime": null,
          "activity.denyOrExpireTime": null,
        },
        {
          "activity.cancelTime": null,
          "activity.denyOrExpireTime": {
            "$gte": new Date(parseInt(checkTime)),
            "$lte": new Date(),
          },
        },
        {
          "activity.cancelTime": {
            "$gte": new Date(parseInt(checkTime)),
            "$lte": new Date(),
          },
          "activity.denyOrExpireTime": null,
        },
        {
          "activity.cancelTime": {
            "$gte": new Date(parseInt(checkTime)),
            "$lte": new Date(),
          },
          "activity.denyOrExpireTime": {
            "$gte": new Date(parseInt(checkTime)),
            "$lte": new Date(),
          },
        },
      ],
    },
  });
  // } else {
  //   query.push({
  //     "$match": {
  //       "bookingDate": { "$gte": new Date(parseInt(checkTime)) },
  //       "$or": [
  //         { "activity.cancelTime": { "$lte": new Date() } },
  //         { "activity.denyOrExpireTime": { "$lte": new Date() } },
  //       ],
  //     },
  //   });
  // }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      // "all": [
      //   { "$sort": { "bookingDate": 1, "createdAt": -1 } },
      //   {
      //     "$match": {
      //       ...professionalVehicleHubQuery,
      //     },
      //   },
      //   { "$count": "all" },
      // ],
      "response": [
        // { "$sort": { "bookingDate": 1, "createdAt": -1 } },
        { "$sort": { "bookingDate": -1 } },
        //// { "$skip": parseInt(skip) },
        //// { "$limit": parseInt(limit) },
        // {
        //   "$lookup": {
        //     "from": "professionals",
        //     "localField": "professional",
        //     "foreignField": "_id",
        //     "as": "professional",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$professional",
        //     // "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$match": {
            ...professionalVehicleHubQuery,
          },
        },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        {
          "$unwind": {
            "path": "$category",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingStatus": "$bookingStatus",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "currentActivityDate": {
              "$switch": {
                "branches": [
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.AWAITING],
                    },
                    "then": "$bookingDate",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.ACCEPTED],
                    },
                    "then": "$activity.acceptTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.ARRIVED],
                    },
                    "then": "$activity.arriveTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.STARTED],
                    },
                    "then": "$activity.pickUpTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.ENDED],
                    },
                    "then": "$activity.dropTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.USERCANCELLED],
                    },
                    "then": "$activity.cancelTime",
                  },
                  {
                    "case": {
                      "$eq": [
                        "$bookingStatus",
                        constantUtil.PROFESSIONALCANCELLED,
                      ],
                    },
                    "then": "$activity.cancelTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.USERDENY],
                    },
                    "then": "$activity.denyOrExpireTime",
                  },
                  {
                    "case": {
                      "$eq": ["$bookingStatus", constantUtil.EXPIRED],
                    },
                    "then": "$activity.denyOrExpireTime",
                  },
                ],
                "default": "$bookingDate",
              },
            },
            "currencySymbol": "$currencySymbol",
            "estimation": "$estimation",
            "bookedEstimation": "$bookedEstimation",
            "payment": "$payment",
            "cityId": "$category._id",
            "cityName": "$category.locationName",
            "amount": "$invoice.payableAmount",
            "categoryId": "$vehicle.vehicleCategoryId",
            "distanceType": "$category.distanceType",
            "distanceTypeUnitValue": {
              "$cond": {
                "if": {
                  "$eq": ["$category.distanceType", constantUtil.KM],
                },
                "then": constantUtil.KM_VALUE,
                "else": constantUtil.MILES_VALUE,
              },
            },
            //------
            "rideStops": "$activity.rideStops",
            "userName": "$user.firstName",
            "userContact": {
              "$concat": ["$user.phone.code", "$user.phone.number"],
            },
            "driverName": "$professional.firstName",
            "driverContact": {
              "$concat": [
                "$professional.phone.code",
                "$professional.phone.number",
              ],
            },
            "services": {
              "serviceCategory": "$serviceCategory",
              "isEnableAssistanceCare": "$invoice.isEnableAssistanceCare",
            },
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = jsonData?.[0]?.response;

  if (responseJson.length === 0) {
    return { "code": 0, "data": {}, "message": INVALID_BOOKING };
  } else {
    return {
      "code": 1,
      "data": responseJson,
      "message": "",
    };
  }
};

bookingEvent.bookingTimeOut = async function (context) {
  const generalSettingsJson = await storageUtil.read(
    constantUtil.GENERALSETTING
  );
  // const yesterday = new Date(Date.now() - 24 * 60 * 60 * 1000);//need to Check
  const yesterday =
    Date.now() -
    generalSettingsJson.data.adminBookingExpiredThreshold * 60 * 60 * 1000; //need to Check
  const timeOutSec =
    (parseInt(generalSettingsJson.data.driverRequestTimeout) *
      parseInt(generalSettingsJson.data.bookingRetryCount) +
      10) *
    1000;
  const ridesJson = await this.adapter.model
    .find({
      "bookingStatus": constantUtil.AWAITING,
      "bookingBy": {
        "$in": [
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.CORPORATEOFFICER,
        ],
      },
      //"bookingDate": { "$lt": Date.now() },
      "bookingDate": { "$lt": yesterday },
      // "bookingType": constantUtil.SCHEDULE,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();

  if (!ridesJson[0]) return;
  each(ridesJson, async (r, upnext) => {
    const currentTime = Date.now();
    const rideTime = new Date(r?.bookingDate?.toString()).getTime();
    const differTime = parseInt(currentTime) - parseInt(rideTime);
    if (timeOutSec < differTime) {
      if (r.guestType === constantUtil.USER && r.user) {
        const userData = r.user;
        // if (userData && userData.length !== 0 && userData[0])
        //   userData = userData[0]
        if (
          r.bookingType === constantUtil.INSTANT &&
          r.payment.option === constantUtil.PAYMENTCARD
        ) {
          const finalAmount = parseFloat(userData.wallet.availableAmount);
          //---------Refund-Ajith Start-----------
          //refund
          if (r.payment.option === constantUtil.PAYMENTCARD) {
            const paymentInitId = r?.paymentInitId?.toString();
            const cardId = r?.payment?.card?.cardId?.toString();
            const refBookingId = r?.bookingId?.toString();
            const bookingId = r?._id;

            await this.broker.emit("transaction.newBookingCancelRefund", {
              "clientId": context.params?.clientId,
              "paymentTokenId": r?.payment?.details?.id,
              "userId": r?.user?._id?.toString(),
              "amount": 0,
              "currencyCode": r.currencyCode,
              "paymentInitId": paymentInitId,
              "cardId": cardId,
              "bookingId": bookingId,
              "refBookingId": refBookingId,
              "transactionType": constantUtil.CANCELCAPTURE,
            });
          }
          //---------Refund-Ajith End-----------
          this.broker.emit("user.updateWalletAmountInBooking", {
            "clientId": context.params?.clientId,
            "userId": r.user._id.toString(),
            "amount": 0.0,
            "finalAmount": parseFloat(finalAmount.toFixed(2)),
          });
        }
        if (
          r.bookingType === constantUtil.SCHEDULE &&
          r.payment.option === constantUtil.PAYMENTWALLET
        ) {
          const updateValue =
            parseFloat(userData.wallet.availableAmount) +
            parseFloat(r.invoice.estimationAmount);
          const scheduleFreezedAmount =
            parseFloat(userData.wallet.scheduleFreezedAmount) -
            parseFloat(r.invoice.estimationAmount);
          this.broker.emit("user.scheduleWalletUpdate", {
            "clientId": context.params?.clientId,
            "userId": r.user._id.toString(),
            "finalAmount": updateValue,
            "amount": scheduleFreezedAmount,
          });
        }
        if (r.guestType === constantUtil.USER && r.user) {
          this.broker.emit("user.updateOnGoingBooking", {
            "clientId": context.params?.clientId,
            "userId": r.user._id.toString(),
            "bookingType": r.bookingType,
          });
          if (r.bookingType === constantUtil.SCHEDULE) {
            this.broker.emit("user.removeScheduleBooking", {
              "clientId": context.params?.clientId,
              "userId": r.user._id.toString(),
              "bookingId": r._id.toString(),
              "bookingType": constantUtil.SCHEDULE,
            });
          }
          // send Notification
          if (
            generalSettingsJson.data.emailConfigurationEnable &&
            userData?.isEmailVerified
          ) {
            this.broker.emit("admin.sendServiceMail", {
              "clientId": context.params?.clientId,
              "templateName": "rideExpiredEmail",
              ...(await mappingUtils.rideExpiredEmailObject(r)),
            });
          }
        }
      }
      await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(r._id.toString()),
        },
        {
          "bookingStatus": constantUtil.EXPIRED,
        }
      );
      upnext();
    }
    upnext();
  });
  return;
};

bookingEvent.updateFCMAccessToken = async function (context) {
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const fcmSettings = await storageUtil.read(constantUtil.FIREBASESETTING);
  // const fcmAccessTokenDetailsFromRedis = await storageUtil.read(
  //   constantUtil.FCMACCESSTOKEN
  // );
  const getAccessToken = () => {
    return new Promise(function (resolve, reject) {
      // const key = require("../placeholders/service-account.json");
      const jwtClient = new google.auth.JWT(
        fcmSettings.data.firebaseNotifyConfig.client_email,
        null,
        fcmSettings.data.firebaseNotifyConfig.private_key,
        ["https://www.googleapis.com/auth/firebase.messaging"], // SCOPES
        null
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
  };

  let fcmAccessTokenDetails = [];
  // await gerenalsetting.map(async () => {
  const accessToken = await getAccessToken();
  const projectId = fcmSettings.data.firebaseNotifyConfig.project_id;
  fcmAccessTokenDetails.push({
    "clientId": context.params.clientId,
    "accessToken": accessToken,
    "projectId": projectId,
  });
  // });
  await storageUtil.write(constantUtil.FCMACCESSTOKEN, fcmAccessTokenDetails);
  return accessToken;
};

bookingEvent.adminBookingTimeOut = async function (context) {
  const generalSettingsJson = await storageUtil.read(
    constantUtil.GENERALSETTING
  );
  const nowDate =
    Date.now() -
    generalSettingsJson.data.adminBookingExpiredThreshold * 60 * 60 * 1000;
  const ridesJson = await this.adapter.model
    .find({
      "bookingStatus": constantUtil.AWAITING,
      "bookingBy": {
        "$in": [constantUtil.ADMIN, constantUtil.COORPERATEOFFICE],
      },
      "bookingDate": { "$lt": nowDate },
    })
    .lean();

  each(ridesJson, async (r, upnext) => {
    if (r.guestType === constantUtil.USER && r.user) {
      if (
        r.bookingType === constantUtil.INSTANT &&
        (r.payment.option === constantUtil.PAYMENTCARD ||
          r.payment.option === constantUtil.PAYMENTWALLET)
      ) {
        let userData = await this.broker.emit("user.getById", {
          "clientId": context.params?.clientId,
          "id": r.user.toString(),
        });

        if (userData && userData.length !== 0 && userData[0])
          userData = userData[0];
        const finalAmount =
          parseFloat(userData.wallet.availableAmount) +
          parseFloat(r.invoice.estimationAmount);

        this.broker.emit("user.updateWalletAmountInBooking", {
          "clientId": context.params?.clientId,
          "userId": r.user.toString(),
          "amount": 0.0,
          "finalAmount": parseFloat(finalAmount.toFixed(2)),
        });
      }
      if (
        r.bookingType === constantUtil.SCHEDULE &&
        (r.payment.option === constantUtil.PAYMENTCARD ||
          r.payment.option === constantUtil.PAYMENTWALLET)
      ) {
        let userData = await this.broker.emit("user.getById", {
          "clientId": context.params?.clientId,
          "id": r.user.toString(),
        });
        if (userData && userData.length !== 0 && userData[0])
          userData = userData[0];

        const updateValue =
          parseFloat(userData.wallet.availableAmount) +
          parseFloat(r.invoice.estimationAmount);
        const scheduleFreezedAmount =
          parseFloat(userData.wallet.scheduleFreezedAmount) -
          parseFloat(r.invoice.estimationAmount);
        this.broker.emit("user.scheduleWalletUpdate", {
          "clientId": context.params?.clientId,
          "userId": r.user.toString(),
          "finalAmount": updateValue,
          "amount": scheduleFreezedAmount,
        });
      }
      if (r.guestType === constantUtil.USER && r.user)
        this.broker.emit("user.updateOnGoingBooking", {
          "clientId": context.params?.clientId,
          "userId": r.user.toString(),
          "bookingType": r.bookingType,
        });
      if (
        r.guestType === constantUtil.USER &&
        r.bookingType === constantUtil.SCHEDULE
      ) {
        this.broker.emit("user.removeScheduleBooking", {
          "clientId": context.params?.clientId,
          "userId": r.user.toString(),
          "bookingId": r._id.toString(),
          "bookingType": constantUtil.SCHEDULE,
        });
      }
    }
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(r._id.toString()),
      },
      {
        "bookingStatus": constantUtil.EXPIRED,
      }
    );
    upnext();
  });
  return;
};

bookingEvent.getUserRideDetails = async function (context) {
  const checkTime = Date.now() - 30 * 60 * 60 * 1000;
  const totalRides = await this.adapter.model.count({
    "user": context.params.userId,
    "bookingStatus": constantUtil.ENDED,
  });
  const totalRidesFor30Days = await this.adapter.model.count({
    "user": context.params.userId,
    "createdAt": { "$gte": checkTime },
  });
  return {
    "totalRides": totalRides,
    "totalRidesFor30Days": totalRidesFor30Days,
  };
};

bookingEvent.getProfessionalRideDetails = async function (context) {
  const days = parseInt(context.params.days || 30);
  const checkTime = Date.now() - days * 24 * 60 * 60 * 1000;
  const totalRides = await this.adapter.model
    .find(
      {
        "professional": context.params.professionalId,
        "bookingStatus": constantUtil.ENDED,
      },
      { "invoice": 1 }
    )
    .lean();
  const ridesCount = totalRides.length;
  let data = 0;
  await totalRides.forEach((rides) => {
    data = data + rides.invoice.payableAmount;
  });
  // const totalRidesFor30Days = await this.adapter.model.count({
  //   "professional": context.params.professionalId,
  //   "createdAt": { "$gte": checkTime },
  // });
  const rideDataForSelectedDays = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "professional": mongoose.Types.ObjectId(
            context.params.professionalId
          ),
          "createdAt": { "$gte": new Date(checkTime) },
        },
      },
      {
        "$project": {
          "_id": "$bookingStatus",
          "userRidesNotReviewRating": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", null],
              },
              "then": 1,
              "else": 0,
            },
          },
          "userRidesReviewRating1": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", 1],
              },
              "then": 1,
              "else": 0,
            },
          },
          "userRidesReviewRating2": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", 2],
              },
              "then": 1,
              "else": 0,
            },
          },
          "userRidesReviewRating3": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", 3],
              },
              "then": 1,
              "else": 0,
            },
          },
          "userRidesReviewRating4": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", 4],
              },
              "then": 1,
              "else": 0,
            },
          },
          "userRidesReviewRating5": {
            "$cond": {
              "if": {
                "$eq": ["$bookingReview.userReview.rating", 5],
              },
              "then": 1,
              "else": 0,
            },
          },
          // "professionalRidesReviewRating": {
          //   "$ifNull": ["$bookingReview.professionalReview.rating", 0],
          // },
          "acceptedRidesForSelectedDays": {
            "$cond": {
              "if": {
                "$in": [
                  "$bookingStatus",
                  [
                    constantUtil.USERDENY,
                    constantUtil.USERCANCELLED,
                    constantUtil.PROFESSIONALCANCELLED,
                    constantUtil.ENDED,
                  ],
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "completedRidesForSelectedDays": {
            "$cond": {
              "if": {
                "$eq": ["$bookingStatus", constantUtil.ENDED],
              },
              "then": 1,
              "else": 0,
            },
          },
          "cancelledRidesForSelectedDays": {
            "$cond": {
              "if": {
                "$in": ["$bookingStatus", [constantUtil.PROFESSIONALCANCELLED]],
              },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": null,
          "totalRidesForSelectedDays": { "$sum": 1 },
          "acceptedRidesForSelectedDays": {
            "$sum": "$acceptedRidesForSelectedDays",
          },
          "completedRidesForSelectedDays": {
            "$sum": "$completedRidesForSelectedDays",
          },
          "cancelledRidesForSelectedDays": {
            "$sum": "$cancelledRidesForSelectedDays",
          },
          // "userRidesReviewRating": { "$sum": "$userRidesReviewRating" },
          "userRidesNotReviewRating": { "$sum": "$userRidesNotReviewRating" },
          "userRidesReviewRating1": { "$sum": "$userRidesReviewRating1" },
          "userRidesReviewRating2": { "$sum": "$userRidesReviewRating2" },
          "userRidesReviewRating3": { "$sum": "$userRidesReviewRating3" },
          "userRidesReviewRating4": { "$sum": "$userRidesReviewRating4" },
          "userRidesReviewRating5": { "$sum": "$userRidesReviewRating5" },
          // "professionalRidesReviewRating": {
          //   "$sum": "$professionalRidesReviewRating",
          // },
        },
      },
    ])
    .allowDiskUse(true);
  return {
    "totalRides": ridesCount,
    "totalEarnings": data,
    "totalRidesForSelectedDays":
      rideDataForSelectedDays?.[0]?.totalRidesForSelectedDays || 0,
    "acceptedRidesForSelectedDays":
      rideDataForSelectedDays?.[0]?.acceptedRidesForSelectedDays || 0,
    "completedRidesForSelectedDays":
      rideDataForSelectedDays?.[0]?.completedRidesForSelectedDays || 0,
    "cancelledRidesForSelectedDays":
      rideDataForSelectedDays?.[0]?.cancelledRidesForSelectedDays || 0,
    //
    "userRidesNotReviewRating":
      rideDataForSelectedDays?.[0]?.userRidesNotReviewRating || 0,
    "userRidesReviewRating1":
      rideDataForSelectedDays?.[0]?.userRidesReviewRating1 || 0,
    "userRidesReviewRating2":
      rideDataForSelectedDays?.[0]?.userRidesReviewRating2 || 0,
    "userRidesReviewRating3":
      rideDataForSelectedDays?.[0]?.userRidesReviewRating3 || 0,
    "userRidesReviewRating4":
      rideDataForSelectedDays?.[0]?.userRidesReviewRating4 || 0,
    "userRidesReviewRating5":
      rideDataForSelectedDays?.[0]?.userRidesReviewRating5 || 0,
    // "userRidesReviewRating": rideDataForSelectedDays?.[0]?.userRidesReviewRating || 0,
    // "professionalRidesReviewRating":
    //   rideDataForSelectedDays?.[0]?.professionalRidesReviewRating || 0,
  };
};

bookingEvent.getUserRatingList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      "bookingStatus": constantUtil.ENDED,
      "user": mongoose.Types.ObjectId(context.params.userId),
    },
  });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        { "$unwind": "$category" },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "Date": "$activity.pickUpTime",
            "review": "$bookingReview.professionalReview",
            "vehicleCategory": "$vehicle.vehicleCategoryId",
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "avatar": "$professional.avatar",
            },
            "cityName": "$category.locationName",
            "categoryName": "$category.categoryName",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  return jsonData[0]?.response || [];
};

bookingEvent.getProfessionalRatingList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      "bookingStatus": constantUtil.ENDED,
      "guestType": constantUtil.USER,
      "professional": mongoose.Types.ObjectId(context.params.professionalId),
    },
  });

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        { "$unwind": "$category" },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "Date": "$activity.pickUpTime",
            "review": "$bookingReview.userReview",
            "vehicleCategory": "$vehicle.vehicleCategoryId",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
              "avatar": "$user.avatar",
            },
            "cityName": "$category.locationName",
            "categoryName": "$category.categoryName",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return jsonData[0]?.response || [];
};

bookingEvent.getAllBillingBookings = async function (context) {
  const responseJson = this.adapter.model
    .find(
      {
        "bookingStatus": constantUtil.ENDED,
        "paymentSection": constantUtil.ALL,
        "createdAt": {
          "$gt": context.params.startDate,
          "$lte": context.params.endDate,
        },
        "professional": mongoose.Types.ObjectId(context.params.professional),
        "bookedBy": { "$ne": constantUtil.COORPERATEOFFICE },
      },
      {
        "_id": 1,
        "invoice": 1,
      }
    )
    .lean();
  if (!responseJson) return [];

  return responseJson;
};

bookingEvent.getAllCoorperateBillingBookings = async function (context) {
  const responseJson = await this.adapter.model
    .find(
      {
        "createdAt": {
          "$gt": new Date(context.params.startDate),
          "$lte": new Date(context.params.endDate),
        },
        "bookingStatus": constantUtil.ENDED,
        "bookingBy": constantUtil.COORPERATEOFFICE,
        "coorperate": mongoose.Types.ObjectId(context.params.office.toString()),
      },
      {
        "_id": 1,
      }
    )
    .lean();
  if (!responseJson) return [];

  return responseJson;
};
bookingEvent.updateBillingId = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": context.params.bookingId,
    },
    {
      "billingId": context.params.billingId,
    }
  );
  return responseJson;
};
bookingEvent.getDriverBillingList = async function (context) {
  const query = [
    {
      "$match": {
        "billingId": context.params.billingId,
        "professionals": context.params.professionalId,
      },
    },
    {
      "$facet": {
        "documentData": [
          {
            "$project": {
              "rideData": {
                "paymentType": "$payment.option",
                "vehicleName": "$vehicle.model",
                "vehicleNo": "$vehicle.plateNumber",
                "professionalEarnings": "$invoice.professionalCommision",
                "currencySymbol": 1,
                "currencyCode": 1,
                "bookingId": 1,
                "time": "$activity.acceptTime",
              },
            },
          },
          {
            "$group": {
              "_id": {
                "$dateToString": {
                  "format": "%Y-%m-%d",
                  "date": "$rideData.time",
                },
              },
              "rideArray": { "$push": "$rideData" },
            },
          },
        ],
      },
    },
  ];
  const responseJson = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);
  if (!responseJson || (responseJson && responseJson.length == 0)) {
    return {
      "code": 503,
      "response": {},
      "message": "",
    };
  } else {
    if (
      responseJson[0].documentData &&
      responseJson[0].documentData.length > 0
    ) {
      return {
        "code": 503,
        "response": responseJson[0].documentData,
        "message": "",
      };
    } else {
      return {
        "code": 503,
        "response": {},
        "message": "",
      };
    }
  }
};
bookingEvent.getGuestRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "guestType": constantUtil.GUESTUSER,
    "bookingBy": constantUtil.ADMIN,
  });
  if (context.params.filter != "undefined") {
    const filterData =
      context.params.filter === constantUtil.AWAITING
        ? { "bookingStatus": constantUtil.AWAITING }
        : context.params.filter === constantUtil.ARRIVED
        ? { "bookingStatus": constantUtil.ARRIVED }
        : context.params.filter === constantUtil.ACCEPTED
        ? { "bookingStatus": constantUtil.ACCEPTED }
        : context.params.filter === constantUtil.USERDENY
        ? { "bookingStatus": constantUtil.USERDENY }
        : context.params.filter === constantUtil.USERCANCELLED
        ? { "bookingStatus": constantUtil.USERCANCELLED }
        : context.params.filter === constantUtil.CANCELLED
        ? { "bookingStatus": constantUtil.CANCELLED }
        : context.params.filter === constantUtil.PROFESSIONALCANCELLED
        ? { "bookingStatus": constantUtil.PROFESSIONALCANCELLED }
        : context.params.filter === constantUtil.EXPIRED
        ? { "bookingStatus": constantUtil.EXPIRED }
        : context.params.filter === constantUtil.STARTED
        ? { "bookingStatus": constantUtil.STARTED }
        : context.params.filter === constantUtil.ENDED
        ? { "bookingStatus": constantUtil.ENDED }
        : {};
    match["$and"].push(filterData);
  }

  query.push({
    "$match": match,
  });

  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": 1,
            "bookingId": 1,
            "bookingType": 1,
            "bookingStatus": 1,
            "bookingDate": 1,
            "bookingFor": 1,
            "bookingBy": 1,
            "invoice": 1,
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};

bookingEvent.getManualMeterRidesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "guestType": constantUtil.PROFESSIONALGUEST,
    "bookingBy": constantUtil.PROFESSIONAL,
  });
  if (context.params.filter != "undefined") {
    const filterData =
      context.params.filter === constantUtil.AWAITING
        ? { "bookingStatus": constantUtil.AWAITING }
        : context.params.filter === constantUtil.ARRIVED
        ? { "bookingStatus": constantUtil.ARRIVED }
        : context.params.filter === constantUtil.ACCEPTED
        ? { "bookingStatus": constantUtil.ACCEPTED }
        : context.params.filter === constantUtil.USERDENY
        ? { "bookingStatus": constantUtil.USERDENY }
        : context.params.filter === constantUtil.USERCANCELLED
        ? { "bookingStatus": constantUtil.USERCANCELLED }
        : context.params.filter === constantUtil.CANCELLED
        ? { "bookingStatus": constantUtil.CANCELLED }
        : context.params.filter === constantUtil.PROFESSIONALCANCELLED
        ? { "bookingStatus": constantUtil.PROFESSIONALCANCELLED }
        : context.params.filter === constantUtil.EXPIRED
        ? { "bookingStatus": constantUtil.EXPIRED }
        : context.params.filter === constantUtil.STARTED
        ? { "bookingStatus": constantUtil.STARTED }
        : context.params.filter === constantUtil.ENDED
        ? { "bookingStatus": constantUtil.ENDED }
        : {};
    match["$and"].push(filterData);
  }

  query.push({
    "$match": match,
  });

  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  // FOR BOOKING BY FILTER
  if (context.params.bookingByFilter && context.params.bookingByFilter != "") {
    query.push({
      "$match": {
        "bookingBy": context.params.bookingByFilter,
      },
    });
  }
  // FOR BOOKING BY FILTER
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": 1,
            "bookingId": 1,
            "bookingType": 1,
            "bookingStatus": 1,
            "bookingDate": 1,
            "bookingFor": 1,
            "bookingBy": 1,
            "invoice": 1,
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
bookingEvent.getCoorperateRidesList = async function (context) {
  let query = [];
  if (!!context.params.filter === true) {
    query.push({ "$match": { "bookingStatus": context.params.filter } });
  }
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    context.params.fromDate = new Date(context.params.fromDate);
    context.params.toDate = new Date(
      new Date(context.params.toDate).setDate(
        new Date(context.params.toDate).getDate() + 1
      )
    );
    query.push({
      "$match": {
        "bookingDate": {
          "$gte": context.params.fromDate,
          "$lt": context.params.toDate,
        },
      },
    });
  }
  if (!!context.params?.bookingByFilter === true) {
    query.push({ "$match": { "bookingBy": context.params.bookingByFilter } });
  }
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push(
    {
      "$match": {
        // "$and": [{ "bookingBy": constantUtil.COORPERATEOFFICE }],
        "bookingBy": constantUtil.COORPERATEOFFICE,
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
    },
    {
      "$facet": {
        "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
        "response": [
          { "$sort": { "_id": -1 } },
          {
            "$lookup": {
              "from": "professionals",
              "localField": "professional",
              "foreignField": "_id",
              "as": "professional",
            },
          },
          {
            "$unwind": {
              "path": "$professional",
              "preserveNullAndEmptyArrays": true,
            },
          },
          {
            "$lookup": {
              "from": "admins",
              "localField": "coorperate",
              "foreignField": "_id",
              "as": "coorperate",
            },
          },
          {
            "$unwind": {
              "path": "$coorperate",
              "preserveNullAndEmptyArrays": true,
            },
          },
          searchQuery,
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
          {
            "$project": {
              "_id": 1,
              "bookingId": 1,
              "bookingType": 1,
              "bookingStatus": 1,
              "bookingDate": 1,
              "bookingFor": 1,
              "bookingBy": 1,
              "invoice": 1,
              "origin": 1,
              "destination": 1,
              "estimation": 1,
              "professional": {
                "_id": "$professional._id",
                "firstName": "$professional.firstName",
                "lastName": "$professional.lastName",
                "email": "$professional.email",
                "phone": "$professional.phone",
              },
              "coorperate": {
                "_id": "$coorperate._id",
                "officeName": "$coorperate.data.officeName",
                "email": "$coorperate.data.email",
                "phone": "$coorperate.data.phone",
              },
            },
          },
        ],
      },
    }
  );
  const [jsonData] = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);
  return {
    "code": 1,
    "data": {
      "count": jsonData?.all?.[0]?.all || 0,
      "response": jsonData?.response || [],
    },
    "message": "",
  };
};

bookingEvent.getNotifyRidesList = async function (context) {
  const checkTime = new Date(Date.now() + 25 * 60 * 1000);
  const today = new Date(new Date().setHours(new Date().getHours() - 1));
  const responseJson = await this.adapter.model
    .find({
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      // "guestType": constantUtil.USER,
      "bookingStatus": constantUtil.AWAITING,
      // "bookingType": constantUtil.SCHEDULE, // All bookingType Rides
      // "bookingDate": { "$gte": new Date(), "$lte": checkTime },
      "bookingDate": { "$gte": today, "$lte": checkTime },
    })
    .sort({ "bookingDate": 1, "createdAt": -1 })
    .lean();
  return responseJson;
};

bookingEvent.updateUpcommingScheduledRideInRedis = async function () {
  const date = new Date(Date.now() - 60 * 60 * 1000);
  const bookingDataList = await this.adapter.model
    .find(
      {
        "bookingStatus": constantUtil.AWAITING,
        "bookingType": constantUtil.SCHEDULE,
        "bookingDate": { "$gte": date },
      },
      {
        "_id": 1,
        "bookingId": 1,
        "bookingDate": 1,
      }
    )
    .lean();
  storageUtil.write(constantUtil.SCHEDULE, bookingDataList);
};

bookingEvent.professionalCancelRideList = async function (context) {
  const responseJson = await this.adapter.model
    .find({
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "bookingBy": constantUtil.USER,
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.INSTANT,
      "isCancelToNearbyProfessionals": true,
    })
    .lean();
  return responseJson;
};
bookingEvent.generateInvoiceMail = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const ids = context.params.rideLists?.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.find(
    {
      "_id": { "$in": ids },
    },
    { "invoice": 1 }
  );
  let finalData = 0;
  await responseJson?.map(async (data) => {
    finalData += data.invoice.payableAmount;
  });
  finalData = Math.ceil(finalData);
  const amountInWords = converter.toWords(finalData);
  const mailData = {};
  mailData.template = "Invoice";
  mailData.from = generalSettings.data.emailAddress;
  mailData.to = context.params.office.data.email;
  mailData.html = [];
  mailData.html.push({
    "name": "siteTitle",
    "value": generalSettings.data.siteTitle,
  });
  mailData.html.push({
    "name": "newDate",
    "value": new Date().toLocaleDateString("en-GB") || "",
  });
  mailData.html.push({
    "name": "refNum",
    "value": context.params.fileName.slice(3, 2) + Date.now() || "",
  });
  mailData.html.push({
    "name": "coorperateName",
    "value": `${context.params.office.data.officeName}` || "",
  });
  mailData.html.push({
    "name": "coorperateAddress",
    "value": `${context.params.office.data.showAddress}` || "",
  });
  mailData.html.push({
    "name": "currentMonth",
    "value": new Date().toLocaleString("default", { "month": "long" }) || "",
  });
  mailData.html.push({
    "name": "amount",
    "value":
      `${amountInWords}(${finalData} ${generalSettings.data.currencyCode})` ||
      "",
  });
  let mailContent = await this.broker.emit("admin.mailContentLink", mailData);
  mailContent = mailContent[0];
  // if (mailContent && mailContent !== null && mailContent !== undefined) {
  //   await this.broker.emit('admin.sendMail', mailContent)
  // }
  let finalImageData;
  const options = { "format": "Letter" };
  pdf.create(mailContent.html, options).toBuffer(async (err, buffer) => {
    finalImageData = await this.broker.emit("admin.uploadPdfInSpaces", {
      "files": buffer,
      "fileName": context.params.fileName,
    });
    finalImageData = finalImageData[0];
    await this.broker.emit("billings.updatePdfImage", {
      "billingId": context.params.billingId.toString(),
      "coorperateId": context.params.office._id.toString(),
      "invoiceUrl": finalImageData,
    });
    return null;
  });
};
bookingEvent.checkBookingCountforCoupon = async function (context) {
  //----------------Old -------------------
  // const responseJson = await this.adapter.model.count({
  //   "bookingBy": constantUtil.USER,
  //   "user": mongoose.Types.ObjectId(context.params.userId.toString()),
  //   "couponId": { "$ne": null },
  //   "$or": [
  //     { "bookingStatus": constantUtil.ENDED }, //COUPON COUNT TAKE BY ENDED
  //     {
  //       //OR
  //       "$and": [
  //         { "professional": { "$exists": true } }, //USERCANCELLED RIDE BASED ON PROFESSIONAL IS ACCEPT THE RIDE
  //         { "bookingStatus": constantUtil.USERCANCELLED },
  //       ],
  //     },
  //   ],
  // });
  //----------------New -------------------
  const responseJson = await this.adapter.model
    .aggregate([
      {
        "$match": {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "user": mongoose.Types.ObjectId(context.params.userId.toString()),
          "bookingBy": constantUtil.USER,
          "couponId": { "$ne": null },
          "$or": [
            { "bookingStatus": constantUtil.ENDED }, //COUPON COUNT TAKE BY ENDED
            {
              //OR
              "$and": [
                { "professional": { "$exists": true } }, //USERCANCELLED RIDE BASED ON PROFESSIONAL IS ACCEPT THE RIDE
                { "bookingStatus": constantUtil.USERCANCELLED },
              ],
            },
          ],
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "couponId",
          "foreignField": "_id",
          "as": "couponList",
        },
      },
      { "$unwind": "$couponList" },
      {
        "$group": {
          "_id": "$couponList.data.consumersType",
          "couponUsedCount": { "$sum": 1 },
        },
      },
      {
        "$project": {
          "_id": 0,
          "consumersType": "$_id",
          "couponUsedCount": "$couponUsedCount",
        },
      },
    ])
    .allowDiskUse(true);

  return responseJson;
};
bookingEvent.nextProfessionalBuffer = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    SOMETHING_WENT_WRONG,
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_RIDE_CANCEL,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  if (!generalSettings) {
    throw new MoleculerError(SOMETHING_WENT_WRONG);
  }
  const bufferFun = async () => {
    let bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("admin", { "password": 0 })
      .lean();
    // bookingData = bookingData.toJSON();
    const paymentInitId = bookingData?.paymentInitId?.toString();
    // it ends means ride end so return
    if (bookingData.bookingStatus !== constantUtil.AWAITING) {
      return;
    }
    if (bookingData.bookingStatus === constantUtil.AWAITING) {
      if (bookingData.bookingType === constantUtil.INSTANT) {
        const updatedBookingData = await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
          },
          {
            "bookingStatus": constantUtil.PROFESSIONALCANCELLED,
            "bookingDate": context.params.bookingDate,
            "cancellationReason": context.params.cancellationReason,
            "isCancelToNearbyProfessionals": true,
            "professional": mongoose.Types.ObjectId(
              context.params.professionalId.toString()
            ),

            "invoice.professionalCancellationStatus": true,
            "invoice.professionalCancellationMinute":
              helperUtils.getMinutesDiff(
                context.params.cancelledTime,
                bookingData.activity.acceptTime // IN CANCEL BOOKIKNG WE NOT UPDATE ACCEPT TIME SO WE CAN USE HERE
              ),
            "invoice.professionalCancellationAmount":
              context.params.cancellationAmount,
            "invoice.payableAmount": 0,
            "activity.cancelledBy": {
              "id": context.params.professionalId,
              "userType": constantUtil.PROFESSIONAL,
            },
            "activity.cancelTime": new Date(context.params.cancelledTime),
            "regionalData.cancelTime": helperUtil.toRegionalUTC(
              new Date(context.params.cancelledTime)
            ),
            "regionalData.bookingDate": helperUtil.toRegionalUTC(
              new Date(context.params.bookingDate)
            ),
          }
        );

        if (!updatedBookingData.nModified)
          throw new MoleculerError(
            "SOMETHING WRONG IN NEXT PROFESSIONL NOT ACCEPTING RIDE UPDATION"
          );
        // data intialation

        // vehicle category from redis
        let vehicleCategoryData = await storageUtil.read(
          constantUtil.VEHICLECATEGORY
        );
        if (!vehicleCategoryData)
          throw new MoleculerError(
            VEHICLE_CATEGORY_NOT_FOUND + " " + SOMETHING_WENT_WRONG,
            +"/REDIS"
          );
        vehicleCategoryData =
          vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];
        if (!vehicleCategoryData)
          throw new MoleculerError(
            VEHICLE_CATEGORY_NOT_FOUND + " " + SOMETHING_WENT_WRONG
          );
        //  service Category
        if (!bookingData.category._id) {
          throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
        }
        vehicleCategoryData = {
          ...vehicleCategoryData,
          ...bookingData.category.categoryData.vehicles.find(
            (vehicle) =>
              vehicle.categoryId.toString() ===
              bookingData.vehicle.vehicleCategoryId.toString()
          ),
        };

        if (bookingData.guestType === constantUtil.USER && bookingData.user) {
          //  update ongoing ride in booked user
          if (
            bookingData.user.bookingInfo.ongoingBooking?.toString() ===
            context.params.bookingId.toString()
          ) {
            // udate ongoing booking
            this.broker.emit("user.removeOngoingBooking", {
              "clientId": context.params?.clientId,
              "userId": bookingData.user._id.toString(),
              "bookingId": context.params.bookingId.toString(),
            });
            // //------------ Refund-Ajith Start -----------------------
            // // refund
            if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
              this.broker.emit("transaction.newBookingCancelRefund", {
                "clientId": context.params?.clientId,
                "paymentTokenId": bookingData?.payment?.details?.id,
                "userId": bookingData?.user?._id?.toString(),
                "amount": 0,
                "currencyCode": bookingData.currencyCode,
                "paymentInitId": paymentInitId,
                "cardId": bookingData?.payment?.card?.cardId?.toString(),
                "bookingId": bookingData._id.toString(),
                "refBookingId": bookingData?.bookingId?.toString(),
                "transactionType": constantUtil.CANCELCAPTURE,
              });
              //   this.broker.emit('transaction.newBookingCancelRefund', {
              //     'paymentTokenId': bookingData.payment.details.id,
              //     'userId': bookingData.user._id.toString(),
              //     'amount': bookingData.invoice.estimationAmount.toFixed(),
              //     'currencyCode': bookingData.currencyCode,
              //     'paymentInitId': paymentInitId,
              //   })
              //   // update wallet amount here beacuse if profesional accept schedule ride  is ongoing so amount is freezedAmount
              //   this.broker.emit('user.updateWalletAmount', {
              //     'userId': bookingData.user._id.toString(),
              //     'availableAmount': bookingData.user.wallet.availableAmount,
              //     'freezedAmount':
              //       bookingData.user.wallet.freezedAmount -
              //       bookingData.invoice.estimationAmount,
              //     'scheduleFreezedAmount':
              //       bookingData.user.wallet.scheduleFreezedAmount,
              //   })
            }
            // //-----------Refund-Ajith End -------------------------
            if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
              this.broker.emit("user.updateWalletAmount", {
                "clientId": context.params?.clientId,
                "userId": bookingData.user._id.toString(),
                "availableAmount":
                  bookingData.user.wallet.availableAmount +
                  bookingData.invoice.estimationAmount,
                "freezedAmount":
                  bookingData.user.wallet.freezedAmount -
                  bookingData.invoice.estimationAmount,
                "scheduleFreezedAmount":
                  bookingData.user.wallet.scheduleFreezedAmount,
              });
            }
          }

          // SEND NOTIFICATION
          //  GET DATA ONCE AGAIN FOR DATA SO GER UPDATED DATA
          bookingData = await this.adapter.model
            .findOne({
              "_id": mongoose.Types.ObjectId(
                context.params.bookingId.toString()
              ),
            })
            .populate("user", {
              "password": 0,
              "bankDetails": 0,
              "cards": 0,
              "trustedContacts": 0,
            })
            .populate("professional", {
              "password": 0,
              "bankDetails": 0,
              "cards": 0,
              "trustedContacts": 0,
            })
            .populate("category")
            .populate("security")
            .populate("officer", { "password": 0, "preferences": 0 })
            .lean();
          // bookingData = bookingData.toJSON();
          // some another mapping data for booking object
          bookingData.vehicle.vehicleCategoryName =
            vehicleCategoryData.vehicleCategory;
          bookingData.vehicle.vehicleCategoryImage =
            vehicleCategoryData.categoryImage;
          bookingData.vehicle.vehicleCategoryMapImage =
            vehicleCategoryData.categoryMapImage;
          bookingData.retryTime = generalSettings.data.driverRequestTimeout;
          bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
          // bookingData.distanceUnit = bookingData.category.distanceType;
          bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
          bookingData["imageURLPath"] =
            generalSettings.data.spaces.spacesBaseUrl +
            "/" +
            generalSettings.data.spaces.spacesObjectName;

          //send to user
          if (bookingData.guestType === constantUtil.USER && bookingData.user) {
            const notificationObject = {
              "clientId": context.params.clientId,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.USER,
                "action": constantUtil.ACTION_CANCELBOOKING,
                "timestamp": Date.now(),
                "message": PROFESSIONAL_RIDE_CANCEL,
                "details": lzStringEncode(bookingObject(bookingData)),
              },
              "registrationTokens": [
                {
                  "token": bookingData.user?.deviceInfo[0]?.deviceId,
                  "id": bookingData.user?._id?.toString(),
                  "deviceType": bookingData.user?.deviceInfo[0]?.deviceType,
                  "platform": bookingData.user?.deviceInfo[0]?.platform,
                  "socketId": bookingData.user?.deviceInfo[0]?.socketId,
                },
              ],
            };

            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
          }
        }
      }
      if (bookingData.bookingType === constantUtil.SCHEDULE) {
        const updatedBookingData = await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
          },
          {
            "isCancelToNearbyProfessionals": true,
          }
        );

        if (!updatedBookingData.nModified)
          throw new MoleculerError(
            SOMETHING_WENT_WRONG
            //"SOMETHING WENT WRONG IN NEXT PROFESSIONAL SCHEDULE RIDE NOT ACCEPT NOTIFYING REDIRECTION TO ADMIN BOOKING UPDATION ERROR"
          );
        // notify to admin
        const notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": constantUtil.ACTION_CANCELBOOKING,
            "timestamp": Date.now(),
            "message": PROFESSIONAL_RIDE_CANCEL,
            "details": bookingObject(bookingData),
          },
          "registrationTokens": [
            {
              "token": "",
              "id": context.params.professionalId,
              "deviceType": "",
              "platform": "",
              "socketId": "",
            },
          ],
        };
        this.broker.emit(
          "socket.sendSecurityNotificationToAdmin",
          notificationObject
        );
        // after if admin not assiging the bookingneedtoCancel
        this.broker.emit("booking.adminCannotAssignProfessionalCanceledRide", {
          "cancellationReason": context.params.cancellationReason,
          "professsionalId": context.meta.professionalId.toString(),
          "cancelledTime": context.params.cancellationTime,
          "bookingId": bookingData._id.toString(),
          "professionalCancellationAmount": context.params.cancellationAmount,
          "isCancelToNearbyProfessionals": true,
        });
      }
      return;
    }
    return;
  };
  setTimeout(bufferFun, generalSettings.data.driverRequestTimeout); //driverRequestTimeout is second so dont need to convert in second
};
bookingEvent.adminCannotAssignProfessionalCanceledRide = async function (
  context
) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    SOMETHING_WENT_WRONG,
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_RIDE_CANCEL,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );

  const bufferFun = async () => {
    let bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();
    if (bookingData.bookingStatus !== constantUtil.AWAITING) {
      return;
    }
    const paymentInitId = bookingData?.paymentInitId?.toString();
    const cardId = bookingData?.payment?.card?.cardId?.toString();
    const refBookingId = bookingData?.bookingId?.toString();
    const bookingId = bookingData?._id;
    if (bookingData.bookingStatus === constantUtil.AWAITING) {
      const updatedBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
        },
        {
          "bookingStatus": constantUtil.PROFESSIONALCANCELLED,
          "cancellationReason": context.params.cancellationReason,
          "isCancelToNearbyProfessionals": context.params
            .isCancelToNearbyProfessionals
            ? true
            : false,
          "professional": mongoose.Types.ObjectId(
            context.params.professionalId?.toString()
          ),
          "activity.cancelldBy": {
            "id": context.params?.professionalId,
            "userType": constantUtil.PROFESSIONAL,
          },
          "activity.cancelTime": new Date(context.params.cancelledTime),
          "invoice.professionalCancellationStatus": true,
          "invoice.professionalCancellationMinute": helperUtils.getMinutesDiff(
            context.params.cancelledTime,
            bookingData.activity.acceptTime
          ),
          "invoice.professionalCancellationAmount":
            context.params.professionalCancellationAmount,
          "invoice.payableAmount": 0,
          "regionalData.cancelTime": helperUtil.toRegionalUTC(
            new Date(context.params.cancelledTime)
          ),
        }
      );
      if (!updatedBookingData.nModified) {
        throw new MoleculerError(
          SOMETHING_WENT_WRONG
          // "SOMETHING WRONG IN ADMIN FORGOT TO ASSIGN THE SCHEDULE RIDE REPLIED BOOKING DETAILS"
        );
      }
      // vehicle category from redis
      let vehicleCategoryData = await storageUtil.read(
        constantUtil.VEHICLECATEGORY
      );
      if (!vehicleCategoryData)
        throw new MoleculerError(
          VEHICLE_CATEGORY_NOT_FOUND + " " + SOMETHING_WENT_WRONG + "/REDIS"
        );
      vehicleCategoryData =
        vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];
      if (!vehicleCategoryData)
        throw new MoleculerError(
          VEHICLE_CATEGORY_NOT_FOUND + " " + SOMETHING_WENT_WRONG
        );
      //  service Category
      if (!bookingData.category._id) {
        throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
      }
      vehicleCategoryData = {
        ...vehicleCategoryData,
        ...bookingData.category.categoryData.vehicles.find(
          (vehicle) =>
            vehicle.categoryId.toString() ===
            bookingData.vehicle.vehicleCategoryId.toString()
        ),
      };
      if (bookingData.guestType === constantUtil.USER && bookingData.user) {
        //  update ongoing ride in booked user
        if (
          bookingData.user.bookingInfo.ongoingBooking?.toString() ===
          context.params.bookingId.toString()
        ) {
          // udate ongoing booking
          this.broker.emit("user.removeOngoingBooking", {
            "clientId": context.params?.clientId,
            "userId": bookingData.user._id.toString(),
            "bookingId": context.params.bookingId.toString(),
          });
          //----------Refund-Ajith Start------
          // refund
          if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
            await this.broker.emit("transaction.newBookingCancelRefund", {
              "clientId": context.params?.clientId,
              "paymentTokenId": bookingData?.payment?.details?.id,
              "userId": bookingData?.user?._id?.toString(),
              "amount": 0,
              "currencyCode": bookingData.currencyCode,
              "paymentInitId": paymentInitId,
              "cardId": cardId,
              "bookingId": bookingId,
              "refBookingId": refBookingId,
              "transactionType": constantUtil.CANCELCAPTURE,
            });
            // // update wallet amount here beacuse if profesional accept schedule ride  is ongoing so amount is freezedAmount
            // this.broker.emit('user.updateWalletAmount', {
            //   'userId': bookingData.user._id.toString(),
            //   'availableAmount': bookingData.user.wallet.availableAmount,
            //   'freezedAmount':
            //     bookingData.user.wallet.freezedAmount -
            //     bookingData.invoice.estimationAmount,
            //   'scheduleFreezedAmount':
            //     bookingData.user.wallet.scheduleFreezedAmount,
            // })
          }
          //--------Refund-Ajith End
          if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
            this.broker.emit("user.updateWalletAmount", {
              "clientId": context.params?.clientId,
              "userId": bookingData.user._id.toString(),
              "availableAmount":
                bookingData.user.wallet.availableAmount +
                bookingData.invoice.estimationAmount,
              "freezedAmount":
                bookingData.user.wallet.freezedAmount -
                bookingData.invoice.estimationAmount,
              "scheduleFreezedAmount":
                bookingData.user.wallet.scheduleFreezedAmount,
            });
          }
        }
        // if some times ride not assign into ongoing ride
        if (
          bookingData.bookingType === constantUtil.SCHEDULE &&
          bookingData.user.bookingInfo.scheduledBooking.some(
            (element) =>
              element?.bookingId?.toString() === context.params.bookingId
          )
        ) {
          // REMOVE SCHEDULE RIDE
          this.broker.emit("user.removeScheduleBooking", {
            "clientId": context.params?.clientId,
            "userId": bookingData.user._id.toString(),
            "bookingType": bookingData.bookingType,
            "bookingId": context.params.bookingId,
          });
          //----------Refund-Ajith Start------
          //Refund
          if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
            await this.broker.emit("transaction.newBookingCancelRefund", {
              "clientId": context.params?.clientId,
              "paymentTokenId": bookingData?.payment?.details?.id,
              "userId": bookingData?.user?._id?.toString(),
              "amount": 0,
              "currencyCode": bookingData.currencyCode,
              "paymentInitId": paymentInitId,
              "cardId": cardId,
              "bookingId": bookingId,
              "refBookingId": refBookingId,
              "transactionType": constantUtil.CANCELCAPTURE,
            });
            // // update wallet amount here beacuse if profesional NOT accept schedule ride  is SCHEDULE so amount is schedulefreezedAmount
            // this.broker.emit('user.updateWalletAmount', {
            //   'userId': bookingData.user._id.toString(),
            //   'availableAmount': bookingData.user.wallet.availableAmount,
            //   'freezedAmount': bookingData.user.wallet.freezedAmount,
            //   'scheduleFreezedAmount':
            //     bookingData.user.wallet.scheduleFreezedAmount -
            //     bookingData.invoice.estimationAmount,
            // })
          }
          //--------Refund-Ajith End ---------------
          if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
            this.broker.emit("user.updateWalletAmount", {
              "clientId": context.params?.clientId,
              "userId": bookingData.user._id.toString(),
              "availableAmount":
                bookingData.user.wallet.availableAmount +
                bookingData.invoice.estimationAmount,
              "freezedAmount": bookingData.user.wallet.freezedAmount,
              "scheduleFreezedAmount":
                bookingData.user.wallet.scheduleFreezedAmount -
                bookingData.invoice.estimationAmount,
            });
          }
        }

        // SEND NOTIFICATION
        //  GET DATA ONCE AGAIN FOR DATA SO GER UPDATED DATA
        bookingData = await this.adapter
          .findById(
            mongoose.Types.ObjectId(context.params.bookingId.toString())
          )
          .populate("user", {
            "password": 0,
            "bankDetails": 0,
            "cards": 0,
            "trustedContacts": 0,
          })
          .populate("professional", {
            "password": 0,
            "bankDetails": 0,
            "cards": 0,
            "trustedContacts": 0,
          })
          .populate("category")
          .populate("security")
          .populate("officer", { "password": 0, "preferences": 0 })
          .lean();
        // bookingData = bookingData.toJSON();
        // some another mapping data for booking object
        bookingData.vehicle.vehicleCategoryName =
          vehicleCategoryData.vehicleCategory;
        bookingData.vehicle.vehicleCategoryImage =
          vehicleCategoryData.categoryImage;
        bookingData.vehicle.vehicleCategoryMapImage =
          vehicleCategoryData.categoryMapImage;
        bookingData.retryTime = generalSettings.data.driverRequestTimeout;
        bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
        // bookingData.distanceUnit = bookingData.category.distanceType;
        bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
        bookingData["imageURLPath"] =
          generalSettings.data.spaces.spacesBaseUrl +
          "/" +
          generalSettings.data.spaces.spacesObjectName;
        //send to user

        const notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": constantUtil.ACTION_CANCELBOOKING,
            "timestamp": Date.now(),
            "message": PROFESSIONAL_RIDE_CANCEL,
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "token": bookingData.user?.deviceInfo[0]?.deviceId,
              "id": bookingData.user?._id?.toString(),
              "deviceType": bookingData.user?.deviceInfo[0]?.deviceType,
              "platform": bookingData.user?.deviceInfo[0]?.platform,
              "socketId": bookingData.user?.deviceInfo[0]?.socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
    }
  };
  setTimeout(
    bufferFun,
    (generalSettings.data.adminAssignThreshold || 5) * 60 * 100
  );
};
bookingEvent.getBookingDetailsById = async function (context) {
  const userType = context.params?.userType?.toUpperCase();
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    INVALID_BOOKING,
    SOMETHING_WENT_WRONG,
    VEHICLE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
  } = await notifyMessage.setNotifyLanguage(languageCode);

  const bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  } else {
    // return bookingData.toJSON();
    return bookingData;
  }
};
// bookingEvent.trackBookingDetailsByIdFromWebBookingApp = async function (
//   context
// ) {
//   let responseData = {},
//     bookingData = {};
//   try {
//     if (context.params.bookingStatus !== "ENDED") {
//       bookingData = await this.adapter.model
//         .findOne({
//           "_id": context.params.bookingId,
//         })
//         .lean();
//     }
//     if (!bookingData) throw new MoleculerError("ERROR IN BOOKING DETAILS", 500);
//     responseData = bookingObject(bookingData);
//   } catch (e) {
//     responseData = {};
//   }
//   return responseData;
// };
bookingEvent.sendBookingRequestToProfessionals = async function (context) {
  const clientId = context.params.clientId;
  const {
    INVALID_BOOKING,
    SOMETHING_WENT_WRONG,
    VEHICLE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    PROFESSIONAL_NEW_RIDE,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  // const bookingData = await this.adapter.model
  //   .findOne({
  //     "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //   })
  //   .populate("category")
  //   .lean();
  const bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId?.toString(),
  }); // Get booking Details
  if (!bookingData) {
    throw new MoleculerError(SOMETHING_WENT_WRONG);
  }
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData)
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);

  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

  bookingData.category.categoryData.vehicles?.map((vehicle) => {
    if (
      vehicle.categoryId.toString() ===
      bookingData.vehicle.vehicleCategoryId.toString()
    ) {
      vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
      return;
    }
  });
  const radius =
    parseInt(generalSettings.data.requestDistance) +
    parseInt(generalSettings.data.retryRequestDistance);
  const searchData = {
    "clientId": clientId,
    "lat": bookingData.origin.lat,
    "lng": bookingData.origin.lng,
    "droplat": bookingData.destination.lat,
    "droplng": bookingData.destination.lng,
    "vehicleCategoryId": bookingData.vehicle.vehicleCategoryId.toString(),
    "professionalOnlineStatus": true,
    "actionType": constantUtil.BOOKING,
    "rideType": context.params.rideType,
    "radius": context.params?.radius || radius,
    // "maxRadius": context.params.maxRadius,
    "checkOngoingBooking": false,
    "checkLastPriority": true,
    "forTailRide": generalSettings.data.isTailRideNeeded,
    "isSubCategoryAvailable": vehicleCategoryData.isSubCategoryAvailable,
    "isForceAppliedToProfessional":
      vehicleCategoryData.isForceAppliedToProfessional,
    "isGenderAvailable": bookingData.isGenderAvailable,
    "childseatAvailable": bookingData.childseatAvailable,
    "handicapAvailable": bookingData.handicapAvailable,
    "isRestrictMaleBookingToFemaleProfessional": false,
    "serviceAreaId": bookingData.category,
    "isShareRide": bookingData.isShareRide,
    "paymentOption": bookingData?.payment.option || constantUtil.CASH,
    "languageCode": bookingData?.languageCode,
    "isPetAllowed": bookingData?.isPetAllowed,
    "isEnableLuggage": bookingData?.isEnableLuggage,
  };

  let professionalData = await this.broker.emit(
    "professional.getProfessionalByLocation",
    searchData
  );
  professionalData = professionalData[0] && professionalData[0];
  if (!professionalData[0]) {
    return {
      "code": 404,
      "message": PROFESSIONAL_NOT_FOUND,
      "data": [],
    };
  }
  professionalData =
    await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
      professionalData,
      bookingData,
      null
    );
  if (!professionalData || !professionalData[0]) {
    return {
      "code": 404,
      "message": PROFESSIONAL_NOT_FOUND,
      "data": [],
    };
  }
  if (
    generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
    generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST
  ) {
    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_BOOKINGREQUEST,
        "timestamp": Date.now(),
        "message": PROFESSIONAL_NEW_RIDE,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": professionalData?.map((professional) => {
        if (
          // parseFloat(professional.wallet.availableAmount) <
          // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
          bookingData.payment.option === constantUtil.WALLET ||
          bookingData.payment.option === constantUtil.PAYMENTCARD ||
          // bookingData.payment.option !== constantUtil.CASH &&
          // bookingData.payment.option !== constantUtil.CREDIT) ||
          parseFloat(professional.wallet.availableAmount) >=
            parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
        ) {
          return {
            "id": professional?._id?.toString(),
            "token": professional?.deviceInfo[0]?.deviceId,
            "deviceType": professional?.deviceInfo[0]?.deviceType,
            "platform": professional?.deviceInfo[0]?.platform,
            "socketId": professional?.deviceInfo[0]?.socketId,
            "duration": professional?.duration,
            "distance": professional?.distance,
          };
        }
      }),
    };
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
};

//#region third party Booking
// third party Booking
bookingEvent.newRideBookingForThirdParty = async function (context) {
  const clientId = context.params.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    INFO_NOT_FOUND,
    PROFESSIONAL_NEW_RIDE,
    SERVICE_CATEGORY_NOT_FOUND,
    VEHICLE_CATEGORY_NOT_FOUND,
    INFO_INVALID_DETAILS,
    PROFESSIONAL_NOT_FOUND,
    INFO_SERVICE_NOT_PROVIDE,
    PROFESSIONAL_NOTIFICATION,
  } = notifyMessage.setNotifyLanguage(context.meta?.userDetails?.languageCode);

  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "clientId": context.params?.clientId,
    "id": context.params.serviceCategoryId,
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
  if (!serviceCategoryData) {
    return {
      "code": 400,
      "message": "",
      "data": [
        SERVICE_CATEGORY_NOT_FOUND,
        400,
        INFO_NOT_FOUND,
        { "serviceCategoryId": context.params.serviceCategoryId },
      ],
    };
  }

  // VEHICLE CATEGORY FINDING
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

  if (!vehicleCategoryData) {
    return {
      "code": 400,
      "message": "",
      "data": [
        VEHICLE_CATEGORY_NOT_FOUND,
        404,
        INFO_INVALID_DETAILS,
        { "vehicleCategoryId": context.params.vehicleCategoryId },
      ],
    };
  }

  const vehicleCategoryDatafromServiceCategory =
    serviceCategoryData.categoryData.vehicles.find(
      (vehicleCategory) =>
        vehicleCategory.categoryId.toString() ===
        context.params.vehicleCategoryId.toString()
    );

  if (!vehicleCategoryDatafromServiceCategory) {
    return {
      "code": 400,
      "message": "",
      "data": [
        VEHICLE_CATEGORY_NOT_FOUND,
        404,
        INFO_INVALID_DETAILS,
        { "vehicleCategoryId": context.params.vehicleCategoryId },
      ],
    };
  }

  const mapData = await googleApiUtil.directions({
    "clientId": clientId,
    "from": `${context.params.origin.lat},${context.params.origin.lng}`,
    "to": `${context.params.destination.lat},${context.params.destination.lng}`,
  });
  if (mapData.status > 200) {
    return {
      "code": 400,
      "message": "",
      "data": [mapData.status, 400, INFO_NOT_FOUND, mapData],
    };
  }
  vehicleCategoryData = {
    ...vehicleCategoryData,
    ...vehicleCategoryDatafromServiceCategory,
  };
  //
  const radius =
    parseInt(generalSettings.data.requestDistance) +
    parseInt(generalSettings.data.retryRequestDistance);
  let professionalRetryData = await this.getProfessionalByLocation(
    //clientId,
    context.params.origin.lat,
    context.params.origin.lng,
    context.params.destination.lat,
    context.params.destination.lng,
    context.params.vehicleCategoryId,
    constantUtil.BOOKING, // actionType
    context.params.rideType,
    radius, // generalSettings.data.requestDistance,
    // radius + parseInt(generalSettings.data.requestDistance), // maxRadius
    true,
    true,
    false,
    vehicleCategoryData.isSubCategoryAvailable,
    vehicleCategoryData.isForceAppliedToProfessional,
    context.params.isGenderAvailable || false,
    context.params.childseatAvailable || false,
    context.params.handicapAvailable || false,
    false,
    null, //isRentalSupported
    context.params?.serviceAreaId,
    context.params?.isShareRide,
    context.params?.paymentOption || null,
    context.params?.languageCode || null,
    context.params?.isPetAllowed || false
  );
  if (!professionalRetryData[0][0]) {
    professionalRetryData = await this.getProfessionalByLocation(
      //clientId,
      context.params.origin.lat,
      context.params.origin.lng,
      context.params.destination.lat,
      context.params.destination.lng,
      context.params.vehicleCategoryId,
      constantUtil.BOOKING, // actionType
      context.params.rideType,
      context.params?.radius || radius,
      // context.params?.maxRadius ||
      //   radius + parseInt(generalSettings.data.requestDistance), // maxRadius,
      false,
      true,
      generalSettings.data.isTailRideNeeded,
      vehicleCategoryData.isSubCategoryAvailable,
      vehicleCategoryData.isForceAppliedToProfessional,
      context.params.isGenderAvailable || false,
      context.params.childseatAvailable || false,
      context.params.handicapAvailable || false,
      false,
      null, //isRentalSupported
      context.params?.serviceAreaId,
      context.params?.isShareRide,
      context.params?.paymentOption || null,
      context.params?.languageCode || null,
      context.params?.isPetAllowed || false
    );
  }

  if (!professionalRetryData[0][0]) {
    return {
      "code": 204,
      "message": "",
      "data": [PROFESSIONAL_NOT_FOUND, 204, INFO_SERVICE_NOT_PROVIDE, {}],
    };
  }
  professionalRetryData = professionalRetryData[0];

  const requestSendProfessionals = [];
  if (
    generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
    generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST
  ) {
    requestSendProfessionals.push({
      "professionalId": professionalRetryData[0]?._id?.toString(),
      "requestReceiveDateTime": new Date().toISOString(),
      "count": 1,
      "location.coordinates": professionalRetryData[0]?.location?.coordinates,
      "duration": professionalRetryData[0]?.duration,
      "distance": professionalRetryData[0]?.distance,
    });
    professionalRetryData = [professionalRetryData[0]];
  }
  // timeing
  const peakTiming = invoiceUtils.checkPeakTiming({
    ...serviceCategoryData.isPeakFareAvailable,
    "bookingDate": new Date(),
  });
  const nightTiming = invoiceUtils.checkNightTiming({
    ...serviceCategoryData.isNightFareAvailable,
    "bookingDate": new Date(),
  });

  const invoice = invoiceUtils.calculateInvoiceforThirdPartyBookingService({
    "vehicleCategoryData": vehicleCategoryData,
    "peakTiming": peakTiming,
    "nightTiming": nightTiming,
    "estimationDistance": mapData.data.routes[0].legs[0].distance.value,
    "estimationTime": mapData.data.routes[0].legs[0].duration.value,
  });

  const bookingData = await this.adapter.model.create({
    "clientId": context.params.clientId,
    "bookingId": `EX-${customAlphabet("1234567890", 9)()}`,
    "bookingOTP": customAlphabet("1234567890", 4)().toString(),
    "bookingType": constantUtil.INSTANT,
    "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
    "guestType": constantUtil.BOOKINGSERVICEMANAGEMET,
    "bookingDate": helperUtils.toUTC(new Date().toISOString()),
    "bookingFor": context.params.bookingFor,
    "bookingBy": constantUtil.BOOKINGSERVICEMANAGEMET,
    "tripType": constantUtil.DAILYTRIP,
    "paymentSection": constantUtil.ALL,
    "admin": null,
    "user": null,
    "category": context.params.serviceCategoryId,
    "couponId": null,
    "isGenderAvailable": false,
    "childseatAvailable": false,
    "handicapAvailable": false,
    "vehicle": {
      "vehicleCategoryId": context.params.vehicleCategoryId,
    },
    "origin": {
      "addressName": mapData.data.routes[0].legs[0].start_address,
      "fullAddress": mapData.data.routes[0].legs[0].start_address,
      "shortAddress": mapData.data.routes[0].legs[0].start_address,
      "lat": context.params.origin.lat,
      "lng": context.params.origin.lng,
    },
    "destination": {
      "addressName": mapData.data.routes[0].legs[0].end_address,
      "fullAddress": mapData.data.routes[0].legs[0].end_address,
      "shortAddress": mapData.data.routes[0].legs[0].end_address,
      "lat": context.params.destination.lat,
      "lng": context.params.destination.lng,
    },
    "estimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
    },
    "waitingTime": {
      "waitingStartTime": null,
      "waitingEndTime": null,
    },
    "currencySymbol": serviceCategoryData.currencySymbol,
    "currencyCode": serviceCategoryData.currencyCode,
    "distanceUnit": serviceCategoryData.distanceType,
    "invoice": invoice,
    "activity": {
      "noOfUsersInVehicle": null,
      "noOfSeatsAvailable": null,
      "isUserPickedUp": false,
      "isUserDropped": false,
      "waitingDuration": null,
      "actualBookingTime": new Date(),
      "bookingTime": new Date(),
      "denyOrExpireTime": null,
      "cancelTime": null,
      "acceptTime": null,
      "arriveTime": null,
      "pickUpTime": null,
      "dropTime": null,
      "rideStops": context.params?.bookingLocations ?? [],
    },
    "rideStops": context.params.bookingLocations ?? [],
    "payment": {
      "option": constantUtil.PAYMENTCOMMISSION,
      "details": null,
      "card": {},
      "paid": false,
    },
    // for alogorithim
    "requestSendProfessionals": requestSendProfessionals,
    "requestRetryCount": 1,
    "regionalData": {
      "bookingDate": helperUtil.toRegionalUTC(new Date()),
      "cancelTime": null,
      "acceptTime": null,
      "arriveTime": null,
      "pickUpTime": null,
      "dropTime": null,
    },
    "pickupLocation.coordinates": [
      [context.params.origin.lng, context.params.origin.lat],
    ],
    "dropLocation.coordinates": [
      [context.params.destination.lng, context.params.destination.lat],
    ],
    "upcomingLocation.coordinates": [
      [context.params.origin.lng, context.params.origin.lat],
    ],
  });
  //  send request to professionals

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;

  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = serviceCategoryData.distanceType;

  const notificationObject = {
    "clientId": context.params.clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": constantUtil.PROFESSIONAL,
      "action": constantUtil.ACTION_BOOKINGREQUEST,
      "timestamp": Date.now(),
      "message": PROFESSIONAL_NEW_RIDE,
      "details": lzStringEncode(bookingObject(bookingData)),
    },
    "registrationTokens": professionalRetryData?.map((professional) => {
      if (
        // parseFloat(professional.wallet.availableAmount) <
        // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
        bookingData.payment.option === constantUtil.WALLET ||
        bookingData.payment.option === constantUtil.PAYMENTCARD ||
        // bookingData.payment.option !== constantUtil.CASH &&
        // bookingData.payment.option !== constantUtil.CREDIT) ||
        parseFloat(professional.wallet.availableAmount) >=
          parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
      ) {
        return {
          "token": professional?.deviceInfo[0]?.deviceId,
          "id": professional?._id?.toString(),
          "deviceType": professional?.deviceInfo[0]?.deviceType,
          "platform": professional?.deviceInfo[0]?.platform,
          "socketId": professional?.deviceInfo[0]?.socketId,
          "duration": professional?.duration,
          "distance": professional?.distance,
        };
      }
    }),
  };

  /* SOCKET PUSH NOTIFICATION */
  this.broker.emit("socket.sendNotification", notificationObject);
  // this.broker.emit('socket.statusChangeEvent', notificationObject)

  /* FCM */
  this.broker.emit("admin.sendFCM", notificationObject);

  return {
    "code": 200,
    "message": PROFESSIONAL_NOTIFICATION,
    "data": helperUtil.thirdPartyBookingObjet(bookingData),
  };
};

bookingEvent.getAllRideDetailsExternal = async function (context) {
  const data = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "createdAt": {
            "$gte": setDayStartHours(context.params.fromDate),
            "$lte": setDayEndHours(context.params.toDate),
          },
          "bookingStatus": constantUtil.ENDED,
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "let": { "prof": "$professional" },
          "pipeline": [
            {
              "$match": {
                "$expr": { "$eq": ["$$prof", "$_id"] },
              },
            },
            {
              "$project": {
                "vehicleRegistrationNumber": {
                  "$arrayElemAt": ["$vehicles.plateNumber", 0],
                },
                "licenceNo": 1,
              },
            },
          ],
          "as": "professional",
        },
      },
      {
        "$project": {
          "tripId": "$bookingId",
          "pickUp": {
            "$concat": [
              { "$toString": "$origin.lng" },
              ",",
              { "$toString": "$origin.lat" },
            ],
          },
          "drop": {
            "$concat": [
              { "$toString": "$destination.lng" },
              ",",
              { "$toString": "$destination.lat" },
            ],
          },
          "startTime": "$activity.pickUpTime",
          "endTime": "$activity.dropTime",
          "totalFareAmount": "$invoice.payableAmount",
          "tripDistance": "$estimation.distance",
          "userRating": "$bookingReview.userReview.rating",
          "driverRating": "$bookingReview.professionalReview.rating",
          "driverEarnings": "$invoice.professionalCommision",
          "vehicleRegistrationNumber": {
            "$arrayElemAt": ["$professional.vehicleRegistrationNumber", 0],
          },
          "licenseNumber": {
            "$arrayElemAt": ["$professional.licenceNo", 0],
          },
        },
      },
    ])
    .allowDiskUse(true);
  if (!data) {
    return "helo";
  }
  return data;
};
//#endregion third party Booking

//#region Ride Booking Details
bookingEvent.rideBookingDetails = async function (context) {
  let bookingData = null;

  const bookingId = `${context.params.bookingPrefix}-${customAlphabet(
    "1234567890",
    9
  )()}`;
  const invoiceObject = await mappingUtils.calculateInvoiceObject(
    context.params
  );
  //-----------------------------------
  try {
    bookingData = await this.adapter.model.create({
      "bookingId": bookingId,
      "bookingOTP": customAlphabet("1234567890", 4)().toString(),
      "bookingType": context.params.bookingType,
      "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
      "guestType": context.params.guestType,
      "bookingDate": context.params.newDate,
      "regionalData": {
        "bookingDate": context.params.newRegionalDate,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
      },
      "bookingFor": context.params.bookingFor,
      "bookingBy": context.params.bookingBy,
      "bookingFrom": context.params.bookingFrom,
      "tripType": context.params.tripType,
      "paymentSection": context.params.paymentSection,
      "admin": null,
      "user": context.params.userId,
      "corporateEmailId": context.params.corporateEmailId || null,
      "category": context.params.categoryId,
      "couponId": context.params.couponId || null,
      "isGenderAvailable": context.params.isGenderAvailable || false,
      "childseatAvailable": context.params.childseatAvailable || false,
      "handicapAvailable": context.params.handicapAvailable || false,
      "isShareRide": context.params.isShareRide || false,
      "isPetAllowed": context.params.isPetAllowed || false,
      "vehicle": {
        "vehicleCategoryId": context.params.vehicleCategoryId,
      },
      "origin": {
        "addressName": context.params.pickUpAddressName,
        "fullAddress": context.params.pickUpFullAddress,
        "shortAddress": context.params.pickUpShortAddress,
        "lat": context.params.pickUpLat,
        "lng": context.params.pickUpLng,
      },
      "destination": {
        "addressName": context.params.dropAddressName,
        "fullAddress": context.params.dropFullAddress,
        "shortAddress": context.params.dropShortAddress,
        "lat": context.params.dropLat,
        "lng": context.params.dropLng,
      },
      "estimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "bookedEstimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "waitingTime": {
        "waitingStartTime": null,
        "waitingEndTime": null,
      },
      "currencySymbol": context.params.currencySymbol,
      "currencyCode": context.params.currencyCode,
      "distanceUnit": context.params.distanceType,
      "invoice": invoiceObject,
      "serviceCategory": context.params.serviceCategory,
      "activity": {
        "noOfUsersInVehicle": null,
        "noOfSeatsAvailable": null,
        "isUserPickedUp": false,
        "isUserDropped": false,
        "waitingDuration": null,
        "actualBookingTime": new Date(),
        "bookingTime": new Date(),
        "denyOrExpireTime": null,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
        "rideStops": context.params.bookingLocations || [],
      },
      "rideStops": context.params.bookingLocations || [],
      "notes": context.params.notes || null,
      "payment": context.params.payment,
      // for strip Payment wave
      "paymentInitId": context.params.paymentInitId || null,
      "paymentInitAmount": context.params.paymentInitAmount || 0,
      // for algorithm
      "requestSendProfessionals": context.params.requestSendProfessionals,
      "requestRetryCount": 1,
      "isRestrictMaleBookingToFemaleProfessional":
        context.params.isRestrictMaleBookingToFemaleProfessional,
      // "rental": {
      //   "package": context.params.rentalPackage || null,
      // },
      "totalPassengerCount": context.params.seatCount || 0,
      "currentPassengerCount": context.params.passengerCount || 0,
      "passengerCount": context.params.passengerCount || 0,
      // "location": [],
      "pickupLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "dropLocation.coordinates": [
        [context.params.dropLng, context.params.dropLat],
      ],
      "upcomingLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "rideCoordinatesEncoded": context.params.rideCoordinatesEncoded,
      "flightNumber": context.params.flightNumber,
      "welcomeSignboardText": context.params.welcomeSignboardText,
      "isRoundTrip": context.params.isRoundTrip,
      "roundTripDetails": context.params.roundTripDetails,
    });
  } catch (e) {
    bookingData = null;
  }
  return bookingData;
};
//#endregion Ride Booking Details

//#region Data Migration
bookingEvent.bookingDataMigrationScript = async function (context) {
  try {
    const dataList = await this.adapter.model
      .find(
        { "regionalData.bookingDate": { "$eq": null } },
        {
          "regionalData": 1,
          "activity.bookingTime": 1,
          "activity.acceptTime": 1,
          "activity.arriveTime": 1,
          "activity.pickUpTime": 1,
          "activity.dropTime": 1,
          "activity.cancelTime": 1,
        }
      )
      .limit(1000);

    dataList?.map(async (element) => {
      // await this.adapter.model.findOneAndUpdate(
      await this.adapter.model.updateOne(
        { "_id": mongoose.Types.ObjectId(element._id.toString()) },
        {
          "$set": {
            "regionalData.bookingDate": element.activity.bookingTime,
            "regionalData.acceptTime": element.activity.acceptTime,
            "regionalData.arriveTime": element.activity.arriveTime,
            "regionalData.pickUpTime": element.activity.pickUpTime,
            "regionalData.dropTime": element.activity.dropTime,
            "regionalData.cancelTime": element.activity.cancelTime,
          },
        }
      );
      console.log(JSON.stringify(element));
    });
    // each(dataList, async (element) => {
    //   await this.adapter.model.findOneAndUpdate(
    //     { "_id": mongoose.Types.ObjectId(element._id.toString()) },
    //     {
    //       "$set": {
    //         "regionalData.bookingDate": element.activity.bookingTime,
    //         "regionalData.acceptTime": element.activity.acceptTime,
    //         "regionalData.arriveTime": element.activity.arriveTime,
    //         "regionalData.pickUpTime": element.activity.pickUpTime,
    //         "regionalData.dropTime": element.activity.dropTime,
    //         "regionalData.cancelTime": element.activity.cancelTime,
    //       },
    //     }
    //   );
    //   console.log(JSON.stringify(element));
    //   // upnext();
    // });
  } catch (e) {
    const log = e;
  }
};
//#endregion

bookingEvent.getOnlineProfessionals = async function (context) {
  let professionalJSON = [];
  const clientId = context.params.clientId;
  const generalSettingsJson = await storageUtil.read(
    constantUtil.GENERALSETTING
  );
  const radius =
    parseInt(generalSettingsJson.data.requestDistance) +
    parseInt(generalSettingsJson.data.retryRequestDistance);
  const checkData = {
    ...context.params,
    "clientId": clientId,
    "vehicleCategoryId": null,
    "professionalOnlineStatus": true,
    "actionType": constantUtil.CONST_ONLINE,
    "rideType": context.params.rideType,
    "radius": context.params?.radius || radius,
    // "maxRadius":
    //   context.params.maxRadius ||
    //   radius + parseInt(generalSettingsJson.data.requestDistance),
    "checkOngoingBooking": false,
    "checkLastPriority": false,
    "forTailRide": generalSettingsJson.data.isTailRideNeeded,
    "isSubCategoryAvailable": true,
    "isForceAppliedToProfessional": true,
    "isGenderAvailable": false,
    "childseatAvailable": false,
    "handicapAvailable": false,
    "isRestrictMaleBookingToFemaleProfessional": false,
    "isRentalSupported": false,
    "serviceAreaId": context.params?.serviceAreaId || null,
    "isShareRide": context.params?.isShareRide ?? null, // null --> Default For get Online Professional
    "paymentOption": context.params?.paymentOption || null, // null --> Default For get All PaymentMode Professional
    "languageCode": context.params?.languageCode || null, // null --> Default For get All language Known Professional
    "isPetAllowed": context.params?.isPetAllowed || false,
    "isEnableLuggage": context.params?.isEnableLuggage || false,
  };
  let professionalDataResponse = await this.broker.emit(
    "professional.getProfessionalByLocation",
    checkData
  );
  professionalDataResponse =
    professionalDataResponse && professionalDataResponse[0];
  if (professionalDataResponse?.length === 0) {
    return professionalJSON;
  } else {
    //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    const userType = (context.params.userType || "").toUpperCase();
    let userData = null;
    if (userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "clientId": context.params?.clientId,
        "id": context.params?.userId?.toString(),
      });
      userData = userData && userData[0];
    }
    professionalDataResponse =
      await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
        professionalDataResponse,
        null, // bookingData
        userData
      );
    professionalJSON = professionalDataResponse;
    // if (!professionalData || (professionalData && professionalData.length === 0))
    //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    // const vehicleCategoryJson = await storageUtil.read(
    //   constantUtil.VEHICLECATEGORY
    // );
    // professionalJSON = professionalDataResponse.map((professional) => {
    //   const defaultVehicle = professional.vehicles.filter((vehicle) => {
    //     vehicle = JSON.parse(JSON.stringify(vehicle));
    //     return (
    //       vehicle.defaultVehicle === true &&
    //       vehicle.status === constantUtil.ACTIVE
    //     );
    //   });
    //   if (defaultVehicle[0]?.vehicleCategoryId) {
    //     const vehicalCategoryMetaData =
    //       vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];
    //     return {
    //       "currentBearing": professional.currentBearing,
    //       "driverId": professional._id,
    //       "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
    //       "isSubCategoryAvailable": defaultVehicle[0].isSubCategoryAvailable,
    //       "applySubCategory": defaultVehicle[0].applySubCategory,
    //       "subCategoryIds": defaultVehicle[0].subCategoryIds,
    //       "isHaveShare": vehicalCategoryMetaData?.mandatoryShare || false,
    //       "vehicleCategoryMapImage":
    //         vehicalCategoryMetaData?.categoryMapImage || "",
    //       "lat": professional.location.coordinates[1],
    //       "lng": professional.location.coordinates[0],
    //       "serviceType": vehicalCategoryMetaData?.vehicleCategory || "",
    //       "fullName":
    //         professional?.firstName ?? "" + " " + professional?.lastName ?? "",
    //       "avatar": professional?.avatar ?? "",
    //       "reviewRating": professional?.review?.avgRating ?? 0,
    //     };
    //   }
    // });
    return professionalJSON;
  }
};

bookingEvent.updatePackageDetailsById = async function (context) {
  let isSuccess = true;
  try {
    const mongoId = mongoose.Types.ObjectId();
    let updateImageQuery = null;
    switch (context.params.uploadedBy?.toUpperCase()) {
      case constantUtil.ADMIN:
        break;

      case constantUtil.USER:
        updateImageQuery = {
          "$set": {
            "packageData.userImage": context.params.packageData.userImage || [],
            "packageData.userImageUploadedAt": new Date(),
          },
        };
        break;

      case constantUtil.PROFESSIONAL:
        updateImageQuery = {
          "$set": {
            "packageData.professionalImagePickup":
              context.params.packageData.professionalImagePickup || [],
            "packageData.professionalPickupedAt": new Date(),
          },
        };
        break;

      case constantUtil.PACKAGEDELIVERY:
        updateImageQuery = {
          "$set": {
            "packageData.professionalImageDelivery":
              context.params.packageData.professionalImageDelivery || [],
            "packageData.professionaDeliveredAt": new Date(),
            // "packageData.packageReceiver":
            //   context.params.packageData.packageReceiver || [],
            // "packageData.packageReceivedAt": new Date(),
          },
        };
        break;

      case constantUtil.PACKAGEDELIVERYANDRECEIVER:
        updateImageQuery = {
          "$set": {
            "packageData.professionalImageDelivery":
              context.params.packageData.professionalImageDelivery || [],
            "packageData.professionaDeliveredAt": new Date(),
            "packageData.packageReceiver":
              context.params.packageData.packageReceiver || [],
            "packageData.packageReceivedAt": new Date(),
          },
        };
        break;

      case constantUtil.PACKAGERECEIVER:
        updateImageQuery = {
          "$set": {
            "packageData.packageReceiver":
              context.params.packageData.packageReceiver || [],
            "packageData.packageReceivedAt": new Date(),
          },
        };
        break;
    }

    if (context.params.action === constantUtil.ADD) {
      //Update Package Details
      const updatedPackageData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
        },
        {
          "$set": {
            "packageData.recipientNo":
              context.params?.packageData?.recipientNo || null,
            "packageData.recipientPhoneCode":
              context.params?.packageData?.recipientPhoneCode || null,
            "packageData.recipientPhoneNo":
              context.params?.packageData?.recipientPhoneNo || null,
            "packageData.recipientName":
              context.params?.packageData?.recipientName || null,
            "packageData.isEnableOTPVerification":
              context.params?.packageData?.isEnableOTPVerification || false,
            "packageData.isEnableNoContactDelivery":
              context.params?.packageData?.isEnableNoContactDelivery || false,
            "packageData.isEnablePetsAtMyHome":
              context.params?.packageData?.isEnablePetsAtMyHome || false,
            "packageData.selectedPackageContent":
              context.params?.packageData?.selectedPackageContent || null,
            "packageData.instruction":
              context.params?.packageData?.instruction || null,
          },
        }
      );
    }
    //Update Image Details
    if (updateImageQuery) {
      const updatedPackageImageData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
        },
        {
          ...updateImageQuery,
        }
      );
    }
  } catch (e) {
    isSuccess = false;
  }
  return isSuccess;
};
//#region Share Ride
bookingEvent.checkAndUpdateOngoingShareRide = async function (context) {
  let responseData,
    shareRideProfessional,
    bookingId = null,
    rideCheckQuery = {};
  try {
    const actionStatus = context.params?.actionStatus?.toUpperCase();
    // switch (actionStatus) {
    //   case constantUtil.STATUS_UPCOMING:
    //     {
    //       rideCheckQuery = {
    //         // "bookingStatus": constantUtil.ACCEPTED,
    //         "bookingStatus": {
    //           "$in": [
    //             constantUtil.ACCEPTED,
    //             constantUtil.ARRIVED,
    //             constantUtil.STARTED,
    //           ],
    //         },
    //         "pickupLocation": {
    //           // "$nearSphere": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [context.params.lng, context.params.lat],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 500, //need to change dynamic
    //           },
    //         },
    //       };
    //     }
    //     break;

    //   case constantUtil.ACCEPTED:
    //     {
    //       // "bookingStatus": {
    //       //   "$in": [
    //       //     constantUtil.ACCEPTED,
    //       //     // constantUtil.ARRIVED,
    //       //     // constantUtil.STARTED,
    //       //   ],
    //       // },
    //       rideCheckQuery = {
    //         "bookingStatus": constantUtil.ACCEPTED,
    //         // "bookingStatus": {
    //         //   "$in": [
    //         //     constantUtil.ACCEPTED,
    //         //     constantUtil.ARRIVED,
    //         //     constantUtil.STARTED,
    //         //   ],
    //         // },
    //         "pickupLocation": {
    //           // "$nearSphere": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [context.params.lng, context.params.lat],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 500, //need to change dynamic
    //           },
    //         },
    //       };
    //     }
    //     break;

    //   case constantUtil.CONST_PICKUP:
    //     {
    //       rideCheckQuery = {
    //         "bookingStatus": constantUtil.ARRIVED,
    //         // "bookingStatus": {
    //         //   "$in": [
    //         //     constantUtil.ACCEPTED,
    //         //     constantUtil.ARRIVED,
    //         //     constantUtil.STARTED,
    //         //   ],
    //         // },
    //         "pickupLocation": {
    //           // "$nearSphere": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [context.params.lng, context.params.lat],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 500, //need to change dynamic
    //           },
    //         },
    //       };
    //     }
    //     break;

    //   case constantUtil.STARTED:
    //     {
    //       rideCheckQuery = {
    //         "bookingStatus": constantUtil.STARTED,
    //         // "bookingStatus": {
    //         //   "$in": [
    //         //     constantUtil.ACCEPTED,
    //         //     constantUtil.ARRIVED,
    //         //     constantUtil.STARTED,
    //         //   ],
    //         // },
    //         "dropLocation": {
    //           // "$nearSphere": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [context.params.lng, context.params.lat],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 500, //need to change dynamic
    //           },
    //         },
    //       };
    //     }
    //     break;

    //   case constantUtil.CONST_DROP:
    //     {
    //       rideCheckQuery = {
    //         "bookingStatus": constantUtil.STARTED,
    //         "dropLocation": {
    //           // "$nearSphere": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [context.params.lng, context.params.lat],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 500, //need to change dynamic
    //           },
    //         },
    //       };
    //     }
    //     break;
    // }
    // find next shortest path Child Share ride
    const nextOngoingChildShardRide = await this.adapter.model
      .findOne({
        // "parentShareRideId": { "$nin": null },
        "isShareRide": true,
        "professional": mongoose.Types.ObjectId(context.params.professionalId),
        "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // ...ridePickupCheckQuery,
        "bookingStatus": {
          "$in": [
            constantUtil.ACCEPTED,
            constantUtil.ARRIVED,
            constantUtil.STARTED,
          ],
        },
        "upcomingLocation": {
          // "$nearSphere": {
          "$near": {
            "$geometry": {
              "type": "Point",
              "coordinates": [context.params.lng, context.params.lat],
            },
          },
        },
      })
      .lean();
    // // find next shortest path Parent Share ride
    // const nextOngoingParentShardRide = await this.adapter.model
    //   .findOne({
    //     "parentShareRideId": null,
    //     "isShareRide": true,
    //     "professional": mongoose.Types.ObjectId(context.params.professionalId),
    //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
    //     "bookingStatus": {
    //       "$in": [
    //         constantUtil.ACCEPTED,
    //         constantUtil.ARRIVED,
    //         constantUtil.STARTED,
    //       ],
    //     },
    //     // ...rideCheckQuery,
    //   })
    //   .lean();
    // -------------------------
    if (nextOngoingChildShardRide) {
      bookingId = nextOngoingChildShardRide?._id?.toString(); // next ongoing Child Ride
      // } else if (
      //   nextOngoingChildShardRide === null &&
      //   nextOngoingParentShardRide
      // ) {
      //   bookingId = nextOngoingParentShardRide?._id?.toString(); // next ongoing Parent Ride
    } else if (context.params.bookingId) {
      bookingId = context.params.bookingId;
    }
    // Update current passanger count
    if (actionStatus === constantUtil.CONST_DROP) {
      await this.adapter.model.updateOne(
        {
          // "_id": mongoose.Types.ObjectId(bookingId?.toString()),
          "_id": mongoose.Types.ObjectId(context.params.parentShareRideId),
          "parentShareRideId": null,
          "isShareRide": true,
          "professional": mongoose.Types.ObjectId(
            context.params.professionalId
          ),
          "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        },
        {
          "$inc": {
            "currentPassengerCount": -parseInt(
              context.params?.lastRidePassengerCount || 0
            ),
          },
        }
      );
    }
    // if (bookingId) {
    shareRideProfessional = await this.broker.emit(
      "professional.updateProfessionalShareRide",
      {
        "clientId": context.params?.clientId,
        "actionStatus": constantUtil.STATUS_ONGOING,
        "bookingId": bookingId, // next ongoing Ride
        "professionalId": context.params.professionalId?.toString(),
        // "pickUpLat": null,
        // "pickUpLng": null,
        // "dropLat": null,
        // "dropLng": null,
      }
    );
    responseData = shareRideProfessional && shareRideProfessional[0];
    // }
  } catch (e) {
    responseData = null;
  }
  return responseData;
};

bookingEvent.sendNotificationActionShareRideStatus = async function (context) {
  let responseData;
  try {
    const shareRideDataList = await this.adapter.model
      .find(
        {
          "$or": [
            {
              "_id": mongoose.Types.ObjectId(
                context.params?.parentShareRideId?.toString()
              ),
            },
            {
              "parentShareRideId": mongoose.Types.ObjectId(
                context.params?.parentShareRideId?.toString()
              ),
            },
          ],
          "bookingStatus": {
            "$in": [
              constantUtil.ACCEPTED,
              constantUtil.ARRIVED,
              constantUtil.STARTED,
            ],
          },
          "_id": {
            "$nin": mongoose.Types.ObjectId(
              context.params?.bookingId?.toString()
            ),
          },
        }
        // {
        //   "professional": 1,
        //   "user": 1,
        //   "parentShareRideId": 1,
        //   "bookingStatus": 1,
        // }
      )
      .populate("admin", { "data.accessToken": 0, "data.password": 0 })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("coorperate")
      .populate("couponId")
      // .populate("professional", { "firstName": 1, "deviceInfo": 1 })
      // .populate("user", { "firstName": 1, "deviceInfo": 1 })
      .lean();
    await shareRideDataList?.map(async (shareRideData) => {
      let registrationTokensData = [];
      if (context.params?.userType === constantUtil.USER) {
        registrationTokensData = [
          {
            "token": shareRideData.user?.deviceInfo[0]?.deviceId,
            "id": shareRideData.user?._id?.toString(),
            "deviceType": shareRideData.user?.deviceInfo[0]?.deviceType,
            "platform": shareRideData.user?.deviceInfo[0]?.platform,
            "socketId": shareRideData.user?.deviceInfo[0]?.socketId,
          },
        ];
      } else if (context.params?.userType === constantUtil.PROFESSIONAL) {
        registrationTokensData = [
          {
            "token": shareRideData.professional?.deviceInfo[0]?.deviceId,
            "id": shareRideData.professional?._id?.toString(),
            "deviceType": shareRideData.professional?.deviceInfo[0]?.deviceType,
            "platform": shareRideData.professional?.deviceInfo[0]?.platform,
            "socketId": shareRideData.professional?.deviceInfo[0]?.socketId,
          },
        ];
      }
      const shareRideNotificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": context.params?.userType,
          "action": constantUtil.CONST_SHARERIDE + context.params?.actionStatus,
          "timestamp": Date.now(),
          "message": context.params?.notificationMessage,
          "details": lzStringEncode(bookingObject(shareRideData)),
        },
        "registrationTokens": registrationTokensData,
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", shareRideNotificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
      /* FCM */
      this.broker.emit("admin.sendFCM", shareRideNotificationObject);
    });
  } catch (e) {
    responseData = null;
  }
  return responseData;
};
//#endregion Share Ride

//#region Professional & User Incentive
bookingEvent.getRideListByProfessionalId = async function (context) {
  let professionalData = [];
  try {
    professionalData = await this.adapter.model.aggregate([
      {
        "$match": {
          "professional": mongoose.Types.ObjectId(
            context.params.professionalId
          ),
          "bookingDate": {
            "$gte": new Date(context.params.startDate),
            "$lte": new Date(context.params.endDate),
          },
          // "bookingReview.userReview.rating": {
          //   "$gte": context.params.minimumReviewRating,
          // },
          // // "serviceCategory": { "$in": context.params.rideType }, // Need to Check
          "serviceCategory": {
            "$regex": ".*" + context.params.rideType?.toLowerCase() + ".*",
          },
        },
      },
      {
        "$project": {
          "endedRideCount": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "ENDED"] },
              "then": 1,
              "else": 0,
            },
          },
          "professionalCommision": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "ENDED"] },
              "then": "$invoice.professionalCommision",
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": null,
          "allRideCount": { "$sum": 1 },
          // "endedRideCount": { "$sum": "$endedRideCount" },
          "professionalCommision": { "$sum": "$professionalCommision" },
        },
      },
    ]);
    if (professionalData.length > 0) {
      professionalData[0]["endedRideCount"] = await this.adapter.model.count({
        "bookingDate": {
          "$gte": new Date(context.params.startDate),
          "$lte": new Date(context.params.endDate),
        },
        "bookingStatus": constantUtil.ENDED,
        "professional": mongoose.Types.ObjectId(context.params.professionalId),
        "bookingReview.userReview.rating": {
          "$gte": context.params.minimumReviewRating,
        },
        // "serviceCategory": { "$in": context.params.rideType }, // Need to Check
        "serviceCategory": {
          "$regex": ".*" + context.params.rideType?.toLowerCase() + ".*",
        },
      });
    }
  } catch (e) {
    const x = e;
  }
  return {
    "professionalId": context.params.professionalId,
    "allRideCount": professionalData?.[0]?.allRideCount || 0,
    "endedRideCount": professionalData?.[0]?.endedRideCount || 0,
    "endedRideCount": professionalData?.[0]?.endedRideCount || 0,
    "professionalCommision": professionalData?.[0]?.professionalCommision || 0,
  };
};

bookingEvent.getRideListByUserId = async function (context) {
  let userData = [];
  try {
    userData = await this.adapter.model.aggregate([
      {
        "$match": {
          "user": mongoose.Types.ObjectId(context.params.userId),
          "bookingDate": {
            "$gte": new Date(context.params.startDate),
            "$lte": new Date(context.params.endDate),
          },
          // "bookingReview.userReview.rating": {
          //   "$gte": context.params.minimumReviewRating,
          // },
          // // "serviceCategory": { "$in": context.params.rideType }, // Need to Check
          "serviceCategory": {
            "$regex": ".*" + context.params.rideType?.toLowerCase() + ".*",
          },
        },
      },
      {
        "$project": {
          "endedRideCount": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "ENDED"] },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": null,
          "allRideCount": { "$sum": 1 },
          "endedRideCount": { "$sum": "$endedRideCount" },
          "totalNoOfEligibleRides": { "$sum": 0 },
        },
      },
    ]);
    if (userData.length > 0) {
      userData[0]["totalNoOfEligibleRides"] = await this.adapter.model.count({
        "bookingDate": {
          "$gte": new Date(context.params.startDate),
          "$lte": new Date(context.params.endDate),
        },
        "bookingStatus": constantUtil.ENDED,
        "user": mongoose.Types.ObjectId(context.params.userId),
        "invoice.payableAmount": {
          "$gte": context.params.minimumEligibleAmount,
        },
        "bookingReview.professionalReview.rating": {
          "$gte": context.params.minimumReviewRating,
        },
        // "serviceCategory": { "$in": context.params.rideType }, // Need to Check
        "serviceCategory": {
          "$regex": ".*" + context.params.rideType?.toLowerCase() + ".*",
        },
      });
    }
  } catch (e) {
    const x = e;
  }
  return {
    "userId": context.params.userId,
    "allRideCount": userData?.[0]?.allRideCount || 0,
    "endedRideCount": userData?.[0]?.endedRideCount || 0,
    "totalNoOfEligibleRides": userData?.[0]?.totalNoOfEligibleRides || 0,
  };
};

//#endregion Professional & User Incentive

//#region Professional Ranking
bookingEvent.getProfessionalRankingReport = async function (context) {
  let jsonData = [],
    matchPhoneNumberQuery = {},
    matchServiceAreaQuery = {};
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    if (
      (context.params?.createdAtFrom || "") !== "" &&
      (context.params?.createdAtTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    //
    if ((context.params?.phoneNumber || "") !== "") {
      matchPhoneNumberQuery = {
        "$match": {
          "allProfessional.phone.number": {
            "$regex": context.params.phoneNumber?.trim(),
            "$options": "si",
          },
        },
      };
    } else {
      matchPhoneNumberQuery = {
        "$match": {
          "allProfessional.phone.number": { "$ne": null },
        },
      };
    }
    //
    if (
      (context.params.city || "") !== "" &&
      context.params.city !== constantUtil.ALL
    ) {
      // query.push({
      //   "$match": {
      //     "category": mongoose.Types.ObjectId(context.params.city),
      //   },
      // });
      matchServiceAreaQuery = {
        "$match": {
          "allProfessional.serviceAreaId": mongoose.Types.ObjectId(
            context.params.city
          ),
        },
      };
    } else {
      matchServiceAreaQuery = {
        "$match": {
          "allProfessional.serviceAreaId": { "$ne": null },
        },
      };
    }
    // query.push({
    //   "$match": {
    //     "bookingStatus": {
    //       $in: [
    //         constantUtil.ENDED,
    //         // constantUtil.PROFESSIONALDENY,
    //         constantUtil.PROFESSIONALCANCELLED,
    //       ],
    //     },
    //   },
    // });
    query.push(
      // {
      //   "$lookup": {
      //     "from": "professionals",
      //     "localField": "requestSendProfessionals.professionalId",
      //     "foreignField": "_id",
      //     "as": "requestSendProfessionals",
      //   },
      // },
      // {
      //   "$unwind": {
      //     "path": "$requestSendProfessionals",
      //     "preserveNullAndEmptyArrays": true,
      //   },
      // },
      // {
      //   "$lookup": {
      //     "from": "professionals",
      //     "localField": "denyProfessionals.professionalId",
      //     "foreignField": "_id",
      //     "as": "denyProfessionals",
      //   },
      // },
      // {
      //   "$unwind": {
      //     "path": "$denyProfessionals",
      //     "preserveNullAndEmptyArrays": true,
      //   },
      // },
      {
        "$project": {
          "_id": "$_id",
          "bookingDate": "$bookingDate",
          "bookingId": "$bookingId",
          "bookingStatus": "$bookingStatus",
          "professional": "$professional",
          "denyProfessionals": "$denyProfessionals",
          "cancelledProfessional": "$cancelledProfessionalList",
          "requestSendProfessionals": "$requestSendProfessionals",
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "denyProfessionals.professionalId",
          "foreignField": "_id",
          "as": "newdenyProfessionals",
        },
      },
      {
        "$unwind": {
          "path": "$newdenyProfessionals",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "cancelledProfessional.professionalId",
          "foreignField": "_id",
          "as": "newCancelProfessionals",
        },
      },
      {
        "$unwind": {
          "path": "$newCancelProfessionals",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "requestSendProfessionals.professionalId",
          "foreignField": "_id",
          "as": "newRequestSendProfessionals",
        },
      },
      {
        "$unwind": {
          "path": "$newRequestSendProfessionals",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$project": {
          "_id": "$_id",
          "bookingDate": "$bookingDate",
          "bookingId": "$bookingId",
          "bookingStatus": "$bookingStatus",
          "professional": "$professional",
          // "allProfessional": {
          //   "$cond": {
          //     "if": { "$eq": ["$newdenyProfessionals._id", "$professional"] },
          //     "then": "$newdenyProfessionals._id",
          //     "else": "$newRequestSendProfessionals._id",
          //   },
          // },
          "allProfessional": "$newRequestSendProfessionals._id",
          // "denyProfessional": "$newdenyProfessionals",
          "denyProfessional": {
            "$cond": {
              "if": {
                "$eq": [
                  "$newRequestSendProfessionals._id",
                  "$newdenyProfessionals._id",
                ],
              },
              "then": "$newdenyProfessionals._id",
              "else": null,
            },
          },
          "cancelProfessional": {
            "$cond": {
              "if": {
                "$eq": [
                  "$newRequestSendProfessionals._id",
                  "$newCancelProfessionals._id",
                ],
              },
              "then": "$newCancelProfessionals._id",
              "else": null,
            },
          },
          // "allProfessional": {
          //   "$cond": {
          //     "if": {
          //       "$eq": ["$newRequestSendProfessionals._id", "$professional"],
          //     },
          //     "then": {
          //       "$cond": {
          //         "if": {
          //           "$eq": ["$bookingStatus", constantUtil.ENDED],
          //         },
          //         "then": "$newRequestSendProfessionals._id",
          //         "else": "$professional",
          //       },
          //     },
          //     "else": "$professional",
          //   },
          // },
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "allProfessional",
          "foreignField": "_id",
          "as": "allProfessional",
        },
      },
      {
        "$unwind": {
          "path": "$allProfessional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      matchPhoneNumberQuery,
      matchServiceAreaQuery
    );

    query.push({
      "$facet": {
        "totalCount": [
          {
            "$count": "count",
          },
        ],
        "responseData": [
          {
            "$project": {
              "_id": 0,
              "bookingDate": "$bookingDate",
              "bookingId": "$bookingId",
              "bookingStatus": "$bookingStatus",
              "professionalName": {
                "$concat": [
                  "$allProfessional.firstName",
                  " ",
                  "$allProfessional.lastName",
                ],
              },
              "professionalNumber": {
                "$concat": [
                  "$allProfessional.phone.code",
                  " ",
                  "$allProfessional.phone.number",
                ],
              },
              "professionalId": "$allProfessional._id",
              "professionalStatus": "$allProfessional.status",
              "deviceInfo": "$allProfessional.deviceInfo",
              "blockedTill": "$allProfessional.blockedTill",
              "unblockedTill": "$allProfessional.unblockedTill",
              "completedTrip": {
                "$cond": {
                  "if": {
                    "$eq": ["$allProfessional._id", "$professional"],
                    "$eq": ["$bookingStatus", constantUtil.ENDED],
                  },
                  "then": 1,
                  "else": 0,
                },
              },
              // "deniedTrip": {
              //   "$cond": {
              //     "if": {
              //       "$eq": ["$allProfessional._id", "$denyProfessional"],
              //     },
              //     "then": 1,
              //     "else": 0,
              //   },
              // },
              "cancelledTrip": {
                "$cond": {
                  "if": {
                    "$eq": ["$allProfessional._id", "$cancelProfessional"],
                  },
                  "then": 1,
                  "else": 0,
                },
              },
              "notAttemptTrip": {
                "$cond": {
                  "if": {
                    "$or": [
                      // { "$eq": ["$allProfessional._id", "$denyProfessional"] },
                      {
                        "$eq": ["$allProfessional._id", "$cancelProfessional"],
                      },
                      {
                        "$eq": ["$allProfessional._id", "$professional"],
                        "$eq": ["$bookingStatus", constantUtil.ENDED],
                      },
                    ],
                  },
                  "then": 0,
                  "else": 1,
                },
              },
            },
          },
          {
            "$group": {
              "_id": "$professionalId",
              // "bookingDate": { "$first": "$bookingDate" },
              // "bookingId": { "$first": "$bookingId" },
              "professionalName": { "$first": "$professionalName" },
              "professionalNumber": { "$first": "$professionalNumber" },
              "professionalStatus": { "$first": "$professionalStatus" },
              "deviceInfo": { "$first": "$deviceInfo" },
              "blockedTill": { "$first": "$blockedTill" },
              "unblockedTill": { "$first": "$unblockedTill" },
              "totalTrip": { "$sum": 1 },
              "completedTrip": { "$sum": "$completedTrip" },
              "completedRatio": {
                "$avg": { "$multiply": [{ "$sum": 1 }, "$completedTrip"] },
              },
              // "deniedTrip": { "$sum": "$deniedTrip" },
              // "deniedRatio": {
              //   "$avg": { "$multiply": [{ "$sum": 1 }, "$deniedTrip"] },
              // },
              "cancelledTrip": { "$sum": "$cancelledTrip" },
              "cancelledRation": {
                "$avg": { "$multiply": [{ "$sum": 1 }, "$cancelledTrip"] },
              },
              "notAttemptTrip": { "$sum": "$notAttemptTrip" },
              "notAttemptRatio": {
                "$avg": { "$multiply": [{ "$sum": 1 }, "$notAttemptTrip"] },
              },
            },
          },
          {
            "$sort":
              context.params.sortBy === "rejectionRate"
                ? { "notAttemptRatio": -1 }
                : { "deniedRatio": -1 },
          },
          // { "$sort": { "deniedRatio": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
          // { "$sort": { "deniedRatio": -1 } },
        ],
      },
    });
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
    //const totalCount = jsonData?.[0]?.responseData?.length;
    // if (totalCount <= context.params.limit) {
    //   jsonData[0]["totalCount"][0]["count"] = totalCount;
    // }
    const xewrqr = "43425";
  } catch (e) {
    jsonData = [];
  }
  return jsonData;
};
//#endregion Professional Ranking
//#region Ride Review Rating
bookingEvent.getRideReviewRatingReport = async function (context) {
  let jsonData = [],
    matchPhoneNumberQuery = {},
    matchServiceAreaQuery = {};
  try {
    const query = [];
    query.push({ "$match": { "bookingStatus": constantUtil.ENDED } });
    // FOR EXPORT DATE FILTER
    if (
      (context.params?.createdAtFrom || "") !== "" &&
      (context.params?.createdAtTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    //
    if ((context.params?.phoneNumber || "") !== "") {
      matchPhoneNumberQuery = {
        "$match": {
          "$or": [
            {
              "allProfessional.phone.number": {
                "$regex": context.params.phoneNumber?.trim(),
                "$options": "si",
              },
            },
            {
              "allUser.phone.number": {
                "$regex": context.params.phoneNumber?.trim(),
                "$options": "si",
              },
            },
          ],
        },
      };
    } else {
      matchPhoneNumberQuery = {
        "$match": {
          "$or": [
            { "allProfessional.phone.number": { "$ne": null } },
            { "allUser.phone.number": { "$ne": null } },
          ],
        },
      };
    }
    if (context.params.ratingCondition) {
      switch (context.params.ratingCondition) {
        case ">=": {
          if (context.params.userType === constantUtil.PROFESSIONAL) {
            query.push({
              "$match": {
                "bookingReview.professionalReview.rating": {
                  "$gte": parseInt(context.params.reviewRating || 0),
                },
              },
            });
          } else if (context.params.userType === constantUtil.USER) {
            query.push({
              "$match": {
                "bookingReview.userReview.rating": {
                  "$gte": parseInt(context.params.reviewRating || 0),
                },
              },
            });
          } else {
            query.push({
              "$match": {
                "$or": [
                  {
                    "bookingReview.professionalReview.rating": {
                      "$gte": parseInt(context.params.reviewRating || 0),
                    },
                  },
                  {
                    "bookingReview.userReview.rating": {
                      "$gte": parseInt(context.params.reviewRating || 0),
                    },
                  },
                ],
              },
            });
          }
          break;
        }
        case "<=": {
          if (context.params.userType === constantUtil.PROFESSIONAL) {
            query.push({
              "$match": {
                "bookingReview.professionalReview.rating": {
                  "$lte": parseInt(context.params.reviewRating || 0),
                },
              },
            });
          } else if (context.params.userType === constantUtil.USER) {
            query.push({
              "$match": {
                "bookingReview.userReview.rating": {
                  "$lte": parseInt(context.params.reviewRating || 0),
                },
              },
            });
          } else {
            query.push({
              "$match": {
                "$or": [
                  {
                    "bookingReview.professionalReview.rating": {
                      "$lte": parseInt(context.params.reviewRating || 0),
                    },
                  },
                  {
                    "bookingReview.userReview.rating": {
                      "$lte": parseInt(context.params.reviewRating || 0),
                    },
                  },
                ],
              },
            });
          }
          break;
        }
        case "===": {
          if (context.params.userType === constantUtil.PROFESSIONAL) {
            query.push({
              "$match": {
                "bookingReview.professionalReview.rating": parseInt(
                  context.params.reviewRating || 0
                ),
              },
            });
          } else if (context.params.userType === constantUtil.USER) {
            query.push({
              "$match": {
                "bookingReview.userReview.rating": parseInt(
                  context.params.reviewRating || 0
                ),
              },
            });
          } else {
            query.push({
              "$match": {
                "$or": [
                  {
                    "bookingReview.professionalReview.rating": parseInt(
                      context.params.reviewRating || 0
                    ),
                  },
                  {
                    "bookingReview.userReview.rating": parseInt(
                      context.params.reviewRating || 0
                    ),
                  },
                ],
              },
            });
          }
        }
      }
    }
    //
    if (
      (context.params.city || "") !== "" &&
      context.params.city !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "category": mongoose.Types.ObjectId(context.params.city),
        },
      });
    }
    //
    query.push(
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "allProfessional",
        },
      },
      {
        "$unwind": {
          "path": "$allProfessional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "users",
          "localField": "user",
          "foreignField": "_id",
          "as": "allUser",
        },
      },
      {
        "$unwind": {
          "path": "$allUser",
          "preserveNullAndEmptyArrays": true,
        },
      },
      matchPhoneNumberQuery
    );

    query.push({
      "$facet": {
        "totalCount": [
          {
            "$count": "count",
          },
        ],
        "responseData": [
          {
            "$project": {
              "_id": 0,
              "bookingDate": "$bookingDate",
              "bookingId": "$bookingId",
              "bookingStatus": "$bookingStatus",
              "professionalName": {
                "$concat": [
                  "$allProfessional.firstName",
                  " ",
                  "$allProfessional.lastName",
                ],
              },
              "professionalNumber": {
                "$concat": [
                  "$allProfessional.phone.code",
                  " ",
                  "$allProfessional.phone.number",
                ],
              },
              "professionalReviewRating":
                "$bookingReview.professionalReview.rating",
              "professionalReviewComment":
                "$bookingReview.professionalReview.comment",
              "userName": {
                "$concat": ["$allUser.firstName", " ", "$allUser.lastName"],
              },
              "userNumber": {
                "$concat": [
                  "$allUser.phone.code",
                  " ",
                  "$allUser.phone.number",
                ],
              },
              "userReviewRating": "$bookingReview.userReview.rating",
              "userReviewComment": "$bookingReview.userReview.comment",
            },
          },
          { "$sort": { "bookingDate": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
        ],
      },
    });
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    jsonData = [];
  }
  return jsonData;
};
//#endregion Ride Review Rating
//---------------------------------
module.exports = bookingEvent;
