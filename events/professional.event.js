const moment = require("moment");
const mongoose = require("mongoose");
const each = require("sync-each");
const turf = require("@turf/turf");
const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const axios = require("axios");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const storageutil = require("../utils/storage.util");
const { convertToFloat, convertToInt } = require("../utils/common.util");
const helperUtil = require("../utils/helper.util");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const commonUtil = require("../utils/common.util");
const {
  professionalObject,
  promtionMailObject,
  professionalDocumentExpiredMailObject,
} = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const professionalEvent = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data.message,
    };
  }
};

professionalEvent.verifyToken = async function (context) {
  if (context.params.deviceId && context.params.socketId !== "") {
    await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      {
        "deviceInfo.0.deviceId": context.params.deviceId,
        "deviceInfo.0.socketId": context.params.socketId,
      }
    );
  }
  if (!!context.params?.langCode === true) {
    await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      {
        "languageCode": context.params.langCode,
      }
    );
  }
  const professionalData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "deviceInfo.accessToken": context.params.accessToken,
    })
    .lean();
  return professionalData;
};

// professionalEvent.getProfessionalByLocation_beforeChange = async function (
//   context
// ) {
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   const queryMatchCondition = {};
//   const ongoingTailBookingQuery = { "bookingInfo.ongoingTailBooking": null };
//   const ongoingBookingQuery = context.params.checkOngoingBooking
//     ? { "bookingInfo.ongoingBooking": null }
//     : {};
//   const isGenderAvailableQuery = context.params.isGenderAvailable
//     ? { "gender": constantUtil.FEMALE }
//     : {};
//   const childseatAvailableQuery = context.params.childseatAvailable
//     ? { "vehicles.childseatAvailable": true }
//     : {};
//   const isRentalSupported = context.params.isRentalSupported
//     ? { "vehicles.isRentalSupported": true }
//     : {};
//   const handicapAvailableQuery = context.params.handicapAvailable
//     ? { "vehicles.handicapAvailable": true }
//     : {};
//   const basedOnVehicleCategory =
//     context.params.vehicleCategoryId !== null
//       ? context.params.isSubCategoryAvailable === false
//         ? {
//             "$or": [
//               {
//                 "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
//               },
//               {
//                 "$and": [
//                   { "vehicles.applySubCategory": true },
//                   {
//                     "vehicles.subCategoryIds": {
//                       "$in": context.params.vehicleCategoryId.toString(),
//                     },
//                   },
//                 ],
//               },
//             ],
//             "vehicles.status": constantUtil.ACTIVE,
//           }
//         : context.params.isForceAppliedToProfessional === false
//         ? {
//             "$or": [
//               {
//                 "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
//               },
//               {
//                 "$and": [
//                   { "vehicles.applySubCategory": true },
//                   {
//                     "vehicles.subCategoryIds": {
//                       "$in": context.params.vehicleCategoryId.toString(),
//                     },
//                   },
//                 ],
//               },
//             ],
//             "vehicles.status": constantUtil.ACTIVE,
//           }
//         : {
//             "$or": [
//               {
//                 "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
//               },
//               {
//                 "vehicles.applySubCategory": true,
//                 "vehicles.subCategoryIds": {
//                   "$in": context.params.vehicleCategoryId.toString(),
//                 },
//               },
//             ],
//             "vehicles.status": constantUtil.ACTIVE,
//           }
//       : {};
//   if (context.params.serviceAreaId) {
//     queryMatchCondition["vehicles.serviceCategory"] =
//       context.params.serviceAreaId;
//   }
//   const professionalData = await this.adapter.model
//     .find({
//       "status": constantUtil.ACTIVE,
//       "onlineStatus": true,
//       ...isGenderAvailableQuery,
//       ...childseatAvailableQuery,
//       ...handicapAvailableQuery,
//       ...isRentalSupported,
//       ...ongoingBookingQuery,
//       ...ongoingTailBookingQuery,
//       ...basedOnVehicleCategory,
//       "location": {
//         "$nearSphere": {
//           "$geometry": {
//             "type": "Point",
//             "coordinates": [
//               parseFloat(context.params.lng),
//               parseFloat(context.params.lat),
//             ],
//           },
//           "$minDistance": 0,
//           "$maxDistance": context.params.radius,
//         },
//       },
//       ...serviceAreaIdQuery,
//     })
//     .lean()
//     .populate("bookingInfo.ongoingBooking")
//     .populate("vehicles.serviceCategory", {
//       "_id": 1, // serviceAreaId
//       "distanceType": 1,
//     })
//     .sort({ "$near": 1 });
//   // //---------------New Start -------------------
//   // const query = [];
//   // query.push(
//   //   {
//   //     "$geoNear": {
//   //       "includeLocs": "location",
//   //       "distanceField": "distance",
//   //       "near": {
//   //         "type": "Point",
//   //         "coordinates": [
//   //           parseFloat(context.params.lng),
//   //           parseFloat(context.params.lat),
//   //         ],
//   //       },
//   //       "$minDistance": 0,
//   //       "maxDistance": context.params.radius,
//   //       "spherical": true,
//   //     },
//   //   },
//   //   {
//   //     "$match": {
//   //       "status": constantUtil.ACTIVE,
//   //       "onlineStatus": true,
//   //       ...isGenderAvailableQuery,
//   //       ...childseatAvailableQuery,
//   //       ...handicapAvailableQuery,
//   //       ...isRentalSupported,
//   //       ...ongoingBookingQuery,
//   //       ...ongoingTailBookingQuery,
//   //       ...basedOnVehicleCategory,
//   //       // "location": {
//   //       //   "$nearSphere": {
//   //       //     "$geometry": {
//   //       //       "type": "Point",
//   //       //       "coordinates": [
//   //       //         parseFloat(context.params.lng),
//   //       //         parseFloat(context.params.lat),
//   //       //       ],
//   //       //     },
//   //       //     "$minDistance": 0,
//   //       //     "$maxDistance": context.params.radius,
//   //       //   },
//   //       // },
//   //     },
//   //   }
//   // );
//   // query.push(
//   //   {
//   //     "$lookup": {
//   //       "from": "bookings",
//   //       "localField": "bookingInfo.ongoingBooking",
//   //       "foreignField": "_id",
//   //       "as": "bookingData",
//   //     },
//   //   },
//   //   {
//   //     "$unwind": {
//   //       "path": "$bookingData",
//   //       "preserveNullAndEmptyArrays": true,
//   //     },
//   //   },
//   //   {
//   //     "$lookup": {
//   //       "from": "categories",
//   //       "localField": "vehicles.serviceCategory",
//   //       "foreignField": "_id",
//   //       "as": "categoryData",
//   //     },
//   //   },
//   //   {
//   //     "$unwind": {
//   //       "path": "$categoryData",
//   //       "preserveNullAndEmptyArrays": true,
//   //     },
//   //   },
//   //   {
//   //     "$project": {
//   //       "_id": 1,
//   //       "vehicles.serviceCategory": 1,
//   //       //"bookingInfo": 1,
//   //        "bookingInfo.ongoingBooking.destination": "$bookingData.destination",
//   //       "isPriorityStatus": 1,
//   //       "isPriorityLocation": 1,
//   //       "distanceType": "$categoryData.distanceType",
//   //       "serviceAreaId": "$categoryData._id",
//   //     },
//   //   }
//   // );
//   // const testQuery = commonUtil.getDbQueryString(query);
//   // const professionalData = await this.adapter.model.aggregate(query);
//   // //---------------New End -------------------
//   const validProfessional = [];
//   await each(professionalData, async (professional, next) => {
//     const distanceType = (
//       professional?.vehicles[0]?.serviceCategory?.distanceType ||
//       constantUtil.KM
//     ).toUpperCase();
//     const distanceTypeUnit =
//       distanceType === constantUtil.KM ? "kilometers" : "miles";
//     const distanceTypeUnitValue =
//       distanceType === constantUtil.KM
//         ? constantUtil.KM_VALUE
//         : constantUtil.MILES_VALUE;
//     professional.distanceType = distanceType;
//     professional.distanceTypeUnitValue = distanceTypeUnitValue;
//     if (
//       context.params.checkLastPriority === true &&
//       generalSettings.data.lastPriorityStatus === true &&
//       professional.isPriorityStatus !== null &&
//       professional.isPriorityStatus === true
//     ) {
//       const from = turf.point([
//         parseFloat(context.params.droplat),
//         parseFloat(context.params.droplng),
//       ]);
//       const to = turf.point([
//         parseFloat(professional.isPriorityLocation.lat),
//         parseFloat(professional.isPriorityLocation.lng),
//       ]);
//       let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//       distance = parseInt(distance * distanceTypeUnitValue);
//       if (distance < context.params.radius) {
//         validProfessional.push(professional);
//       }
//     } else {
//       if (
//         professional.bookingInfo.ongoingBooking !== null &&
//         professional.bookingInfo.ongoingBooking.bookingStatus ===
//           constantUtil.STARTED
//       ) {
//         if (context.params.forTailRide === true) {
//           const checkJson = professional.bookingInfo.ongoingBooking;
//           const from = turf.point([
//             parseFloat(context.params.lat),
//             parseFloat(context.params.lng),
//           ]);
//           const to = turf.point([
//             parseFloat(checkJson.destination.lat),
//             parseFloat(checkJson.destination.lng),
//           ]);
//           let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//           distance = parseInt(distance * distanceTypeUnitValue);
//           if (distance < context.params.radius) {
//             validProfessional.push(professional);
//           }
//         }
//       } else if (professional.bookingInfo.ongoingBooking === null) {
//         validProfessional.push(professional);
//       }
//     }
//     next();
//   });

//   return validProfessional;
// };

professionalEvent.getProfessionalByLocation = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let isEnableShareRideProfessional = false,
    queryMatchCondition = {},
    basedOnVehicleCategory = {
      "defaultVehicle": true,
      "status": constantUtil.ACTIVE,
    },
    basedOnVehicleSubCategory = {
      "defaultVehicle": true,
      "status": constantUtil.ACTIVE,
    };
  if (context.params.professionalOnlineStatus) {
    queryMatchCondition["onlineStatus"] =
      context.params.professionalOnlineStatus;
  }
  queryMatchCondition["bookingInfo.ongoingTailBooking"] = null;
  if (context.params.isShareRide !== null) {
    if (context.params.isShareRide) {
      isEnableShareRideProfessional = true;
      queryMatchCondition["vehicles.isEnableShareRide"] = true;
      basedOnVehicleCategory["isEnableShareRide"] = true;
    } else if (context.params.checkOngoingBooking) {
      queryMatchCondition["bookingInfo.ongoingBooking"] = null;
    }
  } else {
    isEnableShareRideProfessional = true;
  }
  if (context.params.paymentOption) {
    queryMatchCondition["paymentMode"] = {
      "$in": [context.params.paymentOption],
    };
  }
  if (context.params.languageCode) {
    queryMatchCondition["knownLanguages"] = {
      "$in": [context.params.languageCode],
    };
  }
  if (context.params.isGenderAvailable) {
    queryMatchCondition["gender"] = constantUtil.FEMALE;
  }
  //------------ Vehicle Sub-Category Start ------------
  if (
    (context.params.vehicleCategoryId &&
      context.params.isSubCategoryAvailable === false) ||
    (context.params.vehicleCategoryId &&
      context.params.isForceAppliedToProfessional === false)
  ) {
    basedOnVehicleSubCategory = {
      "defaultVehicle": true,
      "status": constantUtil.ACTIVE,
      "$or": [
        {
          "vehicleCategoryId": context.params.vehicleCategoryId,
        },
        {
          "$and": [
            { "applySubCategory": true },
            {
              "subCategoryIds": {
                "$in": [context.params.vehicleCategoryId.toString()],
              },
            },
          ],
        },
      ],
    };
  } else if (
    (context.params.vehicleCategoryId &&
      context.params.isSubCategoryAvailable) ||
    (context.params.vehicleCategoryId &&
      context.params.isForceAppliedToProfessional)
  ) {
    basedOnVehicleSubCategory = {
      "defaultVehicle": true,
      "status": constantUtil.ACTIVE,
      "$or": [
        {
          "vehicleCategoryId": context.params.vehicleCategoryId,
        },
        {
          "applySubCategory": true,
          "subCategoryIds": {
            "$in": [context.params.vehicleCategoryId.toString()],
          },
        },
      ],
    };
  } else if (context.params.vehicleCategoryId) {
    basedOnVehicleSubCategory = {
      "defaultVehicle": true,
      "status": constantUtil.ACTIVE,
      "vehicleCategoryId": context.params.vehicleCategoryId,
    };
  }
  //
  if (context.params.childseatAvailable) {
    queryMatchCondition["vehicles.childseatAvailable"] = true;
    basedOnVehicleSubCategory["childseatAvailable"] = true;
  }
  if (context.params.isRentalSupported) {
    queryMatchCondition["vehicles.isRentalSupported"] = true;
    basedOnVehicleSubCategory["isRentalSupported"] = true;
  }
  if (context.params.handicapAvailable) {
    queryMatchCondition["vehicles.handicapAvailable"] = true;
    basedOnVehicleSubCategory["handicapAvailable"] = true;
  }
  if (context.params.isPetAllowed) {
    queryMatchCondition["vehicles.isPetAllowed"] = true;
    basedOnVehicleSubCategory["isPetAllowed"] = true;
  }
  //------------ Vehicle Sub-Category End ------------
  //-----------------
  // const basedOnVehicleCategory =
  //   context.params.vehicleCategoryId !== null
  //     ? context.params.isSubCategoryAvailable === false
  //       ? {
  //           "$or": [
  //             {
  //               "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "$and": [
  //                 { "vehicles.applySubCategory": true },
  //                 {
  //                   "vehicles.subCategoryIds": {
  //                     "$in": context.params.vehicleCategoryId.toString(),
  //                   },
  //                 },
  //               ],
  //             },
  //           ],
  //           "vehicles.status": constantUtil.ACTIVE,
  //         }
  //       : context.params.isForceAppliedToProfessional === false
  //       ? {
  //           "$or": [
  //             {
  //               "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "$and": [
  //                 { "vehicles.applySubCategory": true },
  //                 {
  //                   "vehicles.subCategoryIds": {
  //                     "$in": context.params.vehicleCategoryId.toString(),
  //                   },
  //                 },
  //               ],
  //             },
  //           ],
  //           "vehicles.status": constantUtil.ACTIVE,
  //         }
  //       : {
  //           "$or": [
  //             {
  //               "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "vehicles.applySubCategory": true,
  //               "vehicles.subCategoryIds": {
  //                 "$in": [context.params.vehicleCategoryId.toString()],
  //               },
  //             },
  //           ],
  //           "vehicles.status": constantUtil.ACTIVE,
  //         }
  //     : {};
  // // if (context.params.serviceAreaId) {
  // //   queryMatchCondition["vehicles.serviceCategory"] =
  // //     context.params.serviceAreaId;
  // // }
  // const professionalData = await this.adapter.model
  //   .find({
  //     "status": constantUtil.ACTIVE,
  //     // "onlineStatus": true,
  //     ...queryMatchCondition,
  //     // ...isGenderAvailableQuery,
  //     // ...childseatAvailableQuery,
  //     // ...handicapAvailableQuery,
  //     // ...isRentalSupported,
  //     // ...ongoingBookingQuery,
  //     // ...ongoingTailBookingQuery,
  //     ...basedOnVehicleCategory,

  //     "location": {
  //       // "$nearSphere": {
  //       "$near": {
  //         "$geometry": {
  //           "type": "Point",
  //           "coordinates": [
  //             parseFloat(context.params.lng),
  //             parseFloat(context.params.lat),
  //           ],
  //         },
  //         "$minDistance": 0,
  //         "$maxDistance": context.params.radius,
  //       },
  //     },
  //   })
  //   .lean()
  //   .populate("bookingInfo.ongoingBooking")
  //   .populate("vehicles.serviceCategory", {
  //     "_id": 1, // serviceAreaId
  //     "distanceType": 1,
  //   });
  // // .sort({ "$near": 1 });
  // //-----------------------------
  // basedOnVehicleCategory =
  //   context.params.vehicleCategoryId !== null
  //     ? context.params.isSubCategoryAvailable === false
  //       ? {
  //           "defaultVehicle": true,
  //           "status": constantUtil.ACTIVE,
  //           "$or": [
  //             {
  //               "vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "$and": [
  //                 { "applySubCategory": true },
  //                 {
  //                   "subCategoryIds": {
  //                     "$in": [context.params.vehicleCategoryId.toString()],
  //                   },
  //                 },
  //               ],
  //             },
  //           ],
  //         }
  //       : context.params.isForceAppliedToProfessional === false
  //       ? {
  //           "defaultVehicle": true,
  //           "status": constantUtil.ACTIVE,
  //           "$or": [
  //             {
  //               "vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "$and": [
  //                 { "applySubCategory": true },
  //                 {
  //                   "subCategoryIds": {
  //                     "$in": [context.params.vehicleCategoryId.toString()],
  //                   },
  //                 },
  //               ],
  //             },
  //           ],
  //         }
  //       : {
  //           "defaultVehicle": true,
  //           "status": constantUtil.ACTIVE,
  //           "$or": [
  //             {
  //               "vehicleCategoryId": context.params.vehicleCategoryId,
  //             },
  //             {
  //               "applySubCategory": true,
  //               "subCategoryIds": {
  //                 "$in": [context.params.vehicleCategoryId.toString()],
  //               },
  //             },
  //           ],
  //         }
  //     : { "defaultVehicle": true };
  let fromLastThreeMin = new Date();
  fromLastThreeMin.setMinutes(fromLastThreeMin.getMinutes() - 3);
  //
  let professionalData = await this.adapter.model
    .find(
      {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "status": constantUtil.ACTIVE,
        "locationUpdatedTime": { "$gte": fromLastThreeMin },
        // "onlineStatus": true,
        ...queryMatchCondition,
        // ...isGenderAvailableQuery,
        // ...childseatAvailableQuery,
        // ...handicapAvailableQuery,
        // ...isRentalSupported,
        // ...ongoingBookingQuery,
        // ...ongoingTailBookingQuery,
        // ...basedOnVehicleCategory,

        "location": {
          // "$nearSphere": {
          "$near": {
            "$geometry": {
              "type": "Point",
              "coordinates": [
                parseFloat(context.params.lng),
                parseFloat(context.params.lat),
              ],
            },
            "$minDistance": 0,
            "$maxDistance": context.params.radius,
          },
        },
      },
      {
        "firstName": 1,
        "lastName": 1,
        "phone": 1,
        "avatar": 1,
        "gender": 1,
        "review": 1,
        "wallet": 1,
        "deviceInfo": 1,
        "distanceType": 1,
        "distanceTypeUnitValue": 1,
        "paymentMode": 1,
        "isTailRideBooking": 1,
        "isPriorityStatus": 1,
        "isPriorityLocation": 1,
        "bookingInfo": 1,
        "location": 1,
        "serviceAreaId": 1,
        "serviceCategory": 1,
        "currentBearing": 1,
        "vehicles": {
          "$elemMatch": {
            // "defaultVehicle": true,
            // "status": "ACTIVE",
            // "$or": [
            //   { "vehicleCategoryId": ObjectId("6181cb4770988dc31c820b9c") },
            //   {
            //     "$and": [
            //       { "applySubCategory": false },
            //       {
            //         "subCategoryIds": {
            //           "$in": ["64913e30504fd9c8b619e1a4"],
            //         },
            //       },
            //     ],
            //   },
            // ],
            ...basedOnVehicleCategory,
            ...basedOnVehicleSubCategory, // Need to Re-Work
          },
        },
        // "vehicles": 1,
      }
    )
    .collation({ "locale": "en", "caseLevel": true })
    .lean()
    .limit(22) // Must will not change Limit size
    .populate("bookingInfo.ongoingBooking")
    .populate("vehicles.serviceCategory", {
      "_id": 1, // serviceAreaId
      "distanceType": 1,
    });
  //-----------------------------------
  // const config = {
  //   "method": "post",
  //   "url":
  //     "https://api.geoapify.com/v1/routematrix?apiKey=d0b7f43924914c85a27022d8d00c85da",
  //   "headers": {
  //     "Content-Type": "application/json",
  //   },
  //   "data": {
  //     "mode": "drive",
  //     "sources": [
  //       {
  //         "location": [
  //           parseFloat(context.params.lng),
  //           parseFloat(context.params.lat),
  //         ],
  //       },
  //     ],
  //     "targets": [
  //       { "location": [77.214655, 8.30208] },
  //       { "location": [77.214912, 8.307855] },
  //       { "location": [77.215727, 8.3045] },
  //     ],
  //   },
  // };
  // let allList = await axiosConfig(config);
  // allList.sources_to_targets?.[0].sort((a, b) => {
  //   a.time - b.time;
  //   console.log(JSON.stringify(a));
  //   console.log(JSON.stringify(b));
  // });
  //-----------------------------------
  // #region Shortest Travel Time Algorithm
  // if (generalSettings[0].data.bookingAlgorithm === constantUtil.COMPETITIVE) {
  if (
    generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST &&
    context.params.actionType === constantUtil.BOOKING
  ) {
    try {
      let destinationsLocationCoordinates =
          context.params.lat + "," + context.params.lng, // User Location
        originsLocationCoordinates = ""; // Driver Location
      await professionalData.map((professional) => {
        originsLocationCoordinates +=
          professional.location.coordinates[1] +
          "," +
          professional.location.coordinates[0] +
          "|";
      });
      const config = {
        "method": "get",
        "url":
          // "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=8.314819,%2077.210191|8.303396,77.219633|&origins=8.305222,77.225598&key=AIzaSyBDrFjDe5kTxbeNGqTlqapaTK4JjDL3qgo",
          "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=" +
          destinationsLocationCoordinates +
          "&origins=" +
          originsLocationCoordinates +
          "&key=" +
          generalSettings.data.mapApi.api, //Key Need to Change Dynamically
        "headers": {
          "Content-Type": "application/json",
        },
      };
      let allList = await axiosConfig(config);
      //-----------------------------------
      // let arrayPosition = null,
      //   arrayValue = null,
      //   selectedDestinationAddresses;
      // // allList.rows?.[0].elements.sort((a, b) => {
      // //   a.duration.value - b.duration.value;
      // //   console.log(JSON.stringify(a));
      // //   console.log(JSON.stringify(b));
      // // });
      // if (allList.rows?.[0]?.elements?.length > 0) {
      //   for (let i = 0; i < allList.rows?.[0]?.elements.length; i++) {
      //     if (allList.rows?.[0]?.elements[i].duration.value < arrayValue) {
      //       arrayPosition = i;
      //       arrayValue = allList.rows?.[0]?.elements[i].duration.value;
      //     } else if (arrayValue === null) {
      //       arrayPosition = i;
      //       arrayValue = allList.rows?.[0]?.elements[i].duration.value;
      //     }
      //   }
      //   destination_addresses = allList.destination_addresses[arrayPosition];
      // }
      //-----------------------------------
      if (allList.origin_addresses.length === professionalData.length) {
        for (let i = 0; i < allList.origin_addresses.length; i++) {
          professionalData[i]["duration"] =
            allList.rows?.[i]?.elements[0].duration.value;
          professionalData[i]["distance"] =
            allList.rows?.[i]?.elements[0].distance.value;
        }

        professionalData = await professionalData.sort((a, b) => {
          return a.duration - b.duration;
        });

        console.log("============================");
        console.log(JSON.stringify(professionalData));
        console.log("============================");
      }
    } catch (e) {
      constx = e.message;
    }
  }
  // #endregion Shortest Travel Time Algorithm
  //-----------------------------------
  const test = "";
  //-----------------------------
  // //---------------New Start -------------------
  // const query = [];
  // query.push(
  //   {
  //     "$geoNear": {
  //       "includeLocs": "location",
  //       "distanceField": "distance",
  //       "near": {
  //         "type": "Point",
  //         "coordinates": [
  //           parseFloat(context.params.lng),
  //           parseFloat(context.params.lat),
  //         ],
  //       },
  //       "$minDistance": 0,
  //       "maxDistance": context.params.radius,
  //       "spherical": true,
  //     },
  //   },
  //   {
  //     "$match": {
  //       "status": constantUtil.ACTIVE,
  //       "onlineStatus": true,
  //       ...isGenderAvailableQuery,
  //       ...childseatAvailableQuery,
  //       ...handicapAvailableQuery,
  //       ...isRentalSupported,
  //       ...ongoingBookingQuery,
  //       ...ongoingTailBookingQuery,
  //       ...basedOnVehicleCategory,
  //       // "location": {
  //       //   "$nearSphere": {
  //       //     "$geometry": {
  //       //       "type": "Point",
  //       //       "coordinates": [
  //       //         parseFloat(context.params.lng),
  //       //         parseFloat(context.params.lat),
  //       //       ],
  //       //     },
  //       //     "$minDistance": 0,
  //       //     "$maxDistance": context.params.radius,
  //       //   },
  //       // },
  //     },
  //   }
  // );
  // query.push(
  //   {
  //     "$lookup": {
  //       "from": "bookings",
  //       "localField": "bookingInfo.ongoingBooking",
  //       "foreignField": "_id",
  //       "as": "bookingData",
  //     },
  //   },
  //   {
  //     "$unwind": {
  //       "path": "$bookingData",
  //       "preserveNullAndEmptyArrays": true,
  //     },
  //   },
  //   {
  //     "$lookup": {
  //       "from": "categories",
  //       "localField": "vehicles.serviceCategory",
  //       "foreignField": "_id",
  //       "as": "categoryData",
  //     },
  //   },
  //   {
  //     "$unwind": {
  //       "path": "$categoryData",
  //       "preserveNullAndEmptyArrays": true,
  //     },
  //   },
  //   {
  //     "$project": {
  //       "_id": 1,
  //       "vehicles.serviceCategory": 1,
  //       //"bookingInfo": 1,
  //        "bookingInfo.ongoingBooking.destination": "$bookingData.destination",
  //       "isPriorityStatus": 1,
  //       "isPriorityLocation": 1,
  //       "distanceType": "$categoryData.distanceType",
  //       "serviceAreaId": "$categoryData._id",
  //     },
  //   }
  // );
  // const testQuery = commonUtil.getDbQueryString(query);
  // const professionalData = await this.adapter.model.aggregate(query);
  // //---------------New End -------------------
  const validProfessional = [];
  await each(professionalData, async (professional, next) => {
    if ((professional?.vehicles?.length || 0) > 0) {
      const distanceType = (
        professional?.vehicles?.[0]?.serviceCategory?.distanceType ||
        constantUtil.KM
      ).toUpperCase();
      const distanceTypeUnit =
        distanceType === constantUtil.KM ? "kilometers" : "miles";
      const distanceTypeUnitValue =
        distanceType === constantUtil.KM
          ? constantUtil.KM_VALUE
          : constantUtil.MILES_VALUE;
      professional.distanceType = distanceType;
      professional.distanceTypeUnitValue = distanceTypeUnitValue;
      professional["isTailRideBooking"] = false;
      if (
        context.params.checkLastPriority === true &&
        generalSettings.data.lastPriorityStatus === true &&
        // professional.isPriorityStatus !== null &&
        professional?.isPriorityStatus === true
      ) {
        const from = turf.point([
          parseFloat(context.params.droplat),
          parseFloat(context.params.droplng),
        ]);
        const to = turf.point([
          parseFloat(professional.isPriorityLocation.lat),
          parseFloat(professional.isPriorityLocation.lng),
        ]);
        let distance = turf.distance(from, to, { "units": distanceTypeUnit });
        distance = parseInt(distance * distanceTypeUnitValue);
        // if (distance < context.params.radius) {
        if (distance <= distanceTypeUnitValue) {
          // professional["serviceCategory"] =
          //   professional?.bookingInfo?.ongoingBooking?.serviceCategory || null;
          // professional["ongoingBooking"] =
          //   professional?.bookingInfo?.ongoingBooking?._id || null;
          validProfessional.push(professional);
        }
      } else {
        // if (professional.vehicles[0]?.isEnableShareRide === true) {
        if (
          (professional.vehicles?.[0]?.isEnableShareRide ===
            isEnableShareRideProfessional &&
            professional?.bookingInfo?.ongoingBooking?.serviceCategory ===
              constantUtil.CONST_SHARERIDE.toLocaleLowerCase()) ||
          (professional.vehicles?.[0]?.isEnableShareRide ===
            isEnableShareRideProfessional &&
            professional.bookingInfo.ongoingBooking === null)
        ) {
          // professional["serviceCategory"] =
          //   professional?.bookingInfo?.ongoingBooking?.serviceCategory || null;
          // professional["ongoingBooking"] =
          //   professional?.bookingInfo?.ongoingBooking?._id || null;
          validProfessional.push(professional);
        } else if (
          professional.bookingInfo.ongoingBooking !== null &&
          professional.bookingInfo.ongoingBooking.bookingStatus ===
            constantUtil.STARTED
        ) {
          if (context.params.forTailRide === true) {
            const checkJson = professional.bookingInfo.ongoingBooking;
            const from = turf.point([
              parseFloat(context.params.lat),
              parseFloat(context.params.lng),
            ]);
            const to = turf.point([
              parseFloat(checkJson.destination.lat),
              parseFloat(checkJson.destination.lng),
            ]);
            let distance = turf.distance(from, to, {
              "units": distanceTypeUnit,
            });
            distance = parseInt(distance * distanceTypeUnitValue);
            // if (distance < context.params.radius) {
            if (distance <= distanceTypeUnitValue) {
              // professional["bookingInfo"]["ongoingBooking"] = null; // Temporary Need to Remove this line
              // professional["serviceCategory"] =
              //   professional?.bookingInfo?.ongoingBooking?.serviceCategory ||
              //   null;
              // professional["ongoingBooking"] =
              //   professional?.bookingInfo?.ongoingBooking?._id || null;
              professional["isTailRideBooking"] = true;
              validProfessional.push(professional);
            }
          }
        } else if (professional.bookingInfo.ongoingBooking === null) {
          // professional["serviceCategory"] =
          //   professional?.bookingInfo?.ongoingBooking?.serviceCategory || null;
          // professional["ongoingBooking"] =
          //   professional?.bookingInfo?.ongoingBooking?._id || null;
          validProfessional.push(professional);
        }
      }
    }
    next();
  });
  return validProfessional;
};

// // ! bellow correct follow of getting professionals by location
// professionalEvent.getProfessionalByLocation = async function (context) {
//   const queryMatchCondition = {
//     "status": constantUtil.ACTIVE,
//     "onlineStatus": true,
//     "bookingInfo.ongoingTailBooking": null,
//     "vehicles.status": constantUtil.ACTIVE,
//     // "location": {
//     //   "$nearSphere": {
//     //     "$geometry": {
//     //       "type": "Point",
//     //       "coordinates": [
//     //         parseFloat(context.params.lng),
//     //         parseFloat(context.params.lat),
//     //       ],
//     //     },
//     //     "$minDistance": 0,
//     //     "$maxDistance": context.params.radius,
//     //   },
//     // },
//   };

//   if (context.params.isGenderAvailable) {
//     queryMatchCondition["gender"] = constantUtil.FEMALE;
//   }
//   if (context.params?.isRestrictMaleBookingToFemaleProfessional) {
//     queryMatchCondition["gender"] = constantUtil.MALE;
//   }
//   if (context.params.childseatAvailable) {
//     queryMatchCondition["vehicles.childseatAvailable"] = true;
//   }
//   if (context.params.handicapAvailable) {
//     queryMatchCondition["vehicles.handicapAvailable"] = true;
//   }
//   if (context.params.checkOngoingBooking) {
//     queryMatchCondition["bookingInfo.ongoingBooking"] = null;
//   }
//   if (context.params?.vehicleCategoryId) {
//     queryMatchCondition["$or"] = [
//       {
//         "vehicles.vehicleCategoryId": context.params.vehicleCategoryId,
//       },
//     ];
//     //  $ Below condition means if requested vehicleCategory is any another vehicle subcategory

//     if (context.params.isSubCategoryAvailable) {
//       // $ Is subcategory applied to compulsory in this vehicle category so should not care about driver sub category preference
//       if (context.params.isForceAppliedToProfessional) {
//         queryMatchCondition["$or"].push({
//           "vehicles.subCategoryIds": {
//             "$in": context.params.vehicleCategoryId.toString(),
//           },
//         });
//       }
//       // $ if driver belongs this vehicle category, not sub category compulsory so which is choice of driver
//       if (!context.params.isForceAppliedToProfessional) {
//         queryMatchCondition["$or"].push({
//           "$and": [
//             { "vehicles.applySubCategory": true },
//             {
//               "vehicles.subCategoryIds": {
//                 "$in": context.params.vehicleCategoryId.toString(),
//               },
//             },
//           ],
//         });
//       }
//     }
//   }
//   // const professionalData = await this.adapter.model
//   //   .find(query)
//   //   .populate("bookingInfo.ongoingBooking");

//   //---------------New Start -------------------
//   const query = [];
//   query.push(
//     {
//       "$geoNear": {
//         "includeLocs": "location",
//         "distanceField": "distance",
//         "near": {
//           "type": "Point",
//           "coordinates": [
//             parseFloat(context.params.lng),
//             parseFloat(context.params.lat),
//           ],
//         },
//         "$minDistance": 0,
//         "maxDistance": context.params.radius,
//         "spherical": true,
//       },
//     },
//     {
//       "$match": {
//         ...queryMatchCondition,
//         // "status": constantUtil.ACTIVE,
//         // "onlineStatus": true,
//         // ...isGenderAvailableQuery,
//         // ...childseatAvailableQuery,
//         // ...handicapAvailableQuery,
//         // ...isRentalSupported,
//         // ...ongoingBookingQuery,
//         // ...ongoingTailBookingQuery,
//         // ...basedOnVehicleCategory,
//         // //-----------------
//         // "location": {
//         //   "$nearSphere": {
//         //     "$geometry": {
//         //       "type": "Point",
//         //       "coordinates": [
//         //         parseFloat(context.params.lng),
//         //         parseFloat(context.params.lat),
//         //       ],
//         //     },
//         //     "$minDistance": 0,
//         //     "$maxDistance": context.params.radius,
//         //   },
//         // },
//       },
//     }
//   );
//   query.push(
//     {
//       "$lookup": {
//         "from": "bookings",
//         "localField": "bookingInfo.ongoingBooking",
//         "foreignField": "_id",
//         "as": "bookingData",
//       },
//     },
//     {
//       "$unwind": {
//         "path": "$bookingData",
//         "preserveNullAndEmptyArrays": true,
//       },
//     },
//     {
//       "$lookup": {
//         "from": "categories",
//         "localField": "vehicles.serviceCategory",
//         "foreignField": "_id",
//         "as": "categoryData",
//       },
//     },
//     {
//       "$unwind": {
//         "path": "$categoryData",
//         "preserveNullAndEmptyArrays": true,
//       },
//     },
//     {
//       "$project": {
//         "_id": 1,
//         // "vehicles.serviceCategory": 1,
//          "phone":1,
//          "wallet":1,
//         "vehicles": 1,
//         "bookingInfo": 1,
//         // "bookingInfo.ongoingBooking.destination": "$bookingData.destination",
//         "isPriorityStatus": 1,
//         "isPriorityLocation": 1,
//         "distanceType": "$categoryData.distanceType",
//         "serviceAreaId": "$categoryData._id",
//       },
//     }
//   );
//   const professionalData = await this.adapter.model.aggregate(query);
//   //---------------New End -------------------

//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   // return professionalData.filter((eachProfessional) => {
//   //   if (!!eachProfessional.bookingInfo?.ongoingBooking?.destination === false) {
//   //     return true;
//   //   }
//   //   const distanceType = (
//   //     eachProfessional?.distanceType || constantUtil.KM
//   //   ).toUpperCase();
//   //   const distanceTypeUnit =
//   //     distanceType === constantUtil.KM ? "kilometers" : "miles";
//   //   const distanceTypeUnitValue =
//   //     distanceType === constantUtil.KM
//   //       ? constantUtil.KM_VALUE
//   //       : constantUtil.MILES_VALUE;
//   //   if (
//   //     generalSettings?.data?.lastPriorityStatus &&
//   //     context.params.checkLastPriority
//   //   ) {
//   //     const from = turf.point([
//   //       parseFloat(context.params.droplat),
//   //       parseFloat(context.params.droplng),
//   //     ]);
//   //     const to = turf.point([
//   //       parseFloat(eachProfessional.isPriorityLocation.lat),
//   //       parseFloat(eachProfessional.isPriorityLocation.lng),
//   //     ]);
//   //     let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//   //     distance = parseInt(distance * distanceTypeUnitValue);
//   //     if (distance < context.params.radius) {
//   //       return true;
//   //     }
//   //   }
//   //   if (context.params?.forTailRide) {
//   //     const checkJson = eachProfessional.bookingInfo.ongoingBooking;
//   //     const from = turf.point([
//   //       parseFloat(context.params.lat),
//   //       parseFloat(context.params.lng),
//   //     ]);
//   //     const to = turf.point([
//   //       parseFloat(checkJson.destination.lat),
//   //       parseFloat(checkJson.destination.lng),
//   //     ]);
//   //     let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//   //     distance = parseInt(distance * distanceTypeUnitValue);
//   //     if (distance < context.params.radius) {
//   //       return true;
//   //     }
//   //   }
//   // });
//   // //---------------New End -------------------
//   const validProfessional = [];
//   await each(professionalData, async (professional, next) => {
//     const distanceType = (
//       professional?.vehicles[0]?.serviceCategory?.distanceType ||
//       constantUtil.KM
//     ).toUpperCase();
//     const distanceTypeUnit =
//       distanceType === constantUtil.KM ? "kilometers" : "miles";
//     const distanceTypeUnitValue =
//       distanceType === constantUtil.KM
//         ? constantUtil.KM_VALUE
//         : constantUtil.MILES_VALUE;
//     professional.distanceType = distanceType;
//     professional.distanceTypeUnitValue = distanceTypeUnitValue;
//     if (
//       context.params.checkLastPriority === true &&
//       generalSettings.data.lastPriorityStatus === true &&
//       professional.isPriorityStatus !== null &&
//       professional.isPriorityStatus === true
//     ) {
//       const from = turf.point([
//         parseFloat(context.params.droplat),
//         parseFloat(context.params.droplng),
//       ]);
//       const to = turf.point([
//         parseFloat(professional.isPriorityLocation.lat),
//         parseFloat(professional.isPriorityLocation.lng),
//       ]);
//       let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//       distance = parseInt(distance * distanceTypeUnitValue);
//       if (distance < context.params.radius) {
//         validProfessional.push(professional);
//       }
//     } else {
//       if (
//         professional.bookingInfo.ongoingBooking !== null &&
//         professional.bookingInfo.ongoingBooking.bookingStatus ===
//           constantUtil.STARTED
//       ) {
//         if (context.params.forTailRide === true) {
//           const checkJson = professional.bookingInfo.ongoingBooking;
//           const from = turf.point([
//             parseFloat(context.params.lat),
//             parseFloat(context.params.lng),
//           ]);
//           const to = turf.point([
//             parseFloat(checkJson.destination.lat),
//             parseFloat(checkJson.destination.lng),
//           ]);
//           let distance = turf.distance(from, to, { "units": distanceTypeUnit });
//           distance = parseInt(distance * distanceTypeUnitValue);
//           if (distance < context.params.radius) {
//             validProfessional.push(professional);
//           }
//         }
//       } else if (professional.bookingInfo.ongoingBooking === null) {
//         validProfessional.push(professional);
//       }
//     }
//     next();
//   });

//   return validProfessional;
// };

professionalEvent.getUnregisteredProfessionalList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {};
  // query["clientId"] = mongoose.Types.ObjectId(context.params.clientId);
  query["status"] =
    context.params.filter && context.params.filter != ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.INCOMPLETE, constantUtil.ATTENDED] };
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["phone.code", "phone.number"],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    // "sort": ["notes", "createdAt", "-updatedAt"],
    "sort": "-_id",
    "search": search,
    "searchFields": ["phone.code", "phone.number"],
  });
  responseJson["count"] = count;
  responseJson["response"] = jsonData.map((professional) => ({
    "_id": professional._id,
    "data": {
      "phone": professional.phone,
      "status": professional.status,
      "notes": professional.notes,
      "createdAt": professional.createdAt,
      "updatedAt": professional.updatedAt,
    },
  }));
  return { "code": 1, "data": responseJson, "message": "" };
};
professionalEvent.closeUnregisteredProfessional = async function (context) {
  if (context.params.id) {
    const responseJson = await this.adapter.model.remove({
      "_id": context.params.id,
      "status": constantUtil.ATTENDED,
    });
    if (!responseJson) {
      return { "code": 0, "data": {}, "message": "ERROR IN CLOSING" };
    } else {
      return { "code": 1, "data": {}, "message": "CLOSED SUCCESSFULLY" };
    }
  }
};
professionalEvent.addUnregisteredProfessionalNotes = async function (context) {
  if (context.params.id) {
    const responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "notes": context.params.notes,
      }
    );
    if (!responseJson)
      return { "code": 0, "data": {}, "message": "ERROR IN NOTES ADDING" };
    return { "code": 1, "data": {}, "message": "NOTES ADDED SUCCESSFULLY" };
  }
};
professionalEvent.addUnregisteredProfessionalStatusChange = async function (
  context
) {
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": [context.params.ids] },
      "status": constantUtil.INCOMPLETE,
    },
    {
      "status": context.params.status,
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
professionalEvent.getProfessionalList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {};
  // query["clientId"] = mongoose.Types.ObjectId(context.params.clientId);
  query["status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };
  if (context.params.vehicleStatus) {
    query["vehicles.status"] =
      (context.params?.vehicleStatus || "") !== "ALL"
        ? { "$eq": context.params.vehicleStatus }
        : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };
  }

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  // const count = await this.adapter.count({
  //   "query": query,
  //   "search": search,
  //   "searchFields": [
  //     "firstName",
  //     "lastName",
  //     "email",
  //     "phone.code",
  //     "phone.number",
  //     "vehicles.plateNumber",
  //   ],
  // }); //  Old
  // const jsonData = await this.adapter.find({
  //   "limit": limit,
  //   "offset": skip,
  //   "query": query,
  //   "sort": "-updatedAt",
  //   "search": search,
  //   "searchFields": [
  //     "firstName",
  //     "lastName",
  //     "email",
  //     "phone.code",
  //     "phone.number",
  //     "vehicles.plateNumber",
  //   ],
  // }); //  Old
  const searchFields = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        "vehicles.hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;

    case constantUtil.HUBSEMPLOYEE:
      professionalVehicleHubQuery = {
        "createdBy": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  if (search) {
    searchFields.push({
      "$match": {
        "$or": [
          {
            "firstName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "lastName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "email": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.number": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "vehicles.plateNumber": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          // professionalVehicleHubQuery,
        ],
      },
    });
  }

  const queryData = [];
  queryData.push(
    {
      "$match": {
        ...query,
        ...professionalVehicleHubQuery,
      },
    },
    ...searchFields, //For Search
    {
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          { "$sort": { "createdAt": -1 } },
          { "$skip": parseInt(skip) },
          { "$limit": parseInt(limit) },
          {
            "$project": {
              "firstName": 1,
              "lastName": 1,
              "email": 1,
              "phone": 1,
              "vehicles.plateNumber": 1,
              "vehicles.status": 1,
              "review": 1,
              "membershipDetails": 1,
              "wallet": 1,
              "status": 1,
              "createdAt": 1,
            },
          },
        ],
      },
    }
  );

  const jsonData = await this.adapter.model
    .aggregate(queryData)
    .allowDiskUse(true);

  const plans = await storageUtil.read(constantUtil.PROFESSIONALMEMBERSHIPPLAN);
  // const responseJson = { "count": count, "response": [] };
  const responseJson = {
    "count": jsonData[0]?.all[0]?.all || 0,
    "response": [],
  };

  for (const professional of jsonData[0]?.response) {
    responseJson.response.push({
      "_id": professional._id,
      "data": {
        "firstName": professional.firstName,
        "lastName": professional.lastName,
        "email": professional.email,
        "phone": professional.phone,
        "status": professional.status,
        "avgRating": professional?.review?.avgRating || 0,
        "wallet": professional.wallet,
        "membershipDetails": {
          ...professional.membershipDetails,
          "membershipPlanId":
            plans[professional.membershipDetails?.membershipPlanId],
        },
        "createdAt": professional.createdAt,
        "vehiclesInActiveList": professional.vehicles?.filter((item) => {
          if (item.status === constantUtil.INACTIVE) return item;
        }),
      },
    });
  }

  return { "code": 1, "data": responseJson, "message": "" };
};
professionalEvent.getEditProfessional = async function (context) {
  let responseJson = null;
  if (context.params.action === constantUtil.STATUS_ONGOING) {
    responseJson = await this.adapter.model
      .findOne({
        "bookingInfo.ongoingBooking": mongoose.Types.ObjectId(
          context.params.id
        ),
      })
      .populate("membershipDetails.membershipPlanId")
      .populate("serviceTypeId")
      .populate("serviceAreaId", { "locationName": 1 })
      .lean();
  } else {
    responseJson = await this.adapter.model
      .findOne({
        "_id": mongoose.Types.ObjectId(context.params.id),
      })
      .populate("membershipDetails.membershipPlanId")
      .populate("serviceTypeId")
      .populate("serviceAreaId", { "locationName": 1 })
      .lean();
  }
  if (!responseJson) {
    return { "code": 0, "data": {}, "message": "INVALID CREDENTIALS" };
  } else {
    return { "code": 1, "data": responseJson, "message": "" };
  }
};
professionalEvent.updateProfessionalStep1 = async function (context) {
  let errorCode = 1, // Success Code is 1
    message = "",
    errorMessage = "",
    responseJson,
    generalSettings = null,
    validReferreral,
    referredByData = {};
  const clientId = context.params.clientId;
  const {
    INFO_INVALID_DATE_FORMAT,
    ALERT_DUPLICATE_EMAIL,
    ALERT_CPF_NO_ALREADY_EXISTS,
    INFO_INVALID_REFERRAL_CODE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
    const referredBy = context.params.referredBy || "";
    if (referredBy) {
      validReferreral = await this.adapter.model
        .findOne(
          {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "uniqueCode": referredBy.toLowerCase(),
          },
          { "_id": 1, "uniqueCode": 1 }
        )
        .collation({ "locale": "en", "caseLevel": true });
      if (!validReferreral) {
        throw new MoleculerError(INFO_INVALID_REFERRAL_CODE);
      }
      referredByData = {
        "referralDetails": {
          "rideCount": inviteSettings.data.forProfessional.rideCount,
          "inviteAmount": inviteSettings.data.forProfessional.amountToInviter,
          "joinerAmount": inviteSettings.data.forProfessional.amountToJoiner,
        },
        "referredBy": referredBy.toLowerCase(),
      };
    }
    const checkEmailCondition =
      context.params.professionalId === "newrecord"
        ? { "email": context.params.email }
        : {
            "_id": {
              "$ne": mongoose.Types.ObjectId(
                context.params.professionalId.toString()
              ),
            },
            "email": context.params.email,
          };
    const checkEmailCount = await this.adapter.model.countDocuments(
      checkEmailCondition
    );
    if (checkEmailCount > 0) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    if (
      !moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"], true).isValid()
    ) {
      return {
        "code": 0,
        "data": {},
        "message": INFO_INVALID_DATE_FORMAT,
      };
    }
    const age =
      moment().year() -
      moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"]).year();
    if (age < parseInt(generalSettings.data.driverMinAge)) {
      return {
        "code": 0,
        "data": {},
        "message":
          "AGE MUST BE" +
          generalSettings.data.driverMinAge +
          " AND ABOVE TO PROCEED",
      };
    }
    // const checkJson = await this.adapter.findOne({
    //   "phone.code": context.params.phoneCode, // Need to te-check
    //   "phone.number": context.params.phoneNumber, // Need to te-check
    //   "email": context.params.email,
    //   "_id": { "$ne": context.params.professionalId },
    // });
    // if (checkJson)
    //   return {
    //     "code": 0,
    //     "data": {},
    //     "message": "EMAIL ALREADY EXIST",
    //   };

    let jsonData = null;
    if (
      context.params.nationalIdNo ||
      context.params.phoneCode === constantUtil.COUNTRYCODE_BRAZIL
    ) {
      // jsonData = await this.adapter.model.findOne({
      //   "$or": [
      //     {
      //       "nationalIdNo": context.params.nationalIdNo,
      //     },
      //     {
      //       "phone.code": context.params.phoneCode,
      //       "phone.number": context.params.phoneNumber,
      //     },
      //   ],
      // });
      const checkDuplicatenationalIdQuery =
        context.params.professionalId === "newrecord"
          ? { "nationalIdNo": context.params.nationalIdNo }
          : {
              "_id": {
                "$ne": mongoose.Types.ObjectId(
                  context.params.professionalId.toString()
                ),
              },
              "nationalIdNo": context.params.nationalIdNo,
            };
      jsonData = await this.adapter.model
        .find(
          {
            // "nationalIdNo": context.params.nationalIdNo,
            ...checkDuplicatenationalIdQuery,
          },
          {
            "_id": 1,
            "phone": 1,
            "nationalIdNo": 1,
          }
        )
        .lean();
      // if (
      //   jsonData.length > 1 ||
      //   (jsonData.length === 0 &&
      //     context.params.professionalId !== "newrecord") ||
      //   (jsonData.length === 1 &&
      //     jsonData?.[0]?._id?.toString() !==
      //       context.params?.professionalId?.toString())
      // )
      if (jsonData.length > 0) {
        return {
          "code": 0,
          "data": {},
          "message": ALERT_CPF_NO_ALREADY_EXISTS,
        };
      }
      // //
      // jsonData = await this.adapter.model
      //   .findOne({
      //     "phone.code": context.params.phoneCode,
      //     "phone.number": context.params.phoneNumber,
      //   })
      //   .lean();
    }
    jsonData = await this.adapter.model
      .findOne({
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        //"clientId": mongoose.Types.ObjectId(clientId),
      })
      .lean();

    if (context.params.professionalId !== "newrecord") {
      const professionalData = await this.adapter.model
        .findById(
          mongoose.Types.ObjectId(context.params.professionalId.toString())
        )
        .lean();
      if (!professionalData)
        return {
          "code": 0,
          "data": {},
          "message": "ERROR IN PROFESSIONAL DETAILS",
        };

      // context.params.uniqueCode = professionalData.uniqueCode;
      if (context.params.files && context.params.files.length > 0) {
        const s3Image = await this.broker.emit(
          "admin.uploadSingleImageInSpaces",
          {
            "clientId": clientId,
            "files": context.params.files,
            "fileName": `${constantUtil.AVATAR}${context.params.uniqueCode}${constantUtil.PROFESSIONAL}`,
            "oldFileName": `${professionalData?.avatar || ""}`,
          }
        );
        context.params.avatar = s3Image && s3Image[0];
      }
    } else {
      // const dynamicUniqueCode = `${context.params.firstName.slice(
      //   0,
      //   4
      // )}${helperUtil.randomString(6, "a")}`;
      const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
        context.params.firstName,
        clientId
      );
      context.params.uniqueCode = dynamicUniqueCode.toLowerCase();
      if (context.params.files && context.params.files.length > 0) {
        const s3Image = await this.broker.emit(
          "admin.uploadSingleImageInSpaces",
          {
            "clientId": clientId,
            "files": context.params.files,
            "fileName": `${constantUtil.AVATAR}${context.params.uniqueCode}${constantUtil.PROFESSIONAL}`,
            "oldFileName": null,
          }
        );
        context.params.avatar = s3Image && s3Image[0];
      }
    }
    const updateData = {
      "clientId": clientId,
      "uniqueCode": context.params.uniqueCode,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "email": context.params.email,
      "isEmailVerified": context.params.isEmailVerified,
      "gender": context.params.gender,
      "avatar": context.params.avatar,
      "dob": context.params.dob?.replace(/-/g, "/"),
      "tin": context.params.tin,
      "age": age,
      "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
      "phone": {
        "code": context.params.phoneCode,
        "number": context.params.phoneNumber,
      },
      "nationalIdNo": context.params.nationalIdNo,
      "nationalInsuranceNo": context.params.nationalInsuranceNo,
      "emailOTP": null,
      "address": {
        "line": context.params.line1,
        "city": context.params.city,
        "state": context.params.state,
        "country": context.params.country,
        "zipcode": context.params.zipcode,
      },
      "serviceAreaId": context.params.serviceAreaId,
      "serviceTypeId": context.params.serviceTypeId,
      "serviceType": context.params.serviceType,
      "paymentMode": context.params?.paymentMode?.split(",") || [],
      // "paymentMode": context.params?.paymentMode || [],
      "knownLanguages": context.params?.knownLanguages?.split(",") || [],
      "notes": context.params.notes,
      ...referredByData,
    };
    if (context.params.professionalId !== "newrecord") {
      if (
        jsonData &&
        jsonData._id.toString() !== context.params.professionalId.toString()
      ) {
        // return {
        //   "code": 0,
        //   "data": {},
        //   "message": "PROFESSIONAL ALREADY EXIST",
        // };
        throw new MoleculerError("PROFESSIONAL ALREADY EXISTS", 0);
      } else {
        responseJson = await this.adapter.updateById(
          mongoose.Types.ObjectId(context.params.professionalId.toString()),
          updateData
        );
        // return { "code": 1, "data": responseJson, "message": "" };
      }
    } else {
      if (jsonData) {
        // return {
        //   "code": 0,
        //   "data": {},
        //   "message": "PROFESSIONAL ALREADY EXIST",
        // };
        throw new MoleculerError("PROFESSIONAL ALREADY EXISTS", 0);
      } else {
        updateData["review"] = {};
        updateData["review"]["avgRating"] = 5;
        responseJson = await this.adapter.insert(updateData);
        const professionalId = responseJson._id?.toString();
        // return { "code": 1, "data": responseJson, "message": "" };
        //----------- invite and earn Start --------------
        if (validReferreral) {
          const inviteAndEarnSettings = await storageUtil.read(
            constantUtil.INVITEANDEARN
          );
          if (inviteAndEarnSettings?.data?.forProfessional?.rideCount === 0) {
            await this.broker.emit("professional.inviteAndEarn", {
              "clientId": clientId,
              "joinerId": professionalId,
              // "id": validReferreral._id.toString(),
            });
          } else {
            await this.adapter.model.updateOne(
              {
                "_id": mongoose.Types.ObjectId(
                  validReferreral?._id?.toString()
                ),
              },
              {
                "$inc": { "invitationCount": 1 }, // "joinCount": 1,
              }
            );
          }
        }
        //----------- invite and earn End --------------
      }
    }
  } catch (e) {
    errorCode = 0; // For Error code is 0
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseJson = {};
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseJson,
  };
};

professionalEvent.getAllDocumentForUpload = async function (context) {
  let professionalDetails = null,
    professionalServiceTypeId = null,
    defaultServiceTypeId = null;
  //
  if (context.params.professionalId && context.params.vehicleId) {
    professionalDetails = await this.adapter.model
      .findOne(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        {
          "vehicles": {
            "$elemMatch": { "_id": context.params.vehicleId },
          },
        },
        { "vehicles.serviceType": 1, "vehicles.serviceTypeId": 1 }
      )
      .lean();
    // defaultServiceTypeId = professionalDetails?.serviceTypeId?.toString();
    defaultServiceTypeId =
      professionalDetails?.vehicles[0]?.serviceTypeId?.toString(); // This Is Correct Don't Change
  } else if (context.params.professionalId) {
    professionalDetails = await this.adapter.model
      .findOne(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        // {
        //   "": {
        //     "_id": mongoose.Types.ObjectId(
        //       context.params.professionalId.toString()
        //     ),
        //   },
        // },
        { "serviceType": 1, "serviceTypeId": 1 }
      )
      .lean();
    defaultServiceTypeId = professionalDetails?.serviceTypeId?.toString();
  }
  // const professionalServiceType =
  //   professionalDetails?.serviceType || constantUtil.CONST_RIDE;
  professionalServiceTypeId =
    context.params.serviceTypeId || defaultServiceTypeId;
  //
  const responseJson = {};
  const profileDocumentList = await storageutil.read(
    constantUtil.PROFILEDOCUMENTS
  );
  // profileDocumentList = profileDocumentList.filter((item) => {
  //   if (item.docsServiceType === professionalDetails.serviceType) return item;
  // });
  if (!profileDocumentList)
    return {
      "code": 1,
      "data": [],
      "message": "",
    };
  responseJson[constantUtil.PROFILEDOCUMENTS] =
    Object.keys(profileDocumentList)
      .map((documentId) => ({
        ...profileDocumentList[documentId],
        // "id": documentId,
      }))
      .filter(function (item) {
        // const serviceType = item.docsServiceType || constantUtil.CONST_RIDE;
        // if (serviceType === professionalServiceType) return item;
        if (
          item.docsServiceTypeId === professionalServiceTypeId &&
          item.status === constantUtil.ACTIVE
        )
          return item;
      }) || [];

  const driverDocumentList = await storageutil.read(
    constantUtil.DRIVERDOCUMENTS
  );

  if (!driverDocumentList)
    return {
      "code": 1,
      "data": [],
      "message": "",
    };
  responseJson[constantUtil.DRIVERDOCUMENTS] =
    Object.keys(driverDocumentList)
      .map((documentId) => ({
        ...driverDocumentList[documentId],
        // "id": documentId,
      }))
      .filter(function (item) {
        // const serviceType = item.docsServiceType || constantUtil.CONST_RIDE;
        // if (serviceType === professionalServiceType) return item;
        if (
          item.docsServiceTypeId === professionalServiceTypeId &&
          item.status === constantUtil.ACTIVE
        )
          return item;
      }) || [];

  return {
    "code": 1,
    "data": responseJson,
  };
};

professionalEvent.updateProfessionalStep2 = async function (context) {
  let expiryDate = "";
  if (context.params?.expiryDate === "Invalid Date") {
    return {
      "code": 0,
      "data": {},
      "message": "INVALID DATE FORMAT",
    };
  }
  if (!!context.params.expiryDate === true) {
    try {
      expiryDate = new Date(context.params.expiryDate).toISOString();
      const diffDays = parseInt(
        (new Date(context.params.expiryDate) - new Date()) /
          (1000 * 60 * 60 * 24)
      ); //gives day difference
      if (diffDays <= 0)
        return {
          "code": 0,
          "data": {},
          "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
        };
    } catch (e) {
      // expiryDate = new Date().toISOString();
      expiryDate = "";
    }
  }
  const professionalData = await this.adapter.model
    .findOne(
      { "_id": context.params.professionalId },
      { "_id": 1, "uniqueCode": 1, "profileVerification": 1, "status": 1 }
    )
    .lean();
  if (context.params.files?.length) {
    const [document] = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `${context.params.professionalId?.toString()}profile${
          constantUtil.PROFESSIONAL
        }`,
      }
    );
    context.params.image = document;
  }
  if (!context.params.image) {
    return {
      "code": 0,
      "message": "image required",
    };
  }

  const documentNameExists = professionalData?.profileVerification?.find(
    (each) => each.documentName === context.params.documentName
  )
    ? { "profileVerification.documentName": context.params.documentName }
    : {};

  const mongoId = mongoose.Types.ObjectId();
  let documentId = null;
  if (
    context.params.documentId?.toString() !== "null" &&
    context.params.documentId?.toString() !== "undefined"
  ) {
    documentId = context.params.documentId;
  }
  const updateDocumentQuery = professionalData?.profileVerification?.find(
    (each) => each.documentName === context.params.documentName
  )
    ? {
        "profileVerification.$.documentId": documentId,
        "profileVerification.$.documentName": context.params.documentName,
        "profileVerification.$.expiryDate": expiryDate || "",
        "profileVerification.$.documents": context.params.image,
        "profileVerification.$.uploadedStatus": constantUtil.VERIFIED,
        "profileVerification.$.reason": context.params.reason,
        "profileVerification.$.status": constantUtil.VERIFIED,
        "status": professionalData.status || constantUtil.VERIFIED,
      }
    : {
        "$push": {
          "profileVerification": {
            "_id": mongoId,
            "documentId": documentId,
            "documentName": context.params.documentName,
            "expiryDate": expiryDate || "",
            "documents": context.params.image,
            "uploadedStatus": constantUtil.VERIFIED,
            "reason": context.params.reason,
            "status": professionalData.status || constantUtil.VERIFIED,
          },
        },
      };
  const responseData = {
    "_id": context.params.id || mongoose.Types.ObjectId(),
    "documentId": documentId,
    "documentName": context.params.documentName,
    "expiryDate": expiryDate,
    "documents": context.params.image,
    "status": constantUtil.VERIFIED,
  };
  const updatedProfessionalData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      ...documentNameExists,
    },
    updateDocumentQuery
  );
  if (!updatedProfessionalData.nModified) {
    return {
      "code": 0,
      "data": {},
      "message": "ERROR IN PROFILE DOCUMENT UPLOAD",
    };
  } else {
    return {
      "code": 1,
      "message": "PROFILE DOCUMENT UPLOADED SUCCESSFULLY",
      "data": responseData,
    };
  }
};
professionalEvent.updateProfessionalStep3 = async function (context) {
  let responseData,
    errorCode = 1,
    message = "",
    vehicleId;
  try {
    let { [context.params.vehicleCategoryId]: vehicleCategoryData } =
      await storageUtil.read(constantUtil.VEHICLECATEGORY);

    const professionalData = await this.adapter.model
      .findOne({
        "_id": context.params.professionalId,
      })
      .lean();

    if (context.params?.files?.length) {
      const [documents] = await this.broker.emit(
        "admin.uploadMultipleImageInSpaces",
        {
          "files": context.params.files,
          "fileName": `${context.params.professionalId?.toString()}${
            constantUtil.VEHICLEDETAILS
          }`,
        }
      );
      if (documents?.length) {
        await documents.forEach((docs) => {
          if (docs.includes("frontimage")) context.params.frontImage = docs;
          if (docs.includes("backimage")) context.params.backImage = docs;
          if (docs.includes("leftimage")) context.params.leftImage = docs;
          if (docs.includes("rightimage")) context.params.rightImage = docs;
        });
      }
    }

    const [serviceCategoryData] = await this.broker.emit(
      "category.getCategoryById",
      {
        "id": context.params.serviceCategory,
      }
    );

    if (!serviceCategoryData) {
      // return {
      //   "code": 404,
      //   "message": "SERVICE CATEGORY NOT AVAILABLE",
      //   "data": {},
      // };
      throw new MoleculerError("SERVICE CATEGORY NOT AVAILABLE", 404);
    }

    vehicleCategoryData = {
      ...vehicleCategoryData,
      ...serviceCategoryData.categoryData.vehicles.find(
        (vehicle) =>
          vehicle.categoryId.toString() === context.params.vehicleCategoryId
      ),
    };

    if (!vehicleCategoryData) {
      // return {
      //   "code": 404,
      //   "message": "VEHICLE CATEGORY DATA NOT FOUND",
      //   "data": {},
      // };
      throw new MoleculerError("VEHICLE CATEGORY DATA NOT FOUND", 404);
    }
    const membershipDetails = [];
    let phoneCode = null,
      phoneNumber = null;
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

    if (
      generalSettings.data?.isProfessionalSubscriptionModel &&
      context.params?.apiFrom === "verify"
    ) {
      // finding default subscription
      const plans = await storageUtil.read(
        constantUtil.PROFESSIONALMEMBERSHIPPLAN
      );
      if (!plans) {
        throw new MoleculerError(
          "SOMETHING WENT WRONG IN SUBSCRIPTION DATA GETTING"
        );
      }
      let defaultPlan;
      let addOnPlan;

      for (const eachPlans in plans) {
        if (plans[eachPlans]?.isDefault) {
          defaultPlan = plans[eachPlans];
        }
      }
      if (!defaultPlan) {
        // return {
        //   "code": 400,
        //   "message": "DEFAULT MEMBERSHIP PLAN NOT FOUND",
        // };
        throw new MoleculerError("DEFAULT MEMBERSHIP PLAN NOT FOUND", 400);
      } else {
        const [{ "_id": transactionId }] = await this.broker.emit(
          "transaction.membershipAddingTransaction",
          {
            "transactionAmount": 0,
            "from": {
              "userType": constantUtil.PROFESSIONAL,
              "userId": professionalData._id,
            },
            "to": {
              "userType": constantUtil.ADMIN,
              "userId": context.params.adminId,
            },
            "paymentType": constantUtil.CASH,
            "transactionType": constantUtil.MEMBERSHIPPLAN,
            "membershipPlanId": defaultPlan._id,
          }
        );
        if (!transactionId) {
          // return {
          //   "code": 400,
          //   "message":
          //     "SOMETHING WENT WRONG IN TRANSACTION IN DEFAULT MEMBERSHIP TRANSACTION",
          // };
          throw new MoleculerError(
            "SOMETHING WENT WRONG IN TRANSACTION IN DEFAULT MEMBERSHIP TRANSACTION",
            400
          );
        }

        membershipDetails.push({
          "membershipPlanId": mongoose.Types.ObjectId(
            defaultPlan._id.toString()
          ),
          "totalNoOfRides": defaultPlan.totalNoOfRides,
          "usedRides": 0,
          "isPlanExpired": false,
          "startDate": new Date(),
          "endDate": new Date(
            new Date().getTime() +
              defaultPlan.validityDays * 24 * 60 * 60 * 1000
          ),
          "transactionId": transactionId,
          "activateDate": new Date(),
          "amount": 0,
        });
      }

      if (context.params.isAddOnPlan) {
        addOnPlan = plans[context.params.addOnPlanId];
        if (!addOnPlan) {
          // return {
          //   "code": 400,
          //   "message": "GIVEN ADDON PLAN NOT FOUND",
          // };
          throw new MoleculerError("GIVEN ADDON PLAN NOT FOUND", 400);
        }

        const [{ "_id": transactionId }] = await this.broker.emit(
          "transaction.membershipAddingTransaction",
          {
            "transactionAmount": addOnPlan.amount ?? 0,
            "from": {
              "userType": constantUtil.PROFESSIONAL,
              "userId": professionalData._id,
            },
            "to": {
              "userType": constantUtil.ADMIN,
              "userId": context.params.adminId,
            },
            "paymentType": constantUtil.CASH,
            "transactionType": constantUtil.MEMBERSHIPPLAN,
            "membershipPlanId": addOnPlan._id,
          }
        );
        if (!transactionId) {
          // return {
          //   "code": 400,
          //   "message": "SOMETHING WENT WRONG IN ADD ON MEMBERSHIP TRANSACTION",
          // };
          throw new MoleculerError(
            "SOMETHING WENT WRONG IN ADD ON MEMBERSHIP TRANSACTION",
            400
          );
        }

        membershipDetails.push({
          "membershipPlanId": mongoose.Types.ObjectId(addOnPlan._id.toString()),

          "totalNoOfRides": addOnPlan.totalNoOfRides,
          "usedRides": 0,
          "isPlanExpired": false,
          "startDate": new Date(
            new Date().getTime() + membershipDetails[0].endDate.getTime()
          ),
          "endDate": new Date(
            new Date().getTime() +
              membershipDetails[0].endDate.getTime() +
              addOnPlan.validityDays * 24 * 60 * 60 * 1000
          ),
          "activateDate": new Date(),
          "transactionId": transactionId,
          "amount": addOnPlan.amount ?? 0,
        });
      }
      const updateMembership = await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.professionalId),
        },
        {
          "membershipDetails": membershipDetails,
          "activeMembershipDetails": membershipDetails[0],
          "isUsedFreePlan": true,
          "isExpiredFreePlan": false,
          "isExpired": false,
        },
        { "new": true }
      );
      // if (!updateMembership.nModified) {
      //   return {
      //     "code": 500,
      //     "message":
      //       "SOMETHING WENT WRONG IN UPDATE VERIFY PAGE MEMBERSHIP DETAILS",
      //   };
      // }
      phoneCode = updateMembership.phone.code;
      phoneNumber = updateMembership.phone.number;
    } else if (context.params?.apiFrom === "verify") {
      const updateMembership = await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.professionalId),
        },
        {
          "isExpired": false,
        },
        { "new": true }
      );
      // if (!updateMembership.nModified) {
      //   return {
      //     "code": 500,
      //     "message": "SOMETHING WENT WRONG IN UPDATE PROFESSIONAL DETAILS",
      //   };
      // }
      phoneCode = updateMembership.phone.code;
      phoneNumber = updateMembership.phone.number;
    }
    // else {
    //   const updateMembership = await this.adapter.model.updateOne(
    //     {
    //       "_id": mongoose.Types.ObjectId(context.params.professionalId),
    //     },
    //     {
    //       "serviceTypeId": context.params.serviceTypeId,
    //       "serviceType": context.params.serviceType,
    //     }
    //   );
    //   if (!updateMembership.nModified) {
    //     return {
    //       "code": 500,
    //       "message": "SOMETHING WENT WRONG IN UPDATE PROFESSIONAL DETAILS",
    //     };
    //   }
    // }
    // send sms
    if (context.params?.apiFrom === "verify") {
      phoneCode = professionalData.phone.code;
      phoneNumber = professionalData.phone.number;
      const defaultVehicle = professionalData?.vehicles?.find((item) => {
        return item.defaultVehicle === true;
      });
      //Get data from Redis
      const notifySMSTemplate = await storageUtil.read(
        constantUtil.NOTIFYSMSTEMPLATE
      );
      //
      let notifySMSData = null;
      Object.values(notifySMSTemplate)
        .filter((option) => option.isActive === true)
        .forEach((option) => {
          if (
            option.data.notifyType === constantUtil.PROFESSIONALACTIVATED &&
            option.data.bookingFrom === constantUtil.CONST_APP
          ) {
            notifySMSData = option;
          }
        });
      //-------------------
      let notifyActiveMsg = notifySMSData.data.notifyMessage || "";
      notifyActiveMsg = notifyActiveMsg.replace(
        /{{professionalName}}/g,
        commonUtil.initialCaps(professionalData.firstName)
      );
      notifyActiveMsg = notifyActiveMsg.replace(
        /{{plateNumber}}/g,
        (defaultVehicle?.plateNumber || "").toUpperCase()
      );
      notifyActiveMsg = notifyActiveMsg.replace(
        /{{carModel}}/g,
        commonUtil.initialCaps(defaultVehicle?.maker || "") +
          " " +
          commonUtil.initialCaps(defaultVehicle?.model || "")
      );
      notifyActiveMsg = notifyActiveMsg.replace(
        /{{carColor}}/g,
        commonUtil.initialCaps(defaultVehicle?.color || "")
      );
      //-------------------
      if (notifySMSData?.data?.smsNotificationsEnable) {
        // let notifyMessage = notifySMSData.data.notifyMessage || "";
        await this.broker.emit("admin.sendSMS", {
          "phoneNumber": `${phoneCode}${phoneNumber}`,
          "message": notifyActiveMsg,
          "notifyType": constantUtil.PROFESSIONALACTIVATED,
          "otpReceiverAppName": constantUtil.SMS,
        });
      }
      // const notifyActiveMsg =
      //   "Great news! Your documents have been verified. You’re all set to take rides.";
      const notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.PROFESSIONALACTIVATED,
          "timestamp": Date.now(),
          "message": notifyActiveMsg,
          "details": lzStringEncode({ "message": notifyActiveMsg }),
        },
        "registrationTokens": [
          {
            "token": professionalData?.deviceInfo[0]?.deviceId,
            "id": professionalData?._id?.toString(),
            "deviceType": professionalData?.deviceInfo[0]?.deviceType,
            "platform": professionalData?.deviceInfo[0]?.platform,
            "socketId": professionalData?.deviceInfo[0]?.socketId,
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }

    // const professionalDataNew = await this.adapter.model
    //   .findOne(
    //     {
    //       "_id": mongoose.Types.ObjectId("65782c54016b934d7c89b32d"),
    //     },
    //     {
    //       "vehicles": {
    //         "$elemMatch": { "defaultVehicle": false },
    //       },
    //     }
    //   )
    //   // .populate("vehicles.serviceCategory")
    //   // .populate({
    //   //   "path": "vehicles.serviceCategory",
    //   //   // "match": { "defaultVehicle": true },
    //   //   "match": { "vehicles.$[].defaultVehicle": true },
    //   //   // "select": "vehicles.$.defaultVehicle",
    //   // })
    //   .populate({
    //     "path": "vehicles.serviceCategory",
    //     // "match": {
    //     //   "vehicles": { "$elemMatch": { "defaultVehicle": false } },
    //     // },
    //   })
    //   .lean();

    // const professionalDataNew1 = await this.adapter.model
    //   .find(
    //     {
    //       "_id": mongoose.Types.ObjectId("65782c54016b934d7c89b32d"),
    //     },
    //     {
    //       "vehicles": {
    //         "$elemMatch": { "serviceType": "RIDE", "defaultVehicle": true },
    //       },
    //     }
    //   )
    //   .lean();

    // const professionalDataNewDefault = await this.adapter.model
    //   .find(
    //     {
    //       "_id": mongoose.Types.ObjectId("65782c54016b934d7c89b32d"),
    //       // "vehicles": {
    //       //   "$elemMatch": {
    //       //     "_id": mongoose.Types.ObjectId("65782c6f016b934d7c89b33d"),
    //       //   },
    //       // },
    //     },
    //     {
    //       "vehicles": {
    //         // "$elemMatch": { "serviceType": "RIDE", "defaultVehicle": true },
    //         "$elemMatch": {
    //           "_id": mongoose.Types.ObjectId("65782c6f016b934d7c89b33d"),
    //         },
    //       },
    //     }
    //   )
    //   .lean();

    const vehicleDetails = {};
    vehicleDetails["vehicles"] = {
      "_id": context.params.id || mongoose.Types.ObjectId(),
      "professionalId": mongoose.Types.ObjectId(context.params.professionalId),
      "vinNumber": context.params.vinNumber,
      "type": context.params.type,
      "plateNumber": context.params.plateNumber,
      "maker": context.params.maker,
      "model": context.params.model,
      "year": context.params.year,
      "color": context.params.color,
      "noOfDoors": context.params.noOfDoors,
      "noOfSeats": context.params.noOfSeats,
      "defaultVehicle": context.params.defaultVehicle,
      "status": constantUtil.ACTIVE,
      "hub":
        !!context.params.hub === true
          ? mongoose.Types.ObjectId(context.params.hub)
          : null,
      "serviceCategory": mongoose.Types.ObjectId(
        context.params.serviceCategory
      ),
      "serviceTypeId": mongoose.Types.ObjectId(context.params.serviceTypeId),
      "serviceType": context.params.serviceType,
      "scheduleDate":
        !!context.params.scheduleDate === true
          ? new Date(context.params.scheduleDate).toISOString()
          : "",
      "frontImage": context.params.frontImage ?? "",
      "backImage": context.params.backImage ?? "",
      "leftImage": context.params.leftImage ?? "",
      "rightImage": context.params.rightImage ?? "",
      "vehicleCategoryId": mongoose.Types.ObjectId(
        context.params.vehicleCategoryId
      ),
      "isSubCategoryAvailable": vehicleCategoryData.isSubCategoryAvailable,
      "handicapAvailable": context.params.handicapAvailable,
      "childseatAvailable": context.params.childseatAvailable,
      "isRentalSupported": context.params.isRentalSupported,
      "isCompanyVehicle": context.params.isCompanyVehicle,
      "isEnableShareRide": context.params.isEnableShareRide,
      "isHasUSBChargePort": context.params.isHasUSBChargePort,
      "isHasWifi": context.params.isHasWifi,
      "isPetAllowed": context.params.isPetAllowed,
      "isEnableLuggage": context.params.isEnableLuggage,
      "isHasAC": context.params.isHasAC,
      "isForceAppliedToProfessional":
        vehicleCategoryData.isForceAppliedToProfessional,
      "subCategoryIds": vehicleCategoryData.subCategoryIds,
      "applySubCategory":
        vehicleCategoryData.isForceAppliedToProfessional ?? false,
    };
    // ---------- Add & Update Service Category  --------------
    if (professionalData.serviceAreaId) {
      vehicleDetails["serviceAreaId"] = professionalData.serviceAreaId;
    } else {
      vehicleDetails["serviceAreaId"] = mongoose.Types.ObjectId(
        context.params.serviceCategory
      );
    }
    // ---------- Add & Update Service Type & Type Id --------------
    if (professionalData.serviceTypeId) {
      vehicleDetails["serviceType"] = professionalData.serviceType;
      vehicleDetails["serviceTypeId"] = professionalData.serviceTypeId;
    } else {
      vehicleDetails["serviceType"] = context.params.serviceType;
      vehicleDetails["serviceTypeId"] = mongoose.Types.ObjectId(
        context.params.serviceTypeId
      );
    }
    //-----------------------------------------------
    vehicleId = vehicleDetails["vehicles"]["_id"]?.toString();
    if (!context.params.id) {
      // ---------- Add & Update defaultVehicle  --------------
      if (context.params.defaultVehicle) {
        vehicleDetails["defaultVehicle"] = vehicleDetails["vehicles"];
      } else {
        vehicleDetails["defaultVehicle"] =
          professionalData.defaultVehicle || vehicleDetails["vehicles"]; // Need to remove Or Condition (vehicleDetails["vehicles"]) after Jul 2024
      }
      // ---------- Update Vehicle Details  --------------
      responseData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.professionalId),
        },
        {
          "serviceAreaId": vehicleDetails["serviceAreaId"],
          "serviceType": vehicleDetails["serviceType"],
          "serviceTypeId": vehicleDetails["serviceTypeId"],
          "$push": {
            // "vehicles": vehicleDetails,
            "vehicles": vehicleDetails["vehicles"],
          },
          "defaultVehicle": vehicleDetails["defaultVehicle"],
        }
      );
    } else {
      responseData = await this.adapter.model.update(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
          "vehicles._id": mongoose.Types.ObjectId(context.params.id.toString()),
        },
        {
          "vehicles.$.professionalId": mongoose.Types.ObjectId(
            context.params.professionalId
          ),
          "vehicles.$.vinNumber": context.params.vinNumber,
          "vehicles.$.type": context.params.type,
          "vehicles.$.plateNumber": context.params.plateNumber,
          "vehicles.$.maker": context.params.maker,
          "vehicles.$.model": context.params.model,
          "vehicles.$.year": context.params.year,
          "vehicles.$.color": context.params.color,
          "vehicles.$.noOfDoors": context.params.noOfDoors,
          "vehicles.$.noOfSeats": context.params.noOfSeats,
          "vehicles.$.hub":
            !!context.params.hub === true
              ? mongoose.Types.ObjectId(context.params.hub)
              : null,
          "vehicles.$.scheduleDate":
            !!context.params.scheduleDate === true
              ? new Date(context.params.scheduleDate).toISOString()
              : "",
          "vehicles.$.frontImage": context.params.frontImage || "",
          "vehicles.$.backImage": context.params.backImage || "",
          "vehicles.$.leftImage": context.params.leftImage || "",
          "vehicles.$.rightImage": context.params.rightImage || "",
          "vehicles.$.serviceCategory": mongoose.Types.ObjectId(
            context.params.serviceCategory
          ),
          "vehicles.$.serviceTypeId": mongoose.Types.ObjectId(
            context.params.serviceTypeId
          ),
          "vehicles.$.serviceType": context.params.serviceType,
          "vehicles.$.defaultVehicle": context.params.defaultVehicle ?? false,
          "vehicles.$.vehicleCategoryId": context.params.vehicleCategoryId,
          "vehicles.$.status": constantUtil.ACTIVE,
          "vehicles.$.isSubCategoryAvailable":
            vehicleCategoryData.isSubCategoryAvailable,
          "vehicles.$.isForceAppliedToProfessional":
            vehicleCategoryData.isForceAppliedToProfessional,
          "vehicles.$.subCategoryIds": vehicleCategoryData.subCategoryIds,
          "vehicles.$.applySubCategory":
            vehicleCategoryData.isForceAppliedToProfessional ?? false,
          "vehicles.$.handicapAvailable": context.params.handicapAvailable,
          "vehicles.$.childseatAvailable": context.params.childseatAvailable,
          "vehicles.$.isCompanyVehicle": context.params.isCompanyVehicle,
          "vehicles.$.isEnableShareRide": context.params.isEnableShareRide,
          "vehicles.$.isRentalSupported": context.params.isRentalSupported,
          "vehicles.$.isHasUSBChargePort": context.params.isHasUSBChargePort,
          "vehicles.$.isHasWifi": context.params.isHasWifi,
          "vehicles.$.isPetAllowed": context.params.isPetAllowed,
          "vehicles.$.isEnableLuggage": context.params.isEnableLuggage,
          "vehicles.$.isHasAC": context.params.isHasAC,
        }
      );
      // ---------- Add & Update defaultVehicle  --------------
      if (context.params.defaultVehicle) {
        // vehicleDetails["vehicles"]["_id"] = mongoose.Types.ObjectId(
        //   context.params.id.toString()
        // );
        const defaultVehicle = await this.adapter.model
          .findOne(
            { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
            {
              "vehicles": {
                "$elemMatch": {
                  "_id": mongoose.Types.ObjectId(context.params.id.toString()),
                },
              },
            }
          )
          .lean();
        vehicleDetails["defaultVehicle"] = defaultVehicle.vehicles?.[0] || {};
      } else {
        // vehicleDetails["vehicles"]["_id"] = mongoose.Types.ObjectId(
        //   context.params.id.toString()
        // ); // Need to remove after Jul 2024
        // vehicleDetails["defaultVehicle"] =
        //   professionalData.defaultVehicle || vehicleDetails["vehicles"]; // Need to remove Or Condition (vehicleDetails["vehicles"]) after Jul 2024
        // //  vehicleDetails["defaultVehicle"] = professionalData.defaultVehicle; // Need to Add/Uncomment this Line after Jul 2024
        const defaultVehicle = await this.adapter.model
          .findOne(
            { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
            {
              "vehicles": {
                "$elemMatch": {
                  "_id": mongoose.Types.ObjectId(context.params.id.toString()),
                  "defaultVehicle": true,
                },
              },
            }
          )
          .lean();
        vehicleDetails["defaultVehicle"] = defaultVehicle.vehicles?.[0] || {};
      }
      // ---------- Update Service Category & Default Vehicle Details --------------
      await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.professionalId),
        },
        {
          "serviceAreaId": vehicleDetails["serviceAreaId"],
          "serviceType": vehicleDetails["serviceType"],
          "serviceTypeId": vehicleDetails["serviceTypeId"],
          "defaultVehicle": vehicleDetails["defaultVehicle"],
        }
      );
      // ---------- Update Vehicle Details  --------------
    }
    if (!responseData.nModified) {
      throw new MoleculerError(
        "SOMETHING WENT WRONG IN PROFESSIONAL STEP3 UPDATING",
        500
      );
    } else {
      errorCode = 1;
    }
    responseData["_id"] = vehicleId;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};
professionalEvent.updateProfessionalStep4 = async function (context) {
  let expiryDate = "";
  if (
    context.params.expiryDate &&
    context.params.expiryDate != undefined &&
    context.params.expiryDate != ""
  ) {
    try {
      expiryDate = new Date(context.params.expiryDate).toISOString();
      const diffDays = parseInt(
        (new Date(context.params.expiryDate) - new Date()) /
          (1000 * 60 * 60 * 24)
      ); //gives day difference
      if (diffDays <= 0)
        return {
          "code": 0,
          "data": {},
          "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
        };
    } catch (e) {
      // expiryDate = new Date().toISOString();
      expiryDate = "";
    }
  }
  const professionalData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.professionalId.toString()),
    })
    .lean();
  if (context.params.files.length) {
    const [documents] = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `${context.params.professionalId?.toString()}vehicle${
          constantUtil.PROFESSIONAL
        }`,
      }
    );
    context.params.image = documents;
  } else {
    context.params.image = context.params.oldImage;
  }
  if (!context.params.image) {
    return {
      "code": 0,
      "message": "image required",
    };
  }
  let documentId = null;
  if (
    context.params.documentId?.toString() !== "null" &&
    context.params.documentId?.toString() !== "undefined"
  ) {
    documentId = context.params.documentId;
  }
  const vehicles = professionalData.vehicles.map((vehicle) => {
    if (vehicle._id.toString() === context.params.vehicleId.toString()) {
      vehicle.status = constantUtil.ACTIVE;
      if (
        vehicle?.vehicleDocuments?.find(
          (eachDoc) => eachDoc?.documentName === context.params.documentName
        )
      ) {
        vehicle.vehicleDocuments = vehicle.vehicleDocuments.map(
          (vehicleDocument) => {
            if (vehicleDocument.documentName === context.params.documentName) {
              vehicleDocument.documentId = documentId;
              vehicleDocument.documentName = context.params.documentName;
              vehicleDocument.expiryDate = expiryDate;
              vehicleDocument.documents = context.params.image;
              vehicleDocument.uploadedStatus = constantUtil.VERIFIED;
              vehicleDocument.reason = context.params.reason;
              vehicleDocument.status = constantUtil.VERIFIED;
            }
            return vehicleDocument;
          }
        );
      } else {
        vehicle.vehicleDocuments.push({
          "_id": mongoose.Types.ObjectId(),
          "documentId": documentId,
          "documentName": context.params.documentName,
          "expiryDate": expiryDate,
          "documents": context.params.image,
          "uploadedStatus": constantUtil.VERIFIED,
          "reason": context.params.reason,
          "status": constantUtil.VERIFIED,
        });
      }
    }
    //  else if (context.params.action === constantUtil.ADD.toLowerCase()) {
    //   vehicle.vehicleDocuments.push({
    //     "_id": mongoose.Types.ObjectId(),
    //     "documentName": context.params.documentName,
    //     "expiryDate": expiryDate,
    //     "documents": context.params.image,
    //     "uploadedStatus": constantUtil.VERIFIED,
    //     "reason": context.params.reason,
    //     "status": constantUtil.VERIFIED,
    //   });
    // }
    return vehicle;
  });
  const responseData = {
    "_id": context.params.id || mongoose.Types.ObjectId(),
    "documentId": documentId,
    "documentName": context.params.documentName,
    "expiryDate": expiryDate,
    "documents": context.params.image,
    "status": constantUtil.VERIFIED,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    },
    { "vehicles": vehicles }
  );
  if (!responseJson) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return {
      "code": 1,
      "data": responseData,
      "message": "UPDATED SUCCESSFULLY",
    };
  }
};

professionalEvent.updateAllVehiclesShareableDetailsUsingCategoryId =
  async function (context) {
    let responseJson = null;
    try {
      if (context.params.vehicleCategoryId) {
        responseJson = await this.adapter.model
          .updateMany(
            {
              "vehicles.vehicleCategoryId": mongoose.Types.ObjectId(
                context.params.vehicleCategoryId
              ),
            },
            {
              "$set": {
                "vehicles.$.isEnableShareRide": context.params.mandatoryShare,
              },
            },
            { "new": true }
          )
          .lean();
        if (!responseJson.nModified) {
          responseJson = constantUtil.FAILED;
        } else {
          responseJson = constantUtil.SUCCESS;
        }
      }
    } catch (e) {
      responseJson = constantUtil.FAILED;
    }

    return responseJson;
  };

professionalEvent.updateBankDetailsProfessional = async function (context) {
  if (context.params.id) {
    const responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "bankDetails": context.params.data,
      }
    );
    if (!responseJson)
      return { "code": 0, "data": {}, "message": "ERROR IN BANK DETAILS" };
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
professionalEvent.changeProfessionalsStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": ids,
    },
    {
      "status": context.params.status,
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
professionalEvent.changeVehicleStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": context.params.professionalId,
      "vehicles": { "$elemMatch": { "_id": { "$in": ids } } },
    },
    {
      "$set": { "vehicles.$[list].status": context.params.status },
    },
    { "arrayFilters": [{ "list._id": { "$in": ids } }] }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
//#region Professional Documents Start
professionalEvent.professionalProfileDocumentVerification = async function (
  context
) {
  const clientId = context.params.clientId;
  const documentStatus = context.params.status;
  if (documentStatus === constantUtil.VERIFIED) {
    if (
      context.params.expiryDate &&
      context.params.expiryDate != undefined &&
      context.params.expiryDate != ""
    ) {
      try {
        const diffDays = parseInt(
          (new Date(context.params.expiryDate) - new Date()) /
            (1000 * 60 * 60 * 24)
        ); //gives day difference
        if (diffDays <= 0)
          return {
            "code": 0,
            "data": {},
            "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
          };
      } catch (e) {
        console.log(e);
      }
    }
  }
  const updateData =
    documentStatus === constantUtil.UNVERIFIED ||
    documentStatus === constantUtil.REJECTED
      ? { "status": constantUtil.UNVERIFIED }
      : {};

  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "profileVerification": {
        "$elemMatch": { "documentName": context.params.documentName },
      },
    },
    {
      "$set": {
        "profileVerification.$[list].reason": context.params.reason,
        "profileVerification.$[list].status": documentStatus,
        "profileVerification.$[list].uploadedStatus": documentStatus, //constantUtil.VERIFIED,
      },
      ...updateData,
    },
    { "arrayFilters": [{ "list.documentName": context.params.documentName }] }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const professionals = await this.adapter.model
      .findOne({
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      })
      .lean();
    if (documentStatus === constantUtil.REJECTED) {
      if (
        generalSettings.data.emailConfigurationEnable &&
        professionals.email &&
        professionals.isEmailVerified
      ) {
        this.broker.emit("admin.sendServiceMail", {
          "clientId": clientId,
          "templateName": "documentRejected",
          ...professionalDocumentExpiredMailObject({
            ...professionals,
            "documentName": context.params.documentName,
            "documentType": "Profile Document",
            "reason": context.params.reason,
          }),
        });
      }
    }
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

professionalEvent.professionalVehicleDocumentVerification = async function (
  context
) {
  const clientId = context.params.clientId;
  const documentStatus = context.params.status;
  if (documentStatus === constantUtil.VERIFIED) {
    if (
      context.params.expiryDate &&
      context.params.expiryDate != undefined &&
      context.params.expiryDate != ""
    ) {
      try {
        const diffDays = parseInt(
          (new Date(context.params.expiryDate) - new Date()) /
            (1000 * 60 * 60 * 24)
        ); //gives day difference
        if (diffDays <= 0)
          return {
            "code": 0,
            "data": {},
            "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
          };
      } catch (e) {
        console.log(e);
      }
    }
  }
  const updateData =
    documentStatus === constantUtil.UNVERIFIED ||
    documentStatus === constantUtil.REJECTED
      ? { "status": constantUtil.UNVERIFIED }
      : {};
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "vehicles": {
        "$elemMatch": { "_id": context.params.id },
      },
    },
    {
      "$set": {
        "vehicles.$[].vehicleDocuments.$[list].reason": context.params.reason,
        "vehicles.$[].vehicleDocuments.$[list].status": documentStatus,
        "vehicles.$[].vehicleDocuments.$[list].uploadedStatus": documentStatus, // constantUtil.VERIFIED,
      },
      ...updateData,
    },
    { "arrayFilters": [{ "list.documentName": context.params.documentName }] }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const professionals = await this.adapter.model
      .findOne({
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      })
      .lean();
    if (documentStatus === constantUtil.REJECTED) {
      if (
        generalSettings.data.emailConfigurationEnable &&
        professionals.email &&
        professionals.isEmailVerified
      ) {
        this.broker.emit("admin.sendServiceMail", {
          "clientId": clientId,
          "templateName": "documentRejected",
          ...professionalDocumentExpiredMailObject({
            ...professionals,
            "documentName": context.params.documentName,
            "documentType": "Vehicle Document",
            "reason": context.params.reason,
          }),
        });
      }
    }
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

professionalEvent.deleteProfessionalProfileDocument = async function (context) {
  const clientId = context.params.clientId;
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "profileVerification": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.params.documentId),
        },
      },
    },
    {
      "$pull": {
        "profileVerification": {
          "_id": mongoose.Types.ObjectId(context.params.documentId),
        },
      },
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

professionalEvent.deleteProfessionalVehicleDocument = async function (context) {
  const clientId = context.params.clientId;
  const profileVerification = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    })
    .lean();
  // Remove Document from vehicles Array
  const vehicles = profileVerification.vehicles.map((vehicle) => {
    vehicle.vehicleDocuments = vehicle.vehicleDocuments.filter(
      (vehicleDocument) => {
        if (vehicleDocument._id?.toString() !== context.params.documentId) {
          return vehicleDocument;
        }
      }
    );
    return vehicle;
  });
  // Update vehicles
  const responseJson = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
    { "vehicles": vehicles }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

// professionalEvent.checkProfessionalDocumentList_backup = async function (
//   professionalData
// ) {
//   // professionalData = professionalData.toJSON();
//   const professionalServiceTypeId = professionalData.serviceTypeId?.toString();
//   // //#region professional, Vehicle & Driver Documents
//   // const documentList = await helperUtil.getRegistrationDocumentFromRedis(
//   //   professionalServiceType
//   // );
//   // const profileDocumentList = documentList?.profileDocumentRideList || [];
//   // if (!profileDocumentList)
//   //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

//   // const driverDocumentList = documentList?.driverDocumentRideList || [];
//   // if (!driverDocumentList)
//   //   return { "code": 200, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };
//   // //#endregion professional, Vehicle & Driver Documents
//   //#region Profile Documents
//   let driverDocumentList = await storageUtil.read(constantUtil.DRIVERDOCUMENTS);
//   if (!driverDocumentList)
//     return { "code": 200, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

//   driverDocumentList = await Object.keys(driverDocumentList)
//     .map((documentId) => ({
//       ...driverDocumentList[documentId],
//       // "id": documentId,
//     }))
//     .filter(function (el) {
//       if (
//         el.status === constantUtil.ACTIVE &&
//         el.docsServiceTypeId === professionalServiceTypeId
//       )
//         return el;
//     });
//   //#endregion Profile Documents
//   //#region Vehicle & Profile Documents
//   let profileDocumentList = await storageUtil.read(
//     constantUtil.PROFILEDOCUMENTS
//   );
//   if (!profileDocumentList)
//     return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

//   profileDocumentList = Object.keys(profileDocumentList)
//     .map((documentId) => ({
//       ...profileDocumentList[documentId],
//       // "id": documentId,
//     }))
//     .filter(function (el) {
//       if (
//         el.status === constantUtil.ACTIVE &&
//         el.docsServiceTypeId === professionalServiceTypeId
//       )
//         return el;
//     });
//   //#endregion Profile Documents
//   const nextStepDetails = {
//     "profileVerification": true,
//     "isVehicleDocumentsUploaded": true,
//     "isHubDetailsPresent": true,
//     "vehicleVerified": true,
//     "isHaveServiceType": true,
//   };
//   const checkNextStepDetails = {
//     "profileVerification": [],
//     "isVehicleDocumentsUploaded": [],
//     "vehicleVerified": [],
//   };

//   profileDocumentList?.forEach(async (docs) => {
//     if (
//       profileDocumentList.length ===
//         professionalData?.profileVerification?.length ||
//       docs.docsMandatory === false
//     ) {
//       professionalData?.profileVerification?.forEach((d) => {
//         if (docs.docsName === d.documentName) {
//           if (docs.docsExpiry === true) {
//             const diffDays = parseInt(
//               (new Date(d.expiryDate) - new Date()) /
//                 helperUtil.getDaysToMilliSeconds(1)
//             ); //gives day difference
//             if (
//               d.uploadedStatus === constantUtil.UPLOADED &&
//               d.status === constantUtil.VERIFIED
//             ) {
//               checkNextStepDetails.vehicleVerified.push("1");
//             }
//             if (diffDays <= 0 || d.status === constantUtil.REJECTED) {
//               checkNextStepDetails.profileVerification.push("1");
//             }
//           } else {
//             if (
//               d.uploadedStatus === constantUtil.UPLOADED &&
//               d.status === constantUtil.VERIFIED
//             ) {
//               checkNextStepDetails.vehicleVerified.push("1");
//             }
//           }
//         }
//       });
//     } else {
//       checkNextStepDetails.profileVerification.push("1");
//     }
//   });

//   const vehicleCategoryList = await storageUtil.read(
//     constantUtil.VEHICLECATEGORY
//   );

//   await professionalData.vehicles.map((vehicle) => {
//     if (vehicle.status === constantUtil.INACTIVE)
//       checkNextStepDetails.vehicleVerified.push("1");

//     // if (vehicle.vehicleDocuments.length > 0) {
//     const docusvehi = vehicle.vehicleDocuments;
//     driverDocumentList?.forEach(async (docs) => {
//       if (
//         driverDocumentList.length === docusvehi.length ||
//         docs.docsMandatory === false
//       ) {
//         docusvehi?.forEach((c) => {
//           if (docs.docsName === c.documentName) {
//             if (docs.docsExpiry === true) {
//               const diffDays = parseInt(
//                 (new Date(c.expiryDate) - new Date()) /
//                   helperUtil.getDaysToMilliSeconds(1)
//               ); //gives day difference
//               if (
//                 c.uploadedStatus === constantUtil.UPLOADED &&
//                 c.status === constantUtil.VERIFIED
//               ) {
//                 checkNextStepDetails.vehicleVerified.push("1");
//               }
//               if (diffDays <= 0 || c.status === constantUtil.REJECTED) {
//                 checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
//               }
//             } else {
//               if (
//                 c.uploadedStatus === constantUtil.UPLOADED &&
//                 c.status === constantUtil.VERIFIED
//               ) {
//                 checkNextStepDetails.vehicleVerified.push("1");
//               }
//             }
//           }
//         });
//       } else {
//         checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
//       }
//     });
//     // } else {
//     //   checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
//     // }

//     // if (vehicle.hub && vehicle.scheduleDate)
//     if (vehicle?.serviceCategory) nextStepDetails.isHubDetailsPresent = false;

//     if (vehicle.vehicleCategoryId) {
//       vehicle.vehicleCategoryName =
//         vehicleCategoryList[vehicle.vehicleCategoryId]?.vehicleCategory;
//     } else {
//       vehicle.vehicleCategoryName = "";
//     }
//   });

//   if (professionalData?.serviceTypeId?.toString())
//     nextStepDetails.isHaveServiceType = false;

//   if (checkNextStepDetails.profileVerification.length === 0)
//     nextStepDetails.profileVerification = false;

//   if (checkNextStepDetails.isVehicleDocumentsUploaded.length === 0)
//     nextStepDetails.isVehicleDocumentsUploaded = false;

//   if (checkNextStepDetails.vehicleVerified.length === 0)
//     nextStepDetails.vehicleVerified = false;
//   //
//   if (professionalData.otpReceiverAppName) {
//     // if (nextStepDetails.isHaveServiceType) {
//     //   professionalData.registrationNextStep =
//     //     constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS;
//     // } else {
//     //   professionalData.registrationNextStep =
//     //     constantUtil.REGISTRATIONSTEP_PROFILE;
//     // }
//     professionalData.registrationNextStep = !professionalData.newProfessional
//       ? nextStepDetails.isHaveServiceType
//         ? constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
//         : nextStepDetails.isHubDetailsPresent
//         ? constantUtil.REGISTRATIONSTEP_HUBDETAILS
//         : nextStepDetails.profileVerification
//         ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS // constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
//         : professionalData.vehicles.length === 0
//         ? constantUtil.REGISTRATIONSTEP_VEHICLEDETAILS
//         : nextStepDetails.isVehicleDocumentsUploaded
//         ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS
//         : nextStepDetails.vehicleVerified
//         ? constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
//         : constantUtil.REGISTRATIONCOMPLETED
//       : constantUtil.REGISTRATIONSTEP_PROFILE;
//   } else {
//     professionalData.registrationNextStep = !professionalData.newProfessional
//       ? nextStepDetails.profileVerification
//         ? constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
//         : professionalData.vehicles.length === 0
//         ? constantUtil.REGISTRATIONSTEP_VEHICLEDETAILS
//         : nextStepDetails.isVehicleDocumentsUploaded
//         ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS
//         : nextStepDetails.isHubDetailsPresent
//         ? constantUtil.REGISTRATIONSTEP_HUBDETAILS
//         : nextStepDetails.vehicleVerified
//         ? constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
//         : constantUtil.REGISTRATIONCOMPLETED
//       : constantUtil.REGISTRATIONSTEP_PROFILE;
//   }
//   if (
//     professionalData.registrationNextStep ===
//     constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
//   ) {
//     await this.adapter.updateById(
//       mongoose.Types.ObjectId(professionalData?._id?.toString()),
//       {
//         "status": constantUtil.UNVERIFIED,
//       }
//     );
//   }

//   professionalData.vehicles = _.map(professionalData.vehicles, (vehicle) => ({
//     ...vehicle,
//     "hubsName": vehicle?.hub?.data?.hubsName || "",
//     "location": vehicle?.hub?.data?.location || "",
//     "phone": vehicle?.hub?.data?.phone || "",
//     "email": vehicle?.hub?.data?.email || "",
//     "showAddress": vehicle?.hub?.data?.showAddress || "",
//     "hub": vehicle?.hub?._id || vehicle?.hub,
//   }));
//   return professionalData;
// };

professionalEvent.checkProfessionalDocumentList = async function (
  professionalData
) {
  const nextStepDetails = {
    "vehicleIds": [],
    "profileVerification": true,
    "vehiclesVerification": false, // Default
    "isVehicleDocumentsUploaded": true,
    "isHubDetailsPresent": true,
    "vehicleVerified": true,
    "isHaveServiceType": true,
  };
  const checkNextStepDetails = {
    "profileVerification": [],
    "isVehicleDocumentsUploaded": [],
    "vehicleVerified": [],
  };

  if (professionalData?.serviceTypeId?.toString())
    nextStepDetails.isHaveServiceType = false;

  // professionalData = professionalData.toJSON();
  const professionalProfileServiceTypeId =
    professionalData.serviceTypeId?.toString();
  // //#region professional, Vehicle & Driver Documents
  // const documentList = await helperUtil.getRegistrationDocumentFromRedis(
  //   professionalServiceType
  // );
  // const profileDocumentList = documentList?.profileDocumentRideList || [];
  // if (!profileDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  // const driverDocumentList = documentList?.driverDocumentRideList || [];
  // if (!driverDocumentList)
  //   return { "code": 200, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };
  // //#endregion professional, Vehicle & Driver Documents
  //#region All Vehicle Documents
  const driverDocumentList = await storageUtil.read(
    constantUtil.DRIVERDOCUMENTS
  );
  if (!driverDocumentList)
    return { "code": 200, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

  const vehicleCategoryList = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );

  // driverDocumentList = await Object.keys(driverDocumentList)
  //   .map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     // "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     if (
  //       el.status === constantUtil.ACTIVE &&
  //       el.docsServiceTypeId === professionalServiceTypeId
  //     )
  //       return el;
  //   });
  //#endregion All Vehicle Documents
  //#region Profile Documents
  let profileDocumentList = await storageUtil.read(
    constantUtil.PROFILEDOCUMENTS
  );
  if (!profileDocumentList)
    return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  profileDocumentList = Object.keys(profileDocumentList)
    .map((documentId) => ({
      ...profileDocumentList[documentId],
      // "id": documentId,
    }))
    .filter(function (el) {
      if (
        el.status === constantUtil.ACTIVE &&
        el.docsServiceTypeId === professionalProfileServiceTypeId
      )
        return el;
    });
  const mandatoryProfileDocCount = profileDocumentList?.filter(
    (item) => item.docsMandatory === true
  ).length;
  const uploadedProfileDocCount = professionalData?.profileVerification?.length;
  profileDocumentList?.forEach(async (docs) => {
    // if (
    //   mandatoryProfileDocCount === uploadedProfileDocCount ||
    //   docs.docsMandatory === false
    // )
    //
    // professionalData?.profileVerification?.forEach((d) => {
    //   // if (docs.docsName === d.documentName) {
    //   if (docs.docsName === d.documentName && docs.docsMandatory === true) {
    //     if (docs.docsExpiry === true) {
    //       const diffDays = parseInt(
    //         (new Date(d.expiryDate) - new Date()) /
    //           helperUtil.getDaysToMilliSeconds(1)
    //       ); //gives day difference
    //       if (
    //         d.uploadedStatus === constantUtil.UPLOADED &&
    //         d.status === constantUtil.VERIFIED
    //       ) {
    //         checkNextStepDetails.vehicleVerified.push("1");
    //       }
    //       if (diffDays <= 0 || d.status === constantUtil.REJECTED) {
    //         checkNextStepDetails.profileVerification.push("1");
    //       }
    //     } else {
    //       if (
    //         d.uploadedStatus === constantUtil.UPLOADED &&
    //         d.status === constantUtil.VERIFIED
    //       ) {
    //         // checkNextStepDetails.vehicleVerified.push("1");
    //         checkNextStepDetails.profileVerification.push("1");
    //       }
    //     }
    //   }
    // });
    professionalData?.profileVerification
      ?.filter((each) => {
        return (
          docs.docsName === each.documentName && docs.docsMandatory === true
        );
      })
      ?.forEach((d) => {
        // if (d.docsMandatory === true) {
        if (docs.docsExpiry === true) {
          const diffDays = parseInt(
            (new Date(d.expiryDate) - new Date()) /
              helperUtil.getDaysToMilliSeconds(1)
          ); //gives day difference
          if (
            d.uploadedStatus === constantUtil.UPLOADED &&
            d.status === constantUtil.VERIFIED
          ) {
            checkNextStepDetails.vehicleVerified.push("1");
          }
          if (diffDays <= 0 || d.status === constantUtil.REJECTED) {
            checkNextStepDetails.profileVerification.push("1");
          }
        } else {
          if (
            d.uploadedStatus === constantUtil.UPLOADED &&
            d.status === constantUtil.VERIFIED
          ) {
            // checkNextStepDetails.vehicleVerified.push("1");
            checkNextStepDetails.profileVerification.push("1");
          }
        }
        // } else if (d.uploadedStatus) {
        //   checkNextStepDetails.profileVerification.push("1");
        // }
      });
    // }
    //  else {
    //   checkNextStepDetails.profileVerification.push("1");
    // }
  });

  // if (checkNextStepDetails.profileVerification.length === 0)
  if (
    mandatoryProfileDocCount === 0 ||
    (checkNextStepDetails.profileVerification.length === 0 &&
      mandatoryProfileDocCount === uploadedProfileDocCount)
  ) {
    nextStepDetails.profileVerification = false;
  }
  //#endregion Profile Documents
  //#region Check Vehicle Documents
  // let mandatoryVehicleDocCount = 0,
  //   docusvehi = [];
  await professionalData.vehicles.map(async (vehicle) => {
    // mandatoryVehicleDocCount = 0;
    // docusvehi = [];
    //
    if (vehicle.status === constantUtil.INACTIVE) {
      checkNextStepDetails.vehicleVerified.push("1");
      nextStepDetails.vehicleIds.push(vehicle._id?.toString());
    }
    // if (vehicle.vehicleDocuments.length > 0) {
    //#region Vehicle Documents
    const vehicleServiceTypeId = vehicle.serviceTypeId?.toString();
    const vehicleDocumentList = await Object.keys(driverDocumentList)
      .map((documentId) => ({
        ...driverDocumentList[documentId],
        // "id": documentId,
      }))
      .filter(function (el) {
        if (
          el.status === constantUtil.ACTIVE &&
          el.docsServiceTypeId === vehicleServiceTypeId
        )
          return el;
      });
    //#endregion Vehicle Documents
    const mandatoryVehicleDocCount = vehicleDocumentList.filter(
      (item) => item.docsMandatory === true
    ).length;
    const docusvehi = vehicle.vehicleDocuments;
    vehicleDocumentList?.forEach(async (docs) => {
      // if (
      //   mandatoryVehicleDocCount === docusvehi.length ||
      //   docs.docsMandatory === false
      // )
      // {
      // docusvehi?.forEach((c) => {
      //   // if (docs.docsName === c.documentName) {
      //   if (docs.docsName === c.documentName && docs.docsMandatory === true) {
      //     if (docs.docsExpiry === true) {
      //       const diffDays = parseInt(
      //         (new Date(c.expiryDate) - new Date()) /
      //           helperUtil.getDaysToMilliSeconds(1)
      //       ); //gives day difference
      //       if (
      //         c.uploadedStatus === constantUtil.UPLOADED &&
      //         c.status === constantUtil.VERIFIED
      //       ) {
      //         checkNextStepDetails.vehicleVerified.push("1");
      //         nextStepDetails.vehicleIds.push(vehicle._id?.toString());
      //       }
      //       if (diffDays <= 0 || c.status === constantUtil.REJECTED) {
      //         checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
      //         nextStepDetails.vehicleIds.push(vehicle._id?.toString());
      //       }
      //     } else {
      //       if (
      //         c.uploadedStatus === constantUtil.UPLOADED &&
      //         c.status === constantUtil.VERIFIED
      //       ) {
      //         checkNextStepDetails.vehicleVerified.push("1");
      //         nextStepDetails.vehicleIds.push(vehicle._id?.toString());
      //       }
      //     }
      //   }
      // });
      docusvehi
        ?.filter((each) => {
          return (
            docs.docsName === each.documentName && docs.docsMandatory === true
          );
        })
        ?.forEach((c) => {
          if (docs.docsExpiry === true) {
            const diffDays = parseInt(
              (new Date(c.expiryDate) - new Date()) /
                helperUtil.getDaysToMilliSeconds(1)
            ); //gives day difference
            if (
              c.uploadedStatus === constantUtil.UPLOADED &&
              c.status === constantUtil.VERIFIED
            ) {
              checkNextStepDetails.vehicleVerified.push("1");
              nextStepDetails.vehicleIds.push(vehicle._id?.toString());
            }
            if (diffDays <= 0 || c.status === constantUtil.REJECTED) {
              checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
              nextStepDetails.vehicleIds.push(vehicle._id?.toString());
            }
          } else {
            if (
              c.uploadedStatus === constantUtil.UPLOADED &&
              c.status === constantUtil.VERIFIED
            ) {
              checkNextStepDetails.vehicleVerified.push("1");
              nextStepDetails.vehicleIds.push(vehicle._id?.toString());
            }
          }
        });
      // }
      // else {
      //   checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
      //   nextStepDetails.vehicleIds.push(vehicle._id?.toString());
      // }
    });
    // } else {
    //   checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
    // }

    // if (vehicle.hub && vehicle.scheduleDate)
    if (vehicle?.serviceCategory) {
      nextStepDetails.isHubDetailsPresent = false;
    }
    if (vehicle.vehicleCategoryId) {
      vehicle.vehicleCategoryName =
        vehicleCategoryList[vehicle.vehicleCategoryId]?.vehicleCategory;
    } else {
      vehicle.vehicleCategoryName = "";
    }
  });
  //
  await professionalData.vehicles?.filter((item) => {
    if (item.defaultVehicle && item?.serviceCategory) {
      nextStepDetails.isHubDetailsPresent = false;
    }
  });
  //
  if (professionalData.vehicles.length === 0) {
    nextStepDetails.vehiclesVerification = true;
  }
  if (checkNextStepDetails.isVehicleDocumentsUploaded.length === 0) {
    nextStepDetails.isVehicleDocumentsUploaded = false;
  }
  if (checkNextStepDetails.vehicleVerified.length === 0) {
    nextStepDetails.vehicleVerified = false;
  }
  // if (
  //   checkNextStepDetails.vehicleVerified.length === 0 &&
  //   mandatoryVehicleDocCount === docusvehi.length
  // ) {
  //   nextStepDetails.vehicleVerified = false;
  // }
  //#endregion Check Vehicle Documents

  if (professionalData.otpReceiverAppName) {
    // if (nextStepDetails.isHaveServiceType) {
    //   professionalData.registrationNextStep =
    //     constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS;
    // } else {
    //   professionalData.registrationNextStep =
    //     constantUtil.REGISTRATIONSTEP_PROFILE;
    // }
    professionalData.registrationNextStep = !professionalData.newProfessional
      ? nextStepDetails.isHaveServiceType
        ? constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
        : nextStepDetails.isHubDetailsPresent
        ? constantUtil.REGISTRATIONSTEP_HUBDETAILS
        : nextStepDetails.profileVerification
        ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS // constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
        : nextStepDetails.vehiclesVerification
        ? constantUtil.REGISTRATIONSTEP_VEHICLEDETAILS
        : nextStepDetails.isVehicleDocumentsUploaded
        ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS
        : nextStepDetails.vehicleVerified
        ? constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
        : constantUtil.REGISTRATIONCOMPLETED
      : constantUtil.REGISTRATIONSTEP_PROFILE;
  } else {
    professionalData.registrationNextStep = !professionalData.newProfessional
      ? nextStepDetails.profileVerification
        ? constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS
        : nextStepDetails.vehiclesVerification
        ? constantUtil.REGISTRATIONSTEP_VEHICLEDETAILS
        : nextStepDetails.isVehicleDocumentsUploaded
        ? constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS
        : nextStepDetails.isHubDetailsPresent
        ? constantUtil.REGISTRATIONSTEP_HUBDETAILS
        : nextStepDetails.vehicleVerified
        ? constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
        : constantUtil.REGISTRATIONCOMPLETED
      : constantUtil.REGISTRATIONSTEP_PROFILE;
  }
  // professionalData.unverifiedVehicleIds = nextStepDetails.vehicleIds;
  professionalData.unverifiedVehicleIds = [
    ...new Set(nextStepDetails.vehicleIds),
  ];
  if (
    professionalData.isDefaultVehicle && // Is defaultVehicle
    professionalData.registrationNextStep ===
      constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION
  ) {
    await this.adapter.updateById(
      mongoose.Types.ObjectId(professionalData?._id?.toString()),
      {
        "status": constantUtil.UNVERIFIED,
      }
    );
  }
  professionalData.vehicles = _.map(professionalData.vehicles, (vehicle) => ({
    ...vehicle,
    "hubsName": vehicle?.hub?.data?.hubsName || "",
    "location": vehicle?.hub?.data?.location || {},
    "phone": vehicle?.hub?.data?.phone || {},
    "email": vehicle?.hub?.data?.email || "",
    "showAddress": vehicle?.hub?.data?.showAddress || "",
    // "hub": vehicle?.hub?._id || vehicle?.hub,
    "hub": vehicle?.hub?._id?.toString() || "",
  }));
  return professionalData;
};

//#region Professional Documents End
professionalEvent.addProfessionalNotes = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    },
    {
      "$set": { "notes": context.params.notes },
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
professionalEvent.addVehicleNotes = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    },
    {
      "$set": { "vehicles.$[details].notes": context.params.notes },
    },
    {
      "arrayFilters": [{ "details._id": { "$eq": context.params.id } }],
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATED" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
professionalEvent.updateLiveLocation = async function (context) {
  const dataResult = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      },
      { "onlineStatus": 1, "locationUpdatedTime": 1 }
    )
    .lean();
  // const updatedProfessionalData = await this.adapter.updateById(
  //   mongoose.Types.ObjectId(context.params.professionalId),
  //   {
  //     "location.coordinates": [
  //       parseFloat(context.params.lng),
  //       parseFloat(context.params.lat),
  //     ],
  //     "currentBearing": context.params.currentBearing,
  //     "altitude": context.params.altitude,
  //     "horizontalAccuracy": context.params.horizontalAccuracy,
  //     "verticalAccuracy": context.params.verticalAccuracy,
  //     "speed": context.params.speed,
  //     "isLocationUpdated": true,
  //     "locationUpdatedTime": new Date(),
  //     "onlineStatus": dataResult.onlineStatus,
  //   },
  //   { "returnOriginal": false }
  // );
  console.log(
    "_____updateLiveLocation_Event=" + context.params.professionalId + ""
  );
  let onlineStatusData = {
    "onlineStatusBy": constantUtil.CONST_MANUAL,
  };
  if (context.params?.onlineStatus) {
    onlineStatusData = {
      "onlineStatus": context.params.onlineStatus,
      "onlineStatusBy": constantUtil.CONST_MANUAL,
      "onlineStatusUpdatedAt": new Date(),
    };
  }
  const updatedProfessionalData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      },
      {
        "location.coordinates": [
          parseFloat(context.params.lng),
          parseFloat(context.params.lat),
        ],
        "currentBearing": context.params.currentBearing,
        "altitude": context.params.altitude,
        "horizontalAccuracy": context.params.horizontalAccuracy,
        "verticalAccuracy": context.params.verticalAccuracy,
        "speed": context.params.speed,
        "isLocationUpdated": true,
        "locationUpdatedTime": new Date(),
        // "onlineStatus": dataResult.onlineStatus,
        ...onlineStatusData,
      },
      { "new": true }
    )
    .lean();
  if (
    dataResult?.onlineStatus === false &&
    context.params?.onlineStatus === true
  ) {
    //Insert Signup log
    await this.broker.emit("log.insertSignupLog", {
      "type": constantUtil.CONST_INCENTIVE,
      "name": constantUtil.LOGIN,
      "userType": constantUtil.PROFESSIONAL,
      "professionalId": context.params.professionalId?.toString(),
    });
  }
  console.log("_____updateLiveLocation_Event______");
  if (!updatedProfessionalData)
    return {
      "code": 0,
      "data": {},
      "message": "LOCATION UPDATE FAILED",
    };
  return {
    "code": 1,
    "data": {
      "onlineStatus": updatedProfessionalData.onlineStatus,
      "isPriorityStatus": updatedProfessionalData.isPriorityStatus,
      "bookingInfo": updatedProfessionalData.bookingInfo,
      "phone": updatedProfessionalData.phone.number,
    },
    "message": "LOCATION UPDATED SUCCESSFULLY",
  };
};
professionalEvent.updateAllOnlineProfessionalStatus = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const { WARNING_CRON_OFFLINE_ALERT } = notifyMessage.setNotifyLanguage(
    generalSettings?.data?.languageCode || constantUtil.DEFAULT_LANGUAGE
  );
  const nowDate =
    Date.now() - generalSettings.data.onlineStatusThreshold * 60 * 1000;
  const updatedProfessionalData = await this.adapter.updateMany(
    {
      "status": constantUtil.ACTIVE,
      "onlineStatus": true,
      "isLocationUpdated": true,
      "locationUpdatedTime": { "$lte": new Date(nowDate) },
      "bookingInfo.ongoingBooking": null,
      "bookingInfo.ongoingTailBooking": null,
    },
    {
      "onlineStatus": false,
      "onlineStatusBy": constantUtil.AUTOMATED,
      "onlineStatusUpdatedAt": new Date(),
    }
  );
  const professionalList = await this.adapter.model
    .find(
      {
        "status": constantUtil.ACTIVE,
        "onlineStatus": false,
        "onlineStatusBy": constantUtil.AUTOMATED,
        "onlineStatusUpdatedAt": {
          "$gte": new Date(new Date().getTime() - 1000 * 60 * 1),
        }, // 1 minutes ago (from now) / Last 1 minutes Updated Records
      },
      { "_id": 1, "onlineStatus": 1, "updatedAt": 1, "deviceInfo": 1 }
    )
    .lean();
  // Update Logout Time in Log
  if (generalSettings?.data?.isEnableIncentive) {
    await professionalList?.map(async (professional) => {
      await this.broker.emit("log.updateSignupLog", {
        "name": constantUtil.LOGOUT,
        "userType": constantUtil.PROFESSIONAL,
        "professionalId": professional._id?.toString(),
      });
    });
  }
  // #region Send Push Notification
  if (professionalList.length > 0) {
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_ASSIGN,
        "timestamp": Date.now(),
        "message": WARNING_CRON_OFFLINE_ALERT,
        "details": lzStringEncode({}),
      },
      "registrationTokens": professionalList?.map((professional) => {
        return {
          "token": professional?.deviceInfo[0]?.deviceId || "",
          "id": professional?._id?.toString(),
          "deviceType": professional?.deviceInfo[0]?.deviceType || "",
          "platform": professional?.deviceInfo[0]?.platform || "",
          "socketId": professional?.deviceInfo[0]?.socketId || "",
        };
      }),
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  // #endregion Send Push Notification
  console.log("_____updateAllOnlineProfessionalStatus______");
  if (!updatedProfessionalData) {
    return { "code": 0, "data": {}, "message": "STATUS UPDATE FAILD BY CRON" };
  } else {
    return {
      "code": 1,
      "data": {},
      "message": "STATUS UPDATED SUCCESSFULLY BY CRON",
    };
  }
};

professionalEvent.sendMinimumAmountWarningNotification = async function (
  context
) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const { PROFESSIONAL_INFO } = notifyMessage.setNotifyLanguage(
    generalSettings?.data?.languageCode || constantUtil.DEFAULT_LANGUAGE
  );
  const professionalList = await this.adapter.model
    .find(
      {
        "status": constantUtil.ACTIVE,
        "wallet.availableAmount": {
          "$lt": generalSettings.data.walletMinAmountThreshold,
        },
        "$or": [
          { "lastWalletAlertSendDate": { "$lt": Date.now() } },
          { "lastWalletAlertSendDate": null },
        ],
      },
      { "_id": 1, "onlineStatus": 1, "updatedAt": 1, "deviceInfo": 1 }
    )
    .lean();
  // #region Send Push Notification
  if (professionalList.length > 0) {
    const toDate = new Date(new Date().setHours(23, 59, 59, 999));
    let professionalIDs = [];
    professionalList.forEach((item) => {
      professionalIDs.push(mongoose.Types.ObjectId(item._id?.toString()));
    });
    const responseJson = await this.adapter.model.updateMany(
      { "_id": { "$in": professionalIDs } },
      { "lastWalletAlertSendDate": toDate }
    );
    if (responseJson.nModified) {
      const notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_ASSIGN,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_INFO,
          "details": lzStringEncode({}),
        },
        "registrationTokens": professionalList?.map((professional) => {
          return {
            "token": professional?.deviceInfo[0]?.deviceId || "",
            "id": professional?._id?.toString(),
            "deviceType": professional?.deviceInfo[0]?.deviceType || "",
            "platform": professional?.deviceInfo[0]?.platform || "",
            "socketId": professional?.deviceInfo[0]?.socketId || "",
          };
        }),
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
  }
  // #endregion Send Push Notification
};

professionalEvent.getById = async function (context) {
  const professionalData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      // "status": constantUtil.ACTIVE,
    })
    .lean();

  if (!professionalData) {
    return null;
  } else {
    return professionalData;
  }
};
professionalEvent.updateOnGoingBooking = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "status": constantUtil.ACTIVE,
    },
    {
      "bookingInfo.ongoingBooking": context.params.bookingId
        ? context.params.bookingId
        : null,
      "isPriorityStatus": false,
    }
  );

  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
professionalEvent.updateOnGoingAndTailBooking = async function (context) {
  const professionalData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.professionalId),
    "status": constantUtil.ACTIVE,
  });
  let updateData;
  if (professionalData && professionalData.bookingInfo) {
    if (
      professionalData?.bookingInfo?.ongoingBooking?.toString() ===
      context.params.bookingId.toString()
    ) {
      if (professionalData.bookingInfo.ongoingTailBooking === null) {
        updateData = {
          "bookingInfo.ongoingBooking": null,
        };
      } else {
        updateData = {
          "bookingInfo.ongoingBooking":
            professionalData.bookingInfo.ongoingTailBooking,
          "bookingInfo.ongoingTailBooking": null,
        };
      }
    } else {
      updateData = {
        "bookingInfo.ongoingTailBooking": null,
      };
    }
  }
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
professionalEvent.updateTailOnGoingBooking = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "status": constantUtil.ACTIVE,
    },
    {
      "bookingInfo.ongoingTailBooking": context.params.bookingId
        ? context.params.bookingId
        : null,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
professionalEvent.updatependingReview = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      // "status": constantUtil.ACTIVE,
    },
    {
      "bookingInfo.ongoingBooking": null,
      "bookingInfo.pendingReview": context.params.bookingId
        ? context.params.bookingId
        : null,
      "lastCurrencyCode": context.params.lastCurrencyCode,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

professionalEvent.updateRating = async function (context) {
  let updateProfessionalData = {};
  try {
    const professionalData = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.professionalId.toString())
      )
      .lean();
    let updateData;
    const avgRating =
      (parseFloat(professionalData?.review?.avgRating || 0) *
        parseInt(professionalData?.review?.totalReview || 0) +
        parseFloat(context.params.rating)) /
      (parseFloat(professionalData?.review?.totalReview || 0) + 1);

    const totalReview =
      parseInt(professionalData?.review?.totalReview || 0) + 1;

    if (
      !professionalData.bookingInfo.ongoingTailBooking &&
      professionalData.bookingInfo.ongoingTailBooking === null
    ) {
      updateData = {
        "review.avgRating": avgRating,
        "review.totalReview": totalReview,
        "bookingInfo.pendingReview": null,
        // "bookingInfo.ongoingBooking": null,
        "bookingInfo.ongoingTailBooking": null,
      };
    } else {
      updateData = {
        "review.avgRating": avgRating,
        "review.totalReview": totalReview,
        "bookingInfo.pendingReview": null,
        "bookingInfo.ongoingTailBooking": null,
        "bookingInfo.ongoingBooking":
          professionalData.bookingInfo.ongoingTailBooking,
      };
    }

    updateProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.professionalId),
          // "status": constantUtil.ACTIVE,
        },
        updateData,
        { "new": true }
      )
      .lean();
    // if (!updateProfessionalData.nModified) {
    //   return null;
    // } else {
    // return updateProfessionalData;
    // }
  } catch (e) {
    updateProfessionalData = {};
  }
  return updateProfessionalData;
};

professionalEvent.updateEscortRating = async function (context) {
  const professionalData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.professionalId.toString()))
    .lean();
  const avgRating =
    (parseFloat(
      professionalData.escortReview.avgRating
        ? professionalData.escortReview.avgRating
        : 0
    ) *
      parseInt(
        professionalData.escortReview.totalReview
          ? professionalData.escortReview.totalReview
          : 0
      ) +
      parseFloat(context.params.rating)) /
    (parseFloat(
      professionalData.escortReview.totalReview
        ? professionalData.escortReview.totalReview
        : 0
    ) +
      1);

  const totalReview =
    parseInt(
      professionalData.escortReview.totalReview
        ? professionalData.escortReview.totalReview
        : 0
    ) + 1;

  const updateProfessionalData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortReview.avgRating": avgRating,
      "escortReview.totalReview": totalReview,
      "escortBookingInfo.pendingReview": null,
    }
  );
  if (!updateProfessionalData.nModified) {
    return null;
  } else {
    return updateProfessionalData;
  }
};
professionalEvent.professionalDocumentExpiryNotify = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
    { "notes": new Date() }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN VERIFICATION" };
  } else {
    return { "code": 1, "data": {}, "message": "VERIFIED SUCCESSFULLY" };
  }
};
professionalEvent.getVehicleList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  query.push(
    {
      "$match": {
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      },
    },
    { "$unwind": "$vehicles" }
  );
  query.push(
    context.params.filter && context.params.filter != ""
      ? { "$match": { "status": { "$eq": context.params.filter } } }
      : {
          "$match": {
            "status": {
              "$in": [
                constantUtil.ACTIVE,
                constantUtil.INACTIVE,
                constantUtil.UNVERIFIED,
              ],
            },
          },
        }
  );

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "vehicles.plateNumber": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "vehicles.vinNumber": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "vehicles._id": -1 } },
        { "$skip": skip },
        { "$limit": limit },
        {
          "$lookup": {
            "from": "admins",
            "localField": "vehicles.vehicleCategoryId",
            "foreignField": "_id",
            "as": "vehicleCategoryData",
          },
        },
        {
          "$unwind": {
            "path": "$vehicleCategoryData",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "professionalId": "$_id",
            "_id": "$vehicles._id",
            "plateNumber": "$vehicles.plateNumber",
            "type": "$vehicles.type",
            "maker": "$vehicles.maker",
            "model": "$vehicles.model",
            "year": "$vehicles.year",
            "vehicleCategory": "$vehicleCategoryData.data.vehicleCategory",
            "isEnableShareRide": "$vehicles.isEnableShareRide",
            "defaultVehicle": "$vehicles.defaultVehicle",
            "status": "$vehicles.status",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
professionalEvent.editVehicleDetails = async function (context) {
  const responseJson = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      },
      {
        "serviceType": 1,
        "serviceTypeId": 1,
        "serviceAreaId": 1,
        "vehicles": {
          "$elemMatch": { "_id": context.params.id },
        },
      }
    )
    .lean();
  if (!responseJson)
    return { "code": 0, "data": {}, "message": "ERROR IN VERIFICATION" };

  return {
    "code": 1,
    "data": {
      ...responseJson?.vehicles?.[0],
      // "serviceType": responseJson.serviceType,
      // "serviceTypeId": responseJson.serviceTypeId,
      "serviceType":
        responseJson?.vehicles?.[0]?.serviceType || responseJson.serviceType,
      "serviceTypeId":
        responseJson?.vehicles?.[0]?.serviceTypeId ||
        responseJson.serviceTypeId,
      "serviceAreaId": responseJson.serviceAreaId,
    },
    "message": "",
  };
};
professionalEvent.professionalVerification = async function (context) {
  const professionalData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.professionalId.toString()))
    .lean();
  if (!professionalData)
    return { "code": 0, "data": {}, "message": "INVALID CREDENTIALS" };
  const professionalServiceTypeId = professionalData?.serviceTypeId?.toString();
  //#region Profile Documents
  let profileDocumentList = await storageutil.read(
    constantUtil.PROFILEDOCUMENTS
  );
  if (!profileDocumentList)
    return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  profileDocumentList = Object.keys(profileDocumentList)
    .map((documentId) => ({
      ...profileDocumentList[documentId],
      // "id": documentId,
    }))
    .filter(function (el) {
      if (
        el.status === constantUtil.ACTIVE &&
        el.docsServiceTypeId === professionalServiceTypeId
      )
        return el;
    });
  //#endregion Profile Documents
  //#region Vehicle & Driver Documents
  let driverDocumentList = await storageutil.read(constantUtil.DRIVERDOCUMENTS);
  if (!driverDocumentList)
    return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

  driverDocumentList = await Object.keys(driverDocumentList)
    .map((documentId) => ({
      ...driverDocumentList[documentId],
      // "id": documentId,
    }))
    .filter(function (el) {
      if (
        el.status === constantUtil.ACTIVE &&
        el.docsServiceTypeId === professionalServiceTypeId
      )
        return el;
    });
  //#endregion Vehicle & Driver Documents
  let isDriverVerify = 1;
  const profileDocs = professionalData.profileVerification;
  const vehicles = professionalData.vehicles;
  const driDocs = [];
  const vehicleDocs = [];

  await profileDocumentList.forEach(async (docs) => {
    if (docs.status == constantUtil.ACTIVE) {
      profileDocs.forEach((d) => {
        if (docs.docsName == d.documentName) {
          if (docs.docsExpiry === true) {
            const diffDays = parseInt(
              (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
            ); //gives day difference
            if (diffDays <= 0) {
              d["status"] = constantUtil.UNVERIFIED;
              isDriverVerify = 0;
            }
          } else {
            d["expiryDate"] = "";
            d["status"] = constantUtil.VERIFIED;
          }
          driDocs.push(d);
        }
      });
    }
  });
  await vehicles.forEach((v) => {
    const docusvehi = v.vehicleDocuments;
    driverDocumentList.forEach(async (docs) => {
      if (docs.status == constantUtil.ACTIVE) {
        docusvehi.forEach((c) => {
          if (docs.docsName == c.documentName) {
            if (docs.docsExpiry === true) {
              const diffDays = parseInt(
                (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
              ); //gives day difference
              if (diffDays <= 0) {
                v.status = constantUtil.INACTIVE;
                c["status"] = constantUtil.UNVERIFIED;
                isDriverVerify = 0;
              }
            } else {
              v.status = constantUtil.ACTIVE;
              c["expiryDate"] = "";
              c["status"] = constantUtil.VERIFIED;
            }
            vehicleDocs.push(c);
          }
        });
      }
    });
    if (v.defaultVehicle === true && v.vehicleCategoryId == "undefined")
      isDriverVerify = 2;

    v["vehicleDocuments"] = vehicleDocs;
  });
  if (isDriverVerify == 0) {
    return {
      "code": 0,
      "data": {},
      "message": "KINDLY VERIFY ALL DOCUMENTS",
    };
  } else if (isDriverVerify == 2) {
    return {
      "code": 0,
      "data": {},
      "message": "KINDLY CHECK VEHICLE DETAILS SOME FIELDS IS MISSING",
    };
  } else {
    const updateProfessionalData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
      },
      {
        "vehicles": vehicles,
        "profileVerification": driDocs,
        "status": constantUtil.ACTIVE,
      }
    );
    if (!updateProfessionalData.nModified)
      return {
        "code": 0,
        "data": {},
        "message": "ERROR IN PROFESSIONAL VERIFICATION",
      };
  }

  return { "code": 1, "data": {}, "message": "VERIFIED SUCCESSFULLY" };
};

professionalEvent.getUnverifiedProfessionalList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        "vehicles.hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;

    case constantUtil.HUBSEMPLOYEE:
      professionalVehicleHubQuery = {
        "createdBy": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  const search = context.params?.search || "";
  const query = {
    ...professionalVehicleHubQuery,
    "status": constantUtil.UNVERIFIED,
    "isExpired": false,
    // "clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-_id",
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  // let profileDocumentList = await storageutil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );
  // if (!profileDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  // let driverDocumentList = await storageutil.read(constantUtil.DRIVERDOCUMENTS);
  // if (!driverDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };
  // const professionalServiceType = constantUtil.CONST_RIDE;
  // //#region Profile Documents
  // profileDocumentList = Object.keys(profileDocumentList)
  //   .map((documentId) => ({
  //     ...profileDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
  //     if (
  //       el.status === constantUtil.ACTIVE &&
  //       serviceType === professionalServiceType
  //     )
  //       return el;
  //   });
  // //#endregion Profile Documents
  // //#region Vehicle & Driver Documents
  // driverDocumentList = await Object.keys(driverDocumentList)
  //   .map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
  //     if (
  //       el.status === constantUtil.ACTIVE &&
  //       serviceType === professionalServiceType
  //     )
  //       return el;
  //   });
  // //#endregion Vehicle & Driver Documents
  //#region professional, Vehicle & Driver Documents
  // const documentListRide = await helperUtil.getRegistrationDocumentFromRedis(
  //   constantUtil.CONST_RIDE
  // );
  // const documentListDelivery =
  //   await helperUtil.getRegistrationDocumentFromRedis(
  //     constantUtil.CONST_DELIVERY
  //   );
  // const allDocumentList = await helperUtil.getRegistrationDocumentFromRedis(
  //   constantUtil.ALL
  // );
  // //#endregion professional, Vehicle & Driver Documents

  // const resultJson = [];
  // await jsonData.map(async (professional) => {
  //   professional = professional.toJSON();
  //   const driDocs = [];
  //   const vehicleDocs = [];
  //   // const professionalServiceType =
  //   //   professional.serviceType || constantUtil.CONST_RIDE;
  //   const professionalServiceTypeId = professional.serviceTypeId?.toString();
  //   // const documentList =
  //   //   await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
  //   //     "64df4dde54d15e036ad18501" //professionalServiceTypeId
  //   //   );
  //   const documentList = null;
  //   // switch (professionalServiceType) {
  //   //   case constantUtil.CONST_RIDE:
  //   //     documentList = documentListRide;
  //   //     break;

  //   //   case constantUtil.CONST_DELIVERY:
  //   //     documentList = documentListDelivery;
  //   //     break;

  //   //   case constantUtil.ALL:
  //   //     documentList = allDocumentList;
  //   //     break;
  //   // }
  //   const profileDocumentList = documentList?.profileDocumentRideList || [];
  //   const driverDocumentList = documentList?.driverDocumentRideList || [];
  //   //
  //   await profileDocumentList.forEach(async (docs) => {
  //     if (docs.status == constantUtil.ACTIVE) {
  //       professional.profileVerification.forEach((d) => {
  //         if (docs.docsName == d.documentName) {
  //           if (docs.docsExpiry === true) {
  //             const diffDays = parseInt(
  //               (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //             ); //gives day difference
  //             d["status"] = constantUtil.VERIFIED;
  //             if (diffDays <= 0) {
  //               d["status"] = constantUtil.UNVERIFIED;
  //             }
  //           } else {
  //             d["expiryDate"] = "";
  //             d["status"] = constantUtil.VERIFIED;
  //           }
  //           driDocs.push(d);
  //         }
  //       });
  //     }
  //   });
  //   professional.vehicles.forEach((v) => {
  //     const docusvehi = v.vehicleDocuments;
  //     driverDocumentList.forEach(async (docs) => {
  //       if (docs.status == constantUtil.ACTIVE) {
  //         docusvehi.forEach((c) => {
  //           if (docs.docsName == c.documentName) {
  //             if (docs.docsExpiry === true) {
  //               const diffDays = parseInt(
  //                 (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //               ); //gives day difference
  //               c["status"] = constantUtil.VERIFIED;
  //               if (diffDays <= 0) {
  //                 c["status"] = constantUtil.UNVERIFIED;
  //               }
  //             } else {
  //               c["expiryDate"] = "";
  //               c["status"] = constantUtil.VERIFIED;
  //             }
  //             vehicleDocs.push(c);
  //           }
  //         });
  //       }
  //     });
  //     v["vehicleDocuments"] = vehicleDocs;
  //   });
  //   professional["profileVerification"] = driDocs;
  //   // return professional;
  //   resultJson.push(professional);
  // });

  responseJson["count"] = count;
  // responseJson["response"] = await resultJson.map((professional) => ({
  responseJson["response"] = await jsonData?.map((professional) => ({
    "_id": professional._id,
    "data": {
      "firstName": professional.firstName,
      "lastName": professional.lastName,
      "email": professional.email,
      "phone": professional.phone,
      "status": professional.status,
      "profileVerification": professional.profileVerification,
      "vehicles": professional.vehicles,
    },
  }));

  return { "code": 1, "data": responseJson, "message": "" };
};

professionalEvent.getExpiredProfessionalList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {
    "status": constantUtil.UNVERIFIED,
    "isExpired": true,
    // "clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-_id",
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  // let profileDocumentList = await storageutil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );
  // if (!profileDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };
  // //#region Profile Documents
  // profileDocumentList = Object.keys(profileDocumentList)
  //   .map((documentId) => ({
  //     ...profileDocumentList[documentId],
  //     // "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
  //     if (
  //       el.status === constantUtil.ACTIVE &&
  //       serviceType === constantUtil.CONST_RIDE
  //     )
  //       return el;
  //   });
  // //#endregion Profile Documents
  // let driverDocumentList = await storageutil.read(constantUtil.DRIVERDOCUMENTS);
  // if (!driverDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

  // //#region Vehicle & Driver Documents
  // driverDocumentList = await Object.keys(driverDocumentList)
  //   .map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     // "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
  //     if (
  //       el.status === constantUtil.ACTIVE &&
  //       serviceType === constantUtil.CONST_RIDE
  //     )
  //       return el;
  //   });
  // //#endregion Vehicle & Driver Documents

  // //#region professional, Vehicle & Driver Documents
  // const documentListRide = await helperUtil.getRegistrationDocumentFromRedis(
  //   constantUtil.CONST_RIDE
  // );
  // const documentListDelivery =
  //   await helperUtil.getRegistrationDocumentFromRedis(
  //     constantUtil.CONST_DELIVERY
  //   );
  // const allDocumentList = await helperUtil.getRegistrationDocumentFromRedis(
  //   constantUtil.ALL
  // );
  // //#endregion professional, Vehicle & Driver Documents
  // const resultJson = [];
  // await jsonData.map(async (professional) => {
  //   professional = professional.toJSON();
  //   const driDocs = [];
  //   const vehicleDocs = [];
  //   const professionalServiceType =
  //     professional.serviceType || constantUtil.CONST_RIDE;
  //   // const documentList = await helperUtil.getRegistrationDocumentFromRedis(
  //   //   professionalServiceType
  //   // );
  //   let documentList = null;
  //   switch (professionalServiceType) {
  //     case constantUtil.CONST_RIDE:
  //       documentList = documentListRide;
  //       break;

  //     case constantUtil.CONST_DELIVERY:
  //       documentList = documentListDelivery;
  //       break;

  //     case constantUtil.ALL:
  //       documentList = allDocumentList;
  //       break;
  //   }
  //   const profileDocumentList = documentList?.profileDocumentRideList || [];
  //   const driverDocumentList = documentList?.driverDocumentRideList || [];
  //   //
  //   profileDocumentList.forEach(async (docs) => {
  //     if (docs.status == constantUtil.ACTIVE) {
  //       professional.profileVerification.forEach((d) => {
  //         if (docs.docsName == d.documentName) {
  //           if (docs.docsExpiry === true) {
  //             const diffDays = parseInt(
  //               (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //             ); //gives day difference
  //             d["status"] = constantUtil.VERIFIED;
  //             if (diffDays <= 0) {
  //               d["status"] = constantUtil.UNVERIFIED;
  //             }
  //           } else {
  //             d["expiryDate"] = "";
  //             d["status"] = constantUtil.VERIFIED;
  //           }
  //           driDocs.push(d);
  //         }
  //       });
  //     }
  //   });
  //   professional.vehicles.forEach((v) => {
  //     const docusvehi = v.vehicleDocuments;
  //     driverDocumentList.forEach(async (docs) => {
  //       if (docs.status == constantUtil.ACTIVE) {
  //         docusvehi.forEach((c) => {
  //           if (docs.docsName == c.documentName) {
  //             if (docs.docsExpiry === true) {
  //               const diffDays = parseInt(
  //                 (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //               ); //gives day difference
  //               c["status"] = constantUtil.VERIFIED;
  //               if (diffDays <= 0) {
  //                 c["status"] = constantUtil.UNVERIFIED;
  //               }
  //             } else {
  //               c["expiryDate"] = "";
  //               c["status"] = constantUtil.VERIFIED;
  //             }
  //             vehicleDocs.push(c);
  //           }
  //         });
  //       }
  //     });
  //     v["vehicleDocuments"] = vehicleDocs;
  //   });
  //   professional["profileVerification"] = driDocs;
  //   // return professional;
  //   resultJson.push(professional);
  // });

  responseJson["count"] = count;

  // responseJson["response"] = await resultJson.map((professional) => ({
  responseJson["response"] = await jsonData?.map((professional) => ({
    "_id": professional._id,
    "data": {
      "firstName": professional.firstName,
      "lastName": professional.lastName,
      "email": professional.email,
      "phone": professional.phone,
      "status": professional.status,
      "profileVerification": professional.profileVerification,
      "vehicles": professional.vehicles,
    },
  }));

  return { "code": 1, "data": responseJson, "message": "" };
};

professionalEvent.updateTrustedContact = async function (context) {
  let trustedContactsProfessionalId = null;
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "ERROR IN CONTACTS" };
  }
  if (context.params.action === "add" && jsonData.trustedContacts.length >= 5) {
    return {
      "code": 0,
      "data": {},
      "message": "ALREADY YOU HAVE 5 TRUSTED CONTACTS",
    };
  }
  let trustedContacts = false;
  if (jsonData.trustedContacts.length > 0 && context.params.contacts)
    await jsonData.trustedContacts.map((contacts) => {
      if (
        contacts.phone.code === context.params.contacts.phone.code &&
        contacts.phone.number === context.params.contacts.phone.number
      ) {
        trustedContacts = true;
      }
    });
  if (trustedContacts === true) {
    return { "code": 0, "data": {}, "message": "CONTACTS ALREADY EXIST" };
  }
  if (context.params.action === "add") {
    trustedContactsProfessionalId = await this.adapter.model
      .findOne(
        {
          "_id": {
            "$ne": mongoose.Types.ObjectId(context.params.professionalId),
          },
          "phone.code": context.params.contacts.phone.code,
          "phone.number": context.params.contacts.phone.number,
        },
        { "_id": 1 }
      )
      .lean();
  }
  //
  const updateContacts =
    context.params.action === "add"
      ? {
          "$push": {
            "trustedContacts": {
              "_id": context.params.contacts._id,
              "professionalId":
                trustedContactsProfessionalId?._id?.toString() || null,
              "name": context.params.contacts.name,
              "phone": context.params.contacts.phone,
              "reason": context.params.contacts.reasons,
            },
          },
        }
      : {
          "$pull": {
            "trustedContacts": {
              "_id": mongoose.Types.ObjectId(context.params.id),
            },
          },
        };

  const responseData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    },
    updateContacts
  );
  if (!responseData.nModified) {
    return { "code": 200, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

professionalEvent.changeTrustedContactReason = async function (context) {
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE REASON" };
  }
  // if (jsonData.trustedContacts.length >= 5) {
  //   return {
  //     "code": 0,
  //     "data": {},
  //     "message": "ALREADY YOU HAVE 5 TRUSTED CONTACTS",
  //   };
  // }
  let trustedContacts = false;
  if (jsonData.trustedContacts.length > 0 && context.params.contacts)
    await jsonData.trustedContacts.map((contacts) => {
      if (
        contacts._id.toString() !== context.params.id.toString() &&
        contacts.phone.number === context.params.contacts.phone.number
      ) {
        trustedContacts = true;
      }
    });
  if (trustedContacts === true) {
    return { "code": 0, "data": {}, "message": "CONTACTS ALREADY EXIST" };
  }
  const trustedContactsData = jsonData.trustedContacts.map((contacts) => {
    if (contacts._id.toString() === context.params.id.toString()) {
      contacts.name = context.params.contacts.name;
      contacts.phone = context.params.contacts.phone;
      contacts.reason = context.params.contacts.reasons;
    }
    return contacts;
  });

  const responseData = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
    { "trustedContacts": trustedContactsData }
  );
  if (!responseData.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

professionalEvent.getTrustedContactsList = async function (context) {
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 200, "data": {}, "message": "ERROR IN TRUSTED CONTACT" };
  } else {
    return {
      "code": 200,
      "data":
        jsonData?.trustedContacts?.map((contacts) => {
          return {
            "_id": contacts._id,
            "professionalId": contacts.professionalId,
            "name": contacts.name,
            "phone": contacts.phone,
            "actionType":
              contacts.reason?.action?.type || constantUtil.CONST_MANUAL,
            "fromTime": contacts.reason?.fromTime || null,
            "toTime": contacts.reason?.toTime || null,
            "reason": {
              "_id": contacts.reason._id,
              "reason": contacts.reason.reason,
            },
          };
        }) || [],
      // "trustedContactsLog": jsonData.trustedContactsLog || [],
      "message": "",
    };
  }
};
professionalEvent.updateSubscriptionPlan = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.professionalId.toString()),
    {
      "subscriptionInfo.isSubscribed": true,
      "subscriptionInfo.planId": context.params.planId,
      "subscriptionInfo.subscribedAt": context.params.subscribedAt,
      "subscriptionInfo.subscribedEndAt": context.params.subscribedEndAt,
      "subscriptionInfo.subscribedAtTimestamp":
        context.params.subscribedAtTimestamp,
      "subscriptionInfo.subscribedEndAtTimestamp":
        context.params.subscribedEndAtTimestamp,
      "wallet.availableAmount": context.params.price,
    }
  );
  if (!updateData) {
    return { "code": 0, "data": {}, "message": "FAILED TO UPDATE LOCATION" };
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "LOCATION UPDATED SUCCESSFULLY",
    };
  }
};

professionalEvent.getProfessionalByAdmin = async function (context) {
  let query = {};
  if (
    context.params.vehicleCategoryId &&
    context.params.vehicleCategoryId !== constantUtil.ALL
  ) {
    // query["vehicles.vehicleCategoryId"] = mongoose.Types.ObjectId(
    //   context.params.vehicleCategoryId
    // );
    query = {
      "vehicles": {
        "$elemMatch": {
          "defaultVehicle": true,
          // "status": "ACTIVE",
          "vehicleCategoryId": mongoose.Types.ObjectId(
            context.params.vehicleCategoryId
          ),
        },
      },
    };
  }
  if (context.params.status !== constantUtil.ALL) {
    query["status"] = context.params.status;
  }
  if (context.params.onlineOffline !== constantUtil.ALL) {
    query["onlineStatus"] =
      context.params.onlineOffline === constantUtil.CONST_ONLINE ? true : false;
  }
  if (context.params.onGoing) {
    query["bookingInfo.ongoingBooking"] = { "$ne": null };
  }
  const professionalData = await this.adapter.model
    .find({
      ...query,
      "location": {
        "$geoWithin": {
          "$geometry": {
            "type": "Polygon",
            "coordinates": context.params.coordinates,
          },
        },
      },
    })
    .populate("vehicles.vehicleCategoryId", { "data.categoryMapImage": 1 })
    .lean();
  const responseJson = [];
  await professionalData.forEach(async (professional) => {
    let catData = "";
    await professional.vehicles.forEach((vehicle) => {
      if (vehicle.defaultVehicle) {
        catData = vehicle.vehicleCategoryId;
      }
    });
    const professionalDataSet = {
      "_id": professional._id,
      "clientId": professional?.clientId?.toString(),
      "location": professional.location,
      "firstName": professional.firstName,
      "lastName": professional.lastName,
      "email": professional.email,
      "phone": professional.phone,
      "onlineStatus": professional.onlineStatus,
      "isOngoingBookingStatus": professional.bookingInfo.ongoingBooking,
      "status": professional.status,
      "vehicleCategoryId": catData?._id?.toString() || null,
      "categoryMapImage": catData?.data?.categoryMapImage || null,
    };
    responseJson.push(professionalDataSet);
  });
  return responseJson;
};

// professionalEvent.isProfessionalVerified = async function (context) {
//   const professionalData = context.params;
//   if (!professionalData) return false;

//   let profileDocumentList = await storageutil.read(
//     constantUtil.PROFILEDOCUMENTS
//   );
//   if (!profileDocumentList)
//     return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

//   profileDocumentList = Object.keys(profileDocumentList)
//     .map((documentId) => ({
//       ...profileDocumentList[documentId],
//       "id": documentId,
//     }))
//     .filter(function (el) {
//       return el.status === constantUtil.ACTIVE;
//     });

//   let driverDocumentList = await storageutil.read(constantUtil.DRIVERDOCUMENTS);
//   if (!driverDocumentList)
//     return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

//   driverDocumentList = await Object.keys(driverDocumentList)
//     .map((documentId) => ({
//       ...driverDocumentList[documentId],
//       "id": documentId,
//     }))
//     .filter(function (el) {
//       return el.status === constantUtil.ACTIVE;
//     });

//   let isDriverVerify = true;
//   const profileDocs = professionalData.profileVerification;
//   const vehicles = professionalData.vehicles;

//   await profileDocumentList.forEach(async (docs) => {
//     if (docs.status == constantUtil.ACTIVE) {
//       profileDocs.forEach((d) => {
//         if (docs.docsName == d.documentName) {
//           if (docs.docsExpiry === true) {
//             const diffDays = parseInt(
//               (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
//             ); //gives day difference
//             if (diffDays <= 0) {
//               d["status"] = constantUtil.UNVERIFIED;
//               isDriverVerify = false;
//             }
//           } else {
//             d["expiryDate"] = "";
//             d["status"] = constantUtil.VERIFIED;
//           }
//         }
//       });
//     }
//   });
//   await vehicles.forEach((v) => {
//     const docusvehi = v.vehicleDocuments;
//     driverDocumentList.forEach(async (docs) => {
//       if (docs.status == constantUtil.ACTIVE) {
//         docusvehi.forEach((c) => {
//           if (docs.docsName == c.documentName) {
//             if (docs.docsExpiry === true) {
//               const diffDays = parseInt(
//                 (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
//               ); //gives day difference
//               if (diffDays <= 0) {
//                 v.status = constantUtil.INACTIVE;
//                 c["status"] = constantUtil.UNVERIFIED;
//                 isDriverVerify = false;
//               }
//             } else {
//               v.status = constantUtil.ACTIVE;
//               c["expiryDate"] = "";
//               c["status"] = constantUtil.VERIFIED;
//             }
//           }
//         });
//       }
//     });
//   });
//   return isDriverVerify;
// };

// professionalEvent.cronCheckDocumentsExpire_backup = async function (context) {
//   const nowDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);

//   // const professionalData = await this.adapter.model
//   //   .find({
//   //     "status": constantUtil.ACTIVE,
//   //     "serviceType": context.params.serviceType,
//   //     "updatedAt": { "$gte": nowDate },
//   //   })
//   //   .sort({ "serviceTypeId": -1 });
//   const professionalData = await this.adapter.model
//     .find({
//       // "phone.number": "11967301260", // "48999191146", // "75988532250",
//       //"phone.number": "34998603122",
//       "status": constantUtil.ACTIVE,
//       "serviceType": context.params.serviceType,
//       "updatedAt": { "$gte": nowDate },
//     })
//     .sort({ "serviceTypeId": -1 });
//   // const professionalData = await this.adapter.model
//   //   .find({
//   //     // "phone.number": "11967301260", // "48999191146", // "75988532250",
//   //     //"phone.number": "34998603122",
//   //     "status": constantUtil.ACTIVE,
//   //     "serviceType": context.params.serviceType,
//   //     "serviceTypeId": { "$ne": null },
//   //     // "updatedAt": { "$gte": nowDate },
//   //     "vehicles.vehicleDocuments": {
//   //       "$exists": true,
//   //       "$ne": [],
//   //       "$not": { "$size": 1 },
//   //     },
//   //   })
//   //   .sort({ "serviceTypeId": -1 })
//   //   .limit(10);
//   if (!professionalData || !professionalData?.length) {
//     return {
//       "code": 200,
//       "message": "NO PROFESSIONAL FOUND FOR CHECKING DOCUMENTS EXPIRES IN CRON",
//     };
//   }
//   // let profileDocumentList = await storageutil.read(
//   //   constantUtil.PROFILEDOCUMENTS
//   // );
//   // if (!profileDocumentList) {
//   //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };
//   // }
//   // //#region Profile Documents
//   // const profileDocumentRideList = Object.keys(profileDocumentList)
//   //   .map((documentId) => ({
//   //     ...profileDocumentList[documentId],
//   //     // "id": documentId,
//   //   }))
//   //   .filter(function (el) {
//   //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//   //     if (
//   //       el.status === constantUtil.ACTIVE &&
//   //       serviceType === constantUtil.CONST_RIDE
//   //     )
//   //       return el;
//   //   });
//   // const profileDocumentDeliveryList = Object.keys(profileDocumentList)
//   //   .map((documentId) => ({
//   //     ...profileDocumentList[documentId],
//   //     // "id": documentId,
//   //   }))
//   //   .filter(function (el) {
//   //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//   //     if (
//   //       el.status === constantUtil.ACTIVE &&
//   //       serviceType === constantUtil.CONST_DELIVERY
//   //     )
//   //       return el;
//   //   });
//   // //#region Profile Documents
//   // let driverDocumentList = await storageutil.read(constantUtil.DRIVERDOCUMENTS);
//   // if (!driverDocumentList) {
//   //   return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };
//   // }
//   // //#region Vehicl % Driver Documents
//   // const driverDocumentRideList = Object.keys(driverDocumentList)
//   //   .map((documentId) => ({
//   //     ...driverDocumentList[documentId],
//   //     // "id": documentId,
//   //   }))
//   //   .filter(function (el) {
//   //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//   //     if (
//   //       el.status === constantUtil.ACTIVE &&
//   //       serviceType === constantUtil.CONST_RIDE
//   //     )
//   //       return el;
//   //   });
//   // const driverDocumentDeliveryList = Object.keys(driverDocumentList)
//   //   .map((documentId) => ({
//   //     ...driverDocumentList[documentId],
//   //     // "id": documentId,
//   //   }))
//   //   .filter(function (el) {
//   //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//   //     if (
//   //       el.status === constantUtil.ACTIVE &&
//   //       serviceType === constantUtil.CONST_DELIVERY
//   //     )
//   //       return el;
//   //   });
//   // //#endregion Vehicl % Driver Documents

//   // //#region professional, Vehicle & Driver Documents
//   // const documentListRide = await helperUtil.getRegistrationDocumentFromRedis(
//   //   constantUtil.CONST_RIDE
//   // );
//   // const documentListDelivery =
//   //   await helperUtil.getRegistrationDocumentFromRedis(
//   //     constantUtil.CONST_DELIVERY
//   //   );
//   // const allDocumentList = await helperUtil.getRegistrationDocumentFromRedis(
//   //   constantUtil.ALL
//   // );
//   // //#endregion professional, Vehicle & Driver Documents
//   let professionalServiceTypeId = null,
//     // documentList = null,
//     profileDocumentList = null,
//     driverDocumentList = null;
//   await each(professionalData, async (professionals, next) => {
//     professionals = professionals.toJSON();
//     let isDriverVerify = false;
//     //#region Vehicle & Driver Documents
//     if (
//       professionalServiceTypeId !== professionals?.serviceTypeId?.toString()
//     ) {
//       professionalServiceTypeId = professionals?.serviceTypeId?.toString();
//       //
//       // documentList =
//       //   await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
//       //     professionalServiceTypeId || professionals?.serviceTypeId?.toString()
//       //   );
//       let documentList = await this.broker.emit(
//         "admin.getRegistrationDocumentUsingServiceTypeId",
//         {
//           "serviceTypeId":
//             professionalServiceTypeId ||
//             professionals?.serviceTypeId?.toString(),
//         }
//       );
//       documentList = documentList && documentList[0];
//       profileDocumentList = documentList?.profileDocumentRideList || null;
//       driverDocumentList = documentList?.driverDocumentRideList || null;
//       // console.log(
//       //   professionalServiceTypeId?.toString() ||
//       //     professionals?.serviceTypeId?.toString() +
//       //       "__cronCheckDocumentsExpireCheck___" +
//       //       JSON.stringify(documentList)
//       // );
//     }
//     //#endregion Vehicle & Driver Documents
//     const profileDocs = professionals.profileVerification;
//     const vehicles = professionals.vehicles;

//     const driDocs = [];
//     const vehicleDocs = [];

//     let professionalProfileMandatoryDocLength = 0;
//     let professionalVehicleMandatoryDocLength = 0;
//     //
//     const mandatoryProfileDocumentsLength = profileDocumentList?.filter(
//       (eachDoc) => eachDoc.docsMandatory === true
//     ).length;
//     const mandatoryVehciclesDocumentsLength = driverDocumentList?.filter(
//       (eachDoc) => eachDoc.docsMandatory === true
//     ).length;
//     //
//     if (profileDocumentList) {
//       await profileDocumentList?.forEach(async (docs) => {
//         if (docs.status === constantUtil.ACTIVE) {
//           profileDocs.forEach((d) => {
//             if (docs.docsName === d.documentName) {
//               if (docs.docsMandatory) {
//                 professionalProfileMandatoryDocLength += 1;
//               }

//               if (docs.docsExpiry === true) {
//                 const diffDays = parseInt(
//                   (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
//                 ); //gives day difference
//                 if (diffDays <= 0) {
//                   d["status"] = constantUtil.UNVERIFIED;
//                   isDriverVerify = true;
//                 }
//               } else {
//                 d["expiryDate"] = "";
//                 d["status"] = constantUtil.VERIFIED;
//               }
//               driDocs.push(d);
//             }
//           });
//           professionals.profileVerification = driDocs;
//         }
//       });
//     }
//     if (driverDocumentList) {
//       await vehicles.forEach((v) => {
//         const docusvehi = v.vehicleDocuments;
//         driverDocumentList?.forEach(async (docs) => {
//           if (docs.status === constantUtil.ACTIVE) {
//             docusvehi.forEach((c) => {
//               if (docs.docsName === c.documentName) {
//                 if (docs.docsMandatory) {
//                   professionalVehicleMandatoryDocLength += 1;
//                 }
//                 if (docs.docsExpiry === true) {
//                   const diffDays = parseInt(
//                     (new Date(c.expiryDate) - new Date()) /
//                       (1000 * 60 * 60 * 24)
//                   ); //gives day difference
//                   if (diffDays <= 0) {
//                     v.status = constantUtil.INACTIVE;
//                     c["status"] = constantUtil.UNVERIFIED;
//                     isDriverVerify = true;
//                   }
//                 } else {
//                   v.status = constantUtil.ACTIVE;
//                   c["expiryDate"] = "";
//                   c["status"] = constantUtil.VERIFIED;
//                 }
//                 vehicleDocs.push(c);
//               }
//             });
//           }
//         });
//         v["vehicleDocuments"] = vehicleDocs;
//       });
//     }
//     if (
//       professionals.profileVerification &&
//       professionals.vehicles[0].vehicleDocuments
//     ) {
//       await this.adapter.updateById(
//         mongoose.Types.ObjectId(professionals._id.toString()),
//         {
//           "profileVerification": professionals.profileVerification,
//           "vehicles": professionals.vehicles,
//         }
//       );
//     }
//     // below key is used for if professional does not add a doc which is mandatory but which docs updated latesly so professional does not have these docs so it should need to add then professional should take ride ok.
//     let expiry = false;

//     if (
//       mandatoryProfileDocumentsLength > professionalProfileMandatoryDocLength
//     ) {
//       expiry = true;
//     }

//     if (
//       mandatoryVehciclesDocumentsLength > professionalVehicleMandatoryDocLength
//     ) {
//       expiry = true;
//     }
//     if (isDriverVerify === true || expiry) {
//       const responseJson = await this.adapter.updateById(
//         mongoose.Types.ObjectId(professionals._id.toString()),
//         { "status": constantUtil.UNVERIFIED, "isExpired": true }
//       );
//       if (responseJson.deviceInfo[0]?.deviceId) {
//         const notificationObject = {
//           "clientId": context.params.clientId,
//           "data": {
//             "type": constantUtil.NOTIFICATIONTYPE,
//             "userType": constantUtil.PROFESSIONAL,
//             "action": constantUtil.ACTION_DOCUMENTEXPIRED,
//             "timestamp": Date.now(),
//             "message": "YOUR DOCUMENTS IS EXPIRED AND CHECK UPDATE",
//             "details": lzStringEncode({}),
//           },
//           "registrationTokens": [
//             {
//               "token": responseJson.deviceInfo[0].deviceId,
//               "id": responseJson._id.toString(),
//               "deviceType": responseJson.deviceInfo[0]?.deviceType,
//               "platform": responseJson.deviceInfo[0]?.platform,
//               "socketId": responseJson.deviceInfo[0]?.socketId,
//             },
//           ],
//         };
//         /* SOCKET PUSH NOTIFICATION */
//         this.broker.emit("socket.sendNotification", notificationObject);

//         /* FCM */
//         this.broker.emit("admin.sendFCM", notificationObject);
//       }

//       next();
//     } else {
//       next();
//     }
//   });
//   return;
// };

professionalEvent.cronCheckProfileDocumentsExpire = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    PROFESSIONAL_NOT_FOUND,
    INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED,
    INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS,
  } = notifyMessage.setNotifyLanguage(
    generalSettings.data.languageCode || constantUtil.DEFAULT_LANGUAGE
  );
  const nowDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);

  const professionalData = await this.adapter.model
    .find({
      "status": constantUtil.ACTIVE,
      "serviceType": context.params.serviceType,
      "updatedAt": { "$gte": nowDate },
    })
    .sort({ "serviceTypeId": -1 })
    .lean();
  if (!professionalData || !professionalData?.length) {
    return {
      "code": 200,
      "message": PROFESSIONAL_NOT_FOUND,
    };
  }
  // const allProfileDocumentList = await storageutil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );
  let professionalProfileServiceTypeId = null,
    profileDocumentList = null;
  // await each(professionalData, async (professionals, next) => {
  // await professionalData?.forEach(async (professionals) => {
  for (const professionals of professionalData) {
    // professionals = professionals.toJSON();
    let professionalProfileMandatoryDocLength = 0,
      documentName = "",
      isDriverVerify = false,
      notificationMessage = "";
    driDocs = [];

    // console.log(JSON.stringify(professionals.phone));
    if (professionals.serviceType === context.params.serviceType) {
      //#region Vehicle & Driver Documents
      if (
        professionalProfileServiceTypeId !==
        professionals?.serviceTypeId?.toString()
      ) {
        //#region Profile Documents
        // profileDocumentList = Object.keys(allProfileDocumentList)
        //   .map((documentId) => ({
        //     ...allProfileDocumentList[documentId],
        //     // "id": documentId,
        //   }))
        //   .filter(function (el) {
        //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
        //     if (
        //       el.docsServiceTypeId ===
        //         professionals?.serviceTypeId?.toString() &&
        //       el.status === constantUtil.ACTIVE &&
        //       serviceType === context.params.serviceType
        //     ) {
        //       return el;
        //     }
        //   });
        let documentList = await this.broker.emit(
          "admin.getRegistrationDocumentUsingServiceTypeId",
          {
            "serviceTypeId":
              professionalProfileServiceTypeId ||
              professionals?.serviceTypeId?.toString(),
          }
        );
        documentList = documentList && documentList[0];
        profileDocumentList = documentList?.profileDocumentRideList || null;
        professionalProfileServiceTypeId =
          professionals?.serviceTypeId?.toString();
      }
      //#endregion Vehicle & Driver Documents
      const profileDocs = professionals.profileVerification;
      if (profileDocumentList) {
        const mandatoryProfileDocumentsLength = profileDocumentList?.filter(
          (eachDoc) => eachDoc.docsMandatory === true
        ).length;
        //
        // await profileDocumentList?.forEach(async (docs) => {
        for (const docs of profileDocumentList) {
          if (docs.status === constantUtil.ACTIVE) {
            // await profileDocs.forEach((d) => {
            for (const d of profileDocs) {
              if (docs.docsName === d.documentName) {
                if (docs.docsExpiry === true) {
                  const diffDays = parseInt(
                    (new Date(d.expiryDate) - new Date()) /
                      (1000 * 60 * 60 * 24)
                  ); //gives day difference
                  if (diffDays <= 0) {
                    d["status"] = constantUtil.UNVERIFIED;
                    d["uploadedStatus"] = constantUtil.UNVERIFIED;
                    d["expiryDate"] = d.expiryDate || new Date();
                    isDriverVerify = true;
                    documentName = d.documentName;
                  } else if (diffDays > 0 && diffDays <= 7) {
                    notificationMessage =
                      INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS.replace(
                        "{{days}}",
                        diffDays
                      );
                  }
                } else {
                  d["expiryDate"] = "";
                  d["status"] = constantUtil.VERIFIED;
                }
                let isDriDocsExists = driDocs?.some((el) => {
                  return el._id?.toString() === d._id?.toString();
                });
                if (!isDriDocsExists) {
                  driDocs.push(d);
                  if (docs.docsMandatory) {
                    professionalProfileMandatoryDocLength += 1;
                  }
                }
              }
            }
            // });
            professionals["profileVerification"] = driDocs;
          }
        }
        // });
        // professionals["profileVerification"] = driDocs;
        // driDocs = [];
        // }
        // if (
        //   professionals.profileVerification // && professionals.vehicles[0].vehicleDocuments
        // ) {
        //   await this.adapter.updateById(
        //     mongoose.Types.ObjectId(professionals._id.toString()),
        //     {
        //       "profileVerification": professionals.profileVerification,
        //     }
        //   );
        // }
        // below key is used for if professional does not add a doc which is mandatory but which docs updated latesly so professional does not have these docs so it should need to add then professional should take ride ok.
        if (
          mandatoryProfileDocumentsLength !==
          professionalProfileMandatoryDocLength
        ) {
          isDriverVerify = true;
          notificationMessage = INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED;
        }
        if (isDriverVerify) {
          // const responseJson = await this.adapter.updateById(
          const responseJson = await this.adapter.model
            .findOneAndUpdate(
              { "_id": mongoose.Types.ObjectId(professionals._id.toString()) },
              {
                "profileVerification": professionals.profileVerification,
                "status": constantUtil.UNVERIFIED,
                "isExpired": true,
              },
              { "new": true }
            )
            .lean();

          // // next();
          if (
            generalSettings.data.emailConfigurationEnable &&
            responseJson.email &&
            responseJson.isEmailVerified
          ) {
            this.broker.emit("admin.sendServiceMail", {
              "clientId": context.params.clientId,
              "templateName": "documentExpired",
              ...professionalDocumentExpiredMailObject({
                ...responseJson,
                "documentName": documentName,
                "documentType": "Profile Document",
                "reason": null,
              }),
            });
          }
        }
        if (professionals.deviceInfo[0]?.deviceId && notificationMessage) {
          const notificationObject = {
            "clientId": context.params.clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.PROFESSIONAL,
              "action": constantUtil.ACTION_DOCUMENTEXPIRED,
              "timestamp": Date.now(),
              "message": notificationMessage,
              "details": lzStringEncode({}),
            },
            "registrationTokens": [
              {
                "token": professionals.deviceInfo[0].deviceId,
                "id": professionals._id.toString(),
                "deviceType": professionals.deviceInfo[0]?.deviceType,
                "platform": professionals.deviceInfo[0]?.platform,
                "socketId": professionals.deviceInfo[0]?.socketId,
              },
            ],
          };
          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
        }
      }
      // else {
      //   next();
      // }
    }
  }
  // });
  // return;
};

professionalEvent.cronCheckVehicleDocumentsExpire = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    PROFESSIONAL_NOT_FOUND,
    INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED,
    INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS,
  } = notifyMessage.setNotifyLanguage(
    generalSettings.data.languageCode || constantUtil.DEFAULT_LANGUAGE
  );
  const nowDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);

  //  const responseJson = await this.adapter.model
  //    .findOne(
  //      {
  //        "_id": mongoose.Types.ObjectId(context.params.professionalId),
  //      },
  //      {
  //        "serviceType": 1,
  //        "serviceTypeId": 1,
  //        "serviceAreaId": 1,
  //        "vehicles": {
  //          "$elemMatch": { "_id": context.params.id },
  //        },
  //      }
  //    )
  //    .lean();

  const professionalData = await this.adapter.model
    .find(
      {
        "status": constantUtil.ACTIVE,
        "updatedAt": { "$gte": nowDate },
      }
      // ,{
      //   "_id": 1,
      //   "vehicles": {
      //     "$elemMatch": { "serviceType": context.params.serviceType },
      //   },
      // }
    )
    .sort({ "vehicles.serviceTypeId": -1 })
    .lean();
  if (!professionalData || !professionalData?.length) {
    return {
      "code": 200,
      "message": PROFESSIONAL_NOT_FOUND,
    };
  }
  const allDriverDocumentList = await storageutil.read(
    constantUtil.DRIVERDOCUMENTS
  );
  // await each(professionalData, async (professionals, next) => {
  // await professionalData?.forEach(async (professionals) => {
  for (const professionals of professionalData) {
    // professionals = professionals?.toJSON();
    let documentName = "",
      isNeedDriverVerify = false,
      notificationMessage = "";
    const vehicles = professionals.vehicles || [];
    let vehicleDocs = [];
    //
    // await vehicles.forEach(async (vehicle) => {
    for (const vehicle of vehicles) {
      let professionalVehicleServiceTypeId = null,
        driverDocumentList = null,
        professionalVehicleMandatoryDocLength = 0;
      if (
        vehicle.serviceType === context.params.serviceType &&
        vehicle.defaultVehicle === true
      ) {
        //#region Vehicle & Driver Documents
        // if (
        //   professionalVehicleServiceTypeId !== vehicles?.serviceTypeId?.toString()
        // ) {
        //   professionalVehicleServiceTypeId = vehicles?.serviceTypeId?.toString();
        //   //
        //   // documentList =
        //   //   await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
        //   //     professionalServiceTypeId || professionals?.serviceTypeId?.toString()
        //   //   );
        //   let documentList = await this.broker.emit(
        //     "admin.getRegistrationDocumentUsingServiceTypeId",
        //     {
        //       "serviceTypeId":
        //         professionalVehicleServiceTypeId ||
        //         vehicles?.serviceTypeId?.toString(),
        //     }
        //   );
        //   documentList = documentList && documentList[0];
        //   // profileDocumentList = documentList?.profileDocumentRideList || null;
        //   driverDocumentList = documentList?.driverDocumentRideList || null;
        // }
        if (
          professionalVehicleServiceTypeId !==
          vehicle?.serviceTypeId?.toString()
        ) {
          // driverDocumentList = Object.keys(driverDocumentList)
          //   .map((documentId) => ({
          //     ...driverDocumentList[documentId],
          //     // "id": documentId,
          //   }))
          //   .filter(function (el) {
          //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
          //     if (
          //       el.docsServiceTypeId === vehicle?.serviceTypeId?.toString() &&
          //       el.status === constantUtil.ACTIVE &&
          //       serviceType === context.params.serviceType
          //     )
          //       return el;
          //   });
          let documentList = await this.broker.emit(
            "admin.getRegistrationDocumentUsingServiceTypeId",
            {
              "serviceTypeId":
                professionalVehicleServiceTypeId ||
                vehicle?.serviceTypeId?.toString(),
            }
          );
          documentList = documentList && documentList[0];
          // profileDocumentList = documentList?.profileDocumentRideList || null;
          driverDocumentList = documentList?.driverDocumentRideList || null;
          professionalVehicleServiceTypeId = vehicle?.serviceTypeId?.toString();
        }
        //#endregion Vehicle & Driver Documents

        // if (driverDocumentList) {
        if (driverDocumentList && vehicle.defaultVehicle) {
          const mandatoryVehciclesDocumentsLength = driverDocumentList?.filter(
            (eachDoc) => eachDoc.docsMandatory === true
          ).length;
          //
          const docusvehi = vehicle.vehicleDocuments || [];
          // await driverDocumentList?.forEach(async (docs) => {
          for (const docs of driverDocumentList) {
            if (docs.status === constantUtil.ACTIVE) {
              // await docusvehi.forEach(async (c) => {
              for (const c of docusvehi) {
                if (docs.docsName === c.documentName) {
                  if (docs.docsExpiry === true) {
                    const diffDays = parseInt(
                      (new Date(c.expiryDate) - new Date()) /
                        (1000 * 60 * 60 * 24)
                    ); //gives day difference
                    if (diffDays <= 0) {
                      vehicle.status = constantUtil.INACTIVE;
                      c["status"] = constantUtil.UNVERIFIED;
                      c["uploadedStatus"] = constantUtil.UNVERIFIED;
                      c["expiryDate"] = c.expiryDate || new Date();
                      isNeedDriverVerify = true;
                      documentName = c.documentName;
                    } else if (diffDays > 0 && diffDays <= 7) {
                      notificationMessage =
                        INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS.replace(
                          "{{days}}",
                          diffDays
                        );
                    }
                  } else {
                    vehicle.status = constantUtil.ACTIVE;
                    c["expiryDate"] = "";
                    c["status"] = constantUtil.VERIFIED;
                  }
                  let isVehicleDocsExists = vehicleDocs?.some((el) => {
                    return el._id?.toString() === c._id?.toString();
                  });
                  if (!isVehicleDocsExists) {
                    vehicleDocs.push(c);
                    if (docs.docsMandatory) {
                      professionalVehicleMandatoryDocLength += 1;
                    }
                  }
                }
                // });
              }
            }
            // });
          }
          vehicle["vehicleDocuments"] = vehicleDocs;
          vehicleDocs = [];
          //
          if (
            mandatoryVehciclesDocumentsLength !==
            professionalVehicleMandatoryDocLength
          ) {
            isNeedDriverVerify = true;
            notificationMessage = INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED;
          }
          if (isNeedDriverVerify) {
            // const responseJson = await this.adapter.updateById(
            const responseJson = await this.adapter.model
              .findOneAndUpdate(
                {
                  "_id": mongoose.Types.ObjectId(professionals._id.toString()),
                },
                {
                  "vehicles": professionals.vehicles,
                  "status": constantUtil.UNVERIFIED,
                  "isExpired": true,
                },
                { "new": true }
              )
              .lean();
            // Send Mail
            if (
              generalSettings.data.emailConfigurationEnable &&
              responseJson.email &&
              responseJson.isEmailVerified
            ) {
              this.broker.emit("admin.sendServiceMail", {
                "clientId": context.params.clientId,
                "templateName": "documentExpired",
                ...professionalDocumentExpiredMailObject({
                  ...responseJson,
                  "documentName": documentName,
                  "documentType": "Vehicle Document",
                  "reason": null,
                }),
              });
            }
          }
          if (professionals.deviceInfo[0]?.deviceId && notificationMessage) {
            const notificationObject = {
              "clientId": context.params.clientId,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.PROFESSIONAL,
                "action": constantUtil.ACTION_DOCUMENTEXPIRED,
                "timestamp": Date.now(),
                "message": notificationMessage,
                "details": lzStringEncode({}),
              },
              "registrationTokens": [
                {
                  "token": professionals.deviceInfo[0].deviceId,
                  "id": professionals._id.toString(),
                  "deviceType": professionals.deviceInfo[0]?.deviceType,
                  "platform": professionals.deviceInfo[0]?.platform,
                  "socketId": professionals.deviceInfo[0]?.socketId,
                },
              ],
            };
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
          }
        }
      }
      // });
    }
    //
    // if (professionals.vehicles.length > 0) {
    //   await this.adapter.updateById(
    //     mongoose.Types.ObjectId(professionals._id.toString()),
    //     {
    //       "vehicles": professionals.vehicles,
    //     }
    //   );
    // }
    // if (isNeedDriverVerify) {
    //   const responseJson = await this.adapter.updateById(
    //     mongoose.Types.ObjectId(professionals._id.toString()),
    //     { "status": constantUtil.UNVERIFIED, "isExpired": true }
    //   );
    //   if (responseJson.deviceInfo[0]?.deviceId) {
    //     const notificationObject = {
    //       "clientId": context.params.clientId,
    //       "data": {
    //         "type": constantUtil.NOTIFICATIONTYPE,
    //         "userType": constantUtil.PROFESSIONAL,
    //         "action": constantUtil.ACTION_DOCUMENTEXPIRED,
    //         "timestamp": Date.now(),
    //         "message": INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED,
    //         "details": lzStringEncode({}),
    //       },
    //       "registrationTokens": [
    //         {
    //           "token": responseJson.deviceInfo[0].deviceId,
    //           "id": responseJson._id.toString(),
    //           "deviceType": responseJson.deviceInfo[0]?.deviceType,
    //           "platform": responseJson.deviceInfo[0]?.platform,
    //           "socketId": responseJson.deviceInfo[0]?.socketId,
    //         },
    //       ],
    //     };
    //     /* SOCKET PUSH NOTIFICATION */
    //     this.broker.emit("socket.sendNotification", notificationObject);
    //     /* FCM */
    //     this.broker.emit("admin.sendFCM", notificationObject);
    //   }
    //   next();
    // } else {
    //   next();
    // }
    // });
  }
  // return;
};

professionalEvent.getWishedProfessionalList = async function (context) {
  const responseJson = await this.adapter.model.find({
    "_id": { "$in": context.params.professionalsList },
  });
  if (!responseJson && responseJson.length === 0) return [];
  return responseJson;
};

professionalEvent.updateWalletRechargeOrWithdraw = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.professionalId.toString()))
    .lean();
  const finalAmount =
    context.params.transStatus === constantUtil.CREDIT
      ? parseFloat(userData.wallet.availableAmount) +
        parseFloat(context.params.amount)
      : parseFloat(userData.wallet.availableAmount) -
        parseFloat(context.params.amount);
  const updateData = {
    "wallet.availableAmount": finalAmount,
    "lastWalletAlertSendDate": null,
  };

  const responseJson = await this.adapter.model.updateOne(
    { "_id": context.params.professionalId, "status": constantUtil.ACTIVE },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

professionalEvent.updateAllProfessionals = async function () {
  // const professionalData = await this.adapter.model
  //   .find(
  //     {
  //       "status": { "$nin": [constantUtil.INCOMPLETE, constantUtil.ARCHIVE] },
  //     },
  //     {
  //       "profileVerification": 1,
  //       "vehicles": 1,
  //       "serviceType": 1,
  //       "serviceTypeId": 1,
  //     }
  //   )
  //   .lean();
  const professionalDataCount = await this.adapter.model.count({
    "status": {
      "$nin": [constantUtil.INCOMPLETE, constantUtil.ARCHIVE],
    },
  });
  //   .find(
  let skip = 0;
  const limit = 100;
  const loopCount = 100 / limit;
  for (let i = 0; i < loopCount; i++) {
    let professionalData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "status": {
              "$nin": [constantUtil.INCOMPLETE, constantUtil.ARCHIVE],
            },
          },
        },
        {
          "$project": {
            "_id": 1,
            "profileVerification": "$profileVerification",
            "vehicles": "$vehicles",
            "serviceType": "$serviceType",
            "serviceTypeId": "$serviceTypeId",
          },
        },
        { "$skip": skip },
        { "$limit": limit },
      ])
      .allowDiskUse(true);
    skip = skip + limit;
    if (professionalData && professionalData.length > 0) {
      //   let profileDocumentList = await storageutil.read(
      //     constantUtil.PROFILEDOCUMENTS
      //   );
      //   if (!profileDocumentList)
      //     return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };
      // //#region Profile Documents
      // const profileDocumentRideList = Object.keys(profileDocumentList)
      //   .map((documentId) => ({
      //     ...profileDocumentList[documentId],
      //     // "id": documentId,
      //   }))
      //   .filter(function (el) {
      //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
      //     if (
      //       el.status === constantUtil.ACTIVE &&
      //       serviceType === constantUtil.CONST_RIDE
      //     )
      //       return el;
      //   });
      // const profileDocumentDeliveryList = Object.keys(profileDocumentList)
      //   .map((documentId) => ({
      //     ...profileDocumentList[documentId],
      //     // "id": documentId,
      //   }))
      //   .filter(function (el) {
      //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
      //     if (
      //       el.status === constantUtil.ACTIVE &&
      //       serviceType === constantUtil.CONST_DELIVERY
      //     )
      //       return el;
      //   });
      // //#endregion Profile Documents
      // let driverDocumentList = await storageutil.read(
      //   constantUtil.DRIVERDOCUMENTS
      // );
      // if (!driverDocumentList)
      //   return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

      // //#region Vehicle & Driver Documents
      // const driverDocumentRideList = await Object.keys(driverDocumentList)
      //   .map((documentId) => ({
      //     ...driverDocumentList[documentId],
      //     // "id": documentId,
      //   }))
      //   .filter(function (el) {
      //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
      //     if (
      //       el.status === constantUtil.ACTIVE &&
      //       serviceType === constantUtil.CONST_RIDE
      //     )
      //       return el;
      //   });
      // const driverDocumentDeliveryList = await Object.keys(driverDocumentList)
      //   .map((documentId) => ({
      //     ...driverDocumentList[documentId],
      //     // "id": documentId,
      //   }))
      //   .filter(function (el) {
      //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
      //     if (
      //       el.status === constantUtil.ACTIVE &&
      //       serviceType === constantUtil.CONST_DELIVERY
      //     )
      //       return el;
      //   });
      // //#endregion Vehicle & Driver Documents
      let professionalServiceTypeId = null,
        documentList = null,
        driverDocumentList = null,
        profileDocumentList = null;
      await each(professionalData, async (professionals, next) => {
        const profileDocs = professionals.profileVerification;
        const vehicles = professionals.vehicles;
        //#region Vehicle & Driver Documents
        if (
          professionalServiceTypeId !== professionals?.serviceTypeId?.toString()
        ) {
          professionalServiceTypeId = professionals?.serviceTypeId?.toString();
          //
          documentList =
            await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
              professionalServiceTypeId
            );
          profileDocumentList = documentList?.profileDocumentRideList || null;
          driverDocumentList = documentList?.driverDocumentRideList || null;
        }
        //#endregion Vehicle & Driver Documents
        //#region Documents

        // profileDocumentList =
        //   professionalServiceType === constantUtil.CONST_RIDE
        //     ? profileDocumentRideList
        //     : profileDocumentDeliveryList;
        // driverDocumentList =
        //   professionalServiceType === constantUtil.CONST_RIDE
        //     ? driverDocumentRideList
        //     : driverDocumentDeliveryList;
        //#endregion Documents
        const driDocs = [];
        let vehicleDocs = [];
        if (profileDocumentList) {
          await profileDocumentList.forEach(async (docs) => {
            if (docs.status === constantUtil.ACTIVE) {
              profileDocs.forEach((d) => {
                if (docs.docsName === d.documentName) {
                  if (docs.docsExpiry === true) {
                    const diffDays = parseInt(
                      (new Date(d.expiryDate) - new Date()) /
                        (1000 * 60 * 60 * 24)
                    ); //gives day difference
                    if (diffDays <= 0) {
                      d["status"] = constantUtil.UNVERIFIED;
                    }
                  } else {
                    d["expiryDate"] = "";
                    d["status"] = constantUtil.VERIFIED;
                  }
                  driDocs.push(d);
                }
                professionals.profileVerification = driDocs;
              });
            }
          });
        }
        if (driverDocumentList) {
          await vehicles.forEach((v) => {
            const docusvehi = v.vehicleDocuments;
            driverDocumentList.forEach(async (docs) => {
              if (docs.status === constantUtil.ACTIVE) {
                docusvehi.forEach((c) => {
                  if (docs.docsName === c.documentName) {
                    if (docs.docsExpiry === true) {
                      const diffDays = parseInt(
                        (new Date(c.expiryDate) - new Date()) /
                          (1000 * 60 * 60 * 24)
                      ); //gives day difference
                      if (diffDays <= 0) {
                        v.status = constantUtil.INACTIVE;
                        c["status"] = constantUtil.UNVERIFIED;
                      }
                    } else {
                      v.status = constantUtil.ACTIVE;
                      c["expiryDate"] = "";
                      c["status"] = constantUtil.VERIFIED;
                    }
                    vehicleDocs.push(c);
                  }
                });
              }
            });
            v["vehicleDocuments"] = vehicleDocs;
            vehicleDocs = [];
          });
        }
        await this.adapter.updateById(
          mongoose.Types.ObjectId(professionals._id.toString()),
          {
            "profileVerification": professionals.profileVerification,
            "vehicles": professionals.vehicles,
          }
        );
        next();
      });
      professionalData = null;
    }
  }
};

professionalEvent.blockedTheProfessionalsUsingCron = async function (context) {
  let blockConfig = [];
  const blockedProfessionalConfig = await storageUtil.read(
    constantUtil.CONST_BLOCKPROFESSIONALSETUP
  );
  const { ALERT_PROFESSIONAL_LOW_PERFORMANCE, ALERT_PROFESSIONAL_BLOCK } =
    notifyMessage.setNotifyLanguage(context.params.langCode);
  if (blockedProfessionalConfig) {
    // #region block Config
    blockConfig.push({
      "startValue": blockedProfessionalConfig.data.fromValue,
      "endValue": blockedProfessionalConfig.data.toValue,
      "blockDays": blockedProfessionalConfig.data.blockDays,
      "thresholdValue": blockedProfessionalConfig.data.thresholdValue,
      "notifyAcceptanceRate": blockedProfessionalConfig.data.acceptanceRate,
      "notifyRejectionRate": blockedProfessionalConfig.data.rejectionRate,
      "notifyCancellationRate": blockedProfessionalConfig.data.cancellationRate,
      "isEnableNotification":
        blockedProfessionalConfig.data.isEnableNotification,
    });
    blockConfig.push({
      "startValue": blockedProfessionalConfig.data.fromValue1,
      "endValue": blockedProfessionalConfig.data.toValue1,
      "blockDays": blockedProfessionalConfig.data.blockDays1,
      "thresholdValue": blockedProfessionalConfig.data.thresholdValue,
      "notifyAcceptanceRate": blockedProfessionalConfig.data.acceptanceRate,
      "notifyRejectionRate": blockedProfessionalConfig.data.rejectionRate,
      "notifyCancellationRate": blockedProfessionalConfig.data.cancellationRate,
      "isEnableNotification":
        blockedProfessionalConfig.data.isEnableNotification,
    });
    blockConfig.push({
      "startValue": blockedProfessionalConfig.data.fromValue2,
      "endValue": blockedProfessionalConfig.data.toValue2,
      "blockDays": blockedProfessionalConfig.data.blockDays2,
      "thresholdValue": blockedProfessionalConfig.data.thresholdValue,
      "notifyAcceptanceRate": blockedProfessionalConfig.data.acceptanceRate,
      "notifyRejectionRate": blockedProfessionalConfig.data.rejectionRate,
      "notifyCancellationRate": blockedProfessionalConfig.data.cancellationRate,
      "isEnableNotification":
        blockedProfessionalConfig.data.isEnableNotification,
    });
    blockConfig.push({
      "startValue": blockedProfessionalConfig.data.fromValue3,
      "endValue": blockedProfessionalConfig.data.toValue3,
      "blockDays": blockedProfessionalConfig.data.blockDays3,
      "thresholdValue": blockedProfessionalConfig.data.thresholdValue,
      "notifyAcceptanceRate": blockedProfessionalConfig.data.acceptanceRate,
      "notifyRejectionRate": blockedProfessionalConfig.data.rejectionRate,
      "notifyCancellationRate": blockedProfessionalConfig.data.cancellationRate,
      "isEnableNotification":
        blockedProfessionalConfig.data.isEnableNotification,
    });
    // #endregion block Config
    const fromDayRecord = blockedProfessionalConfig?.data?.expiryDays || 30;
    let professionalData = await this.broker.emit(
      "booking.getProfessionalRankingReport",
      {
        "createdAtFrom": new Date(
          new Date().getTime() - fromDayRecord * 24 * 60 * 60 * 1000
        ), // Last 30/60/90 Days
        "createdAtTo": new Date(),
        "skip": 0,
        "limit": 100000,
      }
    );
    professionalData = professionalData && professionalData[0];
    if (professionalData?.[0]?.responseData?.length > 0) {
      professionalData = professionalData?.[0]?.responseData;
      await professionalData?.map(async (item) => {
        const notAttemptRatioPrecentage = item.notAttemptRatio * 100;
        const cancelledRationPrecentage = item.cancelledRation * 100;
        let blockDays = 0,
          blockedTill = null,
          isBlockProfessional = false,
          isSendNotification = false;
        //
        blockConfig?.map(async (config) => {
          const startValue = config.startValue;
          const endValue = config.endValue;
          const remainTripCount = item.totalTrip - config.thresholdValue;
          if (
            config.blockDays > 0 &&
            remainTripCount > 0 && // item.totalTrip > 0 &&
            notAttemptRatioPrecentage >= startValue &&
            notAttemptRatioPrecentage <= endValue
            // ||  (cancelledRationPrecentage >= startValue &&
            // cancelledRationPrecentage <= endValue)
          ) {
            isBlockProfessional = true;
            blockDays = config.blockDays;
            blockedTill = new Date(
              new Date().getTime() + blockDays * 24 * 60 * 60 * 1000
            );
          }
          //
          if (
            config.isEnableNotification &&
            remainTripCount > 0 && // item.totalTrip > 0 &&
            item.professionalStatus === constantUtil.ACTIVE &&
            // (notAttemptRatioPrecentage <= config.notifyAcceptanceRate ||
            (notAttemptRatioPrecentage >= config.notifyRejectionRate ||
              cancelledRationPrecentage >= config.notifyCancellationRate)
          ) {
            isSendNotification = true;
          }
        });
        //
        if (isBlockProfessional && blockDays > 0) {
          const todayEndAt = new Date(new Date().setHours(23, 59, 59, 999));
          const responseJson = await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(item._id), // "_id"  as professionalId
              "status": constantUtil.ACTIVE,
              "$or": [
                { "unblockedTill": { "$lt": Date.now() } },
                { "unblockedTill": null },
              ],
              "$or": [
                { "lastBlockedAlertSendDate": { "$lt": Date.now() } },
                { "lastBlockedAlertSendDate": null },
              ],
            },
            {
              "$set": {
                "status": constantUtil.BLOCKED,
                "blockedTill": blockedTill,
                "unblockedTill": todayEndAt,
                "lastBlockedAlertSendDate": todayEndAt,
              },
            }
          );
          if (responseJson.nModified) {
            const notificationObject = {
              "temprorySound": true,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.PROFESSIONAL,
                "action": constantUtil.ACTION_ASSIGN, // Need to Change Action ACTION_ALERT
                "timestamp": Date.now(),
                "message": ALERT_PROFESSIONAL_BLOCK.replace(
                  /{{date}}/g,
                  blockedTill.toLocaleDateString("en-GB")
                ),
                "details": lzStringEncode({}),
              },
              "registrationTokens": [
                {
                  "id": item?._id?.toString(),
                  "token": item?.deviceInfo[0]?.deviceId,
                  "deviceType": item?.deviceInfo[0]?.deviceType,
                  "platform": item?.deviceInfo[0]?.platform,
                  "socketId": item?.deviceInfo[0]?.socketId,
                  "duration": 0,
                  "distance": 0,
                },
              ],
            };
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
          }
        }
        //
        if (isSendNotification) {
          const todayEndAt = new Date(new Date().setHours(23, 59, 59, 999));
          const responseJson = await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(item._id), // "_id"  as professionalId
              "status": constantUtil.ACTIVE,
              "$or": [
                { "lastBlockedAlertSendDate": { "$lt": Date.now() } },
                { "lastBlockedAlertSendDate": null },
              ],
            },
            { "$set": { "lastBlockedAlertSendDate": todayEndAt } }
          );
          if (responseJson.nModified) {
            const notificationObject = {
              "temprorySound": true,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.PROFESSIONAL,
                "action": constantUtil.ACTION_ASSIGN, // Need to Change Action ACTION_ALERT
                "timestamp": Date.now(),
                // "message":
                //   notAttemptRatioPrecentage <= item.notifyAcceptanceRate
                //     ? ALERT_PROFESSIONAL_LOW_PERFORMANCE
                //     : ALERT_PROFESSIONAL_LOW_PERFORMANCE,
                "message": ALERT_PROFESSIONAL_LOW_PERFORMANCE,
                "details": lzStringEncode({}),
              },
              "registrationTokens": [
                {
                  "id": item?._id?.toString(),
                  "token": item?.deviceInfo[0]?.deviceId,
                  "deviceType": item?.deviceInfo[0]?.deviceType,
                  "platform": item?.deviceInfo[0]?.platform,
                  "socketId": item?.deviceInfo[0]?.socketId,
                  "duration": 0,
                  "distance": 0,
                },
              ],
            };
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
          }
        }
      });
    }
  }
};

professionalEvent.unBlockedTheProfessionalsUsingCron = async function (
  context
) {
  const { INFO_PROFESSIONAL_ACTIVATED } = notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  const professionalList = await this.adapter.model
    .find(
      {
        "status": constantUtil.BLOCKED,
        "blockedTill": { "$lte": new Date() },
      },
      { "_id": 1, "deviceInfo": 1 }
    )
    .lean();
  const responseJson = await this.adapter.model.updateMany(
    {
      "status": constantUtil.BLOCKED,
      "blockedTill": { "$lte": new Date() },
    },
    {
      "$set": {
        "status": constantUtil.ACTIVE,
        "blockedTill": null,
        "unblockedTill": null,
        "lastBlockedAlertSendDate": null,
      },
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    if (professionalList.length > 0) {
      const notificationObject = {
        "temprorySound": true,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_ASSIGN, // Need to Change Action ACTION_ALERT
          "timestamp": Date.now(),
          "message": INFO_PROFESSIONAL_ACTIVATED,
          "details": lzStringEncode({}),
        },
        "registrationTokens": professionalList.map((item) => {
          return {
            "id": item?._id?.toString(),
            "token": item?.deviceInfo[0]?.deviceId,
            "deviceType": item?.deviceInfo[0]?.deviceType,
            "platform": item?.deviceInfo[0]?.platform,
            "socketId": item?.deviceInfo[0]?.socketId,
            "duration": 0,
            "distance": 0,
          };
        }),
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

// professionalEvent.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId =
//   async function (context) {
//     const professionalData = await this.adapter.model
//       .find({
//         // "serviceType": context.params.serviceType,
//         "serviceTypeId": mongoose.Types.ObjectId(context.params.serviceTypeId),
//         // "status": { "$nin": [constantUtil.INCOMPLETE, constantUtil.ARCHIVE] },
//       })
//       .lean();
//     if (professionalData && professionalData.length > 0) {
//       const documentList =
//         await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
//           context.params.serviceTypeId
//         );
//       // let profileDocumentList = await storageutil.read(
//       //   constantUtil.PROFILEDOCUMENTS
//       // );
//       if (!documentList?.profileDocumentRideList)
//         return {
//           "code": 0,
//           "data": {},
//           "message": "ERROR IN PROFILE DOCUMENTS",
//         };
//       const profileDocumentRideList = documentList?.profileDocumentRideList;
//       // //#region Profile Documents
//       // const profileDocumentRideList = Object.keys(profileDocumentList)
//       //   .map((documentId) => ({
//       //     ...profileDocumentList[documentId],
//       //     // "id": documentId,
//       //   }))
//       //   .filter(function (el) {
//       //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       //     if (
//       //       el.status === constantUtil.ACTIVE &&
//       //       serviceType === constantUtil.CONST_RIDE
//       //     )
//       //       return el;
//       //   });
//       // const profileDocumentDeliveryList = Object.keys(profileDocumentList)
//       //   .map((documentId) => ({
//       //     ...profileDocumentList[documentId],
//       //     // "id": documentId,
//       //   }))
//       //   .filter(function (el) {
//       //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       //     if (
//       //       el.status === constantUtil.ACTIVE &&
//       //       serviceType === constantUtil.CONST_DELIVERY
//       //     )
//       //       return el;
//       //   });
//       // //#endregion Profile Documents
//       // let driverDocumentList = await storageutil.read(
//       //   constantUtil.DRIVERDOCUMENTS
//       // );
//       if (!documentList?.driverDocumentRideList)
//         return {
//           "code": 0,
//           "data": {},
//           "message": "ERROR IN DRIVER DOCUMENTS",
//         };
//       const driverDocumentRideList = documentList?.driverDocumentRideList;
//       // //#region Vehicle & Driver Documents
//       // const driverDocumentRideList = await Object.keys(driverDocumentList)
//       //   .map((documentId) => ({
//       //     ...driverDocumentList[documentId],
//       //     // "id": documentId,
//       //   }))
//       //   .filter(function (el) {
//       //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       //     if (
//       //       el.status === constantUtil.ACTIVE &&
//       //       serviceType === constantUtil.CONST_RIDE
//       //     )
//       //       return el;
//       //   });
//       // const driverDocumentDeliveryList = await Object.keys(driverDocumentList)
//       //   .map((documentId) => ({
//       //     ...driverDocumentList[documentId],
//       //     // "id": documentId,
//       //   }))
//       //   .filter(function (el) {
//       //     const serviceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       //     if (
//       //       el.status === constantUtil.ACTIVE &&
//       //       serviceType === constantUtil.CONST_DELIVERY
//       //     )
//       //       return el;
//       //   });
//       //#endregion Vehicle & Driver Documents
//       await each(professionalData, async (professionals, next) => {
//         // const professionalServiceType =
//         //   professionals?.serviceType || constantUtil.CONST_RIDE;
//         const profileDocs = professionals.profileVerification;
//         const vehicles = professionals.vehicles;
//         // //#region Documents
//         // profileDocumentList =
//         //   professionalServiceType === constantUtil.CONST_RIDE
//         //     ? profileDocumentRideList
//         //     : profileDocumentDeliveryList;
//         // driverDocumentList =
//         //   professionalServiceType === constantUtil.CONST_RIDE
//         //     ? driverDocumentRideList
//         //     : driverDocumentDeliveryList;
//         // //#endregion Documents
//         const driDocs = [];
//         const vehicleDocs = [];
//         await profileDocumentRideList.forEach(async (docs) => {
//           if (docs.status == constantUtil.ACTIVE) {
//             profileDocs.forEach((d) => {
//               if (docs.docsName == d.documentName) {
//                 if (docs.docsExpiry === true) {
//                   const diffDays = parseInt(
//                     (new Date(d.expiryDate) - new Date()) /
//                       (1000 * 60 * 60 * 24)
//                   ); //gives day difference
//                   if (diffDays <= 0) {
//                     d["status"] = constantUtil.UNVERIFIED;
//                   }
//                 } else {
//                   d["expiryDate"] = "";
//                   d["status"] = constantUtil.VERIFIED;
//                 }
//                 driDocs.push(d);
//               }
//               professionals.profileVerification = driDocs;
//             });
//           }
//         });
//         await vehicles.forEach((v) => {
//           const docusvehi = v.vehicleDocuments;
//           driverDocumentRideList.forEach(async (docs) => {
//             if (docs.status == constantUtil.ACTIVE) {
//               docusvehi.forEach((c) => {
//                 if (docs.docsName == c.documentName) {
//                   if (docs.docsExpiry === true) {
//                     const diffDays = parseInt(
//                       (new Date(c.expiryDate) - new Date()) /
//                         (1000 * 60 * 60 * 24)
//                     ); //gives day difference
//                     if (diffDays <= 0) {
//                       v.status = constantUtil.INACTIVE;
//                       c["status"] = constantUtil.UNVERIFIED;
//                     }
//                   } else {
//                     v.status = constantUtil.ACTIVE;
//                     c["expiryDate"] = "";
//                     c["status"] = constantUtil.VERIFIED;
//                   }
//                   vehicleDocs.push(c);
//                 }
//               });
//             }
//           });
//           v["vehicleDocuments"] = vehicleDocs;
//         });
//         await this.adapter.updateById(
//           mongoose.Types.ObjectId(professionals._id.toString()),
//           {
//             "profileVerification": professionals.profileVerification,
//             "vehicles": professionals.vehicles,
//           }
//         );
//         next();
//       });
//     }
//   };
professionalEvent.getAllProfessionals = async function () {
  const professionalData = await this.adapter.model.find({
    "status": { "$nin": [constantUtil.INCOMPLETE, constantUtil.ARCHIVE] },
  });

  if (!professionalData && professionalData.length === 0) return [];

  return professionalData;
};

professionalEvent.getAvailableWalletAmount = async function (context) {
  const query = [];
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "status": {
        "$in": [
          constantUtil.ACTIVE,
          constantUtil.INACTIVE,
          constantUtil.UNVERIFIED,
          constantUtil.ARCHIVE,
        ],
      },
    },
  });
  query.push({
    "$facet": {
      "response": [
        {
          "$project": {
            "_id": 1,
            "wallet": 1,
          },
        },
        {
          "$group": {
            "_id": "",
            "totalWalletAmount": { "$sum": "$wallet.availableAmount" },
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  return jsonData?.[0]?.response?.[0]?.totalWalletAmount || 0;
};

professionalEvent.getWalletAmountList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const match = {};
  match["$and"] = [];
  match["$and"].push({
    //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    "status": {
      "$in": [
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.UNVERIFIED,
        constantUtil.ARCHIVE,
      ],
    },
    "wallet.availableAmount": { "$lt": 0 },
  });

  if ((context.params?.search || "") !== "") {
    match["$and"].push({
      "$or": [
        {
          "firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.code": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    });
  }
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({ "createdAt": { "$gte": fromDate, "$lt": toDate } });
  }
  // FOR EXPORT DATE FILTER

  const query = [];
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": 1,
            "firstName": 1,
            "lastName": 1,
            "phone": 1,
            "email": 1,
            "wallet": 1,
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};
professionalEvent.categoryChanges = async function (context) {
  const categoryData = context.params.categoryData.vehicles;
  const professionals = await this.adapter.model
    .find(
      {
        "clientId": mongoose.Types.ObjectId(
          context.params.clientId?.toString()
        ),
        "vehicles.serviceCategory": mongoose.Types.ObjectId(
          context.params._id.toString()
        ),
      },
      { "vehicles": 1 }
    )
    .lean();
  professionals.forEach(async (professional) => {
    const vehicleData = [];
    professional.vehicles.forEach((vehicle) => {
      categoryData.forEach((catData) => {
        if (
          (vehicle.vehicleCategoryId ?? "").toString() ===
          (catData.categoryId ?? "").toString()
        ) {
          vehicle.isSubCategoryAvailable = catData.isSubCategoryAvailable;
          vehicle.applySubCategory =
            catData?.isForceAppliedToProfessional || false;
          vehicle.isForceAppliedToProfessional =
            catData.isForceAppliedToProfessional;
          vehicle.subCategoryIds = catData.subCategoryIds;
          vehicle.isSupportShareRide = catData?.isSupportShareRide || false;
          vehicle.isSharerideForceAppliedToProfessional =
            catData?.isSharerideForceAppliedToProfessional || false;
          vehicle.isEnableShareRide =
            catData?.isSupportShareRide === true ||
            catData?.isSharerideForceAppliedToProfessional === true
              ? true
              : false;
        }
      });
      vehicleData.push(vehicle);
    });
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(professional._id.toString()),
        //"clientId": mongoose.Types.ObjectId(context.params.clientId.toString()),
      },
      {
        "vehicles": vehicleData,
      }
    );
  });
  // // Update Professional Share Ride Details
  // categoryData.forEach(async (catData) => {
  //   // if (
  //   //   catData.isSupportShareRide === true &&
  //   //   catData.isForceAppliedToProfessional === true
  //   // ) {
  //   const ids = [];
  //   await catData.subCategoryIds.forEach((e) =>
  //     ids.push(mongoose.Types.ObjectId(e))
  //   );
  //   await this.adapter.model.updateMany(
  //     {
  //       "vehicles.vehicleCategoryId": { "$in": ids },
  //     },
  //     {
  //       "$set": {
  //         "vehicles.$.isEnableShareRide":
  //           catData?.isSupportShareRide === true &&
  //           catData?.isForceAppliedToProfessional === true
  //             ? true
  //             : false,
  //         "vehicles.$.isSupportShareRide": catData?.isSupportShareRide || false,
  //       },
  //     }
  //   );
  //   // }
  // });
};

professionalEvent.updateWalletAmountTripEnded = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userData = await this.adapter.model.findOne({
    "_id": context.params.professionalId,
    "status": constantUtil.ACTIVE,
  });

  let valueData;
  if (
    context.params.paymentMethod === constantUtil.PAYMENTWALLET ||
    context.params.paymentMethod === constantUtil.PAYMENTCARD ||
    (context.params.paymentMethod === constantUtil.PAYMENTCREDIT && //  need to Revert //Zayride
      generalSettings.data.isProfessionalsCorporateSiteCommissionCreditToWallet)
  ) {
    valueData =
      parseFloat(userData.wallet.availableAmount) +
      parseFloat(context.params.amount);
  } else {
    valueData =
      parseFloat(userData.wallet.availableAmount) -
      parseFloat(context.params.amount);
  }
  let onlineStatus = true;
  if (
    parseFloat(generalSettings.data.minimumWalletAmountToOnline) >
    parseFloat(valueData)
  ) {
    onlineStatus = false;
  }
  const responseJson = await this.adapter.model.updateOne(
    { "_id": context.params.professionalId, "status": constantUtil.ACTIVE },
    {
      "wallet.availableAmount": valueData,
      "onlineStatus": onlineStatus,
      "onlineStatusUpdatedAt": new Date(),
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

professionalEvent.updateOnGoingSecurityBooking = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortInfo.ongoingBooking": context.params.escortBookingId
        ? context.params.escortBookingId
        : null,
      "escortInfo.type": context.params.type ? context.params.type : "",
      "escortInfo.isOfficerAssigned": context.params.isOfficerAssigned
        ? context.params.isOfficerAssigned
        : false,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

professionalEvent.updateSecurityPendingReview = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortInfo.ongoingBooking": null,
      "escortInfo.pendingReview": context.params.escortBookingId,
      "escortInfo.type": context.params.type,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
//#region Invite And Earn

professionalEvent.inviteAndEarn = async function (context) {
  const inviteAndEarnSettings = await storageUtil.read(
    constantUtil.INVITEANDEARN
  );

  // joiner
  const joiner = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.joinerId.toString()),
  });
  // it means this professional not referred by another professional
  if (!joiner?.referredBy && joiner?.referredBy === "") {
    // if joner not found we dont know who is reffered
    return null;
  }
  const joinerJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.joinerId),
    },
    {
      "$inc": {
        "wallet.availableAmount":
          inviteAndEarnSettings.data.forProfessional.amountToJoiner,
      },
      "joinAmount": inviteAndEarnSettings.data.forProfessional.amountToJoiner,
      "joinerRideCount":
        (inviteAndEarnSettings?.data?.forProfessional?.rideCount || 0) === 0
          ? 0
          : (joiner?.joinerRideCount || 0) + 1,
    }
  );
  if (
    joinerJson.nModified &&
    parseFloat(
      inviteAndEarnSettings.data?.forProfessional?.amountToJoiner || 0
    ) > 0
  ) {
    this.broker.emit("transaction.joinCharge", {
      "clientId": context.params?.clientId,
      "amount": inviteAndEarnSettings.data.forProfessional.amountToJoiner,
      "userType": constantUtil.PROFESSIONAL,
      "data": {
        "id": joiner._id.toString(),
        "firstName": joiner.firstName,
        "lastName": joiner.lastName,
        "avatar": joiner.avatar,
        "phone": {
          "code": joiner.phone.code,
          "number": joiner.phone.number,
        },
      },
      "previousBalance": joiner.wallet.availableAmount,
      "currentBalance":
        joiner.wallet.availableAmount +
        inviteAndEarnSettings.data.forProfessional.amountToJoiner,
    });
  }
  // inviter
  const inviter = await this.adapter.model.findOne({
    "uniqueCode": joiner.referredBy,
  });
  if (!inviter) {
    return null;
  }
  const inviterWalletAmount =
    inviter.wallet.availableAmount +
    inviteAndEarnSettings.data.forProfessional.amountToInviter;

  const referrelTotalAmount =
    (inviter?.referrelTotalAmount ?? 0) +
    inviteAndEarnSettings.data.forProfessional.amountToInviter;

  const inviterJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(inviter._id.toString()),
    },
    {
      "wallet.availableAmount": inviterWalletAmount,
      "referrelTotalAmount": referrelTotalAmount,
      "joinCount": (inviter?.joinCount ?? 0) + 1,
      "invitationCount":
        (inviteAndEarnSettings?.data?.forProfessional?.rideCount || 0) === 0
          ? 1
          : inviter?.invitationCount,
    }
  );
  if (
    inviterJson.nModified &&
    parseFloat(
      inviteAndEarnSettings.data?.forProfessional?.amountToInviter || 0
    ) > 0
  ) {
    this.broker.emit("transaction.inviteCharge", {
      "clientId": context.params?.clientId,
      "amount": inviteAndEarnSettings.data.forProfessional.amountToInviter,
      "userType": constantUtil.PROFESSIONAL,
      "data": {
        "id": inviter._id.toString(),
        "firstName": inviter.firstName,
        "lastName": inviter.lastName,
        "avatar": inviter.avatar,
        "phone": {
          "code": inviter.phone.code,
          "number": inviter.phone.number,
        },
      },
      "previousBalance": inviter.wallet.availableAmount,
      "currentBalance":
        inviter.wallet.availableAmount +
        inviteAndEarnSettings.data.forProfessional.amountToInviter,
    });
  }
  return {
    "code": 200,
    "data": {},
  };
};
professionalEvent.updateJoinerRideCount = async function (context) {
  let responseMessage = constantUtil.SUCCESS;
  try {
    // const inviteAndEarnSettings = await storageUtil.read(
    //   constantUtil.INVITEANDEARN
    // );

    // joiner
    const joiner = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.joinerId.toString()),
    });
    // it means this professional not referred by another professional
    if (!joiner?.referredBy && joiner?.referredBy === "") {
      // if joner not found we dont know who is reffered
      return constantUtil.SUCCESS;
    }
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.joinerId),
      },
      {
        "joinerRideCount": (joiner?.joinerRideCount || 0) + 1,
      }
    );
    // // inviter
    // const inviter = await this.adapter.model.findOne({
    //   "uniqueCode": joiner.referredBy,
    // });
    // if (!inviter) {
    //   return constantUtil.SUCCESS;
    // }
    // await this.adapter.model.updateOne(
    //   {
    //     "_id": mongoose.Types.ObjectId(inviter._id.toString()),
    //   },
    //   {
    //     "invitationCount": (inviter?.invitationCount ?? 0) + 1,
    //   }
    // );
  } catch (e) {
    responseMessage = constantUtil.FAILED;
  }
  return responseMessage;
};
professionalEvent.inviterRideEarnings = async function (context) {
  let responseMessage = constantUtil.SUCCESS;
  try {
    // const inviteAndEarnSettings = await storageUtil.read(
    //   constantUtil.INVITEANDEARN
    // );

    // // joiner
    // const joiner = await this.adapter.model
    //   .findOne(
    //     { "_id": mongoose.Types.ObjectId(context.params.joinerId.toString()) },
    //     { "_id": 1, "referredBy": 1 }
    //   )
    //   .lean();
    // it means this professional not referred by another professional
    if (context.params.inviterUniqueCode) {
      // inviter
      const inviter = await this.adapter.model
        .findOne(
          { "uniqueCode": context.params.inviterUniqueCode },
          { "_id": 1, "wallet": 1 }
        )
        .lean();
      if (inviter) {
        const referralRideEarnings = context.params.amount;
        const inviterWalletAmount =
          inviter.wallet.availableAmount + referralRideEarnings;
        //
        const inviterJson = await this.adapter.model.updateOne(
          { "_id": mongoose.Types.ObjectId(inviter._id.toString()) },
          { "wallet.availableAmount": inviterWalletAmount }
        );
        if (inviterJson.nModified && referralRideEarnings > 0) {
          await this.broker.emit(
            "transaction.insertTransactionBasedOnUserType",
            {
              "transactionType": constantUtil.CONST_INCENTIVE,
              "transactionAmount": referralRideEarnings,
              "previousBalance": inviter?.wallet?.availableAmount || 0,
              "currentBalance": inviterWalletAmount,
              "transactionReason": context.params.transactionReason,
              "transactionStatus": constantUtil.SUCCESS,
              "bookingId": context.params?.bookingId,
              "refBookingId": context.params?.refBookingId,
              //  "transactionId": transactionId,
              "paymentType": constantUtil.CREDIT,
              "paymentGateWay": constantUtil.NONE,
              "gatewayResponse": {},
              "userType": constantUtil.ADMIN, // Debit From
              "userId": null, //bookingData.user._id.toString(), // Debit From
              "professionalId": null, // Debit From
              "paymentToUserType": constantUtil.PROFESSIONAL, // Credit To
              "paymentToUserId": inviter?._id?.toString(), // Credit To
              "adminId": null, // For All Admin Model Ref Id
            }
          );
        }
      }
    }
  } catch (e) {
    responseMessage = constantUtil.FAILED;
  }
  return responseMessage;
};
//#endregion Invite And Earn
professionalEvent.updateWalletAmountInBooking = async function (context) {
  // joiner
  const joiner = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.joinerId.toString()),
  });
};

professionalEvent.updateOnlineStatus = async function (context) {
  const updatedProfessionalData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    {
      "onlineStatus": context.params.status,
      "onlineStatusUpdatedAt": new Date(),
    }
  );
  if (!updatedProfessionalData) {
    return null;
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "STATUS UPDATED SUCCESSFULLY",
    };
  }
};

professionalEvent.sendMoneyToFriends = async function (context) {
  let toProfessionalData;
  const { PROFESSIONAL_NOT_FOUND, INVALID_PROFESSIONAL } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);
  //FROM PROFESSIONL
  const fromProfessionlData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
      },
      {
        "$inc": { "wallet.availableAmount": -context.params.amount }, // There is no $dec command, so used $inc command with negative sign
      },
      { "fields": { "password": 0, "accessToken": 0 } }
    )
    .lean();
  if (!fromProfessionlData) {
    throw new MoleculerError(INVALID_PROFESSIONAL);
  }
  // TO PROFESSIONAL
  if (context.params.receiverType === constantUtil.PROFESSIONAL) {
    toProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
        },
        { "$inc": { "wallet.availableAmount": context.params.amount } },
        { "fields": { "password": 0, "accessToken": 0 } }
      )
      .lean();
  } else {
    toProfessionalData = await this.schema.userModel.model
      .findOneAndUpdate(
        {
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
        },
        { "$inc": { "wallet.availableAmount": context.params.amount } },
        { "fields": { "password": 0, "accessToken": 0 } }
      )
      .lean();
  }
  if (!toProfessionalData) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  // send notification to TO USER
  if (toProfessionalData.deviceInfo[0]?.deviceId) {
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.MSG_WALLETRECHARGE,
        "timestamp": Date.now(),
        "message": "AMOUNT CREDITED",
        "details": lzStringEncode({ "message": "AMOUNT CREDITED" }),
      },
      "registrationTokens": [
        {
          "token": toProfessionalData.deviceInfo[0]?.deviceId || "",
          "id": toProfessionalData._id?.toString() || "",
          "deviceType": toProfessionalData.deviceInfo[0]?.deviceType || "",
          "platform": toProfessionalData.deviceInfo[0]?.platform || "",
          "socketId": toProfessionalData.deviceInfo[0]?.socketId || "",
        },
      ],
    };
    this.broker.emit("admin.sendFCM", notificationObject);
    this.broker.emit("admin.sendNotification", notificationObject);
  }
  return {
    "from": fromProfessionlData,
    "to": toProfessionalData,
  };
};
// professionalEvent.expireDocumentsProfessionlUnverified = async function () {
//   const professionalsData = await this.adapter.model.find({
//     'status': constantUtil.ACTIVE,
//     'updatedAt': { '$gte': new Date(Date.now() - 7 * 24 * 60 * 60 * 1000) },
//   })
//   if (!professionalsData[0]) return //=> notihing happend just return
//   let profileDocumentList = await storageUtil.read(
//     constantUtil.PROFILEDOCUMENTS
//   )
//   if (!profileDocumentList)
//     throw new MoleculerError('CANT TAKE DATA OF PROFEILE DOCUMENT FROM REDIS')

//   let driverDocumentList = await storageUtil.read(constantUtil.DRIVERDOCUMENTS)
//   if (!driverDocumentList)
//     throw new MoleculerError('CANT TAKE DATA OF DRIVER DOCUMENTS FROM REDIS')
//   // document fileter
//   const documentsFilter = (documents) => {
//     const ArrOfDocs = []
//     for (const props in documents) {
//       if (documents[props].status === constantUtil.ACTIVE)
//         ArrOfDocs.push(documents[props])
//     }
//     return ArrOfDocs
//   }
//   // filetered documents
//   profileDocumentList = documentsFilter(profileDocumentList)
//   driverDocumentList = documentsFilter(profileDocumentList)

//   // not avilable active list need to inform admin
//   if (!profileDocumentList[0] || !driverDocumentList[0])
//     throw new MoleculerError(
//       'NO ACTIVE PROFILE AND DRIVER DOUMENT PLEASE UPDATE '
//     )

//   await professionalsData.map((professional) => {
//     const driverVerify = false
//     // profile Document verify
//     professional.profileVerification.map((eachProfileDoc) => {
//       const findDoc = profileDocumentList.find(
//         (doc) => doc.docsName === eachProfileDoc.documentName
//       )
//       if (findDoc) {
//         if (findDoc.docsExpiry === true) {
//           const diff= new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
//           if(diff<=0)
//         }
//       }
//     })

//     // vehicle document verify

//     if (driverVerify === false) {
//       this.adapter.model.updateById(
//         mongoose.Types.ObjectId(professional._id.toString()),
//         {
//           'status': constantUtil.UNVERIFIED,
//           'isExpired': false,
//           'profileVerification': professional.profileVerification,
//           'vehicles': professional.vehicles,
//         }
//       )
//     }
//   })
// }

// DB ACCESS
professionalEvent.removePrefix0 = async function () {
  let professionalData = await this.adapter.model.find(
    {
      "phone.number": { "$regex": /^0/ },
    },
    {
      "_id": 1,
      "phone": 1,
      "clientId": 1,
    }
  );
  if (!professionalData[0]) {
    return {
      "code": 404,
      "data": {},
      "message": "IN PROFESSIONAL PHONE NUMBER START WITH 0 IS NOT FOUND",
    };
  }

  const duplicateDoc = {
    "count": 0,
    "activeProfessional": {
      "count": 0,
      "duplicates": [],
    },
    "unRegisteredProfessionals": {
      "count": 0,
      "duplicates": [],
    },
    "unverifiedDocProfessional": {
      "count": 0,
      "duplicates": [],
    },
    "unverifiedProfessionals": {
      "count": 0,
      "duplicates": [],
    },
  };
  professionalData = await professionalData.map(async (data) => {
    try {
      return await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(data._id.toString()),
        },
        {
          "$set": {
            "phone.number": data.phone.number.substring(1),
          },
        },
        { "fields": { "phone": 1, "_id": 1 }, "new": true }
      );
    } catch (error) {
      if (error.code === 11000) {
        duplicateDoc.count = duplicateDoc.count + 1;
        // find if this is active professionals
        const activeProfessional = await this.broker.emit(
          "professional.getProfessionalList",
          {
            "skip": 0,
            "limit": "10",
            "search": data.phone.number,
            "filter": "",
            "fromDate": "",
            "toDate": "",
            "userTypefilter": "",
            "clientId": data.clientId,
          }
        );

        if (
          activeProfessional &&
          activeProfessional[0] &&
          activeProfessional[0].data.count
        ) {
          duplicateDoc.activeProfessional.count =
            duplicateDoc.activeProfessional.count + 1;
          duplicateDoc.activeProfessional.duplicates.push(
            activeProfessional[0].data.response[0].data.phone
          );
          return;
        }
        // find if this is active professionals un registered professional
        const unRegisteredProfessionals = await this.broker.emit(
          "professional.getUnregisteredProfessionalList",
          {
            "skip": 0,
            "limit": "10",
            "search": data.phone.number,
            "filter": "",
            "fromDate": "",
            "toDate": "",
            "userTypefilter": "",
            "clientId": data.clientId,
          }
        );

        if (
          unRegisteredProfessionals &&
          unRegisteredProfessionals[0] &&
          unRegisteredProfessionals[0].data.count
        ) {
          duplicateDoc.unRegisteredProfessionals.count =
            duplicateDoc.unRegisteredProfessionals.count + 1;

          duplicateDoc.unRegisteredProfessionals.duplicates.push(
            unRegisteredProfessionals[0].data.response[0].data.phone
          );
          return;
        }
        // find if this is active professionals un registered professional
        const unverifiedDocProfessional = await this.broker.emit(
          "professionalAction.unverifiedDocProfessional",
          {
            "skip": 0,
            "limit": "10",
            "search": data.phone.number,
            "filter": "",
            "fromDate": "",
            "toDate": "",
            "userTypefilter": "",
            "clientId": data.clientId,
          }
        );
        if (
          unverifiedDocProfessional &&
          unverifiedDocProfessional[0] &&
          unverifiedDocProfessional[0].data.count
        ) {
          duplicateDoc.unverifiedDocProfessional.count =
            duplicateDoc.unverifiedDocProfessional.count + 1;
          duplicateDoc.unverifiedDocProfessional.duplicates.push(
            unverifiedDocProfessional[0].data.response[0].data.phone
          );
          return;
        }
        const unverifiedProfessionals = await this.broker.emit(
          "professional.getUnverifiedProfessionalList",
          {
            "skip": 0,
            "limit": "10",
            "search": data.phone.number,
            "filter": "",
            "fromDate": "",
            "toDate": "",
            "userTypefilter": "",
            "clientId": data.clientId,
          }
        );
        if (
          unverifiedProfessionals &&
          unverifiedProfessionals[0] &&
          unverifiedProfessionals[0].data.count
        ) {
          duplicateDoc.unverifiedProfessionals.count =
            duplicateDoc.unverifiedProfessionals.count + 1;
          duplicateDoc.unverifiedProfessionals.duplicates.push(
            unverifiedProfessionals[0].data.response[0].data.phone
          );
          return;
        }
      }
    }
  });
  professionalData = await Promise.all(professionalData);
  professionalData = professionalData.filter((data) => !!data !== false); // =>this line filter null values

  if (!professionalData[0]) {
    return {
      "code": 500,
      "data": {
        "duplicates": duplicateDoc,
        "data": professionalData,
      },
      "message":
        "IN PROFESSIONAL PHONE NUMBER START WITH O CANT UPDATE PLEASE CONTACT DEVELOPER",
    };
  } else {
    return {
      "code": 200,
      "data": {
        "duplicates": duplicateDoc,
        "data": professionalData,
      },
      "message": "PROFESSIONAL UPDATE SUCESSFULLY",
    };
  }
};
professionalEvent.forceLogout = async function () {
  const result = await this.adapter.model.updateMany(
    {},
    { "$set": { "deviceInfo": [] } }
  );
  return {
    "code": 200,
    "data": result,
    "message": "REQUESTED SUCCESSFULLY",
  };
};
professionalEvent.updateWalletAmount = async function (context) {
  let updateData = {
    "wallet.availableAmount": context.params.availableAmount,
    "wallet.freezedAmount": context.params.freezedAmount,
  };
  switch (context.params.requestFrom) {
    case constantUtil.WALLETWITHDRAWAL:
      updateData["lastWithdrawDate"] = new Date();
      break;
  }
  const response = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
      },
      { "$set": updateData },
      { "new": true }
    )
    .lean();
  if (!response)
    throw new MoleculerError(
      "SOME THING WENT WRONG IN UPDATE PROFESSIONAL WALLET AMOUNT",
      500
    );
  if (context.params?.isSendNotification) {
    const notificationObject = {
      "temprorySound": true,
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.USER,
        "action": constantUtil.WALLET,
        "timestamp": Date.now(),
        "message": context.params?.notificationMessage || "",
        "details": lzStringEncode({
          "message": context.params?.notificationMessage || "",
        }),
      },
      "registrationTokens": [
        {
          "token": response.deviceInfo[0]?.deviceId || "",
          "id": response._id?.toString() || "",
          "deviceType": response.deviceInfo[0]?.deviceType || "",
          "platform": response.deviceInfo[0]?.platform || "",
          "socketId": response.deviceInfo[0]?.socketId || "",
        },
      ],
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  return {
    "code": 200,
    "data": [],
    "message": "UPDATE SUCCESSFULLY",
  };
};

//
professionalEvent.addVerifiedCard = async function (context) {
  const userData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.professionalId),
    })
    .lean();
  if (!userData) {
    throw new MoleculerError("PROFESSIONAL NOT FOUND SOMETHING WENT WRONG");
  } else {
    const cardUpdating = await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      { "$push": { "cards": { ...context.params.card, "isVerified": true } } }
    );
    if (!cardUpdating.nModified) {
      return {
        "code": 500,
        "message": "SOMETHING WENT WRONG",
        "data": {},
      };
    } else {
      return {
        "code": 200,
        "message": "CARD ADDED SUCCESFULLY",
        "data": { ...context.params.card, "isVerified": true },
      };
    }
  }
};
//Added by Ajith
professionalEvent.getCardDetailsById = async function (context) {
  // let cardData = await this.adapter.model.findOne({
  //   "_id": mongoose.Types.ObjectId(context.params.professionalId),
  // });

  // if (!cardData) throw new MoleculerError("INVALID CARD DETAILS");
  // cardData = cardData.toJSON();
  // return {
  //   "code": 200,
  //   "message": "VALID CARD",
  //   "data": cardData.cards
  //     .find((card) => card._id.toString() === context.params.cardId.toString())
  //     .toJSON(),
  // };
  const cardData = await this.adapter.model.aggregate([
    {
      "$match": {
        "_id": mongoose.Types.ObjectId(context.params.professionalId),
      },
    },
    { "$unwind": "$cards" },
    {
      "$match": {
        "cards._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
      },
    },
    {
      "$project": {
        "_id": "$cards._id",
        "cardId": "$cards.cardId",
        "type": "$cards.type",
        "cardNumber": "$cards.cardNumber",
        "embedtoken": "$cards.embedtoken",
        "isVerified": "$cards.isVerified",
        "isCVVRequired": "$cards.isCVVRequired",
        "isDefault": "$cards.isDefault",
        "expiryMonth": "$cards.expiryMonth",
        "expiryYear": "$cards.expiryYear",
      },
    },
  ]);
  return {
    "code": 200,
    "message": "VALID CARD",
    "data": cardData?.[0] || null,
  };
};
//Added by Ajith
professionalEvent.updateDefaultCardStatusInActiveActive = async function (
  context
) {
  let updateDefaultCardStatus = {};
  try {
    updateDefaultCardStatus = await this.adapter.model.updateMany(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
        "cards.isDefault": true,
      },
      { "$set": { "cards.$.isDefault": false } }
    );
  } catch (error) {
    console.log(updateDefaultCardStatus);
  }
  if (context.params.cardId) {
    updateDefaultCardStatus = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
        "cards._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
      },
      {
        "$set": {
          "defaultPayment": constantUtil.PAYMENTCARD,
          "cards.$.isDefault": true,
        },
      }
    );
  }
  if (!updateDefaultCardStatus.nModified) {
    return {
      "code": 500,
      "message": "SOMETHING WENT WRONG",
      "data": {},
    };
  } else {
    return {
      "code": 200,
      "message": "DEFAULT CARD ACTIVATED",
      "data": {},
    };
  }
};
professionalEvent.getCategoryList = async function () {
  const clientId = context.params?.clientId;
  const response = await this.adapter.model.aggregate([
    {
      "$match": {
        "clientId": mongoose.Types.ObjectId(clientId),
        "$and": [
          { "status": constantUtil.ACTIVE },
          { "deviceInfo.deviceId": { "$exists": true } },
          { "deviceInfo.deviceId": { "$ne": "" } },
        ],
      },
    },
    {
      "$group": {
        "_id": { "$toLower": { "$substrCP": ["$firstName", 0, 1] } },
      },
    },
  ]);
  const category = [];
  // response.forEach((c) => !!c?._id === true && category.push(c?._id));
  response.forEach(
    (c) => !!c?._id === true && category.push({ "_id": c._id, "name": c._id })
  );
  return category.sort((a, b) => {
    return a.name.localeCompare(b.name);
  });
};

professionalEvent.getProfessionalListForNotification = async function (
  context
) {
  const clientId = context.params?.clientId;
  let query;
  switch (context.params.category?.toUpperCase()) {
    case constantUtil.ALPHABETICALORDER:
      query = {
        "status": constantUtil.ACTIVE,
        "firstName": new RegExp("^" + context.params.filter, "i"),
        "clientId": mongoose.Types.ObjectId(clientId),
      };
      break;

    case constantUtil.CITY:
      query = {
        "status": constantUtil.ACTIVE,
        "vehicles.serviceCategory": mongoose.Types.ObjectId(
          context.params.filter
        ),
        "clientId": mongoose.Types.ObjectId(clientId),
      };
      if (context.params.gender) {
        query["gender"] = context.params.gender;
      }
      break;

    case constantUtil.CONST_ONLINE:
      query = {
        "status": constantUtil.ACTIVE,
        "clientId": mongoose.Types.ObjectId(clientId),
        "onlineStatusUpdatedAt": {
          "$gte": context.params.startDate,
          "$lte": context.params.endDate,
        },
      };
      break;

    case constantUtil.CONST_CUSTOM:
      query = {
        // "status": constantUtil.ACTIVE,
        "clientId": mongoose.Types.ObjectId(clientId),
        "updatedAt": {
          "$gte": context.params.startDate,
          "$lte": context.params.endDate,
        },
      };
      break;

    case constantUtil.ALL:
      query = { "clientId": mongoose.Types.ObjectId(clientId) };
      break;

    case constantUtil.EXPIRED:
      query = {
        "clientId": mongoose.Types.ObjectId(clientId),
        "status": constantUtil.UNVERIFIED,
        "isExpired": true,
      };
      break;

    case constantUtil.REJECTED:
      query = {
        "clientId": mongoose.Types.ObjectId(clientId),
        "$or": [
          { "profileVerification.status": constantUtil.REJECTED },
          { "vehicles.vehicleDocuments.status": constantUtil.CONST_REJECTED },
        ],
      };
      break;
  }

  const count = await this.adapter.count({
    "query": query,
    "search": !!context.params.search === true ? context.params.search : "",
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  const response = await this.adapter.find({
    "offset": parseInt(context.params.skip || 0),
    "limit": parseInt(context.params.limit || 0),
    "query": query,
    "sort": "-updatedAt",
    "search": !!context.params.search === true ? context.params.search : "",
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  return {
    count,
    response,
  };
};
professionalEvent.membershipExpiring = async function () {
  const expiredProfessionals = await this.adapter.model
    .find(
      {
        "membershipDetails.isPlanExpired": false,
        "membershipDetails.endDate": { "$lt": Date.now() },
        "isEmailVerified": true,
      },
      {
        "phone": 1,
        "email": 1,
        "firstName": 1,
        "lastName": 1,
        "memberShipDetails": 1,
      }
    )
    .populate("memberShipDetails.membershipPlanId");

  const data = await this.adapter.model.update(
    {
      "membershipDetails.isPlanExpired": false,
      "membershipDetails.endDate": { "$lt": Date.now() },
      "isEmailVerified": true,
    },
    { "membershipDetails.isPlanExpired": true }
  );
  this.broker.emit("admin.sendBulkServiceMail", {
    "templateName": "PROFESSIONALMEMBERSHIPEXPIRY",
    "receiversData": expiredProfessionals.map((professional) => ({
      "phoneNumber": professional.phone.code + " " + professional.phone.number,
      "membershipPlanName":
        professional.memberShipDetails.membershipPlanId.data.name,
      "totalNoOfRides": professional.memberShipDetails.totalNoOfRides,
      "endDate": professional.memberShipDetails.totalNoOfRides,
      "startDate": professional.memberShipDetails.startDate,
      "usedRides": professional.memberShipDetails.usedRides,
      "validityDays":
        professional.memberShipDetails.membershipPlanId.data.validityDays,
      "planType": professional.memberShipDetails.membershipPlanId.data.planType,
      "firstName": professional.firstName,
      "email": professional.email,
      "walletAvailableAmount": professional.wallet.availableAmount,
    })),
  });
};

// Cron jobs
professionalEvent.membershipPlanExpiring = async function () {
  const professionalsData = await this.adapter.model
    .find({
      "membershipDetails.endDate": { "$lt": new Date() },
    })
    .populate("membershipDetails.membershipPlanId");

  for (const professional of professionalsData) {
    let shiftedData;
    let isExpiredFreePlan = {};

    professional.membershipDetails?.forEach((eachPlans) => {
      if (eachPlans.endDate < new Date()) {
        shiftedData = professional.membershipDetails.shift();
        professional.membershipDetailsHistory.push(shiftedData);
        if (shiftedData?.membershipPlanId?.data?.isDefault) {
          isExpiredFreePlan = { "isExpiredFreePlan": true };
        }
      }
    });

    const data = await this.adapter.model.updateOne(
      {
        "_id": professional._id,
      },
      {
        "activeMembershipDetails": professional.membershipDetails[0]
          ? professional.membershipDetails[0]
          : { ...shiftedData, "isPlanExpired": true },
        "membershipDetails": professional.membershipDetails,
        "membershipDetailsHistory": professional.membershipDetailsHistory,
        ...isExpiredFreePlan,
      }
    );
    console.log(data);
  }
};
professionalEvent.getMembershipList = async function (context) {
  const query = {};
  if (context.params.filter === "ALL") {
    query["membershipDetails.0"] = { "$exists": true };
  }

  const count = await this.adapter.count({
    "query": query,
    "search": context.params.search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.number",
      "phone.code",
    ],
  });
  const professionals = await this.adapter.find({
    "limit": context.params.limit,
    "offset": context.params.skip,
    "query": query,
    "populate": [
      "membershipDetails.membershipPlanId",
      "activeMembershipDetails.membershipPlanId",
    ],
    "sort": "-updatedAt",
    "search": context.params.search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.number",
      "phone.code",
    ],
  });
  return {
    "code": 200,
    "message": "SUCCESS",
    "data": { "count": count, "response": professionals },
  };
};

//--------- Wallet Point Added Start --------------------------
professionalEvent.updateWalletRewardPoint = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId.toString()),
    },
    {
      "$inc": {
        "wallet.rewardPoints": convertToFloat(
          context?.params?.rewardPoints || 0
        ),
      },
    }
    // {
    //   "upsert": true,
    // }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
//--------- Wallet Point Added End --------------------------
professionalEvent.getProfessionalDetails = async function (context) {
  let phone = [];
  const query = {};
  if (context.params.professionalId) {
    query._id = context.params.professionalId;
  } else {
    phone = context.params.phoneNumber.split("-");
    //query["clientId"] = mongoose.Types.ObjectId(context.params.clientId);
    query["phone.code"] = phone[0] || 0;
    query["phone.number"] = phone[1] || 0;
  }
  let responseJson = await this.adapter.model.find(query, {
    "_id": 1,
    "phone": 1,
    "avatar": 1,
    "onlineStatus": 1,
    "status": 1,
    "wallet": 1,
    "firstName": 1,
    "lastName": 1,
    "lastRedeemDate": 1,
    "lastWithdrawDate": 1,
  });

  responseJson = responseJson && responseJson[0];
  // if (!responseJson) throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  return responseJson;
};

professionalEvent.checkServiceAreaUsingId = async function (context) {
  const professionalCount = await this.adapter.model.count({
    "vehicles.serviceCategory": mongoose.Types.ObjectId(
      context.params.serviceAreaId.toString()
    ),
  });
  return professionalCount;
};

// #region DataMigration Script

professionalEvent.professionalProfileDocumentsDataMigrationScript =
  async function (context) {
    try {
      const professionalData = await this.adapter.model
        .find(
          {
            "isExpired": true,
          },
          {
            "_id": 1,
            "phone": 1,
            "profileVerification": 1,
            "vehicles": 1,
          }
        )
        .lean()
        .limit(100);
      if (!professionalData)
        throw new MoleculerError("ERROR IN PROFESSIONAL DETAILS");

      const expiryDate = new Date("2034 Dec 31").toISOString();

      await professionalData.map(async (profileVerification) => {
        const updateDocument = [],
          vehicleDocuments = [];
        profileVerification.profileVerification.map((each) => {
          updateDocument.push({
            "_id": each._id,
            "documentName": each.documentName,
            "expiryDate":
              each.expiryDate === "" || each.expiryDate === null
                ? ""
                : expiryDate,
            "documents": each.documents,
            "uploadedStatus": constantUtil.VERIFIED,
            "reason": each.reason,
            "status": constantUtil.VERIFIED,
          });

          const xx = "fdgfdsg";
        });
        const vehicles = profileVerification.vehicles.map((vehicle) => {
          vehicle.status = constantUtil.ACTIVE;
          vehicle.vehicleDocuments = vehicle.vehicleDocuments.map(
            (vehicleDocument) => {
              if (vehicleDocument._id === vehicleDocument._id) {
                // vehicleDocument.documentName = vehicleDocument.documentName;
                vehicleDocument.expiryDate =
                  vehicleDocument.expiryDate === "" ||
                  vehicleDocument.expiryDate === null
                    ? ""
                    : expiryDate;
                // vehicleDocument.documents = context.params.documents;
                vehicleDocument.uploadedStatus = constantUtil.VERIFIED;
                vehicleDocument.status = constantUtil.VERIFIED;
              }
              return vehicleDocument;
            }
          );
          return vehicle;
        });
        // setInterval(async () => {
        const status = await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(profileVerification._id.toString()),
          },
          {
            "isExpired": false,
            "status": constantUtil.ACTIVE,
            "profileVerification": updateDocument,
            // "vehicles.$.vehicleDocuments": vehicleDocuments,
            "vehicles": vehicles,
          }
        );
        // }, 500);

        const x = "fdgfdsg";
      });
      const status = "sucess";
    } catch (e) {
      const expiryDate = new Date();
    }
  };
// #endregion DataMigration Script

professionalEvent.sendPromotion = async function (context) {
  let mailData,
    message = "",
    trustedContactsLog = [];
  const responseMessage = constantUtil.SUCCESS;
  const clientId = context.params?.clientId;
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));

  const selectedRecordCount = 400;
  const loopCount = parseInt(ids.length / selectedRecordCount);
  //
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const emergencyResponseJson = await storageUtil.read(
    constantUtil.EMERGENCYSMSSETTING
  );
  if (context.params.type === constantUtil.EMERGENCY) {
    message = emergencyResponseJson.data?.notifyMessage || "";
    context.params.template = {
      "title": "emergencyNotification",
      "content": emergencyResponseJson.data?.notifyMessage,
      "image": null,
      "subject": null,
    };
    //
    message = message.replace(/{{siteTitle}}/g, generalSettings.data.siteTitle);
    message = message.replace(/{{senderName}}/g, context.params.senderName);
    message = message.replace(/{{trackUrl}}/g, context.params.trackLink);
  } else {
    message = context.params.template?.content || "";
  }
  //------ For Email (Get Email Template) -------------
  if (context.params.type === constantUtil.EMAIL) {
    mailData = await this.broker.emit("admin.getEmailNotificationTemplate", {
      "templateName": context.params.template.title,
    });
    mailData = mailData && mailData[0];
  }
  //------ For Email (Get Email Template) -------------
  for (let i = 0; i <= loopCount; i++) {
    const selectedRecordIds = ids.splice(0, selectedRecordCount);
    const professionalList = await this.adapter.model
      .find(
        {
          // "status": constantUtil.ACTIVE,
          "_id": { "$in": selectedRecordIds },
          "clientId": mongoose.Types.ObjectId(clientId),
        },
        {
          "_id": 1,
          "firstName": 1,
          "lastName": 1,
          "email": 1,
          "phone": 1,
          "isEmailVerified": 1,
          "wallet.availableAmount": 1,
          "deviceInfo.deviceId": 1,
          "deviceInfo.deviceType": 1,
          "deviceInfo.platform": 1,
          "deviceInfo.socketId": 1,
        }
      )
      .lean();
    // professionalList.forEach(async (reciverData) => {
    //   if (!reciverData || !reciverData.deviceInfo || !reciverData.deviceInfo[0])
    //     return;

    if (
      context.params.type === constantUtil.PUSH ||
      (context.params.type === constantUtil.EMERGENCY &&
        emergencyResponseJson.data?.isEnablePushNotify)
    ) {
      let notificationObject;
      if (context.params.type === constantUtil.EMERGENCY) {
        message = message.replace(/{{reciverName}}/g, "");
      }
      try {
        notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": context.params.userType.toUpperCase(),
            "action":
              context.params.type === constantUtil.EMERGENCY
                ? constantUtil.ACTION_EMERGENCY
                : constantUtil.ACTION_PROMOTION,
            "image": context.params.template?.image || "",
            "timestamp": Date.now(),
            "message": message,
            "details": lzStringEncode(context.params.template),
            // "details": lzStringEncode(message),
            "urlLink": context.params.trackLink,
          },
          "registrationTokens": professionalList.map((professional) => {
            return {
              "token": professional?.deviceInfo[0]?.deviceId || "",
              "id": professional?._id?.toString(),
              "deviceType": professional?.deviceInfo[0]?.deviceType || "",
              "platform": professional?.deviceInfo[0]?.platform || "",
              "socketId": professional?.deviceInfo[0]?.socketId || "",
            };
          }),
        };
        //
        if (professionalList.length > 0) {
          trustedContactsLog = professionalList.map((professional) => {
            return {
              "userType": constantUtil.PROFESSIONAL,
              "professionalId": professional?._id?.toString(),
              "phone": {
                "code": professional.phone.code,
                "number": professional.phone.number,
              },
              "notifyType": constantUtil.PUSH,
            };
          });
        }
      } catch (error) {
        console.log("error in notification object", error);
      }

      // if (notificationObject.registrationTokens[0].token === "") {
      //   return; // =>just retun bescuse without token we cant send notification
      // }

      // send notification
      this.broker.emit("admin.sendFCM", notificationObject);
      // this.broker.emit('socket.sendNotification', notificationObject)
    } else if (context.params.type === constantUtil.EMAIL) {
      professionalList.forEach(async (reciverData) => {
        if (reciverData.email && reciverData.isEmailVerified) {
          this.broker.emit("admin.sendPromotionMail", {
            "mailData": mailData,
            "templateName": context.params.template.title,
            ...(await promtionMailObject(reciverData)),
          });
        }
      });
    } else if (context.params.type === constantUtil.SMS) {
      if (
        context.params.requestFrom === constantUtil.CONST_APP &&
        context.params.phone?.number
      ) {
        professionalList.push({ phone: context.params.phone });
      }
      professionalList.forEach(async (reciverData) => {
        const phoneNumber =
          reciverData?.phone?.code?.trim() + reciverData?.phone?.number?.trim();
        this.broker.emit("booking.sendSMSAlertForPromotions", {
          "notifyType": constantUtil.PROMOTION,
          "notifyFor": constantUtil.PROFESSIONAL,
          "promotionMessage": context.params.template.content,
          "phoneNumber": phoneNumber,
        });
      });
    }
    //#region Emergency Notification
    // EMAIL
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.emailConfigurationEnable
    ) {
      professionalList.forEach(async (reciverData) => {
        if (reciverData.email && reciverData.isEmailVerified) {
          // const trackLink = `${generalSettings.data.redirectUrls.trackBooking}${context.params.bookingId}`;
          reciverData["siteTitle"] = generalSettings.data.siteTitle;
          reciverData["senderName"] = context.params.senderName;
          reciverData["reciverName"] =
            reciverData.firstName + " " + reciverData.lastName;
          reciverData["trackUrl"] = context.params.trackLink;
          //
          this.broker.emit("admin.sendServiceMail", {
            "clientId": clientId,
            "templateName": "emergencyNotification",
            ...(await promtionMailObject(reciverData)),
          });
          //
          trustedContactsLog.push({
            "userType": constantUtil.PROFESSIONAL,
            "professionalId": reciverData?._id?.toString(),
            "phone": {
              "code": reciverData.phone.code,
              "number": reciverData.phone.number,
            },
            "notifyType": constantUtil.EMAIL,
          });
        }
      });
    }
    // SMS
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.isEnableSMSGatwayOTP
    ) {
      // userList.forEach(async (reciverData) => {
      context.params.phone?.forEach(async (reciverData) => {
        message = message.replace(/{{reciverName}}/g, reciverData?.fullName);
        const phoneNumber =
          reciverData?.code?.trim() + reciverData?.number?.trim();
        //
        this.broker.emit("booking.sendSMSAlertForPromotions", {
          "notifyType": constantUtil.EMERGENCY,
          "notifyFor": constantUtil.PROFESSIONAL,
          "promotionMessage": message,
          "phoneNumber": phoneNumber,
        });
        //
        trustedContactsLog.push({
          "userType": constantUtil.PROFESSIONAL,
          "professionalId": null,
          "fullName": reciverData.fullName,
          "phone": {
            "code": reciverData.code?.trim(),
            "number": reciverData.number?.trim(),
          },
          "notifyType": constantUtil.SMS,
        });
      });
    }
    // WhatsUp
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.isEnableWhatsupOTP
    ) {
      // Need to Work WhatsUp Notification
      context.params.phone?.forEach(async (reciverData) => {
        message = message.replace(/{{reciverName}}/g, reciverData.fullName);
        //
        this.broker.emit("booking.sendWhatsupAlert", {
          "clientId": context.params?.clientId,
          "phoneCode": reciverData?.code?.trim(),
          "phoneNumber": reciverData?.number?.trim(),
          "rideId": null,
          "bookingFrom": constantUtil.ADMIN,
          "notifyType": constantUtil.NONE,
          "notifyFor": constantUtil.ALL,
          "alertType": constantUtil.OTP,
          "message": message,
        });
        //
        //
        trustedContactsLog.push({
          "userType": constantUtil.PROFESSIONAL,
          "professionalId": null,
          "fullName": reciverData.fullName,
          "phone": {
            "code": reciverData.code?.trim(),
            "number": reciverData.number?.trim(),
          },
          "notifyType": constantUtil.WHATSUP,
        });
      });
    }
    //#endregion Emergency Notification
  }
  return {
    "code": 200,
    "message": responseMessage,
    "trustedContactsLog": trustedContactsLog,
  };
};
//#region Share Ride
professionalEvent.updateProfessionalShareRide = async function (context) {
  let responseData = null;
  try {
    switch (context.params?.actionStatus?.toUpperCase()) {
      case constantUtil.STATUS_UPCOMING:
        responseData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "$push": {
                "bookingInfo.upcomingBooking": {
                  "bookingId": context.params.bookingId,
                  // "bookingDate": new Date(context.params.bookingDate),
                  // "estimationTime": context.params.estimationTime,
                },
              },
            },
            { "new": true }
          )
          .lean();
        break;

      case constantUtil.STATUS_ONGOING:
        responseData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "bookingInfo.ongoingBooking": context.params?.bookingId,
              // "$pull": {
              //   "bookingInfo.upcomingBooking": {
              //     "bookingId": mongoose.Types.ObjectId(context.params.bookingId),
              //     // "bookingDate": new Date(context.params.bookingDate),
              //     // "estimationTime": context.params.estimationTime,
              //   },
              // },
            },
            { "new": true }
          )
          .lean();
        break;

      case constantUtil.ENDED:
      case constantUtil.CANCELLED:
      case constantUtil.USERCANCELLED:
      case constantUtil.PROFESSIONALCANCELLED:
        responseData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "$pull": {
                "bookingInfo.upcomingBooking": {
                  "bookingId": mongoose.Types.ObjectId(
                    context.params.bookingId
                  ),
                  // "bookingDate": new Date(context.params.bookingDate),
                  // "estimationTime": context.params.estimationTime,
                },
              },
            },
            { "new": true }
          )
          .lean();
        break;
    }
    // }
  } catch (e) {
    responseData = null;
  }
  return responseData;
};
professionalEvent.getEndRideIssueList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "$or": [
        // { "bookingInfo.pendingReview": { "$ne": null } },
        { "bookingInfo.ongoingBooking": { "$ne": null } },
      ],
    },
  });
  // FOR BOOKING BY FILTER
  query.push(
    {
      "$lookup": {
        "from": "bookings",
        "localField": "bookingInfo.ongoingBooking",
        "foreignField": "_id",
        "as": "bookingList",
      },
    },
    {
      "$unwind": {
        "path": "$bookingList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$match": {
        "bookingList.bookingStatus": {
          "$in": [
            constantUtil.ENDED,
            constantUtil.EXPIRED,
            constantUtil.USERDENY,
            constantUtil.USERCANCELLED,
            constantUtil.PROFESSIONALCANCELLED,
          ],
        },
      },
    },
    {
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          {
            "$project": {
              "_id": "$_id",
              "bookingStatus": "$bookingList.bookingStatus",
              "bookingId": "$bookingList.bookingId",
              "refBookingId": "$bookingList._id",
              "bookingType": "$bookingList.bookingType",
              "bookingBy": "$bookingList.bookingBy",
              "bookingDate": "$bookingList.bookingDate",
              "ongoingBooking": "$bookingInfo.ongoingBooking",
              "pendingReview": "$bookingInfo.pendingReview",
              "Name": { "$concat": ["$firstName", "$lastName"] },
              "PhoneNumber": { "$concat": ["$phone.code", "$phone.number"] },
            },
          },
          { "$sort": { "bookingList.bookingDate": -1 } },
          { "$skip": skip },
          { "$limit": limit },
        ],
      },
    }
  );

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
professionalEvent.updateBookingInfoIds = async function (context) {
  if (context.params.updatedId === "ongoingBooking") {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      },
      {
        "$set": { "bookingInfo.ongoingBooking": null },
      }
    );
  } else if (context.params.updatedId === "pendingReview") {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      },
      {
        "$set": { "bookingInfo.pendingReview": null },
      }
    );
  }

  return constantUtil.SUCCESS;
};
//#endregion Share Ride
//#region Professional Incentive
professionalEvent.calculateAndUpdateIncentiveAmount = async function (context) {
  const { INFO_INCENTIVE_CREDIT } = await notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  try {
    // const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    // const professionalList = await this.adapter.model
    //   .find(
    //     {
    //       "status": constantUtil.ACTIVE,
    //       "onlineStatus": false,
    //       "updatedAt": {
    //         "$gte": new Date(new Date().getTime() - 1000 * 60 * 1),
    //       }, // 1 minutes ago (from now) / Last 1 minutes Updated Records
    //     },
    //     { "_id": 1, "onlineStatus": 1, "updatedAt": 1 }
    //   )
    //   .lean();
    // // await professionalList?.map(async (professional) => {
    // //   await this.broker.emit("log.updateSignupLog", {
    // //     "name": constantUtil.LOGOUT,
    // //     "userType": constantUtil.PROFESSIONAL,
    // //     "professionalId": professional._id?.toString(),
    // //   });
    // // });
    const currentAvailableAmount =
      parseFloat(context.params.availableAmount) +
      parseFloat(context.params.amount);

    const responseJson = await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
      { "$set": { "wallet.availableAmount": currentAvailableAmount } }
    );
    //-------- Transaction Entry Start ---------------------
    if (responseJson.nModified) {
      this.broker.emit("transaction.insertTransactionBasedOnUserType", {
        "transactionType": constantUtil.CONST_INCENTIVE,
        "transactionAmount": context.params.amount,
        "previousBalance": context.params.availableAmount,
        "currentBalance": currentAvailableAmount,
        "transactionReason": INFO_INCENTIVE_CREDIT,
        "transactionStatus": constantUtil.SUCCESS,
        //  "transactionId": transactionId,
        "paymentType": constantUtil.CREDIT,
        "paymentGateWay": constantUtil.NONE,
        "gatewayResponse": {},
        "userType": constantUtil.ADMIN, // Debit From
        "userId": null, //bookingData.user._id.toString(), // Debit From
        "professionalId": null, // Debit From
        "paymentToUserType": constantUtil.PROFESSIONAL, // Credit To
        "paymentToUserId": context.params.professionalId, // Credit To
        "adminId": context.params.incentiveId, // For All Admin Model Ref Id
      });
    }
    //-------- Transaction Entry End ---------------------
  } catch (e) {
    const xx = e.message;
  }
};

professionalEvent.getProfessionalListByServiceAreaId = async function (
  context
) {
  try {
    const responseJson = await this.adapter.model
      .find(
        {
          "clientId": mongoose.Types.ObjectId(context.params.clientId),
          "serviceAreaId": mongoose.Types.ObjectId(
            context.params.serviceAreaId
          ),
        },
        {
          "_id": 1,
          "wallet": 1,
          "serviceAreaId": 1,
          // "wallet.availableAmount": 1,
        }
      )
      .lean();

    return responseJson || [];
  } catch (e) {}
};

//#endregion Professional Incentive

professionalEvent.checkProfessionalAvail = async function (context) {
  let professionalData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
      "status": constantUtil.ACTIVE,
    })
    .lean();
  if (professionalData) {
    professionalData = professionalObject(professionalData);
  } else {
    professionalData = null;
  }
  return professionalData;
};

professionalEvent.checkUniqueReferralCode = async function (context) {
  let professionalData = await this.adapter.model
    .findOne(
      {
        "_id": {
          "$ne": mongoose.Types.ObjectId(context.params.professionalId),
        },
        "uniqueCode": context.params.uniqueCode,
      },
      { "_id": 1, "firstName": 1, "lastName": 1, "uniqueCode": 1 }
    )
    .lean();
  if (professionalData) {
    professionalData = { "isValidUniqueCode": false };
  } else {
    professionalData = { "isValidUniqueCode": true };
  }
  return professionalData;
};

//-------------------------
module.exports = professionalEvent;
