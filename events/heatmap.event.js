const moment = require("moment");
const mongoose = require("mongoose");
const each = require("sync-each");
const turf = require("@turf/turf");
const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const commonUtil = require("../utils/common.util");
const mappingUtil = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const heatmapEvent = {};

//-----------------------------------------
heatmapEvent.manageHeatmap = async function (context) {
  let status = "",
    heatmapSetupData;
  try {
    if (context.params.serviceAreaId) {
      heatmapSetupData = await this.schema.adminModel.model
        .findOne(
          {
            "name": constantUtil.HEATMAP,
            "isActive": true,
            "data.serviceAreaId": mongoose.Types.ObjectId(
              context.params.serviceAreaId
            ),
          },
          { "_id": 1 }
        )
        .lean();
    }
    const bookingDate = helperUtil.toUTC(context.params.bookingDate);
    if (heatmapSetupData?._id?.toString()) {
      await this.schema.adapter.model.create({
        "userId": context.params.userId,
        "bookingDate": bookingDate,
        "serviceAreaId": context.params.serviceAreaId,
        "heatmapId": heatmapSetupData?._id?.toString(),
        // "locationTileId": context.params.locationTileId,
        "plusCodePrefix": context.params.plusCodePrefix,
        "locationName": context.params.locationName,
        "location.coordinates": [
          context.params.latLng?.lng,
          context.params.latLng?.lat,
        ],
      });
    }
    //Delete Log History (30 Days older Records)
    await this.schema.adapter.model.deleteMany({
      "bookingDate": { "$lt": new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) }, //30 Days older Records
    });
    status = constantUtil.SUCCESS;
  } catch (e) {
    status = constantUtil.FAILED;
  }
  return { "status": status };
};

heatmapEvent.getHeatmapDataByLocation = async function (context) {
  let status = "",
    professionalCurrentServiceAreaId = null;

  const responseJson = [];
  try {
    let professionalCurrentServiceArea = null,
      heatmapRedisData = null;
    //#region  professional Current Service Area
    if (!context.params.serviceAreaId) {
      professionalCurrentServiceArea = await this.broker.emit(
        "category.getServieCategory",
        {
          "clientId": context.params.clientId,
          "lat": context.params.lat,
          "lng": context.params.lng,
          "categoryName": constantUtil.CONST_RIDE.toLowerCase(), //Must be Lowercase
        }
      );
      professionalCurrentServiceArea =
        professionalCurrentServiceArea && professionalCurrentServiceArea[0];
    }
    //#endregion  professional Current Service Area
    professionalCurrentServiceAreaId =
      context.params.serviceAreaId ||
      professionalCurrentServiceArea?._id?.toString() ||
      null;
    const heatmapRedisDataList = await storageUtil.read(constantUtil.HEATMAP);
    heatmapRedisDataList?.forEach((item) => {
      if (
        item.isActive === true &&
        item.data?.serviceAreaId?.toString() ===
          professionalCurrentServiceAreaId
      ) {
        heatmapRedisData = item;
      }
    });
    const maxSearchDistance = heatmapRedisData?.data?.searchRadius || 100; // Meter
    const maxSearchTimeInterval = heatmapRedisData?.data?.timeInterval || 5; // Minutes
    const fakeUsedCount = heatmapRedisData?.data?.usedCount || 0; // For Manual Heat - Tried Booking Count
    //-------------------------
    const professionaCountList = await this.schema.professionalModel.model.find(
      {
        "status": constantUtil.ACTIVE,
        "onlineStatus": true,
        "bookingInfo.ongoingBooking": null,
        "location": {
          // "$nearSphere": {
          "$near": {
            "$geometry": {
              "type": "Point",
              "coordinates": [
                parseFloat(context.params.lng),
                parseFloat(context.params.lat),
              ],
            },
            "$minDistance": 0,
            "$maxDistance": maxSearchDistance, //context.params.radius,
          },
        },
      }
    );
    const professionaCount = professionaCountList?.length || 0;
    //-------------------------
    const fromDate = new Date(
      new Date().getTime() - 1000 * 60 * maxSearchTimeInterval
    );
    const toDate = new Date();
    //-------------------------
    const bookingTriedData = await this.schema.adapter.model
      .find(
        {
          "location": {
            // "$nearSphere": {
            "$near": {
              "$geometry": {
                "type": "Point",
                "coordinates": [
                  parseFloat(context.params.lng),
                  parseFloat(context.params.lat),
                ],
              },
              "$minDistance": 0,
              "$maxDistance": maxSearchDistance, //context.params.radius,
            },
          },
          "bookingDate": { "$gte": fromDate, "$lt": toDate },
        },
        {
          "_id": 1,
          "location": 1,
          "serviceAreaId": 1,
          "plusCodePrefix": 1,
          "heatmapId": 1,
        }
      )
      .populate("heatmapId")
      .sort({ "plusCodePrefix": 1 })
      .lean();
    //--------

    //--------
    let lastPlusCodePrefix = "";
    // professionalPercentage = professionaCount / bookingTriedData.length;
    await bookingTriedData.map(async (item) => {
      let peakFare = 0,
        density = 0,
        professionalPercentage = 0;
      const plusCodePrefixCount = await bookingTriedData.filter(
        (eachDoc) => eachDoc?.plusCodePrefix === item.plusCodePrefix
      ).length;
      professionalPercentage =
        (professionaCount / (plusCodePrefixCount + fakeUsedCount)) * 100;
      if (item.heatmapId?.data) {
        if (
          professionalPercentage >= item.heatmapId?.data?.fromValue &&
          professionalPercentage < item.heatmapId?.data?.toValue
        ) {
          peakFare = item.heatmapId?.data?.peakFare || 0;
        } else if (
          professionalPercentage >= item.heatmapId?.data?.fromValue1 &&
          professionalPercentage < item.heatmapId?.data?.toValue1
        ) {
          peakFare = item.heatmapId?.data?.peakFare1 || 0;
        } else if (
          professionalPercentage >= item.heatmapId?.data?.fromValue2 &&
          professionalPercentage < item.heatmapId?.data?.toValue2
        ) {
          peakFare = item.heatmapId?.data?.peakFare2 || 0;
        } else if (
          professionalPercentage >= item.heatmapId?.data?.fromValue3 &&
          professionalPercentage < item.heatmapId?.data?.toValue3
        ) {
          peakFare = item.heatmapId?.data?.peakFare3 || 0;
        } else if (
          professionalPercentage >= item.heatmapId?.data?.fromValue4 &&
          professionalPercentage <= item.heatmapId?.data?.toValue4
        ) {
          peakFare = item.heatmapId?.data?.peakFare4 || 0;
        }
      }
      density = parseInt((plusCodePrefixCount / bookingTriedData.length) * 100);
      //---------------------------------
      // const newRecordData = {
      //   "lat": item.location.coordinates[1],
      //   "lng": item.location.coordinates[0],
      //   "peakFare":
      //     peakFare > 0 && lastPlusCodePrefix !== item.plusCodePrefix
      //       ? peakFare.toString() + " X"
      //       : "",
      //   "plusCodePrefix": item.plusCodePrefix,
      //   "density": density,
      // };
      // if (!responseJson.includes(newRecordData)) {
      //   responseJson.push(newRecordData);
      // }
      const isDuplicate = await responseJson.filter(
        (obj) =>
          obj.lat === item.lat &&
          obj.lng === item.lng &&
          obj.plusCodePrefix === item.plusCodePrefix
      ).length;
      if (!isDuplicate) {
        responseJson.push({
          "lat": item.location.coordinates[1],
          "lng": item.location.coordinates[0],
          "peakFare":
            // peakFare > 0 && lastPlusCodePrefix !== item.plusCodePrefix ? peakFare.toString() + " X" : ""
            peakFare > 0 ? peakFare.toString() + " X" : "",
          "plusCodePrefix": item.plusCodePrefix,
          "density": density,
        });
      }
      //---------------------------------
      lastPlusCodePrefix = item.plusCodePrefix;
    });
    status = constantUtil.SUCCESS;
  } catch (e) {
    status = constantUtil.FAILED;
    responseJson.push({});
  }
  return {
    "status": status,
    "response": responseJson,
    "serviceAreaId": professionalCurrentServiceAreaId,
  };
};

heatmapEvent.calculatePeakFareUsingHeatmap = async function (context) {
  let status = "",
    peakFare = 0;

  const professionalCount = context.params.professionalCount || 0,
    userCurrentServiceAreaId = context.params.serviceAreaId || "";
  try {
    //----------------
    let heatmapRedisData = null;
    const heatmapRedisDataList = await storageUtil.read(constantUtil.HEATMAP);
    heatmapRedisDataList?.forEach((item) => {
      if (
        item.isActive === true &&
        item.data?.serviceAreaId?.toString() === userCurrentServiceAreaId
      ) {
        heatmapRedisData = item;
      }
    });
    const maxSearchDistance = heatmapRedisData?.data?.searchRadius || 100; // Meter
    const maxSearchTimeInterval = heatmapRedisData?.data?.timeInterval || 5; // Minutes
    const fakeUsedCount = heatmapRedisData?.data?.usedCount || 0; // For Manual Heat - Tried Booking Count
    //-------------------------
    const fromDate = new Date(
      new Date().getTime() - 1000 * 60 * maxSearchTimeInterval
    );
    const toDate = new Date();
    //-------------------------
    const bookingTriedCountList = await this.schema.adapter.model
      .find({
        "location": {
          // "$nearSphere": {
          "$near": {
            "$geometry": {
              "type": "Point",
              "coordinates": [
                parseFloat(context.params.lng),
                parseFloat(context.params.lat),
              ],
            },
            "$minDistance": 0,
            "$maxDistance": maxSearchDistance, //context.params.radius,
          },
        },
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
      })
      .lean();
    const bookingTriedCount = bookingTriedCountList.length || 0;
    //--------
    const professionalPercentage =
      (professionalCount / (bookingTriedCount + fakeUsedCount)) * 100;
    //--------
    if (
      professionalPercentage >= heatmapRedisData?.data?.fromValue &&
      professionalPercentage < heatmapRedisData?.data?.toValue
    ) {
      peakFare = heatmapRedisData?.data?.peakFare || 0;
    } else if (
      professionalPercentage >= heatmapRedisData?.data?.fromValue1 &&
      professionalPercentage < heatmapRedisData?.data?.toValue1
    ) {
      peakFare = heatmapRedisData?.data?.peakFare1 || 0;
    } else if (
      professionalPercentage >= heatmapRedisData?.data?.fromValue2 &&
      professionalPercentage < heatmapRedisData?.data?.toValue2
    ) {
      peakFare = heatmapRedisData?.data?.peakFare2 || 0;
    } else if (
      professionalPercentage >= heatmapRedisData?.data?.fromValue3 &&
      professionalPercentage < heatmapRedisData?.data?.toValue3
    ) {
      peakFare = heatmapRedisData?.data?.peakFare3 || 0;
    } else if (
      professionalPercentage >= heatmapRedisData?.data?.fromValue4 &&
      professionalPercentage <= heatmapRedisData?.data?.toValue4
    ) {
      peakFare = heatmapRedisData?.data?.peakFare4 || 0;
    }
    status = constantUtil.SUCCESS;
  } catch (e) {
    status = constantUtil.FAILED;
    peakFare = 0;
  }
  return {
    "status": status,
    "peakFare": peakFare,
  };
};
//-------------------------
module.exports = heatmapEvent;
