const { MoleculerError, MoleculerClientError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const googleApiUtil = require("../utils/googleApi.util");
const invoiceUtils = require("../utils/invoice.utils");
const storageUtil = require("../utils/storage.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const categoryEvent = {};

categoryEvent.updateServiceCategory = async function (context) {
  let errorCode,
    errorMessage = "",
    responseJson,
    checkConditionData = {};
  const {
    // ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS,
    // ALERT_CORPORATE_IS_ALREADY_EXISTS,
    // ALERT_SERVICE_AREA_IS_ALREADY_EXISTS,
    SOMETHING_WENT_WRONG,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    const categoryName = context.params.data.categoryName;
    const clientId = context.params.clientId || context.params.data.clientId;
    if (categoryName === "ride") {
      checkConditionData = {
        "clientId": mongoose.Types.ObjectId(clientId),
        "locationName": context.params.data.locationName,
        "categoryName": categoryName,
        "recordType": "parent",
      };
    } else if (categoryName === "corporateride") {
      checkConditionData = {
        "clientId": mongoose.Types.ObjectId(clientId),
        // "locationName": context.params.data.locationName,
        "categoryName": categoryName,
        "corporateId": mongoose.Types.ObjectId(
          context.params.data.corporateId.toString()
        ),
        "recordType": "child",
      };
      // } else if (categoryName === "intercityride") {
    } else if (categoryName !== "ride" || categoryName !== "corporateride") {
      checkConditionData = {
        "clientId": mongoose.Types.ObjectId(clientId),
        // "locationName": context.params.data.locationName,
        "categoryName": categoryName,
        "corporateId": null,
        "serviceAreaId": mongoose.Types.ObjectId(
          context.params.data.serviceAreaId
        ),
        "recordType": "child",
      };
    }
    const cityJson = await this.adapter.model
      .findOne({
        ...checkConditionData,
      })
      .lean();
    if (context.params.id) {
      if (
        cityJson !== null &&
        cityJson?._id?.toString() !== context.params.id.toString()
      ) {
        return {
          "code": 0,
          "data": {},
          "message":
            categoryName === "ride"
              ? "SERVICE LOCATION IS ALREADY EXIST" // ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS
              : categoryName === "corporateride"
              ? "CORPORATE IS ALREADY EXIST" // ALERT_CORPORATE_IS_ALREADY_EXISTS
              : "SERVICE AREA IS ALREADY EXIST", // ALERT_SERVICE_AREA_IS_ALREADY_EXISTS
        };
      } else {
        responseJson = await this.adapter.updateById(
          mongoose.Types.ObjectId(context.params.id.toString()),
          context.params.data
        );
      }
      //----- Update location Start ----------
      if (categoryName === "ride") {
        await this.adapter.updateMany(
          {
            "clientId": mongoose.Types.ObjectId(clientId),
            "categoryName": { "$nin": "ride" },
            "serviceAreaId": mongoose.Types.ObjectId(
              context.params.id.toString()
            ),
          },
          { "$set": { "location": context.params.data.location } }
        );
      }

      //----- Update location End ----------
    } else {
      if (cityJson) {
        return {
          "code": 0,
          "data": {},
          "message":
            categoryName === "ride"
              ? "SERVICE LOCATION IS ALREADY EXIST" // ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS
              : categoryName === "corporateride"
              ? "CORPORATE IS ALREADY EXIST" // ALERT_CORPORATE_IS_ALREADY_EXISTS
              : "SERVICE AREA IS ALREADY EXIST", // ALERT_SERVICE_AREA_IS_ALREADY_EXISTS
        };
      } else {
        responseJson = await this.adapter.insert(context.params.data);
      }
    }
    if (!responseJson) {
      return {
        "code": 0,
        "data": {},
        "message": SOMETHING_WENT_WRONG, // "ERROR IN SERVICE LOCATION",
      };
    }
    if (categoryName === "ride") {
      await this.broker.emit("professional.categoryChanges", responseJson);
    }
    errorCode = 1;
  } catch (e) {
    responseJson = {};
    errorCode = 0;
    errorMessage = "SERVICE AREA OR CITY ALREADY EXISTS"; //ALERT_SERVICE_AREA_IS_ALREADY_EXISTS
  }
  return { "code": errorCode, "data": responseJson, "message": errorMessage };
};

categoryEvent.updateServiceArea = async function (context) {
  const clientId = context.params.clientId;
  if (context.params.categoryName === "corporateride") {
    // "categoryName": categoryName,
    const locationData = await this.adapter.model
      .findOne(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.serviceAreaId.toString()
          ),
          //"clientId": mongoose.Types.ObjectId(clientId),
        },
        { "location": 1 }
      )
      .lean();

    const respoData = await this.adapter.model.update(
      {
        //"clientId": mongoose.Types.ObjectId(clientId),
        "categoryName": "corporateride",
        "corporateId": mongoose.Types.ObjectId(
          context.params.corporateId.toString()
        ),
      },
      {
        "$set": {
          "serviceAreaId": context.params.serviceAreaId.toString(),
          "location": locationData.location,
        },
      }
    );
    return respoData;
  }
};

categoryEvent.getServiceCategoryList = async function (context) {
  // const skip = parseInt(context.params.skip ?? 0);
  // const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  const clientId = context.params.clientId;
  query.push(
    (context?.params?.filter || "") !== ""
      ? {
          "$match": {
            "clientId": mongoose.Types.ObjectId(clientId),
            "status": { "$eq": context.params.filter },
            "categoryName": {
              "categoryName": { "$eq": context.params.requestFrom },
              // "$in": ["ride", "intercityride"],
            },
          },
        }
      : {
          "$match": {
            "clientId": mongoose.Types.ObjectId(clientId),
            "status": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
            "categoryName": { "$eq": context.params.requestFrom },
            // "categoryName": {
            //   "$in": ["ride", "intercityride"],
            // },
          },
        }
  );
  if ((context?.params?.search || "") !== "")
    query.push({
      "$match": {
        "$or": [
          {
            "categoryName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "locationName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "updatedAt": -1 } },
        { "$skip": parseInt(context.params.skip ?? 0) },
        { "$limit": parseInt(context.params.limit ?? 20) },
        // -----------Corporate Id----------
        {
          "$lookup": {
            "from": "admins",
            "localField": "corporateId",
            "foreignField": "_id",
            "as": "corporateList",
          },
        },
        {
          "$unwind": {
            "path": "$corporateList",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // --------------------
        //------------Corporate serviceArea----------
        {
          "$lookup": {
            "from": "categories",
            "localField": "serviceAreaId",
            "foreignField": "_id",
            "as": "serviceAreaList",
          },
        },
        {
          "$unwind": {
            "path": "$serviceAreaList",
            "preserveNullAndEmptyArrays": true,
          },
        },
        //----------------
        {
          "$project": {
            "categoryName": 1,
            "locationName": 1,
            "status": 1,
            "serviceAreaId": "$serviceAreaList.locationName",
            "corporateOfficeName": "$corporateList.data.officeName",
            "corporateServiceArea": "$serviceAreaList.locationName",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const serviceCategory = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);
  if (!serviceCategory)
    return {
      "code": 0,
      "data": {},
      "message": "NO SERVICE LOCATION AVAILABLE", //INFO_NO_SERVICE_LOCATION_AVAILABLE
    };
  responseJson["count"] = serviceCategory[0]?.all[0]?.all || 0;
  responseJson["response"] = serviceCategory[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};

categoryEvent.getAllServiceCategory = async function (context) {
  const categoryName = (context.params?.categoryName || "ride").toLowerCase();
  const responseJson = {};
  const serviceCategory = await this.adapter.model
    .find(
      {
        "clientId": mongoose.Types.ObjectId(context.params.clientId),
        "categoryName": categoryName,
        "status": constantUtil.ACTIVE,
      },
      {
        "_id": 1,
        "categoryName": 1,
        "locationName": 1,
        "location": 1,
        "address": 1,
      }
    )
    .sort({ "locationName": 1, "categoryName": 1 })
    .collation({ "locale": "en", "caseLevel": true })
    .lean();
  if (!serviceCategory)
    return {
      "code": 0,
      "data": {},
      "message": "NO SERVICE LOCATION AVAILABLE", //INFO_NO_SERVICE_LOCATION_AVAILABLE
    };
  responseJson["count"] = parseInt(serviceCategory.length);
  responseJson["response"] = serviceCategory.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
    "location": serviceLocation.location,
    "address": serviceLocation.address,
  }));

  return { "code": 1, "data": responseJson, "message": "" };
};
categoryEvent.getAllServiceActiveInactiveCategory = async function (context) {
  const categoryName = (context.params?.categoryName || "ride").toLowerCase();
  const responseJson = {};
  const serviceCategory = await this.adapter.model
    .find(
      {
        "categoryName": categoryName,
        "clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
      {
        "_id": 1,
        "categoryName": 1,
        "locationName": 1,
        "location": 1,
        "address": 1,
      }
    )
    .sort({ "locationName": 1, "categoryName": 1 })
    .collation({ "locale": "en", "caseLevel": true })
    .lean();
  if (!serviceCategory)
    return {
      "code": 0,
      "data": {},
      "message": "NO SERVICE LOCATION AVAILABLE", //INFO_NO_SERVICE_LOCATION_AVAILABLE
    };
  responseJson["count"] = parseInt(serviceCategory.length);
  responseJson["response"] = serviceCategory.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
    "location": serviceLocation.location,
    "address": serviceLocation.address,
  }));
  return { "code": 1, "data": responseJson, "message": "" };
};

categoryEvent.getServiceCategoryById = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "_id": context.params.id,
      "clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  if (!responseJson) {
    return {
      "code": 0,
      "data": {},
      "message": "NO SERVICE LOCATION AVAILABLE", //INFO_NO_SERVICE_LOCATION_AVAILABLE
    };
  } else {
    return { "code": 1, "data": responseJson, "message": "" };
  }
};

categoryEvent.getAllServiceMultiPoint = async function () {
  const serviceLocationList = await this.adapter.model
    .find({
      "status": constantUtil.ACTIVE,
      "clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  return serviceLocationList.map((serviceLocation) => serviceLocation.location);
};

categoryEvent.getServiceLocationList = async function (context) {
  let serviceLocationList = [];
  const clientId = context.params.clientId;
  if (context.params.requestFrom === constantUtil.COUPON) {
    serviceLocationList = await this.adapter.model
      .find({
        "status": constantUtil.ACTIVE,
        "clientId": mongoose.Types.ObjectId(clientId),
        "categoryName": {
          "$nin": constantUtil.CONST_CORPORATERIDE.toLocaleLowerCase(),
        },
      })
      .sort({ "locationName": 1 })
      .collation({ "locale": "en", "caseLevel": true })
      .lean();
  } else {
    serviceLocationList = await this.adapter.model
      .find({
        "clientId": mongoose.Types.ObjectId(clientId),
        "status": constantUtil.ACTIVE,
        "categoryName": constantUtil.CONST_RIDE.toLocaleLowerCase(),
      })
      .sort({ "locationName": 1 })
      .collation({ "locale": "en", "caseLevel": true })
      .lean();
  }
  return serviceLocationList.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
    "currencySymbol": serviceLocation.currencySymbol,
  }));
};

categoryEvent.changeServiceCategoryStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId;
  if (ids.length === 1) {
    const serviceAreaId = ids[0];
    let checkCount = await this.broker.emit(
      "professional.checkServiceAreaUsingId",
      {
        "clientId": clientId,
        "serviceAreaId": serviceAreaId.toString(),
      }
    );
    checkCount =
      context.params.loginUserType === constantUtil.DEVELOPER //DEVELOPER have Inactive Permission
        ? 0
        : (checkCount && checkCount[0]) || 0;
    if (
      checkCount === 0 ||
      context.params.status === constantUtil.ACTIVE ||
      (checkCount === 0 && context.params.status === constantUtil.INACTIVE)
    ) {
      const responseJson = await this.adapter.model.updateOne(
        {
          "_id": serviceAreaId,
          "clientId": mongoose.Types.ObjectId(clientId),
        },
        { "status": context.params.status }
      );
      if (!responseJson.nModified) {
        return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
      } else {
        return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
      }
    } else {
      return {
        "code": 0,
        "data": {},
        "message": "Service Area Already Used",
      };
    }
  } else {
    const responseJson = await this.adapter.model.updateMany(
      {
        "_id": { "$in": ids },
        "clientId": mongoose.Types.ObjectId(clientId),
      },
      { "status": context.params.status }
    );
    if (!responseJson.nModified) {
      return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
    } else {
      return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
    }
  }
};

categoryEvent.getCategoryById = async function (context) {
  const categoryData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .lean();
  return categoryData;
};

categoryEvent.getServiceBasedLocation = async function (context) {
  const jsonData = await this.adapter.model
    .find({
      "clientId": mongoose.Types.ObjectId(context.params.clientId),
      "categoryName": context.params.categoryName,
      "status": constantUtil.ACTIVE,
    })
    .sort({ "locationName": 1, "categoryName": 1 })
    .collation({ "locale": "en", "caseLevel": true })
    .lean();
  return jsonData.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
    "locationCoordinates": serviceLocation.location.coordinates,
    "vehicleCategories": serviceLocation.categoryData.vehicles.map((e) => {
      return e.categoryId;
    }),
    "distanceType": serviceLocation.distanceType,
  }));
};

categoryEvent.getServiceBasedCategoryData = async function (context) {
  const jsonData = await this.adapter.model
    .find({
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "categoryName": context.params.categoryName,
      "status": constantUtil.ACTIVE,
    })
    .sort({ "locationName": 1, "categoryName": 1 })
    .collation({ "locale": "en", "caseLevel": true })
    .lean();
  if (!jsonData && jsonData.length === 0) return;
  return jsonData.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
    "locationCoordinates": serviceLocation.location.coordinates,
    "vehicleCategoryData": serviceLocation.categoryData.vehicles,
  }));
};

categoryEvent.getEagleServiceCategory = async function (context) {
  const jsonData = await this.adapter.model
    .find({
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "categoryName": context.params.categoryName,
    })
    .sort({ "locationName": 1, "categoryName": 1 })
    .collation({ "locale": "en", "caseLevel": true })
    .lean();
  return jsonData.map((serviceLocation) => ({
    "_id": serviceLocation._id,
    "categoryName": serviceLocation.categoryName,
    "locationName": serviceLocation.locationName,
  }));
};

categoryEvent.getPopularServiceCategory = async function (context) {
  let serviceCategoryData = null;
  if (context.params.serviceAreaId) {
    serviceCategoryData = await this.adapter.model
      .findOne({
        "_id": mongoose.Types.ObjectId(
          context.params?.serviceAreaId?.toString()
        ),
        //"clientId": mongoose.Types.ObjectId(context.params?.clientId),
        "status": constantUtil.ACTIVE,
        // "categoryName": constantUtil.CONST_RIDE.toLowerCase(),
        // "location": {
        //   "$geoIntersects": {
        //     "$geometry": {
        //       "type": "Point",
        //       "coordinates": [context.params.lng, context.params.lat],
        //     },
        //   },
        // },
      })
      .lean();
  } else {
    serviceCategoryData = await this.adapter.model
      .findOne({
        // "categoryName": "ride",
        //"clientId": mongoose.Types.ObjectId(context.params?.clientId),
        "status": constantUtil.ACTIVE,
        "categoryName": constantUtil.CONST_RIDE.toLowerCase(),
        "location": {
          "$geoIntersects": {
            "$geometry": {
              "type": "Point",
              "coordinates": [context.params.lng, context.params.lat],
            },
          },
        },
      })
      .lean();
  }
  if (!serviceCategoryData) return null;
  return serviceCategoryData;
};

categoryEvent.updateCategoryFareBreakup = async function (context) {
  const updateData = {
    "title": context.params.title,
    "description": context.params.description,
    "amenities": context.params.amenities,
    "fleets": context.params.fleets,
    "guidelines": context.params.guidelines,
    "fareMessage": context.params.fareMessage,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.cityId.toString()),
      "categoryData.vehicles": {
        "$elemMatch": { "categoryId": context.params.id.toString() },
      },
    },
    {
      "$set": { "categoryData.vehicles.$[list].fareBreakUpDetail": updateData },
    },
    { "arrayFilters": [{ "list.categoryId": context.params.id.toString() }] }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

categoryEvent.getCategoryFareBreakup = async function (context) {
  let responseJson = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.cityId.toString()),
        "clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
      { "categoryData.vehicles": 1 }
    )
    .lean();
  if (!responseJson) {
    return {
      "code": 0,
      "data": {},
      "message": "ERROR IN FARE DETAILS",
    };
  } else {
    responseJson = responseJson.categoryData.vehicles.filter((data) => {
      return data.categoryId.toString() === context.params.id.toString();
    });
    return {
      "code": 1,
      "data": responseJson[0].fareBreakUpDetail,
      "message": "",
    };
  }
};
categoryEvent.getServieCategory = async function (context) {
  const clientId = context.params.clientId;
  if (context.params.categoryName) {
    return await this.adapter.model
      .findOne(
        {
          "clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": (
            context.params.categoryName || constantUtil.CONST_RIDE.toLowerCase()
          ).toLowerCase(),
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.lng, context.params.lat],
              },
            },
          },
        },
        { "locationName": 1, "_id": 1 }
      )
      .lean();
  } else {
    return await this.adapter.model
      .findOne(
        {
          "clientId": mongoose.Types.ObjectId(clientId),
          "status": constantUtil.ACTIVE,
          "location": {
            "$near": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.lng, context.params.lat],
              },
            },
          },
        },
        { "locationName": 1, "_id": 1 }
      )
      .lean();
  }
};

// third party booking service
categoryEvent.getCategoriesForThirdPartyBookingService = async function (
  context
) {
  const clientId = context.params.clientId;
  const serviceCategoryData = await this.adapter.model
    .findOne({
      "status": constantUtil.ACTIVE,
      "clientId": clientId,
      "location": {
        "$geoIntersects": {
          "$geometry": {
            "type": "Point",
            "coordinates": [
              context.params.origin.lng,
              context.params.origin.lat,
            ],
          },
        },
      },
    })
    .lean();
  if (!serviceCategoryData)
    return {
      "code": 400,
      "message": "",
      "data": [
        constantUtil.PICKUPLOCATIONOUT,
        400,
        "NOT FOUND",
        {
          "origin": context.params.origin,
        },
      ],
    };

  const dropExist = await this.adapter.model.count({
    "status": constantUtil.ACTIVE,
    "clientId": clientId,
    "location": {
      "$geoIntersects": {
        "$geometry": {
          "type": "Point",
          "coordinates": [
            context.params.destination.lng,
            context.params.destination.lat,
          ],
        },
      },
    },
  });
  if (dropExist <= 0) {
    return {
      "code": 400,
      "message": "",
      "data": [
        constantUtil.DROPLOCATIONOUT,
        400,
        "NOT FOUND",
        {
          "destination": context.params.destination,
        },
      ],
    };
  }
  const mapData = await googleApiUtil.directions({
    "clientId": clientId,
    "from": `${context.params.origin.lat},${context.params.origin.lng}`,
    "to": `${context.params.destination.lat},${context.params.destination.lng}`,
  });
  if (mapData.status > 200) {
    return {
      "code": 400,
      "message": "",
      "data": [mapData.status, 400, "NOT FOUND", {}],
    };
  }
  // from redis
  const vehicleCategoriesData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  // -------- Assign Distance Value Start---------------
  const distanceTypeUnitValue =
    (serviceCategoryData?.distanceType ?? constantUtil.KM).toUpperCase() ===
    constantUtil.KM
      ? constantUtil.KM_VALUE
      : constantUtil.MILES_VALUE;
  // -------- Assign Distance Value End---------------
  // timeing
  const peakTiming = invoiceUtils.checkPeakTiming({
    ...serviceCategoryData.isPeakFareAvailable,
    "bookingDate": new Date(),
  });
  const nightTiming = invoiceUtils.checkNightTiming({
    ...serviceCategoryData.isNightFareAvailable,
    "bookingDate": new Date(),
  });
  // getting avilable vehicles detailes from serviceCategory
  const vehicles = [];
  serviceCategoryData.categoryData.vehicles.forEach((vehicle) => {
    if (!vehicleCategoriesData[vehicle.categoryId]) {
      return null;
    }
    const vehicleCategoryData = {
      ...vehicle,
      ...vehicleCategoriesData[vehicle.categoryId],
    };
    const fareDetails = {
      "baseFare": vehicleCategoryData.baseFare,
      "farePerDistance": vehicleCategoryData.farePerDistance,
      "farePerMinute": vehicleCategoryData.farePerMinute,
      "minimumCharge": vehicleCategoryData.minimumCharge,
      "nightFare": {
        "status": nightTiming,
        "calcType": constantUtil.PERCENTAGE,
        "value": nightTiming ? vehicleCategoryData.nightFare : 0,
      },
      "peakFare": {
        "status": peakTiming,
        "calcType": constantUtil.PERCENTAGE,
        "value": peakTiming ? vehicle.peakFare : 0,
      },
      "serviceTaxPercentage": vehicleCategoryData.serviceTaxPercentage,
      "siteCommissionPercentage": vehicleCategoryData.siteCommission,
      // Surcharge
      "surchargeFee": vehicleCategoryData.surchargeFee || 0,
      "isSurchargeEnable": vehicleCategoryData?.isSurchargeEnable ?? true,
      "surchargeTitle": vehicleCategoryData?.surchargeTitle ?? "Surcharge",
      //
    };
    // invoice
    const {
      estimationAmount,
      travelCharge,
      distanceFare,
      timeFare,
      serviceTax,
      professionalCommision,
    } = invoiceUtils.calculateEstimationAmount({
      "vehicleCategoryData": vehicleCategoryData,
      "distanceTypeUnitValue": distanceTypeUnitValue,
      "peakTiming": peakTiming,
      "nightTiming": nightTiming,
      "estimationDistance": mapData.data.routes[0].legs[0].distance.value,
      "estimationTime": mapData.data.routes[0].legs[0].duration.value,
    });
    const invoice = {
      "estimationAmount": estimationAmount,
      "travelCharge": travelCharge,
      "distanceFare": distanceFare,
      "timeFare": timeFare,
      "serviceTax": serviceTax,
      "professionalCommision": professionalCommision,
    };
    // getvehicles return data
    vehicles.push({
      "vehicleCategoryId": vehicle.categoryId,
      "vehicleCategoryName": vehicleCategoryData.vehicleCategory,
      "vehicleCategoryImage": vehicleCategoryData.categoryImage,
      "vehicleCategoryMapImage": vehicleCategoryData.categoryMapImage,
      "seatCount": vehicleCategoryData.seatCount,
      "fareDetails": fareDetails,
      "invoice": invoice,
    });
  });
  return {
    "code": 200,
    "status": constantUtil.CATEGORYAVAILABLE,
    "data": {
      "serviceCategoryId": serviceCategoryData._id,
      "serviceCategoryName": serviceCategoryData.locationName,
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceType": serviceCategoryData.distanceType,
      "vehicles": vehicles,
      "originAddress": mapData.data.routes[0].legs[0].start_address,
      "destinationAddress": mapData.data.routes[0].legs[0].end_address,
    },
  };
};

// RIDE RENTAL LOCATION CATEGORIES
categoryEvent.getBookingLocationCategories = async function (context) {
  if (context.params.rideType === constantUtil.RENTAL) {
    const response = await this.getRentalLocationCategories(context.params);

    return response;
  }
};

// Update Vehicle Share Ride Details
categoryEvent.updateVehicleShareRideConfigDetails = async function (context) {
  try {
    const isSupportShareRide = context.params.isSupportShareRide;
    await this.adapter.updateMany(
      {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "categoryName": "ride",
        "categoryData.vehicles.categoryId":
          context.params?.vehicleCategoryId?.toString(),
      },
      {
        "$set": {
          "categoryData.vehicles.$.isSupportShareRide": isSupportShareRide,
        },
      }
    );
  } catch (e) {
    console.log(e.message);
  }
};
//---------------------
module.exports = categoryEvent;
