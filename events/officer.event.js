const moment = require("moment");
const mongoose = require("mongoose");
const turf = require("@turf/turf");

const constantUtil = require("../utils/constant.util");
const helperUtil = require("../utils/helper.util");
const storageUtil = require("../utils/storage.util");
const { setDayStartHours, setDayEndHours } = require("../utils/common.util");

const officerEvent = {};

//#region Response Officer

officerEvent.verifyToken = async function (context) {
  const officerData = await this.adapter.model
    .findOne({
      "_id": context.params.officerId,
      "deviceInfo.accessToken": context.params.accessToken,
    })
    .lean();

  if (context.params.deviceId && context.params.socketId !== "") {
    await this.adapter.model.updateOne(
      { "_id": context.params.officerId },
      {
        "deviceInfo.0.deviceId": context.params.deviceId,
        "deviceInfo.0.socketId": context.params.socketId,
      }
    );
  }

  return officerData;
};

// ADMIN MODULES APIS
officerEvent.getOfficerList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const search =
    context.params.search && context.params.search != ""
      ? context.params.search
      : "";

  const query = {};
  query["status"] =
    context.params.filter && context.params.filter != ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  const officerData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-_id",
    "search": search,
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = officerData.map((officer) => {
    return {
      "_id": officer._id,
      "data": {
        "firstName": officer.firstName,
        "lastName": officer.lastName,
        "email": officer.email,
        "phone": officer.phone,
        "gender": officer.gender,
        "officeId": officer.officeId,
        "status": officer.status,
      },
    };
  });
  return { "code": 1, "data": responseJson, "message": "" };
};

officerEvent.getEditOfficer = async function (context) {
  const jsonData = await this.adapter.model.findOne({
    "_id": context.params.id,
  });

  if (!jsonData) return { "code": 0, "data": {}, "message": "NO USER FOUND" };
  return {
    "code": 1,
    "data": {
      "_id": jsonData._id,
      "data": {
        "firstName": jsonData.firstName,
        "lastName": jsonData.lastName,
        "email": jsonData.email,
        "dob": jsonData.dob,
        "phone": jsonData.phone,
        "avatar": jsonData.avatar,
        "gender": jsonData.gender,
        "officeId": jsonData.officeId,
        "status": jsonData.status,
      },
    },
  };
};

officerEvent.changeOfficerStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
    },
    {
      "status": context.params.status,
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

officerEvent.updateOfficer = async function (context) {
  const clientId = context.params.clientId;
  const { INFO_INVALID_DATE_FORMAT, ALERT_DUPLICATE_EMAIL } =
    notifyMessage.setNotifyLanguage(context.params.langCode);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const checkEmailCount = await this.adapter.model.countDocuments({
    "_id": {
      "$ne": mongoose.Types.ObjectId(context.params.id.toString()),
    },
    "email": context.params.email,
  });
  if (checkEmailCount > 0) {
    throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
  }
  if (!moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"], true).isValid())
    return {
      "code": 0,
      "data": {},
      "message": INFO_INVALID_DATE_FORMAT,
    };

  const age =
    moment().year() -
    moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"]).year();
  if (age < parseInt(generalSettings.data.officerMinAge))
    return {
      "code": 0,
      "data": {},
      "message":
        "AGE MUST BE" +
        parseInt(generalSettings.data.officerMinAge) +
        " AND ABOVE TO PROCEED",
    };
  const checkJson = await this.adapter.model
    .findOne({
      "email": context.params.email,
      "_id": { "$ne": context.params.id },
    })
    .lean();
  if (checkJson)
    return {
      "code": 0,
      "data": {},
      "message": "EMAIL ALREADY EXIST",
    };

  const jsonData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
    })
    .lean();

  if (context.params.id) {
    const officerData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.id.toString()))
      .lean();
    if (!officerData)
      return {
        "code": 0,
        "data": {},
        "message": "ERROR IN PROFESSIONAL DETAILS",
      };

    context.params.uniqueCode = officerData.uniqueCode;
    if (context.params.files.length && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `avatar${context.params.uniqueCode}${constantUtil.OFFICER}`,
        }
      );
      context.params.avatar = s3Image[0];
    }
  } else {
    context.params.uniqueCode = `${context.params.firstName.slice(
      0,
      4
    )}${helperUtil.randomString(6, "a")}`;
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `avatar${context.params.uniqueCode}${constantUtil.OFFICER}`,
        }
      );
      context.params.avatar = s3Image[0];
    }
  }
  const updateData = {
    "uniqueCode": context.params.uniqueCode,
    "firstName": context.params.firstName,
    "lastName": context.params.lastName,
    "email": context.params.email,
    "isEmailVerified": context.params.isEmailVerified || false,
    "gender": context.params.gender,
    "avatar": context.params.avatar,
    "officeId": context.params.officeId,
    "dob": context.params.dob,
    "age": age,
    "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
    "phone": {
      "code": context.params.phoneCode,
      "number": context.params.phoneNumber,
    },
    "status": context.params.status,
  };
  if (context.params.id) {
    if (jsonData && jsonData._id.toString() !== context.params.id.toString()) {
      return { "code": 0, "data": {}, "message": "OFFICER ALREADY EXIST" };
    }
    const responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      updateData
    );
    return { "code": 1, "data": responseJson, "message": "" };
  } else {
    if (jsonData) {
      return { "code": 0, "data": {}, "message": "OFFICER ALREADY EXIST" };
    }
    const responseJson = await this.adapter.insert(updateData);
    return { "code": 1, "data": responseJson, "message": "" };
  }
};
officerEvent.updateLiveLocation = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.officerId.toString()),
    {
      "location.coordinates": [context.params.lng, context.params.lat],
      "currentBearing": context.params.currentBearing,
      "altitude": context.params.altitude,
      "horizontalAccuracy": context.params.horizontalAccuracy,
      "verticalAccuracy": context.params.verticalAccuracy,
      "speed": context.params.speed,
      "isLocationUpdated": true,
      "locationUpdatedTime": new Date(),
    }
  );
  if (!updateData)
    return { "code": 0, "data": {}, "message": "FAILED TO UPDATE LOCATION" };

  return {
    "code": 200,
    "data": {
      "onlineStatus": updateData.onlineStatus,
      "bookingInfo": updateData.bookingInfo,
    },
    "message": "LOCATION UPDATED SUCCESSFULLY",
  };
};

officerEvent.getOfficerByLocation = async function (context) {
  const responseData = await this.adapter.model.find({
    "status": constantUtil.ACTIVE,
    "onlineStatus": true,
    "location": {
      "$nearSphere": {
        "$geometry": {
          "type": "Point",
          "coordinates": [
            parseFloat(context.params.lng),
            parseFloat(context.params.lat),
          ],
        },
        "$minDistance": 0,
        "$maxDistance": context.params.radius,
      },
    },
  });
  const to = turf.point([
    parseFloat(context.params.lat),
    parseFloat(context.params.lng),
  ]);
  const professionalJSON = await responseData.map((professional) => {
    const from = turf.point([
      parseFloat(professional.location.coordinates[1]),
      parseFloat(professional.location.coordinates[0]),
    ]);
    return {
      "distance": turf.distance(from, to, { "units": "kilometers" }).toFixed(2),
      "_id": professional._id,
      "firstName": professional.firstName,
      "lastName": professional.lastName,
      "email": professional.email,
      "phone": professional.phone,
    };
  });
  return professionalJSON;
};

officerEvent.getOfficerByLocationRequest = async function (context) {
  const responseData = await this.adapter.model.find({
    "status": constantUtil.ACTIVE,
    "onlineStatus": true,
    "bookingInfo.ongoingBooking": null,
    "location": {
      "$nearSphere": {
        "$geometry": {
          "type": "Point",
          "coordinates": [
            parseFloat(context.params.lng),
            parseFloat(context.params.lat),
          ],
        },
        "$minDistance": 0,
        "$maxDistance": context.params.radius,
      },
    },
  });

  return responseData;
};

officerEvent.updateOnGoingBooking = async function (context) {
  const updateData =
    context.params.type === constantUtil.ACCEPTED
      ? {
          "bookingInfo.ongoingBooking": context.params.escortBookingId
            ? context.params.escortBookingId
            : null,
        }
      : context.params.type === constantUtil.ENDED
      ? {
          "bookingInfo.ongoingBooking": null,
          "bookingInfo.pendingReview": context.params.escortBookingId
            ? context.params.escortBookingId
            : null,
        }
      : {
          "bookingInfo.ongoingBooking": null,
          "bookingInfo.pendingReview": null,
        };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.officerId.toString()),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

officerEvent.updateEscortRating = async function (context) {
  const officerData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.officerId.toString()))
    .lean();
  const avgRating =
    (parseFloat(
      officerData.review.avgRating ? officerData.review.avgRating : 0
    ) *
      parseInt(
        officerData.review.totalReview ? officerData.review.totalReview : 0
      ) +
      parseFloat(context.params.rating)) /
    (parseFloat(
      officerData.review.totalReview ? officerData.review.totalReview : 0
    ) +
      1);

  const totalReview =
    parseInt(
      officerData.review.totalReview ? officerData.review.totalReview : 0
    ) + 1;
  const updateProfessionalData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.officerId),
      "status": constantUtil.ACTIVE,
    },
    {
      "bookingInfo.pendingReview": null,
      "review.avgRating": avgRating,
      "review.totalReview": totalReview,
    }
  );
  if (!updateProfessionalData.nModified) {
    return null;
  } else {
    return updateProfessionalData;
  }
};

officerEvent.updateAllOnlineOfficerStatus = async function () {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const nowDate =
    Date.now() - generalSettings.data.onlineStatusThreshold * 60 * 1000;
  const updatedProfessionalData = await this.adapter.updateMany(
    {
      "status": constantUtil.ACTIVE,
      "onlineStatus": true,
      "isLocationUpdated": true,
      "locationUpdatedTime": { "$lte": new Date(nowDate) },
      "bookingInfo.ongoingBooking": null,
    },
    {
      "onlineStatus": false,
    }
  );
  if (!updatedProfessionalData)
    return { "code": 0, "data": {}, "message": "STATUS UPDATE FAILD BY CRON" };

  return {
    "code": 1,
    "data": {},
    "message": "STATUS UPDATED SUCCESSFULLY BY CRON",
  };
};

//#endregion Response Officer

//#region Corporate Officer

officerEvent.getCorporateDetailsByUserId = async function (context) {
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        "userType": constantUtil.CORPORATEOFFICER,
        "user": mongoose.Types.ObjectId(context.params.userId.toString()),
        "status": constantUtil.ACTIVE,
        // "isActive": true,
        // "isDeleted": false,
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "corporate",
        "foreignField": "_id",
        "as": "corporateDetails",
      },
    },
    { "$unwind": "$corporateDetails" },
    {
      "$project": {
        "_id": "$corporate",
        "name": "$corporateDetails.data.officeName",
        "img": "$corporateDetails.data.image",
        "categoryIds": "$corporateDetails.data.categoryIds",
        "walletAmount": "$corporateDetails.data.amount",
      },
    },
  ]);

  // const responseJson = await this.adapter.model
  //   .find(
  //     {
  //       "userType": constantUtil.CORPORATEOFFICER,
  //       "user": mongoose.Types.ObjectId(context.params.userId.toString()),
  //       "isActive": true,
  //     },
  //     { "userType": 1, "isActive": 1 }
  //   )
  //   .populate("user", {
  //     "firstName": 1,
  //     "lastName": 1,
  //     "email": 1,
  //     "gender": 1,
  //     "avatar": 1,
  //     "languageCode": 1,
  //   })
  //   .populate("corporate", { "data.officeName": 1 });

  // if (!responseJson.nModified) return null;

  return responseJson;
};

officerEvent.getCorporateOfficerList = async function (context) {
  const search = context.params?.search || "";

  const query = {
    "clientId": mongoose.Types.ObjectId(context.params.clientId),
    "userType": constantUtil.CORPORATEOFFICER,
    //"isDeleted": false,
  };
  // query["userType"] = constantUtil.CORPORATEOFFICER;
  // query["isDeleted"] = false;
  if ((context.params?.corporateId || "0") !== "0") {
    query["corporate"] = mongoose.Types.ObjectId(
      context.params.corporateId.toString()
    );
  }
  // query["isActive"] =
  //   (context.params.filter || "") !== ""
  //     ? { "$eq": context.params.filter === constantUtil.ARCHIVE ? false : true }
  //     : { "$in": [true, false] };

  query["status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params.fromDate || "") !== "" &&
    (context.params.toDate || "") !== ""
  ) {
    const fromDate = setDayStartHours(context.params.fromDate);
    const toDate = setDayEndHours(context.params.toDate);
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.code": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  const officerData = await this.adapter.model
    .aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, { "$count": "all" }],
          "response": [
            {
              "$lookup": {
                "from": "admins",
                "localField": "corporate",
                "foreignField": "_id",
                "as": "corporateDetails",
              },
            },
            // { "$unwind": "$corporateDetails" },
            {
              "$unwind": {
                "path": "$corporateDetails",
                "preserveNullAndEmptyArrays": true,
              },
            },
            {
              "$lookup": {
                "from": "users",
                "localField": "user",
                "foreignField": "_id",
                "as": "userDetails",
              },
            },
            // { "$unwind": "$userDetails" },
            {
              "$unwind": {
                "path": "$userDetails",
                "preserveNullAndEmptyArrays": true,
              },
            },
            searchQuery,
            {
              "$project": {
                "_id": 1,
                //"user.fullName": "$userDetails.firstName",
                "user.fullName": {
                  "$concat": [
                    {
                      "$toUpper": {
                        "$substrCP": ["$firstName", 0, 1],
                      },
                    },
                    { "$substrCP": ["$firstName", 1, 20] },
                    " ",
                    {
                      "$toUpper": {
                        "$substrCP": ["$lastName", 0, 1],
                      },
                    },
                    { "$substrCP": ["$lastName", 1, 20] },
                  ],
                },
                "user.phone": {
                  "$concat": ["$phone.code", "  ", "$phone.number"],
                },

                // "user.gender": "$userDetails.gender",
                // "phoneCode": "$corporateDetails.phone.code",
                // "phoneNumber": "$corporateDetails.phone.number",
                // "corporate.firstName": "$corporateDetails.data.firstName",
                // "corporate.lastName": "$corporateDetails.data.lastName",
                // "corporate.fullName": {
                //   "$concat": [
                //     {
                //       "$toUpper": {
                //         "$substrCP": ["$corporateDetails.firstName", 0, 1],
                //       },
                //     },
                //     { "$substrCP": ["$corporateDetails.firstName", 1, 20] },
                //     " ",
                //     {
                //       "$toUpper": {
                //         "$substrCP": ["$corporateDetails.lastName", 0, 1],
                //       },
                //     },
                //     { "$substrCP": ["$corporateDetails.lastName", 1, 20] },
                //   ],
                // },
                "corporate.officeName": {
                  "$concat": [
                    {
                      "$toUpper": {
                        "$substrCP": [
                          "$corporateDetails.data.officeName",
                          0,
                          1,
                        ],
                      },
                    },
                    {
                      "$substrCP": ["$corporateDetails.data.officeName", 1, 20],
                    },
                  ],
                },
                "employeeId": 1,
                "gender": 1,
                "isActive": 1, // Need to Remove this Key
                "status": 1,
              },
            },
            { "$sort": { "updatedAt": -1 } },
            { "$skip": parseInt(context.params.skip ?? 0) },
            { "$limit": parseInt(context.params.limit ?? 10) },
          ],
        },
      },
    ])
    .allowDiskUse(true);
  // responseJson["count"] = count;
  // responseJson["response"] = officerData;
  responseJson["count"] = officerData[0]?.all[0]?.all || 0;
  responseJson["response"] = officerData[0]?.response || [];
  return responseJson;
};

officerEvent.getCorporateOfficerById = async function (context) {
  // try {
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        "userType": constantUtil.CORPORATEOFFICER,
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        // "isActive": true,
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "corporate",
        "foreignField": "_id",
        "as": "corporateDetails",
      },
    },
    // { "$unwind": "$corporateDetails" },
    {
      "$unwind": {
        "path": "$corporateDetails",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$lookup": {
        "from": "users",
        "localField": "user",
        "foreignField": "_id",
        "as": "userDetails",
      },
    },
    // { "$unwind": "$userDetails" },
    {
      "$unwind": {
        "path": "$userDetails",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$project":
        // {
        //   "_id": "$corporate",
        //   "name": "$corporateDetails.data.officeName",
        //   "img": "$corporateDetails.data.image",
        // },
        {
          "_id": 1,
          // "firstName": "$userDetails.firstName",
          // "lastName": "$userDetails.lastName",
          "phoneCode": "$phone.code",
          "phoneNumber": "$phone.number",
          // "email": "$userDetails.email",
          // "gender": "$userDetails.gender",
          "corporate": "$corporateDetails._id",
          "officeName": "$corporateDetails.data.officeName",
          // "corporate2": "$corporateDetails.corporate",

          // "corporate": 1,
          "firstName": 1,
          "lastName": 1,
          "email": 1,
          "isEmailVerified": 1,
          // "phone.code": 1,
          // "phone.number": 1,
          "employeeId": 1,
          "gender": 1,
          "isActive": 1, // Need to Remove this Key
          "status": 1,
        },
    },
  ]);

  return responseJson[0]; //return single Record
  // }
  // catch (e) {
  //   const x = e;
  // }
};
officerEvent.checkCorporateOfficerByPhoneNumber = async function (context) {
  // try {
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        "userType": constantUtil.CORPORATEOFFICER,
        "corporate": mongoose.Types.ObjectId(context.params.corporateId),
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        "status": constantUtil.ACTIVE,
        // "isActive": true,
        // "isDeleted": false,
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "corporate",
        "foreignField": "_id",
        "as": "corporateDetails",
      },
    },
    { "$unwind": "$corporateDetails" },
    {
      "$lookup": {
        "from": "users",
        "localField": "user",
        "foreignField": "_id",
        "as": "userDetails",
      },
    },
    // { "$unwind": "$userDetails" },
    {
      "$unwind": {
        "path": "$userDetails",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$project":
        // {
        //   "_id": "$corporate",
        //   "name": "$corporateDetails.data.officeName",
        //   "img": "$corporateDetails.data.image",
        // },
        {
          "_id": "$userDetails._id", //UserId
          "firstName": "$userDetails.firstName",
          "lastName": "$userDetails.lastName",
          "phone.code": "$userDetails.phone.code",
          "phone.number": "$userDetails.phone.number",
          "email": "$userDetails.email",
          "gender": "$userDetails.gender",
          "bookingInfo": "$userDetails.bookingInfo",
          "corporate": "$corporateDetails._id",
          // "corporate2": "$corporateDetails.corporate",
          "notes": "$userDetails.notes",

          // "corporate": 1,

          "employeeId": 1,
          // "isActive": 1,
        },
    },
  ]);

  return responseJson[0];
};

//#endregion Corporate Officer

module.exports = officerEvent;
