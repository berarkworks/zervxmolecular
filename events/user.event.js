const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const moment = require("moment");

const notifyMessage = require("../mixins/notifyMessage.mixin");
const { convertToFloat, convertToInt } = require("../utils/common.util");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const mappingUtil = require("../utils/mapping.util");
const helperUtil = require("../utils/helper.util");

const userEvent = {};

userEvent.verifyToken = async function (context) {
  if (context.params.deviceId && context.params.socketId !== "") {
    await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      {
        "deviceInfo.0.deviceId": context.params.deviceId,
        "deviceInfo.0.socketId": context.params.socketId,
      }
    );
  }

  if (!!context.params?.langCode === true) {
    await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      { "languageCode": context.params.langCode }
    );
  }
  const userData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.userId),
    "deviceInfo.accessToken": context.params.accessToken,
  });
  return userData;
};
// UNREGISTERED USER LIST
userEvent.getUnregisteredUserList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {};
  // query["clientId"] = mongoose.Types.ObjectId(context.params.clientId);
  query["status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.INCOMPLETE, constantUtil.ATTENDED] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["phone.code", "phone.number"],
  });
  const userData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-_id",
    "search": search,
    "searchFields": ["phone.code", "phone.number"],
  });
  responseJson["count"] = count;
  responseJson["response"] = userData.map((user) => {
    return {
      "_id": user._id,
      "data": {
        "phone": user.phone,
        "status": user.status,
        "notes": user.notes,
        "createdAt": user.createdAt,
        "updatedAt": user.updatedAt,
      },
    };
  });
  return { "code": 1, "data": responseJson, "message": "" };
};

userEvent.addUnregisteredUserNotes = async function (context) {
  if (context.params.id) {
    const responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "notes": context.params.notes }
    );
    if (!responseJson) {
      return { "code": 0, "data": {}, "message": "ERROR IN NOTES ADDING" };
    } else {
      return { "code": 1, "data": {}, "message": "NOTES ADDED SUCCESSFULLY" };
    }
  }
};
userEvent.changeUnregisteredUsersStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "status": constantUtil.INCOMPLETE,
    },
    { "status": context.params.status }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
userEvent.closeUnregisteredUser = async function (context) {
  let responseJson = null;
  if (
    context.params.action === constantUtil.ACTION_DELETE &&
    context.params.ids
  ) {
    const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
    responseJson = await this.adapter.model.deleteMany({
      "_id": { "$in": ids },
      "status": { "$in": [constantUtil.INCOMPLETE, constantUtil.ATTENDED] },
    });
  } else if (context.params.id) {
    responseJson = await this.adapter.model.remove({
      "_id": context.params.id,
      // "status": constantUtil.ATTENDED,
    });
  }
  if (!responseJson) {
    return { "code": 0, "data": {}, "message": "ERROR IN CLOSING" };
  } else {
    return { "code": 1, "data": {}, "message": "CLOSED SUCCESSFULLY" };
  }
};
userEvent.getUsersList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {};
  // query["clientId"] = mongoose.Types.ObjectId(context.params.clientId);
  query["status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  // const count = await this.adapter.count({
  //   "query": query,
  //   "search": search,
  //   "searchFields": [
  //     "firstName",
  //     "lastName",
  //     "email",
  //     "phone.code",
  //     "phone.number",
  //   ],
  // }); // Old
  // const userData = await this.adapter.find({
  //   "sort": "-_id",
  //   "query": query,
  //   "search": search,
  //   "limit": limit,
  //   "offset": skip,
  //   "searchFields": [
  //     "firstName",
  //     "lastName",
  //     "email",
  //     "phone.code",
  //     "phone.number",
  //   ],
  // }); //Old
  const searchFields = [];
  if (search) {
    searchFields.push({
      "$match": {
        "$or": [
          {
            "firstName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "lastName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "email": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.number": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });
  }
  const queryData = [];
  queryData.push(
    {
      "$match": {
        ...query,
      },
    },
    ...searchFields, //For Search
    {
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          { "$sort": { "createdAt": -1 } },
          { "$skip": parseInt(skip) },
          { "$limit": parseInt(limit) },
          {
            "$project": {
              "clientId": 1,
              "firstName": 1,
              "lastName": 1,
              "email": 1,
              "phone": 1,
              "review": 1,
              "wallet": 1,
              "gender": 1,
              "status": 1,
              "createdAt": 1,
            },
          },
        ],
      },
    }
  );

  const userData = await this.adapter.model
    .aggregate(queryData)
    .allowDiskUse(true);
  // responseJson["count"] = count;
  responseJson["count"] = userData[0]?.all[0]?.all || 0;
  responseJson["response"] = userData[0]?.response.map((user) => {
    return {
      "_id": user._id,
      "data": {
        "clientId": user.clientId,
        "firstName": user.firstName,
        "lastName": user.lastName,
        "email": user.email,
        "phone": user.phone,
        "status": user.status,
        "gender": user.gender,
        "avgRating": user?.review?.avgRating || 0,
        "wallet": user.wallet,
        "createdAt": user.createdAt,
      },
    };
  });
  return { "code": 1, "data": responseJson, "message": "" };
};
userEvent.getEditUser = async function (context) {
  const jsonData = await this.adapter.model
    .findOne({
      "_id": context.params.id,
    })
    .lean();

  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "NO USER FOUND" };
  } else {
    return {
      "code": 1,
      "data": {
        "_id": jsonData._id,
        "data": {
          "clientId": jsonData.clientId,
          "firstName": jsonData.firstName,
          "lastName": jsonData.lastName,
          "email": jsonData.email,
          "isEmailVerified": jsonData.isEmailVerified,
          "uniqueCode": jsonData.uniqueCode,
          "nationalIdNo": jsonData.nationalIdNo,
          "phone": jsonData.phone,
          "avatar": jsonData.avatar,
          "gender": jsonData.gender,
          "dob": jsonData.dob,
          "status": jsonData.status,
          "notes": jsonData.notes,
        },
      },
    };
  }
};

userEvent.updateUser = async function (context) {
  let errorCode = 1, // Success Code is 1
    message = "",
    errorMessage = "",
    responseJson,
    generalSettings = null,
    userCheckData,
    referredByData = {};
  const clientId = context.params.clientId;
  const {
    INFO_INVALID_DATE_FORMAT,
    ALERT_DUPLICATE_EMAIL,
    ALERT_CPF_NO_ALREADY_EXISTS,
    INFO_INVALID_REFERRAL_CODE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
    if (
      !moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"], true).isValid()
    ) {
      return {
        "code": 0,
        "data": {},
        "message": INFO_INVALID_DATE_FORMAT,
      };
    }
    const referredBy = context.params.referredBy || "";
    if (referredBy) {
      userCheckData = await this.adapter.model
        .findOne(
          {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "uniqueCode": referredBy.toLowerCase(),
          },
          { "_id": 1, "uniqueCode": 1 }
        )
        .collation({ "locale": "en", "caseLevel": true });
      if (!userCheckData) {
        throw new MoleculerError(INFO_INVALID_REFERRAL_CODE);
      }
      referredByData = {
        "referralDetails": {
          "rideCount": inviteSettings.data.forUser.rideCount,
          "inviteAmount": inviteSettings.data.forUser.amountToInviter,
          "joinerAmount": inviteSettings.data.forUser.amountToJoiner,
        },
        "referredBy": referredBy.toLowerCase(),
      };
    }
    const checkEmailCondition =
      context.params.id === "newrecord"
        ? { "email": context.params.email }
        : {
            "_id": {
              "$ne": mongoose.Types.ObjectId(context.params.id.toString()),
            },
            "email": context.params.email,
          };
    const checkEmailCount = await this.adapter.model.countDocuments(
      checkEmailCondition
    );
    if (checkEmailCount > 0) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    let jsonData = null;
    if (
      context.params.nationalIdNo ||
      context.params.phoneCode === constantUtil.COUNTRYCODE_BRAZIL
    ) {
      // jsonData = await this.adapter.model
      //   .findOne({
      //     "$or": [
      //       {
      //         "nationalIdNo": context.params.nationalIdNo,
      //       },
      //       {
      //         "phone.code": context.params.phoneCode,
      //         "phone.number": context.params.phoneNumber,
      //       },
      //     ],
      //   })
      //   .lean();
      const checkDuplicatenationalIdQuery =
        context.params.id === "newrecord"
          ? { "nationalIdNo": context.params.nationalIdNo }
          : {
              "_id": {
                "$ne": mongoose.Types.ObjectId(context.params.id.toString()),
              },
              "nationalIdNo": context.params.nationalIdNo,
            };
      jsonData = await this.adapter.model
        .find({
          // "nationalIdNo": context.params.nationalIdNo,
          ...checkDuplicatenationalIdQuery,
        })
        .lean();
      // if (
      //   jsonData.length > 1 ||
      //   (jsonData.length === 0 && context.params.id !== "newrecord") ||
      //   (jsonData.length === 1 &&
      //     jsonData?.[0]?._id?.toString() !== context.params.id?.toString())
      // )
      if (jsonData.length > 0) {
        return {
          "code": 0,
          "data": {},
          "message": ALERT_CPF_NO_ALREADY_EXISTS,
        };
      }
      // //
      // jsonData = await this.adapter.model
      //   .findOne({
      //     "phone.code": context.params.phoneCode,
      //     "phone.number": context.params.phoneNumber,
      //   })
      //   .lean();
    }
    jsonData = await this.adapter.model
      .findOne({
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        //"clientId": mongoose.Types.ObjectId(clientId),
      })
      .lean();

    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${context.params.phoneNumber}${constantUtil.USER}`,
          "oldFileName": `${jsonData?.avatar || ""}`,
        }
      );
      context.params.avatar = s3Image && s3Image[0];
    }
    if (context.params.id === "newrecord") {
      // const dynamicUniqueCode = `${context.params.firstName.slice(
      //   0,
      //   4
      // )}${helperUtil.randomString(6, "a")}`;
      const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
        context.params.firstName,
        clientId
      );
      context.params.uniqueCode = dynamicUniqueCode.toLowerCase();
    }
    const updateData = {
      "clientId": clientId,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "email": context.params.email,
      "isEmailVerified": context.params.isEmailVerified,
      "uniqueCode": context.params.uniqueCode,
      "phone": {
        "code": context.params.phoneCode,
        "number": context.params.phoneNumber,
      },
      "nationalIdNo": context.params.nationalIdNo,
      "emailOTP": null,
      "dob": context.params?.dob?.replace(/-/g, "/"),
      "gender": context.params.gender,
      "avatar": context.params.avatar,
      "status": constantUtil.ACTIVE,
      "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
      "notes": context.params.notes,
      ...referredByData,
    };
    if (context.params.id !== "newrecord") {
      if (
        jsonData &&
        jsonData?._id?.toString() !== context.params.id.toString()
      ) {
        // return { "code": 0, "data": {}, "message": "USER ALREADY EXIST" };
        throw new MoleculerError("USER ALREADY EXISTS", 0);
      } else {
        responseJson = await this.adapter.updateById(
          mongoose.Types.ObjectId(context.params.id.toString()),
          updateData
        );
        // return { "code": 1, "data": responseJson, "message": "" };
      }
    } else {
      if (jsonData) {
        // return { "code": 0, "data": {}, "message": "USER ALREADY EXIST" };
        throw new MoleculerError("USER ALREADY EXISTS", 0);
      } else {
        responseJson = await this.adapter.insert(updateData);
        const userId = responseJson._id?.toString();
        // return { "code": 1, "data": responseJson, "message": "" };
        //----------- invite and earn Start --------------
        if (userCheckData) {
          const inviteAndEarnSettings = await storageUtil.read(
            constantUtil.INVITEANDEARN
          );
          if (inviteAndEarnSettings?.data?.forUser?.rideCount === 0) {
            await this.broker.emit("user.inviteAndEarn", {
              "clientId": clientId,
              "joinerId": userId,
            });
          } else {
            await this.adapter.model.updateOne(
              {
                "_id": mongoose.Types.ObjectId(userCheckData?._id?.toString()),
              },
              {
                "$inc": { "invitationCount": 1 }, // "joinCount": 1,
              }
            );
          }
        }
        //----------- invite and earn End --------------
      }
    }
  } catch (e) {
    errorCode = 0; // For Error code is 0
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseJson = {};
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseJson,
  };
};

userEvent.changeUsersStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    {
      "status": context.params.status,
    }
  );
  if (!responseJson.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN STATUS CHANGE" };
  } else {
    return { "code": 1, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
userEvent.getById = async function (context) {
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .lean();
  if (!responseJson) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateScheduleRide = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
      "status": constantUtil.ACTIVE,
    },
    {
      "$push": {
        "bookingInfo.scheduledBooking": {
          "bookingId": context.params.bookingId,
          "bookingDate": new Date(context.params.bookingDate),
          "estimationTime": context.params.estimationTime,
        },
      },
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateOnGoingBooking = async function (context) {
  const updateData =
    context.params.bookingType === constantUtil.INSTANT
      ? {
          "bookingInfo.ongoingBooking": context.params.bookingId
            ? mongoose.Types.ObjectId(context.params?.bookingId?.toString())
            : null,
        }
      : {
          "bookingInfo.ongoingBooking": context.params.bookingId
            ? mongoose.Types.ObjectId(context.params?.bookingId?.toString())
            : null,
          "$pull": {
            "bookingInfo.scheduledBooking": {
              "bookingId": context.params.bookingId
                ? mongoose.Types.ObjectId(context.params?.bookingId?.toString())
                : null,
            },
          },
        };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": { "$in": [constantUtil.ACTIVE, constantUtil.INCOMPLETE] },
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.removeOngoingBooking = async function (context) {
  const userData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
      "bookingInfo.ongoingBooking": mongoose.Types.ObjectId(
        context.params.bookingId.toString()
      ),
    },
    {
      "bookingInfo.ongoingBooking": null,
    }
  );
  if (!userData.nModified) {
    throw new MoleculerError(
      `REMOVING OF ONGOING BOOKING OF USER ${context.params?.user?.toString()} AND BOOKING ID ${context.params?.bookingId?.toString()}`
    );
  } else {
    return {
      "code": 200,
      "data": [],
      "message": "USER ONGOING BOOKING SUCCESSFULLY",
    };
  }
};
userEvent.removeScheduleBooking = async function (context) {
  const updateData = {
    "$pull": {
      "bookingInfo.scheduledBooking": {
        "bookingId": mongoose.Types.ObjectId(
          context.params.bookingId.toString()
        ),
      },
    },
  };
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.userId.toString()),
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updatependingReview = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      // "status": constantUtil.ACTIVE,
    },
    {
      "bookingInfo.ongoingBooking": null,
      "bookingInfo.pendingReview": context.params.bookingId
        ? context.params.bookingId
        : null,
      "lastCurrencyCode": context.params.lastCurrencyCode,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateRating = async function (context) {
  let responseJson = {};
  try {
    const userData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.userId.toString()))
      .lean();
    const avgRating =
      (parseFloat(userData.review.avgRating ? userData.review.avgRating : 0) *
        parseInt(
          userData.review.totalReview ? userData.review.totalReview : 0
        ) +
        parseFloat(context.params.rating)) /
      (parseFloat(
        userData.review.totalReview ? userData.review.totalReview : 0
      ) +
        1);

    const totalReview =
      parseInt(userData.review.totalReview ? userData.review.totalReview : 0) +
      1;

    responseJson = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.userId),
          // "status": constantUtil.ACTIVE,
        },
        {
          "review.avgRating": avgRating,
          "review.totalReview": totalReview,
          "bookingInfo.pendingReview": null,
          "bookingInfo.ongoingBooking": null,
        },
        { "new": true }
      )
      .lean();
    // if (!responseJson.nModified) {
    //   return null;
    // } else {
    // return responseJson;
    // }
  } catch (e) {
    responseJson = {};
  }
  return responseJson;
};
userEvent.updateEscortRating = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.userId.toString()))
    .lean();
  const avgRating =
    (parseFloat(
      userData.escortReview.avgRating ? userData.escortReview.avgRating : 0
    ) *
      parseInt(
        userData.escortReview.totalReview
          ? userData.escortReview.totalReview
          : 0
      ) +
      parseFloat(context.params.rating)) /
    (parseFloat(
      userData.escortReview.totalReview ? userData.escortReview.totalReview : 0
    ) +
      1);

  const totalReview =
    parseInt(
      userData.escortReview.totalReview ? userData.escortReview.totalReview : 0
    ) + 1;

  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortReview.avgRating": avgRating,
      "escortReview.totalReview": totalReview,
      "escortInfo.pendingReview": null,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

userEvent.updateTrustedContact = async function (context) {
  let trustedContactsUserId = null;
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "ERROR IN CONTACTS" };
  }
  if (context.params.action === "add" && jsonData.trustedContacts.length >= 5)
    return {
      "code": 0,
      "data": {},
      "message": "ALREADY YOU HAVE 5 TRUSTED CONTACTS",
    };
  let trustedContacts = false;
  if (jsonData.trustedContacts.length > 0 && context.params.contacts)
    await jsonData.trustedContacts.map((contacts) => {
      if (
        contacts.phone.code === context.params.contacts.phone.code &&
        contacts.phone.number === context.params.contacts.phone.number
      ) {
        trustedContacts = true;
      }
    });
  if (trustedContacts === true) {
    return { "code": 0, "data": {}, "message": "CONTACTS ALREADY EXIST" };
  }
  if (context.params.action === "add") {
    trustedContactsUserId = await this.adapter.model
      .findOne(
        {
          "_id": { "$ne": mongoose.Types.ObjectId(context.params.userId) },
          "phone.code": context.params.contacts.phone.code,
          "phone.number": context.params.contacts.phone.number,
        },
        { "_id": 1 }
      )
      .lean();
  }
  //
  const updateContacts =
    context.params.action === "add"
      ? {
          "$push": {
            "trustedContacts": {
              "_id": context.params.contacts._id,
              "userId": trustedContactsUserId?._id?.toString() || null,
              "name": context.params.contacts.name,
              "phone": context.params.contacts.phone,
              "reason": context.params.contacts.reasons,
            },
          },
        }
      : {
          "$pull": {
            "trustedContacts": {
              "_id": mongoose.Types.ObjectId(context.params.id),
            },
          },
        };

  const responseData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
    },
    updateContacts
  );
  if (!responseData.nModified) {
    return { "code": 200, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

userEvent.changeTrustedContactReason = async function (context) {
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE REASON" };
  }
  // if (jsonData.trustedContacts.length >= 5)
  //   return {
  //     "code": 0,
  //     "data": {},
  //     "message": "ALREADY YOU HAVE 5 TRUSTED CONTACTS",
  //   };
  let trustedContacts = false;
  if (jsonData.trustedContacts.length > 0 && context.params.contacts)
    await jsonData.trustedContacts.map((contacts) => {
      if (
        contacts._id.toString() !== context.params.id.toString() &&
        contacts.phone.number === context.params.contacts.phone.number
      ) {
        trustedContacts = true;
      }
    });
  if (trustedContacts === true) {
    return { "code": 0, "data": {}, "message": "CONTACTS ALREADY EXIST" };
  }
  const trustedContactsData = jsonData.trustedContacts.map((contacts) => {
    if (contacts._id.toString() === context.params.id.toString()) {
      contacts.name = context.params.contacts.name;
      contacts.phone = context.params.contacts.phone;
      contacts.reason = context.params.contacts.reasons;
    }
    return contacts;
  });
  const responseData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
    },
    { "trustedContacts": trustedContactsData }
  );
  if (!responseData.nModified) {
    return { "code": 0, "data": {}, "message": "ERROR IN UPDATE" };
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

userEvent.getTrustedContactsList = async function (context) {
  const jsonData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      { "trustedContacts": 1 }
    )
    .lean();
  if (!jsonData) {
    return { "code": 200, "data": {}, "message": "ERROR IN TRUSTED CONTACT" };
  } else {
    return {
      "code": 200,
      "data":
        jsonData?.trustedContacts?.map((contacts) => {
          return {
            "_id": contacts._id,
            "userId": contacts.userId,
            "name": contacts.name,
            "phone": contacts.phone,
            "actionType":
              contacts.reason?.action?.type || constantUtil.CONST_MANUAL,
            "fromTime": contacts.reason?.fromTime || null,
            "toTime": contacts.reason?.toTime || null,
            "reason": {
              "_id": contacts.reason._id,
              "reason": contacts.reason.reason,
            },
          };
        }) || [],
      // "trustedContactsLog": jsonData.trustedContactsLog || [],
      "message": "",
    };
  }
};
userEvent.updateLiveLocation = async function (context) {
  const updateData = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.userId) },
    {
      "location.coordinates": [context.params.lng, context.params.lat],
      "currentBearing": context.params.currentBearing,
      "altitude": context.params.altitude,
      "horizontalAccuracy": context.params.horizontalAccuracy,
      "verticalAccuracy": context.params.verticalAccuracy,
      "speed": context.params.speed,
      "isLocationUpdated": true,
      "locationUpdatedTime": new Date(),
    }
  );
  if (!updateData.nModified) {
    return { "code": 0, "data": {}, "message": "FAILED TO UPDATE LOCATION" };
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "LOCATION UPDATED SUCCESSFULLY",
    };
  }
};
userEvent.updateSubscriptionPlan = async function (context) {
  const updateData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
    },
    {
      "subscriptionInfo.isSubscribed": true,
      "subscriptionInfo.planId": context.params.planId,
      "subscriptionInfo.subscribedAt": context.params.subscribedAt,
      "subscriptionInfo.subscribedEndAt": context.params.subscribedEndAt,
      "subscriptionInfo.subscribedAtTimestamp":
        context.params.subscribedAtTimestamp,
      "subscriptionInfo.subscribedEndAtTimestamp":
        context.params.subscribedEndAtTimestamp,
      "wallet.availableAmount": context.params.price,
    }
  );
  if (!updateData.nModified) {
    return {
      "code": 0,
      "data": {},
      "message": "ERROR IN ADD SUBSCRIPTION PLAN",
    };
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "SUBSCRIPTION PLANE ADDED SUCCESSFULLY",
    };
  }
};
userEvent.updateWalletAmountInBooking = async function (context) {
  const updateData = {
    "wallet.availableAmount": context.params.finalAmount,
    "wallet.freezedAmount": context.params.amount,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateCancelFeeInWallet = async function (context) {
  const updateData = {
    "wallet.availableAmount": context.params.amount,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateWalletForPendingAmount = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    {
      "wallet.availableAmount": 0,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateWalletAmountTripEnded = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.userId.toString()))
    .lean();
  let valueData;
  if (context.params.paymentMethod === constantUtil.PAYMENTWALLET) {
    valueData =
      context.params.transStatus === constantUtil.EQUALTOPAID
        ? {
            "availableAmount": parseFloat(
              userData.wallet.availableAmount.toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : context.params.transStatus === constantUtil.LESSTHENPAID
        ? {
            "availableAmount": parseFloat(
              (
                parseFloat(userData.wallet.availableAmount) +
                (parseFloat(userData.wallet.freezedAmount) -
                  parseFloat(context.params.amount))
              ).toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : context.params.transStatus === constantUtil.GREATERTHENPAID
        ? {
            "availableAmount": parseFloat(
              (
                parseFloat(userData.wallet.availableAmount) +
                (parseFloat(userData.wallet.freezedAmount) -
                  parseFloat(context.params.amount))
              ).toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : {
            "availableAmount": parseFloat(
              userData.wallet.availableAmount.toFixed(2)
            ),
            "freezedAmount": parseFloat(
              userData.wallet.freezedAmount.toFixed(2)
            ),
          };
  } else if (context.params.paymentMethod === constantUtil.PAYMENTCARD) {
    valueData =
      context.params.transStatus === constantUtil.EQUALTOPAID
        ? {
            "availableAmount": parseFloat(
              userData.wallet.availableAmount.toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : context.params.transStatus === constantUtil.LESSTHENPAID
        ? {
            "availableAmount": parseFloat(
              (
                parseFloat(userData.wallet.availableAmount) +
                (parseFloat(userData.wallet.freezedAmount) -
                  parseFloat(context.params.amount))
              ).toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : context.params.transStatus === constantUtil.GREATERTHENPAID
        ? {
            "availableAmount": parseFloat(
              (
                parseFloat(userData.wallet.availableAmount) +
                (parseFloat(userData.wallet.freezedAmount) -
                  parseFloat(context.params.amount))
              ).toFixed(2)
            ),
            "freezedAmount": 0,
          }
        : {
            "availableAmount": parseFloat(
              userData.wallet.availableAmount.toFixed(2)
            ),
            "freezedAmount": parseFloat(
              userData.wallet.freezedAmount.toFixed(2)
            ),
          };
  }
  const updateData = {
    "wallet.availableAmount": valueData.availableAmount,
    "wallet.freezedAmount": valueData.freezedAmount,
  };

  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) return null;

  const finalamount = Math.abs(
    parseFloat(userData.wallet.freezedAmount) -
      parseFloat(context.params.amount)
  );
  if (context.params.paymentMethod === constantUtil.PAYMENTCARD) {
    if (context.params.transStatus === constantUtil.GREATERTHENPAID) {
      this.broker.emit("transaction.userLessAndGreaterEndTransaction", {
        "clientId": userData?.clientId?.toString(),
        "amount": finalamount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": valueData.availableAmount,
        "transType": constantUtil.GREATERTHENPAID,
        "transactionType": constantUtil.EXTRAAMOUNTDEBIT,
        "paymentType": constantUtil.DEBIT,
        "userType": constantUtil.USER,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
      });
    } else if (context.params.transStatus === constantUtil.LESSTHENPAID) {
      this.broker.emit("transaction.userLessAndGreaterEndTransaction", {
        "clientId": userData?.clientId?.toString(),
        "amount": finalamount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": valueData.availableAmount,
        "transType": constantUtil.LESSTHENPAID,
        "transactionType": constantUtil.CARDAMOUNTREFUND,
        "paymentType": constantUtil.CREDIT,
        "userType": constantUtil.USER,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
      });
    }
  }

  return responseJson;
};

userEvent.updateWalletRechargeOrWithdraw = async function (context) {
  console.log("i am from updatewalletRechrge");
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.userId.toString()))
    .lean();
  console.log("this is userData from ", userData);
  const finalAmount =
    context.params.transStatus === constantUtil.CREDIT
      ? parseFloat(userData.wallet.availableAmount) +
        parseFloat(context.params.amount)
      : parseFloat(userData.wallet.availableAmount) -
        parseFloat(context.params.amount);
  const updateData = {
    "wallet.availableAmount": finalAmount,
  };

  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  console.log("this is updated data", responseJson);
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

userEvent.sendMoneyToFriends = async function (context) {
  let toUserData;
  const { USER_NOT_FOUND, INVALID_USER_DATA } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);
  //REQUEST USER
  const fromUserData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
      },
      {
        "$inc": { "wallet.availableAmount": -context.params.amount }, // There is no $dec command, so used $inc command with negative sign
      },
      {
        "fields": { "password": 0, "accessToken": 0 },
      }
    )
    .lean();
  if (!fromUserData) {
    throw new MoleculerError(INVALID_USER_DATA);
  }
  // TO USER
  if (context.params.receiverType === constantUtil.USER) {
    toUserData = await this.adapter.model
      .findOneAndUpdate(
        {
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
        },
        {
          "$inc": { "wallet.availableAmount": context.params.amount },
        },
        {
          "fields": { "password": 0, "accessToken": 0 },
        }
      )
      .lean();
  } else {
    toUserData = await this.schema.professionalModel.model
      .findOneAndUpdate(
        {
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
        },
        {
          "$inc": { "wallet.availableAmount": context.params.amount },
        },
        {
          "fields": { "password": 0, "accessToken": 0 },
        }
      )
      .lean();
  }
  if (!toUserData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  // send notification to TO USER
  if (toUserData?.deviceInfo[0]?.deviceId) {
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.USER,
        "action": constantUtil.MSG_WALLETRECHARGE,
        "timeStamp": Date.now(),
        "message": "AMOUNT CREDITED",
        "details": lzStringEncode({ "message": "AMOUNT CREDITED" }),
      },
      "registrationTokens": [
        {
          "token": toUserData.deviceInfo[0]?.deviceId || "",
          "id": toUserData._id?.toString() || "",
          "deviceType": toUserData.deviceInfo[0]?.deviceType || "",
          "platform": toUserData.deviceInfo[0]?.platform || "",
          "socketId": toUserData.deviceInfo[0]?.socketId || "",
        },
      ],
    };
    this.broker.emit("admin.sendFCM", notificationObject);
    this.broker.emit("admin.sendNotification", notificationObject);
  }
  return {
    "from": fromUserData,
    "to": toUserData,
  };
};
userEvent.scheduleWalletUpdate = async function (context) {
  const updateData = {
    "wallet.availableAmount": context.params.finalAmount,
    "wallet.scheduleFreezedAmount": context.params.amount,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.updateWalletAmountInScheduleBooking = async function (context) {
  const updateData = {
    "wallet.scheduleFreezedAmount": context.params.finalAmount,
    "wallet.freezedAmount": context.params.amount,
  };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId),
      "status": constantUtil.ACTIVE,
    },
    updateData
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
userEvent.getAvailableWalletAmount = async function (context) {
  const query = [];
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "status": {
        "$in": [
          constantUtil.ACTIVE,
          constantUtil.INACTIVE,
          constantUtil.UNVERIFIED,
          constantUtil.ARCHIVE,
        ],
      },
    },
  });
  query.push({
    "$facet": {
      "response": [
        {
          "$project": {
            "_id": 1,
            "wallet": 1,
          },
        },
        {
          "$group": {
            "_id": "",
            "totalWalletAmount": { "$sum": "$wallet.availableAmount" },
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return jsonData[0]?.response?.[0]?.totalWalletAmount || 0;
};

userEvent.getWalletAmountList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const match = {};
  match["$and"] = [];
  match["$and"].push({
    //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    "status": {
      "$in": [
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.UNVERIFIED,
        constantUtil.ARCHIVE,
      ],
    },
    "wallet.availableAmount": { "$lt": 0 },
  });

  if ((context.params?.search || "") !== "") {
    match["$and"].push({
      "$or": [
        {
          "firstName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "lastName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "email": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.code": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "phone.number": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    });
  }
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({ "createdAt": { "$gte": fromDate, "$lt": toDate } });
  }
  // FOR EXPORT DATE FILTER

  const query = [];
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": 1,
            "firstName": 1,
            "lastName": 1,
            "phone": 1,
            "email": 1,
            "wallet": 1,
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};
userEvent.checkUserAvailable = async function (context) {
  const contextData = context.params.contacts;
  const contacts = await context.params.contacts.map((contact) => {
    return {
      "code": contact.code,
      "number": contact.number,
    };
  });
  const responseJson = await this.adapter.model.find(
    {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "phone": { "$in": contacts },
    },
    {
      "_id": 1,
      "phone": 1,
    }
  );
  if (!responseJson) return { "code": 0 };
  const respoData = [];
  await contextData.forEach((contact) => {
    responseJson.forEach((data) => {
      if (data.phone.number.toString() === contact.number.toString()) {
        respoData.push({
          "_id": data._id,
          "status": contact.status,
        });
      }
    });
  });
  return {
    "code": 1,
    "data": respoData,
  };
};

userEvent.getUsers = async function (context) {
  const contextData = context.params.ids;
  const contacts = await context.params.ids.map((contact) => {
    return contact._id;
  });
  const responseJson = await this.adapter.model.find({
    //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    "_id": { "$in": contacts },
  });
  if (!responseJson) return [];
  const respoData = [];
  await contextData.forEach((contact) => {
    responseJson.forEach((data) => {
      if (data._id.toString() === contact._id.toString()) {
        respoData.push({
          "_id": data._id,
          "firstName": data.firstName,
          "lastName": data.lastName,
          "number": data.phone.number,
          "code": data.phone.code,
          "avatar": data.avatar,
          "status": contact.status,
        });
      }
    });
  });
  return respoData;
};

userEvent.getUserDetails = async function (context) {
  // const contacts = {
  //   "code": context.params.code,
  //   "number": context.params.number,
  // };
  // const responseJson = await this.adapter.model.findOne(
  //   {
  //     "phone": contacts,
  //   },
  //   {
  //     "_id": 1,
  //     "phone": 1,
  //     "wallet": 1,
  //   }
  // );
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "phone.code": context.params.code,
        "phone.number": context.params.number,
      },
    },
    {
      "$project": {
        "_id": "$_id",
        "phone": 1,
        "wallet": "$wallet",
        "lastRedeemDate": 1,
        "lastWithdrawDate": 1,
      },
    },
  ]);
  if (!responseJson) {
    return { "code": 0 };
  } else {
    return responseJson[0]; // Return First array record (aggregate return array data)
  }
};

userEvent.updateOnGoingSecurityBooking = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortInfo.ongoingBooking": context.params.escortBookingId
        ? context.params.escortBookingId
        : null,
      "escortInfo.type": context.params.type ? context.params.type : "",
      "escortInfo.isOfficerAssigned": context.params.isOfficerAssigned
        ? context.params.isOfficerAssigned
        : false,
      "escortInfo.isEscortClosed": context.params.isEscortClosed
        ? context.params.isEscortClosed
        : false,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};

userEvent.updateSecurityPendingReview = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
      "status": constantUtil.ACTIVE,
    },
    {
      "escortInfo.ongoingBooking": null,
      "escortInfo.pendingReview": context.params.escortBookingId,
      "escortInfo.type": context.params.type,
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
//#region Invite And Earn
userEvent.inviteAndEarn = async function (context) {
  const inviteAndEarnSettings = await storageUtil.read(
    constantUtil.INVITEANDEARN
  );
  // joiner
  const joiner = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.joinerId),
  });
  // it means this user not referred by another user
  if (!joiner?.referredBy && joiner?.referredBy === "") {
    return null;
  }

  const joinerJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.joinerId),
    },
    {
      "wallet.availableAmount":
        joiner.wallet.availableAmount +
        inviteAndEarnSettings.data.forUser.amountToJoiner,
      "joinAmount": inviteAndEarnSettings.data.forUser.amountToJoiner,
      "joinerRideCount":
        (inviteAndEarnSettings?.data?.forUser?.rideCount || 0) === 0
          ? 0
          : (joiner?.joinerRideCount || 0) + 1,
    }
  );
  if (
    joinerJson.nModified &&
    parseFloat(inviteAndEarnSettings.data?.forUser?.amountToJoiner || 0) > 0
  ) {
    this.broker.emit("transaction.joinCharge", {
      "clientId": context.params?.clientId,
      "amount": inviteAndEarnSettings.data.forUser.amountToJoiner,
      "userType": constantUtil.USER,
      "data": {
        "id": joiner._id.toString(),
        "firstName": joiner.firstName,
        "lastName": joiner.lastName,
        "avatar": joiner.avatar,
        "phone": {
          "code": joiner.phone.code,
          "number": joiner.phone.number,
        },
      },
      "previousBalance": joiner.wallet.availableAmount,
      "currentBalance":
        joiner.wallet.availableAmount +
        inviteAndEarnSettings.data.forUser.amountToJoiner,
    });
  }
  // invitor
  let inviter;
  if (joiner.referredBy) {
    inviter = await this.adapter.model.findOne({
      //"clientId": mongoose.Types.ObjectId(context.params?.clientId),
      "uniqueCode": joiner.referredBy.toString(),
    });
  }
  if (!inviter) return null;

  const referrelTotalAmount =
    (inviter?.referrelTotalAmount ?? 0) +
    inviteAndEarnSettings.data.forUser.amountToInviter;

  const inviterJson = await this.adapter.model.updateOne(
    {
      "uniqueCode": joiner.referredBy.toString(),
    },
    {
      "wallet.availableAmount":
        inviter.wallet.availableAmount +
        inviteAndEarnSettings.data.forUser.amountToInviter,
      "referrelTotalAmount": referrelTotalAmount,
      "joinCount": (inviter?.joinCount ?? 0) + 1,
      "invitationCount":
        (inviteAndEarnSettings?.data?.forProfessional?.rideCount || 0) === 0
          ? 1
          : inviter?.invitationCount,
    }
  );
  if (
    inviterJson.nModified &&
    parseFloat(inviteAndEarnSettings.data?.forUser?.amountToInviter || 0) > 0
  ) {
    this.broker.emit("transaction.inviteCharge", {
      "clientId": context.params?.clientId,
      "amount": parseFloat(
        inviteAndEarnSettings.data.forUser.amountToInviter.toFixed(2)
      ),
      "userType": constantUtil.USER,
      "data": {
        "id": inviter._id.toString(),
        "firstName": inviter.firstName,
        "lastName": inviter.lastName,
        "avatar": inviter.avatar,
        "phone": {
          "code": inviter.phone.code,
          "number": inviter.phone.number,
        },
      },
      "previousBalance": inviter.wallet.availableAmount,
      "currentBalance":
        inviter.wallet.availableAmount +
        inviteAndEarnSettings.data.forUser.amountToInviter,
    });
  }
  return {
    "code": 200,
  };
};

userEvent.updateJoinerRideCount = async function (context) {
  let responseMessage = constantUtil.SUCCESS;
  try {
    // const inviteAndEarnSettings = await storageUtil.read(
    //   constantUtil.INVITEANDEARN
    // );
    // joiner
    const joiner = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.joinerId),
    });
    // it means this user not referred by another user
    if (!joiner?.referredBy && joiner?.referredBy === "") {
      return constantUtil.SUCCESS;
    }
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.joinerId),
      },
      {
        "joinerRideCount": (joiner?.joinerRideCount || 0) + 1,
      }
    );
    // // invitor
    // let inviter;
    // if (joiner.referredBy) {
    //   inviter = await this.adapter.model.findOne({
    //     "uniqueCode": joiner.referredBy?.toString(),
    //   });
    // }
    // if (!inviter) {
    //   return constantUtil.SUCCESS;
    // }
    // await this.adapter.model.updateOne(
    //   {
    //     "uniqueCode": joiner.referredBy.toString(),
    //   },
    //   {
    //     "invitationCount": (inviter?.invitationCount ?? 0) + 1,
    //   }
    // );
  } catch (e) {
    responseMessage = constantUtil.FAILED;
  }
  return responseMessage;
};

userEvent.inviterRideEarnings = async function (context) {
  let responseMessage = constantUtil.SUCCESS;
  try {
    // const inviteAndEarnSettings = await storageUtil.read(
    //   constantUtil.INVITEANDEARN
    // );
    // joiner
    // const joiner = await this.adapter.model.findOne({
    //   "_id": mongoose.Types.ObjectId(context.params.joinerId),
    // });
    // it means this user not referred by another user
    if (context.params.inviterUniqueCode) {
      // inviter
      const inviter = await this.adapter.model
        .findOne(
          { "uniqueCode": context.params.inviterUniqueCode },
          { "_id": 1, "wallet": 1 }
        )
        .lean();
      if (inviter) {
        const referralRideEarnings = context.params.amount;
        const inviterWalletAmount =
          inviter.wallet.availableAmount + referralRideEarnings;
        //
        const inviterJson = await this.adapter.model.updateOne(
          { "_id": mongoose.Types.ObjectId(inviter._id.toString()) },
          { "wallet.availableAmount": inviterWalletAmount }
        );
        if (inviterJson.nModified && referralRideEarnings > 0) {
          await this.broker.emit(
            "transaction.insertTransactionBasedOnUserType",
            {
              "transactionType": constantUtil.CONST_INCENTIVE,
              "transactionAmount": referralRideEarnings,
              "previousBalance": inviter?.wallet?.availableAmount || 0,
              "currentBalance": inviterWalletAmount,
              "transactionReason": context.params.transactionReason,
              "transactionStatus": constantUtil.SUCCESS,
              "bookingId": context.params?.bookingId,
              "refBookingId": context.params?.refBookingId,
              //  "transactionId": transactionId,
              "paymentType": constantUtil.CREDIT,
              "paymentGateWay": constantUtil.NONE,
              "gatewayResponse": {},
              "userType": constantUtil.ADMIN, // Debit From
              "userId": null, //bookingData.user._id.toString(), // Debit From
              "professionalId": null, // Debit From
              "paymentToUserType": constantUtil.USER, // Credit To
              "paymentToUserId": inviter?._id?.toString(), // Credit To
              "adminId": null, // For All Admin Model Ref Id
            }
          );
        }
      }
    }
  } catch (e) {
    responseMessage = constantUtil.FAILED;
  }
  return responseMessage;
};
//#endregion Invite And Earn
// DB ACCESS
userEvent.removePrefix0 = async function () {
  let userData = await this.adapter.model.find(
    { "phone.number": { "$regex": /^0/ } },
    {
      "_id": 1,
      "phone": 1,
    }
  );
  if (!userData[0]) {
    return {
      "code": 404,
      "data": {},
      "message": "IN USER PHONE NUMBER START WITH O IS NOT FOUND",
    };
  }
  const duplicateDoc = [];
  userData = await userData.map(async (data) => {
    try {
      return await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(data._id.toString()),
        },
        {
          "$set": {
            "phone.number": data.phone.number.substring(1),
          },
        },
        { "fields": { "phone": 1, "_id": 1 }, "new": true }
      );
    } catch (error) {
      if (error.code === 11000) {
        duplicateDoc.push(data.phone);
      }
    }
  });
  userData = await Promise.all(userData);
  userData = userData.filter((data) => !!data !== false);
  if (!userData[0]) {
    return {
      "code": 500,
      "data": {
        "duplicates": {
          "count": duplicateDoc.length,
          "duplicates": duplicateDoc,
        },
        "data": userData,
      },
      "message":
        "IN USER PHONE NUMBER START WITH O CANT UPDATE PLEASE CONTACT DEVELOPER",
    };
  } else {
    return {
      "code": 200,
      "data": {
        "duplicates": {
          "count": duplicateDoc.length,
          "duplicates": duplicateDoc,
        },
        "data": userData,
      },
      "message": "IN USERUPDATE SUCESSFULLY",
    };
  }
};

userEvent.forceLogout = async function () {
  const result = await this.adapter.model.updateMany(
    {},
    { "$set": { "deviceInfo": [] } }
  );
  return {
    "code": 200,
    "data": result,
    "message": "REQUESTED SUCCESSFULLY",
  };
};

userEvent.updateWalletAmount = async function (context) {
  const { INVALID_USER_DATA, SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  let updateData = {
    "wallet.availableAmount": context.params.availableAmount,
    "wallet.freezedAmount": context.params.freezedAmount,
    "wallet.scheduleFreezedAmount": context.params.scheduleFreezedAmount,
  };
  switch (context.params.requestFrom) {
    case constantUtil.WALLETWITHDRAWAL:
      updateData["lastWithdrawDate"] = new Date();
      break;
  }
  const response = await this.adapter.model
    .findOneAndUpdate(
      { "_id": mongoose.Types.ObjectId(context.params.userId.toString()) },
      { "$set": updateData },
      { "new": true }
    )
    .lean();
  if (!response) {
    throw new MoleculerError(
      INVALID_USER_DATA + " " + SOMETHING_WENT_WRONG,
      500
    );
  } else {
    if (context.params?.isSendNotification) {
      const notificationObject = {
        "temprorySound": true,
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": constantUtil.WALLET,
          "timestamp": Date.now(),
          "message": context.params?.notificationMessage || "",
          "details": lzStringEncode({
            "message": context.params?.notificationMessage || "",
          }),
        },
        "registrationTokens": [
          {
            "token": response.deviceInfo[0]?.deviceId || "",
            "id": response._id?.toString() || "",
            "deviceType": response.deviceInfo[0]?.deviceType || "",
            "platform": response.deviceInfo[0]?.platform || "",
            "socketId": response.deviceInfo[0]?.socketId || "",
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
    return {
      "code": 200,
      "data": [],
      "message": "UPDATED SUCCESSFULLY",
    };
  }
};
//
userEvent.addVerifiedCard = async function (context) {
  const userData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.userId),
    })
    .lean();
  if (!userData) {
    throw new MoleculerError("USER NOT FOUND SOMETHING WENT WRONG");
  }
  const cardUpdating = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.userId) },
    { "$push": { "cards": { ...context.params.card, "isVerified": true } } }
  );
  if (!cardUpdating.nModified) {
    return {
      "code": 500,
      "message": "SOMETHING WENT WRONG",
      "data": {},
    };
  } else {
    return {
      "code": 200,
      "message": "CARD ADDED SUCCESFULLY",
      "data": { ...context.params.card, "isVerified": true },
    };
  }
};
// To Admin
// refund details
userEvent.getRefundDetails = async function (context) {
  const refundData = await this.adapter.find({
    "skip": context.params.skip ?? 0,
    "limit": context.params.limit ?? 20,
    "sort": "-_id",
    "search": context.params.search ?? "",
    "searchFields": ["from.phoneNumber", "from.userType"],
  });
};
//Added by Ajith
userEvent.getCardDetailsById = async function (context) {
  const cardData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.userId),
    })
    .lean();
  if (!cardData) {
    throw new MoleculerError("INVALID CARD DETAILS");
  } else {
    return {
      "code": 200,
      "message": "VALID CARD",
      "data": cardData.cards.find(
        (card) => card._id.toString() === context.params.cardId
      ), //.toJSON(),
    };
  }
};
//Added by Ajith
userEvent.updateDefaultCardStatusInActiveActive = async function (context) {
  let updateDefaultCardStatus = {};
  try {
    updateDefaultCardStatus = await this.adapter.model.updateMany(
      {
        "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
        "cards.isDefault": true,
      },
      { "$set": { "cards.$.isDefault": false } }
    );
  } catch (error) {
    console.log(updateDefaultCardStatus);
  }
  if (context.params.cardId) {
    updateDefaultCardStatus = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
        "cards._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
      },
      {
        "$set": {
          "defaultPayment": constantUtil.PAYMENTCARD,
          "cards.$.isDefault": true,
        },
      }
    );
  }
  if (!updateDefaultCardStatus.nModified) {
    return {
      "code": 500,
      "message": "SOMETHING WENT WRONG",
      "data": {},
    };
  } else {
    return {
      "code": 200,
      "message": "DEFAULT CARD ACTIVATED",
      "data": {},
    };
  }
};
userEvent.changeCardDefaultStatus = async function (context) {
  const updateCardDefaultStatus = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
      "card._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
    },
    { "$set": { "cards.$.isDefault": context.params.status } }
  );
  if (!updateCardDefaultStatus.nModified) {
    throw new MoleculerError(
      "SOMETING WENT WRONG IN 'changeCardDefaultStatus' user event"
    );
  }
};
userEvent.getCategoryList = async function () {
  const clientId = context.params?.clientId;
  const response = await this.adapter.model.aggregate([
    {
      "$match": {
        "clientId": mongoose.Types.ObjectId(clientId),
        "$and": [
          { "status": constantUtil.ACTIVE },
          { "deviceInfo.deviceId": { "$exists": true } },
          { "deviceInfo.deviceId": { "$ne": "" } },
        ],
      },
    },
    {
      "$group": {
        "_id": { "$toLower": { "$substrCP": ["$firstName", 0, 1] } },
      },
    },
  ]);
  const category = [];
  // response.forEach((c) => !!c?._id === true && category.push(c?._id));
  response.forEach(
    (c) => !!c?._id === true && category.push({ "_id": c._id, "name": c._id })
  );
  return category.sort((a, b) => {
    return a.name.localeCompare(b.name);
  });
};
userEvent.getUserListForNotification = async function (context) {
  const clientId = context.params?.clientId;
  let query;
  switch (context.params.category?.toUpperCase()) {
    case constantUtil.ALPHABETICALORDER:
      query = {
        "status": constantUtil.ACTIVE,
        "firstName": new RegExp("^" + context.params.filter, "i"),
        "clientId": mongoose.Types.ObjectId(clientId),
      };
      break;

    case constantUtil.CITY:
      query = {
        "status": constantUtil.ACTIVE,
        "serviceAreaId": mongoose.Types.ObjectId(context.params.filter),
        "clientId": mongoose.Types.ObjectId(clientId),
      };
      if (context.params.gender) {
        query["gender"] = context.params.gender;
      }
      break;

    case constantUtil.CONST_ONLINE:
      query = {
        "clientId": mongoose.Types.ObjectId(clientId),
        "status": constantUtil.ACTIVE,
        "updatedAt": {
          "$gte": context.params.startDate,
          "$lte": context.params.endDate,
        },
      };
      break;

    case constantUtil.CONST_CUSTOM:
      query = {
        // "status": constantUtil.ACTIVE,
        "clientId": mongoose.Types.ObjectId(clientId),
        "updatedAt": {
          "$gte": context.params.startDate,
          "$lte": context.params.endDate,
        },
      };
      break;

    case constantUtil.ALL:
    case constantUtil.EXPIRED: // Profile Document Not Available & Vehicle Document Not Available
    case constantUtil.REJECTED: // Profile Document Not Available & Vehicle Document Not Available
      query = { "clientId": mongoose.Types.ObjectId(clientId) };
      break;
  }

  const count = await this.adapter.count({
    "query": query,
    "search": !!context.params.search === true ? context.params.search : "",
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  const response = await this.adapter.find({
    "offset": parseInt(context.params.skip || 0),
    "limit": parseInt(context.params.limit || 0),
    "query": query,
    "sort": "-_id",
    "search": !!context.params.search === true ? context.params.search : "",
    "searchFields": [
      "firstName",
      "lastName",
      "email",
      "phone.code",
      "phone.number",
    ],
  });
  return {
    count,
    response,
  };
};
// FOR PROMOTION CAMPIGN
userEvent.getUsersDataforPromtionCampaign = async function (context) {
  if (context.params.category === constantUtil.CITY) {
    const [{ location }] = await this.broker.emit("category.getCategoryById", {
      "id": context.params.id,
      "clientId": context.params.clientId,
    });
    if (!location) {
      throw new MoleculerError("SOMETHING WENT WRONG SELCETED CITY");
    }
    const users = await this.adapter.model
      .aggregate([
        {
          "$match": {
            //"clientId": mongoose.Types.ObjectId(context.params.clientId),
            "$and": [
              {
                "$deviceInfo.0.deviceId": { "$exists": true },
              },
              {
                "location": {
                  "$geoWithin": {
                    "$geometey": {
                      "type": "Polygon",
                      "coordinates": location.coordinates,
                    },
                  },
                },
              },
            ],
          },
        },
        {
          "$group": {
            "_id": {
              "$first": {
                "$spilt": ["$deviceInfo.0.platform", "/"],
              },
            },
            "count": { "$sum": 1 },
            "deviceIds": { "$push": "$deviceInfo.0.deviceId" },
          },
        },
      ])
      .allowDiskUse(true);
    if (!users) {
      return {
        "code": 204,
        "message": "NO USERS FOUND IN THIS CITY CATGRORY",
      };
    }
    return {
      "code": 200,
      "data": users,
    };
  }
};
//--------- Wallet Point Added Start --------------------------
userEvent.updateWalletRewardPoint = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
    },
    {
      "$inc": {
        "wallet.rewardPoints": convertToFloat(
          context?.params?.rewardPoints || 0
        ),
      },
    }
    // {
    //   "upsert": true,
    // }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return responseJson;
  }
};
//--------- Wallet Point Added End --------------------------
//#region Cororate Officer
userEvent.updateAndGetCororateOfficerByPhoneNumber = async function (context) {
  let userId;
  const clientId = context.params.clientId;
  userId = await this.adapter.model.findOne(
    {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
    },
    // { ...context.params },
    { "_id": 1 }
  );
  // const checkEmailCondition = userId?._id?.toString()
  //   ? {
  //       //"clientId": mongoose.Types.ObjectId(clientId),
  //       "_id": { "$ne": userId?._id },
  //       "email": context.params.email,
  //     }
  //   : {
  //       //"clientId": mongoose.Types.ObjectId(clientId),
  //       "email": context.params.email,
  //     };
  const checkEmailCount = await this.adapter.model.countDocuments(
    //checkEmailCondition
    {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "email": context.params.email,
    }
  );
  if (!userId && checkEmailCount > 0) {
    return { "code": 400, "userId": null };
  } else if (userId?._id) {
    const updateData = await this.adapter.model.findOneAndUpdate(
      { "_id": userId?._id },
      {
        "isCororateOfficer": true,
        // "email": context.params.email,
        // "name": context.params.name,
        // "gender": context.params.gender,
        // "phone": context.params.phone,
        // "isActive": context.params.isActive,
      },
      { "new": true }
    );
    userId = userId?._id?.toString();
  } else {
    const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
    const updateData = await this.adapter.insert({
      "clientId": clientId,
      "email": context.params.email,
      "isEmailVerified": context.params.isEmailVerified || false,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "gender": context.params.gender,
      "phone.number": context.params.phoneNumber,
      "phone.code": context.params.phoneCode,
      "nationalIdNo": context.params.nationalIdNo,
      "emailOTP": null,
      "dob": context.params.dob,
      // "isActive": context.params.isActive,
      "isCororateOfficer": true,
      "currencyCode": gerenalsetting?.data?.currencyCode,
      "lastCurrencyCode": gerenalsetting?.data?.currencyCode,
      "status": constantUtil.ACTIVE,
    });
    userId = updateData?._id?.toString();
  }
  // else
  // {
  //   //update
  // }
  // if (!responseJson.nModified) return null;

  return { "code": 200, "userId": userId };
};
//#endregion Cororate Officer
userEvent.sendPromotion = async function (context) {
  let mailData,
    message = "",
    trustedContactsLog = [];
  const responseMessage = constantUtil.SUCCESS;
  const clientId = context.params?.clientId;
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));

  const selectedRecordCount = 400;
  const loopCount = parseInt(ids.length / selectedRecordCount);
  //
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const emergencyResponseJson = await storageUtil.read(
    constantUtil.EMERGENCYSMSSETTING
  );
  if (context.params.type === constantUtil.EMERGENCY) {
    message = emergencyResponseJson.data?.notifyMessage || "";
    context.params.template = {
      "title": "emergencyNotification",
      "content": emergencyResponseJson.data?.notifyMessage,
      "image": null,
      "subject": null,
    };
    //
    message = message.replace(/{{siteTitle}}/g, generalSettings.data.siteTitle);
    message = message.replace(/{{senderName}}/g, context.params.senderName);
    message = message.replace(/{{trackUrl}}/g, context.params.trackLink);
  } else {
    message = context.params.template?.content || "";
  }
  //------ For Email (Get Email Template) -------------
  if (context.params.type === constantUtil.EMAIL) {
    mailData = await this.broker.emit("admin.getEmailNotificationTemplate", {
      "templateName": context.params.template.title,
    });
    mailData = mailData && mailData[0];
  }
  //------ For Email (Get Email Template) -------------
  for (let i = 0; i <= loopCount; i++) {
    const selectedRecordIds = ids.splice(0, selectedRecordCount);
    const userList = await this.adapter.model
      .find(
        {
          // "status": constantUtil.ACTIVE,
          "_id": { "$in": selectedRecordIds },
          "clientId": mongoose.Types.ObjectId(clientId),
        },
        {
          "_id": 1,
          "firstName": 1,
          "lastName": 1,
          "email": 1,
          "phone": 1,
          "isEmailVerified": 1,
          "wallet.availableAmount": 1,
          "deviceInfo.deviceId": 1,
          "deviceInfo.deviceType": 1,
          "deviceInfo.platform": 1,
          "deviceInfo.socketId": 1,
        }
      )
      .lean();
    // userList.forEach(async (reciverData) => {
    //   if (!reciverData || !reciverData.deviceInfo || !reciverData.deviceInfo[0])
    //     return;
    if (
      context.params.type === constantUtil.PUSH ||
      (context.params.type === constantUtil.EMERGENCY &&
        emergencyResponseJson.data?.isEnablePushNotify)
    ) {
      let notificationObject;
      if (context.params.type === constantUtil.EMERGENCY) {
        message = message.replace(/{{reciverName}}/g, "");
      }
      try {
        notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": context.params.userType.toUpperCase(),
            "action":
              context.params.type === constantUtil.EMERGENCY
                ? constantUtil.ACTION_EMERGENCY
                : constantUtil.ACTION_PROMOTION,
            "image": context.params.template?.image || "",
            "timestamp": Date.now(),
            "message": message,
            "details": lzStringEncode(context.params.template),
            // "details": lzStringEncode(message),
            "urlLink": context.params.trackLink,
          },
          "registrationTokens": userList.map((user) => {
            return {
              "token": user?.deviceInfo[0]?.deviceId || "",
              "id": user?._id?.toString(),
              "deviceType": user?.deviceInfo[0]?.deviceType || "",
              "platform": user?.deviceInfo[0]?.platform || "",
              "socketId": user?.deviceInfo[0]?.socketId || "",
            };
          }),
        };
        //
        if (userList.length > 0) {
          trustedContactsLog = userList.map((user) => {
            return {
              "userType": constantUtil.USER,
              "userId": user?._id?.toString(),
              "fullName": null,
              "phone": {
                "code": user.phone.code,
                "number": user.phone.number,
              },
              "notifyType": constantUtil.PUSH,
            };
          });
        }
      } catch (error) {
        console.log("error in notification object", error);
      }

      // if (notificationObject.registrationTokens[0].token === "") {
      //   return; // =>just retun bescuse without token we cant send notification
      // }

      // send notification
      this.broker.emit("admin.sendFCM", notificationObject);
      // this.broker.emit('socket.sendNotification', notificationObject)
    } else if (context.params.type === constantUtil.EMAIL) {
      userList.forEach(async (reciverData) => {
        if (reciverData.email && reciverData.isEmailVerified) {
          this.broker.emit("admin.sendPromotionMail", {
            "mailData": mailData,
            "templateName": context.params.template.title,
            ...(await mappingUtil.promtionMailObject(reciverData)),
          });
        }
      });
    } else if (context.params.type === constantUtil.SMS) {
      userList.forEach(async (reciverData) => {
        const phoneNumber =
          reciverData?.phone?.code?.trim() + reciverData?.phone?.number?.trim();
        this.broker.emit("booking.sendSMSAlertForPromotions", {
          "notifyType": constantUtil.PROMOTION,
          "notifyFor": constantUtil.USER,
          "promotionMessage": context.params.template.content,
          "phoneNumber": phoneNumber,
        });
      });
    }
    //#region Emergency Notification
    // EMAIL
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.emailConfigurationEnable
    ) {
      userList.forEach(async (reciverData) => {
        if (reciverData.email && reciverData.isEmailVerified) {
          // const trackLink = `${generalSettings.data.redirectUrls.trackBooking}${context.params.bookingId}`;
          reciverData["siteTitle"] = generalSettings.data.siteTitle;
          reciverData["senderName"] = context.params.senderName;
          reciverData["reciverName"] =
            reciverData.firstName + " " + reciverData.lastName;
          reciverData["trackUrl"] = context.params.trackLink;
          //
          this.broker.emit("admin.sendServiceMail", {
            "clientId": clientId,
            "templateName": "emergencyNotification",
            ...(await mappingUtil.promtionMailObject(reciverData)),
          });
          //
          trustedContactsLog.push({
            "userType": constantUtil.USER,
            "userId": reciverData?._id?.toString(),
            "fullName": null,
            "phone": {
              "code": reciverData.code?.trim(),
              "number": reciverData.number?.trim(),
            },
            "notifyType": constantUtil.EMAIL,
          });
        }
      });
    }
    // SMS
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.isEnableSMSGatwayOTP
    ) {
      // userList.forEach(async (reciverData) => {
      context.params.phone?.forEach(async (reciverData) => {
        message = message.replace(/{{reciverName}}/g, reciverData?.fullName);
        const phoneNumber =
          reciverData?.code?.trim() + reciverData?.number?.trim();
        //
        this.broker.emit("booking.sendSMSAlertForPromotions", {
          "notifyType": constantUtil.EMERGENCY,
          "notifyFor": constantUtil.USER,
          "promotionMessage": message,
          "phoneNumber": phoneNumber,
        });
        //
        trustedContactsLog.push({
          "userType": constantUtil.USER,
          "userId": null,
          "fullName": reciverData.fullName,
          "phone": {
            "code": reciverData.code?.trim(),
            "number": reciverData.number?.trim(),
          },
          "notifyType": constantUtil.SMS,
        });
      });
    }
    // WhatsUp
    if (
      context.params.type === constantUtil.EMERGENCY &&
      emergencyResponseJson.data?.isEnableWhatsupOTP
    ) {
      // Need to Work WhatsUp Notification
      context.params.phone?.forEach(async (reciverData) => {
        message = message.replace(/{{reciverName}}/g, reciverData.fullName);
        //
        this.broker.emit("booking.sendWhatsupAlert", {
          "clientId": context.params?.clientId,
          "phoneCode": reciverData?.code?.trim(),
          "phoneNumber": reciverData?.number?.trim(),
          "rideId": null,
          "bookingFrom": constantUtil.ADMIN,
          "notifyType": constantUtil.NONE,
          "notifyFor": constantUtil.ALL,
          "alertType": constantUtil.OTP,
          "message": message,
        });
        //
        trustedContactsLog.push({
          "userType": constantUtil.USER,
          "userId": null,
          "fullName": reciverData.fullName,
          "phone": {
            "code": reciverData.code?.trim(),
            "number": reciverData.number?.trim(),
          },
          "notifyType": constantUtil.WHATSUP,
        });
      });
    }
    //#endregion Emergency Notification
  }
  return {
    "code": 200,
    "message": responseMessage,
    "trustedContactsLog": trustedContactsLog,
  };
};

userEvent.updateServiceAreaId = async function (context) {
  await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params?.userId?.toString()),
    },
    { "serviceAreaId": mongoose.Types.ObjectId(context.params.serviceAreaId) }
  );
  return constantUtil.SUCCESS;
};
userEvent.getEndRideIssueList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "$or": [
        // { "bookingInfo.pendingReview": { "$ne": null } },
        { "bookingInfo.ongoingBooking": { "$ne": null } },
      ],
    },
  });

  // FOR BOOKING BY FILTER
  query.push(
    {
      "$lookup": {
        "from": "bookings",
        "localField": "bookingInfo.ongoingBooking",
        "foreignField": "_id",
        "as": "bookingList",
      },
    },

    {
      "$unwind": {
        "path": "$bookingList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$match": {
        "bookingList.bookingStatus": {
          "$in": [
            constantUtil.ENDED,
            constantUtil.EXPIRED,
            constantUtil.USERDENY,
            constantUtil.USERCANCELLED,
            constantUtil.PROFESSIONALCANCELLED,
          ],
        },
      },
    },
    {
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          {
            "$project": {
              "_id": "$_id",
              "bookingStatus": "$bookingList.bookingStatus",
              "bookingId": "$bookingList.bookingId",
              "refBookingId": "$bookingList._id",
              "bookingType": "$bookingList.bookingType",
              "bookingBy": "$bookingList.bookingBy",
              "bookingDate": "$bookingList.bookingDate",
              "ongoingBooking": "$bookingInfo.ongoingBooking",
              "pendingReview": "$bookingInfo.pendingReview",
              "Name": { "$concat": ["$firstName", "$lastName"] },
              "phoneNumber": {
                "$concat": ["$phone.code", "$phone.number"],
              },
            },
          },
          { "$sort": { "bookingList.bookingDate": -1 } },
          { "$skip": skip },
          { "$limit": limit },
        ],
      },
    }
  );
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return { "code": 1, "data": responseJson, "message": "" };
};
userEvent.updateBookingInfoIds = async function (context) {
  if (context.params.updatedId === "ongoingBooking") {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      },
      {
        "$set": { "bookingInfo.ongoingBooking": null },
      }
    );
  } else if (context.params.updatedId === "pendingReview") {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params?.userId?.toString()),
      },
      {
        "$set": { "bookingInfo.pendingReview": null },
      }
    );
  }

  return constantUtil.SUCCESS;
};

//#region User Incentive
userEvent.calculateAndUpdateIncentiveAmount = async function (context) {
  const { INFO_INCENTIVE_CREDIT } = await notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  try {
    // const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    // const professionalList = await this.adapter.model
    //   .find(
    //     {
    //       "status": constantUtil.ACTIVE,
    //       "onlineStatus": false,
    //       "updatedAt": {
    //         "$gte": new Date(new Date().getTime() - 1000 * 60 * 1),
    //       }, // 1 minutes ago (from now) / Last 1 minutes Updated Records
    //     },
    //     { "_id": 1, "onlineStatus": 1, "updatedAt": 1 }
    //   )
    //   .lean();
    // // await professionalList?.map(async (professional) => {
    // //   await this.broker.emit("log.updateSignupLog", {
    // //     "name": constantUtil.LOGOUT,
    // //     "userType": constantUtil.PROFESSIONAL,
    // //     "userId": professional._id?.toString(),
    // //   });
    // // });
    const currentAvailableAmount =
      parseFloat(context.params.availableAmount) +
      parseFloat(context.params.amount);

    const responseJson = await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(context.params.userId) },
      { "$set": { "wallet.availableAmount": currentAvailableAmount } }
    );
    //-------- Transaction Entry Start ---------------------
    if (responseJson.nModified) {
      this.broker.emit("transaction.insertTransactionBasedOnUserType", {
        "transactionType": constantUtil.CONST_INCENTIVE,
        "transactionAmount": context.params.amount,
        "previousBalance": context.params.availableAmount,
        "currentBalance": currentAvailableAmount,
        "transactionReason": INFO_INCENTIVE_CREDIT,
        "transactionStatus": constantUtil.SUCCESS,
        //  "transactionId": transactionId,
        "paymentType": constantUtil.CREDIT,
        "paymentGateWay": constantUtil.NONE,
        "gatewayResponse": {},
        "userType": constantUtil.ADMIN, // Debit From
        "userId": null, //bookingData.user._id.toString(), // Debit From
        "professionalId": null, // Debit From
        "paymentToUserType": constantUtil.USER, // Credit To
        "paymentToUserId": context.params.userId, // Credit To
        "adminId": context.params.incentiveId, // For All Admin Model Ref Id
      });
    }
    //-------- Transaction Entry End ---------------------
  } catch (e) {
    const xx = e.message;
  }
};

userEvent.getUserListByServiceAreaId = async function (context) {
  try {
    const responseJson = await this.adapter.model
      .find(
        {
          "clientId": mongoose.Types.ObjectId(context.params.clientId),
          "serviceAreaId": mongoose.Types.ObjectId(
            context.params.serviceAreaId
          ),
        },
        {
          "_id": 1,
          "wallet": 1,
          "serviceAreaId": 1,
          // "wallet.availableAmount": 1,
        }
      )
      .lean();

    return responseJson || [];
  } catch (e) {
    return null;
  }
};

//#endregion User Incentive

userEvent.checkUserAvail = async function (context) {
  let userData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
      "status": constantUtil.ACTIVE,
    })
    .lean();
  if (userData) {
    userData = mappingUtil.userObjectForAdmin(userData);
  } else {
    userData = null;
  }
  return userData;
};

userEvent.checkUniqueReferralCode = async function (context) {
  let userData = await this.adapter.model
    .findOne(
      {
        "_id": {
          "$ne": mongoose.Types.ObjectId(context.params.userId),
        },
        "uniqueCode": context.params.uniqueCode,
      },
      { "_id": 1, "firstName": 1, "lastName": 1, "uniqueCode": 1 }
    )
    .lean();
  if (userData) {
    userData = { "isValidUniqueCode": false };
  } else {
    userData = { "isValidUniqueCode": true };
  }
  return userData;
};

//----------------------------
module.exports = userEvent;
