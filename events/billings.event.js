const each = require("sync-each");

const storageUtil = require("../utils/storage.util");
const constantUtil = require("../utils/constant.util");
const mongoose = require("mongoose");

const billingsEvent = {};

billingsEvent.saveBillingCycleCronJob = async function () {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let diffTime = "";
  const earningData = {};
  const currentDate = new Date();
  const currentTime = Date.now();
  const billingcycle = generalSettings.data.billingDays * 24 * 60 * 60 * 1000;
  const startTime = parseInt(generalSettings.data.lastBilledTime);
  diffTime = currentTime - startTime;
  const diffMin = Math.abs(diffTime - billingcycle);
  const billingCycleFunction = async () => {
    const billingData = await this.adapter.insert({
      "startDate": new Date(generalSettings.data.lastBilledDate),
      "endDate": currentDate,
      "startTime": startTime,
      "endTime": currentTime,
      "payoutDate": currentDate,
      "billingcycle":
        parseInt(generalSettings.data.billingDays) === 1
          ? new Date(generalSettings.data.lastBilledDate).toLocaleDateString(
              "en-GB"
            )
          : (
              new Date(generalSettings.data.lastBilledDate).toLocaleDateString(
                "en-GB"
              ) +
              " - " +
              currentDate.toLocaleDateString("en-GB")
            ).toString(),
      "status": constantUtil.ACTIVE,
      "type": constantUtil.NORMAL,
    });
    this.broker.emit("admin.updateSettingBillingTime", {
      "clientId": context.params?.clientId,
      "lastBilledDate": currentDate,
      "lastBilledTime": currentTime,
    });
    let professionals = await this.broker.emit(
      "professional.getAllProfessionals",
      { "clientId": context.params?.clientId }
    );
    professionals = professionals[0];

    if (professionals && professionals.length > 0) {
      each(professionals, async (professional, next) => {
        earningData["professional"] = professional._id.toString();
        earningData["rideLists"] = [];
        earningData["paidStatus"] = true;
        let bookings = await this.broker.emit("booking.getAllBillingBookings", {
          "clientId": context.params?.clientId,
          "startDate": billingData.startDate,
          "endDate": billingData.endDate,
          "professional": professional._id.toString(),
        });
        bookings = bookings[0];
        if (bookings && bookings.length > 0) {
          let payoutCashAmount = 0,
            payoutCardAmount = 0,
            toleranceAmount = 0;
          await bookings.forEach(async (c) => {
            if (c.payment.option === constantUtil.PAYMENTCASH) {
              payoutCashAmount =
                parseFloat(payoutCashAmount) +
                parseFloat(c.invoice.siteCommission);
            } else {
              payoutCardAmount =
                parseFloat(payoutCardAmount) +
                parseFloat(c.invoice.professionalCommision);
            }
            toleranceAmount =
              parseFloat(toleranceAmount) +
              parseFloat(c.invoice.professionalTolerenceAmount);
            earningData["rideLists"].push(c._id.toString());
            await this.broker.emit("booking.updateBillingId", {
              "clientId": context.params?.clientId,
              "billingId": billingData._id.toString(),
              "bookingId": c._id.toString(),
            });
          });
          const finalAmount =
            parseFloat(payoutCardAmount) +
            parseFloat(toleranceAmount) +
            parseFloat(professional.wallet.availableAmount) -
            parseFloat(payoutCashAmount);

          await this.broker.emit("professional.updateWalletAmountInBooking", {
            "clientId": context.params?.clientId, // Need to Check
            "id": professional._id.toString(),
            "finalAmount": parseFloat(finalAmount.toFixed(2)),
          });
          await this.broker.emit("transaction.professionalPayoutTransaction", {
            "clientId": professional?.clientId?.toString(),
            "professionalId": professional._id.toString(),
            "amount": parseFloat(
              (
                parseFloat(payoutCardAmount) +
                parseFloat(toleranceAmount) -
                parseFloat(payoutCashAmount)
              ).toFixed(2)
            ),
            "previousBalance": parseFloat(professional.wallet.availableAmount),
            "currentBalance": parseFloat(finalAmount.toFixed(2)),
          });
          await this.adapter.model.updateOne(
            {
              "_id": billingData._id,
            },
            { "$push": { "professionalEarnings": earningData } }
          );
          next();
        } else {
          next();
        }
      });
    }
  };
  if (diffTime > billingcycle) {
    billingCycleFunction();
  } else {
    if (diffMin < 60 * 1000) {
      billingCycleFunction();
    }
  }
};

billingsEvent.fetchBillingCycle = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "type": constantUtil.NORMAL,
    },
    { "billingcycle": 1 }
  );
  if (!responseJson && responseJson.length > 0) return;
  return responseJson;
};

billingsEvent.fetchCoorperateBillingCycle = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      "type": constantUtil.COORPERATEOFFICE,
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "billingcycle": 1 }
  );
  if (!responseJson && responseJson.length > 0) return;
  return responseJson;
};

billingsEvent.getBillingIds = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "professionalEarnings.professional": mongoose.Types.ObjectId(
        context.params.professionalId
      ),
    },
    { "_id": 1, "billingcycle": 1, "payoutDate": 1 },
    {
      "sort": context.params.sort,
      "skip": context.params.skip,
      "limit": context.params.limit,
    }
  );
  if (!responseJson && responseJson.length > 0) return;
  return responseJson;
};

billingsEvent.getAdminBillingCycle = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "type": constantUtil.NORMAL,
    },
    { "billingcycle": 1 }
  );
  if (!responseJson && responseJson.length > 0) return;
  return responseJson;
};

billingsEvent.getAdminCoorperateBillingCycle = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      "type": constantUtil.COORPERATEOFFICE,
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "billingcycle": 1 }
  );
  if (!responseJson && responseJson.length > 0) return;
  return responseJson;
};

billingsEvent.saveCoorperateBillingCycleCronJob = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const earningData = {};
  const currentDate = new Date();
  const currentTime = Date.now();
  const startTime = parseInt(generalSettings.data.lastCoorperateBilledTime);
  const billingCycleFunction = async () => {
    const billingData = await this.adapter.insert({
      "startDate": new Date(generalSettings.data.lastCoorperateBilledDate),
      "endDate": currentDate,
      "startTime": startTime,
      "endTime": currentTime,
      "payoutDate": currentDate,
      "billingcycle": (
        new Date(
          generalSettings.data.lastCoorperateBilledDate
        ).toLocaleDateString("en-GB") +
        " - " +
        currentDate.toLocaleDateString("en-GB")
      ).toString(),
      "status": constantUtil.ACTIVE,
      "type": constantUtil.COORPERATEOFFICE,
    });
    this.broker.emit("admin.updateSettingCoorperateBillingTime", {
      "clientId": context.params?.clientId,
      "lastCoorperateBilledDate": currentDate,
      "lastCoorperateBilledTime": currentTime,
    });
    let offices = await this.broker.emit("admin.getAllCoorperateOffices", {
      "clientId": context.params?.clientId,
    });
    offices = offices[0];

    if (offices && offices.length > 0) {
      each(offices, async (office, next) => {
        const urlName = `${billingData._id.toString()}${office._id.toString()}`;
        earningData["office"] = office._id.toString();
        earningData["rideLists"] = [];
        earningData["paidStatus"] = false;
        earningData["invoiceUrl"] = null;
        let bookings = await this.broker.emit(
          "booking.getAllCoorperateBillingBookings",
          {
            "clientId": context.params?.clientId,
            "startDate": new Date(
              generalSettings.data.lastCoorperateBilledDate
            ),
            "endDate": currentDate,
            "office": office._id.toString(),
          }
        );
        bookings = bookings[0];
        if (bookings && bookings.length > 0) {
          await bookings.forEach(async (c) => {
            earningData["rideLists"].push(c._id.toString());
            await this.broker.emit("booking.updateBillingId", {
              "clientId": context.params?.clientId,
              "billingId": billingData._id.toString(),
              "bookingId": c._id.toString(),
            });
          });
          await this.broker.emit("booking.generateInvoiceMail", {
            "clientId": context.params?.clientId,
            "billingId": billingData._id.toString(),
            "rideLists": earningData["rideLists"],
            "fileName": urlName,
            "office": office,
          });
          await this.adapter.model.updateOne(
            {
              "_id": billingData._id,
            },
            { "$push": { "coorperateEarnings": earningData } }
          );
          await this.broker.emit("admin.updateCoorperateBillData", {
            "clientId": context.params?.clientId,
            "billingId": billingData._id.toString(),
            "paidStatus": false,
            "id": office._id.toString(),
          });
          next();
        } else {
          next();
        }
      });
    }
  };
  billingCycleFunction();
};

billingsEvent.getCoorperateOfficeBillingList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "coorperateEarnings.office": mongoose.Types.ObjectId(
      context.params.id.toString()
    ),
  });
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "startDate": 1,
            "endDate": 1,
            "payoutDate": 1,
            "status": 1,
            "type": 1,
            "startTime": 1,
            "endTime": 1,
            "billingcycle": 1,
            "coorperateEarnings": 1,
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query);
  let result = [];
  if (jsonData[0].response && jsonData[0].response.length > 0) {
    result = await jsonData[0].response.map((data) => {
      let dataSet = {};
      data.coorperateEarnings.map((cat) => {
        if (cat.office.toString() === context.params.id.toString()) {
          dataSet = {
            "_id": data._id,
            "billedDate": data.payoutDate,
            "ridesCount": cat.rideLists.length,
            "ridesList": cat.rideLists,
            "invoiceUrl": cat.invoiceUrl,
            "paidStatus": cat.paidStatus,
            "paidDate": cat.paidDate || "",
            "paidNotes": cat.comments || "",
          };
        }
      });
      return dataSet;
    });
  }
  responseJson["count"] = jsonData?.[0]?.all?.[0]?.all || 0;
  responseJson["response"] =
    jsonData[0].response && jsonData[0].response.length > 0 ? result : [];
  return responseJson;
};

billingsEvent.changeCoorperatePaidStatus = async function (context) {
  const updateData =
    context.params.status === true
      ? {
          "coorperateEarnings.$[list].paidStatus": context.params.status,
          "coorperateEarnings.$[list].comments": context.params.comments,
          "coorperateEarnings.$[list].paidDate": new Date(),
        }
      : {
          "coorperateEarnings.$[list].paidStatus": context.params.status,
          "coorperateEarnings.$[list].comments": context.params.comments,
          "coorperateEarnings.$[list].paidDate": null,
        };
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.billingId.toString()),
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "coorperateEarnings": {
        "$elemMatch": {
          "office": mongoose.Types.ObjectId(
            context.params.coorperateId.toString()
          ),
        },
      },
    },
    {
      "$set": updateData,
    },
    {
      "arrayFilters": [
        {
          "list.office": mongoose.Types.ObjectId(
            context.params.coorperateId.toString()
          ),
        },
      ],
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return {
      "code": 200,
      "response": {},
      "message": "STATUS CHANGED SUCCESSFULLY",
    };
  }
};

billingsEvent.updatePdfImage = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.billingId.toString()),
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "coorperateEarnings": {
        "$elemMatch": {
          "office": mongoose.Types.ObjectId(
            context.params.coorperateId.toString()
          ),
        },
      },
    },
    {
      "$set": {
        "coorperateEarnings.$[list].invoiceUrl": context.params.invoiceUrl,
      },
    },
    {
      "arrayFilters": [
        {
          "list.office": mongoose.Types.ObjectId(
            context.params.coorperateId.toString()
          ),
        },
      ],
    }
  );
  if (!responseJson.nModified) {
    return null;
  } else {
    return {
      "code": 200,
      "response": {},
      "message": "IMAGE UPLOAD CHANGED SUCCESSFULLY",
    };
  }
};
//----------------------------------------
module.exports = billingsEvent;
