const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const billingsSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "startDate": { "type": Date, "default": null, "required": true },
    "endDate": { "type": Date, "default": null, "required": true },
    "startTime": { "type": Number, "required": true },
    "endTime": { "type": Number, "required": true },
    "billingcycle": { "type": String, "required": true, "trim": true },
    "payoutDate": { "type": Date, "default": null, "required": true },
    "professionalEarnings": [
      {
        "paidStatus": { "type": Boolean, "default": false, "required": false },
        "professional": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "required": false,
        },
        "rideLists": {
          "type": Array,
          "required": false,
        },
      },
    ],
    "coorperateEarnings": [
      {
        "paidStatus": { "type": Boolean, "default": false, "required": false },
        "comments": { "type": String, "default": null, "required": false },
        "paidDate": { "type": Date, "default": null, "required": false },
        "invoiceUrl": { "type": String, "default": null, "required": false },
        "office": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
          "required": false,
        },
        "rideLists": {
          "type": Array,
          "required": false,
        },
      },
    ],
    "status": {
      /* ACTIVE, INACTIVE */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.ACTIVE, constantUtil.INACTIVE],
      "default": constantUtil.INACTIVE,
    },
    "type": {
      /* ACTIVE, INACTIVE */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.NORMAL, constantUtil.COORPERATEOFFICE],
      "default": null,
    },
  },
  { "timestamps": true, "versionKey": false }
);

const billings = mongoose.model("billings", billingsSchema);

module.exports = billings;
