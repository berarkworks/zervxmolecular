/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");

const eventWatchSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "eventName": { "type": String, "required": false, "trim": true },
    "eventLink": { "type": String, "required": false },
    "description": { "type": String, "required": false, "trim": true },
    "eventLocation": {
      "addressName": { "type": String, "required": false, "trim": true },
      "fullAddress": { "type": String, "required": false, "trim": true },
      "shortAddress": { "type": String, "required": false, "trim": true },
      "lat": { "type": Number, "required": true },
      "lng": { "type": Number, "required": true },
    },
    "user": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "required": true,
    },
    "eventDate": { "type": Date, "required": true },
    "participants": [
      {
        "_id": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAUSERS,
          "required": true,
        },
        "status": {
          "type": String,
          "required": true,
          "trim": true,
          "enum": [
            constantUtil.NOTASSIGNED,
            constantUtil.NOTINTERESTED,
            constantUtil.INTERESTED,
            constantUtil.MAYBE,
            constantUtil.STARTED,
            constantUtil.REACHED,
          ],
          "default": constantUtil.NOTASSIGNED,
        },
      },
    ],
    "eventType": {
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.PRIVATE, constantUtil.PUBLIC],
      "default": constantUtil.PUBLIC,
    },
    "eventStatus": {
      "type": String,
      "required": true,
      "trim": true,
      "enum": [
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.STARTED,
        constantUtil.ENDED,
      ],
      "default": constantUtil.ACTIVE,
    },
    "status": {
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.ACTIVE, constantUtil.INACTIVE],
      "default": constantUtil.ACTIVE,
    },
  },
  { "timestamps": true, "versionKey": false }
);

const eventWatch = mongoose.model("eventWatch", eventWatchSchema);

module.exports = eventWatch;
