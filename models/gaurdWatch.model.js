/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");

const gaurdWatchSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "locationName": { "type": String, "required": true },
    "qrImage": { "type": String, "required": true },
    // 'locationDescription': { 'type': String, 'required': true },
    "locationAddress": {
      "placeId": { "type": String, "required": true },
      "addressName": { "type": String, "required": true },
      "fullAddress": { "type": String, "required": true },
      "shortAddress": { "type": String, "required": true },
      "lat": { "type": Number, "required": true },
      "lng": { "type": Number, "required": true },
    },
    "owner": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "required": true,
    },
    "locationStatus": {
      "type": String,
      "required": true,
      "trim": true,
      "enum": [
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.ARCHIVE,
      ],
      "default": constantUtil.ACTIVE,
    },
    "gaurd": {
      "type": Object,
      "required": true,
      "default": {},
      /* CHECK ON VALIDATION JS FOR GAURDWATCH DATA SCHEMA */
    },
  },
  { "timestamps": true, "versionKey": false }
);

const gaurdWatch = mongoose.model("gaurdWatch", gaurdWatchSchema);

module.exports = gaurdWatch;
