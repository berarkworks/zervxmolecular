/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": {
    "type": String,
    "enum": ["Point"],
  },
  "coordinates": {
    "type": [Number],
  },
});

const userSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "isNewUser": { "type": Boolean, "default": true },
    "uniqueCode": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "referredBy": {
      "type": String,
      "optional": true,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "firstName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "lastName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "email": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
      // "index": {
      //   // "unique": true,
      //   "partialFilterExpression": { "email": { "$type": "string" } },
      // },
    },
    "isEmailVerified": { "type": Boolean, "convert": true, "default": false },
    "emailOTP": { "type": String, "trim": true },
    "gender": { "type": String, "trim": true, "default": null },
    "phone": {
      "code": { "type": String, "required": true, "trim": true },
      "number": { "type": String, "required": true, "trim": true },
    },
    "nationalIdNo": { "type": String, "trim": true, "default": null },
    "otp": { "type": String, "trim": true },
    "dob": { "type": String, "trim": true },
    "ipAddress": { "type": String, "trim": true },
    "password": { "type": String, "default": "" },
    "avatar": {
      "type": String,
      "default": constantUtil.DEFAULTAVATAR,
      "trim": true,
    },
    "languageCode": {
      "type": String,
      "default": constantUtil.DEFAULT_LANGUAGE,
      "trim": true,
    },
    "currencyCode": { "type": String, "default": "USD" },
    "lastCurrencyCode": { "type": String, "default": "USD" },
    "serviceAreaId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMACATEGORIES,
      "alias": "serviceCategory",
    },
    "addressList": [
      {
        "placeId": { "type": String },
        "addressType": { "type": String, "required": true },
        "addressName": { "type": String, "required": true, "lowercase": true },
        "fullAddress": { "type": String, "required": true },
        "shortAddress": { "type": String, "required": true },
        "lat": { "type": Number, "required": true },
        "lng": { "type": Number, "required": true },
      },
    ],
    "bookingInfo": {
      "ongoingBooking": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "trim": true,
        "default": null,
      },
      "pendingReview": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "trim": true,
        "default": null,
      },
      "scheduledBooking": [
        {
          "bookingId": {
            "type": mongoose.Types.ObjectId,
            "ref": constantUtil.DBSCHEMABOOKINGS,
            "trim": true,
          },
          "bookingDate": { "type": Date, "trim": true },
          "estimationTime": { "type": Number, "trim": true },
        },
      ],
    },
    "escortInfo": {
      "ongoingBooking": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMASECURITYESCORT,
        "trim": true,
        "default": null,
      },
      "pendingReview": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMASECURITYESCORT,
        "trim": true,
        "default": null,
      },
      "type": { "type": String, "default": "" },
      "isOfficerAssigned": { "type": Boolean, "default": true },
      "isEscortClosed": {
        "type": Boolean,
        "default": true,
      },
    },
    "review": {
      "avgRating": { "type": Number, "default": 0 },
      "totalReview": { "type": Number, "default": 0 },
    },
    "escortReview": {
      "avgRating": { "type": Number, "default": 0 },
      "totalReview": { "type": Number, "default": 0 },
    },
    "status": {
      /* INCOMPLETE, ACTIVE, INACTIVE, ARCHIVE */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [
        constantUtil.INCOMPLETE,
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.ARCHIVE,
      ],
      "default": constantUtil.INCOMPLETE,
    },
    "notes": { "type": String, "trim": true, "default": null },
    "cards": [
      {
        "type": { "type": String, "default": undefined },
        "stripeCardToken": { "type": String, "default": undefined },
        "cardId": { "type": String, "trim": true, "default": null },
        "cardNumber": { "type": String, "trim": true, "lowercase": true },
        "expiryYear": { "type": String, "trim": true, "lowercase": true },
        "expiryMonth": { "type": String, "trim": true, "lowercase": true },
        "holderName": { "type": String, "trim": true, "lowercase": true },
        "embedtoken": { "type": String, "default": undefined },
        "isVerified": { "type": Boolean, "required": true },
        "isDefault": { "type": Boolean, "default": false },
        "isCVVRequired": { "type": Boolean, "default": false }, //needed when create charge using token
        "securityCode": { "type": String, "default": undefined },
      },
    ],
    "defaultPayment": { "type": String, "default": null },
    "appliedCoupon": { "type": String, "default": null },
    "scanAndPayQR": { "type": String, "default": null },
    "stripeCustomerId": { "type": String, "default": null },
    "stripeConnectAccountId": { "type": String, "default": null },
    "wallet": {
      "availableAmount": { "type": Number, "default": 0, "convert": true },
      "freezedAmount": { "type": Number, "default": 0, "convert": true },
      "dueAmount": { "type": Number, "default": 0, "convert": true },
      "scheduleFreezedAmount": {
        "type": Number,
        "default": 0,
        "convert": true,
      },
      "rewardPoints": {
        "type": Number,
        "default": 0,
        "optional": true,
        "convert": true,
      },
    },
    "trustedContacts": [
      {
        "userId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAUSERS,
          "trim": true,
          "default": null,
        },
        "name": { "type": String, "trim": true },
        "phone": {
          "code": { "type": String, "trim": true },
          "number": { "type": String, "trim": true },
        },
        "reason": {},
      },
    ],
    // "trustedContactsLog": [
    //   {
    //     "userId": {
    //       "type": mongoose.Types.ObjectId,
    //       "ref": constantUtil.DBSCHEMAUSERS,
    //       "trim": true,
    //       "default": null,
    //     },
    //     "phone": {
    //       "code": { "type": String, "trim": true },
    //       "number": { "type": String, "trim": true },
    //     },
    //     "notifyType": {
    //       "type": String,
    //       "enum": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
    //     },
    //   },
    // ],
    "deviceInfo": [
      {
        "deviceId": { "type": String, "trim": true, "default": "" },
        "socketId": { "type": String, "trim": true, "default": "" },
        "deviceType": { "type": String, "trim": true } /* WEB, ANDROID, IOS */,
        "accessToken": { "type": String, "trim": true },
        "platform": { "type": String, "trim": true },
        "createdAt": { "type": "Date", "default": Date.now() },
      },
    ],
    "location": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "Point",
        "coordinates": [0, 0],
      },
    },
    "currentBearing": { "type": Number },
    "altitude": { "type": Number },
    "horizontalAccuracy": { "type": Number },
    "verticalAccuracy": { "type": Number },
    "speed": { "type": Number },
    "isLocationUpdated": { "type": Boolean, "default": false },
    "locationUpdatedTime": { "type": Date, "default": null },
    "subscriptionInfo": {
      "planId": {
        "type": mongoose.Types.ObjectId,
        "trim": true,
        "default": null,
      },
      "isSubscribed": { "type": Boolean, "default": false },
      "subscribedAt": { "type": Date, "default": null },
      "subscribedEndAt": { "type": Date, "default": null },
      "subscribedAtTimestamp": { "type": Number, "default": null },
      "subscribedEndAtTimestamp": { "type": Number, "default": null },
    },
    "bankDetails": [{}],

    "referrelTotalAmount": { "type": Number },
    "joinAmount": { "type": Number },
    "joinCount": { "type": Number },
    "joinerRideCount": { "type": Number, "convert": true, "default": 0 },
    "inviterRideCount": { "type": Number, "convert": true, "default": 0 },
    "invitationCount": { "type": Number, "convert": true, "default": 0 },
    "isOtpBypass": { "type": Boolean, "default": false },
    "permissionData": {
      "osType": {
        "type": String,
        "trim": true,
        "enum": [constantUtil.IOS, constantUtil.ANDROID],
      },
      "brand": { "type": String, "min": 1 },
      "model": { "type": String, "min": 1 },
      "osVersion": { "type": String, "min": 1 },
      "screenSize": { "type": String, "min": 1 },
      "internetConnectivityMedium": { "type": String, "min": 1 },
      "token": { "type": String, "min": 1 },
      "fcmId": { "type": String, "min": 1 },
      "deviceLocationStatus": { "type": Boolean, "default": false },
      "appLocationStatus": { "type": Boolean, "default": false },
      "baseUrl": { "type": String, "min": 1 },
      "appVersionInServer": { "type": String, "convert": true },
      "appVersionInStore": { "type": String, "convert": true },
      "deviceTime": { "type": String, "min": 1 },
      "timezoneInNumber": { "type": String, "min": 1 },
      "timezoneInformat": { "type": String, "min": 1 },
      "connectivityMediumName": { "type": String, "min": 1 },
      "socketConnetivity": { "type": Boolean, "default": false },
      "currentTime": { "type": Date, "convert": true },
    },
    // referral detailes
    "referralDetails": {},
    "lastRedeemDate": { "type": Date, "convert": true },
    "isCorporateOfficer": { "type": Boolean, "default": false },
    "inActivatedAt": { "type": Date, "convert": true, "default": null },
    "inActivatedBy": {
      "type": String,
      "trim": true,
      "optional": true,
      "enum": [constantUtil.USER, constantUtil.ADMIN],
    },
    "paymentGateWayCustomerId": { "type": String, "default": null },
    "blockedProfessionalList": { "type": Array, "default": [] },
    "createdFrom": {
      "type": String,
      "enum": [
        constantUtil.CONST_APP,
        constantUtil.IOS,
        constantUtil.ANDROID,
        constantUtil.CONST_WEBAPP,
        constantUtil.ADMIN,
        constantUtil.COORPERATEOFFICE,
      ],
      "default": constantUtil.CONST_APP,
    },
    "createdBy": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "lastWithdrawDate": { "type": Date, "convert": true },
  },
  { "timestamps": true, "versionKey": false }
);
// compound index
userSchema.index({ "phone.code": 1, "phone.number": 1 }, { "unique": true });
userSchema.index(
  { "phone.code": 1, "phone.number": 1, "email": 1 },
  { "unique": true }
);
//---------------------------
const users = mongoose.model("users", userSchema);

module.exports = users;
