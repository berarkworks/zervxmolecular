const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": { "type": String, "enum": ["Point"] },
  "coordinates": { "type": [Number] },
});

const heatmapSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    // "bookingId": {
    //   "type": mongoose.Schema.Types.ObjectId,
    //   "ref": constantUtil.DBSCHEMABOOKINGS,
    //   "trim": true,
    //   "default": null,
    // },
    "bookingDate": { "type": Date, "required": true },
    // "locationTileId": { "type": String, "convert": true },
    "plusCodePrefix": { "type": String, "convert": true },
    "locationName": { "type": String, "convert": true },
    "location": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "Point",
        "coordinates": [0, 0],
      },
    },
    "heatmapId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "default": null,
    },
    "serviceAreaId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMACATEGORIES,
    },
    "userId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "default": null,
    },
    // "bookingStatus": {
    //   "type": String,
    //   "required": true,
    //   "enum": [
    //     constantUtil.AWAITING,
    //     constantUtil.ARRIVED,
    //     constantUtil.ACCEPTED,
    //     constantUtil.REJECTED,
    //     constantUtil.USERDENY,
    //     constantUtil.USERCANCELLED,
    //     constantUtil.PROFESSIONALCANCELLED,
    //     constantUtil.EXPIRED,
    //     constantUtil.STARTED,
    //     constantUtil.ENDED,
    //   ],
    //   "default": constantUtil.AWAITING,
    // },
  },
  { "timestamps": true, "versionKey": false }
);
//---------------------------
const heatmapModel = mongoose.model("heatmap", heatmapSchema);

module.exports = heatmapModel;
