const mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");

const rewardsSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "name": { "type": String },
    "description": { "type": String },
    // "couponId": { "type": mongoose.Schema.Types.ObjectId },
    "couponId": {
      "type": mongoose.Schema.Types.ObjectId,
      "optional": true,
      "ref": constantUtil.DBSCHEMAADMIN,
      // "localField": "couponData",
      "alias": "couponData",
      // "foreignField": "_id",
      // "justOne": true,
    },
    "userType": {
      "type": String,
      "trim": true,
      "required": true,
      "enum": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
    },
    "userId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "optional": true,
      "alias": "userData",
      "foreignField": "_id",
    },
    "professionalId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAPROFESSIONAL,
      "optional": true,
      //"localField": "professionalId",
      "alias": "professionalData",
      "foreignField": "_id",
    },
    "pointsType": {
      "type": String,
      "enum": [constantUtil.FLAT, constantUtil.PERCENTAGE],
    },
    "rewardType": {
      "type": String,
      "optional": true,
      "enum": [constantUtil.POINTS, constantUtil.PARTNERDEALS],
    },
    "transactionType": {
      "type": String,
      "optional": true,
      "enum": [constantUtil.CREDIT, constantUtil.DEBIT],
    },
    "couponCode": { "type": String, "optional": true },
    "rewardPoints": { "type": Number, "optional": true },
    "isCouponScratched": { "type": Boolean, "default": false },
    "couponScratchedDate": { "type": Date },
    // "vaildFrom": { "type": Date, "optional": true },
    // "vaildTo": { "type": Date, "optional": true },
    "expiredDate": { "type": Date, "default": new Date() },
    "isExpired": { "type": Boolean, "default": false },
    "rideId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMABOOKINGS,
      "optional": true,
      "alias": "bookingData",
      // "foreignField": "_id",
    },
    "createdId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "optional": true,
      "alias": "adminData",
      "foreignField": "_id",
    },
    "reason": { "type": String, "optional": true },
    // "bookingDate": {
    //   "type": Date,
    //   "required": true,
    //   "alias": "rideId",
    //   "default": new Date(),
    // },
    // "transactions": {
    //   "type": mongoose.Schema.Types.ObjectId,
    //   "ref": constantUtil.DBSCHEMATRANSACTION,
    // },
    // "transactionDate": {
    //   "type": mongoose.Schema.Types.Date,
    //   "default": undefined,
    // },
  },
  { "timestamps": true, "versionKey": false }
);

// rewardsSchema.virtual("couponData", {
//   "ref": "admins", //constantUtil.DBSCHEMAADMIN,
//   "localField": "couponId",
//   "foreignField": "_id",
//   // "justOne": true,
//   // "getters": true,
// });
// rewardsSchema.set("toObject", { "virtuals": true });
// rewardsSchema.set("toJSON", { "virtuals": true });

const rewardsModel = mongoose.model("rewards", rewardsSchema);

module.exports = rewardsModel;
