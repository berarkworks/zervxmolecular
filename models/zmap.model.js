/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtils = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": {
    "type": String,
    "enum": ["Point"],
  },
  "coordinates": {
    "type": [Number],
  },
});

const zmapSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "data": { "type": Object },
    "countryCode": { "type": String, "required": true },
    "mapType": {
      "type": String,
      "required": true,
      "enum": [
        constantUtils.CONST_GOOGLE,
        constantUtils.CONST_BING,
        constantUtils.CONST_GOOGLE,
        constantUtils.CONST_BING,
        constantUtils.CONST_TOMTOM,
        constantUtils.CONST_GEOAPIFY,
        constantUtils.CONST_LOCATIONIQ,
        constantUtils.CONST_GBETA,
        constantUtils.CONST_MAPBOX,
      ],
    },
    "mapServiceType": {
      "type": String,
      "required": true,
      "enum": [
        constantUtils.CONST_AUTOCOMPLETE,
        constantUtils.CONST_DIRECTION,
        constantUtils.CONST_LOCATION,
        constantUtils.CONST_PLACE,
        constantUtils.CONST_SNAP,
      ],
    },
    "location": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "Point",
        "coordinates": [0, 0],
      },
    },
  },
  { "timestamps": true, "versionKey": false }
);

const zmapModel = mongoose.model("zmap", zmapSchema);

module.exports = zmapModel;
