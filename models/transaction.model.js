/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const transactionSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "bookingId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMABOOKINGS,
      "default": undefined,
    },
    "refBookingId": { "type": String, "default": undefined },
    "transactionAmount": { "type": Number, "required": true, "convert": true },
    "transactionReason": { "type": String },

    "paidTransactionId": { "type": String },
    "from": {
      "userType": {
        "type": String,
        "trim": true,
        "required": true,
        "enum": [
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.ADMIN,
          constantUtil.HUBS,
          constantUtil.COORPERATEOFFICE,
        ],
      },
      "userId": {
        "type": String,
        "trim": true,
        "default": undefined,
      },
      "name": { "type": String, "default": undefined },
      "avatar": { "type": String, "default": undefined },
      "phoneNumber": { "type": String, "default": undefined },
    },
    "to": {
      "userType": {
        "type": String,
        "trim": true,
        "required": true,
        "enum": [
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.ADMIN,
          constantUtil.HUBS,
          constantUtil.COORPERATEOFFICE,
        ],
      },
      "userId": {
        "type": String,
        "trim": true,
        "default": undefined,
      },
      "name": { "type": String, "default": undefined },
      "avatar": { "type": String, "default": undefined },
      "phoneNumber": {
        "type": String,
        "default": undefined,
      },
    },
    "accountsDetail": {
      "type": {
        "accountName": { "type": String, "default": undefined },
        "accountNumber": { "type": String, "default": undefined },
        "bankName": { "type": String, "default": undefined },
        "routingNumber": { "type": String, "default": undefined },
        "branchCode": { "type": String, "default": undefined },
        "bankCode": { "type": String, "default": undefined },
        "country": { "type": String, "default": undefined },
        "currency": { "type": String, "default": undefined },
      },
    },
    "transactionDate": {
      "type": Date,
      "required": true,
      "default": new Date(),
    },
    "paymentType": {
      "type": String,
      "trim": true,
      "required": true,
      "enum": [
        constantUtil.CREDIT,
        constantUtil.DEBIT,
        constantUtil.PAYMENTGATEWAY,
        constantUtil.CASH,
        constantUtil.WALLET,
      ],
    },
    "gatewayName": {
      "type": String,
      "enum": [
        constantUtil.NONE,
        constantUtil.FLUTTERWAVE,
        constantUtil.STRIPE,
        constantUtil.WALLET,
        constantUtil.RAZORPAY,
        constantUtil.PEACH,
        constantUtil.CONST_TIGO,
        constantUtil.CONST_TELEPORT,
        constantUtil.CONST_CYBERSOURCE,
        constantUtil.CONST_ZCREDITAPIARY,
        constantUtil.CONST_MERCADOPAGO,
        constantUtil.CONST_TEYA,
      ],
      "default": undefined,
    },
    "transactionType": {
      "type": String,
      "required": true,
      "enum": [
        constantUtil.WALLETRECHARGE,
        constantUtil.WALLETWITHDRAWAL,
        constantUtil.SENDMONEYTOFRIEND,
        constantUtil.BOOKINGCHARGE,
        constantUtil.ADMINRECHARGE,
        constantUtil.ADMINDEBIT,
        constantUtil.SUBSCRIPTIONCHARGE,
        constantUtil.CANCELLATIONCHARGE,
        constantUtil.PROFESSIONALEARNINGS,
        constantUtil.JOININGCHARGE,
        constantUtil.INVITECHARGE,
        constantUtil.CARDAMOUNTREFUND,
        constantUtil.EXTRAAMOUNTDEBIT,
        constantUtil.PROFESSIONALTOLERENCEAMOUNT,
        constantUtil.VERIFYCARD,
        constantUtil.MEMBERSHIPPLAN,
        constantUtil.REWARDPOINT,
        constantUtil.REDEEMREWARDPOINT,
        constantUtil.REDEEMREWARDAIRTIME,
        constantUtil.BOOKINGCHARGECARD,
        constantUtil.CANCELLATIONCREDIT,
        constantUtil.HUBSEARNINGS,
        constantUtil.CONST_INCENTIVE,
        constantUtil.TIPSCHARGE,
        constantUtil.PENALITY,
      ],
    },
    "transactionId": { "type": String, "required": true },
    "gatewayDocId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "default": undefined,
    },
    "transactionStatus": {
      "type": String,
      "trim": true,
      "enum": [
        constantUtil.SUCCESS,
        constantUtil.FAILED,
        constantUtil.PENDING,
        constantUtil.PROCESSING,
      ],
    },
    "cardDetailes": {
      "cardNumber": { "type": String, "default": undefined },
      "expiryMonth": { "type": Number, "default": undefined },
      "expiryYear": { "type": Number, "default": undefined },
      // 'cvv': { 'type': String, 'default': undefined },
      // 'pin': { 'type': String, 'default': undefined },
      "holderName": { "type": String, "default": undefined },
    },

    "isReimbursement": {
      "type": Boolean,
      "required": false,
      "default": undefined,
    },
    "gatewayResponse": { "type": Object, "default": undefined },
    "refundResponse": { "type": Object, "default": undefined },
    "webhookresponse": { "type": Object, "default": undefined },
    "webhookHitCount": { "type": Number, "default": 0 },
    "payoutOption": {
      // it only for withdrawal
      "type": String,
      "enum": [
        constantUtil.AUTOMATED,
        constantUtil.MANUAL,
        constantUtil.DISPLAYINFO,
      ],
    },
    "membershipPlanId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "default": undefined,
    },
    "paymentIntentId": { "type": String },
    "paymentIntentClientSecret": { "type": String },
    "paymentGateWay": {
      "type": String,
      "trim": true,
      "enum": [
        constantUtil.NONE,
        constantUtil.FLUTTERWAVE,
        constantUtil.STRIPE,
        constantUtil.WALLET,
        constantUtil.RAZORPAY,
        constantUtil.PEACH,
        constantUtil.CONST_TIGO,
        constantUtil.CONST_TELEPORT,
        constantUtil.CONST_CYBERSOURCE,
        constantUtil.CONST_ZCREDITAPIARY,
        constantUtil.CONST_MERCADOPAGO,
        constantUtil.CONST_TEYA,
      ],
      "default": undefined,
    },
    "currentBalance": { "type": Number, "convert": true, "default": undefined },
    "previousBalance": {
      "type": Number,
      "convert": true,
      "default": undefined,
    },
    "adminId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "default": undefined,
    },
  },
  { "timestamps": true, "versionKey": false }
);

const transactions = mongoose.model("transactions", transactionSchema);

module.exports = transactions;
