/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const subscriptionSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "name": { "type": String, "trim": true, "default": null, "required": true },
    "hint": { "type": String, "trim": true, "default": null, "required": true },
    "price": { "type": Number, "required": true },
    "expireDays": { "type": Number, "required": true },
    "services": { "type": Object, "required": true },
    "markAsPopular": { "type": Boolean, "required": true },
    "description": {
      "type": String,
      "trim": true,
      "default": null,
      "required": true,
    },
    "expireInText": {
      "type": String,
      "trim": true,
      "default": null,
      "required": true,
    },
    "currencyCode": {
      "type": String,
      "trim": true,
      "default": null,
      "required": true,
    },
    "currencySymbol": {
      "type": String,
      "trim": true,
      "default": null,
      "required": true,
    },
    "status": {
      /* ACTIVE, INACTIVE */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.ACTIVE, constantUtil.INACTIVE],
      "default": constantUtil.INACTIVE,
    },
  },
  { "timestamps": true, "versionKey": false }
);

const subscription = mongoose.model("subscription", subscriptionSchema);

module.exports = subscription;
