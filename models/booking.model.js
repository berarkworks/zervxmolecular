/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": { "type": String, "enum": ["MultiPoint"] },
  "coordinates": { "type": [[Number]] },
});

const bookingSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": null,
    },
    "bookingId": {
      "type": String,
      "required": true,
      "trim": true,
      "unique": true,
    },
    "bookingOTP": { "type": String, "required": true, "trim": true },
    "bookingType": { "type": String, "required": true, "trim": true },
    "bookingSubType": { "type": String, "required": false, "trim": true },
    "bookingBy": { "type": String, "required": true, "trim": true },
    "bookingFrom": {
      "type": String,
      "enum": [
        constantUtil.CONST_APP,
        constantUtil.IOS,
        constantUtil.ANDROID,
        constantUtil.CONST_WEBAPP,
        constantUtil.ADMIN,
        constantUtil.COORPERATEOFFICE,
      ],
      "default": constantUtil.CONST_APP,
    },
    "tripType": { "type": String, "required": true, "trim": true },
    "rental": {
      "type": {
        "package": { "type": Number, "required": true, "default": null },
      },
    },
    "paymentSection": { "type": String, "required": true, "trim": true },
    "bookingDate": { "type": Date, "required": true },
    "regionalData": {
      "bookingDate": { "type": Date, "required": true },
      "cancelTime": { "type": Date, "default": null },
      "acceptTime": { "type": Date, "default": null },
      "arriveTime": { "type": Date, "default": null },
      "pickUpTime": { "type": Date, "default": null },
      "dropTime": { "type": Date, "default": null },
    },
    "bookingFor": {
      "name": { "type": String, "trim": true, "required": false },
      "phoneCode": { "type": String, "trim": true, "required": true },
      "phoneNumber": { "type": String, "trim": true, "required": true },
    },
    "tipsInfo": {
      "amount": { "type": Number, "default": 0 },
      "status": { "type": Boolean, "default": false },
      "paymentType": { "type": String },
    },
    "admin": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "coorperate": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "user": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "required": false,
      "default": null,
    },
    "isAdminForceAssign": {
      "type": Boolean,
      "default": false,
      "convert": true,
    },
    "isGenderAvailable": { "type": Boolean, "default": false, "convert": true },
    "childseatAvailable": {
      "type": Boolean,
      "default": false,
      "convert": true,
    },
    "handicapAvailable": { "type": Boolean, "default": false, "convert": true },
    "isShareRide": { "type": Boolean, "default": false, "convert": true },
    "isPetAllowed": { "type": Boolean, "default": false, "convert": true },
    "isAcceptOtherCategory": {
      "type": Boolean,
      "default": false,
    },
    "professional": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAPROFESSIONAL,
      "required": false,
      "default": null,
    },
    "billingId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMABILLINGS,
      "required": false,
      "default": null,
    },
    "couponId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "vehicle": {
      "vehicleCategoryId": {
        "type": mongoose.Schema.Types.ObjectId,
        "convert": true,
        "ref": constantUtil.DBSCHEMAADMIN,
        "default": null,
      },
      "vinNumber": { "type": String, "trim": true },
      "type": { "type": String, "trim": true },
      "plateNumber": { "type": String, "trim": true },
      "model": { "type": String, "trim": true },
      "year": { "type": String, "trim": true },
      "color": { "type": String, "trim": true },
      "noOfDoors": { "type": String, "trim": true },
      "noOfSeats": { "type": String, "trim": true },
    },
    "category": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMACATEGORIES, // serviceAreaId
      "required": true,
      "default": null,
    },
    "hub": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "default": null,
    },
    "wishList": [mongoose.Types.ObjectId],
    "origin": {
      "addressName": { "type": String, "required": false, "trim": true },
      "fullAddress": { "type": String, "required": true, "trim": true },
      "shortAddress": { "type": String, "required": true, "trim": true },
      "lat": { "type": Number, "required": true },
      "lng": { "type": Number, "required": true },
    },
    "destination": {
      "addressName": { "type": String, "required": false, "trim": true },
      "fullAddress": { "type": String, "required": true, "trim": true },
      "shortAddress": { "type": String, "required": true, "trim": true },
      "lat": { "type": Number, "required": true, "convert": true },
      "lng": { "type": Number, "required": true, "convert": true },
    },
    "estimation": {
      "distance": { "type": Number, "required": true, "default": 0 },
      "time": { "type": Number, "required": true, "default": 0 },
      "pickupDistance": { "type": Number, "convert": true, "default": 0 },
      "pickupTime": { "type": Number, "convert": true, "default": 0 },
    },
    "waitingTime": {
      "waitingStartTime": { "type": Date, "default": null },
      "waitingEndTime": { "type": Date, "default": null },
    },
    "waitingTimesArray": [
      {
        "waitingStartTime": { "type": Date, "default": null },
        "waitingEndTime": { "type": Date, "default": null },
      },
    ],
    "bookedEstimation": {
      "distance": { "type": Number, "required": false },
      "time": { "type": Number, "required": false },
      "pickupDistance": { "type": Number, "default": 0 },
      "pickupTime": { "type": Number, "default": 0 },
    },
    "currencySymbol": { "type": String, "required": true },
    "currencyCode": { "type": String, "required": true },
    "distanceUnit": { "type": String, "required": true },
    "payment": {
      "option": { "type": String, "trim": true },
      "details": { "type": Object, "default": {} },
      "card": {
        "cardId": { "type": String, "trim": true },
        "type": { "type": String, "trim": true },
        "cardNumber": { "type": String, "trim": true },
        "expiryYear": { "type": String, "trim": true },
        "expiryMonth": { "type": String, "trim": true },
        "holderName": { "type": String, "trim": true },
        "cvv": { "type": String, "trim": true },
      },
      "paid": { "type": Boolean, "required": true },
      "paymentId": { "type": mongoose.Types.ObjectId },
      "isPrepaidRide": { "type": Boolean, "default": false },
    },
    "invoice": {
      "isEnableDynamicTravelCharge": {
        "type": Boolean,
        "required": true,
        "default": false,
      },
      "baseFare": { "type": Number, "required": true, "convert": true },
      "fareSettings": { "type": Object, "required": true },
      // //farePerDistance
      "farePerDistance": { "type": Number, "convert": true },
      // "farePerDistance1": { "type": Number, "required": true },
      // "farePerDistance2": { "type": Number, "required": true },
      // "farePerDistance3": { "type": Number, "required": true },
      // "farePerDistance4": { "type": Number, "required": true },
      // //farePerMinute
      "farePerMinute": { "type": Number, "convert": true },
      // "farePerMinute1": { "type": Number, "required": true },
      // "farePerMinute2": { "type": Number, "required": true },
      // "farePerMinute3": { "type": Number, "required": true },
      // "farePerMinute4": { "type": Number, "required": true },
      // //
      "isPeakTiming": { "type": Boolean, "required": true, "convert": true },
      "isNightTiming": { "type": Boolean, "required": true, "convert": true },
      "peakFareText": { "type": String, "convert": true },
      "peakFare": { "type": Number, "required": true, "convert": true },
      "nightFare": { "type": Number, "required": true, "convert": true },
      "multiStopChargeList": [
        {
          "amount": { "type": Number, "default": 0, "convert": true },
          "stopedLatLng": { "type": Object, "default": { "lat": 0, "lng": 0 } },
          "stopedTime": { "type": Date, "default": new Date() },
        },
      ],
      "stopCharge": { "type": Number, "default": 0, "convert": true }, //Per Stop
      "multiStopCharge": { "type": Number, "default": 0, "convert": true }, // Total Stop Charge
      "actualPeakFare": { "type": Number, "required": true, "convert": true },
      "heatmapPeakFare": { "type": Number, "required": true, "convert": true },

      // "sharePercent": { "type": Number, "required": true, "convert": true },
      "cancellationStatus": {
        "type": Boolean,
        "required": true,
        "convert": true,
      },
      "cancellationAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "cancellationMinute": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "professionalCancellationStatus": {
        "type": Boolean,
        "required": true,
        "convert": true,
      },
      "professionalCancellationAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "professionalCancellationMinute": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "minimumChargeStatus": {
        "type": Boolean,
        "required": true,
        "convert": true,
      },
      "minimumChargeAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "isMinimumChargeApplied": {
        "type": Boolean,
        "default": false,
        "convert": true,
      },
      // for sur charge
      "surchargeFee": { "type": Number, "default": 0, "convert": true },
      "isSurchargeEnable": {
        "type": Boolean,
        "default": false,
        "convert": true,
      },
      "surchargeTitle": { "type": String, "default": "" },
      //
      "estimationAmount": { "type": Number, "required": true, "convert": true },
      "actualEstimationAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "couponApplied": { "type": Boolean, "required": true, "convert": true },
      "isProfessionalTolerance": {
        "type": "boolean",
        "optional": true,
        "convert": true,
      },
      "couponValue": { "type": Number, "required": true, "convert": true },
      "couponType": { "type": String, "minLength": 0, "default": "" },
      "couponAmount": { "type": Number, "required": true, "convert": true },
      "serviceTaxPercentage": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "serviceTax": { "type": Number, "required": true, "convert": true },
      "travelCharge": { "type": Number, "required": true, "convert": true },
      "distanceFare": { "type": Number, "required": true, "convert": true },
      "timeFare": { "type": Number, "required": true, "convert": true },
      "tipsAmount": { "type": Number, "required": true, "convert": true },
      "estimationPayableAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "payableAmount": { "type": Number, "required": true, "convert": true },
      "professionalCommision": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "professionalCommisionWithoutTips": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "siteCommissionPercentage": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "siteCommission": { "type": Number, "required": true, "convert": true },
      "siteCommissionWithoutTax": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "amountInDriver": { "type": Number, "required": true, "convert": true },
      "amountInSite": { "type": Number, "required": true, "convert": true },
      "peakFareCharge": { "type": Number, "required": true, "convert": true },
      "nightFareCharge": { "type": Number, "required": true, "convert": true },
      "waitingCharge": { "type": Number, "required": true, "convert": true },
      "tollFareAmount": { "type": Number, "required": true, "convert": true },
      "airportFareAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "professionalTolerenceAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "pendingPaymentAmount": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      //----- before/after Ride Waiting Charge Start ----
      "isBeforeRideWaitingCharge": {
        "type": Boolean,
        "required": true,
        "convert": true,
      },
      "beforeRideWaitingChargePerMin": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "beforeRideWaitingMins": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "beforeRideWaitingGracePeriod": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "beforeRideWaitingCharge": {
        "type": Number,
        "required": true,
        "default": 0,
      },
      "isAfterRideWaitingCharge": {
        "type": Boolean,
        "required": true,
        "convert": true,
      },
      "afterRideWaitingChargePerMin": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "afterRideWaitingMins": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "afterRideWaitingGracePeriod": {
        "type": Number,
        "required": true,
        "convert": true,
      },
      "afterRideWaitingCharge": {
        "type": Number,
        "required": true,
        "default": 0,
        "convert": true,
      },
      //----- before/after Ride Waiting Charge End ----
      "isFixedPayableAmount": {
        "type": Boolean,
        "convert": true,
        "default": false,
      },
      //----- InterCity Start ----
      "intercityBoundaryFareType": {
        "type": String,
        "enum": [constantUtil.FLAT, constantUtil.PERCENTAGE, constantUtil.NONE],
        "default": "",
      },
      "isEnableRoundTrip": { "type": Boolean, "default": false },
      "intercityPickupTimeFare": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityPickupDistanceFare": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityBoundaryFare": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityPickupCharge": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityDropCharge": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityPickupFarePerDistance": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "intercityPickupFarePerMinites": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      //----- InterCity End ----
      "cancellationFeeCreditToUserPrecentage": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "cancellationFeeCreditToProfessionalPrecentage": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "roundingType": {
        "type": String,
        "enum": [constantUtil.CONST_DECIMAL, constantUtil.CONST_INTEGER],
        "default": constantUtil.CONST_INTEGER,
      },
      "roundingFactor": { "type": Number, "convert": true, "default": 0 },
      "roundingAmount": { "type": Number, "convert": true, "default": 0 },
      "hubSiteCommission": { "type": Number, "convert": true, "default": 0 },
      "hubSiteCommissionPercentage": {
        "type": Number,
        "convert": true,
        "default": 0,
      },
      "sharePercentageUser1": { "type": Number, "convert": true, "default": 0 },
      "sharePercentageUser2": { "type": Number, "convert": true, "default": 0 },
      "shareRideAmountUser1": { "type": Number, "convert": true, "default": 0 },
      "shareRideAmountUser2": { "type": Number, "convert": true, "default": 0 },
      "discountAmount": { "type": Number, "convert": true, "default": 0 },
      //Assistance Care
      "isEnableAssistanceCare": { "type": Boolean, "convert": true },
      "beforeRideAssistanceCareGracePeriod": {
        "type": Number,
        "convert": true,
      },
      "beforeRideAssistanceCareNormalChargePerMin": {
        "type": Number,
        "convert": true,
      },
      "beforeRideAssistanceCareRevisedChargePerMin": {
        "type": Number,
        "convert": true,
      },
      "beforeRideAssistanceCareAmount": {
        "type": Number,
        "convert": true,
      },
      "beforeRideAssistanceCareNeededMins": {
        "type": Number,
        "convert": true,
      },
      "beforeRideAssistanceCareProvidesMins": {
        "type": Number,
        "convert": true,
      },
      "afterRideAssistanceCareGracePeriod": { "type": Number, "convert": true },
      "afterRideAssistanceCareNormalChargePerMin": {
        "type": Number,
        "convert": true,
      },
      "afterRideAssistanceCareRevisedChargePerMin": {
        "type": Number,
        "convert": true,
      },
      "afterRideAssistanceCareAmount": {
        "type": Number,
        "convert": true,
      },
      "afterRideAssistanceCareNeededMins": {
        "type": Number,
        "convert": true,
      },
      "afterRideAssistanceCareProvidesMins": {
        "type": Number,
        "convert": true,
      },
      "assistanceCareAmount": { "type": Number, "convert": true },
      "assistanceCareNotes": { "type": String, "default": "" },
      "isEnableAddOn": { "type": Boolean, "convert": true, "default": false },
      "addOnDetails": [
        {
          "addOnId": {
            "type": mongoose.Types.ObjectId,
            "ref": constantUtil.DBSCHEMAADMIN,
            "convert": true,
          },
          "count": { "type": Number, "convert": true },
          "amount": { "type": Number, "convert": true },
          "total": { "type": Number, "convert": true },
        },
      ],
      "addOnAmount": { "type": Number, "convert": true, "default": 0 },
      // "isEnableAmenity": { "type": Boolean, "default": false },
      // "amenityDetails": {
      //   "amenityId": {
      //     "type": mongoose.Types.ObjectId,
      //     "ref": constantUtil.DBSCHEMAADMIN,
      //     "required": true,
      //     "convert": true,
      //   },
      //   "count": { "type": Number, "convert": true },
      //   "amount": { "type": Number, "convert": true },
      //   "total": { "type": Number, "convert": true },
      // },
    },
    "serviceCategory": {
      "type": String,
      "lowercase": true, // Must be lowercase
      "convert": true,
      "enum": [
        constantUtil.CONST_RIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_CORPORATERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_INTERCITYRIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PICKUPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_DROPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PICKUPANDDROPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PACKAGES.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_SHARERIDE.toLowerCase(), // Must Lowercase Type
      ],
      "default": constantUtil.CONST_RIDE.toLowerCase(), // Must Lowercase Type
    }, // Need to change ("required": true)

    "bookingReview": {
      "isUserReviewed": { "type": Boolean, "convert": true, "default": false },
      "isProfessionalReviewed": { "type": Boolean, "default": false },
      "userReview": {
        "rating": { "type": Number, "trim": true, "default": null },
        "comment": { "type": String, "default": null },
        "commentId": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
          "default": null,
        },
      },
      "professionalReview": {
        "rating": { "type": Number, "trim": true, "default": null },
        "comment": { "type": String, "trim": true, "default": null },
        "commentId": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
          "default": null,
        },
      },
    },
    "security": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMASECURITYESCORT,
      "required": false,
      "default": null,
    },
    "officer": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAOFFICER,
      "required": false,
      "default": null,
    },
    "isSecurityAvail": { "type": Boolean, "default": false },
    // -------------------
    // denyDriverList & denyProfessionals Both are same, denyProfessionals --> used get Report purpose
    "denyDriverList": { "type": Array, "default": [] },
    "denyProfessionals": [
      {
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "required": true,
          "convert": true,
        },
        "requestDenyDateTime": { "type": Date, "default": new Date() },
      },
    ],
    // -------------------
    "cancelledProfessionalList": [
      {
        // "professionalId": { "type": mongoose.Types.ObjectId, "required": true },
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "required": true,
          "convert": true,
        },
        "cancelledTime": { "type": Date, "required": true },
        "cancellationReason": { "type": String, "required": true },
        "cancellationReasonId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
          "required": false,
          "convert": true,
        },
        "cancellationMinute": { "type": String, "required": true },
        "cancellationPenalityStatus": { "type": Boolean, "required": true },
        "cancellationAmount": { "type": Number, "required": true },
        "isHasPenalty": { "type": Boolean, "required": false },
      },
    ],
    "activity": {
      "noOfUsersInVehicle": { "type": String, "trim": true, "default": null },
      "noOfSeatsAvailable": { "type": String, "trim": true, "default": null },
      "isUserPickedUp": { "type": Boolean, "default": false },
      "isUserDropped": { "type": Boolean, "default": false },
      "waitingDuration": { "type": Number, "default": 0 },
      "bookingTime": { "type": Date, "default": null },
      "actualBookingTime": { "type": Date, "default": null },
      "denyOrExpireTime": { "type": Date, "default": null },
      "cancelTime": { "type": Date, "default": null },
      "acceptTime": { "type": Date, "default": null },
      "arriveTime": { "type": Date, "default": null },
      "pickUpTime": { "type": Date, "default": null },
      "dropTime": { "type": Date, "default": null },
      "stopTime": { "type": Array, "default": [] },
      "workedMins": { "type": Number, "default": null },
      "waitingMins": { "type": Number, "default": null },
      "rideStops": [
        {
          "addressName": { "type": String, "required": false, "trim": true },
          "fullAddress": { "type": String, "required": true, "trim": true },
          "shortAddress": { "type": String, "required": true, "trim": true },
          "lat": { "type": Number },
          "lng": { "type": Number },
          "status": { "type": String, "trim": true, "default": null },
          "type": { "type": String, "trim": true, "default": null },
          "locationType": { "type": String, "trim": true, "default": "END" },
        },
      ],
      "rideStopsUpdated": { "type": Boolean, "default": false },
      "rideStopsUpdatedBy": { "type": String, "default": null },
      "rideStopsUpdatedTime": { "type": Date, "default": null },
      "endedBy": { "type": Object, "default": {} },
      "cancelledBy": { "type": Object, "default": {} },
      "acceptLatLng": { "type": Object, "default": { "lat": 0, "lng": 0 } },
      "arriveLatLng": { "type": Object, "default": { "lat": 0, "lng": 0 } },
      "pickUpLatLng": { "type": Object, "default": { "lat": 0, "lng": 0 } },
      "dropLatLng": { "type": Object, "default": { "lat": 0, "lng": 0 } }, //Another type --> "type": { "enum": "Point" }, "default": [0, 0]
    },
    "rideStops": [
      {
        "addressName": { "type": String, "required": false, "trim": true },
        "fullAddress": { "type": String, "required": true, "trim": true },
        "shortAddress": { "type": String, "required": true, "trim": true },
        "lat": { "type": Number },
        "lng": { "type": Number },
        "status": { "type": String, "trim": true, "default": null },
        "type": { "type": String, "trim": true, "default": null },
        "locationType": { "type": String, "trim": true, "default": "BOOKED" },
      },
    ],
    "notes": { "type": String, "trim": true },
    //
    "cancellationReason": { "type": String, "trim": true },
    "cancellationReasonId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "convert": true,
    },
    "cancelReasonDescription": { "type": String },
    "isHasPenalty": { "type": Boolean, "required": false },
    //
    "rideMapRouteImage": { "type": String, "trim": true },
    "encodedPolyline": { "type": Array, "trim": true },
    "bookingStatus": {
      /* AWAITING, ARRIVED, ACCEPTED, REJECTED, CANCELLED, EXPIRED, STARTED, ENDED */
      "type": String,
      "required": true,
      "enum": [
        constantUtil.AWAITING,
        constantUtil.ARRIVED,
        constantUtil.ACCEPTED,
        constantUtil.REJECTED,
        constantUtil.USERDENY,
        constantUtil.USERCANCELLED,
        constantUtil.PROFESSIONALCANCELLED,
        constantUtil.EXPIRED,
        constantUtil.STARTED,
        constantUtil.ENDED,
      ],
      "default": constantUtil.AWAITING,
    },
    "guestType": {
      "type": String,
      "required": true,
      "enum": [
        constantUtil.GUESTUSER,
        constantUtil.USER,
        constantUtil.PROFESSIONALGUEST,
        constantUtil.BOOKINGSERVICEMANAGEMET,
      ],
      "default": constantUtil.USER,
    },
    "isCancelToNearbyProfessionals": { "type": Boolean, "default": false },
    "mapErrorData": { "type": Object, "default": {} },
    "snapErrorData": { "type": Object, "default": {} },
    "pickupWayData": { "type": Array, "default": [] },
    "wayData": { "type": Array, "default": [] },
    "snapWayData": { "type": Array, "default": [] },
    "manualWayData": { "type": Array, "default": [] },
    "finalWayData": { "type": Array, "default": [] },
    "originalWayData": { "type": Array, "default": [] },
    "passedTollList": { "type": Array, "default": [] },
    "totalTollPassed": { "type": Number, "default": null },
    "manualDistance": { "type": Number, "default": null },
    "distanceCalculateType": { "type": String, "trim": true },
    "statusChangedByAdmin": { "type": Boolean },
    "adminChangedStatus": [
      { "type": String, "enum": [constantUtil.ARRIVED, constantUtil.STARTED] },
    ],
    "paymentInitId": { "type": String, "default": null }, // (or) paymentIntentId
    "paymentInitAmount": { "type": Number, "default": 0 }, //(or) paymentIntentAmount

    // for algorithm purpose
    "requestSendProfessionals": [
      {
        // "professionalId": { "type": String, "required": true },
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "required": true,
          "convert": true,
        },
        "requestReceiveDateTime": { "type": Date, "default": null },
        "count": { "type": Number, "required": true },
        "location": {
          "_id": 0,
          "type": PointSchema,
          "index": "2dsphere",
          "default": {
            "type": "MultiPoint",
            "coordinates": [[0, 0]],
          },
        },
        "duration": { "type": Number, "default": 0 },
        "distance": { "type": Number, "default": 0 },
      },
    ],
    "requestRetryCount": { "type": Number, "required": false },
    "isRestrictMaleBookingToFemaleProfessional": {
      "type": Boolean,
      "required": true,
      "default": false,
    },
    "securityImageName": { "type": String },
    "corporateEmailId": { "type": String },
    "meterReadingType": {
      "type": String,
      "optional": true,
      "enum": [
        constantUtil.CONST_LIVEMETER,
        constantUtil.CONST_SNAPWAY,
        constantUtil.CONST_INTERRUPT,
        constantUtil.CONST_TIMEINTERRUPT,
        constantUtil.CONST_DISTANCEINTERRUPT,
        constantUtil.CONST_MANUAL,
      ],
    },
    "liveMeter": {
      "isEnableLiveMeter": { "type": Boolean, "default": false },
      "traveledDistance": {
        "type": Number,
        "default": null,
      },
      "traveledTime": { "type": Number, "default": null },
    },
    "trackData": { "type": "object" },
    // "packageData": { "type": "object" },
    // "packageData": [
    //   {
    //     "_id": {
    //       "type": mongoose.Schema.Types.ObjectId,
    //     },
    //     "documentName": {
    //       "type": String,
    //     },
    //     "documentUrl": {
    //       "type": String,
    //     },
    //     "uploadedStatus": {
    //       "type": String,
    //       "optional": true,
    //       "enum": [constantUtil.UPLOADED],
    //     },
    //     "uploadedBy": {
    //       "type": String,
    //       "optional": true,
    //       "enum": [
    //         constantUtil.USER,
    //         constantUtil.PROFESSIONAL,
    //         constantUtil.ADMIN,
    //       ],
    //     },
    //   },
    // ],
    "packageData": {
      "recipientName": { "type": String },
      "recipientNo": { "type": String },
      "recipientPhoneCode": { "type": String },
      "recipientPhoneNo": { "type": String },
      "isEnableOTPVerification": { "type": Boolean, "convert": true },
      "isEnableNoContactDelivery": { "type": Boolean, "convert": true },
      "isEnablePetsAtMyHome": { "type": Boolean, "convert": true },
      "selectedPackageContent": { "type": Array },
      "instruction": { "type": String },
      "userImage": { "type": "object" },
      "userImageUploadedAt": { "type": Date },
      "professionalImagePickup": { "type": "object" },
      "professionalPickupedAt": { "type": Date },
      "professionalImageDelivery": { "type": "object" },
      "professionaDeliveredAt": { "type": Date },
      "packageReceiver": { "type": "object" },
      "packageReceivedAt": { "type": Date },
    },
    //#region share ride
    "totalPassengerCount": {
      "type": Number,
      "convert": true,
      "required": false,
      "default": 1, // default total Passenger Count must be 1
    },
    "currentPassengerCount": {
      "type": Number,
      "convert": true,
      "required": false,
      "default": 1, // default current Passenger Count must be 1
    },
    "passengerCount": {
      "type": Number,
      "convert": true,
      "required": false,
      "default": 1, // default current Passenger Count must be 1
    },
    "parentShareRideId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMABOOKINGS,
      "trim": true,
      "default": null,
    },
    "parentShareRideProfessional": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAPROFESSIONAL,
      "trim": true,
      "default": null,
    },
    // "location": {
    //   "type": {
    //     "type": {
    //       "type": String,
    //       "enum": ["MultiPoint"],
    //     },
    //     "coordinates": {
    //       "type": [[Number]],
    //     },
    //   },
    //   // "index": "2dsphere",
    //   // "default": {
    //   //   "type": "MultiPoint",
    //   //   "coordinates": [[0, 0]],
    //   // },
    // },
    "rideLocation": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "MultiPoint",
        // "coordinates": [[0, 0]],
        "coordinates": [[0, 0]],
      },
    },
    "pickupLocation": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "MultiPoint",
        // "coordinates": [[0, 0]],
        "coordinates": [[0, 0]],
      },
    },
    "dropLocation": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "MultiPoint",
        // "coordinates": [[0, 0]],
        "coordinates": [[0, 0]],
      },
    },
    "upcomingLocation": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "MultiPoint",
        // "coordinates": [[0, 0]],
        "coordinates": [[0, 0]],
      },
    },
    //#endregion share ride
    "rideCoordinatesEncoded": { "type": String },
    "adminAssignedProfessional": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAPROFESSIONAL,
      "convert": true,
      "default": null,
    },
    "flightNumber": { "type": String, "default": null }, // flightNumber Or vehicleNumber
    "welcomeSignboardText": { "type": String, "default": null },
    "isRoundTrip": { "type": Boolean, "convert": true, "default": false },
    "roundTripDetails": {
      "roundTripId": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "convert": true,
      },
      "roundTripType": {
        "type": String,
        "trim": true,
        "enum": [constantUtil.CONST_PARENT, constantUtil.CONST_CHILD],
      },
    },
    "isHasUSBChargePort": {
      "type": Boolean,
      "convert": true,
      "default": false,
    },
    "isHasWifi": { "type": Boolean, "default": false, "convert": true },
    "isHasAC": { "type": Boolean, "default": false, "convert": true },
    "knownLanguages": { "type": [{ "type": String }] },
    "trustedContactsLog": [
      {
        "userType": {
          "type": String,
          "enum": [constantUtil.USER, constantUtil.PROFESSIONAL],
        },
        "userId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAUSERS,
          "trim": true,
          "default": null,
        },
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "trim": true,
          "default": null,
        },
        "fullName": { "type": String, "trim": true },
        "phone": {
          "code": { "type": String, "trim": true },
          "number": { "type": String, "trim": true },
        },
        "notifyType": {
          "type": String,
          "enum": [
            constantUtil.SMS,
            constantUtil.EMAIL,
            constantUtil.PUSH,
            constantUtil.WHATSUP,
          ],
        },
      },
    ],
  },
  { "timestamps": true, "versionKey": false }
);
// bookingSchema.index({ "location": "2dsphere" });

bookingSchema.index({ "bookingDate": 1 });
bookingSchema.index({
  "admin": 1,
  "bookingId": 1,
  "bookingStatus": 1,
  "bookingType": 1,
});
bookingSchema.index({
  "user": 1,
  "bookingId": 1,
  "bookingStatus": 1,
  "bookingType": 1,
});
bookingSchema.index({
  "professional": 1,
  "bookingId": 1,
  "bookingStatus": 1,
  "bookingType": 1,
});
bookingSchema.index({
  "coorperate": 1,
  "bookingId": 1,
  "bookingStatus": 1,
  "bookingType": 1,
});
bookingSchema.index({
  "serviceCategory": 1,
  "bookingStatus": 1,
  "bookingType": 1,
});

const booking = mongoose.model("bookings", bookingSchema);

module.exports = booking;
