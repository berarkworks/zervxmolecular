/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": {
    "type": String,
    "enum": ["Point"],
  },
  "coordinates": {
    "type": [Number],
  },
});

const officerSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "uniqueCode": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "referredBy": {
      "type": String,
      "optional": true,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "firstName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "lastName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "email": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "index": {
        // "unique": true,
        "partialFilterExpression": { "email": { "$type": "string" } },
      },
      "default": null,
    },
    "isEmailVerified": { "type": Boolean, "convert": true, "default": false },
    "gender": { "type": String, "trim": true, "default": null },
    "dob": { "type": String, "trim": true },
    "age": { "type": Number },
    "phone": {
      "code": {
        "type": String,
        "trim": true,
      },
      "number": {
        "type": String,
        // "unique": true,
        // "required": true,
        "trim": true,
      },
    },
    "officeId": {
      "type": String,
      "trim": true,
    },
    "otp": {
      "type": String,
      "trim": true,
    },
    "avatar": {
      "type": String,
      "default": constantUtil.DEFAULTAVATAR,
      "trim": true,
    },
    "languageCode": {
      "type": String,
      "default": constantUtil.DEFAULT_LANGUAGE,
      "trim": true,
    },
    "currencyCode": {
      "type": String,
      "default": "USD",
      "trim": true,
    },
    "location": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "Point",
        "coordinates": [0, 0],
      },
    },
    "currentBearing": { "type": Number },
    "altitude": { "type": Number },
    "horizontalAccuracy": { "type": Number },
    "verticalAccuracy": { "type": Number },
    "speed": { "type": Number },
    "isLocationUpdated": { "type": Boolean, "default": false },
    "locationUpdatedTime": { "type": Date, "default": new Date() },
    "bookingInfo": {
      "ongoingBooking": {
        "type": mongoose.Types.ObjectId,
        "trim": true,
        "default": null,
      },
      "pendingReview": {
        "type": mongoose.Types.ObjectId,
        "trim": true,
        "default": null,
      },
    },
    "review": {
      "avgRating": { "type": Number, "default": null },
      "totalReview": { "type": Number, "default": null },
    },
    "onlineStatus": { "type": Boolean, "default": false },
    "notes": { "type": String, "default": null },
    "status": {
      /*  ACTIVE, INACTIVE, ARCHIVE */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [
        constantUtil.ACTIVE,
        constantUtil.INACTIVE,
        constantUtil.ARCHIVE,
      ],
      "default": constantUtil.INACTIVE,
    },
    "deviceInfo": [
      {
        "deviceId": {
          "type": String,
          "trim": true,
          "default": "",
        },
        "socketId": {
          "type": String,
          "trim": true,
          "default": "",
        },
        "deviceType": {
          /* WEB, ANDROID, IOS */
          "type": String,
          "trim": true,
        },
        "accessToken": {
          "type": String,
          "trim": true,
        },
        "platform": {
          "type": String,
          "trim": true,
        },
        "createdAt": {
          "type": "Date",
          "default": Date.now(),
        },
      },
    ],
    "permissionData": {
      "osType": {
        "type": String,
        "enum": [constantUtil.IOS, constantUtil.ANDROID],
      },
      "brand": { "type": String, "min": 1 },
      "model": { "type": String, "min": 1 },
      "osVersion": { "type": String, "min": 1 },
      "screenSize": { "type": String, "min": 1 },
      "internetConnectivityMedium": { "type": String, "min": 1 },
      "token": { "type": String, "min": 1 },
      "fcmId": { "type": String, "min": 1 },
      "deviceLocationStatus": { "type": Boolean, "default": false },
      "appLocationStatus": { "type": Boolean, "default": false },
      "baseUrl": { "type": String, "min": 1 },
      "appVersionInServer": { "type": String, "convert": true },
      "appVersionInStore": { "type": String, "convert": true },
      "deviceTime": { "type": String, "min": 1 },
      "timezoneInNumber": { "type": String, "min": 1 },
      "timezoneInformat": { "type": String, "min": 1 },
      "connectivityMediumName": { "type": String, "min": 1 },
      "socketConnetivity": { "type": Boolean, "default": false },
      "currentTime": { "type": Date, "convert": true },
    },
    "userType": {
      "type": String,
      "required": true,
      "trim": true,
      "enum": [constantUtil.CORPORATEOFFICER, constantUtil.RESPONSEOFFICER],
      "default": constantUtil.RESPONSEOFFICE,
    },
    "user": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "required": false,
    },
    "corporate": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "employeeId": { "type": String, "default": null },
    "isActive": { "type": Boolean, "convert": true, "default": false },
    "isDeleted": { "type": Boolean, "convert": true, "default": false },
  },
  { "timestamps": true, "versionKey": false }
);
// compound index
officerSchema.index(
  { "phone.code": 1, "phone.number": 1, "email": 1, "corporate": 1 },
  { "unique": true }
);

const officer = mongoose.model("officers", officerSchema);

module.exports = officer;
