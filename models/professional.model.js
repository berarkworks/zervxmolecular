/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const PointSchema = new mongoose.Schema({
  "type": { "type": String, "enum": ["Point"] },
  "coordinates": { "type": [Number] },
});
const MembershipSchema = new mongoose.Schema({
  "membershipPlanId": {
    "type": mongoose.Schema.Types.ObjectId,
    "ref": constantUtil.DBSCHEMAADMIN,
    "required": true,
  },
  "totalNoOfRides": { "type": Number, "required": true },
  "usedRides": { "type": Number, "required": true },
  "startDate": { "type": mongoose.Schema.Types.Date, "required": true },
  "endDate": { "type": mongoose.Schema.Types.Date, "required": true },
  "activateDate": { "type": mongoose.Schema.Types.Date, "required": true },
  "isPlanExpired": { "type": Boolean, "required": true },
  "transactionId": {
    "type": mongoose.Schema.Types.ObjectId,
    "ref": constantUtil.DBSCHEMATRANSACTION,
  },
  "amount": { "type": Number },
});

const professionalSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "isNewUser": { "type": Boolean, "default": true },
    "uniqueCode": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "firstName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "lastName": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "email": {
      "type": String,
      "trim": true,
      "lowercase": true,
      "default": null,
      // "index": {
      //   // "unique": true,
      //   "partialFilterExpression": { "email": { "$type": "string" } },
      // },
    },
    "isEmailVerified": { "type": Boolean, "convert": true, "default": false },
    "emailOTP": { "type": String, "trim": true },
    "gender": { "type": String, "trim": true, "default": null },
    "dob": { "type": String, "trim": true },
    "age": { "type": Number },
    "tin": { "type": String, "trim": true },
    "address": {
      "line": { "type": String, "trim": true },
      "city": { "type": String, "trim": true },
      "state": { "type": String, "trim": true },
      "country": { "type": String, "trim": true },
      "zipcode": { "type": String },
    },
    "phone": {
      "code": { "type": String, "trim": true },
      "number": {
        "type": String,
        // "unique": true,
        "required": true,
        "trim": true,
      },
    },
    "otpReceiverAppName": {
      "type": String,
      "values": [constantUtil.WHATSUP, constantUtil.SMS],
      "optional": true,
      // "default": constantUtil.SMS,
    },
    "nationalIdNo": { "type": String, "trim": true, "default": null },
    "nationalInsuranceNo": { "type": String, "trim": true, "default": null },
    "otp": { "type": String, "trim": true },
    "ipAddress": { "type": String, "trim": true },
    "password": { "type": String, "trim": true, "password": "" },
    "avatar": {
      "type": String,
      "default": constantUtil.DEFAULTAVATAR,
      "trim": true,
    },
    "languageCode": {
      "type": String,
      "default": constantUtil.DEFAULT_LANGUAGE,
      "trim": true,
    },
    "currencyCode": { "type": String, "default": "USD" },
    "lastCurrencyCode": { "type": String, "default": "USD" },
    "wallet": {
      "availableAmount": { "type": Number, "default": 0, "convert": true },
      "freezedAmount": { "type": Number, "default": 0, "convert": true },
      "dueAmount": { "type": Number, "default": 0, "convert": true },
      "rewardPoints": {
        "type": Number,
        "default": 0,
        "optional": true,
        "convert": true,
      },
    },
    "bankDetails": { "type": Object, "required": true, "default": {} },
    "location": {
      "_id": 0,
      "type": PointSchema,
      "index": "2dsphere",
      "default": {
        "type": "Point",
        "coordinates": [0, 0],
      },
    },
    "currentBearing": { "type": Number },
    "altitude": { "type": Number },
    "horizontalAccuracy": { "type": Number },
    "verticalAccuracy": { "type": Number },
    "speed": { "type": Number },
    "isLocationUpdated": { "type": Boolean, "default": false },
    "locationUpdatedTime": { "type": Date, "default": null },
    "stripeCustomerId": { "type": String, "default": null },
    "stripeConnectAccountId": { "type": String, "default": null },
    "profileVerification": [
      {
        "documentId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
          "default": null,
        }, // Need to Work Based on documentId, After Jan 2027 (In CronJob & other Functions)
        "documentName": { "type": String },
        "documents": [{ "type": String, "default": [] }],
        "expiryDate": { "type": String },
        "reason": { "type": String, "default": null },
        "isMandatory": { "type": Boolean, "default": true, "convert": true },
        "status": { "type": String, "default": constantUtil.UNVERIFIED },
        "uploadedStatus": { "type": String, "default": constantUtil.UPLOADED },
      },
    ],
    // "defaultVehicle": { "type": Object }, // "defaultVehicle": { "type": Array, "default": [] },
    "vehicles": [
      {
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
        }, // Need to Check
        "vehicleCategoryId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
        },
        "vinNumber": { "type": String },
        "type": { "type": String },
        "serviceCategory": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMACATEGORIES,
        },
        "serviceTypeId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
        }, //**Important** --> For Vehicle Documents Verification (Key field --> vehicles.serviceTypeId --> Cron Job Update)
        "serviceType": {
          "type": String,
          "trim": true,
          "optional": true,
          "enum": [
            constantUtil.ALL,
            constantUtil.CONST_RIDE,
            constantUtil.CONST_DELIVERY,
          ],
          "default": constantUtil.CONST_RIDE,
        }, //**Important** --> For Vehicle Documents Verification (Key field --> vehicles.serviceTypeId --> Cron Job Update)
        "plateNumber": { "type": String },
        "maker": { "type": String },
        "model": { "type": String },
        "year": { "type": String },
        "notes": { "type": String },
        "color": { "type": String },
        "noOfDoors": { "type": String },
        "noOfSeats": { "type": String },
        "frontImage": { "type": String },
        "backImage": { "type": String },
        "leftImage": { "type": String },
        "rightImage": { "type": String },
        "vehicleDocuments": [
          {
            "_id": 0,
            "documentId": {
              "type": mongoose.Types.ObjectId,
              "ref": constantUtil.DBSCHEMAADMIN,
              "default": null,
            }, // Need to Work Based on documentId, After Jan 2027 (In CronJob & other Functions)
            "documentName": { "type": String },
            "documents": [{ "type": String, "default": [] }],
            "expiryDate": { "type": String },
            "reason": { "type": String, "default": null },
            "isMandatory": {
              "type": Boolean,
              "default": true,
              "convert": true,
            },
            "status": { "type": String, "default": constantUtil.UNVERIFIED },
            "uploadedStatus": {
              "type": String,
              "default": constantUtil.UPLOADED,
            },
          },
        ],
        "hub": {
          "type": mongoose.Schema.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAADMIN,
        },
        "scheduleDate": { "type": Date, "default": "" },
        "defaultVehicle": { "type": Boolean, "convert": true },
        "isSubCategoryAvailable": { "type": Boolean, "convert": true },
        "applySubCategory": { "type": Boolean, "default": false },
        "handicapAvailable": { "type": Boolean, "default": false },
        "childseatAvailable": { "type": Boolean, "default": false },
        "isCompanyVehicle": { "type": Boolean, "default": false },
        "isForceAppliedToProfessional": { "type": Boolean, "convert": true },
        "isSharerideForceAppliedToProfessional": {
          "type": Boolean,
          "default": false,
        },
        "isSupportShareRide": { "type": Boolean, "default": false },
        "isRentalSupported": {
          "type": Boolean,
          "required": true,
          "default": false,
        },
        "subCategoryIds": [],
        "status": { "type": String, "default": constantUtil.UNVERIFIED },
        "isEnableShareRide": {
          "type": Boolean,
          "default": false,
          "convert": true,
        },
        "isHasUSBChargePort": { "type": Boolean, "default": false },
        "isHasWifi": { "type": Boolean, "default": false, "convert": true },
        "isPetAllowed": { "type": Boolean, "default": false, "convert": true },
        "isEnableLuggage": {
          "type": Boolean,
          "default": false,
          "convert": true,
        },
        "isHasAC": { "type": Boolean, "default": false, "convert": true },
      },
    ],
    "knownLanguages": { "type": [{ "type": String }] },
    "cards": [
      {
        "type": { "type": String, "default": undefined },
        "stripeCardToken": { "type": String, "default": undefined },
        "cardId": { "type": String, "trim": true, "default": null },
        "cardNumber": { "type": String, "trim": true, "lowercase": true },
        "expiryYear": { "type": String, "trim": true, "lowercase": true },
        "expiryMonth": { "type": String, "trim": true, "lowercase": true },
        "holderName": { "type": String, "trim": true, "lowercase": true },
        "embedtoken": { "type": String, "default": undefined },
        "isVerified": { "type": Boolean, "required": true },
        "isDefault": { "type": Boolean, "default": false },
        "isCVVRequired": { "type": Boolean, "default": false }, //needed when create charge using token
        "securityCode": { "type": String, "default": undefined },
      },
    ],
    "bookingInfo": {
      "ongoingBooking": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "trim": true,
        "default": null,
      },
      "ongoingTailBooking": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "trim": true,
        "default": null,
      },
      "pendingReview": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMABOOKINGS,
        "trim": true,
        "default": null,
      },
      "upcomingBooking": [
        {
          "bookingId": {
            "type": mongoose.Types.ObjectId,
            "ref": constantUtil.DBSCHEMABOOKINGS,
            "trim": true,
          },
        },
      ],
    },
    "escortInfo": {
      "ongoingBooking": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMASECURITYESCORT,
        "trim": true,
        "default": null,
      },
      "pendingReview": {
        "type": mongoose.Types.ObjectId,
        "ref": constantUtil.DBSCHEMASECURITYESCORT,
        "trim": true,
        "default": null,
      },
      "type": { "type": String, "default": "" },
      "isOfficerAssigned": { "type": Boolean, "default": true },
      "isEscortClosed": { "type": Boolean, "default": false },
    },
    "review": {
      "avgRating": { "type": Number, "default": 0 },
      "totalReview": { "type": Number, "default": 0 },
    },
    "escortReview": {
      "avgRating": { "type": Number, "default": 0 },
      "totalReview": { "type": Number, "default": 0 },
    },
    "onlineStatus": { "type": Boolean, "default": false },
    "onlineStatusUpdatedAt": { "type": Date, "default": null },
    "onlineStatusBy": {
      "type": String,
      "optional": true,
      "trim": true,
      "enum": [constantUtil.AUTOMATED, constantUtil.CONST_MANUAL],
      "default": constantUtil.AUTOMATED,
    },
    "isExpired": { "type": Boolean, "default": false },
    "notes": { "type": String, "default": null },
    "status": {
      /*INCOMPLETE, ATTENDED, UNVERIFIED, ACTIVE, OFFLINE, BLOCKED */
      "type": String,
      "required": true,
      "trim": true,
      "enum": [
        constantUtil.INCOMPLETE,
        constantUtil.UPLOADED,
        constantUtil.UNVERIFIED,
        constantUtil.ACTIVE,
        constantUtil.OFFLINE,
        constantUtil.INACTIVE,
        constantUtil.ARCHIVE,
        constantUtil.BLOCKED,
      ],
      "default": constantUtil.INCOMPLETE,
    },
    "trustedContacts": [
      {
        "professionalId": {
          "type": mongoose.Types.ObjectId,
          "ref": constantUtil.DBSCHEMAPROFESSIONAL,
          "trim": true,
          "default": null,
        },
        "name": { "type": String, "trim": true },
        "phone": {
          "code": { "type": String, "trim": true },
          "number": { "type": String, "trim": true },
        },
        "reason": {},
      },
    ],
    // "trustedContactsLog": [
    //   {
    //     "professionalId": {
    //       "type": mongoose.Types.ObjectId,
    //       "ref": constantUtil.DBSCHEMAPROFESSIONAL,
    //       "trim": true,
    //       "default": null,
    //     },
    //     "phone": {
    //       "code": { "type": String, "trim": true },
    //       "number": { "type": String, "trim": true },
    //     },
    //     "notifyType": {
    //       "type": String,
    //       "enum": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
    //     },
    //   },
    // ],
    "deviceInfo": [
      {
        "deviceId": { "type": String, "trim": true, "default": "" },
        "socketId": { "type": String, "trim": true, "default": "" },
        "deviceType": { "type": String, "trim": true } /* WEB, ANDROID, IOS */,
        "accessToken": { "type": String, "trim": true },
        "platform": { "type": String, "trim": true },
        "createdAt": { "type": "Date", "default": Date.now() },
      },
    ],
    "subscriptionInfo": {
      "planId": {
        "type": mongoose.Types.ObjectId,
        "trim": true,
        "default": null,
      },
      "isSubscribed": { "type": Boolean, "default": false },
      "subscribedAt": { "type": Date, "default": null },
      "subscribedEndAt": { "type": Date, "default": null },
      "subscribedAtTimestamp": { "type": Number, "default": null },
      "subscribedEndAtTimestamp": { "type": Number, "default": null },
    },

    "isOtpBypass": { "type": Boolean, "default": false },
    "isPriorityCount": { "type": Number, "optional": true, "default": null },
    "isPriorityStatus": { "type": Boolean, "optional": true, "default": false },
    "isPriorityUpdatedTime": {
      "type": Date,
      "optional": true,
      "default": null,
    },
    "isPriorityLocation": {
      "placeId": { "type": String, "optional": true },
      "addressType": { "type": String, "optional": true },
      "addressName": { "type": String, "optional": true, "lowercase": true },
      "fullAddress": { "type": String, "optional": true },
      "shortAddress": { "type": String, "optional": true },
      "lat": { "type": Number, "optional": true },
      "lng": { "type": Number, "optional": true },
    },
    "scanQr": { "type": String, "default": null },
    "permissionData": {
      "osType": {
        "type": String,
        "enum": [constantUtil.IOS, constantUtil.ANDROID],
      },
      "brand": { "type": String, "min": 1 },
      "model": { "type": String, "min": 1 },
      "osVersion": { "type": String, "min": 1 },
      "screenSize": { "type": String, "min": 1 },
      "internetConnectivityMedium": { "type": String, "min": 1 },
      "token": { "type": String, "min": 1 },
      "fcmId": { "type": String, "min": 1 },
      "deviceLocationStatus": { "type": Boolean, "default": false },
      "appLocationStatus": { "type": Boolean, "default": false },
      "baseUrl": { "type": String, "min": 1 },
      "appVersionInServer": { "type": String, "convert": true },
      "appVersionInStore": { "type": String, "convert": true },
      "deviceTime": { "type": String, "min": 1 },
      "timezoneInNumber": { "type": String, "min": 1 },
      "timezoneInformat": { "type": String, "min": 1 },
      "connectivityMediumName": { "type": String, "min": 1 },
      "socketConnetivity": { "type": Boolean, "default": false },
      "currentTime": { "type": Date, "convert": true },
    },

    // reffereal settings
    "referredBy": {
      "type": String,
      "optional": true,
      "trim": true,
      "lowercase": true,
      "default": null,
    },
    "referralDetails": {},
    "referrelTotalAmount": { "type": Number, "default": 0 },
    "joinCount": { "type": Number, "default": 0 }, // how much professional joined from referral
    "joinAmount": { "type": Number, "default": 0 },
    "joinerRideCount": { "type": Number, "convert": true, "default": 0 },
    "inviterRideCount": { "type": Number, "convert": true, "default": 0 },
    "invitationCount": { "type": Number, "convert": true, "default": 0 },
    // ! MEMBERSHIP
    "isUsedFreePlan": { "type": Boolean, "default": false },
    "isExpiredFreePlan": { "type": Boolean, "default": false },
    "activeMembershipDetails": { "type": MembershipSchema },
    "membershipDetails": { "type": [MembershipSchema], "default": [] },
    "membershipDetailsHistory": { "type": [MembershipSchema], "default": [] },
    "lastRedeemDate": { "type": Date, "convert": true },
    "inActivatedAt": { "type": Date, "convert": true, "default": null },
    "inActivatedBy": {
      "type": String,
      "trim": true,
      "optional": true,
      "enum": [constantUtil.PROFESSIONAL, constantUtil.ADMIN],
    },
    "licenceNo": {
      "type": "string",
      "convert": true,
      "optional": true,
      "default": null,
    },
    "paymentGateWayCustomerId": { "type": String, "default": null },
    "blockedUserList": { "type": Array, "default": [] },
    "registrationSetupId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "optional": true,
      "default": null,
    },
    "serviceTypeId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
    }, //**Important** --> For Profile Documents Verification (Key field --> profileVerification --> Cron Job Update)
    "serviceType": {
      "type": String,
      "trim": true,
      "optional": true,
      "enum": [
        constantUtil.ALL,
        constantUtil.CONST_RIDE,
        constantUtil.CONST_DELIVERY,
      ],
      "default": constantUtil.CONST_RIDE,
    }, //**Important** --> For Profile Documents Verification (Key field --> profileVerification --> Cron Job Update)
    "serviceAreaId": {
      "type": mongoose.Types.ObjectId,
      "ref": constantUtil.DBSCHEMACATEGORIES,
    },
    "blockedTill": { "type": Date, "convert": true, "default": null },
    "unblockedTill": { "type": Date, "convert": true, "default": null },
    "createdBy": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "paymentMode": {
      "type": [{ "type": String }],
      "default": [constantUtil.CASH],
    },
    "lastWithdrawDate": { "type": Date, "convert": true, "default": null },
    "lastWalletAlertSendDate": {
      "type": Date,
      "convert": true,
      "default": null,
    },
    "lastBlockedAlertSendDate": {
      "type": Date,
      "convert": true,
      "default": null,
    },
  },
  { "timestamps": true, "versionKey": false }
);
// compound index
professionalSchema.index(
  { "phone.code": 1, "phone.number": 1 },
  { "unique": true }
);
professionalSchema.index(
  { "phone.code": 1, "phone.number": 1, "email": 1 },
  { "unique": true }
);
//---------------------------
const professional = mongoose.model("professionals", professionalSchema);

module.exports = professional;
