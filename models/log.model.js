/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtils = require("../utils/constant.util");

const logSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "type": {
      "type": String,
      "enum": [
        constantUtils.CONST_LOG,
        constantUtils.CONST_INCENTIVE,
        constantUtils.ERRORDATA,
      ],
      "default": constantUtils.CONST_LOG,
    },
    "name": {
      "type": String,
      "required": true,
      "enum": [
        constantUtils.NOTIFICATIONTEMPLATES,
        constantUtils.LOGIN,
        constantUtils.LOGOUT,
        constantUtils.ERRORDATA,
      ],
    },
    "userType": {
      "type": String,
      "required": true,
      "enum": [
        constantUtils.ADMIN,
        constantUtils.DEVELOPER,
        constantUtils.OPERATORS,
        constantUtils.COORPERATEOFFICE,
        constantUtils.USER,
        constantUtils.PROFESSIONAL,
      ],
    },
    "professional": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAPROFESSIONAL,
      "required": false,
      "default": null,
    },
    "user": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAUSERS,
      "required": false,
      "default": null,
    },
    "admin": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "notificationId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtils.DBSCHEMAADMIN,
      "required": false,
      "alias": "notificationData",
      "default": null,
    },
    "logInTime": { "type": Date, "default": null },
    "logOutTime": { "type": Date, "default": null },
    "onlineMinutes": { "type": Number, "default": 0, "convert": true },
    "logDate": { "type": Date, "default": new Date() },
    "regionalLogDate": { "type": Date, "default": new Date() },
    "reason": { "type": String, "trim": true },
    "trackData": { "type": Object },
    //---------
    "isActive": { "type": Boolean, "default": true },
    "isDeleted": { "type": Boolean, "default": false },
  },
  { "timestamps": true, "versionKey": false }
);

const logModel = mongoose.model("log", logSchema);

module.exports = logModel;
