/* NPM DEPENDENCIES */
const mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");

const CategoryDataVehiclesSchema = {
  "recordNo": { "type": Number, "convert": true },
  "categoryId": { "type": String, "required": true },
  "isDefaultCategory": { "type": Boolean, "required": true },
  "isSubCategoryAvailable": { "type": Boolean, "required": true },
  "isForceAppliedToProfessional": { "type": Boolean, "required": true },
  "isSharerideForceAppliedToProfessional": {
    "type": Boolean,
    "required": true,
    "default": false,
  },
  "forAdminAndMobile": { "type": Boolean, "required": true },
  "subCategoryIds": { "type": [{ "type": String }] },
  "baseFare": { "type": Number, "default": 0 },
  "isEnableDynamicTravelCharge": {
    "type": Boolean,
    "required": true,
    "default": false,
  },

  "fromDistance": { "type": Number, "required": true },
  "toDistance": { "type": Number, "required": true },
  "farePerDistance": { "type": Number, "required": true },
  "farePerMinute": { "type": Number, "required": true },

  "fromDistance1": { "type": Number, "required": true },
  "toDistance1": { "type": Number, "required": true },
  "farePerDistance1": { "type": Number, "required": true },
  "farePerMinute1": { "type": Number, "required": true },

  "fromDistance2": { "type": Number, "required": true },
  "toDistance2": { "type": Number, "required": true },
  "farePerDistance2": { "type": Number, "required": true },
  "farePerMinute2": { "type": Number, "required": true },

  "fromDistance3": { "type": Number, "required": true },
  "toDistance3": { "type": Number, "required": true },
  "farePerDistance3": { "type": Number, "required": true },
  "farePerMinute3": { "type": Number, "required": true },

  "fromDistance4": { "type": Number, "required": true },
  "toDistance4": { "type": Number, "required": true },
  "farePerDistance4": { "type": Number, "required": true },
  "farePerMinute4": { "type": Number, "required": true },

  "siteCommission": { "type": Number, "required": true },
  "siteCommissionManualMeter": { "type": Number, "required": true },
  "serviceTaxPercentage": { "type": Number, "required": true },

  "peakFare": { "type": Number, "required": true },
  "nightFare": { "type": Number, "required": true },
  "multiStopCharge": { "type": Number, "convert": true },
  // Intercity
  "isEnableRoundTrip": {
    "type": Boolean,
    "required": true,
    "default": false,
  },
  "intercityBoundaryFareType": {
    "type": String,
    // "required": true,
    "enum": [constantUtil.FLAT, constantUtil.PERCENTAGE, constantUtil.NONE],
    "default": constantUtil.NONE,
  },
  "intercityBoundaryFare": { "type": Number, "required": true },
  "intercityPickupFarePerDistance": { "type": Number, "required": true },
  "intercityPickupFarePerMinites": { "type": Number, "required": true },
  "isShowEstimationfareRange": {
    "type": Boolean,
    "required": true,
    "default": false,
  },
  "showEstimationfareRangeAmount": { "type": Number, "required": true },
  "sharePercentageUser1": { "type": Number, "convert": true, "default": 0 },
  "sharePercentageUser2": { "type": Number, "convert": true, "default": 0 },

  // surCharge
  "isSurchargeEnable": { "type": Boolean, "required": true },
  "surchargeFee": { "type": Number, "required": true },
  "surchargeTitle": { "type": String, "required": true, "trim": true },

  // schedule support
  "isScheduleSupport": { "type": Boolean, "required": true },

  // package Support
  "isPackageSupport": { "type": Boolean, "required": true },

  //ShareRide Support
  "isSupportShareRide": { "type": Boolean, "convert": true, "default": true },

  // user cancellation
  "cancellation": {
    "type": {
      "status": { "type": Boolean, "required": true },
      "amount": { "type": Number, "required": true, "default": 0 },
      "thresholdMinute": { "type": Number, "required": true, "default": 0 },
    },
    "required": true,
  },

  // professional Cancellation
  "professionalCancellation": {
    "type": {
      "status": { "type": Boolean, "required": true },
      "amount": { "type": Number, "required": true, "default": 0 },
      "thresholdMinute": { "type": Number, "required": true, "default": 0 },
    },
    "required": true,
  },

  // minimum Charge
  "minimumCharge": {
    "type": {
      "status": { "type": Boolean, "required": true },
      "amount": { "type": Number, "required": true, "default": 0 },
    },
    "required": true,
  },

  // waiting Charge Details
  "waitingChargeDetails": {
    "type": {
      // before waiting charge
      "isBeforeRideWaitingCharge": {
        "type": Boolean,
        "required": true,
      },
      "beforeRideWaitingChargePerMin": {
        "type": Number,
        "required": true,
        "default": 0,
      },
      "beforeRideWaitingGracePeriod": {
        "type": Number,
        "required": true,
        "default": 0,
      },
      // after waiting charge
      "isAfterRideWaitingCharge": {
        "type": Boolean,
        "required": true,
      },
      "afterRideWaitingChargePerMin": {
        "type": Number,
        "required": true,
        "default": 0,
      },
      "afterRideWaitingGracePeriod": {
        "type": Number,
        "required": true,
        "default": 0,
      },
    },
    "required": true,
  },

  // RENTAL DATA
  "rental": {
    "isActive": { "type": Boolean, "required": true, "default": false },
    "estimate": {
      "farePerDistance": { "type": Number, "required": true, "default": 0 },
      "farePerTime": { "type": Number, "required": true, "default": 0 },
    },
    "exceed": {
      "farePerDistance": { "type": Number, "required": true, "default": 0 },
      "farePerTime": { "type": Number, "required": true, "default": 0 },
    },
  },

  // fare Breakup details

  "fareBreakUpDetail": {
    "type": {
      "title": { "type": String, "default": "", "required": true },
      "description": { "type": String, "default": "", "required": true },
      "fareMessage": { "type": String, "default": "", "required": true },
      "amenities": {
        "type": [
          {
            "type": {
              "title": { "type": "string", "trim": true },
              "image": { "type": "string", "trim": true },
            },
          },
        ],
        "default": [],
      },
      "fleets": {
        "type": [
          {
            "type": {
              "title": { "type": "string", "trim": true },
              "image": { "type": "string", "trim": true },
            },
          },
        ],
        "default": [],
      },
      "guidelines": {
        "type": [
          {
            "type": {
              "title": { "type": String },
              "points": {
                "type": [{ "type": String }],
              },
            },
          },
        ],
        "default": [],
      },
    },
    "required": true,
  },
  //
  "cancellationFeeCreditToUserPrecentage": {
    "type": Number,
    "required": true,
    "convert": true,
  },
  "cancellationFeeCreditToProfessionalPrecentage": {
    "type": Number,
    "required": true,
    "convert": true,
  },
};

const CategoryDataSchema = {
  "type": {
    "vehicles": {
      "type": [CategoryDataVehiclesSchema],
    },
  },
  "required": true,

  /* CHECK ON VALIDATION JS FOR CATEGORY DATA SCHEMA */
};

const categorySchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "categoryName": {
      "type": String,
      "lowercase": true, // Must be lowercase
      "required": true,
      "convert": true,
      "enum": [
        constantUtil.CONST_RIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_CORPORATERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_INTERCITYRIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PICKUPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_DROPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PICKUPANDDROPOUTSIDERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_SHARERIDE.toLowerCase(), // Must Lowercase Type
        constantUtil.CONST_PACKAGES.toLowerCase(), // Must Lowercase Type
      ],
    },
    "locationName": {
      "type": String,
      // "unique": true,
      "trim": true,
      "lowercase": true,
      "required": true,
    },
    "recordType": {
      "type": String,
      // "unique": true,
      "trim": true,
      "enum": [
        constantUtil.CONST_PARENT.toLowerCase(),
        constantUtil.CONST_CHILD.toLowerCase(),
      ],
      "lowercase": true,
      "required": true,
    },
    "recordTypeNumber": {
      "type": Number,
      "required": true,
      "default": 0, // 0 --> ride, 1--> corporateride, 2 -->  intercityride
    },

    "address": {
      "type": Object,
      // "lowercase": true,
      // "required": true,
    },
    "corporateId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
      "default": null,
    },
    "serviceAreaId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMACATEGORIES,
      "required": false,
      "default": null,
    },
    "location": {
      "type": {
        "type": String,
        "enum": ["Polygon"],
        "required": true,
      },
      "coordinates": {
        "type": [[[Number]]],
        "required": true,
      },
    },
    "isPeakFareAvailable": {
      "status": { "type": Boolean, "required": true, "default": false },
      "startTime": { "type": String },
      "endTime": { "type": String },
      "weekDays": { "type": Object },
    },
    "isNightFareAvailable": {
      "status": { "type": Boolean, "required": true, "default": false },
      "startTime": { "type": String },
      "endTime": { "type": String },
    },

    "categoryData": CategoryDataSchema,
    // 'serviceTaxPercentage': {
    //   'type': Number,
    //   'required': true,
    // },
    "distanceType": {
      "type": String,
      "required": true,
    },
    "currencySymbol": {
      "type": String,
      "required": true,
    },
    "currencyCode": {
      "type": String,
      "required": true,
    },
    "status": {
      /* ACTIVE, INACTIVE */
      "type": String,
      "required": true,
    },
    "isShowProfessionalList": { "type": Boolean, "default": false },
    // 'country': {
    //   'code': { 'type': String, 'trim': true, 'default': 'IN' },
    //   'name': { 'type': String, 'trim': true, 'default': 'INDIA' },
    // },
    // 'payout': {
    //   'type': String,
    //   'enum': [
    //     constantUtils.AUTOMATED,
    //     constantUtils.MANUAL,
    //     constantUtils.DISPLAYINFO,
    //   ],
    //   'default': constantUtils.AUTOMATED,
    // },
    "isRestrictMaleBookingToFemaleProfessional": {
      "type": Boolean,
      "default": false,
    },
    "isNeedSecurityImageUpload": { "type": Boolean, "default": false },

    // for rental
    "rental": {
      "isActive": { "type": Boolean, "default": false },
      "timeRange": {
        "from": {
          "type": Number,
          "default": 0,
        },
        "to": {
          "type": Number,
          "default": 0,
        },
        "type": {
          "type": String,
          "enum": ["HR", "MIN"],
          "default": "HR",
        },
      },
      "distanceRange": {
        "distanceForATimeRange": {
          "type": Number,
          "Default": 0,
        },
        "type": {
          "type": String,
          "enum": ["KM", "MILES"],
          "default": "KM",
        },
      },
    },
    "paymentMode": { "type": [{ "type": String }] },
    "maxRetryRequestDistance": { "type": Number, "convert": true },
    "roundingType": {
      "type": String,
      "enum": [constantUtil.CONST_DECIMAL, constantUtil.CONST_INTEGER],
      "default": constantUtil.CONST_INTEGER,
    },
    "roundingFactor": { "type": Number, "convert": true },
  },
  { "timestamps": true, "versionKey": false }
);

categorySchema.index({ "location": "2dsphere" });
// compound index
categorySchema.index(
  {
    "locationName": 1,
    "categoryName": 1,
    "corporateId": 1,
    "serviceAreaId": 1,
  },
  { "unique": true }
);
categorySchema.index({
  "locationName": 1,
  "categoryName": 1,
  "status": 1,
});
const category = mongoose.model("categories", categorySchema);

module.exports = category;
