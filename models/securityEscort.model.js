/* NPM DEPENDENCIES */
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");

const securityEscortSchema = new mongoose.Schema(
  {
    "clientId": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "convert": true,
      "default": undefined,
    },
    "user": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAUSERS,
      "required": false,
    },
    "professional": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAPROFESSIONAL,
      "required": false,
    },
    "userType": {
      "type": String,
      "required": true,
      "enum": [constantUtil.USER, constantUtil.PROFESSIONAL],
    },
    "isUserSubscribe": {
      "type": Boolean,
      "required": true,
      "default": false,
    },
    "isEscortApply": {
      "type": Boolean,
      "required": true,
      "default": false,
    },
    "officer": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAOFFICER,
      "required": false,
    },
    "operator": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMAADMIN,
      "required": false,
    },
    "booking": {
      "type": mongoose.Schema.Types.ObjectId,
      "ref": constantUtil.DBSCHEMABOOKINGS,
      "required": false,
    },
    "alertLocation": {
      "addressName": { "type": String, "required": false, "trim": true },
      "fullAddress": { "type": String, "required": false, "trim": true },
      "shortAddress": { "type": String, "required": false, "trim": true },
      "lat": { "type": Number, "required": true },
      "lng": { "type": Number, "required": true },
      "type": { "type": String, "default": constantUtil.AWAITING },
    },
    "alertType": {
      "type": String,
      "required": true,
      "enum": [constantUtil.SOS, constantUtil.EMERGENCY, constantUtil.TRACKING],
    },
    "alertDate": { "type": Date, "required": true },
    "escortStatus": {
      "type": String,
      "required": true,
      "enum": [
        constantUtil.AWAITING,
        constantUtil.ARRIVED,
        constantUtil.CANCELLED,
        constantUtil.USERDENY,
        constantUtil.PROFESSIONALDENY,
        constantUtil.ACCEPTED,
        constantUtil.EXPIRED,
        constantUtil.STARTED,
        constantUtil.ENDED,
        constantUtil.USERCANCELLED,
        constantUtil.PROFESSIONALCANCELLED,
      ],
      "default": constantUtil.AWAITING,
    },
    "threadMessage": {
      "type": String,
      "required": true,
    },
    "status": {
      "type": String,
      "required": true,
      "enum": [constantUtil.NEW, constantUtil.ATTENDED, constantUtil.CLOSED],
      "default": constantUtil.NEW,
    },
    "priority": {
      "type": String,
      "required": true,
      "enum": [
        constantUtil.NOTASSIGNED,
        constantUtil.CRITICAL,
        constantUtil.MINIMAL,
        constantUtil.MEDIUM,
      ],
      "default": constantUtil.NOTASSIGNED,
    },
    "securityServiceName": {
      "type": String,
      "required": true,
    },
    "securityThreadId": {
      "type": String,
      "required": true,
      "unique": true,
    },
    "ticketId": {
      "type": String,
      "required": false,
    },
    "securityServiceId": {
      "type": String,
      "required": true,
    },
    "adminNotes": {
      "type": String,
      "required": false,
      "default": null,
    },
    "acknowledge": {
      "isArrived": { "type": Boolean, "required": true, "default": false },
      "isEnded": { "type": Boolean, "required": true, "default": false },
      "isArrivedStatus": { "type": String, "default": null },
      "isEndedStatus": { "type": String, "default": null },
    },
    "isAdminForceAssign": {
      "type": Boolean,
      "default": false,
    },
    "isviewed": {
      "type": Boolean,
      "default": false,
    },
    "mediaType": {
      "type": String,
      "enum": [
        constantUtil.MESSAGE,
        constantUtil.IMAGE,
        constantUtil.VIDEO,
        constantUtil.AUDIO,
      ],
    },
    "mediaReportMessage": {
      "type": "string",
      "default": null,
    },
    "mediaReportImage": {
      "type": Array,
      "required": false,
    },
    "mediaReportVideo": {
      "type": String,
      "required": false,
    },
    "mediaReportAudio": {
      "type": String,
      "required": false,
    },
    "sendedTrustedContact": {
      "type": Array,
      "required": false,
    },
    "activity": {
      "acceptTime": { "type": Date, "default": null },
      "arriveTime": { "type": Date, "default": null },
      "endTime": { "type": Date, "default": null },
      "denyOrExpireTime": { "type": Date, "default": null },
      "cancelTime": { "type": Date, "default": null },
      "pickUpTime": { "type": Date, "default": null },
      "workedMins": { "type": Number, "default": 0 },
    },
    "fromLocation": {
      "addressName": { "type": String, "default": null },
      "fullAddress": { "type": String, "default": null },
      "shortAddress": { "type": String, "default": null },
      "lat": { "type": Number, "default": null },
      "lng": { "type": Number, "default": null },
      "status": { "type": String, "default": null },
      "type": { "type": String, "default": null },
    },
    "toLocation": {
      "addressName": { "type": String, "default": null },
      "fullAddress": { "type": String, "default": null },
      "shortAddress": { "type": String, "default": null },
      "lat": { "type": Number, "default": null },
      "lng": { "type": Number, "default": null },
      "status": { "type": String, "default": null },
      "type": { "type": String, "default": null },
    },
    "escortReview": {
      "isUserReviewed": { "type": Boolean, "default": false },
      "isProfessionalReviewed": { "type": Boolean, "default": false },
      "isOfficerReviewed": { "type": Boolean, "default": false },
      "userReview": {
        "rating": { "type": Number, "trim": true, "default": null },
        "comment": { "type": String, "default": null },
      },
      "professionalReview": {
        "rating": { "type": Number, "trim": true, "default": null },
        "comment": { "type": String, "trim": true, "default": null },
      },
      "officerReview": {
        "rating": { "type": Number, "trim": true, "default": null },
        "comment": { "type": String, "trim": true, "default": null },
      },
    },
  },
  { "timestamps": true, "versionKey": false }
);

const securityEscort = mongoose.model("securityescorts", securityEscortSchema);

module.exports = securityEscort;
