"use strict";

const DbService = require("moleculer-db");

const constantUtil = require("../utils/constant.util");

const models = {
  [constantUtil.DBSCHEMAADMIN]: require("../models/admin.model"),
  [constantUtil.DBSCHEMAUSERS]: require("../models/user.model"),
  [constantUtil.DBSCHEMAPROFESSIONAL]: require("../models/professional.model"),
  [constantUtil.DBSCHEMACATEGORIES]: require("../models/category.model"),
  [constantUtil.DBSCHEMABOOKINGS]: require("../models/booking.model"),
  [constantUtil.DBSCHEMATRANSACTION]: require("../models/transaction.model"),
  [constantUtil.DBSCHEMAOFFICER]: require("../models/officer.model"),
  [constantUtil.DBSCHEMASUBSCRIPTION]: require("../models/subscription.model"),
  [constantUtil.DBSCHEMASECURITYESCORT]: require("../models/securityEscort.model"),
  [constantUtil.DBSCHEMABILLINGS]: require("../models/billings.model"),
  [constantUtil.DBSCHEMAEVENTSWATCH]: require("../models/eventWatch.model"),
  [constantUtil.DBSCHEMAGAURDWATCH]: require("../models/gaurdWatch.model"),

  // new modles or collections only inside of admin service
  [constantUtil.DBSCHEMAREWARDS]: require("../models/rewards.model"),
  [constantUtil.DBSCHEMALOG]: require("../models/log.model"),
  [constantUtil.DBSCHEMA_ZMAP]: require("../models/zmap.model"),
  [constantUtil.DBSCHEMA_HEATMAP]: require("../models/heatmap.model"),
};

module.exports = function (collection) {
  const cacheCleanEventName = `cache.clean.${collection}`;

  const schema = {
    "mixins": [DbService],

    "events": {
      /**
       * Subscribe to the cache clean event. If it's triggered
       * clean the cache entries for this service.
       *
       * @param {Context} ctx
       */
      async [cacheCleanEventName]() {
        if (this.broker.cacher) {
          await this.broker.cacher.clean(`${this.fullName}.*`);
        }
      },
    },

    "methods": {
      /**
       * Send a cache clearing event when an entity changed.
       *
       * @param {String} type
       * @param {any} json
       * @param {Context} ctx
       */
      async entityChanged(type, json, ctx) {
        ctx.broadcast(cacheCleanEventName);
      },
    },

    async started() {
      // Check the count of items in the DB. If it's empty,
      // call the `seedDB` method of the service.
      if (this.seedDB) {
        const count = await this.adapter.count();
        if (count == 0) {
          this.logger.info(
            `The '${collection}' collection is empty. Seeding the collection...`
          );
          await this.seedDB();
          this.logger.info(
            "Seeding is done. Number of records:",
            await this.adapter.count()
          );
        }
      }
    },
  };

  const MongooseAdapter = require("moleculer-db-adapter-mongoose");
  const dbName = process.env.CLIENT_KEY ? process.env.CLIENT_KEY : "rays";
  const mongoURL = process.env.MONGO_URL
    ? process.env.MONGO_URL
    : "mongodb://0.0.0.0:27017/" + dbName;
  // const mongoURL = "mongodb://0.0.0.0:27017/rays";
  console.log(mongoURL);
  schema.adapter = new MongooseAdapter(
    mongoURL,
    // : "mongodb://localhost:27017/rays",
    { "useNewUrlParser": true, "useUnifiedTopology": true }
  );

  schema.model = models[collection];

  // collection list
  // schema.collections = {};

  // schema.collections[constantUtil.DBSCHEMAPARNERDEALSREWARDS] =
  //   models[constantUtil.DBSCHEMAPARNERDEALSREWARDS];

  return schema;
};
