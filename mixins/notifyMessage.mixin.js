"use strict";

const english = require("../notifyMessage/english.notifyMessage");
const portuguese = require("../notifyMessage/portuguese.notifyMessage");
const tamil = require("../notifyMessage/tamil.notifyMessage");
const hindi = require("../notifyMessage/hindi.notifyMessage");
const swahili = require("../notifyMessage/swahili.notifyMessage");
const arabic = require("../notifyMessage/arabic.notifyMessage");
const chinese = require("../notifyMessage/chinese.notifyMessage");
const hebrew = require("../notifyMessage/hebrew.notifyMessage");
const spanish = require("../notifyMessage/spanish.notifyMessage");
const french = require("../notifyMessage/french.notifyMessage");
const russian = require("../notifyMessage/russian.notifyMessage");
const german = require("../notifyMessage/german.notifyMessage");
const amharic = require("../notifyMessage/amharic.notifyMessage");
const afrikaans = require("../notifyMessage/afrikaans.notifyMessage");
const zulu = require("../notifyMessage/zulu.notifyMessage");
const xhosa = require("../notifyMessage/xhosa.notifyMessage");
const tsonga = require("../notifyMessage/tsonga.notifyMessage");

const notifyMessage = {};

// -------------- Set Default Notify Language Start ------------
notifyMessage.setNotifyLanguage = (languageCode) => {
  if (languageCode) {
    switch (languageCode) {
      case "pt":
      case "pt-br":
        return portuguese;

      case "ta":
      case "tam":
        return tamil;

      case "hi":
        return hindi;

      case "sw":
      case "swa":
        return swahili;

      case "zh":
        return chinese;

      case "ar":
        return arabic;

      case "he":
      case "iw":
        return hebrew;

      case "es":
        return spanish;

      case "fr":
        return french;

      case "ru":
        return russian;

      case "de":
        return german;

      case "am":
      case "amh":
        return amharic;

      case "af":
        return afrikaans;

      case "zu":
      case "zul":
        return zulu;

      case "xh":
      case "xho":
        return xhosa;

      // case "ts":
      // case "tso":
      //   return tsonga;

      default:
        return english;
    }
  } else {
    return english;
  }
};

// notifyMessage.getNotifyMessage = (languageCode, messageKey) => {
//   let responseMsg = "";
//   if (languageCode) {
//     switch (languageCode) {
//       case "pt":
//       case "pt-br":
//         responseMsg = portuguese[messageKey];
//         break;

//       case "ta":
//         responseMsg = tamil[messageKey];
//         break;

//       default:
//         responseMsg = english[messageKey];
//         break;
//     }
//   } else {
//     responseMsg = english[messageKey];
//   }
//   return responseMsg;
// };
// -------------- Set Default Notify Language Start ------------

module.exports = notifyMessage;

// module.exports = (async () => await setNotifyLanguage())();
