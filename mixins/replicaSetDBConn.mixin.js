const mongoose = require("mongoose");

const dbName = process.env.CLIENT_KEY ? process.env.CLIENT_KEY : "rays";
const mongoURL = process.env.MONGO_URL
  ? process.env.MONGO_URL
  : "mongodb://0.0.0.0:27017/" + dbName + "?replicaSet=rs";

///#region Method 1 --> Create Connection  Method
mongoose.createConnection(mongoURL, {
  "useNewUrlParser": true,
  "useUnifiedTopology": true,
  // "retryWrites": true,
});
const conn = mongoose.connection;

conn.once("open", () => console.info("Connection to Database is successful"));

conn.on("error", () => console.error.bind(console, "connection error"));

conn.on("disconnected", () => {
  "connection disconnected";
});

conn.on("connected", () => {
  "connection connected";
});

conn.on("close", () => {
  "connection close";
});

module.exports = conn;
///#endregion Method 1

// // -------------------
// // mongoose.connect(mongoURL, {
// //   "useNewUrlParser": true,
// //   "useUnifiedTopology": true,
// //   // "retryWrites": true,
// // });
// // const conn = mongoose.connection;

// // conn.on("error", () => console.error.bind(console, "connection error"));

// // conn.once("open", () => console.info("Connection to Database is successful"));

// // module.exports = conn;

///#region Method 2 --> Pass only Connection String & Property

// module.exports = {
//   "connectionString": mongoURL,
//   "connectionProperty": {
//     "useNewUrlParser": true,
//     "useUnifiedTopology": true,
//     // "retryWrites": true,
//   },
// };

///#endregion Method 2
