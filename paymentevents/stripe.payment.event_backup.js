//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");

const defaultCent = 100;
//----------------- Need to Remove End --------------------

const transactionStripeEvents = {};

transactionStripeEvents.checkValidCard = async function (context) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let finalCustomerData = {};
  let finalPaymentData = {};
  // let tokenizationConfig = {};
  let customerConfig = {};
  let paymentConfig = {};
  const keyConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const isCVVRequired = context.params?.paymentData["isCVVRequired"] || false;
  // stripe(tokenizationConfig.secretKey)
  // //------------------ Create Token Start-------------------------------
  // // if (
  // //   !context.params.userData.embedtoken &&
  // //   context.params.userData.embedtoken == null
  // // ) {
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // tokenizationConfig["data"]["card"] = {};
  // tokenizationConfig["data"]["card"]["number"] = context.params.cardNumber;
  // tokenizationConfig["data"]["card"]["exp_month"] = context.params.expiryMonth;
  // tokenizationConfig["data"]["card"]["exp_year"] = context.params.expiryYear;
  // tokenizationConfig["data"]["card"]["cvc"] = context.params.cvv;
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // // } else {
  // //   finalTokenData['id'] = 1
  // // }
  // //------------------ Create Token End-------------------------------
  // if (finalTokenData) {
  // cardBrand = finalTokenData?.card?.brand.toLowerCase();
  // responseData["cardToken"] = finalTokenData.id;
  if (context.params.cardTokenId) {
    responseData["cardToken"] = context.params.cardTokenId;
    let stripeCustomerId =
      context.params?.userData?.paymentGateWayCustomerId || null;

    customerConfig = {
      ...keyConfig,
      "data": {},
    };

    if (!stripeCustomerId && stripeCustomerId == null) {
      //------------------ Create Customer Start ---------------------------------
      customerConfig["data"]["name"] = context.params.holderName;
      customerConfig["data"]["email"] = context.params.email;
      customerConfig["data"]["address"] = {};
      customerConfig["data"]["address"]["city"] = context.params.city
        ? context.params.city
        : "city";
      customerConfig["data"]["address"]["line1"] = context.params.line1
        ? context.params.line1
        : "Address line1";
      customerConfig["data"]["address"]["line2"] = context.params.line2
        ? context.params.line2
        : "Address line2";
      customerConfig["data"]["address"]["postal_code"] = context.params.zipCode
        ? context.params.zipCode
        : "600000";
      customerConfig["data"]["address"]["state"] = context.params.state
        ? context.params.state
        : "state";
      customerConfig["data"]["address"]["country"] = context.params.country
        ? context.params.country
        : context.params.generalSettings.data.defaultCountryCode;
      customerConfig["data"]["description"] = context.params.description
        ? context.params.description
        : "Create New Customer";
      // customerConfig["data"]["source"] = finalTokenData.id;
      customerConfig["data"]["source"] = context.params.cardTokenId;
      // Stripe Call For create customer
      try {
        const stripe = require("stripe")(customerConfig.secretKey);
        finalCustomerData = await stripe.customers.create(customerConfig.data);
        stripeCustomerId = finalCustomerData.id;
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
    } else {
      // customerConfig["data"]["source"] = finalTokenData.id;
      customerConfig["data"]["source"] = context.params.cardTokenId;
      // Stripe Call For create customer
      try {
        const stripe = require("stripe")(customerConfig.secretKey);
        finalCustomerData = await stripe.customers.update(
          stripeCustomerId, //stripeCustomerId
          customerConfig.data
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
    }
    //------------------ Create Customer End ---------------------------------
    if (finalCustomerData) {
      //---------------- create Charge End--------------------------
      const minimumChargeAmount =
        parseFloat(context.params.minimumChargeAmount) * defaultCent; // Defaule Card Verification deduction
      paymentConfig = {
        ...keyConfig,
        "data": {},
      };
      paymentConfig["data"]["amount"] = parseInt(minimumChargeAmount); // Defaule Card Verification deduction
      paymentConfig["data"]["currency"] =
        context.params.generalSettings.data.currencyCode;
      // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
      paymentConfig["data"]["payment_method_types"] = ["card"];
      paymentConfig["data"]["capture_method"] = "automatic"; //automatic or manual
      // paymentConfig["data"]["confirm"] = false; //CZ Altered
      paymentConfig["data"]["confirm"] = true; //CZ Altered
      // paymentConfig['data']['automatic_payment_methods'] = true
      paymentConfig["data"]["customer"] = stripeCustomerId;
      paymentConfig["data"]["description"] = context.params.transactionReason;
      // paymentConfig["data"]["automatic_tax"] = {};
      // paymentConfig["data"]["automatic_tax"]["enabled]"] = true;
      //Stripe Call For create Charge
      try {
        const stripe = require("stripe")(paymentConfig.secretKey);
        finalPaymentData = await stripe.paymentIntents.create(
          paymentConfig.data
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      // Response
      if (finalPaymentData) {
        responseData["authType"] = "NOAUTH";
        // // responseData['stripeCardToken'] = finalTokenData.id
        // responseData["cardToken"] = finalTokenData.id;
        // responseData["embedtoken"] = finalTokenData.id;
        responseData["cardToken"] = context.params.cardTokenId;
        responseData["embedtoken"] = context.params.cardTokenId;
        responseData["isDefault"] = true;
        responseData["isCVVRequired"] = isCVVRequired;
        responseData["stripeCustomerId"] = finalCustomerData.id; //  Need to Remove this key
        responseData["paymentGateWayCustomerId"] = finalCustomerData.id;
        responseData["paymentIntentId"] = finalPaymentData.id;
        responseData["paymentIntentClientSecret"] =
          finalPaymentData.client_secret;
        responseData["transactionAmount"] = parseInt(
          context.params.minimumChargeAmount
        );
        responseData["transactionId"] = finalPaymentData.id;
        // responseData['paymentTransactionId'] = finalPaymentData.id.toString()
        responseData["successResponse"] = finalPaymentData;
        responseData["status"] = finalPaymentData.status;
        return { "code": 200, "data": responseData, "message": "Valid Card" };
      } else {
        return { "code": 400, "data": {}, "message": "invalid Card" };
      }
    }
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
};

transactionStripeEvents.newRechargeWalletTransaction = async function (
  context
) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let finalPaymentData = {};
  let paymentConfig = {};
  let customerConfig = {};
  // let tokenizationConfig = {};
  const responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // //------------------ Create Token Start-------------------------------
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // tokenizationConfig["data"]["card"] = {};
  // tokenizationConfig["data"]["card"]["number"] =
  //   context.params.cardData.data.cardNumber;
  // tokenizationConfig["data"]["card"]["exp_month"] =
  //   context.params.cardData.data.expiryMonth;
  // tokenizationConfig["data"]["card"]["exp_year"] =
  //   context.params.cardData.data.expiryYear;
  // if (context.params.cardData.data.isCVVRequired) {
  //   tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
  // }
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // //------------------ Create Token End-------------------------------
  const stripeCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;
  if (stripeCustomerId) {
    // cardBrand = finalTokenData?.card?.brand.toLowerCase();
    //------------------ Update Customer Token Start ---------------------------------
    customerConfig = {
      ...keyConfig,
      "data": {},
    };
    // customerConfig["data"]["source"] = finalTokenData.id;
    customerConfig["data"]["source"] = context.params?.cardTokenId;
    // Stripe Call For create customer
    let finalCustomerData = {};
    try {
      const stripe = require("stripe")(customerConfig.secretKey);
      finalCustomerData = await stripe.customers.update(
        stripeCustomerId, //stripeCustomerId
        customerConfig.data
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //------------------ Update Customer Token End ---------------------------------
    // //---------------- create Charge End--------------------------
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    // paymentConfig['data']['amount'] =
    //   parseInt(parseFloat(context.params.amount)) * defaultCent
    // paymentConfig['data']['currency'] =
    //   context.params.generalSettings.data.currencyCode
    // paymentConfig['data']['customer'] = context.params.userData.stripeCustomerId
    // paymentConfig['data']['description'] = context.params.transactionReason
    // paymentConfig['data']['capture'] = true
    // //Stripe Call For create Charge
    // try {
    //   const stripe = require('stripe')(paymentConfig.secretKey)
    //   finalPaymentData = await stripe.charges.create(paymentConfig.data)
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    //================= paymentIntents ======================
    paymentConfig["data"]["amount"] = parseInt(
      parseFloat(context.params.amount) * defaultCent
    ); // Defaule Card Verification deduction
    paymentConfig["data"]["currency"] =
      context.params.generalSettings.data.currencyCode;
    // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
    paymentConfig["data"]["payment_method_types"] = ["card"];
    paymentConfig["data"]["capture_method"] = "automatic"; //automatic
    paymentConfig["data"]["confirm"] = true;
    // paymentConfig['data']['confirmation_method'] = 'manual'
    // paymentConfig['data']['requires_capture'] = false
    // paymentConfig['data']['requires_action'] = false

    // paymentConfig['data']['automatic_payment_methods'] = true
    paymentConfig["data"]["customer"] = stripeCustomerId;
    paymentConfig["data"]["description"] = context.params.transactionReason;
    //Stripe Call For create Charge
    try {
      const stripe = require("stripe")(paymentConfig.secretKey);
      finalPaymentData = await stripe.paymentIntents.create(paymentConfig.data);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    // let data = {}
    // data['paymentData'] = context.params.paymentData
    // data['transactionId'] = finalPaymentData.id
    // paymentIntentsCapture(data)
    //================= paymentIntents =====================
    // =================================
    // let dataChargeCapture = {}
    let captureConfig = {};
    //
    //-------------- charges payment Intents Start -----------------------
    captureConfig = {
      ...keyConfig,
      "data": {},
    };
    // //------- confirm -------------------
    // try {
    //   const stripe = require('stripe')(captureConfig.secretKey)
    //   const confirmPaymentIntent = await stripe.paymentIntents.confirm(
    //     finalPaymentData.id
    //   )
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    //----------- confirm ----------
    captureConfig["data"]["charge"] = finalPaymentData.id;
    // For capture payment
    // try {
    //   const stripe = require('stripe')(captureConfig.secretKey)
    //   dataChargeCapture = await stripe.paymentIntents.capture(
    //     captureConfig.data.charge
    //   )
    //   // dataChargeCapture = await stripe.paymentIntents.capture(
    //   //   captureConfig.data
    //   // )
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    //-------------- charges payment Intents End -----------------------
    //=================================
    //---------------- create Charge End--------------------------
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    // responseData['stripeCardToken'] = finalTokenData.id
    responseData["paymentGateWayCustomerId"] = stripeCustomerId;
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] = finalPaymentData.id;
    responseData["paymentIntentId"] = finalPaymentData.id;
    responseData["paymentIntentAmount"] = context.params.amount;
    responseData["transactionAmount"] = context.params.amount;
    responseData["paymentIntentClientSecret"] = finalPaymentData.client_secret;
    responseData["successResponse"] = finalPaymentData;
    responseData["status"] = finalPaymentData.status;
    return { "code": 200, "data": responseData, "message": "Valid Card" };
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
};

transactionStripeEvents.newRechargeWalletTransactionUsingPixkey =
  async function (context) {
    // let finalTokenData = {};
    // let cardBrand = {};
    let finalPaymentData = {};
    let paymentConfig = {};
    // let customerConfig = {};
    // let tokenizationConfig = {};
    const responseData = {};
    const keyConfig = {};

    if (
      context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX
    ) {
      keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
    } else {
      keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
    }
    const redirectUrl = `${
      context.params.generalSettings.data.redirectUrls.paymentRedirectApiUrl
    }/api/transaction/stripe/webhookingStripePixKeyPayment${""}`;
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    const inputData = {
      "payment_method_types": ["pix"],
      "payment_method_options": { "pix": { "expires_after_seconds": 180 } },
      "customer_email": context.params.userData.email,
      // "payment_intent_data": {
      //   "next_action": {
      //     "pix_display_qr_code": { "hosted_instructions_url": redirectUrl },
      //   },
      // },
      "line_items": [
        {
          "price_data": {
            "currency": "brl",
            "product_data": {
              "name": context.params.transactionReason,
            },
            "unit_amount": parseInt(
              parseFloat(context.params.amount) * defaultCent
            ),
          },
          "quantity": 1,
        },
      ],
      "mode": "payment",
      "success_url": redirectUrl + "?status=success",
      "cancel_url": redirectUrl + "?status=cancel",
    };
    try {
      const stripe = require("stripe")(paymentConfig.secretKey);
      finalPaymentData = await stripe.checkout.sessions.create(inputData);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }

    // Response
    if (finalPaymentData.status === "open") {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["paymentGateWayCustomerId"] = null;
      // responseData['transactionId'] = finalPaymentData.id.toString()
      responseData["transactionId"] = finalPaymentData.id;
      responseData["paymentIntentId"] = finalPaymentData.payment_intent;
      responseData["paymentIntentAmount"] = context.params.amount;
      responseData["transactionAmount"] = context.params.amount;
      responseData["paymentIntentClientSecret"] =
        finalPaymentData.payment_intent;
      responseData["successResponse"] = finalPaymentData;
      responseData["redirectUrl"] = finalPaymentData.url;
      responseData["status"] = finalPaymentData.status;
      return { "code": 200, "data": responseData, "message": "Valid Card" };
    } else {
      return { "code": 400, "data": {}, "message": "invalid Card" };
    }
  };

transactionStripeEvents.newWithdrawalWalletTransaction = async function (
  context
) {
  let paymentConfig = {};
  const responseData = {};
  let finalPaymentData = {};
  let finalTokenData = {};
  let tokenizationConfig = {};
  let finalCustomerData = {};
  let customerConfig = {};
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const stripeDestinationId = context.params.bankDetails.stripeAccountId;
  const bankAccountId = context.params.bankDetails.bankAccountId;
  const currencyCode = context.params.generalSettings.data.currencyCode;
  // //------------------ Create Token Start-------------------------------
  // // if (
  // //   !context.params.userData.embedtoken &&
  // //   context.params.userData.embedtoken == null
  // // ) {
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // const countryCode = context.params.country
  //   ? context.params.country
  //   : context.params.generalSettings.data.defaultCountryCode;
  // const currencyCode = context.params.generalSettings.data.currencyCode;
  // //
  // tokenizationConfig["data"]["bank_account"] = {};
  // tokenizationConfig["data"]["bank_account"]["country"] = countryCode;
  // tokenizationConfig["data"]["bank_account"]["currency"] = currencyCode;
  // tokenizationConfig["data"]["bank_account"]["account_holder_name"] =
  //   context.params.accountName;
  // tokenizationConfig["data"]["bank_account"]["account_holder_type"] =
  //   "individual";
  // if (countryCode === "US" || countryCode === "BR") {
  //   tokenizationConfig["data"]["bank_account"]["routing_number"] =
  //     context.params.routingNumber;
  // }
  // tokenizationConfig["data"]["bank_account"]["account_number"] =
  //   context.params.accountNumber;
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // // } else {
  // //   finalTokenData['id'] = 1
  // // }
  // //------------------ Create Token End-------------------------------
  // const stripeCustomerId =
  //   context.params?.userData?.paymentGateWayCustomerId || null;
  // if (finalTokenData) {
  //   //------------------ Update Customer Token Start ---------------------------------
  //   customerConfig = {
  //     ...keyConfig,
  //     "data": {},
  //   };
  //   customerConfig["data"]["source"] = finalTokenData.id;
  //   // Stripe Call For create customer
  //   try {
  //     const stripe = require("stripe")(customerConfig.secretKey);
  //     finalCustomerData = await stripe.customers.update(
  //       stripeCustomerId,
  //       customerConfig.data
  //     );
  //   } catch (error) {
  //     return { "code": 400, "data": {}, "message": error.message };
  //   }
  //   //------------------ Update Customer Token End ---------------------------------
  //   // }
  // //------------- Update Account Start -----------------------------
  // customerConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // customerConfig["data"]["tos_acceptance"] = {};
  // customerConfig["data"]["tos_acceptance"]["date"] = 1609798905;
  // customerConfig["data"]["tos_acceptance"]["ip"] = context.params.clientIp; // "8.8.8.8";

  // // Stripe Call For create customer
  // try {
  //   const stripe = require("stripe")(customerConfig.secretKey);
  //   finalCustomerData = await stripe.accounts.update(
  //     stripeDestinationId,
  //     customerConfig.data
  //   );
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  //------------- Update Account End -----------------------------
  //------------- Transfer payment Start -----------------------------
  paymentConfig = {
    ...keyConfig,
    "data": {},
  };
  paymentConfig["data"]["amount"] =
    parseInt(parseFloat(context.params.amount)) * defaultCent;
  paymentConfig["data"]["currency"] = currencyCode;
  // paymentConfig["data"]["description"] = context.params.transactionReason;
  paymentConfig["data"]["method"] = "instant";
  paymentConfig["data"]["destination"] = bankAccountId; // "acct_1PZ8TcIPXiHK9MBJ"; // stripeDestinationId;
  // paymentConfig["data"]["transfer_group"] = "ORDER100";
  //Stripe Call For Transfer Amount
  try {
    // For Reference -->  https://docs.stripe.com/connect/instant-payouts#external-account-eligibility
    const stripe = require("stripe")(paymentConfig.secretKey);
    // finalPaymentData = await stripe.transfers.create(paymentConfig.data);
    finalPaymentData = await stripe.payouts.create(paymentConfig.data, {
      // "stripeAccount": context.params.userData.paymentGateWayCustomerId,
      "stripeAccount": stripeDestinationId,
    });
  } catch (error) {
    return {
      "code": 400,
      "data": {},
      "message": "Bank server Busy Now. Please try Later.",
    };
  }
  //------------- Transfer payment End -----------------------------
  // //------------- Payout Start Via Stripe connect BankA Count Start ---------------------------
  // customerConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // customerConfig["data"]["amount"] = parseInt(
  //   parseFloat(context.params.amount) * defaultCent
  // );
  // customerConfig["data"]["currency"] = currencyCode;
  // customerConfig["data"]["method"] = "standard"; // "instant";
  // customerConfig["data"]["destination"] = bankAccountId;
  // try {
  //   const stripe = require("stripe")(customerConfig.secretKey);
  //   finalCustomerData = await stripe.payouts.create(customerConfig.data);
  // } catch (error) {
  //   return {
  //     "code": 400,
  //     "data": {},
  //     "message": "Bank server Busy Now. Please try Later.",
  //   };
  // }
  // //------------- Payout Start Via Stripe connect BankA Count End -----------------------------
  // //------------- Payout Start -----------------------------
  // paymentConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // paymentConfig["data"]["amount"] = parseInt(
  //   parseFloat(context.params.amount) * defaultCent
  // );
  // paymentConfig["data"]["currency"] =
  //   context.generalSettings.data.currencyCode;
  // paymentConfig["data"]["method"] = "instant";
  // paymentConfig["data"]["stripeAccount"] =
  //   context.params.userData.stripeConnectAccountId;
  // try {
  //   const stripe = require("stripe")(paymentConfig.secretKey);
  //   const payout = await stripe.payouts.create(paymentConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": "invalid Account Details" };
  // }
  // //------------- Payout End -----------------------------
  // if (finalPaymentData.status === "succeeded") {
  if (finalPaymentData.id) {
    // Response
    responseData["authType"] = "NOAUTH";
    // responseData["paymentGateWayCustomerId"] = stripeDestinationId;
    responseData["transactionId"] = finalPaymentData.id;
    responseData["paymentIntentId"] = finalPaymentData.id;
    responseData["paymentIntentAmount"] = context.params.amount;
    responseData["transactionAmount"] = context.params.amount;
    responseData["paymentIntentClientSecret"] = finalPaymentData.client_secret;
    responseData["successResponse"] = finalPaymentData;
    responseData["status"] = constantUtils.SUCCESS; //finalPaymentData.status;
    return {
      "code": 200,
      "data": responseData,
      "message": "Valid Account Details",
    };
  } else {
    return { "code": 400, "data": {}, "message": "Invalid Account Details" };
  }
};

// transactionStripeEvents.newWithdrawalWalletTransaction = async function (
//   context
// ) {
//   let paymentConfig = {};
//   const responseData = {};
//   const finalPaymentData = {};
//   let finalTokenData = {};
//   let tokenizationConfig = {};
//   let finalCustomerData = {};
//   let customerConfig = {};
//   const keyConfig = {};
//   if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
//     keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
//   } else {
//     keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
//   }
//   //------------------ Create Token Start-------------------------------
//   // if (
//   //   !context.params.userData.embedtoken &&
//   //   context.params.userData.embedtoken == null
//   // ) {
//   tokenizationConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   const countryCode = context.params.country
//     ? context.params.country
//     : context.params.generalSettings.data.defaultCountryCode;
//   const currencyCode = context.params.generalSettings.data.currencyCode;
//   //
//   tokenizationConfig["data"]["bank_account"] = {};
//   tokenizationConfig["data"]["bank_account"]["country"] = countryCode;
//   tokenizationConfig["data"]["bank_account"]["currency"] = currencyCode;
//   tokenizationConfig["data"]["bank_account"]["account_holder_name"] =
//     context.params.accountName;
//   tokenizationConfig["data"]["bank_account"]["account_holder_type"] =
//     "individual";
//   if (countryCode === "US" || countryCode === "BR") {
//     tokenizationConfig["data"]["bank_account"]["routing_number"] =
//       context.params.routingNumber;
//   }
//   tokenizationConfig["data"]["bank_account"]["account_number"] =
//     context.params.accountNumber;
//   // Stripe Call For card tokenization
//   try {
//     const stripe = require("stripe")(tokenizationConfig.secretKey);
//     finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   // } else {
//   //   finalTokenData['id'] = 1
//   // }
//   //------------------ Create Token End-------------------------------
//   const stripeCustomerId =
//     context.params?.userData?.paymentGateWayCustomerId || null;
//   if (finalTokenData) {
//     //------------------ Update Customer Token Start ---------------------------------
//     customerConfig = {
//       ...keyConfig,
//       "data": {},
//     };
//     customerConfig["data"]["source"] = finalTokenData.id;
//     // Stripe Call For create customer
//     try {
//       const stripe = require("stripe")(customerConfig.secretKey);
//       finalCustomerData = await stripe.customers.update(
//         stripeCustomerId,
//         customerConfig.data
//       );
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//     //------------------ Update Customer Token End ---------------------------------
//     // }
//     // //------------- Transfer payment Start -----------------------------
//     // paymentConfig = {
//     //   ...keyConfig,
//     //   'data': {},
//     // }
//     // paymentConfig['data']['amount'] =
//     //   parseInt(parseFloat(context.params.amount)) * defaultCent
//     // paymentConfig['data']['currency'] = context.generalSettings.data.currencyCode
//     // paymentConfig['data']['destination'] =
//     //   context.params.userData.stripeConnectAccountId
//     // //Stripe Call For Transfer Amount
//     // try {
//     //   const stripe = require('stripe')(paymentConfig.secretKey)
//     //   finalPaymentData = await stripe.transfers.create(paymentConfig.data)
//     // } catch (error) {
//     //   return { 'code': 400, 'data': {}, 'message': 'invalid Account Details' }
//     // }
//     //   //------------- Transfer payment End -----------------------------
//     //------------- Payout Start -----------------------------
//     paymentConfig = {
//       ...keyConfig,
//       "data": {},
//     };
//     paymentConfig["data"]["amount"] = parseInt(
//       parseFloat(context.params.amount) * defaultCent
//     );
//     paymentConfig["data"]["currency"] =
//       context.generalSettings.data.currencyCode;
//     paymentConfig["data"]["method"] = "instant";
//     paymentConfig["data"]["stripeAccount"] =
//       context.params.userData.stripeConnectAccountId;
//     try {
//       const stripe = require("stripe")(paymentConfig.secretKey);
//       const payout = await stripe.payouts.create(paymentConfig.data);
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": "invalid Account Details" };
//     }
//     //------------- Payout End -----------------------------
//     // if (finalPaymentData.status === "succeeded") {
//     // Response
//     responseData["authType"] = "NOAUTH";
//     responseData["paymentGateWayCustomerId"] = stripeCustomerId;
//     responseData["transactionId"] = finalPaymentData.id;
//     responseData["paymentIntentId"] = finalPaymentData.id;
//     responseData["paymentIntentAmount"] = context.params.amount;
//     responseData["transactionAmount"] = context.params.amount;
//     responseData["paymentIntentClientSecret"] = finalPaymentData.client_secret;
//     responseData["successResponse"] = finalPaymentData;
//     responseData["status"] = finalPaymentData.status;
//     return {
//       "code": 200,
//       "data": responseData,
//       "message": "Valid Account Details",
//     };
//   } else {
//     return { "code": 400, "data": {}, "message": "Invalid Account Details" };
//   }
// };

transactionStripeEvents.createChargeCapture = async (context) => {
  const keyConfig = {};
  let finalPaymentData = {};
  let paymentConfig = {};
  if (context.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.paymentData["livePublicKey"];
  }
  //-------------- Create charges Start -----------------------
  paymentConfig = {
    ...keyConfig,
    "data": {},
  };
  paymentConfig["data"]["amount"] = parseInt(
    parseFloat(context.params.amount) * defaultCent
  );
  paymentConfig["data"]["currency"] = context.generalSettings.data.currencyCode;
  paymentConfig["data"]["customer"] = context.params.stripeCustomerId;
  paymentConfig["data"]["description"] = "Wallet Recharge";
  paymentConfig["data"]["capture"] = false;
  ////Stripe Call For create Charge
  const stripe = require("stripe")(paymentConfig.secretKey);
  finalPaymentData = await stripe.charges.create(paymentConfig.data);
  //-------------- Create charges End -----------------------
  return finalPaymentData;
};

transactionStripeEvents.chargeCapture = async (context) => {
  const keyConfig = {};
  let dataChargeCapture = {};
  let captureConfig = {};
  if (context.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.paymentData["livePublicKey"];
  }
  //-------------- charges charges Start -----------------------
  captureConfig = {
    ...keyConfig,
    "data": {},
  };
  captureConfig["charge"] = context.params.transactionId;
  // For capture payment
  const stripe = require("stripe")(captureConfig.secretKey);
  dataChargeCapture = await stripe.charges.capture(captureConfig.charge);
  //-------------- charges charges End -----------------------
  return dataChargeCapture;
};

transactionStripeEvents.refundsAmount = async function (context) {
  const keyConfig = {};
  const responseData = {};
  let dataRefunds = {};
  let refundsConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- refund payment Start -----------------------
  refundsConfig = {
    ...keyConfig,
    "data": {},
  };
  // refundsConfig["data"]["charge"] = context.params.transactionId.toString();
  refundsConfig["data"]["payment_intent"] =
    context.params.transactionId.toString();
  // capturedata['data']['reverse_transfer'] = true
  // capturedata['data']['refund_application_fee'] = true
  //For refund payment
  try {
    const striperefunds = require("stripe")(refundsConfig.secretKey);
    dataRefunds = await striperefunds.refunds.create(refundsConfig.data);
  } catch (error) {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
  //-------------- refund payment End -----------------------
  if (dataRefunds.status === "succeeded") {
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["transactionId"] = context.params.transactionId.toString();
    // dataRefunds['successResponse'] = finalPaymentData
    return { "code": 200, "data": responseData, "message": "Valid Card" };
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
};

transactionStripeEvents.tripEndCardCharge = async function (context) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let finalPaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  let customerConfig = {};
  let paymentConfig = {};
  const responseData = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // //------------------ Create Token Start-------------------------------
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // tokenizationConfig["data"]["card"] = {};
  // tokenizationConfig["data"]["card"]["number"] =
  //   context.params.cardData.data.cardNumber;
  // tokenizationConfig["data"]["card"]["exp_month"] =
  //   context.params.cardData.data.expiryMonth;
  // tokenizationConfig["data"]["card"]["exp_year"] =
  //   context.params.cardData.data.expiryYear;
  // tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // //------------------ Create Token End-------------------------------
  const stripeCustomerId = context.params.userData.stripeCustomerId;
  if (stripeCustomerId) {
    // cardBrand = finalTokenData?.card?.brand.toLowerCase();
    //------------------ Update Customer Token Start ---------------------------------
    customerConfig = {
      ...keyConfig,
      "data": {},
    };
    // customerConfig["data"]["source"] = finalTokenData.id;
    customerConfig["data"]["source"] = context.params.cardTokenId;
    // Stripe Call For create customer
    let finalCustomerData = {};
    try {
      const stripe = require("stripe")(customerConfig.secretKey);
      finalCustomerData = await stripe.customers.update(
        stripeCustomerId, //stripeCustomerId
        customerConfig.data
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //------------------ Update Customer Token End ---------------------------------

    //-------------- Create charges Start -----------------------
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    paymentConfig["data"]["amount"] = parseInt(
      parseFloat(context.params.amount) * defaultCent
    );
    paymentConfig["data"]["currency"] =
      context.params.generalSettings.data.currencyCode;
    // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
    paymentConfig["data"]["payment_method_types"] = ["card"];
    paymentConfig["data"]["capture_method"] = "automatic"; //automatic or manual
    paymentConfig["data"]["confirm"] = true;
    // paymentConfig['data']['confirmation_method'] = 'manual'
    // paymentConfig['data']['requires_capture'] = false
    // paymentConfig['data']['requires_action'] = false

    // paymentConfig['data']['automatic_payment_methods'] = true
    paymentConfig["data"]["customer"] = stripeCustomerId;
    paymentConfig["data"]["description"] = context.params.transactionReason;
    //Stripe Call For create Charge
    try {
      const stripe = require("stripe")(paymentConfig.secretKey);
      finalPaymentData = await stripe.paymentIntents.create(paymentConfig.data);
    } catch (error) {
      return {
        "code": 400,
        "data": {},
        "message": "Card Payment Failed",
      };
    }
    //-------------- Create charges End -----------------------
    //-------------- Response Data Start -----------------------
    if (finalPaymentData.status === "succeeded") {
      // Response
      responseData["authType"] = "NOAUTH";
      // responseData['transactionId'] = finalPaymentData.id.toString()
      responseData["transactionId"] =
        finalPaymentData?.charges?.data[0].id.toString();
      responseData["successResponse"] = finalPaymentData;
      return {
        "code": 200,
        "data": responseData,
        "message": "Card Payment Successfully",
      };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": "Card Payment Failed",
      };
    }
    //-------------- Response Data End -----------------------
  }
};

transactionStripeEvents.verifyBankAccount = async function (context) {
  let finalTokenData = {};
  let finalAccountData = {};
  let finalBankAccountData = {};
  let tokenizationConfig = {};
  // let customerConfig = {}
  // let paymentConfig = {}
  const keyConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // stripe(tokenizationConfig.secretKey)
  //------------------ Create Token Start-------------------------------
  tokenizationConfig = {
    ...keyConfig,
    "data": {},
  };
  const countryCode = context.params.generalSettings.data.defaultCountryCode;
  const currencyCode = context.params.generalSettings.data.currencyCode;
  //
  tokenizationConfig["data"]["bank_account"] = {};
  tokenizationConfig["data"]["bank_account"]["country"] = countryCode;
  tokenizationConfig["data"]["bank_account"]["currency"] = currencyCode;
  tokenizationConfig["data"]["bank_account"]["account_holder_name"] =
    context.params.bankDetails.accountName;
  tokenizationConfig["data"]["bank_account"]["account_holder_type"] =
    "individual";
  if (countryCode === "GB" || countryCode === "US" || countryCode === "BR") {
    tokenizationConfig["data"]["bank_account"]["routing_number"] =
      context.params.bankDetails.routingNumber;
  }
  tokenizationConfig["data"]["bank_account"]["account_number"] =
    context.params.bankDetails.accountNumber;
  // Stripe Call For card tokenization
  try {
    const stripe = require("stripe")(tokenizationConfig.secretKey);
    finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //------------------ Create Token End-------------------------------
  if (finalTokenData) {
    const accountJson = {
      ...keyConfig,
      "data": {
        "type": "custom",
        "country": countryCode,
        "business_type": "individual",
        // "requested_capabilities": [
        //   "card_payments",
        //   "transfers",
        //   "bank_transfer_payments",
        // ],
        "capabilities": {
          "card_payments": { "requested": true },
          "transfers": { "requested": true },
          "bank_transfer_payments": { "requested": true },
        },
        // "external_account": {
        //   "country": countryCode,
        //   "currency": currencyCode,
        //   "object": "bank_account",
        //   "account_holder_type": "individual",
        //   "account_number": context.params.accountNumber,
        //   "routing_number": context.params.routingNumber,
        //   "account_holder_name": context.params.accountName,
        // },
        "company": {
          "name": "",
          "address": {
            "line1": context.params.userData.line1 ?? "Address line1",
            "line2": context.params.userData.line2 ?? "Address line2",
            "city": context.params.userData.city ? context.params.city : "city",
            "country": countryCode,
            // "postal_code":
            //   context.params.userData.zipCode ||
            //   context.params.bankDetails.zipCode ||
            //   "600000",
            "state": context.params.userData.state ?? "state",
          },
        },
        "business_profile": {
          "url": "https://www.director.com",
          "mcc": "4722",
        }, // 4722-->Travel Agencies, Tour Operators
        "tos_acceptance": {
          "date": parseInt(Date.now() / 1000),
          "ip": context.params.clientIp || "8.8.8.8", //"60.243.36.44",
          "user_agent":
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
        },
        // "payouts_enabled": true,
        "individual": {
          "address": {
            "line1": context.params.userData.line1 ?? "Address line1",
            "line2": context.params.userData.line2 ?? "Address line2",
            "city": context.params.userData.city ? context.params.city : "city",
            "country": countryCode,
            "postal_code": "bs59sn",
            // "postal_code":
            //   context.params.userData.zipCode ||
            //   context.params.bankDetails.zipCode ||
            //   "600000",
            "state": context.params.userData.state ?? "state",
          },
          "dob": {
            "day": "01",
            "month": "01",
            "year": "2000",
          },
          "email": context.params.userData.email || "test@email.com",
          "first_name": context.params.userData.firstName ?? "New ",
          "last_name": context.params.userData.lastName ?? "User",
          "phone":
            context.params.userData.phone.code +
              "" +
              context.params.userData.phone.number ?? "",
        },
        // "settings": {
        //   "payouts": {
        //     "schedule": {
        //       "delay_days": 7,
        //       "interval": "daily",
        //     },
        //   },
        // },
        // "settings": {
        //   "payouts": {
        //     "schedule": {
        //       "delay_days": 1,
        //       "interval": "daily",
        //     },
        //   },
        // },
      },
    };
    //-----------
    // const accountJson = {
    //   ...keyConfig,
    //   "data": {
    //     "type": "custom",
    //     "country": countryCode,
    //     "capabilities": {
    //       "card_payments": { "requested": true },
    //       "transfers": { "requested": true },
    //     },
    //     "tos_acceptance": {
    //       "date": parseInt(Date.now() / 1000),
    //       "ip": context.params.clientIp, //"60.243.36.44",
    //       "user_agent":
    //         "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
    //     },
    //   },
    // };
    try {
      const stripe = require("stripe")(accountJson.secretKey);
      finalAccountData = await stripe.accounts.create(accountJson.data);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    // console.log('finalAccountData', finalAccountData)
    if (finalAccountData) {
      const extAcountJson = {
        ...keyConfig,
        "customer": finalAccountData.id,
        "data": { "external_account": finalTokenData.id },
      };
      try {
        const stripe = require("stripe")(extAcountJson.secretKey);
        finalBankAccountData = await stripe.accounts.createExternalAccount(
          extAcountJson.customer,
          extAcountJson.data
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
    }
    if (
      finalBankAccountData.status === "succeeded" ||
      finalBankAccountData.status === "new"
    ) {
      // Response
      responseData["status"] = "succeeded";
      responseData["authType"] = "NOAUTH";
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["embedtoken"] = finalTokenData.id;
      responseData["stripeConnectAccountId"] = finalAccountData.id;
      responseData["bankAccountId"] = finalBankAccountData.id;
      responseData["bankAccountToken"] = finalTokenData.id;
      // responseData['transactionId'] = finalPaymentData.id.toString()
      responseData["transactionReason"] = context.params.transactionReason;
      // responseData['successResponse'] = finalPaymentData
      return {
        "code": 200,
        "data": responseData,
        "message": "Valid Bank Details",
      };
    } else {
      return { "code": 400, "data": {}, "message": "Invalid Bank Details" };
    }
  } else {
    return { "code": 400, "data": {}, "message": "invalid Bank Details" };
  }
};

transactionStripeEvents.verifyBankAccountOldMethodBackup = async function (
  context
) {
  let finalTokenData = {};
  let finalCustomerData = {};
  let finalPaymentData = {};
  let tokenizationConfig = {};
  let customerConfig = {};
  let paymentConfig = {};
  const keyConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // stripe(tokenizationConfig.secretKey)
  //------------------ Create Token Start-------------------------------
  // if (
  //   !context.params.userData.embedtoken &&
  //   context.params.userData.embedtoken == null
  // ) {
  tokenizationConfig = {
    ...keyConfig,
    "data": {},
  };
  tokenizationConfig["data"]["bank_account"] = {};
  tokenizationConfig["data"]["bank_account"]["country"] = context.params.country
    ? context.params.country
    : context.params.generalSettings.data.defaultCountryCode;
  tokenizationConfig["data"]["bank_account"]["currency"] =
    context.params.generalSettings.data.currencyCode;
  tokenizationConfig["data"]["bank_account"]["account_holder_name"] =
    context.params.accountName;
  tokenizationConfig["data"]["bank_account"]["account_holder_type"] =
    "individual";
  tokenizationConfig["data"]["bank_account"]["routing_number"] =
    context.params.routingNumber;
  tokenizationConfig["data"]["bank_account"]["account_number"] =
    context.params.accountNumber;
  // Stripe Call For card tokenization
  try {
    const stripe = require("stripe")(tokenizationConfig.secretKey);
    finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  // } else {
  //   finalTokenData['id'] = 1
  // }
  //------------------ Create Token End-------------------------------
  if (finalTokenData) {
    responseData["cardToken"] = finalTokenData.id;
    if (
      !context.params.userData.stripeCustomerId &&
      context.params.userData.stripeCustomerId == null
    ) {
      //------------------ Create Customer Start ---------------------------------
      customerConfig = {
        ...keyConfig,
        "data": {},
      };
      customerConfig["data"]["name"] = context.params.holderName;
      customerConfig["data"]["email"] = context.params.email;
      customerConfig["data"]["address"] = {};
      customerConfig["data"]["address"]["city"] = context.params.city
        ? context.params.city
        : "city";
      customerConfig["data"]["address"]["line1"] = context.params.line1
        ? context.params.line1
        : "Address line 1";
      customerConfig["data"]["address"]["line2"] = context.params.line2
        ? context.params.line2
        : "Address line 2";
      customerConfig["data"]["address"]["postal_code"] = context.params.zipCode
        ? context.params.zipCode
        : "60000";
      customerConfig["data"]["address"]["state"] = context.params.state
        ? context.params.state
        : "state";
      customerConfig["data"]["address"]["country"] = context.params.country
        ? context.params.country
        : context.params.generalSettings.data.defaultCountryCode;
      customerConfig["data"]["description"] = context.params.description
        ? context.params.description
        : "Create New Customer";
      customerConfig["data"]["source"] = finalTokenData.id;
      // Stripe Call For create customer
      try {
        const stripe = require("stripe")(customerConfig.secretKey);
        finalCustomerData = await stripe.customers.create(customerConfig.data);
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      //------------------ Create Customer End ---------------------------------
      if (finalCustomerData) {
        //---------------- create Charge End--------------------------
        paymentConfig = {
          ...keyConfig,
          "data": {},
        };
        paymentConfig["data"]["amount"] = parseInt(
          parseFloat(context.params.minimumChargeAmount)
        ); // Defaule Card Verification deduction
        paymentConfig["data"]["currency"] =
          context.params.generalSettings.data.currencyCode;
        paymentConfig["data"]["customer"] = finalCustomerData.id;
        paymentConfig["data"]["description"] = context.params.transactionReason;

        //Stripe Call For create Charge
        try {
          const stripe = require("stripe")(paymentConfig.secretKey);
          finalPaymentData = await stripe.charges.create(paymentConfig.data);
        } catch (error) {
          return { "code": 400, "data": {}, "message": error.message };
        }
        //---------------- create Charge End--------------------------
        // Response
        responseData["authType"] = "NOAUTH";
        // responseData['stripeCardToken'] = finalTokenData.id
        responseData["embedtoken"] = finalTokenData.id;
        responseData["isDefault"] = true;
        responseData["stripeCustomerId"] = finalCustomerData.id;
        responseData["transactionId"] = finalPaymentData.id.toString();
        // responseData['paymentTransactionId'] = finalPaymentData.id.toString()
        responseData["successResponse"] = finalPaymentData;
        return { "code": 200, "data": responseData, "message": "Valid Card" };
      }
    } else {
      //------------------ Update Customer Token Start ---------------------------------
      customerConfig = {
        ...keyConfig,
        "data": {},
      };
      customerConfig["data"]["source"] = finalTokenData.id;
      // Stripe Call For create customer
      try {
        const stripe = require("stripe")(customerConfig.secretKey);
        finalCustomerData = await stripe.customers.update(
          context.params.userData.stripeCustomerId, //stripeCustomerId
          //   finalTokenData.id, //stripe Token
          customerConfig.data
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      //------------------ Update Customer Token End ---------------------------------
      if (finalCustomerData) {
        //---------------- create Charge End--------------------------
        paymentConfig = {
          ...keyConfig,
          "data": {},
        };
        paymentConfig["data"]["amount"] = parseInt(
          parseFloat(context.params.minimumChargeAmount)
        ); // Defaule Card Verification deduction
        paymentConfig["data"]["currency"] =
          context.params.generalSettings.data.currencyCode;
        paymentConfig["data"]["customer"] = finalCustomerData.id;
        paymentConfig["data"]["description"] = context.params.transactionReason;
        //Stripe Call For create Charge
        try {
          const stripe = require("stripe")(paymentConfig.secretKey);
          finalPaymentData = await stripe.charges.create(paymentConfig.data);
        } catch (error) {
          return { "code": 400, "data": {}, "message": error.message };
        }
        //---------------- create Charge End--------------------------
        // Response
        responseData["authType"] = "NOAUTH";
        // responseData['stripeCardToken'] = finalTokenData.id
        responseData["embedtoken"] = finalTokenData.id;
        responseData["stripeCustomerId"] = finalCustomerData.id;
        responseData["transactionId"] = finalPaymentData.id.toString();
        responseData["transactionReason"] = context.params.transactionReason;
        responseData["successResponse"] = finalPaymentData;
        return {
          "code": 200,
          "data": responseData,
          "message": "Valid Bank Details",
        };
      } else {
        return { "code": 400, "data": {}, "message": "Invalid Bank Details" };
      }
      //   //------------------ Create Customer End ---------------------------------
      //   responseData['authType'] = 'noauth'
      //   responseData['customerId'] = context.params.userData.stripeCustomerId
      //   return { 'code': 200, 'data': responseData, 'message': 'Valid Card' }
    }
  } else {
    return { "code": 400, "data": {}, "message": "invalid Bank Details" };
  }
};

// webhook
transactionStripeEvents.webhooking = async function (context) {
  let transactionStatus = {};
  let updateTransaction = {};
  let notificationObject = {};
  const eventType = context.params.type;
  const transactionId = context.params.data?.object?.id;

  switch (eventType) {
    case "account.updated":
    case "account.external_account.created":
    case "account.external_account.deleted":
    case "account.external_account.updated":
    case "balance.available":
    case "billing_portal.configuration.created":
    case "billing_portal.configuration.updated":
    case "capability.updated":
    case "charge.captured":
    case "charge.refunded":
    case "charge.succeeded":
    case "charge.updated":
    case "charge.dispute.closed":
    case "charge.dispute.created":
    case "charge.dispute.funds_reinstated":
    case "charge.dispute.funds_withdrawn":
    case "charge.dispute.updated":
    case "charge.refund.updated":
    case "coupon.created":
    case "coupon.deleted":
    case "coupon.updated":
    case "credit_note.created":
    case "credit_note.updated":
    case "credit_note.voided":
    case "customer.created":
    case "customer.deleted":
    case "customer.updated":
    case "customer.discount.created":
    case "customer.discount.deleted":
    case "customer.discount.updated":
    case "customer.source.created":
    case "customer.source.deleted":
    case "customer.source.updated":
    case "customer.subscription.created":
    case "customer.subscription.deleted":
    case "customer.subscription.pending_update_applied":
    case "customer.subscription.trial_will_end":
    case "customer.subscription.updated":
    case "customer.tax_id.created":
    case "customer.tax_id.deleted":
    case "customer.tax_id.updated":
    case "file.created":
    case "identity.verification_session.canceled":
    case "identity.verification_session.created":
    case "identity.verification_session.processing":
    case "identity.verification_session.requires_input":
    case "identity.verification_session.verified":
    case "invoice.created":
    case "invoice.deleted":
    case "invoice.finalized":
    case "invoice.marked_uncollectible":
    case "invoice.paid":
    case "invoice.payment_action_required":
    case "invoice.payment_succeeded":
    case "invoice.sent":
    case "invoice.upcoming":
    case "invoice.updated":
    case "invoice.voided":
    case "invoiceitem.created":
    case "invoiceitem.deleted":
    case "invoiceitem.updated":
    case "issuing_authorization.created":
    case "issuing_authorization.updated":
    case "issuing_card.created":
    case "issuing_card.updated":
    case "issuing_cardholder.created":
    case "issuing_cardholder.updated":
    case "issuing_dispute.closed":
    case "issuing_dispute.created":
    case "issuing_dispute.updated":
    case "issuing_transaction.created":
    case "issuing_transaction.updated":
    case "mandate.updated":
    case "order.created":
    case "order.payment_succeeded":
    case "order.updated":
    case "order_return.created":
    case "payment_intent.amount_capturable_updated":
    case "payment_intent.created":
    case "payment_intent.requires_action":
    case "payment_intent.succeeded":
    case "payment_link.created":
    case "payment_link.updated":
    case "payment_method.attached":
    case "payment_method.automatically_updated":
    case "payment_method.detached":
    case "payment_method.updated":
    case "payout.canceled":
    case "payout.created":
    case "payout.paid":
    case "payout.updated":
    case "person.created":
    case "person.deleted":
    case "person.updated":
    case "plan.created":
    case "plan.deleted":
    case "plan.updated":
    case "price.created":
    case "price.deleted":
    case "price.updated":
    case "product.created":
    case "product.deleted":
    case "product.updated":
    case "promotion_code.created":
    case "promotion_code.updated":
    case "quote.accepted":
    case "quote.canceled":
    case "quote.created":
    case "quote.finalized":
    case "radar.early_fraud_warning.created":
    case "radar.early_fraud_warning.updated":
    case "recipient.created":
    case "recipient.deleted":
    case "recipient.updated":
    case "reporting.report_run.succeeded":
    case "review.closed":
    case "review.opened":
    case "setup_intent.canceled":
    case "setup_intent.created":
    case "setup_intent.requires_action":
    case "setup_intent.succeeded":
    case "sigma.scheduled_query_run.created":
    case "sku.created":
    case "sku.deleted":
    case "sku.updated":
    case "source.canceled":
    case "source.chargeable":
    case "source.mandate_notification":
    case "source.refund_attributes_required":
    case "source.transaction.created":
    case "source.transaction.updated":
    case "subscription_schedule.aborted":
    case "subscription_schedule.canceled":
    case "subscription_schedule.completed":
    case "subscription_schedule.created":
    case "subscription_schedule.released":
    case "subscription_schedule.updated":
    case "tax_rate.created":
    case "tax_rate.updated":
    case "topup.canceled":
    case "topup.created":
    case "topup.reversed":
    case "topup.succeeded":
    case "transfer.created":
    case "transfer.paid":
    case "transfer.reversed":
    case "transfer.updated":
      transactionStatus = constantUtils.SUCCESS;
      break;
    case "charge.failed":
    case "invoice.finalization_failed":
    case "invoice.payment_failed":
    case "order.payment_failed":
    case "payment_intent.payment_failed":
    case "payout.failed":
    case "reporting.report_run.failed":
    case "setup_intent.setup_failed":
    case "source.failed":
    case "subscription_schedule.expiring":
    case "topup.failed":
    case "transfer.failed":
      transactionStatus = constantUtils.FAILED;
      break;
    case "charge.pending":
    case "issuing_dispute.funds_reinstated":
    case "issuing_dispute.submitted":
    case "payment_intent.canceled":
    case "payment_intent.processing":
      transactionStatus = constantUtils.PENDING;
      break;
    case "charge.expired":
    case "customer.source.expiring":
    case "customer.subscription.pending_update_expired":
      transactionStatus = constantUtils.FAILED;
      break;
    // ... handle other event types
    default:
      transactionStatus = constantUtils.FAILED;
      return null;
    // break
  }

  const transaction = await this.adapter.model.findOneAndUpdate(
    { "transactionId": transactionId },
    { "webhookresponse": context.params },
    {
      "new": true, //unique reference find in webhook
    }
  );

  if (transactionStatus === constantUtils.SUCCESS) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    return null;
  } else if (transactionStatus === constantUtils.FAILED) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    //-------------------------------------
    // reterive the amount to whose is the user or professional
    if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
      // retrn the amount of professional
      if (transaction.to.userType === constantUtils.PROFESSIONAL) {
        let userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        let walletupdated = await this.broker.emit(
          "professional.updateWalletAmount",
          {
            "professionalId": userData._id.toString(),
            "availableAmount":
              userData.wallet.availableAmount + transaction.transactionAmount,
            "freezedAmount": userData.wallet.freezedAmount,
          }
        );
        walletupdated = walletupdated && walletupdated[0];
        if (walletupdated.code !== 200) {
          throw new MoleculerError(
            "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED",
            500
          );
        }

        // send notification to professional
        let updatedtransaction = await this.adapter.model.findOne({
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        });
        updatedtransaction = updatedtransaction.toJSON();

        // remove not neede data for send notification

        //updated user detailes

        userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        //
        notificationObject = {
          "data": {
            "type": constantUtils.NOTIFICATIONTYPE,
            "userType": constantUtils.PROFESSIONAL,
            "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
            "timestamp": Date.now(),
            "message":
              updatedtransaction.webhookresponse.data.object.description,
            "details": lzStringEncode({
              "walletAmount": userData.wallet.availableAmount,
              "paymentType": updatedtransaction.paymentType,
              "transactionStatus": updatedtransaction.transactionStatus,
              "_id": updatedtransaction._id,
            }),
          },
          "registrationTokens": [
            {
              "token": userData.deviceInfo[0].deviceId,
              "id": userData._id.toString(),
              "deviceType": userData.deviceInfo[0].deviceType,
              "platform": userData.deviceInfo[0].platform,
              "socketId": userData.deviceInfo[0].socketId,
            },
          ],
        };
        console.log(notificationObject);
      }
      // USER
      if (transaction.to.userType === constantUtils.USER) {
        let userData = await this.broker.emit("user.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        let walletupdated = await this.broker.emit("user.updateWalletAmount", {
          "userId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount + transaction.transactionAmount,
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        });
        walletupdated = walletupdated && walletupdated[0];
        if (walletupdated.code !== 200) {
          throw new MoleculerError(
            "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED",
            500
          );
        }

        // send notification to professional
        let updatedtransaction = await this.adapter.model.findOne({
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        });
        updatedtransaction = updatedtransaction.toJSON();
        //updated user detailes

        userData = await this.broker.emit("user.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        //
        notificationObject = {
          "data": {
            "type": constantUtils.NOTIFICATIONTYPE,
            "userType": constantUtils.USER,
            "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
            "timestamp": Date.now(),
            "message":
              updatedtransaction.webhookresponse.data.object.description,
            "details": lzStringEncode({
              "walletAmount": userData.wallet.availableAmount,
              "paymentType": updatedtransaction.paymentType,
              "transactionStatus": updatedtransaction.transactionStatus,
              "_id": updatedtransaction._id,
            }),
          },
          "registrationTokens": [
            {
              "token": userData.deviceInfo[0].deviceId,
              "id": userData._id.toString(),
              "deviceType": userData.deviceInfo[0].deviceType,
              "platform": userData.deviceInfo[0].platform,
              "socketId": userData.deviceInfo[0].socketId,
            },
          ],
        };
        console.log(notificationObject);
      }
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
      return null;
    }
  } else if (transactionStatus === constantUtils.PENDING) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    return null;
  }
  return null;
};

transactionStripeEvents.createPaymentIntents = async function (context) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let finalPaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  let customerConfig = {};
  let paymentConfig = {};
  const responseData = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // //------------------ Create Token Start-------------------------------
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // tokenizationConfig["data"]["card"] = {};
  // tokenizationConfig["data"]["card"]["number"] =
  //   context.params.cardData.data.cardNumber;
  // tokenizationConfig["data"]["card"]["exp_month"] =
  //   context.params.cardData.data.expiryMonth;
  // tokenizationConfig["data"]["card"]["exp_year"] =
  //   context.params.cardData.data.expiryYear;
  // if (context.params.cardData.data.isCVVRequired) {
  //   tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
  // }
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // //------------------ Create Token End-------------------------------
  const stripeCustomerId = context.params.userData.paymentGateWayCustomerId;
  if (stripeCustomerId) {
    // cardBrand = finalTokenData?.card?.brand.toLowerCase();
    //------------------ Update Customer Token Start ---------------------------------
    customerConfig = {
      ...keyConfig,
      "data": {},
    };
    // customerConfig["data"]["source"] = finalTokenData.id;
    customerConfig["data"]["source"] = context.params.cardTokenId;
    // Stripe Call For create customer
    let finalCustomerData = {};
    try {
      const stripe = require("stripe")(customerConfig.secretKey);
      finalCustomerData = await stripe.customers.update(
        stripeCustomerId, //stripeCustomerId
        customerConfig.data
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //------------------ Update Customer Token End ---------------------------------

    //-------------- Create charges Start -----------------------
    const estimationAmount =
      parseFloat(context.params.estimationAmount) * defaultCent;
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    paymentConfig["data"]["amount"] = parseInt(estimationAmount);
    paymentConfig["data"]["currency"] =
      context.params.generalSettings.data.currencyCode;
    // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
    paymentConfig["data"]["payment_method_types"] = ["card"];
    paymentConfig["data"]["capture_method"] = "manual"; //automatic or manual
    paymentConfig["data"]["confirm"] = true;
    // paymentConfig['data']['confirmation_method'] = 'manual'
    // paymentConfig['data']['requires_capture'] = false
    // paymentConfig['data']['requires_action'] = false

    // paymentConfig['data']['automatic_payment_methods'] = true
    paymentConfig["data"]["customer"] = stripeCustomerId;
    paymentConfig["data"]["description"] = context.params.transactionReason;
    //Stripe Call For create Charge
    try {
      const stripe = require("stripe")(paymentConfig.secretKey);
      finalPaymentData = await stripe.paymentIntents.create(paymentConfig.data);
    } catch (error) {
      return {
        "code": 400,
        "data": {},
        "message": "Card Payment Failed",
      };
    }
    //-------------- Create charges End -----------------------
    //-------------- Response Data Start -----------------------
    if (finalPaymentData) {
      // Response
      responseData["authType"] = "NOAUTH";
      responseData["transactionId"] = finalPaymentData.id;
      responseData["transactionAmount"] = context.params.estimationAmount;
      responseData["paymentIntentId"] = finalPaymentData.id;
      responseData["paymentIntentClientSecret"] =
        finalPaymentData.client_secret;
      responseData["successResponse"] = finalPaymentData;
      responseData["status"] = finalPaymentData.status;
      return {
        "code": 200,
        "data": responseData,
        "message": "Card Payment Initiated",
      };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": "Card Payment Failed",
      };
    }
    //-------------- Response Data End -----------------------
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Invalid Stripe Customer",
    };
  }
};

transactionStripeEvents.capturePaymentIntentsTransaction = async function (
  context
) {
  const responseData = {};
  let dataChargeCapture = {};
  let captureConfig = {};
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- charges payment Intents Start -----------------------
  captureConfig = {
    ...keyConfig,
    "data": {},
  };
  captureConfig["data"]["charge"] = context.params?.paymentInitId;
  captureConfig["data"]["amount_to_capture"] = parseInt(
    parseFloat(context.params.captureAmount) * defaultCent
  );
  try {
    const stripe = require("stripe")(captureConfig.secretKey);
    dataChargeCapture = await stripe.paymentIntents.capture(
      captureConfig.data.charge,
      { "amount_to_capture": captureConfig.data.amount_to_capture }
    );
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- charges payment Intents End -----------------------
  // Response
  if (dataChargeCapture.status === "succeeded") {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["stripeCustomerId"] = context.params.stripeCustomerId;
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] =
      dataChargeCapture?.charges?.data[0].id.toString();
    responseData["successResponse"] = dataChargeCapture;
    return { "code": 200, "data": responseData, "message": "Amount Captured" };
  } else {
    return { "code": 400, "data": {}, "message": "Transaction Failed" };
  }
};

transactionStripeEvents.cancelCapturePaymentIntents = async function (context) {
  const responseData = {};
  let dataChargeCapture = {};
  let captureConfig = {};
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- Cancel Capture payment Intents Start -----------------------
  captureConfig = {
    ...keyConfig,
    "data": {},
  };
  captureConfig["data"]["charge"] = context.params?.paymentInitId;
  try {
    const stripe = require("stripe")(captureConfig.secretKey);
    dataChargeCapture = await stripe.paymentIntents.cancel(
      captureConfig.data.charge
    );
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Cancel Capture payment Intents End -----------------------
  // Response
  //if (dataChargeCapture.status === "canceled") {
  if (dataChargeCapture) {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["stripeCustomerId"] = context.params.stripeCustomerId;
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] =
      dataChargeCapture?.charges?.data[0].id.toString();
    responseData["successResponse"] = dataChargeCapture;
    return { "code": 200, "data": responseData, "message": "Amount Captured" };
  } else {
    return { "code": 400, "data": {}, "message": "Transaction Failed" };
  }
};

transactionStripeEvents.cancelCaptureAndCreatePaymentTransaction =
  async function (context) {
    const keyConfig = {};
    // let cardBrand = "visa";
    let finalPaymentData = {};
    let dataChargeCapture = {};
    // let finalTokenData = {};
    // let tokenizationConfig = {};
    let customerConfig = {};
    let paymentConfig = {};
    let captureConfig = {};
    const responseData = {};
    if (
      context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX
    ) {
      keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
    } else {
      keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
    }
    //-------------- Cancel Capture payment Intents Start -----------------------
    captureConfig = {
      ...keyConfig,
      "data": {},
    };
    captureConfig["data"]["charge"] = context.params?.paymentInitId;
    try {
      const stripe = require("stripe")(captureConfig.secretKey);
      dataChargeCapture = await stripe.paymentIntents.cancel(
        captureConfig.data.charge
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //-------------- Cancel Capture payment Intents End -----------------------
    // //------------------ Create Token Start-------------------------------
    // tokenizationConfig = {
    //   ...keyConfig,
    //   "data": {},
    // };
    // tokenizationConfig["data"]["card"] = {};
    // tokenizationConfig["data"]["card"]["number"] =
    //   context.params.cardData.data.cardNumber;
    // tokenizationConfig["data"]["card"]["exp_month"] =
    //   context.params.cardData.data.expiryMonth;
    // tokenizationConfig["data"]["card"]["exp_year"] =
    //   context.params.cardData.data.expiryYear;
    // tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
    // // Stripe Call For card tokenization
    // try {
    //   const stripe = require("stripe")(tokenizationConfig.secretKey);
    //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
    // } catch (error) {
    //   return { "code": 400, "data": {}, "message": error.message };
    // }
    // //------------------ Create Token End-------------------------------
    const stripeCustomerId = context.params.userData.stripeCustomerId;
    if (stripeCustomerId) {
      // cardBrand = finalTokenData?.card?.brand.toLowerCase();
      //------------------ Update Customer Token Start ---------------------------------
      customerConfig = {
        ...keyConfig,
        "data": {},
      };
      // customerConfig["data"]["source"] = finalTokenData.id;
      customerConfig["data"]["source"] = context.params.cardTokenId;
      // Stripe Call For create customer
      let finalCustomerData = {};
      try {
        const stripe = require("stripe")(customerConfig.secretKey);
        finalCustomerData = await stripe.customers.update(
          stripeCustomerId, //stripeCustomerId
          customerConfig.data
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      //------------------ Update Customer Token End ---------------------------------

      //-------------- Create charges Start -----------------------
      paymentConfig = {
        ...keyConfig,
        "data": {},
      };
      paymentConfig["data"]["amount"] = parseInt(
        parseFloat(context.params.paymentInitAmount) * defaultCent
      );
      paymentConfig["data"]["currency"] =
        context.params.generalSettings.data.currencyCode;
      // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
      paymentConfig["data"]["payment_method_types"] = ["card"];
      paymentConfig["data"]["capture_method"] = "automatic"; //automatic or manual
      paymentConfig["data"]["confirm"] = true;
      // paymentConfig['data']['confirmation_method'] = 'manual'
      // paymentConfig['data']['requires_capture'] = false
      // paymentConfig['data']['requires_action'] = false

      // paymentConfig['data']['automatic_payment_methods'] = true
      paymentConfig["data"]["customer"] = stripeCustomerId;
      paymentConfig["data"]["description"] = context.params.transactionReason;
      //Stripe Call For create Charge
      try {
        const stripe = require("stripe")(paymentConfig.secretKey);
        finalPaymentData = await stripe.paymentIntents.create(
          paymentConfig.data
        );
      } catch (error) {
        return {
          "code": 400,
          "data": {},
          "message": "Card Payment Failed",
        };
      }
      //-------------- Create charges End -----------------------
      //-------------- Response Data Start -----------------------
      if (finalPaymentData.status === "succeeded") {
        // Response
        responseData["authType"] = "NOAUTH";
        // responseData['transactionId'] = finalPaymentData.id.toString()
        responseData["transactionId"] =
          finalPaymentData?.charges?.data[0].id.toString();
        responseData["successResponse"] = finalPaymentData;
        return {
          "code": 200,
          "data": responseData,
          "message": "Card Payment Successfully",
        };
      } else {
        return {
          "code": 400,
          "data": {},
          "message": "Card Payment Failed",
        };
      }
      //-------------- Response Data End -----------------------
    }
  };

transactionStripeEvents.updateCaptureAmountAndCapturePaymentTransaction =
  async function (context) {
    const keyConfig = {};
    // let cardBrand = "visa";
    let finalPaymentData = {};
    let dataChargeCapture = {};
    // let finalTokenData = {};
    // let tokenizationConfig = {};
    // let customerConfig = {};
    let paymentConfig = {};
    let captureConfig = {};
    const responseData = {};
    if (
      context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX
    ) {
      keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
    } else {
      keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
      keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
    }
    //-------------- Cancel Capture payment Intents Start -----------------------
    const paymentIntentId = context.params?.paymentInitId || null;
    const paymentInitAmount = parseInt(
      parseFloat(context.params.paymentInitAmount) * defaultCent
    );
    if (paymentIntentId) {
      captureConfig = {
        ...keyConfig,
        "data": {},
      };
      // captureConfig["data"]["amount"] = context.params?.paymentInitAmount;
      try {
        const stripe = require("stripe")(captureConfig.secretKey);
        dataChargeCapture = await stripe.paymentIntents.update(
          paymentIntentId,
          // captureConfig.data.amount
          { "amount": paymentInitAmount }
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      //------------------ Create Token End-------------------------------
      // if (context.params.userData.stripeCustomerId) {
      //-------------- Create charges Start -----------------------
      paymentConfig = {
        ...keyConfig,
        "data": {},
      };
      //Stripe Call For create Charge
      try {
        const stripe = require("stripe")(paymentConfig.secretKey);
        finalPaymentData = await stripe.paymentIntents.confirm(paymentIntentId);
      } catch (error) {
        return {
          "code": 400,
          "data": {},
          "message": "Card Payment Failed",
        };
      }
      //-------------- Create charges End -----------------------
      //-------------- Response Data Start -----------------------
      if (finalPaymentData.status === "succeeded") {
        // Response
        responseData["authType"] = "NOAUTH";
        // responseData['transactionId'] = finalPaymentData.id.toString()
        responseData["transactionId"] = paymentIntentId;
        responseData["paymentIntentId"] = paymentIntentId;
        responseData["successResponse"] = finalPaymentData;
        return {
          "code": 200,
          "data": responseData,
          "message": "Card Payment Successfully",
        };
      } else {
        return {
          "code": 400,
          "data": {},
          "message": "Card Payment Failed",
        };
      }
      //-------------- Response Data End -----------------------
      // }
    } else {
      return {
        "code": 400,
        "data": {},
        "message": "Card Payment Failed",
      };
    }
  };

//--------------------------------------
module.exports = transactionStripeEvents;
