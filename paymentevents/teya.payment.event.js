//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const axios = require("axios");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const fs = require("fs");
const path = require("path");
const https = require("https");
// const { base64Encode } = require("../utils/crypto.util");
const { randomString } = require("../utils/helper.util");
// import { MercadoPagoConfig, Payment } from "mercadopago";
// const {
//   MercadoPagoConfig,
//   Payment,
//   Customer,
//   CustomerCard,
//   CardToken,
// } = require("mercadopago");

const notifyMessage = require("../mixins/notifyMessage.mixin");

const defaultCent = 100;
//----------------- Need to Remove End --------------------

const transactionTeyaEvents = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    const successData = {
      "code": response.status,
      "config": config,
      "data": response.data,
      "status": response.data?.status,
      "message": response.statusText,
    };
    // try {
    //   await axios({
    //     "method": "GET",
    //     "port": 443,
    //     "url": `https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec`,
    //     "headers": {
    //       "Content-Type": "application/json",
    //     },
    //     "data": { "APIStatus": "success", "gatwayResponse": successData },
    //   });
    // } catch (e) {}
    return successData;
  } catch (error) {
    const errorData = {
      "code": error?.response?.status,
      // "config": config,
      "data": error?.response?.data, // || JSON.stringify(error) || {},
      "status": "failed",
      "message":
        error?.response?.data?.Error ||
        error?.response?.data?.errors?.[0] ||
        error?.response?.data?.message ||
        error.message,
    };
    // try {
    //   await axios({
    //     "method": "GET",
    //     "port": 443,
    //     "url": `https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec`,
    //     "headers": {
    //       "Content-Type": "application/json",
    //     },
    //     "data": {
    //       "APIStatus": "failed",
    //       "gatwayResponse": errorData,
    //     },
    //   });
    // } catch (e) {
    //   const x = "sss";
    // }
    return errorData;
  }
};

transactionTeyaEvents.checkValidCard = async function (context) {
  const keyConfig = {};
  const responseData = {};

  const { INFO_TRANSACTIONS_SUCCESS, INFO_INVALID_DETAILS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const isCVVRequired = context.params?.paymentData["isCVVRequired"] || false;
  //------------------------------
  let redirectUrl = null,
    cardToken = null,
    threeDSCardData = null,
    multiTokenData = null,
    statusCode = 400,
    status = constantUtils.FAILED,
    message = INFO_INVALID_DETAILS;
  //------------------------------
  let paymentGateWayCustomerId =
    context.params.paymentGateWayCustomerId || null;
  //------------------------------
  if (!paymentGateWayCustomerId) {
    // const base64EncodeData = base64Encode(keyConfig.privateKey);
    // const base64EncodeData = keyConfig.privateKey;
    const base64EncodeData = Buffer.from(
      keyConfig.privateKey + ":" + ""
    ).toString("base64");
    try {
      const accessTokenConfigParams = {
        "method": "POST",
        "port": 443,
        "url": `${keyConfig.baseUrl}api/mpi/v2/enrollment`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Basic " + base64EncodeData,
        },
        "data": {
          "CardDetails": {
            "PaymentType": "Card",
            "PAN": context.params.cardNumber,
            "ExpMonth": context.params.expiryMonth,
            "ExpYear": context.params.expiryYear,
            //"CVC2": "111",
          },
          "PurchAmount": 0, //By Default will Not Change the value
          "Currency": "352", // 352 --> ISK
          "Exponent": 0,
          // "TermUrl":
          //   context.params.generalSettings?.data?.redirectUrls
          //     ?.paymentRedirectApiUrl + "/teya/webhook",
          "TermUrl":
            "https://webhook.site/17d9d432-ed63-4e00-a9d0-57f0539fada1",
          // "MD": "My MD 1",
          "Description": context.params?.transactionReason
            ?.replace(/\//g, " ")
            ?.replace("|", ""),
          "TDS2ThreeDSMethodNotificationURL":
            context.params.generalSettings?.data?.redirectUrls
              ?.paymentRedirectApiUrl + "/teya/webhook",
        },
      };
      threeDSCardData = await axiosConfig(accessTokenConfigParams);
      if (threeDSCardData.code === 200) {
        threeDSCardData["action"] = constantUtils.SUCCESS;
        redirectUrl = threeDSCardData.data.RedirectToACSForm;
        paymentGateWayCustomerId = null;
        // paymentGateWayCustomerId =
        //   threeDSCardData?.data?.MpiToken?.toString() ||
        //   threeDSCardData?.data?.MPIToken?.toString();
        //--------------------------------------
        const multiTokenConfigParams = {
          "method": "POST",
          "port": 443,
          "url": `${keyConfig.baseUrl}api/token/multi`,
          "headers": {
            "Content-Type": "application/json",
            "Authorization": "Basic " + base64EncodeData,
          },
          "data": {
            //"TokenSingle": "string",
            "PAN": context.params.cardNumber,
            "ExpMonth": context.params.expiryMonth,
            "ExpYear": context.params.expiryYear,
            "VerifyCard": {
              "CheckAmount": 0, //By Default will Not Change the value
              "Currency": "352", //352 --> ISK,
              "CVC": context.params.cvv,
            },
            "Metadata": {
              "Payload": "Card Payment",
            },
          },
        };
        multiTokenData = await axiosConfig(multiTokenConfigParams);
        if (
          (multiTokenData.code === 200 || multiTokenData.code === 201) &&
          multiTokenData.data.VerifyCardResult?.ActionCode?.toString() === "000"
        ) {
          multiTokenData["action"] = constantUtils.SUCCESS;
          cardToken = multiTokenData.data.Token?.toString();
        }
      } else {
        // paymentGateWayCustomerId = threeDSCardData?.data?.id?.toString();
        paymentGateWayCustomerId = null;
      }
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    statusCode = threeDSCardData?.action === multiTokenData?.action ? 200 : 400;
    status =
      threeDSCardData?.action === multiTokenData?.action
        ? constantUtils.SUCCESS
        : constantUtils.FAILED;
    message =
      threeDSCardData?.action === multiTokenData?.action
        ? INFO_TRANSACTIONS_SUCCESS
        : INFO_INVALID_DETAILS;
  }
  //------------------------------
  return {
    "code": statusCode,
    "data": {
      "authType": "NOAUTH",
      "isDefault": true, //need to Change Dynamic
      "isCVVRequired": isCVVRequired,
      "cardToken": cardToken, //TokenId
      "embedtoken": cardToken, //TokenId
      "successResponse": multiTokenData, // finalPaymentData,
      "transactionAmount": 0, //By Default will Not Change the value
      "transactionId": null, // finalPaymentData["id"],
      "paymentGateWayCustomerId": paymentGateWayCustomerId,
      "paymentIntentId": null, // finalPaymentData["id"],
      "paymentIntentClientSecret": null,
      "redirectUrl": redirectUrl,
      "redirectMethodType": "HTML",
      "status": status,
    },
    "message": message,
  };
};

transactionTeyaEvents.newRechargeWalletTransaction = async function (context) {
  let finalPaymentData = {},
    redirectUrl = null,
    redirectMethodType = null;
  const responseData = {};
  const keyConfig = {};

  const {
    INFO_TRANSACTIONS_SUCCESS,
    ERROR_IN_TRANSACTIONS,
    WARNING_BANK_SERVER_BUSY,
  } = await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  try {
    const base64EncodeData = Buffer.from(
      keyConfig.privateKey + ":" + ""
    ).toString("base64");
    //-------
    const Amount = parseInt(parseFloat(context.params.amount) * defaultCent);
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}api/payment`,
      "headers": {
        "Content-Type": "application/json",
        "Authorization": "Basic " + base64EncodeData,
      },
      "data": {
        "TransactionType": "Sale",
        "Amount": Amount,
        "Currency": "352", // 352 --> ISK,
        "TransactionDate": new Date(),
        "OrderId": "INTEGR" + randomString(6, "#"),
        "PaymentMethod": {
          "PaymentType": "TokenMulti",
          "Token": context.params.cardData.data.embedtoken,
        },
        "Metadata": {
          "Payload": "Card Payment",
        },
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  if (
    finalPaymentData?.code === 201 &&
    finalPaymentData.data?.ActionCode?.toString() === "000"
  ) {
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["paymentGateWayCustomerId"] = null;
    responseData["transactionId"] = finalPaymentData.data.TransactionId;
    responseData["paymentIntentId"] = finalPaymentData.data.TransactionId;
    responseData["paymentIntentAmount"] = context.params.amount;
    responseData["transactionAmount"] = context.params.amount;
    responseData["paymentIntentClientSecret"] = null;
    responseData["redirectUrl"] = null;
    responseData["redirectMethodType"] = null;
    responseData["successResponse"] = finalPaymentData.data;
    responseData["status"] = finalPaymentData.data.TransactionStatus;
    return {
      "code": 200,
      "data": responseData,
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": WARNING_BANK_SERVER_BUSY,
    };
  }
};

transactionTeyaEvents.tripEndCardCharge = async function (context) {
  const keyConfig = {};
  let finalPaymentData = {};
  const responseData = {};

  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const amount = parseInt(parseFloat(context.params.amount) * defaultCent);
  try {
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}api/payment`,
      "headers": {
        "Content-Type": "application/json",
        "Authorization": "Basic " + base64EncodeData,
      },
      "data": {
        "TransactionType": "Sale",
        "Amount": amount,
        "Currency": "352", // 352 --> ISK,
        "TransactionDate": new Date(),
        "OrderId": "INTEGR" + randomString(6, "#"),
        "PaymentMethod": {
          "PaymentType": "TokenMulti",
          "Token": context.params.cardData.data.embedtoken,
        },
        "Metadata": {
          "Payload": "Card Payment",
        },
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Create charges End -----------------------
  //-------------- Response Data Start -----------------------
  if (
    finalPaymentData?.code === 201 &&
    finalPaymentData.data?.ActionCode?.toString() === "000"
  ) {
    // Response
    responseData["authType"] = "NOAUTH";
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] =
      finalPaymentData?.data.TransactionId.toString();
    responseData["successResponse"] = finalPaymentData;
    return {
      "code": 200,
      "data": responseData,
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
  //-------------- Response Data End -----------------------
};

// transactionTeyaEvents.verifyBankAccount = async function (context) {
//   const responseData = {};
//   let finalBankAccountData = {},
//     httpsAgent = null;
//   const certificatesPath = path.resolve(
//     __dirname,
//     "../mTLSCertificates/delbank_mTLS_certificado_flixmobi.pem"
//   );
//   const keyPath = path.resolve(__dirname, "../mTLSCertificates/flixmobi.key");
//   const countryCode = context.params.generalSettings.data.defaultDialCode;
//   let pixkeyNumber = context.params.bankDetails?.pixkeyNumber?.toString();
//   //
//   const keyConfig = {};
//   if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
//     keyConfig["baseUrl"] = "https://apisandbox.delbank.com.br/";
//     keyConfig["publishableKey"] = context.params.paymentData["testAccountNo"]; // delbank API Key
//     //
//     // httpsAgent = new https.Agent({});
//     pixkeyNumber = pixkeyNumber.replace(countryCode, "");
//   } else {
//     keyConfig["baseUrl"] = "https://api.delbank.com.br/";
//     keyConfig["publishableKey"] = context.params.paymentData["liveAccountNo"]; // delbank API Key
//     //
//     if (fs.existsSync(certificatesPath) && fs.existsSync(keyPath)) {
//       httpsAgent = new https.Agent({
//         key: fs.readFileSync(keyPath, "utf8"),
//         cert: fs.readFileSync(certificatesPath, "utf8"),
//       });
//     } else {
//       return { "code": 400, "data": {}, "message": "Invalid Access" };
//     }
//     // if (!pixkeyNumber.startsWith(countryCode)) {
//     //   pixkeyNumber = countryCode + pixkeyNumber;
//     // }
//   }
//   //------------- Transfer payment Start -----------------------------
//   // //---------------------------------
//   // let data = JSON.stringify({
//   //   "eventType": "PIX_PAYMENT_UPDATED",
//   //   // "url": "https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec",
//   //   "url": "https://api.flixmobi.net/api/transaction/mercado/webhook",
//   //   "authorizationScheme": "NONE",
//   //   "authorization": "NONE",
//   //   "status": "PIX_EFFECTIVE",
//   // });
//   // let config = {
//   //   "method": "get",
//   //   "port": 443,
//   //   "url": "https://api.delbank.com.br/baas/api/v1/charges/webhooks",
//   //   "httpsAgent": httpsAgent,
//   //   "headers": {
//   //     "IdempotencyKey": "fliximobi_" + Date.now().toString(),
//   //     "x-delbank-api-key": keyConfig.publishableKey,
//   //   },
//   //   "data": data,
//   // };
//   // const res = await axiosConfig(config);
//   // //------------------------------
//   try {
//     const paymentInitializationParams = {
//       "method": "POST",
//       "port": 443,
//       "url": `${keyConfig.baseUrl}baas/api/v2/pix/dict/payment-initialization`,
//       "httpsAgent": httpsAgent,
//       "headers": {
//         "IdempotencyKey": "fliximobi_" + Date.now().toString(),
//         "accept": "application/json",
//         "Content-Type": "application/json",
//         "x-delbank-api-key": keyConfig.publishableKey,
//       },
//       "data": {
//         "key": pixkeyNumber,
//       },
//     };
//     finalBankAccountData = await axiosConfig(paymentInitializationParams);
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   if (finalBankAccountData.code === 200) {
//     if (
//       finalBankAccountData?.data?.beneficiary?.number ===
//       context.params.bankDetails.accountNumber
//     ) {
//       // Response
//       responseData["status"] = "succeeded";
//       responseData["authType"] = "NOAUTH";
//       responseData["accountholderName"] =
//         finalBankAccountData?.data?.beneficiary?.holder?.name;
//       responseData["bankName"] =
//         finalBankAccountData?.data?.beneficiary?.participant?.name;
//       responseData["ispb"] =
//         finalBankAccountData?.data?.beneficiary?.participant?.ispb;
//       responseData["bankAccountDocumentNo"] =
//         finalBankAccountData?.data?.beneficiary?.holder?.document;
//       return {
//         "code": 200,
//         "data": responseData,
//         "message": "Valid Bank Details",
//       };
//     } else {
//       return { "code": 400, "data": {}, "message": null }; //*** Must Pass null Value in message Key
//     }
//   } else {
//     return {
//       "code": 400,
//       "data": {},
//       "message": finalBankAccountData.message || "Invalid Bank Details",
//     };
//   }
// };

transactionTeyaEvents.createPaymentIntents = async function (context) {
  const keyConfig = {};
  let finalPaymentData = {};
  const responseData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  try {
    const base64EncodeData = Buffer.from(
      keyConfig.privateKey + ":" + ""
    ).toString("base64");
    //-------
    const Amount = parseInt(
      parseFloat(context.params.estimationAmount) * defaultCent
    );
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}api/payment`,
      "headers": {
        "Content-Type": "application/json",
        "Authorization": "Basic " + base64EncodeData,
      },
      "data": {
        "TransactionType": "PreAuthorization",
        "Amount": Amount,
        "Currency": "352", // 352 --> ISK,
        "TransactionDate": new Date(),
        "OrderId": "INTEGR" + randomString(6, "#"),
        "PaymentMethod": {
          "PaymentType": "TokenMulti",
          "Token": context.params.cardData.data.embedtoken,
        },
        "Metadata": {
          "Payload": "Card Payment",
        },
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Response Data Start -----------------------
  if (
    finalPaymentData?.code === 201 &&
    finalPaymentData.data?.ActionCode?.toString() === "000"
  ) {
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["transactionId"] = finalPaymentData.data.TransactionId;
    responseData["transactionAmount"] = context.params.estimationAmount;
    responseData["paymentIntentId"] = finalPaymentData.data.TransactionId;
    responseData["paymentIntentClientSecret"] = null;
    responseData["successResponse"] = finalPaymentData.data;
    responseData["status"] = finalPaymentData.data.TransactionStatus;
    return {
      "code": 200,
      "data": responseData,
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
  //-------------- Response Data End -----------------------
};

// transactionTeyaEvents.capturePaymentIntentsTransaction = async function (
//   context
// ) {
//   const responseData = {};
//   let dataChargeCapture = {};
//   const keyConfig = {};

//   const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
//     await notifyMessage.setNotifyLanguage(context.params.langCode);

//   if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
//     keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
//     keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
//   } else {
//     keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
//     keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
//   }
//   //-------------- charges payment Intents Start -----------------------
//   try {
//     const base64EncodeData = Buffer.from(
//       keyConfig.privateKey + ":" + ""
//     ).toString("base64");
//     //-------
//     const rideAmount = parseInt(
//       parseFloat(context.params.captureAmount) * defaultCent
//     );
//     const accessTokenConfigParams = {
//       "method": "PUT",
//       "port": 443,
//       "url": `${keyConfig.baseUrl}api/payment/${context.params?.paymentInitId}/capture`,
//       "headers": {
//         "Content-Type": "application/json",
//         "Authorization": "Basic " + base64EncodeData,
//       },
//       "data": { "Amount": rideAmount },
//     };
//     dataChargeCapture = await axiosConfig(accessTokenConfigParams);
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   //-------------- charges payment Intents End -----------------------
//   // Response
//   if (
//     dataChargeCapture?.code === 200 &&
//     dataChargeCapture.data?.ActionCode?.toString() === "000"
//   ) {
//     responseData["authType"] = "NOAUTH";
//     responseData["isDefault"] = true;
//     responseData["transactionId"] =
//       dataChargeCapture?.data?.TransactionId.toString();
//     responseData["successResponse"] = dataChargeCapture.data;
//     return {
//       "code": 200,
//       "data": responseData,
//       "message": "Amount Captured",
//     };
//   } else {
//     return { "code": 400, "data": {}, "message": "Transaction Failed" };
//   }
// };

transactionTeyaEvents.captureAndRefundPaymentIntentsTransaction =
  async function (context) {
    const responseData = {},
      keyConfig = {};
    let dataChargeCapture = {},
      dataRefundCapture = {},
      refundAmount = 0;

    const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
      await notifyMessage.setNotifyLanguage(context.params.langCode);

    if (
      context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX
    ) {
      keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
      keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
      keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
    } else {
      keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
      keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
      keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
    }
    //-------------- charges payment Intents Start -----------------------
    const base64EncodeData = Buffer.from(
      keyConfig.privateKey + ":" + ""
    ).toString("base64");
    //-------
    try {
      const captureAmount = parseInt(
        parseFloat(context.params.captureAmount) * defaultCent
      );
      const paymentInitAmount = parseInt(
        parseFloat(context.params.paymentInitAmount) * defaultCent
      );
      refundAmount = paymentInitAmount - captureAmount;
      //
      const accessTokenConfigParams = {
        "method": "PUT",
        "port": 443,
        "url": `${keyConfig.baseUrl}api/payment/${context.params?.paymentInitId}/capture`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Basic " + base64EncodeData,
        },
        "data": {},
      };
      dataChargeCapture = await axiosConfig(accessTokenConfigParams);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //-------------- charges payment Intents End -----------------------
    // Response
    if (
      dataChargeCapture?.code === 200 &&
      dataChargeCapture.data?.ActionCode?.toString() === "000"
    ) {
      //-------------- Refund payment Intents Start -----------------------
      try {
        if (refundAmount > 0) {
          const refundConfigParams = {
            "method": "PUT",
            "port": 443,
            "url": `${keyConfig.baseUrl}api/payment/${context.params?.paymentInitId}/refund`,
            "headers": {
              "Content-Type": "application/json",
              "Authorization": "Basic " + base64EncodeData,
            },
            "data": { "PartialAmount": refundAmount },
          };
          dataRefundCapture = await axiosConfig(refundConfigParams);
        }
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }
      //-------------- Refund payment Intents End -----------------------
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      responseData["transactionId"] =
        dataChargeCapture?.data?.TransactionId.toString();
      responseData["successResponse"] = dataChargeCapture.data;
      return {
        "code": 200,
        "data": responseData,
        "message": "Amount Captured",
      };
    } else {
      return { "code": 400, "data": {}, "message": "Transaction Failed" };
    }
  };

transactionTeyaEvents.cancelCapturePaymentIntents = async function (context) {
  const responseData = {};
  let dataChargeCapture = {};
  let captureConfig = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["testPrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["privateKey"] = context.params.paymentData["livePrivateKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- charges payment Intents Start -----------------------
  try {
    const base64EncodeData = Buffer.from(
      keyConfig.privateKey + ":" + ""
    ).toString("base64");
    const accessTokenConfigParams = {
      "method": "PUT",
      "port": 443,
      "url": `${keyConfig.baseUrl}api/payment/${context.params?.paymentInitId}/Cancel`,
      "headers": {
        "Content-Type": "application/json",
        "Authorization": "Basic " + base64EncodeData,
      },
      "data": {},
    };
    dataChargeCapture = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Cancel Capture payment Intents End -----------------------
  // Response
  if (
    dataChargeCapture?.code === 200 &&
    dataChargeCapture.data?.ActionCode?.toString() === "000"
  ) {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["transactionId"] =
      dataChargeCapture.data.TransactionId?.toString();
    responseData["successResponse"] = dataChargeCapture;
    return { "code": 200, "data": responseData, "message": "Amount Captured" };
  } else {
    return { "code": 400, "data": {}, "message": "Transaction Failed" };
  }
};

// transactionTeyaEvents.newWithdrawalWalletTransaction = async function (
//   context
// ) {
//   const responseData = {},
//     keyConfig = {};
//   let finalPaymentData = {},
//     dataPaymentInitialization = null,
//     httpsAgent = null;
//   const certificatesPath = path.resolve(
//     __dirname,
//     "../mTLSCertificates/delbank_mTLS_certificado_flixmobi.pem"
//   );
//   const keyPath = path.resolve(__dirname, "../mTLSCertificates/flixmobi.key");
//   const countryCode = context.params.generalSettings.data.defaultDialCode;
//   let pixkeyNumber = context.params.bankDetails?.pixkeyNumber?.toString();
//   //
//   if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
//     keyConfig["baseUrl"] = "https://apisandbox.delbank.com.br/";
//     keyConfig["publishableKey"] = context.params.paymentData["testAccountNo"]; // delbank API Key
//     //
//     pixkeyNumber = pixkeyNumber.replace(countryCode, "");
//   } else {
//     keyConfig["baseUrl"] = "https://api.delbank.com.br/";
//     keyConfig["publishableKey"] = context.params.paymentData["liveAccountNo"]; // delbank API Key
//     //
//     if (fs.existsSync(certificatesPath) && fs.existsSync(keyPath)) {
//       httpsAgent = new https.Agent({
//         cert: fs.readFileSync(certificatesPath),
//         key: fs.readFileSync(keyPath),
//       });
//     } else {
//       return { "code": 400, "data": {}, "message": "Invalid Access" };
//     }
//     // if (!pixkeyNumber.startsWith(countryCode)) {
//     //   pixkeyNumber = countryCode + pixkeyNumber;
//     // }
//   }
//   //------------- Transfer payment Start -----------------------------
//   try {
//     const paymentInitializationParams = {
//       "method": "POST",
//       "port": 443,
//       "url": `${keyConfig.baseUrl}baas/api/v2/pix/dict/payment-initialization`,
//       "httpsAgent": httpsAgent,
//       "headers": {
//         "IdempotencyKey": "fliximobi_" + Date.now().toString(),
//         "accept": "application/json",
//         "Content-Type": "application/json",
//         "x-delbank-api-key": keyConfig.publishableKey,
//       },
//       "data": {
//         "key": pixkeyNumber,
//       },
//     };
//     dataPaymentInitialization = await axiosConfig(paymentInitializationParams);
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   if (dataPaymentInitialization.code === 200) {
//     try {
//       const paymentConfig = {
//         "method": "POST",
//         "port": 443,
//         "url": `${keyConfig.baseUrl}baas/api/v2/transfers`,
//         "httpsAgent": httpsAgent,
//         "headers": {
//           "IdempotencyKey":
//             context.params.generalSettings.data.siteTitle +
//             +"_" +
//             Date.now().toString(),
//           "accept": "application/json",
//           "Content-Type": "application/json",
//           "x-delbank-api-key": keyConfig.publishableKey,
//         },
//         "data": {
//           "amount": parseFloat(context.params.amount),
//           "description": context.params.transactionReason,
//           "endToEndId": dataPaymentInitialization.data.endToEndId,
//           "initiationType": "KEY",
//         },
//       };
//       finalPaymentData = await axiosConfig(paymentConfig);
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//   }
//   if (finalPaymentData.code === 200) {
//     //------------- Transfer payment End -----------------------------
//     // Response
//     responseData["authType"] = "NOAUTH";
//     // responseData["paymentGateWayCustomerId"] = stripeDestinationId;
//     responseData["transactionId"] = finalPaymentData.data.id;
//     responseData["paymentIntentId"] = finalPaymentData.data.id;
//     responseData["paymentIntentAmount"] = context.params.amount;
//     responseData["transactionAmount"] = context.params.amount;
//     responseData["paymentIntentClientSecret"] =
//       finalPaymentData.data.endToEndId;
//     responseData["successResponse"] = finalPaymentData.data;
//     responseData["status"] = constantUtils.SUCCESS; //finalPaymentData.status;
//     return {
//       "code": 200,
//       "data": responseData,
//       "message": "Valid Account Details",
//     };
//   } else {
//     return {
//       "code": 400,
//       "data": {},
//       // "message": "Bank server Busy Now. Please try Later.",
//       "message": finalPaymentData.message,
//     };
//   }
// };

//--------------------------------------
module.exports = transactionTeyaEvents;
