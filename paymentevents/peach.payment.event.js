//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const Razorpay = require("razorpay");

const peachPaymentUtils = require("../utils/peachpayment.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const defaultCent = 100;
//----------------- Need to Remove End --------------------

const transactionPeachEvents = {};

transactionPeachEvents.checkValidCard = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, INFO_INVALID_DETAILS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
    keyData["checkoutId"] = context.params.paymentData["testAccountId"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
    keyData["checkoutId"] = context.params.paymentData["liveAccountId"];
  }
  tokenJson = {
    ...keyData,
    "data": {},
  };
  // Method 1 ------  Start --> Ok Working
  tokenJson["data"]["entityId"] = keyData["checkoutId"];
  // tokenJson["data"]["paymentBrand"] = context.params.type || "VISA"; // Need to Check
  tokenJson["data"]["card.number"] = context.params.cardNumber;
  tokenJson["data"]["card.expiryMonth"] = context.params.expiryMonth;
  tokenJson["data"]["card.expiryYear"] = context.params.expiryYear;
  tokenJson["data"]["card.holder"] = context.params.cardHolderName;
  tokenJson["data"]["card.cvv"] = context.params.cvv;
  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    tokenJson["data"]["testMode"] = "EXTERNAL";
  }
  // tokenJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // tokenJson["data"]["standingInstruction.mode"] = "INITIAL";
  // tokenJson["data"]["standingInstruction.source"] = "CIT";
  // tokenJson["data"]["createRegistration"] = true;
  //-----------------------------
  //   tokenJson["data"]["threeDSecure.authenticationStatus"] = "Y";
  //   tokenJson["data"]["threeDSecure.version"] = "2.2.0";
  //   tokenJson["data"]["shopperResultUrl"] = "https://www.peachpayments.com/"; // New Param Peach Pament Reffered
  // tokenJson["data"]["shopperResultUrl"] =
  //   "https://api.veedo.co.za/api/transaction/peach/webhook"; // New Param Peach Pament Reffered
  tokenJson["data"]["shopperResultUrl"] =
    context.params.generalSettings.data.redirectUrls.paymentRedirectApiUrl +
    "/api/transaction/peach/webhook";
  //   tokenJson["data"]["amount"] = 1.15;
  tokenJson["data"]["currency"] = "ZAR";
  //-----------------------------
  // tokenJson["data"]["paymentBrand"] = "VISA"; //"MASTER";
  tokenJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  tokenJson["data"]["standingInstruction.mode"] = "INITIAL";
  tokenJson["data"]["standingInstruction.source"] = "CIT";
  tokenJson["data"]["amount"] = parseFloat(context.params.minimumChargeAmount);
  tokenJson["data"]["paymentType"] = "DB";
  tokenJson["data"]["createRegistration"] = true;
  //-----------------------------
  //   tokenJson["data"]["transactionCategory"] = "EC";
  // Method 1 ------  End
  // // Method 2 ------  Start --> Ok Working
  // tokenJson["data"]["entityId"] = keyData["entityId"];
  // tokenJson["data"]["amount"] = parseFloat(
  //   context.params.minimumChargeAmount
  // ).toFixed(2);
  // tokenJson["data"]["currency"] =
  //   context.params.currencyCode?.toLowerCase() || "ZAR";
  // tokenJson["data"]["paymentBrand"] = context.params.type || "VISA";
  // // tokenJson["data"]["paymentType"] = "DB"; // "PA"; // "DB";
  // tokenJson["data"]["card.number"] = context.params.cardNumber;
  // tokenJson["data"]["card.expiryMonth"] = context.params.expiryMonth;
  // tokenJson["data"]["card.expiryYear"] = context.params.expiryYear;
  // tokenJson["data"]["card.holder"] = context.params.cardHolderName;
  // tokenJson["data"]["card.cvv"] = context.params.cvv;
  // tokenJson["data"]["standingInstruction.mode"] = "INITIAL";
  // tokenJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // tokenJson["data"]["standingInstruction.source"] = "CIT";
  // // tokenJson["data"]["createRegistration"] = true;
  // // Method 2 ------  End
  // //---------------
  // tokenJson["data"]["entityId"] = keyData["entityId"]; //context.params.amount?.toString();
  // tokenJson["data"]["amount"] = "100"; //context.params.amount?.toString();
  // tokenJson["data"]["currency"] =
  //   context.params.currencyCode?.toLowerCase() || "ZAR";
  // // tokenJson["data"]["currency"] = context.params.currencyCode
  // //   ? context.params.currencyCode?.toUpperCase()
  // //   : context.params.generalSettings.data.currencyCode?.toUpperCase();
  // tokenJson["data"]["paymentBrand"] = context.params.type?.toUpperCase() || "VISA";
  // tokenJson["data"]["paymentType"] = "DB";
  // tokenJson["data"]["card.number"] = context.params.cardNumber;
  // tokenJson["data"]["card.holder"] = context.params.cardHolderName;
  // tokenJson["data"]["card.expiryMonth"] = context.params.expiryMonth;
  // tokenJson["data"]["card.expiryYear"] = "2034"; //context.params.expiryYear;
  // tokenJson["data"]["card.cvv"] = context.params.cvv;
  // finalPaymentData = await peachPaymentUtils.debitTransactions(tokenJson);
  // //-------------------
  const finalTokenData = await peachPaymentUtils.createTokenization(tokenJson);
  if (
    finalTokenData?.result?.code === "000.000.000" ||
    finalTokenData?.result?.code === "000.000.100" ||
    finalTokenData?.result?.code === "000.200.000" ||
    finalTokenData?.result?.code === "000.100.105" ||
    finalTokenData?.result?.code === "000.100.106" ||
    finalTokenData?.result?.code === "000.100.110" ||
    finalTokenData?.result?.code === "000.100.111" ||
    finalTokenData?.result?.code === "000.100.112"
  ) {
    // //   {
    // // if (1 === 1) {
    // paymentJson = {
    //   ...keyData,
    //   "data": {},
    // };
    // // Method 1 -------- Start
    // // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
    // paymentJson["cardTokenId"] = finalTokenData["id"]; //TokenId
    // //
    // paymentJson["data"]["entityId"] = keyData["entityId"];
    // // paymentJson["data"]["paymentBrand"] =
    // //   context.params.type?.toUpperCase() || "VISA";
    // // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
    // paymentJson["data"]["amount"] = parseFloat(
    //   context.params.minimumChargeAmount
    // ).toFixed(2);
    // paymentJson["data"]["currency"] =
    //   context.params.generalSettings.data.currencyCode?.toUpperCase() || "ZAR";
    // paymentJson["data"]["merchantTransactionId"] =
    //   context.params.transactionReason;
    // // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
    // paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
    // // paymentJson["data"]["customParameters[transactionReference]"] =
    // //   context.params.transactionReason;
    // paymentJson["data"]["customParameters[transactionReason]"] =
    //   context.params.transactionReason;
    // paymentJson["data"]["card.cvv"] = context.params.cvv;
    // // paymentJson["data"]["shopperResultUrl"] = "https://www.peachpayments.com/"; // New Param Peach Pament Reffered
    // // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
    // // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
    // // paymentJson["data"]["standingInstruction.source"] = "MIT";
    // // paymentJson["data"]["testMode"] = "EXTERNAL";
    // // Method 1 -------- End
    // // Method 2 -------- Start
    // // paymentJson["id"] = finalTokenData["id"]; //TokenId
    // //
    // // paymentJson["data"]["entityId"] = keyData["entityId"];
    // // paymentJson["data"]["paymentBrand"] =
    // //   context.params.type?.toUpperCase() || "VISA";
    // // paymentJson["data"]["paymentType"] = "DB";
    // // paymentJson["data"]["amount"] = parseFloat(
    // //   context.params.minimumChargeAmount
    // // ).toFixed(2);
    // // paymentJson["data"]["currency"] =
    // //   context.params.currencyCode?.toLowerCase() || "ZAR";
    // // paymentJson["data"]["standingInstruction.type"] = "RECURRING";
    // // paymentJson["data"]["standingInstruction.mode"] = "INITIAL";
    // // paymentJson["data"]["standingInstruction.source"] = "CIT";
    // // // paymentJson["data"]["createRegistration"] = true;
    // // paymentJson["data"]["threeDSecure.eci"] = "05";
    // // paymentJson["data"]["threeDSecure.authenticationStatus"] = "Y";
    // // paymentJson["data"]["threeDSecure.version"] = "2.2.0";
    // // paymentJson["data"]["threeDSecure.dsTransactionId"] =
    // //   Mongoose.Types.ObjectId()?.toString();
    // // paymentJson["data"]["threeDSecure.acsTransactionId"] =
    // //   Mongoose.Types.ObjectId()?.toString();
    // // paymentJson["data"]["threeDSecure.verificationId"] =
    // //   "MTIzNDU2Nzg5MDEyMzQ1Njc4OTA=";
    // // paymentJson["data"]["threeDSecure.amount"] = parseFloat(
    // //   context.params.minimumChargeAmount
    // // ).toFixed(2);
    // // paymentJson["data"]["threeDSecure.currency"] =
    // //   context.params.currencyCode?.toLowerCase() || "ZAR";
    // // paymentJson["data"]["threeDSecure.flow"] = "challenge";
    // // paymentJson["data"]["testMode"] = "EXTERNAL";
    // // Method 2 -------- End
    // // // Method 3 -------- Start
    // // paymentJson["id"] = finalTokenData["id"]; //TokenId
    // // paymentJson["data"]["entityId"] = keyData["entityId"];
    // // paymentJson["data"]["amount"] = parseFloat(
    // //   context.params.minimumChargeAmount
    // // ).toFixed(2);
    // // paymentJson["data"]["currency"] =
    // //   context.params.currencyCode?.toLowerCase() || "ZAR";
    // // paymentJson["data"]["paymentBrand"] =
    // //   context.params.type?.toUpperCase() || "VISA";
    // // paymentJson["data"]["paymentType"] = "DB"; // "PA"; // "DB";
    // // // paymentJson["data"]["card.number"] = context.params.cardNumber;
    // // // paymentJson["data"]["card.expiryMonth"] = context.params.expiryMonth;
    // // // paymentJson["data"]["card.expiryYear"] = context.params.expiryYear;
    // // paymentJson["data"]["card.holder"] = context.params.cardHolderName;
    // // paymentJson["data"]["card.cvv"] = context.params.cvv;
    // // // Method 3 -------- End
    // const finalPaymentData = await peachPaymentUtils.instantCardPayment(
    //   paymentJson
    // );
    // // if (
    // //   finalPaymentData?.result?.code === "000.000.000" ||
    // //   finalPaymentData?.result?.code === "000.000.100" ||
    // //   //   finalPaymentData?.result?.code === "000.200.100" ||
    // //   finalPaymentData?.result?.code === "000.100.105" ||
    // //   finalPaymentData?.result?.code === "000.100.106" ||
    // //   finalPaymentData?.result?.code === "000.100.110" ||
    // //   finalPaymentData?.result?.code === "000.100.111" ||
    // //   finalPaymentData?.result?.code === "000.100.112"
    // // )
    let queryString = "",
      redirectUrl = "";
    Object.entries(finalTokenData["redirect"]["parameters"])?.map((s) => {
      queryString = queryString + "&" + `${s[1].name}=${s[1].value || ""}`;
    });
    redirectUrl =
      finalTokenData["redirect"]["url"] +
      (queryString ? "?" + queryString?.substring(1) : "");
    return {
      "code": 200,
      "data": {
        ...finalTokenData, // ...finalPaymentData,
        "authType": "NOAUTH",
        "isDefault": true, //need to Change Dynamic
        "isCVVRequired": true, //need to Change Dynamic
        "cardToken": finalTokenData["id"], //finalTokenData["registrationId"], //TokenId
        "embedtoken": finalTokenData["registrationId"], //finalTokenData["registrationId"], //TokenId
        "successResponse": finalTokenData, // finalPaymentData,
        "transactionAmount": parseFloat(
          context.params.minimumChargeAmount
        ).toFixed(2),
        "transactionId": finalTokenData["id"],
        "paymentGateWayCustomerId": null,
        "paymentIntentId": finalTokenData["registrationId"], // finalPaymentData["id"],
        "paymentIntentClientSecret": null,
        "redirectUrl": redirectUrl,
        "redirectMethodType": "POST",
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": INFO_INVALID_DETAILS,
    };
  }
  //   } else {
  //     return {
  //       "code": 400,
  //       "data": {},
  //       "message": INFO_INVALID_DETAILS,
  //     };
  //   }
};

transactionPeachEvents.validateCardRegistrationStatus = async function (
  context
) {
  let paymentJson = {};
  const keyData = {};
  const {
    INFO_TRANSACTIONS_SUCCESS,
    INFO_INVALID_DETAILS,
    WARNING_CARD_VALIDATION_FAILED,
    WARNING_BANK_SERVER_BUSY,
  } = await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  // Method 1 ------  Start --> Ok Working
  // paymentJson["cardTokenId"] = context.params.cardTokenId; //TokenId
  // paymentJson["transactionId"] = context.params.transactionId;
  paymentJson["paymentIntentId"] = context.params.paymentIntentId;

  paymentJson["data"]["entityId"] = keyData["entityId"];
  // //-------------------
  const finalPaymentData =
    await peachPaymentUtils.validateCardTokenizationStatus(paymentJson);
  if (
    finalPaymentData?.result?.code === "000.000.000" // || finalPaymentData?.result?.code === "000.000.100"
  ) {
    return {
      "code": 200,
      "data": {
        // ...finalPaymentData,
        "authType": "NOAUTH",
        "successResponse": finalPaymentData,
        "cardTokenId": finalPaymentData["id"],
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message":
        finalPaymentData?.result?.description || WARNING_BANK_SERVER_BUSY,
    };
  }
};

transactionPeachEvents.newRechargeWalletTransaction = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const transactionAmount = parseFloat(context.params.amount).toFixed(2);
  // Method 1 -------- Start
  // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
  paymentJson["cardTokenId"] = context.params?.cardData?.data?.embedtoken; //TokenId
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  paymentJson["data"]["amount"] = transactionAmount;
  paymentJson["data"]["currency"] =
    context.params.generalSettings.data.currencyCode?.toUpperCase() || "ZAR";
  paymentJson["data"]["merchantTransactionId"] =
    context.params.transactionReason;
  // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
  paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReference]"] =
  //   context.params.transactionReason;
  paymentJson["data"]["customParameters[transactionReason]"] =
    context.params.transactionReason;
  //   paymentJson["data"]["card.cvv"] = context.params.cvv;
  // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
  // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // paymentJson["data"]["standingInstruction.source"] = "MIT";
  // paymentJson["data"]["testMode"] = "EXTERNAL";
  // Method 1 -------- End
  const finalPaymentData = await peachPaymentUtils.instantCardPayment(
    paymentJson
  );
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "isDefault": true, //need to Change Dynamic
        "paymentGateWayCustomerId": null,
        "transactionId": finalPaymentData["id"],
        "paymentIntentId": finalPaymentData["id"],
        "paymentIntentAmount": transactionAmount,
        "transactionAmount": transactionAmount,
        "paymentIntentClientSecret": null,
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

transactionPeachEvents.createPaymentIntents = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const estimationAmount = parseFloat(context.params.estimationAmount).toFixed(
    2
  );
  // Method 1 -------- Start
  // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
  paymentJson["cardTokenId"] = context.params?.cardData?.data?.embedtoken; //TokenId
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  paymentJson["data"]["amount"] = estimationAmount;
  paymentJson["data"]["currency"] =
    context.params.generalSettings.data.currencyCode?.toUpperCase() || "ZAR";
  paymentJson["data"]["merchantTransactionId"] =
    context.params.transactionReason;
  // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
  // paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReference]"] =
  //   context.params.transactionReason;
  paymentJson["data"]["customParameters[transactionReason]"] =
    context.params.transactionReason;
  paymentJson["data"]["card.cvv"] = context.params.cvv;
  // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
  // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // paymentJson["data"]["standingInstruction.source"] = "MIT";
  // paymentJson["data"]["testMode"] = "EXTERNAL";
  // Method 1 -------- End
  const finalPaymentData = await peachPaymentUtils.preAuthTransaction(
    paymentJson
  );
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "transactionId": finalPaymentData["id"],
        "paymentIntentId": finalPaymentData["id"],
        "paymentIntentAmount": estimationAmount,
        "transactionAmount": estimationAmount,
        "paymentIntentClientSecret": null,
        "redirectUrl": null,
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
        // responseData["isDefault"] = false;
        // responseData["paymentGateWayCustomerId"] = null;
        // responseData["transactionId"] =
        //   responseData.data.ReferenceNumber.toString();
        // responseData["paymentIntentId"] =
        //   responseData.data.ReferenceNumber.toString();
        // // responseData["paymentIntentAmount"] = parseFloat(
        // //   context.params.estimationAmount
        // // );
        // responseData["paymentIntentClientSecret"] = null;
        // responseData["transactionAmount"] = parseFloat(
        //   context.params.estimationAmount
        // );
        // responseData["redirectUrl"] = null;
        // responseData["successResponse"] = responseData.data;
        // responseData["status"] = "SUCESS";
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

transactionPeachEvents.refundsAmount = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }

  paymentJson = {
    ...keyData,
    "data": {},
  };
  // Method 1 -------- Start
  // paymentJson["id"] = context.params.transactionId.toString();
  paymentJson["paymentIntentId"] = context.params.transactionId.toString();
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  paymentJson["data"]["amount"] = parseFloat(context.params.transactionAmount);
  paymentJson["data"]["currency"] =
    context.params.currencyCode?.toLowerCase() || "ZAR";
  // Method 1 -------- End
  // const finalPaymentData = await peachPaymentUtils.refundPayment(paymentJson);
  const finalPaymentData = await peachPaymentUtils.cancelCapturePayment(
    paymentJson
  );
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "transactionId": context.params.transactionId.toString(),
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

// // webhook
// transactionPeachEvents.webhooking = async function (context) {
//   let transactionStatus = {};
//   let updateTransaction = {};
//   let notificationObject = {};
//   const eventType = context.params.type;
//   const transactionId = context.params.data?.object?.id;

//   switch (eventType) {
//     case "account.updated":
//     case "account.external_account.created":
//     case "account.external_account.deleted":
//     case "account.external_account.updated":
//     case "balance.available":
//     case "billing_portal.configuration.created":
//     case "billing_portal.configuration.updated":
//     case "capability.updated":
//     case "charge.captured":
//     case "charge.refunded":
//     case "charge.succeeded":
//     case "charge.updated":
//     case "charge.dispute.closed":
//     case "charge.dispute.created":
//     case "charge.dispute.funds_reinstated":
//     case "charge.dispute.funds_withdrawn":
//     case "charge.dispute.updated":
//     case "charge.refund.updated":
//     case "checkout.session.async_payment_succeeded":
//     case "checkout.session.completed":
//     case "coupon.created":
//     case "coupon.deleted":
//     case "coupon.updated":
//     case "credit_note.created":
//     case "credit_note.updated":
//     case "credit_note.voided":
//     case "customer.created":
//     case "customer.deleted":
//     case "customer.updated":
//     case "customer.discount.created":
//     case "customer.discount.deleted":
//     case "customer.discount.updated":
//     case "customer.source.created":
//     case "customer.source.deleted":
//     case "customer.source.updated":
//     case "customer.subscription.created":
//     case "customer.subscription.deleted":
//     case "customer.subscription.pending_update_applied":
//     case "customer.subscription.trial_will_end":
//     case "customer.subscription.updated":
//     case "customer.tax_id.created":
//     case "customer.tax_id.deleted":
//     case "customer.tax_id.updated":
//     case "file.created":
//     case "identity.verification_session.canceled":
//     case "identity.verification_session.created":
//     case "identity.verification_session.processing":
//     case "identity.verification_session.requires_input":
//     case "identity.verification_session.verified":
//     case "invoice.created":
//     case "invoice.deleted":
//     case "invoice.finalized":
//     case "invoice.marked_uncollectible":
//     case "invoice.paid":
//     case "invoice.payment_action_required":
//     case "invoice.payment_succeeded":
//     case "invoice.sent":
//     case "invoice.upcoming":
//     case "invoice.updated":
//     case "invoice.voided":
//     case "invoiceitem.created":
//     case "invoiceitem.deleted":
//     case "invoiceitem.updated":
//     case "issuing_authorization.created":
//     case "issuing_authorization.updated":
//     case "issuing_card.created":
//     case "issuing_card.updated":
//     case "issuing_cardholder.created":
//     case "issuing_cardholder.updated":
//     case "issuing_dispute.closed":
//     case "issuing_dispute.created":
//     case "issuing_dispute.updated":
//     case "issuing_transaction.created":
//     case "issuing_transaction.updated":
//     case "mandate.updated":
//     case "order.created":
//     case "order.payment_succeeded":
//     case "order.updated":
//     case "order_return.created":
//     case "payment_intent.amount_capturable_updated":
//     case "payment_intent.created":
//     case "payment_intent.requires_action":
//     case "payment_intent.succeeded":
//     case "payment_link.created":
//     case "payment_link.updated":
//     case "payment_method.attached":
//     case "payment_method.automatically_updated":
//     case "payment_method.detached":
//     case "payment_method.updated":
//     case "payout.canceled":
//     case "payout.created":
//     case "payout.paid":
//     case "payout.updated":
//     case "person.created":
//     case "person.deleted":
//     case "person.updated":
//     case "plan.created":
//     case "plan.deleted":
//     case "plan.updated":
//     case "price.created":
//     case "price.deleted":
//     case "price.updated":
//     case "product.created":
//     case "product.deleted":
//     case "product.updated":
//     case "promotion_code.created":
//     case "promotion_code.updated":
//     case "quote.accepted":
//     case "quote.canceled":
//     case "quote.created":
//     case "quote.finalized":
//     case "radar.early_fraud_warning.created":
//     case "radar.early_fraud_warning.updated":
//     case "recipient.created":
//     case "recipient.deleted":
//     case "recipient.updated":
//     case "reporting.report_run.succeeded":
//     case "review.closed":
//     case "review.opened":
//     case "setup_intent.canceled":
//     case "setup_intent.created":
//     case "setup_intent.requires_action":
//     case "setup_intent.succeeded":
//     case "sigma.scheduled_query_run.created":
//     case "sku.created":
//     case "sku.deleted":
//     case "sku.updated":
//     case "source.canceled":
//     case "source.chargeable":
//     case "source.mandate_notification":
//     case "source.refund_attributes_required":
//     case "source.transaction.created":
//     case "source.transaction.updated":
//     case "subscription_schedule.aborted":
//     case "subscription_schedule.canceled":
//     case "subscription_schedule.completed":
//     case "subscription_schedule.created":
//     case "subscription_schedule.released":
//     case "subscription_schedule.updated":
//     case "tax_rate.created":
//     case "tax_rate.updated":
//     case "topup.canceled":
//     case "topup.created":
//     case "topup.reversed":
//     case "topup.succeeded":
//     case "transfer.created":
//     case "transfer.paid":
//     case "transfer.reversed":
//     case "transfer.updated":
//       transactionStatus = constantUtils.SUCCESS;
//       break;
//     case "charge.failed":
//     case "invoice.finalization_failed":
//     case "invoice.payment_failed":
//     case "checkout.session.async_payment_failed":
//     case "order.payment_failed":
//     case "payment_intent.payment_failed":
//     case "payout.failed":
//     case "reporting.report_run.failed":
//     case "setup_intent.setup_failed":
//     case "source.failed":
//     case "subscription_schedule.expiring":
//     case "topup.failed":
//     case "transfer.failed":
//       transactionStatus = constantUtils.FAILED;
//       break;
//     case "charge.pending":
//     case "issuing_dispute.funds_reinstated":
//     case "issuing_dispute.submitted":
//     case "payment_intent.canceled":
//     case "payment_intent.processing":
//       transactionStatus = constantUtils.PENDING;
//       break;
//     case "charge.expired":
//     case "checkout.session.expired":
//     case "customer.source.expiring":
//     case "customer.subscription.pending_update_expired":
//       transactionStatus = constantUtils.FAILED;
//       break;
//     // ... handle other event types
//     default:
//       transactionStatus = constantUtils.FAILED;
//       return null;
//     // break
//   }

//   const transaction = await this.adapter.model.findOneAndUpdate(
//     { "transactionId": transactionId },
//     { "webhookresponse": context.params },
//     {
//       "new": true, //unique reference find in webhook
//     }
//   );

//   if (transactionStatus === constantUtils.SUCCESS) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     return null;
//   } else if (transactionStatus === constantUtils.FAILED) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     //-------------------------------------
//     // reterive the amount to whose is the user or professional
//     if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
//       // retrn the amount of professional
//       if (transaction.to.userType === constantUtils.PROFESSIONAL) {
//         let userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         let walletupdated = await this.broker.emit(
//           "professional.updateWalletAmount",
//           {
//             "professionalId": userData._id.toString(),
//             "availableAmount":
//               userData.wallet.availableAmount + transaction.transactionAmount,
//             "freezedAmount": userData.wallet.freezedAmount,
//           }
//         );
//         walletupdated = walletupdated && walletupdated[0];
//         if (walletupdated.code !== 200) {
//           throw new MoleculerError(
//             "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED",
//             500
//           );
//         }

//         // send notification to professional
//         let updatedtransaction = await this.adapter.model.findOne({
//           "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//         });
//         updatedtransaction = updatedtransaction.toJSON();

//         // remove not neede data for send notification

//         //updated user detailes

//         userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         //

//         notificationObject = {
//           "data": {
//             "type": constantUtils.NOTIFICATIONTYPE,
//             "userType": constantUtils.PROFESSIONAL,
//             "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
//             "timestamp": Date.now(),
//             "message":
//               updatedtransaction.webhookresponse.data.object.description,
//             "details": {
//               "walletAmount": userData.wallet.availableAmount,
//               "paymentType": updatedtransaction.paymentType,
//               "transactionStatus": updatedtransaction.transactionStatus,
//               "_id": updatedtransaction._id,
//             },
//           },
//           "registrationTokens": [
//             {
//               "token": userData.deviceInfo[0].deviceId,
//               "id": userData._id.toString(),
//               "deviceType": userData.deviceInfo[0].deviceType,
//               "platform": userData.deviceInfo[0].platform,
//               "socketId": userData.deviceInfo[0].socketId,
//             },
//           ],
//         };
//         console.log(notificationObject);
//       }
//       // USER
//       if (transaction.to.userType === constantUtils.USER) {
//         let userData = await this.broker.emit("user.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         let walletupdated = await this.broker.emit("user.updateWalletAmount", {
//           "userId": userData._id.toString(),
//           "availableAmount":
//             userData.wallet.availableAmount + transaction.transactionAmount,
//           "freezedAmount": userData.wallet.freezedAmount,
//           "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
//         });
//         walletupdated = walletupdated && walletupdated[0];
//         if (walletupdated.code !== 200) {
//           throw new MoleculerError(
//             "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED",
//             500
//           );
//         }

//         // send notification to professional
//         let updatedtransaction = await this.adapter.model.findOne({
//           "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//         });
//         updatedtransaction = updatedtransaction.toJSON();
//         //updated user detailes

//         userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         //

//         notificationObject = {
//           "data": {
//             "type": constantUtils.NOTIFICATIONTYPE,
//             "userType": constantUtils.USER,
//             "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
//             "timestamp": Date.now(),
//             "message":
//               updatedtransaction.webhookresponse.data.object.description,
//             "details": lzStringEncode({
//               "walletAmount": userData.wallet.availableAmount,
//               "paymentType": updatedtransaction.paymentType,
//               "transactionStatus": updatedtransaction.transactionStatus,
//               "_id": updatedtransaction._id,
//             }),
//           },
//           "registrationTokens": [
//             {
//               "token": userData.deviceInfo[0].deviceId,
//               "id": userData._id.toString(),
//               "deviceType": userData.deviceInfo[0].deviceType,
//               "platform": userData.deviceInfo[0].platform,
//               "socketId": userData.deviceInfo[0].socketId,
//             },
//           ],
//         };
//         console.log(notificationObject);
//       }
//       /* SOCKET PUSH NOTIFICATION */
//       this.broker.emit("socket.sendNotification", notificationObject);
//       // this.broker.emit('socket.statusChangeEvent', notificationObject)

//       /* FCM */
//       this.broker.emit("admin.sendFCM", notificationObject);
//       return null;
//     }
//   } else if (transactionStatus === constantUtils.PENDING) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     return null;
//   }
//   return null;
// };

transactionPeachEvents.createOrderPaymentForBooking = async function (context) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let orderPaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  // let customerConfig = {};
  let orderConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;

  if (paymentGateWayCustomerId) {
    orderConfig = {
      ...keyConfig,
      "data": {},
    };
    try {
      const razorPay = new Razorpay({
        "key_id": orderConfig.key_id,
        "key_secret": orderConfig.key_secret,
      });

      orderPaymentData = await razorPay.orders.create({
        "amount": parseInt(
          parseFloat(context.params.estimationAmount) * defaultPaisa
        ),
        "currency": context.params.generalSettings.data.currencyCode,
        "receipt": context.params.transactionReason.split("/")[1],
      });
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //-------------- Response Data Start -----------------------
    if (orderPaymentData.status === "created") {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] = orderPaymentData.id.toString();
      responseData["paymentIntentId"] = orderPaymentData.id.toString();
      responseData["paymentIntentAmount"] = orderPaymentData.amount;
      responseData["successResponse"] = orderPaymentData;
      responseData["status"] = orderPaymentData.status;
      return { "code": 200, "data": responseData, "message": "Valid Card" };
    } else {
      return { "code": 400, "data": {}, "message": "invalid Card" };
    }
    //-------------- Response Data End -----------------------
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Card Payment Failed",
    };
  }
};

transactionPeachEvents.capturePaymentIntentsAndRefund = async function (
  context
) {
  let cancelJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const captureAmount = parseFloat(context.params.captureAmount).toFixed(2);
  const reversalAmount =
    parseFloat(context.params.paymentInitAmount) - parseFloat(captureAmount);
  // Method 1 -------- Start
  // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
  paymentJson["paymentInitId"] = context.params?.paymentInitId;
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  paymentJson["data"]["amount"] = captureAmount;
  paymentJson["data"]["currency"] =
    context.params.currencyCode?.toUpperCase() || "ZAR";
  paymentJson["data"]["merchantTransactionId"] =
    context.params.transactionReason;
  // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
  // paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReference]"] =
  //   context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReason]"] =
  //   context.params.transactionReason;
  // paymentJson["data"]["card.cvv"] = context.params.cvv;
  // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
  // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // paymentJson["data"]["standingInstruction.source"] = "MIT";
  // paymentJson["data"]["testMode"] = "EXTERNAL";
  // Method 1 -------- End
  const finalPaymentData = await peachPaymentUtils.capturePayment(paymentJson);
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    if (reversalAmount > 0) {
      cancelJson = {
        ...keyData,
        "data": {},
      };
      cancelJson["paymentIntentId"] = context.params?.paymentInitId;
      cancelJson["data"]["entityId"] = keyData["entityId"];
      cancelJson["data"]["amount"] = reversalAmount.toFixed(2);
      cancelJson["data"]["currency"] =
        context.params.currencyCode?.toUpperCase() || "ZAR";
      await peachPaymentUtils.cancelCapturePayment(cancelJson);
    }
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "isDefault": true,
        "paymentGateWayCustomerId": null,
        "transactionId": finalPaymentData["id"],
        "paymentIntentId": finalPaymentData["id"],
        "paymentIntentAmount": captureAmount,
        "transactionAmount": captureAmount,
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

transactionPeachEvents.cancelCapturePaymentIntents = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const transactionAmount = parseFloat(context.params.amount).toFixed(2);
  // Method 1 -------- Start
  // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
  paymentJson["paymentIntentId"] = context.params?.paymentInitId;
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  // // paymentJson["data"]["paymentBrand"] =
  // //   context.params.type?.toUpperCase() || "VISA";
  // // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  // paymentJson["data"]["amount"] = transactionAmount;
  // paymentJson["data"]["currency"] =
  //   context.params.generalSettings.data.currencyCode?.toUpperCase() || "ZAR";
  // // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
  // // paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
  // // paymentJson["data"]["customParameters[transactionReference]"] =
  // //   context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReason]"] =
  //   context.params.transactionReason;
  // // paymentJson["data"]["card.cvv"] = context.params.cvv;
  // // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
  // // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // // paymentJson["data"]["standingInstruction.source"] = "MIT";
  // // paymentJson["data"]["testMode"] = "EXTERNAL";
  // // Method 1 -------- End
  const finalPaymentData = await peachPaymentUtils.cancelCapturePayment(
    paymentJson
  );
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "isDefault": true, //need to Change Dynamic
        "transactionId": finalPaymentData["id"],
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

transactionPeachEvents.tripEndCardCharge = async function (context) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    keyData["entityId"] = context.params.paymentData["testSecretKey"];
  } else {
    keyData["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    keyData["entityId"] = context.params.paymentData["liveSecretKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const amount = parseFloat(context.params.amount).toFixed(2);
  // Method 1 -------- Start
  // paymentJson["id"] = "8ac7a4a190339cf90190345efd920573"; // finalTokenData["id"]; //TokenId
  // paymentJson["paymentInitId"] = context.params?.paymentInitId;
  paymentJson["cardTokenId"] = context.params.cardData.data.embedtoken; //TokenId
  //
  paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  paymentJson["data"]["amount"] = amount;
  paymentJson["data"]["currency"] =
    context.params.currencyCode?.toUpperCase() || "ZAR";
  paymentJson["data"]["merchantTransactionId"] =
    context.params.transactionReason;
  // paymentJson["data"]["merchantTransactionId"] =  "INV-63730f76fc61dc118237bc6f";
  // paymentJson["data"]["merchantInvoiceId"] = context.params.transactionReason;
  // paymentJson["data"]["customParameters[transactionReference]"] =
  //   context.params.transactionReason;
  paymentJson["data"]["customParameters[transactionReason]"] =
    context.params.transactionReason;
  paymentJson["data"]["card.cvv"] = context.params.cardData.data.cvv;
  // paymentJson["data"]["standingInstruction.mode"] = "REPEATED";
  // paymentJson["data"]["standingInstruction.type"] = "UNSCHEDULED";
  // paymentJson["data"]["standingInstruction.source"] = "MIT";
  // paymentJson["data"]["testMode"] = "EXTERNAL";
  // Method 1 -------- End
  // Method 2 -------- Start
  // paymentJson["id"] = finalTokenData["id"]; //TokenId
  //
  // paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = "DB";
  // paymentJson["data"]["amount"] = parseFloat(
  //   context.params.minimumChargeAmount
  // ).toFixed(2);
  // paymentJson["data"]["currency"] =
  //   context.params.currencyCode?.toLowerCase() || "ZAR";
  // paymentJson["data"]["standingInstruction.type"] = "RECURRING";
  // paymentJson["data"]["standingInstruction.mode"] = "INITIAL";
  // paymentJson["data"]["standingInstruction.source"] = "CIT";
  // // paymentJson["data"]["createRegistration"] = true;
  // paymentJson["data"]["threeDSecure.eci"] = "05";
  // paymentJson["data"]["threeDSecure.authenticationStatus"] = "Y";
  // paymentJson["data"]["threeDSecure.version"] = "2.2.0";
  // paymentJson["data"]["threeDSecure.dsTransactionId"] =
  //   Mongoose.Types.ObjectId()?.toString();
  // paymentJson["data"]["threeDSecure.acsTransactionId"] =
  //   Mongoose.Types.ObjectId()?.toString();
  // paymentJson["data"]["threeDSecure.verificationId"] =
  //   "MTIzNDU2Nzg5MDEyMzQ1Njc4OTA=";
  // paymentJson["data"]["threeDSecure.amount"] = parseFloat(
  //   context.params.minimumChargeAmount
  // ).toFixed(2);
  // paymentJson["data"]["threeDSecure.currency"] =
  //   context.params.currencyCode?.toLowerCase() || "ZAR";
  // paymentJson["data"]["threeDSecure.flow"] = "challenge";
  // paymentJson["data"]["testMode"] = "EXTERNAL";
  // Method 2 -------- End
  // // Method 3 -------- Start
  // paymentJson["id"] = finalTokenData["id"]; //TokenId
  // paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["amount"] = parseFloat(
  //   context.params.minimumChargeAmount
  // ).toFixed(2);
  // paymentJson["data"]["currency"] =
  //   context.params.currencyCode?.toLowerCase() || "ZAR";
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = "DB"; // "PA"; // "DB";
  // // paymentJson["data"]["card.number"] = context.params.cardNumber;
  // // paymentJson["data"]["card.expiryMonth"] = context.params.expiryMonth;
  // // paymentJson["data"]["card.expiryYear"] = context.params.expiryYear;
  // paymentJson["data"]["card.holder"] = context.params.cardHolderName;
  // paymentJson["data"]["card.cvv"] = context.params.cvv;
  // // Method 3 -------- End
  const finalPaymentData = await peachPaymentUtils.instantCardPayment(
    paymentJson
  );
  if (
    finalPaymentData?.result?.code === "000.000.000" ||
    finalPaymentData?.result?.code === "000.000.100" ||
    finalPaymentData?.result?.code === "000.100.105" ||
    finalPaymentData?.result?.code === "000.100.106" ||
    finalPaymentData?.result?.code === "000.100.110" ||
    finalPaymentData?.result?.code === "000.100.111" ||
    finalPaymentData?.result?.code === "000.100.112"
  ) {
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "transactionId": finalPaymentData["id"],
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
};

// transactionPeachEvents.verifyBankAccount = async function (context) {
//   const keyConfig = {};
//   const responseData = {};
//   if (
//     finalBankAccountData.status === "succeeded" ||
//     finalBankAccountData.status === "new"
//   ) {
//     // Response
//     responseData["status"] = "succeeded";
//     responseData["authType"] = "NOAUTH";
//     // responseData['stripeCardToken'] = finalTokenData.id
//     responseData["embedtoken"] = finalTokenData.id;
//     responseData["stripeConnectAccountId"] = finalAccountData.id;
//     responseData["bankAccountId"] = finalBankAccountData.id;
//     responseData["bankAccountToken"] = finalTokenData.id;
//     // responseData['transactionId'] = finalPaymentData.id.toString()
//     responseData["transactionReason"] = context.params.transactionReason;
//     // responseData['successResponse'] = finalPaymentData
//     return {
//       "code": 200,
//       "data": responseData,
//       "message": "Valid Bank Details",
//     };
//   } else {
//     return { "code": 400, "data": {}, "message": "Invalid Bank Details" };
//   }
// };

transactionPeachEvents.newWithdrawalWalletTransaction = async function (
  context
) {
  let tokenJson = {};
  let paymentJson = {};
  const keyData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyData["baseUrl"] = "https://test.peachpay.co.za"; // context.params.paymentData["testPaymentBaseURL"];
    keyData["token"] = context.params.paymentData["testPublicKey"];
    // keyData["entityId"] = context.params.paymentData["testSecretKey"];
    keyData["key"] = context.params.paymentData["testAccountNo"];
    keyData["clientCode"] = context.params.paymentData["testEncryptionKey"];
  } else {
    keyData["baseUrl"] = "https://peachpay.co.za"; // "https://api.peachpay.co.za"; // context.params.paymentData["livePaymentBaseURL"];
    keyData["token"] = context.params.paymentData["livePublicKey"];
    // keyData["entityId"] = context.params.paymentData["liveSecretKey"];
    keyData["key"] = context.params.paymentData["liveAccountNo"];
    keyData["clientCode"] = context.params.paymentData["liveEncryptionKey"];
  }
  paymentJson = {
    ...keyData,
    "data": {},
  };
  const transactionAmount = parseFloat(context.params.amount).toFixed(2);
  // const accountNumber = context.params.bankDetails.accountNumber;
  // const branchCode = context.params.bankDetails.branchCode;
  //
  // paymentJson["key"] = keyData["key"];
  paymentJson["companyName"] = context.params.generalSettings.data.siteTitle;
  //
  // paymentJson["data"]["entityId"] = keyData["entityId"];
  // paymentJson["data"]["paymentBrand"] =
  //   context.params.type?.toUpperCase() || "VISA";
  // paymentJson["data"]["paymentType"] = null; // "PA" --> PreAuth, "DB" --> Debit
  paymentJson["data"]["amount"] = transactionAmount;
  paymentJson["data"]["branchCode"] = context.params.bankDetails.branchCode;
  paymentJson["data"]["accountNumber"] =
    context.params.bankDetails.accountNumber;
  paymentJson["data"]["initial"] = "";
  paymentJson["data"]["firstName"] = context.params.userData.firstName;
  paymentJson["data"]["lastName"] = context.params.userData.lastName;
  paymentJson["data"]["currency"] =
    context.params.generalSettings.data.currencyCode?.toUpperCase() || "ZAR";
  paymentJson["data"]["transactionReason"] = context.params.transactionReason;
  const finalPaymentData = await peachPaymentUtils.debitWalletTransactions(
    paymentJson
  );
  if (
    finalPaymentData?.status === "OK" &&
    (finalPaymentData?.transactionStatus === "COMPLETE" ||
      finalPaymentData?.transactionStatus === "VALID")
  ) {
    // finalPaymentData = JSON.stringify(finalPaymentData).replace(/e:/g, "e");
    // finalPaymentData = JSON.parse(finalPaymentData);
    finalPaymentData["resultDetails"] = {};
    return {
      "code": 200,
      "data": {
        "authType": "NOAUTH",
        "transactionId": finalPaymentData["id"],
        "paymentIntentId": finalPaymentData["id"],
        "paymentIntentAmount": transactionAmount,
        "transactionAmount": transactionAmount,
        "paymentIntentClientSecret": null,
        "successResponse": finalPaymentData,
        "status": constantUtils.SUCCESS,
      },
      "message": INFO_TRANSACTIONS_SUCCESS,
      // "message": "Valid Account Details",
    };
  } else {
    return {
      "code": 400,
      "data": {},
      // "message": ERROR_IN_TRANSACTIONS,
      "message": "Bank server Busy Now. Please try Later.",
    };
  }
};
//--------------------------------------
module.exports = transactionPeachEvents;

//------------------------------------------
// if (paymentData.gateWay === 'PEACH') {
//   // console.log('payment is peach')
//   const paymentJson = {
//     'data': {},
//   }
//   if (paymentData.mode === constantUtil.SANDBOX) {
//     paymentJson['baseUrl'] = paymentData.testUrl
//     paymentJson['token'] = paymentData.testAuthToken
//     paymentJson['data']['entityId'] = paymentData.testEntityId
//   } else {
//     paymentJson['baseUrl'] = paymentData.liveUrl
//     paymentJson['token'] = paymentData.authToken
//     paymentJson['data']['entityId'] = paymentData.entityId
//   }
//   paymentJson['data']['amount'] = context.params.amount.toString()
//   paymentJson['data']['currency'] = context.params.currencyCode
//     ? context.params.currencyCode.toUpperCase()
//     : generalSettings.data.currencyCode.toUpperCase()
//   paymentJson['data']['paymentBrand'] = context.params.card.type.toUpperCase()
//   paymentJson['data']['paymentType'] = 'DB'
//   paymentJson['data']['card.number'] = context.params.card.cardNumber
//   paymentJson['data']['card.holder'] = context.params.card.holderName
//   paymentJson['data']['card.expiryMonth'] = context.params.card.expiryMonth
//   paymentJson['data']['card.expiryYear'] = context.params.card.expiryYear
//   paymentJson['data']['card.cvv'] = context.params.card.cvv

//   finalPaymentData = await peachPaymentUtils.debitTransactions(paymentJson)
//   const transId = `${helperUtil.randomString(8, 'a')}`
//   if (
//     finalPaymentData &&
//     finalPaymentData.result &&
//     finalPaymentData.result.code === '000.100.110'
//   ) {
//     if (context.params.userType === constantUtil.USER) {
//       await this.broker.emit('user.updateWalletRechargeOrWithdraw', {
//         'userId': id.toString(),
//         'amount': parseFloat(context.params.amount),
//         'transStatus': constantUtil.CREDIT,
//       })
//     } else {
//       await this.broker.emit('professional.updateWalletRechargeOrWithdraw', {
//         'professionalId': id.toString(),
//         'amount': parseFloat(context.params.amount),
//         'transStatus': constantUtil.CREDIT,
//       })
//     }
//     const updateTrasactionData = this.adapter.insert({
//       'transactionType': constantUtil.WALLETRECHARGE,
//       'transactionAmount': context.params.amount,
//       'transactionReason': context.params.reason || '',
//       'paidTransactionId': finalPaymentData.id,
//       'transactionId': transId,
//       'transactionDate': new Date(),
//       'paymentType': constantUtil.CREDIT,
//       'from': {
//         'userType': context.params.userType,
//         'userId': id,
//         'name': userData.firstName + ' ' + userData.lastName || null,
//         'avatar': userData.avatar || null,
//         'phoneNumber': userData.phone.number || null,
//       },
//       'to': {
//         'userType': constantUtil.ADMIN,
//         'userId': null,
//         'name': userData.firstName + ' ' + userData.lastName || null,
//         'avatar': userData.avatar || null,
//         'phoneNumber': userData.phone.number || null,
//       },
//       // 'isActive': 1,
//     })
//     if (!updateTrasactionData)
//       throw new MoleculerError('ERROR IN WALLET REACHARGE')

//     return {
//       'code': 200,
//       'data': {
//         'wallet': {
//           'availableAmount':
//             userData.wallet.availableAmount + context.params.amount,
//           'freezedAmount': userData.wallet.freezedAmount,
//           'dueAmount': userData.wallet.dueAmount,
//           'scheduleFreezedAmount': userData.wallet.scheduleFreezedAmount,
//         },
//       },
//       'message': 'WALLET REACHARGED SUCCESSFULLY',
//     }
//   } else {
//     throw new MoleculerError('ERROR IN WALLET REACHARGE')
//   }
// }
