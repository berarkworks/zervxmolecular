//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
// const Mongoose = require("mongoose");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const axios = require("axios");
const { replace } = require("lodash");

const defaultPaisa = 1;
//----------------- Need to Remove End --------------------

const transactionTigoEvents = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    return {
      "code": response.status,
      "data": response.data,
      "message": response.statusText,
    };
  } catch (error) {
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data?.Error || error.response.data?.message,
    };
  }
};

transactionTigoEvents.newRechargeWalletTransaction = async function (context) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let accessTokenConfig = {};
  // let customerConfig = {};
  // let tokenizationConfig = {};
  let accessTokenData = {};
  let responseData = {};
  const keyConfig = {};
  let accessTokenConfigURL = null;
  let paymentAuthConfigURL = null;

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["client_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["client_id"] = context.params.paymentData["testPublicKey"];
    keyConfig["pin"] = context.params.paymentData["testEncryptionKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
    keyConfig["id"] = context.params.paymentData["testAccountId"];
    keyConfig["account"] = context.params.paymentData["testAccountNo"];
  } else {
    keyConfig["client_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["client_id"] = context.params.paymentData["livePublicKey"];
    keyConfig["pin"] = context.params.paymentData["liveEncryptionKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
    keyConfig["id"] = context.params.paymentData["liveAccountId"];
    keyConfig["account"] = context.params.paymentData["liveAccountNo"];
  }
  const redirectUrl = `${
    context.params.generalSettings.data.redirectUrls.paymentRedirectApiUrl
  }/api/transaction/tigo/webhook${""}`;
  const currencyCode = context.params.generalSettings.data.currencyCode;
  const countryCode = context.params.generalSettings.data.defaultDialCode;
  const countryCodeLong =
    context.params?.generalSettings?.data?.defaultCountryCodeLong || "TZA";
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;
  // //---------------- create Charge End--------------------------
  accessTokenConfig = {
    ...keyConfig,
    "data": {},
  };
  //#region Get Access token
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      accessTokenConfigURL =
        "/v1/oauth/generate/accesstoken-test-2018?grant_type=client_credentials";
    } else {
      accessTokenConfigURL =
        "/v1/oauth/generate/accesstoken?grant_type=client_credentials";
    }
    // const formBody = [];
    // formBody.push("client_secret=" + accessTokenConfig.client_secret);
    // formBody.push("client_id=" + accessTokenConfig.client_id);

    const accessTokenConfigParams = {
      "method": "POST",
      "url": accessTokenConfig.basePaymentURL + accessTokenConfigURL,
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      "data":
        "client_secret=" +
        accessTokenConfig.client_secret +
        "&client_id=" +
        accessTokenConfig.client_id,
      // "x-www-form-urlencoded": {
      //   // "data": {
      //   "client_secret": accessTokenConfig.client_secret,
      //   "client_id": accessTokenConfig.client_id,

      //   // },
      // },
    };
    accessTokenData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //#endregion Get Access token

  if (accessTokenData.code === 200) {
    //#region Get Payment Authentication
    try {
      if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
        paymentAuthConfigURL = "/v1/tigo/payment-auth-test-2018/authorize";
      } else {
        paymentAuthConfigURL = "/v1/tigo/payment-auth/authorize";
      }
      const phoneNumber =
        (context.params?.phoneCode || countryCode) +
        (context.params?.phoneNumber || "");
      const transactionId = (
        context.params.transactionId || accessTokenData.data.issuedAt
      ).toString();
      const transactionAmount = parseFloat(context.params.amount).toFixed(2);

      const paymentAuthConfigParams = {
        "method": "post",
        "url": accessTokenConfig.basePaymentURL + paymentAuthConfigURL,
        "headers": {
          "Content-Type": "application/json",
          "accessToken": accessTokenData.data.accessToken,
        },
        "data": {
          "MasterMerchant": {
            "account": replace(accessTokenConfig.account.trim(), "+", ""),
            "pin": accessTokenConfig.pin,
            "id": accessTokenConfig.id,
          },
          "Subscriber": {
            "account": replace(phoneNumber.trim(), "+", ""),
            "countryCode": replace(countryCode.trim(), "+", ""),
            "country": countryCodeLong,
            "firstName": context.params.userData?.firstName || "firstName",
            "lastName": context.params.userData?.lastName || "lastName",
            "emailId": context.params.userData?.email || "info@twendetech.com",
          },
          "redirectUri": redirectUrl,
          "callbackUri": redirectUrl,
          "language": "eng",
          "originPayment": {
            "amount": transactionAmount,
            "currencyCode": currencyCode,
            "tax": "0.00",
            "fee": "0.00",
          },
          "LocalPayment": {
            "amount": transactionAmount,
            "currencyCode": currencyCode,
          },
          "transactionRefId": transactionId,
        },
      };
      responseData = await axiosConfig(paymentAuthConfigParams);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //#endregion Get Payment Authentication

    // Response
    if (responseData.code === 200) {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = false;
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] =
        responseData.data.transactionRefId.toString();
      responseData["paymentIntentId"] =
        responseData.data.transactionRefId.toString();
      responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
      responseData["redirectUrl"] = responseData.data.redirectUrl.toString();
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Details" };
    } else {
      return { "code": 400, "data": {}, "message": responseData.message };
    }
  } else {
    return { "code": 400, "data": {}, "message": accessTokenData.message };
  }
};

transactionTigoEvents.newWithdrawalWalletCardPayment = async function (
  context
) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let accessTokenConfig = {};
  // let customerConfig = {};
  // let tokenizationConfig = {};
  let accessTokenData = {};
  let responseData = {};
  const keyConfig = {};
  let accessTokenConfigURL = null;
  let paymentAuthConfigURL = null;

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["client_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["client_id"] = context.params.paymentData["testPublicKey"];
    keyConfig["pin"] = context.params.paymentData["testEncryptionKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
    keyConfig["id"] = context.params.paymentData["testAccountId"];
    keyConfig["account"] = context.params.paymentData["testAccountNo"];
  } else {
    keyConfig["client_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["client_id"] = context.params.paymentData["livePublicKey"];
    keyConfig["pin"] = context.params.paymentData["liveEncryptionKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
    keyConfig["id"] = context.params.paymentData["liveAccountId"];
    keyConfig["account"] = context.params.paymentData["liveAccountNo"];
  }
  const redirectUrl = `${
    context.params.generalSettings.data.redirectUrls.paymentRedirectApiUrl
  }/api/transaction/tigo/webhook${""}`;
  const currencyCode = context.params.generalSettings.data.currencyCode;
  const countryCode = context.params.generalSettings.data.defaultDialCode;
  const countryCodeLong =
    context.params?.generalSettings?.data?.defaultCountryCodeLong || "TZA";
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;
  // //---------------- create Charge End--------------------------
  accessTokenConfig = {
    ...keyConfig,
    "data": {},
  };
  //#region Get Access token
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      accessTokenConfigURL =
        "/v1/oauth/generate/accesstoken-test-2018?grant_type=client_credentials";
    } else {
      accessTokenConfigURL =
        "/v1/oauth/generate/accesstoken?grant_type=client_credentials";
    }
    // const formBody = [];
    // formBody.push("client_secret=" + accessTokenConfig.client_secret);
    // formBody.push("client_id=" + accessTokenConfig.client_id);

    const accessTokenConfigParams = {
      "method": "POST",
      "url": accessTokenConfig.basePaymentURL + accessTokenConfigURL,
      "headers": {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      "data":
        "client_secret=" +
        accessTokenConfig.client_secret +
        "&client_id=" +
        accessTokenConfig.client_id,
      // "x-www-form-urlencoded": {
      //   // "data": {
      //   "client_secret": accessTokenConfig.client_secret,
      //   "client_id": accessTokenConfig.client_id,

      //   // },
      // },
    };
    accessTokenData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //#endregion Get Access token

  if (accessTokenData.code === 200) {
    //#region Get Payment Authentication
    try {
      if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
        paymentAuthConfigURL = "/v1/tigo/mfs/depositRemittance-test-2018";
      } else {
        paymentAuthConfigURL = "/v1/tigo/mfs/depositRemittance";
      }
      const phoneNumber =
        (context.params?.phoneCode || countryCode) +
        (context.params?.phoneNumber || "");
      const transactionId = (
        context.params.transactionId || accessTokenData.data.issuedAt
      ).toString();
      const transactionAmount = parseFloat(context.params.amount).toFixed(2);

      const paymentAuthConfigParams = {
        "method": "post",
        "url": accessTokenConfig.basePaymentURL + paymentAuthConfigURL,
        "headers": {
          "Content-Type": "application/json",
          "accessToken": accessTokenData.data.accessToken,
        },
        "data": {
          "transactionRefId": "1300074238",
          "PaymentAggregator": {
            "account": "255123123123",
            "pin": "1234",
            "id": "Company Name",
          },
          "Sender": {
            "firstName": "Jane",
            "lastName": "Doe",
            "msisdn": "2551234123423",
            "emailAddress": "janedoe@mail.com",
          },
          "ReceivingSubscriber": {
            "account": "255111111111",
            "countryCallingCode": "255",
            "countryCode": "TZA",
            "firstName": "John",
            "lastName": "Doe",
          },
          "OriginPayment": {
            "amount": "100.00",
            "currencyCode": "EUR",
            "tax": "10.00",
            "fee": "25.00",
          },
          "exchangeRate": "2182.23",
          "verificationRequest": "true",
          "sendTextMessage": "true",
          "LocalPayment": {
            "amount": "200",
            "currencyCode": "TZS",
          },
        },
        "data1": {
          "MasterMerchant": {
            "account": replace(accessTokenConfig.account.trim(), "+", ""),
            "pin": accessTokenConfig.pin,
            "id": accessTokenConfig.id,
          },
          "Subscriber": {
            "account": replace(phoneNumber.trim(), "+", ""),
            "countryCode": replace(countryCode.trim(), "+", ""),
            "country": countryCodeLong,
            "firstName": context.params.userData?.firstName || "firstName",
            "lastName": context.params.userData?.lastName || "lastName",
            "emailId": context.params.userData?.email || "info@twendetech.com",
          },
          "redirectUri": redirectUrl,
          "callbackUri": redirectUrl,
          "language": "eng",
          "originPayment": {
            "amount": transactionAmount,
            "currencyCode": currencyCode,
            "tax": "0.00",
            "fee": "0.00",
          },
          "LocalPayment": {
            "amount": transactionAmount,
            "currencyCode": currencyCode,
          },
          "transactionRefId": transactionId,
        },
      };
      responseData = await axiosConfig(paymentAuthConfigParams);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //#endregion Get Payment Authentication

    // Response
    if (responseData.code === 200) {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = false;
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] =
        responseData.data.transactionRefId.toString();
      responseData["paymentIntentId"] =
        responseData.data.transactionRefId.toString();
      responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
      responseData["redirectUrl"] = responseData.data.redirectUrl.toString();
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Details" };
    } else {
      return { "code": 400, "data": {}, "message": responseData.message };
    }
  } else {
    return { "code": 400, "data": {}, "message": accessTokenData.message };
  }
};

// transactionTigoEvents.webhooking = async function (context) {
//   let transactionStatus = {};
//   let updateTransaction = {};
//   let notificationObject = {};
//   const eventType = context.params.type;
//   const transactionId = context.params.data?.object?.id;

//   switch (eventType) {
//     case "account.updated":
//     case "account.external_account.created":
//     case "account.external_account.deleted":
//     case "account.external_account.updated":
//     case "balance.available":
//     case "billing_portal.configuration.created":
//     case "billing_portal.configuration.updated":
//     case "capability.updated":
//     case "charge.captured":
//     case "charge.refunded":
//     case "charge.succeeded":
//     case "charge.updated":
//     case "charge.dispute.closed":
//     case "charge.dispute.created":
//     case "charge.dispute.funds_reinstated":
//     case "charge.dispute.funds_withdrawn":
//     case "charge.dispute.updated":
//     case "charge.refund.updated":
//     case "checkout.session.async_payment_succeeded":
//     case "checkout.session.completed":
//     case "coupon.created":
//     case "coupon.deleted":
//     case "coupon.updated":
//     case "credit_note.created":
//     case "credit_note.updated":
//     case "credit_note.voided":
//     case "customer.created":
//     case "customer.deleted":
//     case "customer.updated":
//     case "customer.discount.created":
//     case "customer.discount.deleted":
//     case "customer.discount.updated":
//     case "customer.source.created":
//     case "customer.source.deleted":
//     case "customer.source.updated":
//     case "customer.subscription.created":
//     case "customer.subscription.deleted":
//     case "customer.subscription.pending_update_applied":
//     case "customer.subscription.trial_will_end":
//     case "customer.subscription.updated":
//     case "customer.tax_id.created":
//     case "customer.tax_id.deleted":
//     case "customer.tax_id.updated":
//     case "file.created":
//     case "identity.verification_session.canceled":
//     case "identity.verification_session.created":
//     case "identity.verification_session.processing":
//     case "identity.verification_session.requires_input":
//     case "identity.verification_session.verified":
//     case "invoice.created":
//     case "invoice.deleted":
//     case "invoice.finalized":
//     case "invoice.marked_uncollectible":
//     case "invoice.paid":
//     case "invoice.payment_action_required":
//     case "invoice.payment_succeeded":
//     case "invoice.sent":
//     case "invoice.upcoming":
//     case "invoice.updated":
//     case "invoice.voided":
//     case "invoiceitem.created":
//     case "invoiceitem.deleted":
//     case "invoiceitem.updated":
//     case "issuing_authorization.created":
//     case "issuing_authorization.updated":
//     case "issuing_card.created":
//     case "issuing_card.updated":
//     case "issuing_cardholder.created":
//     case "issuing_cardholder.updated":
//     case "issuing_dispute.closed":
//     case "issuing_dispute.created":
//     case "issuing_dispute.updated":
//     case "issuing_transaction.created":
//     case "issuing_transaction.updated":
//     case "mandate.updated":
//     case "order.created":
//     case "order.payment_succeeded":
//     case "order.updated":
//     case "order_return.created":
//     case "payment_intent.amount_capturable_updated":
//     case "payment_intent.created":
//     case "payment_intent.requires_action":
//     case "payment_intent.succeeded":
//     case "payment_link.created":
//     case "payment_link.updated":
//     case "payment_method.attached":
//     case "payment_method.automatically_updated":
//     case "payment_method.detached":
//     case "payment_method.updated":
//     case "payout.canceled":
//     case "payout.created":
//     case "payout.paid":
//     case "payout.updated":
//     case "person.created":
//     case "person.deleted":
//     case "person.updated":
//     case "plan.created":
//     case "plan.deleted":
//     case "plan.updated":
//     case "price.created":
//     case "price.deleted":
//     case "price.updated":
//     case "product.created":
//     case "product.deleted":
//     case "product.updated":
//     case "promotion_code.created":
//     case "promotion_code.updated":
//     case "quote.accepted":
//     case "quote.canceled":
//     case "quote.created":
//     case "quote.finalized":
//     case "radar.early_fraud_warning.created":
//     case "radar.early_fraud_warning.updated":
//     case "recipient.created":
//     case "recipient.deleted":
//     case "recipient.updated":
//     case "reporting.report_run.succeeded":
//     case "review.closed":
//     case "review.opened":
//     case "setup_intent.canceled":
//     case "setup_intent.created":
//     case "setup_intent.requires_action":
//     case "setup_intent.succeeded":
//     case "sigma.scheduled_query_run.created":
//     case "sku.created":
//     case "sku.deleted":
//     case "sku.updated":
//     case "source.canceled":
//     case "source.chargeable":
//     case "source.mandate_notification":
//     case "source.refund_attributes_required":
//     case "source.transaction.created":
//     case "source.transaction.updated":
//     case "subscription_schedule.aborted":
//     case "subscription_schedule.canceled":
//     case "subscription_schedule.completed":
//     case "subscription_schedule.created":
//     case "subscription_schedule.released":
//     case "subscription_schedule.updated":
//     case "tax_rate.created":
//     case "tax_rate.updated":
//     case "topup.canceled":
//     case "topup.created":
//     case "topup.reversed":
//     case "topup.succeeded":
//     case "transfer.created":
//     case "transfer.paid":
//     case "transfer.reversed":
//     case "transfer.updated":
//       transactionStatus = constantUtils.SUCCESS;
//       break;
//     case "charge.failed":
//     case "invoice.finalization_failed":
//     case "invoice.payment_failed":
//     case "checkout.session.async_payment_failed":
//     case "order.payment_failed":
//     case "payment_intent.payment_failed":
//     case "payout.failed":
//     case "reporting.report_run.failed":
//     case "setup_intent.setup_failed":
//     case "source.failed":
//     case "subscription_schedule.expiring":
//     case "topup.failed":
//     case "transfer.failed":
//       transactionStatus = constantUtils.FAILED;
//       break;
//     case "charge.pending":
//     case "issuing_dispute.funds_reinstated":
//     case "issuing_dispute.submitted":
//     case "payment_intent.canceled":
//     case "payment_intent.processing":
//       transactionStatus = constantUtils.PENDING;
//       break;
//     case "charge.expired":
//     case "checkout.session.expired":
//     case "customer.source.expiring":
//     case "customer.subscription.pending_update_expired":
//       transactionStatus = constantUtils.FAILED;
//       break;
//     // ... handle other event types
//     default:
//       transactionStatus = constantUtils.FAILED;
//       return null;
//     // break
//   }

//   const transaction = await this.adapter.model.findOneAndUpdate(
//     { "transactionId": transactionId },
//     { "webhookresponse": context.params },
//     {
//       "new": true, //unique reference find in webhook
//     }
//   );

//   if (transactionStatus === constantUtils.SUCCESS) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     return null;
//   } else if (transactionStatus === constantUtils.FAILED) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     //-------------------------------------
//     // reterive the amount to whose is the user or professional
//     if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
//       // retrn the amount of professional
//       if (transaction.to.userType === constantUtils.PROFESSIONAL) {
//         let userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         let walletupdated = await this.broker.emit(
//           "professional.updateWalletAmount",
//           {
//             "professionalId": userData._id.toString(),
//             "availableAmount":
//               userData.wallet.availableAmount + transaction.transactionAmount,
//             "freezedAmount": userData.wallet.freezedAmount,
//           }
//         );
//         walletupdated = walletupdated && walletupdated[0];
//         if (walletupdated.code !== 200) {
//           throw new MoleculerError(
//             "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED",
//             500
//           );
//         }

//         // send notification to professional
//         let updatedtransaction = await this.adapter.model.findOne({
//           "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//         });
//         updatedtransaction = updatedtransaction.toJSON();

//         // remove not neede data for send notification

//         //updated user detailes

//         userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         //
//         notificationObject = {
//           "clientId": context.params.clientId,
//           "data": {
//             "type": constantUtils.NOTIFICATIONTYPE,
//             "userType": constantUtils.PROFESSIONAL,
//             "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
//             "timestamp": Date.now(),
//             "message":
//               updatedtransaction.webhookresponse.data.object.description,
//             "details": {
//               "walletAmount": userData.wallet.availableAmount,
//               "paymentType": updatedtransaction.paymentType,
//               "transactionStatus": updatedtransaction.transactionStatus,
//               "_id": updatedtransaction._id,
//             },
//           },
//           "registrationTokens": [
//             {
//               "token": userData.deviceInfo[0].deviceId,
//               "id": userData._id.toString(),
//               "deviceType": userData.deviceInfo[0].deviceType,
//               "platform": userData.deviceInfo[0].platform,
//               "socketId": userData.deviceInfo[0].socketId,
//             },
//           ],
//         };
//         console.log(notificationObject);
//       }
//       // USER
//       if (transaction.to.userType === constantUtils.USER) {
//         let userData = await this.broker.emit("user.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         let walletupdated = await this.broker.emit("user.updateWalletAmount", {
//           "userId": userData._id.toString(),
//           "availableAmount":
//             userData.wallet.availableAmount + transaction.transactionAmount,
//           "freezedAmount": userData.wallet.freezedAmount,
//           "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
//         });
//         walletupdated = walletupdated && walletupdated[0];
//         if (walletupdated.code !== 200) {
//           throw new MoleculerError(
//             "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED",
//             500
//           );
//         }

//         // send notification to professional
//         let updatedtransaction = await this.adapter.model.findOne({
//           "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//         });
//         updatedtransaction = updatedtransaction.toJSON();
//         //updated user detailes

//         userData = await this.broker.emit("professional.getById", {
//           "id": transaction.to.userId.toString(),
//         });
//         userData = userData && userData[0];
//         if (!userData) {
//           // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
//           throw new MoleculerError("SOMETHING WENT WRONG", 500);
//         }
//         //
//         notificationObject = {
//           "clientId": context.params.clientId,
//           "data": {
//             "type": constantUtils.NOTIFICATIONTYPE,
//             "userType": constantUtils.USER,
//             "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
//             "timestamp": Date.now(),
//             "message":
//               updatedtransaction.webhookresponse.data.object.description,
//             "details": lzStringEncode({
//               "walletAmount": userData.wallet.availableAmount,
//               "paymentType": updatedtransaction.paymentType,
//               "transactionStatus": updatedtransaction.transactionStatus,
//               "_id": updatedtransaction._id,
//             }),
//           },
//           "registrationTokens": [
//             {
//               "token": userData.deviceInfo[0].deviceId,
//               "id": userData._id.toString(),
//               "deviceType": userData.deviceInfo[0].deviceType,
//               "platform": userData.deviceInfo[0].platform,
//               "socketId": userData.deviceInfo[0].socketId,
//             },
//           ],
//         };
//         console.log(notificationObject);
//       }
//       /* SOCKET PUSH NOTIFICATION */
//       this.broker.emit("socket.sendNotification", notificationObject);
//       // this.broker.emit('socket.statusChangeEvent', notificationObject)

//       /* FCM */
//       this.broker.emit("admin.sendFCM", notificationObject);
//       return null;
//     }
//   } else if (transactionStatus === constantUtils.PENDING) {
//     updateTransaction = await this.adapter.model.updateOne(
//       {
//         "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         "transactionStatus": transactionStatus,
//       }
//     );
//     return null;
//   }
//   return null;
// };
//--------------------------------------
module.exports = transactionTigoEvents;
