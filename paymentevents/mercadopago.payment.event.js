//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const axios = require("axios");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const fs = require("fs");
const path = require("path");
const https = require("https");
// import { MercadoPagoConfig, Payment } from "mercadopago";
// const {
//   MercadoPagoConfig,
//   Payment,
//   Customer,
//   CustomerCard,
//   CardToken,
// } = require("mercadopago");

const notifyMessage = require("../mixins/notifyMessage.mixin");

const defaultCent = 100;
//----------------- Need to Remove End --------------------

const baseURL = "https://walrus-app-vfyf9.ondigitalocean.app";

const transactionMercadopagoEvents = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    const successData = {
      "code": response.status,
      "config": config,
      "data": response.data,
      "status": response.data?.status,
      "message": response.statusText,
    };
    // try {
    //   await axios({
    //     "method": "GET",
    //     "port": 443,
    //     "url": `https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec`,
    //     "headers": {
    //       "Content-Type": "application/json",
    //     },
    //     "data": { "APIStatus": "success", "gatwayResponse": successData },
    //   });
    // } catch (e) {}
    return successData;
  } catch (error) {
    const errorData = {
      "code": error?.response?.status,
      // "config": config,
      "data": error?.response?.data, // || JSON.stringify(error) || {},
      "status": "failed",
      "message":
        error?.response?.data?.Error ||
        error?.response?.data?.errors?.[0] ||
        error?.response?.data?.message ||
        error.message,
    };
    // try {
    //   await axios({
    //     "method": "GET",
    //     "port": 443,
    //     "url": `https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec`,
    //     "headers": {
    //       "Content-Type": "application/json",
    //     },
    //     "data": {
    //       "APIStatus": "failed",
    //       "gatwayResponse": errorData,
    //     },
    //   });
    // } catch (e) {
    //   const x = "sss";
    // }
    return errorData;
  }
};

transactionMercadopagoEvents.checkValidCard = async function (context) {
  const keyConfig = {};
  const responseData = {};

  const { INFO_TRANSACTIONS_SUCCESS, INFO_INVALID_DETAILS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const isCVVRequired = context.params?.paymentData["isCVVRequired"] || false;
  //------------------------------
  // const redirectUrl =
  //   "http://192.168.1.36:3000/admin/home/mercadopago/" +
  //   keyConfig.idempotencyKey +
  //   "/" +
  //   keyConfig.publishableKey;
  // //------------------------------
  // const redirectUrl =
  //   context.params.generalSettings.data.redirectUrls?.paymentGatewayUrl +
  //     "/admin/home/mercadopago/" +
  //     "fliximobi_" +
  //     Date.now().toString() + //keyConfig.idempotencyKey + // -->idempotencyKey
  //     "/" +
  //     keyConfig.publishableKey +
  //     "/" +
  //     context.params.userData?.firstName || "test"; //FirstName
  // const redirectUrl =
  //   "https://lobster-app-yxmgx.ondigitalocean.app/admin/home/mercadopago/" +
  //     "fliximobi_" +
  //     Date.now().toString() + //keyConfig.idempotencyKey + // -->idempotencyKey
  //     "/" +
  //     keyConfig.publishableKey +
  //     "/" +
  //     context.params.userData?.firstName || "test"; //FirstName
  const redirectUrl =
    `${baseURL}/card?publicKey=` +
      keyConfig.publishableKey +
      "&languageCode=" +
      context.params?.userData?.languageCode || "en";
  //------------------------------
  let paymentGateWayCustomerId =
    context.params.paymentGateWayCustomerId || null;
  //------------------------------
  if (!paymentGateWayCustomerId) {
    try {
      const accessTokenConfigParams = {
        "method": "POST",
        "port": 443,
        "url": `${keyConfig.baseUrl}v1/customers`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + keyConfig.secretKey,
        },
        "data": {
          "email": context.params.userData.email,
          "first_name": context.params.userData.firstName,
          "last_name": context.params.userData.lastName,
          "phone": {
            "area_code": context.params.userData.phone.code,
            "number": context.params.userData.phone.number,
          },
          // "identification": {
          //   "type": "CPF",
          //   "number": "12345678900",
          // },
        },
      };
      let finalCustomerData = await axiosConfig(accessTokenConfigParams);
      if (finalCustomerData.code === 400) {
        const checkCustomerConfigParams = {
          "method": "GET",
          "port": 443,
          "url": `${keyConfig.baseUrl}v1/customers/search`,
          "headers": {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + keyConfig.secretKey,
          },
          "data": {
            "email": context.params.userData.email,
          },
        };
        finalCustomerData = await axiosConfig(checkCustomerConfigParams);
      }
      if (finalCustomerData.code === 400) {
        accessTokenConfigParams["data"]["email"] =
          Date.now() + context.params.userData.email;
        finalCustomerData = await axiosConfig(accessTokenConfigParams);
      }
      if (finalCustomerData.code === 200) {
        paymentGateWayCustomerId =
          finalCustomerData?.data?.results?.[0]?.id?.toString();
      } else if (finalCustomerData.code === 201) {
        paymentGateWayCustomerId = finalCustomerData?.data?.id?.toString();
      }
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  }
  //------------------------------
  return {
    "code": 200,
    "data": {
      "authType": "NOAUTH",
      "isDefault": true, //need to Change Dynamic
      "isCVVRequired": isCVVRequired,
      "cardToken": null, //TokenId
      "embedtoken": null, //TokenId
      "successResponse": {}, // finalPaymentData,
      "transactionAmount": context.params.minimumChargeAmount,
      "transactionId": null, // finalPaymentData["id"],
      "paymentGateWayCustomerId": paymentGateWayCustomerId,
      "paymentIntentId": null, // finalPaymentData["id"],
      "paymentIntentClientSecret": null,
      "redirectUrl": redirectUrl,
      "redirectMethodType": "GET",
      "status": constantUtils.SUCCESS,
    },
    "message": INFO_TRANSACTIONS_SUCCESS,
  };

  // try {
  //   const accessTokenConfigParams = {
  //     "method": "POST",
  //  "port": 443,
  //     "url": `${keyConfig.baseUrl}v1/customers`,
  //     "headers": {
  //       "Content-Type": "application/json",
  //       "Authorization": "Bearer " + keyConfig.secretKey,
  //     },
  //     "data": {
  //       "email": "jhon1@doe.com",
  //       // "first_name": "Jhon",
  //       // "last_name": "Doe",
  //       // "phone": {
  //       //   "area_code": "55",
  //       //   "number": "991234567",
  //       // },
  //       // "identification": {
  //       //   "type": "CPF",
  //       //   "number": "12345678900",
  //       // },
  //     },
  //   };
  //   // finalCustomerData = await axiosConfig(accessTokenConfigParams);
  //   //--------------------------------------
  //   const client = new MercadoPagoConfig({ accessToken: keyConfig.secretKey });
  //   const customer = new Customer(client);
  //   const customerCard = new CustomerCard(client);
  //   const cardToken = new CardToken(client);
  //   const cardClient = new CustomerCard(client);
  //   // //--------------------------------------
  //   // const body = {
  //   //   email: Date.now() + "@doe.com",
  //   // };
  //   // try {
  //   //   await customer.create({ body: body }).then((result) => {
  //   //     const customerCard = new CustomerCard(client);
  //   //     const body = {
  //   //       token: result.token,
  //   //     };
  //   //     customerCard
  //   //       // .create({ customerId: "customer_id", body })
  //   //       .create({ id: result.id, body })
  //   //       .then((result1) => {
  //   //         const vvv = console.log(result1);
  //   //       });
  //   //   });
  //   // } catch (eee) {
  //   //   const cccc = eee.message;
  //   // }
  //   // //--------------------------------------
  //   const custEmail = Date.now() + "@doe.com";
  //   let body = {
  //     // email: "test_payer_123451@testuser.com",
  //     "email": custEmail,
  //     // "first_name": "Jhon",
  //     // "last_name": "Doe",
  //     // "phone": {
  //     //   "area_code": "55",
  //     //   "number": "991234567",
  //     // },
  //     // "identification": {
  //     //   "type": "CPF",
  //     //   "number": "12345678900",
  //     // },
  //     "cards": [
  //       {
  //         "expiration_month": 6,
  //         "expiration_year": 2023,
  //         "first_six_digits": 423564,
  //         "last_four_digits": 5682,
  //       },
  //     ],
  //   };
  //   let newCustomer, card, tttt444;
  //   try {
  //     // const tttt = await customer.create({ body: body }).then((result) => {
  //     //   console.log(JSON.stringify(result));
  //     // });
  //     newCustomer = await customer.create({ body: body });
  //     card = await cardToken.create({
  //       // "cardholderName": "test",
  //       // "cardNumber": 5031433215406351,
  //       // "expirationMonth": 06,
  //       // "expirationYear": 2024,
  //       // "expirationDate": 423564,
  //       // "securityCode": 5682,
  //       // "installments": 1,
  //       // "identificationType": 1,
  //       // "identificationNumber": 1,
  //       // "issuer": 1,
  //       // //------------
  //       // cardNumber: "5031433215406351",
  //       // cardholderName: "APRO",
  //       // cardExpirationMonth: "11",
  //       // cardExpirationYear: "2025",
  //       // securityCode: "123",
  //       // identificationType: "CPF",
  //       // identificationNumber: "12345678912",
  //       //---------------
  //       "cardNumber": "4509953566233704",
  //       "email": "test1@test.com.ar",
  //       "cardholder": {
  //         "name": "APRO",
  //       },
  //       "expirationYear": "2025",
  //       "expirationMonth": "11",
  //       "securityCode": "123",
  //       "payment_method_id": "visa",
  //       "issuer_id": "123456",
  //     });
  //     // .then((item) => {
  //     //   console.log(JSON.stringify(item));
  //     // });
  //     body = {
  //       "token": card.id, // "9b2d63e00d66a8c721607214ceda233a",
  //       // "cards": {
  //       //   // "token": tttt.token,
  //       //   "expiration_month": 6,
  //       //   "expiration_year": 2023,
  //       //   "first_six_digits": 423564,
  //       //   "last_four_digits": 5682,
  //       // },
  //     };
  //     tttt444 = await customerCard.create({
  //       customerId: newCustomer.id,
  //       body,
  //     });
  //     await cardClient.create({
  //       customerId: newCustomer.id,
  //       body: {
  //         "token": card.id,
  //         "payment_method": "debit_card",
  //       },
  //     });
  //     const serchCustomer = await customer.search({
  //       options: { email: custEmail },
  //     });
  //     // customer.create({ body: body }).then((result) => {
  //     //   const customerCard = new CustomerCard(client);
  //     //   body = {
  //     //     token: result.token,
  //     //   };
  //     //   customerCard
  //     //     .create({ customerId: "customer_id", body })
  //     //     .then((result) => console.log(result));
  //     // });
  //   } catch (sss) {
  //     const xxxx = sss.message;
  //   }
  //   //--------------------------------------
  // } catch (error) {
  //   // return { "code": 400, "data": {}, "message": error.message };
  // }
  // return { "code": 200, "data": {}, "message": "" };
  // // if (finalCustomerData) {
  // //   //---------------- create Charge End--------------------------
  // //   const minimumChargeAmount =
  // //     parseFloat(context.params.minimumChargeAmount) * defaultCent; // Defaule Card Verification deduction
  // //   paymentConfig = {
  // //     ...keyConfig,
  // //     "data": {},
  // //   };
  // //   paymentConfig["data"]["amount"] = parseInt(minimumChargeAmount); // Defaule Card Verification deduction
  // //   paymentConfig["data"]["currency"] =
  // //     context.params.generalSettings.data.currencyCode;
  // //   // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
  // //   paymentConfig["data"]["payment_method_types"] = ["card"];
  // //   paymentConfig["data"]["capture_method"] = "automatic"; //automatic or manual
  // //   // paymentConfig["data"]["confirm"] = false; //CZ Altered
  // //   paymentConfig["data"]["confirm"] = true; //CZ Altered
  // //   // paymentConfig['data']['automatic_payment_methods'] = true
  // //   paymentConfig["data"]["customer"] = stripeCustomerId;
  // //   paymentConfig["data"]["description"] = context.params.transactionReason;
  // //   // paymentConfig["data"]["automatic_tax"] = {};
  // //   // paymentConfig["data"]["automatic_tax"]["enabled]"] = true;
  // //   //Stripe Call For create Charge
  // //   try {
  // //     const stripe = require("stripe")(paymentConfig.secretKey);
  // //     finalPaymentData = await stripe.paymentIntents.create(paymentConfig.data);
  // //   } catch (error) {
  // //     return { "code": 400, "data": {}, "message": error.message };
  // //   }
  // //   // Response
  // //   if (finalPaymentData) {
  // //     responseData["authType"] = "NOAUTH";
  // //     // // responseData['stripeCardToken'] = finalTokenData.id
  // //     // responseData["cardToken"] = finalTokenData.id;
  // //     // responseData["embedtoken"] = finalTokenData.id;
  // //     responseData["cardToken"] = context.params.cardTokenId;
  // //     responseData["embedtoken"] = context.params.cardTokenId;
  // //     responseData["isDefault"] = true;
  // //     responseData["isCVVRequired"] = true;
  // //     responseData["stripeCustomerId"] = finalCustomerData.id; //  Need to Remove this key
  // //     responseData["paymentGateWayCustomerId"] = finalCustomerData.id;
  // //     responseData["paymentIntentId"] = finalPaymentData.id;
  // //     responseData["paymentIntentClientSecret"] =
  // //       finalPaymentData.client_secret;
  // //     responseData["transactionAmount"] = parseInt(
  // //       context.params.minimumChargeAmount
  // //     );
  // //     responseData["transactionId"] = finalPaymentData.id;
  // //     // responseData['paymentTransactionId'] = finalPaymentData.id.toString()
  // //     responseData["successResponse"] = finalPaymentData;
  // //     responseData["status"] = finalPaymentData.status;
  // //     return { "code": 200, "data": responseData, "message": "Valid Card" };
  // //   } else {
  // //     return { "code": 400, "data": {}, "message": "invalid Card" };
  // //   }
  // // } else {
  // //   return { "code": 400, "data": {}, "message": "invalid Card" };
  // // }
};

transactionMercadopagoEvents.updateCardToken = async function (context) {
  const keyConfig = {};
  const responseData = {};
  let finalCustomerData = {};

  const { INFO_TRANSACTIONS_SUCCESS, INFO_INVALID_DETAILS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //------------------------------
  let paymentGateWayCustomerId =
    context.params.paymentGateWayCustomerId || null;
  //------------------------------
  if (!paymentGateWayCustomerId) {
    try {
      const accessTokenConfigParams = {
        "method": "POST",
        "port": 443,
        "url": `${keyConfig.baseUrl}v1/customers`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + keyConfig.secretKey,
        },
        "data": {
          "email": context.params.userData.email,
          "first_name": context.params.userData.firstName,
          "last_name": context.params.userData.lastName,
          "phone": {
            "area_code": context.params.userData.phone.code,
            "number": context.params.userData.phone.number,
          },
          // "identification": {
          //   "type": "CPF",
          //   "number": "12345678900",
          // },
        },
      };
      finalCustomerData = await axiosConfig(accessTokenConfigParams);
      if (finalCustomerData.code === 400) {
        const checkCustomerConfigParams = {
          "method": "GET",
          "port": 443,
          "url": `${keyConfig.baseUrl}v1/customers/search`,
          "headers": {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + keyConfig.secretKey,
          },
          "data": {
            "email": context.params.userData.email,
          },
        };
        finalCustomerData = await axiosConfig(checkCustomerConfigParams);
      }
      if (finalCustomerData.code === 400) {
        accessTokenConfigParams["data"]["email"] =
          Date.now() + context.params.userData.email;
        finalCustomerData = await axiosConfig(accessTokenConfigParams);
      }
      if (finalCustomerData.code === 200) {
        paymentGateWayCustomerId =
          finalCustomerData?.data?.results?.[0]?.id?.toString();
      } else if (finalCustomerData.code === 201) {
        paymentGateWayCustomerId = finalCustomerData?.data?.id?.toString();
      }
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  }
  //----------------------------
  if (paymentGateWayCustomerId) {
    try {
      const accessTokenConfigParams = {
        "method": "POST",
        "port": 443,
        "url": `${keyConfig.baseUrl}v1/customers/${paymentGateWayCustomerId}/cards`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + keyConfig.secretKey,
        },
        "data": {
          "token": context.params.embedtoken,
          "payment_method_id": context.params.paymentMethodId,
        },
      };
      finalCustomerData = await axiosConfig(accessTokenConfigParams);
      if (finalCustomerData.code === 201) {
        responseData["cardId"] = finalCustomerData.data.id;
        responseData["embedtoken"] = context.params.embedtoken;
        responseData["paymentMethodId"] = context.params.paymentMethodId;
        responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      }
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    return {
      "code": 200,
      "data": responseData,
      "message": "Card Updated Successfully",
    };
  } else {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

transactionMercadopagoEvents.newRechargeWalletTransaction = async function (
  context
) {
  let finalPaymentData = {},
    redirectUrl = null,
    redirectMethodType = null;
  const responseData = {};
  const keyConfig = {};

  const {
    INFO_TRANSACTIONS_SUCCESS,
    ERROR_IN_TRANSACTIONS,
    WARNING_BANK_SERVER_BUSY,
  } = await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  // const client = new MercadoPagoConfig({ accessToken: keyConfig.secretKey });

  // let captureConfig = {};
  //
  //-------------- charges payment Intents Start -----------------------
  // captureConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // const body = {
  //   // transaction_amount: 100.0, // context.params.amount,
  //   // token: context.params?.cardData?.data?.embedtoken,
  //   // description: "context.params.transactionReason",
  //   // installments: 1,
  //   // payment_method_id: "master", //context.params?.cardData?.data?.type, // "visa",
  //   // payer: {
  //   //   email: context.params.userData.email,
  //   // },
  //   // capture: false,
  //   // //----------------
  //   // additional_info: {
  //   //   items: [
  //   //     {
  //   //       id: "MLB2907679857",
  //   //       title: "Point Mini",
  //   //       quantity: 1,
  //   //       unit_price: 58.8,
  //   //     },
  //   //   ],
  //   // },
  //   // payer: {
  //   //   email: context.params.userData.email,
  //   // },
  //   // transaction_amount: 110.0,
  //   // installments: 1,
  //   // payment_method_id: "pix",
  //   //------------
  //   additional_info: {
  //     items: [
  //       {
  //         id: "MLB2907679857",
  //         title: "Point Mini",
  //         description: "Point product for card payments via Bluetooth.",
  //         picture_url:
  //           "https://http2.mlstatic.com/resources/frontend/statics/growth-sellers-landings/device-mlb-point-i_medium2x.png",
  //         category_id: "electronics",
  //         quantity: 1,
  //         unit_price: 58,
  //       },
  //     ],
  //     payer: {
  //       first_name: "Test",
  //       last_name: "Test",
  //       phone: {
  //         area_code: "11",
  //         number: "987654321",
  //       },
  //       address: {
  //         street_number: null,
  //       },
  //     },
  //     shipments: {
  //       receiver_address: {
  //         zip_code: "12312-123",
  //         state_name: "Rio de Janeiro",
  //         city_name: "Buzios",
  //         street_name: "Av das Nacoes Unidas",
  //         street_number: 3003,
  //       },
  //     },
  //   },
  //   application_fee: null,
  //   binary_mode: false,
  //   campaign_id: null,
  //   capture: false,
  //   coupon_amount: null,
  //   description: "Payment for product",
  //   differential_pricing_id: null,
  //   external_reference: "MP0001",
  //   installments: 1,
  //   metadata: null,
  //   payer: {
  //     entity_type: "individual",
  //     type: "customer",
  //     email: "test_user_12322@testuser.com",
  //     identification: {
  //       type: "CPF",
  //       number: "95749019047",
  //     },
  //   },
  //   payment_method_id: "master",
  //   token: "ff8080814c11e237014c1ff593b57b4d",
  //   transaction_amount: 58,
  // };
  //-------------------

  //-------------------
  try {
    // const deviceId =
    // "f_DMgABJRkmj7odPglPZQA:APA91bGzaTED6bS3gqAfqkEWTj8F4bgqKYnTnQYD0Z9cWX2ofzcdNDq8mF-d_z9496odQDZ57u9vgcjXcBnjRPKTRHeI86D8ayw7tR4RhE7iem19CtzoXJPdwv0LsSoDKMMF1EXKqS2Y";
    const notificationUrl =
      context.params.generalSettings.data.redirectUrls?.paymentRedirectApiUrl +
      "/api/transaction/mercadopago/webhookingMercadopagoPixKeyPayment";
    //--------
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}v1/payments`,
      "headers": {
        "accept": "application/json",
        "Content-Type": "application/json",
        // "X-meli-session-id": deviceId,
        "Authorization": "Bearer " + keyConfig.secretKey,
        "X-Idempotency-Key":
          context.params.generalSettings.data.siteTitle +
          "_" +
          Date.now().toString(), //context.params.transactionReason,
      },
      "data":
        context.params.cardId === constantUtils.CONST_PIX
          ? {
              "transaction_amount": parseFloat(
                context.params.amount.toFixed(2)
              ),
              "description": context.params.transactionReason,
              "payment_method_id": "pix", // "pix",
              "payer": {
                "email": context.params.email || context.params.userData.email,
              },
              "capture": true,
              "notification_url": notificationUrl,
              //   context.params.generalSettings.data.redirectUrls
              //     ?.paymentRedirectApiUrl +
              //   "/api/transaction/mercadopago/webhookingMercadopagoPixKeyPayment",
              // // "https://api.flixmobi.net/api/transaction/stripe/webhook",
            }
          : {
              // "transaction_amount": context.params.amount,
              // "description": context.params.transactionReason,
              // "installments": 1,
              // // "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
              // "payer": {
              //   // "id": null,
              //   // "first_name": "Valdir de Jesus Correia Junior ",
              //   // "entity_type": "individual",
              //   "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
              //   "type": "customer", //"valdircorreia2012@gmail.com",
              //   // "email": testEmail, //context.params.userData.email,
              //   // "identification": {
              //   //   "type": "CPF",
              //   //   "number": "02092429574",
              //   // },
              // },
              // "capture": true,
              // "token": context.params?.cardData?.data?.embedtoken,
              // "metadata": {
              //   "order_number": context.params.transactionReason, //"ordrr_CAC5J7A3c34Ne1Wv1D1240",
              // },
              // "external_reference": context.params.transactionReason,
              // "notification_url": notificationUrl,
              //-----------------------
              "transaction_amount": parseFloat(
                context.params.amount.toFixed(2)
              ),
              "installments": 1,
              "capture": true,
              "binary_mode": false,
              "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
              "token":
                context.params?.embedtoken ||
                context.params?.cardData?.data?.embedtoken,
              "external_reference": context.params.transactionReason,
              "notification_url": notificationUrl,
              "metadata": {
                "order_number": context.params.transactionReason,
              },
              "payer": {
                "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
                "type": "customer",
              },
              "statement_descriptor": "Flixmobi",
              "description": context.params.transactionReason,
              // "additional_info": {
              //   "items": [
              //     {
              //       "id": "1941",
              //       "title": "flixmobi",
              //       "description": "flixmobi Ride Payment",
              //       "picture_url":
              //         "https://flixmobi.com/storage/logo/logo-topo1.png",
              //       "category_id": "services",
              //       "quantity": 1,
              //       "unit_price": context.params.amount,
              //     },
              //   ],
              //   "payer": {
              //     "first_name": "Valdir",
              //     "last_name": "De Jesus Correia Junior",
              //     "is_prime_user": "-1",
              //     "is_first_purchase_online": "-1",
              //     "last_purchase": new Date().toISOString(),
              //     "phone": {
              //       "area_code": "75",
              //       "number": "999813125",
              //     },
              //     "address": {
              //       "zip_code": "38408256",
              //       "street_name": "Rua Armando Tucci",
              //       "street_number": "452",
              //     },
              //     "registration_date": new Date().toISOString(),
              //   },
              // },
            },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
    //------------------
    // const client = new MercadoPagoConfig({
    //   accessToken:
    //     "TEST-669017362209586-082509-df511c9983f32302bdf925ee69b29293-1962818044",
    // });
    // const payment = new Payment(client);
    // finalPaymentData = await payment.create({
    //   body: body,
    //   // requestOptions: { idempotencyKey: "<SOME_UNIQUE_VALUE>" },
    //   requestOptions: { idempotencyKey: Date.now() },
    // });
    // .then((item) => {
    //   console.log(item);
    // })
    // .catch((each) => {
    //   console.log(each);
    // });
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  if (
    finalPaymentData?.code === 201 &&
    (finalPaymentData?.status?.toLowerCase() === "approved" ||
      (context.params.cardId === constantUtils.CONST_PIX &&
        finalPaymentData?.status?.toLowerCase() === "pending"))
  ) {
    if (context.params.cardId === constantUtils.CONST_PIX) {
      redirectMethodType = "GET";
      redirectUrl =
        finalPaymentData?.data?.point_of_interaction?.transaction_data
          ?.ticket_url;
    }
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    // responseData['stripeCardToken'] = finalTokenData.id
    responseData["paymentGateWayCustomerId"] = null;
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] = finalPaymentData.data.id;
    responseData["paymentIntentId"] = finalPaymentData.data.id;
    responseData["paymentIntentAmount"] = context.params.amount;
    responseData["transactionAmount"] = context.params.amount;
    responseData["paymentIntentClientSecret"] = null;
    responseData["redirectUrl"] = redirectUrl;
    responseData["redirectMethodType"] = redirectMethodType;
    responseData["successResponse"] = finalPaymentData.data;
    responseData["status"] = finalPaymentData.status;
    return {
      "code": 200,
      "data": responseData,
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": WARNING_BANK_SERVER_BUSY,
    };
  }
};

transactionMercadopagoEvents.verifyCVVByPaymentGatway = async function (
  context
) {
  let paymentJson = {};
  const keyConfig = {};
  const {
    INFO_INVALID_DETAILS,
    INFO_CVV_VERIFY_SUCCESS,
    WARNING_CVV_VEEIFY_FAILED,
  } = await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //------------------------------
  const isCVVRequired = context.params.paymentData["isCVVRequired"] || false;
  const paymentGateWayCustomerId =
    context.params.userData.paymentGateWayCustomerId || null;
  if (paymentGateWayCustomerId) {
    try {
      //
      // const redirectUrl =
      //   context.params.generalSettings.data.redirectUrls?.paymentGatewayUrl +
      //     "/admin/home/mercadopago/" +
      //     "fliximobi_" +
      //     Date.now().toString() + //keyConfig.idempotencyKey + // -->idempotencyKey
      //     "/" +
      //     keyConfig.publishableKey +
      //     "/" +
      //     context.params.userData?.firstName || "test"; //FirstName
      const lastFourDigit =
        context.params.cardDetails.data.cardNumber.slice(-4);
      const redirectUrl = `${baseURL}/cvv?publicKey=${
        keyConfig.publishableKey
      }&cardId=${
        context.params.cardDetails.data.cardId
      }&lastFourDigit=${lastFourDigit}&paymentMethod=${
        context.params.cardDetails.data.type
      }&languageCode=${context.params?.userData?.languageCode || "en"}`;
      //------------------------------
      return {
        "code": 200,
        "data": {
          "authType": "NOAUTH",
          "isCVVRequired": isCVVRequired,
          "successResponse": null,
          "redirectUrl": redirectUrl,
          "redirectMethodType": "GET",
          "status": constantUtils.SUCCESS,
        },
        "message": INFO_CVV_VERIFY_SUCCESS,
      };
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  } else {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

// transactionMercadopagoEvents.createChargeCapture = async (context) => {
//   const keyConfig = {};
//   let finalPaymentData = {};
//   let paymentConfig = {};
//   if (context.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
//     keyConfig["secretKey"] = context.paymentData["testSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["testPublicKey"];
//   } else {
//     keyConfig["secretKey"] = context.paymentData["liveSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["livePublicKey"];
//   }
//   //-------------- Create charges Start -----------------------
//   paymentConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   paymentConfig["data"]["amount"] = parseInt(
//     parseFloat(context.params.amount) * defaultCent
//   );
//   paymentConfig["data"]["currency"] = context.generalSettings.data.currencyCode;
//   paymentConfig["data"]["customer"] = context.params.stripeCustomerId;
//   paymentConfig["data"]["description"] = "Wallet Recharge";
//   paymentConfig["data"]["capture"] = false;
//   ////Stripe Call For create Charge
//   const stripe = require("stripe")(paymentConfig.secretKey);
//   finalPaymentData = await stripe.charges.create(paymentConfig.data);
//   //-------------- Create charges End -----------------------
//   return finalPaymentData;
// };

transactionMercadopagoEvents.tripEndCardCharge = async function (context) {
  const keyConfig = {};
  let finalPaymentData = {};
  const responseData = {};

  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  const amount = parseFloat(context.params.amount.toFixed(2));
  try {
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}v1/payments`,
      "headers": {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + keyConfig.secretKey,
        "X-Idempotency-Key":
          context.params.generalSettings.data.siteTitle +
          "_" +
          Date.now().toString(), //context.params.transactionReason,
      },
      "data": {
        "transaction_amount": amount,
        "description": context.params.transactionReason,
        "installments": 1,
        "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
        "payer": {
          // "id": null,
          // "first_name": "Valdir de Jesus Correia Junior ",
          // "entity_type": "individual",
          "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
          "type": "customer", //"valdircorreia2012@gmail.com",
          // "email": testEmail, //context.params.userData.email,
          // "identification": {
          //   "type": "CPF",
          //   "number": "02092429574",
          // },
        },
        "capture": true,
        "token":
          context.params?.embedtoken ||
          context.params?.cardData?.data?.embedtoken,
        "metadata": {
          "order_number": context.params.transactionReason, //"ordrr_CAC5J7A3c34Ne1Wv1D1240",
        },
        "external_reference": context.params.transactionReason,
        "notification_url": notificationUrl,
        //   //-----------------------
        //   "transaction_amount": context.params.amount,
        //   "installments": 1,
        //   "capture": true,
        //   "binary_mode": false,
        //   "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
        //   "token":
        //     context.params?.embedtoken ||
        //     context.params?.cardData?.data?.embedtoken,
        //   "external_reference": context.params.transactionReason,
        //   "notification_url": notificationUrl,
        //   "metadata": {
        //     "order_number": context.params.transactionReason,
        //   },
        //   "payer": {
        //     "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
        //     "type": "customer",
        //   },
        //   "statement_descriptor": "Flixmobi",
        //   "description": context.params.transactionReason,
        //   "additional_info": {
        //     "items": [
        //       {
        //         "id": "1941",
        //         "title": "flixmobi",
        //         "description": "flixmobi Ride Payment",
        //         "picture_url": "https://flixmobi.com/storage/logo/logo-topo1.png",
        //         "category_id": "services",
        //         "quantity": 1,
        //         "unit_price": context.params.amount,
        //       },
        //     ],
        //     "payer": {
        //       "first_name": "Valdir",
        //       "last_name": "De Jesus Correia Junior",
        //       "is_prime_user": "-1",
        //       "is_first_purchase_online": "-1",
        //       "last_purchase": new Date().toISOString(),
        //       "phone": {
        //         "area_code": "75",
        //         "number": "999813125",
        //       },
        //       "address": {
        //         "zip_code": "38408256",
        //         "street_name": "Rua Armando Tucci",
        //         "street_number": "452",
        //       },
        //       "registration_date": new Date().toISOString(),
        //     },
        //   },
        //   //-----------------------
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Create charges End -----------------------
  //-------------- Response Data Start -----------------------
  if (
    finalPaymentData?.code === 201 &&
    finalPaymentData?.status?.toLowerCase() === "approved"
  ) {
    // Response
    responseData["authType"] = "NOAUTH";
    // responseData['transactionId'] = finalPaymentData.id.toString()
    responseData["transactionId"] =
      finalPaymentData?.charges?.data[0].id.toString();
    responseData["successResponse"] = finalPaymentData;
    return {
      "code": 200,
      "data": responseData,
      "message": "Card Payment Successfully",
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Card Payment Failed",
    };
  }
  //-------------- Response Data End -----------------------
};

transactionMercadopagoEvents.verifyBankAccount = async function (context) {
  const responseData = {};
  let finalBankAccountData = {},
    httpsAgent = null;
  const certificatesPath = path.resolve(
    __dirname,
    "../mTLSCertificates/delbank_mTLS_certificado_flixmobi.pem"
  );
  const keyPath = path.resolve(__dirname, "../mTLSCertificates/flixmobi.key");
  const countryCode = context.params.generalSettings.data.defaultDialCode;
  let pixkeyNumber = context.params.bankDetails?.pixkeyNumber?.toString();
  //
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = "https://apisandbox.delbank.com.br/";
    keyConfig["publishableKey"] = context.params.paymentData["testAccountNo"]; // delbank API Key
    //
    // httpsAgent = new https.Agent({});
    pixkeyNumber = pixkeyNumber.replace(countryCode, "");
  } else {
    keyConfig["baseUrl"] = "https://api.delbank.com.br/";
    keyConfig["publishableKey"] = context.params.paymentData["liveAccountNo"]; // delbank API Key
    //
    if (fs.existsSync(certificatesPath) && fs.existsSync(keyPath)) {
      httpsAgent = new https.Agent({
        key: fs.readFileSync(keyPath, "utf8"),
        cert: fs.readFileSync(certificatesPath, "utf8"),
      });
    } else {
      return { "code": 400, "data": {}, "message": "Invalid Access" };
    }
    // if (!pixkeyNumber.startsWith(countryCode)) {
    //   pixkeyNumber = countryCode + pixkeyNumber;
    // }
  }
  //------------- Transfer payment Start -----------------------------
  // //---------------------------------
  // let data = JSON.stringify({
  //   "eventType": "PIX_PAYMENT_UPDATED",
  //   // "url": "https://webhook.site/46dd4c4c-4bf9-49fe-b27b-a0d5987c19ec",
  //   "url": "https://api.flixmobi.net/api/transaction/mercado/webhook",
  //   "authorizationScheme": "NONE",
  //   "authorization": "NONE",
  //   "status": "PIX_EFFECTIVE",
  // });
  // let config = {
  //   "method": "get",
  //   "port": 443,
  //   "url": "https://api.delbank.com.br/baas/api/v1/charges/webhooks",
  //   "httpsAgent": httpsAgent,
  //   "headers": {
  //     "IdempotencyKey": "fliximobi_" + Date.now().toString(),
  //     "x-delbank-api-key": keyConfig.publishableKey,
  //   },
  //   "data": data,
  // };
  // const res = await axiosConfig(config);
  // //------------------------------
  try {
    const paymentInitializationParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}baas/api/v2/pix/dict/payment-initialization`,
      "httpsAgent": httpsAgent,
      "headers": {
        "IdempotencyKey": "fliximobi_" + Date.now().toString(),
        "accept": "application/json",
        "Content-Type": "application/json",
        "x-delbank-api-key": keyConfig.publishableKey,
      },
      "data": {
        "key": pixkeyNumber,
      },
    };
    finalBankAccountData = await axiosConfig(paymentInitializationParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  if (finalBankAccountData.code === 200) {
    if (
      finalBankAccountData?.data?.beneficiary?.number ===
      context.params.bankDetails.accountNumber
    ) {
      // Response
      responseData["status"] = "succeeded";
      responseData["authType"] = "NOAUTH";
      responseData["accountholderName"] =
        finalBankAccountData?.data?.beneficiary?.holder?.name;
      responseData["bankName"] =
        finalBankAccountData?.data?.beneficiary?.participant?.name;
      responseData["ispb"] =
        finalBankAccountData?.data?.beneficiary?.participant?.ispb;
      responseData["bankAccountDocumentNo"] =
        finalBankAccountData?.data?.beneficiary?.holder?.document;
      return {
        "code": 200,
        "data": responseData,
        "message": "Valid Bank Details",
      };
    } else {
      return { "code": 400, "data": {}, "message": null }; //*** Must Pass null Value in message Key
    }
  } else {
    return {
      "code": 400,
      "data": {},
      "message": finalBankAccountData.message || "Invalid Bank Details",
    };
  }
};

transactionMercadopagoEvents.createPaymentIntents = async function (context) {
  const keyConfig = {};
  let finalPaymentData = {};
  const responseData = {};
  const { INFO_TRANSACTIONS_SUCCESS, ERROR_IN_TRANSACTIONS } =
    await notifyMessage.setNotifyLanguage(context.params.langCode);

  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  try {
    // const deviceId =
    // "f_DMgABJRkmj7odPglPZQA:APA91bGzaTED6bS3gqAfqkEWTj8F4bgqKYnTnQYD0Z9cWX2ofzcdNDq8mF-d_z9496odQDZ57u9vgcjXcBnjRPKTRHeI86D8ayw7tR4RhE7iem19CtzoXJPdwv0LsSoDKMMF1EXKqS2Y";
    const notificationUrl =
      context.params.generalSettings.data.redirectUrls?.paymentRedirectApiUrl +
      "/api/transaction/mercadopago/webhookingMercadopagoPixKeyPayment";
    const estimationAmount = parseFloat(
      context.params.estimationAmount.toFixed(2)
    );
    //--------
    const accessTokenConfigParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}v1/payments`,
      "headers": {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + keyConfig.secretKey,
        "X-Idempotency-Key":
          context.params.generalSettings.data.siteTitle +
          "_" +
          Date.now().toString(), //context.params.transactionReason,
      },
      "data": {
        "transaction_amount": estimationAmount,
        "description": context.params.transactionReason,
        "installments": 1,
        "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
        "payer": {
          // "id": null,
          // "first_name": "Valdir de Jesus Correia Junior ",
          // "entity_type": "individual",
          "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
          "type": "customer", //"valdircorreia2012@gmail.com",
          // "email": testEmail, //context.params.userData.email,
          // "identification": {
          //   "type": "CPF",
          //   "number": "02092429574",
          // },
        },
        "capture": false,
        "token":
          context.params?.embedtoken ||
          context.params?.cardData?.data?.embedtoken,
        "metadata": {
          "order_number": context.params.transactionReason, //"ordrr_CAC5J7A3c34Ne1Wv1D1240",
        },
        "external_reference": context.params.transactionReason,
        "notification_url": notificationUrl,
        //   //-----------------------
        //   "transaction_amount": context.params.amount,
        //   "installments": 1,
        //   "capture": true,
        //   "binary_mode": false,
        //   "payment_method_id": context.params?.cardData?.data?.type, // "visa/master/pix",
        //   "token":
        //     context.params?.embedtoken ||
        //     context.params?.cardData?.data?.embedtoken,
        //   "external_reference": context.params.transactionReason,
        //   "notification_url": notificationUrl,
        //   "metadata": {
        //     "order_number": context.params.transactionReason,
        //   },
        //   "payer": {
        //     "id": context.params?.userData?.paymentGateWayCustomerId?.toString(),
        //     "type": "customer",
        //   },
        //   "statement_descriptor": "Flixmobi",
        //   "description": context.params.transactionReason,
        //   "additional_info": {
        //     "items": [
        //       {
        //         "id": "1941",
        //         "title": "flixmobi",
        //         "description": "flixmobi Ride Payment",
        //         "picture_url": "https://flixmobi.com/storage/logo/logo-topo1.png",
        //         "category_id": "services",
        //         "quantity": 1,
        //         "unit_price": context.params.amount,
        //       },
        //     ],
        //     "payer": {
        //       "first_name": "Valdir",
        //       "last_name": "De Jesus Correia Junior",
        //       "is_prime_user": "-1",
        //       "is_first_purchase_online": "-1",
        //       "last_purchase": new Date().toISOString(),
        //       "phone": {
        //         "area_code": "75",
        //         "number": "999813125",
        //       },
        //       "address": {
        //         "zip_code": "38408256",
        //         "street_name": "Rua Armando Tucci",
        //         "street_number": "452",
        //       },
        //       "registration_date": new Date().toISOString(),
        //     },
        //   },
        //   //-----------------------
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Response Data Start -----------------------
  // finalPaymentData?.code === 200 &&
  //   finalPaymentData?.status?.toLowerCase() === "approved";
  // if (finalPaymentData) {
  if (
    finalPaymentData?.code === 201 &&
    finalPaymentData?.status?.toLowerCase() === "authorized"
  ) {
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["transactionId"] = finalPaymentData.data.id;
    responseData["transactionAmount"] = context.params.estimationAmount;
    responseData["paymentIntentId"] = finalPaymentData.data.id;
    responseData["paymentIntentClientSecret"] =
      finalPaymentData.data.client_secret;
    responseData["successResponse"] = finalPaymentData.data;
    responseData["status"] = finalPaymentData.status;
    return {
      "code": 200,
      "data": responseData,
      "message": INFO_TRANSACTIONS_SUCCESS,
    };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": ERROR_IN_TRANSACTIONS,
    };
  }
  //-------------- Response Data End -----------------------
};

transactionMercadopagoEvents.capturePaymentIntentsTransaction = async function (
  context
) {
  const responseData = {};
  let dataChargeCapture = {};
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- charges payment Intents Start -----------------------
  try {
    // const notificationUrl =
    //   context.params.generalSettings.data.redirectUrls?.paymentRedirectApiUrl +
    //   "/api/transaction/mercadopago/webhookingMercadopagoPixKeyPayment";
    //--------
    const accessTokenConfigParams = {
      "method": "PUT",
      "port": 443,
      "url": `${keyConfig.baseUrl}v1/payments/${context.params.paymentInitId}`,
      "headers": {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + keyConfig.secretKey,
      },
      "data": {
        "transaction_amount": parseFloat(
          context.params.captureAmount.toFixed(2)
        ),
        "capture": true,
      },
    };
    dataChargeCapture = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- charges payment Intents End -----------------------
  // Response
  if (
    (dataChargeCapture?.code === 200 || dataChargeCapture?.code === 201) &&
    dataChargeCapture?.status?.toLowerCase() === "approved"
  ) {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["transactionId"] =
      dataChargeCapture?.data?.charges?.data[0].id.toString();
    responseData["successResponse"] = dataChargeCapture.data;
    return {
      "code": 200,
      "data": responseData,
      "message": "Amount Captured",
    };
  } else {
    return { "code": 400, "data": {}, "message": "Transaction Failed" };
  }
};

transactionMercadopagoEvents.cancelCapturePaymentIntents = async function (
  context
) {
  const responseData = {};
  let dataChargeCapture = {};
  let captureConfig = {};
  const keyConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = context.params.paymentData["testPaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["baseUrl"] = context.params.paymentData["livePaymentBaseURL"];
    keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
    keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- charges payment Intents Start -----------------------
  try {
    // const notificationUrl =
    //   context.params.generalSettings.data.redirectUrls?.paymentRedirectApiUrl +
    //   "/api/transaction/mercadopago/webhookingMercadopagoPixKeyPayment";
    //--------
    const accessTokenConfigParams = {
      "method": "PUT",
      "port": 443,
      "url": `${keyConfig.baseUrl}v1/payments/${context.params.paymentInitId}`,
      "headers": {
        "accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + keyConfig.secretKey,
      },
      "data": {
        "status": "cancelled",
      },
    };
    dataChargeCapture = await axiosConfig(accessTokenConfigParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  //-------------- Cancel Capture payment Intents End -----------------------
  // Response
  if (
    dataChargeCapture?.code === 201 &&
    dataChargeCapture?.status?.toLowerCase() === "cancelled"
  ) {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["transactionId"] =
      dataChargeCapture?.charges?.data[0].id.toString();
    responseData["successResponse"] = dataChargeCapture;
    return { "code": 200, "data": responseData, "message": "Amount Captured" };
  } else {
    return { "code": 400, "data": {}, "message": "Transaction Failed" };
  }
};

transactionMercadopagoEvents.newWithdrawalWalletTransaction = async function (
  context
) {
  const responseData = {},
    keyConfig = {};
  let finalPaymentData = {},
    dataPaymentInitialization = null,
    httpsAgent = null;
  const certificatesPath = path.resolve(
    __dirname,
    "../mTLSCertificates/delbank_mTLS_certificado_flixmobi.pem"
  );
  const keyPath = path.resolve(__dirname, "../mTLSCertificates/flixmobi.key");
  const countryCode = context.params.generalSettings.data.defaultDialCode;
  let pixkeyNumber = context.params.bankDetails?.pixkeyNumber?.toString();
  //
  if (context.params.paymentData.mode.toUpperCase() === constantUtils.SANDBOX) {
    keyConfig["baseUrl"] = "https://apisandbox.delbank.com.br/";
    keyConfig["publishableKey"] = context.params.paymentData["testAccountNo"]; // delbank API Key
    //
    pixkeyNumber = pixkeyNumber.replace(countryCode, "");
  } else {
    keyConfig["baseUrl"] = "https://api.delbank.com.br/";
    keyConfig["publishableKey"] = context.params.paymentData["liveAccountNo"]; // delbank API Key
    //
    if (fs.existsSync(certificatesPath) && fs.existsSync(keyPath)) {
      httpsAgent = new https.Agent({
        cert: fs.readFileSync(certificatesPath),
        key: fs.readFileSync(keyPath),
      });
    } else {
      return { "code": 400, "data": {}, "message": "Invalid Access" };
    }
    // if (!pixkeyNumber.startsWith(countryCode)) {
    //   pixkeyNumber = countryCode + pixkeyNumber;
    // }
  }
  //------------- Transfer payment Start -----------------------------
  try {
    const paymentInitializationParams = {
      "method": "POST",
      "port": 443,
      "url": `${keyConfig.baseUrl}baas/api/v2/pix/dict/payment-initialization`,
      "httpsAgent": httpsAgent,
      "headers": {
        "IdempotencyKey": "fliximobi_" + Date.now().toString(),
        "accept": "application/json",
        "Content-Type": "application/json",
        "x-delbank-api-key": keyConfig.publishableKey,
      },
      "data": {
        "key": pixkeyNumber,
      },
    };
    dataPaymentInitialization = await axiosConfig(paymentInitializationParams);
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
  if (dataPaymentInitialization.code === 200) {
    try {
      const paymentConfig = {
        "method": "POST",
        "port": 443,
        "url": `${keyConfig.baseUrl}baas/api/v2/transfers`,
        "httpsAgent": httpsAgent,
        "headers": {
          "IdempotencyKey":
            context.params.generalSettings.data.siteTitle +
            +"_" +
            Date.now().toString(),
          "accept": "application/json",
          "Content-Type": "application/json",
          "x-delbank-api-key": keyConfig.publishableKey,
        },
        "data": {
          "amount": parseFloat(context.params.amount),
          "description": context.params.transactionReason,
          "endToEndId": dataPaymentInitialization.data.endToEndId,
          "initiationType": "KEY",
        },
      };
      finalPaymentData = await axiosConfig(paymentConfig);
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  }
  if (finalPaymentData.code === 200) {
    //------------- Transfer payment End -----------------------------
    // Response
    responseData["authType"] = "NOAUTH";
    // responseData["paymentGateWayCustomerId"] = stripeDestinationId;
    responseData["transactionId"] = finalPaymentData.data.id;
    responseData["paymentIntentId"] = finalPaymentData.data.id;
    responseData["paymentIntentAmount"] = context.params.amount;
    responseData["transactionAmount"] = context.params.amount;
    responseData["paymentIntentClientSecret"] =
      finalPaymentData.data.endToEndId;
    responseData["successResponse"] = finalPaymentData.data;
    responseData["status"] = constantUtils.SUCCESS; //finalPaymentData.status;
    return {
      "code": 200,
      "data": responseData,
      "message": "Valid Account Details",
    };
  } else {
    return {
      "code": 400,
      "data": {},
      // "message": "Bank server Busy Now. Please try Later.",
      "message": finalPaymentData.message,
    };
  }
};

//--------------------------------------
module.exports = transactionMercadopagoEvents;
