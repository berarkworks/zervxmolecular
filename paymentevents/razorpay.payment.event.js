//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const Razorpay = require("razorpay");

const defaultPaisa = 100;
//----------------- Need to Remove End --------------------

const transactionRazorpayEvents = {};

transactionRazorpayEvents.checkValidCard = async function (context) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let finalCustomerData = {};
  // let finalPaymentData = {};
  // let tokenizationConfig = {};
  let customerConfig = {};
  // let paymentConfig = {};
  const keyConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }

  //------------------ Create Token End-------------------------------
  // if (finalTokenData) {
  // cardBrand = finalTokenData?.card?.brand.toLowerCase();
  // responseData["cardToken"] = finalTokenData.id;
  let paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;

  customerConfig = {
    ...keyConfig,
    "data": {},
  };

  if (!paymentGateWayCustomerId && paymentGateWayCustomerId == null) {
    //------------------ Create Customer Start ---------------------------------
    try {
      const razor = new Razorpay({
        "key_id": customerConfig.key_id,
        "key_secret": customerConfig.key_secret,
      });

      finalCustomerData = await razor.customers.create({
        "name": context.params.holderName || "New",
        "contact":
          context.params.userData.phone.code +
          " " +
          context.params.userData.phone.number,
        "email": context.params.userData.email || "",
        "fail_existing": 0,
        // "gstin": "",
        "notes": {
          // "notes_key_1": "Tea, Earl Grey, Hot",
          // "notes_key_2": "Tea, Earl Grey… decaf.",
        },
      });

      paymentGateWayCustomerId = finalCustomerData.id;
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  } else {
    // create customer
    try {
      const razorpay = new Razorpay({
        "key_id": customerConfig.key_id,
        "key_secret": customerConfig.key_secret,
      });
      finalCustomerData = await razorpay.customers.edit(
        paymentGateWayCustomerId,
        {
          "name": context.params.holderName,
          // "contact":
          //   context.params.userData.phone.code +
          //   context.params.userData.phone.number,
          // "email": context.params.userData.email,
          // "name": "Suman",
          // "contact": "+919487551995",
          // "email": "rsuman132@gmail.com",
        }
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  }
  //------------------ Create Customer End ---------------------------------
  // if (finalCustomerData) {
  //   //---------------- create Charge End--------------------------
  //   const minimumChargeAmount =
  //     parseFloat(context.params.minimumChargeAmount ?? 1) * defaultPaisa; // Defaule Card Verification deduction
  //   paymentConfig = {
  //     ...keyConfig,
  //     "data": {},
  //   };

  // try {
  //   const razorPay = new Razorpay({
  //     "key_id": paymentConfig.key_id,
  //     "key_secret": paymentConfig.key_secret,
  //   });

  //   // const orderData = await razorPay.orders.create({
  //   //   "amount": minimumChargeAmount,
  //   //   "currency": context.params.generalSettings.data.currencyCode,
  //   //   //-------
  //   //   // "amount": minimumChargeAmount,
  //   //   // "currency": context.params.generalSettings.data.currencyCode,
  //   //   // "receipt": "rcptid_11",
  //   //   // // "capture ": true,
  //   //   // "payment": {
  //   //   //   "capture ": "manual",
  //   //   //   "capture_options ": {
  //   //   //     "automatic_expiry_period ": 12,
  //   //   //     "manual_expiry_period ": 7200,
  //   //   //     "refund_speed": "optimum",
  //   //   //   },
  //   //   // },
  //   // });
  //   //----------
  //   // const captureData = await razorPay.payments.capture(
  //   //   "pay_KainVsOp0FvWzW",
  //   //   2000, //minimumChargeAmount
  //   //   context.params.generalSettings.data.currencyCode
  //   // );
  //   // const refundData = await razorPay.payments.refund("pay_KaivBv21ijUVql", {
  //   //   "amount": "200",
  //   //   "speed": "normal",
  //   //   "notes": {
  //   //     "notes_key_1": "Beam me up Scotty.",
  //   //     "notes_key_2": "Engage",
  //   //   },
  //   //   "receipt": "Receipt No. 31",
  //   // });
  //   //----------
  //   // const razorPayNew = new Razorpay({
  //   //   "key_id": paymentConfig.key_id,
  //   //   "key_secret": paymentConfig.key_secret,
  //   // });
  //   // finalPaymentData = await razorPayNew.payments.createPaymentJson(
  //   //   {
  //   //     "amount": minimumChargeAmount,
  //   //     "currency": context.params.generalSettings.data.currencyCode,
  //   //     // "order_id": orderData.id,
  //   //     "email": "gaurav.kumar@example.com",
  //   //     "contact": 9090909090,
  //   //     "method": "card",
  //   //     "card": {
  //   //       "number": 5267318187975449,
  //   //       "name": "Gaurav",
  //   //       "expiry_month": 11,
  //   //       "expiry_year": 23,
  //   //       "cvv": 100,
  //   //     },
  //   //     "notes": { "customerId": finalCustomerData.id },
  //   //   },
  //   //   ""
  //   // );
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  // Response
  if (finalCustomerData) {
    responseData["authType"] = "NOAUTH";
    responseData["isDefault"] = true;
    responseData["isCVVRequired"] = false;
    responseData["paymentGateWayCustomerId"] = finalCustomerData.id;
    responseData["paymentIntentId"] = null;
    // responseData["paymentIntentClientSecret"] =
    //   finalPaymentData.client_secret;
    responseData["transactionAmount"] = parseInt(
      context.params.minimumChargeAmount
    );
    responseData["transactionId"] = null;
    responseData["successResponse"] = null;
    responseData["status"] = constantUtils.SUCCESS;
    return { "code": 200, "data": responseData, "message": "Valid Card" };
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
  // } else {
  //   return { "code": 400, "data": {}, "message": "invalid Card" };
  // }
};

transactionRazorpayEvents.newRechargeWalletTransaction = async function (
  context
) {
  // let finalTokenData = {};
  // let cardBrand = {};
  let orderPaymentData = {};
  let paymentConfig = {};
  // let customerConfig = {};
  // let tokenizationConfig = {};
  const responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  //------------------ Create Token Start-------------------------------
  // tokenizationConfig = {
  //   ...keyConfig,
  //   "data": {},
  // };
  // tokenizationConfig["data"]["card"] = {};
  // tokenizationConfig["data"]["card"]["number"] =
  //   context.params.cardData.data.cardNumber;
  // tokenizationConfig["data"]["card"]["exp_month"] =
  //   context.params.cardData.data.expiryMonth;
  // tokenizationConfig["data"]["card"]["exp_year"] =
  //   context.params.cardData.data.expiryYear;
  // tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
  // // Stripe Call For card tokenization
  // try {
  //   const stripe = require("stripe")(tokenizationConfig.secretKey);
  //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
  // } catch (error) {
  //   return { "code": 400, "data": {}, "message": error.message };
  // }
  //------------------ Create Token End-------------------------------
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;
  if (paymentGateWayCustomerId) {
    // cardBrand = finalTokenData?.card?.brand.toLowerCase();
    //------------------ Update Customer Token Start ---------------------------------
    // customerConfig = {
    //   ...keyConfig,
    //   "data": {},
    // };
    // customerConfig["data"]["source"] = finalTokenData.id;
    // // Stripe Call For create customer
    // let finalCustomerData = {};
    // try {
    //   const stripe = require("stripe")(customerConfig.secretKey);
    //   finalCustomerData = await stripe.customers.update(
    //     context.params.userData.stripeCustomerId, //stripeCustomerId
    //     //   finalTokenData.id, //stripe Token
    //     customerConfig.data
    //   );
    // } catch (error) {
    //   return { "code": 400, "data": {}, "message": error.message };
    // }
    //------------------ Update Customer Token End ---------------------------------
    // //---------------- create Charge End--------------------------
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    // paymentConfig['data']['amount'] =
    //   parseInt(parseFloat(context.params.amount)) * defaultPaisa
    // paymentConfig['data']['currency'] =
    //   context.params.generalSettings.data.currencyCode
    // paymentConfig['data']['customer'] = context.params.userData.stripeCustomerId
    // paymentConfig['data']['description'] = context.params.transactionReason
    // paymentConfig['data']['capture'] = true
    // //Stripe Call For create Charge
    // try {
    //   const stripe = require('stripe')(paymentConfig.secretKey)
    //   finalPaymentData = await stripe.charges.create(paymentConfig.data)
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    // //================= paymentIntents ======================
    // paymentConfig["data"]["amount"] = parseInt(
    //   parseFloat(context.params.amount) * defaultPaisa
    // ); // Defaule Card Verification deduction
    // paymentConfig["data"]["currency"] =
    //   context.params.generalSettings.data.currencyCode;
    // // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
    // paymentConfig["data"]["payment_method_types"] = ["card"];
    // paymentConfig["data"]["capture_method"] = "automatic"; //automatic
    // paymentConfig["data"]["confirm"] = true;
    // // paymentConfig['data']['confirmation_method'] = 'manual'
    // // paymentConfig['data']['requires_capture'] = false
    // // paymentConfig['data']['requires_action'] = false

    // // paymentConfig['data']['automatic_payment_methods'] = true
    // paymentConfig["data"]["customer"] =
    //   context.params.userData.stripeCustomerId;
    // paymentConfig["data"]["description"] = context.params.transactionReason;
    //Stripe Call For create Charge
    try {
      // const stripe = require("stripe")(paymentConfig.secretKey);
      // finalPaymentData = await stripe.paymentIntents.create(paymentConfig.data);
      const razorPay = new Razorpay({
        "key_id": paymentConfig.key_id,
        "key_secret": paymentConfig.key_secret,
      });

      orderPaymentData = await razorPay.orders.create({
        "amount": parseInt(parseFloat(context.params.amount) * defaultPaisa),
        "currency": context.params.generalSettings.data.currencyCode,
        "receipt": context.params.transactionReason,
        //-------
        // "amount": minimumChargeAmount,
        // "currency": context.params.generalSettings.data.currencyCode,
        // "receipt": "rcptid_11",
        // // "capture ": true,
        // "payment": {
        //   "capture ": "manual",
        //   "capture_options ": {
        //     "automatic_expiry_period ": 12,
        //     "manual_expiry_period ": 7200,
        //     "refund_speed": "optimum",
        //   },
        // },
      });
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    // let data = {}
    // data['paymentData'] = context.params.paymentData
    // data['transactionId'] = finalPaymentData.id
    // paymentIntentsCapture(data)
    //================= paymentIntents =====================
    // =================================
    // let dataChargeCapture = {}
    // let captureConfig = {};
    // //
    // //-------------- charges payment Intents Start -----------------------
    // captureConfig = {
    //   ...keyConfig,
    //   "data": {},
    // };
    // //------- confirm -------------------
    // try {
    //   const stripe = require('stripe')(captureConfig.secretKey)
    //   const confirmPaymentIntent = await stripe.paymentIntents.confirm(
    //     finalPaymentData.id
    //   )
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    //----------- confirm ----------
    // captureConfig["data"]["charge"] = finalPaymentData.id;
    // For capture payment
    // try {
    //   const stripe = require('stripe')(captureConfig.secretKey)
    //   dataChargeCapture = await stripe.paymentIntents.capture(
    //     captureConfig.data.charge
    //   )
    //   // dataChargeCapture = await stripe.paymentIntents.capture(
    //   //   captureConfig.data
    //   // )
    // } catch (error) {
    //   return { 'code': 400, 'data': {}, 'message': error.message }
    // }
    //-------------- charges payment Intents End -----------------------
    //=================================
    //---------------- create Charge End--------------------------
    // Response
    if (orderPaymentData.status === "created") {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] = orderPaymentData.id.toString();
      responseData["paymentIntentId"] = orderPaymentData.id.toString();
      responseData["paymentIntentAmount"] = orderPaymentData.amount;
      responseData["successResponse"] = orderPaymentData;
      responseData["status"] = orderPaymentData.status;
      return { "code": 200, "data": responseData, "message": "Valid Card" };
    } else {
      return { "code": 400, "data": {}, "message": "invalid Card" };
    }
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
};

// transactionRazorpayEvents.createChargeCapture = async (context) => {
//   const keyConfig = {};
//   let finalPaymentData = {};
//   let paymentConfig = {};
//   if (context.paymentData.mode.toUpperCase() === "SANDBOX") {
//     keyConfig["secretKey"] = context.paymentData["testSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["testPublicKey"];
//   } else {
//     keyConfig["secretKey"] = context.paymentData["liveSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["livePublicKey"];
//   }
//   //-------------- Create charges Start -----------------------
//   paymentConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   paymentConfig["data"]["amount"] = parseInt(
//     parseFloat(context.params.amount) * defaultPaisa
//   );
//   paymentConfig["data"]["currency"] = context.generalSettings.data.currencyCode;
//   paymentConfig["data"]["customer"] = context.params.stripeCustomerId;
//   paymentConfig["data"]["description"] = "Wallet Recharge";
//   paymentConfig["data"]["capture"] = false;
//   ////Stripe Call For create Charge
//   const stripe = require("stripe")(paymentConfig.secretKey);
//   finalPaymentData = await stripe.charges.create(paymentConfig.data);
//   //-------------- Create charges End -----------------------
//   return finalPaymentData;
// };

// transactionRazorpayEvents.chargeCapture = async (context) => {
//   const keyConfig = {};
//   let dataChargeCapture = {};
//   let captureConfig = {};
//   if (context.paymentData.mode.toUpperCase() === "SANDBOX") {
//     keyConfig["secretKey"] = context.paymentData["testSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["testPublicKey"];
//   } else {
//     keyConfig["secretKey"] = context.paymentData["liveSecretKey"];
//     keyConfig["publishableKey"] = context.paymentData["livePublicKey"];
//   }
//   //-------------- charges charges Start -----------------------
//   captureConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   captureConfig["charge"] = context.params.transactionId;
//   // For capture payment
//   const stripe = require("stripe")(captureConfig.secretKey);
//   dataChargeCapture = await stripe.charges.capture(captureConfig.charge);
//   //-------------- charges charges End -----------------------
//   return dataChargeCapture;
// };

transactionRazorpayEvents.refundsAmount = async function (context) {
  const keyConfig = {};
  const responseData = {};
  let refundData = {};
  let refundsConfig = {};
  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  //-------------- refund payment Start -----------------------
  refundsConfig = {
    ...keyConfig,
    "data": {},
  };
  // refundsConfig["data"]["charge"] = context.params.transactionId.toString();
  // refundsConfig["data"]["payment_intent"] =
  //   context.params.transactionId.toString();
  // capturedata['data']['reverse_transfer'] = true
  // capturedata['data']['refund_application_fee'] = true
  //For refund payment
  try {
    // const striperefunds = require("stripe")(refundsConfig.secretKey);
    // dataRefunds = await striperefunds.refunds.create(refundsConfig.data);
    const razorPay = new Razorpay({
      "key_id": refundsConfig.key_id,
      "key_secret": refundsConfig.key_secret,
    });
    refundData = await razorPay.payments.refund(
      context.params.transactionId.toString(),
      {
        // "amount": "100",//Optional
        "speed": "normal",
        "notes": {
          "description": constantUtils.CARDAMOUNTREFUND,
        },
        "receipt": context.params.transactionId.toString(),
      }
    );
  } catch (error) {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
  //-------------- refund payment End -----------------------
  if (refundData.status === "processed") {
    // Response
    responseData["authType"] = "NOAUTH";
    responseData["transactionId"] = context.params.transactionId.toString();
    // dataRefunds['successResponse'] = finalPaymentData
    return { "code": 200, "data": responseData, "message": "Valid Card" };
  } else {
    return { "code": 400, "data": {}, "message": "invalid Card" };
  }
};

// transactionRazorpayEvents.tripEndCardCharge = async function (context) {
//   const keyConfig = {};
//   // let cardBrand = "visa";
//   let orderPaymentData = {};
//   // let finalTokenData = {};
//   // let tokenizationConfig = {};
//   // let customerConfig = {};
//   const paymentConfig = {};
//   const responseData = {};

//   if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//     keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
//     keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
//   } else {
//     keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
//     keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
//   }
//   //------------------ Create Token Start-------------------------------
//   // tokenizationConfig = {
//   //   ...keyConfig,
//   //   "data": {},
//   // };
//   // tokenizationConfig["data"]["card"] = {};
//   // tokenizationConfig["data"]["card"]["number"] =
//   //   context.params.cardData.data.cardNumber;
//   // tokenizationConfig["data"]["card"]["exp_month"] =
//   //   context.params.cardData.data.expiryMonth;
//   // tokenizationConfig["data"]["card"]["exp_year"] =
//   //   context.params.cardData.data.expiryYear;
//   // tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
//   // // Stripe Call For card tokenization
//   // try {
//   //   const stripe = require("stripe")(tokenizationConfig.secretKey);
//   //   finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
//   // } catch (error) {
//   //   return { "code": 400, "data": {}, "message": error.message };
//   // }
//   //------------------ Create Token End-------------------------------
//   const paymentGateWayCustomerId =
//     context.params?.userData?.paymentGateWayCustomerId || null;

//   if (paymentGateWayCustomerId) {
//     try {
//       const razorPay = new Razorpay({
//         "key_id": paymentConfig.key_id,
//         "key_secret": paymentConfig.key_secret,
//       });

//       orderPaymentData = await razorPay.orders.create({
//         "amount": parseInt(parseFloat(context.params.amount) * defaultPaisa),
//         "currency": context.params.generalSettings.data.currencyCode,
//         "receipt": context.params.transactionReason,
//       });
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//     //-------------- Response Data Start -----------------------
//     if (orderPaymentData.status === "created") {
//       responseData["authType"] = "NOAUTH";
//       responseData["isDefault"] = true;
//       // responseData['stripeCardToken'] = finalTokenData.id
//       responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
//       responseData["transactionId"] = orderPaymentData.id.toString();
//       responseData["paymentIntentId"] = orderPaymentData.id.toString();
//       responseData["paymentIntentAmount"] = orderPaymentData.amount;
//       responseData["successResponse"] = orderPaymentData;
//       responseData["status"] = orderPaymentData.status;
//       return { "code": 200, "data": responseData, "message": "Valid Card" };
//     } else {
//       return { "code": 400, "data": {}, "message": "invalid Card" };
//     }
//     //-------------- Response Data End -----------------------
//   } else {
//     return {
//       "code": 400,
//       "data": {},
//       "message": "Card Payment Failed",
//     };
//   }
// };

// webhook

transactionRazorpayEvents.webhooking = async function (context) {
  let transactionStatus = {};
  let updateTransaction = {};
  let notificationObject = {};
  const eventType = context.params.type;
  const transactionId = context.params.data?.object?.id;

  switch (eventType) {
    case "account.updated":
    case "account.external_account.created":
    case "account.external_account.deleted":
    case "account.external_account.updated":
    case "balance.available":
    case "billing_portal.configuration.created":
    case "billing_portal.configuration.updated":
    case "capability.updated":
    case "charge.captured":
    case "charge.refunded":
    case "charge.succeeded":
    case "charge.updated":
    case "charge.dispute.closed":
    case "charge.dispute.created":
    case "charge.dispute.funds_reinstated":
    case "charge.dispute.funds_withdrawn":
    case "charge.dispute.updated":
    case "charge.refund.updated":
    case "checkout.session.async_payment_succeeded":
    case "checkout.session.completed":
    case "coupon.created":
    case "coupon.deleted":
    case "coupon.updated":
    case "credit_note.created":
    case "credit_note.updated":
    case "credit_note.voided":
    case "customer.created":
    case "customer.deleted":
    case "customer.updated":
    case "customer.discount.created":
    case "customer.discount.deleted":
    case "customer.discount.updated":
    case "customer.source.created":
    case "customer.source.deleted":
    case "customer.source.updated":
    case "customer.subscription.created":
    case "customer.subscription.deleted":
    case "customer.subscription.pending_update_applied":
    case "customer.subscription.trial_will_end":
    case "customer.subscription.updated":
    case "customer.tax_id.created":
    case "customer.tax_id.deleted":
    case "customer.tax_id.updated":
    case "file.created":
    case "identity.verification_session.canceled":
    case "identity.verification_session.created":
    case "identity.verification_session.processing":
    case "identity.verification_session.requires_input":
    case "identity.verification_session.verified":
    case "invoice.created":
    case "invoice.deleted":
    case "invoice.finalized":
    case "invoice.marked_uncollectible":
    case "invoice.paid":
    case "invoice.payment_action_required":
    case "invoice.payment_succeeded":
    case "invoice.sent":
    case "invoice.upcoming":
    case "invoice.updated":
    case "invoice.voided":
    case "invoiceitem.created":
    case "invoiceitem.deleted":
    case "invoiceitem.updated":
    case "issuing_authorization.created":
    case "issuing_authorization.updated":
    case "issuing_card.created":
    case "issuing_card.updated":
    case "issuing_cardholder.created":
    case "issuing_cardholder.updated":
    case "issuing_dispute.closed":
    case "issuing_dispute.created":
    case "issuing_dispute.updated":
    case "issuing_transaction.created":
    case "issuing_transaction.updated":
    case "mandate.updated":
    case "order.created":
    case "order.payment_succeeded":
    case "order.updated":
    case "order_return.created":
    case "payment_intent.amount_capturable_updated":
    case "payment_intent.created":
    case "payment_intent.requires_action":
    case "payment_intent.succeeded":
    case "payment_link.created":
    case "payment_link.updated":
    case "payment_method.attached":
    case "payment_method.automatically_updated":
    case "payment_method.detached":
    case "payment_method.updated":
    case "payout.canceled":
    case "payout.created":
    case "payout.paid":
    case "payout.updated":
    case "person.created":
    case "person.deleted":
    case "person.updated":
    case "plan.created":
    case "plan.deleted":
    case "plan.updated":
    case "price.created":
    case "price.deleted":
    case "price.updated":
    case "product.created":
    case "product.deleted":
    case "product.updated":
    case "promotion_code.created":
    case "promotion_code.updated":
    case "quote.accepted":
    case "quote.canceled":
    case "quote.created":
    case "quote.finalized":
    case "radar.early_fraud_warning.created":
    case "radar.early_fraud_warning.updated":
    case "recipient.created":
    case "recipient.deleted":
    case "recipient.updated":
    case "reporting.report_run.succeeded":
    case "review.closed":
    case "review.opened":
    case "setup_intent.canceled":
    case "setup_intent.created":
    case "setup_intent.requires_action":
    case "setup_intent.succeeded":
    case "sigma.scheduled_query_run.created":
    case "sku.created":
    case "sku.deleted":
    case "sku.updated":
    case "source.canceled":
    case "source.chargeable":
    case "source.mandate_notification":
    case "source.refund_attributes_required":
    case "source.transaction.created":
    case "source.transaction.updated":
    case "subscription_schedule.aborted":
    case "subscription_schedule.canceled":
    case "subscription_schedule.completed":
    case "subscription_schedule.created":
    case "subscription_schedule.released":
    case "subscription_schedule.updated":
    case "tax_rate.created":
    case "tax_rate.updated":
    case "topup.canceled":
    case "topup.created":
    case "topup.reversed":
    case "topup.succeeded":
    case "transfer.created":
    case "transfer.paid":
    case "transfer.reversed":
    case "transfer.updated":
      transactionStatus = constantUtils.SUCCESS;
      break;
    case "charge.failed":
    case "invoice.finalization_failed":
    case "invoice.payment_failed":
    case "checkout.session.async_payment_failed":
    case "order.payment_failed":
    case "payment_intent.payment_failed":
    case "payout.failed":
    case "reporting.report_run.failed":
    case "setup_intent.setup_failed":
    case "source.failed":
    case "subscription_schedule.expiring":
    case "topup.failed":
    case "transfer.failed":
      transactionStatus = constantUtils.FAILED;
      break;
    case "charge.pending":
    case "issuing_dispute.funds_reinstated":
    case "issuing_dispute.submitted":
    case "payment_intent.canceled":
    case "payment_intent.processing":
      transactionStatus = constantUtils.PENDING;
      break;
    case "charge.expired":
    case "checkout.session.expired":
    case "customer.source.expiring":
    case "customer.subscription.pending_update_expired":
      transactionStatus = constantUtils.FAILED;
      break;
    // ... handle other event types
    default:
      transactionStatus = constantUtils.FAILED;
      return null;
    // break
  }

  const transaction = await this.adapter.model.findOneAndUpdate(
    { "transactionId": transactionId },
    { "webhookresponse": context.params },
    {
      "new": true, //unique reference find in webhook
    }
  );

  if (transactionStatus === constantUtils.SUCCESS) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    return null;
  } else if (transactionStatus === constantUtils.FAILED) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    //-------------------------------------
    // reterive the amount to whose is the user or professional
    if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
      // retrn the amount of professional
      if (transaction.to.userType === constantUtils.PROFESSIONAL) {
        let userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        let walletupdated = await this.broker.emit(
          "professional.updateWalletAmount",
          {
            "professionalId": userData._id.toString(),
            "availableAmount":
              userData.wallet.availableAmount + transaction.transactionAmount,
            "freezedAmount": userData.wallet.freezedAmount,
          }
        );
        walletupdated = walletupdated && walletupdated[0];
        if (walletupdated.code !== 200) {
          throw new MoleculerError(
            "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED",
            500
          );
        }

        // send notification to professional
        let updatedtransaction = await this.adapter.model.findOne({
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        });
        updatedtransaction = updatedtransaction.toJSON();

        // remove not neede data for send notification

        //updated user detailes

        userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        //
        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtils.NOTIFICATIONTYPE,
            "userType": constantUtils.PROFESSIONAL,
            "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
            "timestamp": Date.now(),
            "message":
              updatedtransaction.webhookresponse.data.object.description,
            "details": {
              "walletAmount": userData.wallet.availableAmount,
              "paymentType": updatedtransaction.paymentType,
              "transactionStatus": updatedtransaction.transactionStatus,
              "_id": updatedtransaction._id,
            },
          },
          "registrationTokens": [
            {
              "token": userData.deviceInfo[0].deviceId,
              "id": userData._id.toString(),
              "deviceType": userData.deviceInfo[0].deviceType,
              "platform": userData.deviceInfo[0].platform,
              "socketId": userData.deviceInfo[0].socketId,
            },
          ],
        };
        console.log(notificationObject);
      }
      // USER
      if (transaction.to.userType === constantUtils.USER) {
        let userData = await this.broker.emit("user.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        let walletupdated = await this.broker.emit("user.updateWalletAmount", {
          "userId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount + transaction.transactionAmount,
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        });
        walletupdated = walletupdated && walletupdated[0];
        if (walletupdated.code !== 200) {
          throw new MoleculerError(
            "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED",
            500
          );
        }

        // send notification to professional
        let updatedtransaction = await this.adapter.model.findOne({
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        });
        updatedtransaction = updatedtransaction.toJSON();
        //updated user detailes

        userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
          throw new MoleculerError("SOMETHING WENT WRONG", 500);
        }
        //
        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtils.NOTIFICATIONTYPE,
            "userType": constantUtils.USER,
            "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
            "timestamp": Date.now(),
            "message":
              updatedtransaction.webhookresponse.data.object.description,
            "details": lzStringEncode({
              "walletAmount": userData.wallet.availableAmount,
              "paymentType": updatedtransaction.paymentType,
              "transactionStatus": updatedtransaction.transactionStatus,
              "_id": updatedtransaction._id,
            }),
          },
          "registrationTokens": [
            {
              "token": userData.deviceInfo[0].deviceId,
              "id": userData._id.toString(),
              "deviceType": userData.deviceInfo[0].deviceType,
              "platform": userData.deviceInfo[0].platform,
              "socketId": userData.deviceInfo[0].socketId,
            },
          ],
        };
        console.log(notificationObject);
      }
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
      return null;
    }
  } else if (transactionStatus === constantUtils.PENDING) {
    updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "transactionStatus": transactionStatus,
      }
    );
    return null;
  }
  return null;
};

transactionRazorpayEvents.createOrderPaymentForBooking = async function (
  context
) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let orderPaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  // let customerConfig = {};
  let orderConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;

  if (paymentGateWayCustomerId) {
    orderConfig = {
      ...keyConfig,
      "data": {},
    };
    try {
      const razorPay = new Razorpay({
        "key_id": orderConfig.key_id,
        "key_secret": orderConfig.key_secret,
      });

      orderPaymentData = await razorPay.orders.create({
        "amount": parseInt(
          parseFloat(context.params.estimationAmount) * defaultPaisa
        ),
        "currency": context.params.generalSettings.data.currencyCode,
        "receipt": context.params.transactionReason.split("/")[1],
      });
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //-------------- Response Data Start -----------------------
    if (orderPaymentData.status === "created") {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] = orderPaymentData.id.toString();
      responseData["paymentIntentId"] = orderPaymentData.id.toString();
      responseData["paymentIntentAmount"] = orderPaymentData.amount;
      responseData["successResponse"] = orderPaymentData;
      responseData["status"] = orderPaymentData.status;
      return { "code": 200, "data": responseData, "message": "Valid Card" };
    } else {
      return { "code": 400, "data": {}, "message": "invalid Card" };
    }
    //-------------- Response Data End -----------------------
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Card Payment Failed",
    };
  }
};

transactionRazorpayEvents.capturePaymentIntentsTransaction = async function (
  context
) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let capturePaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  // let customerConfig = {};
  let captureConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;

  if (paymentGateWayCustomerId) {
    captureConfig = {
      ...keyConfig,
      "data": {},
    };
    try {
      const razorPay = new Razorpay({
        "key_id": captureConfig.key_id,
        "key_secret": captureConfig.key_secret,
      });

      capturePaymentData = await razorPay.payments.capture(
        context.params.paymentInitId,
        parseInt(parseFloat(context.params.captureAmount) * defaultPaisa)
        // context.params.generalSettings.data.currencyCode
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    //-------------- Response Data Start -----------------------
    if (capturePaymentData.status === "captured") {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = true;
      // responseData['stripeCardToken'] = finalTokenData.id
      responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
      responseData["transactionId"] = capturePaymentData.id.toString();
      responseData["paymentIntentId"] = capturePaymentData.id.toString();
      responseData["paymentIntentAmount"] = capturePaymentData.amount;
      responseData["successResponse"] = capturePaymentData;
      responseData["status"] = capturePaymentData.status;
      return {
        "code": 200,
        "data": responseData,
        "message": "Payment Captured",
      };
    } else {
      return { "code": 400, "data": {}, "message": "Payment Capture Failed " };
    }
    //-------------- Response Data End -----------------------
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Card Payment Failed",
    };
  }
};

transactionRazorpayEvents.capturePaymentIntentsAndRefund = async function (
  context
) {
  const keyConfig = {};
  // let cardBrand = "visa";
  let capturePaymentData = {};
  let refundPaymentData = {};
  // let finalTokenData = {};
  // let tokenizationConfig = {};
  // let customerConfig = {};
  let paymentConfig = {};
  const responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["key_secret"] = context.params.paymentData["testSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["testPublicKey"];
  } else {
    keyConfig["key_secret"] = context.params.paymentData["liveSecretKey"];
    keyConfig["key_id"] = context.params.paymentData["livePublicKey"];
  }
  const paymentGateWayCustomerId =
    context.params?.userData?.paymentGateWayCustomerId || null;

  if (paymentGateWayCustomerId) {
    paymentConfig = {
      ...keyConfig,
      "data": {},
    };
    try {
      const razorPay = new Razorpay({
        "key_id": paymentConfig.key_id,
        "key_secret": paymentConfig.key_secret,
      });

      capturePaymentData = await razorPay.payments.capture(
        context.params.paymentInitId,
        parseInt(parseFloat(context.params.captureAmount) * defaultPaisa)
        // context.params.generalSettings.data.currencyCode
      );
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
    if (capturePaymentData.status === "captured") {
      try {
        const razorPayRefund = new Razorpay({
          "key_id": paymentConfig.key_id,
          "key_secret": paymentConfig.key_secret,
        });

        refundPaymentData = await razorPayRefund.payments.refund(
          context.params.paymentInitId,
          {
            "amount": parseInt(
              parseFloat(context.params.refundAmount) * defaultPaisa
            ),
            "speed": "normal",
            // "notes": {
            //   "notes_key_1": "Beam me up Scotty.",
            //   "notes_key_2": "Engage",
            // },
            // "receipt": "Receipt No. 31",
          }
          // context.params.generalSettings.data.currencyCode
        );
      } catch (error) {
        return { "code": 400, "data": {}, "message": error.message };
      }

      //-------------- Response Data Start -----------------------
      if (refundPaymentData.status === "processed") {
        responseData["authType"] = "NOAUTH";
        responseData["isDefault"] = true;
        // responseData['stripeCardToken'] = finalTokenData.id
        responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
        responseData["transactionId"] = refundPaymentData.id.toString();
        responseData["paymentIntentId"] = refundPaymentData.id.toString();
        responseData["paymentIntentAmount"] = refundPaymentData.amount;
        responseData["successResponse"] = refundPaymentData;
        responseData["status"] = refundPaymentData.status;
        return { "code": 200, "data": responseData, "message": "Valid Card" };
      } else {
        return { "code": 400, "data": {}, "message": "Payment Refund Failed" };
      }
    } else {
      return { "code": 400, "data": {}, "message": "Payment Capture Failed" };
    }
    //-------------- Response Data End -----------------------
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Card Payment Failed",
    };
  }
};
// transactionRazorpayEvents.cancelCapturePaymentIntents = async function (
//   context
// ) {
//   const responseData = {};
//   let dataChargeCapture = {};
//   let captureConfig = {};
//   const keyConfig = {};
//   if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//     keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
//   } else {
//     keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
//     keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
//   }
//   //-------------- Cancel Capture payment Intents Start -----------------------
//   captureConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   captureConfig["data"]["charge"] = context.params?.paymentInitId;
//   try {
//     const stripe = require("stripe")(captureConfig.secretKey);
//     dataChargeCapture = await stripe.paymentIntents.cancel(
//       captureConfig.data.charge
//     );
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   //-------------- Cancel Capture payment Intents End -----------------------
//   // Response
//   if (dataChargeCapture.status === "succeeded") {
//     responseData["authType"] = "NOAUTH";
//     responseData["isDefault"] = true;
//     responseData["stripeCustomerId"] = context.params.stripeCustomerId;
//     // responseData['transactionId'] = finalPaymentData.id.toString()
//     responseData["transactionId"] =
//       dataChargeCapture?.charges?.data[0].id.toString();
//     responseData["successResponse"] = dataChargeCapture;
//     return { "code": 200, "data": responseData, "message": "Amount Captured" };
//   } else {
//     return { "code": 400, "data": {}, "message": "Transaction Failed" };
//   }
// };

// transactionRazorpayEvents.cancelCaptureAndCreatePaymentTransaction =
//   async function (context) {
//     const keyConfig = {};
//     let cardBrand = "visa";
//     let finalPaymentData = {};
//     let dataChargeCapture = {};
//     let finalTokenData = {};
//     let tokenizationConfig = {};
//     let customerConfig = {};
//     let paymentConfig = {};
//     let captureConfig = {};
//     const responseData = {};
//     if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//       keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
//       keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
//     } else {
//       keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
//       keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
//     }
//     //-------------- Cancel Capture payment Intents Start -----------------------
//     captureConfig = {
//       ...keyConfig,
//       "data": {},
//     };
//     captureConfig["data"]["charge"] = context.params?.paymentInitId;
//     try {
//       const stripe = require("stripe")(captureConfig.secretKey);
//       dataChargeCapture = await stripe.paymentIntents.cancel(
//         captureConfig.data.charge
//       );
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//     //-------------- Cancel Capture payment Intents End -----------------------
//     //------------------ Create Token Start-------------------------------
//     tokenizationConfig = {
//       ...keyConfig,
//       "data": {},
//     };
//     tokenizationConfig["data"]["card"] = {};
//     tokenizationConfig["data"]["card"]["number"] =
//       context.params.cardData.data.cardNumber;
//     tokenizationConfig["data"]["card"]["exp_month"] =
//       context.params.cardData.data.expiryMonth;
//     tokenizationConfig["data"]["card"]["exp_year"] =
//       context.params.cardData.data.expiryYear;
//     tokenizationConfig["data"]["card"]["cvc"] = context.params.cardData.cvv;
//     // Stripe Call For card tokenization
//     try {
//       const stripe = require("stripe")(tokenizationConfig.secretKey);
//       finalTokenData = await stripe.tokens.create(tokenizationConfig.data);
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//     //------------------ Create Token End-------------------------------
//     if (context.params.userData.stripeCustomerId) {
//       cardBrand = finalTokenData?.card?.brand.toLowerCase();
//       //------------------ Update Customer Token Start ---------------------------------
//       customerConfig = {
//         ...keyConfig,
//         "data": {},
//       };
//       customerConfig["data"]["source"] = finalTokenData.id;
//       // Stripe Call For create customer
//       let finalCustomerData = {};
//       try {
//         const stripe = require("stripe")(customerConfig.secretKey);
//         finalCustomerData = await stripe.customers.update(
//           context.params.userData.stripeCustomerId, //stripeCustomerId
//           //   finalTokenData.id, //stripe Token
//           customerConfig.data
//         );
//       } catch (error) {
//         return { "code": 400, "data": {}, "message": error.message };
//       }
//       //------------------ Update Customer Token End ---------------------------------

//       //-------------- Create charges Start -----------------------
//       paymentConfig = {
//         ...keyConfig,
//         "data": {},
//       };
//       paymentConfig["data"]["amount"] = parseInt(
//         parseFloat(context.params.paymentInitAmount) * defaultPaisa
//       );
//       paymentConfig["data"]["currency"] =
//         context.params.generalSettings.data.currencyCode;
//       // paymentConfig['data']['payment_method'] = 'pm_card_' + cardBrand
//       paymentConfig["data"]["payment_method_types"] = ["card"];
//       paymentConfig["data"]["capture_method"] = "automatic"; //automatic or manual
//       paymentConfig["data"]["confirm"] = true;
//       // paymentConfig['data']['confirmation_method'] = 'manual'
//       // paymentConfig['data']['requires_capture'] = false
//       // paymentConfig['data']['requires_action'] = false

//       // paymentConfig['data']['automatic_payment_methods'] = true
//       paymentConfig["data"]["customer"] =
//         context.params.userData.stripeCustomerId;
//       paymentConfig["data"]["description"] = context.params.transactionReason;
//       //Stripe Call For create Charge
//       try {
//         const stripe = require("stripe")(paymentConfig.secretKey);
//         finalPaymentData = await stripe.paymentIntents.create(
//           paymentConfig.data
//         );
//       } catch (error) {
//         return {
//           "code": 400,
//           "data": {},
//           "message": "Card Payment Failed",
//         };
//       }
//       //-------------- Create charges End -----------------------
//       //-------------- Response Data Start -----------------------
//       if (finalPaymentData.status === "succeeded") {
//         // Response
//         responseData["authType"] = "NOAUTH";
//         // responseData['transactionId'] = finalPaymentData.id.toString()
//         responseData["transactionId"] =
//           finalPaymentData?.charges?.data[0].id.toString();
//         responseData["successResponse"] = finalPaymentData;
//         return {
//           "code": 200,
//           "data": responseData,
//           "message": "Card Payment Successfully",
//         };
//       } else {
//         return {
//           "code": 400,
//           "data": {},
//           "message": "Card Payment Failed",
//         };
//       }
//       //-------------- Response Data End -----------------------
//     }
//   };

// transactionRazorpayEvents.updateCaptureAmountAndCapturePaymentTransaction =
//   async function (context) {
//     const keyConfig = {};
//     // let cardBrand = "visa";
//     let finalPaymentData = {};
//     let dataChargeCapture = {};
//     // let finalTokenData = {};
//     // let tokenizationConfig = {};
//     // let customerConfig = {};
//     let paymentConfig = {};
//     let captureConfig = {};
//     const responseData = {};
//     if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//       keyConfig["secretKey"] = context.params.paymentData["testSecretKey"];
//       keyConfig["publishableKey"] = context.params.paymentData["testPublicKey"];
//     } else {
//       keyConfig["secretKey"] = context.params.paymentData["liveSecretKey"];
//       keyConfig["publishableKey"] = context.params.paymentData["livePublicKey"];
//     }
//     //-------------- Cancel Capture payment Intents Start -----------------------
//     const paymentIntentId = context.params?.paymentInitId || null;
//     const paymentInitAmount = parseInt(
//       parseFloat(context.params.paymentInitAmount) * defaultPaisa
//     );
//     if (paymentIntentId) {
//       captureConfig = {
//         ...keyConfig,
//         "data": {},
//       };
//       // captureConfig["data"]["amount"] = context.params?.paymentInitAmount;
//       try {
//         const stripe = require("stripe")(captureConfig.secretKey);
//         dataChargeCapture = await stripe.paymentIntents.update(
//           paymentIntentId,
//           // captureConfig.data.amount
//           { "amount": paymentInitAmount }
//         );
//       } catch (error) {
//         return { "code": 400, "data": {}, "message": error.message };
//       }
//       //------------------ Create Token End-------------------------------
//       // if (context.params.userData.stripeCustomerId) {
//       //-------------- Create charges Start -----------------------
//       paymentConfig = {
//         ...keyConfig,
//         "data": {},
//       };
//       //Stripe Call For create Charge
//       try {
//         const stripe = require("stripe")(paymentConfig.secretKey);
//         finalPaymentData = await stripe.paymentIntents.confirm(paymentIntentId);
//       } catch (error) {
//         return {
//           "code": 400,
//           "data": {},
//           "message": "Card Payment Failed",
//         };
//       }
//       //-------------- Create charges End -----------------------
//       //-------------- Response Data Start -----------------------
//       if (finalPaymentData.status === "succeeded") {
//         // Response
//         responseData["authType"] = "NOAUTH";
//         // responseData['transactionId'] = finalPaymentData.id.toString()
//         responseData["transactionId"] = paymentIntentId;
//         responseData["paymentIntentId"] = paymentIntentId;
//         responseData["successResponse"] = finalPaymentData;
//         return {
//           "code": 200,
//           "data": responseData,
//           "message": "Card Payment Successfully",
//         };
//       } else {
//         return {
//           "code": 400,
//           "data": {},
//           "message": "Card Payment Failed",
//         };
//       }
//       //-------------- Response Data End -----------------------
//       // }
//     } else {
//       return {
//         "code": 400,
//         "data": {},
//         "message": "Card Payment Failed",
//       };
//     }
//   };
//--------------------------------------
module.exports = transactionRazorpayEvents;
