const { MoleculerError } = require("moleculer").Errors;
const storageUtils = require("../utils/storage.util");
const constantUtils = require("../utils/constant.util");
const flutterwave = require("../paymentGateways/fultterwave.payment");
const Mongoose = require("mongoose");
const { customAlphabet } = require("nanoid");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const constantUtil = require("../utils/constant.util");
const Flutterwave = require("flutterwave-node-v3");

// export json
const transactionFlutterWaveEvents = {};

const clientEncryption = (body, sec_key) => {
  function getKey() {
    const keymd5 = md5(sec_key);
    const keymd5last12 = keymd5.substr(-12);

    const seckeyadjusted = sec_key.replace("FLWSECK-", "");
    const seckeyadjustedfirst12 = seckeyadjusted.substr(0, 12);

    return seckeyadjustedfirst12 + keymd5last12;
  }
  //
  body = JSON.stringify(body);
  const cipher = forge.cipher.createCipher(
    "3DES-ECB",
    forge.util.createBuffer(getKey())
  );
  cipher.start({ "iv": "" });
  cipher.update(forge.util.createBuffer(body, "utf-8"));
  cipher.finish();
  const encrypted = cipher.output;
  return forge.util.encode64(encrypted.getBytes());
};

//-------------- Flutter Wave V1 ---------------
//#region Flutter Wave V1
transactionFlutterWaveEvents.checkValidCard = async function (context) {
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList)
    throw new MoleculerError(
      "CANT FIND PAYMENT OPTIONS LIST FROM DB REDIS PROBLEM"
    );
  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  // find userData
  let userData;
  if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  } else if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) throw new MoleculerError("cant userdata");
  // transaction id for forther use
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}`;

  const transaction = await this.adapter.model.create({
    "paymentType": constantUtils.PAYMENTGATEWAY,
    "gatewayName": constantUtils.FLUTTERWAVE,
    "transactionType": constantUtils.VERIFYCARD,
    "transactionId": transactionId, //unique reference find in webhook and user identificaatin from admin

    "gatewayDocId": paymentData._id,
    "transactionAmount": 50, // change dynamically
    "previousBalance": null,
    "currentBalance": null,
    "transactionReason": `card adding into ${generalSettings.data.siteTitle.toLowerCase()} ${Date.now()}`,
    "from": {
      "name": `${userData.firstName} ${userData.lastName}`,
      "userType": context.params.userType,
      "userId": context.params.userId.toString(),
    },
    "to": {
      "userType": constantUtils.ADMIN,
      "userId": paymentData.accountId,
    },
    "transactionStatus": constantUtils.PENDING,
    "cardDetailes": {
      "cardNumber": context.params.cardNumber,
      "expiryMonth": context.params.expiryMonth,
      "expiryYear": context.params.expiryYear,
      "holderName": context.params.holderName,
    },
  });
  // setup key base on payemnt gateway live or sandbox
  let secretKey;
  let publicKey;
  let redirectUrl;
  if (paymentData.mode === constantUtils.LIVE) {
    secretKey = paymentData.liveSecretKey;
    publicKey = paymentData.livePublicKey;
    redirectUrl = `${
      generalSettings.data.redirectUrls.paymentRedirectApiUrl
    }/api/transaction/redirectPayment/${transaction._id.toString()}`;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    secretKey = paymentData.testSecretKey;
    publicKey = paymentData.testPublicKey;
    redirectUrl = `${
      generalSettings.data.redirectUrls.paymentRedirectApiUrl
    }/api/transaction/redirectPayment/${transaction._id.toString()}`;
  }

  //

  const payload = {
    "PBFPubKey": publicKey,
    "cardno": context.params.cardNumber,
    "cvv": context.params.cvv,
    "expirymonth": context.params.expiryMonth,
    "expiryyear": context.params.expiryYear,
    "currency": "NGN",
    "country": "NG",
    "amount": "50", // its hard coded need to chage we have to work
    "email": userData.email,
    "phonenumber": userData.phone.number,
    "firstname": userData.firstName,
    "lastname": userData.lastName,
    "txRef": transactionId, //unique reference find in webhook
    "redirect_url": redirectUrl,
    // "do_3ds": true,
    "usesecureauth": true,
  };
  const charge = await flutterwave.charge({
    "payload": payload,
    "secretkey": secretKey,
  });

  if (charge.status === "success") {
    //
    const updatedData = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "gatewayResponse": charge.data,
      }
    );
    //
    if (!updatedData.nModified) {
      throw new MoleculerError("SOME THING WENT WRONG");
    }
    //
    if (
      charge.data.authModelUsed === "NOAUTH" &&
      charge.data.authurl === "N/A"
    ) {
      //  verify
      const verify = await flutterwave.chargeVerify({
        "txref": charge.data.txRef || charge.data.txref,
        "SECKEY": secretKey,
      });
      // manage status
      if (verify.status === "success") {
        // success or not want save data on db
        const transactionUpdate = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          { "gatewayResponse": verify.data }
        );

        if (!transactionUpdate.nModified) {
          throw new MoleculerError("transaction not found");
        }
        if (verify.data.status === "successful") {
          // refund
          this.broker.emit("transaction.flutterwave.refund", {
            "transactionId": transaction._id.toString(),
            "amount": transaction.transactionAmount,
          });

          // return save card
          return {
            "code": 200,
            "message": "CARD VERIFIED SUCCESSFULLY",
            "data": {
              "authType": constantUtils.NOAUTH,
              "embedtoken": verify.data.card.card_tokens[0].embedtoken,
              "type": verify.data.card.type,
            },
          };
        }
      }
      // this means hanldle conditions
      return {
        "code": 400,
        "message":
          "AMOUNT DETECTION NOT YET VERIFIED IF AMOUNT IS DTEUCTED IT WILL REFUND SOON",
        "data": {},
      };
    }
    if (
      (charge.data.authModelUsed &&
        charge.data.authModelUsed === "VBVSECURECODE") ||
      (charge.data.authModelUsed === "NOAUTH" && charge.data.authurl !== "N/A")
    ) {
      return {
        "code": 200,
        "message": charge.message,
        "data": {
          "authType": "REDIRECT",
          "authurl": charge.data.authurl,
          "redirectUrl": charge.data.redirectUrl,
          "transactionId": transaction._id,
          "payload": payload,
        },
      };
    }
    if (charge.data.suggested_auth === "PIN") {
      return {
        "code": 200,
        "message": "PLEASE ENTER YOUR PIN",
        "data": {
          "authType": constantUtils.PIN,
          "transactionId": transaction._id,
          "payload": payload,
        },
      };
    }
    if (
      charge.data.suggested_auth === "NOAUTH_INTERNATIONAL" ||
      charge.data.suggested_auth === "AVS_VBVSECURECODE"
    ) {
      return {
        "code": 200,
        "data": {
          "authType": constantUtils.ADDRESS,
          "payload": payload,
          "transactionId": transaction._id.toString(),
          "addressDetails": [
            {
              "key": "billingzip",
              "title": "Billing Zip",
              "hint": "Zip code or postal card registered with the card",
            },
            {
              "key": "billingcity",
              "title": "Billing City",
              "hint": "City registered with the card",
            },
            {
              "key": "billingaddress",
              "title": "Billing Address",
              "hint": "House/Building address registered with the card",
            },
            {
              "key": "billingstate",
              "title": "Billing State",
              "hint": "State registered with the card",
            },
            {
              "key": "billingcountry",
              "title": "Billing Country",
              "hint": "Country registered with the card",
            },
          ],
        },
      };
    }
    return charge;
  }
  if (charge.status === "error") {
    return {
      "code": 400,
      "data": charge.data,
      "message": charge.data.message || charge.message,
    };
  }
  return charge;
};
transactionFlutterWaveEvents.refund = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG REDIS ERROR CANT FIND PAYMENT"
    );
  }
  let gatewayData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }
  if (!gatewayData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG FLUTTERWAVE PAYEMENT GATEWAY DATE NOT FOUND OR STATUS OF PLEASE CHECK CAREFULLY "
    );
  }
  // GET TRANSACTION DETAILES
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction) {
    throw new MoleculerError(
      `SOMETHING WENT WRONG CANT FIND ONE TRANSACTION , ID IS ${context.params._id} BUT NEED TO REFUND IT ,THIS IS PAYEMNT CHECK CAREFULLY`
    );
  }

  const refund = await flutterwave.refund({
    "ref":
      transaction.gatewayResponse?.flwref ||
      transaction.gatewayResponse?.flwRef ||
      transaction.gatewayResponse?.data?.flwref ||
      transaction.gatewayResponse?.data?.flwRef,
    "seckey": secretKey,
    "amount": context.params.amount,
  });
  console.log("refund Detailes", refund);
  if (refund.status === "error") {
    // need to set code inform to ADMIN
  }
  if (refund.status === "success") {
    if (
      refund.data.status === "completed" ||
      refund.data.status === "completed-mpgs"
    ) {
      // update db
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "$set": {
            "refundResponse": refund.data,
          },
        }
      );
      // verify the refund
      const reufundVerify = await flutterwave.refundVerify({
        "id": refund.data.id,
        "seckey": secretKey,
      });
      if (reufundVerify.status === "fail") {
        // need to inform admin
      }
      if (reufundVerify.status === "success") {
        const updateTransaction = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          {
            "transactionStatus": constantUtils.SUCCESS,
            "refundResponse": reufundVerify.data,
          }
        );
        return {
          "code": 200,
          "message": "successfully refunded",
          "data": {},
        };
      }
      // need to inform admin
    }
    // need to inform admin
  }
  return refund;
};
transactionFlutterWaveEvents.walletRecharge = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) throw new MoleculerError("SOMETHING WENT WRONG");
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
    }
  }
  if (!gatewayData) {
    throw new MoleculerError("NO ACTIVE PAYMENTGATEWAY");
  }

  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }
  // GET USERDATA
  let userData;
  if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  } else if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      "CANT FIND USER SOMETHING WENT WRONG IN FLUTTERWAVE WALLET AMOUNT RECHARGE"
    );
  }
  // card detailes
  const cardDetailes = userData.cards.find(
    (card) => card._id.toString() === context.params.cardId
  );
  if (!!cardDetailes === false) {
    return {
      "code": 400,
      "message": "GIVEN CARD  NOT FOUND SOMETHING WENT WRONG",
      "data": {},
    };
  }
  // need generalSettings
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);

  //
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}-${context.params.reason || ""}`;

  // IF EMBED TOKEN FOUND ONLY OTP PROCESS
  if (cardDetailes.embedtoken) {
    const payload = {
      "currency": "NGN",
      "SECKEY": secretKey,
      "token": cardDetailes.embedtoken,
      "country": "NG",
      "amount": context.params.amount,
      "email": userData.email,
      "firstname": userData.firstName,
      "lastname": userData.lastName,
      "txRef": transactionId,
    };
    const charge = await flutterwave.tokenizedCharge(payload);

    if (charge.status === "success") {
      if (charge.data.status === "successful") {
        const transaction = await this.adapter.model.create({
          "transactionAmount": charge.data.amount,
          "previousBalance": null,
          "currentBalance": null,
          "transactionReason": context.params.reason,
          "from": {
            "userType": context.params.userType,
            "userId": context.params.userId,
            "name": userData.firstName + userData.lastName,
            "phoneNumber": userData.phoneNumber,
          },
          "to": {
            "userType": constantUtils.ADMIN,
            "userId": null,
          },
          "paymentType": constantUtils.CREDIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
          "gatewayName": constantUtils.FLUTTERWAVE,
          "transactionType": constantUtils.WALLETRECHARGE,
          "transactionId": transactionId,
          "gatewayDocId": gatewayData._id,
          "transactionStatus": constantUtils.PENDING,
          "cardDetailes": cardDetailes,
          "gatewayResponse": charge.data,
        });
        if (
          charge.data.authModelUsed === "noauth" ||
          charge.data.authModelUsed === "NOAUTH"
        ) {
          // auth type checking
          // find need verify
          if (charge.data.chargeResponseCode === "00") {
            // verify the charge transaction
            const verify = await flutterwave.chargeVerify({
              "txref": charge.data.txRef || charge.data.txrsef,
              "SECKEY": secretKey,
            });
            // verify success
            if (verify.status === "success") {
              if (verify.data.status === "successful") {
                if (verify.data.chargecode === "00") {
                  // updateDb
                  const updateTransaction = await this.adapter.model.updateOne(
                    {
                      "_id": Mongoose.Types.ObjectId(
                        transaction._id.toString()
                      ),
                    },
                    {
                      "gatewayResponse": verify.data,
                      "transactionStatus": constantUtils.SUCCESS,
                    }
                  );
                  if (!updateTransaction.nModified) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN DB UPDATION"
                    );
                  }
                  // add amount to userData
                  let amountUpdateInUser;
                  if (context.params.userType === constantUtils.PROFESSIONAL) {
                    amountUpdateInUser = await this.broker.emit(
                      "professional.updateWalletAmount",
                      {
                        "professionalId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                      }
                    );
                  } else if (context.params.userType === constantUtils.USER) {
                    amountUpdateInUser = await this.broker.emit(
                      "user.updateWalletAmount",
                      {
                        "userId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                        "scheduleFreezedAmount":
                          userData.wallet.scheduleFreezedAmount,
                      }
                    );
                  }
                  amountUpdateInUser =
                    amountUpdateInUser && amountUpdateInUser[0];
                  if (!amountUpdateInUser) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN WALLET UPDATE IN WALLET RECHARE NO AUTH"
                    );
                  }
                  return {
                    "code": 200,
                    "message": "AMOUNT IS ADDED TO YOUR WALLET SUCCESSFULLY",
                    "data": {
                      "availableAmount":
                        userData.wallet.availableAmount + context.params.amount,
                    },
                  };
                }
                return {
                  "code": 400,
                  "message": verify.message,
                  "data": verify.data,
                };
                // then dont need some times need to check
              }
              return {
                "code": 400,
                "message": verify.message,
                "data": verify.data,
              };
            }
            //  SOMTHIG HAPPEND WRONG NEED TO INFORM ADMIN

            throw new MoleculerError(
              "SOME THING WENT WRONG IN WALLET RECHARGE "
            );
          }
          // it means dont know what happens need to know for developers
          return {
            "code": 400,
            "message": charge.message,
            "data": charge.data,
          };
        }
      }
      // charge not approved
      return {
        "code": 400,
        "message": charge.data.vbvrespmessage,
        "data": charge.data,
      };
    }
    // it handles error
    return {
      "code": 400,
      "message": charge.message,
      "data": charge.data,
    };
  }

  return {
    "code": 400,
    "message": "CARD NOT VALID CARD REMOVE YOUR CARD OR ADD THE CARD AGAIN",
    "data": {},
  };
};

// webhook
transactionFlutterWaveEvents.webhooking = async function (context) {
  // get gatewayData form db

  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }

  if (context.params["event.type"] === "CARD_TRANSACTION") {
    //
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "successful") {
      if (transaction.transactionStatus === constantUtils.SUCCESS) {
        // jsust return alredy working correctly dont nedd any process
        return null;
      }

      // it menas sucess transaction is pending
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );

      if (transaction.transactionType === constantUtils.WALLETRECHARGE) {
        // walletRecharge
        const verify = await flutterwave.chargeVerify({
          "txref": context.params.txRef || context.params.txref,
          "SECKEY": gatewayData.testSecretKey,
        });
        //
        if (verify.status === "success") {
          const transactionUpdate = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            { "gatewayResponse": verify.data }
          );
          if (!transactionUpdate.nModified) {
            throw new MoleculerError("transaction not found");
          }
        }

        // need to set the status in db and substract  the amount from db
      }
      if (transaction.transactionType === constantUtils.VERIFYCARD) {
        // save card to the professioanl or user
        // and refund it
        const verify = await flutterwave.chargeVerify({
          "txref": context.params.txRef || context.params.txref,
          "SECKEY": gatewayData.testSecretKey,
        });

        if (verify.status === "success") {
          // success or not want save data on db
          const transactionUpdate = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            { "gatewayResponse": verify.data }
          );

          if (!transactionUpdate.nModified) {
            throw new MoleculerError("transaction not found");
          }
          // verify successs
          if (verify.data.status === "successful") {
            if (transaction.from.userType === constantUtils.PROFESSIONAL) {
              let cardAdded = await this.broker.emit(
                "professional.addVerifiedCard",
                {
                  "card": {
                    ...transaction.cardDetailes,
                    "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                    "type": verify.data.card.type,
                  },
                  "professionalId": transaction.from.userId.toString(),
                }
              );
              cardAdded = cardAdded && cardAdded[0];
              if (!cardAdded) {
                throw new MoleculerError(
                  "SOMETHING WENT WRONG IN CARD ADDING TO PROFESSIONAL"
                );
              }

              if (cardAdded.code > 200) {
                throw new MoleculerError("SOMETHING WENT WRONG");
              }
            }
            if (transaction.from.userType === constantUtils.USER) {
              let cardAdded = await this.broker.emit("user.addVerifiedCard", {
                "card": {
                  ...transaction.cardDetailes,
                  "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                  "type": verify.data.card.type,
                },
                "userId": transaction.from.userId.toString(),
              });
              cardAdded = cardAdded && cardAdded[0];

              if (!cardAdded) {
                throw new MoleculerError(
                  "SOMETHING WENT WRONG IN CARD ADDING TO PROFESSIONAL"
                );
              }

              if (cardAdded.code > 200) {
                throw new MoleculerError("SOMETHING WENT WRONG");
              }
            }

            // REFUND

            this.broker.emit("transaction.flutterwave.refund", {
              "transactionId": transaction._id.toString(),
              "amount": transaction.transactionAmount,
            }); // need to work

            return; // return the function
          } //need to inform admin
          else {
            throw new MoleculerError(
              "SOMETHING WENT WRONG IN CARD VERIFY IN WEB HOOK CARD"
            );
          }
        } // status comes with error
        else {
          // need to inform amdin
          throw new MoleculerError("SOME THING WENT WRONG");
        }
      }
      // SOME CONDITIONS WILL ADDED MAYBE
    }
    // this means some redirect payment
    else {
      // HAI
    }
  }
  if (context.params["event.type"] === "ACCOUNT_TRANSACTION") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "successful") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
    }
  }
  if (context.params["event.type"] === "Transfer") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.transfer.reference },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.transfer.status === "SUCCESSFUL") {
      if (transaction.transactionStatus === constantUtils.SUCCESS) {
        return null;
      }
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
      return null;
    } else {
      // it means failed or cancelled or pending
      // it depends on the payment gateway
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "transactionStatus": context.params.transfer.status.toUpperCase(),
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );

      if (
        context.params.transfer.status.toUpperCase() === constantUtils.FAILED
      ) {
        // reterive the amount to whose is the user or professional
        if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
          // retrn the amount of professional
          if (transaction.to.userType === constantUtils.PROFESSIONAL) {
            let userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            let walletupdated = await this.broker.emit(
              "professional.updateWalletAmount",
              {
                "professionalId": userData._id.toString(),
                "availableAmount":
                  userData.wallet.availableAmount +
                  transaction.transactionAmount,
                "freezedAmount": userData.wallet.freezedAmount,
              }
            );
            walletupdated = walletupdated && walletupdated[0];
            if (walletupdated.code !== 200) {
              throw new MoleculerError(
                "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED"
              );
            }

            // send notification to professional
            let updatedtransaction = await this.adapter.model.findOne({
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            });
            updatedtransaction = updatedtransaction.toJSON();

            // remove not neede data for send notification

            //updated user detailes

            userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            //

            const notificationObject = {
              "clientId": context.params.clientId,
              "data": {
                "type": constantUtils.NOTIFICATIONTYPE,
                "userType": constantUtils.PROFESSIONAL,
                "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
                "timestamp": Date.now(),
                "message":
                  updatedtransaction.webhookresponse.transfer.complete_message,
                "details": lzStringEncode({
                  "walletAmount": userData.wallet.availableAmount,
                  "paymentType": updatedtransaction.paymentType,
                  "transactionStatus": updatedtransaction.transactionStatus,
                  "_id": updatedtransaction._id,
                }),
              },
              "registrationTokens": [
                {
                  "token": userData.deviceInfo[0].deviceId,
                  "id": userData._id.toString(),
                  "deviceType": userData.deviceInfo[0].deviceType,
                  "platform": userData.deviceInfo[0].platform,
                  "socketId": userData.deviceInfo[0].socketId,
                },
              ],
            };
            console.log(notificationObject);
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)

            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
            return;
          }
          // USER
          if (transaction.to.userType === constantUtils.USER) {
            let userData = await this.broker.emit("user.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            let walletupdated = await this.broker.emit(
              "user.updateWalletAmount",
              {
                "userId": userData._id.toString(),
                "availableAmount":
                  userData.wallet.availableAmount +
                  transaction.transactionAmount,
                "freezedAmount": userData.wallet.freezedAmount,
                "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
              }
            );
            walletupdated = walletupdated && walletupdated[0];
            if (walletupdated.code !== 200) {
              throw new MoleculerError(
                "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED"
              );
            }

            // send notification to professional
            let updatedtransaction = await this.adapter.model.findOne({
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            });
            updatedtransaction = updatedtransaction.toJSON();
            //updated user detailes

            userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            //

            const notificationObject = {
              "clientId": context.params.clientId,
              "data": {
                "type": constantUtils.NOTIFICATIONTYPE,
                "userType": constantUtils.USER,
                "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
                "timestamp": Date.now(),
                "message":
                  updatedtransaction.webhookresponse.transfer.complete_message,
                "details": lzStringEncode({
                  "walletAmount": userData.wallet.availableAmount,
                  "paymentType": updatedtransaction.paymentType,
                  "transactionStatus": updatedtransaction.transactionStatus,
                  "_id": updatedtransaction._id,
                }),
              },
              "registrationTokens": [
                {
                  "token": userData.deviceInfo[0].deviceId,
                  "id": userData._id.toString(),
                  "deviceType": userData.deviceInfo[0].deviceType,
                  "platform": userData.deviceInfo[0].platform,
                  "socketId": userData.deviceInfo[0].socketId,
                },
              ],
            };
            console.log(notificationObject);

            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)

            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);

            return null;
          }
          return;
        }
      }
      return null;
    }
  }

  if (context.params["event.type"] === "BANK_TRANSFER_TRANSACTION") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "SUCCESSFUL") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
    }
  }
  return null;
};

transactionFlutterWaveEvents.instantBooking = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }
  if (!gatewayData) {
    throw new MoleculerError("SOMETHING WENT WRONG");
  }
  // IN INSTANT BOOKING WE PRE AUTH THE MONEY
};
transactionFlutterWaveEvents.tripEndCardCharge = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }
  // only user is use this event
  let userData = await this.broker.emit("user.getById", {
    "id": context.params.userId,
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      "CANT FIND USER SOMETHING WENT WRONG IN FLUTTERWAVE WALLET AMOUNT RECHARGE"
    );
  }
  // gate live and public key finding
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }

  //
  // card detailes
  const cardDetailes = userData.cards.find(
    (card) => card._id.toString() === context.params.cardId
  );
  if (!!cardDetailes === false) {
    return {
      "code": 400,
      "message": "GIVEN CARD  NOT FOUND SOMETHING WENT WRONG",
      "data": {},
    };
  }
  // need generalSettings
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);

  //
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}-${context.params.reason || ""}`;
  if (cardDetailes.embedtoken) {
    const payload = {
      "currency": "NGN",
      "SECKEY": secretKey,
      "token": cardDetailes.embedtoken,
      "country": "NG",
      "amount": context.params.amount,
      "email": userData.email,
      "firstname": userData.firstName,
      "lastname": userData.lastName,
      "txRef": transactionId,
    };

    const charge = await flutterwave.tokenizedCharge(payload);

    if (charge.status === "success") {
      // db update
      const transaction = await this.adapter.model.create({
        "transactionAmount": charge.data.amount,
        "previousBalance": null,
        "currentBalance": null,
        "transactionReason": context.params.reason ?? "",
        "from": {
          "userType": constantUtils.USER,
          "userId": context.params.userId,
          "name": userData.firstName + userData.lastName,
          "phoneNumber": userData.phoneNumber,
        },
        "to": {
          "userType": constantUtils.ADMIN,
          "userId": null,
        },
        "paymentType": constantUtils.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
        "gatewayName": constantUtils.FLUTTERWAVE,
        "transactionType": constantUtils.BOOKINGCHARGE,
        "transactionId": transactionId,
        "gatewayDocId": gatewayData._id,
        "transactionStatus": constantUtils.PENDING,
        "cardDetailes": cardDetailes,
        "gatewayResponse": charge.data,
      });

      // sucess
      if (charge.data.status === "successful") {
        if (charge.data.authModelUsed === "noauth") {
          // auth type checking
          // find need verify
          if (charge.data.chargeResponseCode === "00") {
            // verify the charge transaction
            const verify = await flutterwave.chargeVerify({
              "txref": charge.data.txRef || charge.data.txref,
              "SECKEY": secretKey,
            });
            if (verify.status === "success") {
              if (verify.data.status === "successful") {
                if (verify.data.chargecode === "00") {
                  // updateDb
                  const updateTransaction = await this.adapter.model.updateOne(
                    {
                      "_id": Mongoose.Types.ObjectId(
                        transaction._id.toString()
                      ),
                    },
                    {
                      "gatewayResponse": verify.data,
                      "transactionStatus": constantUtils.SUCCESS,
                    }
                  );
                  if (!updateTransaction.nModified) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN DB UPDATION"
                    );
                  }
                  //
                  return {
                    "code": 200,
                    "message": "AMOUNT IS TAKEN FEOM YOUR CARD",
                    "data": {},
                  };
                }
              }
            }
          }
        }
      }
      //
    }
  }
  // this else of all above if condition  no condition matches bleow code excutes
  // substract amount from user avilable amount

  let amountUpdateInUser = await this.broker.emit("user.updateWalletAmount", {
    "userId": userData._id.toString(),
    "availableAmount": userData.wallet.availableAmount - context.params.amount,
    "freezedAmount": userData.wallet.freezedAmount,
    "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
  });
  amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
  if (!amountUpdateInUser) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN WALLET UPDATE IN WALLET RECHARE NO AUTH"
    );
  }
  // create transaction
  const updateTrasactionData = this.adapter.model.create({
    "transactionType": constantUtils.BOOKINGCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": userData.wallet.availableAmount,
    "currentBalance": userData.wallet.availableAmount - context.params.amount,
    "paidTransactionId": null,
    "transactionId": `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
      "1234567890",
      9
    )()}`,
    "transactionDate": new Date(),
    "paymentType": constantUtils.DEBIT,
    "from": {
      "userType": constantUtils.USER,
      "userId": context.params.userId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtils.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN END TRIP CARD MONEY TAKING WALLET TRASACTION"
    );
  }
  //
  return {
    "code": 200,
    "message": "AMOUNT IS TAKEN FROM YOUR WALLET ",
    "data": {
      "availableAmount":
        userData.wallet.availableAmount - context.params.amount,
    },
  };
};
transactionFlutterWaveEvents.withdraw = async function (context) {
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  // GET REQUESTER DATA
  let userData;
  if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId.toString(),
    });
  } else if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId.toString(),
    });
  }
  userData = userData && userData[0];

  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}${context.params.reason ? "-" + context.params.reason : ""}`;

  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  }
  if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  let bankDetails;
  if (userData.bankDetails?.accountNumber) {
    //its for professional because professional had only one bank account
    bankDetails = userData.bankDetails;
  }
  if (Array.isArray(userData.bankDetails)) {
    // its for user =>user can add multiple accounts but in long days before user can add signle bank
    if (context.params?.bankId) {
      bankDetails = userData.bankDetails.filter(
        (data) => data._id.toString() === context.params.bankId
      );
    } else {
      // if some users use single bank
      bankDetails = userData.bankDetails[0];
    }
  }

  if (!bankDetails?.accountNumber) {
    return {
      "code": 500,
      "data": {},
      "message": "KINDLEY ADD BANK DETAILES BEFORE WITHDRAW",
    };
  }
  const payload = {
    "meta": {
      "FirstName": userData.firstName,
      "LastName": userData.lastName,
      "EmailAddress": userData.email,
      "MobileNumber": `${userData.phone.code}${userData.phone.number}`,
      "Address": `${userData.address?.line},${userData.address?.city},${userData.address?.state},${userData.address?.country}`,
    },
    "amount": context.params.amount,
    "narration": context.params.reason || "",
    "currency": "NGN",
    "seckey": secretKey,
    "reference": transactionId, //unique
    "callback_url": `${generalSettings.data.paymentRedirectApiUrl}/api/transaction/flutterwave/webhook`,
    "beneficiary_name": bankDetails.accountName,
    "destination_branch_code": bankDetails?.bankCode,
    "debit_currency": "NGN",
    "account_bank": bankDetails?.bankCode,
    "account_number": bankDetails?.accountNumber,
  };

  const response = await flutterwave.createTransfer(payload);

  if (response.status === "error") {
    return {
      "code": 400,
      "data": null,
      "message": response.message,
    };
  }
  if (response.status === "success") {
    if (context.params.userType === constantUtils.USER) {
      // updateAmount
      this.broker.emit("user.updateWalletAmount", {
        "userId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
        "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
      });
    }
    if (context.params.userType === constantUtils.PROFESSIONAL) {
      this.broker.emit("professional.updateWalletAmount", {
        "professionalId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
      });
    }

    const transaction = await this.adapter.model.create({
      "gatewayName": constantUtils.FLUTTERWAVE,
      "transactionId": transactionId,
      "gatewayDocId": paymentData._id,
      "transactionAmount": parseFloat(context.params.amount),
      "previousBalance": userData.wallet.availableAmount,
      "currentBalance":
        userData.wallet.availableAmount - parseFloat(context.params.amount),
      "transactionReason": "WITHDRAW MONEY TO FROM WALLET",
      "transactionType": constantUtils.WALLETWITHDRAWAL,
      "transactionStatus": constantUtils.PROCESSING,
      "transactionDate": new Date(),
      "paymentType": constantUtils.DEBIT,
      "accountsDetail": {
        "accountName": userData.bankDetails.accountName,
        "accountNumber": userData.bankDetails.accountNumber,
        "bankCode": userData.bankDetails.bankCode,
      },
      "from": {
        "userType": constantUtils.ADMIN,
      },
      "to": {
        "userType": context.params.userType,
        "userId": userData._id,
        "name": userData.firstName + " " + userData.lastName || null,
        "avatar": userData.avatar || undefined,
        "phoneNumber": userData.phone.number || undefined,
      },
      "gatewayResponse": response.data || {},
    });

    if (!transaction) throw new MoleculerError("ERROR IN WALLET WITHDRAWAL");
    return {
      "code": 200,
      "data": {
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
      },
      "message": "MONEY SEND TO YOUT BANK ACCOUNT WITHIN 5- 15 BUSINESS DAYS",
    };
  }
  return response;
};
transactionFlutterWaveEvents.verifyBankAccount = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  const response = await flutterwave.bankAccountVerify({
    "secretKey": secretKey,
    "data": {
      "account_number": context.params.accountNumber,
      "account_bank": context.params.bankCode,
    },
  });

  if (response.status === "success") {
    return {
      "code": 200,
      "message": response.message,

      "data": [
        {
          "title": "Account Holder Name",
          "value": response.data.account_name,
          "key": "accountName",
        },
        {
          "title": "Account Number",
          "value": response.data.account_number,
          "key": "accountNumber",
        },

        {
          "title": "Bank Name",
          "value": context.params.bankName,
          "key": "bankName",
        },
        {
          "title": "Bank Code",
          "value": context.params.bankCode,
          "key": "bankCode",
        },
      ],
    };
  }
  return response;
};
transactionFlutterWaveEvents.adminRefundAction = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG REDIS ERROR CANT FIND PAYMENT"
    );
  }
  let gatewayData;

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }

  if (!gatewayData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG FLUTTERWAVE PAYEMENT GATEWAY DATE NOT FOUND OR STATUS OF PLEASE CHECK CAREFULLY "
    );
  }
  // find transaction

  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });

  if (!transaction) {
    return {
      "code": 400,
      "message": "WE CANT FIND YOUR TRANSACTION ID",
      "data": {},
    };
  }
  if (context.params.action === "verify") {
    const reufundVerify = await flutterwave.refundVerify({
      "id":
        transaction?.gatewayResponse?.id ??
        transaction?.gatewayResponse?.data?.id,
      "seckey": secretKey,
    });
    if (reufundVerify.status === "fail") {
      // need to inform admin
      return {
        "code": 400,
        "message": reufundVerify.message,
        "data": {},
      };
    }
    if (reufundVerify.status === "success") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(context.params.transactionId),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
          "refundResponse": reufundVerify.data,
        }
      );
      return {
        "code": 200,
        "message": "successfully refunded",
        "data": {},
      };
    }
    return reufundVerify;
  }
  if (context.params.action === "retry") {
    const reufundVerify = await flutterwave.refundVerify({
      "id":
        transaction?.gatewayResponse?.id ??
        transaction?.gatewayResponse?.data?.id,
      "seckey": secretKey,
    });

    if (reufundVerify.status === "success") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(context.params.transactionId),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
          "refundResponse": reufundVerify.data,
        }
      );
      return {
        "code": 200,
        "message": "successfully refunded",
        "data": {},
      };
    }
    // this means all fail

    const refund = await flutterwave.refund({
      "ref":
        transaction.gatewayResponse.flwref ||
        transaction.gatewayResponse.flwRef ||
        transaction.gatewayResponse.data.flwref ||
        transaction.gatewayResponse.data.flwRef,
      "seckey": secretKey,
      "amount": context.params.amount,
    });
    if (refund.status === "success") {
      if (
        refund.data.status === "completed" ||
        refund.data.status === "completed-mpgs"
      ) {
        // update db
        const updateTransaction = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          {
            "refundResponse": refund.data,
          }
        );
        // verify the refund
        const reufundVerify = await flutterwave.refundVerify({
          "id": refund.data.id,
          "seckey": secretKey,
        });
        if (reufundVerify.status === "fail") {
          // need to inform admin
          return {
            "code": 400,
            "message": reufundVerify.message,
            "data": {},
          };
        }
        if (reufundVerify.status === "success") {
          const updateTransaction = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            {
              "transactionStatus": constantUtils.SUCCESS,
              "refundResponse": reufundVerify.data,
            }
          );
          return {
            "code": 200,
            "message": "successfully refunded",
            "data": {},
          };
        }
        // need to inform admin
        return reufundVerify;
      }
      // need to inform admin
    }
    return refund;
  }
  return {
    "code": 400,
    "message": "SOMETHING  WENT WRONG ",
    "data": context.params,
  };
};

transactionFlutterWaveEvents.adminTransfer = async function (context) {
  if (
    context.params.action === "verify" ||
    context.params.action === "transferretry"
  ) {
    return {
      "code": 400,
      "message": `${context.params.action.toUpperCase()} FEATURE IS BEATA STAGE IT WILL WORK IN ONE WEEK`,
    };
  }
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction) {
    return {
      "code": 400,
      "message": "ID NOT FOUND",
      "data": {},
    };
  }
  if (
    transaction.transactionStatus === constantUtils.SUCCESS ||
    transaction.transactionStatus === constantUtils.PROCESSING
  ) {
    return {
      "code": 400,
      "message": `TRENSACTION IS ALREADY IN ${transaction.transactionStatus} SO YOU CANT TAKE ACTION THIS TIME`,
      "data": {},
    };
  }
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let paymentData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  let userData;
  if (transaction.to.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": transaction.to.userId.toString(),
    });
  } else if (transaction.to.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": transaction.to.userId.toString(),
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    return {
      "code": 500,
      "data": {},
      "message": "THIS TRANSACTION IS INVALID PLEASE IGNORE THIS TRANSACTION ",
    };
  }
  let bankDetails;
  if (userData?.bankDetails?.accountNumber) {
    //its for professional because professional had only one bank account
    bankDetails = userData.bankDetails;
  }
  if (Array.isArray(userData.bankDetails)) {
    // its for user =>user can add multiple accounts but in long days before user can add signle bank
    if (context.params?.bankId) {
      bankDetails = userData.bankDetails.filter(
        (data) => data._id.toString() === context.params.bankId
      );
    } else {
      // if some users use single bank
      bankDetails = userData.bankDetails[0];
    }
  }
  if (!bankDetails?.accountNumber) {
    return {
      "code": 500,
      "data": {},
      "message": "KINDLEY ADD BANK DETAILES BEFORE WITHDRAW",
    };
  }

  if (context.params.action.toUpperCase() === constantUtils.TRANSFERRETRY) {
    if (userData.wallet.availableAmount < transaction.transactionAmount) {
      return {
        "code": 400,
        "message": "THIS ACTION CANT MOVE FURTHER USER NOT HAVE AMOUNT",
        "data": {},
      };
    }
  }

  const payload = {
    "meta": {
      "FirstName": userData.firstName,
      "LastName": userData.lastName,
      "EmailAddress": userData.email,
      "MobileNumber": `${userData.phone.code}${userData.phone.number}`,
      "Address": `${userData.address?.line},${userData.address?.city},${userData.address?.state},${userData.address?.country}`,
    },
    "amount": transaction.transactionAmount,
    "narration": context.params.reason || "",
    "currency": "NGN",
    "seckey": secretKey,
    "reference": transaction.transactionId, //unique
    "callback_url": `${generalSettings.data.paymentRedirectApiUrl}/api/transaction/flutterwave/webhook`,
    "beneficiary_name": bankDetails?.accountName,
    "destination_branch_code": bankDetails?.bankCode,
    "debit_currency": "NGN",
    "account_bank": bankDetails?.bankCode,
    "account_number": bankDetails?.accountNumber,
  };
  const response = await flutterwave.createTransfer(payload);

  if (response.status === "error") {
    return {
      "code": 400,
      "data": null,
      "message": response.message,
    };
  }
  if (response.status === "success") {
    // status
    // if(response.data.status)
    const updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "gatewayResponse": response.data || {},
          "accountsDetail": {
            "accountName": userData.bankDetails.accountName,
            "accountNumber": userData.bankDetails.accountNumber,
            "bankCode": userData.bankDetails.bankCode,
          },
          "transactionStatus": constantUtils.PROCESSING,
          "to": {
            "userType": transaction.to.userType,
            "userId": userData._id.toString(),
            "name": userData.firstName + " " + userData.lastName || null,
            "avatar": userData.avatar || undefined,
            "phoneNumber": userData.phone.number || undefined,
          },
        },
      }
    );
    if (!updateTransaction.nModified) {
      throw new MoleculerError(
        "SOMETING WENT WRONG IN TRANSCTON UPDATE ADMIN MANULA RECHAGE"
      );
    }
    if (context.params.action.toUpperCase() === constantUtils.TRANSFERRETRY) {
      // TAKE AMOUNT
      if (transaction.to.userType === constantUtils.USER) {
        // updateAmount
        this.broker.emit("user.updateWalletAmount", {
          "userId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount -
            parseFloat(transaction.transactionAmount),
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        });
      }
      if (transaction.to.userType === constantUtils.PROFESSIONAL) {
        this.broker.emit("professional.updateWalletAmount", {
          "professionalId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount -
            parseFloat(transaction.transactionAmount),
          "freezedAmount": userData.wallet.freezedAmount,
        });
      }
    }
    return {
      "code": 200,
      "data": null,
      "message":
        "TRANSFER IS INTATED AMOUNT SEND TO RECIPIANT ACCOUNT SHORTLEY",
    };
  }
};

//#endregion Flutter Wave V1

//-------------- Flutter Wave V3 ---------------
//#region Flutter Wave V3

transactionFlutterWaveEvents.checkValidCardV3 = async function (context) {
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList)
    throw new MoleculerError(
      "CANT FIND PAYMENT OPTIONS LIST FROM DB REDIS PROBLEM"
    );
  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  // find userData
  let userData;
  if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  } else if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) throw new MoleculerError("cant userdata");
  // transaction id for forther use
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}`;

  const transaction = await this.adapter.model.create({
    "paymentType": constantUtils.PAYMENTGATEWAY,
    "gatewayName": constantUtils.FLUTTERWAVE,
    "transactionType": constantUtils.VERIFYCARD,
    "transactionId": transactionId, //unique reference find in webhook and user identificaatin from admin

    "gatewayDocId": paymentData._id,
    "transactionAmount": 50, // change dynamically
    "previousBalance": null,
    "currentBalance": null,
    "transactionReason": `card adding into ${generalSettings.data.siteTitle.toLowerCase()} ${Date.now()}`,
    "from": {
      "name": `${userData.firstName} ${userData.lastName}`,
      "userType": context.params.userType,
      "userId": context.params.userId.toString(),
    },
    "to": {
      "userType": constantUtils.ADMIN,
      "userId": paymentData.accountId,
    },
    "transactionStatus": constantUtils.PENDING,
    "cardDetailes": {
      "cardNumber": context.params.cardNumber,
      "expiryMonth": context.params.expiryMonth,
      "expiryYear": context.params.expiryYear,
      "holderName": context.params.holderName,
    },
  });
  // setup key base on payemnt gateway live or sandbox
  let secretKey;
  let publicKey;
  let redirectUrl;
  if (paymentData.mode === constantUtils.LIVE) {
    secretKey = paymentData.liveSecretKey;
    publicKey = paymentData.livePublicKey;
    redirectUrl = `${
      generalSettings.data.redirectUrls.paymentRedirectApiUrl
    }/api/transaction/redirectPayment/${transaction._id.toString()}`;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    secretKey = paymentData.testSecretKey;
    publicKey = paymentData.testPublicKey;
    redirectUrl = `${
      generalSettings.data.redirectUrls.paymentRedirectApiUrl
    }/api/transaction/redirectPayment/${transaction._id.toString()}`;
  }
  //
  const payload = {
    "PBFPubKey": publicKey,
    "cardno": context.params.cardNumber,
    "cvv": context.params.cvv,
    "expirymonth": context.params.expiryMonth,
    "expiryyear": context.params.expiryYear,
    "currency": "NGN",
    "country": "NG",
    "amount": "50", // its hard coded need to chage we have to work
    "email": userData.email,
    "phonenumber": userData.phone.number,
    "firstname": userData.firstName,
    "lastname": userData.lastName,
    "txRef": transactionId, //unique reference find in webhook
    "redirect_url": redirectUrl,
    // "do_3ds": true,
    "usesecureauth": true,
  };
  const charge = await flutterwave.chargeV3({
    "payload": payload,
    "secretkey": secretKey,
  });

  if (charge.status === "success") {
    //
    const updatedData = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "gatewayResponse": charge.data,
      }
    );
    //
    if (!updatedData.nModified) {
      throw new MoleculerError("SOME THING WENT WRONG");
    }
    //
    if (
      charge.data.authModelUsed === "NOAUTH" &&
      charge.data.authurl === "N/A"
    ) {
      //  verify
      const verify = await flutterwave.chargeVerifyV3({
        "txref": charge.data.txRef || charge.data.txref,
        "SECKEY": secretKey,
      });
      // manage status
      if (verify.status === "success") {
        // success or not want save data on db
        const transactionUpdate = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          { "gatewayResponse": verify.data }
        );

        if (!transactionUpdate.nModified) {
          throw new MoleculerError("transaction not found");
        }
        if (verify.data.status === "successful") {
          // refund
          this.broker.emit("transaction.flutterwave.refundV3", {
            "transactionId": transaction._id.toString(),
            "amount": transaction.transactionAmount,
          });
          // return save card
          return {
            "code": 200,
            "message": "CARD VERIFIED SUCCESSFULLY",
            "data": {
              "authType": constantUtils.NOAUTH,
              "embedtoken": verify.data.card.card_tokens[0].embedtoken,
              "type": verify.data.card.type,
            },
          };
        }
      }
      // this means hanldle conditions
      return {
        "code": 400,
        "message":
          "AMOUNT DETECTION NOT YET VERIFIED IF AMOUNT IS DTEUCTED IT WILL REFUND SOON",
        "data": {},
      };
    }
    if (
      (charge.data.authModelUsed &&
        charge.data.authModelUsed === "VBVSECURECODE") ||
      (charge.data.authModelUsed === "NOAUTH" && charge.data.authurl !== "N/A")
    ) {
      return {
        "code": 200,
        "message": charge.message,
        "data": {
          "authType": "REDIRECT",
          "authurl": charge.data.authurl,
          "redirectUrl": charge.data.redirectUrl,
          "transactionId": transaction._id,
          "payload": payload,
        },
      };
    }
    if (charge.data.suggested_auth === "PIN") {
      return {
        "code": 200,
        "message": "PLEASE ENTER YOUR PIN",
        "data": {
          "authType": constantUtils.PIN,
          "transactionId": transaction._id,
          "payload": payload,
        },
      };
    }
    if (
      charge.data.suggested_auth === "NOAUTH_INTERNATIONAL" ||
      charge.data.suggested_auth === "AVS_VBVSECURECODE"
    ) {
      return {
        "code": 200,
        "data": {
          "authType": constantUtils.ADDRESS,
          "payload": payload,
          "transactionId": transaction._id.toString(),
          "addressDetails": [
            {
              "key": "billingzip",
              "title": "Billing Zip",
              "hint": "Zip code or postal card registered with the card",
            },
            {
              "key": "billingcity",
              "title": "Billing City",
              "hint": "City registered with the card",
            },
            {
              "key": "billingaddress",
              "title": "Billing Address",
              "hint": "House/Building address registered with the card",
            },
            {
              "key": "billingstate",
              "title": "Billing State",
              "hint": "State registered with the card",
            },
            {
              "key": "billingcountry",
              "title": "Billing Country",
              "hint": "Country registered with the card",
            },
          ],
        },
      };
    }
    return charge;
  }
  if (charge.status === "error") {
    return {
      "code": 400,
      "data": charge.data,
      "message": charge.data.message || charge.message,
    };
  }
  return charge;
};

transactionFlutterWaveEvents.refundV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG REDIS ERROR CANT FIND PAYMENT"
    );
  }
  let gatewayData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }
  if (!gatewayData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG FLUTTERWAVE PAYEMENT GATEWAY DATE NOT FOUND OR STATUS OF PLEASE CHECK CAREFULLY "
    );
  }
  // GET TRANSACTION DETAILES
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction) {
    throw new MoleculerError(
      `SOMETHING WENT WRONG CANT FIND ONE TRANSACTION , ID IS ${context.params._id} BUT NEED TO REFUND IT ,THIS IS PAYEMNT CHECK CAREFULLY`
    );
  }

  const refund = await flutterwave.refundV3({
    "ref":
      transaction.gatewayResponse?.flwref ||
      transaction.gatewayResponse?.flwRef ||
      transaction.gatewayResponse?.data?.flwref ||
      transaction.gatewayResponse?.data?.flwRef,
    "seckey": secretKey,
    "amount": context.params.amount,
  });
  console.log("refund Detailes", refund);
  if (refund.status === "error") {
    // need to set code inform to ADMIN
  }
  if (refund.status === "success") {
    if (
      refund.data.status === "completed" ||
      refund.data.status === "completed-mpgs"
    ) {
      // update db
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "$set": {
            "refundResponse": refund.data,
          },
        }
      );
      // verify the refund
      const reufundVerify = await flutterwave.refundVerifyV3({
        "id": refund.data.id,
        "seckey": secretKey,
      });
      if (reufundVerify.status === "fail") {
        // need to inform admin
      }
      if (reufundVerify.status === "success") {
        const updateTransaction = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          {
            "transactionStatus": constantUtils.SUCCESS,
            "refundResponse": reufundVerify.data,
          }
        );
        return {
          "code": 200,
          "message": "successfully refunded",
          "data": {},
        };
      }
      // need to inform admin
    }
    // need to inform admin
  }
  return refund;
};

transactionFlutterWaveEvents.walletRechargeV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) throw new MoleculerError("SOMETHING WENT WRONG");
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
    }
  }
  if (!gatewayData) {
    throw new MoleculerError("NO ACTIVE PAYMENTGATEWAY");
  }
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }
  // GET USERDATA
  let userData;
  if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  } else if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      "CANT FIND USER SOMETHING WENT WRONG IN FLUTTERWAVE WALLET AMOUNT RECHARGE"
    );
  }
  // card detailes
  const cardDetailes = userData.cards.find(
    (card) => card._id.toString() === context.params.cardId
  );
  if (!!cardDetailes === false) {
    return {
      "code": 400,
      "message": "GIVEN CARD  NOT FOUND SOMETHING WENT WRONG",
      "data": {},
    };
  }
  // need generalSettings
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  //
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}-${context.params.reason || ""}`;

  // IF EMBED TOKEN FOUND ONLY OTP PROCESS
  if (cardDetailes.embedtoken) {
    const payload = {
      "currency": "NGN",
      "SECKEY": secretKey,
      "token": cardDetailes.embedtoken,
      "country": "NG",
      "amount": context.params.amount,
      "email": userData.email,
      "firstname": userData.firstName,
      "lastname": userData.lastName,
      "txRef": transactionId,
    };
    const charge = await flutterwave.tokenizedChargeV3(payload);
    if (charge.status === "success") {
      if (charge.data.status === "successful") {
        const transaction = await this.adapter.model.create({
          "transactionAmount": charge.data.amount,
          "previousBalance": null,
          "currentBalance": null,
          "transactionReason": context.params.reason,
          "from": {
            "userType": context.params.userType,
            "userId": context.params.userId,
            "name": userData.firstName + userData.lastName,
            "phoneNumber": userData.phoneNumber,
          },
          "to": {
            "userType": constantUtils.ADMIN,
            "userId": null,
          },
          "paymentType": constantUtils.CREDIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
          "gatewayName": constantUtils.FLUTTERWAVE,
          "transactionType": constantUtils.WALLETRECHARGE,
          "transactionId": transactionId,
          "gatewayDocId": gatewayData._id,
          "transactionStatus": constantUtils.PENDING,
          "cardDetailes": cardDetailes,
          "gatewayResponse": charge.data,
        });
        if (
          charge.data.authModelUsed === "noauth" ||
          charge.data.authModelUsed === "NOAUTH"
        ) {
          // auth type checking
          // find need verify
          if (charge.data.chargeResponseCode === "00") {
            // verify the charge transaction
            const verify = await flutterwave.chargeVerifyV3({
              "txref": charge.data.txRef || charge.data.txrsef,
              "SECKEY": secretKey,
            });
            // verify success
            if (verify.status === "success") {
              if (verify.data.status === "successful") {
                if (verify.data.chargecode === "00") {
                  // updateDb
                  const updateTransaction = await this.adapter.model.updateOne(
                    {
                      "_id": Mongoose.Types.ObjectId(
                        transaction._id.toString()
                      ),
                    },
                    {
                      "gatewayResponse": verify.data,
                      "transactionStatus": constantUtils.SUCCESS,
                    }
                  );
                  if (!updateTransaction.nModified) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN DB UPDATION"
                    );
                  }
                  // add amount to userData
                  let amountUpdateInUser;
                  if (context.params.userType === constantUtils.PROFESSIONAL) {
                    amountUpdateInUser = await this.broker.emit(
                      "professional.updateWalletAmount",
                      {
                        "professionalId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                      }
                    );
                  } else if (context.params.userType === constantUtils.USER) {
                    amountUpdateInUser = await this.broker.emit(
                      "user.updateWalletAmount",
                      {
                        "userId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                        "scheduleFreezedAmount":
                          userData.wallet.scheduleFreezedAmount,
                      }
                    );
                  }
                  amountUpdateInUser =
                    amountUpdateInUser && amountUpdateInUser[0];
                  if (!amountUpdateInUser) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN WALLET UPDATE IN WALLET RECHARE NO AUTH"
                    );
                  }
                  return {
                    "code": 200,
                    "message": "AMOUNT IS ADDED TO YOUR WALLET SUCCESSFULLY",
                    "data": {
                      "availableAmount":
                        userData.wallet.availableAmount + context.params.amount,
                      "wallet": {
                        ...userData.wallet,
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                      },
                    },
                  };
                } else {
                  return {
                    "code": 400,
                    "message": verify.message,
                    "data": verify.data,
                  };
                }
                // then dont need some times need to check
              } else {
                return {
                  "code": 400,
                  "message": verify.message,
                  "data": verify.data,
                };
              }
            } else {
              //  SOMTHIG HAPPEND WRONG NEED TO INFORM ADMIN
              throw new MoleculerError(
                "SOME THING WENT WRONG IN WALLET RECHARGE "
              );
            }
          }
          // it means dont know what happens need to know for developers
          return {
            "code": 400,
            "message": charge.message,
            "data": charge.data,
          };
        }
      }
      // charge not approved
      return {
        "code": 400,
        "message": charge.data.vbvrespmessage,
        "data": charge.data,
      };
    }
    // it handles error
    return {
      "code": 400,
      "message": charge.message,
      "data": charge.data,
    };
  }
  return {
    "code": 400,
    "message": "CARD NOT VALID CARD REMOVE YOUR CARD OR ADD THE CARD AGAIN",
    "data": {},
  };
};

// webhook
transactionFlutterWaveEvents.webhookingV3 = async function (context) {
  // get gatewayData form db

  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }
  if (context.params["event.type"] === "CARD_TRANSACTION") {
    //
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "successful") {
      if (transaction.transactionStatus === constantUtils.SUCCESS) {
        // jsust return alredy working correctly dont nedd any process
        return null;
      }

      // it menas sucess transaction is pending
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );

      if (transaction.transactionType === constantUtils.WALLETRECHARGE) {
        // walletRecharge
        const verify = await flutterwave.chargeVerifyV3({
          "txref": context.params.txRef || context.params.txref,
          "SECKEY": gatewayData.testSecretKey,
        });
        //
        if (verify.status === "success") {
          const transactionUpdate = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            { "gatewayResponse": verify.data }
          );
          if (!transactionUpdate.nModified) {
            throw new MoleculerError("transaction not found");
          }
        }
        // need to set the status in db and substract  the amount from db
      }
      if (transaction.transactionType === constantUtils.VERIFYCARD) {
        // save card to the professioanl or user
        // and refund it
        const verify = await flutterwave.chargeVerifyV3({
          "txref": context.params.txRef || context.params.txref,
          "SECKEY": gatewayData.testSecretKey,
        });

        if (verify.status === "success") {
          // success or not want save data on db
          const transactionUpdate = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            { "gatewayResponse": verify.data }
          );

          if (!transactionUpdate.nModified) {
            throw new MoleculerError("transaction not found");
          }
          // verify successs
          if (verify.data.status === "successful") {
            if (transaction.from.userType === constantUtils.PROFESSIONAL) {
              let cardAdded = await this.broker.emit(
                "professional.addVerifiedCard",
                {
                  "card": {
                    ...transaction.cardDetailes,
                    "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                    "type": verify.data.card.type,
                  },
                  "professionalId": transaction.from.userId.toString(),
                }
              );
              cardAdded = cardAdded && cardAdded[0];
              if (!cardAdded) {
                throw new MoleculerError(
                  "SOMETHING WENT WRONG IN CARD ADDING TO PROFESSIONAL"
                );
              }

              if (cardAdded.code > 200) {
                throw new MoleculerError("SOMETHING WENT WRONG");
              }
            }
            if (transaction.from.userType === constantUtils.USER) {
              let cardAdded = await this.broker.emit("user.addVerifiedCard", {
                "card": {
                  ...transaction.cardDetailes,
                  "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                  "type": verify.data.card.type,
                },
                "userId": transaction.from.userId.toString(),
              });
              cardAdded = cardAdded && cardAdded[0];

              if (!cardAdded) {
                throw new MoleculerError(
                  "SOMETHING WENT WRONG IN CARD ADDING TO PROFESSIONAL"
                );
              }

              if (cardAdded.code > 200) {
                throw new MoleculerError("SOMETHING WENT WRONG");
              }
            }

            // REFUND
            this.broker.emit("transaction.flutterwave.refundV3", {
              "transactionId": transaction._id.toString(),
              "amount": transaction.transactionAmount,
            }); // need to work
            return; // return the function
          } //need to inform admin
          else {
            throw new MoleculerError(
              "SOMETHING WENT WRONG IN CARD VERIFY IN WEB HOOK CARD"
            );
          }
        } // status comes with error
        else {
          // need to inform amdin
          throw new MoleculerError("SOME THING WENT WRONG");
        }
      }
      // SOME CONDITIONS WILL ADDED MAYBE
    }
    // this means some redirect payment
    else {
      // HAI
    }
  }
  if (context.params["event.type"] === "ACCOUNT_TRANSACTION") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "successful") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
    }
  }
  if (context.params["event.type"] === "Transfer") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.transfer.reference },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.transfer.status === "SUCCESSFUL") {
      if (transaction.transactionStatus === constantUtils.SUCCESS) {
        return null;
      }
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
      return null;
    } else {
      // it means failed or cancelled or pending
      // it depends on the payment gateway
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "transactionStatus": context.params.transfer.status.toUpperCase(),
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );

      if (
        context.params.transfer.status.toUpperCase() === constantUtils.FAILED
      ) {
        // reterive the amount to whose is the user or professional
        if (transaction.transactionType === constantUtils.WALLETWITHDRAWAL) {
          // retrn the amount of professional
          if (transaction.to.userType === constantUtils.PROFESSIONAL) {
            let userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            let walletupdated = await this.broker.emit(
              "professional.updateWalletAmount",
              {
                "professionalId": userData._id.toString(),
                "availableAmount":
                  userData.wallet.availableAmount +
                  transaction.transactionAmount,
                "freezedAmount": userData.wallet.freezedAmount,
              }
            );
            walletupdated = walletupdated && walletupdated[0];
            if (walletupdated.code !== 200) {
              throw new MoleculerError(
                "SOMETHING WENT WRONG  WALLET UPDATE IN PRFESSIONAL TRANSFER FALILED"
              );
            }

            // send notification to professional
            let updatedtransaction = await this.adapter.model.findOne({
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            });
            updatedtransaction = updatedtransaction.toJSON();

            // remove not neede data for send notification

            //updated user detailes

            userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            //

            const notificationObject = {
              "data": {
                "type": constantUtils.NOTIFICATIONTYPE,
                "userType": constantUtils.PROFESSIONAL,
                "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
                "timestamp": Date.now(),
                "message":
                  updatedtransaction.webhookresponse.transfer.complete_message,
                "details": lzStringEncode({
                  "walletAmount": userData.wallet.availableAmount,
                  "paymentType": updatedtransaction.paymentType,
                  "transactionStatus": updatedtransaction.transactionStatus,
                  "_id": updatedtransaction._id,
                }),
              },
              "registrationTokens": [
                {
                  "token": userData.deviceInfo[0].deviceId,
                  "id": userData._id.toString(),
                  "deviceType": userData.deviceInfo[0].deviceType,
                  "platform": userData.deviceInfo[0].platform,
                  "socketId": userData.deviceInfo[0].socketId,
                },
              ],
            };
            console.log(notificationObject);
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)

            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
            return;
          }
          // USER
          if (transaction.to.userType === constantUtils.USER) {
            let userData = await this.broker.emit("user.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            let walletupdated = await this.broker.emit(
              "user.updateWalletAmount",
              {
                "userId": userData._id.toString(),
                "availableAmount":
                  userData.wallet.availableAmount +
                  transaction.transactionAmount,
                "freezedAmount": userData.wallet.freezedAmount,
                "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
              }
            );
            walletupdated = walletupdated && walletupdated[0];
            if (walletupdated.code !== 200) {
              throw new MoleculerError(
                "SOMETHING WENT WRONG  WALLET UPDATE IN USER TRANSFER FALILED"
              );
            }

            // send notification to professional
            let updatedtransaction = await this.adapter.model.findOne({
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            });
            updatedtransaction = updatedtransaction.toJSON();
            //updated user detailes

            userData = await this.broker.emit("professional.getById", {
              "id": transaction.to.userId.toString(),
            });
            userData = userData && userData[0];
            if (!userData) {
              // NEED TO INFORM ADMIN = WHICH PROCESS NEED WORKING
              throw new MoleculerError("SOMETHING WENT WRONG");
            }
            //

            const notificationObject = {
              "data": {
                "type": constantUtils.NOTIFICATIONTYPE,
                "userType": constantUtils.USER,
                "action": constantUtils.ACTION_WALLETTRANSACTIONUPDATE,
                "timestamp": Date.now(),
                "message":
                  updatedtransaction.webhookresponse.transfer.complete_message,
                "details": lzStringEncode({
                  "walletAmount": userData.wallet.availableAmount,
                  "paymentType": updatedtransaction.paymentType,
                  "transactionStatus": updatedtransaction.transactionStatus,
                  "_id": updatedtransaction._id,
                }),
              },
              "registrationTokens": [
                {
                  "token": userData.deviceInfo[0].deviceId,
                  "id": userData._id.toString(),
                  "deviceType": userData.deviceInfo[0].deviceType,
                  "platform": userData.deviceInfo[0].platform,
                  "socketId": userData.deviceInfo[0].socketId,
                },
              ],
            };
            console.log(notificationObject);

            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            // this.broker.emit('socket.statusChangeEvent', notificationObject)

            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);

            return null;
          }
          return;
        }
      }
      return null;
    }
  }

  if (context.params["event.type"] === "BANK_TRANSFER_TRANSACTION") {
    const transaction = await this.adapter.model.findOneAndUpdate(
      { "transactionId": context.params.txRef || context.params.txref },
      { "webhookresponse": context.params },
      {
        "new": true, //unique reference find in webhook
      }
    );
    if (!transaction) {
      throw new MoleculerError(
        "SOMETHING REQUEST COME TO THE FLUTTERWAVE WEBHOOK BUT CANT FIND OUR TRANSACTION"
      );
    }
    if (context.params.status === "SUCCESSFUL") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": transaction._id.toString(),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
        }
      );
      if (!updateTransaction)
        return new MoleculerError(
          "SOMETHING WENT WRONG TRANSACTION WEBHOOK  DB UPDATION "
        );
    }
  }
  return null;
};

transactionFlutterWaveEvents.instantBookingV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }
  if (!gatewayData) {
    throw new MoleculerError("SOMETHING WENT WRONG");
  }
  // IN INSTANT BOOKING WE PRE AUTH THE MONEY
};

transactionFlutterWaveEvents.tripEndCardChargeV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
      break;
    }
  }
  // only user is use this event
  let userData = await this.broker.emit("user.getById", {
    "id": context.params.userId,
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      "CANT FIND USER SOMETHING WENT WRONG IN FLUTTERWAVE WALLET AMOUNT RECHARGE"
    );
  }
  // gate live and public key finding
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }

  //
  // card detailes
  const cardDetailes = userData.cards.find(
    (card) => card._id.toString() === context.params.cardId
  );
  if (!!cardDetailes === false) {
    return {
      "code": 400,
      "message": "GIVEN CARD  NOT FOUND SOMETHING WENT WRONG",
      "data": {},
    };
  }
  // need generalSettings
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);

  //
  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}-${context.params.reason || ""}`;
  if (cardDetailes.embedtoken) {
    const payload = {
      "currency": "NGN",
      "SECKEY": secretKey,
      "token": cardDetailes.embedtoken,
      "country": "NG",
      "amount": context.params.amount,
      "email": userData.email,
      "firstname": userData.firstName,
      "lastname": userData.lastName,
      "txRef": transactionId,
    };

    const charge = await flutterwave.tokenizedChargeV3(payload);

    if (charge.status === "success") {
      // db update
      const transaction = await this.adapter.model.create({
        "transactionAmount": charge.data.amount,
        "previousBalance": null,
        "currentBalance": null,
        "transactionReason": context.params.reason ?? "",
        "from": {
          "userType": constantUtils.USER,
          "userId": context.params.userId,
          "name": userData.firstName + userData.lastName,
          "phoneNumber": userData.phoneNumber,
        },
        "to": {
          "userType": constantUtils.ADMIN,
          "userId": null,
        },
        "paymentType": constantUtils.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
        "gatewayName": constantUtils.FLUTTERWAVE,
        "transactionType": constantUtils.BOOKINGCHARGE,
        "transactionId": transactionId,
        "gatewayDocId": gatewayData._id,
        "transactionStatus": constantUtils.PENDING,
        "cardDetailes": cardDetailes,
        "gatewayResponse": charge.data,
      });

      // sucess
      if (charge.data.status === "successful") {
        if (charge.data.authModelUsed === "noauth") {
          // auth type checking
          // find need verify
          if (charge.data.chargeResponseCode === "00") {
            // verify the charge transaction
            const verify = await flutterwave.chargeVerifyV3({
              "txref": charge.data.txRef || charge.data.txref,
              "SECKEY": secretKey,
            });
            if (verify.status === "success") {
              if (verify.data.status === "successful") {
                if (verify.data.chargecode === "00") {
                  // updateDb
                  const updateTransaction = await this.adapter.model.updateOne(
                    {
                      "_id": Mongoose.Types.ObjectId(
                        transaction._id.toString()
                      ),
                    },
                    {
                      "gatewayResponse": verify.data,
                      "transactionStatus": constantUtils.SUCCESS,
                    }
                  );
                  if (!updateTransaction.nModified) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN DB UPDATION"
                    );
                  }
                  //
                  return {
                    "code": 200,
                    "message": "AMOUNT IS TAKEN FEOM YOUR CARD",
                    "data": {},
                  };
                }
              }
            }
          }
        }
      }
      //
    }
  }
  // this else of all above if condition  no condition matches bleow code excutes
  // substract amount from user avilable amount

  let amountUpdateInUser = await this.broker.emit("user.updateWalletAmount", {
    "userId": userData._id.toString(),
    "availableAmount": userData.wallet.availableAmount - context.params.amount,
    "freezedAmount": userData.wallet.freezedAmount,
    "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
  });
  amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
  if (!amountUpdateInUser) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN WALLET UPDATE IN WALLET RECHARE NO AUTH"
    );
  }
  // create transaction
  const updateTrasactionData = this.adapter.model.create({
    "transactionType": constantUtils.BOOKINGCHARGE,
    "transactionAmount": context.params.amount,
    "previousBalance": userData.wallet.availableAmount,
    "currentBalance": userData.wallet.availableAmount - context.params.amount,
    "paidTransactionId": null,
    "transactionId": `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
      "1234567890",
      9
    )()}`,
    "transactionDate": new Date(),
    "paymentType": constantUtils.DEBIT,
    "from": {
      "userType": constantUtils.USER,
      "userId": context.params.userId,
      "name": userData.firstName + " " + userData.lastName || null,
      "avatar": userData.avatar || null,
      "phoneNumber": userData.phone.number || null,
    },
    "to": {
      "userType": constantUtils.ADMIN,
      "userId": null,
      "name": null,
      "avatar": null,
      "phoneNumber": null,
    },
  });
  if (!updateTrasactionData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN END TRIP CARD MONEY TAKING WALLET TRASACTION"
    );
  }
  //
  return {
    "code": 200,
    "message": "AMOUNT IS TAKEN FROM YOUR WALLET ",
    "data": {
      "availableAmount":
        userData.wallet.availableAmount - context.params.amount,
    },
  };
};

transactionFlutterWaveEvents.withdrawV3 = async function (context) {
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  // GET REQUESTER DATA
  let userData;
  if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId.toString(),
    });
  } else if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId.toString(),
    });
  }
  userData = userData && userData[0];

  const transactionId = `FL-${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
    "1234567890",
    9
  )()}${context.params.reason ? "-" + context.params.reason : ""}`;

  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  }
  if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  let bankDetails;
  if (userData.bankDetails?.accountNumber) {
    //its for professional because professional had only one bank account
    bankDetails = userData.bankDetails;
  }
  if (Array.isArray(userData.bankDetails)) {
    // its for user =>user can add multiple accounts but in long days before user can add signle bank
    if (context.params?.bankId) {
      bankDetails = userData.bankDetails.filter(
        (data) => data._id.toString() === context.params.bankId
      );
    } else {
      // if some users use single bank
      bankDetails = userData.bankDetails[0];
    }
  }

  if (!bankDetails?.accountNumber) {
    return {
      "code": 500,
      "data": {},
      "message": "KINDLEY ADD BANK DETAILES BEFORE WITHDRAW",
    };
  }
  const payload = {
    "meta": {
      "FirstName": userData.firstName,
      "LastName": userData.lastName,
      "EmailAddress": userData.email,
      "MobileNumber": `${userData.phone.code}${userData.phone.number}`,
      "Address": `${userData.address?.line},${userData.address?.city},${userData.address?.state},${userData.address?.country}`,
    },
    "amount": context.params.amount,
    "narration": context.params.reason || "",
    "currency": "NGN",
    "seckey": secretKey,
    "reference": transactionId, //unique
    "callback_url": `${generalSettings.data.paymentRedirectApiUrl}/api/transaction/flutterwave/webhook`,
    "beneficiary_name": bankDetails.accountName,
    "destination_branch_code": bankDetails?.bankCode,
    "debit_currency": "NGN",
    "account_bank": bankDetails?.bankCode,
    "account_number": bankDetails?.accountNumber,
  };

  const response = await flutterwave.createTransferV3(payload);

  if (response.status === "error") {
    return {
      "code": 400,
      "data": null,
      "message": response.message,
    };
  }
  if (response.status === "success") {
    if (context.params.userType === constantUtils.USER) {
      // updateAmount
      this.broker.emit("user.updateWalletAmount", {
        "userId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
        "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
      });
    }
    if (context.params.userType === constantUtils.PROFESSIONAL) {
      this.broker.emit("professional.updateWalletAmount", {
        "professionalId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
      });
    }

    const transaction = await this.adapter.model.create({
      "gatewayName": constantUtils.FLUTTERWAVE,
      "transactionId": transactionId,
      "gatewayDocId": paymentData._id,
      "transactionAmount": parseFloat(context.params.amount),
      "previousBalance": userData.wallet.availableAmount,
      "currentBalance":
        userData.wallet.availableAmount - parseFloat(context.params.amount),
      "transactionReason": "WITHDRAW MONEY TO FROM WALLET",
      "transactionType": constantUtils.WALLETWITHDRAWAL,
      "transactionStatus": constantUtils.PROCESSING,
      "transactionDate": new Date(),
      "paymentType": constantUtils.DEBIT,
      "accountsDetail": {
        "accountName": userData.bankDetails.accountName,
        "accountNumber": userData.bankDetails.accountNumber,
        "bankCode": userData.bankDetails.bankCode,
      },
      "from": {
        "userType": constantUtils.ADMIN,
      },
      "to": {
        "userType": context.params.userType,
        "userId": userData._id,
        "name": userData.firstName + " " + userData.lastName || null,
        "avatar": userData.avatar || undefined,
        "phoneNumber": userData.phone.number || undefined,
      },
      "gatewayResponse": response.data || {},
    });

    if (!transaction) throw new MoleculerError("ERROR IN WALLET WITHDRAWAL");
    return {
      "code": 200,
      "data": {
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
      },
      "message": "MONEY SEND TO YOUT BANK ACCOUNT WITHIN 5- 15 BUSINESS DAYS",
    };
  }
  return response;
};

transactionFlutterWaveEvents.verifyBankAccountV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let paymentData = {};

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  const response = await flutterwave.bankAccountVerifyV3({
    "secretKey": secretKey,
    "data": {
      "account_number": context.params.accountNumber,
      "account_bank": context.params.bankCode,
    },
  });

  if (response.status === "success") {
    return {
      "code": 200,
      "message": response.message,

      "data": [
        {
          "title": "Account Holder Name",
          "value": response.data.account_name,
          "key": "accountName",
        },
        {
          "title": "Account Number",
          "value": response.data.account_number,
          "key": "accountNumber",
        },

        {
          "title": "Bank Name",
          "value": context.params.bankName,
          "key": "bankName",
        },
        {
          "title": "Bank Code",
          "value": context.params.bankCode,
          "key": "bankCode",
        },
      ],
    };
  }
  return response;
};

transactionFlutterWaveEvents.adminRefundActionV3 = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG REDIS ERROR CANT FIND PAYMENT"
    );
  }
  let gatewayData;

  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (gatewayData.mode === constantUtils.LIVE) {
    publicKey = gatewayData.livePublicKey;
    secretKey = gatewayData.liveSecretKey;
  } else if (gatewayData.mode === constantUtils.SANDBOX) {
    publicKey = gatewayData.testPublicKey;
    secretKey = gatewayData.testSecretKey;
  }

  if (!gatewayData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG FLUTTERWAVE PAYEMENT GATEWAY DATE NOT FOUND OR STATUS OF PLEASE CHECK CAREFULLY "
    );
  }
  // find transaction

  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });

  if (!transaction) {
    return {
      "code": 400,
      "message": "WE CANT FIND YOUR TRANSACTION ID",
      "data": {},
    };
  }
  if (context.params.action === "verify") {
    const reufundVerify = await flutterwave.refundVerifyV3({
      "id":
        transaction?.gatewayResponse?.id ??
        transaction?.gatewayResponse?.data?.id,
      "seckey": secretKey,
    });
    if (reufundVerify.status === "fail") {
      // need to inform admin
      return {
        "code": 400,
        "message": reufundVerify.message,
        "data": {},
      };
    }
    if (reufundVerify.status === "success") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(context.params.transactionId),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
          "refundResponse": reufundVerify.data,
        }
      );
      return {
        "code": 200,
        "message": "successfully refunded",
        "data": {},
      };
    }
    return reufundVerify;
  }
  if (context.params.action === "retry") {
    const reufundVerify = await flutterwave.refundVerifyV3({
      "id":
        transaction?.gatewayResponse?.id ??
        transaction?.gatewayResponse?.data?.id,
      "seckey": secretKey,
    });

    if (reufundVerify.status === "success") {
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(context.params.transactionId),
        },
        {
          "transactionStatus": constantUtils.SUCCESS,
          "refundResponse": reufundVerify.data,
        }
      );
      return {
        "code": 200,
        "message": "successfully refunded",
        "data": {},
      };
    }
    // this means all fail

    const refund = await flutterwave.refundV3({
      "ref":
        transaction.gatewayResponse.flwref ||
        transaction.gatewayResponse.flwRef ||
        transaction.gatewayResponse.data.flwref ||
        transaction.gatewayResponse.data.flwRef,
      "seckey": secretKey,
      "amount": context.params.amount,
    });
    if (refund.status === "success") {
      if (
        refund.data.status === "completed" ||
        refund.data.status === "completed-mpgs"
      ) {
        // update db
        const updateTransaction = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          {
            "refundResponse": refund.data,
          }
        );
        // verify the refund
        const reufundVerify = await flutterwave.refundVerifyV3({
          "id": refund.data.id,
          "seckey": secretKey,
        });
        if (reufundVerify.status === "fail") {
          // need to inform admin
          return {
            "code": 400,
            "message": reufundVerify.message,
            "data": {},
          };
        }
        if (reufundVerify.status === "success") {
          const updateTransaction = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            {
              "transactionStatus": constantUtils.SUCCESS,
              "refundResponse": reufundVerify.data,
            }
          );
          return {
            "code": 200,
            "message": "successfully refunded",
            "data": {},
          };
        }
        // need to inform admin
        return reufundVerify;
      }
      // need to inform admin
    }
    return refund;
  }
  return {
    "code": 400,
    "message": "SOMETHING  WENT WRONG ",
    "data": context.params,
  };
};

transactionFlutterWaveEvents.adminTransferV3 = async function (context) {
  if (
    context.params.action === "verify" ||
    context.params.action === "transferretry"
  ) {
    return {
      "code": 400,
      "message": `${context.params.action.toUpperCase()} FEATURE IS BEATA STAGE IT WILL WORK IN ONE WEEK`,
    };
  }
  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transactionId),
  });
  if (!transaction) {
    return {
      "code": 400,
      "message": "ID NOT FOUND",
      "data": {},
    };
  }
  if (
    transaction.transactionStatus === constantUtils.SUCCESS ||
    transaction.transactionStatus === constantUtils.PROCESSING
  ) {
    return {
      "code": 400,
      "message": `TRENSACTION IS ALREADY IN ${transaction.transactionStatus} SO YOU CANT TAKE ACTION THIS TIME`,
      "data": {},
    };
  }
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  let paymentData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtils.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  } else if (paymentData.mode === constantUtils.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  let userData;
  if (transaction.to.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": transaction.to.userId.toString(),
    });
  } else if (transaction.to.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": transaction.to.userId.toString(),
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    return {
      "code": 500,
      "data": {},
      "message": "THIS TRANSACTION IS INVALID PLEASE IGNORE THIS TRANSACTION ",
    };
  }
  let bankDetails;
  if (userData?.bankDetails?.accountNumber) {
    //its for professional because professional had only one bank account
    bankDetails = userData.bankDetails;
  }
  if (Array.isArray(userData.bankDetails)) {
    // its for user =>user can add multiple accounts but in long days before user can add signle bank
    if (context.params?.bankId) {
      bankDetails = userData.bankDetails.filter(
        (data) => data._id.toString() === context.params.bankId
      );
    } else {
      // if some users use single bank
      bankDetails = userData.bankDetails[0];
    }
  }
  if (!bankDetails?.accountNumber) {
    return {
      "code": 500,
      "data": {},
      "message": "KINDLEY ADD BANK DETAILES BEFORE WITHDRAW",
    };
  }

  if (context.params.action.toUpperCase() === constantUtils.TRANSFERRETRY) {
    if (userData.wallet.availableAmount < transaction.transactionAmount) {
      return {
        "code": 400,
        "message": "THIS ACTION CANT MOVE FURTHER USER NOT HAVE AMOUNT",
        "data": {},
      };
    }
  }

  const payload = {
    "meta": {
      "FirstName": userData.firstName,
      "LastName": userData.lastName,
      "EmailAddress": userData.email,
      "MobileNumber": `${userData.phone.code}${userData.phone.number}`,
      "Address": `${userData.address?.line},${userData.address?.city},${userData.address?.state},${userData.address?.country}`,
    },
    "amount": transaction.transactionAmount,
    "narration": context.params.reason || "",
    "currency": "NGN",
    "seckey": secretKey,
    "reference": transaction.transactionId, //unique
    "callback_url": `${generalSettings.data.paymentRedirectApiUrl}/api/transaction/flutterwave/webhook`,
    "beneficiary_name": bankDetails?.accountName,
    "destination_branch_code": bankDetails?.bankCode,
    "debit_currency": "NGN",
    "account_bank": bankDetails?.bankCode,
    "account_number": bankDetails?.accountNumber,
  };
  const response = await flutterwave.createTransferV3(payload);

  if (response.status === "error") {
    return {
      "code": 400,
      "data": null,
      "message": response.message,
    };
  }
  if (response.status === "success") {
    // status
    // if(response.data.status)
    const updateTransaction = await this.adapter.model.updateOne(
      {
        "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
      },
      {
        "$set": {
          "gatewayResponse": response.data || {},
          "accountsDetail": {
            "accountName": userData.bankDetails.accountName,
            "accountNumber": userData.bankDetails.accountNumber,
            "bankCode": userData.bankDetails.bankCode,
          },
          "transactionStatus": constantUtils.PROCESSING,
          "to": {
            "userType": transaction.to.userType,
            "userId": userData._id.toString(),
            "name": userData.firstName + " " + userData.lastName || null,
            "avatar": userData.avatar || undefined,
            "phoneNumber": userData.phone.number || undefined,
          },
        },
      }
    );
    if (!updateTransaction.nModified) {
      throw new MoleculerError(
        "SOMETING WENT WRONG IN TRANSCTON UPDATE ADMIN MANULA RECHAGE"
      );
    }
    if (context.params.action.toUpperCase() === constantUtils.TRANSFERRETRY) {
      // TAKE AMOUNT
      if (transaction.to.userType === constantUtils.USER) {
        // updateAmount
        this.broker.emit("user.updateWalletAmount", {
          "userId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount -
            parseFloat(transaction.transactionAmount),
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        });
      }
      if (transaction.to.userType === constantUtils.PROFESSIONAL) {
        this.broker.emit("professional.updateWalletAmount", {
          "professionalId": userData._id.toString(),
          "availableAmount":
            userData.wallet.availableAmount -
            parseFloat(transaction.transactionAmount),
          "freezedAmount": userData.wallet.freezedAmount,
        });
      }
    }
    return {
      "code": 200,
      "data": null,
      "message":
        "TRANSFER IS INTATED AMOUNT SEND TO RECIPIANT ACCOUNT SHORTLEY",
    };
  }
};

//#endregion Flutter Wave V3
//-----------------------------
module.exports = transactionFlutterWaveEvents;
