//----------------- Need to Remove Start --------------------
const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
// const Mongoose = require("mongoose");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const axios = require("axios");
const { replace } = require("lodash");

const defaultCent = 100;
//----------------- Need to Remove End --------------------

const transactionZcreditapiaryEvents = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    return {
      "code": response.status,
      "data": response.data,
      "message": response.statusText,
    };
  } catch (error) {
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data?.Error || error.response.data?.message,
    };
  }
};

transactionZcreditapiaryEvents.checkValidCard = async function (context) {
  let finalPaymentData = {},
    paymentURL = null;
  const keyConfig = {},
    responseData = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["Password"] = context.params.paymentData["testSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
  } else {
    keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
  }
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      paymentURL = keyConfig.basePaymentURL + "/ValidateCard";
    } else {
      paymentURL = keyConfig.basePaymentURL + "/ValidateCard";
    }
    const ExpDate_MMYY =
      context.params.expiryMonth.toString() +
      context.params.expiryYear?.toString()?.substring(2, 4);
    const accessTokenConfigParams = {
      "method": "POST",
      "url": paymentURL,
      "headers": {
        // "Content-Type": "application/x-www-form-urlencoded",
        "Content-Type": "application/json; charset=utf-8",
      },
      "data": {
        "TerminalNumber": keyConfig.TerminalNumber,
        "Password": keyConfig.Password,
        "Track2": "",
        "CardNumber": context.params.cardNumber,
        "ExpDate_MMYY": ExpDate_MMYY,
      },
    };
    finalPaymentData = await axiosConfig(accessTokenConfigParams);
    // Response
    if (finalPaymentData.data.HasError === false) {
      responseData["authType"] = "NOAUTH";
      responseData["cardToken"] = finalPaymentData.data.Token;
      responseData["embedtoken"] = finalPaymentData.data.Token;
      responseData["isDefault"] = true;
      responseData["isCVVRequired"] = true;
      responseData["paymentIntentId"] = null;
      responseData["paymentIntentClientSecret"] = null;
      responseData["transactionAmount"] = null;
      responseData["transactionId"] = null;
      // responseData['paymentTransactionId'] = finalPaymentData.id.toString()
      responseData["successResponse"] = finalPaymentData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Card" };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": finalPaymentData?.data?.ReturnMessage,
      };
    }
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

transactionZcreditapiaryEvents.newRechargeWalletTransaction = async function (
  context
) {
  let paymentURL = null,
    responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["Password"] = context.params.paymentData["testSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
  } else {
    keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
  }
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    } else {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    }
    const transactionAmount = parseFloat(context.params.amount);
    const phoneNumber =
      context.params?.userData?.phone?.code +
      "-" +
      context.params?.userData?.phone?.number;
    const ExpDate_MMYY =
      context.params.cardData?.data?.expiryMonth?.toString() +
      context.params.cardData?.data?.expiryYear?.toString()?.substring(2, 4);
    //---------------
    const accessTokenConfigParams = {
      "method": "POST",
      "url": paymentURL,
      "headers": {
        // "Content-Type": "application/x-www-form-urlencoded",
        "Content-Type": "application/json; charset=utf-8",
      },
      "data": {
        "TerminalNumber": keyConfig.TerminalNumber,
        "Password": keyConfig.Password,
        "Track2": "",
        "CardNumber": context.params.cardData.data.cardNumber,
        "CVV": context.params.cardData.cvv,
        "ExpDate_MMYY": ExpDate_MMYY,
        "TransactionSum": transactionAmount,
        "NumberOfPayments": "1",
        "FirstPaymentSum": "0",
        "OtherPaymentsSum": "0",
        "TransactionType": "01",
        "CurrencyType": "1",
        "CreditType": "1",
        "J": "0", // For 0 --> Capture & Cancel, 2 & 5 --> Authentication
        "IsCustomerPresent": "false",
        "AuthNum": "",
        "HolderID": "",
        "ExtraData": "",
        "CustomerName": context.params.userData?.firstName || "firstName",
        "CustomerAddress": "",
        "CustomerEmail":
          context.params.userData?.email || "info@taximobility.co.il",
        "PhoneNumber": phoneNumber,
        "ItemDescription":
          context.params.transactionReason || "Wallet Recharge",
        "ObeligoAction": "",
        "OriginalZCreditReferenceNumber": "",
        "TransactionUniqueIdForQuery": "",
        "TransactionUniqueID": Date.now(), // must provide
        "UseAdvancedDuplicatesCheck": "",
      },
    };
    responseData = await axiosConfig(accessTokenConfigParams);
    // Response
    if (responseData.data.HasError === false) {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = false;
      responseData["paymentGateWayCustomerId"] = null;
      responseData["transactionId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
      responseData["redirectUrl"] = null;
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Details" };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": responseData.data.ReturnMessage,
      };
    }
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

// transactionZcreditapiaryEvents.newWithdrawalWalletCardPayment = async function (
//   context
// ) {
//   // let finalTokenData = {};
//   // let cardBrand = {};
//   let accessTokenConfig = {};
//   // let customerConfig = {};
//   // let tokenizationConfig = {};
//   let accessTokenData = {};
//   let responseData = {};
//   const keyConfig = {};
//   let accessTokenConfigURL = null;
//   let paymentAuthConfigURL = null;

//   if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//     keyConfig["client_secret"] = context.params.paymentData["testSecretKey"];
//     keyConfig["client_id"] = context.params.paymentData["testPublicKey"];
//     keyConfig["pin"] = context.params.paymentData["testEncryptionKey"];
//     keyConfig["basePaymentURL"] =
//       context.params.paymentData["testPaymentBaseURL"];
//     keyConfig["id"] = context.params.paymentData["testAccountId"];
//     keyConfig["account"] = context.params.paymentData["testAccountNo"];
//   } else {
//     keyConfig["client_secret"] = context.params.paymentData["liveSecretKey"];
//     keyConfig["client_id"] = context.params.paymentData["livePublicKey"];
//     keyConfig["pin"] = context.params.paymentData["liveEncryptionKey"];
//     keyConfig["basePaymentURL"] =
//       context.params.paymentData["livePaymentBaseURL"];
//     keyConfig["id"] = context.params.paymentData["liveAccountId"];
//     keyConfig["account"] = context.params.paymentData["liveAccountNo"];
//   }
//   const redirectUrl = `${
//     context.params.generalSettings.data.redirectUrls.paymentRedirectApiUrl
//   }/api/transaction/tigo/webhook${""}`;
//   const currencyCode = context.params.generalSettings.data.currencyCode;
//   const countryCode = context.params.generalSettings.data.defaultDialCode;
//   const countryCodeLong =
//     context.params?.generalSettings?.data?.defaultCountryCodeLong || "NIS";
//   const paymentGateWayCustomerId =
//     context.params?.userData?.paymentGateWayCustomerId || null;
//   // //---------------- create Charge End--------------------------
//   accessTokenConfig = {
//     ...keyConfig,
//     "data": {},
//   };
//   //#region Get Access token
//   try {
//     if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//       accessTokenConfigURL =
//         "/v1/oauth/generate/accesstoken-test-2018?grant_type=client_credentials";
//     } else {
//       accessTokenConfigURL =
//         "/v1/oauth/generate/accesstoken?grant_type=client_credentials";
//     }
//     // const formBody = [];
//     // formBody.push("client_secret=" + accessTokenConfig.client_secret);
//     // formBody.push("client_id=" + accessTokenConfig.client_id);

//     const accessTokenConfigParams = {
//       "method": "POST",
//       "url": accessTokenConfig.basePaymentURL + accessTokenConfigURL,
//       "headers": {
//         "Content-Type": "application/x-www-form-urlencoded",
//       },
//       "data":
//         "client_secret=" +
//         accessTokenConfig.client_secret +
//         "&client_id=" +
//         accessTokenConfig.client_id,
//       // "x-www-form-urlencoded": {
//       //   // "data": {
//       //   "client_secret": accessTokenConfig.client_secret,
//       //   "client_id": accessTokenConfig.client_id,

//       //   // },
//       // },
//     };
//     accessTokenData = await axiosConfig(accessTokenConfigParams);
//   } catch (error) {
//     return { "code": 400, "data": {}, "message": error.message };
//   }
//   //#endregion Get Access token

//   if (accessTokenData.code === 200) {
//     //#region Get Payment Authentication
//     try {
//       if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
//         paymentAuthConfigURL = "/v1/tigo/mfs/depositRemittance-test-2018";
//       } else {
//         paymentAuthConfigURL = "/v1/tigo/mfs/depositRemittance";
//       }
//       const phoneNumber =
//         (context.params?.phoneCode || countryCode) +
//         (context.params?.phoneNumber || "");
//       const transactionId = (
//         context.params.transactionId || accessTokenData.data.issuedAt
//       ).toString();
//       const transactionAmount = parseFloat(context.params.amount).toFixed(2);

//       const paymentAuthConfigParams = {
//         "method": "post",
//         "url": accessTokenConfig.basePaymentURL + paymentAuthConfigURL,
//         "headers": {
//           "Content-Type": "application/json",
//           "accessToken": accessTokenData.data.accessToken,
//         },
//         "data": {
//           "transactionRefId": "1300074238",
//           "PaymentAggregator": {
//             "account": "255123123123",
//             "pin": "1234",
//             "id": "Company Name",
//           },
//           "Sender": {
//             "firstName": "Jane",
//             "lastName": "Doe",
//             "msisdn": "2551234123423",
//             "emailAddress": "janedoe@mail.com",
//           },
//           "ReceivingSubscriber": {
//             "account": "255111111111",
//             "countryCallingCode": "255",
//             "countryCode": "NIS",
//             "firstName": "John",
//             "lastName": "Doe",
//           },
//           "OriginPayment": {
//             "amount": "100.00",
//             "currencyCode": "EUR",
//             "tax": "10.00",
//             "fee": "25.00",
//           },
//           "exchangeRate": "2182.23",
//           "verificationRequest": "true",
//           "sendTextMessage": "true",
//           "LocalPayment": {
//             "amount": "200",
//             "currencyCode": "TZS",
//           },
//         },
//         "data1": {
//           "MasterMerchant": {
//             "account": replace(accessTokenConfig.account.trim(), "+", ""),
//             "pin": accessTokenConfig.pin,
//             "id": accessTokenConfig.id,
//           },
//           "Subscriber": {
//             "account": replace(phoneNumber.trim(), "+", ""),
//             "countryCode": replace(countryCode.trim(), "+", ""),
//             "country": countryCodeLong,
//             "firstName": context.params.userData?.firstName || "firstName",
//             "lastName": context.params.userData?.lastName || "lastName",
//             "emailId": context.params.userData?.email || "info@twendetech.com",
//           },
//           "redirectUri": redirectUrl,
//           "callbackUri": redirectUrl,
//           "language": "eng",
//           "originPayment": {
//             "amount": transactionAmount,
//             "currencyCode": currencyCode,
//             "tax": "0.00",
//             "fee": "0.00",
//           },
//           "LocalPayment": {
//             "amount": transactionAmount,
//             "currencyCode": currencyCode,
//           },
//           "transactionRefId": transactionId,
//         },
//       };
//       responseData = await axiosConfig(paymentAuthConfigParams);
//     } catch (error) {
//       return { "code": 400, "data": {}, "message": error.message };
//     }
//     //#endregion Get Payment Authentication

//     // Response
//     if (responseData.code === 200) {
//       responseData["authType"] = "NOAUTH";
//       responseData["isDefault"] = false;
//       responseData["paymentGateWayCustomerId"] = paymentGateWayCustomerId;
//       responseData["transactionId"] =
//         responseData.data.transactionRefId.toString();
//       responseData["paymentIntentId"] =
//         responseData.data.transactionRefId.toString();
//       responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
//       responseData["redirectUrl"] = responseData.data.redirectUrl.toString();
//       responseData["successResponse"] = responseData.data;
//       responseData["status"] = "SUCESS";
//       return { "code": 200, "data": responseData, "message": "Valid Details" };
//     } else {
//       return { "code": 400, "data": {}, "message": responseData.message };
//     }
//   } else {
//     return { "code": 400, "data": {}, "message": accessTokenData.message };
//   }
// };

transactionZcreditapiaryEvents.createPaymentIntents = async function (context) {
  let paymentURL = null,
    responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["Password"] = context.params.paymentData["testSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
  } else {
    keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
  }
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    } else {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    }
    const transactionAmount = parseFloat(context.params.estimationAmount);
    const phoneNumber =
      context.params?.userData?.phone?.code +
      "-" +
      context.params?.userData?.phone?.number;
    const ExpDate_MMYY =
      context.params.cardData?.data?.expiryMonth?.toString() +
      context.params.cardData?.data?.expiryYear?.toString()?.substring(2, 4);
    //---------------
    const accessTokenConfigParams = {
      "method": "POST",
      "url": paymentURL,
      "headers": {
        // "Content-Type": "application/x-www-form-urlencoded",
        "Content-Type": "application/json; charset=utf-8",
      },
      "data": {
        "TerminalNumber": keyConfig.TerminalNumber,
        "Password": keyConfig.Password,
        "Track2": "",
        "CardNumber": context.params.cardData.data.cardNumber,
        "CVV": context.params.cardData.cvv,
        "ExpDate_MMYY": ExpDate_MMYY,
        // "CardNumber": context.params.cardToken, // Using Auth Tokenization Method
        // "CVV": "",
        // "ExpDate_MMYY": "",
        "TransactionSum": transactionAmount,
        "NumberOfPayments": "1",
        "FirstPaymentSum": "0",
        "OtherPaymentsSum": "0",
        "TransactionType": "01",
        "CurrencyType": "1",
        "CreditType": "1",
        "J": "5", // For 0 --> Capture & Cancel, 2 & 5 --> Authentication
        "IsCustomerPresent": "false",
        "AuthNum": "",
        "HolderID": "",
        "ExtraData": "",
        "CustomerName": context.params.userData?.firstName || "firstName",
        "CustomerAddress": "",
        "CustomerEmail":
          context.params.userData?.email || "info@taximobility.co.il",
        "PhoneNumber": phoneNumber,
        "ItemDescription": context.params.transactionReason,
        "ObeligoAction": "", // "" --> Authorization, Cature & Cancel Amount, 1 --> Capture, 2 --> Release/cancel
        "OriginalZCreditReferenceNumber": "",
        "TransactionUniqueIdForQuery": "",
        "TransactionUniqueID": "", //context.params.bookingId, // must provide
        "UseAdvancedDuplicatesCheck": "",
      },
    };
    responseData = await axiosConfig(accessTokenConfigParams);
    // Response
    if (responseData.data.HasError === false) {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = false;
      responseData["paymentGateWayCustomerId"] = null;
      responseData["transactionId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentId"] =
        responseData.data.ReferenceNumber.toString();
      // responseData["paymentIntentAmount"] = parseFloat(
      //   context.params.estimationAmount
      // );
      responseData["paymentIntentClientSecret"] = null;
      responseData["transactionAmount"] = parseFloat(
        context.params.estimationAmount
      );
      responseData["redirectUrl"] = null;
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return {
        "code": 200,
        "data": responseData,
        "message": "Valid Details",
      };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": responseData.data.ReturnMessage,
      };
    }
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

transactionZcreditapiaryEvents.capturePaymentIntentsTransaction =
  async function (context) {
    let paymentURL = null,
      responseData = {};
    const keyConfig = {};

    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      keyConfig["Password"] = context.params.paymentData["testSecretKey"];
      keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
      keyConfig["basePaymentURL"] =
        context.params.paymentData["testPaymentBaseURL"];
    } else {
      keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
      keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
      keyConfig["basePaymentURL"] =
        context.params.paymentData["livePaymentBaseURL"];
    }

    try {
      if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
        paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
      } else {
        paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
      }
      const transactionAmount = parseFloat(context.params.captureAmount);
      const phoneNumber =
        context.params?.userData?.phone?.code +
        "-" +
        context.params?.userData?.phone?.number;
      const ExpDate_MMYY =
        context.params.cardData?.data?.expiryMonth?.toString() +
        context.params.cardData?.data?.expiryYear?.toString()?.substring(2, 4);
      //---------------
      const accessTokenConfigParams = {
        "method": "POST",
        "url": paymentURL,
        "headers": {
          // "Content-Type": "application/x-www-form-urlencoded",
          "Content-Type": "application/json; charset=utf-8",
        },
        "data": {
          "TerminalNumber": keyConfig.TerminalNumber,
          "Password": keyConfig.Password,
          "Track2": "",
          "CardNumber": context.params.cardData.data.cardNumber,
          "CVV": context.params.cardData.cvv,
          "ExpDate_MMYY": ExpDate_MMYY,
          "TransactionSum": transactionAmount,
          "NumberOfPayments": "1",
          "FirstPaymentSum": "0",
          "OtherPaymentsSum": "0",
          "TransactionType": "01",
          "CurrencyType": "1",
          "CreditType": "1",
          "J": "0", // For 0 --> Capture & Cancel, 2 & 5 --> Authentication
          "IsCustomerPresent": "false",
          "AuthNum": "",
          "HolderID": "",
          "ExtraData": "",
          "CustomerName": context.params.userData?.firstName || "firstName",
          "CustomerAddress": "",
          "CustomerEmail":
            context.params.userData?.email || "info@taximobility.co.il",
          "PhoneNumber": phoneNumber,
          "ItemDescription": context.params.transactionReason,
          "ObeligoAction": "1", // "" --> Authorization, Cature & Cancel Amount, 1 --> Capture, 2 --> Release/cancel
          "OriginalZCreditReferenceNumber": context.params.paymentInitId,
          "TransactionUniqueIdForQuery": "",
          "TransactionUniqueID": Date.now(), // must provide
          "UseAdvancedDuplicatesCheck": "",
        },
      };
      responseData = await axiosConfig(accessTokenConfigParams);
      // Response
      if (responseData.data.HasError === false) {
        responseData["authType"] = "NOAUTH";
        responseData["isDefault"] = false;
        responseData["paymentGateWayCustomerId"] = null;
        responseData["transactionId"] =
          responseData.data.ReferenceNumber.toString();
        responseData["paymentIntentId"] =
          responseData.data.ReferenceNumber.toString();
        responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
        responseData["redirectUrl"] = null;
        responseData["successResponse"] = responseData.data;
        responseData["status"] = "SUCESS";
        return {
          "code": 200,
          "data": responseData,
          "message": "SUCESS",
        };
      } else {
        return {
          "code": 400,
          "data": {},
          "message": responseData.data.ReturnMessage,
        };
      }
    } catch (error) {
      return { "code": 400, "data": {}, "message": error.message };
    }
  };

transactionZcreditapiaryEvents.cancelCapturePaymentIntents = async function (
  context
) {
  let paymentURL = null,
    responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["Password"] = context.params.paymentData["testSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
  } else {
    keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
  }
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    } else {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    }
    const transactionAmount = parseFloat(context.params.amount);
    const phoneNumber =
      context.params?.userData?.phone?.code +
      "-" +
      context.params?.userData?.phone?.number;
    const ExpDate_MMYY =
      context.params.cardData?.data?.expiryMonth?.toString() +
      context.params.cardData?.data?.expiryYear?.toString()?.substring(2, 4);
    //---------------
    const accessTokenConfigParams = {
      "method": "POST",
      "url": paymentURL,
      "headers": {
        // "Content-Type": "application/x-www-form-urlencoded",
        "Content-Type": "application/json; charset=utf-8",
      },
      "data": {
        "TerminalNumber": keyConfig.TerminalNumber,
        "Password": keyConfig.Password,
        "Track2": "",
        "CardNumber": context.params.cardData.data.cardNumber,
        "CVV": context.params.cardData.cvv,
        "ExpDate_MMYY": ExpDate_MMYY,
        "TransactionSum": transactionAmount,
        "NumberOfPayments": "1",
        "FirstPaymentSum": "0",
        "OtherPaymentsSum": "0",
        "TransactionType": "01",
        "CurrencyType": "1",
        "CreditType": "1",
        "J": "0", // For 0 --> Capture & Cancel, 2 & 5 --> Authentication
        "IsCustomerPresent": "false",
        "AuthNum": "",
        "HolderID": "",
        "ExtraData": "",
        "CustomerName": context.params.userData?.firstName || "firstName",
        "CustomerAddress": "",
        "CustomerEmail":
          context.params.userData?.email || "info@taximobility.co.il",
        "PhoneNumber": phoneNumber,
        "ItemDescription": context.params.transactionReason,
        "ObeligoAction": "2", // "" --> Authorization, Cature & Cancel Amount, 1 --> Capture, 2 --> Release/cancel
        "OriginalZCreditReferenceNumber": context.params.paymentInitId,
        "TransactionUniqueIdForQuery": "",
        "TransactionUniqueID": "", // must provide
        "UseAdvancedDuplicatesCheck": "",
      },
    };
    responseData = await axiosConfig(accessTokenConfigParams);
    // Response
    if (responseData.data.HasError === false) {
      responseData["authType"] = "NOAUTH";
      responseData["paymentGateWayCustomerId"] = null;
      responseData["transactionId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
      responseData["redirectUrl"] = null;
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Details" };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": responseData.data.ReturnMessage,
      };
    }
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
};

transactionZcreditapiaryEvents.tripEndCardCharge = async function (context) {
  let paymentURL = null,
    responseData = {};
  const keyConfig = {};

  if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
    keyConfig["Password"] = context.params.paymentData["testSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["testPublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["testPaymentBaseURL"];
  } else {
    keyConfig["Password"] = context.params.paymentData["liveSecretKey"];
    keyConfig["TerminalNumber"] = context.params.paymentData["livePublicKey"];
    keyConfig["basePaymentURL"] =
      context.params.paymentData["livePaymentBaseURL"];
  }
  try {
    if (context.params.paymentData.mode.toUpperCase() === "SANDBOX") {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    } else {
      paymentURL = keyConfig.basePaymentURL + "/CommitFullTransaction";
    }
    const transactionAmount = parseFloat(context.params.amount);
    const phoneNumber =
      context.params?.userData?.phone?.code +
      "-" +
      context.params?.userData?.phone?.number;
    const ExpDate_MMYY =
      context.params.cardData?.data?.expiryMonth?.toString() +
      context.params.cardData?.data?.expiryYear?.toString()?.substring(2, 4);
    //---------------
    const accessTokenConfigParams = {
      "method": "POST",
      "url": paymentURL,
      "headers": {
        // "Content-Type": "application/x-www-form-urlencoded",
        "Content-Type": "application/json; charset=utf-8",
      },
      "data": {
        "TerminalNumber": keyConfig.TerminalNumber,
        "Password": keyConfig.Password,
        "Track2": "",
        "CardNumber": context.params.cardData.data.cardNumber,
        "CVV": context.params.cardData.cvv,
        "ExpDate_MMYY": ExpDate_MMYY,
        "TransactionSum": transactionAmount,
        "NumberOfPayments": "1",
        "FirstPaymentSum": "0",
        "OtherPaymentsSum": "0",
        "TransactionType": "01",
        "CurrencyType": "1",
        "CreditType": "1",
        "J": "0", // For 0 --> Capture & Cancel, 2 & 5 --> Authentication
        "IsCustomerPresent": "false",
        "AuthNum": "",
        "HolderID": "",
        "ExtraData": "",
        "CustomerName": context.params.userData?.firstName || "firstName",
        "CustomerAddress": "",
        "CustomerEmail":
          context.params.userData?.email || "info@taximobility.co.il",
        "PhoneNumber": phoneNumber,
        "ItemDescription": context.params.transactionReason,
        "ObeligoAction": "",
        "OriginalZCreditReferenceNumber": "",
        "TransactionUniqueIdForQuery": "",
        "TransactionUniqueID": Date.now(), // must provide
        "UseAdvancedDuplicatesCheck": "",
      },
    };
    responseData = await axiosConfig(accessTokenConfigParams);
    // Response
    if (responseData.data.HasError === false) {
      responseData["authType"] = "NOAUTH";
      responseData["isDefault"] = false;
      responseData["paymentGateWayCustomerId"] = null;
      responseData["transactionId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentId"] =
        responseData.data.ReferenceNumber.toString();
      responseData["paymentIntentAmount"] = parseFloat(context.params.amount);
      responseData["redirectUrl"] = null;
      responseData["successResponse"] = responseData.data;
      responseData["status"] = "SUCESS";
      return { "code": 200, "data": responseData, "message": "Valid Details" };
    } else {
      return {
        "code": 400,
        "data": {},
        "message": responseData.data.ReturnMessage,
      };
    }
  } catch (error) {
    return { "code": 400, "data": {}, "message": error.message };
  }
};
//--------------------------------------
module.exports = transactionZcreditapiaryEvents;
