const constantUtils = require("../utils/constant.util");
const { MoleculerError } = require("moleculer").Errors;
const Mongoose = require("mongoose");
const axios = require("axios");
//
const notifyMessage = require("../mixins/notifyMessage.mixin");
//
const transactionCyberSourceEvents = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    return {
      "code": response.status,
      "data": response.data,
      "message": response.statusText,
    };
  } catch (error) {
    console.log(JSON.stringify(error));
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data?.Error || error.response.data?.message,
    };
  }
};

transactionCyberSourceEvents.newRechargeWalletTransaction = async function (
  context
) {
  try {
    const transactionAmount = parseFloat(context.params.amount).toFixed(2);
    const currencyCode = context.params.generalSettings.data.currencyCode;
    const userType =
      context.params.userType === constantUtils.USER
        ? constantUtils.USER
        : constantUtils.DRIVER;
    const userId =
      context.params.userType === constantUtils.USER
        ? context.params.userId
        : context.params?.professionalId;
    const transactionId = Date.now();
    // let paymentURL = "http://payment.zervx.com/cybersourceHostedCheckout";
    // if (context.params.generalSettings.data.currencyCode === "zayride") {
    const paymentURL =
      "https://payment.fastrack.site/cybersourceHostedCheckout";
    // }
    const billToForename = context.params?.userData?.firstName || "";
    const billToSurname = context.params?.userData?.lastName || "";
    const billToEmail = context.params?.userData?.email || "";
    const billToAddressLine1 =
      context.params?.userData?.address?.line || "Address";
    const billToAddressCity = context.params?.userData?.address?.city || "city";
    const billToAddressState =
      context.params?.userData?.address?.state || "state";
    const billToAddressCountry =
      // context.params?.userData?.address?.country ||
      context.params.generalSettings.data.defaultCountryCode;
    const billToAddressPostalCode =
      context.params?.userData?.address?.zipcode || "1000";
    const cardNumber = context.params?.cardData?.data?.cardNumber;
    const cardExpiryDate = context.params?.cardData?.data
      ? (context.params?.cardData?.data?.expiryMonth?.length === 2
          ? context.params?.cardData?.data?.expiryMonth
          : "0" + context.params?.cardData?.data?.expiryMonth) +
        "-" +
        (context.params?.cardData?.data?.expiryYear?.length === 4
          ? context.params?.cardData?.data?.expiryYear
          : "20" + context.params?.cardData?.data?.expiryYear)
      : "-";
    const phoneNumber =
      context.params?.userData?.phone.code +
      context.params?.userData?.phone.number;
    const redirectUrl =
      paymentURL +
      "/" +
      currencyCode +
      "/" +
      transactionAmount +
      "/" +
      userType +
      "/" +
      userId +
      "/" +
      transactionId +
      "/" +
      billToForename +
      "/" +
      billToSurname +
      "/" +
      billToEmail +
      "/" +
      billToAddressLine1 +
      "/" +
      billToAddressCity +
      "/" +
      billToAddressState +
      "/" +
      billToAddressCountry +
      "/" +
      billToAddressPostalCode +
      "/" +
      cardNumber +
      "/" +
      phoneNumber +
      "/" +
      cardExpiryDate;
    console.log(JSON.stringify(redirectUrl));
    return {
      "code": 200,
      "message": "SUCESS",
      "data": {
        "transactionId": transactionId,
        "authType": "NOAUTH",
        "paymentIntentId": transactionId,
        "transactionAmount": 0,
        "paymentIntentClientSecret": transactionId,
        // "redirectUrl": "http://payment.zervx.com/cybersourceHostedCheckout",
        // "redirectUrl":
        //   "http://payment.zervx.com/cybersourceHostedCheckout?currencyCode=" +
        //   currencyCode +
        //   "&amount=" +
        //   transactionAmount,
        "redirectUrl": redirectUrl,
        "status": "SUCESS",
        "successResponse": {},
      },
    };
  } catch (error) {
    console.log(JSON.stringify(error));
  }
};

transactionCyberSourceEvents.cyberSourceWebhooking = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    userData = {},
    transactionStatus;
  const {
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    WALLET_AMOUNT_LOADED,
    SOMETHING_WENT_WRONG,
    WALLET_NOTIFICATION,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);

  const transactionAmount = context.params?.transactionAmount || 0;
  const cybersourceTransactionStatus = (
    context.params?.transactionStatus || ""
  ).toUpperCase();
  switch (cybersourceTransactionStatus) {
    case "ACCEPT":
      transactionStatus = constantUtils.SUCCESS;
      break;

    case "DECLINE":
    case "ERROR":
    case "CANCEL":
      transactionStatus = constantUtils.FAILED;
      break;

    default:
      transactionStatus = constantUtils.PENDING;
      break;
  }
  const userType = context.params.userType;
  try {
    //#region user Details
    if (userType === constantUtils.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.professionalId,
      });
      userData = userData && userData[0];
      if (!userData) throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    } else if (userType === constantUtils.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      userData = userData && userData[0];
      if (!userData) throw new MoleculerError(USER_NOT_FOUND, 500);
    }
    //#endregion user Details
    // add amount to userData
    // let amountUpdateInUser;
    if (
      userType === constantUtils.PROFESSIONAL &&
      transactionStatus === constantUtils.SUCCESS
    ) {
      await this.broker.emit("professional.updateWalletAmount", {
        "professionalId": context.params.professionalId.toString(),
        "availableAmount": userData.wallet.availableAmount + transactionAmount,
        "freezedAmount": userData.wallet.freezedAmount,
      });
    } else if (
      userType === constantUtils.USER &&
      transactionStatus === constantUtils.SUCCESS
    ) {
      await this.broker.emit("user.updateWalletAmount", {
        "userId": context.params.userId.toString(),
        "availableAmount": userData.wallet.availableAmount + transactionAmount,
        "freezedAmount": userData.wallet.freezedAmount,
        "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
      });
    }
    const stringTransactionReason = constantUtils.MSG_WALLETRECHARGE;
    //-------- Transaction Entry Start ---------------------
    let transaction = this.broker.emit(
      "transaction.insertTransactionBasedOnUserType",
      {
        "transactionType": constantUtils.WALLETRECHARGE,
        "transactionAmount": context.params.transactionAmount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": userData.wallet.availableAmount + transactionAmount,
        "transactionReason": stringTransactionReason,
        "transactionStatus": transactionStatus,
        "transactionId": context.params.transactionId,
        "paymentType": constantUtils.CREDIT,
        "paymentGateWay": constantUtils.CONST_CYBERSOURCE,
        "gatewayResponse": context.params?.gatewayResponse || {},
        "userType": userType,
        "userId":
          userType === constantUtils.USER
            ? context.params.userId.toString()
            : context.params.professionalId.toString(),
        "professionalId":
          userType === constantUtils.USER
            ? context.params.userId.toString()
            : context.params.professionalId.toString(),
        "paymentToUserType": constantUtils.ADMIN,
        "paymentToUserId": null,
      }
    );
    //-------- Transaction Entry End ---------------------
    transaction = transaction && transaction[0];
    if (!transaction) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    message = WALLET_AMOUNT_LOADED;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": {
      "availableAmount": userData.wallet.availableAmount + transactionAmount,
    },
  };
};

transactionCyberSourceEvents.checkCyberSourceTransactionStatusUsingTransactionId =
  async function (context) {
    let errorCode = 200,
      message = "",
      errorMessage = "",
      userData = {},
      transactionDetails = {};
    const {
      USER_NOT_FOUND,
      PROFESSIONAL_NOT_FOUND,
      WALLET_AMOUNT_LOADED,
      SOMETHING_WENT_WRONG,
      WALLET_NOTIFICATION,
    } = notifyMessage.setNotifyLanguage(context.params.langCode);
    const userType = context.params.userType;
    try {
      //#region user Details
      if (userType === constantUtils.PROFESSIONAL) {
        userData = await this.broker.emit("professional.getById", {
          "id": context.params.professionalId,
        });
        userData = userData && userData[0];
        if (!userData) throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
      } else if (userType === constantUtils.USER) {
        userData = await this.broker.emit("user.getById", {
          "id": context.params.userId,
        });
        userData = userData && userData[0];
        if (!userData) throw new MoleculerError(USER_NOT_FOUND, 500);
      }
      //#endregion user Details
      transactionDetails = await this.adapter.model.findOne(
        {
          "paymentType": constantUtils.CREDIT,
          "paymentGateWay": constantUtils.CONST_CYBERSOURCE,
          "transactionId": context.params.transactionId,
        },
        { "transactionStatus": 1, "transactionAmount": 1 }
      );
      message = constantUtils.SUCCESS;
    } catch (e) {
      errorCode = e.code || 400;
      errorMessage = e.message;
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
    }
    return {
      "code": errorCode,
      "error": errorMessage,
      "message": message,
      "data": {
        "availableAmount": parseFloat(userData?.wallet?.availableAmount),
        "status": transactionDetails?.transactionStatus || "",
        "transactionAmount":
          transactionDetails?.transactionStatus?.toUpperCase() ===
          constantUtils.SUCCESS
            ? parseFloat(transactionDetails?.transactionAmount || 0)
            : 0,
      },
    };
  };

//--------------------------------------
module.exports = transactionCyberSourceEvents;
