const notifyMessage = {};

notifyMessage.CREATED = "सफलतापूर्वक बनाया गया";
notifyMessage.UPDATED = "सफलतापूर्वक अपडेट किया गया";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "अमान्य उपयोगकर्ता डेटा";
notifyMessage.TRIP_END = "यात्रा समाप्त हो गई है";
notifyMessage.WALLET_LOADED = "बटुआ राशि लोड की गई";
notifyMessage.CASH = "नकद";
notifyMessage.CARD = "कार्ड";
notifyMessage.WALLET = "बटुआ";
notifyMessage.TRIP_FEE = "यात्रा -शुल्क";
notifyMessage.TOTAL_BILL = "कुल बिल";
notifyMessage.EARNINGS = "आपकी कमाई";
notifyMessage.WAITING_CHARGE = "प्रतीक्षा प्रभार";
notifyMessage.PENDING_AMOUNT = "बकाया राशि";
notifyMessage.CASH_IN_HAND = "हाथ में पैसे";
notifyMessage.BOOKING_ACCEPTED = "आपकी बुकिंग स्वीकार की जाती है";
notifyMessage.BOOKING_PAID = "बुकिंग के लिए भुगतान किया गया";
notifyMessage.PROFESSIONAL_STARTED = "पेशेवर ने आपकी यात्रा शुरू की";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "स्थान सफलतापूर्वक बदल गया";
notifyMessage.PROFESSIONAL_ARRIVED = "पेशेवर आपके पिकअप स्थान पर पहुंचे";
notifyMessage.PROFESSIONAL_COMMISSION = "व्यावसायिक आयोग";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "बुकिंग आय प्राप्त हुई";
notifyMessage.PROFESSIONAL_INFO =
  "आपका वॉलेट बैलेंस कैश राइड स्वीकार करने के लिए आवश्यक न्यूनतम सीमा से कम हो गया है। कृपया अपने वॉलेट को रिचार्ज करें या आप केवल कार्ड और वॉलेट राइड तब तक स्वीकार कर सकते हैं जब तक आपकी शेष राशि पर्याप्त नहीं हो जाती";
notifyMessage.PROFESSIONAL_NOT_FOUND = "पेशेवर नहीं मिला";
notifyMessage.PROFESSIONAL_NEW_RIDE = "आपको एक नई सवारी मिली";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "पेशेवर द्वारा रद्द की गई सवारी";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "आपकी बुकिंग हमारे पेशेवर द्वारा स्वीकार की जाती है";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "अधिसूचना आस -पास के पेशेवरों को भेजें";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "पेशेवर स्थान को रोकने के लिए पहुंचे";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "पेशेवर स्टॉप स्थान से सवारी शुरू करें";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "पेशेवर आपके बुकिंग स्थान पर पहुंचे";
notifyMessage.PROFESSIONAL_RIDE_START_INFO = "पेशेवर बुकिंग स्थान से शुरू हुआ";
notifyMessage.USER_NOT_FOUND = "उपयोगकर्ता नहीं मिला";
notifyMessage.USER_HAVE_RIDE = "उपयोगकर्ता के पास पहले से ही सवारी है";
notifyMessage.USER_RIDE_CANCEL_FAILD = "उपयोगकर्ता में कुछ गलत हो गया";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "कृपया अपने बटुए को रिचार्ज करें या कोई अन्य भुगतान विकल्प चुनें";
notifyMessage.USER_PAYMENT_DECLINED =
  "इस भुगतान पद्धति को अस्वीकार कर दिया गया है। कृपया एक और भुगतान विधि चुनें";
notifyMessage.INVALID_PROFESSIONAL = "अमान्य पेशेवर विवरण";
notifyMessage.INVALID_BOOKING = "अमान्य बुकिंग विवरण";
notifyMessage.INVALID_ACCESS = "अमान्य अभिगम";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "वाहन श्रेणी नहीं मिली";
notifyMessage.VEHICLE_NOT_AVAILABLE = "कोई वाहन उपलब्ध नहीं है";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "सेवा श्रेणी नहीं मिली";
notifyMessage.BOOKING_SCHEDULED_RIDE = "बुकिंग शेड्यूल्ड सफलतापूर्वक";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "बुकिंग श्रेणी नहीं मिली";
notifyMessage.BOOKING_ALREADY = "बुकिंग पहले से ही है";
notifyMessage.BOOKING_STATUS_IS = "बुकिंग है";
notifyMessage.BOOKING_STATUS_INFO =
  "पहले से ही प्रगति में बुकिंग या समाप्त हो गई";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "एक अन्य ड्राइवर के लिए ड्राइवर की जाँच द्वारा रद्द की गई बुकिंग";
notifyMessage.BOOKING_CANCELLED_BY = "द्वारा रद्द की गई बुकिंग";
notifyMessage.BOOKING_CANCELLED_BY_USER = "उपयोगकर्ता द्वारा रद्द की गई बुकिंग";
notifyMessage.BOOKING_CANCELLED = "बुकिंग सफलतापूर्वक रद्द कर दी जाती है";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "नई बुकिंग को व्यवस्थापक द्वारा सौंपा गया है";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "अनुरोध सफलतापूर्वक सौंपा गया";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "बुकिंग पहले से ही दूसरे ड्राइवर द्वारा स्वीकार की जाती है";
notifyMessage.BOOKING_UPDATE_FAILED = "कैंट अपडेट बुकिंग कुछ गलत हो गया";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "बुकिंग स्थिति अद्यतन में त्रुटि";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "रद्द करने का कारण नहीं मिला";
notifyMessage.SOMETHING_WENT_WRONG = "रद्द करने का कारण नहीं मिला";
notifyMessage.RIDE_CANCELLED = "सवारी सफलतापूर्वक रद्द कर दी गई";
notifyMessage.RIDE_IS = "यह सवारी है";
notifyMessage.RIDE_STATUS_INFO =
  "सवारी पहले से ही समाप्त हो गई है या अभी तक शुरू हुई है";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "सवारी स्थानों को बदलने में त्रुटि";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "सवारी का पता उपयोगकर्ता द्वारा बदल गया है";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "पेशेवर द्वारा सवारी का पता बदल दिया गया है";
notifyMessage.OTP_INCORRECT = "OTP गलत है कृपया सही ओटीपी दर्ज करें";
notifyMessage.RATING_SUBMITED = "रेटिंग सफलतापूर्वक प्रस्तुत की गई";
notifyMessage.RATING_UPDATE_FAILED = "रेटिंग अद्यतन में त्रुटि";
notifyMessage.LOCATION_ACCESS_FAILED = "आपके स्थान में कुछ गलत हुआ";
notifyMessage.TRIP_NOT_AVAILABLE = "टिप्स उपलब्ध नहीं है";
notifyMessage.TRIP_SHARED = "यात्रा सफलतापूर्वक साझा की गई";
notifyMessage.TRIP_END_PROFESSIONAL =
  "आपकी सवारी पेशेवर द्वारा समाप्त हो गई है";
notifyMessage.OPERATOR_STATUS_END =
  "हमारे ऑपरेटर ने आपकी यात्रा की स्थिति को समाप्त करने के लिए बदल दिया";
notifyMessage.ERROR_SECURITY_UPDATE = "सुरक्षा अद्यतन में त्रुटि";
notifyMessage.ERROR_GENEREL_SETTINGS = "सामान्य सेटिंग्स में त्रुटि";
notifyMessage.WISHLIST_ACTION_FAILED = "क्रिया {{कार्रवाई}} विशलिस्ट विफल है";
notifyMessage.SUR_CHARGE = "सुर -प्रभार";
notifyMessage.SERVICE_TAX = "सेवा कर";
notifyMessage.TIPS_AMOUNT = "युक्तियाँ राशि";
notifyMessage.COUPON_SAVINGS = "कूपन बचत";
notifyMessage.TOLL_FEE = "टोल मुक्त";
notifyMessage.DISCOUNT_APPLIED = "लागू किया गया छूट";
notifyMessage.REFUND_TO_WALLET = "बटुए को वापस करना";
notifyMessage.TIPS_MINIMUM = "टिप्स शूलूड न्यूनतम हो";
notifyMessage.TIPS_MAXIMUM = "टिप्स शूलूड अधिकतम हो";
notifyMessage.TIPS_ADDED = "टिप्स सफलतापूर्वक जोड़े गए";
notifyMessage.ERROR_PAYMENT_CARD =
  "कार्ड भुगतान में त्रुटि, कृपया कुछ समय बाद प्रयास करें";
notifyMessage.ERROR_EARNINGS = "कमाई में त्रुटि";
notifyMessage.ERROR_DAY_EARNINGS = "दिन की कमाई में त्रुटि";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "ऐसा लगता है कि पेशेवर बंद है। कृपया इस यात्रा को बुक करने से पहले पेशेवर को ऑनलाइन जाने के लिए कहें";
notifyMessage.INFO_PAYMENT_OPTIONS = "भुगतान विकल्प नहीं मिले";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT = "गेटवे इवेंट्स में कुछ गलत हो गया";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "खिचड़ी भाषा का पता लगाने के लिए उपयोगकर्ता कुछ गलत हो गया बटुए की राशि रिचार्ज";
notifyMessage.INFO_CHECK_CREDENTIAL = "कृपया अगली क्रेडेंशियल की जाँच करें";
notifyMessage.INFO_SEND_MONEY = "पैसा सफलतापूर्वक भेजा";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "सदस्यता के लिए भुगतान किया गया";
notifyMessage.INFO_TIPS = "आपने टिप्स दिए";
notifyMessage.INFO_JOINING_CHARGE = "जुड़ाव राशि";
notifyMessage.INFO_REFERREL_CHARGE = "संदर्भ राशि";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "शेष भुगतान राशि लोड की गई";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "शेष भुगतान राशि डेबिट की गई";
notifyMessage.WALLET_AMOUNT_LOADED =
  "राशि आपके बटुए में सफलतापूर्वक जोड़ी जाती है";
notifyMessage.WALLET_NOTIFICATION =
  "वॉलेट अपडेट में कुछ गलत हो गया बटुए में कोई प्रामाणिक नहीं";
notifyMessage.WALLET_WITHDRAW = "बटुए से राशि वापस लेना";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "बटुआ निकासी सफलतापूर्वक";
notifyMessage.WALLET_DEBIT = "बटुए राशि पर डेबिट";
notifyMessage.ERROR_WALLET_RECHARGE = "वॉलेट रिचार्ज करते समय त्रुटि हुई";
notifyMessage.ERROR_WALLET_WITHDRAW = "बटुए की निकासी में त्रुटि";
notifyMessage.ERROR_PAYMENT_OPTION = "भुगतान विकल्प सूची में त्रुटि";
notifyMessage.ERROR_SEND_MONEY = "पैसे भेजने में त्रुटि";
notifyMessage.BANK_CREDET_MONEY =
  "7 व्यावसायिक दिनों के भीतर Yout बैंक खाते के लिए धन का श्रेय होगा";
notifyMessage.AMOUT_SEND_TO_FRIEND = "मित्र को स्थानांतरित कर दिया गया";
notifyMessage.BOOKING_CHARGE_FREEZED = "बुकिंग के लिए फ्रीजेड राशि";
notifyMessage.RIDE_CANCELLATION_CHARGE = "सवारी रद्द राशि";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "लागू किए गए छूट के लिए धनवापसी";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "लेनदेन नहीं मिला";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "प्रतिक्रिया नहीं मिली है कि हम मान लें कि आप इस URL के एक अवैध उपयोगकर्ता हैं";
notifyMessage.INFO_CARD_ADDED = "कार्ड को सफलतापूर्वक जोड़ा जाता है";
notifyMessage.INFO_CARD_VERIFIED = "कार्ड को सफलतापूर्वक सत्यापित किया गया";
notifyMessage.INFO_BANK_DETAILS = "बैंक विवरण";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "लेन -देन की सफलता";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "खिचड़ी भाषा को बैंक विवरण मिला";
notifyMessage.INFO_CARD_DECLINED =
  "बटुए से कटौती की गई राशि। कार्ड में गिरावट आई";
notifyMessage.INFO_AMOUNT_PAID = "सफलतापूर्वक भुगतान की गई राशि";
notifyMessage.ERROR_CARD_ADD = "एक कार्ड नहीं जोड़ सकता";
notifyMessage.ERROR_CARD_ADD_FAILED = "कार्ड जोड़ने में कुछ गलत हुआ";
notifyMessage.ERROR_IN_TRANSACTIONS = "लेन -देन में त्रुटि";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "कार्ड भुगतान विफल";
notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "क्षमा करें, हम यहां सेवा प्रदान नहीं करते हैं";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "फ़ाइल सफलतापूर्वक अपलोड की गई";
notifyMessage.INFO_NOT_FOUND = "विवरण नहीं मिला";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "बिंदु को आपके बटुए में सफलतापूर्वक जोड़ा जाता है";
notifyMessage.ERROR_WALLET_POINT_ADDED = "बटुए बिंदु करने में सक्षम नहीं है";
notifyMessage.INFO_SUCCESS = "सफलता";
notifyMessage.INFO_FAILED = "असफल";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "इनाम अंक को भुनाएं";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "न्यूनतम इनाम बिंदुओं को भुनाएं";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "अधिकतम इनाम बिंदुओं को भुनाएं";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "हमारे ऑपरेटर ने आने के लिए आपकी यात्रा की स्थिति बदल दी";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "हमारे ऑपरेटर ने शुरू करने के लिए आपकी यात्रा की स्थिति बदल दी";
notifyMessage.INFO_RIDE_CANCELLED_BY = "सवारी द्वारा रद्द कर दिया है";
notifyMessage.INFO_PROFESSIONAL = "पेशेवर";
notifyMessage.INFO_USER = "उपयोगकर्ता";
notifyMessage.INFO_ADMIN = "व्यवस्थापक";
notifyMessage.INFO_INVALID_DETAILS = "अमान्य विवरण";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "यह फ़ोन नंबर पहले से ही कॉर्पोरेट नाम से पंजीकृत है";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "अमान्य रेफ़रल कोड, कृपया अपने कोड की जाँच करें और पुनः प्रयास करें";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "ईमेल आईडी पहले से पंजीकृत है";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "अमान्य जन्म तिथि MM-DD-YYYY या MM/DD/YYYY होनी चाहिए";
notifyMessage.INFO_INVALID_AGE =
  "आगे बढ़ने के लिए उम्र  {{minAgeKey}} और उससे ज्यादा होनी चाहिए";
notifyMessage.INFO_CARD_ALREADY_EXIST = "कार्ड पहले से मौजूद है";
notifyMessage.INFO_WALLET_RECHARGE = "वॉलेट रिचार्ज";
notifyMessage.INFO_INTERCITY_RIDE = "इंटरसिटी की सवारी";
notifyMessage.INFO_SITE_COMMISSION = "साइट आयोग";
//--------- Newly added -------------
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Ride Cancellation Credit";
notifyMessage.INFO_ROUNDING = "Rounding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONAL ALREADY HAVE RIDE";
notifyMessage.INFO_BASE_FARE = "Base Fare";
notifyMessage.INFO_SERVICE_TAX = "Service Tax";
notifyMessage.INFO_TRAVEL_CHARGE = "Travel Charge";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum Charge";
notifyMessage.INFO_SURCHARGE_FEE = "SurCharge Fee";
notifyMessage.INFO_BOUNDARY_CHARGE = "boundary Charge";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE = "Your {{siteTitle}} OTP code is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Earnings";
notifyMessage.INFO_FELLOW_PASSENGER = "FELLOW PASSENGER";
notifyMessage.INFO_ALREADY_EXISTS = "ALREADY EXIST";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Invalid Coupon Code, Please Check Your Code.";
notifyMessage.WALLET_RECHARGE_FAILED = "WALLET RECHARGE FAILED";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "YOUR VEHICLE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "YOUR PROFILE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Your Vehicle Documents will Expire in {{day}} Day(s). Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Your Profile Documents will Expire in {{day}} Day(s). Please Check And Update";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Assistance Care Fee";
notifyMessage.INFO_TIME_FARE = "Time Fare";
notifyMessage.INFO_DISTANCE_FARE = "Distance Fare";
notifyMessage.INFO_PEAK_FARE = "Peak Fare";
notifyMessage.INFO_NIGHT_FARE = "Night Fare";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oops! Connection is lost. You’re offline now. Please go online to take ride requests.";
notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your Rating is below the Range, Please improve, or your Account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Your account is activated. Please go online to take ride requests.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Card validation failed";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bank server Busy Now. Please try Later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Verified Successfully";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV Verification Failed";
notifyMessage.ALERT_CHECK_CPF = "Please Check CPF Number.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "Please Check From Date / To Date.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "City Already Exist.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF Number Already Exist.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maximum Withdraw Amount Limit is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Amount Withdrawal Allowed Once Per Day.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Account Holder Name";
notifyMessage.INFO_BANK_NAME = "Bank Name";
notifyMessage.INFO_DOCUMENT_NUMBER = "Document Number";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Wallet withdrawal failed. Amount credited to the wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No Service Location Is Available";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Your fellow passenger has arrived at the pickup location";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Your fellow passenger has started the ride";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "The ride has come to an end for your fellow passenger.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "This Email Is Already Associated With Another Account. Please Use A Different Email.";
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
