const notifyMessage = {};

notifyMessage.CREATED = "በተሳካ ሁኔታ ተፈጠረ";
notifyMessage.UPDATED = "በተሳካ ሁኔታ ዘምኗል";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "ልክ ያልሆነ የተጠቃሚ ውሂብ";
notifyMessage.TRIP_END = "ጉዞ ተጠናቅቋል";
notifyMessage.WALLET_LOADED = "የኪስ ቦርሳ መጠን ተጭኗል";
notifyMessage.CASH = "ጥሬ ገንዘብ";
notifyMessage.CARD = "ካርድ";
notifyMessage.WALLET = "Wallet";
notifyMessage.TRIP_FEE = "የጉዞ ክፍያ";
notifyMessage.TOTAL_BILL = "ጠቅላላ ሂሳብ";
notifyMessage.EARNINGS = "ገቢዎችዎ";
notifyMessage.WAITING_CHARGE = "ክፍያ";
notifyMessage.PENDING_AMOUNT = "በመጠባበቅ ላይ";
notifyMessage.CASH_IN_HAND = "በጥሬ ገንዘብ";
notifyMessage.BOOKING_ACCEPTED = "የእርስዎ ማስቀመጫ ተቀባይነት አለው";
notifyMessage.BOOKING_PAID = "ለመያዣ ገንዘብ ተከፍሏል";
notifyMessage.PROFESSIONAL_STARTED = "ባለሙያዎ ለጉዞዎ ተጀምሯል";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "አካባቢ በተሳካ ሁኔታ ተለው changed ል";
notifyMessage.PROFESSIONAL_ARRIVED = "ባለሙያ ወደ የመጫኛ ቦታዎ ደርሷል";
notifyMessage.PROFESSIONAL_COMMISSION = "የባለሙያ ኮሚሽን";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "ገቢዎች የተቀበሉ ገቢዎች";
notifyMessage.PROFESSIONAL_INFO = "በመስመር ላይ ለመሄድ የኪስ ቦርሳዎን በደግነት ያድጉ";
notifyMessage.PROFESSIONAL_NOT_FOUND = "ባለሙያ አልተገኘም";
notifyMessage.PROFESSIONAL_NEW_RIDE = "አዲስ ጉዞ አለዎት";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "በባለሙያ ተወሰደ";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING = "የእርስዎ ማስያዣ በባለሙያችን ተቀባይነት አግኝቷል";
notifyMessage.PROFESSIONAL_NOTIFICATION = "ማስታወቂያ በአቅራቢያዎ ወደሚገኝ ባለሙያዎች ይላኩ";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION = "ባለሙያ ቦታን ለማቆም መጣ";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION = "የባለሙያ ቦታውን ከማቆም ቦታ ይጀምራል";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION = "ባለሙያ ወደ ማቆያ ቦታዎ ደርሷል";
notifyMessage.PROFESSIONAL_RIDE_START_INFO = "ሙያዊ ከተያያዘ ቦታ ጀምሮ የተጀመረው ባለሙያ ነው";
notifyMessage.USER_NOT_FOUND = "ተጠቃሚ አልተገኘም";
notifyMessage.USER_HAVE_RIDE = "ተጠቃሚው ቀድሞውኑ ይሽከረከሩ";
notifyMessage.USER_RIDE_CANCEL_FAILD = "ይህንን ጉዞ በተጠቃሚው ውስጥ አንድ ነገር ተሳስቷል";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "የኪስ ቦርሳዎን በደግነት ይሙሉ ወይም ሌላ ማንኛውንም የክፍያ አማራጭ ይምረጡ";
notifyMessage.USER_PAYMENT_DECLINED =
  "ይህ የክፍያ ዘዴ ተቀባይነት አላገኘም. እባክዎን ሌላ የክፍያ ዘዴ ይምረጡ";
notifyMessage.INVALID_PROFESSIONAL = "ልክ ያልሆኑ የባለሙያ ዝርዝሮች";
notifyMessage.INVALID_BOOKING = "ልክ ያልሆኑ የመጫያ ዝርዝሮች";
notifyMessage.INVALID_ACCESS = "ልክ ያልሆነ መዳረሻ";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "የተሽከርካሪ ምድብ አልተገኘም";
notifyMessage.VEHICLE_NOT_AVAILABLE = "ምንም ተሽከርካሪ አይገኝም";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "የአገልግሎት ምድብ አልተገኘም";
notifyMessage.BOOKING_SCHEDULED_RIDE = "ቦታ ማስያዝ በተሳካ ሁኔታ መርሐግብር ተይዞለታል";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "ማስያዝ ምድብ አልተገኘም";
notifyMessage.BOOKING_ALREADY = "ማስያዝ ቀድሞውኑ ነው";
notifyMessage.BOOKING_STATUS_IS = "ማስያዝ ነው";
notifyMessage.BOOKING_STATUS_INFO = "ቀድሞውኑ በሂደት ላይ ያለ ቦታ ማስያዝ ወይም ተጠናቅቋል";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "ለሌላ ነጂ ለሌላ ነጂ በማጣራት በማሽከርከር ምልክት ተደርጎበታል";
notifyMessage.BOOKING_CANCELLED_BY = "ማስያዝ በ";
notifyMessage.BOOKING_CANCELLED_BY_USER = "ቦታ ማስያዣ በተጠቃሚ ተሰር ed ል";
notifyMessage.BOOKING_CANCELLED = "ማስያዣ በተሳካ ሁኔታ ተሰር is ል";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "አዲስ ቦታ ማስያዝ በአስተዳዳሪ ተመድቧል";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "ጥያቄ በተሳካ ሁኔታ ተመድቧል";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS = "ማስያዣው በሌላ አሽከርካሪ ተቀባይነት አግኝቷል";
notifyMessage.BOOKING_UPDATE_FAILED = "አንድ መጥፎ ነገር የተካሄደውን ማዘመኛ ዝርዝር መግለጫዎች";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "በሁኔታ ሁኔታ ዝመና ውስጥ ስህተት";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "ስረዛ ምክንያት አልተገኘም";
notifyMessage.SOMETHING_WENT_WRONG = "ስረዛ ምክንያት አልተገኘም";
notifyMessage.RIDE_CANCELLED = "ተሳክቷል";
notifyMessage.RIDE_IS = "ይህ ግልቢያ ነው";
notifyMessage.RIDE_STATUS_INFO = "ቀድሞውኑ ተጠናቅቋል ወይም አልተጀመረም";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "የመንገድ ቦታዎችን ለመለወጥ ስህተት";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER = "የመንዳት አድራሻ በአሜሪካ ተቀይሯል";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL = "የራይድ አድራሻ በባለሙያ ተለውጧል";
notifyMessage.OTP_INCORRECT = "OTP ትክክል ያልሆነው ትክክለኛ ኦቲፒን ያስገቡ";
notifyMessage.RATING_SUBMITED = "ደረጃ በተሳካ ሁኔታ የተቀመጠ ደረጃ";
notifyMessage.RATING_UPDATE_FAILED = "በደግነት ማዘመኛ ውስጥ ስህተት";
notifyMessage.LOCATION_ACCESS_FAILED = "የሆነ ነገር በአካባቢዎ ተሳስቷል";
notifyMessage.TRIP_NOT_AVAILABLE = "ጠቃሚ ምክሮች አይገኙም";
notifyMessage.TRIP_SHARED = "ጉዞ በተሳካ ሁኔታ ተካሄደ";
notifyMessage.TRIP_END_PROFESSIONAL = "የእርስዎ ጉዞ በባለሙያ ተጠናቅቋል";
notifyMessage.OPERATOR_STATUS_END = "ኦፕሬተርዎ እንዲቆም የጉዞዎን ሁኔታ ወደ መጨረሻው ቀይሯል";
notifyMessage.ERROR_SECURITY_UPDATE = "በደህንነት ማዘመኛ ውስጥ ስህተት";
notifyMessage.ERROR_GENEREL_SETTINGS = "በጄኔራል ቅንብሮች ውስጥ ስህተት";
notifyMessage.WISHLIST_ACTION_FAILED = "እርምጃ {{action}} ምኞት አልተሳካም";
notifyMessage.SUR_CHARGE = "Sups Sit";
notifyMessage.SERVICE_TAX = "የአገልግሎት ግብር";
notifyMessage.TIPS_AMOUNT = "ጠቃሚ ምክሮች";
notifyMessage.COUPON_SAVINGS = "የኩፖን ቁጠባዎች";
notifyMessage.TOLL_FEE = "ቶል ክፍያ";
notifyMessage.DISCOUNT_APPLIED = "ቅናሽ ተተግብሯል";
notifyMessage.REFUND_TO_WALLET = "ወደ Wallet ተመላሽ ያድርጉ";
notifyMessage.TIPS_MINIMUM = "ጠቃሚ ምክሮች በትንሹ ይሁኑ";
notifyMessage.TIPS_MAXIMUM = "ጠቃሚ ምክሮች ሱሱሩ ከፍተኛው";
notifyMessage.TIPS_ADDED = "ጠቃሚ ምክሮች በተሳካ ሁኔታ ታክለዋል";
notifyMessage.ERROR_PAYMENT_CARD = "በካርድ ክፍያ ውስጥ ስህተት, ከተወሰነ ጊዜ በኋላ በደግነት ይሞክሩ";
notifyMessage.ERROR_EARNINGS = "በገቢዎች ውስጥ ስህተት";
notifyMessage.ERROR_DAY_EARNINGS = "በቀን ገቢዎች ስህተት";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "ሙያዊው ብልሹነት የሚመስለው ይመስላል. እባክዎን ከዚህ ጉዞ ከመግባትዎ በፊት እባክዎን ባለሙያውን በመስመር ላይ እንዲሄዱ ይጠይቁ";
notifyMessage.INFO_PAYMENT_OPTIONS = "የክፍያ አማራጮች አልተገኙም";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT = "አንድ ነገር በደረጃ ዝግጅቶች ላይ ተሳክቷል";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "Walt ተጠቃሚን ማግኘት አልተቻለም በኪስ ቦርሳ መጠን መሙላት ስህተት ነው";
notifyMessage.INFO_CHECK_CREDENTIAL = "እባክዎ የሚቀጥለውን መረጃ ያረጋግጡ";
notifyMessage.INFO_SEND_MONEY = "ገንዘብ በተሳካ ሁኔታ ተልኳል";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "ለደንበኝነት ምዝገባ የተከፈለው";
notifyMessage.INFO_TIPS = "ምክሮችን ሰጡ";
notifyMessage.INFO_JOINING_CHARGE = "መጠናቀፍ";
notifyMessage.INFO_REFERREL_CHARGE = "የጥበብ መጠን";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "ቀሪ የክፍያ መጠን ተጭኗል";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "ቀሪ የክፍያ ሂሳብ ተከራክሯል";
notifyMessage.WALLET_AMOUNT_LOADED = "ገንዘብ በተሳካ ሁኔታ በተሳካ ሁኔታ ወደ Wallet ታክሏል";
notifyMessage.WALLET_NOTIFICATION = "በኪስ ቦርሳ ዝመና ውስጥ የሆነ ነገር አልተሳካም";
notifyMessage.WALLET_WITHDRAW = "ከኪስ ቦርሳ ማውጣት";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "ቦርሳ በተሳካ ሁኔታ ማውጣት";
notifyMessage.WALLET_DEBIT = "የኪስ ቦርሳ መጠን ተከራክሯል";
notifyMessage.ERROR_WALLET_RECHARGE = "ቦርሳ መሙላት በሚከሰትበት ጊዜ ስህተት ተገኝቷል";
notifyMessage.ERROR_WALLET_WITHDRAW = "በኪስ ቦርሳ ማውጣት ስህተት";
notifyMessage.ERROR_PAYMENT_OPTION = "በክፍያ አማራጭ ዝርዝር ውስጥ ስህተት";
notifyMessage.ERROR_SEND_MONEY = "ገንዘብ ለመላክ ስህተት";
notifyMessage.BANK_CREDET_MONEY = "ገንዘብ ከ 7 የሥራ ቀናት ውስጥ ለዩቲ ባንክ ሂሳብ ይመዝናል";
notifyMessage.AMOUT_SEND_TO_FRIEND = "ለጓደኛ ተላለፈ";
notifyMessage.BOOKING_CHARGE_FREEZED = "ለማብራራት የተዘበራረቀ መጠን";
notifyMessage.RIDE_CANCELLATION_CHARGE = "መሽከርከር መጠን መጠን";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "ቅናሽ ለተተገበረ ቅናሽ";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "ግብይት አልተገኘም";
notifyMessage.INFO_RESPONSE_NOT_FOUND = "ምላሽ አልተገኘም ለዚህ ዩአርኤል ህገ-ወጥ ተጠቃሚ ነዎት";
notifyMessage.INFO_CARD_ADDED = "ካርድ በተሳካ ሁኔታ ታክሏል";
notifyMessage.INFO_CARD_VERIFIED = "ካርድ በተሳካ ሁኔታ ተረጋግ .ል";
notifyMessage.INFO_BANK_DETAILS = "የባንክ ዝርዝሮች";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "ግብይቶች ስኬት";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "የባንክ ዝርዝሮችን አገኘ";
notifyMessage.INFO_CARD_DECLINED = "ከኪስ ቦርሳ የተቆረጠ መጠን. በካርድ ምክንያት ተቀባይነት አላገኘም";
notifyMessage.INFO_AMOUNT_PAID = "መጠን በተሳካ ሁኔታ የተከፈለ";
notifyMessage.ERROR_CARD_ADD = "ካርድ ማከል አልተቻለም";
notifyMessage.ERROR_CARD_ADD_FAILED = "የሆነ ነገር በካርድ ላይ አንድ ችግር ተፈጥሯል";
notifyMessage.ERROR_IN_TRANSACTIONS = "በግብይቶች ውስጥ ስህተት";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "የካርድ ክፍያ አልተሳካም";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "ይቅርታ እዚህ አገልግሎት አንሰጥም";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "ፋይል በተሳካ ሁኔታ ተሰቅሏል";
notifyMessage.INFO_NOT_FOUND = "ዝርዝሮች አልተገኙም";
notifyMessage.INFO_WALLET_POINT_LOADED = "ነጥብ በተሳካ ሁኔታ በኪስ ቦርሳዎ ውስጥ ታክሏል";
notifyMessage.ERROR_WALLET_POINT_ADDED = "የ Wallet ነጥቦችን የማትችል";
notifyMessage.INFO_SUCCESS = "ስኬት";
notifyMessage.INFO_FAILED = "አልተሳካም";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "የሽልማት ነጥቦችን ይቤዣቸዋል";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "አነስተኛ የሽልማት ነጥቦችን ይቤዣቸዋል";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "ከፍተኛ የሽልማት ነጥቦችን ይቤዥ";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED = "ኦፕሬተርዎ ለመድረስ የጉዞ ሁኔታዎን ወደቀ";
notifyMessage.INFO_OPERATOR_STATUS_START = "ኦፕሬተሩ ለመጀመር የጉዞ ሁኔታዎን ቀይሮታል";
notifyMessage.INFO_RIDE_CANCELLED_BY = "ማሽከርከር በ";
notifyMessage.INFO_PROFESSIONAL = "ባለሙያ";
notifyMessage.INFO_USER = "ተጠቃሚ";
notifyMessage.INFO_ADMIN = "አስተዳዳሪ";
notifyMessage.INFO_INVALID_DETAILS = "ልክ ያልሆኑ ዝርዝሮች";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "ይህ ስልክ ቁጥር ቀድሞውኑ በኮርፖሬሽን ስም ተመዝግቧል";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "ልክ ያልሆነ ሪፈራል ኮድ, እባክዎ ኮድዎን ያረጋግጡ እና እንደገና ይሞክሩ";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "የኢሜል መታወቂያ አስቀድሞ ተመዝግቧል";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "ልክ ያልሆነ DOB ቅርጸት MM-DD-yyyy ወይም MM / DD / yyyy መሆን አለበት";
notifyMessage.INFO_INVALID_AGE = "ዕድሜው {{minAgeKey}} እና ከዚያ በላይ";
notifyMessage.INFO_CARD_ALREADY_EXIST = "ካርድ ቀድሞውኑ አለ";
notifyMessage.INFO_WALLET_RECHARGE = "የኪስ ቦርሳ መሙላት";
notifyMessage.INFO_INTERCITY_RIDE = "መለካት";
notifyMessage.INFO_SITE_COMMISSION = "የጣቢያ ኮሚሽን";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "የመኪና ስረዛ ብድር";
notifyMessage.INFO_ROUNDING = "ዙር";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "ሙያዊው ቀድሞውኑ ይሽከረከራሉ";
notifyMessage.INFO_BASE_FARE = "የመሠረት ክፍያ";
notifyMessage.INFO_SERVICE_TAX = "የአገልግሎት ግብር";
notifyMessage.INFO_TRAVEL_CHARGE = "የጉዞ ክፍያ";
notifyMessage.INFO_MINIMUM_CHARGE = "አነስተኛ ክፍያ";
notifyMessage.INFO_SURCHARGE_FEE = "የመሙያ ክፍያ";
notifyMessage.INFO_BOUNDARY_CHARGE = "ድንበር ክስ";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "የመጫወቻ ክፍል የመጫኛ ክፍያ";
notifyMessage.INFO_OTP_MESSAGE = "{{siteTitle}} OTP ኮድ {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "የእርስዎ መለያ የተቆለፈ የተሳሳተ የምዝግብ ማስታወሻ ሙከራዎች ብዛት ደርሰዋል ምክንያቱም.";
notifyMessage.INFO_HUBS_EARNINGS = "የማዕድን ገቢዎች";
notifyMessage.INFO_FELLOW_PASSENGER = "ተፋሰስ";
notifyMessage.INFO_ALREADY_EXISTS = "አስቀድሞ አለ።";
notifyMessage.INFO_INVALID_COUPON_CODE = "ልክ ያልሆነ የኩፖን ኮድ፣ እባክዎን ኮድዎን ያረጋግጡ።";
notifyMessage.WALLET_RECHARGE_FAILED = "የኪስ ቦርሳ መሙላት አልተሳካም።";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "የተሽከርካሪ ሰነዶችዎ ጊዜው አልፎባቸዋል። እባክዎ ይፈትሹ እና ያዘምኑ";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "የመገለጫ ሰነዶችዎ ጊዜያቸው አልፎባቸዋል። እባክዎ ይፈትሹ እና ያዘምኑ";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "የተሽከርካሪ ሰነዶችዎ በ{{days}} ቀን(ዎች) ውስጥ የአገልግሎት ጊዜው ያበቃል። እባክዎ ያረጋግጡ እና ያዘምኑ";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "የመገለጫ ሰነዶችዎ በ{{days}} ቀን(ዎች) ውስጥ የአገልግሎት ጊዜው ያበቃል። እባክዎ ያረጋግጡ እና ያዘምኑ";
notifyMessage.INFO_INCENTIVE_CREDIT = "ማበረታቻ ተሰጥቷል።";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "የእርዳታ እንክብካቤ ክፍያ";
notifyMessage.INFO_TIME_FARE = "የጊዜ ዋጋ";
notifyMessage.INFO_DISTANCE_FARE = "የርቀት ዋጋ";
notifyMessage.INFO_PEAK_FARE = "ከፍተኛ ዋጋ";
notifyMessage.INFO_NIGHT_FARE = "የምሽት ዋጋ";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "ውይ! ግንኙነት ጠፍቷል። አሁን ከመስመር ውጭ ነዎት። የማሽከርከር ጥያቄዎችን ለመውሰድ እባክዎ መስመር ላይ ይሂዱ።";
notifyMessage.AIRPORT_FEE = "የአየር ማረፊያ ክፍያ";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "ይህ ባለሙያ የተመረጠውን የክፍያ አማራጭ አይቀበልም። እባክዎ የክፍያ አማራጩን ይቀይሩ።";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "የእርስዎ ደረጃ ከክልሉ በታች ነው፣ እባክዎ ያሻሽሉ፣ አለበለዚያ መለያዎ በጥቂት ቀናት ውስጥ ይታገዳል።";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "የእርስዎ ደረጃ ከክልል በታች ስለሆነ መለያዎ ታግዷል። መለያው በ{{date}} እኩለ ሌሊት ላይ እንደገና ይሠራል።";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "መለያህ ነቅቷል። እባክዎን የማሽከርከር ጥያቄዎችን ለመውሰድ መስመር ላይ ይሂዱ።";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "ከጓደኛ የተላለፈ መጠን";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "የካርድ ማረጋገጫ አልተሳካም።";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "የባንክ አገልጋይ አሁን ስራ ላይ ነው። እባክዎ ቆይተው ይሞክሩ";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV በተሳካ ሁኔታ ተረጋግጧል";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV ማረጋገጥ አልተሳካም።";
notifyMessage.ALERT_CHECK_CPF = "እባክዎ የሲፒኤፍ ቁጥሩን ያረጋግጡ።";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "እባክዎን ከቀን / እስከ ቀን ይመልከቱ።";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "ከተማ ቀድሞውንም አለ።";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "የሲፒኤፍ ቁጥር አስቀድሞ አለ።";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "ከፍተኛው የመውጣት መጠን ገደብ {{amount}} ነው";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT = "በቀን አንድ ጊዜ የሚፈቀደው የገንዘብ መጠን።";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "መለያ ያዥ ስም";
notifyMessage.INFO_BANK_NAME = "የባንክ ስም";
notifyMessage.INFO_DOCUMENT_NUMBER = "የሰነድ ቁጥር";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "የኪስ ቦርሳ ማውጣት አልተሳካም። ለኪስ ቦርሳ የተገባው መጠን።";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE = "ምንም የአገልግሎት ቦታ የለም";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED = "አብሮህ ተሳፋሪ በሚነሳበት ቦታ ደርሷል";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED = "አብሮህ ተሳፋሪ ጉዞውን ጀምሯል።";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED = "ለባልንጀራህ ተሳፋሪ ጉዞው አብቅቷል።";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "ይህ ኢሜይል አስቀድሞ ከሌላ መለያ ጋር የተቆራኘ ነው። እባክዎ የተለየ ኢሜይል ይጠቀሙ።";
notifyMessage.INFO_TRIP_NUMBER = "የጉዞ ቁጥር";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS = "የአገልግሎት ቦታ አስቀድሞ አለ።";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "ኮርፖሬት አስቀድሞ አለ።";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS = "የአገልግሎት ክልል አስቀድሞ አለ።";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "የሰነድዎ ማረጋገጫ በግምገማ ላይ፣ እባክዎ ማረጋገጫው እስኪጠናቀቅ ድረስ ይጠብቁ።";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
