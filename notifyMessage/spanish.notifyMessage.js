const notifyMessage = {};

notifyMessage.CREATED = "Creado con éxito";
notifyMessage.UPDATED = "Actualizado con éxito";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Datos de usuario no válidos";
notifyMessage.TRIP_END = "El viaje ha terminado";
notifyMessage.WALLET_LOADED = "Cantidad de la billetera cargada";
notifyMessage.CASH = "Dinero";
notifyMessage.CARD = "Tarjeta";
notifyMessage.WALLET = "Billetera";
notifyMessage.TRIP_FEE = "Tarifa de viaje";
notifyMessage.TOTAL_BILL = "Cuenta total";
notifyMessage.EARNINGS = "Tus ganancias";
notifyMessage.WAITING_CHARGE = "Cargo de espera";
notifyMessage.PENDING_AMOUNT = "Monto pendiente";
notifyMessage.CASH_IN_HAND = "Dinero en efectivo";

notifyMessage.BOOKING_ACCEPTED = "Tu reserva es aceptada";
notifyMessage.BOOKING_PAID = "Pagado por la reserva";

notifyMessage.PROFESSIONAL_STARTED = "El profesional comenzó a su viaje";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "La ubicación cambió con éxito";
notifyMessage.PROFESSIONAL_ARRIVED =
  "El profesional llegó a su ubicación de recogida";

notifyMessage.PROFESSIONAL_COMMISSION = "Comisión profesional";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Reserva de ganancias recibidas";

notifyMessage.PROFESSIONAL_INFO =
  "El saldo de su billetera está por debajo del límite mínimo requerido para aceptar viajes en efectivo. Recargue su billetera o solo puede aceptar viajes con tarjeta y billetera hasta que su saldo sea suficiente";
notifyMessage.PROFESSIONAL_NOT_FOUND = "Profesional no encontrado";
notifyMessage.PROFESSIONAL_NEW_RIDE = "Profesional no encontrado";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Viaje cancelado por profesional";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Su reserva es aceptada por nuestro profesional";

notifyMessage.PROFESSIONAL_NOTIFICATION =
  "Notificación Enviar a profesionales cercanos";

notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "El profesional llegó a la ubicación de detener";

notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Professional Comience el viaje desde la ubicación de la parada";

notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "El profesional llegó a su ubicación de reserva";

notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "El profesional comenzó desde la ubicación de la reserva";

notifyMessage.USER_NOT_FOUND = "USUARIO NO ENCONTRADO";
notifyMessage.USER_HAVE_RIDE = "El usuario ya tiene viaje";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "Algo salió mal en el usuario Cancelar este viaje";

notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Recarga amablemente su billetera o elija cualquier otra opción de pago";

notifyMessage.USER_PAYMENT_DECLINED =
  "Este método de pago ha sido rechazado. Elija otro método de pago";

notifyMessage.INVALID_PROFESSIONAL = "Detalles profesionales no válidos";
notifyMessage.INVALID_BOOKING = "Detalles de reservas no válidos";
notifyMessage.INVALID_ACCESS = "ACCESO INVALIDO";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND =
  "Categoría de vehículos no se encuentra";
notifyMessage.VEHICLE_NOT_AVAILABLE = "No hay vehículo disponible";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND =
  "Categoría de servicio no encontrada";

notifyMessage.BOOKING_SCHEDULED_RIDE = "Reserva programada con éxito";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "Categoría de reserva no encontrada";
notifyMessage.BOOKING_ALREADY = "La reserva ya es";
notifyMessage.BOOKING_STATUS_IS = "La reserva es";
notifyMessage.BOOKING_STATUS_INFO = "Reserva ya en progreso o terminado";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Reserva cancelada por la verificación del conductor para otro conductor";

notifyMessage.BOOKING_CANCELLED_BY = "Reserva cancelada por";
notifyMessage.BOOKING_CANCELLED_BY_USER = "Reserva cancelada por el usuario";
notifyMessage.BOOKING_CANCELLED = "La reserva se cancela con éxito";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "Admin asigna una nueva reserva";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Solicitud asignada correctamente";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "La reserva ya es aceptada por otro conductor";

notifyMessage.BOOKING_UPDATE_FAILED =
  "No puedo actualizar los detalles de la reserva que algo salió mal";

notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Error en la actualización del estado de reserva";

notifyMessage.CANCELLATION_REASON_NOT_FOUND =
  "Razón de cancelación no encontrada";

notifyMessage.SOMETHING_WENT_WRONG = "Razón de cancelación no encontrada";

notifyMessage.RIDE_CANCELLED = "Viaje cancelado con éxito";
notifyMessage.RIDE_IS = "Este viaje es";
notifyMessage.RIDE_STATUS_INFO = "El paseo ya terminó o no comenzó todavía";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "Error al cambiar las ubicaciones de conducción";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "La dirección de viaje ha cambiado por el usuario";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "La dirección del viaje ha cambiado por un profesional";
notifyMessage.OTP_INCORRECT = "OTP es incorrecto, ingrese a OTP correcto";

notifyMessage.RATING_SUBMITED = "Calificación presentada con éxito";
notifyMessage.RATING_UPDATE_FAILED =
  "Error en la actualización de calificación";

notifyMessage.LOCATION_ACCESS_FAILED = "Algo salió mal en tu ubicación";

notifyMessage.TRIP_NOT_AVAILABLE = "Consejos no está disponible";
notifyMessage.TRIP_SHARED = "Viaje compartido con éxito";
notifyMessage.TRIP_END_PROFESSIONAL = "Tu viaje ha terminado por profesional";

notifyMessage.OPERATOR_STATUS_END =
  "Nuestro operador cambió el estado de su viaje al final";

notifyMessage.ERROR_SECURITY_UPDATE = "Error en la actualización de seguridad";
notifyMessage.ERROR_GENEREL_SETTINGS = "Error en la configuración de Generel";

notifyMessage.WISHLIST_ACTION_FAILED =
  "Acción {{Action}} Wishlist está fallado";

notifyMessage.SUR_CHARGE = "CARGA DE SUR";
notifyMessage.SERVICE_TAX = "Impuesto de servicio";
notifyMessage.TIPS_AMOUNT = "Cantidad de consejos";
notifyMessage.COUPON_SAVINGS = "Ahorro de cupón";
notifyMessage.TOLL_FEE = "Peaje";
notifyMessage.DISCOUNT_APPLIED = "Descuento aplicado";
notifyMessage.REFUND_TO_WALLET = "Reembolso a la billetera";

notifyMessage.TIPS_MINIMUM = "Los consejos son mínimos de";
notifyMessage.TIPS_MAXIMUM = "Consejos que sean máximos de ser máximos";
notifyMessage.TIPS_ADDED = "Consejos agregados con éxito";

notifyMessage.ERROR_PAYMENT_CARD =
  "Error en el pago de la tarjeta, intente por favor después de algún tiempo";

notifyMessage.ERROR_EARNINGS = "Error en las ganancias";
notifyMessage.ERROR_DAY_EARNINGS = "Error en las ganancias diarias";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "Parece que el profesional es ofiline. Por favor, solicite al profesional que se conecte antes de reservar este viaje.";

notifyMessage.INFO_PAYMENT_OPTIONS = "Opciones de pago no encontradas";
notifyMessage.INFO_CARD = "No puedo encontrar los detalles de la tarjeta";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Algo salió mal en los eventos de Gateway, verifique con cuidado paso a paso";

notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "No puedo encontrar el usuario que algo salió mal en la cantidad de billetera recarga";

notifyMessage.INFO_CHECK_CREDENTIAL = "Por favor revise la próxima credencial";
notifyMessage.INFO_SEND_MONEY = "Dinero enviado con éxito";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Pagado por la suscripción";
notifyMessage.INFO_TIPS = "Diste consejos";
notifyMessage.INFO_JOINING_CHARGE = "Cantidad de unión";
notifyMessage.INFO_REFERREL_CHARGE = "Cantidad de referencia";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Monto de pago restante cargado";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Monto de pago restante debitado";

notifyMessage.WALLET_AMOUNT_LOADED =
  "La cantidad se agrega con éxito a su billetera";

notifyMessage.WALLET_NOTIFICATION =
  "Algo salió mal en la actualización de la billetera en la billetera.";

notifyMessage.WALLET_WITHDRAW = "Monto retirarse de la billetera";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Retiro de billetera con éxito";
notifyMessage.WALLET_DEBIT = "Cantidad de billetera debitado";

notifyMessage.ERROR_WALLET_RECHARGE =
  "Se produjo un error mientras se recarga la billetera";
notifyMessage.ERROR_WALLET_WITHDRAW = "Error en la retirada de la billetera";
notifyMessage.ERROR_PAYMENT_OPTION = "Error en la lista de opciones de pago";
notifyMessage.ERROR_SEND_MONEY = "Error en enviar dinero";

notifyMessage.BANK_CREDET_MONEY =
  "El dinero será el crédito a la cuenta bancaria de YouT dentro de los 7 días hábiles";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Transferido a amigo";

notifyMessage.BOOKING_CHARGE_FREEZED = "Cantidad congelada para reservar";

notifyMessage.RIDE_CANCELLATION_CHARGE = "Monto de cancelación";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "Reembolso por descuento aplicado";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "No se encuentra la transacción";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "La respuesta no se encuentra que asumimos que es un usuario ilegal de esta URL";

notifyMessage.INFO_CARD_ADDED = "La tarjeta se agrega con éxito";
notifyMessage.INFO_CARD_VERIFIED = "Tarjeta verificada con éxito";
notifyMessage.INFO_BANK_DETAILS = "DETALLES DEL BANCO";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Éxito de transacciones";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND =
  "No puedo encontrar los datos bancarios";
notifyMessage.INFO_CARD_DECLINED =
  "Cantidad deducida de la billetera. Debido a la tarjeta disminuyó";

notifyMessage.INFO_AMOUNT_PAID = "Monto pagado con éxito";

notifyMessage.ERROR_CARD_ADD = "No se puede agregar una tarjeta";
notifyMessage.ERROR_CARD_ADD_FAILED = "Algo salió mal en la tarjeta agregando";
notifyMessage.ERROR_IN_TRANSACTIONS = "Error en las transacciones";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "El pago de la tarjeta falló";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "Lo siento, no brindamos servicio aquí";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Documento cargado exitosamente";
notifyMessage.INFO_NOT_FOUND = "Detalles no encontrados";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "El punto se agrega con éxito a su billetera";

notifyMessage.ERROR_WALLET_POINT_ADDED =
  "No capaz de hacer el punto de la billetera";
notifyMessage.INFO_SUCCESS = "ÉXITO";
notifyMessage.INFO_FAILED = "FALLIDO";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "Canjear puntos de recompensa";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Canjear puntos de recompensa mínimos";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Canjear puntos de recompensa máximos";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Nuestro operador cambió el estado de su viaje a la llegada";

notifyMessage.INFO_OPERATOR_STATUS_START =
  "Nuestro operador cambió el estado de su viaje para comenzar";

notifyMessage.INFO_RIDE_CANCELLED_BY = "El paseo se ha cancelado por";
notifyMessage.INFO_PROFESSIONAL = "PROFESIONAL";
notifyMessage.INFO_USER = "USUARIO";
notifyMessage.INFO_ADMIN = "ADMINISTRACIÓN";
notifyMessage.INFO_INVALID_DETAILS = "Detalles no válidos";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Este número de teléfono ya está registrado en nombre corporativo";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "Código de referencia no válido, consulte su código e intente nuevamente";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS =
  "ID de correo electrónico ya registrado";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "El formato de DOB no válido debe ser MM-DD-YYYY o MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE =
  "La edad debe ser {{minAgeKey}} y arriba para proceder";
notifyMessage.INFO_CARD_ALREADY_EXIST = "La tarjeta ya existe";
notifyMessage.INFO_WALLET_RECHARGE = "Recarga de billetera";
notifyMessage.INFO_INTERCITY_RIDE = "Paseo interurbano";
notifyMessage.INFO_SITE_COMMISSION = "Comisión del sitio";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Crédito de cancelación de viaje";
notifyMessage.INFO_ROUNDING = "Redondeo";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESIONAL YA TENGO VIAJE";
notifyMessage.INFO_BASE_FARE = "Tarifa básica";
notifyMessage.INFO_SERVICE_TAX = "Impuesto de servicio";
notifyMessage.INFO_TRAVEL_CHARGE = "Cargo por viaje";
notifyMessage.INFO_MINIMUM_CHARGE = "Cargo mínimo";
notifyMessage.INFO_SURCHARGE_FEE = "Tarifa de recargo";
notifyMessage.INFO_BOUNDARY_CHARGE = "carga límite";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Cargo por recogida interurbana";
notifyMessage.INFO_OTP_MESSAGE = "Su código OTP de {{siteTitle}} es {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Su cuenta ha sido bloqueada porque ha alcanzado el número máximo de intentos de inicio de sesión no válidos.";
notifyMessage.INFO_HUBS_EARNINGS = "Ganancias de los centros";
notifyMessage.INFO_FELLOW_PASSENGER = "COMPAÑERO DE PASAJE";
notifyMessage.INFO_ALREADY_EXISTS = "YA EXISTE";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Código de cupón no válido, verifique su código.";
notifyMessage.WALLET_RECHARGE_FAILED = "RECARGA DE BILLETERA FALLÓ";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "LA DOCUMENTACIÓN DE SU VEHÍCULO ESTÁ VENCIDA. POR FAVOR, CONSULTE Y ACTUALICE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "LOS DOCUMENTOS DE SU PERFIL ESTÁN VENCIDOS. POR FAVOR, VERIFIQUE Y ACTUALICE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Los documentos de su vehículo vencerán en {{days}} día(s). Por favor, verifique y actualice";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Los documentos de su perfil vencerán en {{days}} día(s). Por favor, compruébelos y actualícelos";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentivo acreditado";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Tarifa de atención de asistencia";
notifyMessage.INFO_TIME_FARE = "Tarifa horaria";
notifyMessage.INFO_DISTANCE_FARE = "Tarifa de distancia";
notifyMessage.INFO_PEAK_FARE = "Tarifa pico";
notifyMessage.INFO_NIGHT_FARE = "Tarifa nocturna";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "¡Ups! Se perdió la conexión. Ahora estás desconectado. Conéctate para recibir solicitudes de viajes.";
notifyMessage.AIRPORT_FEE = "Tarifa de aeropuerto";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Este profesional no acepta la opción de pago seleccionada. Por favor cambie la opción de pago";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Su calificación está por debajo del rango, mejórela o su cuenta será bloqueada en pocos días.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Su cuenta ha sido bloqueada porque su calificación está por debajo del rango. La cuenta será reactivada el {{date}} a medianoche";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Su cuenta está activada. Conéctese a Internet para recibir solicitudes de viajes.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Cantidad transferida de amigo";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "La validación de la tarjeta falló";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "El servidor del banco está ocupado ahora. Inténtelo más tarde.";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV verificado exitosamente";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Verificación CVV fallida";
notifyMessage.ALERT_CHECK_CPF = "Por favor verifique el número CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Por favor, verifique desde fecha / hasta fecha.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "La ciudad ya existe.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "El número CPF ya existe.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "El límite máximo de retiro es {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Retiro de monto permitido una vez al día.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Nombre del titular de la cuenta";
notifyMessage.INFO_BANK_NAME = "Nombre del banco";
notifyMessage.INFO_DOCUMENT_NUMBER = "Número de documento";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Error en el retiro de la billetera. Importe acreditado en la billetera.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No hay ninguna ubicación de servicio disponible";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Su compañero de viaje ha llegado al lugar de recogida.";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Tu compañero de viaje ha comenzado el viaje.";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "El viaje ha llegado a su fin para tu compañero de viaje.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Este correo electrónico ya está asociado a otra cuenta. Utilice un correo electrónico diferente.";
notifyMessage.INFO_TRIP_NUMBER = "Número de viaje";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "La ubicación del servicio ya existe.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "La corporación ya existe.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "El área de servicio ya existe.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Su verificación de documento está bajo revisión. Espere hasta que se complete la verificación.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
