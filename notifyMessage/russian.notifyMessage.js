const notifyMessage = {};

notifyMessage.CREATED = "Создан успешно";
notifyMessage.UPDATED = "Успешно Обновлено";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Неверные пользовательские данные";
notifyMessage.TRIP_END = "Поездка была закончена";
notifyMessage.WALLET_LOADED = "Сумма кошелька загружено";
notifyMessage.CASH = "Наличные";
notifyMessage.CARD = "Карта";
notifyMessage.WALLET = "Кошелек";
notifyMessage.TRIP_FEE = "Плата за поездку";
notifyMessage.TOTAL_BILL = "Общий счет";
notifyMessage.EARNINGS = "Ваш заработок";
notifyMessage.WAITING_CHARGE = "Заряд ожидания";
notifyMessage.PENDING_AMOUNT = "В ожидании суммы";
notifyMessage.CASH_IN_HAND = "Деньги в руки";

notifyMessage.BOOKING_ACCEPTED = "Ваше бронирование принимается";
notifyMessage.BOOKING_PAID = "Заплатил за бронирование";

notifyMessage.PROFESSIONAL_STARTED = "Профессионал начал свою поездку";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "Место успешно изменилось";
notifyMessage.PROFESSIONAL_ARRIVED = "Профессионал прибыл в ваше место питания";

notifyMessage.PROFESSIONAL_COMMISSION = "Профессиональная комиссия";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED =
  "Забронирование доходов получено";

notifyMessage.PROFESSIONAL_INFO =
  "Баланс вашего кошелька стал ниже минимального предела, необходимого для оплаты проезда наличными. Пожалуйста, пополните свой кошелек, или вы можете принимать поездки только по карте и кошельку, пока ваш баланс не станет достаточным.";
notifyMessage.PROFESSIONAL_NOT_FOUND = "Профессионал не найден";
notifyMessage.PROFESSIONAL_NEW_RIDE = "Профессионал не найден";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Езда отменена профессионалами";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Ваше бронирование принимается нашим профессионалом";

notifyMessage.PROFESSIONAL_NOTIFICATION =
  "Уведомление отправить ближайшим профессионалам";

notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "Профессионал прибыл, чтобы остановить место";

notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Профессиональный начинай поездку с места остановки";

notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "Профессионал прибыл в ваше место бронирования";

notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "Профессионал начал с места бронирования";

notifyMessage.USER_NOT_FOUND = "ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН";
notifyMessage.USER_HAVE_RIDE = "У пользователя уже есть езда";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "Что -то пошло не так в пользователе отменить эту поездку";

notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Пожалуйста, перезарядите свой кошелек или выберите любой другой вариант оплаты";

notifyMessage.USER_PAYMENT_DECLINED =
  "Этот способ оплаты был отклонен. Пожалуйста, выберите другой способ оплаты";

notifyMessage.INVALID_PROFESSIONAL = "Неверные профессиональные детали";
notifyMessage.INVALID_BOOKING = "Неверные детали бронирования";
notifyMessage.INVALID_ACCESS = "Недействительный доступ";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND =
  "Категория транспортных средств не найдена";
notifyMessage.VEHICLE_NOT_AVAILABLE = "Транспортное средство не доступно";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "Категория обслуживания не найдена";

notifyMessage.BOOKING_SCHEDULED_RIDE = "Бронирование запланировано успешно";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "Категория бронирования не найдена";
notifyMessage.BOOKING_ALREADY = "Бронирование уже";
notifyMessage.BOOKING_STATUS_IS = "Бронирование есть";
notifyMessage.BOOKING_STATUS_INFO =
  "Бронирование уже в процессе или закончилось";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Бронирование отменена при проверке водителя для другого водителя";

notifyMessage.BOOKING_CANCELLED_BY = "Бронирование отменено";
notifyMessage.BOOKING_CANCELLED_BY_USER = "Бронирование отменено пользователем";
notifyMessage.BOOKING_CANCELLED = "Бронирование отменено успешно";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "Новое бронирование назначено администратором";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Запрос назначен успешно";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "Бронирование уже принимается другим водителем";

notifyMessage.BOOKING_UPDATE_FAILED =
  "Не могу обновить подробности бронирования, что -то пошло не так";

notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Ошибка в обновлении статуса бронирования";

notifyMessage.CANCELLATION_REASON_NOT_FOUND = "Причина отмены не найдена";

notifyMessage.SOMETHING_WENT_WRONG = "Причина отмены не найдена";

notifyMessage.RIDE_CANCELLED = "Ride отменил успешно";
notifyMessage.RIDE_IS = "Эта поездка";
notifyMessage.RIDE_STATUS_INFO = "Езда уже закончилась или еще не началась";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "Ошибка в изменении местоположений езды";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "Адрес езды изменился пользователем";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Адрес поездки изменен профессионалом";
notifyMessage.OTP_INCORRECT = "OTP неверно, любезно введите правильный OTP";

notifyMessage.RATING_SUBMITED = "Рейтинг успешно";
notifyMessage.RATING_UPDATE_FAILED = "Ошибка в обновлении рейтинга";

notifyMessage.LOCATION_ACCESS_FAILED = "Что -то пошло не так в вашем месте";

notifyMessage.TRIP_NOT_AVAILABLE = "Советы недоступны";
notifyMessage.TRIP_SHARED = "Путешествие поделилась успешно";
notifyMessage.TRIP_END_PROFESSIONAL = "Ваша поездка закончилась профессионалом";

notifyMessage.OPERATOR_STATUS_END =
  "Наш оператор изменил статус поездки на окончание";

notifyMessage.ERROR_SECURITY_UPDATE = "Ошибка в обновлении безопасности";
notifyMessage.ERROR_GENEREL_SETTINGS = "Ошибка в настройках генера";

notifyMessage.WISHLIST_ACTION_FAILED =
  "Действие {{action}} WishList не удастся";

notifyMessage.SUR_CHARGE = "SUR заряд";
notifyMessage.SERVICE_TAX = "Налоговая служба";
notifyMessage.TIPS_AMOUNT = "Количество советов";
notifyMessage.COUPON_SAVINGS = "Сэкономить купона";
notifyMessage.TOLL_FEE = "Плата за проезд";
notifyMessage.DISCOUNT_APPLIED = "Скидка применяется";
notifyMessage.REFUND_TO_WALLET = "Возврат в кошелек";

notifyMessage.TIPS_MINIMUM = "Советы Shoulud будут минимальными";
notifyMessage.TIPS_MAXIMUM = "Советы Shoulud будут максимум";
notifyMessage.TIPS_ADDED = "Советы добавлены успешно";

notifyMessage.ERROR_PAYMENT_CARD =
  "Ошибка в платеже карты, пожалуйста, попробуйте через некоторое время";

notifyMessage.ERROR_EARNINGS = "Ошибка в заработках";
notifyMessage.ERROR_DAY_EARNINGS = "Ошибка в дневных доходах";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "Кажется, профессионал - это Offiline. Пожалуйста, попросите профессионала выйти в интернет, прежде чем забронировать эту поездку";

notifyMessage.INFO_PAYMENT_OPTIONS = "Варианты оплаты не найдены";
notifyMessage.INFO_CARD = "Не могу найти данные карты";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Что -то пошло не так в событиях Gateway, проверяйте кардиологически шаг за шагом";

notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "Не могу найти пользователя, что -то пошло не так в перезарядке суммы кошелька";

notifyMessage.INFO_CHECK_CREDENTIAL =
  "Пожалуйста, проверьте следующий учетный данные";
notifyMessage.INFO_SEND_MONEY = "Деньги отправлены успешно";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Заплатил за подписку";
notifyMessage.INFO_TIPS = "Вы дали советы";
notifyMessage.INFO_JOINING_CHARGE = "Объединение суммы";
notifyMessage.INFO_REFERREL_CHARGE = "Сумма направления";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Загружено оставшаяся сумма платежа";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Оставшаяся сумма оплаты списана";

notifyMessage.WALLET_AMOUNT_LOADED =
  "Количество успешно добавлено в ваш кошелек";

notifyMessage.WALLET_NOTIFICATION =
  "Что -то пошло не так в обновлении кошелька в кошельке.";

notifyMessage.WALLET_WITHDRAW = "Снятие количества из кошелька";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Успех кошелек успешно";
notifyMessage.WALLET_DEBIT = "Количество кошелька списано";

notifyMessage.ERROR_WALLET_RECHARGE =
  "Произошла ошибка во время перезарядки кошелька";
notifyMessage.ERROR_WALLET_WITHDRAW = "Ошибка в снятии кошелька";
notifyMessage.ERROR_PAYMENT_OPTION = "Ошибка в списке опций оплаты";
notifyMessage.ERROR_SEND_MONEY = "Ошибка отправки денег";

notifyMessage.BANK_CREDET_MONEY =
  "Деньги будут кредит на банковский счет в течение 7 рабочих дней";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Передано другу";

notifyMessage.BOOKING_CHARGE_FREEZED = "Замороженная сумма для бронирования";

notifyMessage.RIDE_CANCELLATION_CHARGE = "Ездить на отмену суммы";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "Возврат на скидку применяется";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "Транзакция не найдена";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "Ответ не найден, мы предполагаем, что вы нелегальный пользователь этого URL -адреса";

notifyMessage.INFO_CARD_ADDED = "Карта успешно добавлена";
notifyMessage.INFO_CARD_VERIFIED = "Карта проверена успешно";
notifyMessage.INFO_BANK_DETAILS = "Банковские детали";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Успех транзакций";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "Не нашел банковские детали";
notifyMessage.INFO_CARD_DECLINED =
  "Сумма вычтена из кошелька. Из -за снижения карты";

notifyMessage.INFO_AMOUNT_PAID = "Сумма уплачена успешно";

notifyMessage.ERROR_CARD_ADD = "Не могу добавить карту";
notifyMessage.ERROR_CARD_ADD_FAILED = "Что -то пошло не так в добавлении карты";
notifyMessage.ERROR_IN_TRANSACTIONS = "Ошибка в транзакциях";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "Оплата карты не удалась";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "Извините, мы здесь не предоставляем услуги";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Файл успешно загружен";
notifyMessage.INFO_NOT_FOUND = "Детали не найдены";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "Точка успешно добавлена ​​в ваш кошелек";

notifyMessage.ERROR_WALLET_POINT_ADDED = "Не в состоянии точку кошелька";
notifyMessage.INFO_SUCCESS = "УСПЕХ";
notifyMessage.INFO_FAILED = "НЕУСПЕШНЫЙ";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "Получить очки вознаграждения";
notifyMessage.TRAN_REDEEM_REWARD_MIN =
  "Выкупить минимальные точки вознаграждения";
notifyMessage.TRAN_REDEEM_REWARD_MAX =
  "Получить максимальные точки вознаграждения";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Наш оператор изменил статус поездки на прибытие";

notifyMessage.INFO_OPERATOR_STATUS_START =
  "Наш оператор изменил статус вашей поездки на начало";

notifyMessage.INFO_RIDE_CANCELLED_BY = "Ride отменил";
notifyMessage.INFO_PROFESSIONAL = "Профессионал";
notifyMessage.INFO_USER = "ПОЛЬЗОВАТЕЛЬ";
notifyMessage.INFO_ADMIN = "Администратор";
notifyMessage.INFO_INVALID_DETAILS = "Неверные детали";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Этот номер телефона уже зарегистрирован в корпоративном названии";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "Неверный реферальный код, проверьте свой код и попробуйте еще раз";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS =
  "Идентификатор электронной почты уже зарегистрирован";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "Неверный формат DOB должен быть mm-dd-yyyy или mm/dd/yyyy";
notifyMessage.INFO_INVALID_AGE =
  "Возраст должен быть {{minAgeKey}} и выше, чтобы продолжить";
notifyMessage.INFO_CARD_ALREADY_EXIST = "Карта уже существует";
notifyMessage.INFO_WALLET_RECHARGE = "Пополнение кошелька";
notifyMessage.INFO_INTERCITY_RIDE = "Межгородка";
notifyMessage.INFO_SITE_COMMISSION = "Сайт Комиссия";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Кредит на отмену поездки";
//--------- Newly added -------------
notifyMessage.INFO_ROUNDING = "Rounding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONAL ALREADY HAVE RIDE";
notifyMessage.INFO_BASE_FARE = "Base Fare";
notifyMessage.INFO_SERVICE_TAX = "Service Tax";
notifyMessage.INFO_TRAVEL_CHARGE = "Travel Charge";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum Charge";
notifyMessage.INFO_SURCHARGE_FEE = "SurCharge Fee";
notifyMessage.INFO_BOUNDARY_CHARGE = "boundary Charge";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE = "Your {{siteTitle}} OTP code is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Earnings";
notifyMessage.INFO_FELLOW_PASSENGER = "FELLOW PASSENGER";
notifyMessage.INFO_ALREADY_EXISTS = "ALREADY EXIST";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Invalid Coupon Code, Please Check Your Code.";
notifyMessage.WALLET_RECHARGE_FAILED = "WALLET RECHARGE FAILED";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "YOUR VEHICLE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "YOUR PROFILE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Your Vehicle Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Your Profile Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Assistance Care Fee";
notifyMessage.INFO_TIME_FARE = "Time Fare";
notifyMessage.INFO_DISTANCE_FARE = "Distance Fare";
notifyMessage.INFO_PEAK_FARE = "Peak Fare";
notifyMessage.INFO_NIGHT_FARE = "Night Fare";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oops! Connection is lost. You’re offline now. Please go online to take ride requests.";
notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your Rating is below the Range, Please improve, or your Account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Your account is activated. Please go online to take ride requests.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Card validation failed";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bank server Busy Now. Please try Later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Verified Successfully";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV Verification Failed";
notifyMessage.ALERT_CHECK_CPF = "Please Check CPF Number.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "Please Check From Date / To Date.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "City Already Exist.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF Number Already Exist.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maximum Withdraw Amount Limit is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Amount Withdrawal Allowed Once Per Day.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Account Holder Name";
notifyMessage.INFO_BANK_NAME = "Bank Name";
notifyMessage.INFO_DOCUMENT_NUMBER = "Document Number";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Wallet withdrawal failed. Amount credited to the wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No Service Location Is Available";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Your fellow passenger has arrived at the pickup location";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Your fellow passenger has started the ride";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "The ride has come to an end for your fellow passenger.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "This Email Is Already Associated With Another Account. Please Use A Different Email.";
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
