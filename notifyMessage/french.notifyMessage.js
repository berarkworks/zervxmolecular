const notifyMessage = {};

notifyMessage.CREATED = "Créé avec succès";
notifyMessage.UPDATED = "Mis à jour avec succés";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Données utilisateur non valides";
notifyMessage.TRIP_END = "Le voyage a été terminé";
notifyMessage.WALLET_LOADED = "Montant du portefeuille chargé";
notifyMessage.CASH = "Espèces";
notifyMessage.CARD = "Carte";
notifyMessage.WALLET = "Portefeuille";
notifyMessage.TRIP_FEE = "Frais de voyage";
notifyMessage.TOTAL_BILL = "Facture totale";
notifyMessage.EARNINGS = "Vos gains";
notifyMessage.WAITING_CHARGE = "En attente";
notifyMessage.PENDING_AMOUNT = "Montant en attente";
notifyMessage.CASH_IN_HAND = "Du liquide en main";

notifyMessage.BOOKING_ACCEPTED = "Votre réservation est acceptée";
notifyMessage.BOOKING_PAID = "Payé pour la réservation";

notifyMessage.PROFESSIONAL_STARTED = "Le professionnel a commencé votre voyage";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY =
  "L'emplacement a changé avec succès";
notifyMessage.PROFESSIONAL_ARRIVED =
  "Un professionnel est arrivé à votre lieu de ramassage";

notifyMessage.PROFESSIONAL_COMMISSION = "Commission professionnelle";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Réservation des revenus reçus";

notifyMessage.PROFESSIONAL_INFO =
  "Le solde de votre portefeuille est passé en dessous de la limite minimale requise pour accepter les transferts en espèces. Veuillez recharger votre portefeuille ou vous ne pouvez accepter les trajets par carte et portefeuille que jusqu'à ce que votre solde devienne suffisant";
notifyMessage.PROFESSIONAL_NOT_FOUND = "Professionnel introuvable";
notifyMessage.PROFESSIONAL_NEW_RIDE = "Professionnel introuvable";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Ride annulée par professionnel";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Votre réservation est acceptée par notre professionnel";

notifyMessage.PROFESSIONAL_NOTIFICATION =
  "Notification envoyée aux professionnels à proximité";

notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "Le professionnel est arrivé pour arrêter l'emplacement";

notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Professionnel Démarrez le trajet à partir de l'emplacement d'arrêt";

notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "Un professionnel est arrivé à votre lieu de réservation";

notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "Professionnel a commencé à partir de l'emplacement de la réservation";

notifyMessage.USER_NOT_FOUND = "UTILISATEUR NON TROUVÉ";
notifyMessage.USER_HAVE_RIDE = "L'utilisateur a déjà une balade";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "Quelque chose s'est mal passé à l'utilisateur annuler cette balade";

notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Veuillez recharger votre portefeuille ou choisir une autre option de paiement";

notifyMessage.USER_PAYMENT_DECLINED =
  "Ce mode de paiement a été refusé. Veuillez choisir un autre mode de paiement";

notifyMessage.INVALID_PROFESSIONAL = "Détails professionnels non valides";
notifyMessage.INVALID_BOOKING = "Détails de réservation non valides";
notifyMessage.INVALID_ACCESS = "Accès invalide";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "Catégorie de véhicules introuvable";
notifyMessage.VEHICLE_NOT_AVAILABLE = "Aucun véhicule n'est disponible";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "Catégorie de service introuvable";

notifyMessage.BOOKING_SCHEDULED_RIDE = "Réservation programmée avec succès";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND =
  "Catégorie de réservation introuvable";
notifyMessage.BOOKING_ALREADY = "La réservation est déjà";
notifyMessage.BOOKING_STATUS_IS = "La réservation est";
notifyMessage.BOOKING_STATUS_INFO =
  "Réservation déjà en cours ou s'est terminée";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Réservation annulée par le conducteur vérifiant un autre pilote";

notifyMessage.BOOKING_CANCELLED_BY = "Réservation annulée par";
notifyMessage.BOOKING_CANCELLED_BY_USER =
  "Réservation annulée par l'utilisateur";
notifyMessage.BOOKING_CANCELLED = "La réservation est annulée avec succès";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "La nouvelle réservation est attribuée par l'administrateur";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Demande attribuée avec succès";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "La réservation est déjà acceptée par un autre pilote";

notifyMessage.BOOKING_UPDATE_FAILED =
  "Je ne peux pas mettre à jour les détails de réservation, quelque chose s'est mal passé";

notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Erreur dans la mise à jour de l'état de la réservation";

notifyMessage.CANCELLATION_REASON_NOT_FOUND = "Raison d'annulation non trouvée";

notifyMessage.SOMETHING_WENT_WRONG = "Raison d'annulation non trouvée";

notifyMessage.RIDE_CANCELLED = "Ride annulée avec succès";
notifyMessage.RIDE_IS = "Cette balade est";
notifyMessage.RIDE_STATUS_INFO =
  "Le trajet s'est déjà terminé ou n'est pas encore commencé";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "Erreur dans la modification des emplacements de conduite";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "L'adresse de conduite a changé par l'utilisateur";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "L'adresse du trajet a changé par le professionnel";
notifyMessage.OTP_INCORRECT = "OTP est incorrect, saisissez bien OTP correct";

notifyMessage.RATING_SUBMITED = "Évaluation soumise avec succès";
notifyMessage.RATING_UPDATE_FAILED = "Erreur de mise à jour de notation";

notifyMessage.LOCATION_ACCESS_FAILED =
  "Quelque chose s'est mal passé dans votre emplacement";

notifyMessage.TRIP_NOT_AVAILABLE = "Tips n'est pas disponible";
notifyMessage.TRIP_SHARED = "Trip partagé avec succès";
notifyMessage.TRIP_END_PROFESSIONAL =
  "Votre trajet s'est terminé par un professionnel";

notifyMessage.OPERATOR_STATUS_END =
  "Notre opérateur a changé votre statut de voyage pour terminer";

notifyMessage.ERROR_SECURITY_UPDATE =
  "Erreur dans la mise à jour de la sécurité";
notifyMessage.ERROR_GENEREL_SETTINGS = "Erreur dans les paramètres de générel";

notifyMessage.WISHLIST_ACTION_FAILED =
  "Action {{Action}} La liste de souhaits est échouée";

notifyMessage.SUR_CHARGE = "Accusation";
notifyMessage.SERVICE_TAX = "Taxe de service";
notifyMessage.TIPS_AMOUNT = "Montant des conseils";
notifyMessage.COUPON_SAVINGS = "Économies de coupons";
notifyMessage.TOLL_FEE = "Péage";
notifyMessage.DISCOUNT_APPLIED = "Réduction appliquée";
notifyMessage.REFUND_TO_WALLET = "Rembourser au portefeuille";

notifyMessage.TIPS_MINIMUM = "Conseils shoulud être minimum de";
notifyMessage.TIPS_MAXIMUM = "Conseils shoulud être maximum de";
notifyMessage.TIPS_ADDED = "Conseils ajoutés avec succès";

notifyMessage.ERROR_PAYMENT_CARD =
  "Erreur dans le paiement de la carte, veuillez essayer après un certain temps";

notifyMessage.ERROR_EARNINGS = "Erreur de gains";
notifyMessage.ERROR_DAY_EARNINGS = "Erreur dans les gains de jour";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "Il semble que le professionnel soit offiline. Veuillez demander au professionnel d'aller en ligne avant de réserver ce voyage";

notifyMessage.INFO_PAYMENT_OPTIONS = "Options de paiement introuvables";
notifyMessage.INFO_CARD = "Je ne trouve pas les détails de la carte";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Quelque chose s'est mal passé dans les événements de passerelle vérifiez avec étape de façon prudente";

notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "Je ne peux pas trouver l'utilisateur que quelque chose a mal tourné dans le montant du portefeuille Recharge";

notifyMessage.INFO_CHECK_CREDENTIAL = "Veuillez vérifier le prochain diplôme";
notifyMessage.INFO_SEND_MONEY = "L'argent envoyé avec succès";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Payé pour abonnement";
notifyMessage.INFO_TIPS = "Vous avez donné des conseils";
notifyMessage.INFO_JOINING_CHARGE = "Montant du montant";
notifyMessage.INFO_REFERREL_CHARGE = "Montant de références";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Montant de paiement restant chargé";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Montant de paiement restant débité";

notifyMessage.WALLET_AMOUNT_LOADED =
  "Le montant est ajouté à votre portefeuille avec succès";

notifyMessage.WALLET_NOTIFICATION =
  "Quelque chose s'est mal passé dans la mise à jour du portefeuille dans Wallet Rechare pas";

notifyMessage.WALLET_WITHDRAW = "Montant se retirer du portefeuille";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Retrait du portefeuille avec succès";
notifyMessage.WALLET_DEBIT = "Montant du portefeuille débité";

notifyMessage.ERROR_WALLET_RECHARGE =
  "Erreur s'est produite pendant la recharge du portefeuille";
notifyMessage.ERROR_WALLET_WITHDRAW = "Erreur dans le retrait du portefeuille";
notifyMessage.ERROR_PAYMENT_OPTION =
  "Erreur de paiement des options de paiement";
notifyMessage.ERROR_SEND_MONEY = "Erreur dans l'envoi de l'argent";

notifyMessage.BANK_CREDET_MONEY =
  "L'argent sera créatif sur le compte bancaire Yout dans les 7 jours ouvrables";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Transféré à l'ami";

notifyMessage.BOOKING_CHARGE_FREEZED = "Montant glacial pour la réservation";

notifyMessage.RIDE_CANCELLATION_CHARGE = "Montant d'annulation";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "Remboursement de la remise appliquée";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "La transaction n'est pas trouvée";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "Réponse non découverte que nous supposons que vous êtes un utilisateur illégal de cette URL";

notifyMessage.INFO_CARD_ADDED = "La carte est ajoutée avec succès";
notifyMessage.INFO_CARD_VERIFIED = "Carte vérifiée avec succès";
notifyMessage.INFO_BANK_DETAILS = "COORDONNÉES BANCAIRES";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Succès des transactions";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND =
  "Je ne peux pas trouver les détails de la banque";
notifyMessage.INFO_CARD_DECLINED =
  "Montant déduit du portefeuille. En raison de la baisse de la carte";

notifyMessage.INFO_AMOUNT_PAID = "Montant payé avec succès";

notifyMessage.ERROR_CARD_ADD = "Impossible d'ajouter une carte";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "Quelque chose s'est mal passé dans l'ajout de la carte";
notifyMessage.ERROR_IN_TRANSACTIONS = "Erreur dans les transactions";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "Le paiement de la carte a échoué";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "Désolé, nous ne fournissons pas de service ici";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Fichier téléchargé avec succès";
notifyMessage.INFO_NOT_FOUND = "Détails introuvables";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "Le point est ajouté à votre portefeuille avec succès";

notifyMessage.ERROR_WALLET_POINT_ADDED =
  "Impossible de porter le point de portefeuille";
notifyMessage.INFO_SUCCESS = "SUCCÈS";
notifyMessage.INFO_FAILED = "ÉCHOUÉ";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "Échanger des points de récompense";
notifyMessage.TRAN_REDEEM_REWARD_MIN =
  "Échanger des points de récompense minimum";
notifyMessage.TRAN_REDEEM_REWARD_MAX =
  "Échanger les points de récompense maximum";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Notre opérateur a changé votre statut de voyage pour être arrivé";

notifyMessage.INFO_OPERATOR_STATUS_START =
  "Notre opérateur a changé votre statut de voyage pour commencer";

notifyMessage.INFO_RIDE_CANCELLED_BY = "La conduite a annulé par";
notifyMessage.INFO_PROFESSIONAL = "PROFESSIONNEL";
notifyMessage.INFO_USER = "UTILISATEUR";
notifyMessage.INFO_ADMIN = "Administrer";
notifyMessage.INFO_INVALID_DETAILS = "Détails non valides";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Ce numéro de téléphone est déjà enregistré au nom de l'entreprise";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "Code de référence non valide, veuillez vérifier votre code et réessayer";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "ID de messagerie déjà enregistré";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "Le format DOB non valide doit être mm-dd-yyy ou mm / dd / yyyy";
notifyMessage.INFO_INVALID_AGE =
  "L'âge doit être {{minAgeKey}} et au-dessus pour continuer";
notifyMessage.INFO_CARD_ALREADY_EXIST = "La carte existe déjà";
notifyMessage.INFO_WALLET_RECHARGE = "Recharge de portefeuille";
notifyMessage.INFO_INTERCITY_RIDE = "Conduite interurbaine";
notifyMessage.INFO_SITE_COMMISSION = "Commission de site";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Crédit d'annulation de conduite";
notifyMessage.INFO_ROUNDING = "Arrondi";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONNEL ONT DÉJÀ ROULÉ";
notifyMessage.INFO_BASE_FARE = "Tarif de base";
notifyMessage.INFO_SERVICE_TAX = "Taxe de service";
notifyMessage.INFO_TRAVEL_CHARGE = "Frais de déplacement";
notifyMessage.INFO_MINIMUM_CHARGE = "Frais minimaux";
notifyMessage.INFO_SURCHARGE_FEE = "Frais supplémentaires";
notifyMessage.INFO_BOUNDARY_CHARGE = "Frais de frontière";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Frais de ramassage interurbain";
notifyMessage.INFO_OTP_MESSAGE = "Votre code OTP {{siteTitle}} est {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Votre compte a été verrouillé car vous avez atteint le nombre maximal de tentatives de connexion non valides.";
notifyMessage.INFO_HUBS_EARNINGS = "Bénéfices des hubs";
notifyMessage.INFO_FELLOW_PASSENGER = "COMPAGNON PASSAGER";
notifyMessage.INFO_ALREADY_EXISTS = "EXISTE DÉJÀ";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Code promo non valide, veuillez vérifier votre code.";
notifyMessage.WALLET_RECHARGE_FAILED = "ÉCHEC DE LA RECHARGE DU PORTEFEUILLE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "LES DOCUMENTS DE VOTRE VÉHICULE SONT EXPIRÉS. VEUILLEZ LES VÉRIFIER ET LES METTRE À JOUR";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "VOS DOCUMENTS DE PROFIL SONT EXPIRÉS. VEUILLEZ LES VÉRIFIER ET LES METTRE À JOUR";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Les documents de votre véhicule expireront dans {{days}} jour(s). Veuillez les vérifier et les mettre à jour";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Vos documents de profil expireront dans {{days}} jour(s). Veuillez vérifier et mettre à jour";
notifyMessage.INFO_INCENTIVE_CREDIT = "Crédit d'incitation";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Frais d'assistance";
notifyMessage.INFO_TIME_FARE = "Tarif horaire";
notifyMessage.INFO_DISTANCE_FARE = "Tarif à distance";
notifyMessage.INFO_PEAK_FARE = "Tarif de pointe";
notifyMessage.INFO_NIGHT_FARE = "Tarif de nuit";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oups ! La connexion est perdue. Vous êtes hors ligne pour le moment. Veuillez vous connecter pour prendre des demandes de course.";
notifyMessage.AIRPORT_FEE = "Frais d'aéroport";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Ce professionnel n'accepte pas l'option de paiement sélectionnée. Veuillez modifier l'option de paiement";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Votre note est inférieure à la fourchette, veuillez l'améliorer, sinon votre compte sera bloqué dans quelques jours.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Votre compte a été bloqué car votre note est inférieure à la fourchette. Le compte sera réactivé le {{date}} minuit";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Votre compte est activé. Veuillez vous connecter pour effectuer des demandes de transport.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Montant transféré d'un ami";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "La validation de la carte a échoué";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Serveur bancaire occupé actuellement. Veuillez réessayer plus tard";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV vérifié avec succès";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Échec de la vérification CVV";
notifyMessage.ALERT_CHECK_CPF = "Veuillez vérifier le numéro CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Veuillez vérifier la date de début / de fin.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "La ville existe déjà.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "Le numéro CPF existe déjà.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "La limite du montant maximum de retrait est de {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Montant de retrait autorisé une fois par jour.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Nom du titulaire du compte";
notifyMessage.INFO_BANK_NAME = "Nom de la banque";
notifyMessage.INFO_DOCUMENT_NUMBER = "Numéro de document";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Le retrait du portefeuille a échoué. Montant crédité sur le portefeuille.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Aucun emplacement de service n'est disponible";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Votre compagnon de voyage est arrivé au lieu de prise en charge";
//
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Votre compagnon de voyage a commencé le trajet";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "Le voyage est terminé pour votre compagnon de voyage.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Cet e-mail est déjà associé à un autre compte. Veuillez utiliser une adresse e-mail différente.";
notifyMessage.INFO_TRIP_NUMBER = "Numéro de voyage";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "L'emplacement du service existe déjà.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "L'entreprise existe déjà.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "La zone de service existe déjà.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "La vérification de votre document est en cours d'examen, veuillez patienter jusqu'à ce que la vérification soit terminée.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
