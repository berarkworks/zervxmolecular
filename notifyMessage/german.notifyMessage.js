const notifyMessage = {};

notifyMessage.CREATED = "Erfolgreich erstellt";
notifyMessage.UPDATED = "Erfolgreich geupdated";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Ungültige Benutzerdaten";
notifyMessage.TRIP_END = "Reise wurde beendet";
notifyMessage.WALLET_LOADED = "Brieftasche beladen";
notifyMessage.CASH = "Kasse";
notifyMessage.CARD = "Karte";
notifyMessage.WALLET = "Geldbörse";
notifyMessage.TRIP_FEE = "Ausflugsgebühr";
notifyMessage.TOTAL_BILL = "Gesamte Rechnung";
notifyMessage.EARNINGS = "Ihr Einkommen";
notifyMessage.WAITING_CHARGE = "Wartegebühr";
notifyMessage.PENDING_AMOUNT = "Ausstehende Summe";
notifyMessage.CASH_IN_HAND = "Bar auf die Hand";

notifyMessage.BOOKING_ACCEPTED = "Ihre Buchung wird akzeptiert";
notifyMessage.BOOKING_PAID = "Für die Buchung bezahlt";

notifyMessage.PROFESSIONAL_STARTED = "Profi begann zu Ihrer Reise";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY =
  "Der Ort änderte sich erfolgreich";
notifyMessage.PROFESSIONAL_ARRIVED =
  "Professionell kam zu Ihrem Pickup -Standort";

notifyMessage.PROFESSIONAL_COMMISSION = "Professionelle Kommission";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Erhalt der Einnahmen gebucht";

notifyMessage.PROFESSIONAL_INFO =
  "Ihr Guthaben ist unter den Mindestbetrag gesunken, der für die Annahme von Bargeldfahrten erforderlich ist. Bitte laden Sie Ihr Portemonnaie auf, sonst können Sie nur Karten- und Portemonnaiefahrten akzeptieren, bis Ihr Guthaben ausreicht";
notifyMessage.PROFESSIONAL_NOT_FOUND = "Professionell nicht gefunden";
notifyMessage.PROFESSIONAL_NEW_RIDE = "Professionell nicht gefunden";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Fahrt durch Profi abgesagt";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Ihre Buchung wird von unserem Fachmann akzeptiert";

notifyMessage.PROFESSIONAL_NOTIFICATION =
  "Benachrichtigung an nahe gelegene Profis senden";

notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "Professionell ankam, um den Standort zu stoppen";

notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Professioneller Start Die Fahrt vom Stop Standort";

notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "Professioneller an Ihrem Buchungsort angekommen";

notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "Professional begann mit der Buchungsstunde";

notifyMessage.USER_NOT_FOUND = "BENUTZER NICHT GEFUNDEN";
notifyMessage.USER_HAVE_RIDE = "Benutzer haben bereits Fahrt";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "In dem Benutzer wurde diese Fahrt storniert";

notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Bitte laden Sie Ihre Brieftasche auf oder wählen Sie eine andere Zahlungsoption aus";

notifyMessage.USER_PAYMENT_DECLINED =
  "Diese Zahlungsmethode wurde abgelehnt. Bitte wählen Sie eine andere Zahlungsmethode";

notifyMessage.INVALID_PROFESSIONAL = "Ungültige professionelle Details";
notifyMessage.INVALID_BOOKING = "Ungültige Buchungsdetails";
notifyMessage.INVALID_ACCESS = "UNGÜLTIGER ZUGANG";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "Fahrzeugkategorie nicht gefunden";
notifyMessage.VEHICLE_NOT_AVAILABLE = "Kein Fahrzeug ist verfügbar";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "Servicekategorie nicht gefunden";

notifyMessage.BOOKING_SCHEDULED_RIDE = "Buchung erfolgreich geplant";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "Buchungskategorie nicht gefunden";
notifyMessage.BOOKING_ALREADY = "Buchung ist bereits";
notifyMessage.BOOKING_STATUS_IS = "Buchung ist";
notifyMessage.BOOKING_STATUS_INFO = "Buchung bereits laufend oder beendet";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Buchung storniert durch den Fahrer, der nach einem anderen Fahrer überprüft wird";

notifyMessage.BOOKING_CANCELLED_BY = "Buchung abgesagt von";
notifyMessage.BOOKING_CANCELLED_BY_USER = "Buchung durch den Benutzer abgesagt";
notifyMessage.BOOKING_CANCELLED = "Die Buchung wird erfolgreich abgesagt";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "Neue Buchung wird vom Administrator zugewiesen";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Anfrage erfolgreich zugewiesen";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "Die Buchung wird bereits von einem anderen Fahrer akzeptiert";

notifyMessage.BOOKING_UPDATE_FAILED =
  "Kann Buchungsdetails nicht aktualisieren, dass etwas schief gelaufen ist";

notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Fehler beim Update der Buchungsstatus";

notifyMessage.CANCELLATION_REASON_NOT_FOUND =
  "Stornierungsgrund nicht gefunden";

notifyMessage.SOMETHING_WENT_WRONG = "Stornierungsgrund nicht gefunden";

notifyMessage.RIDE_CANCELLED = "Fahrt erfolgreich abgesagt";
notifyMessage.RIDE_IS = "Diese Fahrt ist";
notifyMessage.RIDE_STATUS_INFO =
  "Fahrt bereits endete oder noch nicht begonnen";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "Fehler bei der Änderung der Fahrpositionen";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "Die Fahradresse hat sich durch den Benutzer geändert";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Die Fahradresse wurde vom Fachmann geändert";
notifyMessage.OTP_INCORRECT =
  "OTP ist falsch. Bitte geben Sie die korrekte OTP ein";

notifyMessage.RATING_SUBMITED = "Bewertung erfolgreich eingereicht";
notifyMessage.RATING_UPDATE_FAILED = "Fehler in der Bewertungs -Aktualisierung";

notifyMessage.LOCATION_ACCESS_FAILED = "In Ihrem Standort ging etwas schief";

notifyMessage.TRIP_NOT_AVAILABLE = "Tipps sind nicht verfügbar";
notifyMessage.TRIP_SHARED = "Reise erfolgreich geteilt";
notifyMessage.TRIP_END_PROFESSIONAL = "Ihre Fahrt endete von Profi";

notifyMessage.OPERATOR_STATUS_END =
  "Unser Betreiber hat Ihren Auslöserstatus auf endet";

notifyMessage.ERROR_SECURITY_UPDATE = "Fehler in der Sicherheitsaktualisierung";
notifyMessage.ERROR_GENEREL_SETTINGS = "Fehler in den Generelleneinstellungen";

notifyMessage.WISHLIST_ACTION_FAILED =
  "Aktion {{action}} Die Wunschliste ist fehlgeschlagen";

notifyMessage.SUR_CHARGE = "Sur Ladung";
notifyMessage.SERVICE_TAX = "Servicesteuer";
notifyMessage.TIPS_AMOUNT = "Tipps Betrag";
notifyMessage.COUPON_SAVINGS = "Gutscheineinsparungen";
notifyMessage.TOLL_FEE = "Mautgebühr";
notifyMessage.DISCOUNT_APPLIED = "Rabatt angewendet";
notifyMessage.REFUND_TO_WALLET = "Rückerstattung in die Brieftasche";

notifyMessage.TIPS_MINIMUM = "Tipps sollten mindestens sein";
notifyMessage.TIPS_MAXIMUM = "Tipps sollten maximal sein";
notifyMessage.TIPS_ADDED = "Tipps erfolgreich hinzugefügt";

notifyMessage.ERROR_PAYMENT_CARD =
  "Fehler in der Kartenzahlung, bitte versuchen Sie es nach einiger Zeit";

notifyMessage.ERROR_EARNINGS = "Fehler beim Einkommen";
notifyMessage.ERROR_DAY_EARNINGS = "Fehler bei Tageseinnahmen";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "Es scheint, dass der Profi Offilin ist. Bitte bitten Sie den Fachmann, online zu gehen, bevor Sie diese Reise buchen";

notifyMessage.INFO_PAYMENT_OPTIONS = "Zahlungsoptionen nicht gefunden";
notifyMessage.INFO_CARD = "Ich kann die Kartendetails nicht finden";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Bei Gateway -Events ging etwas schief auf, um Schritt für Schritt sorgfältig zu überprüfen";

notifyMessage.INFO_USER_WALLET_NOT_FOUND = "Ich kann den Benutzer nicht finden";

notifyMessage.INFO_CHECK_CREDENTIAL =
  "Bitte überprüfen Sie den nächsten Anmeldeinformationen";
notifyMessage.INFO_SEND_MONEY = "Geld erfolgreich gesendet";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Für Abonnement bezahlt";
notifyMessage.INFO_TIPS = "Du hast Tipps gegeben";
notifyMessage.INFO_JOINING_CHARGE = "Betrag verbinden";
notifyMessage.INFO_REFERREL_CHARGE = "Überweisungsmenge";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Verbleibender Zahlungsbetrag geladen";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Verbleibender Zahlungsbetrag belastet";

notifyMessage.WALLET_AMOUNT_LOADED =
  "Die Menge wird Ihrem Brieftasche erfolgreich hinzugefügt";

notifyMessage.WALLET_NOTIFICATION =
  "Im Brieftaschen -Update in der Brieftasche lief etwas schief";

notifyMessage.WALLET_WITHDRAW = "Betrag, der sich aus der Brieftasche entzieht";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Brieftaschenentzug erfolgreich";
notifyMessage.WALLET_DEBIT = "Brieftaschenmenge belastet";

notifyMessage.ERROR_WALLET_RECHARGE =
  "Fehler aufgetreten, während die Brieftasche aufgeladen wurde";
notifyMessage.ERROR_WALLET_WITHDRAW = "Fehler in der Brieftaschenentnahme";
notifyMessage.ERROR_PAYMENT_OPTION = "Fehler in der Zahlungsoptionsliste";
notifyMessage.ERROR_SEND_MONEY = "Fehler beim Geld senden";

notifyMessage.BANK_CREDET_MONEY =
  "Geld wird innerhalb von 7 Werktagen für das YouT Bank-Konto eingeschrieben";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Zu Freund übertragen";

notifyMessage.BOOKING_CHARGE_FREEZED = "Gefrorener Betrag für die Buchung";

notifyMessage.RIDE_CANCELLATION_CHARGE = "Fahrt abbrechen";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "Rückerstattung für Rabatt angewendet";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "Transaktion wird nicht gefunden";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "Antwort nicht gefunden. Wir gehen davon aus, dass Sie ein illegaler Benutzer dieser URL sind";

notifyMessage.INFO_CARD_ADDED = "Karte wird erfolgreich hinzugefügt";
notifyMessage.INFO_CARD_VERIFIED = "Karte erfolgreich überprüft";
notifyMessage.INFO_BANK_DETAILS = "BANKDATEN";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Transaktionen Erfolg";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND =
  "Ich kann die Bankdetails nicht finden";
notifyMessage.INFO_CARD_DECLINED =
  "Betrag, der aus der Brieftasche abgezogen wird. Wegen der Karte abgelehnt";

notifyMessage.INFO_AMOUNT_PAID = "Betrag erfolgreich bezahlt";

notifyMessage.ERROR_CARD_ADD = "Kann keine Karte hinzufügen";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "In der Karte wurde etwas schief gelaufen";
notifyMessage.ERROR_IN_TRANSACTIONS = "Fehler in Transaktionen";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "Kartenzahlung fehlgeschlagen";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "Entschuldigung, wir bieten hier keinen Service an";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Datei erfolgreich hochgeladen";
notifyMessage.INFO_NOT_FOUND = "Details nicht gefunden";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "Punkt wird Ihrem Brieftasche erfolgreich hinzugefügt";

notifyMessage.ERROR_WALLET_POINT_ADDED =
  "Nicht in der Lage, Geldspitze zu beenden";
notifyMessage.INFO_SUCCESS = "ERFOLG";
notifyMessage.INFO_FAILED = "FEHLGESCHLAGEN";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "Belohnungspunkte einlösen";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Mindestbelohnungspunkte einlösen";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Maximale Belohnungspunkte einlösen";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Unser Betreiber hat Ihren Ausflugsstatus in angekommenes Ankommen geändert";

notifyMessage.INFO_OPERATOR_STATUS_START =
  "Unser Betreiber hat Ihren Auslöserstatus geändert, um zu beginnen";

notifyMessage.INFO_RIDE_CANCELLED_BY = "Fahrt hat abgesagt von";
notifyMessage.INFO_PROFESSIONAL = "FACHMANN";
notifyMessage.INFO_USER = "BENUTZER";
notifyMessage.INFO_ADMIN = "ADMINISTRATOR";
notifyMessage.INFO_INVALID_DETAILS = "Ungültige Details";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Diese Telefonnummer ist bereits im Unternehmensnamen registriert";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "Ungültiger Empfehlungscode, überprüfen Sie bitte Ihren Code und versuchen Sie es erneut";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "E -Mail -ID bereits registriert";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "Ungültiges DOB-Format muss MM-dd-yjyy oder MM/DD/JJJJ Jahre sein";
notifyMessage.INFO_INVALID_AGE =
  "Das Alter muss {{minAgeKey}} und höher sein, um fortzufahren";
notifyMessage.INFO_CARD_ALREADY_EXIST = "Karte gibt es bereits";
notifyMessage.INFO_WALLET_RECHARGE = "Brieftasche aufladen";
notifyMessage.INFO_INTERCITY_RIDE = "Intercity Ride";
notifyMessage.INFO_SITE_COMMISSION = "Standortkommission";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Kredite mit Stornieren";
//--------- Newly added -------------
notifyMessage.INFO_ROUNDING = "Rounding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONAL ALREADY HAVE RIDE";
notifyMessage.INFO_BASE_FARE = "Base Fare";
notifyMessage.INFO_SERVICE_TAX = "Service Tax";
notifyMessage.INFO_TRAVEL_CHARGE = "Travel Charge";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum Charge";
notifyMessage.INFO_SURCHARGE_FEE = "SurCharge Fee";
notifyMessage.INFO_BOUNDARY_CHARGE = "boundary Charge";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE = "Your {{siteTitle}} OTP code is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Earnings";
notifyMessage.INFO_FELLOW_PASSENGER = "FELLOW PASSENGER";
notifyMessage.INFO_ALREADY_EXISTS = "ALREADY EXIST";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Invalid Coupon Code, Please Check Your Code.";
notifyMessage.WALLET_RECHARGE_FAILED = "WALLET RECHARGE FAILED";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "YOUR VEHICLE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "YOUR PROFILE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Your Vehicle Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Your Profile Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Assistance Care Fee";
notifyMessage.INFO_TIME_FARE = "Time Fare";
notifyMessage.INFO_DISTANCE_FARE = "Distance Fare";
notifyMessage.INFO_PEAK_FARE = "Peak Fare";
notifyMessage.INFO_NIGHT_FARE = "Night Fare";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oops! Connection is lost. You’re offline now. Please go online to take ride requests.";
notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your Rating is below the Range, Please improve, or your Account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Your account is activated. Please go online to take ride requests.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Card validation failed";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bank server Busy Now. Please try Later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Verified Successfully";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV Verification Failed";
notifyMessage.ALERT_CHECK_CPF = "Please Check CPF Number.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "Please Check From Date / To Date.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "City Already Exist.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF Number Already Exist.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maximum Withdraw Amount Limit is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Amount Withdrawal Allowed Once Per Day.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Account Holder Name";
notifyMessage.INFO_BANK_NAME = "Bank Name";
notifyMessage.INFO_DOCUMENT_NUMBER = "Document Number";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Wallet withdrawal failed. Amount credited to the wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No Service Location Is Available";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Your fellow passenger has arrived at the pickup location";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Your fellow passenger has started the ride";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "The ride has come to an end for your fellow passenger.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "This Email Is Already Associated With Another Account. Please Use A Different Email.";
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
