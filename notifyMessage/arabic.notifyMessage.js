const notifyMessage = {};

notifyMessage.CREATED = "تم إنشاؤها بنجاح";
notifyMessage.UPDATED = "تم التحديث بنجاح";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "بيانات المستخدم غير صالحة";
notifyMessage.TRIP_END = "انتهت الرحلة";
notifyMessage.WALLET_LOADED = "محفظة محفظة";
notifyMessage.CASH = "نقدي";
notifyMessage.CARD = "بطاقة";
notifyMessage.WALLET = "محفظة";
notifyMessage.TRIP_FEE = "رسوم الرحلة";
notifyMessage.TOTAL_BILL = "إجمالي الفاتورة";
notifyMessage.EARNINGS = "أرباحك";
notifyMessage.WAITING_CHARGE = "تهمة الانتظار";
notifyMessage.PENDING_AMOUNT = "المبلغ المعلق";
notifyMessage.CASH_IN_HAND = "نقد في اليد";
notifyMessage.BOOKING_ACCEPTED = "تم قبول حجزك";
notifyMessage.BOOKING_PAID = "دفع مقابل الحجز";
notifyMessage.PROFESSIONAL_STARTED = "بدأ المحترف في رحلتك";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "تغير الموقع بنجاح";
notifyMessage.PROFESSIONAL_ARRIVED = "وصل المحترف إلى موقع الالتقاط الخاص بك";
notifyMessage.PROFESSIONAL_COMMISSION = "اللجنة المهنية";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "الحجز المستلم";
notifyMessage.PROFESSIONAL_INFO =
  "لقد انخفض رصيد محفظتك عن الحد الأدنى المطلوب لقبول المشاوير النقدية. يرجى إعادة شحن محفظتك أو يمكنك قبول مشاوير البطاقة والمحفظة فقط حتى يصبح رصيدك كافيًا";
notifyMessage.PROFESSIONAL_NOT_FOUND = "المحترف لم يتم العثور عليه";
notifyMessage.PROFESSIONAL_NEW_RIDE = "حصلت على رحلة جديدة";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "تم إلغاء الركوب من قبل المحترفين";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "يتم قبول الحجز الخاص بك من قبل المحترف لدينا";
notifyMessage.PROFESSIONAL_NOTIFICATION = "إرسال الإخطار إلى المهنيين القريبين";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION = "وصل المحترف لوقف الموقع";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "بدء تشغيل الرحلة من موقع التوقف";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "وصل المحترف إلى موقع الحجز الخاص بك";
notifyMessage.PROFESSIONAL_RIDE_START_INFO = "بدأ المحترف من موقع الحجز";
notifyMessage.USER_NOT_FOUND = "المستخدم ليس موجود";
notifyMessage.USER_HAVE_RIDE = "المستخدم لديه بالفعل ركوب";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "حدث خطأ ما في المستخدم إلغاء هذه الرحلة";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "يرجى إعادة شحن محفظتك أو اختر أي خيار دفع آخر";
notifyMessage.USER_PAYMENT_DECLINED =
  "تم رفض طريقة الدفع هذه. الرجاء اختيار طريقة دفع أخرى";
notifyMessage.INVALID_PROFESSIONAL = "تفاصيل مهنية غير صالحة";
notifyMessage.INVALID_BOOKING = "تفاصيل الحجز غير صالحة";
notifyMessage.INVALID_ACCESS = "دخول غير صالح";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "لم يتم العثور على فئة المركبات";
notifyMessage.VEHICLE_NOT_AVAILABLE = "لا توجد مركبة متوفرة";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "لم يتم العثور على فئة الخدمة";
notifyMessage.BOOKING_SCHEDULED_RIDE = "الحجز المجدولة بنجاح";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "لم يتم العثور على فئة الحجز";
notifyMessage.BOOKING_ALREADY = "الحجز بالفعل";
notifyMessage.BOOKING_STATUS_IS = "الحجز";
notifyMessage.BOOKING_STATUS_INFO = "الحجز الجاري بالفعل أو انتهى";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "تم إلغاء الحجز عن طريق فحص السائق لسائق آخر";
notifyMessage.BOOKING_CANCELLED_BY = "تم إلغاء الحجز بواسطة";
notifyMessage.BOOKING_CANCELLED_BY_USER = "تم إلغاء الحجز من قبل المستخدم";
notifyMessage.BOOKING_CANCELLED = "تم إلغاء الحجز بنجاح";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "تم تعيين الحجز الجديد بواسطة المسؤول";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "طلب تعيين بنجاح";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "تم قبول الحجز بالفعل من قبل سائق آخر";
notifyMessage.BOOKING_UPDATE_FAILED =
  "غير قادر على تحديث تفاصيل الحجز خطأ ما حدث خطأ";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "خطأ في تحديث حالة الحجز";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "سبب الإلغاء لم يتم العثور عليه";
notifyMessage.SOMETHING_WENT_WRONG = "سبب الإلغاء لم يتم العثور عليه";
notifyMessage.RIDE_CANCELLED = "تم إلغاء ركوب بنجاح";
notifyMessage.RIDE_IS = "هذه الرحلة";
notifyMessage.RIDE_STATUS_INFO = "لقد انتهت عملية الركوب بالفعل أو لم تبدأ بعد";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "خطأ في تغيير مواقع الركوب";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "لقد تغير عنوان الركوب بواسطة المستخدم";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "تم تغيير عنوان الرحلة بواسطة محترف";
notifyMessage.OTP_INCORRECT = "OTP غير صحيح يرجى إدخال OTP الصحيح";
notifyMessage.RATING_SUBMITED = "تصنيف وضع بنجاح";
notifyMessage.RATING_UPDATE_FAILED = "خطأ في تحديث التصنيف";
notifyMessage.LOCATION_ACCESS_FAILED = "حدث خطأ ما في موقعك";
notifyMessage.TRIP_NOT_AVAILABLE = "النصائح غير متوفرة";
notifyMessage.TRIP_SHARED = "مشاركة الرحلة بنجاح";
notifyMessage.TRIP_END_PROFESSIONAL = "انتهت رحلتك من قبل المحترفين";
notifyMessage.OPERATOR_STATUS_END = "غير مشغلنا حالة رحلتك إلى النهاية";
notifyMessage.ERROR_SECURITY_UPDATE = "خطأ في تحديث الأمان";
notifyMessage.ERROR_GENEREL_SETTINGS = "خطأ في إعدادات generel";
notifyMessage.WISHLIST_ACTION_FAILED = "الإجراء {{Action}} فشل قائمة الرغبات";
notifyMessage.SUR_CHARGE = "تهمة شور";
notifyMessage.SERVICE_TAX = "ضريبة الخدمة";
notifyMessage.TIPS_AMOUNT = "نصائح";
notifyMessage.COUPON_SAVINGS = "مدخرات القسيمة";
notifyMessage.TOLL_FEE = "رسوم حصيلة";
notifyMessage.DISCOUNT_APPLIED = "الخصم قد تم";
notifyMessage.REFUND_TO_WALLET = "استرداد المحفظة";
notifyMessage.TIPS_MINIMUM = "نصائح shoulud يكون الحد الأدنى من";
notifyMessage.TIPS_MAXIMUM = "نصائح shoulud تكون كحد أقصى";
notifyMessage.TIPS_ADDED = "تمت إضافة نصائح بنجاح";
notifyMessage.ERROR_PAYMENT_CARD =
  "خطأ في دفع البطاقة ، يرجى المحاولة بعد وقت ما";
notifyMessage.ERROR_EARNINGS = "خطأ في الأرباح";
notifyMessage.ERROR_DAY_EARNINGS = "خطأ في الأرباح اليومية";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "يبدو أن المحترف هو أوفيلين. يرجى اطلب من المحترف الاتصال بالإنترنت قبل حجز هذه الرحلة";
notifyMessage.INFO_PAYMENT_OPTIONS = "لم يتم العثور على خيارات الدفع";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "حدث خطأ ما في أحداث Gateway تحقق من خطوة بخطوة";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "لا يمكن العثور على المستخدم خطأ ما في إعادة شحن مبلغ محفظة";
notifyMessage.INFO_CHECK_CREDENTIAL = "يرجى التحقق من بيانات الاعتماد التالية";
notifyMessage.INFO_SEND_MONEY = "تم إرسال الأموال بنجاح";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "دفعت مقابل الاشتراك";
notifyMessage.INFO_TIPS = "أعطيت نصائح";
notifyMessage.INFO_JOINING_CHARGE = "انضمام المبلغ";
notifyMessage.INFO_REFERREL_CHARGE = "مبلغ الإحالة";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "تم تحميل مبلغ الدفع المتبقي";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "المبلغ المتبقي من المبلغ الذي تم خصمه";
notifyMessage.WALLET_AMOUNT_LOADED = "تتم إضافة المبلغ إلى محفظتك بنجاح";
notifyMessage.WALLET_NOTIFICATION =
  "حدث خطأ ما في تحديث المحفظة في محفظة لا توجد مصادقة";
notifyMessage.WALLET_WITHDRAW = "مبلغ الانسحاب من المحفظة";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "سحب المحفظة بنجاح";
notifyMessage.WALLET_DEBIT = "محفظة مبلغ خصم";
notifyMessage.ERROR_WALLET_RECHARGE = "حدث خطأ أثناء إعادة شحن المحفظة";
notifyMessage.ERROR_WALLET_WITHDRAW = "خطأ في سحب المحفظة";
notifyMessage.ERROR_PAYMENT_OPTION = "خطأ في قائمة خيارات الدفع";
notifyMessage.ERROR_SEND_MONEY = "خطأ في إرسال المال";
notifyMessage.BANK_CREDET_MONEY =
  "سيكون الأموال مرضية لحساب Yout المصرفي في غضون 7  يومًا عملًا";
notifyMessage.AMOUT_SEND_TO_FRIEND = "نقل إلى صديق";
notifyMessage.BOOKING_CHARGE_FREEZED = "المبلغ المتجمد للحجز";
notifyMessage.RIDE_CANCELLATION_CHARGE = "ركوب مبلغ إلغاء";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "استرداد الخصم المطبق";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "لم يتم العثور على المعاملة";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "لم يتم العثور على الرد ، فقد نفترض أنك مستخدم غير قانوني لعنوان URL هذا";
notifyMessage.INFO_CARD_ADDED = "تمت إضافة البطاقة بنجاح";
notifyMessage.INFO_CARD_VERIFIED = "تم التحقق من البطاقة بنجاح";
notifyMessage.INFO_BANK_DETAILS = "التفاصيل المصرفية";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "نجاح المعاملات";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "لا يمكن العثور على تفاصيل البنك";
notifyMessage.INFO_CARD_DECLINED = "مبلغ خصم من محفظة. بسبب انخفاض البطاقة";
notifyMessage.INFO_AMOUNT_PAID = "المبلغ المدفوع بنجاح";
notifyMessage.ERROR_CARD_ADD = "لا يمكن إضافة بطاقة";
notifyMessage.ERROR_CARD_ADD_FAILED = "حدث شيء خاطئ في إضافة البطاقة";
notifyMessage.ERROR_IN_TRANSACTIONS = "خطأ في المعاملات";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "فشل دفع البطاقة";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "آسف نحن لا نقدم الخدمة هنا";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "تم رفع الملف بنجاح";
notifyMessage.INFO_NOT_FOUND = "التفاصيل غير موجودة";
notifyMessage.INFO_WALLET_POINT_LOADED = "تتم إضافة النقطة إلى محفظتك بنجاح";
notifyMessage.ERROR_WALLET_POINT_ADDED = "غير قادر على محفظة نقطة";
notifyMessage.INFO_SUCCESS = "النجاح";
notifyMessage.INFO_FAILED = "باءت بالفشل";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "استرداد نقاط المكافأة";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "استرداد نقاط المكافأة الحد الأدنى";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "استرداد نقاط المكافأة القصوى";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED = "غير مشغلنا حالة رحلتك إلى";
notifyMessage.INFO_OPERATOR_STATUS_START = "غير مشغلنا حالة رحلتك للبدء";
notifyMessage.INFO_RIDE_CANCELLED_BY = "تم إلغاء Ride بواسطة";
notifyMessage.INFO_PROFESSIONAL = "المحترفين";
notifyMessage.INFO_USER = "المستعمل";
notifyMessage.INFO_ADMIN = "مشرف";
notifyMessage.INFO_INVALID_DETAILS = "تفاصيل غير صالحة";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "رقم الهاتف هذا مسجل بالفعل في اسم الشركة";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "رمز الإحالة غير صالح ، يرجى التحقق من الرمز والمحاولة مرة أخرى";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "معرف البريد الإلكتروني مسجل بالفعل";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "يجب أن يكون تنسيق DOB غير صالح MM-DD-YYYY أو MM / DD / YYYY";
notifyMessage.INFO_INVALID_AGE =
  "يجب أن يكون العمر {{minAgeKey}} وما فوق للمتابعة";
notifyMessage.INFO_CARD_ALREADY_EXIST = "البطاقة موجودة بالفعل";
notifyMessage.INFO_WALLET_RECHARGE = "إعادة شحن المحفظة";
notifyMessage.INFO_INTERCITY_RIDE = "ركوب بين المدن";
notifyMessage.INFO_SITE_COMMISSION = "لجنة الموقع";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "رصيد إلغاء الركوب";
notifyMessage.INFO_ROUNDING = "التقريب";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "المهنية بالفعل ركوب";
notifyMessage.INFO_BASE_FARE = "الاجرة الاساسية";
notifyMessage.INFO_SERVICE_TAX = "ضريبة الخدمة";
notifyMessage.INFO_TRAVEL_CHARGE = "رسوم السفر";
notifyMessage.INFO_MINIMUM_CHARGE = "الحد الأدنى للطلب";
notifyMessage.INFO_SURCHARGE_FEE = "رسوم إضافية";
notifyMessage.INFO_BOUNDARY_CHARGE = "تهمة الحدود";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "رسوم الالتقاط بين المدن";
notifyMessage.INFO_OTP_MESSAGE = "رمز OTP الخاص بك {{siteTitle}} هو {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "لقد تم قفل حسابك لأنك وصلت إلى الحد الأقصى لعدد محاولات تسجيل الدخول غير الصالحة.";
notifyMessage.INFO_HUBS_EARNINGS = "أرباح المراكز";
notifyMessage.INFO_FELLOW_PASSENGER = "زميل الركاب";
notifyMessage.INFO_ALREADY_EXISTS = "موجود أصلا";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "رمز القسيمة غير صالح، يرجى التحقق من الرمز الخاص بك.";
notifyMessage.WALLET_RECHARGE_FAILED = "فشلت عملية إعادة شحن المحفظة";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "انتهت صلاحية وثائق سيارتك. يرجى التحقق والتحديث";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "انتهت صلاحية وثائق ملفك الشخصي. يرجى التحقق والتحديث";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "ستنتهي صلاحية وثائق سيارتك خلال {{days}} يوم (أيام). يرجى التحقق والتحديث";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "ستنتهي صلاحية مستندات ملفك الشخصي خلال {{days}} يوم (أيام). يرجى التحقق والتحديث";
notifyMessage.INFO_INCENTIVE_CREDIT = "الحافز المعتمد";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "رسوم رعاية المساعدة";
notifyMessage.INFO_TIME_FARE = "أجرة الوقت";
notifyMessage.INFO_DISTANCE_FARE = "أجرة المسافة";
notifyMessage.INFO_PEAK_FARE = "أجرة الذروة";
notifyMessage.INFO_NIGHT_FARE = "أجرة الليل";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "أُووبس! تم فقدان الاتصال. أنت غير متصل الآن. يرجى الاتصال بالإنترنت لتلقي طلبات الركوب.";
notifyMessage.AIRPORT_FEE = "رسوم المطار";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "هذا المحترف لا يقبل خيار الدفع المحدد. يرجى تغيير خيار الدفع.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "تقييمك أقل من النطاق، يرجى تحسينه، وإلا سيتم حظر حسابك خلال أيام قليلة.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "لقد تم حظر حسابك لأن تقييمك أقل من النطاق. سيتم إعادة تنشيط الحساب في منتصف ليل {{date}}.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "تم تفعيل حسابك. يرجى الاتصال بالإنترنت لتلقي طلبات الركوب.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "المبلغ المحول من صديق";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "فشل التحقق من صحة البطاقة";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "خادم البنك مشغول الآن. يرجى المحاولة لاحقا";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "تم التحقق من CVV بنجاح";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "فشل التحقق من CVV";
notifyMessage.ALERT_CHECK_CPF = "يرجى التحقق من رقم CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "يرجى التحقق من التاريخ / حتى الآن.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "المدينة موجودة بالفعل.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "رقم CPF موجود بالفعل.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "الحد الأقصى لمبلغ السحب هو {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "المبلغ المسموح بسحبه مرة واحدة في اليوم.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "اسم صاحب الحساب";
notifyMessage.INFO_BANK_NAME = "اسم البنك";
notifyMessage.INFO_DOCUMENT_NUMBER = "رقم المستند";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "فشل سحب المحفظة. المبلغ المضاف إلى المحفظة.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE = "لا يوجد موقع خدمة متاح";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "لقد وصل زميلك الراكب إلى موقع الالتقاء";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "لقد بدأ زميلك الراكب الرحلة";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "لقد انتهت الرحلة بالنسبة لزملائك الركاب.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "هذا البريد الإلكتروني مرتبط بالفعل بحساب آخر. الرجاء استخدام بريد إلكتروني مختلف.";
notifyMessage.INFO_TRIP_NUMBER = "رقم الرحلة";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "موقع الخدمة موجود بالفعل.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "الشركة موجودة بالفعل.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "منطقة الخدمة موجودة بالفعل.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "جاري مراجعة مستندك، يرجى الانتظار حتى اكتمال عملية التحقق.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
