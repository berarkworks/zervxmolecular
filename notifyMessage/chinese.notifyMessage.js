const notifyMessage = {};

notifyMessage.CREATED = "成功创建";
notifyMessage.UPDATED = "更新成功";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "无效的用户数据";
notifyMessage.TRIP_END = "旅行已经结束";
notifyMessage.WALLET_LOADED = "钱包量加载";
notifyMessage.CASH = "现金";
notifyMessage.CARD = "卡片";
notifyMessage.WALLET = "钱包";
notifyMessage.TRIP_FEE = "旅行费";
notifyMessage.TOTAL_BILL = "总账单";
notifyMessage.EARNINGS = "您的收入";
notifyMessage.WAITING_CHARGE = "等待充电";
notifyMessage.PENDING_AMOUNT = "待定金额";
notifyMessage.CASH_IN_HAND = "手中的现金";
notifyMessage.BOOKING_ACCEPTED = "您的预订被接受";
notifyMessage.BOOKING_PAID = "为预订而付费";
notifyMessage.PROFESSIONAL_STARTED = "专业开始您的旅行";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "位置成功改变了";
notifyMessage.PROFESSIONAL_ARRIVED = "专业人士到达您的接送位置";
notifyMessage.PROFESSIONAL_COMMISSION = "专业委员会";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "收到的预订收入";
notifyMessage.PROFESSIONAL_INFO =
  "您的钱包余额已低于接受现金乘车所需的最低限额。请为您的钱包充值，否则您只能接受卡和钱包乘车，直到您的余额足够";
notifyMessage.PROFESSIONAL_NOT_FOUND = "未找到专业";
notifyMessage.PROFESSIONAL_NEW_RIDE = "你有了新的旅程";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "被专业人士取消";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING = "您的预订已被我们的专业人员接受";
notifyMessage.PROFESSIONAL_NOTIFICATION = "通知发送给附近的专业人员";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION = "专业人士到达位置";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION = "专业从停车位置开始";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "专业人士到达您的预订地点";
notifyMessage.PROFESSIONAL_RIDE_START_INFO = "专业从预订位置开始";
notifyMessage.USER_NOT_FOUND = "找不到用户";
notifyMessage.USER_HAVE_RIDE = "用户已经有骑车";
notifyMessage.USER_RIDE_CANCEL_FAILD = "用户取消此骑行有问题";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "请为您的钱包充电或选择任何其他付款选项";
notifyMessage.USER_PAYMENT_DECLINED =
  "此付款方式已被拒绝。请选择另一种付款方式";
notifyMessage.INVALID_PROFESSIONAL = "无效的专业详细信息";
notifyMessage.INVALID_BOOKING = "预订细节无效";
notifyMessage.INVALID_ACCESS = "无效访问";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "找不到车辆类别";
notifyMessage.VEHICLE_NOT_AVAILABLE = "没有车辆可用";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "未找到服务类别";
notifyMessage.BOOKING_SCHEDULED_RIDE = "预订计划成功";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "未找到预订类别";
notifyMessage.BOOKING_ALREADY = "预订已经是";
notifyMessage.BOOKING_STATUS_IS = "预订是";
notifyMessage.BOOKING_STATUS_INFO = "预订已经进行或结束";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "通过驾驶员检查另一个驾驶员而取消的预订";
notifyMessage.BOOKING_CANCELLED_BY = "预订取消";
notifyMessage.BOOKING_CANCELLED_BY_USER = "用户取消预订";
notifyMessage.BOOKING_CANCELLED = "预订成功取消";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "新预订由管理员分配";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "成功分配的请求";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS = "预订已经被另一个驾驶员接受";
notifyMessage.BOOKING_UPDATE_FAILED = "无法更新预订详细说明了问题";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "预订状态更新错误";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "未找到取消原因";
notifyMessage.SOMETHING_WENT_WRONG = "未找到取消原因";
notifyMessage.RIDE_CANCELLED = "骑行成功取消";
notifyMessage.RIDE_IS = "这次旅程是";
notifyMessage.RIDE_STATUS_INFO = "骑行已经结束或尚未开始";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "更改乘车位置的错误";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER = "用户改变了乘车地址";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL = "乘车地址已被专业人士更改";
notifyMessage.OTP_INCORRECT = "OTP不正确输入正确的OTP";
notifyMessage.RATING_SUBMITED = "评级成功提交";
notifyMessage.RATING_UPDATE_FAILED = "评级更新错误";
notifyMessage.LOCATION_ACCESS_FAILED = "您所在的位置出了问题";
notifyMessage.TRIP_NOT_AVAILABLE = "提示不可用";
notifyMessage.TRIP_SHARED = "旅行成功分享";
notifyMessage.TRIP_END_PROFESSIONAL = "您的骑行以专业人士结束";
notifyMessage.OPERATOR_STATUS_END = "我们的运营商将您的旅行状态更改为结束";
notifyMessage.ERROR_SECURITY_UPDATE = "安全更新错误";
notifyMessage.ERROR_GENEREL_SETTINGS = "Generel设置中的错误";
notifyMessage.WISHLIST_ACTION_FAILED = "操作{{action}}愿望清单失败";
notifyMessage.SUR_CHARGE = "SUR收费";
notifyMessage.SERVICE_TAX = "服务税";
notifyMessage.TIPS_AMOUNT = "提示金额";
notifyMessage.COUPON_SAVINGS = "优惠券储蓄";
notifyMessage.TOLL_FEE = "过路费";
notifyMessage.DISCOUNT_APPLIED = "适用折扣";
notifyMessage.REFUND_TO_WALLET = "退还钱包";
notifyMessage.TIPS_MINIMUM = "提示至少";
notifyMessage.TIPS_MAXIMUM = "提示最大";
notifyMessage.TIPS_ADDED = "提示成功添加了";
notifyMessage.ERROR_PAYMENT_CARD = "卡付款错误，一段时间后请尝试";
notifyMessage.ERROR_EARNINGS = "收入错误";
notifyMessage.ERROR_DAY_EARNINGS = "日收入错误";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "看来专业人士是奥斯丁。请要求专业人士在预订这次旅行之前上网";
notifyMessage.INFO_PAYMENT_OPTIONS = "找不到付款选项";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT = "网关事件中出现了问题，请逐步检查";
notifyMessage.INFO_USER_WALLET_NOT_FOUND = "找不到用户在钱包金额中出现问题";
notifyMessage.INFO_CHECK_CREDENTIAL = "请检查下一个凭据";
notifyMessage.INFO_SEND_MONEY = "钱成功寄出";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "付费订阅";
notifyMessage.INFO_TIPS = "你给了提示";
notifyMessage.INFO_JOINING_CHARGE = "加入金额";
notifyMessage.INFO_REFERREL_CHARGE = "推荐金额";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "剩余的付款金额已加载";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "剩余的付款金额扣除";
notifyMessage.WALLET_AMOUNT_LOADED = "金额已成功添加到您的钱包中";
notifyMessage.WALLET_NOTIFICATION = "钱包更新中的钱包中出现了问题";
notifyMessage.WALLET_WITHDRAW = "从钱包中撤出的金额";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "钱包成功撤离";
notifyMessage.WALLET_DEBIT = "钱包扣除";
notifyMessage.ERROR_WALLET_RECHARGE = "钱包充电时发生错误";
notifyMessage.ERROR_WALLET_WITHDRAW = "钱包提取错误";
notifyMessage.ERROR_PAYMENT_OPTION = "付款选项列表中的错误";
notifyMessage.ERROR_SEND_MONEY = "汇款错误";
notifyMessage.BANK_CREDET_MONEY = "金钱将在 7 个工作日内信用到Yout银行帐户";
notifyMessage.AMOUT_SEND_TO_FRIEND = "转移给朋友";
notifyMessage.BOOKING_CHARGE_FREEZED = "预订的冻结金额";
notifyMessage.RIDE_CANCELLATION_CHARGE = "乘车取消金额";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "折扣的退款";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "找不到交易";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "答复没有发现我们假设您是此URL的非法用户";
notifyMessage.INFO_CARD_ADDED = "卡成功添加";
notifyMessage.INFO_CARD_VERIFIED = "卡成功验证";
notifyMessage.INFO_BANK_DETAILS = "银行明细";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "交易成功";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "找不到银行详细信息";
notifyMessage.INFO_CARD_DECLINED = "从钱包中扣除的金额。由于卡而下降";
notifyMessage.INFO_AMOUNT_PAID = "成功支付的金额";
notifyMessage.ERROR_CARD_ADD = "无法添加卡";
notifyMessage.ERROR_CARD_ADD_FAILED = "卡中出现了问题";
notifyMessage.ERROR_IN_TRANSACTIONS = "交易错误";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "卡付款失败";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "抱歉，我们在这里不提供服务";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "文件上传成功";
notifyMessage.INFO_NOT_FOUND = "找不到细节";
notifyMessage.INFO_WALLET_POINT_LOADED = "点已成功地添加到您的钱包中";
notifyMessage.ERROR_WALLET_POINT_ADDED = "无法钱包";
notifyMessage.INFO_SUCCESS = "成功";
notifyMessage.INFO_FAILED = "失败的";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "赎回奖励积分";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "赎回最低奖励积分";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "赎回最大奖励积分";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "我们的操作员将您的旅行状态更改为到达";
notifyMessage.INFO_OPERATOR_STATUS_START = "我们的运营商改变了您的旅行状态";
notifyMessage.INFO_RIDE_CANCELLED_BY = "乘车取消了";
notifyMessage.INFO_PROFESSIONAL = "专业的";
notifyMessage.INFO_USER = "用户";
notifyMessage.INFO_ADMIN = "行政";
notifyMessage.INFO_INVALID_DETAILS = "无效的细节";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS = "此电话号码已注册为公司名称";
notifyMessage.INFO_INVALID_REFERRAL_CODE = "推荐代码无效，请检查您的代码并重试";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "电子邮件 ID 已注册";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "无效的 DOB 格式必须是 MM-DD-YYYY 或 MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE = "年龄必须在 {{minAgeKey}} 岁及以上才能继续";
notifyMessage.INFO_CARD_ALREADY_EXIST = "卡已存在";
notifyMessage.INFO_WALLET_RECHARGE = "钱包充值";
notifyMessage.INFO_INTERCITY_RIDE = "城际乘车";
notifyMessage.INFO_SITE_COMMISSION = "工地委托";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "乘車取消積分";
//--------- Newly added -------------
notifyMessage.INFO_ROUNDING = "Rounding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONAL ALREADY HAVE RIDE";
notifyMessage.INFO_BASE_FARE = "Base Fare";
notifyMessage.INFO_SERVICE_TAX = "Service Tax";
notifyMessage.INFO_TRAVEL_CHARGE = "Travel Charge";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum Charge";
notifyMessage.INFO_SURCHARGE_FEE = "SurCharge Fee";
notifyMessage.INFO_BOUNDARY_CHARGE = "boundary Charge";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE = "Your {{siteTitle}} OTP code is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Earnings";
notifyMessage.INFO_FELLOW_PASSENGER = "FELLOW PASSENGER";
notifyMessage.INFO_ALREADY_EXISTS = "ALREADY EXIST";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Invalid Coupon Code, Please Check Your Code.";
notifyMessage.WALLET_RECHARGE_FAILED = "WALLET RECHARGE FAILED";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "YOUR VEHICLE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "YOUR PROFILE DOCUMENTS IS EXPIRED. PLEASE CHECK AND UPDATE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Your Vehicle Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Your Profile Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Assistance Care Fee";
notifyMessage.INFO_TIME_FARE = "Time Fare";
notifyMessage.INFO_DISTANCE_FARE = "Distance Fare";
notifyMessage.INFO_PEAK_FARE = "Peak Fare";
notifyMessage.INFO_NIGHT_FARE = "Night Fare";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oops! Connection is lost. You’re offline now. Please go online to take ride requests.";
notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your Rating is below the Range, Please improve, or your Account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Your account is activated. Please go online to take ride requests.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Card validation failed";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bank server Busy Now. Please try Later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Verified Successfully";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV Verification Failed";
notifyMessage.ALERT_CHECK_CPF = "Please Check CPF Number.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "Please Check From Date / To Date.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "City Already Exist.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF Number Already Exist.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maximum Withdraw Amount Limit is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Amount Withdrawal Allowed Once Per Day.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Account Holder Name";
notifyMessage.INFO_BANK_NAME = "Bank Name";
notifyMessage.INFO_DOCUMENT_NUMBER = "Document Number";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Wallet withdrawal failed. Amount credited to the wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No Service Location Is Available";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Your fellow passenger has arrived at the pickup location";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Your fellow passenger has started the ride";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "The ride has come to an end for your fellow passenger.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "This Email Is Already Associated With Another Account. Please Use A Different Email.";
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
