const notifyMessage = {};

notifyMessage.CREATED = "Criado com sucesso";
notifyMessage.UPDATED = "Atualizado com sucesso";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "DADOS DE USUÁRIO INVÁLIDOS";
notifyMessage.TRIP_END = "Viagem concluida";
notifyMessage.WALLET_LOADED = "Recarga na carteira";
notifyMessage.CASH = "Dinheiro";
notifyMessage.CARD = "Cartão";
notifyMessage.WALLET = "Carteira";
notifyMessage.TRIP_FEE = "Tarifa da viagem";
notifyMessage.TOTAL_BILL = "Valor total";
notifyMessage.EARNINGS = "Seu ganho";
notifyMessage.WAITING_CHARGE = "Taxa de espera";
notifyMessage.PENDING_AMOUNT = "valor pendente";
notifyMessage.CASH_IN_HAND = "Dinheiro em mãos";

notifyMessage.BOOKING_ACCEPTED = "Sua viagem foi aceita";
notifyMessage.BOOKING_PAID = "Pago pela viagem";

//PROFESSIONAL
notifyMessage.PROFESSIONAL_STARTED = "O motorista iniciou a viagem";
notifyMessage.PROFESSIONAL_ARRIVED = "O motorista chegou";
notifyMessage.PROFESSIONAL_COMMISSION = "Comissão do profissional";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Ganho pela viagem";

notifyMessage.PROFESSIONAL_INFO =
  "O saldo da sua carteira ficou abaixo do limite mínimo exigido para aceitar viagens em dinheiro. Por favor, recarregue sua carteira ou você só pode aceitar passeios com cartão e carteira até que seu saldo seja suficiente";
notifyMessage.PROFESSIONAL_NOT_FOUND = "MOTORISTA NÃO ENCONTRADO";
notifyMessage.PROFESSIONAL_NEW_RIDE = "NOVA SOLICITAÇÃO DE VIAGEM";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "VIAGEM CANCELADA PELO MOTORISTA";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "SUA VIAGEM FOI ACEITA PELO MOTORISTA";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "NOTIFICAÇÃO ENVIADA A MOTORISTAS PRÓXIMOS";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "MOTORISTA CHEGOU AO PONTO DE PARADA";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "MOTORISTA INICIOU DO PONTO DE PARADA";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "MOTORISTA CHEGOU AO SEU LOCAL DE RESERVA";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "MOTORISTA INICIADO NO LOCAL DA RESERVA";

notifyMessage.USER_NOT_FOUND = "USUÁRIO NÃO ENCONTRADO";
notifyMessage.USER_HAVE_RIDE = "O USUÁRIO JÁ TEM VIAGEM";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "ALGO DEU ERRADO AO USUÁRIO CANCELAR ESTA VIAGEM";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "POR FAVOR, RECARREGUE SUA CARTEIRA OU ESCOLHA QUALQUER OUTRA OPÇÃO DE PAGAMENTO";
notifyMessage.USER_PAYMENT_DECLINED =
  "Este método de pagamento foi recusado. Por favor, escolha outro método de pagamento";

notifyMessage.INVALID_PROFESSIONAL = "Detalhes profissionais inválidos";
notifyMessage.INVALID_BOOKING = "Detalhes de reserva inválidos";
notifyMessage.INVALID_ACCESS = "ACESSO INVÁLIDO";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND =
  "CATEGORIA DE VEÍCULO NÃO ENCONTRADA";
notifyMessage.VEHICLE_NOT_AVAILABLE = "NENHUM VEÍCULO ESTÁ DISPONÍVEL";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND =
  "CATEGORIA DE SERVIÇO NÃO ENCONTRADA";

notifyMessage.BOOKING_SCHEDULED_RIDE = "VIAGEM AGENDADA COM SUCESSO";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND =
  "CATEGORIA DE RESERVA NÃO ENCONTRADA";
notifyMessage.BOOKING_ALREADY = "RESERVA JÁ EXISTE";
notifyMessage.BOOKING_STATUS_IS = "A RESERVA É";
notifyMessage.BOOKING_STATUS_INFO = "RESERVA JÁ EM ANDAMENTO OU TERMINADA";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "RESERVA CANCELADA PELO MOTORISTA, A VERIFICAR OUTRO MOTORISTA";
notifyMessage.BOOKING_CANCELLED_BY = "RESERVA CANCELADA POR";
notifyMessage.BOOKING_CANCELLED_BY_USER = "RESERVA CANCELADA PELO USUÁRIO";
notifyMessage.BOOKING_CANCELLED = "RESERVA CANCELADA COM SUCESSO";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "NOVA RESERVA É ATRIBUÍDA PELO ADMINISTRADOR";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "SOLICITAÇÃO ATRIBUÍDA COM SUCESSO";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "A RESERVA JÁ FOI ACEITA POR OUTRO MOTORISTA";
notifyMessage.BOOKING_UPDATE_FAILED =
  "NÃO É POSSÍVEL ATUALIZAR OS DETALHES DA RESERVA, ALGO DEU ERRADO";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "ERRO NA ATUALIZAÇÃO DO STATUS DA RESERVA";

notifyMessage.CANCELLATION_REASON_NOT_FOUND =
  "MOTIVO DO CANCELAMENTO NÃO ENCONTRADO";

notifyMessage.SOMETHING_WENT_WRONG = "ALGO DEU ERRADO EM DETALHES";

notifyMessage.RIDE_CANCELLED = "VIAGEM CANCELADA COM SUCESSO";
notifyMessage.RIDE_IS = "ESTA VIAGEM É";
notifyMessage.RIDE_STATUS_INFO = "VIAGEM JÁ CONCLUIDA OU AINDA NÃO COMEÇOU";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "ERRO NA ALTERAÇÃO DE LOCAIS DA VIAGEM";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "O ENDEREÇO DA VIAGEM FOI ALTERADO PELO USUÁRIO";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "O endereço da viagem foi alterado pelo profissional";
notifyMessage.OTP_INCORRECT = "OTP ESTÁ INCORRETO POR FAVOR INSIRA OTP CORRETO";

notifyMessage.RATING_SUBMITED = "CLASSIFICAÇÃO ENVIADA COM SUCESSO";
notifyMessage.RATING_UPDATE_FAILED = "ERRO NA ATUALIZAÇÃO DA CLASSIFICAÇÃO";

notifyMessage.LOCATION_ACCESS_FAILED = "Algo deu errado na sua localização";

notifyMessage.TRIP_NOT_AVAILABLE = "GORJETAS NÃO ESTÃO DISPONÍVEIS";
notifyMessage.TRIP_SHARED = "VIAGEM COMPARTILHADA COM SUCESSO";
notifyMessage.TRIP_END_PROFESSIONAL =
  "SUA VIAGEM FOI FINALIZADA PELO MOTORISTA";

notifyMessage.OPERATOR_STATUS_END =
  "NOSSA OPERADORA ALTEROU SEU STATUS DE VIAGEM PARA CONCLUIDA";

notifyMessage.ERROR_SECURITY_UPDATE = "ERRO NA ATUALIZAÇÃO DE SEGURANÇA";
notifyMessage.ERROR_GENEREL_SETTINGS = "ERRO NAS CONFIGURAÇÕES GERAIS";

notifyMessage.WISHLIST_ACTION_FAILED =
  "AÇÃO {{action}} LISTA DE DESEJOS FALHOU";

notifyMessage.SUR_CHARGE = "Sobretaxa";
notifyMessage.SERVICE_TAX = "Taxa de serviço";
notifyMessage.TIPS_AMOUNT = "Valor das gorjetas";
notifyMessage.COUPON_SAVINGS = "Cupom de desconto";
notifyMessage.TOLL_FEE = "Taxa de pedágio";
notifyMessage.DISCOUNT_APPLIED = "Desconto aplicado";
notifyMessage.REFUND_TO_WALLET = "Reembolso para a carteira";

notifyMessage.TIPS_MINIMUM = "AS GORJETAS DEVEM SER NO MÍNIMO DE";
notifyMessage.TIPS_MAXIMUM = "AS GORJETASDEVEM SER NO MÁXIMO DE";
notifyMessage.TIPS_ADDED = "GORJETAS ADICIONADAS COM SUCESSO";

notifyMessage.ERROR_PAYMENT_CARD =
  "ERRO NO PAGAMENTO COM CARTÃO, POR FAVOR, TENTE DEPOIS DE ALGUM MOMENTO";
notifyMessage.ERROR_EARNINGS = "ERRO NOS LUCROS";
notifyMessage.ERROR_DAY_EARNINGS = "ERRO NO GANHO DIÁRIO";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "PARECE QUE O MOTORISTA ESTÁ OFFLINE. PEÇA AO MOTORISTA PARA ENTRAR ONLINE ANTES DE RESERVAR ESTA VIAGEM";
//---------
notifyMessage.INFO_PAYMENT_OPTIONS = "OPÇÕES DE PAGAMENTO NÃO ENCONTRADA";
notifyMessage.INFO_CARD = "NÃO CONSIGO ENCONTRAR OS DETALHES DO CARTÃO";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "ALGO DEU ERRADO NOS EVENTOS DE GATEWAY VERIFIQUE ATENTAMENTE PASSO A PASSO";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "NÃO CONSIGO ENCONTRAR O USUÁRIO, ALGO ERRADO NA RECARGA DO VALOR DA CARTEIRA";
notifyMessage.INFO_CHECK_CREDENTIAL = "VERIFIQUE A PRÓXIMA CREDENCIAL";
notifyMessage.INFO_SEND_MONEY = "DINHEIRO ENVIADO COM SUCESSO";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Assinatura paga";
notifyMessage.INFO_TIPS = "Você deu gorjetas";
notifyMessage.INFO_JOINING_CHARGE = "Valor de adesão";
notifyMessage.INFO_REFERREL_CHARGE = "Valor da indicação";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Valor de pagamento restante carregado";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Valor de pagamento restante debitado";

notifyMessage.WALLET_AMOUNT_LOADED =
  "O VALOR FOI ADICIONADO À SUA CARTEIRA COM SUCESSO";
notifyMessage.WALLET_NOTIFICATION =
  "ALGO DEU ERRADO NA ATUALIZAÇÃO NA CARTEIRA RECARREGUE SEM AUTH";
notifyMessage.WALLET_WITHDRAW = "QUANTIA RETIRADA DA CARTEIRA";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "RETIRADA DA CARTEIRA COM SUCESSO";
notifyMessage.WALLET_DEBIT = "Valor debitado da carteira";

notifyMessage.ERROR_WALLET_RECHARGE = "OCORREU ERRO AO RECARREGAR A CARTEIRA";
notifyMessage.ERROR_WALLET_WITHDRAW = "ERRO NA RETIRADA DA CARTEIRA";
notifyMessage.ERROR_PAYMENT_OPTION = "ERRO NA LISTA DE OPÇÕES DE PAGAMENTO";
notifyMessage.ERROR_SEND_MONEY = "ERRO AO ENVIAR DINHEIRO";

notifyMessage.BANK_CREDET_MONEY =
  "O DINHEIRO SERÁ CREDITADO NA SUA CONTA BANCÁRIA DENTRO DE 5 A 10 DIAS ÚTEIS";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Transferido para um amigo";

notifyMessage.BOOKING_CHARGE_FREEZED = "Valor congelado para reserva";

notifyMessage.RIDE_CANCELLATION_CHARGE = "Valor de cancelamento de viagem";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "Reembolso do desconto aplicado";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "TRANSAÇÃO NÃO ENCONTRADA";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "RESPOSTA NÃO ENCONTRADA, ASSUMIMOS QUE VOCÊ É UM USUÁRIO ILEGAL DESTE URL";
notifyMessage.INFO_CARD_ADDED = "O CARTÃO FOI ADICIONADO COM SUCESSO";
notifyMessage.INFO_CARD_VERIFIED = "CARTÃO VERIFICADO COM SUCESSO";
notifyMessage.INFO_BANK_DETAILS = "DETALHES BANCÁRIOS";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "SUCESSO DA TRANSAÇÃO";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "NÃO ENCONTREI OS DADOS BANCÁRIOS";
notifyMessage.INFO_CARD_DECLINED =
  "VALOR DEDUZIDO DA CARTEIRA. POR CARTÃO RECUSADO";
notifyMessage.INFO_AMOUNT_PAID = "VALOR PAGO COM SUCESSO";

notifyMessage.ERROR_CARD_ADD = "NÃO É POSSÍVEL ADICIONAR CARTÃO";
notifyMessage.ERROR_CARD_ADD_FAILED = "ALGO DEU ERRADO NO CARTÃO";
notifyMessage.ERROR_IN_TRANSACTIONS = "ERRO NAS TRANSAÇÕES";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "FALHA NO PAGAMENTO DO CARTÃO";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "DESCULPE NÃO OFERECEMOS ATENDIMENTO AQUI";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Arquivo enviado com sucesso";
notifyMessage.INFO_NOT_FOUND = "DETALHES NÃO ENCONTRADOS";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "O PONTO É ADICIONADO À SUA CARTEIRA COM SUCESSO";
notifyMessage.ERROR_WALLET_POINT_ADDED = "NÃO CONSEGUIU ACESSAR O WALLET POINT";
notifyMessage.INFO_SUCCESS = "SUCESSO";
notifyMessage.INFO_FAILED = "FRACASSADO";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "Resgatar pontos de recompensa";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Resgatar pontos mínimos de recompensa";
notifyMessage.TRAN_REDEEM_REWARD_MAX =
  "Resgatar o máximo de pontos de recompensa";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "NOSSA OPERADORA ALTEROU SEU STATUS DE VIAGEM PARA CHEGOU  ";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "NOSSA OPERADORA ALTEROU SEU STATUS DE VIAGEM PARA INICIAR  ";
notifyMessage.INFO_RIDE_CANCELLED_BY = "PASSEIO CANCELADO POR";
notifyMessage.INFO_PROFESSIONAL = "PROFISSIONAL";
notifyMessage.INFO_USER = "DO UTILIZADOR";
notifyMessage.INFO_ADMIN = "ADMINISTRADOR";
notifyMessage.INFO_INVALID_DETAILS = "Detalhes inválidos";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Este número de telefone já está registrado em nome corporativo";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "CÓDIGO DE REFERÊNCIA INVÁLIDO, VERIFIQUE SEU CÓDIGO E TENTE NOVAMENTE";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "Id de e-mail já registrado";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "FORMATO DOB INVÁLIDO DEVE SER MM-DD-AAAA OU MM/DD/AAAA";
notifyMessage.INFO_INVALID_AGE =
  "A IDADE DEVE SER DE  {{minAgeKey}} E ACIMA PARA PROSSEGUIR";
notifyMessage.INFO_CARD_ALREADY_EXIST = "O CARTÃO JÁ EXISTE";
notifyMessage.INFO_WALLET_RECHARGE = "Recarga de Carteira";
notifyMessage.INFO_INTERCITY_RIDE = "passeio intermunicipal";
notifyMessage.INFO_SITE_COMMISSION = "Comissão local";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Crédito do cancelamento da viagem";
notifyMessage.INFO_ROUNDING = "arredondamento";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFISSIONAL JÁ PASSOU";
notifyMessage.INFO_BASE_FARE = "Tarifa base";
notifyMessage.INFO_SERVICE_TAX = "Taxa de serviço";
notifyMessage.INFO_TRAVEL_CHARGE = "Taxa de viagem";
notifyMessage.INFO_MINIMUM_CHARGE = "Carga Mínima";
notifyMessage.INFO_SURCHARGE_FEE = "Taxa de sobretaxa";
notifyMessage.INFO_BOUNDARY_CHARGE = "carga limite";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Taxa de coleta intermunicipal";
notifyMessage.INFO_OTP_MESSAGE =
  "{{siteTitle}}: {{OTP}} é o seu código de verificação."; //"Seu codigo OTP {{siteTitle}} é {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Sua conta foi bloqueada porque você atingiu o número máximo de tentativas de logon inválidas.";
notifyMessage.INFO_HUBS_EARNINGS = "comissão da loja Ganhos";
notifyMessage.INFO_FELLOW_PASSENGER = "companheiro de viagem";
notifyMessage.INFO_ALREADY_EXISTS = "JÁ EXISTE";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Código de cupom inválido, verifique seu código.";
notifyMessage.WALLET_RECHARGE_FAILED = "FALHA NA RECARGA DA CARTEIRA";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "OS DOCUMENTOS DO SEU VEÍCULO ESTÃO EXPIRADOS. VERIFIQUE E ATUALIZE";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "SEUS DOCUMENTOS DE PERFIL ESTÃO EXPIRADOS. VERIFIQUE E ATUALIZE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Os documentos do seu veículo expirarão em {{days}} dia(s). Verifique e atualize";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Os documentos do seu perfil expirarão em {{days}} dia(s). Verifique e atualize";
notifyMessage.INFO_INCENTIVE_CREDIT = "Incentivo creditado";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Taxa de Assistência";
notifyMessage.INFO_TIME_FARE = "Tarifa horária";
notifyMessage.INFO_DISTANCE_FARE = "Tarifa de distância";
notifyMessage.INFO_PEAK_FARE = "Tarifa de pico";
notifyMessage.INFO_NIGHT_FARE = "Tarifa Noturna";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Ops! A conexão foi perdida. Você está off-line agora. Por favor, acesse a Internet para receber solicitações de viagem.";
notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Este profissional não aceita a opção de pagamento selecionada. Altere a opção de pagamento.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Sua classificação está abaixo da faixa. Melhore ou sua conta será bloqueada em alguns dias.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Sua conta foi bloqueada porque sua classificação está abaixo da faixa. A conta será reativada à meia-noite de {{date}}.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Twoje konto zostało aktywowane. Przejdź do trybu online, aby odebrać zamówienie na przejazd.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Valor transferido de amigo";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Falha na validação do cartão";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Servidor do banco ocupado agora. Por favor, tente mais tarde";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV verificado com sucesso";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Falha na verificação do CVV";
notifyMessage.ALERT_CHECK_CPF = "Verifique o número do CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Verifique a data inicial/até a data.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "A cidade já existe.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "O número do CPF já existe.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "O limite máximo de saque é {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Retirada de valor permitida uma vez por dia.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Nome do titular da conta";
notifyMessage.INFO_BANK_NAME = "Nome do banco";
notifyMessage.INFO_DOCUMENT_NUMBER = "Número do documento";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Falha na retirada da carteira. Valor creditado na carteira.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Nenhum local de serviço está disponível";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Seu companheiro de viagem chegou ao local de coleta";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Seu companheiro de viagem iniciou a viagem";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "A viagem chegou ao fim para o seu companheiro de viagem.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Este e-mail já está associado a outra conta. Use um e-mail diferente.";
notifyMessage.INFO_TRIP_NUMBER = "Número da viagem";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "O local do serviço já existe.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "Corporativo já existe.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "A área de serviço já existe.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "A verificação do seu documento está em revisão. Aguarde até que a verificação seja concluída";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
