const notifyMessage = {};

notifyMessage.CREATED = "Imeundwa kwa mafanikio";
notifyMessage.UPDATED = "Imesasishwa kwa mafanikio";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Data batili ya mtumiaji";
notifyMessage.TRIP_END = "Safari imemalizika";
notifyMessage.WALLET_LOADED = "Kiasi cha mkoba kimejaa";
notifyMessage.CASH = "Fedha";
notifyMessage.CARD = "Kadi";
notifyMessage.WALLET = "Mkoba";
notifyMessage.TRIP_FEE = "Ada ya safari";
notifyMessage.TOTAL_BILL = "Jumla ya muswada";
notifyMessage.EARNINGS = "Mapato yako";
notifyMessage.WAITING_CHARGE = "Malipo ya kusubiri";
notifyMessage.PENDING_AMOUNT = "Kiasi kinachosubiri";
notifyMessage.CASH_IN_HAND = "Fedha mikononi";
notifyMessage.BOOKING_ACCEPTED = "Uhifadhi wako unakubaliwa";
notifyMessage.BOOKING_PAID = "Kulipwa kwa uhifadhi";
notifyMessage.PROFESSIONAL_STARTED = "Mtaalam alianza safari yako";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY =
  "Mahali ilibadilika kwa mafanikio";
notifyMessage.PROFESSIONAL_ARRIVED =
  "Mtaalam alifika kwenye eneo lako la picha";
notifyMessage.PROFESSIONAL_COMMISSION = "Tume ya Utaalam";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED =
  "Mapato ya uhifadhi yamepokelewa";
notifyMessage.PROFESSIONAL_INFO =
  "Salio lako la pochi limepita chini ya kiwango cha juu kinachohitajika ili kukubali usafiri wa pesa taslimu. Tafadhali chaji tena mkoba wako au unaweza tu kukubali upandaji wa kadi na pochi hadi salio lako litakapotosha";
notifyMessage.PROFESSIONAL_NOT_FOUND = "Mtaalamu hajapatikana";
notifyMessage.PROFESSIONAL_NEW_RIDE = "Umepata safari mpya";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Safari iliyofutwa na mtaalamu";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Uhifadhi wako unakubaliwa na mtaalamu wetu";
notifyMessage.PROFESSIONAL_NOTIFICATION = "Arifa Tuma kwa wataalamu wa karibu";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "Mtaalam alifika ili kuacha eneo";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Utaalam anza safari kutoka kwa eneo la kuacha";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "Mtaalam alifika kwenye eneo lako la uhifadhi";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "Mtaalam alianza kutoka eneo la uhifadhi";
notifyMessage.USER_NOT_FOUND = "Mtumiaji hajapatikana";
notifyMessage.USER_HAVE_RIDE = "Mtumiaji tayari amepanda";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "Kitu kilienda vibaya kwa mtumiaji kufuta safari hii";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Rejesha mkoba wako au uchague chaguo lingine la malipo";
notifyMessage.USER_PAYMENT_DECLINED =
  "Njia hii ya malipo imekataliwa. Tafadhali chagua njia nyingine ya malipo";
notifyMessage.INVALID_PROFESSIONAL = "Maelezo batili ya kitaalam";
notifyMessage.INVALID_BOOKING = "Maelezo batili ya uhifadhi";
notifyMessage.INVALID_ACCESS = "Ufikiaji batili";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "Jamii ya gari haipatikani";
notifyMessage.VEHICLE_NOT_AVAILABLE = "Hakuna gari inayopatikana";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "Jamii ya huduma haipatikani";
notifyMessage.BOOKING_SCHEDULED_RIDE = "Uhifadhi umepangwa kwa mafanikio";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "Jamii ya uhifadhi haipatikani";
notifyMessage.BOOKING_ALREADY = "Uhifadhi tayari uko";
notifyMessage.BOOKING_STATUS_IS = "Uhifadhi ni";
notifyMessage.BOOKING_STATUS_INFO = "Uhifadhi tayari unaendelea au kumalizika";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Uhifadhi umeghairiwa na kuangalia dereva kwa dereva mwingine";
notifyMessage.BOOKING_CANCELLED_BY = "Uhifadhi umefutwa na";
notifyMessage.BOOKING_CANCELLED_BY_USER = "Uhifadhi umeghairiwa na mtumiaji";
notifyMessage.BOOKING_CANCELLED = "Uhifadhi umefutwa kwa mafanikio";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "Uhifadhi mpya umepewa na admin";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Ombi lililopewa mafanikio";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "Uhifadhi tayari unakubaliwa na dereva mwingine";
notifyMessage.BOOKING_UPDATE_FAILED =
  "Haiwezi kusasisha maelezo ya uhifadhi kuna kitu kilienda vibaya";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Hitilafu katika sasisho la hali ya uhifadhi";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "Sababu ya kufuta haipatikani";
notifyMessage.SOMETHING_WENT_WRONG = "Sababu ya kufuta haipatikani";
notifyMessage.RIDE_CANCELLED = "Wapanda kufutwa kwa mafanikio";
notifyMessage.RIDE_IS = "Safari hii ni";
notifyMessage.RIDE_STATUS_INFO = "Safari tayari imemalizika au haijaanza bado";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "Kosa katika kubadilisha maeneo ya wapanda";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "Anwani ya wapanda imebadilika na mtumiaji";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Anwani ya Kusafiri Imebadilishwa na Mtaalamu";
notifyMessage.OTP_INCORRECT = "OTP sio sahihi ingiza OTP sahihi";
notifyMessage.RATING_SUBMITED = "Ukadiriaji uliwasilisha kwa mafanikio";
notifyMessage.RATING_UPDATE_FAILED = "Kosa katika Usasishaji wa Ukadiriaji";
notifyMessage.LOCATION_ACCESS_FAILED =
  "Kuna kitu kilienda vibaya katika eneo lako";
notifyMessage.TRIP_NOT_AVAILABLE = "Vidokezo havipatikani";
notifyMessage.TRIP_SHARED = "Safari ilishirikiwa kwa mafanikio";
notifyMessage.TRIP_END_PROFESSIONAL = "Safari yako imemalizika kwa mtaalamu";
notifyMessage.OPERATOR_STATUS_END =
  "Operesheni yetu ilibadilisha hali yako ya safari hadi mwisho";
notifyMessage.ERROR_SECURITY_UPDATE = "Kosa katika Sasisho la Usalama";
notifyMessage.ERROR_GENEREL_SETTINGS = "Kosa katika Mipangilio ya General";
notifyMessage.WISHLIST_ACTION_FAILED = "Kitendo {{Action}} WishList imeshindwa";
notifyMessage.SUR_CHARGE = "Malipo ya sur";
notifyMessage.SERVICE_TAX = "Ushuru wa huduma";
notifyMessage.TIPS_AMOUNT = "Vidokezo kiasi";
notifyMessage.COUPON_SAVINGS = "Akiba ya kuponi";
notifyMessage.TOLL_FEE = "Ada ya ushuru";
notifyMessage.DISCOUNT_APPLIED = "Punguzo limetumika";
notifyMessage.REFUND_TO_WALLET = "Refund kwa mkoba";
notifyMessage.TIPS_MINIMUM = "Vidokezo shoulud kuwa chini ya";
notifyMessage.TIPS_MAXIMUM = "Vidokezo shoulud kuwa upeo wa";
notifyMessage.TIPS_ADDED = "Vidokezo viliongezwa kwa mafanikio";
notifyMessage.ERROR_PAYMENT_CARD =
  "Kosa katika malipo ya kadi, jaribu kwa huruma baada ya wakati fulani";
notifyMessage.ERROR_EARNINGS = "Kosa katika mapato";
notifyMessage.ERROR_DAY_EARNINGS = "Kosa katika mapato ya siku";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "Inaonekana mtaalamu ni mbaya. Tafadhali muulize mtaalamu aende mkondoni kabla ya kitabu safari hii";
notifyMessage.INFO_PAYMENT_OPTIONS = "Chaguzi za malipo hazipatikani";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Kuna kitu kilienda vibaya katika hafla za lango angalia hatua kwa hatua kwa hatua";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "Siwezi kupata mtumiaji kitu kilienda vibaya katika recharge ya kiasi cha mkoba";
notifyMessage.INFO_CHECK_CREDENTIAL = "Tafadhali angalia sifa inayofuata";
notifyMessage.INFO_SEND_MONEY = "Pesa zilizotumwa kwa mafanikio";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Kulipwa kwa usajili";
notifyMessage.INFO_TIPS = "Ulitoa vidokezo";
notifyMessage.INFO_JOINING_CHARGE = "Kujiunga na kiasi";
notifyMessage.INFO_REFERREL_CHARGE = "Kiasi cha rufaa";
notifyMessage.INFO_CARD_AMOUNT_REFUND =
  "Kiwango cha malipo kilichobaki kimejaa";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT =
  "Kiwango cha malipo kilichobaki kimejadiliwa";
notifyMessage.WALLET_AMOUNT_LOADED =
  "Kiasi kimeongezwa kwenye mkoba wako kwa mafanikio";
notifyMessage.WALLET_NOTIFICATION =
  "Kuna kitu kilienda vibaya katika sasisho la mkoba katika mkoba wa mkoba hakuna mwandishi";
notifyMessage.WALLET_WITHDRAW = "Kiasi cha kujiondoa kutoka kwa mkoba";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Kuondoa mkoba kwa mafanikio";
notifyMessage.WALLET_DEBIT = "Kiasi cha mkoba kilichojadiliwa";
notifyMessage.ERROR_WALLET_RECHARGE =
  "Hitilafu ilitokea wakati recharge ya mkoba";
notifyMessage.ERROR_WALLET_WITHDRAW = "Kosa katika uondoaji wa mkoba";
notifyMessage.ERROR_PAYMENT_OPTION = "Kosa katika orodha ya chaguo la malipo";
notifyMessage.ERROR_SEND_MONEY = "Kosa katika kutuma pesa";
notifyMessage.BANK_CREDET_MONEY =
  "Pesa itakuwa sifa kwa akaunti ya benki ya Yout ndani ya siku 5- 15 za biashara";
notifyMessage.AMOUT_SEND_TO_FRIEND = "Kuhamishiwa kwa rafiki";
notifyMessage.BOOKING_CHARGE_FREEZED = "Kiasi cha kufungia kwa uhifadhi";
notifyMessage.RIDE_CANCELLATION_CHARGE = "Panda kiasi cha kughairi";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "Refund kwa punguzo linalotumika";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "Shughuli haipatikani";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "Jibu halikupatikana tumedhani wewe ni mtumiaji haramu wa URL hii";
notifyMessage.INFO_CARD_ADDED = "Kadi imeongezwa kwa mafanikio";
notifyMessage.INFO_CARD_VERIFIED = "Kadi imethibitishwa kwa mafanikio";
notifyMessage.INFO_BANK_DETAILS = "TAARIFA ZA BENKI";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Mafanikio ya shughuli";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "Haiwezi kupata maelezo ya benki";
notifyMessage.INFO_CARD_DECLINED =
  "Kiasi kilichotolewa kutoka kwa mkoba. Kwa sababu ya kadi ilipungua";
notifyMessage.INFO_AMOUNT_PAID = "Kiasi kilicholipwa kwa mafanikio";
notifyMessage.ERROR_CARD_ADD = "Haiwezi kuongeza kadi";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "Kuna kitu kilienda vibaya kwa kuongeza kadi";
notifyMessage.ERROR_IN_TRANSACTIONS = "Kosa katika shughuli";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "Malipo ya kadi yalishindwa";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "Samahani hatutoi huduma hapa";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Faili imepakiwa kwa mafanikio";
notifyMessage.INFO_NOT_FOUND = "Maelezo hayapatikani";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "Uhakika umeongezwa kwenye mkoba wako kwa mafanikio";
notifyMessage.ERROR_WALLET_POINT_ADDED = "Haiwezekani kwa uhakika wa mkoba";
notifyMessage.INFO_SUCCESS = "Mafanikio";
notifyMessage.INFO_FAILED = "Imeshindwa";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "Ukomboa alama za malipo";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Ukomboa alama za chini za malipo";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Ukomboa alama za juu za malipo";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Operesheni yetu ilibadilisha hali yako ya safari kuwa ilifika";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "Operesheni yetu ilibadilisha hali yako ya safari kuanza";
notifyMessage.INFO_RIDE_CANCELLED_BY = "Kupanda kumeghairiwa na";
notifyMessage.INFO_PROFESSIONAL = "Mtaalam";
notifyMessage.INFO_USER = "Mtumiaji";
notifyMessage.INFO_ADMIN = "Admin";
notifyMessage.INFO_INVALID_DETAILS = "Maelezo batili";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Nambari hii ya Simu Tayari Imesajiliwa kwa Jina la Shirika";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "MSIMBO BATILI WA RUFAA, TAFADHALI ANGALIA MSIMBO WAKO NA UJARIBU TENA";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS =
  "Kitambulisho cha Barua Pepe Tayari Kimesajiliwa";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "UMUNDO BATILI WA DOB LAZIMA UWE MM-DD-YYYY AU MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE =
  "UMRI LAZIMA UWE {{minAgeKey}} NA ZAIDI ILI KUENDELEA";
notifyMessage.INFO_CARD_ALREADY_EXIST = "KADI IPO TAYARI";
notifyMessage.INFO_WALLET_RECHARGE = "Kuchaji upya kwa Wallet";
notifyMessage.INFO_INTERCITY_RIDE = "Safari ya Mji";
notifyMessage.INFO_SITE_COMMISSION = "Tume ya tovuti";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Salio la Kughairi Safari";
notifyMessage.INFO_ROUNDING = "Kuzungusha";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "MTAALAMU TAYARI WAMEPANDA";
notifyMessage.INFO_BASE_FARE = "Nauli ya Msingi";
notifyMessage.INFO_SERVICE_TAX = "Kodi ya Huduma";
notifyMessage.INFO_TRAVEL_CHARGE = "Malipo ya Kusafiri";
notifyMessage.INFO_MINIMUM_CHARGE = "Kiwango cha chini cha malipo";
notifyMessage.INFO_SURCHARGE_FEE = "Ada ya ziada";
notifyMessage.INFO_BOUNDARY_CHARGE = "Malipo ya mipaka";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Malipo ya Kuchukua Miji";
notifyMessage.INFO_OTP_MESSAGE =
  "Msimbo wako wa OTP wa {{siteTitle}} ni {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Akaunti yako imefungwa kwa sababu umefikia idadi ya juu zaidi ya majaribio batili ya kuingia.";
notifyMessage.INFO_HUBS_EARNINGS = "Mapato ya Hubs";
notifyMessage.INFO_FELLOW_PASSENGER = "ABIRIA MWENZETU";
notifyMessage.INFO_ALREADY_EXISTS = "TAYARI IPO";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Msimbo wa Kuponi si Sahihi, Tafadhali Angalia Nambari Yako.";
notifyMessage.WALLET_RECHARGE_FAILED = "UCHAJI WA WALETI UMESHINDWA";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "NYARAKA ZA GARI YAKO IMEMALIZIKA. TAFADHALI ANGALIA NA USASISHA";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "NYARAKA ZA WASIFU WAKO IMEMALIZIKA. TAFADHALI ANGALIA NA USASISHA";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Nyaraka Zako za Gari zitaisha Muda baada ya Siku {{days}}. Tafadhali Angalia Na Usasishe";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Nyaraka zako za Wasifu zitakwisha baada ya Siku {{days}}. Tafadhali Angalia Na Usasishe";
notifyMessage.INFO_INCENTIVE_CREDIT = "Motisha Imetolewa";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Ada ya Utunzaji wa Usaidizi";
notifyMessage.INFO_TIME_FARE = "Nauli ya Muda";
notifyMessage.INFO_DISTANCE_FARE = "Nauli ya Umbali";
notifyMessage.INFO_PEAK_FARE = "Peak Nauli";
notifyMessage.INFO_NIGHT_FARE = "Nauli ya Usiku";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Lo! Muunganisho umepotea. Hauko mtandaoni sasa. Tafadhali nenda mtandaoni ili kupokea maombi ya usafiri.";
notifyMessage.AIRPORT_FEE = "Ada ya Uwanja wa Ndege";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Mtaalamu Huyu Hakubali Chaguo Lililochaguliwa la Malipo. Tafadhali Badilisha Chaguo la Malipo.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Ukadiriaji wako uko chini ya Masafa, Tafadhali boresha, au Akaunti yako itazuiwa baada ya Siku chache.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Akaunti yako imezuiwa kwa sababu ukadiriaji wako uko chini ya masafa. Akaunti itawashwa tena mnamo {{date}} usiku wa manane.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Akaunti yako imewezeshwa. Tafadhali nenda mtandaoni ili kupokea maombi ya usafiri.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "Uthibitishaji wa kadi haukufaulu";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Seva ya benki Ina shughuli Sasa. Tafadhali jaribu Baadaye";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Imethibitishwa Kwa Mafanikio";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Uthibitishaji wa CVV Umeshindwa";
notifyMessage.ALERT_CHECK_CPF = "Tafadhali Angalia Nambari ya CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Tafadhali Angalia Kuanzia Tarehe / Hadi Tarehe.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "Jiji Tayari Lipo.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "Nambari ya CPF Tayari Ipo.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Kikomo cha juu cha Pesa ya Kutoa ni {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Uondoaji wa Kiasi Unaruhusiwa Mara Moja kwa Siku.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Jina la Mwenye Akaunti";
notifyMessage.INFO_BANK_NAME = "Jina la Benki";
notifyMessage.INFO_DOCUMENT_NUMBER = "Nambari ya Hati";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Utoaji wa Wallet haukufaulu. Kiasi kilichowekwa kwenye pochi.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Hakuna Eneo la Huduma Linapatikana";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Abiria mwenzako amefika mahali pa kuchukua";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Abiria mwenzako ameanza safari";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "Safari imefika mwisho kwa abiria mwenzako.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Barua pepe Hii Tayari Inahusishwa na Akaunti Nyingine. Tafadhali Tumia Barua pepe Tofauti.";
notifyMessage.INFO_TRIP_NUMBER = "Nambari ya Safari";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Eneo la Huduma Tayari Lipo.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "Kampuni Tayari Ipo.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Eneo la Huduma Tayari Lipo.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Uthibitishaji wa Hati yako chini ya Ukaguzi, Tafadhali subiri hadi Uthibitishaji Ukamilike.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
