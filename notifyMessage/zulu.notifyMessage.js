const notifyMessage = {};

notifyMessage.CREATED = "Idalwe Ngempumelelo";
notifyMessage.UPDATED = "Kubuyekezwe ngempumelelo";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "IDATHA YOMSEBENZISI ENGAvumelekile";
notifyMessage.TRIP_END = "UHAMBO LUPHELILE";
notifyMessage.WALLET_LOADED = "Inani Le Wallet Lilayishiwe";
notifyMessage.CASH = "Imali";
notifyMessage.CARD = "Ikhadi";
notifyMessage.WALLET = "Isikhwama semali";
notifyMessage.TRIP_FEE = "Imali Yohambo";
notifyMessage.TOTAL_BILL = "Umthethosivivinywa Ophelele";
notifyMessage.EARNINGS = "Umholo Wakho";
notifyMessage.WAITING_CHARGE = "Inkokhelo yokulinda";
notifyMessage.PENDING_AMOUNT = "Inani Elilindile";
notifyMessage.CASH_IN_HAND = "Imali Esandleni";
notifyMessage.BOOKING_ACCEPTED = "UKUBEKA KWAKHO KWAMUKELWE";
notifyMessage.BOOKING_PAID = "Ikhokhelwe Ukubhuka";
notifyMessage.PROFESSIONAL_STARTED = "I-PROFESSIONAL IQALE OHAMBO LWAKHO";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "Indawo Ishintshwe Ngempumelelo";
notifyMessage.PROFESSIONAL_ARRIVED =
  "OKUNGEZESIFUNDAZWE UFIKE ENDAWENI OLANDELA KUYO";
notifyMessage.PROFESSIONAL_COMMISSION = "Ikhomishini yobungcweti";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Izinzuzo zokubhukha zitholiwe";
notifyMessage.PROFESSIONAL_INFO =
  "NGICELA FINYELELA I-WALETHI YAKHO UKUZE UYE KU-INTANETHI";
notifyMessage.PROFESSIONAL_NOT_FOUND = "UCHWEPHESHE AKATHOLAKALI";
notifyMessage.PROFESSIONAL_NEW_RIDE = "UTHOLE OKUSHA";
notifyMessage.PROFESSIONAL_RIDE_CANCEL =
  "UKUGIBELA KUKHANSELWE NGU-PROFESSIONAL";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "UKUBHEKA KWAKHO KWAMUKELWE NGUCWEPHESHE WETHU";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "ISAZISO SITHUNYELWE KOchwepheshe ABASEDUZE";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "UCHWEPHESHE UFIKILE UKUZOMISA INDAWO";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "NGOKWENGEZIWE QALA UHAMBO OSUKA ENDAWENI YOKUMISA";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "UCHWEPHESHE UFIKE ENDAWENI YAKHO YOKUBEKELA";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "I-PROFESSIONAL IQALE EKUBEKENI INDAWO";
notifyMessage.USER_NOT_FOUND = "UMSEBENZISI AKATHOLAKALI";
notifyMessage.USER_HAVE_RIDE = "UMSEBENZISI USEYIQALILE";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "OKUTHILE AKUMANGAPHANGA KUMSEBENZISI KHANSELA LOKHU KUGIDLA";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "NGOMUSA SHAJA I-WALETHI YAKHO NOMA UKHETHE YIYIPHI INDLELA YOKUKHOKHA";
notifyMessage.USER_PAYMENT_DECLINED =
  "Le ndlela yokukhokha inqatshiwe. Sicela ukhethe enye indlela yokukhokha";
notifyMessage.INVALID_PROFESSIONAL =
  "Imininingwane Yochwepheshe Engavumelekile";
notifyMessage.INVALID_BOOKING = "Imininingwane Yokubhuka Engavumelekile";
notifyMessage.INVALID_ACCESS = "UKUFINYELELA OKUNGAvumelekile";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "Isigaba ESITHOLAKALA";
notifyMessage.VEHICLE_NOT_AVAILABLE = "AYIKHO IMOTO ENGAKHONA";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "UHLU LWENKONZO AYITHOLAKALI";
notifyMessage.BOOKING_SCHEDULED_RIDE = "UKUBHEKA KUHLELWE NGEMPUMELELO";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "ISIGABA SOKUBHEKA ASITHOLAKALI";
notifyMessage.BOOKING_ALREADY = "UKUBHEKA KAHLE";
notifyMessage.BOOKING_STATUS_IS = "UKUBHEKA YI";
notifyMessage.BOOKING_STATUS_INFO = "UKUBHEKA SEKUQHUBEKA NOMA KUYAQEDA";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "UKUBHKHA KUKHANSISWA NGUMSHAYELI UHLOLA OMUNYE UMSHAYELI";
notifyMessage.BOOKING_CANCELLED_BY = "UKUBHKHA KUKHANSISWA NGU";
notifyMessage.BOOKING_CANCELLED_BY_USER = "UKUBHKHA KUKHANSISWA UMSEBENZISI";
notifyMessage.BOOKING_CANCELLED = "UKUBHKHA KUKHANSISWA NGEMPUMELELO";
notifyMessage.BOOKING_ACCEPTED_BY_ADMIN = "UKUBHEKA OKUSHA KWABELWE NGU-ADMIN";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "ISICELO SIBELWE NGEMPUMELELO";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "UKUBHEKA SEKWAMUKELWA ngomunye umshayeli";
notifyMessage.BOOKING_UPDATE_FAILED =
  "AWUKWAZI UKUBUYEKEZA IMINININGWANE YOKUBEKELA INTO AYIPHAMBI kahle";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "IPHUTHA EKUBUYEKEZENI ESIMO SOKUBHEKA";
notifyMessage.CANCELLATION_REASON_NOT_FOUND =
  "ISIZATHU SOKUKHANSELA ASITHOLAKALA";
notifyMessage.SOMETHING_WENT_WRONG = "ISIZATHU SOKUKHANSELA ASITHOLAKALA";
notifyMessage.RIDE_CANCELLED = "UKUGIBELA KUKHANSILE NGEMPUMELELO";
notifyMessage.RIDE_IS = "LOKHU UKUGIBELA";
notifyMessage.RIDE_STATUS_INFO =
  "UKUGIBELA SEKUPHELILE NOMA AKUKAQALI KANGAKATHI";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "IPHUTHA EKUSHINTSHENI IZINDAWO ZOKUGIDELA";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "IKHELI LOKUGAMA LISHINTSHWE UMSEBENZISI";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Ikheli Lokuhamba Lishintshiwe Ngochwepheshe";
notifyMessage.OTP_INCORRECT = "I-OTP AYILUNGILE NGOMUSA FAKA I-OTP ENGALUNGILE";
notifyMessage.RATING_SUBMITED = "ISILINGANISO SITHUNYELWE NGEMPUMELELO";
notifyMessage.RATING_UPDATE_FAILED = "IPHUTHA EKUBUYEKEZENI ISILINGANISO";
notifyMessage.LOCATION_ACCESS_FAILED =
  "Kukhona okungahambanga kahle endaweni yakho";
notifyMessage.TRIP_NOT_AVAILABLE = "AMACEBISO AKATHOLAKALI";
notifyMessage.TRIP_SHARED = "UHAMBO LWABELWE NGEMPUMELELO";
notifyMessage.TRIP_END_PROFESSIONAL = "UKUHAMBA KWAKHO KUPHELILE NGOKUNGCWELE";
notifyMessage.OPERATOR_STATUS_END =
  "UMSEBENZISI WETHU USHINTSHE ISIMO SOHAMBO LWAKHO UKUZE LUPHELE";
notifyMessage.ERROR_SECURITY_UPDATE = "IPHUTHA EKUBUYEKEZENI OKUVIKELA";
notifyMessage.ERROR_GENEREL_SETTINGS = "IPHUTHA KUZILUNGISELELO EZIJWAYELEKILE";
notifyMessage.WISHLIST_ACTION_FAILED =
  "ISENZO {{action}} UHLU LWEZIFISO LWEHLULEKILE";
notifyMessage.SUR_CHARGE = "I-Sur Charge";
notifyMessage.SERVICE_TAX = "Intela Yesevisi";
notifyMessage.TIPS_AMOUNT = "Amathiphu Inani";
notifyMessage.COUPON_SAVINGS = "Ikhuphoni Savings";
notifyMessage.TOLL_FEE = "I-Toll Fee";
notifyMessage.DISCOUNT_APPLIED = "Kusetshenziswe Isaphulelo";
notifyMessage.REFUND_TO_WALLET = "Buyisela imali ku-Wallet";
notifyMessage.TIPS_MINIMUM = "AMACEBISO KUFANELE ABE ENGAPHAMBILI";
notifyMessage.TIPS_MAXIMUM = "AMACEBISO KUFANELE ABE ESIPHEZULU S";
notifyMessage.TIPS_ADDED = "AMACEBISO ENGEZELWE NGEMPUMELELO";
notifyMessage.ERROR_PAYMENT_CARD =
  "IPHUTHA EKUKHOKHELWENI KWEKHADI, NGOMUSA UZAMA NGEMVA KWESIKHATHI";
notifyMessage.ERROR_EARNINGS = "IPHUTHA EZIMALINI";
notifyMessage.ERROR_DAY_EARNINGS = "IPHUTHA EZIMALINI ZESUKU";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "KUBUKEKA UMSEBENZI AYIKU-inthanethi. SICELA UBUZE UCHWEPHESHE UKUBA ABE KU-INTANETHI NGAPHAMBI KOKUBUKA LOLUHAMBO";
notifyMessage.INFO_PAYMENT_OPTIONS = "IZINKETHO ZOKUKHOKHELA AYITHOLAKALI";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "OKUTHILE AKUKHO KAHLE EMCIMBINI WASE-GATEWAY HLOLA KAHLE ISINYATHELO NGESINYATHELO";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "AWUKWAZI UKUTHOLA UMSEBENZISI OKUTHILE OKUNGAMANGA NGENDLELA YEWALETHI IMALI YOKUSHAJA";
notifyMessage.INFO_CHECK_CREDENTIAL = "SICELA UHLOLA ISIVIVINYO ESILANDELAYO";
notifyMessage.INFO_SEND_MONEY = "IMALI ITHUNYELWE NGEMPUMELELO";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Kukhokhelwe Ukubhalisa";
notifyMessage.INFO_TIPS = "Unikeze Amathiphu";
notifyMessage.INFO_JOINING_CHARGE = "Inani lokujoyina";
notifyMessage.INFO_REFERREL_CHARGE = "Inani Lenkomba";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Inani Lokukhokha Elisele Lilayishiwe";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Inani Lokukhokha Elisele Lidonsiwe";
notifyMessage.WALLET_AMOUNT_LOADED =
  "IMALI LENGEZWA NGEMPUMELELO KU-WALETHI YAKHO";
notifyMessage.WALLET_NOTIFICATION =
  "OKUTHILE AKUFANANGA KUBUYEKEZO LWEWELETHI KU-WALETI RECHARE NO AUTH";
notifyMessage.WALLET_WITHDRAW = "INANI LIYAHOXA KUSWELETHI";
notifyMessage.WALLET_WITHDRAW_SUCCESS =
  "UKUHOXISWA KWE-WALETHI KUBE NGEMPUMELELO";
notifyMessage.WALLET_DEBIT = "Imali Yesikhwama Semali Ekhishiwe";
notifyMessage.ERROR_WALLET_RECHARGE =
  "IPHUTHA LENZEKILE NGESIKHATHI SISHAJWA KAKHULU IWALI";
notifyMessage.ERROR_WALLET_WITHDRAW = "IPHUTHA EKUKHISHWENI KWE-WALETHI";
notifyMessage.ERROR_PAYMENT_OPTION = "IPHUTHA KUHLU LWENKETHO YOKUKHOKHA";
notifyMessage.ERROR_SEND_MONEY = "IPHUTHA EKUTHUMENI IMALI";
notifyMessage.BANK_CREDET_MONEY =
  "IMALI IZOKWETHWA KU-AKHAWUNTI YASEBHANGE ELISHA NGAPHAKATHI NEZINSUKU ZEBHIZINISI Eziyi-5- 15";
notifyMessage.AMOUT_SEND_TO_FRIEND = "Kudluliselwe Kumngane";
notifyMessage.BOOKING_CHARGE_FREEZED = "Inani Elifriziwe Lokubhuka";
notifyMessage.RIDE_CANCELLATION_CHARGE = "Inani lokukhansela lokugibela";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "Imbuyiselo Yesaphulelo Isetshenzisiwe";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "UKWENZIWA AKUTHOLAKALI";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "IMPENDULO AYITHOLAKALI SITHI UNGUMSEBENZISI OKUNGAMTHETHO WALE URL";
notifyMessage.INFO_CARD_ADDED = "IKHADI LENGEZWA NGEMPUMELELO";
notifyMessage.INFO_CARD_VERIFIED = "IKHADI LIQINISEKISIWE NGEMPUMELELO";
notifyMessage.INFO_BANK_DETAILS = "IMINININGWANE YEBHANG";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "IMPUMELELO YOKWENZA";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "AYIKUTHOLA IMINININGWANE YEBHANG";
notifyMessage.INFO_CARD_DECLINED =
  "INANI ELIKHISHWE KUSWELETHI. NGENXA YOKUNQINWA KWEKHADI";
notifyMessage.INFO_AMOUNT_PAID = "IMALI EKHOKHELWE NGEMPUMELELO";
notifyMessage.ERROR_CARD_ADD = "ANGIKWAZI UKWEngeza IKHADI";
notifyMessage.ERROR_CARD_ADD_FAILED = "KUKHONA OKUNGAHLELI EKUNENGENI AMAKHADI";
notifyMessage.ERROR_IN_TRANSACTIONS = "IPHUTHA EZINKWENZENI";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "UKUKHOKHA KWEKHADI YEHLULEKILE";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "Uxolo ASINIKEZI INKONZO LAPHA";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Ifayela Lilayishwe Ngempumelelo";
notifyMessage.INFO_NOT_FOUND = "IMINININGWANE AYITHOLAKALI";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "IPHUZU LENGEZWA NGEMPUMELELO KU-WALETHI YAKHO";
notifyMessage.ERROR_WALLET_POINT_ADDED = "AYIKWAZI UKUTHOLA IPHUZU LE-WALLET";
notifyMessage.INFO_SUCCESS = "IMPUMELELO";
notifyMessage.INFO_FAILED = "YEHLULEKILE";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "Sebenzisa Amaphuzu Omvuzo";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Sebenzisa Amaphuzu Omvuzo Obuncane";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Thola Umkhawulo Wamaphuzu Womvuzo";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "UMSEBENZISI WETHU USHINTSHE ISIMO SOHAMBO LWAKHO UKUZE UFIKE";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "UMSEBENZISI WETHU USHINTSHE ISIMO SOHAMBO LWAKHO UKUZE LUQALE";
notifyMessage.INFO_RIDE_CANCELLED_BY = "I-RIDE ikhanseliwe";
notifyMessage.INFO_PROFESSIONAL = "NGOCHWEPHESHE";
notifyMessage.INFO_USER = "USER";
notifyMessage.INFO_ADMIN = "ADMIN";
notifyMessage.INFO_INVALID_DETAILS = "Imininingwane Engavumelekile";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Le nombolo Yocingo Isivele Ibhalisiwe Egameni Lebhizinisi";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "IKHODI YOKUDLULISELA ENGAvumelekile, SICELA UHLOLA IKHODI YAKHO FUTHI UZAMA FUTHI";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "I-Id ye-imeyili Isivele Yabhaliswa";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "IFOMU YEDOB ENGAvumelekile KUMELE IBE MM-DD-YYYY NOMA MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE =
  "UBUDALA KUMELE BUBE ngu-{{minAgeKey}} FUTHI NGENZULU UKUZE UQHUBEKISE";
notifyMessage.INFO_CARD_ALREADY_EXIST = "IKHADI SELIKHONA vele";
notifyMessage.INFO_WALLET_RECHARGE = "Ukushajwa kabusha kwe-Wallet";
notifyMessage.INFO_INTERCITY_RIDE = "I-Intercity Ride";
notifyMessage.INFO_SITE_COMMISSION = "Ikhomishini Yendawo";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Ikhredithi Yokukhansela Ukugibela";
notifyMessage.INFO_ROUNDING = "Ukujikeleza";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "OKUNGEZESIFUNDAZWE SEKUQHUBEKE";
notifyMessage.INFO_BASE_FARE = "Imali Eyisisekelo";
notifyMessage.INFO_SERVICE_TAX = "Intela Yesevisi";
notifyMessage.INFO_TRAVEL_CHARGE = "Inkokhelo Yokuhamba";
notifyMessage.INFO_MINIMUM_CHARGE = "Inkokhelo Encane";
notifyMessage.INFO_SURCHARGE_FEE = "Imali Eyengeziwe";
notifyMessage.INFO_BOUNDARY_CHARGE = "Ukukhokhiswa komngcele";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "I-Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE =
  "{{siteTitle}} Ikhodi yakho ye-OTP ithi {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "I-akhawunti yakho ikhiyiwe ngoba usufinyelele inombolo enkulu yemizamo yokungena engavumelekile.";
notifyMessage.INFO_HUBS_EARNINGS = "Amaholo e-Hubs";
notifyMessage.INFO_FELLOW_PASSENGER = "ABAKWETHU BAGIBELE";
notifyMessage.INFO_ALREADY_EXISTS = "SEKUKHONA";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Ikhodi Yekhuphoni Engavumelekile, Sicela Uhlole Ikhodi Yakho.";
notifyMessage.WALLET_RECHARGE_FAILED = "UKUSHAJWA KWE-WALETHI KWEHLULEKILE";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "AMADOKHUMENTI ESIMOTO SAKHO APHELELWE ISIKHATHI. SICELA Uhlole FUTHI Ubuyekeze";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "AMADOKHUMENTI WAKHO WEPHROFAYILI APHELELWE ISIKHATHI. SICELA Uhlole FUTHI Ubuyekeze";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Amadokhumenti Emoto Yakho azophelelwa Isikhathi ezinsukwini ezingu-{{days}}. Sicela Uhlole Futhi Ubuyekeze";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Imibhalo Yephrofayela Yakho izophelelwa yisikhathi ezinsukwini ezingu-{{days}}. Sicela Uhlole Futhi Ubuyekeze";
notifyMessage.INFO_INCENTIVE_CREDIT = "I-Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Imali Yokunakekelwa Kosizo";
notifyMessage.INFO_TIME_FARE = "Imali Yesikhathi";
notifyMessage.INFO_DISTANCE_FARE = "Imali Yebanga";
notifyMessage.INFO_PEAK_FARE = "Imali Ephakeme";
notifyMessage.INFO_NIGHT_FARE = "Imali Yasebusuku";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Eshu! Uxhumo lulahlekile. Awuxhunyiwe ku-inthanethi manje. Sicela uye ku-inthanethi ukuze uthathe izicelo zokugibela.";
notifyMessage.AIRPORT_FEE = "Imali Yesikhumulo Sezindiza";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Lesi Singcweti Ayemukeli Inketho Yokukhokha Ekhethiwe. Sicela ushintshe inketho yokukhokha.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Isilinganiso sakho singaphansi kweBanga, Sicela uthuthukise, noma i-akhawunti yakho izovinjwa ezinsukwini ezimbalwa.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "I-akhawunti yakho ivinjiwe ngoba isilinganiso sakho singaphansi kobubanzi. I-akhawunti izovulwa kabusha ngomhla ka-{{date}} phakathi kwamabili.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "I-akhawunti yakho ivuliwe. Sicela uye ku-inthanethi ukuze uthathe izicelo zokugibela.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Inani Elidluliselwe Kumngane";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "Ukuqinisekiswa kwekhadi kuhlulekile";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Iseva yasebhange Imatasa Manje. Sicela uzame ngokuhamba kwesikhathi";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "I-CVV Iqinisekiswe Ngempumelelo";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Ukuqinisekiswa kwe-CVV Kwehlulekile";
notifyMessage.ALERT_CHECK_CPF = "Sicela uhlole Inombolo ye-CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Sicela uhlole Kusukela Kudethi / Kuze kube manje.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "Idolobha Selikhona.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "Inombolo ye-CPF Isivele Ikhona.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Umkhawulo Wenani Lokukhipha Okuphezulu ngu-{{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Ukuhoxiswa Kwenani Okuvunyelwe Kanye Ngosuku.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Igama Lomphathi We-akhawunti";
notifyMessage.INFO_BANK_NAME = "Igama Lebhange";
notifyMessage.INFO_DOCUMENT_NUMBER = "Inombolo Yedokhumenti";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Ukukhipha i-wallet kuhlulekile. Inani elifakwe esikhwameni.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Ayikho Indawo Yesevisi Etholakalayo";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Umgibeli ohamba naye usefikile endaweni yokulayisha";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Umgibeli ohamba naye useqalile ukugibela";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "Ukugibela sekufike esiphethweni kumgibeli ohamba naye.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Le Imeyili Isivele Ihlotshaniswa Nenye I-akhawunti. Sicela usebenzise I-imeyili Ehlukile.";
//--------- Newly added -------------
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
