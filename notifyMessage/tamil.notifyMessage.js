const notifyMessage = {};

notifyMessage.CREATED = "வெற்றிகரமாக சேமிக்கப்பட்டது";
notifyMessage.UPDATED = "வெற்றிகரமாக புதுப்பிக்கப்பட்டது";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "தவறான பயனர் தரவு";
notifyMessage.TRIP_END = "பயணம் முடிந்தது";
notifyMessage.WALLET_LOADED = "வாலட் தொகை ஏற்றப்பட்டது";
notifyMessage.CASH = "பணம்";
notifyMessage.CARD = "அட்டை";
notifyMessage.WALLET = "பணப்பை";
notifyMessage.TRIP_FEE = "பயணக் கட்டணம்";
notifyMessage.TOTAL_BILL = "மொத்த கட்டணம்";
notifyMessage.EARNINGS = "உங்கள் வருவாய்";
notifyMessage.WAITING_CHARGE = "காத்திருப்பு கட்டணம்";
notifyMessage.PENDING_AMOUNT = "நிலுவையில் உள்ள தொகை";
notifyMessage.CASH_IN_HAND = "கையில் காசு";

notifyMessage.BOOKING_ACCEPTED = "உங்கள் முன்பதிவு ஏற்றுக்கொள்ளப்பட்டது";
notifyMessage.BOOKING_PAID = "முன்பதிவு செய்த பணம்";

//PROFESSIONAL
notifyMessage.PROFESSIONAL_STARTED = "உங்கள் பயணத்திற்கு தொழில்முறை தொடங்கியது";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "இடம் வெற்றிகரமாக மாற்றப்பட்டது";
notifyMessage.PROFESSIONAL_ARRIVED =
  "உங்கள் பிக்அப் இடத்திற்கு தொழில்முறை வந்து சேர்ந்தது";
notifyMessage.PROFESSIONAL_COMMISSION = "தொழில்முறை கமிஷன்";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "முன்பதிவு வருவாய் கிடைத்தது";

notifyMessage.PROFESSIONAL_INFO =
  "பணச் சவாரிகளை ஏற்கத் தேவையான குறைந்தபட்ச வரம்பிற்குக் கீழே உங்கள் வாலட் இருப்புச் சென்றுவிட்டது. உங்கள் பணப்பையை ரீசார்ஜ் செய்யவும் அல்லது உங்கள் இருப்பு போதுமானதாக இருக்கும் வரை கார்டு மற்றும் வாலட் சவாரிகளை மட்டுமே ஏற்க முடியும்";
notifyMessage.PROFESSIONAL_NOT_FOUND = "ஓட்டுனர் இல்லை";
notifyMessage.PROFESSIONAL_NEW_RIDE = "உங்களுக்கு புதிய சவாரி கிடைத்துள்ளது";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "ஓட்டுனரால் சவாரி ரத்து செய்யப்பட்டது";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "உங்கள் முன்பதிவு எங்கள் ஓட்டுனரால் ஏற்றுக்கொள்ளப்படுகிறது";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "அருகிலுள்ள ஓட்டுனருக்கு அறிவிப்பு அனுப்பப்படும்";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "ஓட்டுனர் நிறுத்தும் இடத்திற்கு வந்தார்";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "ஓட்டுனர் நிறுத்தப்பட்ட இடத்திலிருந்து சவாரியைத் தொடங்கவும்";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "ஓட்டுனர் உங்கள் முன்பதிவு இடத்திற்கு வந்தார்";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "முன்பதிவு செய்யும் இடத்திலிருந்து ஓட்டுநர் பயணத்தைத் தொடங்கினார்";

notifyMessage.USER_NOT_FOUND = "பயனர் கிடைக்கவில்லை";
notifyMessage.USER_HAVE_RIDE = "பயனர் ஏற்கனவே சவாரி செய்துள்ளார்";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "பயனர் தரவில் ஏதோ தவறாகிவிட்டது இந்த சவாரி ரத்து";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "தயவுசெய்து உங்கள் பணப்பையை ரீசார்ஜ் செய்யவும் அல்லது வேறு ஏதேனும் கட்டண விருப்பத்தைத் தேர்வு செய்யவும்";
notifyMessage.USER_PAYMENT_DECLINED =
  "இந்த கட்டண முறை நிராகரிக்கப்பட்டது. மற்றொரு கட்டண முறையைத் தேர்வு செய்யவும்";

notifyMessage.INVALID_PROFESSIONAL = "தவறான ஓட்டுனர் விவரங்கள்";
notifyMessage.INVALID_BOOKING = "தவறான முன்பதிவு விவரங்கள்";
notifyMessage.INVALID_ACCESS = "அணுகல் மறுக்கப்பட்டது";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "வாகன வகை காணப்படவில்லை";
notifyMessage.VEHICLE_NOT_AVAILABLE = "வாகனம் எதுவும் கிடைக்கவில்லை";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "சேவை வகை காணப்படவில்லை";

notifyMessage.BOOKING_SCHEDULED_RIDE = "முன்பதிவு வெற்றிகரமாக திட்டமிடப்பட்டது";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "முன்பதிவு வகை கிடைக்கவில்லை";
notifyMessage.BOOKING_ALREADY = "முன்பதிவு ஏற்கனவே உள்ளது";
notifyMessage.BOOKING_STATUS_IS = "முன்பதிவு";
notifyMessage.BOOKING_STATUS_INFO =
  "முன்பதிவு ஏற்கனவே முன்னேற்றத்தில் அல்லது முடிந்தது";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "ஓட்டுனரால் முன்பதிவு ரத்துசெய்யப்பட்டது. அருகிலுள்ள ஓட்டுனருக்கு அறிவிப்பு அனுப்பப்படும்";
notifyMessage.BOOKING_CANCELLED_BY = "முன்பதிவு ரத்து செய்யப்பட்டது";
notifyMessage.BOOKING_CANCELLED_BY_USER =
  "பயனரால் முன்பதிவு ரத்துசெய்யப்பட்டது";
notifyMessage.BOOKING_CANCELLED = "முன்பதிவு வெற்றிகரமாக ரத்து செய்யப்பட்டது";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN =
  "புதிய முன்பதிவு நிர்வாகியால் ஒதுக்கப்படுகிறது";
notifyMessage.BOOKING_REQUEST_ASSIGNED =
  "முன்பதிவு கோரிக்கை வெற்றிகரமாக ஒதுக்கப்பட்டது";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "முன்பதிவு ஏற்கனவே மற்றொரு ஓட்டுனரால் ஏற்றுக்கொள்ளப்பட்டது";
notifyMessage.BOOKING_UPDATE_FAILED =
  "முன்பதிவு விவரங்களைப் புதுப்பிக்க முடியாது ஏதோ தவறாகிவிட்டது";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "பயண முன்பதிவு நிலை புதுப்பிப்பில் பிழை";

notifyMessage.CANCELLATION_REASON_NOT_FOUND =
  "ரத்து செய்வதற்கான காரணம் கண்டறியப்படவில்லை";

notifyMessage.SOMETHING_WENT_WRONG = "பயண விவரங்களில் ஏதோ தவறாகிவிட்டது";

notifyMessage.RIDE_CANCELLED = "பயணம் வெற்றிகரமாக ரத்து செய்யப்பட்டது";
notifyMessage.RIDE_IS = "இந்தப் பயணம்";
notifyMessage.RIDE_STATUS_INFO =
  "சவாரி ஏற்கனவே முடிந்துவிட்டது அல்லது இன்னும் தொடங்கவில்லை";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "பயண இடங்களை மாற்றுவதில் பிழை";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER = "பயண முகவரி பயனரால் மாற்றப்பட்டது";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "ஓட்டுனர் சவாரி முகவரி மாறிவிட்டது";
notifyMessage.OTP_INCORRECT = "OTP தவறானது, தயவுசெய்து சரியான OTP ஐ உள்ளிடவும்";

notifyMessage.RATING_SUBMITED = "மதிப்பீடு வெற்றிகரமாக சமர்ப்பிக்கப்பட்டது";
notifyMessage.RATING_UPDATE_FAILED = "மதிப்பீடு புதுப்பிப்பில் பிழை";

notifyMessage.LOCATION_ACCESS_FAILED =
  "உங்கள் இருப்பிடத்தில் ஏதோ தவறாகிவிட்டது";

notifyMessage.TRIP_NOT_AVAILABLE = "சவாரி விவரங்கள் கிடைக்கவில்லை";
notifyMessage.TRIP_SHARED = "பயணம் வெற்றிகரமாகப் பகிரப்பட்டது";
notifyMessage.TRIP_END_PROFESSIONAL =
  "ஓட்டுனர் உங்கள் பயணத்தை முடித்துவிட்டார்";

notifyMessage.OPERATOR_STATUS_END =
  "எங்கள் ஆபரேட்டர் உங்கள் பயண நிலையை முடிவுக்கு மாற்றினார்";

notifyMessage.ERROR_SECURITY_UPDATE = "பாதுகாப்பு புதுப்பிப்பில் பிழை";
notifyMessage.ERROR_GENEREL_SETTINGS = "பொது அமைப்புகளில் பிழை";

notifyMessage.WISHLIST_ACTION_FAILED =
  "நடவடிக்கை {{action}} விருப்பப்பட்டியல் தோல்வியடைந்தது";

notifyMessage.SUR_CHARGE = "இதர கட்டணம்"; //SurCharge
notifyMessage.SERVICE_TAX = "சேவை வரி";
notifyMessage.TIPS_AMOUNT = "ஊக்க தொகை";
notifyMessage.COUPON_SAVINGS = "கூப்பன் சேமிப்பு";
notifyMessage.TOLL_FEE = "சுங்க கட்டணம்";
notifyMessage.DISCOUNT_APPLIED = "தள்ளுபடி பயன்படுத்தப்பட்டது";
notifyMessage.REFUND_TO_WALLET = "வாலட்டில் பணத்தைத் திரும்பப் பெறவும்";

notifyMessage.TIPS_MINIMUM = "ஊக்க தொகை குறைந்தபட்சமாக இருக்க வேண்டும்";
notifyMessage.TIPS_MAXIMUM = "ஊக்க தொகை அதிகபட்சமாக இருக்க வேண்டும்";
notifyMessage.TIPS_ADDED = "ஊக்க தொகை வெற்றிகரமாக சேர்க்கப்பட்டது";

notifyMessage.ERROR_PAYMENT_CARD =
  "கார்டு கட்டணத்தில் பிழை, சிறிது நேரம் கழித்து முயற்சிக்கவும்";
notifyMessage.ERROR_EARNINGS = "வருவாய் பிழை";
notifyMessage.ERROR_DAY_EARNINGS = "நாள் வருமானத்தில் பிழை";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "ஓட்டுனர் ஆஃப்லைனில் இருப்பதாகத் தெரிகிறது. இந்தப் பயணத்தை முன்பதிவு செய்வதற்கு முன், ஓட்டுனர் ஆன்லைனில் செல்லச் சொல்லுங்கள்";
//---------
notifyMessage.INFO_PAYMENT_OPTIONS = "கட்டண முறை விருப்பங்கள் இல்லை";
notifyMessage.INFO_CARD = "கார்டு விவரங்களைக் கண்டுபிடிக்க முடியவில்லை";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "கட்டணம் செலுத்தும் முறையில் ஏதோ தவறு நடந்துள்ளது நிகழ்வுகளை கவனமாகப் படிப்படியாகச் சரிபார்க்கவும்";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "வாலட் தொகை ரீசார்ஜ் செய்ததில் ஏதோ தவறு நடந்துள்ளதால், பயனரைக் கண்டறிய முடியவில்லை";
notifyMessage.INFO_CHECK_CREDENTIAL = "தயவு செய்து அடுத்ததைச் சரிபார்க்கவும்";
notifyMessage.INFO_SEND_MONEY = "பணம் வெற்றிகரமாக அனுப்பப்பட்டது";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "சந்தா செலுத்தப்பட்டது";
notifyMessage.INFO_TIPS = "டிப்ஸ் கொடுத்தீர்கள்";
notifyMessage.INFO_JOINING_CHARGE = "சேரும் கட்டணம்";
notifyMessage.INFO_REFERREL_CHARGE = "பரிந்துரை கட்டணம்";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "மீதமுள்ள கட்டணத் தொகை ஏற்றப்பட்டது";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "மீதமுள்ள கட்டணத் தொகை எடுக்கப்பட்டது";

notifyMessage.WALLET_AMOUNT_LOADED =
  "உங்கள் பணப்பையில் தொகை வெற்றிகரமாக சேர்க்கப்பட்டது";
notifyMessage.WALLET_NOTIFICATION =
  "பணப்பையில் ஏதோ தவறாகிவிட்டது வாலட்டில் புதுப்பிப்பு Rechare NO AUTH";
notifyMessage.WALLET_WITHDRAW = "பணப்பையில் இருந்து திரும்பப்பெறும் தொகை";
notifyMessage.WALLET_WITHDRAW_SUCCESS =
  "பணப்பையை திரும்பப் பெறுதல் வெற்றிகரமாக முடிந்தது";
notifyMessage.WALLET_DEBIT = "வாலட் தொகை டெபிட் செய்யப்பட்டது";

notifyMessage.ERROR_WALLET_RECHARGE =
  "வாலட் ரீசார்ஜ் செய்யும் போது பிழை ஏற்பட்டது";
notifyMessage.ERROR_WALLET_WITHDRAW = "பணப்பையை திரும்பப் பெறுவதில் பிழை";
notifyMessage.ERROR_PAYMENT_OPTION = "கட்டண விருப்பப் பட்டியலில் பிழை";
notifyMessage.ERROR_SEND_MONEY = "பணம் அனுப்புவதில் பிழை";

notifyMessage.BANK_CREDET_MONEY =
  "5- 15 வணிக நாட்களுக்குள் உங்கள் வங்கிக் கணக்கில் பணம் வரவு வைக்கப்படும்";

notifyMessage.AMOUT_SEND_TO_FRIEND = "நண்பருக்கு பணம் மாற்றப்பட்டது";

notifyMessage.BOOKING_CHARGE_FREEZED =
  "முன்பதிவு செய்வதற்கான முடக்கப்பட்ட தொகை";

notifyMessage.RIDE_CANCELLATION_CHARGE = "சவாரி ரத்து தொகை";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "தள்ளுபடிக்கான பணம் திரும்பப் பெறப்பட்டது";

notifyMessage.INFO_TRANSACTION_NOT_FOUND =
  "பரிவர்த்தனை விவரங்கள் கிடைக்கவில்லை";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "நீங்கள் இந்த URL ஐ சட்டவிரோதமாக பயன்படுத்துபவர் என்று நாங்கள் கருதுகிறோம், பதில் கிடைக்கவில்லை";
notifyMessage.INFO_CARD_ADDED = "கட்டண அட்டை வெற்றிகரமாக சேர்க்கப்பட்டது";
notifyMessage.INFO_CARD_VERIFIED = "கட்டண அட்டை வெற்றிகரமாக சரிபார்க்கப்பட்டது";
notifyMessage.INFO_BANK_DETAILS = "வங்கி விவரங்கள்";
notifyMessage.INFO_TRANSACTIONS_SUCCESS =
  "பரிவர்த்தனைகள் வெற்றிகரமாக முடிந்தது";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND =
  "வங்கி விவரங்களைக் கண்டுபிடிக்க முடியவில்லை";
notifyMessage.INFO_CARD_DECLINED =
  "பணப்பையில் இருந்து கட்டணம் எடுக்கப்பட்டது. கட்டணம் செலுத்தியதால் கார்டு நிராகரிக்கப்பட்டது";
notifyMessage.INFO_AMOUNT_PAID = "தொகை வெற்றிகரமாக செலுத்தப்பட்டது";

notifyMessage.ERROR_CARD_ADD = "கட்டண அட்டையைச் சேர்க்க முடியாது";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "கட்டண அட்டையைச் சேர்ப்பதில் ஏதோ தவறு ஏற்பட்டது";
notifyMessage.ERROR_IN_TRANSACTIONS = "பரிவர்த்தனை விவரங்களில் பிழை";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "கட்டண அட்டை தோல்வியடைந்தது";

notifyMessage.INFO_SERVICE_NOT_PROVIDE =
  "மன்னிக்கவும், நாங்கள் இங்கு சேவையை வழங்கவில்லை";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "கோப்பு வெற்றிகரமாக பதிவேற்றப்பட்டது";
notifyMessage.INFO_NOT_FOUND = "விவரங்கள் கிடைக்கவில்லை";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "உங்கள் பணப்பையில் புள்ளி வெற்றிகரமாக சேர்க்கப்பட்டது  ";
notifyMessage.ERROR_WALLET_POINT_ADDED = "வாலட் பாயிண்ட்டைப் பெற முடியவில்லை";
notifyMessage.INFO_SUCCESS = "வெற்றி";
notifyMessage.INFO_FAILED = "தோல்வி";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "ரிவார்டு புள்ளிகளைப் பெறுங்கள்";
notifyMessage.TRAN_REDEEM_REWARD_MIN =
  "குறைந்தபட்ச வெகுமதி புள்ளிகளைப் பெறுங்கள்";
notifyMessage.TRAN_REDEEM_REWARD_MAX =
  "அதிகபட்ச ரிவார்டு புள்ளிகளைப் பெறுங்கள்";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "எங்கள் ஆபரேட்டர் உங்கள் பயண நிலையை வருவதற்கு மாற்றினார்";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "எங்கள் ஆபரேட்டர் உங்கள் பயண நிலையைத் தொடங்குவதற்கு மாற்றினார்";
notifyMessage.INFO_RIDE_CANCELLED_BY = "சவாரி ரத்து செய்யப்பட்டது";
notifyMessage.INFO_PROFESSIONAL = "தொழில்முறை";
notifyMessage.INFO_USER = "பயனர்";
notifyMessage.INFO_ADMIN = "நிர்வாகம்";
notifyMessage.INFO_INVALID_DETAILS = "தவறான விவரங்கள்";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "இந்த ஃபோன் எண் ஏற்கனவே கார்ப்பரேட் பெயரில் பதிவு செய்யப்பட்டுள்ளது";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "தவறான பரிந்துரைக் குறியீடு, உங்கள் குறியீட்டைச் சரிபார்த்து, மீண்டும் முயற்சிக்கவும்";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "Email Id Already Registered";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "தவறான DOB வடிவம் MM-DD-YYYY அல்லது MM/DD/YYYY ஆக இருக்க வேண்டும்";
notifyMessage.INFO_INVALID_AGE =
  "தொடர வயது {{minAgeKey}} மற்றும் அதற்கு மேல் இருக்க வேண்டும்";
notifyMessage.INFO_CARD_ALREADY_EXIST = "கார்டு ஏற்கனவே உள்ளது";
notifyMessage.INFO_WALLET_RECHARGE = "வாலட் ரீசார்ஜ்";
notifyMessage.INFO_INTERCITY_RIDE = "இன்டர்சிட்டி சவாரி";
notifyMessage.INFO_SITE_COMMISSION = "தள கமிஷன்";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "சவாரி ரத்து தொகை";
notifyMessage.INFO_ROUNDING = "ரவுண்டிங்";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "டிரைவர் ஏற்கனவே ரைடு செய்கிறார்";
notifyMessage.INFO_BASE_FARE = "அடிப்படை கட்டணம்";
notifyMessage.INFO_SERVICE_TAX = "சேவை வரி";
notifyMessage.INFO_TRAVEL_CHARGE = "பயண கட்டணம்";
notifyMessage.INFO_MINIMUM_CHARGE = "குறைந்தபட்ச கட்டணம்";
notifyMessage.INFO_SURCHARGE_FEE = "சர்சார்ஜ் கட்டணம்";
notifyMessage.INFO_BOUNDARY_CHARGE = "எல்லைக் கட்டணம்";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "எல்லை பிக்அப் கட்டணம்";
notifyMessage.INFO_OTP_MESSAGE = "உங்கள் {{siteTitle}} OTP குறியீடு {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "தவறான உள்நுழைவு முயற்சிகளின் அதிகபட்ச எண்ணிக்கையை நீங்கள் அடைந்துள்ளதால், உங்கள் கணக்கு தற்காலிகமாக இடைநிறுத்தப்பட்டுள்ளது.";
notifyMessage.INFO_HUBS_EARNINGS = "கடை கமிஷன் வருவாய்";
notifyMessage.INFO_FELLOW_PASSENGER = "சக பயணி";
notifyMessage.INFO_ALREADY_EXISTS = "ஏற்கனவே உள்ளது";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "தவறான கூப்பன் குறியீடு, உங்கள் குறியீட்டைச் சரிபார்க்கவும்.";
notifyMessage.WALLET_RECHARGE_FAILED = "வாலட் ரீசார்ஜ் தோல்வியடைந்தது.";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "உங்கள் வாகன ஆவணங்கள் காலாவதியாகிவிட்டது. தயவு செய்து சரிபார்த்து புதுப்பிக்கவும்";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "உங்கள் சுயவிவர ஆவணங்கள் காலாவதியாகிவிட்டது. தயவு செய்து சரிபார்த்து புதுப்பிக்கவும்";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "உங்கள் வாகன ஆவணங்கள் {{days}} நாளில் காலாவதியாகிவிடும். தயவுசெய்து சரிபார்த்து புதுப்பிக்கவும்";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "உங்கள் சுயவிவர ஆவணங்கள் {{days}} நாளில் காலாவதியாகிவிடும். தயவுசெய்து சரிபார்த்து புதுப்பிக்கவும்";
notifyMessage.INFO_INCENTIVE_CREDIT = "ஊக்கத்தொகை வரவு";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "உதவி பராமரிப்பு கட்டணம்";
notifyMessage.INFO_TIME_FARE = "நேர கட்டணம்";
notifyMessage.INFO_DISTANCE_FARE = "தூர கட்டணம்";
notifyMessage.INFO_PEAK_FARE = "உச்சகட்ட கட்டணம்";
notifyMessage.INFO_NIGHT_FARE = "இரவு கட்டணம்";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "இணைப்பு துண்டிக்கப்பட்டது. இப்போது ஆஃப்லைனில் உள்ளீர்கள். சவாரி கோரிக்கைகளை எடுக்க ஆன்லைனில் செல்லவும்.";
notifyMessage.AIRPORT_FEE = "விமான நிலைய கட்டணம்";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "இந்த தொழில்முறை தேர்ந்தெடுக்கப்பட்ட கட்டண விருப்பத்தை ஏற்கவில்லை. கட்டண விருப்பத்தை மாற்றவும்.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "உங்கள் மதிப்பீடு வரம்பிற்குக் கீழே உள்ளது, மேம்படுத்தவும் அல்லது உங்கள் கணக்கு சில நாட்களில் தடுக்கப்படும்.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "உங்கள் மதிப்பீடு வரம்பிற்குக் குறைவாக இருப்பதால் உங்கள் கணக்கு தடுக்கப்பட்டது. கணக்கு {{date}} நள்ளிரவில் மீண்டும் செயல்படுத்தப்படும்.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "உங்கள் கணக்கு செயல்படுத்தப்பட்டது. சவாரி கோரிக்கைகளை எடுக்க ஆன்லைனில் செல்லவும்.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "நண்பரிடமிருந்து மாற்றப்பட்ட தொகை";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "கார்டு சரிபார்ப்பு தோல்வியடைந்தது";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "பேங்க் சர்வர் இப்போது பிஸி. பிறகு முயற்சிக்கவும்";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV வெற்றிகரமாக சரிபார்க்கப்பட்டது";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV சரிபார்ப்பு தோல்வியடைந்தது";
notifyMessage.ALERT_CHECK_CPF = "CPF எண்ணைச் சரிபார்க்கவும்.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "தயவுசெய்து தேதியைச் சரிபார்க்கவும்.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "நகரம் ஏற்கனவே உள்ளது.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF எண் ஏற்கனவே உள்ளது.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "அதிகபட்ச திரும்பப் பெறும் தொகை வரம்பு {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "ஒரு நாளைக்கு ஒரு முறை பணம் திரும்பப் பெற அனுமதிக்கப்படுகிறது.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "கணக்கு வைத்திருப்பவரின் பெயர்";
notifyMessage.INFO_BANK_NAME = "வங்கி பெயர்";
notifyMessage.INFO_DOCUMENT_NUMBER = "ஆவண எண்";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "பணப்பையை திரும்பப் பெற முடியவில்லை. பணப்பையில் வரவு வைக்கப்பட்ட தொகை.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "சேவை இருப்பிடம் எதுவும் கிடைக்கவில்லை";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "உங்கள் சக பயணி பிக்அப் இடத்திற்கு வந்துவிட்டார்";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "உங்கள் சக பயணி பயணத்தைத் தொடங்கியுள்ளார்";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "உங்கள் சக பயணிகளுக்கான சவாரி முடிவுக்கு வந்துவிட்டது.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "இந்த மின்னஞ்சல் ஏற்கனவே மற்றொரு கணக்குடன் இணைக்கப்பட்டுள்ளது. தயவுசெய்து வேறு மின்னஞ்சலைப் பயன்படுத்தவும்.";
notifyMessage.INFO_TRIP_NUMBER = "பயண எண்";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "சேவை பகுதி ஏற்கனவே உள்ளது.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "கார்ப்பரேட் ஏற்கனவே உள்ளது.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "சேவைப் பகுதி ஏற்கனவே உள்ளது.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "உங்கள் ஆவண சரிபார்ப்பு மதிப்பாய்வில் உள்ளது, சரிபார்ப்பு முடியும் வரை காத்திருக்கவும்.";
//--------- Newly added -------------";
//-------------------------
module.exports = notifyMessage;
