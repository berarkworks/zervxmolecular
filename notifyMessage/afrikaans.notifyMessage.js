const notifyMessage = {};

notifyMessage.CREATED = "Suksesvol geskep";
notifyMessage.UPDATED = "Suksesvol opgedateer";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "ONGELDIGE GEBRUIKERSDATA";
notifyMessage.TRIP_END = "REIS IS BEËINDIG";
notifyMessage.WALLET_LOADED = "Beursiebedrag gelaai";
notifyMessage.CASH = "Kontant";
notifyMessage.CARD = "Kaart";
notifyMessage.WALLET = "Beursie";
notifyMessage.TRIP_FEE = "Reisfooi";
notifyMessage.TOTAL_BILL = "Totale rekening";
notifyMessage.EARNINGS = "Jou verdienste";
notifyMessage.WAITING_CHARGE = "Wagheffing";
notifyMessage.PENDING_AMOUNT = "Hangende bedrag";
notifyMessage.CASH_IN_HAND = "Kontant In Hand";
notifyMessage.BOOKING_ACCEPTED = "JOU BESPREKING WORD AANVAAR";
notifyMessage.BOOKING_PAID = "Betaal vir bespreking";
notifyMessage.PROFESSIONAL_STARTED = "PROFESSIONELE BEGIN MET JOU REIS";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "Ligging is suksesvol verander";
notifyMessage.PROFESSIONAL_ARRIVED =
  "PROFESSIONEEL HET BY JOU AFHAALPLEK AANGEKOM";
notifyMessage.PROFESSIONAL_COMMISSION = "Professionele kommissie";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Besprekingsverdienste ontvang";
notifyMessage.PROFESSIONAL_INFO =
  "BEREIK ASSEBLIEF JOU PORTEFEUILLE OM AANLYN TE GAAN";
notifyMessage.PROFESSIONAL_NOT_FOUND = "PROFESSIONELE NIE GEVIND NIE";
notifyMessage.PROFESSIONAL_NEW_RIDE = "JY HET 'N NUWE RIT GEKRY";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "RIT GEKANSELLEER DEUR PROFESSIONAL";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "JOU BESPREKING WORD AANVAAR DEUR ONS PROFESSIONELE";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "KENNISGEWING STUUR AAN NABYE PROFESSIONELE";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "PROFESSIONELE HET BY STOPPLEK AANGEKOM";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "PROFESSIONELE BEGIN DIE RIT VANAF STOPPLEK";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "PROFESSIONELE HET BY JOU BESPREKINGSPLEK AANGEKOM";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "PROFESSIONELE BEGIN VANAF BESPREKINGSPLEK";
notifyMessage.USER_NOT_FOUND = "GEBRUIKER NIE GEVIND NIE";
notifyMessage.USER_HAVE_RIDE = "GEBRUIKER HET REEDS GERY";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "IETS HET VERKEERD GAAN IN GEBRUIKER KANSELLEER HIERDIE RIT";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Laai ASSEBLIEF JOU BELANGRIJK OF KIES ENIGE ANDER BETALINGSOPSIE";
notifyMessage.USER_PAYMENT_DECLINED =
  "Hierdie betaalmetode is afgekeur. Kies asseblief 'n ander betaalmetode";
notifyMessage.INVALID_PROFESSIONAL = "Ongeldige professionele besonderhede";
notifyMessage.INVALID_BOOKING = "Ongeldige besprekingsbesonderhede";
notifyMessage.INVALID_ACCESS = "ONGELDIGE TOEGANG";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "VOERTUIG KATEGORIE NIE GEVIND NIE";
notifyMessage.VEHICLE_NOT_AVAILABLE = "GEEN VOERTUIG IS BESKIKBAAR NIE";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "DIENSKATEGORIE NIE GEVIND NIE";
notifyMessage.BOOKING_SCHEDULED_RIDE = "BESPREKING IS SUKSESVOL GESKEDULEER";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND =
  "BESPREKINGSKATEGORIE NIE GEVIND NIE";
notifyMessage.BOOKING_ALREADY = "BESPREKING IS REEDS";
notifyMessage.BOOKING_STATUS_IS = "BESPREKING IS";
notifyMessage.BOOKING_STATUS_INFO =
  "BESPREKING IS REEDS IN VOLGENDE OF GEËINDIG";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "BESPREKING GEKANSELLEER DEUR BESTUURDER WAT NA 'N ANDER BESTUURDER KONTROLEER";
notifyMessage.BOOKING_CANCELLED_BY = "BESPREKING GEKANSELLEER DEUR";
notifyMessage.BOOKING_CANCELLED_BY_USER =
  "BESPREKING GEKANSELLEER DEUR GEBRUIKER";
notifyMessage.BOOKING_CANCELLED = "BESPREKING IS SUKSESVOL GEKANSELLEER";
notifyMessage.BOOKING_ACCEPTED_BY_ADMIN =
  "NUWE BESPREKING WORD DEUR ADMIN TOEGEKEN";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "VERSOEK IS SUKSESVOL TOEGEKEN";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "BESPREKING WORD REEDS AANVAAR DEUR 'N ANDER DRYWER";
notifyMessage.BOOKING_UPDATE_FAILED =
  "KAN NIE BESPREKINGSBESONDERHEDE OPDATEER IETS HET VERKEERD GAAN";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "FOUT IN BESPREKINGSSTATUSOPDATERING";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "KANSELLASIE REDE NIE GEVIND NIE";
notifyMessage.SOMETHING_WENT_WRONG = "KANSELLASIE REDE NIE GEVIND NIE";
notifyMessage.RIDE_CANCELLED = "RIT IS SUKSESVOL GEKANSELLEER";
notifyMessage.RIDE_IS = "HIERDIE RIT IS";
notifyMessage.RIDE_STATUS_INFO = "RIT HET REEDS GEËINDIG OF NOG NIE BEGIN NIE";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "FOUT BY DIE VERANDERING VAN RITPLEKKE";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "RIT-ADRES HET DEUR GEBRUIKER VERANDER";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Ritadres het deur professionele persoon verander";
notifyMessage.OTP_INCORRECT = "OTP IS VERKEERD VOER ASSEBLIEF KORREKTE OTP in";
notifyMessage.RATING_SUBMITED = "GRAADERING SUKSESVOL INGEDIEN";
notifyMessage.RATING_UPDATE_FAILED = "FOUT IN GRAADERING OPDATERING";
notifyMessage.LOCATION_ACCESS_FAILED =
  "Iets het verkeerd geloop in jou ligging";
notifyMessage.TRIP_NOT_AVAILABLE = "WENKE IS NIE BESKIKBAAR NIE";
notifyMessage.TRIP_SHARED = "REIS SUKSESVOL GEDEEL";
notifyMessage.TRIP_END_PROFESSIONAL = "JOU RIT IS BEËINDIG DEUR PROFESSIONAL";
notifyMessage.OPERATOR_STATUS_END =
  "ONS OPERATOR HET JOU REISSTATUS VERANDER OM TE EINDIG";
notifyMessage.ERROR_SECURITY_UPDATE = "FOUT IN SEKURITEITSOPDATERING";
notifyMessage.ERROR_GENEREL_SETTINGS = "FOUT IN ALGEMENE INSTELLINGS";
notifyMessage.WISHLIST_ACTION_FAILED = "AKSIE {{aksie}} WENSLYS IS MISLUK";
notifyMessage.SUR_CHARGE = "Sur charge";
notifyMessage.SERVICE_TAX = "Diensbelasting";
notifyMessage.TIPS_AMOUNT = "Wenke Bedrag";
notifyMessage.COUPON_SAVINGS = "Koepon Spaar";
notifyMessage.TOLL_FEE = "Tolgeld";
notifyMessage.DISCOUNT_APPLIED = "Afslag Toegepas";
notifyMessage.REFUND_TO_WALLET = "Terugbetaling aan Wallet";
notifyMessage.TIPS_MINIMUM = "WENKE MOET MINIMUM WEES";
notifyMessage.TIPS_MAXIMUM = "WENKE MOET MAKSIMUM WEES";
notifyMessage.TIPS_ADDED = "WENKE SUKSESVOL BYGEVOEG";
notifyMessage.ERROR_PAYMENT_CARD =
  "FOUT MET KAARTBETALING, PROBEER ASSEBLIEF NA EEN TYD";
notifyMessage.ERROR_EARNINGS = "FOUT IN VERDIENSTE";
notifyMessage.ERROR_DAY_EARNINGS = "FOUT IN DAG VERDIENSTE";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "DIT BLYKLIK DIE PROFESSIONELE IS VANAF. VRA ASSEBLIEF DIE PROFESSIONELE OM AANLYN TE GAAN VOORDAT JY HIERDIE REIS BESPREEK";
notifyMessage.INFO_PAYMENT_OPTIONS = "BETALINGSOPSIES NIE GEVIND NIE";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "IETS HET VERKEERD GAAN IN GATEWAY-GEBEURE GAAN DEUR VOLTOOI STAP VIR STAP NA";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "KAN GEBRUIKERS NIE VIND NIE IETS HET VERKEERD GAAN IN DIE BEDRAG VAN DIE beursie herlaai";
notifyMessage.INFO_CHECK_CREDENTIAL =
  "KONTROLEER ASSEBLIEF DIE VOLGENDE AANWYSINGS";
notifyMessage.INFO_SEND_MONEY = "GELD SUKSESVOL GESTUUR";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Betaal vir inskrywing";
notifyMessage.INFO_TIPS = "Jy het wenke gegee";
notifyMessage.INFO_JOINING_CHARGE = "Aansluiting bedrag";
notifyMessage.INFO_REFERREL_CHARGE = "Verwysbedrag";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Oorblywende betalingsbedrag gelaai";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT =
  "Oorblywende betalingsbedrag gedebiteer";
notifyMessage.WALLET_AMOUNT_LOADED =
  "BEDRAG WORD SUKSESVOL BY JOU PORTEFEUILLE TOEGEVOEG";
notifyMessage.WALLET_NOTIFICATION =
  "IETS HET VERKEERD GAAN IN WALLET UPDATE IN WALLET RECHARE NO AUTH";
notifyMessage.WALLET_WITHDRAW = "BEDRAG ONTTREK UIT WALLET";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "WALLET ONTTREKKING SUKSESVOL";
notifyMessage.WALLET_DEBIT = "Beursiebedrag gedebiteer";
notifyMessage.ERROR_WALLET_RECHARGE =
  "FOUT HET VOORKOM TERWYL MUURLEET HERLAAI";
notifyMessage.ERROR_WALLET_WITHDRAW = "FOUT IN WALLET ONTTREKKING";
notifyMessage.ERROR_PAYMENT_OPTION = "FOUT IN BETALINGSOPSIE LYS";
notifyMessage.ERROR_SEND_MONEY = "FOUT IN STUUR GELD";
notifyMessage.BANK_CREDET_MONEY =
  "GELD SAL BINNE 7 BESIGHEIDSDAE AAN JOU BANKREKENING GESKREDIET WORD";
notifyMessage.AMOUT_SEND_TO_FRIEND = "Oorgeplaas na Vriend";
notifyMessage.BOOKING_CHARGE_FREEZED = "Bevriesde bedrag vir bespreking";
notifyMessage.RIDE_CANCELLATION_CHARGE = "Rit kanselleer bedrag";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT =
  "Terugbetaling vir afslag toegepas";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "TRANSAKSIE WORD NIE GEVIND NIE";
notifyMessage.INFO_RESPONSE_NOT_FOUND = "ANTWOORD NIE GEVIND NIE.";
notifyMessage.INFO_CARD_ADDED = "KAART IS SUKSESVOL BYGEVOEG";
notifyMessage.INFO_CARD_VERIFIED = "KAART IS SUKSESVOL GEVERIFIEER";
notifyMessage.INFO_BANK_DETAILS = "BANKBESONDERHEDE";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "TRANSAKSIES SUKSES";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND =
  "KAN NIE DIE BANKBESONDERHEDE VIND NIE";
notifyMessage.INFO_CARD_DECLINED =
  "BEDRAG WAT VAN WALLET AFgetrek word. WEENS KAART GEWEIER";
notifyMessage.INFO_AMOUNT_PAID = "BEDRAG SUKSESVOL BETAAL";
notifyMessage.ERROR_CARD_ADD = "KAN NIE 'N KAART BYVOEG NIE";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "IETS HET VERKEERD GAAN MET KAARTBYVOEGING";
notifyMessage.ERROR_IN_TRANSACTIONS = "FOUT IN TRANSAKSIES";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "KAARTBETALING HET MISLUUK";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "JAMMER ONS LEWER NIE DIENS HIER NIE";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Lêer is suksesvol opgelaai";
notifyMessage.INFO_NOT_FOUND = "BESONDERHEDE NIE GEVIND NIE";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "PUNT WORD SUKSESVOL BY JOU PORTEFEUILLE gevoeg";
notifyMessage.ERROR_WALLET_POINT_ADDED = "NIE IN STAAT OM WALLET PONT NIE";
notifyMessage.INFO_SUCCESS = "SUKSES";
notifyMessage.INFO_FAILED = "MISLUK";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "Gebruik beloningspunte";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Gebruik minimum beloningspunte";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Gebruik maksimum beloningspunte";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "ONS OPERATOR HET JOU REISSTATUS VERANDER NA AANKOMS";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "ONS OPERATOR HET JOU REISSTATUS VERANDER OM TE BEGIN";
notifyMessage.INFO_RIDE_CANCELLED_BY = "RIT IS GEKANSELLEER DEUR";
notifyMessage.INFO_PROFESSIONAL = "PROFESSIONEEL";
notifyMessage.INFO_USER = "GEBRUIKER";
notifyMessage.INFO_ADMIN = "ADMIN";
notifyMessage.INFO_INVALID_DETAILS = "Ongeldige besonderhede";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Hierdie telefoonnommer is reeds in die korporatiewe naam geregistreer";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "ONGELDIGE VERWYSINGSKODE, KONTROLEER ASSEBLIEF JOU KODE EN PROBEER WEER";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "E-pos-ID reeds geregistreer";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "ONGELDIGE DOB-FORMAAT MOET MM-DD-JJJJ OF MM/DD/JJJJ WEES";
notifyMessage.INFO_INVALID_AGE =
  "OUDERDOM MOET {{minAgeKey}} EN BO WEES OM VOORT TE GAAN";
notifyMessage.INFO_CARD_ALREADY_EXIST = "KAART BESTAAN REEDS";
notifyMessage.INFO_WALLET_RECHARGE = "Herlaai beursie";
notifyMessage.INFO_INTERCITY_RIDE = "Intercity rit";
notifyMessage.INFO_SITE_COMMISSION = "Terreinkommissie";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Ritkansellasiekrediet";
notifyMessage.INFO_ROUNDING = "Afronding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "PROFESSIONELE HET REEDS GERY";
notifyMessage.INFO_BASE_FARE = "Basistarief";
notifyMessage.INFO_SERVICE_TAX = "Diensbelasting";
notifyMessage.INFO_TRAVEL_CHARGE = "Reisheffing";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum heffing";
notifyMessage.INFO_SURCHARGE_FEE = "Toeslag fooi";
notifyMessage.INFO_BOUNDARY_CHARGE = "grenslading";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity afhaalfooi";
notifyMessage.INFO_OTP_MESSAGE = "Jou {{siteTitle}} OTP-kode is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Jou rekening is gesluit omdat jy die maksimum aantal ongeldige aanmeldpogings bereik het.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Verdienste";
notifyMessage.INFO_FELLOW_PASSENGER = "MEDE PASSASIER";
notifyMessage.INFO_ALREADY_EXISTS = "BESTAAN REEDS";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Ongeldige koeponkode, gaan asseblief jou kode na.";
notifyMessage.WALLET_RECHARGE_FAILED = "WALLET HERLAAI HET MISLUUK";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "JOU VOERTUIG DOKUMENTE IS VERVAL. KONTROLEER EN DATEER ASSEBLIEF OP";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "JOU PROFIEL DOKUMENTE IS VERVAL. KONTROLEER EN DATEER ASSEBLIEF OP";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Jou voertuigdokumente sal oor {{days}} dag(e) verval. Gaan asseblief na en werk op";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Jou profieldokumente sal oor {{days}} dag(e) verval. Gaan asseblief na en werk op";
notifyMessage.INFO_INCENTIVE_CREDIT = "Aansporing gekrediteer";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Bystandsorgfooi";
notifyMessage.INFO_TIME_FARE = "Tyd Tarief";
notifyMessage.INFO_DISTANCE_FARE = "Afstand Tarief";
notifyMessage.INFO_PEAK_FARE = "Piektarief";
notifyMessage.INFO_NIGHT_FARE = "Nagtarief";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oeps! Verbinding is verlore. Jy is nou vanlyn. Gaan asseblief aanlyn om ritversoeke te neem.";
notifyMessage.AIRPORT_FEE = "Lughawe Fooi";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "Hierdie professionele persoon aanvaar nie die geselekteerde betaalopsie nie. Verander asseblief die betaalopsie.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Jou gradering is onder die reeks, verbeter asseblief, of jou rekening sal oor 'n paar dae geblokkeer word.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Jou rekening is geblokkeer omdat jou gradering onder die reeks is. Die rekening sal op {{date}} middernag heraktiveer word.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Jou rekening is geaktiveer. Gaan asseblief aanlyn om ritversoeke te neem.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Bedrag oorgeplaas vanaf vriend";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Kaartvalidering het misluk";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bankbediener Nou besig. Probeer asseblief later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV suksesvol geverifieer";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV-verifikasie het misluk";
notifyMessage.ALERT_CHECK_CPF = "Gaan asseblief die CPF-nommer na.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Kontroleer asseblief die Van Datum / Tot Datum.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "Stad bestaan ​​reeds.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF-nommer bestaan ​​reeds.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maksimum onttrekkingsbedraglimiet is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Bedrag Onttrekking Een keer Per Dag toegelaat.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Rekeninghouer Naam";
notifyMessage.INFO_BANK_NAME = "Bank Naam";
notifyMessage.INFO_DOCUMENT_NUMBER = "Dokument No";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Beursie-onttrekking het misluk. Bedrag gekrediteer op die beursie.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Geen diensligging is beskikbaar nie";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Jou medepassasier het by die oplaaiplek aangekom";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Jou medepassasier het die rit begin";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "Die rit het tot 'n einde gekom vir jou medepassasier.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Hierdie e-pos word reeds met 'n ander rekening geassosieer. Gebruik asseblief 'n ander e-posadres.";
//--------- Newly added -------------
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------
module.exports = notifyMessage;
