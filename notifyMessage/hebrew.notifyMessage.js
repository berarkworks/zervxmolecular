const notifyMessage = {};

notifyMessage.CREATED = "נוצר בהצלחה";
notifyMessage.UPDATED = "עודכן בהצלחה";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "נתוני משתמש לא חוקיים";
notifyMessage.TRIP_END = "טיול הסתיים";
notifyMessage.WALLET_LOADED = "כמות הארנק טעונה";
notifyMessage.CASH = "כסף מזומן";
notifyMessage.CARD = "כַּרְטִיס";
notifyMessage.WALLET = "ארנק";
notifyMessage.TRIP_FEE = "דמי טיול";
notifyMessage.TOTAL_BILL = "שטר כולל";
notifyMessage.EARNINGS = "הרווחים שלך";
notifyMessage.WAITING_CHARGE = "חיוב מחכה";
notifyMessage.PENDING_AMOUNT = "סכום ממתין";
notifyMessage.CASH_IN_HAND = "מזומן ביד";
notifyMessage.BOOKING_ACCEPTED = "ההזמנה שלך מתקבלת";
notifyMessage.BOOKING_PAID = "שילם עבור ההזמנה";

notifyMessage.PROFESSIONAL_STARTED = "מקצוע המקצועי התחיל לטיול שלך";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "המיקום השתנה בהצלחה";
notifyMessage.PROFESSIONAL_ARRIVED = "מקצוען הגיע למיקום האיסוף שלך";

notifyMessage.PROFESSIONAL_COMMISSION = "ועדה מקצועית";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "הזמנת הרווחים שהתקבלו";

notifyMessage.PROFESSIONAL_INFO =
  "יתרת הארנק שלך ירדה מתחת למגבלה המינימלית הנדרשת כדי לקבל נסיעות במזומן. אנא טען מחדש את הארנק שלך, אחרת תוכל לקבל נסיעות בכרטיס ובארנק רק עד שהיתרה שלך תהיה מספקת";
notifyMessage.PROFESSIONAL_NOT_FOUND = "מקצועי לא נמצא";
notifyMessage.PROFESSIONAL_NEW_RIDE = "מקצועי לא נמצא";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "רכיבה שבוטלה על ידי Professional";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "ההזמנה שלך מתקבלת על ידי המקצוען שלנו";

notifyMessage.PROFESSIONAL_NOTIFICATION = "הודעה שלח לאנשי מקצוע סמוכים";

notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION = "מקצוען הגיע לעצור מיקום";

notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "מקצועי מתחיל את הנסיעה ממיקום עצירה";

notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "מקצוען הגיע למיקום ההזמנה שלך";

notifyMessage.PROFESSIONAL_RIDE_START_INFO = "מקצוע המקצועי התחיל מהזמנת מיקום";

notifyMessage.USER_NOT_FOUND = "המשתמש לא נמצא";
notifyMessage.USER_HAVE_RIDE = "למשתמש כבר יש רכיבה";
notifyMessage.USER_RIDE_CANCEL_FAILD = "משהו השתבש במשתמש בטל את הרכיבה הזו";

notifyMessage.USER_WALLET_PAYMENT_INFO =
  "אנא טוען את הארנק שלך או בחר כל אפשרות תשלום אחרת";

notifyMessage.USER_PAYMENT_DECLINED =
  "שיטת התשלום הזו נדחתה. אנא בחר שיטת תשלום נוספת";

notifyMessage.INVALID_PROFESSIONAL = "פרטים מקצועיים לא חוקיים";
notifyMessage.INVALID_BOOKING = "פרטי הזמנה לא חוקיים";
notifyMessage.INVALID_ACCESS = "גישה לא חוקית";

notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "קטגוריית רכב לא נמצאה";
notifyMessage.VEHICLE_NOT_AVAILABLE = "אין רכב זמין";

notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "קטגוריית השירות לא נמצאה";

notifyMessage.BOOKING_SCHEDULED_RIDE = "הזמנה מתוזמנת בהצלחה";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "קטגוריית הזמנה לא נמצאה";
notifyMessage.BOOKING_ALREADY = "ההזמנה כבר";
notifyMessage.BOOKING_STATUS_IS = "ההזמנה היא";
notifyMessage.BOOKING_STATUS_INFO = "הזמנה כבר בעיצומה או הסתיימה";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "הזמנה שבוטלה על ידי מנהל התקן בדיקת מנהל התקן אחר";

notifyMessage.BOOKING_CANCELLED_BY = "הזמנה שבוטלה על ידי";
notifyMessage.BOOKING_CANCELLED_BY_USER = "הזמנה מבוטלת על ידי המשתמש";
notifyMessage.BOOKING_CANCELLED = "ההזמנה מבוטלת בהצלחה";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "הזמנה חדשה מוקצה על ידי מנהל";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "בקשה שהוקצה בהצלחה";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS = "ההזמנה כבר מתקבלת על ידי נהג אחר";

notifyMessage.BOOKING_UPDATE_FAILED =
  "לא יכול לעדכן את ההזמנה לפרט שמשהו השתבש";

notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "שגיאה בעדכון סטטוס ההזמנה";

notifyMessage.CANCELLATION_REASON_NOT_FOUND = "סיבת ביטול לא נמצאה";

notifyMessage.SOMETHING_WENT_WRONG = "סיבת ביטול לא נמצאה";

notifyMessage.RIDE_CANCELLED = "הרכיבה בוטלה בהצלחה";
notifyMessage.RIDE_IS = "הרכיבה הזו היא";
notifyMessage.RIDE_STATUS_INFO = "הרכיבה כבר הסתיימה או עדיין לא התחילה";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "שגיאה בשינוי מיקומי נסיעה";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER = "כתובת הרכיבה השתנתה על ידי המשתמש";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "כתובת הנסיעה השתנתה על ידי איש מקצוע";
notifyMessage.OTP_INCORRECT = "OTP שגוי הזן בחביבות OTP נכון";

notifyMessage.RATING_SUBMITED = "הדירוג הוגש בהצלחה";
notifyMessage.RATING_UPDATE_FAILED = "שגיאה בעדכון הדירוג";

notifyMessage.LOCATION_ACCESS_FAILED = "משהו השתבש במיקום שלך";

notifyMessage.TRIP_NOT_AVAILABLE = "טיפים אינם זמינים";
notifyMessage.TRIP_SHARED = "טיול משותף בהצלחה";
notifyMessage.TRIP_END_PROFESSIONAL = "הרכיבה שלך הסתיימה על ידי מקצוען";

notifyMessage.OPERATOR_STATUS_END = "המפעיל שלנו שינה את סטטוס הטיול שלך לסיום";

notifyMessage.ERROR_SECURITY_UPDATE = "שגיאה בעדכון האבטחה";
notifyMessage.ERROR_GENEREL_SETTINGS = "שגיאה בהגדרות גנרל";

notifyMessage.WISHLIST_ACTION_FAILED = "פעולה {{Action}} רשימת המשאלות נכשלה";

notifyMessage.SUR_CHARGE = "סור מטען";
notifyMessage.SERVICE_TAX = "מס שירות";
notifyMessage.TIPS_AMOUNT = "סכום טיפים";
notifyMessage.COUPON_SAVINGS = "חיסכון בקופונים";
notifyMessage.TOLL_FEE = "אגרה";
notifyMessage.DISCOUNT_APPLIED = "הנחה מיושמת";
notifyMessage.REFUND_TO_WALLET = "החזר לארנק";

notifyMessage.TIPS_MINIMUM = "טיפים לבלוט להיות מינימום של";
notifyMessage.TIPS_MAXIMUM = "טיפים לבלום להיות מקסימום של";
notifyMessage.TIPS_ADDED = "טיפים שנוספו בהצלחה";

notifyMessage.ERROR_PAYMENT_CARD = "שגיאה בתשלום כרטיס, נסה אנא לאחר מתישהו";

notifyMessage.ERROR_EARNINGS = "שגיאה ברווחים";
notifyMessage.ERROR_DAY_EARNINGS = "שגיאה ברווחי היום";

notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "נראה כי המקצוען אינו מיוחד. אנא בקש מהמקצוען להיכנס לרשת לפני שתזמין טיול זה";

notifyMessage.INFO_PAYMENT_OPTIONS = "אפשרויות תשלום לא נמצאו";
notifyMessage.INFO_CARD = "לא יכול למצוא את פרטי הכרטיס";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "משהו השתבש באירועי Gateway בדוק צעד אחר צעד";

notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "לא יכול למצוא למשתמש משהו השתבש בסכום הארנק טעינה";

notifyMessage.INFO_CHECK_CREDENTIAL = "אנא עיין בתעודה הבאה";
notifyMessage.INFO_SEND_MONEY = "כסף שנשלח בהצלחה";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "שילם עבור מנוי";
notifyMessage.INFO_TIPS = "נתת טיפים";
notifyMessage.INFO_JOINING_CHARGE = "סכום מצטרף";
notifyMessage.INFO_REFERREL_CHARGE = "סכום הפניה";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "סכום התשלום שנותר טעון";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "סכום התשלום שנותר חויב";

notifyMessage.WALLET_AMOUNT_LOADED = "הסכום מתווסף לארנק בהצלחה";

notifyMessage.WALLET_NOTIFICATION = 'משהו השתבש בעדכון הארנק ב"ארנק "לא';

notifyMessage.WALLET_WITHDRAW = "הסכום נסוג מהארנק";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "נסיגת ארנק בהצלחה";
notifyMessage.WALLET_DEBIT = "סכום הארנק חויב";

notifyMessage.ERROR_WALLET_RECHARGE = "שגיאה התרחשה בזמן טעינה של ארנק";
notifyMessage.ERROR_WALLET_WITHDRAW = "שגיאה בנסיגת הארנק";
notifyMessage.ERROR_PAYMENT_OPTION = "שגיאה ברשימת אפשרויות התשלום";
notifyMessage.ERROR_SEND_MONEY = "שגיאה בשליחת כסף";

notifyMessage.BANK_CREDET_MONEY =
  "הכסף יהיה אשראי לחשבון בנק YOUT תוך  7 ימי עסקים";

notifyMessage.AMOUT_SEND_TO_FRIEND = "הועבר לחבר";

notifyMessage.BOOKING_CHARGE_FREEZED = "סכום הקפאה להזמנה";

notifyMessage.RIDE_CANCELLATION_CHARGE = "סכום לביטול רכב";

notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "החזר להנחה שהוחל";

notifyMessage.INFO_TRANSACTION_NOT_FOUND = "עסקה לא נמצאת";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "תגובה לא נמצאה אנו מניחים שאתה משתמש לא חוקי בכתובת האתר הזו";

notifyMessage.INFO_CARD_ADDED = "הכרטיס מתווסף בהצלחה";
notifyMessage.INFO_CARD_VERIFIED = "כרטיס אומת בהצלחה";
notifyMessage.INFO_BANK_DETAILS = "פרטי בנק";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "הצלחת עסקאות";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "לא יכול למצוא את פרטי הבנק";
notifyMessage.INFO_CARD_DECLINED = "הסכום המנוכה מהארנק. בגלל הכרטיס ירד";

notifyMessage.INFO_AMOUNT_PAID = "הסכום ששולם בהצלחה";

notifyMessage.ERROR_CARD_ADD = "לא יכול להוסיף כרטיס";
notifyMessage.ERROR_CARD_ADD_FAILED = "משהו השתבש בכרטיס הוסיף";
notifyMessage.ERROR_IN_TRANSACTIONS = "שגיאה בעסקאות";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "תשלום הכרטיס נכשל";

notifyMessage.INFO_SERVICE_NOT_PROVIDE = "מצטער שאנחנו לא מספקים שירות כאן";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "הקובץ הועלה בהצלחה";
notifyMessage.INFO_NOT_FOUND = "פרטים לא נמצאו";
notifyMessage.INFO_WALLET_POINT_LOADED = "נקודה מתווספת לארנק שלך בהצלחה";

notifyMessage.ERROR_WALLET_POINT_ADDED = "לא מסוגל לנקוט ארנק";
notifyMessage.INFO_SUCCESS = "הַצלָחָה";
notifyMessage.INFO_FAILED = "נִכשָׁל";

notifyMessage.TRAN_REDEEM_REWARD_POINT = "פדה נקודות תגמול";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "מימש נקודות תגמול מינימליות";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "לממש נקודות תגמול מקסימום";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "המפעיל שלנו שינה את סטטוס הטיול שלך להגיע";

notifyMessage.INFO_OPERATOR_STATUS_START =
  "המפעיל שלנו שינה את סטטוס הטיול שלך כדי להתחיל";

notifyMessage.INFO_RIDE_CANCELLED_BY = "הרכיבה בוטלה על ידי";
notifyMessage.INFO_PROFESSIONAL = "מקצועי";
notifyMessage.INFO_USER = "מִשׁתַמֵשׁ";
notifyMessage.INFO_ADMIN = "מנהל";
notifyMessage.INFO_INVALID_DETAILS = "פרטים לא חוקיים";

notifyMessage.INFO_OFFICER_ALREADY_EXISTS = "מספר טלפון זה כבר רשום בשם תאגידי";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "קוד הפניה לא חוקי, אנא בדוק את הקוד שלך ונסה שוב";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = 'מזהה דוא"ל כבר נרשם';
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "פורמט DOB לא חוקי חייב להיות mm-dd-yyyy או mm/dd/yyyy";
notifyMessage.INFO_INVALID_AGE =
  "הגיל חייב להיות {{minAgeKey}} ומעלה כדי להמשיך";
notifyMessage.INFO_CARD_ALREADY_EXIST = "הכרטיס כבר קיים";
notifyMessage.INFO_WALLET_RECHARGE = "טעינה של ארנק";
notifyMessage.INFO_INTERCITY_RIDE = "נסיעה בין -עירונית";
notifyMessage.INFO_SITE_COMMISSION = "ועדת אתרים";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "זיכוי לביטול";
notifyMessage.INFO_ROUNDING = "עיגול";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "למקצוענים כבר יש רכיבה";
notifyMessage.INFO_BASE_FARE = "מחיר בסיס";
notifyMessage.INFO_SERVICE_TAX = "מס שירות";
notifyMessage.INFO_TRAVEL_CHARGE = "דמי נסיעה";
notifyMessage.INFO_MINIMUM_CHARGE = "מינימום חיוב";
notifyMessage.INFO_SURCHARGE_FEE = "עמלת תוספת";
notifyMessage.INFO_BOUNDARY_CHARGE = "תשלום גבול";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "דמי איסוף בין עירוניים";
notifyMessage.INFO_OTP_MESSAGE = "קוד ה-OTP של {{siteTitle}} שלך הוא {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "החשבון שלך ננעל מכיוון שהגעת למספר המרבי של ניסיונות כניסה לא חוקיים.";
notifyMessage.INFO_HUBS_EARNINGS = "רווחי עמלת חנות";
notifyMessage.INFO_FELLOW_PASSENGER = "נוסע עמית";
notifyMessage.INFO_ALREADY_EXISTS = "כבר קיים";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "קוד קופון לא חוקי, אנא בדוק את הקוד שלך";
notifyMessage.WALLET_RECHARGE_FAILED = "טעינת הארנק נכשלה";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "פג תוקף מסמכי הרכב שלך. אנא בדוק ועדכן";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "פג תוקף מסמכי הפרופיל שלך. אנא בדוק ועדכן";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "מסמכי הרכב שלך יפוג בעוד {{days}} ימים. אנא בדוק ועדכן";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "מסמכי הפרופיל שלך יפוג בעוד {{days}} ימים. אנא בדוק ועדכן";
notifyMessage.INFO_INCENTIVE_CREDIT = "זיכוי תמריץ";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "דמי טיפול בסיוע";
notifyMessage.INFO_TIME_FARE = "זמן נסיעה";
notifyMessage.INFO_DISTANCE_FARE = "מחיר מרחק";
notifyMessage.INFO_PEAK_FARE = "מחיר שיא";
notifyMessage.INFO_NIGHT_FARE = "ארוחת לילה";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "אופס! החיבור אבד. אתה במצב לא מקוון עכשיו. אנא היכנס לאינטרנט כדי לקבל בקשות נסיעה.";
notifyMessage.AIRPORT_FEE = "דמי שדה תעופה";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "איש מקצוע זה לא מקבל את אפשרות התשלום שנבחרה. אנא שנה את אפשרות התשלום.";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "הדירוג שלך נמצא מתחת לטווח, אנא שפר, אחרת חשבונך ייחסם בעוד מספר ימים.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "החשבון שלך נחסם כי הדירוג שלך נמוך מהטווח. החשבון יופעל מחדש בחצות {{date}}.";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "החשבון שלך מופעל. אנא היכנס לאינטרנט כדי לקבל בקשות נסיעה.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "סכום הועבר מחבר";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "אימות הכרטיס נכשל";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "שרת הבנק תפוס עכשיו. אנא נסה מאוחר יותר";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV אומת בהצלחה";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "אימות CVV נכשל";
notifyMessage.ALERT_CHECK_CPF = "אנא בדוק את מספר ה-CPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "אנא בדוק את מתאריך / עד תאריך.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "העיר כבר קיימת.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "מספר CPF כבר קיים.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "מגבלת סכום המשיכה המקסימלית היא {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT = "משיכת סכום מותרת פעם ביום.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "שם בעל החשבון";
notifyMessage.INFO_BANK_NAME = "שם הבנק";
notifyMessage.INFO_DOCUMENT_NUMBER = "מספר מסמך";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "משיכת הארנק נכשלה. סכום שזוכה בארנק.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE = "אין מיקום שירות זמין";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "הנוסע האחר שלך הגיע למקום האיסוף";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "הנוסע האחר שלך התחיל בנסיעה";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "הנסיעה הגיעה לסיומה עבור הנוסע האחר שלך.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  'דוא"ל זה כבר משויך לחשבון אחר. אנא השתמש באימייל אחר.';
notifyMessage.INFO_TRIP_NUMBER = "מספר טיול";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "מיקום השירות כבר קיים.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "החברה כבר קיימת.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS = "אזור השירות כבר קיים.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "אימות המסמך שלך בבדיקה, המתן עד להשלמת האימות.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
