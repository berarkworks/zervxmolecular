const notifyMessage = {};

notifyMessage.CREATED = "Yenziwe ngempumelelo";
notifyMessage.UPDATED = "ihlaziywe ngempumelelo";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "IDATHA YOMSEBENZISI ENGAYILUNGILEYO";
notifyMessage.TRIP_END = "UHAMBO LUPHELILE";
notifyMessage.WALLET_LOADED = "Isixa seWalethi siLayishiwe";
notifyMessage.CASH = "Imali";
notifyMessage.CARD = "Ikhadi";
notifyMessage.WALLET = "Isipaji";
notifyMessage.TRIP_FEE = "Umrhumo wohambo";
notifyMessage.TOTAL_BILL = "IBhili iyonke";
notifyMessage.EARNINGS = "Imivuzo Yakho";
notifyMessage.WAITING_CHARGE = "Intlawulo yokulinda";
notifyMessage.PENDING_AMOUNT = "Isixa esiLindiweyo";
notifyMessage.CASH_IN_HAND = "Imali Esesandleni";
notifyMessage.BOOKING_ACCEPTED = "UKUBHEKISELA KWAKHO KWAMMKILE";
notifyMessage.BOOKING_PAID = "Ihlawulelwe Ukubhukisha";
notifyMessage.PROFESSIONAL_STARTED = "UQALISELWE UHAMBO LWAKHO";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "Indawo Itshintshwe ngempumelelo";
notifyMessage.PROFESSIONAL_ARRIVED = "INGCALI UFIKILE KWINDAWO OYILANDELAYO";
notifyMessage.PROFESSIONAL_COMMISSION = "Ikhomishini yobungcali";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED =
  "Ingeniso yokubhukisha ifunyenwe";
notifyMessage.PROFESSIONAL_INFO =
  "NCEDA UFIKE IWALETHI YAKHO UKUZE UYE KWI-INTANETHI";
notifyMessage.PROFESSIONAL_NOT_FOUND = "INGCALI AYIFUMANANI";
notifyMessage.PROFESSIONAL_NEW_RIDE = "UFUMANE OKUNTSHA";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "UKUHAMBA LUKHANGISIWE YINKQUBO";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "UKUBEKELWA KWAKHO KWAMkelwe YINKQUBO YETHU";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "ISAZISO SITHUNYELWE KWIINGCALI EZIKUFUTSHANE";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "INGCALI UFIKILE UKUBA AMEkise indawo";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "UQALISE NGOBUCHULE UKUHAMBA UKUSUKA KWINDAWO YOKUMISA";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "INGCALI UFIKILE KWINDAWO OBHEKE kuyo";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "INKQUBO YOBUCHULE IQALE UKUBUKA INDAWO";
notifyMessage.USER_NOT_FOUND = "UMSEBENZISI AKAFUMANANI";
notifyMessage.USER_HAVE_RIDE = "UMSEBENZISI SELE EQHUBE";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "INTO AYIHHANGAMANGA KUMSEBENZISI LUKHANGELA OKU QHAWA";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "KHAWUBE UHLAWULE IWALETHI YAKHO OKANYE UKHETHE NALUPHI NA OKUNYE OKUNTLAWULO";
notifyMessage.USER_PAYMENT_DECLINED =
  "Le Ndlela Yentlawulo Yaliwe. Nceda ukhethe enye indlela yokuhlawula";
notifyMessage.INVALID_PROFESSIONAL = "Iinkcukacha zobuNgcali ezingasebenziyo";
notifyMessage.INVALID_BOOKING = "Iinkcukacha zokubhukisha ezingasebenziyo";
notifyMessage.INVALID_ACCESS = "UKUFIKELELA OKUNGAMELEKANGA";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "UDIDI LWEZITHUTHI ALUFUMANANI";
notifyMessage.VEHICLE_NOT_AVAILABLE = "AKUKHO SITHUTHI ESIFUMANEKAYO";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "UDIDI LWENKONZO ALUFUMANANI";
notifyMessage.BOOKING_SCHEDULED_RIDE = "UKUBHEKISHA KUCETYWELWE NGEMPUMELELO";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "UDIDI LOKUBHEKISHA ALIFUMANANI";
notifyMessage.BOOKING_ALREADY = "UKUBEKELWA SEKUBUKA";
notifyMessage.BOOKING_STATUS_IS = "UKUBEKELWA KU";
notifyMessage.BOOKING_STATUS_INFO = "UKUBHEKELWA SEKUQHUBEKA OKANYE KUPHELILE";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "UKUBHEKISELA KUKHANGISIWE NGU-Driver EJONGA OMNYE UMQHUBI";
notifyMessage.BOOKING_CANCELLED_BY = "UKUBHEKISELA KURHOXISIWE YI";
notifyMessage.BOOKING_CANCELLED_BY_USER = "UKUBHEKISELA KUCISIWE NGUMSEBENZISI";
notifyMessage.BOOKING_CANCELLED = "UKUBHEKISELA KURHOXISIWE NGEMPUMELELO";
notifyMessage.BOOKING_ACCEPTED_BY_ADMIN = "UKUBUKA OKUTSHA KWABELWA NGU-ADMIN";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "ISICELO SINIKEZELWE NGEMPUMELELO";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "UKUBEKELWA KWAMMKELWE OMNYE UMQHUBI";
notifyMessage.BOOKING_UPDATE_FAILED =
  "CANT UKUHLAZIYA IINKCUKACHA ZOKUBHEKISHA INTO AYIHAMBI kakuhle";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED =
  "Imposiso EKUBHEKISENI IMEKO YOHLAZIYO";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "ISIZATHU SOKURHOXA ASIFUMANANI";
notifyMessage.SOMETHING_WENT_WRONG = "ISIZATHU SOKURHOXA ASIFUMANANI";
notifyMessage.RIDE_CANCELLED = "UKUHAXA NGEMPUMELELO";
notifyMessage.RIDE_IS = "OKU KUKWEBA";
notifyMessage.RIDE_STATUS_INFO = "UKUHAMBA SEKUPHELELE OKANYE AKUKAQALALI";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED =
  "IPHOSO EKUTSHINTSHENI IINDAWO ZOKUHAMBA";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER =
  "Idilesi yokukhwela itshintshiwe NGUMSEBENZISI";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Idilesi Yokukhwela Itshintshwe Ngengcali";
notifyMessage.OTP_INCORRECT =
  "I-OTP AYIFANELEKILEYO NGOBELE FAKA I-OTP ELUNGILEYO";
notifyMessage.RATING_SUBMITED = "UHLELO LUNIKEZELWE NGEMPUMELELO";
notifyMessage.RATING_UPDATE_FAILED = "Imposiso EKUHLAZIYWENI KOMLINGANISO";
notifyMessage.LOCATION_ACCESS_FAILED =
  "Kukhona okungahambanga kakuhle kwindawo yakho";
notifyMessage.TRIP_NOT_AVAILABLE = "IINGCEBISO AYIFUMANANI";
notifyMessage.TRIP_SHARED = "UHAMBO LWABELWE NGEMPUMELELO";
notifyMessage.TRIP_END_PROFESSIONAL = "UHAMBO LWAKHO LUPHELILE NGENKCUKACHA";
notifyMessage.OPERATOR_STATUS_END =
  "UMSEBENZISI WETHU UTSHINTILE IMEKO YOHAMBO LWAKHO lwade lwaphela";
notifyMessage.ERROR_SECURITY_UPDATE = "Imposiso KUHLAZIYO LOKHUSELEKO";
notifyMessage.ERROR_GENEREL_SETTINGS = "Imposiso KWIISETHINGI NGOKUBANZI";
notifyMessage.WISHLIST_ACTION_FAILED =
  "ISENZO {{isenzo}} ULUHLU LOMNQWENZO LUSILELWE";
notifyMessage.SUR_CHARGE = "Sur Charge";
notifyMessage.SERVICE_TAX = "Irhafu Yenkonzo";
notifyMessage.TIPS_AMOUNT = "Iingcebiso Isixa";
notifyMessage.COUPON_SAVINGS = "Ugcino lweKhuphoni";
notifyMessage.TOLL_FEE = "Umrhumo weRhafu";
notifyMessage.DISCOUNT_APPLIED = "Isaphulelo sisetyenziswa";
notifyMessage.REFUND_TO_WALLET = "Buyiselwa kwiWalethi";
notifyMessage.TIPS_MINIMUM = "IINGCEBISO KUFUNEKA UBUNCINCI B";
notifyMessage.TIPS_MAXIMUM = "IINGCEBISO KUFANELE UKUBA UBUMKHULU";
notifyMessage.TIPS_ADDED = "IINGCEBISO EZONGEZWE NGEMPUMELELO";
notifyMessage.ERROR_PAYMENT_CARD =
  "Imposiso KWINTLAWULO YEKHADI, NGOBUBELE UZAMA EMVA KWEXESHA ELINYE";
notifyMessage.ERROR_EARNINGS = "Imposiso kwimivuzo";
notifyMessage.ERROR_DAY_EARNINGS = "Imposiso kwimivuzo yosuku";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "KUBONAKALA INKCAZELO I-OFILEN. NCEDA UCELE INGCALI UKUBA AYE KWI-INTANETHI PHAMBI KOKUBA UBHEKILE OLU HAMBO";
notifyMessage.INFO_PAYMENT_OPTIONS = "IINKETHO ZENTLAWULO AZIFUMANANI";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "INTO AYIHLANGANANGA KWI-GATEWAY IMICIMBI QAPHELA NGOKUQHELEKILEYO INYATHELO NGENYATHELO";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "AYIKUFUMANA UMSEBENZISI INTO ENGALUNGANGA KWIWALETHI IMALI YOKUHLAZIYA";
notifyMessage.INFO_CHECK_CREDENTIAL = "NCEDA UJONGE ISIQINISEKISO ESILANDELAYO";
notifyMessage.INFO_SEND_MONEY = "IMALI ITHUNYELWE NGEMPUMELELO";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Ihlawulelwe Umrhumo";
notifyMessage.INFO_TIPS = "Unike iingcebiso";
notifyMessage.INFO_JOINING_CHARGE = "Isixa sokuDityaniswa";
notifyMessage.INFO_REFERREL_CHARGE = "Isixa soBhekiselo";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Imali eseleyo yeNtlawulo iLayishiwe";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Imali eseleyo yeNtlawulo etsalwayo";
notifyMessage.WALLET_AMOUNT_LOADED =
  "IMALI YONGEZELELWE KWIWALETHI YAKHO NGEMPUMELELO";
notifyMessage.WALLET_NOTIFICATION =
  "INTO AYIHLANGANANGA KWIWALETHI UHLAZIYO KWIWALETHI RECHARE NO AUTH";
notifyMessage.WALLET_WITHDRAW = "ISIXA SEMALI SHISA KWIWALETHI";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "UKUKHUTSHWA KWEWALETHI NGEMPUMELELO";
notifyMessage.WALLET_DEBIT = "Isixa seWallet esitsalwayo";
notifyMessage.ERROR_WALLET_RECHARGE =
  "Impazamo yenzekile NGEXESHA IWALETHI IHLAJWA";
notifyMessage.ERROR_WALLET_WITHDRAW = "Imposiso KWI-WALETI UKUHOXA";
notifyMessage.ERROR_PAYMENT_OPTION = "Imposiso KULUHLU LOKUKHETHA UKUHLELA";
notifyMessage.ERROR_SEND_MONEY = "Imposiso EKUTHUMENI IMALI";
notifyMessage.BANK_CREDET_MONEY =
  "IMALI IYA KUBEKELWA KWI-AKHAWUNTI YEBHNKI ENTSHA NGEENTSUKU ZOSHISHINO NGE-5- 15";
notifyMessage.AMOUT_SEND_TO_FRIEND = "Idluliselwe kuMhlobo";
notifyMessage.BOOKING_CHARGE_FREEZED = "Isixa esiQinisekileyo sokuBhukisha";
notifyMessage.RIDE_CANCELLATION_CHARGE = "Khwela imali yokurhoxisa";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "Imbuyekezo yesaphulelo iFakelwe";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "INTENZISEKO AYIFUMANANI";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "IMPENDULO AYIFUMANANI SIQINGA ukuba UNGUMSEBENZISI Ongekho Mthethweni wale URL.";
notifyMessage.INFO_CARD_ADDED = "IKHADI LONGEZELELWE NGEMPUMELELO";
notifyMessage.INFO_CARD_VERIFIED = "IKHADI LIQINISEKISIWE NGEMPUMELELO";
notifyMessage.INFO_BANK_DETAILS = "IINKCUKACHA ZEBHANKI";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "IMPUMELELO YEENTENGISO";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "AYIFUMANANI IINKCUKACHA ZEBHANKI";
notifyMessage.INFO_CARD_DECLINED =
  "IMALI ETHATHWE KWIWALETHI. NGENXA YEKHADI LAHLUKILEYO";
notifyMessage.INFO_AMOUNT_PAID = "IMALI EHLAWULWE NGEMPUMELELO";
notifyMessage.ERROR_CARD_ADD = "AWUKWAZI UKWENZIWA IKHADI";
notifyMessage.ERROR_CARD_ADD_FAILED =
  "INTO AYIFANELEKILEYO EKUKONGEZENI IKHADI";
notifyMessage.ERROR_IN_TRANSACTIONS = "Imposiso KWINTENZELELO";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "INTLAWULO YEKHADI AYIPHUMELELE";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "UXOLO ASINIKEZI INKONZO APHA";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "Ifayile ilayishwe ngempumelelo";
notifyMessage.INFO_NOT_FOUND = "IINKCUKACHA AZIFUMANANI";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "IPOwunti YONGEZWA KWIWALETHI YAKHO NGEMPUMELELO";
notifyMessage.ERROR_WALLET_POINT_ADDED = "AYIKWAZI UKUBA INGXELO YEWALETHI";
notifyMessage.INFO_SUCCESS = "IMPUMELELO";
notifyMessage.INFO_FAILED = "Akuphumelelanga";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "Sebenzisa amanqaku oMvuzo";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Sebenzisa Amanqaku oMvuzo aphantsi";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Sebenzisa Amanqaku aphezulu oMvuzo";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "UMSEBENZISI WETHU UTSHINTILE IMEKO YOHAMBO LWAKHO UKUZE UFIKE";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "UMSEBENZISI WETHU UTSHINTILE IMEKO YOHAMBO LWAKHO UKUBA IQALE";
notifyMessage.INFO_RIDE_CANCELLED_BY = "UKUHAMBA KUrhoxisiwe";
notifyMessage.INFO_PROFESSIONAL = "INGCALI";
notifyMessage.INFO_USER = "USER";
notifyMessage.INFO_ADMIN = "ADMIN";
notifyMessage.INFO_INVALID_DETAILS = "Iinkcukacha ezingasebenziyo";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "Le Nombolo yeFowuni sele ibhalisiwe kwiGama leNkampani";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "IKHOWUDI YOKUBHEKISELA ENGASEBENZAYO, NCEDA UJONGE IKHOWUDI YAKHO UZE UZAME KWAKHONA";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "I-Id ye-imeyile sele ibhalisiwe";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "IFOMU YEDOBHI ENGALUNGELO MALUNGA YI-MM-DD-YYYY OKANYE MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE =
  "UBUDALA MABUBE {{minAgeKey}} KUNYE PHEZULU UKUQHUBEKA";
notifyMessage.INFO_CARD_ALREADY_EXIST = "IKHADI SELE LIKHONA";
notifyMessage.INFO_WALLET_RECHARGE = "Ukutshaja kwakhona kweWallet";
notifyMessage.INFO_INTERCITY_RIDE = "I-Intercity Ride";
notifyMessage.INFO_SITE_COMMISSION = "IKhomishini yeSiza";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Qhuba iThredithi yokuCima";
notifyMessage.INFO_ROUNDING = "Ukujikeleza";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "INKQUBO ESELE EQEDE";
notifyMessage.INFO_BASE_FARE = "Imali esisiseko";
notifyMessage.INFO_SERVICE_TAX = "Irhafu Yenkonzo";
notifyMessage.INFO_TRAVEL_CHARGE = "Intlawulo yokuhamba";
notifyMessage.INFO_MINIMUM_CHARGE = "Ubuncinci uMrhumo";
notifyMessage.INFO_SURCHARGE_FEE = "Intlawulo eyongezelelweyo";
notifyMessage.INFO_BOUNDARY_CHARGE = "Intlawulo yomda";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intlawulo yokulanda i-Intercity";
notifyMessage.INFO_OTP_MESSAGE =
  "Eyakho {{siteTitle}} ikhowudi yeOTP ithi {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Iakhawunti yakho itshixiwe ngenxa yokuba ufikelele kwelona nani liphezulu lemizamo yelogo engasebenziyo.";
notifyMessage.INFO_HUBS_EARNINGS = "Hubs Imivuzo";
notifyMessage.INFO_FELLOW_PASSENGER = "UMNQWEBO OMHLOBO";
notifyMessage.INFO_ALREADY_EXISTS = "SEKHONA";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Ikhowudi yeKhuphoni engasebenziyo, Nceda ujonge iKhowudi yakho.";
notifyMessage.WALLET_RECHARGE_FAILED = "UKUTSHJWA KWEWALETHI AYIHLULEKI";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "AMAXWEBHU ESITHUTHI SAKHO APHELELE. NCEDA UJONGE KWAYE UHLAZIYA";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "AMAPHEPHA OMXHOLO WAKHO APHELELE. NCEDA UJONGE KWAYE UHLAZIYA";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Amaxwebhu eSithuthi sakho aya Kuphelelwa nge- {{days}} Day(s). Nceda Jonga kwaye Uhlaziye";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Amaxwebhu eProfayile yakho ayakuphelelwa ngeentsuku ezi-{{days}}. Nceda Jonga kwaye Uhlaziye";
notifyMessage.INFO_INCENTIVE_CREDIT = "Inkuthazo Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Umrhumo woNcedo woNcedo";
notifyMessage.INFO_TIME_FARE = "Imali Yexesha";
notifyMessage.INFO_DISTANCE_FARE = "Intlawulo yomgama";
notifyMessage.INFO_PEAK_FARE = "Ixabiso eliphezulu";
notifyMessage.INFO_NIGHT_FARE = "Ukukhwela ebusuku";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Yhu! Uqhagamshelo lulahlekile. Awukho kwi-intanethi ngoku. Nceda uye kwi-intanethi ukuthatha izicelo zokukhwela.";
notifyMessage.AIRPORT_FEE = "Umrhumo wesikhululo seenqwelomoya";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your Rating is below the Range, Please improve, or your Account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Iakhawunti yakho ivuliwe. Nceda uye kwi-intanethi ukuthatha izicelo zokukhwela.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Imali edluliselwe kuMhlobo";
notifyMessage.WARNING_CARD_VALIDATION_FAILED =
  "Ukuqinisekiswa kwekhadi akuphumelelanga";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Umncedisi weBhanki uxakekile ngoku. Nceda uzame Kamva";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "I-CVV iqinisekiswe ngempumelelo";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "Ukuqinisekiswa kweCVV kusilele";
notifyMessage.ALERT_CHECK_CPF = "Nceda ujonge iNombolo yeCPF.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE =
  "Nceda ujonge ukusuka kuMhla / ukuya kumhla.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "ISixeko Sele Sikhona.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "Inombolo yeCPF esele ikhona.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Eyona Myinge yoMyinge wokutsalwa yi {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Ukutsalwa kwemali okuvunyelweyo Kanye ngosuku.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Igama Lomnini-Akhawunti";
notifyMessage.INFO_BANK_NAME = "Igama leBhanki";
notifyMessage.INFO_DOCUMENT_NUMBER = "Inombolo yoxwebhu";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Ukukhupha isipaji akuphumelelanga. Imali ifakwe kwi-wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "Ayikho Indawo Yesevisi Etholakalayo";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Umgibeli ohamba naye usefikile endaweni yokulayisha";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Umntu okhwele naye sele eqalisile ukukhwela.";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "Uhambo lufikelele esiphelweni kumntu okhwele naye.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "Le imeyile sele inxulunyaniswe nenye iakhawunti. Nceda Sebenzisa i-imeyile eyahlukileyo.";
notifyMessage.INFO_TRIP_NUMBER = "Inombolo yohambo";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Indawo yenkonzo sele ikhona.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS = "Ishishini Sele Likhona.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Indawo yeNkonzo sele ikhona.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "UQinisekiso loXwebhu lwakho phantsi koHlolo, Nceda ulinde de uQinisekiso lugqitywe.";
//--------- Newly added -------------
//-------------------------
module.exports = notifyMessage;
