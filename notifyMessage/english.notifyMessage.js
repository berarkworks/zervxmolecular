const notifyMessage = {};

notifyMessage.CREATED = "Created Successfully";
notifyMessage.UPDATED = "Updated Successfully";
notifyMessage.DELETED = "Deleted Successfully";
notifyMessage.INVALID_USER_DATA = "Invalid User Data";
notifyMessage.TRIP_END = "Trip Has Been Ended";
notifyMessage.WALLET_LOADED = "Wallet Amount Loaded";
notifyMessage.CASH = "Cash";
notifyMessage.CARD = "Card";
notifyMessage.WALLET = "Wallet";
notifyMessage.TRIP_FEE = "Trip Fee";
notifyMessage.TOTAL_BILL = "Total Bill";
notifyMessage.EARNINGS = "Your Earnings";
notifyMessage.WAITING_CHARGE = "Waiting Charge";
notifyMessage.PENDING_AMOUNT = "Pending Amount";
notifyMessage.CASH_IN_HAND = "Cash In Hand";
notifyMessage.BOOKING_ACCEPTED = "Your Booking Is Accepted";
notifyMessage.BOOKING_PAID = "Paid For Booking";

//PROFESSIONAL
notifyMessage.PROFESSIONAL_STARTED = "Professional Started To Your Trip";
notifyMessage.LOCATION_CHANGED_SUCCESSFULLY = "Location Changed Successfully";
notifyMessage.PROFESSIONAL_ARRIVED =
  "Professional Arrived To Your Pickup Location";
notifyMessage.PROFESSIONAL_COMMISSION = "Professional commission";
notifyMessage.PROFESSIONAL_EARNINGS_RECEIVED = "Booking earnings received";
notifyMessage.PROFESSIONAL_INFO =
  "Your wallet balance has gone below the minimum limit required to accept cash rides. Please recharge your wallet or you can only accept card and wallet rides until your balance becomes sufficient";

notifyMessage.PROFESSIONAL_NOT_FOUND = "Professional Not Found";
notifyMessage.PROFESSIONAL_NEW_RIDE = "You Got A New Ride";
notifyMessage.PROFESSIONAL_RIDE_CANCEL = "Ride Cancelled By Professional";
notifyMessage.PROFESSIONAL_ACCEPTED_BOOKING =
  "Your Booking Is Accepted By Our Professional";
notifyMessage.PROFESSIONAL_NOTIFICATION =
  "Notification Send To Nearby Professionals";
notifyMessage.PROFESSIONAL_RIDE_STOP_NOTIFICATION =
  "Professional Arrived To Stop Location";
notifyMessage.PROFESSIONAL_RIDE_START_NOTIFICATION =
  "Professional Start The Ride From Stop Location";
notifyMessage.PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION =
  "Professional Arrived To Your Booking Location";
notifyMessage.PROFESSIONAL_RIDE_START_INFO =
  "Professional Started From Booking Location";

notifyMessage.USER_NOT_FOUND = "User Not Found";
notifyMessage.USER_HAVE_RIDE = "User Already Have Ride";
notifyMessage.USER_RIDE_CANCEL_FAILD =
  "Something Went Wrong In User Cancel This Ride";
notifyMessage.USER_WALLET_PAYMENT_INFO =
  "Kindly Recharge Your Wallet Or Choose Any Other Payment Option";
notifyMessage.USER_PAYMENT_DECLINED =
  "This Payment Method Has Been Declined. Please Choose Another Payment Method";

notifyMessage.INVALID_PROFESSIONAL = "Invalid Professional Details";
notifyMessage.INVALID_BOOKING = "Invalid Booking Details";
notifyMessage.INVALID_ACCESS = "Invalid Access";
notifyMessage.VEHICLE_CATEGORY_NOT_FOUND = "Vehicle Category Not Found";
notifyMessage.VEHICLE_NOT_AVAILABLE = "No Vehicle Is Available";
notifyMessage.SERVICE_CATEGORY_NOT_FOUND = "Service Category Not Found";
notifyMessage.BOOKING_SCHEDULED_RIDE = "Booking Scheduled Successfully";
notifyMessage.BOOKING_CATEGORY_NOT_FOUND = "Booking Category Not Found";
notifyMessage.BOOKING_ALREADY = "Booking Is Already";
notifyMessage.BOOKING_STATUS_IS = "Booking Is";
notifyMessage.BOOKING_STATUS_INFO = "Booking Already In-Progress Or Ended";
notifyMessage.BOOKING_CANCELLED_BY_PROFESSIONAL =
  "Booking Cancelled By Driver Checking For Another Driver";

notifyMessage.BOOKING_CANCELLED_BY = "Booking Cancelled By";
notifyMessage.BOOKING_CANCELLED_BY_USER = "Booking Cancelled By User";
notifyMessage.BOOKING_CANCELLED = "Booking Is Cancelled Successfully";
notifyMessage.BOOKING_ASSIGNED_BY_ADMIN = "New Booking Is Assigned By Admin";
notifyMessage.BOOKING_REQUEST_ASSIGNED = "Request Assigned Successfully";
notifyMessage.BOOKING_ACCEPTED_BY_OTHERS =
  "Booking Is Already Accepted By Another Driver";

notifyMessage.BOOKING_UPDATE_FAILED = "Update Booking Details Failed";
notifyMessage.BOOKING_UPDATE_STATUS_FAILED = "Booking Status Update Failed";
notifyMessage.CANCELLATION_REASON_NOT_FOUND = "Cancellation Reason Not Found";
notifyMessage.SOMETHING_WENT_WRONG = "Something Went Wrong In Details";
notifyMessage.RIDE_CANCELLED = "Ride Cancelled Successfully";
notifyMessage.RIDE_IS = "This Ride Is";
notifyMessage.RIDE_STATUS_INFO = "Ride Already Ended Or Not Started Yet";
notifyMessage.RIDE_LOCATION_UPDATE_FAILED = "Changing Ride Location is Failed";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_USER = "Ride Address Has Changed By User";
notifyMessage.RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL =
  "Ride Address Has Changed By Professional";
notifyMessage.OTP_INCORRECT = "OTP Is Incorrect, Kindly Enter Correct OTP";
notifyMessage.RATING_SUBMITED = "Rating Submited Successfully";
notifyMessage.RATING_UPDATE_FAILED = "Error In Rating Update";
notifyMessage.LOCATION_ACCESS_FAILED = "Something went wrong in your location";
notifyMessage.TRIP_NOT_AVAILABLE = "Tips Is Not Available";
notifyMessage.TRIP_SHARED = "Trip Shared Successfully";
notifyMessage.TRIP_END_PROFESSIONAL = "Your Ride Has Ended By Professional";
notifyMessage.OPERATOR_STATUS_END =
  "Our Operator Changed Your Trip Status To End";

notifyMessage.ERROR_SECURITY_UPDATE = "Error In Security update";
notifyMessage.ERROR_GENEREL_SETTINGS = "Error In General Settings";
notifyMessage.WISHLIST_ACTION_FAILED = "Action {{action}} Wishlist Is Failed";
notifyMessage.SUR_CHARGE = "Sur Charge";
notifyMessage.SERVICE_TAX = "Service Tax";
notifyMessage.TIPS_AMOUNT = "Tips Amount";
notifyMessage.COUPON_SAVINGS = "Coupon Savings";
notifyMessage.TOLL_FEE = "Toll Fee";
notifyMessage.DISCOUNT_APPLIED = "Discount Applied";
notifyMessage.REFUND_TO_WALLET = "Refund to Wallet";
notifyMessage.TIPS_MINIMUM = "Tips Should Be Minimum Of";
notifyMessage.TIPS_MAXIMUM = "Tips Should Be Maximum Of";
notifyMessage.TIPS_ADDED = "Tips Added Successfully";
notifyMessage.ERROR_PAYMENT_CARD =
  "Error In Card Payment, Kindly Try After Sometime";

notifyMessage.ERROR_EARNINGS = "Error In Earnings";
notifyMessage.ERROR_DAY_EARNINGS = "Error In Day Earnings";
notifyMessage.PROFESSIONAL_ONLINE_NOTIFICATION =
  "It Seems The Professional Is Offline. Please Ask The Professional To Go Online Before You Book This Trip";

notifyMessage.INFO_PAYMENT_OPTIONS = "Payment Options Not Found";
notifyMessage.INFO_CARD = "Can't Find the Card Details";
notifyMessage.INFO_PAYMENT_GATEWAY_EVENT =
  "Something Went Wrong In Gateway Events Check Carefully Step By Step";
notifyMessage.INFO_USER_WALLET_NOT_FOUND =
  "Can't Find User Details In Wallet Amount Recharge";

notifyMessage.INFO_CHECK_CREDENTIAL = "Please Check The Next Credential";
notifyMessage.INFO_SEND_MONEY = "Money Send Successfully";
notifyMessage.INFO_SUBSCRIPTIONCHARGE = "Paid For Subscription";
notifyMessage.INFO_TIPS = "You gave Tips";
notifyMessage.INFO_JOINING_CHARGE = "Joining Amount";
notifyMessage.INFO_REFERREL_CHARGE = "Referrel Amount";
notifyMessage.INFO_CARD_AMOUNT_REFUND = "Remaining Payment Amount Loaded";
notifyMessage.INFO_EXTRA_AMOUNT_DEBIT = "Remaining Payment Amount Debited";
notifyMessage.WALLET_AMOUNT_LOADED =
  "Amount Is Added To Your Wallet Successfully";
notifyMessage.WALLET_NOTIFICATION =
  "Something Went Wrong IN Wallet Update In Wallet Recharge No Auth";

notifyMessage.WALLET_WITHDRAW = "Amount Withdraw From Wallet";
notifyMessage.WALLET_WITHDRAW_SUCCESS = "Wallet Withdrawal Successfully";
notifyMessage.WALLET_DEBIT = "Wallet Amount Debited";
notifyMessage.ERROR_WALLET_RECHARGE = "Error Occured While Wallet Recharge";
notifyMessage.ERROR_WALLET_WITHDRAW = "Error In Wallet Withdrawal";
notifyMessage.ERROR_PAYMENT_OPTION = "Error In Payment Option List";
notifyMessage.ERROR_SEND_MONEY = "Error In Send Money";
notifyMessage.BANK_CREDET_MONEY =
  "Money Will Be Credit To Your Bank Account Within 7 Business Days";

notifyMessage.AMOUT_SEND_TO_FRIEND = "Transfered To Friend";
notifyMessage.BOOKING_CHARGE_FREEZED = "Freezed Amount For Booking";
notifyMessage.RIDE_CANCELLATION_CHARGE = "Ride cancel amount";
notifyMessage.PROFESSIONAL_TOLERENCE_AMOUNT = "Refund For Discount Applied";
notifyMessage.INFO_TRANSACTION_NOT_FOUND = "Transaction Is Not Found";
notifyMessage.INFO_RESPONSE_NOT_FOUND =
  "Response Not Found We've Assume You’re a Illegal User Of This URL";

notifyMessage.INFO_CARD_ADDED = "Card Is Added Successfully";
notifyMessage.INFO_CARD_VERIFIED = "Card Verified Successfully";
notifyMessage.INFO_BANK_DETAILS = "Bank Details";
notifyMessage.INFO_TRANSACTIONS_SUCCESS = "Transactions Success";
notifyMessage.INFO_BANK_DETAILS_NOT_FOUND = "Can't Found The Bank Details";
notifyMessage.INFO_CARD_DECLINED =
  "Amount Deducted From Wallet. Due to Card Declined";

notifyMessage.INFO_AMOUNT_PAID = "Amount Paid Successfully";
notifyMessage.ERROR_CARD_ADD = "Cannot Add a Card";
notifyMessage.ERROR_CARD_ADD_FAILED = "Something Went Wrong In Card Adding";
notifyMessage.ERROR_IN_TRANSACTIONS = "Error In Transactions";
notifyMessage.ERROR_CARD_PAYMENT_FAILED = "Card Payment Failed";
notifyMessage.INFO_SERVICE_NOT_PROVIDE = "Sorry We Don't Provide Service Here";
notifyMessage.INFO_FILE_UPLOAD_SUCCESS = "File Uploaded Successfully";
notifyMessage.INFO_NOT_FOUND = "Details Not Found";
notifyMessage.INFO_WALLET_POINT_LOADED =
  "Point Is Added To Your Wallet Successfully";

notifyMessage.ERROR_WALLET_POINT_ADDED = "Not Able to Add Wallet Point";
notifyMessage.INFO_SUCCESS = "Success";
notifyMessage.INFO_FAILED = "Failed";
notifyMessage.TRAN_REDEEM_REWARD_POINT = "Redeem Reward Points";
notifyMessage.TRAN_REDEEM_REWARD_MIN = "Redeem Minimum Reward Points";
notifyMessage.TRAN_REDEEM_REWARD_MAX = "Redeem Maximum Reward Points";
notifyMessage.INFO_OPERATOR_STATUS_ARRIVED =
  "Our Operator Changed Your Trip Status To Arrived";
notifyMessage.INFO_OPERATOR_STATUS_START =
  "Our Operator Changed Your Trip Status To Start";

notifyMessage.INFO_RIDE_CANCELLED_BY = "Ride Has Cancelled By";
notifyMessage.INFO_PROFESSIONAL = "Professional";
notifyMessage.INFO_USER = "User";
notifyMessage.INFO_ADMIN = "Admin";
notifyMessage.INFO_INVALID_DETAILS = "Invalid Details";
notifyMessage.INFO_OFFICER_ALREADY_EXISTS =
  "This Phone Number is Already Registered in Corporate Name";
notifyMessage.INFO_INVALID_REFERRAL_CODE =
  "Invalid Referral Code, Please Check Your Code And Try Again";
notifyMessage.INFO_EMAIL_ALREADY_EXISTS = "Email Id Already Registered";
notifyMessage.INFO_INVALID_DATE_FORMAT =
  "Invalid DOB Format Must Be MM-DD-YYYY or MM/DD/YYYY";
notifyMessage.INFO_INVALID_AGE =
  "Age Must Be {{minAgeKey}} And Above To Proceed";

notifyMessage.INFO_CARD_ALREADY_EXIST = "Card Is Already Exist";
notifyMessage.INFO_WALLET_RECHARGE = "Wallet Recharge";
notifyMessage.INFO_INTERCITY_RIDE = "Intercity Ride";
notifyMessage.INFO_SITE_COMMISSION = "Site Commission";
notifyMessage.INFO_RIDE_CANCEL_CREDIT = "Ride Cancellation Credit";
//--------- Newly added -------------
notifyMessage.INFO_ROUNDING = "Rounding";
notifyMessage.INFO_PROFESSIONAL_HAVE_RIDE = "Professional Already Have Ride";
notifyMessage.INFO_BASE_FARE = "Base Fare";
notifyMessage.INFO_SERVICE_TAX = "Service Tax";
notifyMessage.INFO_TRAVEL_CHARGE = "Travel Charge";
notifyMessage.INFO_MINIMUM_CHARGE = "Minimum Charge";
notifyMessage.INFO_SURCHARGE_FEE = "SurCharge Fee";
notifyMessage.INFO_BOUNDARY_CHARGE = "boundary Charge";
notifyMessage.INFO_INTERCITY_PICKUP_CHARGE = "Intercity Pickup Charge";
notifyMessage.INFO_OTP_MESSAGE = "Your {{siteTitle}} OTP code is {{OTP}}";
notifyMessage.INFO_IP_BLOCK =
  "Your account has been locked because you have reached the maximum number of invalid logon attempts.";

notifyMessage.INFO_HUBS_EARNINGS = "Hubs Earnings";
notifyMessage.INFO_FELLOW_PASSENGER = "Fellow Passenger";
notifyMessage.INFO_ALREADY_EXISTS = "Already Exist";
notifyMessage.INFO_INVALID_COUPON_CODE =
  "Invalid Coupon Code, Please Check Your Code.";
notifyMessage.WALLET_RECHARGE_FAILED = "Wallet Recharge Failed";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRED =
  "Your Vehicle Documents Is Expired. Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRED =
  "Your Profile Documents Is Expired. Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_VEHICLE_DOCUMENTS_EXPIRE_DAYS =
  "Your Vehicle Documents will Expire in {{days}} Day(s). Please Check And Update";
notifyMessage.INFO_PROFESSIONAL_PROFILE_DOCUMENTS_EXPIRE_DAYS =
  "Your Profile Documents will Expire in {{days}} Day(s). Please Check And Update";

notifyMessage.INFO_INCENTIVE_CREDIT = "Incentive Credited";
notifyMessage.INFO_ASSISTANCE_CARE_FEE = "Assistance Care Fee";
notifyMessage.INFO_TIME_FARE = "Time Fare";
notifyMessage.INFO_DISTANCE_FARE = "Distance Fare";
notifyMessage.INFO_PEAK_FARE = "Peak Fare";
notifyMessage.INFO_NIGHT_FARE = "Night Fare";
notifyMessage.WARNING_CRON_OFFLINE_ALERT =
  "Oops! Connection is lost. You’re offline now. Please go online to take ride requests.";

notifyMessage.AIRPORT_FEE = "Airport Fee";
notifyMessage.INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT =
  "This Professional Not Accept the Selected Payment Option. Please Change the Payment Option";
notifyMessage.ALERT_PROFESSIONAL_LOW_PERFORMANCE =
  "Your rating is below the range, Please improve, or your account will be blocked in few Days.";
notifyMessage.ALERT_PROFESSIONAL_BLOCK =
  "Your account has been blocked because your rating is below the range. The account will be reactivated on {{date}} midnight";
notifyMessage.INFO_PROFESSIONAL_ACTIVATED =
  "Your account is activated. Please go online to take ride requests.";
notifyMessage.AMOUT_RECEIVED_FROM_FRIEND = "Amount Transferred From Friend";
notifyMessage.INFO_AIRPORT_EXISTS = "Airport Already Exists";
notifyMessage.INFO_TOLL_EXISTS = "Toll Already Exists";
notifyMessage.INFO_COUPON_CODE_EXISTS = "Coupon Code Already Exists";
notifyMessage.INFO_POPULAR_DESTINATION_EXISTS =
  "Popular Destination Already Exists";
notifyMessage.INFO_VEHICLE_CATEGORY_EXISTS = "Vehicle Category Already Exists";

notifyMessage.INFO_OPERATORS_ROLE_EXISTS = "Operators Role Already Exists";
notifyMessage.INFO_PHONE_NUMBER_EXISTS = "Phone Number Already Exists";
notifyMessage.INFO_DOCUMENTS_EXISTS = "Documents Already Exists";
notifyMessage.INFO_CANCELLATION_REASON_EXISTS =
  "Cancellation Reason Already exists";
notifyMessage.INFO_LANGUAGE_EXISTS = "Language Already Exists";
notifyMessage.INFO_POPULAR_PLACE_EXISTS = "Popular Place Already Exists";
notifyMessage.WARNING_CARD_VALIDATION_FAILED = "Card validation failed";
notifyMessage.WARNING_BANK_SERVER_BUSY =
  "Bank server Busy Now. Please try Later";
notifyMessage.INFO_CVV_VERIFY_SUCCESS = "CVV Verified Successfully";
notifyMessage.WARNING_CVV_VEEIFY_FAILED = "CVV Verification Failed";
notifyMessage.ALERT_CHECK_CPF = "Please Check CPF Number.";
notifyMessage.ALERT_CHECK_FROM_OR_TO_DATE = "Please Check From Date / To Date.";
notifyMessage.ALERT_CITY_ALREADY_EXISTS = "City Already Exist.";
notifyMessage.ALERT_CPF_NO_ALREADY_EXISTS = "CPF Number Already Exist.";
notifyMessage.ALERT_MAX_WALLET_WITHDRAW_LIMIT =
  "Maximum Withdraw Amount Limit is {{amount}}";
notifyMessage.ALERT_WALLET_WITHDRAW_LIMIT =
  "Amount Withdrawal Allowed Once Per Day.";
notifyMessage.INFO_ACCOUNT_HOLDER_NAME = "Account Holder Name";
notifyMessage.INFO_BANK_NAME = "Bank Name";
notifyMessage.INFO_DOCUMENT_NUMBER = "Document Number";
notifyMessage.ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET =
  "Wallet withdrawal failed. Amount credited to the wallet.";
notifyMessage.INFO_NO_SERVICE_LOCATION_AVAILABLE =
  "No Service Location Is Available";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ARRIVED =
  "Your fellow passenger has arrived at the pickup location";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_STARTED =
  "Your fellow passenger has started the ride";
notifyMessage.ALERT_FELLOW_PASSENGER_RIDE_ENDED =
  "The ride has come to an end for your fellow passenger.";
notifyMessage.ALERT_DUPLICATE_EMAIL =
  "This Email Is Already Associated With Another Account. Please Use A Different Email.";
notifyMessage.INFO_TRIP_NUMBER = "Trip Number";
notifyMessage.ALERT_SERVICE_LOCATION_IS_ALREADY_EXISTS =
  "Service Location Is Already Exists.";
notifyMessage.ALERT_CORPORATE_IS_ALREADY_EXISTS =
  "Corporate Is Already Exists.";
notifyMessage.ALERT_SERVICE_AREA_IS_ALREADY_EXISTS =
  "Service Area Is Already Exists.";
notifyMessage.ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING =
  "Your Document Verification under Review, Please wait until the Verification is Completed.";
//-------------------------

module.exports = notifyMessage;
