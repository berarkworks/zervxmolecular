const { MoleculerError, ValidationError, MoleculerClientError } =
  require("moleculer").Errors;
const js2xmlparser = require("js2xmlparser");
const xml2js = require("xml2js");
const { customAlphabet } = require("nanoid");
const Mongoose = require("mongoose");
const axios = require("axios");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const cryptoUtils = require("../utils/crypto.util");
const Flutterwave = require("flutterwave-node-v3");

const peachPaymentUtils = require("../utils/peachpayment.util");
const stripePaymentUtils = require("../utils/stripePayment.util");

// payment
const flutterwave = require("../paymentGateways/fultterwave.payment");

// utils
const mappingUtils = require("../utils/mapping.util");
//
const notifyMessage = require("../mixins/notifyMessage.mixin");
//constants
const {
  CONST_PIX,
  CONST_TIGO,
  CONST_CYBERSOURCE,
  RAZORPAY,
  CONST_ZCREDITAPIARY,
} = require("../utils/constant.util");
// Encode & Decode
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const {
  SOMETHING_WENT_WRONG,
} = require("../notifyMessage/english.notifyMessage");

const transactionAction = {};

transactionAction.newRechargeWalletTransaction = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    errorMessage = "",
    responseData = {};
  // need generalSettings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = (context.params.userType || "").toUpperCase();
  const cardId = context.params.cardId;
  //  for params use
  let userId, professionalId;
  // let userType; //this safety sometype postman hitting confused
  // if (context.meta.professionalId) {
  //   userType = constantUtil.PROFESSIONAL;
  //   professionalId = context.meta.professionalId;
  // }
  // if (context.meta.userId) {
  //   userType = constantUtil.USER;
  //   userId = context.meta.userId;
  // }
  let languageCode;
  if (userType === constantUtil.USER) {
    userId = context.meta.userId;
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    professionalId = context.meta.professionalId;
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    INFO_PAYMENT_OPTIONS,
    INFO_CARD,
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    INFO_PAYMENT_GATEWAY_EVENT,
    WALLET_AMOUNT_LOADED,
    WALLET_NOTIFICATION,
    SOMETHING_WENT_WRONG,
    ERROR_WALLET_RECHARGE,
    INFO_CHECK_CREDENTIAL,
    INFO_WALLET_RECHARGE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);

  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    let paymentData = {};
    Object.values(paymentOptionList)
      .filter((option) => option.status === constantUtil.ACTIVE)
      .forEach((option) => {
        if (option.paymentType === constantUtil.PAYMENTCARD) {
          paymentData = option;
        }
      });

    // GET USERDATA
    let userData = {},
      cardData = {},
      finalResponse = {};
    //Get User Details
    if (userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": professionalId,
      });
    } else if (userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": userId,
      });
    }
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(
        userType === constantUtil.USER
          ? USER_NOT_FOUND
          : PROFESSIONAL_NOT_FOUND,
        500
      );
    }
    //Get Card Details
    if (
      (paymentData.gateWay === constantUtil.STRIPE && cardId === CONST_PIX) ||
      (paymentData.gateWay === constantUtil.CONST_MERCADOPAGO &&
        cardId === CONST_PIX)
    ) {
      cardData = {};
    } else {
      if (userType === constantUtil.PROFESSIONAL) {
        cardData = await this.broker.emit("professional.getCardDetailsById", {
          "professionalId": professionalId,
          "cardId": cardId,
        });
      } else if (userType === constantUtil.USER) {
        cardData = await this.broker.emit("user.getCardDetailsById", {
          "userId": userId,
          "cardId": cardId,
        });
      }
      cardData = cardData && cardData[0];
      if (!cardData) {
        throw new MoleculerError(INFO_CARD, 500);
      }
      cardData["cvv"] = context.params.cvv;
    }
    // it means here not work excuted all error management is gateway eventsI

    // const stringTransactionReason = `${generalSettings.data.siteTitle}/${
    //   constantUtil.WALLETRECHARGE
    // }/${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}/`;

    const stringTransactionReason = `${
      generalSettings.data.siteTitle
    }/${INFO_WALLET_RECHARGE}/${Date.now()}/${userType}/${userData?._id?.toString()}`;

    if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
      let response;
      if (paymentData.version === constantUtil.CONST_V3) {
        response = await this.broker.emit(
          "transaction.flutterwave.walletRechargeV3",
          {
            ...context.params,
            "userType": userType,
            "userId":
              userType === constantUtil.PROFESSIONAL ? professionalId : userId,
          }
        );
      } else {
        response = await this.broker.emit(
          "transaction.flutterwave.walletRecharge",
          {
            ...context.params,
            "userType": userType,
            "userId":
              userType === constantUtil.PROFESSIONAL ? professionalId : userId,
          }
        );
      }
      response = response && response[0];
      return response;
    }
    switch (paymentData.gateWay) {
      case constantUtil.STRIPE:
      case constantUtil.RAZORPAY:
      case constantUtil.PEACH:
      case constantUtil.CONST_ZCREDITAPIARY:
      case constantUtil.CONST_MERCADOPAGO:
      case constantUtil.CONST_TEYA:
        // finalResponse = await this.broker.emit(
        //   "transaction.stripe.newRechargeWalletTransaction",
        //   {
        //     ...context.params,
        //     "paymentData": paymentData,
        //     "userData": userData,
        //     "cardData": cardData,
        //     "generalSettings": generalSettings,
        //     //'transactionId': transaction._id.toString(),
        //     "transactionReason": stringTransactionReason,
        //   }
        // );
        finalResponse = [
          {
            "code": 200,
            "data": {
              "authType": "NOAUTH",
              "transactionId": context.params.transactionId,
              "transactionStatus": constantUtil.SUCCESS,
            },
            "message": "",
            ...context.params.gatewayResponse,
          },
        ];
        break;

      // case constantUtil.RAZORPAY:
      //   finalResponse = [
      //     {
      //       "code": 200,
      //       "data": {
      //         "authType": "NOAUTH",
      //         "transactionId": context.params.transactionId,
      //         "transactionStatus": constantUtil.SUCCESS,
      //         ...context.params.gatewayResponse.nameValuePairs,
      //       },
      //       "message": "",
      //     },
      //   ];
      //   break;

      // case constantUtil.CONST_ZCREDITAPIARY:
      //   finalResponse = [
      //     {
      //       "code": 200,
      //       "data": {
      //         "authType": "NOAUTH",
      //         "transactionId": context.params.transactionId,
      //         "transactionStatus": constantUtil.SUCCESS,
      //       },
      //       "message": "",
      //       ...context.params.gatewayResponse,
      //     },
      //   ];
      //   break;
    }
    finalResponse = finalResponse && finalResponse[0];
    if (!finalResponse) {
      throw new MoleculerError(INFO_PAYMENT_GATEWAY_EVENT, 500);
    }
    //--------------- Common  Start -----------------------------
    if (finalResponse.code === 200) {
      if (finalResponse.data.authType.toUpperCase() === "NOAUTH") {
        const currentBalance =
          parseFloat(userData.wallet.availableAmount || 0) +
          parseFloat(context.params.amount || 0);
        const transaction = await this.adapter.model.create({
          "clientId": clientId,
          "transactionAmount": context.params.amount,
          "previousBalance": userData.wallet.availableAmount || 0,
          "currentBalance": currentBalance,
          "transactionReason": stringTransactionReason,
          "from": {
            "userType": userType,
            "userId": userData._id.toString(),
            "name": userData.firstName + userData.lastName,
            "phoneNumber": userData.phoneNumber,
          },
          "to": {
            "userType": constantUtil.ADMIN,
            "userId": null,
          },
          "paymentType": constantUtil.CREDIT, // TO SHOW TO USER WALLET TRANSACTION PAGE
          "gatewayName": paymentData.gateWay,
          "paymentGateWay": paymentData.gateWay,
          "transactionType": constantUtil.WALLETRECHARGE,
          "transactionId": finalResponse.data.transactionId,
          "transactionDate": Date.now(),
          "gatewayDocId": paymentData?._id,
          "transactionStatus": finalResponse.data.transactionStatus,
          // 'cardDetailes': context.params.cardDetailes,
          "gatewayResponse": finalResponse,
        });
        if (!transaction) {
          throw new MoleculerError(SOMETHING_WENT_WRONG + "/DB UPDATE", 500);
        }
        // add amount to userData
        let amountUpdateInUser;
        if (userType === constantUtil.PROFESSIONAL) {
          // if (cardId !== CONST_PIX) {
          //   const updateDefaultCardStatus = await this.broker.emit(
          //     "professional.updateDefaultCardStatusInActiveActive",
          //     {
          //       "professionalId": professionalId.toString(),
          //       "cardId": cardId.toString(),
          //     }
          //   );
          // }
          amountUpdateInUser = await this.broker.emit(
            "professional.updateWalletAmount",
            {
              "professionalId": professionalId.toString(),
              "availableAmount":
                userData.wallet.availableAmount + context.params.amount,
              "freezedAmount": userData.wallet.freezedAmount,
            }
          );
        } else if (userType === constantUtil.USER) {
          // if (cardId !== CONST_PIX) {
          //   const updateDefaultCardStatus = await this.broker.emit(
          //     "user.updateDefaultCardStatusInActiveActive",
          //     {
          //       "userId": userId.toString(),
          //       "cardId": cardId.toString(),
          //     }
          //   );
          // }
          amountUpdateInUser = await this.broker.emit(
            "user.updateWalletAmount",
            {
              "userId": userData._id.toString(),
              "availableAmount":
                userData.wallet.availableAmount + context.params.amount,
              "freezedAmount": userData.wallet.freezedAmount,
              "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
            }
          );
        }
        amountUpdateInUser = amountUpdateInUser && amountUpdateInUser[0];
        if (!amountUpdateInUser) {
          throw new MoleculerError(WALLET_NOTIFICATION, 500);
        } else {
          // return {
          //   "code": 200,
          //   "message": WALLET_AMOUNT_LOADED,
          //   "data": {
          //     "availableAmount":
          //       userData.wallet.availableAmount + context.params.amount, // Need to Remove After Sep 2024
          //     "wallet": {
          //       ...userData.wallet,
          //       "availableAmount":
          //         userData.wallet.availableAmount + context.params.amount,
          //     },
          //   },
          // };
          errorMessage = WALLET_AMOUNT_LOADED;
          responseData = {
            "availableAmount":
              userData.wallet.availableAmount + context.params.amount, // Need to Remove After Sep 2024
            "wallet": {
              ...userData.wallet,
              "availableAmount":
                userData.wallet.availableAmount + context.params.amount,
            },
          };
        }
      } else {
        // return {
        //   "code": 200,
        //   "message": INFO_CHECK_CREDENTIAL,
        //   "data": finalResponse.data,
        // };
        errorMessage = INFO_CHECK_CREDENTIAL;
        responseData = finalResponse.data;
      }
    } else {
      // return {
      //   "code": 400,
      //   "message": ERROR_WALLET_RECHARGE,
      //   "data": {},
      // };
      throw new MoleculerError(ERROR_WALLET_RECHARGE, 400);
    }
    //--------------- Common End -----------------------------
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": errorMessage,
    "data": responseData,
  };
};
transactionAction.newWithdrawalWalletTransaction = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let userData = {};
  let userType = {};
  let bankDetails = {};
  let transactionId = {};
  let transactionStatus = {};
  let finalPaymentData = {};
  let paymentGatwayResponse = {};
  // GET REQUESTER DATA
  if (context.meta.userId) {
    userType = constantUtil.USER;
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
  }
  if (context.meta.professionalId) {
    userType = constantUtil.PROFESSIONAL;
    userData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(),
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(`No ${context.params.userType} FOUND`, 500);
  }
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    ERROR_PAYMENT_OPTION,
    WALLET_WITHDRAW,
    WALLET_WITHDRAW_SUCCESS,
    ERROR_WALLET_WITHDRAW,
    BANK_CREDET_MONEY,
    ALERT_WALLET_WITHDRAW_LIMIT,
    ALERT_MAX_WALLET_WITHDRAW_LIMIT,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  //#region Check Withdraw Eligibility
  if (
    userData?.lastWithdrawDate?.setHours(0, 0, 0, 0) ===
    new Date().setHours(0, 0, 0, 0)
  ) {
    throw new MoleculerError(ALERT_WALLET_WITHDRAW_LIMIT, 500);
  }
  // else if (
  //   parseFloat(context.params.amount) > generalSettings.data.withDrawMaxAmount
  // ) {
  //   throw new MoleculerError(
  //     ALERT_MAX_WALLET_WITHDRAW_LIMIT.replace(
  //       /{{amount}}/g,
  //       generalSettings.data.withDrawMaxAmount
  //     ),
  //     500
  //   );
  // }
  //#endregion Check Withdraw Eligibility
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) {
    return ERROR_PAYMENT_OPTION;
  }
  let paymentData = {};
  const transId = `${helperUtil.randomString(8, "a")}`;
  Object.values(paymentOptionList)
    .filter((option) => option.status === constantUtil.ACTIVE)
    .forEach((option) => {
      if (option.paymentType === constantUtil.PAYMENTCARD) {
        paymentData = option;
      }
    });
  if (userData.wallet.availableAmount <= parseFloat(context.params.amount)) {
    throw new MoleculerError("REQUESTED AMOUNT NOT FOUND", 500);
  }
  const stringTransactionReason =
    WALLET_WITHDRAW + "/" + userData?._id?.toString() + "/" + userType;

  switch (generalSettings.data.payoutOptions) {
    case constantUtil.MANUAL:
    case constantUtil.DISPLAYINFO:
      paymentGatwayResponse["data"] = {};
      paymentGatwayResponse["data"]["status"] = constantUtil.SUCCESS;
      transactionStatus = constantUtil.PENDING;
      transactionId = `${generalSettings.data.siteTitle.toLowerCase()}-${customAlphabet(
        "1234567890",
        9
      )()}${context.params?.reason}`;
      break;
    case constantUtil.AUTOMATED:
      transactionStatus = constantUtil.PENDING;
      // --------- Bank Details Start ----------
      if (context.params.userType === constantUtil.PROFESSIONAL) {
        bankDetails = userData.bankDetails;
      } else {
        bankDetails = userData.bankDetails[0];
        // bankDetails = userData.bankDetails.filter(
        //   (each) => each.accountNumber === context.params.account.accountNumber
        // )
        // bankDetails = bankDetails[0]
      }
      if (!bankDetails) {
        throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
      }
      // --------- Bank Details End ----------
      // if (paymentData.gateWay === "PEACH") {
      //   const paymentJson = {
      //     "data": {},
      //   };
      //   if (paymentData.mode === constantUtil.SANDBOX) {
      //     paymentJson["baseUrl"] = paymentData.testUrl;
      //     paymentJson["token"] = paymentData.testAuthToken;
      //     paymentJson["data"]["entityId"] = paymentData.testEntityId;
      //   } else {
      //     paymentJson["baseUrl"] = paymentData.liveUrl;
      //     paymentJson["token"] = paymentData.authToken;
      //     paymentJson["data"]["entityId"] = paymentData.entityId;
      //   }
      //   const dd = new Date().getDate();
      //   const mm = new Date().getMonth() + 1;
      //   const yyyy = new Date().getFullYear();
      //   const dataSet = {};
      //   dataSet["Header"] = {};
      //   dataSet["Header"]["PsVer"] = "2.0.1";
      //   dataSet["Header"]["Client"] = transId.toUpperCase();
      //   dataSet["Header"]["Service"] = "Creditors";
      //   dataSet["Header"]["ServiceType"] = "SDV";
      //   dataSet["Header"]["DueDate"] =
      //     dd.toString() + mm.toString() + yyyy.toString();
      //   dataSet["Header"]["CallBackUrl"] = "https://www.example.com/";
      //   dataSet["Header"]["Reference"] =
      //     dd.toString() +
      //     mm.toString() +
      //     yyyy.toString() +
      //     customAlphabet("1234567890", 4)().toString();
      //   dataSet["Payments"] = {};
      //   dataSet["Payments"]["FileContents"] = {};
      //   dataSet["Payments"]["FileContents"]["Surname"] =
      //     context.params.account.accountName.toString();
      //   dataSet["Payments"]["FileContents"]["BranchCode"] =
      //     context.params.account.branchCode.toString();
      //   dataSet["Payments"]["FileContents"]["AccountNumber"] =
      //     context.params.account.accountNumber.toString();
      //   dataSet["Payments"]["FileContents"]["FileAmount"] =
      //     context.params.amount;
      //   dataSet["Payments"]["FileContents"]["AmountMultiplier"] = "1";
      //   dataSet["Payments"]["FileContents"]["AccountType"] = "0";
      //   dataSet["Payments"]["FileContents"]["Reference"] = "PeachPayments";
      //   dataSet["Totals"] = {};
      //   dataSet["Totals"]["Records"] = "1";
      //   dataSet["Totals"]["Amount"] = context.params.amount;
      //   dataSet["Totals"]["BranchHash"] =
      //     context.params.account.branchCode.toString();
      //   dataSet["Totals"]["AccountHash"] =
      //     context.params.account.accountNumber.toString();
      //   paymentJson["data"] = js2xmlparser.parse("APIPaymentsRequest", dataSet);
      //   finalPaymentData = await peachPaymentUtils.debitWalletTransactions(
      //     paymentJson
      //   );
      //   const parser = new xml2js.Parser();
      //   parser.parseString(finalPaymentData, function (err, result) {
      //     finalPaymentData = result;
      //   });
      //   if (finalPaymentData.Response.Result[0] === "OK") {
      //     if (context.params.userType === constantUtil.USER) {
      //       this.broker.emit("user.updateWalletRechargeOrWithdraw", {
      //         "userId": userData._id.toString(),
      //         "amount": parseFloat(context.params.amount),
      //         "transStatus": constantUtil.DEBIT,
      //       });
      //     } else {
      //       this.broker.emit("professional.updateWalletRechargeOrWithdraw", {
      //         "professionalId": userData._id.toString(),
      //         "amount": parseFloat(context.params.amount),
      //         "transStatus": constantUtil.DEBIT,
      //       });
      //     }

      //     const updateTrasactionData = await this.adapter.model.create({
      //       "transactionType": constantUtil.WALLETWITHDRAWAL,
      //       "transactionAmount": context.params.amount,
      //       "previousBalance": null,
      //       "currentBalance": null,
      //       "transactionReason": context.params.reason || "",
      //       "paidTransactionId": finalPaymentData.id,
      //       "transactionId": transId,
      //       "transactionDate": new Date(),
      //       "paymentType": constantUtil.DEBIT,
      //       "accountsDetail": context.params.account,
      //       "from": {
      //         "userType": constantUtil.ADMIN,
      //         "userId": null,
      //         "name": null,
      //         "avatar": null,
      //         "phoneNumber": null,
      //       },
      //       "to": {
      //         "userType": context.params.userType,
      //         "userId": userData._id.toString(),
      //         "name": userData.firstName + " " + userData.lastName || null,
      //         "avatar": userData.avatar || null,
      //         "phoneNumber": userData.phone.number || null,
      //       },
      //       // 'isActive': 1,
      //     });
      //     if (!updateTrasactionData)
      //       throw new MoleculerError("ERROR IN WALLET WITHDRAWAL", 500);

      //     return {
      //       "code": 200,
      //       "data": {
      //         "wallet": {
      //           ...userData.wallet,
      //           ...{
      //             "availableAmount":
      //               userData.wallet.availableAmount - context.params.amount,
      //           },
      //         },
      //       },
      //       "message": WALLET_WITHDRAW_SUCCESS,
      //     };
      //   } else {
      //     throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
      //   }
      // }
      switch (paymentData.gateWay) {
        case constantUtil.STRIPE:
          {
            // //----------- InComplete --------------
            paymentGatwayResponse = await this.broker.emit(
              "transaction.stripe.newWithdrawalWalletTransaction",
              {
                ...context.params,
                "paymentData": paymentData,
                "userData": userData,
                "bankDetails": bankDetails,
                "generalSettings": generalSettings,
                //'transactionId': transaction._id.toString(),
                "transactionReason": stringTransactionReason,
                "clientIp": context.meta?.clientIp,
              }
            );
            paymentGatwayResponse =
              paymentGatwayResponse && paymentGatwayResponse[0];
            // //----------- InComplete --------------
            // return {
            //   "code": 400,
            //   "data": null,
            //   "message": "STRIPE AUTOMATED PROCESS UNDER PROGRESS",
            // };
          }
          break;

        case constantUtil.PEACH:
          {
            // //----------- InComplete --------------
            paymentGatwayResponse = await this.broker.emit(
              "transaction.peach.newWithdrawalWalletTransaction",
              {
                ...context.params,
                "paymentData": paymentData,
                "userData": userData,
                "bankDetails": bankDetails,
                "generalSettings": generalSettings,
                //'transactionId': transaction._id.toString(),
                "transactionReason": stringTransactionReason,
                "clientIp": context.meta?.clientIp,
              }
            );
            paymentGatwayResponse =
              paymentGatwayResponse && paymentGatwayResponse[0];
            transactionStatus = constantUtil.SUCCESS;
          }
          break;

        case constantUtil.CONST_MERCADOPAGO:
          {
            // //----------- InComplete --------------
            paymentGatwayResponse = await this.broker.emit(
              "transaction.mercadopago.newWithdrawalWalletTransaction",
              {
                ...context.params,
                "paymentData": paymentData,
                "userData": userData,
                "bankDetails": bankDetails,
                "generalSettings": generalSettings,
                //'transactionId': transaction._id.toString(),
                "transactionReason": stringTransactionReason,
                "clientIp": context.meta?.clientIp,
              }
            );
            paymentGatwayResponse =
              paymentGatwayResponse && paymentGatwayResponse[0];
            transactionStatus = constantUtil.PENDING;
          }
          break;
      }
      //-----------------
      if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
        if (paymentData.version === constantUtil.CONST_V3) {
          paymentGatwayResponse = await this.broker.emit(
            "transaction.flutterwave.withdraw",
            {
              "userType": userType,
              "userId": userData._id.toString(),
              "amount": context.params.amount,
            }
          );
        } else {
          paymentGatwayResponse = await this.broker.emit(
            "transaction.flutterwave.withdraw",
            {
              "userType": userType,
              "userId": userData._id.toString(),
              "amount": context.params.amount,
            }
          );
        }
        paymentGatwayResponse =
          paymentGatwayResponse && paymentGatwayResponse[0];
        return paymentGatwayResponse;
      }
      break;
  }
  //--------------- Common Response Condition -----------
  if (paymentGatwayResponse?.data?.status === constantUtil.SUCCESS) {
    // -------- updateAmount Start ------------
    if (context.params.userType === constantUtil.USER) {
      this.broker.emit("user.updateWalletAmount", {
        "userId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
        "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        "requestFrom": constantUtil.WALLETWITHDRAWAL,
      });
    } else if (context.params.userType === constantUtil.PROFESSIONAL) {
      this.broker.emit("professional.updateWalletAmount", {
        "professionalId": userData._id.toString(),
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount),
        "freezedAmount": userData.wallet.freezedAmount,
        "requestFrom": constantUtil.WALLETWITHDRAWAL,
      });
    }
    // -------- updateAmount End ------------
    // ------------------ create Transaction Start ----------------------
    const accountsDetail = cryptoUtils.cryptoAESEncode(
      JSON.stringify(bankDetails)
    );
    const transaction = await this.adapter.model.create({
      "clientId": clientId,
      "gatewayName": paymentData.gateWay,
      "paymentGateWay": paymentData.gateWay,
      "transactionId":
        paymentGatwayResponse?.data?.transactionId || transactionId,
      "gatewayDocId": paymentData._id,
      "transactionAmount": parseFloat(context.params.amount),
      "previousBalance": userData.wallet.availableAmount,
      "currentBalance":
        userData.wallet.availableAmount - parseFloat(context.params.amount),
      "transactionReason": stringTransactionReason,
      "transactionType": constantUtil.WALLETWITHDRAWAL,
      "transactionStatus": transactionStatus,
      "transactionDate": new Date(),
      "paymentType": constantUtil.DEBIT,
      "accountsDetail": accountsDetail?.toString(),
      "from": {
        "userType": constantUtil.ADMIN,
      },
      "to": {
        "userType": context.params.userType,
        "userId": userData._id,
        "name": userData.firstName + " " + userData.lastName || null,
        "avatar": userData.avatar || undefined,
        "phoneNumber": userData.phone.number || undefined,
      },
      "gatewayResponse": paymentGatwayResponse?.data?.successResponse || {},
      "payoutOption": generalSettings.data.payoutOptions,
    });
    if (!transaction) {
      throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
    }
    // ------------------ create Transaction End ----------------------
    // Response
    return {
      "code": 200,
      "data": {
        "availableAmount":
          userData.wallet.availableAmount - parseFloat(context.params.amount), // Need to Remove After Sep 2024
        "wallet": {
          ...userData.wallet,
          "availableAmount":
            userData.wallet.availableAmount - parseFloat(context.params.amount),
        },
      },
      "message": BANK_CREDET_MONEY,
    };
  } else {
    return {
      "code": 400,
      "data": null,
      "message": paymentGatwayResponse.message,
    };
  }
};
transactionAction.sendMoneyToFriends = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  let responseJson;
  let languageCode = context.params.langCode;
  // if (context.meta.userId) {
  //   responseJson = await this.broker.emit("user.sendMoneyToFriends", {
  //     "userId": context.meta.userId,
  //     "amount": parseFloat(context.params.amount),
  //     "phoneNumber": context.params.phoneNumber,
  //     "phoneCode": context.params.phoneCode,
  //   });
  //   languageCode = context.meta?.userDetails?.languageCode;
  // }
  // if (context.meta.professionalId) {
  //   // console.log('i am professional')
  //   responseJson = await this.broker.emit("professional.sendMoneyToFriends", {
  //     "professionalId": context.meta.professionalId,
  //     "amount": parseFloat(context.params.amount),
  //     "phoneNumber": context.params.phoneNumber,
  //     "phoneCode": context.params.phoneCode,
  //   });
  //   languageCode = context.meta?.professionalDetails?.languageCode;
  // }
  const { USER_NOT_FOUND, INFO_SEND_MONEY } =
    await notifyMessage.setNotifyLanguage(languageCode);
  if (
    context.params.userType === constantUtil.USER &&
    context.params.receiverType === constantUtil.USER
  ) {
    responseJson = await this.broker.emit("user.sendMoneyToFriends", {
      "userId": context.params.userId,
      "amount": parseFloat(context.params.amount),
      "receiverType": context.params.receiverType,
      "phoneCode": context.params.receiverPhoneCode,
      "phoneNumber": context.params.receiverPhoneNumber,
      "langCode": languageCode,
    });
  } else if (
    context.params.userType === constantUtil.PROFESSIONAL &&
    context.params.receiverType === constantUtil.PROFESSIONAL
  ) {
    responseJson = await this.broker.emit("professional.sendMoneyToFriends", {
      "professionalId": context.params.professionalId,
      "amount": parseFloat(context.params.amount),
      "receiverType": context.params.receiverType,
      "phoneCode": context.params.receiverPhoneCode,
      "phoneNumber": context.params.receiverPhoneNumber,
      "langCode": languageCode,
    });
  } else if (
    context.params.userType === constantUtil.USER &&
    context.params.receiverType === constantUtil.PROFESSIONAL
  ) {
    responseJson = await this.broker.emit("user.sendMoneyToFriends", {
      "userId": context.params.userId,
      "amount": parseFloat(context.params.amount),
      "receiverType": context.params.receiverType,
      "phoneCode": context.params.receiverPhoneCode,
      "phoneNumber": context.params.receiverPhoneNumber,
      "langCode": languageCode,
    });
  } else if (
    context.params.userType === constantUtil.PROFESSIONAL &&
    context.params.receiverType === constantUtil.USER
  ) {
    responseJson = await this.broker.emit("professional.sendMoneyToFriends", {
      "professionalId": context.params.professionalId,
      "amount": parseFloat(context.params.amount),
      "receiverType": context.params.receiverType,
      "phoneCode": context.params.receiverPhoneCode,
      "phoneNumber": context.params.receiverPhoneNumber,
      "langCode": languageCode,
    });
  }
  responseJson = responseJson && responseJson[0];
  // console.log('this is responseJson', responseJson)
  if (!responseJson) {
    throw new MoleculerError(USER_NOT_FOUND, 500);
  }
  if (responseJson) {
    // if (
    //   (context.params.userType === constantUtil.USER &&
    //     context.params.receiverType === constantUtil.USER) ||
    //   (context.params.userType === constantUtil.PROFESSIONAL &&
    //     context.params.receiverType === constantUtil.PROFESSIONAL)
    // ) {
    // Sender ( From )
    await this.adapter.insert({
      "clientId": clientId,
      "transactionType": constantUtil.SENDMONEYTOFRIEND,
      "transactionAmount": parseFloat(context.params.amount),
      "previousBalance": responseJson.from.wallet.availableAmount,
      "currentBalance":
        responseJson.from.wallet.availableAmount -
        parseFloat(context.params.amount),
      "transactionReason":
        context.params.reason ||
        "Amount Transfered to " + responseJson.to.phone.number,
      "transactionId": helperUtil.randomString(8, "a"),
      "transactionDate": new Date(),
      "transactionStatus": constantUtil.SUCCESS,
      "paymentType": constantUtil.DEBIT,
      "from": {
        // "userType": context.meta.userId
        //   ? constantUtil.USER
        //   : constantUtil.PROFESSIONAL,
        "userType": context.params.userType,
        "userId": responseJson.from._id.toString(),
        "name": responseJson.from.firstName + responseJson.from.lastName,
        "avatar": responseJson.from.avatar,
        "phoneNumber": responseJson.from.phone.number,
      },
      // "to": {
      //   // "userType": context.meta.userId
      //   //   ? constantUtil.USER
      //   //   : constantUtil.PROFESSIONAL,
      //   "userType": context.params.receiverType,
      //   "userId": responseJson.to._id.toString(),
      //   "name": responseJson.to.firstName + responseJson.to.lastName,
      //   "avatar": responseJson.to.avatar,
      //   "phoneNumber": responseJson.to.phone.number,
      // },
      "to": {
        "userType": constantUtil.ADMIN,
        "userId": null,
      },
      // 'isActive': 1,
    });
    // Receiver ( To )
    await this.adapter.insert({
      "clientId": clientId,
      "transactionType": constantUtil.SENDMONEYTOFRIEND,
      "transactionAmount": parseFloat(context.params.amount),
      "previousBalance": responseJson.to.wallet.availableAmount,
      "currentBalance":
        responseJson.to.wallet.availableAmount +
        parseFloat(context.params.amount),
      "transactionReason":
        context.params.reason ||
        "Amount Transfered From " + responseJson.from.phone.number,
      "transactionId": helperUtil.randomString(8, "a"),
      "transactionDate": new Date(),
      "transactionStatus": constantUtil.SUCCESS,
      "paymentType": constantUtil.CREDIT,
      "from": {
        "userType": constantUtil.ADMIN,
        "userId": null,
      },
      "to": {
        "userType": context.params.receiverType,
        "userId": responseJson.to._id.toString(),
        "name": responseJson.to.firstName + responseJson.to.lastName,
        "avatar": responseJson.to.avatar,
        "phoneNumber": responseJson.to.phone.number,
      },
    });
    // if (!updateTrasactionData) {
    //   throw new MoleculerError(USER_NOT_FOUND, 500);
    // }
    return {
      "code": 200,
      "data": {
        "wallet": {
          ...responseJson.from.wallet,
          "availableAmount":
            responseJson.from.wallet.availableAmount -
            parseFloat(context.params.amount),
        },
      },
      "message": INFO_SEND_MONEY,
    };
    // }
  }
};
transactionAction.getTransactionDetails = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = {};
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    WALLET_LOADED,
    WALLET_WITHDRAW,
    AMOUT_SEND_TO_FRIEND,
    BOOKING_PAID,
    BOOKING_CHARGE_FREEZED,
    RIDE_CANCELLATION_CHARGE,
    PROFESSIONAL_EARNINGS_RECEIVED,
    PROFESSIONAL_TOLERENCE_AMOUNT,
    WALLET_DEBIT,
    INFO_TIPS,
    INFO_JOINING_CHARGE,
    INFO_REFERREL_CHARGE,
    INFO_CARD_AMOUNT_REFUND,
    INFO_EXTRA_AMOUNT_DEBIT,
    INFO_RIDE_CANCEL_CREDIT,
    INFO_HUBS_EARNINGS,
    TRAN_REDEEM_REWARD_POINT,
    INFO_INCENTIVE_CREDIT,
    TIPS_AMOUNT,
    WALLET_AMOUNT_LOADED,
    WALLET_RECHARGE_FAILED,
    AMOUT_RECEIVED_FROM_FRIEND,
    INFO_SUBSCRIPTIONCHARGE,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    // userData = userData && userData[0];
    // if (!userData) {
    //   throw new MoleculerError(USER_NOT_FOUND, 500);
    // }
  } else {
    userData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(),
    });
    // userData = userData && userData[0];
    // if (!userData) {
    //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    // }
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      userType === constantUtil.USER ? USER_NOT_FOUND : PROFESSIONAL_NOT_FOUND,
      500
    );
  }
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];

  const condition =
    userType === constantUtil.USER
      ? {
          "$or": [
            { "from.userId": context.meta.userId, "from.userType": userType },
            { "to.userId": context.meta.userId, "to.userType": userType },
          ],
        }
      : {
          "$or": [
            {
              "from.userId": context.meta.professionalId,
              "from.userType": userType,
            },
            {
              "to.userId": context.meta.professionalId,
              "to.userType": userType,
            },
          ],
        };

  query.push({
    "$match": {
      //"clientId": Mongoose.Types.ObjectId(clientId),
      "transactionType": {
        "$in": [
          constantUtil.WALLETRECHARGE,
          constantUtil.WALLETWITHDRAWAL,
          constantUtil.SENDMONEYTOFRIEND,
          constantUtil.BOOKINGCHARGE,
          constantUtil.SUBSCRIPTIONCHARGE,
          constantUtil.CANCELLATIONCHARGE,
          constantUtil.PROFESSIONALEARNINGS,
          constantUtil.ADMINRECHARGE,
          constantUtil.ADMINDEBIT,
          constantUtil.TIPSCHARGE,
          constantUtil.JOININGCHARGE,
          constantUtil.INVITECHARGE,
          constantUtil.CARDAMOUNTREFUND,
          constantUtil.EXTRAAMOUNTDEBIT,
          constantUtil.PROFESSIONALTOLERENCEAMOUNT,
          constantUtil.REDEEMREWARDPOINT,
          constantUtil.REDEEMREWARDAIRTIME,
          constantUtil.CANCELLATIONCREDIT,
          constantUtil.CONST_INCENTIVE,
          constantUtil.PENALITY,
        ],
      },
      ...condition,
    },
  });

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "transactionType": "$transactionType",
            "transactionAmount": "$transactionAmount",
            "transactionId": "$transactionId",
            "accountsDetail": "$accountsDetail",
            "transactionDate": "$transactionDate",
            "transactionStatus": "$transactionStatus",
            "paymentGateWay": "$paymentGateWay",
            "paymentType": "$paymentType",
            "to": "$to",
            "from": "$from",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return {
    "code": 200,
    "data":
      jsonData?.[0]?.response?.map((data) => {
        let transactionType = null,
          paymentOption = null;
        switch (data.transactionType) {
          case constantUtil.WALLETRECHARGE:
            if (data.transactionStatus === constantUtil.FAILED) {
              transactionType = WALLET_RECHARGE_FAILED;
            } else {
              transactionType = WALLET_AMOUNT_LOADED;
            }
            break;

          case constantUtil.WALLETWITHDRAWAL:
            transactionType = WALLET_WITHDRAW;
            break;

          case constantUtil.SENDMONEYTOFRIEND:
            //transactionType = AMOUT_SEND_TO_FRIEND;
            transactionType =
              data.to.userId === context.meta.userId ||
              data.to.userId === context.meta.professionalId
                ? AMOUT_RECEIVED_FROM_FRIEND
                : AMOUT_SEND_TO_FRIEND;
            break;

          case constantUtil.BOOKINGCHARGE:
          case constantUtil.BOOKINGCHARGECARD:
            transactionType = BOOKING_PAID;
            break;

          case constantUtil.BOOKINGCHARGEFREEZED:
            transactionType = BOOKING_CHARGE_FREEZED;
            break;

          case constantUtil.SUBSCRIPTIONCHARGE:
            transactionType = INFO_SUBSCRIPTIONCHARGE;
            break;

          case constantUtil.CANCELLATIONCHARGE:
            transactionType = RIDE_CANCELLATION_CHARGE;
            break;

          case constantUtil.PROFESSIONALEARNINGS:
            transactionType = PROFESSIONAL_EARNINGS_RECEIVED;
            break;

          case constantUtil.PROFESSIONALTOLERENCEAMOUNT:
            transactionType = PROFESSIONAL_TOLERENCE_AMOUNT;
            break;

          case constantUtil.ADMINRECHARGE:
            transactionType = WALLET_LOADED;
            break;

          case constantUtil.ADMINDEBIT:
            transactionType = WALLET_DEBIT;
            break;

          case constantUtil.TIPSCHARGE:
            transactionType =
              userType === constantUtil.USER ? INFO_TIPS : TIPS_AMOUNT;
            break;

          case constantUtil.JOININGCHARGE:
            transactionType = INFO_JOINING_CHARGE;
            break;

          case constantUtil.INVITECHARGE:
            transactionType = INFO_REFERREL_CHARGE;
            break;

          case constantUtil.CARDAMOUNTREFUND:
            transactionType = INFO_CARD_AMOUNT_REFUND;
            break;

          case constantUtil.EXTRAAMOUNTDEBIT:
            transactionType = INFO_EXTRA_AMOUNT_DEBIT;
            break;

          case constantUtil.REDEEMREWARDPOINT:
            transactionType = TRAN_REDEEM_REWARD_POINT;
            break;

          case constantUtil.REDEEMREWARDAIRTIME:
            transactionType = "Mobile Recharge";
            break;

          case constantUtil.CANCELLATIONCREDIT:
            transactionType = INFO_RIDE_CANCEL_CREDIT;
            break;

          case constantUtil.CONST_INCENTIVE:
            transactionType = INFO_INCENTIVE_CREDIT;
            break;
          case constantUtil.PENALITY:
            transactionType = RIDE_CANCELLATION_CHARGE;
            break;

          default:
            transactionType = data.transactionType;
            break;
        }
        //
        switch (data?.paymentGateWay || constantUtil.NONE) {
          case constantUtil.WALLET:
            paymentOption = constantUtil.WALLET;
            break;
          case constantUtil.NONE:
            paymentOption = constantUtil.CASH;
            break;
          default:
            paymentOption = constantUtil.CARD;
            break;
        }
        //
        return {
          "_id": data._id,
          "transactionType": transactionType,
          "transactionAmount": data.transactionAmount,
          "transactionId": data.transactionId,
          "accountsDetail": data.accountsDetail,
          "transactionDate": data.transactionDate,
          "transactionStatus": data.transactionStatus,
          "paymentOption": paymentOption,
          "paymentType":
            data.to.userId === context.meta.userId ||
            data.to.userId === context.meta.professionalId
              ? "CREDIT"
              : data.paymentType,
          "to": data.to,
          "from": data.from,
        };
      }) || [],
    "walletDetails": userData.wallet,
    "message": "",
  };
};

transactionAction.getParticularTransactionList = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    WALLET_LOADED,
    WALLET_WITHDRAW,
    AMOUT_SEND_TO_FRIEND,
    BOOKING_PAID,
    BOOKING_CHARGE_FREEZED,
    RIDE_CANCELLATION_CHARGE,
    PROFESSIONAL_EARNINGS_RECEIVED,
    PROFESSIONAL_TOLERENCE_AMOUNT,
    WALLET_DEBIT,
    INFO_TIPS,
    INFO_JOINING_CHARGE,
    INFO_REFERREL_CHARGE,
    INFO_CARD_AMOUNT_REFUND,
    INFO_EXTRA_AMOUNT_DEBIT,
    INFO_RIDE_CANCEL_CREDIT,
    INFO_HUBS_EARNINGS,
    TRAN_REDEEM_REWARD_POINT,
    INFO_INCENTIVE_CREDIT,
    TIPS_AMOUNT,
    WALLET_AMOUNT_LOADED,
    WALLET_RECHARGE_FAILED,
    AMOUT_RECEIVED_FROM_FRIEND,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  // let hubTransactionQuery = {};
  // switch (context.params.loginUserType) {
  //   case constantUtil.HUBS:
  //     // case constantUtil.DEVELOPER:
  //     hubTransactionQuery = {
  //       "vehicles.hub": Mongoose.Types.ObjectId(context.params.loginUserId),
  //     };
  //     break;
  // }
  const condition = {
    "$or": [
      { "from.userId": context.params.id, "from.userType": userType },
      { "to.userId": context.params.id, "to.userType": userType },
      // { "from.userId": context.params.id, "from.userType": constantUtil.HUBS }, // For HUBS Only
      // { "to.userId": context.params.id, "to.userType": constantUtil.HUBS }, // For HUBS Only
    ],
  };
  if (context.params.paymentGateWay === constantUtil.NONE) {
    query.push({
      "$match": {
        //"clientId": Mongoose.Types.ObjectId(clientId),
        "transactionType": {
          "$in": [
            constantUtil.WALLETRECHARGE,
            constantUtil.WALLETWITHDRAWAL,
            constantUtil.SENDMONEYTOFRIEND,
            constantUtil.BOOKINGCHARGE,
            constantUtil.SUBSCRIPTIONCHARGE,
            constantUtil.CANCELLATIONCHARGE,
            constantUtil.PROFESSIONALEARNINGS,
            constantUtil.ADMINRECHARGE,
            constantUtil.ADMINDEBIT,
            constantUtil.TIPSCHARGE,
            constantUtil.JOININGCHARGE,
            constantUtil.INVITECHARGE,
            constantUtil.CARDAMOUNTREFUND,
            constantUtil.EXTRAAMOUNTDEBIT,
            constantUtil.PROFESSIONALTOLERENCEAMOUNT,
            constantUtil.CANCELLATIONCREDIT,
            constantUtil.HUBSEARNINGS,
            constantUtil.REDEEMREWARDPOINT,
            constantUtil.CONST_INCENTIVE,
            constantUtil.PENALITY,
          ],
        },
        // ...hubTransactionQuery,
        ...condition,
      },
    });
  } else {
    query.push({
      "$match": {
        "paymentGateWay": {
          "$in": [context.params.paymentGateWay],
        },
        // ...hubTransactionQuery,
        ...condition,
      },
    });
  }

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "transactionType": "$transactionType",
            "transactionAmount": "$transactionAmount",
            "transactionId": "$transactionId",
            "accountsDetail": "$accountsDetail",
            "transactionDate": "$transactionDate",
            "transactionStatus": "$transactionStatus",
            "transactionReason": "$transactionReason",
            "refBookingId": "$refBookingId",
            "paymentGateWay": "$paymentGateWay",
            "paymentType": "$paymentType",
            "isReimbursement": "$isReimbursement",
            "previousBalance": "$previousBalance",
            "currentBalance": "$currentBalance",
            "to": "$to",
            "from": "$from",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseData = {};

  responseData["count"] = jsonData[0]?.all[0]?.all || 0;
  responseData["response"] =
    jsonData?.[0]?.response?.map((data) => {
      let transactionType = null,
        paymentOption = null;
      switch (data.transactionType) {
        case constantUtil.WALLETRECHARGE:
          transactionType = WALLET_LOADED;
          break;
        case constantUtil.WALLETWITHDRAWAL:
          transactionType = WALLET_WITHDRAW;
          break;
        case constantUtil.SENDMONEYTOFRIEND:
          //transactionType = AMOUT_SEND_TO_FRIEND;
          transactionType =
            data.to.userId === context.meta.userId ||
            data.to.userId === context.meta.professionalId
              ? AMOUT_RECEIVED_FROM_FRIEND
              : AMOUT_SEND_TO_FRIEND;
          break;
        case constantUtil.BOOKINGCHARGE:
        case constantUtil.BOOKINGCHARGECARD:
          transactionType = BOOKING_PAID;
          break;
        case constantUtil.BOOKINGCHARGEFREEZED:
          transactionType = BOOKING_CHARGE_FREEZED;
          break;
        case constantUtil.SUBSCRIPTIONCHARGE:
          transactionType = BOOKING_CHARGE_FREEZED;
          break;
        case constantUtil.CANCELLATIONCHARGE:
          transactionType = RIDE_CANCELLATION_CHARGE;
          break;
        case constantUtil.PROFESSIONALEARNINGS:
          transactionType = PROFESSIONAL_EARNINGS_RECEIVED;
          break;
        case constantUtil.ADMINRECHARGE:
          transactionType = WALLET_LOADED;
          break;
        case constantUtil.PROFESSIONALTOLERENCEAMOUNT:
          transactionType = PROFESSIONAL_TOLERENCE_AMOUNT;
          break;
        case constantUtil.ADMINDEBIT:
          transactionType = WALLET_DEBIT;
          break;
        case constantUtil.TIPSCHARGE:
          transactionType =
            userType === constantUtil.USER ? INFO_TIPS : TIPS_AMOUNT;
          break;
        case constantUtil.JOININGCHARGE:
          transactionType = INFO_JOINING_CHARGE;
          break;
        case constantUtil.INVITECHARGE:
          transactionType = INFO_REFERREL_CHARGE;
          break;
        case constantUtil.CARDAMOUNTREFUND:
          transactionType = INFO_CARD_AMOUNT_REFUND;
          break;
        case constantUtil.EXTRAAMOUNTDEBIT:
          transactionType = INFO_EXTRA_AMOUNT_DEBIT;
          break;
        case constantUtil.CANCELLATIONCREDIT:
          transactionType = INFO_RIDE_CANCEL_CREDIT;
          break;
        case constantUtil.HUBSEARNINGS:
          transactionType = INFO_HUBS_EARNINGS;
          break;
        case constantUtil.REDEEMREWARDPOINT:
          transactionType = TRAN_REDEEM_REWARD_POINT;
          break;
        case constantUtil.CONST_INCENTIVE:
          transactionType = INFO_INCENTIVE_CREDIT;
          break;
        case constantUtil.PENALITY:
          transactionType = RIDE_CANCELLATION_CHARGE;
          break;
        default:
          transactionType = data.transactionType;
          break;
      }
      //
      switch (data?.paymentGateWay || constantUtil.NONE) {
        case constantUtil.WALLET:
          paymentOption = constantUtil.WALLET;
          break;
        case constantUtil.NONE:
          paymentOption = constantUtil.CASH;
          break;
        default:
          paymentOption = constantUtil.CARD;
          break;
      }
      return {
        "_id": data._id,
        "transactionType": transactionType,
        "transactionAmount": data.transactionAmount,
        "transactionId": data.transactionId,
        "accountsDetail": data.accountsDetail,
        "transactionDate": data.transactionDate,
        "transactionStatus": data.transactionStatus,
        "transactionReason": data.transactionReason,
        "refBookingId": data.refBookingId,
        "paymentOption": paymentOption,
        "paymentType": data.paymentType,
        "isReimbursement": data.isReimbursement,
        "previousBalance":
          data.previousBalance == null
            ? ""
            : parseFloat(data.previousBalance).toFixed(2),
        "currentBalance":
          data.currentBalance == null ? "" : parseFloat(data.currentBalance),
        "to": data.to,
        "from": data.from,
      };
    }) || [];
  return responseData;
};

transactionAction.getWalletTransactionList = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let walletData = 0;
  let languageCode;
  if (userType === constantUtil.USER) {
    walletData = await this.broker.emit("user.getAvailableWalletAmount", {
      "clientId": clientId,
    });
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    walletData = await this.broker.emit(
      "professional.getAvailableWalletAmount",
      {
        "clientId": clientId,
      }
    );
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    WALLET_LOADED,
    WALLET_WITHDRAW,
    AMOUT_SEND_TO_FRIEND,
    BOOKING_PAID,
    BOOKING_CHARGE_FREEZED,
    RIDE_CANCELLATION_CHARGE,
    PROFESSIONAL_EARNINGS_RECEIVED,
    PROFESSIONAL_TOLERENCE_AMOUNT,
    WALLET_DEBIT,
    INFO_TIPS,
    INFO_JOINING_CHARGE,
    INFO_REFERREL_CHARGE,
    INFO_CARD_AMOUNT_REFUND,
    INFO_EXTRA_AMOUNT_DEBIT,
    INFO_RIDE_CANCEL_CREDIT,
    INFO_HUBS_EARNINGS,
    TRAN_REDEEM_REWARD_POINT,
    INFO_INCENTIVE_CREDIT,
    TIPS_AMOUNT,
    WALLET_AMOUNT_LOADED,
    WALLET_RECHARGE_FAILED,
    AMOUT_RECEIVED_FROM_FRIEND,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  walletData = walletData && walletData[0];
  if (walletData <= 0) walletData = 0;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const query1 = [];
  const query2 = [];

  const match = {};
  match["$and"] = [];
  match["$and"].push({
    //"clientId": Mongoose.Types.ObjectId(context.params.clientId),
    "transactionType": {
      "$in": [
        constantUtil.WALLETRECHARGE,
        constantUtil.WALLETWITHDRAWAL,
        constantUtil.SENDMONEYTOFRIEND,
        constantUtil.ADMINRECHARGE,
        constantUtil.SUBSCRIPTIONCHARGE,
        constantUtil.ADMINDEBIT,
        constantUtil.CANCELLATIONCHARGE,
        constantUtil.BOOKINGCHARGE,
        constantUtil.PROFESSIONALEARNINGS,
        constantUtil.JOININGCHARGE,
        constantUtil.INVITECHARGE,
        constantUtil.CARDAMOUNTREFUND,
        constantUtil.EXTRAAMOUNTDEBIT,
        constantUtil.PROFESSIONALTOLERENCEAMOUNT,
        constantUtil.CANCELLATIONCREDIT,
        constantUtil.CONST_INCENTIVE,
        constantUtil.TIPSCHARGE,
        constantUtil.PENALITY,
      ],
    },
    "$or": [{ "from.userType": userType }, { "to.userType": userType }],
  });

  const match1 = {};
  match1["$and"] = [];
  match1["$and"].push({
    //"clientId": Mongoose.Types.ObjectId(clientId),
    "transactionType": {
      "$in": [constantUtil.ADMINRECHARGE],
    },
    "isReimbursement": true,
    "$or": [{ "from.userType": userType }, { "to.userType": userType }],
  });
  if ((context.params?.filter || "") !== "") {
    match["$and"].push({
      "transactionStatus": context.params.filter,
    });
    match1["$and"].push({
      "transactionStatus": context.params.filter,
    });
  }
  if ((context.params?.reimbursefilter || "") !== "") {
    match["$and"].push({
      "isReimbursement": context.params.reimbursefilter,
    });
  }
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({
      "transactionDate": { "$gte": fromDate, "$lt": toDate },
    });
    match1["$and"].push({
      "transactionDate": { "$gte": fromDate, "$lt": toDate },
    });
  }

  if ((context.params?.search || "") !== "") {
    match["$and"].push({
      "$or": [
        {
          "transactionId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "from.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "from.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "to.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "to.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    });
    match1["$and"].push({
      "$or": [
        {
          "transactionId": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "from.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "from.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "to.name": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        {
          "to.phoneNumber": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
      ],
    });
  }
  query.push({
    "$match": match,
  });
  query1.push({
    "$match": match,
  });
  query2.push({
    "$match": match1,
  });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "transactionType": "$transactionType",
            "transactionAmount": "$transactionAmount",
            "transactionId": "$transactionId",
            "transactionDate": "$transactionDate",
            "transactionStatus": "$transactionStatus",
            "paymentGateWay": "$paymentGateWay",
            "to": "$to",
            "from": "$from",
            "isReimbursement": "$isReimbursement",
          },
        },
      ],
    },
  });

  query1.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "transactionAmount": "$transactionAmount",
          },
        },
        {
          "$group": {
            "_id": "",
            "totalTransactionAmount": { "$sum": "$transactionAmount" },
          },
        },
      ],
    },
  });

  query2.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "totalReimbursementAmount": "$transactionAmount",
          },
        },
        {
          "$group": {
            "_id": "",
            "totalReimbursementAmount": { "$sum": "$totalReimbursementAmount" },
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const jsonData1 = await this.adapter.model
    .aggregate(query1)
    .allowDiskUse(true);
  const jsonData2 = await this.adapter.model
    .aggregate(query2)
    .allowDiskUse(true);

  const responseJson = {};
  responseJson["count"] = jsonData?.[0]?.all?.[0]?.all || 0;
  responseJson["response"] =
    jsonData?.[0]?.response?.map((data) => {
      let transactionType = null,
        paymentOption = null;
      switch (data.transactionType) {
        case constantUtil.WALLETRECHARGE:
          transactionType = WALLET_LOADED;
          break;
        case constantUtil.WALLETWITHDRAWAL:
          transactionType = WALLET_WITHDRAW;
          break;
        case constantUtil.SENDMONEYTOFRIEND:
          //transactionType = AMOUT_SEND_TO_FRIEND;
          transactionType =
            data.to.userId === context.meta.userId ||
            data.to.userId === context.meta.professionalId
              ? AMOUT_RECEIVED_FROM_FRIEND
              : AMOUT_SEND_TO_FRIEND;
          break;
        case constantUtil.BOOKINGCHARGE:
        case constantUtil.BOOKINGCHARGECARD:
          transactionType = BOOKING_PAID;
          break;
        case constantUtil.BOOKINGCHARGEFREEZED:
          transactionType = BOOKING_CHARGE_FREEZED;
          break;
        case constantUtil.SUBSCRIPTIONCHARGE:
          transactionType = BOOKING_CHARGE_FREEZED;
          break;
        case constantUtil.CANCELLATIONCHARGE:
          transactionType = RIDE_CANCELLATION_CHARGE;
          break;
        case constantUtil.PROFESSIONALEARNINGS:
          transactionType = PROFESSIONAL_EARNINGS_RECEIVED;
          break;
        case constantUtil.ADMINRECHARGE:
          transactionType = WALLET_LOADED;
          break;
        case constantUtil.PROFESSIONALTOLERENCEAMOUNT:
          transactionType = PROFESSIONAL_TOLERENCE_AMOUNT;
          break;
        case constantUtil.ADMINDEBIT:
          transactionType = WALLET_DEBIT;
          break;
        case constantUtil.TIPSCHARGE:
          transactionType =
            userType === constantUtil.USER ? INFO_TIPS : TIPS_AMOUNT;
          break;
        case constantUtil.JOININGCHARGE:
          transactionType = INFO_JOINING_CHARGE;
          break;
        case constantUtil.INVITECHARGE:
          transactionType = INFO_REFERREL_CHARGE;
          break;
        case constantUtil.CARDAMOUNTREFUND:
          transactionType = INFO_CARD_AMOUNT_REFUND;
          break;
        case constantUtil.EXTRAAMOUNTDEBIT:
          transactionType = INFO_EXTRA_AMOUNT_DEBIT;
          break;
        case constantUtil.CANCELLATIONCREDIT:
          transactionType = INFO_RIDE_CANCEL_CREDIT;
          break;
        case constantUtil.HUBSEARNINGS:
          transactionType = INFO_HUBS_EARNINGS;
          break;
        case constantUtil.REDEEMREWARDPOINT:
          transactionType = TRAN_REDEEM_REWARD_POINT;
          break;
        case constantUtil.CONST_INCENTIVE:
          transactionType = INFO_INCENTIVE_CREDIT;
          break;
        case constantUtil.PENALITY:
          transactionType = RIDE_CANCELLATION_CHARGE;
          break;
        default:
          transactionType = data.transactionType;
          break;
      }
      //
      switch (data?.paymentGateWay || constantUtil.NONE) {
        case constantUtil.WALLET:
          paymentOption = constantUtil.WALLET;
          break;
        case constantUtil.NONE:
          paymentOption = constantUtil.CASH;
          break;
        default:
          paymentOption = constantUtil.CARD;
          break;
      }
      return {
        "_id": data._id,
        "transactionType": transactionType,
        "transactionAmount": data.transactionAmount,
        "transactionId": data.transactionId,
        "accountsDetail": data.accountsDetail,
        "transactionDate": data.transactionDate,
        "transactionStatus": data.transactionStatus,
        "paymentOption": paymentOption,
        "paymentType": data.paymentType,
        "isReimbursement": data.isReimbursement,
        "to": data.to,
        "from": data.from,
      };
    }) || [];
  responseJson["dashboardData"] =
    jsonData1[0].response && jsonData1[0].response.length > 0
      ? jsonData2[0].response && jsonData2[0].response.length > 0
        ? {
            "_id": "",
            "totalTransactionAmount":
              jsonData1[0].response[0].totalTransactionAmount,
            "totalReimbursementAmount":
              jsonData2[0].response[0].totalReimbursementAmount,
            "totalWalletAmount": walletData || 0,
          }
        : {
            "_id": "",
            "totalTransactionAmount":
              jsonData1[0].response[0].totalTransactionAmount,
            "totalReimbursementAmount": 0,
            "totalWalletAmount": walletData || 0,
          }
      : {
          "_id": "",
          "totalTransactionAmount": 0,
          "totalWalletAmount": 0,
        };
  return responseJson;
};

transactionAction.getTransactionDetailsById = async function (context) {
  const userType = context.params.userType?.toUpperCase();
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { SOMETHING_WENT_WRONG } = await notifyMessage.setNotifyLanguage(
    languageCode
  );
  try {
    responseData = await this.adapter.model
      .findOne({
        "_id": Mongoose.Types.ObjectId(context.params.id.toString()),
      })
      .lean();
    if (!responseData) {
      throw new MoleculerError(SOMETHING_WENT_WRONG);
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "response": responseData,
  };
};

transactionAction.getTransactionHistoryByType = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  const transactionType = context.params.transactionType.toUpperCase();
  let responseData = [],
    errorCode = 200,
    message = "",
    languageCode,
    count = 0,
    condition = {},
    transactionTypeList = [];
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    WALLET_LOADED,
    WALLET_WITHDRAW,
    AMOUT_SEND_TO_FRIEND,
    BOOKING_PAID,
    BOOKING_CHARGE_FREEZED,
    RIDE_CANCELLATION_CHARGE,
    PROFESSIONAL_EARNINGS_RECEIVED,
    PROFESSIONAL_TOLERENCE_AMOUNT,
    WALLET_DEBIT,
    INFO_TIPS,
    INFO_JOINING_CHARGE,
    INFO_REFERREL_CHARGE,
    INFO_CARD_AMOUNT_REFUND,
    INFO_EXTRA_AMOUNT_DEBIT,
    INFO_RIDE_CANCEL_CREDIT,
    INFO_HUBS_EARNINGS,
    TRAN_REDEEM_REWARD_POINT,
    INFO_INCENTIVE_CREDIT,
    TIPS_AMOUNT,
    AMOUT_RECEIVED_FROM_FRIEND,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const skip = parseInt(context.params.skip ?? 0);
    const limit = parseInt(context.params.limit ?? 20);
    const query = [];
    switch (transactionType) {
      case constantUtil.CONST_INCENTIVE:
        condition = { "adminId": Mongoose.Types.ObjectId(context.params.id) };
        transactionTypeList = [constantUtil.CONST_INCENTIVE];
        break;

      default:
        condition = {
          "$or": [
            { "from.userId": context.params.id, "from.userType": userType },
            { "to.userId": context.params.id, "to.userType": userType },
          ],
        };
        transactionTypeList = [
          constantUtil.WALLETRECHARGE,
          constantUtil.WALLETWITHDRAWAL,
          constantUtil.SENDMONEYTOFRIEND,
          constantUtil.BOOKINGCHARGE,
          constantUtil.SUBSCRIPTIONCHARGE,
          constantUtil.CANCELLATIONCHARGE,
          constantUtil.PROFESSIONALEARNINGS,
          constantUtil.ADMINRECHARGE,
          constantUtil.ADMINDEBIT,
          constantUtil.TIPSCHARGE,
          constantUtil.JOININGCHARGE,
          constantUtil.INVITECHARGE,
          constantUtil.CARDAMOUNTREFUND,
          constantUtil.EXTRAAMOUNTDEBIT,
          constantUtil.PROFESSIONALTOLERENCEAMOUNT,
          constantUtil.CANCELLATIONCREDIT,
          constantUtil.HUBSEARNINGS,
          constantUtil.REDEEMREWARDPOINT,
          constantUtil.CONST_INCENTIVE,
          constantUtil.PENALITY,
        ];
        break;
    }

    query.push({
      "$match": {
        //"clientId": Mongoose.Types.ObjectId(clientId),
        "transactionType": {
          "$in": transactionTypeList,
        },
        ...condition,
      },
    });

    query.push({
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          { "$sort": { "_id": -1 } },
          { "$skip": parseInt(skip) },
          { "$limit": parseInt(limit) },
          {
            "$project": {
              "_id": "$_id",
              "transactionType": "$transactionType",
              "transactionAmount": "$transactionAmount",
              "transactionId": "$transactionId",
              "accountsDetail": "$accountsDetail",
              "transactionDate": "$transactionDate",
              "transactionStatus": "$transactionStatus",
              "transactionReason": "$transactionReason",
              "refBookingId": "$refBookingId",
              "paymentGateWay": "$paymentGateWay",
              "paymentType": "$paymentType",
              "isReimbursement": "$isReimbursement",
              "previousBalance": "$previousBalance",
              "currentBalance": "$currentBalance",
              "to": "$to",
              "from": "$from",
            },
          },
        ],
      },
    });
    const jsonData = await this.adapter.model
      .aggregate(query)
      .allowDiskUse(true);

    count = jsonData[0]?.all[0]?.all || 0;
    responseData =
      jsonData?.[0]?.response?.map((data) => {
        let transactionType = null,
          paymentOption = null;
        switch (data.transactionType) {
          case constantUtil.WALLETRECHARGE:
            transactionType = WALLET_LOADED;
            break;
          case constantUtil.WALLETWITHDRAWAL:
            transactionType = WALLET_WITHDRAW;
            break;
          case constantUtil.SENDMONEYTOFRIEND:
            //transactionType = AMOUT_SEND_TO_FRIEND;
            transactionType =
              data.to.userId === context.meta.userId ||
              data.to.userId === context.meta.professionalId
                ? AMOUT_RECEIVED_FROM_FRIEND
                : AMOUT_SEND_TO_FRIEND;
            break;
          case constantUtil.BOOKINGCHARGE:
          case constantUtil.BOOKINGCHARGECARD:
            transactionType = BOOKING_PAID;
            break;
          case constantUtil.BOOKINGCHARGEFREEZED:
            transactionType = BOOKING_CHARGE_FREEZED;
            break;
          case constantUtil.SUBSCRIPTIONCHARGE:
            transactionType = BOOKING_CHARGE_FREEZED;
            break;
          case constantUtil.CANCELLATIONCHARGE:
            transactionType = RIDE_CANCELLATION_CHARGE;
            break;
          case constantUtil.PROFESSIONALEARNINGS:
            transactionType = PROFESSIONAL_EARNINGS_RECEIVED;
            break;
          case constantUtil.ADMINRECHARGE:
            transactionType = WALLET_LOADED;
            break;
          case constantUtil.PROFESSIONALTOLERENCEAMOUNT:
            transactionType = PROFESSIONAL_TOLERENCE_AMOUNT;
            break;
          case constantUtil.ADMINDEBIT:
            transactionType = WALLET_DEBIT;
            break;
          case constantUtil.TIPSCHARGE:
            transactionType =
              userType === constantUtil.USER ? INFO_TIPS : TIPS_AMOUNT;
            break;
          case constantUtil.JOININGCHARGE:
            transactionType = INFO_JOINING_CHARGE;
            break;
          case constantUtil.INVITECHARGE:
            transactionType = INFO_REFERREL_CHARGE;
            break;
          case constantUtil.CARDAMOUNTREFUND:
            transactionType = INFO_CARD_AMOUNT_REFUND;
            break;
          case constantUtil.EXTRAAMOUNTDEBIT:
            transactionType = INFO_EXTRA_AMOUNT_DEBIT;
            break;
          case constantUtil.CANCELLATIONCREDIT:
            transactionType = INFO_RIDE_CANCEL_CREDIT;
            break;
          case constantUtil.HUBSEARNINGS:
            transactionType = INFO_HUBS_EARNINGS;
            break;
          case constantUtil.REDEEMREWARDPOINT:
            transactionType = TRAN_REDEEM_REWARD_POINT;
            break;
          case constantUtil.CONST_INCENTIVE:
            transactionType = INFO_INCENTIVE_CREDIT;
            break;
          case constantUtil.PENALITY:
            transactionType = RIDE_CANCELLATION_CHARGE;
            break;
          default:
            transactionType = data.transactionType;
            break;
        }
        //
        switch (data?.paymentGateWay || constantUtil.NONE) {
          case constantUtil.WALLET:
            paymentOption = constantUtil.WALLET;
            break;
          case constantUtil.NONE:
            paymentOption = constantUtil.CASH;
            break;
          default:
            paymentOption = constantUtil.CARD;
            break;
        }
        //
        return {
          "_id": data._id,
          "transactionType": transactionType,
          "transactionAmount": data.transactionAmount,
          "transactionId": data.transactionId,
          "accountsDetail": data.accountsDetail,
          "transactionDate": data.transactionDate,
          "transactionStatus": data.transactionStatus,
          "transactionReason": data.transactionReason,
          "refBookingId": data.refBookingId,
          "paymentOption": paymentOption,
          "paymentType": data.paymentType,
          "isReimbursement": data.isReimbursement,
          "previousBalance":
            data.previousBalance == null
              ? ""
              : parseFloat(data.previousBalance).toFixed(2),
          "currentBalance":
            data.currentBalance == null ? "" : parseFloat(data.currentBalance),
          "to": data.to,
          "from": data.from,
        };
      }) || [];
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

//
transactionAction.redirectPayment = async function (context) {
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const {
    INFO_RESPONSE_NOT_FOUND,
    SOMETHING_WENT_WRONG,
    INFO_TRANSACTION_NOT_FOUND,
    ERROR_CARD_ADD_FAILED,
    INFO_CARD_ADDED,
    ERROR_CARD_ADD,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  if (!context.params.response) {
    throw {
      "code": 400,
      "message": INFO_RESPONSE_NOT_FOUND,
    };
  }
  if (typeof context.params.response === "string") {
    context.params.response = JSON.parse(context.params.response);
  }
  console.log(context.params.response);

  const transaction = await this.adapter.model.findOne({
    "_id": Mongoose.Types.ObjectId(context.params.transaction.toString()),
  });

  if (!transaction) {
    throw new MoleculerError(
      SOMETHING_WENT_WRONG + "/" + INFO_TRANSACTION_NOT_FOUND,
      500
    );
  }
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let paymentData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === transaction.gatewayName) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtil.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  }
  if (paymentData.mode === constantUtil.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }

  if (transaction.gatewayName === constantUtil.FLUTTERWAVE) {
    if (transaction.transactionType === constantUtil.VERIFYCARD) {
      if (
        context.params.response.status === "failed" ||
        context.params.response.status === "error"
      ) {
        const verify = await flutterwave.chargeVerify({
          "txref":
            context.params.response.txRef || context.params.response.txref,
          "SECKEY": secretKey,
        });

        const transactionUpdate = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(
              context.params.transaction.toString()
            ),
          },
          {
            "$set": {
              "transactionStatus": constantUtil.FAILED,
              "gatewayResponse":
                verify.status === "success"
                  ? verify.data
                  : context.params.response,
            },
          }
        );
        if (!transactionUpdate.nModified) {
          throw new MoleculerError(INFO_TRANSACTION_NOT_FOUND, 500);
        }
        return {
          "code": 400,
          "message":
            verify?.data?.vbvmessage ?? context.params.response.vbvrespmessage,
          "data": {},
        };
      }

      // verify
      if (context.params.response.authModelUsed === "VBVSECURECODE") {
        if (context.params.response.chargeResponseCode == "00") {
          const verify = await flutterwave.chargeVerify({
            "txref":
              context.params.response.txRef || context.params.response.txref,
            "SECKEY": secretKey,
          });
          if (verify.status === "success") {
            // success or not want save data on db
            const transactionUpdate = await this.adapter.model.updateOne(
              {
                "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
              },
              { "gatewayResponse": verify.data }
            );
            if (!transactionUpdate.nModified) {
              throw new MoleculerError(INFO_TRANSACTION_NOT_FOUND, 500);
            }
            if (verify.data.status === "successful") {
              if (transaction.transactionType === constantUtil.VERIFYCARD) {
                // refund
                if (paymentData.version === constantUtil.CONST_V3) {
                  this.broker.emit("transaction.flutterwave.refund", {
                    "transactionId": transaction._id.toString(),
                    "amount": transaction.transactionAmount,
                  });
                } else {
                  this.broker.emit("transaction.flutterwave.refund", {
                    "transactionId": transaction._id.toString(),
                    "amount": transaction.transactionAmount,
                  });
                }
                // need to set code
                //
                // then add card to user
                if (transaction.from.userType === constantUtil.PROFESSIONAL) {
                  let cardAdded = await this.broker.emit(
                    "professional.addVerifiedCard",
                    {
                      "card": {
                        ...transaction.cardDetailes,
                        "embedtoken":
                          verify.data.card.card_tokens[0].embedtoken,
                        "type": verify.data.card.type,
                      },
                      "professionalId": transaction.from.userId.toString(),
                    }
                  );
                  cardAdded = cardAdded && cardAdded[0];
                  if (!cardAdded) {
                    throw new MoleculerError(ERROR_CARD_ADD_FAILED, 500);
                  }
                  if (cardAdded.code === 200) {
                    return {
                      "code": 200,
                      "message": INFO_CARD_ADDED,
                      "data": cardAdded.data,
                    };
                  }
                  if (cardAdded.code > 200) {
                    return {
                      "code": 200,
                      "message": ERROR_CARD_ADD,
                      "DATA": {},
                    };
                  }
                }
                if (transaction.from.userType === constantUtil.USER) {
                  let cardAdded = await this.broker.emit(
                    "user.addVerifiedCard",
                    {
                      "card": {
                        ...transaction.cardDetailes,
                        "embedtoken":
                          verify.data.card.card_tokens[0].embedtoken,
                        "type": verify.data.card.type,
                      },
                      "userId": transaction.from.userId.toString(),
                    }
                  );
                  cardAdded = cardAdded && cardAdded[0];
                  if (!cardAdded) {
                    throw new MoleculerError(ERROR_CARD_ADD_FAILED, 500);
                  }
                  if (cardAdded.code === 200) {
                    return {
                      "code": 200,
                      "message": INFO_CARD_ADDED,
                      "data": cardAdded.data,
                    };
                  }
                  if (cardAdded.code > 200) {
                    return {
                      "code": 200,
                      "message": ERROR_CARD_ADD,
                      "DATA": {},
                    };
                  }
                }
              }
            }
            return {
              "code": 200,
              "message": verify.data.status,
              "data": verify.data,
            };
          }
          return verify;
        }
      }
    }
  }
};

transactionAction.verifyBankAccount = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let languageCode;
  //// ***** Need to remove context.meta.professionalId & context.meta.userId After Dec 2024
  if (
    context.meta.professionalId ||
    context.params.userType === constantUtil.PROFESSIONAL
  ) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (
    context.meta.userId ||
    context.params.userType === constantUtil.USER
  ) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const {
    SOMETHING_WENT_WRONG,
    INFO_NOT_FOUND,
    INFO_BANK_DETAILS,
    INFO_BANK_DETAILS_NOT_FOUND,
    INFO_ACCOUNT_HOLDER_NAME,
    INFO_BANK_NAME,
    INFO_DOCUMENT_NUMBER,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  let paymentData = {};
  let pamentGatwayResponse = {};
  let userData = {};
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  // GET REQUESTER DATA
  if (context.meta.userId || context.params.userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(), //// ***** Need to replace context.meta.userId to context.params.userId  After Dec 2024
    });
  } else if (
    context.meta.professionalId ||
    context.params.userType === constantUtil.PROFESSIONAL
  ) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(), //// ***** Need to replace context.meta.professionalId to context.params.professionalId  After Dec 2024
    });
  } //// ***** Need to remove context.meta.professionalId & context.meta.userId After Dec 2024
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(INFO_NOT_FOUND, 500);
  }
  let reformattedArray = context.params.bankDetails.map(
    ({ key, value, title }) => ({
      "title": title,
      "value": value,
      "key": key,
      "isRestricted": false,
    })
  );
  // console.log(reformattedArray);
  switch (generalSettings.data.payoutOptions) {
    case constantUtil.MANUAL:
    case constantUtil.DISPLAYINFO:
      break;

    case constantUtil.AUTOMATED:
      {
        const accountholderName = context.params.bankDetails.find(
          (details) => details.key === "accountholderName"
        )?.value;
        const accountNumber = context.params.bankDetails.find(
          (details) => details.key === "accountNumber"
        )?.value;
        //----------------------
        if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
          // needed data
          const bankData = paymentData.bankList.find(
            (data) =>
              data.name.toUpperCase() ===
              context.params.bankDetails
                .find((details) => details.key === "bankName")
                .value.toUpperCase()
          );
          //  validation
          if (!accountNumber || !bankData) {
            throw new ValidationError(
              `${
                !accountNumber
                  ? "accountNumber is required"
                  : "we cant find bankData"
              } `,
              null,
              context.params.bankDetails
            );
          }
          //
          if (paymentData.version === constantUtil.CONST_V3) {
            pamentGatwayResponse = await this.broker.emit(
              "transaction.flutterwave.verifyBankAccount",
              {
                "bankName": bankData.name,
                "bankCode": bankData.code,
                "accountNumber": accountNumber,
              }
            );
          } else {
            pamentGatwayResponse = await this.broker.emit(
              "transaction.flutterwave.verifyBankAccount",
              {
                "bankName": bankData.name,
                "bankCode": bankData.code,
                "accountNumber": accountNumber,
              }
            );
          }
          pamentGatwayResponse =
            pamentGatwayResponse && pamentGatwayResponse[0];
          return pamentGatwayResponse;
        }
        switch (paymentData.gateWay) {
          case constantUtil.STRIPE:
            {
              const routingNumber = context.params.bankDetails.find(
                (details) => details.key === "routingNumber"
              )?.value;
              const zipCode = context.params.bankDetails.find(
                (details) => details.key === "postalCode"
              )?.value;
              //  validation
              if (!accountNumber || !routingNumber) {
                throw new ValidationError(
                  `${
                    !accountNumber
                      ? "Account Number is required"
                      : "Routing Number is required"
                  } `,
                  null,
                  context.params.bankDetails
                );
              }
              const bankDetails = {
                "accountName": accountholderName,
                "accountNumber": accountNumber,
                "routingNumber": routingNumber,
                "zipCode": zipCode,
                // 'stripeCustomerId': context.meta.professionalDetails.stripeCustomerId,
              };
              console.log(bankDetails);
              // //----------- InComplete --------------
              pamentGatwayResponse = await this.broker.emit(
                "transaction.stripe.verifyBankAccount",
                {
                  ...context.params,
                  "paymentData": paymentData,
                  "bankDetails": bankDetails,
                  "userData": userData,
                  "generalSettings": generalSettings,
                  "clientIp": context.meta?.clientIp,
                }
              );
              pamentGatwayResponse =
                pamentGatwayResponse && pamentGatwayResponse[0];
              if (pamentGatwayResponse?.code === 200) {
                // embedtoken
                reformattedArray.push({
                  "title": "Embed Token",
                  "value": pamentGatwayResponse.data.embedtoken,
                  "key": "embedtoken",
                  "isRestricted": true,
                });
                // bankAccountId
                reformattedArray.push({
                  "title": "Bank Account Id",
                  "value": pamentGatwayResponse.data.bankAccountId,
                  "key": "bankAccountId",
                  "isRestricted": true,
                });
                // stripeConnectAccountId
                reformattedArray.push({
                  "title": "Stripe Account Id",
                  "value": pamentGatwayResponse.data.stripeConnectAccountId,
                  "key": "stripeAccountId",
                  "isRestricted": true,
                });
              } else {
                throw new MoleculerError(INFO_BANK_DETAILS_NOT_FOUND, 500);
              }
              // //----------- InComplete --------------
            }
            break;

          case constantUtil.CONST_MERCADOPAGO:
            {
              const pixkeyNumber = context.params.bankDetails
                .find((details) => details.key === "pixkeyNumber")
                ?.value?.toString();
              //  validation
              if (!pixkeyNumber) {
                throw new ValidationError(
                  `${"Pixkey is required"} `,
                  null,
                  context.params.bankDetails
                );
              }
              const bankDetails = {
                // "accountName": accountholderName,
                "accountNumber": accountNumber,
                "pixkeyNumber": pixkeyNumber,
              };
              pamentGatwayResponse = await this.broker.emit(
                "transaction.mercadopago.verifyBankAccount",
                {
                  ...context.params,
                  "paymentData": paymentData,
                  "bankDetails": bankDetails,
                  "userData": userData,
                  "generalSettings": generalSettings,
                  "clientIp": context.meta?.clientIp,
                }
              );
              pamentGatwayResponse =
                pamentGatwayResponse && pamentGatwayResponse[0];
              if (pamentGatwayResponse?.code === 200) {
                // accountholderName
                reformattedArray.push({
                  "title": INFO_ACCOUNT_HOLDER_NAME, //"Account Holder Name",
                  "value": pamentGatwayResponse.data.accountholderName,
                  "key": "accountHolderName",
                  "isRestricted": false,
                });
                // bankName
                reformattedArray.push({
                  "title": INFO_BANK_NAME, //"Bank Name",
                  "value": pamentGatwayResponse.data.bankName,
                  "key": "bankName",
                  "isRestricted": false,
                });
                // ispb
                reformattedArray.push({
                  "title": "ISPB",
                  "value": pamentGatwayResponse.data.ispb,
                  "key": "ispb",
                  "isRestricted": false,
                });
                // bankAccountDocumentNo
                reformattedArray.push({
                  "title": INFO_DOCUMENT_NUMBER, //"Document Number",
                  "value": pamentGatwayResponse.data.bankAccountDocumentNo,
                  "key": "bankAccountDocumentNo",
                  "isRestricted": false,
                });
              } else {
                throw new MoleculerError(
                  pamentGatwayResponse.message || INFO_BANK_DETAILS_NOT_FOUND,
                  500
                );
              }
            }
            break;
        }
      }
      break;
  }
  return {
    "code": 200,
    "message": INFO_BANK_DETAILS,
    "data": reformattedArray,
  };
};

transactionAction.validateCharge = async function (context) {
  let languageCode;
  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const {
    INFO_TRANSACTION_NOT_FOUND,
    SOMETHING_WENT_WRONG,
    ERROR_CARD_ADD,
    INFO_CARD_ADDED,
    INFO_CARD_VERIFIED,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  let transaction = await this.adapter.model
    .findOne({
      "_id": Mongoose.Types.ObjectId(context.params.transactionId),
    })
    .lean();
  if (!transaction) {
    throw new MoleculerError(INFO_TRANSACTION_NOT_FOUND, 500);
  }
  // transaction = transaction.toJSON();
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) {
    throw new MoleculerError(
      "CANT FIND PAYMENT OPTIONS LIST FROM DB REDIS PROBLEM",
      500
    );
  }
  let paymentData = {};
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === transaction.gatewayName) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  let publicKey;
  let secretKey;
  if (paymentData.mode === constantUtil.LIVE) {
    publicKey = paymentData.livePublicKey;
    secretKey = paymentData.liveSecretKey;
  }
  if (paymentData.mode === constantUtil.SANDBOX) {
    publicKey = paymentData.testPublicKey;
    secretKey = paymentData.testSecretKey;
  }
  if (context.params.validateType === constantUtil.PIN) {
    const payload = {
      ...context.params.payload,
      "pin": context.params.pin,
      "suggested_auth": "PIN",
    };
    const charge = await flutterwave.charge({
      "payload": payload,
      "secretkey": secretKey,
    });
    if (charge.status === "success") {
      const updatedData = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "gatewayResponse": charge.data,
        }
      );
      if (!updatedData.nModified) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      if (charge.data.chargeResponseCode === "02") {
        return {
          "code": 200,
          "message": charge.data.chargeResponseMessage,
          "data": {
            "authType": "OTP",
            "transactionId": transaction._id,
          },
        };
      } else {
        throw new MoleculerClientError(
          charge.message,
          400,
          SOMETHING_WENT_WRONG,
          charge.data
        );
      }
    } else {
      throw new MoleculerClientError(
        charge.message,
        400,
        SOMETHING_WENT_WRONG,
        charge?.data || {}
      );
    }
  }
  if (context.params.validateType === constantUtil.OTP) {
    const validate = await flutterwave.validateCharge({
      "transaction_reference": transaction.gatewayResponse.flwRef,
      "PBFPubKey": publicKey,
      "otp": context.params.otp,
    });
    console.log("this is validate", validate);
    if (validate.status === "success") {
      const verify = await flutterwave.chargeVerify({
        "txref": validate.data.tx.txRef || validate.data.tx.txref,
        "SECKEY": secretKey,
      });
      if (verify.status === "success") {
        // success or not want save data on db
        const transactionUpdate = await this.adapter.model.updateOne(
          {
            "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
          },
          { "gatewayResponse": verify.data }
        );
        if (!transactionUpdate.nModified) {
          throw new MoleculerError(INFO_TRANSACTION_NOT_FOUND, 500);
        }
        if (verify.data.status === "successful") {
          if (transaction.transactionType === constantUtil.VERIFYCARD) {
            // refund
            if (paymentData.version === constantUtil.CONST_V3) {
              this.broker.emit("transaction.flutterwave.refundV3", {
                "transactionId": transaction._id.toString(),
                "amount": transaction.transactionAmount,
              });
            } else {
              this.broker.emit("transaction.flutterwave.refund", {
                "transactionId": transaction._id.toString(),
                "amount": transaction.transactionAmount,
              });
            }
            // need to set code
            //
            // then add card to user
            if (transaction.from.userType === constantUtil.PROFESSIONAL) {
              let cardAdded = await this.broker.emit(
                "professional.addVerifiedCard",
                {
                  "card": {
                    ...transaction.cardDetailes,
                    "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                    "type": verify.data.card.type,
                  },
                  "professionalId": transaction.from.userId.toString(),
                }
              );
              cardAdded = cardAdded && cardAdded[0];
              if (cardAdded?.code === 200) {
                return {
                  "code": 200,
                  "message": INFO_CARD_ADDED,
                  "data": { "authType": constantUtil.NOAUTH },
                };
              } else {
                throw new MoleculerError(
                  SOMETHING_WENT_WRONG +
                    "/" +
                    ERROR_CARD_ADD +
                    " TO PROFESSIONAL",
                  500
                );
              }
            }
            if (transaction.from.userType === constantUtil.USER) {
              let cardAdded = await this.broker.emit("user.addVerifiedCard", {
                "card": {
                  ...transaction.cardDetailes,
                  "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                  "type": verify.data.card.type,
                },
                "userId": transaction.from.userId.toString(),
              });
              cardAdded = cardAdded && cardAdded[0];
              if (cardAdded?.code === 200) {
                return {
                  "code": 200,
                  "message": INFO_CARD_ADDED,
                  "data": { "authType": constantUtil.NOAUTH },
                };
              } else {
                throw new MoleculerError(
                  SOMETHING_WENT_WRONG + "/" + ERROR_CARD_ADD + " TO USER",
                  500
                );
              }
            }
          }
        } else {
          throw new MoleculerClientError(
            verify?.data?.vbvmessage ?? verify.data?.status,
            400,
            SOMETHING_WENT_WRONG,
            verify.data
          );
        }
      } else {
        throw new MoleculerClientError(
          verify.message,
          400,
          SOMETHING_WENT_WRONG,
          verify.data
        );
      }
    } else {
      throw new MoleculerClientError(
        validate.message || validate.data,
        400,
        SOMETHING_WENT_WRONG,
        validate.data
      );
    }
  }
  // address verification card
  if (context.params.validateType === constantUtil.ADDRESS) {
    if (!context.params.address) {
      throw new ValidationError("ADDRESS IS NOT FOUND");
    }
    const payload = {
      ...context.params.payload,
      ...context.params.address,
    };
    const charge = await flutterwave.charge({
      "payload": payload,
      "secretkey": secretKey,
    });
    if (charge.status === "success") {
      const updatedData = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "gatewayResponse": charge.data,
        }
      );
      if (!updatedData.nModified) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      if (
        charge.data?.authModelUsed === "VBVSECURECODE" ||
        (charge.data?.authModelUsed === "NOAUTH" &&
          charge?.data?.authurl !== "N/A")
      ) {
        return {
          "code": 200,
          "message": charge.message,
          "data": {
            "authType": "REDIRECT",
            "authurl": charge.data.authurl,
            "redirectUrl": charge.data.redirectUrl,
            "transactionId": transaction._id.toString(),
          },
        };
      }
      if (
        charge.data.authModelUsed === "NOAUTH" &&
        charge.data?.authurl === "N/A"
      ) {
        //  verify
        const verify = await flutterwave.chargeVerify({
          "txref": charge.data.txRef || charge.data.txref,
          "SECKEY": secretKey,
        });
        // manage status
        if (verify.status === "success") {
          // success or not want save data on db
          const transactionUpdate = await this.adapter.model.updateOne(
            {
              "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            },
            { "gatewayResponse": verify.data }
          );
          if (!transactionUpdate.nModified) {
            throw new MoleculerError(INFO_TRANSACTION_NOT_FOUND, 500);
          }
          if (verify.data.status === "successful") {
            // refund

            // this.broker.emit("transaction.flutterwave.refund", {
            //   "transactionId": transaction._id.toString(),
            //   "amount": transaction.transactionAmount,
            // });

            if (transaction.transactionType === constantUtil.VERIFYCARD) {
              // refund
              if (paymentData.version === constantUtil.CONST_V3) {
                this.broker.emit("transaction.flutterwave.refundV3", {
                  "transactionId": transaction._id.toString(),
                  "amount": transaction.transactionAmount,
                });
              } else {
                this.broker.emit("transaction.flutterwave.refund", {
                  "transactionId": transaction._id.toString(),
                  "amount": transaction.transactionAmount,
                });
              }
              // need to set code
              //
              // then add card to user
              if (transaction.from.userType === constantUtil.PROFESSIONAL) {
                let cardAdded = await this.broker.emit(
                  "professional.addVerifiedCard",
                  {
                    "card": {
                      ...transaction.cardDetailes,
                      "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                      "type": verify.data.card.type,
                    },
                    "professionalId": transaction.from.userId.toString(),
                  }
                );
                cardAdded = cardAdded && cardAdded[0];
                if (cardAdded?.code === 200) {
                  return {
                    "code": 200,
                    "message": INFO_CARD_ADDED,
                    "data": { "authType": constantUtil.NOAUTH },
                  };
                } else {
                  throw new MoleculerError(
                    SOMETHING_WENT_WRONG +
                      "/" +
                      ERROR_CARD_ADD +
                      " TO PROFESSIONAL",
                    500
                  );
                }
              }
              if (transaction.from.userType === constantUtil.USER) {
                let cardAdded = await this.broker.emit("user.addVerifiedCard", {
                  "card": {
                    ...transaction.cardDetailes,
                    "embedtoken": verify.data.card.card_tokens[0].embedtoken,
                    "type": verify.data.card.type,
                  },
                  "userId": transaction.from.userId.toString(),
                });
                cardAdded = cardAdded && cardAdded[0];
                if (cardAdded?.code === 200) {
                  return {
                    "code": 200,
                    "message": INFO_CARD_ADDED,
                    "data": { "authType": constantUtil.NOAUTH },
                  };
                } else {
                  throw new MoleculerError(
                    SOMETHING_WENT_WRONG + "/" + ERROR_CARD_ADD + " TO USER",
                    500
                  );
                }
              }
            }
            // return save card
            return {
              "code": 200,
              "message": INFO_CARD_VERIFIED,
              "data": {
                "authType": constantUtil.NOAUTH,
              },
            };
          }
        } else {
          throw new MoleculerClientError(
            verify.message,
            400,
            SOMETHING_WENT_WRONG,
            verify.data
          );
        }
      } else {
        throw new MoleculerClientError(
          charge.message,
          400,
          SOMETHING_WENT_WRONG,
          charge.data
        );
      }
    } else {
      throw new MoleculerClientError(
        charge.message || charge.data.message,
        400,
        SOMETHING_WENT_WRONG,
        charge?.data || {}
      );
    }
  } else {
    throw new MoleculerClientError(
      SOMETHING_WENT_WRONG + "/PAYMENTGATWAY",
      400,
      SOMETHING_WENT_WRONG,
      context.params
    );
  }
};

//#region webhook

transactionAction.webhook = async function (context) {
  // //------------
  // const config = {
  //   "method": "get",
  //   "url": "https://webhook.site/2547467a-3013-4e6d-9688-fba2bd212bcc",
  //   "headers": {
  //     "Content-Type": "application/json",
  //   },
  //   "data": context.params,
  // };
  // try {
  //   await axios(config);
  // } catch (error) {
  //   config["data"] = error;
  //   await axios(config);
  // }
  // //-------------------
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseData = {},
    paymentData = {};
  const { INFO_PAYMENT_OPTIONS, SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  console.log("webhooking : " + JSON.stringify(context.params));
  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    this.broker.emit("log.manageLog", {
      "name": "ERRORDATA",
      "userType": "USER",
      "reason": paymentData.gateWay.toUpperCase(),
      "trackData": JSON.stringify(context.params),
    });
    switch (paymentData.gateWay.toUpperCase()) {
      case constantUtil.FLUTTERWAVE:
        if (paymentData.version === constantUtil.CONST_V3) {
          this.broker.emit(
            "transaction.flutterwave.webhookingV3",
            context.params
          );
        } else {
          this.broker.emit(
            "transaction.flutterwave.webhooking",
            context.params
          );
        }
        break;

      case constantUtil.STRIPE:
        if (paymentData.isEnablePixkeyPayment) {
          break;
          // await this.broker.emit(
          //   "transaction.stripe.webhookingPixKeyPayment",
          //   context.params
          // );
        } else {
          await this.broker.emit(
            "transaction.stripe.webhooking",
            context.params
          );
        }
        break;

      case CONST_TIGO:
        responseData = await this.broker.emit("transaction.tigoWebhooking", {
          ...context.params,
        });
        break;

      case CONST_CYBERSOURCE:
        {
          // if (context.params.decision === "ACCEPT") {
          const utf8Key = context.params["utf8"] || null;
          if (context.params.req_reference_number && utf8Key === null) {
            const userType =
              (
                context.params.req_reference_number.split("/")[0] || ""
              ).toUpperCase() === constantUtil.USER
                ? constantUtil.USER
                : constantUtil.PROFESSIONAL;
            const userId = context.params.req_reference_number.split("/")[1];
            const professionalId =
              context.params.req_reference_number.split("/")[1];
            const transactionId =
              context.params.req_reference_number.split("/")[2];
            responseData = await this.broker.emit(
              "transaction.cyberSource.webhooking",
              {
                ...context.params,
                "userType": userType,
                "userId": userId,
                "professionalId": professionalId,
                "transactionAmount": parseFloat(context.params.req_amount || 0),
                "transactionStatus": context.params.decision,
                "transactionId": transactionId,
                "gatewayResponse": context.params,
              }
            );
          }
          // } else {
          //   responseData = [];
          // }
        }
        break;
    }
    responseData = responseData && responseData[0];
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData?.data || {},
  };
};

transactionAction.webhookingStripePixKeyPayment = async function (context) {
  console.log("params_" + JSON.stringify(context.params));
  console.log("--------------------------------------");
  console.log("Data_" + JSON.stringify(context.params?.data));

  let availableAmount = 0,
    transactionAmount = 0,
    transactionStatus = {},
    updatedtransaction = {},
    notificationObject = {},
    userData = {},
    walletupdated = {},
    languageCode,
    notificationMessage = " ";

  const eventType = context.params.type;
  const transactionId = context.params.data?.object?.id;
  console.log(transactionId + "_" + eventType);

  const transaction = await this.adapter.model.findOneAndUpdate(
    { "transactionId": transactionId },
    { "webhookresponse": context.params, "$inc": { "webhookHitCount": 1 } },
    { "new": true }
  );
  const userType = (transaction.from.userType || "").toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  console.log("--------------------------------------");
  console.log("Data_" + JSON.stringify(transaction));
  if (userType === constantUtil.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": transaction.from.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("SOMETHING WENT WRONG", 500);
    }
    languageCode = userData.languageCode;
  } else if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": transaction.from.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("SOMETHING WENT WRONG", 500);
    }
    languageCode = userData.languageCode;
  }

  const {
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    WALLET_AMOUNT_LOADED,
    WALLET_NOTIFICATION,
    SOMETHING_WENT_WRONG,
    ERROR_WALLET_RECHARGE,
    INFO_WALLET_RECHARGE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);

  // Handle the event
  switch (eventType) {
    case "checkout.session.async_payment_failed":
      transactionStatus = constantUtil.FAILED;
      break;

    case "checkout.session.async_payment_succeeded":
      transactionStatus = constantUtil.SUCCESS;
      break;

    case "checkout.session.completed":
      transactionStatus = constantUtil.PENDING;
      break;

    case "checkout.session.expired":
      transactionStatus = constantUtil.FAILED;
      break;
  }

  updatedtransaction = await this.adapter.model.findOneAndUpdate(
    {
      "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
    },
    {
      "transactionStatus": transactionStatus,
      "$inc": { "webhookHitCount": 1 },
    },
    { "new": true }
  );
  if (transaction?.webhookHitCount > 1) {
    throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
  } else if (transactionStatus === constantUtil.SUCCESS) {
    //-------------------------------------
    // reterive the amount to whose is the user or professional
    if (transaction.transactionType === constantUtil.WALLETRECHARGE) {
      availableAmount =
        userData.wallet.availableAmount + transaction.transactionAmount;
      transactionAmount = transaction.transactionAmount;
      notificationMessage = WALLET_AMOUNT_LOADED;
      // retrn the amount of professional
      if (userType === constantUtil.PROFESSIONAL) {
        walletupdated = await this.broker.emit(
          "professional.updateWalletAmount",
          {
            "professionalId": userData._id.toString(),
            "availableAmount": availableAmount,
            "freezedAmount": userData.wallet.freezedAmount,
          }
        );
      } else if (userType === constantUtil.USER) {
        walletupdated = await this.broker.emit("user.updateWalletAmount", {
          "userId": userData._id.toString(),
          "availableAmount": availableAmount,
          "freezedAmount": userData.wallet.freezedAmount,
          "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
        });
      }
      walletupdated = walletupdated && walletupdated[0];
      if (walletupdated.code !== 200) {
        throw new MoleculerError(ERROR_WALLET_RECHARGE, 500);
        // updatedtransaction = await this.adapter.model.findOneAndUpdate(
        //   {
        //     "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        //   },
        //   {
        //     "transactionStatus": constantUtil.FAILED,
        //   },
        //   { "new": true }
        // );
      }
    }
  } else if (transactionStatus === constantUtil.FAILED) {
    availableAmount = userData.wallet.availableAmount;
    notificationMessage = WALLET_NOTIFICATION;
  }

  notificationObject = {
    "clientId": clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_WALLETTRANSACTIONUPDATE,
      "timestamp": Date.now(),
      "message": notificationMessage,
      "details": lzStringEncode({
        "walletAmount": availableAmount,
        // "transactionAmount": transactionAmount,
        "paymentType": updatedtransaction.paymentType,
        "transactionStatus": updatedtransaction.transactionStatus,
        "_id": updatedtransaction._id,
      }),
    },
    "registrationTokens": [
      {
        "token": userData.deviceInfo[0].deviceId,
        "id": userData._id.toString(),
        "deviceType": userData.deviceInfo[0].deviceType,
        "platform": userData.deviceInfo[0].platform,
        "socketId": userData.deviceInfo[0].socketId,
      },
    ],
  };
  console.log(notificationObject);

  if (
    transactionStatus === constantUtil.SUCCESS ||
    transactionStatus === constantUtil.FAILED
  ) {
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  return {
    "code": 200,
    "status": transactionStatus,
    "userType": userType,
    "userId": userData._id.toString(),
    "id": updatedtransaction._id,
    "transactionId": transactionId,
    "eventType": eventType,
  };
};

transactionAction.webhookingMercadopagoPixKeyPayment = async function (
  context
) {
  console.log("params_" + JSON.stringify(context.params));
  console.log("--------------------------------------");
  console.log("Data_" + JSON.stringify(context.params?.data));

  let availableAmount = 0,
    transactionAmount = 0,
    transactionStatus = null,
    updatedtransaction = {},
    notificationObject = {},
    userData = {},
    walletupdated = {},
    languageCode,
    notificationMessage = " ";

  const eventType = context.params.action;
  const transactionId = context.params.data?.id;
  console.log(transactionId + "_" + eventType);

  // Handle the event
  if (eventType === "payment.updated") {
    let paymentData = {},
      paymentResponseData = {};
    //
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    try {
      const paymentDataConfigParams = {
        "method": "GET",
        "port": 443,
        "url": `https://api.mercadopago.com/v1/payments/${transactionId}`,
        "headers": {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + paymentData.liveSecretKey,
        },
      };
      paymentResponseData = await axios(paymentDataConfigParams);
      if (paymentResponseData.data.status === "approved") {
        transactionStatus = constantUtil.SUCCESS;
      } else {
        transactionStatus = constantUtil.FAILED;
      }
    } catch (error) {}
    //
    if (transactionStatus) {
      const transaction = await this.adapter.model.findOneAndUpdate(
        { "transactionId": transactionId },
        {
          "transactionStatus": transactionStatus,
          "gatewayResponse": paymentResponseData.data,
          "webhookresponse": context.params,
          "$inc": { "webhookHitCount": 1 },
        },
        { "new": true }
      );
      const userType = (transaction.from.userType || "").toUpperCase();
      console.log("--------------------------------------");
      console.log("Data_" + JSON.stringify(transaction));
      if (userType === constantUtil.PROFESSIONAL) {
        userData = await this.broker.emit("professional.getById", {
          "id": transaction.from.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          throw new MoleculerError("Invalid Professional Details", 500);
        }
        languageCode = userData.languageCode;
      } else if (userType === constantUtil.USER) {
        userData = await this.broker.emit("user.getById", {
          "id": transaction.from.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          throw new MoleculerError("Invalid User Details", 500);
        }
        languageCode = userData.languageCode;
      }
      const {
        USER_NOT_FOUND,
        PROFESSIONAL_NOT_FOUND,
        WALLET_AMOUNT_LOADED,
        WALLET_NOTIFICATION,
        SOMETHING_WENT_WRONG,
        ERROR_WALLET_RECHARGE,
        INFO_WALLET_RECHARGE,
      } = notifyMessage.setNotifyLanguage(
        context.params.langCode || languageCode
      );
      if (transaction?.webhookHitCount > 1) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      } else if (transactionStatus === constantUtil.SUCCESS) {
        //-------------------------------------
        // reterive the amount to whose is the user or professional
        if (transaction.transactionType === constantUtil.WALLETRECHARGE) {
          availableAmount =
            userData.wallet.availableAmount + transaction.transactionAmount;
          transactionAmount = transaction.transactionAmount;
          notificationMessage = WALLET_AMOUNT_LOADED;
          // retrn the amount of professional
          if (userType === constantUtil.PROFESSIONAL) {
            walletupdated = await this.broker.emit(
              "professional.updateWalletAmount",
              {
                "professionalId": userData._id.toString(),
                "availableAmount": availableAmount,
                "freezedAmount": userData.wallet.freezedAmount,
              }
            );
          } else if (userType === constantUtil.USER) {
            walletupdated = await this.broker.emit("user.updateWalletAmount", {
              "userId": userData._id.toString(),
              "availableAmount": availableAmount,
              "freezedAmount": userData.wallet.freezedAmount,
              "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
            });
          }
          walletupdated = walletupdated && walletupdated[0];
          if (walletupdated.code !== 200) {
            throw new MoleculerError(ERROR_WALLET_RECHARGE, 500);
            // updatedtransaction = await this.adapter.model.findOneAndUpdate(
            //   {
            //     "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
            //   },
            //   {
            //     "transactionStatus": constantUtil.FAILED,
            //   },
            //   { "new": true }
            // );
          }
        }
      } else if (transactionStatus === constantUtil.FAILED) {
        availableAmount = userData.wallet.availableAmount;
        notificationMessage = WALLET_NOTIFICATION;
      }
      notificationObject = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": userType,
          "action": constantUtil.ACTION_WALLETTRANSACTIONUPDATE,
          "timestamp": Date.now(),
          "message": notificationMessage,
          "details": lzStringEncode({
            "walletAmount": availableAmount,
            // "transactionAmount": transactionAmount,
            "paymentType": updatedtransaction.paymentType,
            "transactionStatus": updatedtransaction.transactionStatus,
            "_id": updatedtransaction._id,
          }),
        },
        "registrationTokens": [
          {
            "token": userData.deviceInfo[0].deviceId,
            "id": userData._id.toString(),
            "deviceType": userData.deviceInfo[0].deviceType,
            "platform": userData.deviceInfo[0].platform,
            "socketId": userData.deviceInfo[0].socketId,
          },
        ],
      };
      console.log(notificationObject);

      if (
        transactionStatus === constantUtil.SUCCESS ||
        transactionStatus === constantUtil.FAILED
      ) {
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
      return {
        "code": 200,
        "status": transactionStatus,
        "userType": userType,
        "userId": userData._id.toString(),
        "id": updatedtransaction._id,
        "transactionId": transactionId,
        "eventType": eventType,
      };
    }
  }
};

transactionAction.webhookingDelbankPixKeyPayout = async function (context) {
  console.log("params_" + JSON.stringify(context.params));
  console.log("--------------------------------------");
  console.log("Data_" + JSON.stringify(context.params?.data));

  let availableAmount = 0,
    transactionAmount = 0,
    transactionStatus = null,
    updatedtransaction = {},
    notificationObject = {},
    userData = {},
    walletupdated = {},
    languageCode,
    notificationMessage = " ";

  const eventType = context.params.eventType;
  const transactionId = context.params.idempotencyKey;
  console.log(transactionId + "_" + eventType);

  // Handle the event
  if (
    eventType === "PIX_PAYMENT_EFFECTIVE" ||
    eventType === "PIX_PAYMENT_ERROR"
  ) {
    if (context.params.status === "PIX_EFFECTIVE") {
      transactionStatus = constantUtil.SUCCESS;
    } else {
      transactionStatus = constantUtil.FAILED;
    }
    //
    if (transactionStatus) {
      const transaction = await this.adapter.model
        .findOneAndUpdate(
          { "transactionId": transactionId },
          {
            "transactionStatus": transactionStatus,
            "webhookresponse": context.params,
            "$inc": { "webhookHitCount": 1 },
          },
          { "new": true }
        )
        .lean();
      const userType = (transaction.to.userType || "").toUpperCase();
      console.log("--------------------------------------");
      console.log("Data_" + JSON.stringify(transaction));
      if (userType === constantUtil.PROFESSIONAL) {
        userData = await this.broker.emit("professional.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          throw new MoleculerError("Invalid Professional Details", 500);
        }
        languageCode = userData.languageCode;
      } else if (userType === constantUtil.USER) {
        userData = await this.broker.emit("user.getById", {
          "id": transaction.to.userId.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          throw new MoleculerError("Invalid User Details", 500);
        }
        languageCode = userData.languageCode;
      }
      const {
        USER_NOT_FOUND,
        PROFESSIONAL_NOT_FOUND,
        WALLET_WITHDRAW_SUCCESS,
        SOMETHING_WENT_WRONG,
        ERROR_WALLET_WITHDRAW,
        ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET,
      } = notifyMessage.setNotifyLanguage(
        context.params.langCode || languageCode
      );
      if (transaction?.webhookHitCount > 1) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      } else if (transactionStatus === constantUtil.SUCCESS) {
        availableAmount = userData.wallet.availableAmount;
        notificationMessage = WALLET_WITHDRAW_SUCCESS;
      } else if (transactionStatus === constantUtil.FAILED) {
        // Withdraw Fail Return the Amount
        if (transaction.transactionType === constantUtil.WALLETWITHDRAWAL) {
          availableAmount =
            userData.wallet.availableAmount + transaction.transactionAmount;
          transactionAmount = transaction.transactionAmount;
          notificationMessage = ALERT_WALLET_WITHDRAW_FAILED_CREDIT_TO_WALLET;
          // return the amount of professional
          if (userType === constantUtil.PROFESSIONAL) {
            walletupdated = await this.broker.emit(
              "professional.updateWalletAmount",
              {
                "professionalId": userData._id.toString(),
                "availableAmount": availableAmount,
                "freezedAmount": userData.wallet.freezedAmount,
              }
            );
          } else if (userType === constantUtil.USER) {
            walletupdated = await this.broker.emit("user.updateWalletAmount", {
              "userId": userData._id.toString(),
              "availableAmount": availableAmount,
              "freezedAmount": userData.wallet.freezedAmount,
              "scheduleFreezedAmount": userData.wallet.scheduleFreezedAmount,
            });
          }
          walletupdated = walletupdated && walletupdated[0];
          if (walletupdated.code !== 200) {
            throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
          }
        }
      }
      notificationObject = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": userType,
          "action": constantUtil.ACTION_WALLETTRANSACTIONUPDATE,
          "timestamp": Date.now(),
          "message": notificationMessage,
          "details": lzStringEncode({
            "walletAmount": availableAmount,
            // "transactionAmount": transactionAmount,
            "paymentType": updatedtransaction.paymentType,
            "transactionStatus": updatedtransaction.transactionStatus,
            "_id": updatedtransaction._id,
          }),
        },
        "registrationTokens": [
          {
            "token": userData.deviceInfo[0].deviceId,
            "id": userData._id.toString(),
            "deviceType": userData.deviceInfo[0].deviceType,
            "platform": userData.deviceInfo[0].platform,
            "socketId": userData.deviceInfo[0].socketId,
          },
        ],
      };
      console.log(notificationObject);
      if (
        transactionStatus === constantUtil.SUCCESS ||
        transactionStatus === constantUtil.FAILED
      ) {
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
      return {
        "code": 200,
        "status": transactionStatus,
        "userType": userType,
        "userId": userData._id.toString(),
        "id": updatedtransaction._id,
        "transactionId": transactionId,
        "eventType": eventType,
      };
    }
  }
};
//#endregion webhook

transactionAction.refund = async function (context) {
  return await this.broker.emit(
    "transaction.flutterwave.refund",
    context.params
  );
};
// transaction bankDetails
transactionAction.getBankInputData = async function (context) {
  let errorCode = 200,
    message = null,
    paymentData = null,
    languageCode;

  if (context.meta.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.meta.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const {
    INFO_BANK_DETAILS_NOT_FOUND,
    INFO_BANK_DETAILS,
    SOMETHING_WENT_WRONG,
  } = await notifyMessage.setNotifyLanguage(languageCode);

  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    // let paymentData;
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    // if (!paymentData) {
    //   // return INFO_BANK_DETAILS_NOT_FOUND;
    //   throw new MoleculerError(INFO_BANK_DETAILS_NOT_FOUND, 500);
    // }
    // let bankInputDetails = await this.broker.emit("admin.getBankInputData", {
    //   "paymentType": constantUtil.PAYMENTGATEWAY,
    //   "paymentGatway": paymentData.gateWay,
    // });
    // bankInputDetails = bankInputDetails && bankInputDetails[0];
    // if (!bankInputDetails) {
    //   throw new MoleculerError(INFO_BANK_DETAILS_NOT_FOUND, 500);
    // } else {
    message = INFO_BANK_DETAILS;
    // }
    // return {
    //   "code": 200,
    //   "data": paymentData.bankDetails,
    //   "message": INFO_BANK_DETAILS,
    // };
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    paymentData = null;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": paymentData?.bankDetails || [],
  };
};

//#region  Payment Gatway

transactionAction.verifyCardByPaymentGatway = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    userData,
    minimumAmount,
    paymentData = {},
    gatewayResponse = {},
    languageCode;
  if (context.params.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.params.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const clientId = context.params.clientId || context.meta.clientId;
  const {
    PROFESSIONAL_NOT_FOUND,
    INFO_INVALID_DETAILS,
    INFO_PAYMENT_OPTIONS,
    SOMETHING_WENT_WRONG,
    INFO_CARD_ALREADY_EXIST,
    INFO_CARD,
    ERROR_CARD_ADD_FAILED,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    //#region User Details User/Professional
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.professionalId,
      });
      message = PROFESSIONAL_NOT_FOUND;
    } else if (context.params.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      message = PROFESSIONAL_NOT_FOUND;
    }
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(message, 500);
    }
    //#endregion User Details User/Professional
    const cardAlreadyExist = userData.cards.find(
      (card) => card.cardNumber === context.params.cardNumber.trim()
    );
    if (cardAlreadyExist?.isVerified === true) {
      return {
        "code": 400,
        "error": errorMessage,
        "message": INFO_CARD_ALREADY_EXIST,
        "data": gatewayResponse,
      };
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        // SOME TIME NOT FOUND SO WE NEED TO ADD
        if (!!paymentOptionList[prop]?.availableCurrencies === false) {
          throw new MoleculerError(
            "PLEASE ADD AVILABLECURRENCIES FILED IN  ACTIVE PARMANT GATEWAY",
            500
          );
        }
        minimumAmount = paymentOptionList[prop]?.availableCurrencies.find(
          (currency) =>
            currency.currencyCode === generalSettings.data.currencyCode
        );
        minimumAmount = minimumAmount?.minimumAmount; //value is object please console for better understand
        break;
      }
    }
    const stringTransactionReason = `card adding into ${generalSettings.data.siteTitle.toLowerCase()} ${new Date().toISOString()}/${
      context.params.userType
    }/${userData?._id?.toString()}`;

    const inputparamsData = {
      ...context.params,
      "paymentData": paymentData,
      "userData": userData,
      "generalSettings": generalSettings,
      "minimumChargeAmount": minimumAmount,
      //'transactionId': transaction._id.toString(),
      "transactionReason": stringTransactionReason,
    };
    switch (paymentData.gateWay) {
      case constantUtil.STRIPE:
        gatewayResponse = await this.broker.emit(
          "transaction.stripe.checkValidCard",
          { ...inputparamsData }
        );
        break;

      case constantUtil.RAZORPAY:
        gatewayResponse = await this.broker.emit(
          "transaction.razorpay.checkValidCard",
          { ...inputparamsData }
        );
        break;

      case constantUtil.CONST_ZCREDITAPIARY:
        gatewayResponse = await this.broker.emit(
          "transaction.zcreditapiary.checkValidCard",
          { ...inputparamsData }
        );
        break;

      case constantUtil.PEACH:
        gatewayResponse = await this.broker.emit(
          "transaction.peach.checkValidCard",
          { ...inputparamsData }
        );
        break;

      case constantUtil.CONST_MERCADOPAGO:
        gatewayResponse = await this.broker.emit(
          "transaction.mercadopago.checkValidCard",
          { ...inputparamsData }
        );
        break;

      case constantUtil.CONST_TEYA:
        gatewayResponse = await this.broker.emit(
          "transaction.teya.checkValidCard",
          { ...inputparamsData }
        );
        break;
    }
    gatewayResponse = gatewayResponse && gatewayResponse[0];
    if (!gatewayResponse) {
      throw new MoleculerError(INFO_CARD, 500);
    }
    if (gatewayResponse.code === 200) {
      // if (gatewayResponse.data.authType.toUpperCase() === "NOAUTH") {
      //   const transaction = await this.adapter.model.create({
      //     "paymentType": constantUtil.CREDIT,
      //     "paymentGateWay": paymentData.gateWay,
      //     "transactionType": constantUtil.VERIFYCARD,
      //     "gatewayDocId": paymentData._id,
      //     "transactionReason": stringTransactionReason,
      //     "from": {
      //       "phoneNumber": userData?.phone?.number,
      //       "userType": context.params.userType,
      //       "userId": userData._id.toString(),
      //     },
      //     "to": {
      //       "userType": constantUtil.ADMIN,
      //       "userId": paymentData.accountId,
      //     },
      //     "transactionStatus": constantUtil.PROCESSING,
      //     "cardDetailes": {
      //       "cardNumber": context.params.cardNumber,
      //       "expiryMonth": context.params.expiryMonth,
      //       "expiryYear": context.params.expiryYear,
      //       "holderName": context.params.holderName,
      //     },
      //     "gatewayResponse": gatewayResponse.data.successResponse,
      //     "stripeCustomerId": gatewayResponse.data.stripeCustomerId,
      //     "transactionId": gatewayResponse.data.transactionId,
      //     "paymentIntentId": gatewayResponse.data.paymentIntentId,
      //     "paymentIntentClientSecret":
      //       gatewayResponse.data.paymentIntentClientSecret,
      //     "transactionDate": Date.now(),
      //     "transactionAmount": gatewayResponse.data.transactionAmount, // change dynamically
      //   });
      //   if (!transaction) {
      //     throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      //   }
      // }
      // // gatewayResponse.successResponse = undefined;
      // // gatewayResponse.data.cardDetailes = undefined
      // // gatewayResponse.data.cardDetailes.isCVVRequired = true
      // // gatewayResponse.data.transactionId = transaction._id
      // // gatewayResponse.data.paymentData = paymentData;
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["cardToken"] = gatewayResponse.data.cardToken;
      gatewayResponse["embedtoken"] = gatewayResponse.data.embedtoken;
      gatewayResponse["paymentGateWay"] = paymentData.gateWay;
      gatewayResponse["paymentIntentId"] = gatewayResponse.data.paymentIntentId;
      gatewayResponse["paymentIntentClientSecret"] =
        gatewayResponse.data.paymentIntentClientSecret;
      gatewayResponse["paymentGateWayCustomerId"] =
        gatewayResponse.data.paymentGateWayCustomerId;
      gatewayResponse["transactionId"] = gatewayResponse.data.transactionId;
      gatewayResponse["transactionAmount"] =
        gatewayResponse.data.transactionAmount;
      gatewayResponse["isDefault"] = gatewayResponse.data.isDefault;
      gatewayResponse["isCVVRequired"] = gatewayResponse.data.isCVVRequired;
      gatewayResponse["redirectUrl"] = gatewayResponse?.data?.redirectUrl || "";
      gatewayResponse["redirectMethodType"] =
        gatewayResponse?.data?.redirectMethodType || "";
      gatewayResponse["status"] = gatewayResponse.data.status;
      gatewayResponse.data = null;
      message = gatewayResponse.message;
    } else {
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["cardToken"] = null;
      gatewayResponse["embedtoken"] = null;
      gatewayResponse["paymentGateWay"] = paymentData.gateWay;
      gatewayResponse["paymentIntentId"] = null;
      gatewayResponse["paymentIntentClientSecret"] = null;
      gatewayResponse["paymentGateWayCustomerId"] = null;
      gatewayResponse["transactionId"] = null;
      gatewayResponse["transactionAmount"] = null;
      gatewayResponse["isDefault"] = null;
      gatewayResponse["isCVVRequired"] = null;
      gatewayResponse["redirectUrl"] = null;
      gatewayResponse["redirectUrl"] = null;
      gatewayResponse["redirectMethodType"] = null;
      gatewayResponse["status"] = null;
      errorCode = gatewayResponse.code;
      message = gatewayResponse.message || INFO_INVALID_DETAILS;
    }
    // return details need to alredy parse from payment gatway
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : ERROR_CARD_ADD_FAILED;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": gatewayResponse,
  };
};

transactionAction.verifyCVVByPaymentGatway = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    userData,
    minimumAmount,
    cardDetails = {},
    paymentData = {},
    gatewayResponse = {},
    languageCode;
  if (context.params.professionalId) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  } else if (context.params.userId) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else {
    languageCode = context.meta?.adminDetails?.languageCode;
  }
  const cardId = context.params.cardId;
  const userType = context.params.userType;
  const {
    PROFESSIONAL_NOT_FOUND,
    INFO_INVALID_DETAILS,
    INFO_PAYMENT_OPTIONS,
    SOMETHING_WENT_WRONG,
    INFO_CARD_ALREADY_EXIST,
    INFO_CARD,
    ERROR_CARD_ADD_FAILED,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    const paymentGatway = paymentData.gateWay.toUpperCase();
    //#region User Details User/Professional
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.professionalId,
      });
      message = PROFESSIONAL_NOT_FOUND;
    } else if (context.params.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      message = PROFESSIONAL_NOT_FOUND;
    }
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(message, 500);
    }
    //#endregion User Details User/Professional
    //#region Card Details
    switch (paymentGatway) {
      case CONST_CYBERSOURCE: //Card Details Not needed
      case CONST_TIGO: //Card Details Not needed
        break;

      default:
        if (
          (paymentGatway === constantUtil.STRIPE && cardId === CONST_PIX) ||
          (paymentGatway === constantUtil.CONST_MERCADOPAGO &&
            cardId === CONST_PIX)
        ) {
          break;
        } else {
          if (userType === constantUtil.PROFESSIONAL) {
            // if (paymentOption.gateWay.toUpperCase() !== constantUtil.CONST_TIGO) {
            cardDetails = await this.broker.emit(
              "professional.getCardDetailsById",
              {
                "professionalId": context.params.professionalId,
                "cardId": cardId,
              }
            );
          } else if (userType === constantUtil.USER) {
            cardDetails = await this.broker.emit("user.getCardDetailsById", {
              "userId": context.params.userId,
              "cardId": cardId,
            });
          }
          cardDetails = cardDetails && cardDetails[0];
          if (!cardDetails) {
            throw new MoleculerError(INFO_CARD, 500);
          }
        }
        break;
    }
    //#endregion Card Details
    switch (paymentGatway) {
      case constantUtil.CONST_MERCADOPAGO:
        gatewayResponse = await this.broker.emit(
          "transaction.mercadopago.verifyCVVByPaymentGatway",
          {
            ...context.params,
            "paymentData": paymentData,
            "userData": userData,
            "cardDetails": cardDetails,
            "generalSettings": generalSettings,
          }
        );
        break;
    }
    gatewayResponse = gatewayResponse && gatewayResponse[0];
    if (!gatewayResponse) {
      throw new MoleculerError(INFO_CARD, 500);
    }
    if (gatewayResponse.code === 200) {
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["isCVVRequired"] = gatewayResponse.data.isCVVRequired;
      gatewayResponse["paymentGateWay"] = paymentData.gateWay;
      gatewayResponse["redirectUrl"] = gatewayResponse?.data?.redirectUrl || "";
      gatewayResponse["redirectMethodType"] =
        gatewayResponse?.data?.redirectMethodType || "";
      gatewayResponse["status"] = gatewayResponse.data.status;
      gatewayResponse.data = null;
      message = gatewayResponse.message;
    } else {
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["isCVVRequired"] = null;
      gatewayResponse["paymentGateWay"] = paymentData.gateWay;
      gatewayResponse["redirectUrl"] = null;
      gatewayResponse["redirectMethodType"] = null;
      gatewayResponse["status"] = null;
      errorCode = gatewayResponse.code;
      message = gatewayResponse.message || INFO_INVALID_DETAILS;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : ERROR_CARD_ADD_FAILED;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": gatewayResponse,
  };
};

transactionAction.newBookingCardPayment = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    paymentOption,
    userData = {},
    cardDetails = {},
    gatewayResponse = {};

  const {
    SUCCESS,
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    INFO_PAYMENT_OPTIONS,
    INFO_CARD,
    SOMETHING_WENT_WRONG,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    // if (context.params.userType === constantUtil.PROFESSIONAL) {
    //   userData = await this.broker.emit("professional.getById", {
    //     "id": context.params.userId,
    //   });
    //   userData = userData && userData[0];
    //   if (!userData) throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    // } else
    if (context.params.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      userData = userData && userData[0];
      if (!userData) {
        throw new MoleculerError(USER_NOT_FOUND, 500);
      }
      cardDetails = await this.broker.emit("user.getCardDetailsById", {
        "userId": context.params.userId,
        "cardId": context.params.cardId,
      });
      cardDetails = cardDetails && cardDetails[0];
      if (!cardDetails) {
        throw new MoleculerError(INFO_CARD, 500);
      }
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentOption = paymentOptionList[prop];
        break;
      }
    }
    const bookingId = `${generalSettings.data.bookingPrefix}-${customAlphabet(
      "1234567890",
      9
    )()}`;
    const stringTransactionReason =
      "User Ride Booking/Trip Number : " + bookingId;
    //If isPreAuthRequired is True
    if (paymentOption.isPreAuthRequired === true) {
      cardDetails["cvv"] = context.params.cvv;
      const paramsData = {
        ...context.params,
        "paymentData": paymentOption,
        "userData": userData,
        "cardData": cardDetails,
        "generalSettings": generalSettings,
        "transactionReason": stringTransactionReason,
      };
      switch (paymentOption.gateWay) {
        case constantUtil.STRIPE:
          gatewayResponse = await this.broker.emit(
            "transaction.stripe.createPaymentIntents",
            {
              // ...context.params,
              // "paymentData": paymentOption,
              // "userData": userData,
              // "cardData": cardDetails,
              // "generalSettings": generalSettings,
              // "transactionReason": stringTransactionReason,
              ...paramsData,
            }
          );
          break;

        case constantUtil.RAZORPAY:
          gatewayResponse = await this.broker.emit(
            "transaction.razorpay.createOrderPaymentForBooking",
            {
              // ...context.params,
              // "paymentData": paymentOption,
              // "userData": userData,
              // "cardData": cardDetails,
              // "generalSettings": generalSettings,
              // "transactionReason": stringTransactionReason,
              ...paramsData,
            }
          );
          break;

        case constantUtil.CONST_ZCREDITAPIARY:
          {
            paramsData["bookingId"] = bookingId;
            //-------------------
            gatewayResponse = await this.broker.emit(
              "transaction.zcreditapiary.createPaymentIntents",
              {
                // ...context.params,
                // "paymentData": paymentOption,
                // "userData": userData,
                // "cardData": cardDetails,
                // "generalSettings": generalSettings,
                // "transactionReason": stringTransactionReason,
                // "bookingId": bookingId,
                ...paramsData,
              }
            );
          }
          break;

        case constantUtil.PEACH:
          gatewayResponse = await this.broker.emit(
            "transaction.peach.createPaymentIntents",
            {
              ...paramsData,
            }
          );
          break;

        case constantUtil.CONST_MERCADOPAGO:
          gatewayResponse = await this.broker.emit(
            "transaction.mercadopago.createPaymentIntents",
            {
              ...paramsData,
            }
          );

        case constantUtil.CONST_TEYA:
          gatewayResponse = await this.broker.emit(
            "transaction.teya.createPaymentIntents",
            {
              ...paramsData,
            }
          );
          break;
      }
      gatewayResponse = gatewayResponse && gatewayResponse[0];
      if (!gatewayResponse) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      if (gatewayResponse.code === 200) {
        // if (gatewayResponse.data.authType.toUpperCase() === "NOAUTH") {
        //   const transaction = await this.adapter.model.create({
        //     "paymentType": constantUtil.CREDIT,
        //     "paymentGateWay": paymentOption.gateWay,
        //     "transactionType": constantUtil.BOOKINGCHARGE,
        //     "gatewayDocId": paymentOption._id,
        //     "transactionReason": stringTransactionReason,
        //     "from": {
        //       "phoneNumber": userData?.phone?.number,
        //       "userType": context.params.userType,
        //       "userId": userData._id.toString(),
        //     },
        //     "to": {
        //       "userType": constantUtil.ADMIN,
        //       "userId": paymentOption.accountId,
        //     },
        //     "transactionStatus": constantUtil.PROCESSING,
        //     "cardDetailes": {
        //       // "cardNumber": context.params.cardNumber,
        //       // "expiryMonth": context.params.expiryMonth,
        //       // "expiryYear": context.params.expiryYear,
        //       // "holderName": context.params.holderName,
        //     },
        //     "gatewayResponse": gatewayResponse.data.successResponse,
        //     // "stripeCustomerId": gatewayResponse.data.stripeCustomerId,
        //     "transactionId": gatewayResponse.data.transactionId,
        //     "paymentIntentId": gatewayResponse.data.paymentIntentId,
        //     "paymentIntentClientSecret":
        //       gatewayResponse.data.paymentIntentClientSecret,
        //     "transactionDate": Date.now(),
        //     "transactionAmount": gatewayResponse.data.transactionAmount, // change dynamically
        //   });
        //   if (!transaction) {
        //     throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
        //   }
        // }
        // // gatewayResponse.data.successResponse = undefined;
        // // gatewayResponse.data.cardDetailes = undefined
        // // gatewayResponse.data.cardDetailes.isCVVRequired = true
        // // gatewayResponse.data.transactionId = transaction._id
        // // gatewayResponse.data.paymentData = paymentOption;
        gatewayResponse["authType"] = gatewayResponse.data.authType;
        gatewayResponse["paymentIntentId"] =
          gatewayResponse.data.paymentIntentId;
        gatewayResponse["paymentIntentAmount"] =
          gatewayResponse.data.transactionAmount;
        gatewayResponse["paymentIntentClientSecret"] =
          gatewayResponse.data.paymentIntentClientSecret;
        gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
        gatewayResponse["bookingId"] = bookingId;
        gatewayResponse["transactionId"] = gatewayResponse.data.transactionId;
        gatewayResponse["redirectUrl"] = gatewayResponse.data.redirectUrl;
        gatewayResponse["cardDetails"] = cardDetails;
        gatewayResponse["status"] = gatewayResponse.data.status;
        gatewayResponse.data = null;
        message = gatewayResponse.message;
      } else {
        gatewayResponse["authType"] = null;
        gatewayResponse["paymentIntentId"] = null;
        gatewayResponse["paymentIntentAmount"] = null;
        gatewayResponse["paymentIntentClientSecret"] = null;
        gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
        gatewayResponse["bookingId"] = null;
        gatewayResponse["transactionId"] = null;
        gatewayResponse["redirectUrl"] = null;
        gatewayResponse["cardDetails"] = null;
        gatewayResponse["status"] = null;
        errorCode = gatewayResponse.code;
        message = gatewayResponse.message;
      }
    } else {
      errorCode = 200;
      message = constantUtil.SUCCESS;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": gatewayResponse,
  };
};

transactionAction.newRechargeWalletCardPayment = async function (context) {
  const userType = (context.params.userType || "").toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  const cardId = context.params.cardId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    paymentOption,
    userData = {},
    cardDetails = {},
    gatewayResponse = {},
    transactionStatus = constantUtil.PENDING;

  const {
    SUCCESS,
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    INFO_PAYMENT_OPTIONS,
    INFO_CARD,
    SOMETHING_WENT_WRONG,
    INFO_WALLET_RECHARGE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    //#region paymentOption / PaymentGatway
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentOption = paymentOptionList[prop];
        break;
      }
    }
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    //#endregion paymentOption / PaymentGatway

    //#region user Details
    if (userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.professionalId,
      });
      // userData = userData && userData[0];
      // if (!userData) throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
    } else if (userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      // userData = userData && userData[0];
      // if (!userData) throw new MoleculerError(USER_NOT_FOUND, 500);
    }
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(
        userType === constantUtil.USER
          ? USER_NOT_FOUND
          : PROFESSIONAL_NOT_FOUND,
        500
      );
    }
    //#endregion user Details

    //#region Card Details
    const paymentGatway = paymentOption.gateWay.toUpperCase();
    switch (paymentGatway) {
      case CONST_CYBERSOURCE: //Card Details Not needed
      case CONST_TIGO: //Card Details Not needed
        break;

      default:
        if (
          (paymentGatway === constantUtil.STRIPE && cardId === CONST_PIX) ||
          (paymentGatway === constantUtil.CONST_MERCADOPAGO &&
            cardId === CONST_PIX)
        ) {
          break;
        } else {
          if (userType === constantUtil.PROFESSIONAL) {
            // if (paymentOption.gateWay.toUpperCase() !== constantUtil.CONST_TIGO) {
            cardDetails = await this.broker.emit(
              "professional.getCardDetailsById",
              {
                "professionalId": context.params.professionalId,
                "cardId": cardId,
              }
            );
            // cardDetails = cardDetails && cardDetails[0];
            // if (!cardDetails) {
            //   throw new MoleculerError(INFO_CARD, 500);
            // }
          } else if (userType === constantUtil.USER) {
            cardDetails = await this.broker.emit("user.getCardDetailsById", {
              "userId": context.params.userId,
              "cardId": cardId,
            });
            // cardDetails = cardDetails && cardDetails[0];
            // if (!cardDetails) {
            //   throw new MoleculerError(INFO_CARD, 500);
            // }
          }
          cardDetails = cardDetails && cardDetails[0];
          if (!cardDetails) {
            throw new MoleculerError(INFO_CARD, 500);
          }
          //Pass CVV Parameter
          cardDetails["cvv"] = context.params.cvv;
        }
        break;
    }
    //#endregion Card Details

    const stringTransactionReason = `${
      generalSettings.data.siteTitle
    } / ${INFO_WALLET_RECHARGE} / ${new Date().toISOString()} / ${userType} / ${userData?._id?.toString()}`;
    //------------------
    let paramsData = {
      ...context.params,
      "paymentData": paymentOption,
      "userData": userData,
      "cardData": cardDetails,
      "generalSettings": generalSettings,
      //'transactionId': transaction._id.toString(),
      "transactionReason": stringTransactionReason, //INFO_WALLET_RECHARGE,
    };
    switch (paymentGatway) {
      case constantUtil.STRIPE:
        if (cardId === CONST_PIX) {
          gatewayResponse = await this.broker.emit(
            "transaction.stripe.newRechargeWalletTransactionUsingPixkey",
            {
              // ...context.params,
              // "paymentData": paymentOption,
              // "userData": userData,
              // "cardData": cardDetails,
              // "generalSettings": generalSettings,
              // //'transactionId': transaction._id.toString(),
              // "transactionReason": stringTransactionReason, //INFO_WALLET_RECHARGE,
              ...paramsData,
            }
          );
        } else {
          gatewayResponse = await this.broker.emit(
            "transaction.stripe.newRechargeWalletTransaction",
            {
              // ...context.params,
              // "paymentData": paymentOption,
              // "userData": userData,
              // "cardData": cardDetails,
              // "generalSettings": generalSettings,
              // //'transactionId': transaction._id.toString(),
              // "transactionReason": stringTransactionReason,
              ...paramsData,
            }
          );
        }
        break;

      case constantUtil.RAZORPAY:
        gatewayResponse = await this.broker.emit(
          "transaction.razorpay.newRechargeWalletTransaction",
          {
            // ...context.params,
            // "paymentData": paymentOption,
            // "userData": userData,
            // "cardData": cardDetails,
            // "generalSettings": generalSettings,
            // //'transactionId': transaction._id.toString(),
            // "transactionReason": stringTransactionReason,
            ...paramsData,
          }
        );
        break;

      case CONST_TIGO:
        {
          paramsData["phoneCode"] = context.params.phoneCode;
          paramsData["phoneNumber"] = context.params.phoneNumber;
          gatewayResponse = await this.broker.emit(
            "transaction.tigo.newRechargeWalletTransaction",
            {
              // ...context.params,
              // "paymentData": paymentOption,
              // "userData": userData,
              // "phoneCode": context.params.phoneCode,
              // "phoneNumber": context.params.phoneNumber,
              // "generalSettings": generalSettings,
              // //'transactionId': transaction._id.toString(),
              // "transactionReason": stringTransactionReason,
              ...paramsData,
            }
          );
        }
        break;

      case CONST_CYBERSOURCE:
        {
          // gatewayResponse = [
          //   {
          //     "code": 200,
          //     "message": "SUCESS",
          //     "data": {
          //       "transactionId ": Date.now(),
          //       "authType": "NOAUTH",
          //       "paymentIntentId": Date.now(),
          //       "transactionAmount": 0,
          //       "paymentIntentClientSecret": Date.now(),
          //       "transactionId": Date.now(),
          //       "redirectUrl":
          //         "http://payment.zervx.com/cybersourceHostedCheckout",
          //       "status": "SUCESS",
          //       "successResponse": {},
          //     },
          //   },
          // ];
          paramsData["userId"] = context.params.userId;
          paramsData["professionalId"] = context.params.professionalId;
          gatewayResponse = await this.broker.emit(
            "transaction.cybersource.newRechargeWalletTransaction",
            {
              // ...context.params,
              // "userData": userData,
              // "cardData": cardDetails,
              // "generalSettings": generalSettings,
              // "transactionReason": stringTransactionReason,
              // "userType": userType,
              // "userId": context.params.userId,
              // "professionalId": context.params.professionalId,
              ...paramsData,
            }
          );
          // return {
          //   "code": 200,
          //   "data": {
          //     "paymentURL": "http://payment.zervx.com/cybersourceHostedCheckout",
          //     // "paymentURL": "http://192.168.1.34:3003/cybersourceHostedCheckout",
          //   },
          // };
        }
        break;

      case constantUtil.CONST_ZCREDITAPIARY:
        gatewayResponse = await this.broker.emit(
          "transaction.zcreditapiary.newRechargeWalletTransaction",
          {
            // ...context.params,
            // "paymentData": paymentOption,
            // "userData": userData,
            // "cardData": cardDetails,
            // "generalSettings": generalSettings,
            // //'transactionId': transaction._id.toString(),
            // "transactionReason": stringTransactionReason,
            ...paramsData,
          }
        );
        transactionStatus = constantUtil.SUCCESS;
        break;

      case constantUtil.PEACH:
        gatewayResponse = await this.broker.emit(
          "transaction.peach.newRechargeWalletTransaction",
          {
            ...paramsData,
          }
        );
        transactionStatus = constantUtil.SUCCESS;
        break;

      case constantUtil.CONST_MERCADOPAGO:
        gatewayResponse = await this.broker.emit(
          "transaction.mercadopago.newRechargeWalletTransaction",
          {
            ...paramsData,
            "cardId": cardId,
          }
        );
        transactionStatus = constantUtil.PENDING;
        break;

      case constantUtil.CONST_TEYA:
        gatewayResponse = await this.broker.emit(
          "transaction.teya.newRechargeWalletTransaction",
          {
            ...paramsData,
            "cardId": cardId,
          }
        );
        transactionStatus = constantUtil.PENDING;
        break;
    }
    gatewayResponse = gatewayResponse && gatewayResponse[0];
    if (!gatewayResponse) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    if (gatewayResponse.code === 200) {
      if (
        (paymentGatway === constantUtil.STRIPE && cardId === CONST_PIX) ||
        (paymentGatway === constantUtil.CONST_MERCADOPAGO &&
          cardId === CONST_PIX)
      ) {
        //-------- Transaction Entry Start ---------------------
        const currentBalance =
          parseFloat(userData?.wallet?.availableAmount || 0) +
          parseFloat(context.params.amount || 0);
        this.broker.emit("transaction.insertTransactionBasedOnUserType", {
          "transactionType": constantUtil.WALLETRECHARGE,
          "transactionAmount": context.params.amount,
          "previousBalance": userData?.wallet?.availableAmount || 0,
          "currentBalance": currentBalance,
          "transactionReason": stringTransactionReason,
          "transactionStatus": transactionStatus,
          "transactionId": gatewayResponse.data.transactionId,
          "paymentType": constantUtil.CREDIT,
          "paymentGateWay": paymentGatway,
          "gatewayResponse": gatewayResponse?.successResponse || "",
          "userType": userType,
          "userId":
            userType === constantUtil.USER
              ? context.params.userId.toString()
              : context.params.professionalId.toString(),
          "professionalId":
            userType === constantUtil.USER
              ? context.params.userId.toString()
              : context.params.professionalId.toString(),
          "paymentToUserType": constantUtil.ADMIN,
          "paymentToUserId": null,
        });
        //-------- Transaction Entry End ---------------------
      }
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["paymentIntentId"] = gatewayResponse.data.paymentIntentId;
      gatewayResponse["paymentIntentAmount"] =
        gatewayResponse.data.transactionAmount;
      gatewayResponse["paymentIntentClientSecret"] =
        gatewayResponse.data.paymentIntentClientSecret;
      gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
      gatewayResponse["transactionId"] = gatewayResponse.data.transactionId;
      gatewayResponse["redirectUrl"] = gatewayResponse.data.redirectUrl;
      gatewayResponse["redirectMethodType"] =
        gatewayResponse.data.redirectMethodType;
      gatewayResponse["cardDetails"] = cardDetails;
      gatewayResponse["status"] = gatewayResponse.data.status;
      gatewayResponse["gatewayResponse"] = gatewayResponse.data.successResponse;
      gatewayResponse.data = null;
      message = gatewayResponse.message;
    } else {
      gatewayResponse["authType"] = null;
      gatewayResponse["paymentIntentId"] = null;
      gatewayResponse["paymentIntentAmount"] = null;
      gatewayResponse["paymentIntentClientSecret"] = null;
      gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
      gatewayResponse["transactionId"] = null;
      gatewayResponse["redirectUrl"] = null;
      gatewayResponse["redirectMethodType"] = null;
      gatewayResponse["cardDetails"] = null;
      gatewayResponse["status"] = null;
      gatewayResponse["gatewayResponse"] = null;
      errorCode = gatewayResponse.code;
      message = gatewayResponse.message;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": gatewayResponse,
  };
};

transactionAction.newWithdrawalWalletCardPayment = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    paymentOption,
    userData = {},
    cardDetails = {},
    gatewayResponse = {};
  let bankDetails = {};
  // let transactionId = {};
  // let transactionStatus = {};
  // let finalPaymentData = {};
  // let paymentGatwayResponse = {};

  const {
    ERROR_WALLET_WITHDRAW,
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    INFO_PAYMENT_OPTIONS,
    INFO_CARD,
    SOMETHING_WENT_WRONG,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": context.params.userId,
      });
      userData = userData && userData[0];
      if (!userData) {
        throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
      }
    } else if (context.params.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId,
      });
      userData = userData && userData[0];
      if (!userData) {
        throw new MoleculerError(USER_NOT_FOUND, 500);
      }
      cardDetails = await this.broker.emit("user.getCardDetailsById", {
        "userId": context.params.userId,
        "cardId": context.params.cardId,
      });
      cardDetails = cardDetails && cardDetails[0];
      if (!cardDetails) {
        throw new MoleculerError(INFO_CARD, 500);
      }
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentOption = paymentOptionList[prop];
        break;
      }
    }
    // const bookingId = `${generalSettings.data.bookingPrefix}-${customAlphabet(
    //   "1234567890",
    //   9
    // )()}`;
    const stringTransactionReason = `${generalSettings.data.siteTitle}/${
      constantUtil.WALLETWITHDRAWAL
    }/${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}/`;
    //Pass CVV Parameter
    cardDetails["cvv"] = context.params.cvv;
    if (generalSettings.data.payoutOptions === constantUtil.AUTOMATED) {
      // transactionStatus = constantUtil.PENDING;
      // --------- Bank Details Start ----------
      if (context.params.userType === constantUtil.PROFESSIONAL) {
        bankDetails = userData.bankDetails;
      } else {
        bankDetails = userData.bankDetails[0];
        // bankDetails = userData.bankDetails.filter(
        //   (each) => each.accountNumber === context.params.account.accountNumber
        // )
        // bankDetails = bankDetails[0]
      }
      if (!bankDetails) {
        throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
      }
      // --------- Bank Details End ----------
      if (paymentOption.gateWay === constantUtil.STRIPE) {
        //----------- InComplete --------------
        gatewayResponse = await this.broker.emit(
          "transaction.stripe.newWithdrawalWalletTransaction",
          {
            ...context.params,
            "paymentData": paymentOption,
            "userData": userData,
            "bankDetails": bankDetails,
            "generalSettings": generalSettings,
            //'transactionId': transaction._id.toString(),
            "transactionReason": stringTransactionReason,
          }
        );
        //----------- InComplete --------------
      }
    } else {
      gatewayResponse = [
        {
          "code": 400,
          "data": {
            "authType": "NOAUTH",
          },
          "message": constantUtil.SUCCESS,
        },
      ];
    }
    gatewayResponse = gatewayResponse && gatewayResponse[0];
    if (!gatewayResponse) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    if (gatewayResponse.code === 200) {
      gatewayResponse["authType"] = gatewayResponse.data.authType;
      gatewayResponse["paymentIntentId"] = gatewayResponse.data.paymentIntentId;
      gatewayResponse["paymentIntentAmount"] =
        gatewayResponse.data.transactionAmount;
      gatewayResponse["paymentIntentClientSecret"] =
        gatewayResponse.data.paymentIntentClientSecret;
      gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
      gatewayResponse["transactionId"] = gatewayResponse.data.transactionId;
      gatewayResponse["cardDetails"] = cardDetails;
      gatewayResponse["status"] = gatewayResponse.data.status;
      gatewayResponse.data = null;
      message = gatewayResponse.message;
    } else {
      gatewayResponse["authType"] = null;
      gatewayResponse["paymentIntentId"] = null;
      gatewayResponse["paymentIntentAmount"] = null;
      gatewayResponse["paymentIntentClientSecret"] = null;
      gatewayResponse["paymentGateWay"] = paymentOption.gateWay;
      gatewayResponse["transactionId"] = null;
      gatewayResponse["cardDetails"] = null;
      gatewayResponse["status"] = null;
      errorCode = gatewayResponse.code;
      message = gatewayResponse.message;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": gatewayResponse,
  };
};

transactionAction.checkCardTransactionStatus = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseData = {},
    paymentData = {};
  const { INFO_PAYMENT_OPTIONS, SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    switch (paymentData.gateWay.toUpperCase()) {
      case CONST_CYBERSOURCE:
        responseData = await this.broker.emit(
          "transaction.cyberSource.checkTransactionStatus",
          {
            "userType": context.params.userType,
            "userId": context.params.userId,
            "professionalId": context.params.professionalId,
            "transactionId": context.params.transactionId,
          }
        );
        break;
    }
    responseData = responseData && responseData[0];
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData?.data || {},
  };
};

transactionAction.validateCardRegistrationStatus = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseData = {},
    paymentData = {};
  const { INFO_PAYMENT_OPTIONS, SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  try {
    const paymentOptionList = await storageUtil.read(
      constantUtil.PAYMENTGATEWAY
    );
    if (!paymentOptionList) {
      throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
    }
    for (const prop in paymentOptionList) {
      if (
        paymentOptionList[prop].status === constantUtil.ACTIVE &&
        paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
      ) {
        paymentData = paymentOptionList[prop];
        break;
      }
    }
    switch (paymentData.gateWay.toUpperCase()) {
      case constantUtil.PEACH:
        responseData = await this.broker.emit(
          "transaction.peach.validateCardRegistrationStatus",
          {
            ...context.params,
            "userType": context.params.userType,
            "userId": context.params.userId,
            "professionalId": context.params.professionalId,
            "paymentData": paymentData,
          }
        );
        break;
    }
    responseData = responseData && responseData[0];
    errorCode = responseData.code;
    message = responseData.message;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? INFO_INVALID_DETAILS : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData?.data || {},
  };
};

transactionAction.getTransactionReport = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    errorMessage = "",
    message = "",
    responseData = {};
  try {
    const query = [];

    if (context.params.userType) {
      query.push({
        "$match": {
          "from.userType": context.params.userType,
        },
      });
    }
    // FOR EXPORT DATE FILTER
    if (
      context.params.TransactionDatesFrom &&
      context.params.TransactionDatesFrom != "" &&
      context.params.TransactionDatesTO &&
      context.params.TransactionDatesTO != ""
    ) {
      const fromDate = new Date(
        new Date(context.params.TransactionDatesFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.TransactionDatesTO).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "transactionDate": { "$gte": fromDate, "$lt": toDate } },
      });
    }

    if (context.params.paymentType && context.params.paymentType !== "ALL") {
      query.push({
        "$match": { "paymentType": context.params.paymentType },
      });
    }
    if (context.params.userType !== "") {
      query.push({
        "$match": {
          "from.userType": context.params.userType,
        },
      });
    }
    if (context.params.phoneNumber) {
      query.push({
        "$match": {
          "from.phoneNumber": {
            "$regex": context.params.phoneNumber?.trim(),
            "$options": "si",
          },
        },
      });
    }
    query.push(
      {
        "$lookup": {
          "from": "bookings",
          "localField": "bookingId",
          "foreignField": "_id",
          "as": "bookingData",
        },
      },
      {
        "$unwind": {
          "path": "$bookingData",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );

    query.push({
      "$facet": {
        "totalCount": [
          {
            "$count": "count",
          },
        ],
        "responseData": [
          {
            "$project": {
              "_id": "$_id",
              "names": "$from",
              "transactionDate": "$transactionDate",
              "bookingId": "$bookingData.bookingId",
              "paymentType": "$paymentType",
              "transactionType": "$transactionType",
              "transactionReason": "$transactionReason",
              "transactionAmount": "$transactionAmount",
              "transactionId": "$transactionId",
              "transactionToName": "$to.name",
              // "transactionToUserType": "$to.userType",
              // "bookingData": "$bookingData",
            },
          },
          { "$sort": { "transactionDate": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
        ],
      },
    });

    responseData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData || {},
  };
};

//#endregion  Payment Gatway

//-----------------
module.exports = transactionAction;
