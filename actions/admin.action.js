const { MoleculerError, MoleculerClientError, MoleculerRetryableError } =
  require("moleculer").Errors;
const mongoose = require("mongoose");
const _ = require("lodash");
const turf = require("@turf/turf");
const bcrypt = require("bcrypt-nodejs");
const { customAlphabet } = require("nanoid");

const constantUtil = require("../utils/constant.util");
const mappingUtil = require("../utils/mapping.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

// const adminModel = require("../models/admin.model");
// const professionalModel = require("../models/professional.model");

const { ValidationError } = require("xml2js");

const {
  setDayStartHours,
  setDayEndHours,
  toRegionalUTC,
  getDbQueryString,
} = require("../utils/common.util");
const {
  lzStringEncode,
  lzStringDecode,
  hashSecret,
  jwtSign,
  cryptoAESEncode,
  cryptoAESDecode,
} = require("../utils/crypto.util");
const { ADD, CONST_SPECIALDAYS } = require("../utils/constant.util");
const { trim } = require("lodash");

const adminAction = {};

// // COMMON APIS
// adminAction.reactConfig = async function (context) {
//   let responseJson = null;
//   const clientId =
//     context.params.clientId?.length > 12 ? context.params.clientId : null;
//   responseJson = await storageUtil.read(constantUtil.GENERALSETTING);
//   // //----------------
//   // const { JWT } = require("google-auth-library");
//   // // // const keys = require("./jwt.keys.json");

//   // const client = new JWT(
//   //   "https://oauth2.googleapis.com/oauth/token",
//   //   null,
//   //   "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC4yHW6YOl5ONqJ\nyfYeewEoOO/XClFCiSO3o4Ovg0wSfYab8zQ+9iWFIw44rvZUO9yjbWYzQWpQI0Ha\nJztR0gqBkUGDCYuiWfkLAR4NXs2udYskwnzJBOnSyumWTd/ZMPoOytjlOuZl2L9b\nvoM1hC5GW269xk9BGqip7EhV4HD725tUndHPaCPFiJHnyFaxMMAjPH5Gr+A6guqY\nfm4SWWn1OVmcFS5q1/v4MG16kWKU36fKSLaWLkz+unwKpblb040qRs6OkGTqrSOR\n4KuEnKK94MMDvvb3BCGFS1dRtZ1RNKt20et94Uf3iDCGDdXnFQUJQmnTnJIdmWMX\nVDCzyJXvAgMBAAECggEAGXDH/KwhGOaB+ByNPS8UPrRwGPw7qdx1xW/GzqBWFemg\nYdI/LDf5fHYSHC3CjmBbhUinnIDyISxpgZ6LJT204pfcHC1xQx3+EszJlW+rA8z9\nJkd8pLR/eN0lAeOg4SdEFktJR24vkHAiEICPUgvrzxYBjjFCciU4eYSlhOVMyO/8\nac/3tupquv60xi5LSUdy7F3O4Z/gBqwNKLIkac5Z8V12DLY3ugFJodPUshoWx2Q2\nTlC7wu0DcH47JHOa5TLaQkswJR562yydEGYPQifbo2BtDgamUUrnwazHVJRVsYzz\nw/U1mUSIiufcZ7FAiR55UrRyMqd0isoJY6AMDJhV8QKBgQDhlvzSsUnpY6YUA9ky\nrcAh7NjDQ9QrPNFwTjEqgn78BqXwx5mPH+S1XEyBLRLI8W350CWt3L8uxvWizT54\nBxoaYMlYDYp1XuHtXOAcIRQq0Yy09cKD+Ng1AhbnAS5t8lf74Y/+rfgIJi16ZtGy\nbVVTtckr88zJFup58uNsA4g7hwKBgQDRsT93ndhF3H7w8RjHBbeZTzG7bKoCX8Xa\ngbQjMOYnds5wkhXBKZzkwDi9hPB9DeVVC2LInQpGGCwE8Q/GUNg4DZHvXBfz3lB/\nENpTEF8i6gdEqGUEtngoauc9EI8Qj9eR6JYshaEwoFg0s6/GsFYNKTS56d17u+gh\nwQNKTVX8WQKBgGgJSe+g84U+/4ibfb9xgfojUJQ848xM8AnrBaGlLQrtxfmVE6x9\n+Ovq9R1dvBwPu7hJ1haoh0L7Xq0GGpkgLVmc6T5CgGhAqdDd1D0kxDg7C7ko3dwf\nFxbbL9S5JsBt00XGcLHzaT/Y24Bniq3O3lsMqwoqFXdnqhszeM6HknxhAoGAHkwK\nzICc5/Mk0DE1kEPuNBNyLw9TPQaTcq+81DzTdC5BQ5iH1iHqDk8Lo65ahOcGFJSR\nkETIYDpL2ebujYvOwcKMLFZVC6I6ZTGNKeU6qbPAmxXtipHGqKhRgswqr4zo66b8\nU7B3CqyQMgUoR3/g3wPuH1J/8snCj2Xw0MJ9wvkCgYEA3fT6caBVBiZ+r55804tD\njVpZPBn4je/UjBP/t9glgUEAggxK8VuZFYaY7Bu5o1Q0qLpLTgpObPgRZPlWK5mf\nc6UoFLYC5yn0GTR47ZfKv4xe5c2TOkLyKS7iuDYhwjFt4yI/dNa0RtSZK/5krmdC\nBYBAr4+6jxY+lBO4ym5qcRs=\n-----END PRIVATE KEY-----\n",
//   //   "https://www.googleapis.com/auth/firebase.messaging",
//   //   null
//   // );
//   // // const url = `https://dns.googleapis.com/dns/v1/projects/${"zervx-7b622"}`;
//   // const url = `https://dns.googleapis.com/dns/v1/projects/${"zervx-7b622"}`;
//   // const res = await client.request({ url });
//   // console.log(res.data);
//   // //-----------------
//   // const { google } = require("googleapis");

//   // // // const oauth2Client = new google.auth.OAuth2(
//   // // //     "469093796333-mrvffaqpm0436lkb28fon6nq7nh3t4es.apps.googleusercontent.com",
//   // // //    "UiD6SQ-_U_px-s9DGXsLkaS9",
//   // // //    "http://localhost:8080//api/transaction/stripe/webhook",
//   // // // );
//   // // const oauth2Client = new google.auth.OAuth2({
//   // //   "client_id":
//   // //     "469093796333-mrvffaqpm0436lkb28fon6nq7nh3t4es.apps.googleusercontent.com",
//   // //   "client_secret": "UiD6SQ-_U_px-s9DGXsLkaS9",
//   // //   "redirect_uri": "http://localhost:8080//api/transaction/stripe/webhook",
//   // // });
//   // const oauth2Client = new google.auth.OAuth2(
//   //   "469093796333-mrvffaqpm0436lkb28fon6nq7nh3t4es.apps.googleusercontent.com",
//   //   "UiD6SQ-_U_px-s9DGXsLkaS9",
//   //   "http://localhost:8080//api/transaction/stripe/webhook"
//   // );

//   // // Access scopes for read-only Drive activity.
//   // const scopes = ["https://www.googleapis.com/auth/firebase.messaging"];

//   // // Generate a url that asks permissions for the Drive activity scope
//   // const authorizationUrl = oauth2Client.generateAuthUrl({
//   //   // 'online' (default) or 'offline' (gets refresh_token)
//   //   access_type: "offline",
//   //   /** Pass in the scopes array defined above.
//   //    * Alternatively, if only one scope is needed, you can pass a scope URL as a string */
//   //   scope: scopes,
//   //   // Enable incremental authorization. Recommended as a best practice.
//   //   include_granted_scopes: true,
//   //   // response_type: "token",
//   // });
//   // // res.writeHead(301, { "Location": authorizationUrl });
//   // //------------------

//   if (!responseJson)
//     // responseJson = await this.adapter.model.findOne({
//     //   "name": constantUtil.GENERALSETTING,
//     // });
//     responseJson = await this.schema.adapter.model
//       .findOne({
//         "name": constantUtil.GENERALSETTING,
//       })
//       .lean();

//   if (!responseJson) {
//     throw new MoleculerError("Invalid Data");
//   }
//   // const langJson = await this.adapter.model.findOne({
//   //   "name": constantUtil.LANGUAGES,
//   //   "data.languageDefault": true,
//   // });
//   const langJson = await this.schema.adapter.model
//     .findOne({
//       "name": constantUtil.LANGUAGES,
//       "data.languageDefault": true,
//     })
//     .lean();
//   if (!langJson) {
//     throw new MoleculerError("Invalid Details");
//   }
//   // const firebaseConfig = await this.adapter.model.findOne(
//   //   {
//   //     "name": constantUtil.FIREBASESETTING,
//   //   },
//   //   { "_id": 0, "data.firebaseConfig": 1 }
//   // );
//   const firebaseConfig = await this.schema.adapter.model
//     .findOne(
//       {
//         "name": constantUtil.FIREBASESETTING,
//       },
//       { "_id": 0, "data.firebaseConfig": 1 }
//     )
//     .lean();
//   // //------------------------------------
//   // const replicaSetDBConn = require("../mixins/replicaSetDBConn.mixin");
//   // // const session = await mongoose
//   // //   .createConnection(
//   // //     replicaSetDBConn.connectionString,
//   // //     replicaSetDBConn.connectionProperty
//   // //   )
//   // //   .startSession();
//   // const session = await replicaSetDBConn.startSession();
//   // // const session = await this.schema.adapter.conn.startSession();
//   // try {
//   //   session.startTransaction();
//   //   //  /#region Method 1 Using Session Transaction --> Working
//   //   // const user = await this.schema.userModel.model.create(
//   //   //   [
//   //   //     {
//   //   //       "firstName": "Van Helsing",
//   //   //       "phone.code": "+16",
//   //   //       "phone.number": "111111111",
//   //   //     },
//   //   //   ],
//   //   //   { session }
//   //   // );
//   //   // const pro = await this.schema.userModel.model.create(
//   //   //   [
//   //   //     {
//   //   //       "firstName": "Helsing",
//   //   //       "phone.code": "+23",
//   //   //       "phone.number": "222222222",
//   //   //       "serviceAreaId": user[0]._id?.toString(),
//   //   //     },
//   //   //   ],
//   //   //   { session }
//   //   // );
//   //   // // /#endregion method 1
//   //   // // /#region Method 2 Using Emit & Session Transaction --> Not Working
//   //   // const user = await this.schema.userModel.model.updateOne(
//   //   //   {
//   //   //     "_id": mongoose.Types.ObjectId("65dd7563203199e684ef1b5b"),
//   //   //   },
//   //   //   { "$set": { "firstName": "New " } },
//   //   //   { session }
//   //   // );
//   //   // if (user.nModified) {
//   //   // await this.broker.emit(
//   //   //   "user.updateWalletAmountInBooking",
//   //   //   {
//   //   //     "userId": "65dd7563203199e684ef1b5b",
//   //   //     "amount": 10,
//   //   //     "finalAmount": 15,
//   //   //   },
//   //   //   { session }
//   //   // );
//   //   // }
//   //   // const x = 1 * o;
//   //   // // /#endregion Method 2
//   //   // /#region Method 3 Without Using Session Transaction --> Working
//   //   // const user = await this.schema.userModel.model.updateOne(
//   //   //   {
//   //   //     "_id": mongoose.Types.ObjectId("65dd7563203199e684ef1b5b"),
//   //   //   },
//   //   //   { "$set": { "firstName": "New 6666" } }
//   //   // );
//   //   // /#endregion Method 3 Without Using Session Transaction --> Working
//   //   // /#region Method 4,  Multiple Update on Same Collection Using Session Transaction --> Working
//   //   // const user = await this.schema.userModel.model.updateOne(
//   //   //   {
//   //   //     "_id": mongoose.Types.ObjectId("65e04a0470bd1c218d251cc5"),
//   //   //   },
//   //   //   { "$set": { "firstName": "5555 " } },
//   //   //   { session }
//   //   // );
//   //   // if (user.nModified) {
//   //   //   await this.schema.userModel.model.updateOne(
//   //   //     {
//   //   //       "_id": mongoose.Types.ObjectId("65e04a0470bd1c218d251cc5"),
//   //   //     },
//   //   //     { "$set": { "firstName": "44444 NNNN" } },
//   //   //     { session }
//   //   //   );
//   //   // }
//   //   // /#endregion Method 4
//   //   // /#region Method
//   //   // /#endregion Method
//   //   // /#region Method
//   //   // /#endregion Method
//   //   await session.commitTransaction();

//   //   console.log("success");
//   // } catch (error) {
//   //   console.log(error.message);
//   //   await session.abortTransaction();
//   // } finally {
//   //   session.endSession();
//   //   // replicaSetDBConn.close();
//   // }
//   // //------------------------------------

//   responseJson.data.secretKey = "DoNotDisplay"; //// Must will not send this Key Value
//   responseJson.data.languageCode =
//     langJson.data.languageCode || constantUtil.DEFAULT_LANGUAGE; // Need to Remove
//   // responseJson.data.language = {
//   //   "_id": langJson._id,
//   //   "languageDirection": langJson.data.languageDirection,
//   //   "languageKeys": langJson.data.languageKeys,
//   // };
//   responseJson.data.languageDirection =
//     langJson.data.languageDirection || "LTR"; // Need to Remove
//   responseJson.data.firebaseConfig = firebaseConfig?.data?.firebaseConfig || {};
//   return responseJson;
// };
// COMMON APIS
adminAction.reactConfig = async function (context) {
  let responseJson = null,
    isBillingExpired = false,
    billingExpiredDays = null;
  let clientId =
    context.params.clientId?.length > 12 ? context.params.clientId : null;
  const generalConfigList = await storageUtil.read(constantUtil.GENERALSETTING);
  // //------------
  if (clientId) {
    if (generalConfigList.length > 0) {
      // Object.values(generalConfigList)
      //   .filter(
      //     (each) =>
      //       each.name === constantUtil.GENERALSETTING &&
      //       each.data.status === constantUtil.ACTIVE &&
      //       each.data.clientId === clientId
      //   )
      //   .forEach((item) => {
      //     responseJson = item;
      //   });
      //---------------------------
      //  #region Get Config Settings
      responseJson = await this.broker.emit(
        "admin.getClientsBaseDataFromRedisUsingTypeAndClientId",
        {
          "type": constantUtil.GENERALSETTING,
          "clientId": clientId,
        }
      );
      responseJson = responseJson && responseJson[0];
      //  #endregion Get Config Settings
      //---------------------------
    }
  } else {
    if (generalConfigList.length > 0) {
      Object.values(generalConfigList)
        .filter(
          (each) =>
            each.name === constantUtil.GENERALSETTING &&
            each.dataType === constantUtil.BASEDATA &&
            each.data.status === constantUtil.ACTIVE
        )
        .forEach((item) => {
          responseJson = item;
        });
    } else if (generalConfigList) {
      responseJson = generalConfigList;
      clientId = generalConfigList.data.clientId;
    }
  }
  // //------------

  if (!responseJson) {
    throw new MoleculerError("Invalid Data");
  } else {
    if (clientId) {
      responseJson = await this.schema.adapter.model
        .findOne({
          "name": constantUtil.GENERALSETTING,
          "data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else {
      responseJson = await this.schema.adapter.model
        .findOne({
          "name": constantUtil.GENERALSETTING,
          "dataType": constantUtil.BASEDATA,
        })
        .lean();
    }
  }
  // const langJson = await this.adapter.model.findOne({
  //   "name": constantUtil.LANGUAGES,
  //   "data.languageDefault": true,
  // });
  let langJson = null;
  if (clientId) {
    langJson = await this.schema.adapter.model
      .findOne({
        "name": constantUtil.LANGUAGES,
        "data.languageDefault": true,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      })
      .lean();
  } else {
    langJson = await this.schema.adapter.model
      .findOne({
        "name": constantUtil.LANGUAGES,
        "data.languageDefault": true,
        "dataType": constantUtil.BASEDATA,
      })
      .lean();
  }
  if (!langJson) {
    throw new MoleculerError("Invalid Details");
  }
  // const firebaseConfig = await this.adapter.model.findOne(
  //   {
  //     "name": constantUtil.FIREBASESETTING,
  //   },
  //   { "_id": 0, "data.firebaseConfig": 1 }
  // );
  let firebaseConfig = null;
  if (clientId) {
    firebaseConfig = await this.schema.adapter.model
      .findOne(
        {
          "name": constantUtil.FIREBASESETTING,
          "data.clientId": mongoose.Types.ObjectId(clientId),
        },
        { "_id": 0, "data.firebaseConfig": 1 }
      )
      .lean();
  } else {
    firebaseConfig = await this.schema.adapter.model
      .findOne(
        {
          "name": constantUtil.FIREBASESETTING,
          "dataType": constantUtil.BASEDATA,
        },
        { "_id": 0, "data.firebaseConfig": 1 }
      )
      .lean();
  }
  // //------------------------------------
  // const replicaSetDBConn = require("../mixins/replicaSetDBConn.mixin");
  // // const session = await mongoose
  // //   .createConnection(
  // //     replicaSetDBConn.connectionString,
  // //     replicaSetDBConn.connectionProperty
  // //   )
  // //   .startSession();
  // const session = await replicaSetDBConn.startSession();
  // // const session = await this.schema.adapter.conn.startSession();
  // try {
  //   session.startTransaction();
  //   //  /#region Method 1 Using Session Transaction --> Working
  //   // const user = await this.schema.userModel.model.create(
  //   //   [
  //   //     {
  //   //       "firstName": "Van Helsing",
  //   //       "phone.code": "+16",
  //   //       "phone.number": "111111111",
  //   //     },
  //   //   ],
  //   //   { session }
  //   // );
  //   // const pro = await this.schema.userModel.model.create(
  //   //   [
  //   //     {
  //   //       "firstName": "Helsing",
  //   //       "phone.code": "+23",
  //   //       "phone.number": "222222222",
  //   //       "serviceAreaId": user[0]._id?.toString(),
  //   //     },
  //   //   ],
  //   //   { session }
  //   // );
  //   // // /#endregion method 1
  //   // // /#region Method 2 Using Emit & Session Transaction --> Not Working
  //   // const user = await this.schema.userModel.model.updateOne(
  //   //   {
  //   //     "_id": mongoose.Types.ObjectId("65dd7563203199e684ef1b5b"),
  //   //   },
  //   //   { "$set": { "firstName": "New " } },
  //   //   { session }
  //   // );
  //   // if (user.nModified) {
  //   // await this.broker.emit(
  //   //   "user.updateWalletAmountInBooking",
  //   //   {
  //   //     "userId": "65dd7563203199e684ef1b5b",
  //   //     "amount": 10,
  //   //     "finalAmount": 15,
  //   //   },
  //   //   { session }
  //   // );
  //   // }
  //   // const x = 1 * o;
  //   // // /#endregion Method 2
  //   // /#region Method 3 Without Using Session Transaction --> Working
  //   // const user = await this.schema.userModel.model.updateOne(
  //   //   {
  //   //     "_id": mongoose.Types.ObjectId("65dd7563203199e684ef1b5b"),
  //   //   },
  //   //   { "$set": { "firstName": "New 6666" } }
  //   // );
  //   // /#endregion Method 3 Without Using Session Transaction --> Working
  //   // /#region Method 4,  Multiple Update on Same Collection Using Session Transaction --> Working
  //   // const user = await this.schema.userModel.model.updateOne(
  //   //   {
  //   //     "_id": mongoose.Types.ObjectId("65e04a0470bd1c218d251cc5"),
  //   //   },
  //   //   { "$set": { "firstName": "5555 " } },
  //   //   { session }
  //   // );
  //   // if (user.nModified) {
  //   //   await this.schema.userModel.model.updateOne(
  //   //     {
  //   //       "_id": mongoose.Types.ObjectId("65e04a0470bd1c218d251cc5"),
  //   //     },
  //   //     { "$set": { "firstName": "44444 NNNN" } },
  //   //     { session }
  //   //   );
  //   // }
  //   // /#endregion Method 4
  //   // /#region Method
  //   // /#endregion Method
  //   // /#region Method
  //   // /#endregion Method
  //   await session.commitTransaction();

  //   console.log("success");
  // } catch (error) {
  //   console.log(error.message);
  //   await session.abortTransaction();
  // } finally {
  //   session.endSession();
  //   // replicaSetDBConn.close();
  // }
  // //------------------------------------
  const loginDate = new Date();
  const expiryDate = new Date(responseJson?.data?.expiryDate || new Date());
  responseJson.data.expiryDate = "DoNotDisplay"; //// Must will not send this Key Value
  responseJson.data.secretKey = "DoNotDisplay"; //// Must will not send this Key Value
  responseJson.data.languageCode =
    langJson.data.languageCode || constantUtil.DEFAULT_LANGUAGE; // Need to Remove
  // responseJson.data.language = {
  //   "_id": langJson._id,
  //   "languageDirection": langJson.data.languageDirection,
  //   "languageKeys": langJson.data.languageKeys,
  // };
  responseJson.data.languageDirection =
    langJson.data.languageDirection || "LTR"; // Need to Remove
  responseJson.data.firebaseConfig = firebaseConfig?.data?.firebaseConfig || {};

  if (loginDate.getTime() > expiryDate.getTime()) {
    isBillingExpired = true;
  } else {
    billingExpiredDays = Math.floor(
      (Date.parse(expiryDate) - Date.parse(loginDate)) / 86400000
    );
  }
  responseJson.data.isBillingExpired = isBillingExpired;
  responseJson.data.billingExpiredDays = billingExpiredDays;
  return responseJson;
};

adminAction.reactConfigWebbooking = async function (context) {
  let responseJson = null,
    clientId = null;
  const secretKey = context.params?.secretKey || "";
  const generalConfigList = await storageUtil.read(constantUtil.GENERALSETTING);
  // //------------
  if (secretKey) {
    if (generalConfigList.length > 0) {
      Object.values(generalConfigList)
        .filter(
          (each) =>
            each.name === constantUtil.GENERALSETTING &&
            each.data.status === constantUtil.ACTIVE &&
            each.data.secretKey === secretKey
        )
        .forEach((item) => {
          responseJson = item;
        });
    }
  } else {
    if (generalConfigList.length > 0) {
      Object.values(generalConfigList)
        .filter(
          (each) =>
            each.name === constantUtil.GENERALSETTING &&
            each.dataType === constantUtil.BASEDATA &&
            each.data.status === constantUtil.ACTIVE
        )
        .forEach((item) => {
          responseJson = item;
        });
    }
  }
  // //------------
  if (!responseJson) {
    clientId = responseJson?.data?.clientId;
    if (clientId) {
      responseJson = await this.schema.adapter.model
        .findOne({
          "name": constantUtil.GENERALSETTING,
          "data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else {
      responseJson = await this.schema.adapter.model
        .findOne({
          "name": constantUtil.GENERALSETTING,
          "dataType": constantUtil.BASEDATA,
        })
        .lean();
    }
  }
  if (!responseJson) {
    throw new MoleculerError("Invalid Data");
  }
  // const langJson = await this.adapter.model.findOne({
  //   "name": constantUtil.LANGUAGES,
  //   "data.languageDefault": true,
  // });
  const langJson = await this.schema.adapter.model
    .findOne({
      "name": constantUtil.LANGUAGES,
      "data.languageDefault": true,
      // "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!langJson) {
    throw new MoleculerError("Invalid Details");
  }
  // const firebaseConfig = await this.adapter.model.findOne(
  //   {
  //     "name": constantUtil.FIREBASESETTING,
  //   },
  //   { "_id": 0, "data.firebaseConfig": 1 }
  // );
  let firebaseConfig = null;
  if (clientId) {
    firebaseConfig = await this.schema.adapter.model
      .findOne(
        {
          "name": constantUtil.FIREBASESETTING,
          "data.clientId": mongoose.Types.ObjectId(clientId),
        },
        { "_id": 0, "data.firebaseConfig": 1 }
      )
      .lean();
  } else {
    firebaseConfig = await this.schema.adapter.model
      .findOne(
        {
          "name": constantUtil.FIREBASESETTING,
          "dataType": constantUtil.BASEDATA,
        },
        { "_id": 0, "data.firebaseConfig": 1 }
      )
      .lean();
  }
  responseJson.data.secretKey = "DoNotDisplay"; //// Must will not send this Key Value
  responseJson.data.languageCode = langJson.data.languageCode || "en"; // Need to Remove
  // responseJson.data.language = {
  //   "_id": langJson._id,
  //   "languageDirection": langJson.data.languageDirection,
  //   "languageKeys": langJson.data.languageKeys,
  // };
  responseJson.data.languageDirection =
    langJson.data.languageDirection || "LTR"; // Need to Remove
  responseJson.data.firebaseConfig = firebaseConfig?.data?.firebaseConfig || {};
  return responseJson;
};

// GET ALL DATA ONLY USING ID
adminAction.getDataById = async function (context) {
  const data = this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.id),
    })
    .lean();
  if (!data) {
    throw new MoleculerError("INVALID , ID NOT FOUND");
  } else {
    return data;
  }
};

adminAction.getLanguageKeys = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.LANGUAGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.languageCode": context.params.languageCode,
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return {
      "_id": responseJson._id,
      "languageDirection": responseJson.data.languageDirection,
      "languageKeys": responseJson.data.languageKeys,
    };
  }
};

adminAction.setUserBasedLanguage = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.adminId.toString()),
    {
      "data.languageCode": context.params.languageCode,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    responseJson = await this.adapter.model
      .findOne({
        "name": constantUtil.LANGUAGES,
        //"data.clientId": mongoose.Types.ObjectId(clientId),
        "data.languageCode": context.params.languageCode,
        "data.status": constantUtil.ACTIVE,
      })
      .lean();
    return {
      "_id": responseJson._id,
      "languageDirection": responseJson.data.languageDirection,
      "languageKeys": responseJson.data.languageKeys,
    };
  }
};

adminAction.loginValidation = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId = context.params?.clientId || context.meta?.clientId;
  const adminData = await this.adapter.model
    .findOne({
      "_id": context.meta.adminId,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  if (!adminData) {
    throw new MoleculerError("Invalid Login Data");
  }
  let privilageJson = null;
  if (
    adminData.name !== constantUtil.DEVELOPER &&
    adminData.name !== constantUtil.ADMIN
  ) {
    if (adminData.name === constantUtil.OPERATORS) {
      privilageJson = await this.adapter.model
        .findOne({
          "_id": adminData.data.operatorRole,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else if (
      adminData.name === constantUtil.HUBS ||
      adminData.name === constantUtil.HUBSEMPLOYEE
    ) {
      privilageJson = await this.adapter.model
        .findOne({
          // "name": constantUtil.HUBSPRIVILEGES,
          "name":
            adminData.name === constantUtil.HUBS
              ? constantUtil.HUBSPRIVILEGES
              : constantUtil.HUBSEMPLOYEEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else if (adminData.name === constantUtil.RESPONSEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.OFFICEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else if (adminData.name === constantUtil.COORPERATEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.COORPERATEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    }
    if (!privilageJson) {
      throw new MoleculerError("Invalid Privilege");
    }
    adminData.data.privileges = privilageJson.data.privileges;
  }
  adminData.data.extraPrivileges = adminData.data.extraPrivileges || {};
  adminData.data.defaultPrivileges = generalSettings.data.privileges || {};
  // adminData.data.defaultPrivileges = {};

  return mappingUtil.adminObject(adminData);
};

// SUPER ADMIN
adminAction.loginWithOtp = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    adminData,
    responseData = {},
    generalSettings = null,
    clientId = null,
    userType = context.params.userType;
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    context.params.otp = customAlphabet("1234567890", 6)();
    const hashOtp = hashSecret(context.params.otp);

    let adminData = await this.adapter.model
      .findOne({
        "data.phone.code": context.params.phoneCode,
        "data.phone.number": context.params.phoneNumber,
        "name": {
          "$in": [
            constantUtil.DEVELOPER,
            constantUtil.ADMIN,
            constantUtil.OPERATORS,
            constantUtil.HUBS,
            constantUtil.HUBSEMPLOYEE,
            constantUtil.COORPERATEOFFICE,
            constantUtil.RESPONSEOFFICE,
          ],
        },
      })
      .lean();
    if (!adminData) {
      throw new MoleculerError("Invalid credentials");
    }
    if (adminData && adminData.data.status !== constantUtil.ACTIVE) {
      throw new MoleculerError("Inactive account.. Kindly contact admin");
    }
    adminData = await this.adapter.updateById(
      mongoose.Types.ObjectId(adminData._id.toString()),
      {
        "data.otp": hashOtp,
      }
    );
    if (!adminData) {
      throw new MoleculerError("INVAID CREDENTIALS");
    }
    // context.emit("admin.sendSMS", {
    //   "phoneNumber": `${context.params.phoneCode}${context.params.phoneNumber}`,
    //   "message": "", // smsOtpTemplate
    // });

    if (
      generalSettings?.data?.emailConfigurationEnable === true &&
      adminData.data.email
    ) {
      this.broker.emit("admin.sendServiceMail", {
        //"clientId": clientId,
        "templateName": "AdminOtpVerification",
        ...mappingUtil.adminOtpMailObject({
          ...adminData.data.toJSON(),
          "otp": context.params.otp,
          //"clientId": clientId,
        }),
      });
    }

    responseData = {
      "email": adminData?.data?.email || "",
      "message": "",
      // "clientId": clientId,
      // 'OTP': context.params.otp,
      // smsSetting.data.mode === constantUtil.DEVELOPMENT
      //   ? context.params.otp
      //   : '',
    };
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData,
  };
};

adminAction.verifyOtp = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const checkData = {
    //"name": context.params.userType,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
    "data.phone.code": context.params.phoneCode,
    "data.phone.number": context.params.phoneNumber,
    "name": {
      "$nin": [
        constantUtil.REPORTS,
        constantUtil.BOOKINGSERVICEMANAGEMET,
        constantUtil.CALLCENTER,
      ],
    },
  };
  // const checkData =
  //   gerenalsetting.data.smsType === constantUtil.TWILIO
  //     ? {
  //         'data.phone.code': context.params.phoneCode,
  //         'data.phone.number': context.params.phoneNumber,
  //         'data.otp': cryptoUtils.hashSecret(context.params.otp),
  //       }
  //     : {
  //         'data.phone.code': context.params.phoneCode,
  //         'data.phone.number': context.params.phoneNumber,
  //       }
  const adminData = await this.adapter.model.findOne(checkData).lean();
  if (!adminData) {
    throw new MoleculerError("Invalid credentials");
  }
  //---------------------------
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  //---------------------------
  if (adminData?.data?.status !== constantUtil.ACTIVE) {
    throw new MoleculerError("Inactive account.. Kindly contact admin");
  }
  if (
    context.params.otp &&
    helperUtil.validPassword(context.params.otp, adminData.data.password)
  ) {
    const accessToken = jwtSign({
      "_id": adminData._id,
      "type": constantUtil.ADMIN,
      "deviceType": context.meta.userAgent.deviceType,
      "platform": context.meta.userAgent.platform,
    });

    // let updatedAdminData = await this.adapter.updateById(
    //   mongoose.Types.ObjectId(adminData._id.toString()),
    //   {
    //     "data.otp": "",
    //     // "data.accessToken": accessToken,
    //     // New future For Same Login Use Multiple System Only For COORPERATEOFFICE
    //     "data.accessToken":
    //       adminData.name === constantUtil.COORPERATEOFFICE
    //         ? adminData.data.accessToken
    //         : accessToken,
    //   }
    // );
    const updatedAdminData = await this.adapter.model
      .findOneAndUpdate(
        { "_id": mongoose.Types.ObjectId(adminData._id.toString()) },
        {
          "data.otp": "",
          // "data.accessToken": accessToken,
          // New future For Same Login Use Multiple System Only For COORPERATEOFFICE
          "data.accessToken":
            adminData.data.accessToken &&
            adminData.name === constantUtil.COORPERATEOFFICE
              ? adminData.data.accessToken
              : accessToken,
        },
        { "new": true }
      )
      .lean();
    // if (!updatedAdminData) throw new MoleculerError("Invalid data");

    // updatedAdminData = updatedAdminData.toJSON();
    // updatedAdminData.data.accessToken = accessToken;
    updatedAdminData.data.extraPrivileges =
      adminData.data.extraPrivileges || {};
    let privilageJson = null;
    if (adminData.name === constantUtil.OPERATORS) {
      privilageJson = await this.adapter.model
        .findOne({
          "_id": adminData.data.operatorRole,
          "data.status": constantUtil.ACTIVE,
        })
        .lean();
      if (!privilageJson) {
        throw new MoleculerError("Invalid Privilege");
      }
    } else if (
      adminData.name === constantUtil.HUBS ||
      adminData.name === constantUtil.HUBSEMPLOYEE
    ) {
      privilageJson = await this.adapter.model
        .findOne({
          // "name": constantUtil.HUBSPRIVILEGES,
          "name":
            adminData.name === constantUtil.HUBS
              ? constantUtil.HUBSPRIVILEGES
              : constantUtil.HUBSEMPLOYEEPRIVILEGES,
          "data.clientId": mongoose.Types.ObjectId(
            adminData?.data?.clientId?.toString()
          ),
        })
        .lean();
      if (!privilageJson) {
        throw new MoleculerError("Invalid Privilege");
      }
    } else if (adminData.name === constantUtil.RESPONSEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.OFFICEPRIVILEGES,
          "data.clientId": mongoose.Types.ObjectId(
            adminData?.data?.clientId?.toString()
          ),
        })
        .lean();
      if (!privilageJson) {
        throw new MoleculerError("Invalid Privilege");
      }
    } else if (adminData.name === constantUtil.COORPERATEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.COORPERATEPRIVILEGES,
          "data.clientId": mongoose.Types.ObjectId(
            adminData?.data?.clientId?.toString()
          ),
        })
        .lean();
      if (!privilageJson) {
        throw new MoleculerError("Invalid Privilege");
      }
    }
    updatedAdminData.data.privileges =
      privilageJson?.data?.privileges || generalSettings.data.privileges;
    updatedAdminData.data.defaultPrivileges =
      privilageJson?.data?.privileges || generalSettings.data.privileges;
    //Insert Signup log
    await this.broker.emit("log.insertSignupLog", {
      "type": constantUtil.CONST_LOG,
      "name": constantUtil.LOGIN,
      "userType": adminData.name,
      "adminId": adminData._id,
    });

    return mappingUtil.adminObject(updatedAdminData);
  } else {
    throw new MoleculerError(
      "Password Incorrect... kindly check and enter again..."
    );
  }
};
adminAction.getProfile = async function (context) {
  const clientId = context.params.clientId;
  //---------------------------
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  //---------------------------
  const adminData = await this.adapter.model
    .findOne({
      "_id": context.meta.adminId,
    })
    .lean();
  if (!adminData) {
    throw new MoleculerError("Invalid credentials");
  }
  adminData.data.extraPrivileges = adminData.data.extraPrivileges || {};
  let privilageJson = null;
  if (
    adminData.name !== constantUtil.DEVELOPER &&
    adminData.name !== constantUtil.ADMIN
  ) {
    if (adminData.name === constantUtil.OPERATORS) {
      privilageJson = await this.adapter.model
        .findOne({
          "_id": adminData.data.operatorRole,
        })
        .lean();
    } else if (
      adminData.name === constantUtil.HUBS ||
      adminData.name === constantUtil.HUBSEMPLOYEE
    ) {
      privilageJson = await this.adapter.model
        .findOne({
          // "name": constantUtil.HUBSPRIVILEGES,
          "name":
            adminData.name === constantUtil.HUBS
              ? constantUtil.HUBSPRIVILEGES
              : constantUtil.HUBSEMPLOYEEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else if (adminData.name === constantUtil.RESPONSEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.OFFICEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
    } else if (adminData.name === constantUtil.COORPERATEOFFICE) {
      privilageJson = await this.adapter.model
        .findOne({
          "name": constantUtil.COORPERATEPRIVILEGES,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        })
        .lean();
      // if (!privilageJson) {
      //   throw new MoleculerError("Invalid Privilege");
      // }
      // updatedAdminData.data.privileges = privilageJson.data.privileges;
      // updatedAdminData.data.defaultPrivileges = privilageJson.data.privileges;
    }
    if (!privilageJson) {
      throw new MoleculerError("Invalid Privilege");
    }
    adminData.data.privileges = privilageJson.data.privileges;
  }
  adminData.data.extraPrivileges = adminData.data.extraPrivileges || {};
  adminData.data.defaultPrivileges = generalSettings.data.privileges || {};
  return mappingUtil.adminObject(adminData);
};

adminAction.updateProfile = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const profileJson = await this.adapter.model
    .findOne({
      "_id": context.meta.adminId,
      "data.phone.code": context.params.phoneCode,
      "data.phone.number": context.params.phoneNumber,
    })
    .lean();
  if (!profileJson) {
    throw new MoleculerError("Invalid profile Details");
  }
  // console.log('this is profile json', profileJson)
  // common updation
  let query = {
    "data.email": context.params.email,
    "data.phone": {
      "code": context.params.phoneCode,
      "number": context.params.phoneNumber,
    },
    "data.password":
      !!context.params.password === true
        ? bcrypt.hashSync(context.params.password, bcrypt.genSaltSync(8), null)
        : profileJson.data.password,
  };

  if (
    profileJson.name === constantUtil.ADMIN ||
    profileJson.name === constantUtil.DEVELOPER ||
    profileJson.name === constantUtil.OPERATORS ||
    profileJson.name === constantUtil.HUBSEMPLOYEE
  ) {
    // update uploded file
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${context.params.firstName}${context.params.phoneNumber}${profileJson.name}`,
        }
      );
      context.params.avatar = s3Image[0];
    }
    // then updated data
    query = {
      ...query,
      "data.firstName": context.params.firstName,
      "data.lastName": context.params.lastName,
      "data.gender": context.params.gender,
      "data.avatar": context.params.avatar,
    };
  }
  if (profileJson.name === constantUtil.HUBS) {
    query = { ...query, "data.hubsName": context.params.hubsName };
  }
  if (profileJson.name === constantUtil.RESPONSEOFFICE) {
    query = { ...query, "data.officeName": context.params.officeName };
  }
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(profileJson._id.toString()),
    query
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return mappingUtil.adminObject(responseJson);
  }
};

adminAction.changePassword = async function (context) {
  let errorCode = 200,
    message = null,
    responseData = {};
  try {
    // const oldHashPassword = bcrypt.hashSync(
    //   context.params.oldPassword,
    //   bcrypt.genSaltSync(8),
    //   null
    // );
    const newHashPassword = bcrypt.hashSync(
      context.params.newPassword,
      bcrypt.genSaltSync(8),
      null
    );
    const profileJson = await this.adapter.model
      .findOne(
        {
          "_id": context.meta.adminId,
        },
        { "data.password": 1 }
      )
      .lean();
    if (!profileJson) {
      throw new MoleculerError("Invalid profile Details");
    }
    // if (newHashPassword !== oldHashPassword) {
    if (
      !helperUtil.validPassword(
        context.params.oldPassword,
        profileJson.data.password
      )
    ) {
      throw new MoleculerError(
        "Invalid Old Password. Please Re-Check the Password."
      );
    }
    responseData = await this.adapter.model
      .findOneAndUpdate(
        { "_id": mongoose.Types.ObjectId(context.params.id?.toString()) },
        { "data.password": newHashPassword },
        { "new": true }
      )
      .lean();
    // responseData = mappingUtil.adminObject(responseData);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "response": {}, // responseData,
  };
};

// SETTINGS GENERAL
adminAction.getGenerelConfig = async function (context) {
  let responseJson = null,
    timestamp = null;
  const clientId = context.params?.clientId || context.meta?.clientId;
  //---------------------------
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  //---------------------------
  const osType = context.params?.type || null; // For Only Mobile App (IOS & ANDROID)
  const serviceCategory = context.params.serviceCategory?.toUpperCase();
  if (serviceCategory) {
    responseJson = await storageUtil.read(serviceCategory);
  } else {
    // responseJson = await storageUtil.read(constantUtil.GENERALSETTING);
    responseJson = generalSettings;
  }
  //  "timestamp": generalSettings.data.
  switch (serviceCategory) {
    case constantUtil.PACKAGESETTING:
      {
        const id = responseJson?._id?.toString();
        timestamp = generalSettings.data.packageUpdateTime;
        responseJson = responseJson?.data || {};
        responseJson["_id"] = id;
      }
      break;

    case constantUtil.MAPSETTING:
      {
        const id = responseJson?._id?.toString();
        timestamp = generalSettings.data.mapKeyUpdateTime;
        responseJson = responseJson?.data || {};
        responseJson["_id"] = id;
      }
      break;

    case constantUtil.CONST_AIRPORTTRANSFERSETTING:
      {
        const id = responseJson?.[0]?._id?.toString();
        responseJson = responseJson?.[0]?.data || {};
        responseJson["_id"] = id;
      }
      break;

    case constantUtil.AIRPORT:
      timestamp = generalSettings.data.airportUpdateTime;
      break;

    case constantUtil.POPULARPLACE:
      timestamp = generalSettings.data.popularplaceUpdateTime;
      break;

    case constantUtil.TOLL:
      timestamp = generalSettings.data.tollUpdateTime;
      break;

    case constantUtil.COUPON:
      timestamp = generalSettings.data.couponUpdateTime;
      break;

    case constantUtil.CANCELLATIONREASON:
      timestamp = generalSettings.data.cancelReasonUpdateTime;
      break;
  }
  if (!responseJson) {
    throw new MoleculerError("Invalid Genereal Settings Data", 500);
  }
  // const decodedData = JSON.parse(lzStringDecode(responseJson.data.encodedData));
  // responseJson["data"] = decodedData;
  // responseJson["data"]["encodedData"] = "";
  if (osType) {
    if (context.params.appVersion) {
      responseJson = cryptoAESEncode(JSON.stringify(responseJson));
      responseJson = responseJson?.toString();
    } else {
      responseJson = responseJson?.data;
    }
    return {
      "code": 200,
      "message": constantUtil.SUCCESS,
      "data": {
        "response": responseJson,
        "timestamp": timestamp || Date.now(),
      },
    };
  } else {
    return responseJson;
  }
};

// SETTINGS SERVICE AREA
adminAction.getServiceAreaConfig = async function (context) {
  let responseJson = null,
    rawJsonData = null,
    encodedJsonData = null,
    timestamp = null;
  const clientId = context.params?.clientId || context.meta?.clientId;
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  const osType = context.params?.type || null; // For Only Mobile App (IOS & ANDROID)
  const appVersion = context.params.appVersion;
  const serviceCategory = context.params.serviceCategory?.toUpperCase() || "";
  if (serviceCategory) {
    // encodedJsonData = await this.adapter.model
    //   .find({
    //     "isActive": true,
    //     "isDeleted": false,
    //     "name": serviceCategory,
    //   })
    //   .populate("data.serviceAreaId", { "locationName": 1 })
    //   .lean();
    responseJson = await this.adapter.model.aggregate([
      {
        "$match": {
          "isActive": true,
          "isDeleted": false,
          "name": serviceCategory,
        },
      },
      {
        "$lookup": {
          "from": "categories",
          "localField": "data.serviceAreaId",
          "foreignField": "_id",
          "as": "serviceAreaList",
        },
      },
      {
        "$unwind": {
          "path": "$serviceAreaList",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$project": {
          "_id": 0,
          "id": "$_id",
          "serviceAreaName": "$serviceAreaList.locationName",
          "serviceAreaId": "$serviceAreaList._id",
          "phone": "$data.phone",
        },
      },
      { "$sort": { "serviceAreaName": 1 } },
    ]);
  } else {
    throw new MoleculerError("Invalid Service Area Settings Data", 500);
  }
  switch (serviceCategory) {
    case constantUtil.CALLCENTER:
      timestamp = generalSettings.data.callCenterUpdateTime;
      rawJsonData = responseJson || [];
      break;

    case constantUtil.MAPSETTING:
      timestamp = generalSettings.data.mapKeyUpdateTime;
      rawJsonData = responseJson.data || {};
      break;
  }
  if (!responseJson) {
    throw new MoleculerError("Invalid Service Area Settings Data", 500);
  }
  if (osType && appVersion) {
    encodedJsonData = cryptoAESEncode(JSON.stringify(rawJsonData));
    encodedJsonData = encodedJsonData?.toString();
    return {
      "code": 200,
      "message": constantUtil.SUCCESS,
      "data": {
        "response": encodedJsonData,
        "timestamp": timestamp || Date.now(),
      },
    };
  } else {
    return {
      "code": 200,
      "message": constantUtil.SUCCESS,
      "data": {
        "response": rawJsonData,
        "timestamp": timestamp || Date.now(),
      },
    };
  }
};

adminAction.getMapConfig = async function (context) {
  let responseData = {},
    timestamp = null,
    isClientExists = false;
  const clientId = context.params?.clientId || context.meta?.clientId;
  //---------------------------
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const mapSettings = await storageUtil.read(constantUtil.MAPSETTING);
  //  #endregion Get Config Settings
  //---------------------------
  const osType = context.params?.type || null; // W0S, I0S & A0S
  if (context.params.isEnableZervxMap) {
    isClientExists = await this.adapter.model.countDocuments({
      "name": constantUtil.BRSCLIENTS,
      "_id": mongoose.Types.ObjectId(clientId),
      "data.status": constantUtil.ACTIVE,
    });
  } else {
    isClientExists = true;
  }
  if (isClientExists) {
    responseData = {
      "mapSettings": {
        "autocomplete": mapSettings.data.autocomplete || {},
        "direction": mapSettings.data.direction || {},
        "locationMap": mapSettings.data.locationMap || {},
        "place": mapSettings.data.place || {},
        "snap": mapSettings.data.snap || {},
      },
    };
  } else {
    responseData = {
      "mapSettings": {
        "autocomplete": {},
        "direction": {},
        "locationMap": {},
        "place": {},
        "snap": {},
      },
    };
  }
  if (context.params.appVersion) {
    responseData = cryptoAESEncode(JSON.stringify(responseData));
    responseData = responseData?.toString();
  }
  timestamp = generalSettings.data.mapKeyUpdateTime;
  return {
    "code": 200,
    "message": constantUtil.SUCCESS,
    "data": responseData,
    "timestamp": timestamp || Date.now(),
  };
};

//#region Zervx Services
adminAction.getZervxServices = async function (context) {
  let responseData = {};
  const clientId = context.params?.clientId || context.meta?.clientId;
  responseData = await this.adapter.model
    .findOne(
      {
        "name": constantUtil.CONST_ZERVXSERVICES,
        //"data.clientId": mongoose.Types.ObjectId(clientId),
      },
      { "data.zervxServices": 1 }
    )
    .lean();
  return {
    "code": 200,
    "message": constantUtil.SUCCESS,
    "data": responseData.data || [],
  };
};

adminAction.getServiceTypeById = async function (context) {
  let responseData,
    selectedServiceCategoriesList = [],
    errorCode = 200,
    message = "",
    languageCode = "";
  const clientId = context.params?.clientId || context.meta?.clientId;
  languageCode = context.meta?.adminDetails?.languageCode;
  const { SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    responseData = await this.adapter.model
      .findOne(
        {
          "name": constantUtil.GENERALSETTING,
          "data.clientId": mongoose.Types.ObjectId(clientId),
        },
        {
          "_id": 1,
          "data.clientId": 1,
          "data.selectedServiceCategoriesList": 1,
          "data.selectedAllServiceType": 1,
          "data.selectedParentServiceType": 1,
          "data.selectedChildServiceType": 1,
          "data.selectedDispatchServiceType": 1,
        }
      )
      .lean();
    // selectedServiceCategoriesList = await this.adapter.model.aggregate([
    //   {
    //     "$match": {
    //       "name": constantUtil.GENERALSETTING,
    //       "data.clientId": mongoose.Types.ObjectId(clientId),
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "admins",
    //       "localField": "data.selectedAllServiceType",
    //       "foreignField": "name",
    //       // "let": { "prof": "$allServiceTypeList" },
    //       // "pipeline": [
    //       //   {
    //       //     "$match": {
    //       //       // "$expr": { "$eq": ["$$prof", "$_id"] },
    //       //       "$prof.data.clientId": mongoose.Types.ObjectId(clientId),
    //       //     },
    //       //   },
    //       //   // {
    //       //   //   "$project": {
    //       //   //     "vehicleRegistrationNumber": {
    //       //   //       "$arrayElemAt": ["$vehicles.plateNumber", 0],
    //       //   //     },
    //       //   //     "licenceNo": 1,
    //       //   //   },
    //       //   // },
    //       // ],
    //       "as": "allServiceTypeList",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$allServiceTypeList",
    //       // "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$project": {
    //       "_id": 0,
    //       "label": "$allServiceTypeList.data.name",
    //       "labelKey": "$allServiceTypeList.data.key",
    //       "value": "$allServiceTypeList.data.value",
    //       "parent": "$allServiceTypeList.data.parent",
    //       "imageUrl": "$allServiceTypeList.data.imageUrl",
    //     },
    //   },
    // ]);
    // if (responseData) {
    //   responseData["data"]["selectedServiceCategoriesList"] =
    //     selectedServiceCategoriesList?.map((item) => ({
    //       "value": item.value,
    //       "label": item.label,
    //       "labelKey": item.labelKey,
    //       "parent": item.parent,
    //       "imageUrl": item.imageUrl,
    //     })) || [];
    // }
    // if (responseData) {
    //   responseData["data"]["selectedServiceCategoriesList"] =
    //     selectedServiceCategoriesList?.map((item) => ({
    //       "value": item.data.value,
    //       "label": item.data.name,
    //       "labelKey": item.data.key,
    //       "parent": item.data.parent,
    //       "imageUrl": item.data.imageUrl,
    //     })) || [];
    // }
    selectedServiceCategoriesList = await this.adapter.model.aggregate([
      {
        "$match": {
          "name": { "$in": responseData.data?.selectedAllServiceType },
          "data.clientId": mongoose.Types.ObjectId(clientId),
        },
      },
      {
        "$project": {
          "_id": 0,
          "label": "$data.name",
          "labelKey": "$data.key",
          "value": "$data.value",
          "parent": "$data.parent",
          "imageUrl": "$data.imageUrl",
        },
      },
    ]);
    if (responseData) {
      responseData["data"]["selectedServiceCategoriesList"] =
        selectedServiceCategoriesList;
    }
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

adminAction.manageClientZervxServices = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId?.toString();
  const { SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    responseData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.id?.toString()),
          "name": constantUtil.GENERALSETTING,
        },
        {
          "data.selectedAllServiceType": context.params.selectedAllServiceType,
          "data.selectedParentServiceType":
            context.params.selectedParentServiceType,
          "data.selectedChildServiceType":
            context.params.selectedChildServiceType,
          "data.selectedDispatchServiceType":
            context.params.selectedDispatchServiceType,
        },
        { "new": true }
      )
      .lean();
    //-------------------------
    const selectedServiceCategoriesList =
      context.params.selectedServiceCategoriesList || [];
    await this.adapter.model.updateMany(
      {
        "type": constantUtil.CONST_SERVICETYPE,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      { "isActive": false }
    );
    await selectedServiceCategoriesList?.forEach(async (item) => {
      const isExists = await this.adapter.model.countDocuments({
        "type": constantUtil.CONST_SERVICETYPE,
        "name": item.value,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      });
      if (isExists) {
        await this.adapter.model.updateOne(
          {
            "type": constantUtil.CONST_SERVICETYPE,
            "name": item.value,
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
          {
            // "data": {
            //   "clientId": context.params.id?.toString(),
            //   "name": item.label || item.name,
            //   "value": item.value,
            //   "parent": item.parent || null,
            //   "imageUrl": item.imageUrl || null,
            //   "serviceAreaId": null,
            // },
            "isActive": true,
          }
        );
      } else {
        await this.adapter.model.create({
          "dataType": constantUtil.CLIENTDATA,
          "type": constantUtil.CONST_SERVICETYPE,
          "name": item.value,
          "data": {
            "clientId": clientId,
            "name": item.label || item.name,
            "key": item.labelKey || item.name,
            "value": item.value,
            "parent": item.parent || null,
            "imageUrl": item.imageUrl || null,
            "serviceAreaId": null,
          },
          "isActive": true,
        });
      }
    });
    responseData["id"] = context.params.id;
    await storageUtil.write(constantUtil.GENERALSETTING, responseData);
    // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    //   "type": constantUtil.GENERALSETTING,
    // });
    // await this.broker.emit("admin.updateDataInRedis", {
    //   "type": constantUtil.GENERALSETTING,
    // });
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion Zervx Services

adminAction.uploadGeneralImages = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let documents = await this.broker.emit("admin.uploadMultipleImageInSpaces", {
    "files": context.params.files,
    "fileName": `${clientId}_${constantUtil.GENERALSETTING}`,
  });
  documents = documents && documents[0];
  const responseJson = {};
  if (documents.length > 0) {
    await documents.forEach((docs) => {
      if (docs.includes("lightlogo")) responseJson.lightLogo = docs;
      if (docs.includes("darklogo")) responseJson.darkLogo = docs;
      if (docs.includes("mobilelogo")) responseJson.mobileLogo = docs;
      if (docs.includes("favicon")) responseJson.favicon = docs;
      if (docs.includes("brandimage")) responseJson.brandImage = docs;
      if (docs.includes("bookingappimage")) responseJson.bookingAppImage = docs;
    });
  }
  return responseJson;
};

adminAction.updateGenerelConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      },
      {
        "data.siteTitle": context.params.siteTitle,
        "data.siteUrl": context.params.siteUrl,
        "data.bookingPrefix": context.params.bookingPrefix,
        "data.clientCode": context.params.clientCode,
        // "data.clientId": context.params.clientId,
        "data.emailAddress": context.params.emailAddress,
        "data.driverRequestTimeout": context.params.driverRequestTimeout,
        "data.driverMinAge": context.params.driverMinAge,
        "data.responceOfficerMinAge": context.params.responceOfficerMinAge,
        "data.approxCalculationDistance":
          context.params.approxCalculationDistance,
        "data.approxCalculationTime": context.params.approxCalculationTime,
        "data.minimumWalletAmountToOnline":
          context.params.minimumWalletAmountToOnline || 0,
        "data.requestDistance": context.params.requestDistance || 1000,
        "data.retryRequestDistance":
          context.params.retryRequestDistance || 2000,
        "data.bookingRetryCount": context.params.bookingRetryCount || 0,
        "data.onlineStatusThreshold": context.params.onlineStatusThreshold || 1,
        "data.adminBookingExpiredThreshold":
          context.params.adminBookingExpiredThreshold || 1,
        "data.tipsMinAmount": context.params.tipsMinAmount,
        "data.tipsMaxAmount": context.params.tipsMaxAmount,
        "data.billingDays": context.params.billingDays,
        "data.arrayData.pageViewLimits":
          context.params.arrayData.pageViewLimits,
        "data.tipStatus": context.params.tipStatus || false,
        "data.driverCancelFeeStatus": context.params.driverCancelFeeStatus,

        "data.isCancelToNextProfessional":
          context.params.isCancelToNextProfessional || false,
        "data.isTailRideNeeded": context.params.isTailRideNeeded || false,
        "data.isEnableElectricVehicle":
          context.params.isEnableElectricVehicle || false,
        "data.isReferralNeeded": context.params.isReferralNeeded || false,
        "data.isOtpNeeded": context.params.isOtpNeeded || false,
        "data.isProduction": context.params.isProduction || false,
        "data.isTollNeeded": context.params.isTollNeeded || false,
        "data.isTollManual": context.params.isTollManual || false,
        "data.currencyCode": context.params.currencyCode,
        "data.currencySymbol": context.params.currencySymbol,
        "data.cronTimeZone": context.params.cronTimeZone,
        "data.isTollToDriver": context.params.isTollToDriver || false,

        "data.lightLogo": context.params.lightLogo,
        "data.darkLogo": context.params.darkLogo,
        "data.mobileLogo": context.params.mobileLogo,
        "data.favicon": context.params.favicon,
        // "data.firebaseAdmin": context.params.firebaseAdmin,
        "data.mapApi": context.params.mapApi,
        "data.mapApiArray": context.params.mapApiArray,
        // "data.spaces": context.params.spaces,
        "data.paymentSection": context.params.paymentSection,
        "data.appUpdateStatus":
          context.params.appUpdateStatus || constantUtil.NONE,
        "data.androidUserAppUpdateStatus":
          context.params.androidUserAppUpdateStatus || constantUtil.NONE,
        "data.androidDriverAppUpdateStatus":
          context.params.androidDriverAppUpdateStatus || constantUtil.NONE,
        "data.iosDriverAppUpdateStatus":
          context.params.iosDriverAppUpdateStatus || constantUtil.NONE,
        "data.iosUserAppUpdateStatus":
          context.params.iosUserAppUpdateStatus || constantUtil.NONE,
        "data.siteStatus": context.params.siteStatus,
        "data.siteStatusMessage": context.params.siteStatusMessage,
        "data.isCountryCodeNeeded": context.params.isCountryCodeNeeded,
        "data.androidVersionKey": context.params.androidVersionKey,
        "data.androidUserVersionKey": context.params.androidUserVersionKey || 0,
        "data.androidDriverVersionKey":
          context.params.androidDriverVersionKey || 0,
        "data.iosVersionKey": context.params.iosVersionKey,
        "data.iosUserVersionKey": context.params.iosUserVersionKey || 0,
        "data.iosDriverVersionKey": context.params.iosDriverVersionKey || 0,
        "data.stopLimit": context.params.stopLimit,
        "data.isSocketPolling": context.params.isSocketPolling || false,
        "data.isManualMeterNeeded": context.params.isManualMeterNeeded || false,
        "data.defaultDialCode": context.params.defaultDialCode,
        "data.redirectUrls": context.params.redirectUrls,
        "data.smsType": context.params.smsType,
        "data.distanceCalculateType": context.params.distanceCalculateType,
        "data.lastPriorityStatus": context.params.lastPriorityStatus,
        "data.lastPriorityCount": context.params.lastPriorityCount,
        "data.timeDurationThershold": context.params.timeDurationThershold,
        "data.turfDistanceThershold": context.params.turfDistanceThershold,

        "data.isRerouteEnable": context.params.isRerouteEnable,
        "data.isWaitingEnable": context.params.isWaitingEnable,
        "data.isAddressChangeEnable": context.params.isAddressChangeEnable,
        "data.isQuickTripNeeded": context.params.isQuickTripNeeded || false,
        "data.isDailyTripNeeded": context.params.isDailyTripNeeded || false,
        "data.isOutstationNeeded": context.params.isOutstationNeeded || false,
        "data.isRentalNeeded": context.params.isRentalNeeded || false,
        "data.isAirportTripNeeded": context.params.isAirportTripNeeded || false,
        "data.isCarpoolTripNeeded": context.params.isCarpoolTripNeeded || false,
        "data.isAmbulanceNeeded": context.params.isAmbulanceNeeded || false,
        "data.isSmallPackageNeeded":
          context.params.isSmallPackageNeeded || false,
        "data.isHandicapNeeded": context.params.isHandicapNeeded || false,
        "data.isChildseatNeeded": context.params.isChildseatNeeded || false,
        "data.isgenderTripNeeded": context.params.isgenderTripNeeded || false,
        "data.walletSendMinAmount": context.params.walletSendMinAmount || 50, //=> 50 is default value
        "data.walletSendMaxAmount": context.params.walletSendMaxAmount || 50000, // => 5000 is default value
        "data.walletMinAmountThreshold":
          context.params.walletMinAmountThreshold || 500,
        "data.withDrawMinAmount": context.params.withDrawMinAmount || 50,
        "data.withDrawMaxAmount": context.params.withDrawMaxAmount || 100,
        "data.walletRechargeMinAmount":
          context.params.walletRechargeMinAmount || 100,
        "data.walletRechargeMaxAmount":
          context.params.walletRechargeMaxAmount || 50000,
        "data.emailConfigurationEnable":
          context.params.emailConfigurationEnable,
        "data.smsConfigurationEnable": context.params.smsConfigurationEnable,
        "data.callCenterPhone": context.params.callCenterPhone,
        "data.defaultCountryCode": context.params.defaultCountryCode,
        "data.sosEmergencyNumber": context.params.sosEmergencyNumber,
        // "data.isVinNeeded": context.params.isVinNeeded,
        "data.documentExpireThreshold": context.params.documentExpireThreshold,
        "data.nextProfessionalThreshold":
          context.params.nextProfessionalThreshold,
        "data.adminAssignThreshold": context.params.adminAssignThreshold,
        "data.isMapOptimized": context.params.isMapOptimized,
        "data.isUserReroute": context.params.isUserReroute,
        "data.isDriverReroute": context.params.isDriverReroute,
        "data.paymentRedirectApiUrl": context.params.paymentRedirectApiUrl,
        "data.payoutOptions": context.params.payoutOptions,
        "data.bookingAlgorithm": context.params.bookingAlgorithm,
        "data.nearestAlgorithm": context.params.nearestAlgorithm,
        // "data.isHubNeeded": context.params.isHubNeeded,
        "data.isProfessionalSubscriptionModel":
          context.params.isProfessionalSubscriptionModel,
        "data.brandImage": context.params.brandImage,
        "data.bookingAppImage": context.params.bookingAppImage,
        "data.bookingAppHomePageMessage":
          context.params.bookingAppHomePageMessage,
        "data.companyAddress": context.params.companyAddress,
        "data.companyRegNumber": context.params.companyRegNumber,
        "data.companyCopyRight": context.params.companyCopyRight,
        "data.professionalPenaltyStatus":
          context.params.professionalPenaltyStatus,
        "data.redeemApp": context.params?.redeemApp || {},
        "data.driverArrivalThresholdDistance":
          context.params.driverArrivalThresholdDistance || 100,
        "data.driverCancelThresholdMins":
          context.params.driverCancelThresholdMins || 0,
        "data.defaultPayment": context.params.defaultPayment,
        "data.address": context.params.address,
        "data.location.lat": context.params.location.lat,
        "data.location.lng": context.params.location.lng,

        // good
        "updatedData": {
          "userType": constantUtil.ADMIN,
          "admin": context.meta.adminId,
        },
        "data.isEnableUserBlockList":
          context.params?.isEnableUserBlockList || false,
        "data.isEnableProfessionalBlockList":
          context.params?.isEnableProfessionalBlockList || false,
        "data.notifyAdmin": context.params?.notifyAdmin || {},
      },
      { "new": true }
    )
    .lean();
  // console.log('this is general settings responseData', responseJson)
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    responseJson["id"] = context.params.id;
    //  #region Update Config Settings
    await storageUtil.write(constantUtil.GENERALSETTING, responseJson);
    //  #endregion Update Config Settings
    await this.spacesS3Init();
    return responseJson;
  }
};
// APP SETTINGS
adminAction.updateAppConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      },
      {
        "$set": {
          // "data.requestDistance": context.params.requestDistance,
          // "data.approxCalculationDistance":
          //   context.params.approxCalculationDistance,
          // "data.approxCalculationTime": context.params.approxCalculationTime,
          // "data.minimumWalletAmountToOnline":
          //   context.params.minimumWalletAmountToOnline,
          // "data.retryRequestDistance": context.params.retryRequestDistance,
          // "data.bookingRetryCount": context.params.bookingRetryCount || 0,
          // "data.onlineStatusThreshold": context.params.onlineStatusThreshold || 1,
          "data.clientId": context.params.clientId,
          "data.secretKey": context.params.secretKey,
          "data.adminBookingExpiredThreshold":
            context.params.adminBookingExpiredThreshold || 1,
          "data.tipsMinAmount": context.params.tipsMinAmount,
          "data.tipsMaxAmount": context.params.tipsMaxAmount,
          "data.billingDays": context.params.billingDays,
          "data.arrayData.pageViewLimits":
            context.params.arrayData.pageViewLimits,
          "data.tipStatus": context.params.tipStatus || false,
          "data.driverCancelFeeStatus":
            context.params.driverCancelFeeStatus || false,

          // "data.isCancelToNextProfessional":
          //   context.params.isCancelToNextProfessional || false,
          "data.isTailRideNeeded": context.params.isTailRideNeeded || false,
          "data.isEnableElectricVehicle":
            context.params.isEnableElectricVehicle || false,
          "data.isReferralNeeded": context.params.isReferralNeeded || false,
          "data.isOtpNeeded": context.params.isOtpNeeded || false,
          "data.isProduction": context.params.isProduction || false,
          "data.isTollNeeded": context.params.isTollNeeded || false,
          "data.isTollManual": context.params.isTollManual || false,
          "data.uniqueCodeLength": context.params.uniqueCodeLength || 10,
          "data.currencyCode": context.params.currencyCode,
          "data.currencySymbol": context.params.currencySymbol,
          "data.cronTimeZone": context.params.cronTimeZone,
          "data.isTollToDriver": context.params.isTollToDriver || false,

          "data.firebaseAdmin": context.params.firebaseAdmin,
          // "data.mapApi": context.params.mapApi,
          "data.spaces": context.params.spaces,
          "data.paymentSection": context.params.paymentSection,
          // "data.appUpdateStatus": context.params.appUpdateStatus,
          "data.androidUserAppUpdateStatus":
            context.params.androidUserAppUpdateStatus || constantUtil.NONE,
          "data.androidDriverAppUpdateStatus":
            context.params.androidDriverAppUpdateStatus || constantUtil.NONE,
          "data.iosDriverAppUpdateStatus":
            context.params.iosDriverAppUpdateStatus || constantUtil.NONE,
          "data.iosUserAppUpdateStatus": context.params.iosUserAppUpdateStatus,
          "data.professionalPenaltyStatus":
            context.params.professionalPenaltyStatus,

          // "data.siteStatus": context.params.siteStatus,
          // "data.siteStatusMessage": context.params.siteStatusMessage,
          "data.isCountryCodeNeeded": context.params.isCountryCodeNeeded,
          // "data.androidVersionKey": context.params.androidVersionKey,
          "data.androidUserVersionKey":
            context.params.androidUserVersionKey || 0,
          "data.androidDriverVersionKey":
            context.params.androidDriverVersionKey || 0,
          // "data.iosVersionKey": context.params.iosVersionKey,
          "data.iosUserVersionKey": context.params.iosUserVersionKey || 0,
          "data.iosDriverVersionKey": context.params.iosDriverVersionKey || 0,
          "data.stopLimit": context.params.stopLimit,
          "data.isSocketPolling": context.params.isSocketPolling || false,
          "data.isManualMeterNeeded":
            context.params.isManualMeterNeeded || false,
          "data.defaultDialCode": context.params.defaultDialCode,
          // "data.redirectUrls": context.params.redirectUrls,
          "data.redirectUrls.paymentGatewayUrl":
            context.params.redirectUrls.paymentGatewayUrl,
          "data.redirectUrls.paymentRedirectApiUrl":
            context.params.redirectUrls.paymentRedirectApiUrl,
          "data.smsType": context.params.smsType,
          "data.distanceCalculateType": context.params.distanceCalculateType,
          "data.lastPriorityStatus": context.params.lastPriorityStatus,
          "data.lastPriorityCount": context.params.lastPriorityCount,
          "data.timeDurationThershold": context.params.timeDurationThershold,
          "data.turfDistanceThershold": context.params.turfDistanceThershold,
          // "data.isRerouteEnable": context.params.isRerouteEnable,
          "data.isWaitingEnable": context.params.isWaitingEnable,
          // "data.isAddressChangeEnable": context.params.isAddressChangeEnable,
          // "data.mapApiArray": context.params.mapApiArray,
          "data.isQuickTripNeeded": context.params.isQuickTripNeeded || false,
          "data.isDailyTripNeeded": context.params.isDailyTripNeeded || false,
          "data.isOutstationNeeded": context.params.isOutstationNeeded || false,
          "data.isRentalNeeded": context.params.isRentalNeeded || false,
          "data.isAirportTripNeeded":
            context.params.isAirportTripNeeded || false,
          "data.isCarpoolTripNeeded":
            context.params.isCarpoolTripNeeded || false,
          "data.isAmbulanceNeeded": context.params.isAmbulanceNeeded || false,
          "data.isSmallPackageNeeded":
            context.params.isSmallPackageNeeded || false,
          "data.isHandicapNeeded": context.params.isHandicapNeeded || false,
          "data.isChildseatNeeded": context.params.isChildseatNeeded || false,
          "data.isgenderTripNeeded": context.params.isgenderTripNeeded || false,
          "data.isPetAllowed": context.params.isPetAllowed || false,
          "data.emailConfigurationEnable":
            context.params.emailConfigurationEnable,
          "data.smsConfigurationEnable": context.params.smsConfigurationEnable,
          "data.callCenterPhone": context.params.callCenterPhone,
          "data.defaultCountryCode": context.params.defaultCountryCode,
          "data.defaultCountryCodeLong": context.params.defaultCountryCodeLong,
          "data.sosEmergencyNumber": context.params.sosEmergencyNumber,
          "data.isVinNeeded": context.params?.driverApp?.isVinNeeded || false,
          // "data.documentExpireThreshold": context.params.documentExpireThreshold,
          // "data.nextProfessionalThreshold":
          //   context.params.nextProfessionalThreshold,
          // "data.adminAssignThreshold": context.params.adminAssignThreshold,
          "data.isMapOptimized": context.params.isMapOptimized,
          "data.isUserReroute": context.params.isUserReroute,
          "data.isDriverReroute": context.params.isDriverReroute,
          "data.isShowRouteInfoInMap": context.params.isShowRouteInfoInMap,
          "data.paymentRedirectApiUrl": context.params.paymentRedirectApiUrl,
          "data.payoutOptions": context.params.payoutOptions,
          "data.bookingAlgorithm": context.params.bookingAlgorithm,
          "data.nearestAlgorithm": context.params.nearestAlgorithm,
          "data.isHubNeeded": context.params?.driverApp?.isHubNeeded || false,
          "data.isProfessionalSubscriptionModel":
            context.params.isProfessionalSubscriptionModel,
          "data.islicenceNoNeeded":
            context.params?.driverApp?.isNeededLicenceNo || false, //Need to Remove
          "data.isEnableLiveMeter": context.params.isEnableLiveMeter || false,
          "data.liveMeterAPIHitInterval":
            context.params.liveMeterAPIHitInterval,
          "data.liveMeterTurfInterruptInterval":
            context.params.liveMeterTurfInterruptInterval,
          "data.liveMeterMinInterruptThresholdDistance":
            context.params?.liveMeterMinInterruptThresholdDistance || 1000,
          "data.liveMeterMaxInterruptThresholdDistance":
            context.params?.liveMeterMaxInterruptThresholdDistance || 1000,
          "data.liveMeterMinInterruptThresholdTime":
            context.params?.liveMeterMinInterruptThresholdTime || 60,
          "data.liveMeterMaxInterruptThresholdTime":
            context.params?.liveMeterMaxInterruptThresholdTime || 60,
          "data.dispatchShowCalendarDays":
            context.params?.dispatchShowCalendarDays || 7,
          "data.dispatchShowCalendarTimeInterval":
            context.params?.dispatchShowCalendarTimeInterval || 15,
          "data.userApp": context.params?.userApp || {},
          "data.driverApp": context.params?.driverApp || {},
          "data.oneSignalApp": context.params?.oneSignalApp || {},
          "data.dispatchRide": context.params?.dispatchRide || {},
          // ----- InterCity Start -------
          "data.isEnableIntercityRide":
            context.params.isEnableIntercityRide || false,
          "data.isEnableIntercityBoundaryFareToProfessional":
            context.params.isEnableIntercityBoundaryFareToProfessional || false,
          "data.isCheckIntercityRideDropPoint":
            context.params?.isCheckIntercityRideDropPoint || false,
          "data.isCheckIntercityRidePickupPoint":
            context.params?.isCheckIntercityRidePickupPoint || false,
          // ----- InterCity End -------
          "data.isEnableIPRestriction":
            context.params?.isEnableIPRestriction || false,
          // good
          "data.isProfessionalsCorporateSiteCommissionCreditToWallet":
            context.params
              ?.isProfessionalsCorporateSiteCommissionCreditToWallet || false,
          "data.isEnableFranchising":
            context.params?.isEnableFranchising || false,
          "data.isEnableCorporateRideWalletPay":
            context.params?.isEnableCorporateRideWalletPay || false,
          "data.isEnableMultipleMapCalling":
            context.params?.isEnableMultipleMapCalling || false,
          "data.isEnableZervxMap": context.params?.isEnableZervxMap || false,
          "data.mapType": context.params?.mapType || null,
          "data.privileges": context.params?.privileges || {},
          "data.isEnableShareRide": context.params?.isEnableShareRide || false,
          "data.shareRideMaxThershold":
            context.params?.shareRideMaxThershold || 0,
          "data.shareRideAllowMinDistance":
            context.params?.shareRideAllowMinDistance || 0,
          "data.isEnableHeatmap": context.params?.isEnableHeatmap || false,
          "data.isEnableIncentive": context.params?.isEnableIncentive || false,
          "data.isEnableMultiRideRequestPopup":
            context.params?.isEnableMultiRideRequestPopup || false,
          "data.nationalIdCheck": context.params?.nationalIdCheck || {},
          "data.dbBackupType": context.params?.dbBackupType,
          "data.isEnableAirportTransfer":
            context.params?.isEnableAirportTransfer || false,
          "data.paymentMode": context.params?.paymentMode || [],
          "data.gateWay": context.params?.gateWay,
          "data.voiceAssistant": context.params?.voiceAssistant || {},
          "data.isErrorLogInDB": context.params?.isErrorLogInDB || false,
          "data.expiryDate": context.params.expiryDate
            ? new Date(context.params.expiryDate).setHours(23, 59, 59, 999)
            : null,
          "data.isEnableCronJob": context.params?.isEnableCronJob || false,
        },
      },
      { "new": true }
    )
    .lean();
  // console.log('this is general settings responseData', responseJson)
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    responseJson["id"] = context.params.id;
    //  #region Update Config Settings
    await storageUtil.write(constantUtil.GENERALSETTING, responseJson);
    //  #endregion Update Config Settings
    await this.spacesS3Init();
    return responseJson;
  }
};
// Common Config
adminAction.getCommonConfig = async function (context) {
  let responseJson = null;
  // let clientId = context.params.clientId || context.meta?.clientId;
  let secretKey = context.params.secretKey?.trim();
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const callSetting = await storageUtil.read(constantUtil.CALLSETTING);
  const mapSetting = await storageUtil.read(constantUtil.MAPSETTING);
  const { ERROR_GENEREL_SETTINGS } = notifyMessage.setNotifyLanguage(
    constantUtil.DEFAULT_LANGUAGE
  );
  if (!generalSettings || !generalSettings.data) {
    throw new MoleculerError(ERROR_GENEREL_SETTINGS);
  }
  const selectedAllServiceType =
      generalSettings.data?.selectedAllServiceType || [],
    clientId = generalSettings.data?.clientId || null;
  const selectedServiceCategoriesList = await this.adapter.model.aggregate([
    {
      "$match": {
        "name": { "$in": selectedAllServiceType },
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
    },
    {
      "$project": {
        "_id": 0,
        "label": "$data.name",
        "labelKey": "$data.key",
        "value": "$data.value",
        "parent": "$data.parent",
        "imageUrl": "$data.imageUrl",
      },
    },
  ]);
  const imageURLPath =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;
  const userType = context.params.userType;
  let responseData = {
    "id": generalSettings._id,
    "clientId": generalSettings.data.clientId,
    "clientCode": generalSettings.data.clientCode,
    "companyAddress": generalSettings.data.address,
    // // "secretKey":  "DoNotDisplay", //generalSettings.data.secretKey, // Must will not send this Key Value
    "tipStatus": generalSettings.data.tipStatus,
    "minimumWalletAmountToOnline":
      generalSettings?.data?.minimumWalletAmountToOnline,
    "paymentSection": generalSettings.data.paymentSection,
    "tipsMinAmount": generalSettings.data.tipsMinAmount,
    "tipsMaxAmount": generalSettings.data.tipsMaxAmount,
    "emailAddress": generalSettings.data.emailAddress,
    "mapApi": generalSettings.data.mapApi,
    "siteTitle": generalSettings.data.siteTitle,
    "siteUrl": generalSettings.data.siteUrl,
    "currencyCode": generalSettings.data.currencyCode,
    "currencySymbol": generalSettings.data.currencySymbol,
    "smsType": smsSetting.data.smsType,
    "mode": smsSetting.data.mode,
    // "smsSettings": smsSetting.data,
    "callSettings": callSetting.data,
    "isEnableZervxMap": generalSettings.data.isEnableZervxMap,
    "mapSettings": {
      "autocomplete": mapSetting.data.autocomplete || {},
      "direction": mapSetting.data.direction || {},
      "locationMap": mapSetting.data.locationMap || {},
      "place": mapSetting.data.place || {},
      "snap": mapSetting.data.snap || {},
    },
    "redirectUrls": generalSettings.data.redirectUrls,
    "appUpdateStatus": generalSettings?.data?.appUpdateStatus,
    "androidUserAppUpdateStatus":
      generalSettings?.data?.androidUserAppUpdateStatus,
    "androidDriverAppUpdateStatus":
      generalSettings?.data?.androidDriverAppUpdateStatus,
    "iosDriverAppUpdateStatus": generalSettings?.data?.iosDriverAppUpdateStatus,
    "iosUserAppUpdateStatus": generalSettings?.data?.iosUserAppUpdateStatus,
    "androidUserVersionKey": generalSettings?.data?.androidUserVersionKey,
    "androidDriverVersionKey": generalSettings?.data?.androidDriverVersionKey,
    "iosUserVersionKey": generalSettings?.data?.iosUserVersionKey,
    "iosDriverVersionKey": generalSettings?.data?.iosDriverVersionKey,
    "androidVersionKey": generalSettings.data.androidVersionKey,
    "iosVersionKey": generalSettings.data.iosVersionKey,
    "isSocketPolling": generalSettings.data.isSocketPolling,
    "isReferralNeeded": generalSettings.data.isReferralNeeded,
    "stopLimit": generalSettings.data.stopLimit,
    "isManualMeterNeeded": generalSettings.data.isManualMeterNeeded,
    "isOtpNeeded": generalSettings.data.isOtpNeeded,
    "isTollNeeded": generalSettings.data.isTollNeeded,
    "isTollManual": generalSettings.data.isTollManual,
    "isProduction": generalSettings.data.isProduction,
    "isCancelToNextProfessional":
      generalSettings.data.isCancelToNextProfessional,
    // "isWaitingEnable": generalSettings.data.isWaitingEnable,
    "siteStatus": generalSettings.data.siteStatus,
    "siteStatusMessage": generalSettings.data.siteStatusMessage,
    "lastPriorityStatus": generalSettings.data.lastPriorityStatus,
    "isRerouteEnable": generalSettings.data.isRerouteEnable,
    "isAddressChangeEnable": generalSettings.data.isAddressChangeEnable,
    //--------- Update Time Start ----
    "tollUpdateTime": generalSettings.data.tollUpdateTime,
    "couponUpdateTime": generalSettings.data.couponUpdateTime,
    "airportUpdateTime": generalSettings.data.airportUpdateTime,
    "popularplaceUpdateTime": generalSettings.data.popularplaceUpdateTime,
    "packageUpdateTime": generalSettings.data.packageUpdateTime,
    "mapKeyUpdateTime": generalSettings.data.mapKeyUpdateTime,
    "registrationSetupUpdateTime":
      generalSettings.data.registrationSetupUpdateTime,
    "callCenterUpdateTime": generalSettings.data.callCenterUpdateTime,
    "cancelReasonUpdateTime": generalSettings.data.cancelReasonUpdateTime,
    //--------- Update Time End ----
    "isQuickTripNeeded": generalSettings.data.isQuickTripNeeded,
    "isDailyTripNeeded": generalSettings.data.isDailyTripNeeded,
    "isOutstationNeeded": generalSettings.data.isOutstationNeeded,
    "isRentalNeeded": generalSettings.data.isRentalNeeded,
    "isAirportTripNeeded": generalSettings.data.isAirportTripNeeded,
    "isCarpoolTripNeeded": generalSettings.data.isCarpoolTripNeeded,
    "isAmbulanceNeeded": generalSettings.data.isAmbulanceNeeded,
    "isSmallPackageNeeded": generalSettings.data.isSmallPackageNeeded,
    "isHandicapNeeded": generalSettings.data.isHandicapNeeded,
    "isChildseatNeeded": generalSettings.data.isChildseatNeeded,
    "isgenderTripNeeded": generalSettings.data.isgenderTripNeeded,
    "isPetAllowed": generalSettings.data.isPetAllowed,
    "distanceCalculateType": generalSettings.data.distanceCalculateType,
    "walletSendMinAmount": generalSettings.data.walletSendMinAmount,
    "walletSendMaxAmount": generalSettings.data.walletSendMaxAmount,
    "callCenterPhone": generalSettings.data.callCenterPhone,
    "walletMinAmountThreshold": generalSettings.data.walletMinAmountThreshold,
    "withDrawMinAmount": generalSettings.data.withDrawMinAmount,
    "withDrawMaxAmount": generalSettings.data.withDrawMaxAmount,
    "walletRechargeMinAmount": generalSettings.data.walletRechargeMinAmount,
    "walletRechargeMaxAmount": generalSettings.data.walletRechargeMaxAmount,
    "emailConfigurationEnable": generalSettings.data.emailConfigurationEnable,
    "smsConfigurationEnable": generalSettings.data.smsConfigurationEnable,
    "defaultCountryCode": generalSettings.data.defaultCountryCode,
    "defaultDialCode": generalSettings.data.defaultDialCode,
    "sosEmergencyNumber": generalSettings.data.sosEmergencyNumber,
    "isVinNeeded": generalSettings.data.isVinNeeded,
    "androidUserServerKey": generalSettings.data.firebaseAdmin.androidUser,
    "androidDriverServerKey": generalSettings.data.firebaseAdmin.androidDriver,
    "iosUserServerKey": generalSettings.data.firebaseAdmin.iosUser,
    "iosDriverServerKey": generalSettings.data.firebaseAdmin.iosDriver,
    "documentExpireThreshold": generalSettings.data.documentExpireThreshold,
    "isMapOptimized": generalSettings.data.isMapOptimized,
    "isUserReroute": generalSettings.data.isUserReroute,
    "isDriverReroute": generalSettings.data.isDriverReroute,
    "isShowRouteInfoInMap": generalSettings.data.isShowRouteInfoInMap,
    "algorithimType": generalSettings.data.bookingAlgorithm,
    "isHubNeeded": generalSettings.data?.isHubNeeded,
    // "developmentMode": generalSettings.data.mode,
    // "zervxServices": generalSettings.data?.zervxServices,
    "zervxServices": selectedServiceCategoriesList,
    "imageURLPath": imageURLPath,
    "brandImage": generalSettings.data.brandImage,
    "islicenceNoNeeded": generalSettings?.data?.islicenceNoNeeded, //need to remove
    "isEnableElectricVehicle": generalSettings?.data?.isEnableElectricVehicle,
    "isEnableLiveMeter": generalSettings?.data?.isEnableLiveMeter,
    "userAppSettings": generalSettings?.data?.userApp,
    "driverAppSettings": generalSettings?.data?.driverApp,
    "oneSignalAppSettings": generalSettings?.data?.oneSignalApp,
    "redeemAppSettings": generalSettings?.data?.redeemApp,
    "dispatchRideSettings": generalSettings?.data?.dispatchRide,
    "driverArrivalThresholdDistance":
      generalSettings?.data?.driverArrivalThresholdDistance,
    "driverCancelThresholdMins":
      generalSettings?.data?.driverCancelThresholdMins,
    "requestDistance": generalSettings?.data?.requestDistance,
    "isEnableShareRide": generalSettings?.data?.isEnableShareRide,
    "isEnableHeatmap": generalSettings?.data?.isEnableHeatmap,
    "isEnableCorporateRideWalletPay":
      generalSettings?.data?.isEnableCorporateRideWalletPay,
    "isEnableIncentive": generalSettings?.data?.isEnableIncentive,
    "isEnableMultiRideRequestPopup":
      generalSettings?.data?.isEnableMultiRideRequestPopup,
    "onlineStatusThreshold": generalSettings?.data?.onlineStatusThreshold,
    "nationalIdCheckAPI": generalSettings?.data?.nationalIdCheck,
    "paymentMode": generalSettings?.data?.paymentMode,
    "voiceAssistant": {
      "isEnableLocationsSearch":
        generalSettings?.data?.voiceAssistant?.isEnableLocationsSearch || false,
      "isEnableActionChange": false,
    },
    "isEnableFixedChangeRide":
      generalSettings.data?.dispatchRide?.isEnableFixedChangeRide || false,
    "isProfessionalsCorporateSiteCommissionCreditToWallet":
      generalSettings.data
        ?.isProfessionalsCorporateSiteCommissionCreditToWallet || false,
  };
  if (context.params.appVersion) {
    responseData = cryptoAESEncode(JSON.stringify(responseData));
    responseData = responseData?.toString();
  }
  return {
    "code": 200,
    "data": responseData,
    "message": "",
  };
};

//MAP SETTINGS
adminAction.updateMapConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        // "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        // "data.mapApi": context.params.mapApi,
        "data.mapApiArray": context.params.mapApiArray,
        "data.locationMap": context.params.locationMap,
        "data.autocomplete": context.params.autocomplete,
        "data.direction": context.params.direction,
        "data.place": context.params.place,
        "data.snap": context.params.snap,

        // good
        "updatedData": {
          "userType": constantUtil.ADMIN,
          "admin": context.meta.adminId,
        },
      },
      { "new": true }
    )
    .lean();

  // const encodedData = lzStringEncode(context.params);
  // const responseJson = await this.adapter.model
  //   .findOneAndUpdate(
  //     {
  //       "_id": mongoose.Types.ObjectId(context.params.id.toString()),
  //     },
  //     {
  //       "data.encodedData": encodedData,

  //       // good
  //       "updatedData": {
  //         "userType": constantUtil.ADMIN,
  //         "admin": context.meta.adminId,
  //       },
  //     },
  //     { "new": true }
  //   )
  //   .lean();

  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    // const decodedData = JSON.parse(lzStringDecode(responseJson.data.encodedData));
    // responseJson["data"] = decodedData;
    // responseJson["data"]["encodedData"] = "";

    responseJson["id"] = context.params.id;
    await storageUtil.write(constantUtil.MAPSETTING, responseJson);
    // Setting Update for Mapkey Time
    const settingsUpdate = await this.adapter.model
      .findOneAndUpdate(
        {
          "name": constantUtil.GENERALSETTING,
          // "data.clientId": mongoose.Types.ObjectId(clientId),
        },
        { "data.mapKeyUpdateTime": Date.now() },
        { "new": true }
      )
      .lean();
    settingsUpdate["id"] = settingsUpdate._id;
    await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    return responseJson;
  }
};
// PACKAGE SETTINGS
adminAction.updatePackageConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "name": constantUtil.PACKAGESETTING,
        // "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "data.isEnableOTPVerification":
          context.params?.isEnableOTPVerification || false,
        "data.isEnableNoContactDelivery":
          context.params?.isEnableNoContactDelivery || false,
        "data.isEnablePetsAtMyHome":
          context.params?.isEnablePetsAtMyHome || false,
        //
        "data.userImage": context.params?.userImage || {},
        "data.professionalImagePickup":
          context.params?.professionalImagePickup || {},
        "data.professionalImageDelivery":
          context.params?.professionalImageDelivery || {},
        "data.packageReceiver": context.params?.packageReceiver || {},
        "data.services": context.params.services || [],
      },
      { "new": true }
    )
    .lean();
  // console.log('this is general settings responseData', responseJson)
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    responseJson["id"] = context.params.id;
    await storageUtil.write(constantUtil.PACKAGESETTING, responseJson);
    // Setting Update for Package Time
    const settingsUpdate = await this.adapter.model
      .findOneAndUpdate(
        {
          "name": constantUtil.GENERALSETTING,
          // "data.clientId": mongoose.Types.ObjectId(clientId),
        },
        { "data.packageUpdateTime": Date.now() },
        { "new": true }
      )
      .lean();
    settingsUpdate["id"] = settingsUpdate._id;
    await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    return responseJson;
  }
};
// FIREBASE SETTINGS
adminAction.updateFirebaseConfig = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseJson = {};
  try {
    if (!context.params.id) {
      throw new MoleculerError("Invalid credentials");
    }
    responseJson = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.id.toString()),
          "name": constantUtil.FIREBASESETTING,
        },
        {
          "data.firebaseConfig": context.params?.firebaseConfig || {},
          "data.firebaseNotifyConfig":
            context.params?.firebaseNotifyConfig || {},
        },
        { "new": true }
      )
      .lean();
    if (!responseJson) {
      throw new MoleculerError("Error in Update");
    } else {
      responseJson["id"] = context.params.id;
      await storageUtil.write(constantUtil.FIREBASESETTING, responseJson);
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "response": responseJson,
    "message": message,
  };
};
// AUTO BLOCK PROFESSIONAL
adminAction.updateBlockProfessionalConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "name": constantUtil.CONST_BLOCKPROFESSIONALSETUP,
      },
      {
        "data.expiryDays": context.params?.expiryDays,
        "data.thresholdValue": context.params?.thresholdValue,

        "data.fromValue": context.params?.fromValue,
        "data.toValue": context.params?.toValue,
        "data.blockDays": context.params?.blockDays,

        "data.fromValue1": context.params?.fromValue1,
        "data.toValue1": context.params?.toValue1,
        "data.blockDays1": context.params?.blockDays1,

        "data.fromValue2": context.params?.fromValue2,
        "data.toValue2": context.params?.toValue2,
        "data.blockDays2": context.params?.blockDays2,

        "data.fromValue3": context.params?.fromValue3,
        "data.toValue3": context.params?.toValue3,
        "data.blockDays3": context.params?.blockDays3,

        "data.isEnableNotification": context.params?.isEnableNotification,
        "data.acceptanceRate": context.params?.acceptanceRate,
        "data.rejectionRate": context.params?.rejectionRate,
        "data.cancellationRate": context.params?.cancellationRate,
      },
      { "new": true }
    )
    .lean();
  // console.log('this is general settings responseData', responseJson)
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    responseJson["id"] = context.params.id;
    await storageUtil.write(
      constantUtil.CONST_BLOCKPROFESSIONALSETUP,
      responseJson
    );
    // // Setting Update for Package Time
    // const settingsUpdate = await this.adapter.model
    //   .findOneAndUpdate(
    //     { "name": constantUtil.GENERALSETTING },
    //     {
    //       "data.packageUpdateTime": Date.now(),
    //     },
    //     { "new": true }
    //   )
    //   .lean();
    // settingsUpdate["id"] = settingsUpdate._id;
    // await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    return responseJson;
  }
};
//#region SETTINGS SMS
adminAction.getSMSConfigList = async function (context) {
  // const responseJson = await storageUtil.read(constantUtil.SMSSETTING);
  // if (!responseJson) throw new MoleculerError("Invalid SMS Settings Data");
  // return responseJson;
  const search = context?.params?.search ?? "";
  const query = {
    "name": {
      "$in": [
        constantUtil.SMSSETTING,
        constantUtil.WHATSUPSETTING,
        constantUtil.NOTIFYSMSSETTING,
        constantUtil.NOTIFYSMSTEMPLATE,
        constantUtil.PROMOTIONSMSSETTING,
        constantUtil.EMERGENCYSMSSETTING,
      ],
    },
    "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    query["createdAt"] = {
      "$gte": setDayStartHours(context.params.fromDate),
      "$lt": setDayEndHours(context.params.toDate),
    };
  }
  if (context.params?.filter) {
    query["isActive"] = context.params?.isActive;
  }
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.bookingFrom",
      "data.mode",
      "data.smsType",
      "data.smsTypeNotifications",
      "data.notifyMessage",
      "data.status",
    ],
  });
  const response = await this.adapter.find({
    "sort": "-updatedAt",
    "limit": parseInt(context.params.limit ?? 20),
    "offset": parseInt(context.params?.skip ?? 0),
    "query": query,
    "search": search,
    "searchFields": [
      "data.bookingFrom",
      "data.mode",
      "data.smsType",
      "data.smsTypeNotifications",
      "data.notifyMessage",
      "data.status",
    ],
  });

  return {
    count,
    response,
  };
};
adminAction.getSMSConfig = async function (context) {
  // const responseJson = await storageUtil.read(constantUtil.SMSSETTING);
  // if (!responseJson) throw new MoleculerError("Invalid SMS Settings Data");
  // return responseJson;
  const responseJson = await this.adapter.model
    .findById(context.params.id.toString())
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid SMS Settings Data");
  } else {
    return responseJson;
  }
};

adminAction.updateSMSConfig = async function (context) {
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    { "data": context.params.data }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  }
  //#region Update All Client Data to Redis
  // OTP SMSSETTING
  const smsResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.SMSSETTING })
    .lean();
  storageUtil.write(constantUtil.SMSSETTING, smsResponseJson);
  // NOTIFY SMSSETTING
  const notifySMSResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.NOTIFYSMSSETTING })
    .lean();
  storageUtil.write(constantUtil.NOTIFYSMSSETTING, notifySMSResponseJson);
  // NOTIFY SMS TEMPLATE
  const notifySMSTemplateResponseJson = await this.adapter.model
    .find({ "name": constantUtil.NOTIFYSMSTEMPLATE })
    .lean();
  storageUtil.write(
    constantUtil.NOTIFYSMSTEMPLATE,
    notifySMSTemplateResponseJson
  );
  // NOTIFY PROMOTION SETTING
  const promotionSMSTemplateResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.PROMOTIONSMSSETTING })
    .lean();
  storageUtil.write(
    constantUtil.PROMOTIONSMSSETTING,
    promotionSMSTemplateResponseJson
  );
  // NOTIFY EMERGENCY SETTING
  const emergencySMSTemplateResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.EMERGENCYSMSSETTING })
    .lean();
  storageUtil.write(
    constantUtil.EMERGENCYSMSSETTING,
    emergencySMSTemplateResponseJson
  );
  // NOTIFY WHATSUP SETTING
  const notifyWhatsupResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.WHATSUPSETTING })
    .lean();
  storageUtil.write(constantUtil.WHATSUPSETTING, notifyWhatsupResponseJson);
  //#endregion Update All Client Data to Redis
  return responseJson;
};

adminAction.changeSMSConfigActiveStatus = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": {
        "$in": [
          constantUtil.SMSSETTING,
          constantUtil.WHATSUPSETTING,
          constantUtil.NOTIFYSMSSETTING,
          constantUtil.NOTIFYSMSTEMPLATE,
          constantUtil.PROMOTIONSMSSETTING,
          constantUtil.EMERGENCYSMSSETTING,
        ],
      },
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "isActive": context.params.status === constantUtil.ACTIVE ? true : false,
      "data.status": context.params.status,
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  //#region Update All Client Data to Redis
  // OTP SMSSETTING
  const smsResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.SMSSETTING })
    .lean();
  storageUtil.write(constantUtil.SMSSETTING, smsResponseJson);
  // NOTIFY SMSSETTING
  const notifySMSResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.NOTIFYSMSSETTING })
    .lean();
  storageUtil.write(constantUtil.NOTIFYSMSSETTING, notifySMSResponseJson);
  // NOTIFY SMS TEMPLATE
  const notifySMSTemplateResponseJson = await this.adapter.model
    .find({ "name": constantUtil.NOTIFYSMSTEMPLATE })
    .lean();
  storageUtil.write(
    constantUtil.NOTIFYSMSTEMPLATE,
    notifySMSTemplateResponseJson
  );
  // NOTIFY PROMOTION SETTING
  const promotionSMSTemplateResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.PROMOTIONSMSSETTING })
    .lean();
  storageUtil.write(
    constantUtil.PROMOTIONSMSSETTING,
    promotionSMSTemplateResponseJson
  );
  // NOTIFY EMERGENCY SETTING
  const emergencySMSTemplateResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.EMERGENCYSMSSETTING })
    .lean();
  storageUtil.write(
    constantUtil.EMERGENCYSMSSETTING,
    emergencySMSTemplateResponseJson
  );
  // NOTIFY WHATSUP SETTING
  const notifyWhatsupResponseJson = await this.adapter.model
    .findOne({ "name": constantUtil.WHATSUPSETTING })
    .lean();
  storageUtil.write(constantUtil.WHATSUPSETTING, notifyWhatsupResponseJson);
  //#endregion Update All Client Data to Redis
  return {
    "code": 1,
    "response": {},
    "message": "Updated Successfully",
  };
};
//#endregion SETTINGS SMS
// SETTINGS CALL
adminAction.getCALLConfig = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const callResponseJson = await storageUtil.read(constantUtil.CALLSETTING);
  if (!callResponseJson) {
    throw new MoleculerError("Invalid Call Settings Data");
  } else {
    return callResponseJson;
  }
};
adminAction.updateCALLConfig = async function (context) {
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    {
      // "data": context.params.data,
      "data.callType": context.params.data.callType,
      "data.accountAccessKey": context.params.data.accountAccessKey,
      "data.authToken": context.params.data.authToken,
      "data.defaultCountryCode": context.params.data.defaultCountryCode,
      "data.virtualNumber": context.params.data.virtualNumber,
      "data.isNeedCallMasking": context.params.data.isNeedCallMasking,
      "data.mode": context.params.data.mode,
      "data.senderID": context.params.data.senderID,
      "data.isEnableInAppCalling":
        context.params?.data?.isEnableInAppCalling || false,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    //  #region Update Config Settings
    storageUtil.write(constantUtil.CALLSETTING, responseJson);
    //  #endregion Update Config Settings
    return responseJson;
  }
};
//#region SETTINGS CALL CENTER
adminAction.getCallCenterList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  const languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const query = {
      "name": constantUtil.CALLCENTER,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (context.params.city) {
      query["data.serviceAreaId"] = mongoose.Types.ObjectId(
        context.params.city.toString()
      );
    }
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    const searchCity = {
      "$match": {
        "data.serviceAreaId": context.params.city
          ? mongoose.Types.ObjectId(context.params.city)
          : { "$ne": null },
      },
    };
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "data.phone.number": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, searchCity, { "$count": "all" }],
          "response": [
            {
              "$lookup": {
                "from": "categories",
                "localField": "data.serviceAreaId",
                "foreignField": "_id",
                "as": "serviceAreaList",
              },
            },
            {
              "$unwind": {
                "path": "$serviceAreaList",
                "preserveNullAndEmptyArrays": true,
              },
            },
            searchCity,
            searchQuery,
            {
              "$project": {
                "_id": "$_id",
                "data.serviceArea": "$serviceAreaList.locationName",
                "data.phoneNumber": {
                  "$concat": ["$data.phone.code", " ", "$data.phone.number"],
                },
                "data.isActive": "$isActive",
              },
            },
            { "$sort": { "_id": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};
adminAction.manageCallCenter = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const {
    SOMETHING_WENT_WRONG,
    ALERT_CITY_ALREADY_EXISTS,
    INFO_INVALID_DETAILS,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const alredyExists = await this.adapter.model.countDocuments({
      "name": constantUtil.CALLCENTER,
      "data.serviceAreaId": context.params.serviceAreaId,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    });
    if (alredyExists && context.params.action === "add") {
      //create the new coupon with existing code code is invaid so throw error
      throw new MoleculerClientError(ALERT_CITY_ALREADY_EXISTS);
    } else if (context.params.action === "edit") {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    if (context.params.action === "edit") {
      const updateResponse = await this.adapter.model.updateOne(
        {
          "_id": context.params.id,
        },
        {
          "data": {
            //"clientId": context.params.clientId,
            "serviceAreaId": context.params.serviceAreaId,
            "phone": {
              "code": context.params.phoneCode,
              "number": context.params.phoneNumber,
            },
          },
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    } else if (context.params.action === "add") {
      const newRecord = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.CALLCENTER,
        "name": constantUtil.CALLCENTER,
        "data": {
          "clientId": context.params.clientId,
          "serviceAreaId": context.params.serviceAreaId,
          "phone": {
            "code": context.params.phoneCode,
            "number": context.params.phoneNumber,
          },
        },
        "isActive": true,
      });
      if (!newRecord) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newRecord._id?.toString();
    }
    await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
      "type": constantUtil.CALLCENTER,
    });
    await this.broker.emit("admin.updateDataInRedis", {
      "type": constantUtil.CALLCENTER,
    });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion SETTINGS CALL CENTER

//#region SETUP HEAT MAP
adminAction.getHeatMapList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  const languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const query = {
      "name": constantUtil.HEATMAP,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (context.params.city) {
      query["data.serviceAreaId"] = mongoose.Types.ObjectId(
        context.params.city.toString()
      );
    }
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    const searchCity = {
      "$match": {
        "data.serviceAreaId": context.params.city
          ? mongoose.Types.ObjectId(context.params.city)
          : { "$ne": null },
      },
    };
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "serviceAreaList.locationName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, searchCity, { "$count": "all" }],
          "response": [
            {
              "$lookup": {
                "from": "categories",
                "localField": "data.serviceAreaId",
                "foreignField": "_id",
                "as": "serviceAreaList",
              },
            },
            {
              "$unwind": {
                "path": "$serviceAreaList",
                "preserveNullAndEmptyArrays": true,
              },
            },
            searchCity,
            searchQuery,
            {
              "$project": {
                "_id": "$_id",
                "serviceArea": "$serviceAreaList.locationName",
                "peakFare": "$data.peakFare",
                "timeInterval": "$data.timeInterval",
                "searchRadius": "$data.searchRadius",
                "usedCount": "$data.usedCount",
                "isActive": "$isActive",
              },
            },
            { "$sort": { "_id": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageHeatMap = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const {
    SOMETHING_WENT_WRONG,
    ALERT_CITY_ALREADY_EXISTS,
    INFO_INVALID_DETAILS,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const alredyExists = await this.adapter.model.countDocuments({
      "name": constantUtil.HEATMAP,
      "data.serviceAreaId": context.params.serviceAreaId,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    });
    if (alredyExists && context.params.action === "add") {
      //create the new coupon with existing code code is invaid so throw error
      throw new MoleculerClientError(ALERT_CITY_ALREADY_EXISTS);
    } else if (context.params.action === "edit") {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    if (context.params.action === "edit") {
      const updateResponse = await this.adapter.model.updateOne(
        {
          "_id": context.params.id,
        },
        {
          "data": {
            "clientId": clientId,
            "serviceAreaId": context.params.serviceAreaId,
            "searchRadius": context.params.searchRadius,
            "timeInterval": context.params.timeInterval,
            "usedCount": context.params.usedCount || 0,

            "fromValue": context.params.fromValue,
            "fromValue1": context.params.fromValue1,
            "fromValue2": context.params.fromValue2,
            "fromValue3": context.params.fromValue3,
            "fromValue4": context.params.fromValue4,

            "toValue": context.params.toValue,
            "toValue1": context.params.toValue1,
            "toValue2": context.params.toValue2,
            "toValue3": context.params.toValue3,
            "toValue4": context.params.toValue4,

            "peakFare": context.params.peakFare,
            "peakFare1": context.params.peakFare1,
            "peakFare2": context.params.peakFare2,
            "peakFare3": context.params.peakFare3,
            "peakFare4": context.params.peakFare4,
          },
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    } else if (context.params.action === "add") {
      const newRecord = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.HEATMAP,
        "name": constantUtil.HEATMAP,
        "data": {
          "clientId": clientId,
          "serviceAreaId": context.params.serviceAreaId,
          "searchRadius": context.params.searchRadius,
          "timeInterval": context.params.timeInterval,
          "usedCount": context.params.usedCount || 0,

          "fromValue": context.params.fromValue,
          "fromValue1": context.params.fromValue1,
          "fromValue2": context.params.fromValue2,
          "fromValue3": context.params.fromValue3,
          "fromValue4": context.params.fromValue4,

          "toValue": context.params.toValue,
          "toValue1": context.params.toValue1,
          "toValue2": context.params.toValue2,
          "toValue3": context.params.toValue3,
          "toValue4": context.params.toValue4,

          "peakFare": context.params.peakFare,
          "peakFare1": context.params.peakFare1,
          "peakFare2": context.params.peakFare2,
          "peakFare3": context.params.peakFare3,
          "peakFare4": context.params.peakFare4,
        },
        "isActive": true,
      });
      if (!newRecord) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newRecord._id?.toString();
    }
    // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    //   "type": constantUtil.HEATMAP,
    // });
    await this.broker.emit("admin.updateDataInRedis", {
      "type": constantUtil.HEATMAP,
      "clientId": clientId,
    });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion SETUP HEAT MAP

//#region SETUP SERVICE TYPE
adminAction.getServiceTypeList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  const languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const query = {
      "type": constantUtil.CONST_SERVICETYPE,
      "isActive": true,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    };
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    // const searchCity = {
    //   "$match": {
    //     "data.name": context.params.filter
    //       ? context.params.filter
    //       : { "$ne": null },
    //   },
    // };
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "data.name": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, { "$count": "all" }],
          "response": [
            searchQuery,
            {
              "$project": {
                "_id": "$_id",
                "type": "$type",
                "name": "$data.name",
                "parent": "$data.parent",
                "child": "$data.value",
                "isActive": "$isActive",
              },
            },
            { "$sort": { "_id": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageServiceType = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG, INFO_ALREADY_EXISTS, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const alredyExists = await this.adapter.model.countDocuments({
      "_id": { "$ne": context.params.id },
      "type": constantUtil.CONST_SERVICETYPE,
      // "name": context.params.name,
      "data.name": context.params.name,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    });
    // if (alredyExists && context.params.action === "add") {
    if (alredyExists) {
      //create the new coupon with existing code code is invaid so throw error
      throw new MoleculerClientError(INFO_ALREADY_EXISTS);
    } else if (context.params.action === "edit") {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${clientId}_${context.params.id}${constantUtil.CONST_SERVICETYPE}${context.params.name}`,
          "oldFileName": context.params.imageUrl,
        }
      );
      context.params.imageUrl = s3Image[0];
    }
    if (context.params.action === "edit") {
      const updateResponse = await this.adapter.model.updateOne(
        { "_id": mongoose.Types.ObjectId(context.params.id) },
        {
          "$set": {
            "data.name": context.params.name,
            "data.imageUrl": context.params.imageUrl,
          },
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    }
    // else if (context.params.action === "add") {
    //   const newRecord = await this.adapter.model.create({
    //     "name": constantUtil.CONST_SERVICETYPE,
    //     "data": {
    //       "name": context.params.name,
    //     },
    //     "isActive": true,
    //   });
    //   if (!newRecord) {
    //     throw new MoleculerError(
    //       INFO_INVALID_DETAILS,
    //       500,
    //       SOMETHING_WENT_WRONG,
    //       context.params
    //     );
    //   }
    //   context.params["id"] = newRecord._id?.toString();
    // }
    // // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    // //   "type": constantUtil.CONST_SERVICETYPE,
    // // });
    // await this.broker.emit("admin.updateDataInRedis", {
    //   "type": constantUtil.CONST_SERVICETYPE,
    // });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion SETUP SERVICE TYPE

//#region SETUP COMMON CATEGORY
adminAction.getCommonCategoryDropDownList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    commonCategory = [];
  const languageCode = context.meta?.adminDetails?.languageCode;
  const type = context.params?.type || constantUtil.COMMON;
  const category = context.params.category;
  const clientId = context.params.clientId || context.meta?.clientId;
  //
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    commonCategory = await storageUtil.read(category);
    commonCategory = commonCategory.filter(
      (item) => item.data.clientId === clientId
    );
    //
    if ((commonCategory?.length || 0) > 0) {
      switch (category) {
        case constantUtil.FCMACCESSTOKEN:
          responseData = {
            "accessToken": commonCategory?.[0]?.accessToken || null,
          };
          break;

        default:
          commonCategory.filter((item) => {
            if (item.isActive) {
              responseData.push({
                "id": item._id,
                "label": item.data.name, // For Display Purpose
                "value": item.data.value, // For Constant Key Purpose
                "code": item.data.code,
                "description": item.data.description,
                "languageKeys": item.data.languageKeys,
              });
            }
          });
          break;
      }
    } else {
      responseData = await this.adapter.model.aggregate([
        {
          "$match": {
            "type": type, // constantUtil.COMMON,
            "name": context.params.category,
            "data.clientId": mongoose.Types.ObjectId(clientId),
            "isActive": true,
          },
        },
        {
          "$project": {
            "_id": 0,
            "id": "$_id",
            "label": "$data.name", // For Display Purpose
            "value": "$data.value", // For Constant Key Purpose
            "code": "$data.code",
            "description": "$data.description",
            "languageKeys": "$data.languageKeys",
          },
        },
      ]);
    }
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData || [],
  };
};

adminAction.getCommonCategoryList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  const languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const query = {
      "type": constantUtil.COMMON,
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "name":
        (context.params?.category || "") !== ""
          ? { "$eq": context.params?.category }
          : {
              "$in": [
                constantUtil.CONST_GENDER,
                constantUtil.CONST_WEEKDAYS,
                constantUtil.CONST_PASSENGERTYPE,
                constantUtil.CONST_LUGGAGETYPE,
                constantUtil.CONST_WATERBOTTLETYPE,
                constantUtil.CONST_LANGUAGE,
                constantUtil.CONST_REVIEWCOMMENTS,
                constantUtil.CONST_FUELTYPE,
              ],
            },
      // "isActive": true,
    };
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    // const searchCity = {
    //   "$match": {
    //     "data.name": context.params.filter
    //       ? context.params.filter
    //       : { "$ne": null },
    //   },
    // };
    const searchQuery = {
      "$match": {
        "type": constantUtil.COMMON,
        "data.clientId": mongoose.Types.ObjectId(clientId),
        "$or": [
          {
            "data.name": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, { "$count": "all" }],
          "response": [
            searchQuery,
            {
              "$project": {
                "_id": "$_id",
                "type": "$type",
                "typeName": "$name",
                "labelName": "$data.name",
                "value": "$data.value",
                "description": "$data.description",
                "clientId": "$data.clientId",
                "isActive": "$isActive",
              },
            },
            { "$sort": { "updatedAt": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageCommonCategory = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const action = context.params.action;
  const clientId = context.params.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG, INFO_ALREADY_EXISTS, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const checkDuplicateRecord = {
      "type": constantUtil.COMMON,
      "name": context.params.typeName,
      "data.value": context.params.value,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (action === constantUtil.ACTION_EDIT) {
      checkDuplicateRecord["_id"] = {
        "$ne": mongoose.Types.ObjectId(context.params.id),
      };
    }
    const alredyExists = await this.adapter.model.countDocuments(
      checkDuplicateRecord
    );
    // if (alredyExists && context.params.action === "add") {
    if (alredyExists) {
      throw new MoleculerClientError(INFO_ALREADY_EXISTS); // create the new coupon with existing code code is invaid so throw error
    } else if (action === constantUtil.ACTION_EDIT) {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    if (action === constantUtil.ACTION_EDIT) {
      const updateResponse = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.id),
        },
        {
          "name": context.params.typeName,
          "$set": {
            "data.clientId": clientId,
            "data.name": context.params.labelName,
            "data.value": context.params.value,
            "data.code": context.params.code,
            "data.description": context.params.description,
            "data.languageKeys": JSON.parse(context.params.languageKeys),
          },
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    } else if (action === constantUtil.ACTION_ADD) {
      const newRecord = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.COMMON,
        "name": context.params.typeName,
        "data": {
          "clientId": clientId,
          "name": context.params.labelName,
          "value": context.params.value,
          "code": context.params.code,
          "description": context.params.description,
          "languageKeys": JSON.parse(context.params.languageKeys),
        },
        "isActive": true,
      });
      if (!newRecord) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newRecord._id?.toString();
    }
    // // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    // //   "type": constantUtil.CONST_SERVICETYPE,
    // // });
    await this.broker.emit("admin.updateDataInRedis", {
      "type": constantUtil.COMMON,
      "category": context.params.typeName,
    });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion SETUP COMMON CATEGORY

//#region SETUP Landing Page
adminAction.getLandingPageList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  const languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const query = {
      "name": constantUtil.CONST_LANDINGPAGE,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (context.params.city) {
      query["data.serviceAreaId"] = mongoose.Types.ObjectId(
        context.params.city.toString()
      );
    }
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    const searchCity = {
      "$match": {
        "data.serviceAreaId": context.params.city
          ? mongoose.Types.ObjectId(context.params.city)
          : { "$ne": null },
      },
    };
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "serviceAreaList.locationName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, searchCity, { "$count": "all" }],
          "response": [
            {
              "$lookup": {
                "from": "categories",
                "localField": "data.serviceAreaId",
                "foreignField": "_id",
                "as": "serviceAreaList",
              },
            },
            {
              "$unwind": {
                "path": "$serviceAreaList",
                "preserveNullAndEmptyArrays": true,
              },
            },
            searchCity,
            searchQuery,
            {
              "$project": {
                "_id": "$_id",
                "serviceArea": "$serviceAreaList.locationName",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                "isActive": "$isActive",
              },
            },
            { "$sort": { "updatedAt": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageLandingPage = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params.clientId || context.meta?.clientId;
  const {
    SOMETHING_WENT_WRONG,
    ALERT_CITY_ALREADY_EXISTS,
    INFO_INVALID_DETAILS,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const alredyExists = await this.adapter.model.countDocuments({
      "type": constantUtil.CONST_LANDINGPAGE,
      "name": constantUtil.CONST_LANDINGPAGE,
      "data.serviceAreaId": context.params.serviceAreaId,
    });
    if (alredyExists && context.params.action === "add") {
      //create the new coupon with existing code code is invaid so throw error
      throw new MoleculerClientError(ALERT_CITY_ALREADY_EXISTS);
    } else if (context.params.action === "edit") {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    //--------------------------
    context.params.header = JSON.parse(context.params.header);
    context.params.selectedAllServiceType =
      context.params.selectedAllServiceType?.split(",") || []; // Save the Service Type Must be In Array Formate
    //--------------------------
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${context.params.id}${constantUtil.CONST_SERVICETYPE}${context.params.name}`,
          "oldFileName": context.params.header.imageUrl,
        }
      );
      context.params.header.imageUrl = s3Image[0];
    }
    if (context.params.action === "edit") {
      const updateResponse = await this.adapter.model.updateOne(
        {
          "_id": context.params.id,
        },
        {
          "data.serviceAreaId": context.params.serviceAreaId,
          "data.selectedAllServiceType": context.params.selectedAllServiceType,
          "data.header": context.params.header,
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    } else if (context.params.action === "add") {
      const newRecord = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.CONST_LANDINGPAGE,
        "name": constantUtil.CONST_LANDINGPAGE,
        "data": {
          "serviceAreaId": context.params.serviceAreaId,
          "selectedAllServiceType": context.params.selectedAllServiceType,
          "header": context.params.header,
        },
        "isActive": true,
      });
      if (!newRecord) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newRecord._id?.toString();
    }
    // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    //   "type": constantUtil.HEATMAP,
    // });
    // await this.broker.emit("admin.updateDataInRedis", {
    //   "type": constantUtil.HEATMAP,
    // });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

adminAction.getLandingPageTrendingSliderList = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const { SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    const type = (context.params?.type || constantUtil.COUPON).toUpperCase();
    const dataList = await this.adapter.model
      .find(
        {
          "name": type,
          // "data.serviceAreaId": context.params?.serviceAreaId,
          // "data.serviceArea": context.params?.serviceAreaId,
          "data.status": constantUtil.ACTIVE,
        },
        {
          "_id": 1,
          "data.Title": 1,
          "data.couponTitle": 1,
          "data.couponDescription": 1,
          "data.couponCode": 1,
          "data.imageUrl": 1,
        }
      )
      // .sort({ "locationName": 1, "categoryName": 1 })
      .collation({ "locale": "en", "caseLevel": true })
      .lean();
    if (!dataList) {
      throw new MoleculerError(INFO_INVALID_DETAILS, 400);
    }
    // responseData["count"] = parseInt(serviceCategory.length);
    // responseData["response"] = serviceCategory.map((serviceLocation) => ({
    responseData = dataList.map((eachItem) => ({
      "_id": eachItem._id,
      "value": eachItem._id,
      "label": eachItem.data.couponTitle,
      "description": eachItem.data.couponDescription,
      "couponCode": eachItem.data.couponCode,
      "imageUrl": eachItem.data.imageUrl,
    }));
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#endregion SETUP Landing Page

// SETTINGS SMTP
adminAction.getSMTPConfig = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.SMTPSETTING,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid SMS Settings Data");
  } else {
    return responseJson;
  }
};
adminAction.updateSMTPConfig = async function (context) {
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    {
      "data": context.params.data,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    //  #region Update Config Settings
    storageUtil.write(constantUtil.SMTPSETTING, responseJson);
    //  #endregion Update Config Settings
    return responseJson;
  }
};

// VEHICLE CATEGORY
adminAction.updateVehicleCategory = async function (context) {
  let responseJson;
  const clientId = context.params.clientId || context.meta?.clientId;
  if (context.params.files && context.params.files.length > 0) {
    let documents = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `${context.params.vehicleCategory}${constantUtil.VEHICLECATEGORY}`,
      }
    );
    documents = documents && documents[0];
    if (documents.length > 0) {
      await documents.forEach((docs) => {
        if (docs.includes("categoryimage"))
          context.params.categoryImage = docs.toString();
        if (docs.includes("categorymapimage"))
          context.params.categoryMapImage = docs.toString();
      });
    }
  }
  const vehicleJson = await this.adapter.model
    .findOne({
      "name": constantUtil.VEHICLECATEGORY,
      "data.vehicleCategory": context.params.vehicleCategory,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (
      vehicleJson &&
      vehicleJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Vehicle Category Already Exists"); //INFO_VEHICLE_CATEGORY_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "recordNo": context.params.recordNo,
        "data": {
          "clientId": clientId,
          "vehicleCategory": context.params.vehicleCategory,
          "mandatoryShare": context.params.mandatoryShare,
          "seatCount": context.params.seatCount,
          // "childSeatCount": context.params.childSeatCount,
          "sharePercent": context.params.sharePercent,
          "shareStatus": context.params.shareStatus,
          "categoryImage": context.params.categoryImage,
          "categoryMapImage": context.params.categoryMapImage,
          "isEnableLuggage": context.params.isEnableLuggage,
          "luggageCount": context.params.luggageCount,
          "status": context.params.status,
          "languageKeys": JSON.parse(context.params.languageKeys),
        },
      }
    );
  } else {
    if (vehicleJson) {
      throw new MoleculerError("Vehicle Category Already Exists"); //INFO_VEHICLE_CATEGORY_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.VEHICLECATEGORY,
      "name": constantUtil.VEHICLECATEGORY,
      "recordNo": context.params.recordNo,
      "data": {
        "clientId": clientId,
        "vehicleCategory": context.params.vehicleCategory,
        "mandatoryShare": context.params.mandatoryShare,
        "seatCount": context.params.seatCount,
        // "childSeatCount": context.params.childSeatCount,
        "sharePercent": context.params.sharePercent,
        "shareStatus": context.params.shareStatus,
        "categoryImage": context.params.categoryImage,
        "categoryMapImage": context.params.categoryMapImage,
        "isEnableLuggage": context.params.isEnableLuggage,
        "luggageCount": context.params.luggageCount,
        "status": context.params.status,
        "languageKeys": JSON.parse(context.params.languageKeys),
      },
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  }
  if (
    vehicleJson?.data?.shareStatus !== context.params.shareStatus ||
    vehicleJson?.data?.mandatoryShare !== context.params.mandatoryShare
  ) {
    await this.broker.emit(
      "professional.updateAllVehiclesShareableDetailsUsingCategoryId",
      {
        ...context.params,
        "vehicleCategoryId": context.params.id,
        // "shareStatus": context.params.shareStatus,
        "mandatoryShare": context.params.mandatoryShare,
        "clientId": clientId,
      }
    );
    // Share Ride Config
    await this.broker.emit("category.updateVehicleShareRideConfigDetails", {
      ...context.params,
      "vehicleCategoryId": context.params?.id?.toString(),
      "isSupportShareRide": context.params?.shareStatus || false,
      "clientId": clientId,
    });
    // const session = await mongoose.startSession();
    // try {
    //   session.startTransaction();
    //   await adminModel.updateOne(
    //     {
    //       "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    //     },
    //     {
    //       "recordNo": context.params.recordNo,
    //       "data": {
    //         "vehicleCategory": context.params.vehicleCategory,
    //         "mandatoryShare": context.params.mandatoryShare,
    //         "seatCount": context.params.seatCount,
    //         "sharePercent": context.params.sharePercent,
    //         "shareStatus": context.params.shareStatus,
    //         "categoryImage": context.params.categoryImage,
    //         "categoryMapImage": context.params.categoryMapImage,
    //         "status": context.params.status,
    //       },
    //     },
    //     {
    //       session,
    //     }
    //   );

    //   const responseJsonX = await professionalModel
    //     .updateMany(
    //       {
    //         "vehicles.vehicleCategoryId": mongoose.Types.ObjectId(
    //           context.params.id
    //         ),
    //       },
    //       {
    //         "$set": {
    //           "vehicles.$.isEnableShareRide": context.params.mandatoryShare,
    //         },
    //       },
    //       { "new": true },
    //       { session }
    //     )
    //     .lean();

    //   const fff = "";
    //   await session.commitTransaction();

    //   console.log("success");
    // } catch (error) {
    //   console.log("error" + error.message);
    //   await session.abortTransaction();
    // }
    // session.endSession();
  }
  // LOCAL REDIS STORAGE WORK
  const categoryJson = await this.adapter.model
    .find({
      "name": constantUtil.VEHICLECATEGORY,
    })
    .lean();

  const vehicleCategoryObject = {};
  categoryJson.forEach((vehicle) => {
    vehicleCategoryObject[vehicle._id] = {
      "_id": vehicle._id,
      "name": constantUtil.VEHICLECATEGORY,
      "data": {
        "status": vehicle.data.status,
        "clientId": vehicle.data.clientId,
      },
      "recordNo": vehicle.recordNo,
      ...vehicle.data,
      "createdAt": vehicle.createdAt,
      "updatedAt": vehicle.updatedAt,
    };
  });
  storageUtil.write(constantUtil.VEHICLECATEGORY, vehicleCategoryObject);
  // LOCAL REDIS STORAGE WORK
  return responseJson;
};
adminAction.getVehicleCategoryList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const search = context.params?.search || "";

  const query = {
    "name": constantUtil.VEHICLECATEGORY,
    "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.vehicleCategory"],
  });
  const vehicleCategoryJson = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.vehicleCategory"],
  });

  responseJson["count"] = count;

  responseJson["response"] = vehicleCategoryJson;
  return responseJson;
};

adminAction.editVehicleCategory = async function (context) {
  let responseJson;
  const vehicleCategoryJson = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  Object.keys(vehicleCategoryJson).map((key) => {
    if (key.toString() == context.params.id.toString())
      responseJson = {
        "_id": key,
        ...vehicleCategoryJson[key],
      };
  });
  return responseJson;
  // let responseJson = null;
  // const vehicleCategoryJson = await this.adapter.model
  //   .find({
  //     "name": constantUtil.VEHICLECATEGORY,
  //     "_id": mongoose.Types.ObjectId(context.params.id.toString()),
  //     "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  //   })
  //   .lean();
  // if (vehicleCategoryJson) {
  //   responseJson = {
  //     "_id": vehicleCategoryJson?._id?.toString(),
  //     ...vehicleCategoryJson,
  //   };
  // }
  // return responseJson;
};

adminAction.changeVehicleCategoryStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.VEHICLECATEGORY,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified)
    throw new MoleculerError("Error in status change");
  // LOCAL REDIS STORAGE WORK
  const categoryJson = await this.adapter.model
    .find({
      "name": constantUtil.VEHICLECATEGORY,
    })
    .lean(); // Update All Client Data in Redis

  const vehicleCategoryObject = {};
  categoryJson.forEach((vehicle) => {
    vehicleCategoryObject[vehicle._id] = {
      "_id": vehicle._id,
      "name": constantUtil.VEHICLECATEGORY,
      "data": {
        "status": vehicle.data.status,
        "clientId": vehicle.data.clientId,
      },
      "recordNo": vehicle.recordNo,
      ...vehicle.data,
      "createdAt": vehicle.createdAt,
      "updatedAt": vehicle.updatedAt,
    };
  });
  storageUtil.write(constantUtil.VEHICLECATEGORY, vehicleCategoryObject);
  // LOCAL REDIS STORAGE WORK
  return { "code": 1, "response": "Updated Successfully" };
};
adminAction.getAllVehicleCategory = async function (context) {
  const status = context.params.status || null;
  const clientId = context?.params?.clientId;
  const vehicleCategoryJson = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  let responseJson = Object.keys(vehicleCategoryJson)?.map((key) => {
    if (status === null || status === vehicleCategoryJson[key].status) {
      return {
        "_id": key,
        "clientId": vehicleCategoryJson[key].clientId,
        "vehicleCategory": vehicleCategoryJson[key].vehicleCategory,
        "recordNo": vehicleCategoryJson[key].recordNo,
        "isEnableShareRide": vehicleCategoryJson[key].shareStatus || false,
        "status": vehicleCategoryJson[key].status,
      };
    }
  });
  responseJson = responseJson?.filter((each) => {
    if (each?._id) {
      return each;
    }
  });
  return responseJson || [];
};
adminAction.getEagleVehicleCategory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const vehicleCategoryJson = await this.adapter.model
    .find({
      "name": constantUtil.VEHICLECATEGORY,
      "data.status": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .sort({ "data.vehicleCategory": 1 })
    .collation({ "locale": "en", "caseLevel": true });
  return vehicleCategoryJson.map((data) => {
    return {
      "_id": data._id,
      "name": data.data.vehicleCategory,
    };
  });
};
adminAction.getCityBasedVehicleCategory = async function (context) {
  let categoryData;
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.getServiceCategoryById", {
    "id": context.params.city,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  }
  if (responseJson?.data?.categoryData) {
    categoryData = responseJson?.data?.categoryData?.vehicles;
  }
  const vehicleCategoryJson = await this.adapter.model
    .find(
      {
        "name": constantUtil.VEHICLECATEGORY,
        "data.status": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "_id": 1,
        "data": 1,
      }
    )
    .lean();
  const finalValue = [];
  categoryData.forEach((cat) => {
    vehicleCategoryJson.forEach((categorys) => {
      if (cat.categoryId.toString() === categorys._id.toString()) {
        finalValue.push({
          "_id": categorys._id.toString(),
          "name": categorys.data.vehicleCategory,
          "isEnableShareRide": categorys?.data?.shareStatus || false,
          "isEnableLuggage": categorys?.data?.isEnableLuggage || false,
          "isHasAC": categorys?.data?.isHasAC || false,
        });
      }
    });
  });

  return finalValue;
};

adminAction.getActiveServiceCategoryBasedOnServiceAreaId = async function (
  context
) {
  // let categoryData;
  // const responseJson = await this.broker.emit("category.getServiceCategoryById", {
  //   "id": context.params.city,
  // });
  // if (responseJson[0].code === 0)
  //   throw new MoleculerError(responseJson[0].message);

  // if (
  //   responseJson[0] &&
  //   responseJson[0].data &&
  //   responseJson[0].data.categoryData
  // )
  //   categoryData = responseJson[0].data.categoryData.vehicles;
  const clientId = context.params.clientId || context.meta.clientId;
  const serviceCategoryJson = await this.adapter.model.find(
    {
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.serviceAreaId": mongoose.Types.ObjectId(context.params.city),
      "name": constantUtil.CONST_REGISTRATIONSETTING,
      "isActive": true,
    },
    {
      "_id": 1,
      "data": 1,
    }
  );
  const finalValue = [];
  // categoryData.forEach((cat) => {
  serviceCategoryJson.forEach((item) => {
    // if (cat.categoryId.toString() === categorys._id.toString())
    {
      finalValue.push({
        "_id": item._id.toString(),
        "name": item.data.title,
        "serviceType": item?.data?.serviceType || constantUtil.CONST_RIDE,
      });
    }
  });
  // });

  return finalValue;
};
// SERVICE CATEGORY
adminAction.updateServiceCategory = async function (context) {
  const clientId = context.params.data.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.updateServiceCategory", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson?.data || {};
  }
};
adminAction.manageSpecialDays = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    redisData;

  try {
    if (context.params.actionType === ADD) {
      // const checkCount = await this.adapter.model.count({
      //   "name": CONST_SPECIALDAYS,
      //   "data.serviceAreaId": context.params.serviceAreaId,
      // });

      // if (checkCount === 0 || checkCount >= 0) {
      // return;
      const regionalDate = toRegionalUTC(
        new Date(context.params.specialDays.date)
      );
      const date = regionalDate.substring(0, 11);
      const startTime = context.params.specialDays.startTime.substring(11);
      const endTime = context.params.specialDays.endTime.substring(11);
      const manageDays = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": CONST_SPECIALDAYS,
        "name": CONST_SPECIALDAYS,
        "recordId": mongoose.Types.ObjectId(),
        //"data.clientId": context.params.clientId,
        "data.serviceAreaId": context.params.serviceAreaId,
        // "data.specialDays": context.params.specialDays,
        "data.specialDays.status": true,
        "data.specialDays.date": regionalDate,
        "data.specialDays.startTime": date + "" + startTime,
        "data.specialDays.endTime": date + "" + endTime,
        "data.specialDays.notes": context.params.specialDays.notes,
        "data.specialDays.peakFare": parseFloat(
          context.params.specialDays.peakFare
        ),
      });
      // } else {
      //   const manageDays = await this.adapter.model.update(
      //     {
      //       "name": CONST_SPECIALDAYS,
      //       "data.serviceAreaId": context.params.serviceAreaId,
      //     },
      //     {
      //       "$push": { "data.specialDays": context.params.specialDays },
      //     }
      //   );
      // }
    } else if (context.params.actionType === "DELETE") {
      const manageDays = await this.adapter.model.updateOne(
        {
          "name": CONST_SPECIALDAYS,
          //"data.clientId": context.params.clientId,
          "data.serviceAreaId": context.params.serviceAreaId,
          "recordId": context.params.recordId,
          "data.specialDays.date": context.params.specialDays.date,
        },
        {
          "data.specialDays.status": false,
        }
      );
    }

    // responseData = await this.adapter.model
    //   .find(
    //     {
    //       "name": CONST_SPECIALDAYS,
    //       "data.serviceAreaId": context.params.serviceAreaId,
    //       "data.specialDays.status": true,
    //       "data.specialDays.date": {
    //         "$gte": helperUtil.toRegionalUTC(new Date()),
    //         // "$gte": new Date(),
    //         // "$gte": { "format": "%Y-%m-%d", "date": "data.specialDays.date" },
    //       },
    //     },
    //     {
    //       "_id": 1,
    //       "name": 1,
    //       "data.serviceAreaId": 1,
    //       "data.specialDays": 1,
    //       // "data.specialDays.date": {
    //       //   "$dateToString": { "format": "%Y-%m-%d" },
    //       // },
    //       // "data.specialDays.startTime": 1,
    //       // "data.specialDays.endTime": 1,
    //       "recordId": 1,
    //       // "data.specialDays.date": 1,
    //       // "data.specialDays.startTime": 1,
    //       // "data.specialDays.endTime": 1,
    //     }
    //   )
    //   .lean();

    const responseRedisData = await this.adapter.model
      .find(
        {
          "name": CONST_SPECIALDAYS,
          "data.specialDays.status": true,
          // "data.specialDays.date": {
          //   "$gte": helperUtil.toRegionalUTC(new Date().setDate(-1)),
          //   // "$gte": new Date(),
          // },
          "createdAt": {
            // "$gte": new Date().setHours(0, 0, 0, 0),
            "$gte": new Date(
              new Date().setDate(new Date().getDate() - 1)
            ).setHours(0, 0, 0, 0),
          },
        },
        {
          "_id": 1,
          "name": 1,
          "data.serviceAreaId": 1,
          "data.specialDays": 1,
          "recordId": 1,
        }
      )
      .lean();

    await storageUtil.write(constantUtil.CONST_SPECIALDAYS, responseRedisData);
    message = constantUtil.SUCCESS;
    redisData = await storageUtil.read(constantUtil.CONST_SPECIALDAYS);
    responseData = await responseRedisData.filter((e) => {
      return e.data.serviceAreaId?.toString() === context.params.serviceAreaId;
    });
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }

  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
  // return responseData;
};
adminAction.getAllServiceCategory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.getAllServiceCategory", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson?.data || [];
  }
};
adminAction.getAllServiceActiveInactiveCategory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.getAllServiceActiveInactiveCategory",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson?.data || [];
  }
};
adminAction.getServiceCategoryList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.getServiceCategoryList", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson.data || [];
  }
};
adminAction.getServiceCategoryById = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.getServiceCategoryById", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson?.message);
  } else {
    return responseJson?.data || {};
  }
};
adminAction.getAllServiceMultiPoint = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.getAllServiceMultiPoint",
    { "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson?.message);
  } else {
    return responseJson || {};
  }
};
adminAction.changeServiceCategoryStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.changeServiceCategoryStatus",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (!responseJson.code) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson || {};
  }
};
adminAction.getServiceBasedLocation = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.getServiceBasedLocation",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson || [];
  }
};

adminAction.getEagleServiceCategory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.getEagleServiceCategory",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson || [];
  }
};
adminAction.updateCategoryFareBreakup = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "category.updateCategoryFareBreakup",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson || [];
  }
};
adminAction.getCategoryFareBreakup = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("category.getCategoryFareBreakup", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson?.code === 0) {
    throw new MoleculerError(responseJson.message);
  } else {
    return responseJson.data || [];
  }
};
adminAction.getCategoryFareBreakupImages = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${clientId + "_" + Date.now()}_${context.params.filesName}`,
    });
    return s3Image[0];
  } else {
    return null;
  }
};

// PAYMENT GATEWAY
adminAction.getAllPaymentGateway = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params.search || "";

  const query = {
    "name": constantUtil.PAYMENTGATEWAY,
    "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.name", "data.gateWay"],
  });
  const paymentJson = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.name", "data.gateWay"],
  });

  responseJson["count"] = count;
  responseJson["response"] = paymentJson;
  return responseJson;
};
adminAction.updatePaymentGateway = async function (context) {
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params._id.toString()),
    {
      "type": constantUtil.PAYMENTGATEWAY,
      "data": context.params.data,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  }
  // // LOCAL STORAGE REDIS WORK
  this.adapter
    .find({
      "type": constantUtil.PAYMENTGATEWAY,
      "name": constantUtil.PAYMENTGATEWAY,
    })
    .then((responseData) => {
      const paymentsObject = {};
      responseData.forEach((payments) => {
        paymentsObject[payments._id] = {
          "_id": payments._id,
          "id": payments._id,
          "type": payments.type,
          "data": {
            "status": payments.data.status,
            "clientId": payments.data.clientId,
          },
          ...payments.data,
          "createdAt": payments.createdAt,
          "updatedAt": payments.updatedAt,
        };
      });
      storageUtil.write(constantUtil.PAYMENTGATEWAY, paymentsObject);
    });

  // // LOCAL STORAGE REDIS WORK
  return responseJson;
};
adminAction.getEditPaymentGateway = async function (context) {
  const responseJson = await this.adapter.model
    .findById(context.params.id.toString())
    .lean();
  return responseJson;
};
adminAction.changePaymentGatewayStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "type": constantUtil.PAYMENTGATEWAY,
      "name": constantUtil.PAYMENTGATEWAY,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  const responseData = await this.adapter.model
    .find({
      "type": constantUtil.PAYMENTGATEWAY,
      "name": constantUtil.PAYMENTGATEWAY,
    })
    .then((responseData) => {
      const paymentsObject = {};
      responseData.forEach((payments) => {
        paymentsObject[payments._id] = {
          "_id": payments._id,
          "id": payments._id,
          "type": payments.type,
          "data": {
            "status": payments.data.status,
            "clientId": payments.data.clientId,
          },
          ...payments.data,
          "createdAt": payments.createdAt,
          "updatedAt": payments.updatedAt,
        };
      });
      storageUtil.write(constantUtil.PAYMENTGATEWAY, paymentsObject);
    });
  return { "code": 1, "response": "Updated Successfully" };
};

adminAction.userGetAvailableCurrencies = async function () {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await this.adapter.model
    .find({
      "type": constantUtil.PAYMENTGATEWAY,
      "name": constantUtil.PAYMENTGATEWAY,
    })
    .lean();
  if (!paymentOptionList) {
    throw new MoleculerError("PAYMENT OPTION LIST NOT FOUND");
  }
  let responseData = [];
  paymentOptionList.map((option) => {
    if (
      option.data.paymentType === constantUtil.PAYMENTCARD &&
      option.data.status === constantUtil.ACTIVE
    ) {
      if (generalSettings.data.paymentSection === constantUtil.ALL) {
        responseData = option.data.availableCurrencies;
      }
    }
  });
  return { "code": 200, "data": responseData };
};

//OPERATORS ROLE
adminAction.getOperatorsRoleList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {
    // "name": constantUtil.OPERATORSROLE,
    "name": {
      "$in": [
        constantUtil.OPERATORSROLE,
        constantUtil.HUBSPRIVILEGES,
        constantUtil.HUBSEMPLOYEEPRIVILEGES,
        constantUtil.OFFICEPRIVILEGES,
        constantUtil.COORPERATEPRIVILEGES,
      ],
    },
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.role"],
  });
  const operatorRoles = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.role"],
  });
  responseJson["count"] = count;
  responseJson["response"] = operatorRoles;
  return responseJson;
};

adminAction.updateOperatorsRole = async function (context) {
  let responseJson,
    errorCode = 200,
    message = "";
  try {
    const clientId = context.params.data.clientId || context.meta.clientId;
    const rolesJson = await this.adapter.model.findOne({
      "data.role": context.params.data.role,
      // "name": constantUtil.OPERATORSROLE,
      "name": {
        "$in": [
          constantUtil.OPERATORSROLE,
          constantUtil.HUBSPRIVILEGES,
          constantUtil.HUBSEMPLOYEEPRIVILEGES,
          constantUtil.OFFICEPRIVILEGES,
          constantUtil.COORPERATEPRIVILEGES,
        ],
      },
      "data.clientId": mongoose.Types.ObjectId(clientId),
    });
    if (context.params.id) {
      if (
        rolesJson &&
        rolesJson._id.toString() !== context.params.id.toString()
      ) {
        throw new MoleculerError("Operators Role Already Exists"); //INFO_OPERATORS_ROLE_EXISTS
      }
      responseJson = await this.adapter.updateById(
        mongoose.Types.ObjectId(context.params.id.toString()),
        {
          "data": context.params.data,
        }
      );
      context.emit("admin.rolesForceUpdate", {
        "_id": responseJson._id,
        "forceUpdate": context.params.data.forceUpdate,
        "clientId": clientId,
      });
    } else {
      if (rolesJson) {
        throw new MoleculerError("Operators Role Already Exists"); //INFO_OPERATORS_ROLE_EXISTS
      }
      responseJson = await this.adapter.insert({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.OPERATORSROLE,
        "name": constantUtil.OPERATORSROLE,
        "data": context.params.data,
      });
    }
    if (!responseJson) {
      throw new MoleculerError("Error in Update");
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseJson = {};
  }
  // return responseJson;
  return {
    "code": errorCode,
    "data": responseJson,
    "message": message,
  };
};

adminAction.getOperatorRole = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "_id": context.params.id,
      // "name": constantUtil.OPERATORSROLE,
      "name": {
        "$in": [
          constantUtil.OPERATORSROLE,
          constantUtil.HUBSPRIVILEGES,
          constantUtil.HUBSEMPLOYEEPRIVILEGES,
          constantUtil.OFFICEPRIVILEGES,
          constantUtil.COORPERATEPRIVILEGES,
        ],
      },
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Operators Role Already Exists"); //INFO_OPERATORS_ROLE_EXISTS
  } else {
    return responseJson;
  }
};

adminAction.changeOperatorsRoleStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.OPERATORSROLE,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    if (context.params.status === constantUtil.INACTIVE) {
      context.emit("admin.rolesChangeInactive", {
        "_id": ids,
        "clientId": clientId,
      });
    }
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.getAllOperatorsRoles = async function (context) {
  const responseJson = {};
  const operatorRoles = await this.adapter.model
    .find({
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      "name": constantUtil.OPERATORSROLE,
      "data.status": constantUtil.ACTIVE,
    })
    .lean();
  responseJson["count"] = operatorRoles.length;
  responseJson["response"] = operatorRoles.map((roles) => ({
    "_id": roles._id,
    "name": roles.name,
    "data": {
      "role": roles.data.role,
      "privileges": roles.data.privileges,
    },
  }));
  return responseJson;
};

// OPERATORS
adminAction.getOperatorsList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {
    "name": constantUtil.OPERATORS,
    "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    "_id": { "$ne": context.meta.adminId },
    "data.status": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
  };
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.number",
    ],
  });
  const operators = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = operators.map((respo) => ({
    "_id": respo._id,
    "data": {
      "firstName": respo.data.firstName,
      "lastName": respo.data.lastName,
      "email": respo.data.email,
      "phone": respo.data.phone,
      "status": respo.data.status,
      "operatorRole": respo.data.operatorRole,
      "extraPrivileges": respo.data.extraPrivileges,
    },
  }));
  return responseJson;
};
adminAction.updateOperator = async function (context) {
  const { INFO_PHONE_NUMBER_EXISTS } = notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  let responseJson;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${context.params.clientId + "_" + Date.now()}_${
        context.params.firstName
      }${context.params.phoneNumber}${constantUtil.OPERATORS}`,
    });
    context.params.avatar = s3Image[0];
  }
  const operatorJson = await this.adapter.model
    .findOne({
      "type": constantUtil.OPERATORS,
      "name": constantUtil.OPERATORS,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      "data.phone.code": context.params.phoneCode,
      "data.phone.number": context.params.phoneNumber,
    })
    .lean();
  if ((context.params?.password || "") !== "") {
    context.params.password = bcrypt.hashSync(
      context.params.password,
      bcrypt.genSaltSync(8),
      null
    );
  } else {
    context.params.password =
      (operatorJson?.data?.password || "") !== ""
        ? operatorJson.data.password
        : null;
  }
  const updateData = {
    "clientId": clientId,
    "firstName": context.params.firstName,
    "lastName": context.params.lastName,
    "email": context.params.email,
    "isEmailVerified": context.params.isEmailVerified || false,
    "gender": context.params.gender,
    "status": context.params.status,
    "operatorRole": context.params.operatorRole,
    "avatar": context.params.avatar,
    "password": context.params.password,
    "extraPrivileges": JSON.parse(context.params?.extraPrivileges) || {},
    "phone": {
      "code": context.params.phoneCode,
      "number": context.params.phoneNumber,
    },
  };

  if (context.params.id) {
    if (operatorJson && operatorJson?._id?.toString() !== context.params.id) {
      // throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
      throw new MoleculerError(INFO_PHONE_NUMBER_EXISTS);
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": updateData }
    );
  } else {
    if (operatorJson) {
      // throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
      throw new MoleculerError(INFO_PHONE_NUMBER_EXISTS);
    }
    responseJson = await this.adapter.insert({
      //"dataType": constantUtil.CLIENTDATA,
      // "clientId": clientId,
      "type": constantUtil.OPERATORS,
      "name": constantUtil.OPERATORS,
      "data": updateData,
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getOperator = async function (context) {
  const responseJson = await this.adapter.model.findOne(
    {
      "name": constantUtil.OPERATORS,
      "_id": context.params.id,
    },
    {
      "data.accessToken": 0,
      "data.otp": 0,
      "data.password": 0,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Operator Credentials");
  } else {
    responseJson.data.extraPrivileges = responseJson.data.extraPrivileges || {};
    return responseJson;
  }
};

adminAction.changeOperatorStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context?.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.OPERATORS,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// RESPONSE OFFICE
adminAction.updateResponseOfficePrivileges = async function (context) {
  const clientId = context?.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateOne(
    {
      "name": constantUtil.OFFICEPRIVILEGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data": context.params.data }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getResponseOfficePrivileges = async function (context) {
  const clientId = context?.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.OFFICEPRIVILEGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Error in Response office Privileges");
  } else {
    return responseJson;
  }
};

adminAction.getResponseOfficeList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const clientId = context?.params?.clientId || context.meta?.clientId;

  const search = context.params?.search || "";
  const query = {
    "name": constantUtil.RESPONSEOFFICE,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.number",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "officeName": respo.data.officeName,
      "officeId": respo.data.officeId,
      "email": respo.data.email,
      "phone": respo.data.phone,
      "status": respo.data.status,
      "locationAddress": respo.data.locationAddress,
    },
  }));
  return responseJson;
};
adminAction.updateResponseOffice = async function (context) {
  let responseJson;
  const responseOfficeJson = await this.adapter.model.findOne({
    "data.phone.code": context.params.data.phone.code,
    "data.phone.number": context.params.data.phone.number,
  });
  if (context.params.data.password && context.params.data.password !== "") {
    context.params.data.password = bcrypt.hashSync(
      context.params.data.password,
      bcrypt.genSaltSync(8),
      null
    );
  } else {
    context.params.data.password =
      responseOfficeJson && responseOfficeJson.data.password
        ? responseOfficeJson.data.password
        : null;
  }
  if (context.params.id) {
    if (
      responseOfficeJson &&
      responseOfficeJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "data": context.params.data,
      }
    );
  } else {
    if (responseOfficeJson) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.RESPONSEOFFICE,
      "name": constantUtil.RESPONSEOFFICE,
      "data": context.params.data,
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};
adminAction.getResponseOffice = async function (context) {
  const responseJson = await this.adapter.model.findOne(
    {
      "name": constantUtil.RESPONSEOFFICE,
      "_id": context.params.id,
    },
    {
      "data.accessToken": 0,
      "data.otp": 0,
      "data.password": 0,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Emergency Response Office Details");
  } else {
    return responseJson;
  }
};

adminAction.changeResponseOfficeStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.RESPONSEOFFICE,
      // "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};
adminAction.getAllResponseOffices = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseOffice = await this.adapter.model
    .find({
      "name": constantUtil.RESPONSEOFFICE,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.status": constantUtil.ACTIVE,
    })
    .sort({ "_id": -1 })
    .collation({ "locale": "en", "caseLevel": true });
  return responseOffice.map((data) => {
    return {
      "_id": data._id,
      "officeName": data.data.officeName,
      "officeId": data.data.officeId,
    };
  });
};

// DOCUMENTS
adminAction.getDocumentsList = async function (context) {
  const responseJson = {};
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta?.clientId;

  const query = {
    "name": constantUtil.DOCUMENTS,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const searchCity = {
    "$match": {
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      "data.serviceAreaId": context.params.city
        ? mongoose.Types.ObjectId(context.params.city)
        : { "$ne": null },
    },
  };

  // const count = await this.adapter.count({
  //   "query": query,
  //   "search": search,
  //   "searchFields": ["data.docsFor", "data.docsName"],
  // });
  // const documents = await this.adapter.find({
  //   "limit": limit,
  //   "offset": skip,
  //   "query": query,
  //   "sort": "-updatedAt",
  //   "search": search,
  //   "searchFields": ["data.docsFor", "data.docsName"],
  // });
  // // .populate("data.docsServiceTypeId");
  // // .populate("data.docsServiceTypeId.serviceAreaId");
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "data.docsName": {
            "$regex": context.params.search + ".*",
            "$options": "si",
          },
        },
        // {
        //   "data.docsName": { "$regex": ".*", "$options": "si" },
        //   "serviceTypeList": { "$regex": ".*", "$options": "si" },
        //   "serviceAreaList.locationName": {
        //     "$regex": context.params.search + ".*",
        //     "$options": "si",
        //   },
        // },
      ],
    },
  };
  const jsonData = await this.adapter.model.aggregate([
    {
      "$match": {
        ...query,
      },
    },
    {
      "$facet": {
        "all": [searchQuery, searchCity, { "$count": "all" }],
        "response": [
          // { "$search": search },
          // { "$sort": { "_id": -1 } },
          // searchQuery,
          // { "$skip": skip },
          // { "$limit": limit },
          {
            "$lookup": {
              "from": "admins",
              "localField": "data.docsServiceTypeId",
              "foreignField": "_id",
              "as": "serviceTypeList",
            },
          },
          {
            "$unwind": {
              "path": "$serviceTypeList",
              "preserveNullAndEmptyArrays": true,
            },
          },
          {
            "$lookup": {
              "from": "categories",
              "localField": "data.serviceAreaId",
              "foreignField": "_id",
              "as": "serviceAreaList",
            },
          },
          {
            "$unwind": {
              "path": "$serviceAreaList",
              "preserveNullAndEmptyArrays": true,
            },
          },
          searchCity,
          {
            "$project": {
              "_id": "$_id",
              "data.docsName": "$data.docsName",
              "data.docsFor": "$data.docsFor",
              "data.docsServiceType": "$data.docsServiceType",
              "data.docsServiceTypeId": "$data.docsServiceTypeId",
              "data.docsExpiry": "$data.docsExpiry",
              "data.docsMandatory": "$data.docsMandatory",
              "data.status": "$data.status",
              "data.serviceTypeName": "$serviceTypeList.data.title",
              "data.serviceAreaName": "$serviceAreaList.locationName",
            },
          },
          { "$sort": { "_id": -1 } },
          searchQuery,
          { "$skip": skip },
          { "$limit": limit },
        ],
      },
    },
  ]);
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  // responseJson["count"] =
  //   context.params.search && context.params.search != ""
  //     ? documents.length
  //     : count;
  // responseJson["response"] = documents;
  return responseJson;
};

adminAction.updateDocument = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${context.params.docsFor}${context.params.docsName}${constantUtil.DOCUMENTS}`,
    });
    context.params.docsReferImage = s3Image[0];
  }
  const documentsJson = await this.adapter.model.findOne({
    //"data.clientId": mongoose.Types.ObjectId(clientId),
    "data.docsName": context.params.docsName,
    "data.docsFor": context.params.docsFor,
    "name": constantUtil.DOCUMENTS,
  });
  const updateData = {
    "clientId": clientId,
    "docsName": context.params.docsName,
    "docsFor": context.params.docsFor,
    "docsDetail": context.params.docsDetail,
    "docsReferImage": context.params.docsReferImage,
    "docsMandatory": context.params.docsMandatory,
    "docsExpiry": context.params.docsExpiry,
    "status": context.params.status,
  };
  let responseJson;
  if (context.params.id) {
    if (
      documentsJson &&
      documentsJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Documents Already Exists"); //INFO_DOCUMENTS_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "data": updateData,
      }
    );
  } else {
    if (documentsJson) {
      throw new MoleculerError("Documents Already Exists"); //INFO_DOCUMENTS_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.DOCUMENTS,
      "name": constantUtil.DOCUMENTS,
      "data": updateData,
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  }
  /* LOCAL STORAGE:- DOCUMENT */
  const documentJson = await this.adapter.find({
    "name": constantUtil.DOCUMENTS,
  });
  const documentsObjectFn = (data) => {
    const documentsObject = {};
    documentJson.forEach((documents) => {
      if (documents.data.docsFor === data) {
        documentsObject[documents._id] = {
          "id": documents._id,
          "_id": documents._id,
          ...documents.data,
        };
      }
    });
    return documentsObject;
  };
  storageUtil.write(
    constantUtil.PROFILEDOCUMENTS,
    documentJson ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS) : []
  );
  storageUtil.write(
    constantUtil.DRIVERDOCUMENTS,
    documentJson ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS) : []
  );
  /* LOCAL STORAGE:- DOCUMENT */

  // await this.broker.emit("professional.updateAllProfessionals", {}); // UPDATE ALL PROFESSIONALS
  await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    "type": constantUtil.CONST_REGISTRATIONSETTING,
    //"clientId": clientId,
  });
  return responseJson;
};
adminAction.getEditDocument = async function (context) {
  // const responseJson = await this.adapter.model
  //   .findOne({
  //     "name": constantUtil.DOCUMENTS,
  //     "_id": context.params.id,
  //   })
  //   .populate("data.docsServiceTypeId")
  //   .populate("data.docsServiceTypeId.data.serviceAreaId")
  //   .lean();
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        "name": constantUtil.DOCUMENTS,
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "data.docsServiceTypeId",
        "foreignField": "_id",
        "as": "serviceTypeList",
      },
    },
    {
      "$unwind": {
        "path": "$serviceTypeList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$lookup": {
        "from": "categories",
        "localField": "data.serviceAreaId",
        "foreignField": "_id",
        "as": "serviceAreaList",
      },
    },
    {
      "$unwind": {
        "path": "$serviceAreaList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$project": {
        "_id": "$_id",
        "data.docsName": "$data.docsName",
        "data.docsFor": "$data.docsFor",
        "data.docsServiceType": "$data.docsServiceType",
        "data.docsServiceTypeId": "$data.docsServiceTypeId",
        "data.docsExpiry": "$data.docsExpiry",
        "data.docsMandatory": "$data.docsMandatory",
        "data.status": "$data.status",
        "data.serviceTypeName": "$serviceTypeList.data.title",
        "data.serviceAreaName": "$serviceAreaList.locationName",
      },
    },
  ]);
  if (responseJson.length === 0) {
    throw new MoleculerError("Invalid Document details");
  } else {
    return responseJson[0] || {};
  }
};

adminAction.changeDocumentStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.DOCUMENTS,
      //"data.clientId": context.params.clientId,
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  /* LOCAL STORAGE:- DOCUMENT */
  const documentJson = await this.adapter.model
    .find({
      "name": constantUtil.DOCUMENTS,
    })
    .lean();

  const documentsObjectFn = (data) => {
    const documentsObject = {};
    documentJson.forEach((documents) => {
      if (documents.data.docsFor === data) {
        documentsObject[documents._id] = {
          "id": documents._id,
          "_id": documents._id,
          ...documents.data,
        };
      }
    });
    return documentsObject;
  };
  await storageUtil.write(
    constantUtil.PROFILEDOCUMENTS,
    documentJson ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS) : []
  );
  await storageUtil.write(
    constantUtil.DRIVERDOCUMENTS,
    documentJson ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS) : []
  );
  /* LOCAL STORAGE:- DOCUMENT */

  // await this.broker.emit("professional.updateAllProfessionals", {}); // UPDATE ALL PROFESSIONALS
  await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    "type": constantUtil.CONST_REGISTRATIONSETTING,
    //"clientId": clientId,
  });
  return { "code": 1, "response": "Updated Successfully" };
};

// CANCELLATION REASONS
adminAction.getCancellationReasonsList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta.clientId;

  const query = {
    "name": constantUtil.CANCELLATIONREASON,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.reasonFor", "data.reason"],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.reasonFor", "data.reason"],
  });

  responseJson["count"] = count;
  responseJson["response"] = jsonData;
  return responseJson;
};

adminAction.updateCancellationReason = async function (context) {
  let responseJson,
    languageCode = "";
  const clientId = context.params.data.clientId || context.meta.clientId;
  const { INFO_CANCELLATION_REASON_EXISTS } = notifyMessage.setNotifyLanguage(
    context.params.langCode || languageCode
  );
  const reasonsJson = await this.adapter.model
    .findOne({
      //"data.clientId": mongoose.Types.ObjectId(context.params.data.clientId),
      "data.reason": context.params.data.reason,
      "data.reasonFor": context.params.data.reasonFor,
      "data.bookingStatus": context.params.data.bookingStatus,
      "name": constantUtil.CANCELLATIONREASON,
    })
    .lean();
  if (context.params.id) {
    if (
      reasonsJson &&
      reasonsJson?._id?.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError(INFO_CANCELLATION_REASON_EXISTS);
    } else {
      responseJson = await this.adapter.updateById(
        mongoose.Types.ObjectId(context.params.id.toString()),
        { "data": context.params.data }
      );
    }
  } else {
    if (reasonsJson) {
      throw new MoleculerError(INFO_CANCELLATION_REASON_EXISTS);
    } else {
      responseJson = await this.adapter.insert({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.CANCELLATIONREASON,
        "name": constantUtil.CANCELLATIONREASON,
        "data": context.params.data,
      });
    }
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  }
  // Setting Update for Cancel Reson Time
  const settingsUpdate = await this.adapter.model
    .findOneAndUpdate(
      {
        "name": constantUtil.GENERALSETTING,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      { "data.cancelReasonUpdateTime": Date.now() },
      { "new": true }
    )
    .lean();
  settingsUpdate["id"] = settingsUpdate?._id?.toString();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Cancel Reson Time
  const cancellationReasonJson = await this.adapter.model
    .find({
      "name": constantUtil.CANCELLATIONREASON,
      "data.status": constantUtil.ACTIVE,
    })
    .sort({ "data.reason": -1 })
    .lean();
  // cancellationReasonJson = Object.keys(cancellationReasonJson).map((key) => {
  //   return {
  //     "_id": key,
  //     ...cancellationReasonJson[key],
  //   };
  // });
  // //--------------
  const cancellationReasonObject = {};
  cancellationReasonJson.forEach((reason) => {
    cancellationReasonObject[reason._id] = {
      "_id": reason._id,
      "id": reason._id,
      "data": {
        "status": reason.data.status,
        "clientId": reason.data.clientId,
      },
      ...reason.data,
    };
  });
  storageUtil.write(constantUtil.CANCELLATIONREASON, cancellationReasonObject);
  return responseJson;
};
// adminAction.getEditCancellationReason = async function (context) {
//   const responseJson = await this.adapter.model
//     .findOne({
//       "name": constantUtil.CANCELLATIONREASON,
//       "_id": context.params.id,
//     })
//     .lean();
//   if (!responseJson) {
//     throw new MoleculerError("Invalid cancellation reason credentials");
//   }
//   const cancellationReasonJson = await this.adapter.model
//     .find({
//       "name": constantUtil.CANCELLATIONREASON,
//       "data.status": constantUtil.ACTIVE,
//     })
//     .lean();
//   const cancellationReasonObject = {};
//   cancellationReasonJson.forEach((reason) => {
//     cancellationReasonObject[reason._id] = {
//       "_id": reason._id,
//       "id": reason._id,
//       "data": {
//         "status": reason.data.status,
//         "clientId": reason.data.clientId,
//       },
//       ...reason.data,
//     };
//   });
//   storageUtil.write(constantUtil.CANCELLATIONREASON, cancellationReasonObject);
//   return responseJson;
// };
adminAction.changeCancellationReasonStatus = async function (context) {
  const clientId = context.params.clientId;
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.CANCELLATIONREASON,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for Cancel Reson Time
  const settingsUpdate = await this.adapter.model
    .findOneAndUpdate(
      {
        "name": constantUtil.GENERALSETTING,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      { "data.cancelReasonUpdateTime": Date.now() },
      { "new": true }
    )
    .lean();
  settingsUpdate["id"] = settingsUpdate?._id?.toString();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  const cancellationReasonJson = await this.adapter.model
    .find({
      "name": constantUtil.CANCELLATIONREASON,
      "data.status": constantUtil.ACTIVE,
    })
    .sort({ "data.reason": -1 })
    .lean();
  const cancellationReasonObject = {};
  cancellationReasonJson.forEach((reason) => {
    cancellationReasonObject[reason._id] = {
      "_id": reason._id,
      "id": reason._id,
      "data": {
        "status": reason.data.status,
        "clientId": reason.data.clientId,
      },
      ...reason.data,
    };
  });
  storageUtil.write(constantUtil.CANCELLATIONREASON, cancellationReasonObject);
  return { "code": 1, "response": "Updated Successfully" };
};

// HUBS
//#region Hub
adminAction.updateHubsPrivileges = async function (context) {
  const responseJson = await this.adapter.model.updateOne(
    {
      "name": constantUtil.HUBSPRIVILEGES,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data": context.params.data }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getHubsPrivileges = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.HUBSPRIVILEGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Error in Hubs Privileges");
  } else {
    return responseJson;
  }
};
adminAction.getHubsList = async function (context) {
  let hubsEmpData = null;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const search = context.params?.search || "";
  const clientId = context.params.clientId;
  const query = {
    "name": constantUtil.HUBS,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  switch (context.params?.userType?.toUpperCase()) {
    case constantUtil.HUBS:
      query["_id"] = mongoose.Types.ObjectId(context.params.userId);
      break;

    case constantUtil.HUBSEMPLOYEE:
      hubsEmpData = await this.adapter.model
        .findOne(
          {
            "_id": mongoose.Types.ObjectId(context.params.userId),
            "name": constantUtil.HUBSEMPLOYEE,
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
          { "data.hubsId": 1 }
        )
        .lean();
      query["_id"] = mongoose.Types.ObjectId(hubsEmpData?.data?.hubsId);
      break;
  }
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.hubsName",
      "data.email",
      "data.phone.number",
      "data.locationAddress.city",
    ],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.hubsName",
      "data.email",
      "data.phone.number",
      "data.locationAddress.city",
    ],
  });

  responseJson["count"] = count;
  responseJson["response"] = jsonData.map((respo) => ({
    "_id": respo._id,
    "data": {
      "hubsName": respo.data.hubsName,
      "email": respo.data.email,
      "phone": respo.data.phone,
      "status": respo.data.status,
      "locationAddress": respo.data.locationAddress,
      "timeSettings": respo.data.timeSettings,
    },
  }));
  return responseJson;
};
adminAction.updateHubs = async function (context) {
  let responseJson;
  const clientId = context.params.data.clientId || context.meta.clientId;
  const hubsJson = await this.adapter.model
    .findOne({
      "data.clientId": mongoose.Types.ObjectId(context.params.data.clientId),
      "data.phone.code": context.params.data.phone.code,
      "data.phone.number": context.params.data.phone.number,
    })
    .lean();
  if (context.params.data.password && context.params.data.password !== "") {
    context.params.data.password = bcrypt.hashSync(
      context.params.data.password,
      bcrypt.genSaltSync(8),
      null
    );
  } else {
    context.params.data.password =
      hubsJson && hubsJson.data.password ? hubsJson.data.password : null;
  }
  if (context.params.id) {
    if (
      hubsJson &&
      hubsJson?._id?.toString() !== context.params.id?.toString()
    ) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        // "data": context.params.data,
        "data.hubsName": context.params.data.hubsName,
        "data.serviceArea": context.params.data.serviceArea,
        "data.serviceAreaId": context.params.data.serviceArea,
        "data.locationAddress": context.params.data.locationAddress,
        "data.showAddress": context.params.data.showAddress,
        "data.email": context.params.data.email,
        "data.password": context.params.data.password,
        "data.commissionPercentage": context.params.data.commissionPercentage,
        "data.phone": context.params.data.phone,
        "data.location": context.params.data.location,
        "data.timeSettings": context.params.data.timeSettings,
        "data.arrayData": context.params.data.arrayData,
        "data.status": context.params.data.status,
        //"data.amount" --> Must will not add this Field
      }
    );
  } else {
    if (hubsJson) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.HUBS,
      "name": constantUtil.HUBS,
      // "data": context.params.data,
      "data.clientId": clientId,
      "data.hubsName": context.params.data.hubsName,
      "data.serviceArea": context.params.data.serviceArea,
      "data.serviceAreaId": context.params.data.serviceArea,
      "data.locationAddress": context.params.data.locationAddress,
      "data.showAddress": context.params.data.showAddress,
      "data.email": context.params.data.email,
      "data.password": context.params.data.password,
      "data.commissionPercentage": context.params.data.commissionPercentage,
      "data.phone": context.params.data.phone,
      "data.location": context.params.data.location,
      "data.timeSettings": context.params.data.timeSettings,
      "data.arrayData": context.params.data.arrayData,
      "data.status": context.params.data.status,
      "data.amount": 0, // For Initial Amount
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getEditHub = async function (context) {
  const clientId = context.params.clientId;
  const responseJson = await this.adapter.model
    .findOne(
      {
        "name": constantUtil.HUBS,
        "_id": context.params.id,
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "data.accessToken": 0,
        "data.otp": 0,
        "data.password": 0,
      }
    )
    .populate("data.serviceAreaId", { "locationName": 1 })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Hubs credentials");
  } else {
    return responseJson;
  }
};

adminAction.changeHubsStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.HUBS,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.addWalletAmount = async function (context) {
  const userData = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
      {
        "data.amount": 1,
        "_id": 1,
        "data.hubsName": 1,
        "data.officeName": 1,
        "data.phone": 1,
      }
    )
    .lean();
  const updateData =
    context.params.type === constantUtil.CREDIT
      ? {
          "data.amount": (userData.data.amount || 0) + context.params.amount,
        }
      : {
          "data.amount": (userData.data.amount || 0) - context.params.amount,
        };
  if (!userData) {
    throw new MoleculerError("ERROR IN USER");
  }
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
    },
    updateData
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("ERROR IN USER");
  }
  if (context.params.type === constantUtil.CREDIT) {
    this.broker.emit("transaction.updateWalletAdminRecharge", {
      "clientId": context.params.clientId,
      "amount": context.params.amount,
      "previousBalance": userData.data.amount,
      "currentBalance": userData.data.amount + context.params.amount,
      "reason": context.params.reason || "",
      "isReimbursement": context.params.isReimbursement || false,
      "userType": context.params.userType, // constantUtil.HUBS,
      "data": {
        "id": userData._id.toString(),
        "firstName":
          context.params.userType === constantUtil.HUBS
            ? userData.data.hubsName
            : userData.data.officeName,
        "lastName": "",
        "avatar": "",
        "phone": {
          "code": userData.data.phone.code,
          "number": userData.data.phone.number,
        },
      },
      "admin": {
        "id": context.meta.adminId.toString(),
        "firstName": context.meta.adminDetails.firstName,
        "lastName": context.meta.adminDetails.lastName,
        "avatar": context.meta.adminDetails.avatar,
        "phone": {
          "code": context.meta.adminDetails.phone.code,
          "number": context.meta.adminDetails.phone.number,
        },
      },
    });
  } else {
    this.broker.emit("transaction.updateWalletAdminDebit", {
      "clientId": userData?.clientId?.toString(),
      "amount": context.params.amount,
      "previousBalance": userData.data.amount,
      "currentBalance": userData.data.amount - context.params.amount,
      "reason": context.params.reason || "",
      "isReimbursement": context.params.isReimbursement || false,
      "userType": context.params.userType, // constantUtil.HUBS,
      "data": {
        "id": userData._id.toString(),
        "firstName":
          context.params.userType === constantUtil.HUBS
            ? userData.data.hubsName
            : userData.data.officeName,
        "lastName": "",
        "avatar": "",
        "phone": {
          "code": userData.data.phone.code,
          "number": userData.data.phone.number,
        },
      },
      "admin": {
        "id": context.meta.adminId.toString(),
        "firstName": context.meta.adminDetails.firstName,
        "lastName": context.meta.adminDetails.lastName,
        "avatar": context.meta.adminDetails.avatar,
        "phone": {
          "code": context.meta.adminDetails.phone.code,
          "number": context.meta.adminDetails.phone.number,
        },
      },
    });
  }
  return {
    "code": 200,
    "response": {},
    "message": "WALLET REACHARGE SUCCESSFULLY",
  };
};
//#endregion Hub

// HUBS EMPLOYEE
adminAction.getHubEmployeeList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const search = context.params?.search || "";
  const query = {
    "name": constantUtil.HUBSEMPLOYEE,
    "data.hubsId": mongoose.Types.ObjectId(context.params.hubsId),
    "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  switch (context.params?.userType?.toUpperCase()) {
    case constantUtil.HUBSEMPLOYEE:
      query["_id"] = mongoose.Types.ObjectId(context.params.userId);
      break;
  }
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.code",
      "data.phone.number",
    ],
  });
  const jsonData = await this.adapter.find({
    "query": query,
    "limit": limit,
    "offset": skip,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.email",
      "data.phone.code",
      "data.phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = jsonData.map((respo) => ({
    "_id": respo._id,
    "data": {
      "firstName": respo.data.firstName,
      "lastName": respo.data.lastName,
      "email": respo.data.email,
      "phone": respo.data.phone,
      "status": respo.data.status,
    },
  }));
  return responseJson;
};

adminAction.updateHubsEmployee = async function (context) {
  let responseJson;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params?.files?.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${context.params.firstName}${context.params.phoneNumber}${constantUtil.HUBSEMPLOYEE}`,
    });
    context.params.avatar = s3Image[0];
  }
  const checkJson = await this.adapter.model
    .findOne({
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "data.phone.code": context.params.phoneCode,
      "data.phone.number": context.params.phoneNumber,
    })
    .lean();
  if ((context.params?.password || "") !== "") {
    context.params.password = bcrypt.hashSync(
      context.params.password,
      bcrypt.genSaltSync(8),
      null
    );
  } else {
    context.params.password = checkJson?.data?.password || null;
  }
  if (context.params.id) {
    if (
      checkJson &&
      checkJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Phone Number Already Exists");
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "data.firstName": context.params.firstName,
        "data.lastName": context.params.lastName,
        "data.gender": context.params.gender,
        "data.avatar": context.params.avatar,
        "data.email": context.params.email,
        "data.password": context.params.password,
        "data.phone": {
          "code": context.params.phoneCode,
          "number": context.params.phoneNumber,
        },
      }
    );
  } else {
    if (checkJson) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.HUBSEMPLOYEE,
      "name": constantUtil.HUBSEMPLOYEE,
      "data": {
        "clientId": clientId,
        "hubsId": mongoose.Types.ObjectId(context.params.hubsId),
        "firstName": context.params.firstName,
        "lastName": context.params.lastName,
        "gender": context.params.gender,
        "avatar": context.params.avatar,
        "email": context.params.email,
        "password": context.params.password,
        "phone": {
          "code": context.params.phoneCode,
          "number": context.params.phoneNumber,
        },
        "status": context.params.status,
      },
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getEditHubEmployees = async function (context) {
  const clientId = context.params.clientId;
  const responseJson = await this.adapter.model.findOne(
    {
      "name": constantUtil.HUBSEMPLOYEE,
      "_id": context.params.id,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "data.accessToken": 0,
      "data.otp": 0,
      "data.password": 0,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Hubs Employee details");
  } else {
    return responseJson;
  }
};
adminAction.changeHubEmployeesStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "name": constantUtil.HUBSEMPLOYEE,
      "_id": { "$in": ids },
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// VERIFICATION DOCUMENTS
adminAction.getVerificationDocumentList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";

  const query = {
    "name": constantUtil.VERIFICATIONDOCUMENT,
    //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.docName"],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.docName"],
  });
  responseJson["count"] = count;
  responseJson["response"] = jsonData;
  return responseJson;
};

adminAction.updateVerificationDocument = async function (context) {
  let responseJson;
  const clientId = context.params.data.clientId || context.meta.clientId;
  const docsJson = await this.adapter.model.findOne({
    //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    "data.docName": context.params.data.docName,
    "name": constantUtil.VERIFICATIONDOCUMENT,
  });
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("verification document already exist");
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": context.params.data }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("verification document already exist");
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.VERIFICATIONDOCUMENT,
      "name": constantUtil.VERIFICATIONDOCUMENT,
      "data": context.params.data,
    });
  }
  return responseJson;
};

adminAction.getEditVerificationDocument = async function (context) {
  const responseJson = await this.adapter.model.findOne({
    "name": constantUtil.VERIFICATIONDOCUMENT,
    "_id": context.params.id,
  });
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};

adminAction.changeVerificationDocumentStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.VERIFICATIONDOCUMENT,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// USERS
adminAction.getUnregisteredUserList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.getUnregisteredUserList", {
    ...context.params,
    //"clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || {};
  }
};

adminAction.addUnregisteredUserNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.addUnregisteredUserNotes", {
    ...context.params,
    //"clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.changeUnregisteredUsersStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.changeUnregisteredUsersStatus", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.closeUnregisteredUser = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.closeUnregisteredUser", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};

adminAction.getUsersList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.getUsersList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getEditUser = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.getEditUser", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || {};
  }
};
adminAction.updateUser = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.updateUser", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};
adminAction.changeUsersStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("user.changeUsersStatus", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};

// PROFESSIONALS
adminAction.getUnregisteredProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.getUnregisteredProfessionalList",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.closeUnregisteredProfessional = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.closeUnregisteredProfessional",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};

adminAction.addUnregisteredProfessionalNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.addUnregisteredProfessionalNotes",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};
adminAction.addUnregisteredProfessionalStatusChange = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.addUnregisteredProfessionalStatusChange",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};
adminAction.getProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.getProfessionalList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getEditProfessional = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.getEditProfessional", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData?.data || {};
  }
};

adminAction.updateProfessionalStep1 = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.updateProfessionalStep1",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData?.message);
  } else {
    return jsonData || {};
  }
};

adminAction.getAllDocumentForUpload = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.getAllDocumentForUpload",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData?.message);
  } else {
    return jsonData || {};
  }
};

adminAction.updateProfessionalStep2 = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.updateProfessionalStep2",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData?.message);
  } else {
    return jsonData || {};
  }
};

adminAction.getHubListByLocation = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  // let jsonData = await this.events["admin.getHubListByLocation"]({
  //   ...context.params,
  // }); //Need to Check Ajith
  let jsonData = await this.broker.emit("admin.getHubListByLocation", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (!jsonData.length) {
    throw new MoleculerError("NO HUBS AVAILABLE IN THIS SERVICE AREA");
  } else {
    return { "code": 1, "data": jsonData };
  }
};

adminAction.updateProfessionalStep3 = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.updateProfessionalStep3",
    { ...context.params, "adminId": context.meta.adminId, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData ?? {};
  }
};

adminAction.updateProfessionalStep4 = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.updateProfessionalStep4",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.changeProfessionalsStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.changeProfessionalsStatus",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.updateBankDetailsProfessional = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.updateBankDetailsProfessional",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData?.message);
  } else {
    return jsonData || {};
  }
};

adminAction.changeVehicleStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.changeVehicleStatus", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.professionalProfileDocumentVerification = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.professionalProfileDocumentVerification",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.professionalVehicleDocumentVerification = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.professionalVehicleDocumentVerification",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.deleteProfessionalDocument = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = null;
  if (context.params.documentType === constantUtil.PROFILEDOCUMENTS) {
    jsonData = await this.broker.emit(
      "professional.deleteProfessionalProfileDocument",
      { ...context.params, "clientId": clientId }
    );
  } else if (context.params.documentType === constantUtil.DRIVERDOCUMENTS) {
    jsonData = await this.broker.emit(
      "professional.deleteProfessionalVehicleDocument",
      { ...context.params, "clientId": clientId }
    );
  }
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message || "Invalid Document Details");
  } else {
    return jsonData || {};
  }
};

adminAction.addProfessionalNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.addProfessionalNotes", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

adminAction.addVehicleNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.addVehicleNotes", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData?.code === 0) {
    throw new MoleculerError(jsonData?.message);
  } else {
    return jsonData || {};
  }
};

adminAction.professionalDocumentExpiryNotify = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.professionalDocumentExpiryNotify",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};
adminAction.getVehicleList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.getVehicleList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.editVehicleDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("professional.editVehicleDetails", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.professionalVerification = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.professionalVerification",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};
adminAction.getUnverifiedProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.getUnverifiedProfessionalList",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getExpiredProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "professional.getExpiredProfessionalList",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

// LANGUAGES
adminAction.getAllLanguages = async function (context) {
  const responseJson = {};
  const jsonData = await this.adapter.model
    .find({
      "data.status": { "$eq": constantUtil.ACTIVE },
      "name": { "$eq": constantUtil.LANGUAGES },
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  responseJson["count"] = jsonData.length;
  responseJson["response"] = jsonData.map((language) => ({
    "_id": language._id,
    "data": {
      "languageName": language.data.languageName,
      "languageCode": language.data.languageCode,
    },
  }));
  return responseJson;
};
adminAction.getLanguagesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params.search || "";
  const query = { "name": constantUtil.LANGUAGES };
  query["data.clientId"] = mongoose.Types.ObjectId(context.params.clientId);
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.languageName"],
  });
  const jsonData = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.languageName"],
  });

  responseJson["count"] = count;
  responseJson["response"] = jsonData.map((language) => ({
    "_id": language._id,
    "data": {
      "languageName": language.data.languageName,
      "languageCode": language.data.languageCode,
      "languageDirection": language.data.languageDirection,
      "languageDefault": language.data.languageDefault,
      "status": language.data.status,
    },
  }));
  return responseJson;
};
adminAction.getEditLanguage = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.LANGUAGES,
      "_id": context.params.id,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};
adminAction.updateLanguage = async function (context) {
  let responseJson;
  const clientId = context.params.data.clientId || context.meta.clientId;
  const docsJson = await this.adapter.model
    .findOne({
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "data.languageCode": context.params.data.languageCode,
      "name": constantUtil.LANGUAGES,
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Language Already Exists"); //INFO_LANGUAGE_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "data.languageName": context.params.data.languageName,
        "data.languageCode": context.params.data.languageCode,
        "data.languageDirection": context.params.data.languageDirection,
        "data.languageKeys": context.params.data.languageKeys,
      }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Language Already Exists"); //INFO_LANGUAGE_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.LANGUAGES,
      "name": constantUtil.LANGUAGES,
      "data": {
        "clientId": clientId,
        "languageName": context.params.data.languageName,
        "languageCode": context.params.data.languageCode,
        "languageDirection": context.params.data.languageDirection,
        "languageDefault": false,
        "status": context.params.data.status,
        "languageKeys": context.params.data.languageKeys || {},
      },
    });
  }
  return responseJson;
};

adminAction.changeLanguageStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.LANGUAGES,
      "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.getDefaultLanguage = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.LANGUAGES,
      "data.languageDefault": true,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};
adminAction.updateTranslateKeys = async function (context) {
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    { "data.languageKeys": context.params.languageKeys }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};

adminAction.setDefaultLanguage = async function (context) {
  let responseJson = await this.adapter.model.updateMany(
    {
      "name": constantUtil.LANGUAGES,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.languageDefault": false }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  }
  responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "name": constantUtil.LANGUAGES,
        "_id": context.params.id,
      },
      {
        "data.languageDefault": true,
      },
      { "new": true }
    )
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    await this.adapter.model.updateOne(
      {
        "name": constantUtil.GENERALSETTING,
        //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
      {
        "$set": {
          "data.languageCode": responseJson.data.languageCode || "en",
          "data.languageDirection":
            responseJson.data.languageDirection || "LTR",
        },
      }
    );
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// SECURITY SERVICES
adminAction.getSecurityServiceList = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.SECURITYSERVICE,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};
adminAction.updateSecurityServiceList = async function (context) {
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.id.toString()),
    { "data": context.params.data }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};

// RIDES
adminAction.viewRideDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.viewRideDetails", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || {};
  }
};
adminAction.getNewRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getNewRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getGuestRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getGuestRidesList", {
    ...context.params,
    "adminId": context.meta.adminId.toString(),
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getManualMeterRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getManualMeterRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getCoorperateRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const [jsonData] = await this.broker.emit("booking.getCoorperateRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData?.data || [];
  }
};
adminAction.getScheduleRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getScheduleRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getOngoingRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getOngoingRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getEndedRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getEndedRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getEndRideIssueList = async function (context) {
  let jsonData;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.type === "Users") {
    jsonData = await this.broker.emit("user.getEndRideIssueList", {
      ...context.params,
      "clientId": clientId,
    });
  } else {
    jsonData = await this.broker.emit("professional.getEndRideIssueList", {
      ...context.params,
      "clientId": clientId,
    });
  }
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.updateBookingInfo = async function (context) {
  let jsonData;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.bookingType === "USER") {
    jsonData = await this.broker.emit("user.updateBookingInfoIds", {
      ...context.params,
      "clientId": clientId,
    });
  } else if (context.params.bookingType === "PROFESSIONAL") {
    jsonData = await this.broker.emit("professional.updateBookingInfoIds", {
      ...context.params,
      "clientId": clientId,
    });
  }
  return jsonData;
};

adminAction.getExpiredRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getExpiredRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getUserCancelledRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getUserCancelledRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getProfessionalCancelledRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "booking.getProfessionalCancelledRidesList",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.paymentFailedIssueList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.paymentFailedIssueList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getSearchRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getSearchRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getAssignProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getAssignProfessionalList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.sendRequstToProfessional = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.sendRequstToProfessional", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

// RESPONSE OFFICER APIS
adminAction.getOfficerList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("officer.getOfficerList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.getEditOfficer = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("officer.getEditOfficer", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.changeOfficerStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("officer.changeOfficerStatus", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

adminAction.updateOfficer = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("officer.updateOfficer", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};

// SUBSCRIPTION APIS
adminAction.getSubscriptionList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("subscription.getSubscriptionList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.getEditSubscription = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("subscription.getEditSubscription", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
adminAction.changeSubscriptionStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit(
    "subscription.changeSubscriptionStatus",
    { ...context.params, "clientId": clientId }
  );
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || [];
  }
};
adminAction.updateSubscription = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("subscription.updateSubscription", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData.data || [];
  }
};
// DASHBOARD
adminAction.getOperatorData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const todayCount = await this.adapter.count({
    "query": {
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "name": constantUtil.OPERATORS,
      "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
      // "category": mongoose.Types.ObjectId(context.params.city),
    },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "name": constantUtil.OPERATORS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
        }
      : {
          "name": constantUtil.OPERATORS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        };
  const data = await this.adapter.model.find(
    // context.params.city === ""
    //   ? matchData
    //   : {
    //       ...matchData,
    //       "category": mongoose.Types.ObjectId(context.params.city),
    //     },
    matchData,
    {
      "data.status": 1,
      "createdAt": 1,
    }
  );
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0;

  await _.forEach(data, (val) => {
    switch (val.data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
    }
  });
  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });
  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "todayCount": todayCount,
    "graphData": graphData,
  };
  return responseJson;
};

adminAction.getHubsData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const todayCount = await this.adapter.count({
    "query": {
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "name": constantUtil.HUBS,
      "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
      // "category": mongoose.Types.ObjectId(context.params.city),
    },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);
  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "name": constantUtil.HUBS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
        }
      : {
          "name": constantUtil.HUBS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        };
  const data = await this.adapter.model.find(matchData, {
    "data.status": 1,
    "createdAt": 1,
  });
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0;
  await _.forEach(data, (val) => {
    switch (val.data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
    }
  });
  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });
  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "todayCount": todayCount,
    "graphData": graphData,
  };
  return responseJson;
};

adminAction.getHubsBasedCityData = async function (context) {
  const clientId = context.params.clientId;
  const todayCount = await this.adapter.count({
    "query": {
      "name": constantUtil.HUBS,
      "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "data.serviceArea":
        context.params?.city === ""
          ? { "$ne": null }
          : mongoose.Types.ObjectId(context.params.city),
    },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "name": constantUtil.HUBS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
        }
      : {
          "name": constantUtil.HUBS,
          "data.clientId": mongoose.Types.ObjectId(clientId),
          "data.status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        };

  const cityFilter =
    context.params.city === ""
      ? matchData
      : {
          ...matchData,
          "data.serviceArea": mongoose.Types.ObjectId(context.params.city),
        };

  const data = await this.adapter.model.find(cityFilter, {
    "data.status": 1,
    "createdAt": 1,
  });
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0;

  await _.forEach(data, (val) => {
    switch (val.data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
    }
  });

  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });
  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "todayCount": todayCount,
    "graphData": graphData,
  };
  return responseJson;
};

adminAction.getNotificationData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const reportsData = await this.adapter.model
    .find({
      "name": constantUtil.REPORTS,
      "data.isViewed": false,
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "data.status": constantUtil.NEW,
    })
    .sort({ "createdAt": -1 })
    .limit(5);

  let securityJson = await this.broker.emit("securityEscort.getUnViewedData", {
    ...context.params,
    "clientId": clientId,
  });
  securityJson = (securityJson && securityJson[0]) || [];
  let scheduleData = await this.broker.emit("booking.getNotifyRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  scheduleData = (scheduleData && scheduleData[0]) || [];
  let professionalCancelData = await this.broker.emit(
    "booking.professionalCancelRideList",
    { ...context.params, "clientId": clientId }
  );
  professionalCancelData =
    (professionalCancelData && professionalCancelData[0]) || [];
  return {
    "reportsData": await reportsData.map((reports) => {
      return {
        "_id": reports._id,
        "clientId": clientId,
        "userType": reports.data.userType,
        "firstName": reports.data.firstName,
        "lastName": reports.data.lastName,
        "phone": reports.data.phone,
        "createdAt": reports.createdAt,
        "status": reports.data.status,
        "comment": reports.data.comment,
      };
    }),
    "securityData": securityJson,
    "scheduleData": scheduleData,
    "professionalCancelData": professionalCancelData,
  };
};
// GODS VIEW
adminAction.getLocationProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let cityJson = await this.broker.emit("category.getServiceCategoryById", {
    "id": context.params.city,
    "clientId": clientId,
  });
  cityJson = cityJson && cityJson[0];
  if (cityJson.code === 0) {
    throw new MoleculerError("ERROR IN CITY DETAIL");
  }
  const polygon = turf.polygon(cityJson.data.location.coordinates);
  // cityJson = cityJson && cityJson?.[0]?.data;
  let professionalJson = await this.broker.emit(
    "professional.getProfessionalByAdmin",
    {
      "coordinates": cityJson.data.location.coordinates,
      "clientId": clientId,
      "vehicleCategoryId": context.params.vehicleCategoryId,
      "status": context.params.status,
      "onlineOffline": context.params.onlineOffline,
      "onGoing": context.params.onGoing,
    }
  );
  professionalJson = professionalJson && professionalJson[0];
  professionalJson = _.map(professionalJson, function (driver) {
    const point2 = turf.point(driver.location.coordinates);
    if (turf.inside(point2, polygon)) {
      return driver;
    }
  });
  const ids = [];
  const cat = await cityJson.data.categoryData.vehicles.map((e) =>
    mongoose.Types.ObjectId(e.categoryId)
  );
  ids.push(...cat);
  const vehicleCategoryList = await this.adapter.model.find(
    {
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "name": constantUtil.VEHICLECATEGORY,
      "_id": { "$in": ids },
    },
    {
      "_id": 1,
      "data.vehicleCategory": 1,
    }
  );
  const responseData = {
    "professional": professionalJson || [],
    "vehicleCategory":
      vehicleCategoryList.map((data) => {
        return {
          "_id": data._id,
          "category": data.data.vehicleCategory,
        };
      }) || [],
  };
  return responseData;
};

adminAction.getAllRidesList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let jsonData = await this.broker.emit("booking.getAllRidesList", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (!jsonData || jsonData?.code === 0) {
    throw new MoleculerError(jsonData?.message || "No Data Found");
  } else {
    return jsonData?.data || [];
  }
};
adminAction.getProfessionalsAndServiceArea = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const point = turf.point([
    parseFloat(context.params.lng),
    parseFloat(context.params.lat),
  ]);
  let cityJson = await this.broker.emit(
    "category.getServiceBasedCategoryData",
    {
      "categoryName": "ride",
      "clientId": clientId,
    }
  );
  cityJson = cityJson && cityJson[0];
  if (cityJson.length === 0) {
    throw new MoleculerError("ERROR IN CITY DETAILS");
  }
  let inside = false,
    cityId = "",
    vehicleCategoryList = [];
  const ids = [];
  await cityJson.forEach(async (city) => {
    if (typeof city.locationCoordinates !== "undefined") {
      const parentPolygon = turf.polygon(city.locationCoordinates);
      if (turf.inside(point, parentPolygon)) {
        inside = true;
        cityId = city._id.toString();
        const cat = await city.vehicleCategoryData.map((e) =>
          mongoose.Types.ObjectId(e.categoryId)
        );
        ids.push(...cat);
      }
    }
  });
  if (inside === true) {
    if (context.params.bookingType == constantUtil.INSTANT) {
      const professionalJson = await this.broker.emit(
        "professional.getProfessionalByAdmin",
        {
          "coordinates": cityJson.location.coordinates,
          "clientId": clientId,
        }
      );
      professionalJson = professionalJson && professionalJson[0];
      if (professionalJson.length === 0) {
        throw new MoleculerError(
          "SORRY NO PROFESSIONALS AVAILABLE IN YOUR LOCATION"
        );
      } else {
        vehicleCategoryList = await this.adapter.model.find(
          {
            "name": constantUtil.VEHICLECATEGORY,
            //"data.clientId": mongoose.Types.ObjectId(clientId),
            "_id": { "$in": ids },
          },
          {
            "_id": 1,
            "data.vehicleCategory": 1,
          }
        );
      }
    } else {
      vehicleCategoryList = await this.adapter.model.find(
        {
          "name": constantUtil.VEHICLECATEGORY,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
          "_id": { "$in": ids },
        },
        {
          "_id": 1,
          "data.vehicleCategory": 1,
        }
      );
    }
  }
  return {
    "vehicleCategoryList": vehicleCategoryList.map((data) => {
      return {
        "_id": data._id,
        "category": data.data.vehicleCategory,
      };
    }),
    "cityId": cityId,
  };
};

adminAction.fetchBillingCycle = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("billings.getAdminBillingCycle", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("ERROR IN FETCH BILLINGS");
  } else {
    return responseJson;
  }
};

adminAction.fetchCoorperateBillingCycle = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "billings.getAdminCoorperateBillingCycle",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("ERROR IN FETCH BILLINGS");
  } else {
    return responseJson;
  }
};

adminAction.getWalletAmountList = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson;
  if (userType === constantUtil.USER) {
    responseJson = await this.broker.emit("user.getWalletAmountList", {
      ...context.params,
      "clientId": clientId,
    });
  } else {
    responseJson = await this.broker.emit("professional.getWalletAmountList", {
      ...context.params,
      "clientId": clientId,
    });
  }
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("ERROR IN WALLET LIST");
  } else {
    return responseJson;
  }
};

adminAction.addReports = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = {};
  if (userType === constantUtil.USER) {
    userData = {
      "firstName": context.meta.userDetails.firstName,
      "lastName": context.meta.userDetails.lastName,
      "phone": context.meta.userDetails.phone,
    };
  } else {
    userData = {
      "firstName": context.meta.professionalDetails.firstName,
      "lastName": context.meta.professionalDetails.lastName,
      "phone": context.meta.professionalDetails.phone,
    };
  }
  const updateData = {
    "clientId": clientId,
    "userType": userType,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "firstName": userData.firstName,
    "lastName": userData.lastName,
    "phone": userData.phone,
    "comment": context.params.comment,
    "status": constantUtil.NEW,
    "isViewed": false,
    "adminNotes": null,
  };
  const responseJson = await this.adapter.insert({
    "dataType": constantUtil.CLIENTDATA,
    "type": constantUtil.REPORTS,
    "name": constantUtil.REPORTS,
    "data": updateData,
  });
  if (!responseJson) {
    throw new MoleculerError("ERROR IN REPORTS");
  }
  const adminNotificationObject = {
    "clientId": clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_REPORTSNOTIFICATION,
      "timestamp": Date.now(),
      "message": "YOU HAVE REPORT NOTIFICATION",
      "details": mappingUtil.reportObject(responseJson),
    },
  };
  /* SOCKET PUSH NOTIFICATION */
  this.broker.emit(
    "socket.sendSecurityNotificationToAdmin",
    adminNotificationObject
  );
  return {
    "code": 200,
    "data": {},
    "message": "REPORTS ADDED SUCESSFULLY",
  };
};

adminAction.getReportsList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const query = {
    "name": constantUtil.REPORTS,
    //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : {
          "$in": [constantUtil.NEW, constantUtil.ATTENDED, constantUtil.CLOSED],
        };
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.phone.code",
      "data.phone.number",
    ],
  });
  const jsonData = await this.adapter.find({
    "query": query,
    "limit": limit,
    "offset": skip,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.firstName",
      "data.lastName",
      "data.phone.code",
      "data.phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = jsonData.map((respo) => ({
    "_id": respo._id,
    "data": {
      "firstName": respo.data.firstName,
      "lastName": respo.data.lastName,
      "phone": respo.data.phone,
      "comment": respo.data.comment,
      "userType": respo.data.userType,
      "adminNotes": respo.data.adminNotes,
      "status": respo.data.status,
    },
  }));
  return responseJson;
};

adminAction.reportChangeStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.REPORTS,
      //"data.clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "message": "Updated Successfully" };
  }
};

adminAction.addReportsNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
      "name": constantUtil.REPORTS,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.adminNotes": context.params.notes }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("ERROR IN NOTES");
  } else {
    return { "code": 1, "message": "UPDATED SUCCESSFULLY" };
  }
};

adminAction.viewReports = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.id),
    "name": constantUtil.REPORTS,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  });
  if (!responseJson) {
    throw new MoleculerError("ERROR IN REPORT DETAIL");
  } else {
    return {
      "code": 1,
      "response": {
        "_id": responseJson._id,
        ...responseJson.data,
      },
      "message": "",
    };
  }
};
// SECURITY SERVICES MANAGEMENT
adminAction.getSecurityList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("securityEscort.getSecurityList", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson.length === 0) {
    throw new MoleculerError("ERROR IN LIST");
  } else {
    return responseJson;
  }
};

adminAction.getSecurityClosedList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "securityEscort.getSecurityClosedList",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.length === 0) {
    throw new MoleculerError("ERROR IN LIST");
  } else {
    return responseJson;
  }
};

adminAction.getSecurityOperatorList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "securityEscort.getSecurityOperatorList",
    {
      ...context.params,
      ...context.meta,
      "clientId": clientId,
    }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.length === 0) {
    throw new MoleculerError("ERROR IN LIST");
  } else {
    return responseJson;
  }
};

adminAction.changeSecurityStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "securityEscort.changeSecurityStatus",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 0) {
    throw new MoleculerError("ERROR IN STATUS CHANGE");
  } else {
    return responseJson;
  }
};

adminAction.addSecurityNotes = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("securityEscort.addSecurityNotes", {
    ...context.params,
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 0) {
    throw new MoleculerError("ERROR IN UPDATING NOTES");
  } else {
    return responseJson;
  }
};

adminAction.operatorAcceptSecurity = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "securityEscort.operatorAcceptSecurity",
    {
      "id": context.params.id,
      "adminId": context.meta.adminId,
      "clientId": clientId,
    }
  );
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 0) {
    throw new MoleculerError("ERROR IN UPDATING NOTES");
  } else {
    return responseJson;
  }
};

//INVITE AND EARN SETTINGS
adminAction.getInviteAndEarnConfig = async function (context) {
  // const responseJson = await storageUtil.read(constantUtil.INVITEANDEARN);
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      // "type": constantUtil.INVITEANDEARN,
      "name": constantUtil.INVITEANDEARN,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Invite And Earn Settings Data");
  } else {
    return responseJson;
  }
};
adminAction.updateInviteAndEarn = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "type": constantUtil.INVITEANDEARN,
        "name": constantUtil.INVITEANDEARN,
      },
      {
        //for users
        "data.forUser.showText": context.params.forUser.showText,
        "data.forUser.amountToInviter": context.params.forUser.amountToInviter,
        "data.forUser.amountToJoiner": context.params.forUser.amountToJoiner,
        "data.forUser.image": context.params.forUser.image,
        "data.forUser.description": context.params.forUser.description,
        "data.forUser.messageText": context.params.forUser.messageText,
        "data.forUser.rideCount": context.params.forUser.rideCount,
        "data.forUser.isEnableReferralRideEarning":
          context.params.forUser.isEnableReferralRideEarning,
        "data.forUser.referralRideEarningPercentage":
          context.params.forUser.referralRideEarningPercentage,
        "data.forUser.languageKeys": context.params.forUser.languageKeys,
        //for professional
        "data.forProfessional.showText":
          context.params.forProfessional.showText,
        "data.forProfessional.amountToInviter":
          context.params.forProfessional.amountToInviter,
        "data.forProfessional.amountToJoiner":
          context.params.forProfessional.amountToJoiner,
        "data.forProfessional.image": context.params.forProfessional.image,
        "data.forProfessional.description":
          context.params.forProfessional.description,
        "data.forProfessional.messageText":
          context.params.forProfessional.messageText,
        "data.forProfessional.rideCount":
          context.params.forProfessional.rideCount,
        "data.forProfessional.isEnableReferralRideEarning":
          context.params.forProfessional.isEnableReferralRideEarning,
        "data.forProfessional.referralRideEarningPercentage":
          context.params.forProfessional.referralRideEarningPercentage,
        "data.forProfessional.languageKeys":
          context.params.forProfessional.languageKeys,
      },
      { "new": true }
    )
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    storageUtil.write(constantUtil.INVITEANDEARN, responseJson);
    return responseJson;
  }
};
adminAction.updateInviteAndEarnImage = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${clientId}_${context.params.for}${constantUtil.INVITEANDEARN}`,
    });
    return {
      "image": s3Image[0],
    };
  }
};
adminAction.saveErrorData = async function (context) {
  const responseJson = await this.adapter.model.create({
    "dataType": constantUtil.CLIENTDATA,
    "name": constantUtil.ERRORDATA,
    "data.userType": context.params.userType,
    "data.parameter": context.params.parameter,
    "data.errorCode": context.params.errorCode,
    "data.errorResponse": context.params.errorResponse.toString(),
    "data.phone.code": context.params.phone.code,
    "data.phone.number": context.params.phone.number,
  });
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};
// COORPERATE OFFICE
adminAction.getCoorperateOfficeList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta.clientId;

  const query = {
    "name": constantUtil.COORPERATEOFFICE,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  if (
    context.params.paidStatusFilter &&
    context.params.paidStatusFilter === true
  )
    query["$expr"] = {
      "$gt": [
        {
          "$size": {
            "$cond": [
              { "$isArray": "$data.billingData" },
              "$data.billingData",
              [],
            ],
          },
        },
        0,
      ],
    };
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.officeName",
      "data.officeId",
      "data.financePersonName",
      "data.contactPersonName",
      "data.email",
      "data.phone.number",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.officeName",
      "data.officeId",
      "data.financePersonName",
      "data.contactPersonName",
      "data.email",
      "data.phone.number",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "officeName": respo.data.officeName,
      "officeId": respo.data.officeId,
      "email": respo.data.email,
      "phone": respo.data.phone,
      "status": respo.data.status,
      "locationAddress": respo.data.locationAddress,
    },
  }));
  return responseJson;
};
adminAction.updateCoorperateOffice = async function (context) {
  const { INFO_ALREADY_EXISTS } = notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  let responseJson;
  const clientId = context.params.data.clientId || context.meta.clientId;
  const responseOfficeJson = await this.adapter.model
    .findOne({
      "name": constantUtil.COORPERATEOFFICE,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.phone.code": context.params.data.phone.code,
      "data.phone.number": context.params.data.phone.number,
      // "data.officeId": context.params.data.officeId,
    })
    .lean();
  if ((context.params?.data?.password || "") !== "") {
    context.params.data.password = bcrypt.hashSync(
      context.params.data.password,
      bcrypt.genSaltSync(8),
      null
    );
  } else {
    context.params.data.password = responseOfficeJson?.data?.password || null;
  }
  try {
    context.params.data.guidelines = JSON.parse(context.params.data.guidelines);
  } catch (error) {
    context.params.data.guidelines = [];
  }
  if (context.params.id) {
    if (
      responseOfficeJson &&
      responseOfficeJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Phone Number Already Exists"); //INFO_PHONE_NUMBER_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": context.params.data }
    );
    //Update serviceAreaId for the Corprate
    if (responseJson) {
      await this.broker.emit("category.updateServiceArea", {
        "categoryName": "corporateride",
        "corporateId": context.params.id.toString(),
        "serviceAreaId": context.params.data.serviceAreaId.toString(),
        //"clientId": clientId,
      });
    }
  } else {
    if (responseOfficeJson) {
      throw new MoleculerError("Phone Number Already Axists");
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.COORPERATEOFFICE,
      "name": constantUtil.COORPERATEOFFICE,
      "data": context.params.data,
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getCoorperateOffice = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne(
      {
        "name": constantUtil.COORPERATEOFFICE,
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "data.accessToken": 0,
        "data.otp": 0,
        "data.password": 0,
      }
    )
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid Emergency Response Office Details");
  } else {
    // let serviceArea = await this.broker.emit("category.getServiceCategoryById", {
    //   "id": responseJson.data.serviceArea,
    // });
    // serviceArea = serviceArea && serviceArea[0];
    // responseJson["categoryData"] = {};
    // responseJson["categoryData"] = serviceArea.data || {};
    responseJson["data"]["id"] = responseJson?._id;
    return responseJson;
  }
};
adminAction.changeCoorperateOfficeStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.COORPERATEOFFICE,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};
adminAction.getAllCoorperateOffices = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let projectedFields = {
    "_id": "$_id",
    "officeName": "$data.officeName",
    "officeId": "$data.officeId",
    "amount": "$data.amount",
    "serviceAreaId": "$data.serviceAreaId",
    "categoryIds": "$data.categoryIds",
  };
  switch (context.params.requestFrom?.toUpperCase()) {
    case constantUtil.DISPATCH:
      projectedFields["guidelines"] = "$data.guidelines";
      projectedFields["isEnableFixedChangeRide"] = {
        "$ifNull": ["$data.isEnableFixedChangeRide", false],
      };
      break;

    case constantUtil.CORPORATEOFFICER:
      break;

    default:
      projectedFields["city"] = "$data.locationAddress.city";
      projectedFields["address"] = "$addressList.address";
      break;
  }
  const responseOffice = await this.adapter.model.aggregate([
    {
      "$match": {
        "data.clientId": mongoose.Types.ObjectId(clientId),
        "name": constantUtil.COORPERATEOFFICE,
        "data.status": constantUtil.ACTIVE,
      },
    },
    {
      "$lookup": {
        "from": "categories",
        "localField": "data.serviceAreaId",
        "foreignField": "_id",
        "as": "addressList",
      },
    },
    {
      "$unwind": {
        "path": "$addressList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    { "$project": projectedFields },
    { "$sort": { "officeName": 1 } },
  ]);
  return responseOffice;
  //   .find({
  //     "name": constantUtil.COORPERATEOFFICE,
  //     "data.status": constantUtil.ACTIVE,
  //   })
  //   .sort({ "data.officeName": 1 });
  // return responseOffice.map((data) => {
  //   return {
  //     "_id": data._id,
  //     "officeName": data.data.officeName,
  //     "officeId": data.data.officeId,
  //     "isEnableFixedChangeRide": data.data?.isEnableFixedChangeRide || false,
  //   };
  // });
};

adminAction.updateCoorperateOfficePrivileges = async function (context) {
  const clientId =
    context.params?.data?.clientId ||
    context.params.clientId ||
    context.meta.clientId;
  const responseJson = await this.adapter.model.updateOne(
    {
      "name": constantUtil.COORPERATEPRIVILEGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data": context.params.data }
  );
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getCoorperateOfficePrivileges = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.COORPERATEPRIVILEGES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Error in Response office Privileges");
  } else {
    return responseJson;
  }
};

// COORPERATE OFFICE BILLING LIST
adminAction.getCoorperateOfficeBillingList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "billings.getCoorperateOfficeBillingList",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("Error in Response office Privileges");
  } else {
    return responseJson;
  }
};

adminAction.changeCoorperatePaidStatus = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit(
    "billings.changeCoorperatePaidStatus",
    { ...context.params, "clientId": clientId }
  );
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("ERROR IN PAID STATUS UPDATE");
  }
  if (context.params.status === true) {
    await this.adapter.model.updateOne(
      {
        "name": constantUtil.COORPERATEOFFICE,
        "_id": mongoose.Types.ObjectId(context.params.coorperateId.toString()),
        //"data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "$pull": {
          "data.billingData": {
            "billingId": mongoose.Types.ObjectId(
              context.params.billingId.toString()
            ),
          },
        },
      }
    );
  } else {
    await this.adapter.model.updateOne(
      {
        "name": constantUtil.COORPERATEOFFICE,
        "_id": mongoose.Types.ObjectId(context.params.coorperateId.toString()),
        //"data.clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "$push": {
          "data.billingData": {
            "billingId": mongoose.Types.ObjectId(
              context.params.billingId.toString()
            ),
            "paidStatus": false,
          },
        },
      }
    );
  }
  return responseJson;
};

// EMAIL TEMPLATE MANAGEMENT
adminAction.getEmailTemplateList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta.clientId;

  const query = {
    // "name": constantUtil.EMAILTEMPLATE,
    "type": constantUtil.EMAILTEMPLATE,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0) // only send date so set that time start
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999) //to end
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.name",
      "data.emailSubject",
      "data.senderName",
      "data.senderEmail",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.name",
      "data.emailSubject",
      "data.senderName",
      "data.senderEmail",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "name": respo.data.name,
      "emailSubject": respo.data.emailSubject,
      "senderName": respo.data.senderName,
      "senderEmail": respo.data.senderEmail,
      "status": respo.data.status,
      "emailTemplateEditableFields":
        respo.data.emailTemplateEditableFields || [],
    },
  }));
  return responseJson;
};
adminAction.updateEmailTemplate = async function (context) {
  // console.log(context.params)
  // if id exist it means request update is avialble => else create new
  const { INVALID_BOOKING } = await notifyMessage.setNotifyLanguage(
    context.params?.languageCode
  );
  let responseData;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.id) {
    responseData = await this.adapter.model.findOneAndUpdate(
      {
        // "name": constantUtil.EMAILTEMPLATE,
        "type": constantUtil.EMAILTEMPLATE,
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      },
      {
        "data.emailTemplateEditableFields":
          context.params.emailTemplateEditableFields,
        "data.name": context.params.name,
        "data.emailSubject": context.params.emailSubject,
        "data.senderName": context.params.senderName,
        "data.senderEmail": context.params.senderEmail,
        "data.emailContent": context.params.emailContent,
        "data.status": context.params.status,
      },
      { "new": true }
    );
    if (!responseData) {
      throw new MoleculerError("UPDATED TEMPLATE IS NOT FOUND");
    }
  }
  if (!context.params.id) {
    console.log("i am inside");
    // first find email alredy exist
    const exist = await this.adapter.model.countDocuments({
      // "name": constantUtil.EMAILTEMPLATE,
      "type": constantUtil.EMAILTEMPLATE,
      "data.name": context.params.name,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    });
    if (exist) {
      throw new MoleculerError("EMAIL TEMPLATE IS ALREADY EXIST");
    }
    responseData = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.EMAILTEMPLATE,
      "name": constantUtil.EMAILTEMPLATE,
      "data": {
        "clientId": clientId,
        "name": context.params.name,
        "emailTemplateEditableFields":
          context.params.emailTemplateEditableFields,
        "emailSubject": context.params.emailSubject,
        "senderName": context.params.senderName,
        "senderEmail": context.params.senderEmail,
        "emailContent": context.params.emailContent,
        "status": context.params.status,
      },
    });
    if (!responseData) {
      new MoleculerError("CANNOT ADD TEMPLATE");
    }
  }
  return responseData;
};

adminAction.editEmailTemplate = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      // "name": constantUtil.EMAILTEMPLATE,
      "type": constantUtil.EMAILTEMPLATE,
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};

adminAction.changeEmailTemplateStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      // "name": constantUtil.EMAILTEMPLATE,
      "type": constantUtil.EMAILTEMPLATE,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};
// POPULAR PLACE MANAGEMENT
adminAction.getPopularPlacesList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta.clientId;
  const query = {
    "name": constantUtil.POPULARPLACE,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  if ((context.params?.city || "") !== "") {
    query["data.serviceArea"] = mongoose.Types.ObjectId(
      context.params.city.toString()
    );
  }
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.placeName",
      "data.placeDescription",
      "data.locationAddress",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.placeName",
      "data.placeDescription",
      "data.locationAddress",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "city": respo.data.serviceArea,
      "placeName": respo.data.placeName,
      "placeDescription": respo.data.placeDescription,
      "placeImage": respo.data.placeImage,
      "locationAddress": respo.data.locationAddress,
      "status": respo.data.status,
    },
  }));
  return responseJson;
};

adminAction.updatePopularPlace = async function (context) {
  let responseJson;
  const clientId =
    context.params?.data?.clientId ||
    context.params.clientId ||
    context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${clientId}_${context.params.placeName}${context.params.serviceArea}${constantUtil.POPULARPLACE}`,
    });
    context.params.placeImage = s3Image[0];
  }
  const docsJson = await this.adapter.model
    .findOne({
      "data.placeName": context.params.data.placeName,
      "name": constantUtil.POPULARPLACE,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Popular Place Already Exists"); //INFO_POPULAR_PLACE_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": context.params.data }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Popular Place Already Exists"); //INFO_POPULAR_PLACE_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.POPULARPLACE,
      "name": constantUtil.POPULARPLACE,
      "data": context.params.data,
    });
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.popularplaceUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Airport Time
  return responseJson;
};

adminAction.uploadPopularPlaceImage = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${clientId}_${context.params.placeName}${context.params.serviceArea}${constantUtil.POPULARPLACE}`,
    });
    return s3Image[0];
  } else {
    return null;
  }
};

adminAction.getEditPopularPlace = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.POPULARPLACE,
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};
adminAction.changePopularPlaceStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.POPULARPLACE,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.popularplaceUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Airport Time

  return { "code": 1, "response": "Updated Successfully" };
};

adminAction.getPopularPlaces = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let getServiceCategory = await this.broker.emit(
    "category.getPopularServiceCategory",
    { ...context.params, "clientId": clientId }
  );
  getServiceCategory = getServiceCategory && getServiceCategory[0];
  if (!getServiceCategory) {
    throw new MoleculerError("SORRY WE DONT PROVIDE SERVICE HERE");
  }
  const responseJson = await this.adapter.model
    .find({
      "name": constantUtil.POPULARPLACE,
      "data.status": constantUtil.ACTIVE,
      "data.serviceArea": mongoose.Types.ObjectId(
        getServiceCategory._id.toString()
      ),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("SORRY.. NO POPULAR PLACES HERE");
  }
  return {
    "code": 200,
    "data": await responseJson.map((each) => {
      return {
        "_id": each._id,
        "clientId": clientId,
        "city": each.data.serviceArea,
        "placeName": each.data.placeName,
        "placeDescription": each.data.placeDescription,
        "placeImage": each.data.placeImage,
        "locationAddress": each.data.locationAddress,
        "location": each.data.location,
        "locationCoordinates": each.data.locationCoordinates,
        "status": each.data.status,
      };
    }),
    "message": "",
  };
};

adminAction.getAllPopularPlacesData = async function () {
  const clientId = context.params.clientId || context.meta.clientId;
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const query = {
    "name": constantUtil.POPULARPLACE,
    "data.status": constantUtil.ACTIVE,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  const responseOffice = await this.adapter.model.find(query).lean();
  const responseJson = await responseOffice.map((each) => ({
    "_id": each._id,
    "clientId": clientId,
    "city": each.data.serviceArea,
    "placeName": each.data.placeName,
    "placeDescription": each.data.placeDescription,
    "placeImage": each.data.placeImage,
    "locationAddress": each.data.locationAddress,
    "location": each.data.location,
    "locationCoordinates": each.data.locationCoordinates,
    "status": each.data.status,
  }));
  return {
    "code": 200,
    "data": {
      "response": responseJson,
      "timestamp": generalSettings.data.popularplaceUpdateTime || Date.now(),
    },
    "message": "",
  };
};

// AIRPORT MANAGEMENT
adminAction.getAirportList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params.clientId || context.meta.clientId;
  const query = {
    "name": constantUtil.AIRPORT,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER
  if ((context.params?.city || "") !== "")
    query["data.serviceArea"] = mongoose.Types.ObjectId(
      context.params.city.toString()
    );
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.airportShortName",
      "data.airportLongName",
      "data.airportDescription",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.airportShortName",
      "data.airportLongName",
      "data.airportDescription",
    ],
  });
  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "airportShortName": respo.data.airportShortName,
      "airportLongName": respo.data.airportLongName,
      "airportDescription": respo.data.airportDescription,
      "airportImage": respo.data.airportImage,
      "status": respo.data.status,
      "serviceArea": respo.data.serviceArea,
    },
  }));
  return responseJson;
};

adminAction.updateAirport = async function (context) {
  let responseJson;
  const clientId =
    context.params?.data?.clientId ||
    context.params.clientId ||
    context.meta.clientId;
  const docsJson = await this.adapter.model
    .findOne({
      "data.airportLongName": context.params.data.airportLongName,
      "name": constantUtil.AIRPORT,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Airport Already Exists"); //INFO_AIRPORT_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "$set": { "data": context.params.data } }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Airport Already Exists"); //INFO_AIRPORT_EXISTS
    } else {
      responseJson = await this.adapter.insert({
        "dataType": constantUtil.CLIENTDATA,
        "name": constantUtil.AIRPORT,
        "data": context.params.data,
      });
    }
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.airportUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  } else {
    settingsUpdate = await this.adapter.model
      .findOne({
        "name": constantUtil.GENERALSETTING,
      })
      .lean();
    await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    // Setting Update for Airport Time
    return responseJson;
  }
};

adminAction.uploadAirportImage = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${clientId}_${context.params.airportShortName}${context.params.serviceArea}${constantUtil.AIRPORT}`,
    });
    return s3Image[0];
  } else {
    return null;
  }
};

adminAction.getEditAirport = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.AIRPORT,
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};

adminAction.changeAirportStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.AIRPORT,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.airportUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Airport Time

  return { "code": 1, "response": "Updated Successfully" };
};

adminAction.getAirportStopList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const clientId = context.params.clientId || context.meta.clientId;

  const query = [];
  query.push(
    {
      "$match": {
        "_id": mongoose.Types.ObjectId(context.params.airportId.toString()),
        "data.clientId": mongoose.Types.ObjectId(clientId),
      },
    },
    { "$unwind": "$data.stops" }
  );
  query.push(
    (context.params.filter || "") !== ""
      ? { "$match": { "data.stops.status": { "$eq": context.params.filter } } }
      : {
          "$match": {
            "data.stops.status": {
              "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE],
            },
          },
        }
  );

  if ((context.params.search || "") !== "")
    query.push({
      "$match": {
        "$or": [
          {
            "data.stops.name": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "data.stops._id": -1 } },
        { "$skip": skip },
        { "$limit": limit },
        {
          "$project": {
            "airportId": "$_id",
            "_id": "$data.stops._id",
            "name": "$data.stops.name",
            "location": "$data.stops.location",
            "status": "$data.stops.status",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};

adminAction.updateAirportStops = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const airportData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.airportId.toString()),
    "name": constantUtil.AIRPORT,
  });
  const mongoId = context.params.id
    ? context.params.id
    : mongoose.Types.ObjectId();

  let stops = airportData.data.stops || [];
  let isStopAvail;
  if (
    airportData.data &&
    airportData.data.stops &&
    Array.isArray(airportData.data.stops) &&
    airportData.data.stops.length > 0
  )
    if (!context.params.id) {
      airportData.data.stops.map((stop) => {
        if (stop.name.toLowerCase() === context.params.name.toLowerCase())
          isStopAvail = stop;
      });
    } else {
      airportData.data.stops.map((stop) => {
        if (stop.name.toLowerCase() === context.params.name.toLowerCase())
          isStopAvail = stop;
      });
    }
  if (!context.params.id) {
    if (isStopAvail) {
      throw new MoleculerError("Stop is already exist");
    } else {
      stops.push({
        "_id": mongoId,
        "name": context.params.name,
        "location": context.params.location,
        "status": context.params.status,
      });
    }
  } else {
    if (
      !isStopAvail ||
      isStopAvail == {} ||
      isStopAvail["_id"] == undefined ||
      (isStopAvail &&
        isStopAvail !== {} &&
        isStopAvail["_id"] !== undefined &&
        isStopAvail["_id"].toString() === context.params.id.toString())
    ) {
      stops = airportData.data.stops.map((stop) => {
        if (stop._id.toString() === mongoId.toString()) {
          stop.name = context.params.name;
          stop.location = context.params.location;
          stop.status = context.params.status;
        }
        return stop;
      });
    } else {
      throw new MoleculerError("Stop is already exist");
    }
  }
  const responseJson = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.airportId.toString()),
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.stops": stops }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.airportUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Airport Time

  return { "code": 1, "response": "Updated Successfully" };
};
adminAction.getEditAirportStop = async function (context) {
  let responseJson = await this.adapter.model.findOne(
    {
      "name": constantUtil.AIRPORT,
      "_id": mongoose.Types.ObjectId(context.params.airportId.toString()),
    },
    { "data.stops": 1 }
  );
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    responseJson = responseJson.data.stops.filter((data) => {
      return data._id.toString() === context.params.id.toString();
    });
    return responseJson[0];
  }
};
adminAction.changeAirportStopStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "name": constantUtil.AIRPORT,
      "_id": mongoose.Types.ObjectId(context.params.airportId.toString()),
      "data.clientId": mongoose.Types.ObjectId(clientId),
      "data.stops": { "$elemMatch": { "_id": { "$in": ids } } },
    },
    {
      "$set": { "data.stops.$[list].status": context.params.status },
    },
    { "arrayFilters": [{ "list._id": { "$in": ids } }] }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for Airport Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.airportUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Airport Time

  return { "code": 1, "response": "Updated Successfully" };
};

adminAction.getAirports = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let getServiceCategory = await this.broker.emit(
    "category.getPopularServiceCategory",
    { ...context.params, "clientId": clientId }
  );
  getServiceCategory = getServiceCategory && getServiceCategory[0];
  if (!getServiceCategory) {
    throw new MoleculerError("SORRY WE DONT PROVIDE SERVICE HERE");
  }
  const responseJson = await this.adapter.model
    .find({
      "name": constantUtil.AIRPORT,
      "data.status": constantUtil.ACTIVE,
      "data.serviceArea": mongoose.Types.ObjectId(
        getServiceCategory._id.toString()
      ),
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("SORRY.. NO POPULAR PLACES HERE");
  } else {
    return {
      "code": 200,
      "data": await responseJson.map((each) => {
        return {
          "_id": each._id,
          "clientId": clientId,
          "city": each.data.serviceArea,
          "airportShortName": each.data.airportShortName,
          "airportLongName": each.data.airportLongName,
          "airportDescription": each.data.airportDescription,
          "airportImage": each.data.airportImage,
          "location": each.data.location,
          "locationAddress": each.data.locationAddress,
          "locationCoordinates": each.data.locationCoordinates,
          "stops": each.data?.stops || [],
          "status": each.data.status,
          "fareCategories": each.data?.tollCategorys || [],
        };
      }),
      "message": "",
    };
  }
};

adminAction.getAllAirportData = async function (context) {
  let responseJson = [];
  const clientId = context.params?.clientId || context.meta?.clientId;
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

  //  #endregion Get Config Settings
  // const query = {
  //   "name": constantUtil.AIRPORT,
  //   "data.status": constantUtil.ACTIVE,
  // };
  // const responseOffice = await this.adapter.model.find(query);

  // const responseJson = await responseOffice.map((respo) => ({
  //   "_id": respo._id,
  //   "city": respo.data.serviceArea,
  //   "airportShortName": respo.data.airportShortName,
  //   "airportLongName": respo.data.airportLongName,
  //   "airportDescription": respo.data.airportDescription,
  //   "airportImage": respo.data.airportImage,
  //   "location": respo.data.location,
  //   "locationAddress": respo.data.locationAddress,
  //   "locationCoordinates": respo.data.locationCoordinates,
  //   "stops": respo.data.stops,
  //   "status": respo.data.status,
  // }));
  responseJson = await this.broker.emit("admin.getAllAirportData", {
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  return {
    "code": 200,
    "data": {
      "response": responseJson,
      "timestamp": generalSettings.data.airportUpdateTime || Date.now(),
    },
    "message": "",
  };
};

adminAction.getAllAirportDataForWebBookingApp = async function (context) {
  let responseJson = null;
  const secretKey = context.params?.secretKey || "";
  const clientId = context.params?.clientId || context.meta?.clientId;
  // const genConfig = await this.schema.adapter.model
  //   .findOne(
  //     {
  //       "name": constantUtil.GENERALSETTING,
  //       "data.secretKey": secretKey,
  //     },
  //     { "data.secretKey": 1, "data.clientId": 1, "data.airportUpdateTime": 1, }
  //   )
  //   .lean();
  // clientId = genConfig?.data?.clientId?.toString();
  // if (!clientId) {
  //   throw new MoleculerError("Invalid Data");
  // }
  responseJson = await this.broker.emit("admin.getAllAirportData", {
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  return {
    "code": 200,
    "data": responseJson,
    "message": "",
  };
};

// AIRPORT TRANSFER SETTINGS
adminAction.getAirportTransferConfig = async function (context) {
  let responseJson = null;
  const secretKey = context.params?.secretKey || "";
  const clientId = context.params?.clientId || context.meta?.clientId;
  // const genConfig = await this.schema.adapter.model
  //   .findOne(
  //     {
  //       "name": constantUtil.GENERALSETTING,
  //       "data.secretKey": secretKey,
  //     },
  //     { "data.secretKey": 1, "data.clientId": 1 }
  //   )
  //   .lean();
  // clientId = genConfig?.data?.clientId?.toString();
  // if (!clientId) {
  //   throw new MoleculerError("Invalid Data");
  // }
  responseJson = await this.adapter.model
    .aggregate([
      {
        "$match": {
          "type": constantUtil.CONST_AIRPORTTRANSFERSETTING,
          "name": constantUtil.CONST_AIRPORTTRANSFERSETTING,
          // "data.clientId": mongoose.Types.ObjectId(clientId),
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "data.bookingPassengerConfig.categoryId",
          "foreignField": "_id",
          "as": "bookingPassengerConfig",
        },
      },
      {
        "$unwind": {
          "path": "$bookingPassengerConfig",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "data.bookingLuggageConfig.categoryId",
          "foreignField": "_id",
          "as": "bookingLuggageConfig",
        },
      },
      {
        "$unwind": {
          "path": "$bookingLuggageConfig",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "data.returningPassengerConfig.categoryId",
          "foreignField": "_id",
          "as": "returningPassengerConfig",
        },
      },
      {
        "$unwind": {
          "path": "$returningPassengerConfig",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "data.returningLuggageConfig.categoryId",
          "foreignField": "_id",
          "as": "returningLuggageConfig",
        },
      },
      {
        "$unwind": {
          "path": "$returningLuggageConfig",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "data.waterBottleConfig.categoryId",
          "foreignField": "_id",
          "as": "waterBottleConfig",
        },
      },
      {
        "$unwind": {
          "path": "$waterBottleConfig",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$group": {
          "_id": "$_id",
          //
          "isEnableBookingPassengerConfig": {
            "$first": "$data.isEnableBookingPassengerConfig",
          },
          "bookingPassengerConfigData": {
            "$addToSet": {
              "bookingPassengerConfigData": "$data.bookingPassengerConfig",
            },
          },
          "bookingPassengerConfig": {
            "$addToSet": {
              "categoryId": "$bookingPassengerConfig._id",
              "name": "$bookingPassengerConfig.data.name",
              "description": "$bookingPassengerConfig.data.description",
            },
          },
          //
          "isEnableBookingLuggageConfig": {
            "$first": "$data.isEnableBookingLuggageConfig",
          },
          "bookingLuggageConfigData": {
            "$addToSet": {
              "bookingLuggageConfigData": "$data.bookingLuggageConfig",
            },
          },
          "bookingLuggageConfig": {
            "$addToSet": {
              "categoryId": "$bookingLuggageConfig._id",
              "name": "$bookingLuggageConfig.data.name",
              "description": "$bookingLuggageConfig.data.description",
            },
          },
          //
          "isEnableFlightNumber": {
            "$first": "$data.isEnableFlightNumber",
          },
          "isEnableWelcomeSignboard": {
            "$first": "$data.isEnableWelcomeSignboard",
          },
          "isEnableReturnTransfer": {
            "$first": "$data.isEnableReturnTransfer",
          },
          //
          "isEnableReturningPassengerConfig": {
            "$first": "$data.isEnableReturningPassengerConfig",
          },
          "returningPassengerConfigData": {
            "$addToSet": {
              "returningPassengerConfigData": "$data.returningPassengerConfig",
            },
          },
          "returningPassengerConfig": {
            "$addToSet": {
              "categoryId": "$returningPassengerConfig._id",
              "name": "$returningPassengerConfig.data.name",
              "description": "$returningPassengerConfig.data.description",
            },
          },
          //
          "isEnableReturningLuggageConfig": {
            "$first": "$data.isEnableReturningLuggageConfig",
          },
          "returningLuggageConfigData": {
            "$addToSet": {
              "returningLuggageConfigData": "$data.returningLuggageConfig",
            },
          },
          "returningLuggageConfig": {
            "$addToSet": {
              "categoryId": "$returningLuggageConfig._id",
              "name": "$returningLuggageConfig.data.name",
              "description": "$returningLuggageConfig.data.description",
            },
          },
          //
          "isProvideWaterBottles": { "$first": "$data.isProvideWaterBottles" },
          "isEnableLanguagePreference": {
            "$first": "$data.isEnableLanguagePreference",
          },
          "waterBottleConfigData": {
            "$addToSet": {
              "waterBottleConfigData": "$data.waterBottleConfig",
            },
          },
          "waterBottleConfig": {
            "$addToSet": {
              "categoryId": "$waterBottleConfig._id",
              "name": "$waterBottleConfig.data.name",
              "description": "$waterBottleConfig.data.description",
            },
          },
        },
      },
    ])
    .allowDiskUse(true);

  if (responseJson.length > 0) {
    responseJson = responseJson[0];
    // booking Passenger Config
    responseJson?.bookingPassengerConfig?.forEach((item, idx) => {
      if (!item?.categoryId) {
        responseJson["bookingPassengerConfig"] = [];
      } else {
        const selectedBookingPassengerConfigData =
          responseJson["bookingPassengerConfigData"][0][
            "bookingPassengerConfigData"
          ]?.filter(
            (each) => each.categoryId.toString() === item.categoryId.toString()
          )?.[0] || {};
        //
        item["count"] = selectedBookingPassengerConfigData?.count || 0;
        item["amount"] = selectedBookingPassengerConfigData?.amount || 0;
      }
    });
    // booking Luggage Config
    responseJson?.bookingLuggageConfig?.forEach((item, idx) => {
      if (!item?.categoryId) {
        responseJson["bookingLuggageConfig"] = [];
      } else {
        const selectedBookingLuggageConfigData =
          responseJson["bookingLuggageConfigData"][0][
            "bookingLuggageConfigData"
          ]?.filter(
            (each) => each.categoryId.toString() === item.categoryId.toString()
          )?.[0] || {};
        //
        item["count"] = selectedBookingLuggageConfigData?.count || 0;
        item["amount"] = selectedBookingLuggageConfigData?.amount || 0;
      }
    });
    // returning Passenger Config
    responseJson?.returningPassengerConfig?.forEach((item, idx) => {
      if (!item?.categoryId) {
        responseJson["returningPassengerConfig"] = [];
      } else {
        const selectedReturningPassengerConfigData =
          responseJson["returningPassengerConfigData"][0][
            "returningPassengerConfigData"
          ]?.filter(
            (each) => each.categoryId.toString() === item.categoryId.toString()
          )?.[0] || {};
        //
        item["count"] = selectedReturningPassengerConfigData?.count || 0;
        item["amount"] = selectedReturningPassengerConfigData?.amount || 0;
      }
    });
    // returning Luggage Config
    responseJson.returningLuggageConfig?.forEach((item, idx) => {
      if (!item?.categoryId) {
        responseJson["returningLuggageConfig"] = [];
      } else {
        const selectedReturningLuggageConfigData =
          responseJson["returningLuggageConfigData"][0][
            "returningLuggageConfigData"
          ]?.filter(
            (each) => each.categoryId.toString() === item.categoryId.toString()
          )?.[0] || {};
        //
        item["count"] = selectedReturningLuggageConfigData?.count || 0;
        item["amount"] = selectedReturningLuggageConfigData?.amount || 0;
      }
    });
    // Water Bottle Config
    responseJson?.waterBottleConfig?.forEach((item, idx) => {
      if (!item?.categoryId) {
        responseJson["waterBottleConfig"] = [];
      } else {
        const selectedWaterBottleConfigData =
          responseJson["waterBottleConfigData"][0][
            "waterBottleConfigData"
          ]?.filter(
            (each) => each.categoryId.toString() === item.categoryId.toString()
          )?.[0] || {};
        //
        item["count"] = selectedWaterBottleConfigData?.count || 0;
        item["amount"] = selectedWaterBottleConfigData?.amount || 0;
      }
    });
    // Clear Config Data
    responseJson["bookingPassengerConfigData"] = undefined;
    responseJson["bookingLuggageConfigData"] = undefined;
    responseJson["returningPassengerConfigData"] = undefined;
    responseJson["returningLuggageConfigData"] = undefined;
    responseJson["waterBottleConfigData"] = undefined;
  } else {
    responseJson = {};
  }
  return responseJson;
};

adminAction.updateAirportTransferConfig = async function (context) {
  // console.log('this is parmas log', context.params)
  if (!context.params.id) {
    throw new MoleculerError("Invalid credentials");
  }
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "name": constantUtil.CONST_AIRPORTTRANSFERSETTING,
      },
      {
        "type": constantUtil.CONST_AIRPORTTRANSFERSETTING,
        "data.clientId": context.params.clientId,
        //
        "data.isEnableBookingPassengerConfig":
          context.params?.isEnableBookingPassengerConfig || false,
        "data.isEnableBookingLuggageConfig":
          context.params?.isEnableBookingLuggageConfig || false,
        "data.bookingPassengerConfig":
          context.params?.bookingPassengerConfig || [],
        "data.bookingLuggageConfig": context.params?.bookingLuggageConfig || [],
        "data.isEnableFlightNumber":
          context.params?.isEnableFlightNumber || false,
        "data.isEnableWelcomeSignboard":
          context.params?.isEnableWelcomeSignboard || false,
        "data.isEnableReturnTransfer":
          context.params?.isEnableReturnTransfer || false,
        "data.isEnableReturningPassengerConfig":
          context.params?.isEnableReturningPassengerConfig || false,
        "data.isEnableReturningLuggageConfig":
          context.params?.isEnableReturningLuggageConfig || false,
        "data.returningPassengerConfig":
          context.params?.returningPassengerConfig || [],
        "data.returningLuggageConfig": context.params?.returningLuggageConfig,
        "data.isProvideWaterBottles":
          context.params?.isProvideWaterBottles || false,
        "data.waterBottleConfig": context.params?.waterBottleConfig || [],
        "data.isEnableLanguagePreference":
          context.params?.isEnableLanguagePreference || false,
      },
      { "new": true }
    )
    .lean();
  // console.log('this is general settings responseData', responseJson)
  if (!responseJson) {
    throw new MoleculerError("Error in Config Update");
  }
  // -------------- Update Data in Redis Start --------------
  // Get Data
  const newResponseJson = await this.adapter.model
    .find({
      "name": constantUtil.CONST_AIRPORTTRANSFERSETTING,
      "type": constantUtil.CONST_AIRPORTTRANSFERSETTING,
    })
    .lean();
  await newResponseJson.map((item) => {
    item["id"] = item._id;
  });
  // Update Data in Redis
  await storageUtil.write(
    constantUtil.CONST_AIRPORTTRANSFERSETTING,
    newResponseJson
  );
  // -------------- Update Data in Redis End --------------
  return responseJson;
};

// TOLL MANAGEMENT
adminAction.getTollList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params?.clientId || context.meta?.clientId;

  const query = {
    "name": constantUtil.TOLL,
    "data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params?.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  if ((context.params?.city || "") !== "") {
    query["data.serviceArea"] = mongoose.Types.ObjectId(
      context.params.city.toString()
    );
  }
  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.tollName", "data.locationAddress"],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.tollName", "data.locationAddress"],
  });

  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "city": respo.data.serviceArea,
      "tollName": respo.data.tollName,
      "locationAddress": respo.data.locationAddress,
      "status": respo.data.status,
    },
  }));
  return responseJson;
};

adminAction.updateToll = async function (context) {
  let responseJson;
  const clientId = context.params?.data?.clientId || context.meta?.clientId;
  const docsJson = await this.adapter.model
    .findOne({
      "data.tollName": context.params.data.tollName,
      "name": constantUtil.TOLL,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Toll Already Exists"); //INFO_TOLL_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": context.params.data }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Toll Already Exists"); //INFO_TOLL_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.TOLL,
      "name": constantUtil.TOLL,
      "data": context.params.data,
    });
  }
  // Setting Update for tollTime
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.tollUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for tollTime
  return responseJson;
};
adminAction.getEditToll = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.findOne({
    "name": constantUtil.TOLL,
    "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    "data.clientId": mongoose.Types.ObjectId(clientId),
  });
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};

adminAction.changeTollStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.TOLL,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  }
  // Setting Update for tollTime
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.tollUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for tollTime
  return { "code": 1, "response": "Updated Successfully" };
};
adminAction.getAllTollData = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  // const query = {
  //   "name": constantUtil.TOLL,
  //   "data.status": constantUtil.ACTIVE,
  // };
  // const responseOffice = await this.adapter.model.find(query);

  // const responseJson = await responseOffice.map((respo) => ({
  //   "_id": respo._id,
  //   "city": respo.data.serviceArea,
  //   "tollName": respo.data.tollName,
  //   "locationAddress": respo.data.locationAddress,
  //   "locationCoordinates": respo.data.locationCoordinates,
  //   "tollCategorys": respo.data.tollCategorys,
  //   "status": respo.data.status,
  // }));
  const responseJson = await this.adapter.model.aggregate([
    {
      "$match": {
        // "type": constantUtil.TOLL,
        "name": constantUtil.TOLL,
        // "data.clientId": mongoose.Types.ObjectId(context.params.clientId),
        "data.status": constantUtil.ACTIVE,
      },
    },
    {
      "$project": {
        "_id": "$_id",
        "city": "$data.serviceArea",
        "tollName": "$data.tollName",
        "locationAddress": "$data.locationAddress",
        "locationCoordinates": "$data.locationCoordinates",
        "tollCategorys": { "$ifNull": ["$data.tollCategorys", []] },
        "status": "$data.status",
      },
    },
  ]);
  return {
    "code": 200,
    "data": {
      "response": responseJson,
      "timestamp": generalSettings.data.tollUpdateTime || Date.now(),
    },
    "message": "",
  };
};
// COUPON CODE MANAGEMENT
adminAction.getCouponList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const search = context.params?.search || "";
  const clientId = context.params?.clientId || context.meta?.clientId;

  const query = {
    "name": constantUtil.COUPON,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  if ((context.params?.city || "") !== "")
    query["data.serviceArea"] = mongoose.Types.ObjectId(
      context.params.city.toString()
    );

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.couponTitle", "data.couponCode"],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.couponTitle", "data.couponCode"],
  });

  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "city": respo.data.serviceArea,
      "couponTitle": respo.data.couponTitle,
      "couponCode": respo.data.couponCode,
      "couponType": respo.data.couponType,
      "consumersType": respo.data.consumersType,
      "isAutomatic": respo.data.isAutomatic,
      "couponAmount": respo.data.couponAmount,
      "couponMaxAmount": respo.data?.couponMaxAmount || 0,
      "couponGuideLines": respo.data?.couponGuideLines || [],
      "couponPaymentType": respo.data?.couponPaymentType || [],
      "totalUsageCount": respo.data.totalUsageCount,
      "usageCountPerPerson": respo.data.usageCountPerPerson,
      "appliedCount": respo.data.appliedCount,
      "vaildFrom": respo.data.vaildFrom,
      "vaildTo": respo.data.vaildTo,
      "status": respo.data.status,
      "isCouponCheckWithMaxTripAmount":
        respo.data?.isCouponCheckWithMaxTripAmount || false,
    },
  }));
  return responseJson;
};

adminAction.updateCoupon = async function (context) {
  let responseJson;
  const clientId =
    context.params?.data?.clientId ||
    context.params?.clientId ||
    context.meta?.clientId;
  if (
    context.params.data.couponType === constantUtil.FLAT &&
    context.params.data.couponAmount > context.params.data.couponMaxAmount
  ) {
    throw new MoleculerError(
      403,
      "coupon max amount must be equal or grater than coupon amount"
    );
  }
  const docsJson = await this.adapter.model
    .findOne({
      "data.couponCode": context.params.couponCode,
      "name": constantUtil.COUPON,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Coupon Code Already Exists"); //INFO_COUPON_CODE_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "$set": {
          "data.serviceArea": context.params.data.serviceArea,
          "data.serviceAreaId": context.params.data.serviceArea,
          "data.categoryIds": context.params.data.categoryIds,
          "data.categoryType": context.params.data.categoryType,
          "data.couponTitle": context.params.data.couponTitle,
          "data.couponDescription": context.params.data.couponDescription,
          "data.couponCode": context.params.data.couponCode,
          "data.couponType": context.params.data.couponType,
          "data.couponAmount": context.params.data.couponAmount,
          "data.couponPaymentType": context.params.data.couponPaymentType,
          "data.couponMaxAmount": context.params.data.couponMaxAmount,
          "data.couponGuideLines": context.params.data.couponGuideLines,
          "data.totalUsageCount": context.params.data.totalUsageCount,
          "data.usageCountPerPerson": context.params.data.usageCountPerPerson,
          "data.vaildFrom": context.params.data.vaildFrom,
          "data.vaildTo": context.params.data.vaildTo,
          "data.consumersType": context.params.data.consumersType,
          "data.isAutomatic": context.params.data.isAutomatic,
          "data.isProfessionalTolerance":
            context.params.data.isProfessionalTolerance,
          "data.isDisplayInApp": context.params.data.isDisplayInApp,
          "data.couponCodeMessage": context.params.data.couponCodeMessage,
          "data.isCouponCheckWithMaxTripAmount":
            context.params.data.isCouponCheckWithMaxTripAmount,
        },
      }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Coupon Code Already Exists"); //INFO_COUPON_CODE_EXISTS
    }
    context.params.data["appliedCount"] = 0;
    context.params.data["appliedDetails"] = [];
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.COUPON,
      "name": constantUtil.COUPON,
      "data": context.params.data,
    });
  }

  // Setting Update for Coupon Code
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.couponUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  }
  settingsUpdate = await this.adapter.model
    .findOne({
      "name": constantUtil.GENERALSETTING,
    })
    .lean();
  await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  // Setting Update for Coupon Code
  return responseJson;
};

adminAction.getEditCoupon = async function (context) {
  const responseJson = await this.adapter.model.findOne({
    "name": constantUtil.COUPON,
    "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    // "data.clientId": mongoose.Types.ObjectId(clientId),
  });
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};

adminAction.changeCouponStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.COUPON,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.getUserBasedCouponList = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const clientId = context.params?.clientId || context.meta?.clientId;
  let responseData,
    errorCode = 200,
    message = "",
    languageCode,
    generalSettings;

  try {
    if (userType === constantUtil.USER) {
      languageCode = context.meta?.userDetails?.languageCode;
    } else if (userType === constantUtil.PROFESSIONAL) {
      languageCode = context.meta?.professionalDetails?.languageCode;
    }
    const { SOMETHING_WENT_WRONG, SUCCESS } =
      await notifyMessage.setNotifyLanguage(languageCode);

    generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    if (!generalSettings) {
      throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
    }
    responseData = await this.broker.emit("admin.getUserBasedCouponList", {
      ...context.params,
      "userId":
        context.params.userId?.toString() || context.meta.userId?.toString(),
      "clientId": clientId,
    });
    responseData = responseData && responseData[0];
    message = SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
  }
  return {
    "code": errorCode,
    "message": message,
    "data": {
      "response": responseData,
      "timestamp": generalSettings.data.couponUpdateTime || Date.now(),
    },
  };
  // // find category because coupon works location based
  // let serviceCategory = await this.broker.emit(
  //   "category.getPopularServiceCategory",
  //   context.params
  // );
  // serviceCategory = serviceCategory && serviceCategory[0];

  // if (!serviceCategory)
  //   throw new MoleculerError("SORRY WE DONT PROVIDE SERVICE HERE");

  // // get booking count of user
  // let userBookingsCount = await this.broker.emit(
  //   "booking.checkBookingCountforCoupon",
  //   {
  //     "userId": context.meta.userId.toString(),
  //   }
  // );
  // userBookingsCount = userBookingsCount && userBookingsCount[0];

  // // find coupons list

  // // query for which kind of coupon is applicable for this user
  // const firstUserCouponsList = await this.adapter.model //Need to Change FIRSTUSER & EVERYONE in Sigle Group Query
  //   .find({
  //     "name": constantUtil.COUPON,
  //     "data.status": constantUtil.ACTIVE,
  //     "data.serviceArea": mongoose.Types.ObjectId(
  //       serviceCategory._id.toString()
  //     ),
  //     "data.consumersType": constantUtil.FIRSTUSER,
  //   })
  //   .sort({ "updatedAt": -1 })
  //   .limit(1);
  // const everyUserCouponsList = await this.adapter.model //Need to Change FIRSTUSER & EVERYONE in Sigle Group Query
  //   .find({
  //     "name": constantUtil.COUPON,
  //     "data.status": constantUtil.ACTIVE,
  //     "data.serviceArea": mongoose.Types.ObjectId(
  //       serviceCategory._id.toString()
  //     ),
  //     "data.consumersType": constantUtil.EVERYONE,
  //   })
  //   .sort({ "updatedAt": -1 })
  //   .limit(1);
  // const usageCountPerPerson =
  //   firstUserCouponsList[0]?.data?.usageCountPerPerson || 0;
  // const isAutomaticCoupon = firstUserCouponsList[0]?.data?.isAutomatic || false;
  // const usageCountPerPersonEvery =
  //   everyUserCouponsList[0]?.data?.usageCountPerPerson || 0;

  // let queryData = {},
  //   firstUserBookingsCount = 0,
  //   everyUserBookingsCount = 0;
  // //----------------------------------------
  // await userBookingsCount.find((e) => {
  //   if (e.consumersType === constantUtil.FIRSTUSER)
  //     firstUserBookingsCount = e.couponUsedCount;
  //   if (e.consumersType === constantUtil.EVERYONE)
  //     everyUserBookingsCount = e.couponUsedCount;
  //   // console.log("userBookingsCountTest_" + JSON.stringify(e))
  // });
  // //-----------------
  // // if (
  // //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  // //   isAutomaticCoupon === true
  // // ) {
  // //   queryData = {
  // //     "data.consumersType": {
  // //       "$in": [constantUtil.FIRSTUSER],
  // //     },
  // //   };
  // // } else if (
  // //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  // //   isAutomaticCoupon === false
  // // ) {
  // //   queryData = {
  // //     "data.consumersType": {
  // //       "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
  // //     },
  // //   };
  // // } else if (everyUserBookingsCount < parseInt(usageCountPerPersonEvery)) {
  // //   queryData = {
  // //     "data.consumersType": {
  // //       "$in": [constantUtil.EVERYONE],
  // //     },
  // //   };
  // // } else {
  // //   //For Not Apply Any Coupon
  // //   queryData = {
  // //     "data.consumersType": {
  // //       "$in": [constantUtil.NONE],
  // //     },
  // //   };
  // // }
  // if (
  //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  //   isAutomaticCoupon === true
  // ) {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.FIRSTUSER],
  //     },
  //   };
  // } else if (
  //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
  //   isAutomaticCoupon === false
  // ) {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
  //     },
  //   };
  // } else {
  //   queryData = {
  //     "data.consumersType": {
  //       "$in": [constantUtil.EVERYONE],
  //     },
  //   };
  // }
  // //----------------------------------------
  // // const queryData =
  // // userBookingsCount <= parseInt(usageCountPerPerson || 0)
  // //   ? {
  // //       "data.consumersType": {
  // //         "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
  // //       },
  // //     }
  // //   : {
  // //       "data.consumersType": {
  // //         "$in": [constantUtil.EVERYONE],
  // //       },
  // //     };

  // const couponsList = await this.adapter.model
  //   .find({
  //     "name": constantUtil.COUPON,
  //     "data.status": constantUtil.ACTIVE,
  //     "data.serviceArea": mongoose.Types.ObjectId(
  //       serviceCategory._id.toString()
  //     ),
  //     ...queryData,
  //   })
  //   .sort({ "data.consumersType": 1 });
  // // filter detailes
  // const responseData = [];

  // // below means if sigle coupon is exist
  // if (couponsList[0]) {
  //   for (let coupon of couponsList) {
  //     coupon = coupon.toJSON();

  //     //below condition applies when coupon has in between valid date  or not have valid date
  //     if (
  //       (!coupon.data?.vaildFrom && !coupon.data?.vaildTo) || // it means this coupon doest not validation date
  //       (coupon.data.vaildFrom &&
  //         coupon.data.vaildTo &&
  //         new Date(coupon.data.vaildFrom) <= new Date() &&
  //         new Date(coupon.data.vaildTo) >= new Date())
  //     ) {
  //       // below condition applies when coupon has apply count
  //       if (
  //         !coupon.data?.appliedCount ||
  //         (!coupon.data?.appliedCount && !coupon.data?.totalUsageCount) || //this means this coupon does not have limit
  //         (coupon.data.appliedCount &&
  //           coupon.data.totalUsageCount &&
  //           coupon.data.appliedCount < coupon.data.totalUsageCount)
  //       ) {
  //         const alredyUsed = coupon.data?.appliedDetails?.find(
  //           (e) => e.userId?.toString() === context.meta.userId?.toString()
  //         );
  //         // below condition applies when requested user does not used yet or person usage count does not exeed usage count of usageCountPerPerson
  //         if (
  //           !alredyUsed ||
  //           alredyUsed?.count < coupon.data.usageCountPerPerson
  //         ) {
  //           responseData.push({
  //             "_id": coupon._id,
  //             "city": coupon.data.serviceArea,
  //             "categoryIds": coupon.data.categoryIds,
  //             "categoryType": coupon.data.categoryType,
  //             "couponTitle": coupon.data.couponTitle,
  //             "couponDescription": coupon.data.couponDescription,
  //             "couponCode": coupon.data.couponCode,
  //             "couponType": coupon.data.couponType,
  //             "couponAmount": coupon.data.couponAmount,
  //             "couponMaxAmount": coupon.data?.couponMaxAmount || 0,
  //             "totalUsageCount": coupon.data.totalUsageCount,
  //             "usageCountPerPerson": coupon.data.usageCountPerPerson,
  //             "vaildFrom": coupon.data.vaildFrom,
  //             "vaildTo": coupon.data.vaildTo,
  //             "consumersType": coupon.data.consumersType,
  //             "isAutomatic": coupon.data.isAutomatic,
  //             "isProfessionalTolerance": coupon.data.isProfessionalTolerance,
  //             "isDisplayInApp": coupon.data.isDisplayInApp,
  //             "couponCodeMessage": coupon.data.couponCodeMessage,
  //             "currencyCode": serviceCategory.currencyCode,
  //             "currencySymbol": serviceCategory.currencySymbol,
  //             "couponGuideLines": coupon.data?.couponGuideLines || [],
  //             "couponPaymentType": coupon.data?.couponPaymentType || [],
  //             "isCouponCheckWithMaxTripAmount":
  //               coupon.data?.isCouponCheckWithMaxTripAmount || false,
  //           });
  //           // this coupon retuns successfully so next coupon
  //           continue;
  //         } // bleow else means this user exceed the usage count
  //         else {
  //           // so skip this and go next

  //           continue;
  //         }
  //       }
  //     }

  //     // this is else => above condition does not match ,have inactive this  coupon
  //     this.adapter.model.updateOne(
  //       { "_id": mongoose.Types.ObjectId(coupon._id.toString()) },
  //       {
  //         "data.status": constantUtil.INACTIVE,
  //         "data.appliedDetails": [],
  //         "data.appliedCount": 0,
  //       }
  //     );

  //     // Setting Update for Coupon Code
  //     let settingsUpdate = await this.adapter.model.updateOne(
  //       { "name": constantUtil.GENERALSETTING },
  //       {
  //         "data.couponUpdateTime": Date.now(),
  //       }
  //     );
  //     if (!settingsUpdate?.nModified)
  //       throw new MoleculerError("Error in Update");

  //     settingsUpdate = await this.adapter.model.findOne({
  //       "name": constantUtil.GENERALSETTING,
  //     });

  //     await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
  //     // Setting Update for Coupon Code

  //     continue;
  //   }
  // }
  // return {
  //   "code": 200,
  //   "data": {
  //     "response": responseData,
  //     "timestamp": generalSettings.data.tollUpdateTime || Date.now(),
  //   },
  //   "message": "",
  // };
};

adminAction.checkAndApplyManualCoupon = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const clientId = context.params?.clientId || context.meta?.clientId;
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;

  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    SOMETHING_WENT_WRONG,
    INFO_SUCCESS,
    SERVICE_CATEGORY_NOT_FOUND,
    INFO_INVALID_COUPON_CODE,
  } = await notifyMessage.setNotifyLanguage(languageCode);

  try {
    // find category because coupon works location based
    let serviceCategory = await this.broker.emit(
      "category.getPopularServiceCategory",
      { ...context.params, "clientId": clientId }
    );
    serviceCategory = serviceCategory && serviceCategory[0];
    if (!serviceCategory) {
      throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
    }
    // // get booking count of user
    // let userBookingsCount = await this.broker.emit(
    //   "booking.checkBookingCountforCoupon",
    //   {
    //     "userId": context.params.userId?.toString(),
    //   }
    // );
    // userBookingsCount = userBookingsCount && userBookingsCount[0];

    // // find coupons list
    // const firstUserCouponsList = await this.adapter.model.aggregate([
    //   {
    //     "$match": {
    //       "name": constantUtil.COUPON,
    //       "data.status": constantUtil.ACTIVE,
    //       "data.serviceArea": serviceCategory._id.toString(),
    //       "data.consumersType": constantUtil.FIRSTUSER,
    //     },
    //   },
    //   // {
    //   //   "$match": {
    //   //     "$or": [
    //   //       {
    //   //         "data.vaildTo": { "$gte": new Date() },
    //   //       },
    //   //       {
    //   //         "data.vaildTo": null,
    //   //       },
    //   //     ],
    //   //   },
    //   // },
    //   {
    //     "$project": {
    //       "_id": "$_id",
    //       "data": "$data",
    //     },
    //   },
    // ]);
    // // const everyUserCouponsList1 = await this.adapter.model //Need to Change FIRSTUSER & EVERYONE in Sigle Group Query
    // //   .find({
    // //     "name": constantUtil.COUPON,
    // //     "data.status": constantUtil.ACTIVE,
    // //     "data.serviceArea": mongoose.Types.ObjectId(
    // //       serviceCategory._id.toString()
    // //     ),
    // //     "data.consumersType": constantUtil.EVERYONE,
    // //   })
    // //   .lean()
    // //   .sort({ "updatedAt": -1 })
    // //   .limit(1);
    // const everyUserCouponsList = await this.adapter.model.aggregate([
    //   {
    //     "$match": {
    //       "name": constantUtil.COUPON,
    //       "data.status": constantUtil.ACTIVE,
    //       "data.serviceArea": serviceCategory._id.toString(),
    //       "data.consumersType": constantUtil.EVERYONE,
    //     },
    //   },
    //   // {
    //   //   "$match": {
    //   //     "$or": [
    //   //       {
    //   //         "data.vaildTo": { "$gte": new Date() },
    //   //       },
    //   //       {
    //   //         "data.vaildTo": null,
    //   //       },
    //   //     ],
    //   //   },
    //   // },
    //   {
    //     "$project": {
    //       "_id": "$_id",
    //       "data": "$data",
    //     },
    //   },
    // ]);
    // const usageCountPerPerson =
    //   firstUserCouponsList[0]?.data?.usageCountPerPerson || 0;
    // const isAutomaticCoupon =
    //   firstUserCouponsList[0]?.data?.isAutomatic || false;
    // const usageCountPerPersonEvery =
    //   everyUserCouponsList[0]?.data?.usageCountPerPerson || 0;

    // let queryData = {},
    //   firstUserBookingsCount = 0,
    //   everyUserBookingsCount = 0,
    //   promotionCouponBookingsCount = 0;
    // //----------------------------------------
    // await userBookingsCount.find((e) => {
    //   if (e.consumersType === constantUtil.FIRSTUSER)
    //     firstUserBookingsCount = e.couponUsedCount;
    //   if (e.consumersType === constantUtil.EVERYONE)
    //     everyUserBookingsCount = e.couponUsedCount;
    //   if (e.consumersType === constantUtil.PROMOTION)
    //     promotionCouponBookingsCount = e.couponUsedCount;
    //   // console.log("userBookingsCountTest_" + JSON.stringify(e))
    // });
    // // if (firstUserCouponsList.length > 0) {
    // if (
    //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
    //   isAutomaticCoupon === true
    // ) {
    //   queryData = {
    //     "data.consumersType": {
    //       "$in": [constantUtil.FIRSTUSER],
    //     },
    //   };
    // } else if (
    //   firstUserBookingsCount < parseInt(usageCountPerPerson) &&
    //   isAutomaticCoupon === false
    // ) {
    //   queryData = {
    //     "data.consumersType": {
    //       "$in": [constantUtil.FIRSTUSER, constantUtil.EVERYONE],
    //     },
    //   };
    // } else {
    //   queryData = {
    //     "data.consumersType": {
    //       "$in": [constantUtil.EVERYONE],
    //     },
    //   };
    // }
    const couponsList = await this.adapter.model
      .find({
        "name": constantUtil.COUPON,
        "data.status": constantUtil.ACTIVE,
        "data.serviceArea": mongoose.Types.ObjectId(
          serviceCategory._id.toString()
        ),
        "data.couponCode": context.params.couponCode,
        // ...queryData,
      })
      .collation({ "locale": "en", "strength": 2 })
      .sort({ "data.consumersType": 1 })
      .lean();
    if (couponsList.length === 0) {
      throw new MoleculerError(INFO_INVALID_COUPON_CODE, 400);
    }
    // below means if sigle coupon is exist
    // if (couponsList[0]) {
    if (couponsList.length > 0) {
      for (let coupon of couponsList) {
        // coupon = coupon.toJSON();
        //below condition applies when coupon has in between valid date  or not have valid date
        if (
          (!coupon.data?.vaildFrom && !coupon.data?.vaildTo) || // it means this coupon doest not validation date
          (coupon.data.vaildFrom &&
            coupon.data.vaildTo &&
            new Date(coupon.data.vaildFrom) <= new Date() &&
            new Date(coupon.data.vaildTo) >= new Date())
        ) {
          // below condition applies when coupon has apply count
          if (
            !coupon.data?.appliedCount ||
            (!coupon.data?.appliedCount && !coupon.data?.totalUsageCount) || //this means this coupon does not have limit
            (coupon.data.appliedCount &&
              coupon.data.totalUsageCount &&
              coupon.data.appliedCount < coupon.data.totalUsageCount)
          ) {
            const alredyUsed = coupon.data?.appliedDetails?.find(
              (e) => e.userId?.toString() === context.params?.userId?.toString()
            );
            // below condition applies when requested user does not used yet or person usage count does not exeed usage count of usageCountPerPerson
            if (
              !alredyUsed ||
              alredyUsed?.count < coupon.data.usageCountPerPerson
            ) {
              // responseData.push({
              responseData = {
                "_id": coupon._id,
                "city": coupon.data.serviceArea,
                "categoryIds": coupon.data.categoryIds,
                "categoryType": coupon.data.categoryType,
                "couponTitle": coupon.data.couponTitle,
                "couponDescription": coupon.data.couponDescription,
                "couponCode": coupon.data.couponCode,
                "couponType": coupon.data.couponType,
                "couponAmount": coupon.data.couponAmount,
                "couponMaxAmount": coupon.data?.couponMaxAmount || 0,
                "totalUsageCount": coupon.data.totalUsageCount,
                "usageCountPerPerson": coupon.data.usageCountPerPerson,
                "vaildFrom": coupon.data.vaildFrom,
                "vaildTo": coupon.data.vaildTo,
                "consumersType": coupon.data.consumersType,
                "isAutomatic": coupon.data.isAutomatic,
                "isProfessionalTolerance": coupon.data.isProfessionalTolerance,
                "isDisplayInApp": coupon.data.isDisplayInApp,
                "couponCodeMessage": coupon.data.couponCodeMessage,
                "currencyCode": serviceCategory.currencyCode,
                "currencySymbol": serviceCategory.currencySymbol,
                "couponGuideLines": coupon.data?.couponGuideLines || [],
                "couponPaymentType": coupon.data?.couponPaymentType || [],
                "isCouponCheckWithMaxTripAmount":
                  coupon.data?.isCouponCheckWithMaxTripAmount || false,
              };
            }
          }
          // continue; // so skip this and go next
        }
        //  else {
        //   // this is else => above condition does not match ,have inactive this  coupon
        //   this.adapter.model.updateOne(
        //     { "_id": mongoose.Types.ObjectId(coupon._id.toString()) },
        //     {
        //       "data.status": constantUtil.INACTIVE,
        //       "data.appliedDetails": [],
        //       "data.appliedCount": 0,
        //     }
        //   );
        //   await this.broker.emit(
        //     "admin.updateGeneralSettingTimestampInDBAndRedis",
        //     {
        //       "type": constantUtil.COUPON,
        //     }
        //   ); // Setting Update for Coupon Code
        //   // continue;
        // }
      }
    }
    message = INFO_SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    // "data": {
    //   "response": responseData,
    // },
    "data": responseData,
  };
};

// NOTIFICATIONS MANAGEMENT
adminAction.getNotificationTemplatesList = async function (context) {
  const skip = parseInt(context.params?.skip ?? 0);
  const limit = parseInt(context.params?.limit ?? 20);
  const clientId = context.params?.clientId || context.meta?.clientId;

  const query = [];
  switch (context.params.type) {
    case "EMAIL":
      query.push(
        {
          "$match": {
            "name": constantUtil.NOTIFICATIONTEMPLATES,
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
        },
        { "$unwind": `$data.emailNotifications` }
      );
      query.push(
        (context.params.filter || "") !== ""
          ? {
              "$match": {
                "data.emailNotifications.status": {
                  "$eq": context.params.filter,
                },
              },
            }
          : {
              "$match": {
                "data.emailNotifications.status": {
                  "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE],
                },
              },
            }
      );

      if ((context.params.search || "") !== "") {
        query.push({
          "$match": {
            "$or": [
              {
                "data.emailNotifications.title": {
                  "$regex": context.params.search + ".*",
                  "$options": "si",
                },
              },
            ],
          },
        });
      }

      query.push({
        "$facet": {
          "all": [{ "$count": "all" }],
          "response": [
            { "$sort": { "data.emailNotifications._id": -1 } },
            { "$skip": skip },
            { "$limit": limit },
            {
              "$project": {
                "notificationId": "$_id",
                "_id": "$data.emailNotifications._id",
                "title": "$data.emailNotifications.title",
                "content": "$data.emailNotifications.content",
                "image": "$data.emailNotifications.image",
                "subject": "$data.emailNotifications.subject",
                "type": "Email",
                "status": "$data.emailNotifications.status",
              },
            },
          ],
        },
      });
      break;
    case "SMS":
      query.push(
        {
          "$match": {
            "name": constantUtil.NOTIFICATIONTEMPLATES,
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
        },
        {
          "$unwind": `$data.smsNotifications`,
        }
      );
      query.push(
        (context.params.filter || "") !== ""
          ? {
              "$match": {
                "data.smsNotifications.status": {
                  "$eq": context.params.filter,
                },
              },
            }
          : {
              "$match": {
                "data.smsNotifications.status": {
                  "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE],
                },
              },
            }
      );

      if ((context.params?.search || "") !== "")
        query.push({
          "$match": {
            "$or": [
              {
                "data.smsNotifications.title": {
                  "$regex": context.params.search + ".*",
                  "$options": "si",
                },
              },
            ],
          },
        });

      query.push({
        "$facet": {
          "all": [{ "$count": "all" }],
          "response": [
            { "$sort": { "data.smsNotifications._id": -1 } },
            { "$skip": skip },
            { "$limit": limit },
            {
              "$project": {
                " notificationId": "$_id",
                "_id": "$data.smsNotifications._id",
                "title": "$data.smsNotifications.title",
                "content": "$data.smsNotifications.content",
                "subject": "$data.smsNotifications.subject",
                "type": "SMS",
                "status": "$data.smsNotifications.status",
              },
            },
          ],
        },
      });
      break;
    case "PUSH":
      query.push(
        {
          "$match": {
            "name": constantUtil.NOTIFICATIONTEMPLATES,
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
        },
        {
          "$unwind": `$data.pushNotifications`,
        }
      );
      query.push(
        (context.params?.filter || "") !== ""
          ? {
              "$match": {
                "data.pushNotifications.status": {
                  "$eq": context.params.filter,
                },
              },
            }
          : {
              "$match": {
                "data.pushNotifications.status": {
                  "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE],
                },
              },
            }
      );
      if ((context.params.search || "") !== "")
        query.push({
          "$match": {
            "$or": [
              {
                "data.pushNotifications.title": {
                  "$regex": context.params.search + ".*",
                  "$options": "si",
                },
              },
            ],
          },
        });
      query.push({
        "$facet": {
          "all": [{ "$count": "all" }],
          "response": [
            { "$sort": { "data.pushNotifications._id": -1 } },
            { "$skip": skip },
            { "$limit": limit },
            {
              "$project": {
                " notificationId": "$_id",
                "_id": "$data.pushNotifications._id",
                "title": "$data.pushNotifications.title",
                "content": "$data.pushNotifications.content",
                "image": "$data.pushNotifications.image",
                "subject": "$data.pushNotifications.subject",
                "type": "Push",
                "status": "$data.pushNotifications.status",
                "userType": "$data.pushNotifications.userType",
              },
            },
          ],
        },
      });
      break;
  }
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};

adminAction.updateNotificationTemplates = async function (context) {
  const type = context.params.type.toLowerCase();
  const dataKey = `${type}Notifications`;
  const clientId = context.params?.clientId || context.meta?.clientId;

  let templateData = await this.adapter.model
    .findOne({
      "name": constantUtil.NOTIFICATIONTEMPLATES,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!templateData) {
    templateData = await this.adapter.model.create({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.NOTIFICATIONTEMPLATES,
      "name": constantUtil.NOTIFICATIONTEMPLATES,
      "data": {
        "data.clientId": clientId,
        "emailTemplateEditableFields": [],
        "emailNotifications": [],
        "smsNotifications": [],
        "pushNotifications": [],
      },
    });
  }
  const mongoId = context.params.id
    ? context.params.id
    : mongoose.Types.ObjectId();

  let stops =
    templateData && templateData["data"] && templateData["data"][`${dataKey}`]
      ? templateData["data"][`${dataKey}`]
      : [];

  let isStopAvail;
  if (
    templateData?.data[dataKey] &&
    Array.isArray(templateData.data[dataKey]) &&
    templateData.data[dataKey]?.length
  )
    if (!context.params.id) {
      templateData.data[dataKey].map((stop) => {
        if (stop.title.toLowerCase() === context.params.title.toLowerCase())
          isStopAvail = stop;
      });
    } else {
      templateData.data[dataKey].map((stop) => {
        if (stop.title.toLowerCase() === context.params.title.toLowerCase())
          isStopAvail = stop;
      });
    }
  if (!context.params.id) {
    if (isStopAvail) {
      throw new MoleculerError("Template is Already Exists");
    } else {
      stops.push({
        "_id": mongoId,
        "title": context.params.title,
        "content": context.params.content,
        "image": context.params.image,
        "subject": context.params.subject,
        "status": context.params.status,
        "date": context.params.date,
        "userType": context.params?.userType || null,
        "contentEditableFields": context.params.contentEditableFields || [],
      });
    }
  } else {
    if (isStopAvail?._id?.toString() === context.params.id.toString()) {
      stops = templateData.data[dataKey].map((stop) => {
        if (stop._id.toString() === mongoId.toString()) {
          stop.title = context.params.title;
          stop.content = context.params.content;
          stop.image = context.params.image;
          stop.subject = context.params.subject;
          stop.status = context.params.status;
          stop.date = context.params.date;
          stop.userType = context.params?.userType || null;
          stop.contentEditableFields =
            context.params.contentEditableFields ||
            stop?.contentEditableFields ||
            [];
        }
        return stop;
      });
    } else {
      throw new MoleculerError("Template does not Exists");
    }
  }

  const queryData =
    context.params.type === constantUtil.EMAIL
      ? {
          "data.emailNotifications": stops,
          //"data.clientId": clientId,
        }
      : context.params.type === constantUtil.SMS
      ? {
          "data.smsNotifications": stops,
          //"data.clientId": clientId,
        }
      : {
          "data.pushNotifications": stops,
          //"data.clientId": clientId,
        };
  let responseJson;
  if (templateData) {
    responseJson = await this.adapter.model.updateOne(
      {
        "name": constantUtil.NOTIFICATIONTEMPLATES,
        // "data.clientId": mongoose.Types.ObjectId(clientId),
      },
      queryData
    );
    if (!responseJson.nModified) {
      throw new MoleculerError("Error in status change");
    }
  } else {
    responseJson = await this.adapter.model.create({
      "dataType": constantUtil.CLIENTDATA,
      "name": constantUtil.NOTIFICATIONTEMPLATES,
      ...queryData,
    });
  }

  return { "code": 1, "response": {}, "message": "Updated Successfully" };
};
// adminAction.updateNotificationTemplates= async function(context){
// const notificationDoc=await this.adapter.model.findOne({
//   'name':constantUtil.NOTIFICATIONTEMPLATES
// })
// if(notificationDoc){
// }
// if(!notificationDoc){
//   const responseJson=await this.adapter.model.create({
//     'name':constantUtil.NOTIFICATIONTEMPLATES,
//     'data':{
//       'emailNotifications':context.params.type===constantUtil.EMAIL?[{'content':context.params,}]
//     }
//   })
// }

// // else case if notificationDoc not existist

// }

adminAction.getNotificationTemplates = async function (context) {
  const type = context.params.type.toLowerCase();
  const dataKey = `${type}Notifications`;
  const responseJson = await this.adapter.model
    .findOne({
      "name": constantUtil.NOTIFICATIONTEMPLATES,
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    const finalResponse = responseJson.data[`${dataKey}`].filter(
      (each) => each._id.toString() === context.params.id.toString()
    );
    finalResponse[0]["type"] = context.params.type;
    return finalResponse[0];
  }
};

adminAction.changeNotificationTemplateStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const queryData =
    context.params.type === constantUtil.EMAIL
      ? {
          "$set": {
            "data.emailNotifications.$[list].status": context.params.status,
          },
        }
      : context.params.type === constantUtil.SMS
      ? {
          "$set": {
            "data.smsNotifications.$[list].status": context.params.status,
          },
        }
      : {
          "$set": {
            "data.pushNotifications.$[list].status": context.params.status,
          },
        };
  const responseJson = await this.adapter.model.updateMany(
    { "name": constantUtil.NOTIFICATIONTEMPLATES },
    queryData,
    { "arrayFilters": [{ "list._id": { "$in": ids } }] }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.deleteNotificationTemplateById = async function (context) {
  try {
    const queryData =
      context.params.type === constantUtil.EMAIL
        ? {
            "$pull": {
              "data.emailNotifications._id": context.params.id,
            },
          }
        : context.params.type === constantUtil.SMS
        ? {
            "$pull": {
              "data.smsNotifications._id": mongoose.Types.ObjectId(
                context.params.id
              ),
            },
          }
        : {
            "$pull": {
              "data.pushNotifications._id": mongoose.Types.ObjectId(
                context.params.id
              ),
            },
          };
    // const responseJson = await this.adapter.model.updateOne(
    //   { "name": constantUtil.NOTIFICATIONTEMPLATES },
    //   { ...queryData },
    // );
    // if (!responseJson.nModified) {
    //   throw new MoleculerError("Error in Delete Record");
    // } else {
    return { "code": 1, "response": "Updated Successfully" };
    // }
  } catch (e) {
    const xxx = e.message;
  }
};

adminAction.getUserListForNotification = async function (context) {
  let response = null;
  const clientId = context.params?.clientId || context.meta?.clientId;
  // switch (context.params.category?.toUpperCase()) {
  //   case constantUtil.ALPHABETICALORDER:
  //   case constantUtil.CITY:
  if (context.params.userType === constantUtil.PROFESSIONAL.toLowerCase()) {
    response = await this.broker.emit(
      "professional.getProfessionalListForNotification",
      { ...context.params, "clientId": clientId }
    );
  } else if (context.params.userType === constantUtil.USER.toLowerCase()) {
    response = await this.broker.emit("user.getUserListForNotification", {
      ...context.params,
      "clientId": clientId,
    });
  }
  response = response && response[0];
  // break;
  // }
  return response;
};
adminAction.sendPromotion = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (context.params.userType === constantUtil.USER) {
    await this.broker.emit("user.sendPromotion", {
      ...context.params,
      "clientId": clientId,
    });
  } else if (context.params.userType === constantUtil.PROFESSIONAL) {
    await this.broker.emit("professional.sendPromotion", {
      ...context.params,
      "clientId": clientId,
    });
  }

  // context.params.ids.forEach(async (id) => {
  //   let reciverData;
  //   if (context.params.userType === constantUtil.USER) {
  //     reciverData = await this.broker.emit("user.getById", {
  //       "id": id.toString(),
  //     });
  //   }
  //   if (context.params.userType === constantUtil.PROFESSIONAL) {
  //     reciverData = await this.broker.emit("professional.getById", {
  //       "id": id.toString(),
  //     });
  //   }

  //   reciverData = reciverData && reciverData[0];
  //   if (!reciverData || !reciverData.deviceInfo || !reciverData.deviceInfo[0])
  //     return;

  //   if (context.params.type === constantUtil.PUSH) {
  //     let notificationObject;
  //     try {
  //       notificationObject = {
  //         "clientId": context.params.clientId,
  //         "data": {
  //           "type": constantUtil.NOTIFICATIONTYPE,
  //           "userType": context.params.userType.toUpperCase(),
  //           "action": constantUtil.ACTION_PROMOTION,
  //           "image": context.params.template?.image || "",
  //           "timestamp": Date.now(),
  //           "message": context.params.template.content,
  //           "details": lzStringEncode(context.params.template),
  //         },
  //         "registrationTokens": [
  //           {
  //             "token": reciverData?.deviceInfo[0]?.deviceId || "",
  //             "id": reciverData?._id?.toString(),
  //             "deviceType": reciverData?.deviceInfo[0]?.deviceType || "",
  //             "platform": reciverData?.deviceInfo[0]?.platform || "",
  //             "socketId": reciverData?.deviceInfo[0]?.socketId || "",
  //           },
  //         ],
  //       };
  //     } catch (error) {
  //       console.log("error in notification object", error);
  //     }

  //     if (notificationObject.registrationTokens[0].token === "") {
  //       return; // =>just retun bescuse without token we cant send notification
  //     }

  //     // send notification
  //     this.broker.emit("admin.sendFCM", notificationObject);
  //     // this.broker.emit('socket.sendNotification', notificationObject)
  //   }
  //   if (context.params.type === constantUtil.EMAIL) {
  //     if (reciverData.email) {
  //       this.broker.emit("admin.sendPromotionMail", {
  //         "templateName": context.params.template.title,
  //         ...(await mappingUtil.promtionMailObject(reciverData)),
  //       });
  //     }
  //   }
  //   return;
  // });

  return { "code": 200, "data": [], "message": "requested successfully" };
};
// POPULAR DESTINATION MANAGEMENT
adminAction.getPopularDestinationList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const clientId = context.params?.clientId || context.meta?.clientId;

  const search = context.params.search || "";

  const query = {
    "name": constantUtil.POPULARDESTINATION,
  };
  query["data.status"] =
    (context.params.filter || "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  // if (context.params.city && context.params.city != '')
  //   query['data.serviceArea'] = mongoose.Types.ObjectId(
  //     context.params.city.toString()
  //   )

  const responseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.placeName",
      "data.placeDescription",
      "data.locationAddress",
    ],
  });
  const responseOffice = await this.adapter.find({
    "limit": limit,
    "offset": skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.placeName",
      "data.placeDescription",
      "data.locationAddress",
    ],
  });

  responseJson["count"] = count;
  responseJson["response"] = responseOffice.map((respo) => ({
    "_id": respo._id,
    "data": {
      "placeName": respo.data.placeName,
      "placeDescription": respo.data.placeDescription,
      "placeImage": respo.data.placeImage,
      "locationAddress": respo.data.locationAddress,
      "status": respo.data.status,
    },
  }));
  return responseJson;
};

adminAction.updatePopularDestination = async function (context) {
  let responseJson;
  const clientId =
    context.params?.data?.clientId ||
    context.params?.clientId ||
    context.meta?.clientId;
  const docsJson = await this.adapter.model
    .findOne({
      "data.placeName": context.params.data.placeName,
      "name": constantUtil.POPULARDESTINATION,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Popular Destination Already Exists"); //INFO_POPULAR_DESTINATION_EXISTS
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      { "data": context.params.data }
    );
  } else {
    if (docsJson) {
      throw new MoleculerError("Popular Destination Already Exists"); //INFO_POPULAR_DESTINATION_EXISTS
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.POPULARDESTINATION,
      "name": constantUtil.POPULARDESTINATION,
      "data": context.params.data,
    });
  }
  // Setting Update for destination Time
  let settingsUpdate = await this.adapter.model.updateOne(
    {
      "name": constantUtil.GENERALSETTING,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.popularDestinationUpdateTime": Date.now() }
  );
  if (!settingsUpdate?.nModified) {
    throw new MoleculerError("Error in Update");
  } else {
    settingsUpdate = await this.adapter.model
      .findOne({
        "name": constantUtil.GENERALSETTING,
      })
      .lean();
    await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    // Setting Update for destination Time
    return responseJson;
  }
};

adminAction.getEditPopularDestination = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.findOne({
    "name": constantUtil.POPULARDESTINATION,
    "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    "data.clientId": mongoose.Types.ObjectId(clientId),
  });
  if (!responseJson) {
    throw new MoleculerError("Invalid details");
  } else {
    return responseJson;
  }
};

adminAction.changePopularDestinationStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.POPULARDESTINATION,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    },
    { "data.status": context.params.status }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// DOCUMENT EXPIRE VERIFY
adminAction.expireDocumentsProfessionlUnverified = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  this.broker.emit("professional.expireDocumentsProfessionlUnverified", {
    ...context.params,
    "clientId": clientId,
  });
};

// DB ACCESS
adminAction.remove0 = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let userData = await this.broker.emit("user.removePrefix0", {
    ...context.params,
    "clientId": clientId,
  });
  userData = userData && userData[0];
  let professionalData = await this.broker.emit("professional.removePrefix0", {
    ...context.params,
    "clientId": clientId,
  });
  professionalData = professionalData && professionalData[0];
  let adminData = await this.broker.emit("admin.removePrefix0", {
    ...context.params,
    "clientId": clientId,
  });
  adminData = adminData && adminData[0];
  return {
    "code": 200,
    "data": {
      "userData": userData,
      "professionalData": professionalData,
      "adminData": adminData,
    },
    "message": "REQUESTED SUCCESSFULLY",
  };
};
adminAction.forceLogout = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let userData = await this.broker.emit("user.forceLogout", {
    ...context.params,
    "clientId": clientId,
  });
  userData = userData && userData[0];
  let professionalData = await this.broker.emit("professional.forceLogout", {
    ...context.params,
    "clientId": clientId,
  });
  professionalData = professionalData && professionalData[0];
  return {
    "code": 200,
    "userData": userData,
    "professionalData": professionalData,
    "message": "REQUESTED SUCCESSFULLY",
  };
};
// promotion notifications
adminAction.getUpdateEmailEditableFields = async function (context) {
  let responseJson;
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (context.params.id === undefined) {
    responseJson = await this.adapter.model.findOne(
      {
        "name": constantUtil.NOTIFICATIONTEMPLATES,
        //"data.clientId": mongoose.Types.ObjectId(clientId),
      },
      { "data.emailTemplateEditableFields": 1 }
    );
    console.log("this is responseJson", responseJson);

    // means document not exists
    if (!responseJson) {
      responseJson = await this.adapter.model.create(
        {
          "dataType": constantUtil.CLIENTDATA,
          "name": constantUtil.NOTIFICATIONTEMPLATES,
          "data": {
            "clientId": clientId,
            "emailTemplateEditableFields": [],
            "emailNotifications": [],
            "smsNotifications": [],
            "pushNotifications": [],
          },
        },
        { "data.emailTemplateEditableFields": 1 }
      );
      if (!responseJson) {
        throw new MoleculerError(
          "SOMETHING WENT WRONG IN EMAIL NOTIFICATIONS CONTENT EDITABLE FIELDS OPRATIONS"
        );
      }
    }
    // need

    //
    if (!!responseJson?.data?.emailTemplateEditableFields === false) {
      console.log("this is responseJson");
      responseJson = await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(responseJson._id.toString()),
        },
        {
          "data.emailTemplateEditableFields": [],
        },
        {
          "new": true,
          "fields": { "data.emailTemplateEditableFields": 1 },
        }
      );
    }
    // all conditions satisfies retun
    return {
      "code": 200,
      "data": responseJson,
      "message": "",
    };
  }
  if (context.params.id) {
    if (!context.params.emailTemplateEditableFields) {
      throw new MoleculerError(422, "emailTemplateEditableFields is required");
    }
    responseJson = await this.adapter.model.findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      },
      {
        "data.emailTemplateEditableFields":
          context.params.emailTemplateEditableFields,
      },
      {
        "new": true,
        "fields": { "data.emailTemplateEditableFields": 1 },
      }
    );
    if (!responseJson) {
      throw new MoleculerError(
        "SOMETHING WENT WRONG IN UPDATING NOTIFICATIONTEMPLATE DOCUMENT"
      );
    } else {
      return {
        "code": 200,
        "message": "",
        "data": responseJson,
      };
    }
  }
};
// developers testing purpose
adminAction.fcmTesting = async function () {
  // let notificationObject
  // try {
  //   notificationObject = {
  //     'data': {
  //       'type': constantUtil.NOTIFICATIONTYPE,
  //       'userType': 'PROFESSIONAL',
  //       'action': constantUtil.ACTION_PROMOTION,
  //       'timestamp': Date.now(),
  //       'message': 'testing from local',
  //       'details': 'hello friends',
  //     },
  //     'registrationTokens': [
  //       {
  //         'token':
  //           'elartq6pQiaseFxSYsCLxW:APA91bG_4Di0oe1NRYBWDF7nK1tJEFMqxayDyEy7N7u20-x2PR9VMdKJ4qgotjh2EtgdjxFzTl0hy_SGf_giQsRG_KpjlwzqYtjDiSXMkw28NHpssigisG1SQZlFW3j6JWhSttZXZFyt',
  //         'id': '60cd98d0a11610f2cf4f7758',
  //         'deviceType': 'Android/undefined',
  //         'platform': '',
  //         'socketId': '8BqxAhEyb9gjcs_RAAAH',
  //       },
  //     ],
  //   }
  // } catch (error) {
  //   console.log('error in notification object', error)
  // }

  // // send notification
  // this.broker.emit('admin.sendFCM', notificationObject)
  const buffer = async () => {
    console.log("this is finding");
    console.log(await this.adapter.model.distinct("name"));
  };
  setTimeout(buffer, 5 * 60 * 1000);
  return "super";
};

adminAction.sendMessageToFCM = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  /* FCM */
  let responseData = await this.broker.emit("admin.sendFCM", {
    ...context.params.notificationObject,
    "accessToken": context.params.accessToken,
    "clientId": clientId,
  });
  responseData = responseData && responseData[0];
  return responseData;
};

adminAction.getFCMAccessTokenUsingSecretKey = async function (context) {
  console.log("-----------------------------");
  console.log(JSON.stringify(context.params));
  console.log("-----------------------------");
  const { google } = require("googleapis");
  let fcmSettings, gerenalsetting;
  gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  fcmSettings = await storageUtil.read(constantUtil.FIREBASESETTING);
  const getAccessToken = () => {
    return new Promise(function (resolve, reject) {
      // const key = require("../placeholders/service-account.json");
      const jwtClient = new google.auth.JWT(
        fcmSettings.data.firebaseNotifyConfig.client_email,
        null,
        fcmSettings.data.firebaseNotifyConfig.private_key,
        ["https://www.googleapis.com/auth/firebase.messaging"], // SCOPES
        null
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
  };

  let fcmAccessTokenDetails = [];
  // await gerenalsetting.map(async () => {
  const accessToken = await getAccessToken();
  const projectId = fcmSettings.data.firebaseNotifyConfig.project_id;
  fcmAccessTokenDetails.push({
    "clientId": context.params.clientId,
    "accessToken": accessToken,
    "projectId": projectId,
  });
  // });
  // await storageUtil.write(constantUtil.FCMACCESSTOKEN, fcmAccessTokenDetails);
  return {
    "access_token": accessToken,
    "expires_in": 3000,
    "token_type": "Bearer",
  };
};

// WALLET

// refund
adminAction.getRefundDetails = async function (context) {
  let response;
  const clientId = context.params?.clientId || context.meta?.clientId;
  // if (context.params.userType === constantUtil.USER) {
  //   response = await this.broker.emit('user.getRefundDetails', context.params)
  // }
  // if (context.params.userType === constantUtil.PROFESSIONAL) {
  //   response = await this.broker.emit(
  //     'professional.getRefundDetails',
  //     context.params
  //   )
  // }
  // if (
  //   !response &&
  //   context.params.userType !== constantUtil.USER &&
  //   context.params.userType !== constantUtil.PROFESSIONAL
  // ) {
  //   return {
  //     'code': 400,
  //     'message': `INVALID USERTYPE -${context.params.userType}`,
  //     'data': {},
  //   }
  // }
  // return response
  let data = await this.broker.emit("transaction.getRefundDetails", {
    ...context.params,
    "clientId": clientId,
  });
  data = data && data[0];
  if (!data) {
    return {
      "code": 400,
      "message": "SOMETHING WENT WRONG",
      "data": context.params,
    };
  } else {
    return data;
  }
};

adminAction.refundAction = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  // const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY)
  // let paymentData
  // for (const prop in paymentOptionList) {
  //   if (
  //     paymentOptionList[prop].status === constantUtil.ACTIVE &&
  //     paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
  //   ) {
  //     paymentData = paymentOptionList[prop]
  //     break
  //   }
  // }
  // if (!paymentData) return 'ERROR IN PAYMENT OPTION LIST'
  // if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
  console.log("ehllo");
  let data = await this.broker.emit(
    "transaction.flutterwave.adminRefundAction",
    { ...context.params, "clientId": clientId }
  );
  data = data && data[0];
  if (!data) {
    return {
      "code": 400,
      "message": "SOMETHING WENT WRONG",
      "data": context.params,
    };
  } else {
    return data;
  }
};
adminAction.getWithDrawDetails = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let data = await this.broker.emit("transaction.adminGetWithDrawDetails", {
    ...context.params,
    "clientId": clientId,
  });
  data = data && data[0];
  if (!data) {
    return {
      "code": 400,
      "message": "SOMETHING WENT WRONG",
      "data": context.params,
    };
  } else {
    return data;
  }
};

adminAction.walletWithDrawManualTransfer = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) {
    throw new MoleculerError(
      "CANT FIND PAYMENT OPTIONS LIST FROM DB REDIS PROBLEM"
    );
  }
  let paymentData = {};
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      // SOME TIME NOT FOUND SO WE NEED TO ADD
    }
  }
  let response;
  if (paymentData.gateWay === constantUtil.FLUTTERWAVE) {
    if (paymentData.version === constantUtil.CONST_V3) {
      response = await this.broker.emit(
        "transaction.flutterwave.adminTransfer",
        { ...context.params, "clientId": clientId }
      );
    } else {
      response = await this.broker.emit(
        "transaction.flutterwave.adminTransfer",
        { ...context.params, "clientId": clientId }
      );
    }
  }
  response = response && response[0];
  if (!response) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG IN TRANSACTION EVENT",
      500,
      "TRANSACTION EVENT NEVER RETUR ANYTHING",
      context.params
    );
  }
  if (response?.code > 200) {
    throw new MoleculerClientError(
      response.message,
      400,
      "SOMETHING WENT WRONG",
      response
    );
  }
  return response;
};

adminAction.adminGetTransaction = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  console.log("hello");
  let transactionData = await this.broker.emit(
    "transaction.getTransactionById",
    { "transactionId": context.params.transactionId, "clientId": clientId }
  );
  transactionData = transactionData && transactionData[0];
  if (!transactionData) {
    throw new MoleculerClientError(
      "WE CANT FIND THE TRANSACTION ID",
      400,
      "SOMTHIG WENT wRONG",
      context.params
    );
  }

  let userData;
  if (transactionData?.from?.userId) {
    if (transactionData?.from?.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": transactionData?.from?.userId?.toString(),
        "clientId": clientId,
      });
    }
    if (transactionData?.from?.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": transactionData?.from?.userId?.toString(),
        "clientId": clientId,
      });
    }
    userData = userData && userData[0];
    if (userData) {
      transactionData.from.userId = userData;
    }
  }
  if (transactionData?.to?.userId) {
    if (transactionData?.to?.userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": transactionData?.to?.userId?.toString(),
        "clientId": clientId,
      });
    }
    if (transactionData?.to?.userType === constantUtil.PROFESSIONAL) {
      userData = await this.broker.emit("professional.getById", {
        "id": transactionData?.to?.userId?.toString(),
        "clientId": clientId,
      });
    }
    userData = userData && userData[0];
    if (userData) {
      transactionData.to.userId = userData;
    }
  }
  return {
    "code": 200,
    "data": transactionData,
    "message": "TRANSACTION DETAILES FETCHED",
  };
};

adminAction.changeTransactionStatus = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  console.log("i am vishnu");
  let changedData = await this.broker.emit(
    "transaction.changeTransactionStatus",
    { ...context.params, "clientId": clientId }
  );
  changedData = changedData && changedData;
  if (changedData.code > 200) {
    throw new MoleculerClientError(
      changedData.message,
      400,
      "SOMTHING WENT WRONG  ",
      context.params
    );
  } else {
    return changedData;
  }
};

// Bookingservice Management

adminAction.createBookingServiceManagement = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  // find requested account is existed
  const exist = await this.adapter.model.findOne({
    //"data.clientId": mongoose.Types.ObjectId(clientId),
    "name": constantUtil.BOOKINGSERVICEMANAGEMET,
    "$or": [
      { "data.phone.number": context.params.phone.number },
      { "data.email": context.params.email },
    ],
  });
  if (exist) {
    const errors = [];
    if (context.params.phone.number === exist.data?.phone?.number) {
      errors.push({
        "message": "phone number already regitered",
        "data": context.params.phone,
      });
    }
    if (context.params.email === exist.data?.email) {
      errors.push({
        "message": "eamil already regitered",
        "data": context.params.eamil,
      });
    }
    throw new MoleculerClientError(
      "ALREADY FOUND",
      204,
      "NON RETRYABLE",
      errors
    );
  }
  // create doucment

  const newManagement = await this.adapter.model.create({
    "dataType": constantUtil.CLIENTDATA,
    "type": constantUtil.BOOKINGSERVICEMANAGEMET,
    "name": constantUtil.BOOKINGSERVICEMANAGEMET,
    "data": {
      "clientId": clientId,
      "name": context.params.name,
      "email": context.params.email,
      "phone": {
        "code": context.params.phone.code,
        "number": context.params.phone.number,
      },
      "contactPersonName": context.params.contactPersonName,
      "financePersonName": context.params.financePersonName,
      "password": bcrypt.hashSync(
        context.params.password,
        bcrypt.genSaltSync(8),
        null
      ),
    },
  });
  if (!newManagement) {
    throw new MoleculerError(
      "SOMETHING WENT WORONG IN ADDING NEW BOOKING SERVICE MANAGEMENT",
      500,
      "SERVER ERROR",
      context.params
    );
  }
  const authToken = jwtSign({
    "bookingServiceMangementId": newManagement._id,
  });
  const updateManagement = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(newManagement._id.toString()) },
    { "accessToken": authToken }
  );
  if (!updateManagement.nModified) {
    throw new MoleculerError(
      "SOMETHING WENT WORONG IN UPDATE TOKEN NEW BOOKINGSERVICEMANAGEMENT",
      500,
      "SERVER ERROR",
      context.params
    );
  } else {
    return {
      "code": 200,
      "message": "Booking service created successfully",
      "data": {
        "authenticationKey": authToken,
        "name": context.params.name,
        "email": context.params.email,
        "phone": context.params.phone,
      },
    };
  }
};

adminAction.editBookingServiceManagement = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const exist = await this.adapter.model.findOne({
    //"data.clientId": mongoose.Types.ObjectId(clientId),
    "name": constantUtil.BOOKINGSERVICEMANAGEMET,
    "$or": [
      { "data.phone.number": context.params.phone.number },
      { "data.email": context.params.email },
    ],
    "_id": { "$ne": mongoose.Types.ObjectId(context.params.id) },
  });
  if (exist) {
    const errors = [];
    if (context.params.phone.number === exist.data?.phone?.number) {
      errors.push({
        "message": "phone number already regitered",
        "data": context.params.phone,
      });
    }
    if (context.params.email === exist.data?.email) {
      errors.push({
        "message": "eamil already regitered",
        "data": context.params.eamil,
      });
    }
    throw new MoleculerRetryableError(
      "ALREADY FOUND",
      204,
      "NON RETRYABLE",
      errors
    );
  }
  const query = {
    "data.clientId": clientId,
    "data.name": context.params.name,
    "data.email": context.params.email,
    "data.phone": {
      "code": context.params.phone.code,
      "number": context.params.phone.number,
    },
    "data.contactPersonName": context.params.contactPersonName,
    "data.financePersonName": context.params.financePersonName,
  };
  if (!!context.params.password === true) {
    query["data.password"] = bcrypt.hashSync(
      context.params.password,
      bcrypt.genSaltSync(8),
      null
    );
  }
  const newManagement = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.params.id) },
    query
  );
  if (!newManagement.nModified) {
    throw new MoleculerError(
      "SOMTHING WENT WRONG IN UPDATE SERVICE MANAGEMENT"
    );
  } else {
    return {
      "code": 200,
      "message": "UPDATED SUCCESSFULLY",
      "data": {},
    };
  }
};

adminAction.bookingServiceManagementsList = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const search = context?.params?.search ?? "";
  const query = {
    "name": constantUtil.BOOKINGSERVICEMANAGEMET,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    query["createdAt"] = {
      "$gte": new Date(new Date(context.params.fromDate).setHours(0, 0, 0, 0)),
      "$lt": new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      ),
    };
  }
  //
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.name",
      "data.financePersonName",
      "data.contactPersonName",
      "data.email",
      "data.phone.number",
    ],
  });
  const response = await this.adapter.find({
    "limit": parseInt(context.params.limit ?? 20),
    "offset": parseInt(context.params?.skip ?? 0),
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": [
      "data.name",
      "data.financePersonName",
      "data.contactPersonName",
      "data.email",
      "data.phone.number",
    ],
  });
  return {
    "count": count,
    "response": response,
  };
};

adminAction.getBookingServiceMangement = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.findOne(
    { "_id": mongoose.Types.ObjectId(context.params.id) },
    {
      "data.password": 0,
      "data.accessToken": 0,
    }
  );
  if (!responseJson) {
    throw new MoleculerError("Management not found");
  } else {
    return responseJson;
  }
};

adminAction.changeBookingServiceManagementStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId || context.meta?.clientId;
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.BOOKINGSERVICEMANAGEMET,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    },
    {
      "data.status": context.params.status,
      "data.accessToken": "",
    }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

// booking
adminAction.getServiceCategoryForThirdPartyBooking = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let response = await this.broker.emit(
    "category.getCategoriesForThirdPartyBookingService",
    { ...context.params, "clientId": clientId }
  );
  response = response && response[0];
  if (!response) {
    throw new MoleculerError("SOMETHING WENT WRONG", 500, "NO RETRYABLE", {});
  }
  if (response.code > 200) {
    throw new MoleculerError(
      response.data[0],
      400,
      response.data[2],
      response.data[3]
    );
  } else {
    return response;
  }
};

adminAction.newRideBookingForThirdParty = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  let response = await this.broker.emit("booking.newRideBookingForThirdParty", {
    ...context.params,
    "clientId": clientId,
  });
  response = response && response[0];
  if (!response) {
    throw new MoleculerError("SOMETHING WENT WRONG", 500, "NO RETRYABLE", {});
  }
  if (response.code > 200) {
    throw new MoleculerClientError(
      response.data[0],
      400,
      response.data[2],
      response.data[3]
    );
    // throw new MoleculerError('hello', 400, null, {})
  } else {
    return response;
  }
};

// CLIENT ACTION RELATED
// /#region Clients Data
adminAction.createClient = async function (context) {
  let errorCode = 200,
    message = "";
  try {
    // trim the key value is essential
    context.params.key = context.params.key.replace(/ /g, "");
    const exists = await this.adapter.model.countDocuments({
      "name": constantUtil.BRSCLIENTS,
      "data.key": context.params.key,
    });
    if (exists) {
      throw new MoleculerClientError("CLIENT KEY ALREADY EXISTS");
    }
    const response = await this.adapter.model.create({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.BRSCLIENTS,
      "name": constantUtil.BRSCLIENTS,
      "isActive": true,
      "data": {
        "key": context.params.key,
        "secretKey": context.params.secretKey,
        "name": context.params.name,
        "email": context.params.email,
        "isEmailVerified": context.params.isEmailVerified || false,
        "plan": context.params.plan,
        "status": context.params.status,
        "billingDate": context.params.billingDate,
        "expiryDays": context.params.expiryDays,
        "notifyMessage": context.params.notifyMessage,
        "isZervxServer": context.params.isZervxServer,
        "phone": {
          "code": context.params.phoneCode,
          "number": context.params.phoneNumber,
        },
      },
    });
    if (!response) {
      throw new MoleculerError("SOMETHING WENT WRONG");
    }
    if (context.params.isZervxServer) {
      //Admin
      await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.ADMINISTRATORS,
        "name": constantUtil.ADMIN,
        "isActive": true,
        "data": {
          "clientId": response._id?.toString(),
          "userName": context.params.name,
          "firstName": context.params.name,
          "lastName": context.params.name,
          "email": context.params.email,
          "isEmailVerified": context.params.isEmailVerified || false,
          "gender": context.params.gender,
          "avatar": constantUtil.DEFAULT_AVATAR, // context.params.avatar,
          "status": constantUtil.ACTIVE,
          "phone": {
            "code": context.params.phoneCode,
            "number": context.params.phoneNumber,
          },
          "languageCode": constantUtil.DEFAULT_LANGUAGE,
          "otp": null,
          "privileges": {},
          "accessToken": null,
          "password": bcrypt.hashSync(
            constantUtil.DEFAULT_PASSWORD,
            bcrypt.genSaltSync(8),
            null
          ),
        },
      });
      // Developer
      await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.ADMINISTRATORS,
        "name": constantUtil.DEVELOPER,
        "isActive": true,
        "data": {
          "clientId": response._id?.toString(),
          "userName": context.params.name,
          "firstName": context.params.name,
          "lastName": context.params.name,
          "email": context.params.email,
          "isEmailVerified": context.params.isEmailVerified || false,
          "gender": context.params.gender,
          "avatar": constantUtil.DEFAULT_AVATAR, // context.params.avatar,
          "status": constantUtil.ACTIVE,
          "phone": {
            "code": "+0000",
            "number": context.params.phoneNumber,
          },
          "languageCode": constantUtil.DEFAULT_LANGUAGE,
          "otp": null,
          "privileges": {},
          "accessToken": null,
          "password": bcrypt.hashSync(
            constantUtil.DEFAULT_PASSWORD,
            bcrypt.genSaltSync(8),
            null
          ),
        },
      });
      // Migrate Base Data
      await this.broker.emit("admin.manageClientsBaseData", {
        "clientId": response._id?.toString(),
        "phoneCode": context.params.phoneCode,
        "phoneNumber": context.params.phoneNumber,
      });
      // Send Enail
      await this.broker.emit("admin.sendServiceMail", {
        "templateName": "clientWelcomeMail",
        "clientId": response._id?.toString(),
        "loginPhone":
          context.params.phoneCode + " " + context.params.phoneNumber,
        "password": constantUtil.DEFAULT_PASSWORD,
        "firstName": context.params.name,
        "lastName": context.params.name,
        // "adminUrl": "",
        "email": context.params.email,
      });
    }
    message = "SUCCESSFULLY ADDED A CLIENT";
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
  }
  return {
    "code": errorCode,
    "message": message,
  };
};

adminAction.getclients = async function (context) {
  const search = context?.params?.search ?? "";
  const query = {
    "name": constantUtil.BRSCLIENTS,
  };
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    query["createdAt"] = {
      "$gte": setDayStartHours(context.params.fromDate),
      "$lt": setDayEndHours(context.params.toDate),
    };
  }
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": [
      "data.name",
      "data.key",
      "data.notifyMessage",
      "data.email",
      "data.status",
      "data.isZervxServer",
    ],
  });
  const response = await this.adapter.find({
    "sort": "-updatedAt",
    "limit": parseInt(context.params.limit ?? 20),
    "offset": parseInt(context.params?.skip ?? 0),
    "query": query,
    "search": search,
    "searchFields": [
      "data.name",
      "data.key",
      "data.notifyMessage",
      "data.email",
      "data.status",
      "data.isZervxServer",
    ],
  });
  return {
    count,
    response,
  };
};

adminAction.editClient = async function (context) {
  let errorCode = 200,
    message = "";
  try {
    const response = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
        "name": constantUtil.BRSCLIENTS,
      },
      {
        "data.secretKey": context.params.secretKey,
        "data.name": context.params.name,
        "data.email": context.params.email,
        "data.plan": context.params.plan,
        "data.status": context.params.status,
        "data.billingDate": context.params.billingDate,
        "data.expiryDays": context.params.expiryDays,
        "data.notifyMessage": context.params.notifyMessage,
        // "data.isZervxServer": context.params.isZervxServer,
      }
    );
    if (!response.nModified) {
      throw new MoleculerError("SOMETHING WENT WRONG");
    } else {
      message = "UPDATED";
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
  }
  return {
    "code": errorCode,
    "message": message,
  };
};

adminAction.clientPaymentDetails = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const response = await this.adapter.model.findOne({
    "data.key": context.params.key,
    "data.status": "true",
  });
  if (!response) {
    throw new MoleculerError(
      "SOMTHING WENT WRONG IN CLIENT PAYMENT DETAILS API"
    );
  } else {
    return response;
  }
};
adminAction.changeClientActiveStatus = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "name": constantUtil.BRSCLIENTS,
    },
    { "isActive": context.params.status === constantUtil.ACTIVE ? true : false }
  );
  if (!responseJson.nModified) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

adminAction.clientPayBill = async function (context) {
  let lastBillPaidDate = null;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId = context.params?.clientId || context.meta?.clientId;
  const days = context.params.days || 1;
  let todayStart = new Date(new Date().setHours(0, 0, 0, 0));
  let newExpiryDate = new Date();
  newExpiryDate.setDate(newExpiryDate.getDate() + days);
  //
  const responseJson = await this.adapter.model
    .findOneAndUpdate(
      {
        "data.clientId": mongoose.Types.ObjectId(clientId),
        "data.lastBillPaidDate": { "$lte": todayStart },
      },
      {
        "$set": {
          "data.expiryDate": newExpiryDate,
          "data.lastBillPaidDate": new Date(),
        },
      },
      { "new": true }
    )
    .lean();
  if (responseJson) {
    lastBillPaidDate = responseJson.data.lastBillPaidDate;
    responseJson["id"] = responseJson?._id?.toString();
    await storageUtil.write(constantUtil.GENERALSETTING, responseJson);
  } else {
    lastBillPaidDate = generalSettings.data.lastBillPaidDate;
  }
  return {
    "code": 200,
    "data": { "lastBillPaidDate": lastBillPaidDate },
    "message": "",
  };
};

adminAction.getAllActiveInActiveClients = async function () {
  const responseOffice = await this.adapter.model
    .find(
      {
        "name": constantUtil.BRSCLIENTS,
      },
      {
        "_id": 1,
        "data.key": 1,
        "data.name": 1,
        "data.plan": 1,
        "data.secretKey": 1,
      }
    )
    .populate("data.plan", { "_id": 1, "data.privileges": 1 })
    .lean();

  const responseJson = await responseOffice.map((each) => ({
    "_id": each._id,
    "name": each?.data?.name || "",
    "key": each?.data?.key || "",
    "secretKey": each?.data?.secretKey || "",
    "privileges": each.data?.plan?.data?.privileges || {},
  }));
  return {
    "code": 200,
    "data": {
      "count": parseInt(responseJson.length),
      "response": responseJson,
    },
    "message": "",
  };
};
// /#endregion Clients Data
//PLANS
adminAction.getPlansList = async function (context) {
  const search = context?.params?.search ?? "";
  const clientId = context.params?.clientId || context.meta?.clientId;
  const query = {
    "name": constantUtil.PLANS,
    // "dataType": constantUtil.BASEDATA,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    query["createdAt"] = {
      "$gte": new Date(new Date(context.params.fromDate).setHours(0, 0, 0, 0)),
      "$lt": new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      ),
    };
  }
  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.status", "data.role"],
  });
  const response = await this.adapter.find({
    "limit": parseInt(context.params.limit ?? 20),
    "offset": parseInt(context.params?.skip ?? 0),
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.status", "data.role"],
  });
  return {
    count,
    response,
  };
};

adminAction.createAndEditPlans = async function (context) {
  let responseJson;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const plansJson = await this.adapter.model.findOne({
    "data.role": context.params.data.role,
    "name": constantUtil.PLANS,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  });
  const menuList = {};
  const privileges = context.params?.data?.privileges || {};
  Object.keys(privileges).map((mainMenu) => {
    Object.keys(privileges[mainMenu]).map((subMenu) => {
      if (privileges[mainMenu][subMenu].VIEW === true) {
        // If not exist Add mainmenu object
        if (!menuList[mainMenu]) {
          menuList[mainMenu] = {};
        }
        // If not exist Add submenu object
        if (!menuList[mainMenu][subMenu]) {
          menuList[mainMenu][subMenu] = {};
        }
        menuList[mainMenu][subMenu] = privileges[mainMenu][subMenu];
      }
    });
  });
  context.params["data"]["privileges"] = menuList;

  if (context.params.id) {
    if (
      plansJson &&
      plansJson._id.toString() !== context.params.id.toString()
    ) {
      throw new MoleculerError("Plan Already Exists");
    }
    responseJson = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.params.id.toString()),
      {
        "data": context.params.data,
      }
    );
    context.emit("admin.rolesForceUpdate", {
      "_id": responseJson._id,
      "forceUpdate": context.params.data.forceUpdate,
      "clientId": clientId,
    });
  } else {
    if (plansJson) {
      throw new MoleculerError("Plan Already Axists");
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.PLANS,
      "name": constantUtil.PLANS,
      "data": context.params.data,
    });
  }
  if (!responseJson) {
    throw new MoleculerError("Error in Update");
  } else {
    return responseJson;
  }
};

adminAction.getPlanById = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "_id": context.params.id,
      "name": constantUtil.PLANS,
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("plan already exist");
  } else {
    return responseJson;
  }
};

adminAction.getAllPlans = async function (context) {
  const responseJson = {};
  const clientId = context.params?.clientId || context.meta?.clientId;
  const operatorRoles = await this.adapter.model
    .find({
      "data.status": { "$eq": constantUtil.ACTIVE },
      "name": { "$eq": constantUtil.PLANS },
    })
    .lean();
  responseJson["count"] = operatorRoles.length;
  responseJson["response"] = operatorRoles.map((roles) => ({
    "_id": roles._id,
    "name": roles.name,
    "data": {
      "role": roles.data.role,
      "name": roles.data.name,
    },
  }));
  return responseJson;
};
adminAction.getCategoryList = async function (context) {
  let response = null;
  const clientId = context.params?.clientId || context.meta?.clientId;
  switch (context.params.category?.toUpperCase()) {
    case constantUtil.ALPHABETICALORDER:
      if (context.params.userType === constantUtil.USER.toLowerCase()) {
        response = await this.broker.emit("user.getCategoryList", {
          ...context.params,
          "clientId": clientId,
        });
      } else if (
        context.params.userType === constantUtil.PROFESSIONAL.toLowerCase()
      ) {
        response = await this.broker.emit("professional.getCategoryList", {
          ...context.params,
          "clientId": clientId,
        });
      }
      response = response && response[0];
      break;

    case constantUtil.CITY:
      response = await this.broker.emit("category.getAllServiceCategory", {
        ...context.params,
        "clientId": clientId,
      });
      response = (response && response[0]?.data?.response) || [];
      break;
  }
  return response;
};
// PROMOTION CAMPAIGN
adminAction.createPromotionCampaign = async function (context) {
  const skip = 0;
  const limit = 0;
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (context.params.userType === constantUtil.USER) {
    const [userData] = await this.broker.emit(
      "user.getUsersDataforPromtionCampaign",
      {
        "category": context.params.category,
        "filter": context.paramsfilter,
        "type": context.params.type,
        "clientId": clientId,
      }
    );
    console.log(userData);
  }
};
// ------------- LOYALTY COUPONS Start -------------
adminAction.getRewardsList = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (context.params.rewardType === constantUtil.POINTS) {
    const data = await this.adapter.model.aggregate([
      {
        "$match": {
          "name": constantUtil.REWARDS,
          "data.name": constantUtil.POINTS,
          "data.rewardType": constantUtil.POINTS,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        },
      },
      {
        "$group": {
          "_id": "$data.serviceArea",
          "count": { "$sum": 1 },
          "data": { "$push": "$data" },
        },
      },
    ]);
    return {
      "code": 200,
      "message": "SUCCESSFULLY IMPLEMENTED",
      "response": data,
      "count": data.length,
    };
  }

  // $ BELOW IS ELSE CASE WHICH MEANS PARTNER DEALS
  const search = context.params?.search ?? "";
  const query = {
    "name": constantUtil.REWARDS,
    "data.rewardType": context.params.rewardType,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  };
  if (context.params.rewardType === constantUtil.PARTNERDEALS) {
    query["data.userType"] = context.params.userType.toUpperCase();
  }

  if (!!context.params.serviceArea === true) {
    query["data.city"] = mongoose.Types.ObjectId(
      context.params.serviceArea.toString()
    );
  }
  // FOR EXPORT DATE FILTER
  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }

  const count = await this.adapter.count({
    "query": query,
    "search": search,
    "searchFields": ["data.rewardTitle", "data.couponCode"],
  });
  const data = await this.adapter.find({
    "limit": context.params?.limit,
    "offset": context.params?.skip,
    "query": query,
    "sort": "-updatedAt",
    "search": search,
    "searchFields": ["data.rewardTitle", "data.couponCode"],
    "fields": [
      "data.serviceArea",
      "data.rewardTitle",
      "data.couponCode",
      "data.rewardType",
      "data.totalUsageCount",
      "data.minimumEligibleAmount",
      "data.validFrom",
      "data.validTo",
      "data.isStatus",
    ],
  });

  return {
    count,
    "response": data.map((eachCoupons) => {
      if (context.params.rewardType === constantUtil.PARTNERDEALS) {
        return {
          "_id": eachCoupons._id,
          "data": {
            "serviceArea": eachCoupons.data.serviceArea,
            "rewardTitle": eachCoupons.data.title,
            "couponCode": eachCoupons.data.couponCode,
            "rewardType": eachCoupons.data.rewardType,
            "totalUsageCount": eachCoupons.data.totalUsageCount,
            "minimumEligibleAmount": eachCoupons.data.minimumEligibleAmount,
            "validFrom": eachCoupons.data.validFrom,
            "validTo": eachCoupons.data.validTo,
            "status": eachCoupons.isActive,
          },
        };
      }
    }),
  };
};

adminAction.managePartnerDealsReward = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  try {
    context.params.guidelines = JSON.parse(context.params.guidelines);
  } catch (error) {
    throw new MoleculerClientError("guidelines must be json array");
  }
  // count
  const alredyExists = await this.adapter.model.countDocuments({
    "data.couponCode": context.params.couponCode,
    //"data.clientId": mongoose.Types.ObjectId(clientId),
  });
  if (alredyExists && context.params.requestType === "add") {
    //create the new coupon with existing code code is invaid so throw error
    throw new MoleculerClientError("COUPON CODE ALREADY EXISTS"); //INFO_COUPON_CODE_EXISTS
  }
  const data = {
    "data.clientId": clientId,
    "data.title": context.params.title,
    "data.description": context.params.description,
    "data.rewardType": constantUtil.PARTNERDEALS,
    "data.totalUsageCount": context.params.totalUsageCount,
    "data.minimumEligibleAmount": context.params.minimumEligibleAmount,
    "data.userType": context.params.userType,
    "data.validFrom": setDayStartHours(context.params?.validFrom),
    "data.validTo": setDayEndHours(context.params?.validTo),
    "data.expiryDate": setDayEndHours(context.params?.expiryDate),
    "data.termsAndCondition": context.params.termsAndCondition,
    "data.sponsorName": context.params.sponsorName,
    "data.sponsorLink": context.params.sponsorLink,
    "data.guidelines": context.params.guidelines,
    "data.imageUrl": context.params.imageUrl,
    "isActive": context.params.isActive,
  };

  // update uploded file
  if (context.params.files?.length) {
    const imageResponse = await this.broker.emit(
      "admin.uploadSingleImageInSpaces",
      {
        "clientId": clientId,
        "files": context.params.files,
        "fileName": `${clientId + "_" + Date.now() + "_"}${
          context.params.couponCode
        }`,
      }
    );
    if (!imageResponse || !imageResponse[0]) {
      throw new MoleculerError(
        `ERORR IN UPLOAD IMAGE FROM ${context.params.requestType.toUpperCase()} LOYALTY COUPON `
      );
    }
    context.params.imageUrl = imageResponse[0];
    data["data.imageUrl"] = imageResponse[0];
  }
  // valid time period checking
  if (context.params.validFrom > context.params.validTo) {
    throw new MoleculerClientError(
      "VALID TO DATE SHOULD BE GREATER THAN VALIDFROM DATE"
    );
  }
  if (context.params.validTo > context.params.expiryDate) {
    throw new MoleculerClientError(
      "EXPIRYDATE DATE SHOULD BE GREATER THAN VALIDTO DATE"
    );
  }
  if (context.params.requestType === "edit") {
    if (!!context.params.id === false) {
      throw new ValidationError(
        "VALIDATION ERROR",
        422,
        "SOMTHING WENT WRONG",
        [
          {
            "type": "string",
            "expected": "STRING",
            "actual": context?.params?.id,
            "field": "id",
            "message": "For update the loyality coupon id filed is required",
          },
        ]
      );
    }
    const updateResponse = await this.adapter.model.updateOne(
      {
        "_id": context.params.id,
      },
      data
    );
    if (!updateResponse.nModified) {
      throw new MoleculerError(
        "ERROR IN UPDATE LOYALTY COUPON",
        500,
        "SOMETING WENT WRONG",
        context.params
      );
    } else {
      return {
        "code": 200,
        "message": "Loyalty coupon updated successfully",
        "data": context.params,
      };
    }
  }
  if (context.params.requestType === "add") {
    const loyaltyCoupon = await this.adapter.model.create({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.REWARDS,
      "name": constantUtil.REWARDS,
      "data.couponCode": context.params.couponCode,
      "data.name": constantUtil.PARTNERDEALS,
      "data.serviceArea": context.params.serviceArea,
      "data.usedCount": 0,
      ...data,
    });
    if (!loyaltyCoupon) {
      throw new MoleculerError(
        "ERROR IN CREATE LOYALTY COUPON",
        500,
        "SOMETING WENT WRONG",
        context.params
      );
    } else {
      return {
        "code": 200,
        "message": "A loyalty coupon added successfully",
        "data": context.params,
      };
    }
  }
};
adminAction.managePointsRewards = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const professionalData = {
    //"data.clientId": clientId,
    "data.title": context.params.professional.title,
    "data.description": context.params.professional.description,
    "data.pointsType": context.params.professional.pointsType,
    "data.rewardPoints": context.params.professional.rewardPoints,
    "data.redeemLimitMin": context.params.professional.redeemLimitMin,
    "data.redeemLimitMax": context.params.professional.redeemLimitMax,
    "data.redeemPointValue": context.params.professional.redeemPointValue,
    "data.termsAndCondition": context.params.professional.termsAndCondition,
    "data.guidelines": context.params.professional.guidelines,
    "isActive": context.params.professional.isActive,
  };
  const usersData = {
    //"data.clientId": clientId,
    "data.title": context.params.user.title,
    "data.description": context.params.user.description,
    "data.pointsType": context.params.user.pointsType,
    "data.rewardPoints": context.params.user.rewardPoints,
    "data.redeemLimitMin": context.params.user.redeemLimitMin,
    "data.redeemLimitMax": context.params.user.redeemLimitMax,
    "data.redeemPointValue": context.params.user.redeemPointValue,
    "data.termsAndCondition": context.params.user.termsAndCondition,
    "data.guidelines": context.params.user.guidelines,
    "isActive": context.params.user.isActive,
  };
  if (context.params.requestType === "add") {
    const exist = await this.adapter.model.countDocuments({
      "name": constantUtil.REWARDS,
      "data.serviceArea": context.params.serviceArea,
      //"data.clientId": clientId,
      "data.name": constantUtil.POINTS,
      "data.rewardType": constantUtil.POINTS,
      "data.userType": {
        "$in": [constantUtil.PROFESSIONAL, constantUtil.USER],
      },
    });
    if (exist) {
      throw new MoleculerError("THIS IS CITY ALREDAY HAVE POINTS REWARD");
    }
    const pointReward = await this.adapter.model.insertMany([
      {
        "name": constantUtil.REWARDS,
        "data.name": constantUtil.POINTS,
        "data.rewardType": constantUtil.POINTS,
        "data.userType": constantUtil.PROFESSIONAL,
        "data.serviceArea": context.params.serviceArea,
        ...professionalData,
      },
      {
        "name": constantUtil.REWARDS,
        "data.name": constantUtil.POINTS,
        "data.rewardType": constantUtil.POINTS,
        "data.userType": constantUtil.USER,
        "data.serviceArea": context.params.serviceArea,
        ...usersData,
      },
    ]);
    if (!pointReward?.length) {
      throw new MoleculerError(
        "POINTS REWARD NOT ADD CORRECTLY SOMETHING WENT WRONGF"
      );
    } else {
      return {
        "code": 200,
        "message": "SUCESSFULLY POINTS REWARD ADDED",
        "data": pointReward,
      };
    }
  }
  if (context.params.requestType === "edit") {
    if (!context.params.professional.rewardDocId) {
      throw new MoleculerError("FOR EDIT 'professional.rewardDocId' REQUIRED");
    }
    if (!context.params.user.rewardDocId) {
      throw new MoleculerError("FOR EDIT 'user.rewardDocId' REQUIRED");
    }
    const professionalUpdate = await this.adapter.model.updateOne(
      { "_id": context.params.professional.rewardDocId },
      professionalData
    );
    if (!professionalUpdate.nModified) {
      throw new MoleculerError(
        "SOMETHING WENT WRONG IN PROINT REWARD PROFESSIONAL DOC UPDATE"
      );
    }
    const userUpdate = await this.adapter.model.updateOne(
      { "_id": context.params.user.rewardDocId },
      usersData
    );
    if (!userUpdate.nModified) {
      throw new MoleculerError(
        "SOMETHING WENT WRONG IN PROINT REWARD USER DOC UPDATE"
      );
    }
    return {
      "code": "200",
      "message": "DOCUMENTED EDITED SUCCESSFULLY",
    };
  }
};

adminAction.redeemRewardsPointConfig = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  const clientId = context.params?.clientId || context.meta?.clientId;
  let errorCode = 200,
    message = "",
    languageCode = "",
    activeLoyaltyCouponsList = {};
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode || "";
  }
  const { SOMETHING_WENT_WRONG } = await notifyMessage.setNotifyLanguage(
    languageCode
  );
  try {
    // //--------------find category because Reward works location based Start ----------
    // let serviceCategory = await this.broker.emit(
    //   "category.getPopularServiceCategory",
    //   context.params
    // );
    // if (!serviceCategory)
    //   throw new MoleculerError(INFO_SERVICE_NOT_PROVIDE);
    // serviceCategory = serviceCategory && serviceCategory[0];
    // //--------------find category because Reward works location based End ----------
    // activeLoyaltyCouponsList = await this.adapter.model.aggregate([
    //   {
    //     "$match": {
    //       "name": constantUtil.REWARDS,
    //       "isActive": true,
    //       "data.rewardType": constantUtil.POINTS,
    //       "data.serviceArea": serviceCategory._id.toString(),
    //       "data.userType": userType,
    //     },
    //   },
    //   {
    //     "$project": {
    //       "_id": 0,
    //       "redeemPointValue": "$data.redeemPointValue",
    //       "redeemLimitMin": "$data.redeemLimitMin",
    //       "redeemLimitMax": "$data.redeemLimitMax",
    //     },
    //   },
    // ]);
    // Return only one row
    // activeLoyaltyCouponsList = activeLoyaltyCouponsList && activeLoyaltyCouponsList[0];

    //-------- Active Reward List (POINT) Start -----------
    activeLoyaltyCouponsList = await this.broker.emit(
      "admin.redeemRewardsPointConfig",
      {
        "clientId": clientId,
        "lng": context.params?.lng,
        "lat": context.params?.lat,
        "userType": userType,
        "languageCode": languageCode,
      }
    );
    activeLoyaltyCouponsList =
      activeLoyaltyCouponsList && activeLoyaltyCouponsList[0];
    // if (!activeLoyaltyCouponsList)
    //   throw new MoleculerError(INFO_NOT_FOUND);
    //-------- Active Reward List (POINT) End -----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": activeLoyaltyCouponsList,
  };
};

// ------------- LOYALTY COUPONS END -------------
// ------------ Subscription Plan END --------------------
// adminAction.getAllLanguages = async function () {
//   const responseJson = {};
//   const jsonData = await this.adapter.find({
//     "query": {
//       "data.status": { "$eq": constantUtil.ACTIVE },
//       "name": { "$eq": constantUtil.LANGUAGES },
//     },
//   });
//   responseJson["count"] = jsonData.length;
//   responseJson["response"] = jsonData.map((language) => ({
//     "_id": language._id,
//     "data": {
//       "languageName": language.data.languageName,
//       "languageCode": language.data.languageCode,
//     },
//   }));
//   return responseJson;
// };

adminAction.getMembershipPlanList = async function (context) {
  // const skip = parseInt(context?.params?.skip ?? 0);
  // const limit = parseInt(context?.params?.limit ?? 20);
  // const search = context?.params?.search ?? "";
  const clientId = context.params?.clientId || context.meta?.clientId;
  const query = {
    "name": constantUtil.MEMBERSHIPPLAN,
  };
  query["data.status"] =
    (context?.params?.filter ?? "") !== ""
      ? { "$eq": context.params.filter }
      : { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] };

  // FOR EXPORT DATE FILTER
  if (
    (context?.params?.fromDate ?? "") !== "" &&
    (context?.params?.toDate ?? "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
  }
  // FOR EXPORT DATE FILTER

  const membershipPlanResponseJson = {};
  const count = await this.adapter.count({
    "query": query,
    "search": context?.params?.search ?? "",
    "searchFields": ["data.name"],
  });
  const jsonData = await this.adapter.find({
    "limit": parseInt(context?.params?.limit ?? 20),
    "offset": parseInt(context?.params?.skip ?? 0),
    "query": query,
    "sort": "-updatedAt",
    "search": context?.params?.search ?? "",
    "searchFields": ["data.name"],
  });
  membershipPlanResponseJson["count"] = count;
  membershipPlanResponseJson["response"] = jsonData.map((membershipPlan) => ({
    "_id": membershipPlan._id,
    "data": {
      "name": membershipPlan.data.name,
      "userType": membershipPlan.data.userType,
      "planType": membershipPlan.data.planType,
      "totalNoOfRides": membershipPlan.data.totalNoOfRides,
      "validityDays": membershipPlan.data.validityDays,
      "isDefault": membershipPlan.data.isDefault,
      "status": membershipPlan.data.status,
    },
  }));
  return membershipPlanResponseJson;
};

adminAction.getMembershipPlanById = async function (context) {
  const responseJson = await this.adapter.model.findOne({
    "name": constantUtil.MEMBERSHIPPLAN,
    "_id": context.params.id,
  });
  if (!responseJson) {
    throw new MoleculerError("Invalid Details");
  } else {
    return responseJson;
  }
};

adminAction.updateMembershipPlan = async function (context) {
  let responseJson;
  const clientId = context.params.data.clientId || context.meta.clientId;
  const docsJson = await this.adapter.model.findOne({
    "data.name": context.params.data.name,
    "name": constantUtil.MEMBERSHIPPLAN,
  });
  if (context.params.id) {
    if (docsJson && docsJson._id.toString() !== context.params.id.toString()) {
      throw new MoleculerError("Plan already exist");
    }
    responseJson = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id.toString()),
      },
      {
        "data.planType": context.params.data.planType,
        "data.validityDays": context.params.data.validityDays,
        "data.totalNoOfRides": context.params.data.totalNoOfRides,
        "data.status": context.params.data.status,
        "data.isDefault": context.params.data.isDefault,
      }
    );
    if (!responseJson.nModified) {
      throw new MoleculerError("SOMTHING WENT WRONG");
    }
    // REMOVE ANOTHER DEFAULT PLAN
    if (context.params.data.isDefault) {
      this.adapter.model.updateMany(
        {
          "name": constantUtil.MEMBERSHIPPLAN,
          "_id": { "$ne": context.params.id },
          "data.status": true,
        },
        { "data.status": false }
      );
    }
  } else {
    if (docsJson) {
      throw new MoleculerError("Plan already exist");
    }
    responseJson = await this.adapter.insert({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.MEMBERSHIPPLAN,
      "name": constantUtil.MEMBERSHIPPLAN,
      "data": {
        "clientId": clientId,
        "name": context.params.data.planName,
        "userType": context.params.data.userType,
        "planType": context.params.data.planType,
        "validityDays": context.params.data.validityDays,
        "totalNoOfRides": context.params.data.totalNoOfRides,
        "isDefault": false,
        "status": context.params.data.status,
      },
    });
  }
  return responseJson;
};

// adminAction.changeLanguageStatus = async function (context) {
//   const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
//   const responseJson = await this.adapter.model.updateMany(
//     {
//       "_id": { "$in": ids },
//       "name": constantUtil.LANGUAGES,
//     },
//     {
//       "data.status": context.params.status,
//     }
//   );
//   if (responseJson.nModified == 0)
//     throw new MoleculerError("Error in status change");
//   return { "code": 1, "response": "Updated Successfully" };
// };
// adminAction.getDefaultLanguage = async function () {
//   const responseJson = await this.adapter.model.findOne({
//     "name": constantUtil.LANGUAGES,
//     "data.languageDefault": true,
//   });
//   if (!responseJson) throw new MoleculerError("Invalid Details");
//   return responseJson;
// };
// adminAction.updateTranslateKeys = async function (context) {
//   const responseJson = await this.adapter.updateById(
//     mongoose.Types.ObjectId(context.params.id.toString()),
//     {
//       "data.languageKeys": context.params.languageKeys,
//     }
//   );
//   if (!responseJson) throw new MoleculerError("Invalid Details");
//   return responseJson;
// };
// adminAction.setDefaultLanguage = async function (context) {
//   let responseJson = await this.adapter.model.updateMany(
//     {
//       "name": constantUtil.LANGUAGES,
//     },
//     {
//       "data.languageDefault": false,
//     }
//   );
//   if (!responseJson) throw new MoleculerError("Invalid Details");

//   responseJson = await this.adapter.model.updateOne(
//     {
//       "name": constantUtil.LANGUAGES,
//       "_id": context.params.id,
//     },
//     {
//       "data.languageDefault": true,
//     }
//   );
//   if (!responseJson) throw new MoleculerError("Invalid Details");

//   return { "code": 1, "response": "Updated Successfully" };
// };
// ------------ Subscription Plan END --------------------

adminAction.getMembershipListByUserType = async function (context) {
  let response;
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (context.params.userType === "user") {
    [response] = await this.broker.emit("users.getMembeshipersList", {
      ...context.params,
      "clientId": clientId,
    });
  } else if (context.params.userType === "professional") {
    [response] = await this.broker.emit("professional.getMembershipList", {
      ...context.params,
      "clientId": clientId,
    });
  }
  if (response?.code > 200) {
    throw new MoleculerError("SOMETHING WENT WRONG");
  } else {
    return response.data;
  }
};

adminAction.getRewardPointOfCity = async function (context) {
  return this.adapter.model.find({
    "name": constantUtil.REWARDS,
    "data.name": constantUtil.POINTS,
    "data.rewardType": constantUtil.POINTS,
    "data.serviceArea": context.params.serviceAreaId,
  });
};

adminAction.getAllRideDetailsExternal = async function (context) {
  // const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  // const { INVALID_ACCESS } = notifyMessage.setNotifyLanguage(
  //   context?.params?.langCode
  // );
  // if (context.params.access_token === generalSettings.data.authToken) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  return this.broker
    .emit("booking.getAllRideDetailsExternal", {
      ...context.params,
      "clientId": clientId,
    })
    .then((response) => response[0]);
  // } else {
  //   return INVALID_ACCESS;
  // }
};

//#region Admin Common API's

adminAction.getAllList = async function (context) {
  let query = {},
    getFields = {},
    responseData = {},
    count = 0;
  const name = (context?.params?.name || "").toUpperCase();
  const search = context?.params?.search ?? "";
  const clientId = context.params?.clientId || context.meta?.clientId;

  switch (name) {
    case constantUtil.ADMINISTRATORS:
      query = {
        "name": { "$in": [constantUtil.DEVELOPER, constantUtil.ADMIN] },
        //"data.clientId": mongoose.Types.ObjectId(clientId),
      };
      getFields = [
        "name",
        "data.firstName",
        "data.lastName",
        "data.financePersonName",
        "data.contactPersonName",
        "data.email",
        "data.phone.code",
        "data.phone.number",
      ];
      break;
  }
  if (query?.name) {
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      query["createdAt"] = {
        "$gte": new Date(
          new Date(context.params.fromDate).setHours(0, 0, 0, 0)
        ),
        "$lt": new Date(
          new Date(context.params.toDate).setHours(23, 59, 59, 999)
        ),
      };
    }
    //
    count = await this.adapter.count({
      "query": query,
      "search": search,
      "searchFields": getFields,
      // "searchFields": [
      // "data.name",
      // "data.financePersonName",
      // "data.contactPersonName",
      // "data.email",
      // "data.phone.number",
      // ],
    });
    responseData = await this.adapter.find({
      "limit": parseInt(context.params?.limit ?? 20),
      "offset": parseInt(context.params?.skip ?? 0),
      "query": query,
      "sort": "-updatedAt",
      "search": search,
      "searchFields": getFields,
      // "searchFields": [
      // "data.name",
      // "data.financePersonName",
      // "data.contactPersonName",
      // "data.email",
      // "data.phone.number",
      // ],
    });
  }
  return {
    "count": count,
    "response": responseData,
  };
};

adminAction.changeStatus = async function (context) {
  let query = {},
    responseJson = null;
  const name = (context?.params?.name || "").toUpperCase();
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const clientId = context.params?.clientId || context.meta?.clientId;

  switch (name) {
    case constantUtil.ADMINISTRATORS:
      query = {
        "_id": { "$in": ids },
        "name": { "$in": [constantUtil.DEVELOPER, constantUtil.ADMIN] },
      };
      break;

    default:
      query = {
        "_id": { "$in": ids },
      };
      break;
  }
  switch (name) {
    case constantUtil.CONST_REGISTRATIONSETTING:
    case constantUtil.CALLCENTER:
    case constantUtil.HEATMAP:
    case constantUtil.COMMON:
      responseJson = await this.adapter.model.updateMany(
        { ...query },
        {
          "isActive":
            context.params.status === constantUtil.ACTIVE ? true : false,
        }
      );
      break;

    default:
      responseJson = await this.adapter.model.updateMany(
        {
          // "_id": { "$in": ids },
          // "name": constantUtil.BOOKINGSERVICEMANAGEMET,
          ...query,
        },
        {
          "data.status": context.params.status,
          "data.accessToken": "",
        }
      );
      break;
  }
  if (!responseJson?.nModified) {
    throw new MoleculerError("Error in status change", 500);
  }
  await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    "type": name,
    "clientId": clientId,
  });
  switch (name) {
    case constantUtil.COMMON:
      {
        const category = await this.adapter.model
          .findOne({ ...query }, { "name": 1 })
          .sort({ "_id": -1 })
          .lean();
        await this.broker.emit("admin.updateDataInRedis", {
          "type": constantUtil.COMMON,
          "category": (
            category.name || constantUtil.CONST_GENDER
          ).toUpperCase(),
        });
      }
      break;

    default:
      await this.broker.emit("admin.updateDataInRedis", {
        "type": name,
      });
      break;
  }
  return { "code": 1, "response": "Updated Successfully" };
};
//#endregion

adminAction.administratorsCreateAndUpdate = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "";
  // find requested account is existed
  const action = context.params.action;
  const roleName = (context.params.role || constantUtil.ADMIN).toUpperCase();
  const clientId = context.params?.clientId || context.meta?.clientId;
  try {
    const exist = await this.adapter.model
      .findOne(
        {
          // "name": roleName,
          "name": { "$in": [constantUtil.ADMIN, constantUtil.DEVELOPER] },
          "data.phone.code": context.params.phoneCode,
          "data.phone.number": context.params.phoneNumber,
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        },
        { "data.phone.number": 1 }
      )
      .lean();
    if (exist) {
      // const errors = [];
      // if (context.params.phoneNumber === exist.data?.phone?.number) {
      //   errors.push({
      //     "message": "phone number already regitered",
      //     "data": context.params.phoneNumber,
      //   });
      throw new MoleculerClientError("Phone Number Already Regitered");
      // }
      // throw new MoleculerClientError(
      //   "ALREADY FOUND",
      //   204,
      //   "NON RETRYABLE",
      //   errors
      // );
    }
    const password = bcrypt.hashSync(
      context.params.password,
      bcrypt.genSaltSync(8),
      null
    );
    // create doucment
    const newManagement = await this.adapter.model.create({
      "dataType": constantUtil.CLIENTDATA,
      "type": constantUtil.ADMINISTRATORS,
      "name": roleName,
      "data": {
        "clientId": clientId,
        "userName": context.params.userName,
        "firstName": context.params.firstName,
        "lastName": context.params.lastName,
        "email": context.params.email,
        "isEmailVerified": context.params.isEmailVerified || false,
        "gender": context.params.gender,
        "avatar": context.params.avatar,
        "status": context.params.status || constantUtil.ACTIVE,
        "phone": {
          "code": context.params.phoneCode,
          "number": context.params.phoneNumber,
        },
        "languageCode": context.params.languageCode,
        "otp": null,
        "privileges": {},
        "accessToken": null,
        "password": password,
      },
    });
    if (!newManagement) {
      throw new MoleculerError("SOMETHING WENT WORONG");
    }
    const authToken = await jwtSign({
      "_id": newManagement._id,
    });
    const updateManagement = await this.adapter.model.updateOne(
      { "_id": mongoose.Types.ObjectId(newManagement._id.toString()) },
      { "accessToken": authToken }
    );
    if (!updateManagement.nModified) {
      throw new MoleculerError("SOMETHING WENT WORONG");
    }
    // else {
    //   return {
    //     "code": 200,
    //     "message": "Created successfully",
    //     "data": {
    //       // "authenticationKey": authToken,
    //       // "name": context.params.name,
    //       // "email": context.params.email,
    //       // "phone": context.params.phone,
    //     },
    //   };
    // }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//#region Registration Setup
adminAction.getRegistrationSetupList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    languageCode,
    count = 0;
  try {
    const search = context.params?.search ?? "";
    const clientId = context.params?.clientId || context.meta?.clientId;
    const query = {
      "name": constantUtil.CONST_REGISTRATIONSETTING,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (!!context.params.serviceArea === true) {
      query["data.city"] = mongoose.Types.ObjectId(
        context.params.serviceArea.toString()
      );
    }
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    const searchCity = {
      "$match": {
        "data.serviceAreaId": context.params.city
          ? mongoose.Types.ObjectId(context.params.city)
          : { "$ne": null },
      },
    };
    // count = await this.adapter.count({
    //   "query": query,
    //   "search": search,
    //   "searchFields": ["data.title", "data.serviceType"],
    // });
    // const data = await this.adapter.find({
    //   "limit": context.params?.limit,
    //   "offset": context.params?.skip,
    //   "query": query,
    //   "sort": "-updatedAt",
    //   "search": search,
    //   "searchFields": ["data.title", "data.serviceType"],
    //   "fields": [
    //     "data.serviceAreaId",
    //     "data.serviceType",
    //     "data.title",
    //     "data.Status",
    //   ],
    // });
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "data.serviceType": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "data.title": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, searchCity, { "$count": "all" }],
          "response": [
            // {
            //   "$lookup": {
            //     "from": "categories",
            //     "localField": "data.serviceAreaId",
            //     "foreignField": "_id",
            //     "as": "serviceAreaList",
            //   },
            // },
            // {
            //   "$unwind": {
            //     "path": "$serviceAreaList",
            //     "preserveNullAndEmptyArrays": true,
            //   },
            // },
            searchCity,
            {
              "$project": {
                "_id": "$_id",
                "data.serviceAreaId": "$data.serviceAreaId",
                "data.serviceType": "$data.serviceType",
                "data.title": "$data.title",
                "data.isActive": "$isActive",
              },
            },
            { "$sort": { "_id": -1 } },
            searchQuery,
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];

    // // return {
    // //   count,
    // responseData = data?.map((eachItem) => {
    //   return {
    //     "_id": eachItem._id,
    //     "data": {
    //       "serviceAreaId": eachItem.data.serviceAreaId,
    //       "serviceType": eachItem.data.serviceType,
    //       "title": eachItem.data.title,
    //       "isActive": eachItem.isActive,
    //     },
    //   };
    // });
    // // };
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageRegistrationSetup = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    try {
      context.params.guidelines = JSON.parse(context.params.guidelines);
    } catch (error) {
      context.params.guidelines = [];
    }
    try {
      context.params.commonDocuments = JSON.parse(
        context.params.commonDocuments
      );
    } catch (error) {
      context.params.commonDocuments = [];
    }
    try {
      context.params.vehicleDocuments = JSON.parse(
        context.params.vehicleDocuments
      );
    } catch (error) {
      context.params.vehicleDocuments = [];
    }
    try {
      context.params.otherDocuments = JSON.parse(context.params.otherDocuments);
    } catch (error) {
      context.params.otherDocuments = [];
    }
    try {
      context.params.profileVerificationDocuments = JSON.parse(
        context.params.profileVerificationDocuments
      );
    } catch (error) {
      context.params.profileVerificationDocuments = [];
    }
    // count
    const alredyExists = await this.adapter.model.countDocuments({
      "name": constantUtil.CONST_REGISTRATIONSETTING,
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      "data.title": trim(context.params.title),
      "data.serviceAreaId": context.params.serviceAreaId,
      "data.serviceType": context.params.serviceType,
      "data.userType": context.params.userType,
    });
    if (alredyExists && context.params.action === "add") {
      //create the new coupon with existing code code is invaid so throw error
      throw new MoleculerClientError("ALREADY EXISTS");
    } else if (context.params.action === "edit") {
      if (!!context.params.id === false) {
        throw new MoleculerError("VALIDATION ERROR");
      }
    }
    const data = {
      //"data.clientId": clientId,
      "data.description": context.params.description,
      "data.guidelines": context.params.guidelines,
      "data.imageUrl": context.params.imageUrl,
      "data.commonDocuments": context.params.commonDocuments,
      "data.vehicleDocuments": context.params.vehicleDocuments,
      "data.profileVerificationDocuments":
        context.params.profileVerificationDocuments,
      "data.otherDocuments": context.params.otherDocuments,
    };

    // update uploded file
    if (context.params.files?.length) {
      let imageResponse = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${clientId + "_" + Date.now() + "_"}${
            context.params.serviceType + context.params.userType
          }`,
        }
      );
      imageResponse = imageResponse && imageResponse[0];
      if (imageResponse) {
        context.params.imageUrl = imageResponse;
        data["data.imageUrl"] = imageResponse;
      }
    }
    if (context.params.action === "edit") {
      const updateResponse = await this.adapter.model.updateOne(
        { "_id": context.params.id },
        data
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          "ERROR IN UPDATE DATA",
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
    } else if (context.params.action === "add") {
      const newServiceType = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.CONST_REGISTRATIONSETTING,
        "name": constantUtil.CONST_REGISTRATIONSETTING,
        "data.clientId": clientId,
        "data.serviceAreaId": context.params.serviceAreaId,
        "data.serviceType": context.params.serviceType,
        "data.userType": context.params.userType,
        "data.title": trim(context.params.title),
        "isActive": true,
        ...data,
      });
      if (!newServiceType) {
        throw new MoleculerError(
          "ERROR IN CREATE DATA",
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newServiceType._id?.toString();
    }
    // // Update All Documents --> Must update Only One time
    // await this.adapter.updateMany(
    //   {
    //     "name": constantUtil.DOCUMENTS,
    //     "data.docsServiceTypeId": mongoose.Types.ObjectId(context.params.id),
    //   },
    //   {
    //     "data.status": constantUtil.INACTIVE,
    //     "isActive": false,
    //   }
    // );
    // // Setting Update for RegistrationSetup Time
    // // const settingsUpdate = await this.adapter.model
    // //   .findOneAndUpdate(
    // //     { "name": constantUtil.GENERALSETTING },
    // //     {
    // //       "data.registrationSetupUpdateTime": Date.now(),
    // //     },
    // //     { "new": true }
    // //   )
    // //   .lean();
    // // //
    // // settingsUpdate["id"] = settingsUpdate._id;
    // // await storageUtil.write(constantUtil.GENERALSETTING, settingsUpdate);
    await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
      "type": constantUtil.CONST_REGISTRATIONSETTING,
      "clientId": clientId,
    });
    //
    await this.broker.emit("admin.updateRegistrationDocuments", {
      "action": context.params.action,
      "clientId": clientId,
      "userType": context.params.userType,
      "documentsList": context.params.vehicleDocuments,
      "docsFor": constantUtil.DRIVERDOCUMENTS,
      "docsServiceType": context.params.serviceType,
      "docsServiceTypeId": context.params.id, // serviceTypeId
      "serviceAreaId": context.params.serviceAreaId,
    });
    await this.broker.emit("admin.updateRegistrationDocuments", {
      "action": context.params.action,
      "clientId": clientId,
      "userType": context.params.userType,
      "documentsList": context.params.profileVerificationDocuments,
      "docsFor": constantUtil.PROFILEDOCUMENTS,
      "docsServiceType": context.params.serviceType,
      "docsServiceTypeId": context.params.id, // serviceTypeId
      "serviceAreaId": context.params.serviceAreaId,
    });
    await this.broker.emit("admin.updateDataInRedis", {
      "type": constantUtil.DOCUMENTS,
    });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
// adminAction.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId =
//   async function (context) {
//     let responseData,
//       errorCode = 200,
//       message = "",
//       languageCode = "";
//     languageCode = context.meta?.adminDetails?.languageCode;
//     const { SOMETHING_WENT_WRONG } =
//       notifyMessage.setNotifyLanguage(languageCode);
//     try {
//       //#region LOCAL STORAGE:- DOCUMENT
//       const documentJson = await this.adapter.model
//         .find({
//           "name": constantUtil.DOCUMENTS,
//         })
//         .lean();

//       const documentsObjectFn = (data) => {
//         const documentsObject = {};
//         documentJson.forEach((documents) => {
//           if (documents.data.docsFor === data) {
//             documentsObject[documents._id] = {
//               "_id": documents._id,
//               "id": documents._id,
//               ...documents.data,
//             };
//           }
//         });
//         return documentsObject;
//       };
//       await storageUtil.write(
//         constantUtil.PROFILEDOCUMENTS,
//         documentJson ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS) : {}
//       );
//       await storageUtil.write(
//         constantUtil.DRIVERDOCUMENTS,
//         documentJson ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS) : {}
//       );
//       //#endregion LOCAL STORAGE:- DOCUMENT
//       // this.broker.emit("professional.updateAllProfessionals", {}); // UPDATE ALL PROFESSIONALS
//       // await this.broker.emit(
//       //   "professional.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId",
//       //   {
//       //     // "serviceType": context.params.docsServiceType,
//       //     "serviceTypeId": context.params.docsServiceTypeId,
//       //   }
//       // ); // UPDATE ALL PROFESSIONALS
//       message = constantUtil.SUCCESS;
//     } catch (e) {
//       errorCode = e.code || 400;
//       message = e.code ? e.message : SOMETHING_WENT_WRONG;
//       responseData = {};
//     }
//     return {
//       "code": errorCode,
//       "message": message,
//       "data": responseData,
//     };
//   };

adminAction.getRegistrationSetupByServiceAreaId = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    languageCode = "";
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  languageCode = context.meta?.professionalDetails?.languageCode;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    responseData = await this.adapter.model.aggregate([
      {
        "$match": {
          "name": constantUtil.CONST_REGISTRATIONSETTING,
          "isActive": true,
          "data.serviceAreaId": mongoose.Types.ObjectId(
            context.params.serviceAreaId?.toString()
          ),
          //"data.clientId": mongoose.Types.ObjectId(clientId),
        },
      },
      {
        "$project": {
          "_id": "$_id",
          "serviceAreaId": "$data.serviceAreaId",
          "serviceType": "$data.serviceType",
          "userType": "$data.userType",
          "title": "$data.title",
          "description": "$data.description",
          "imageUrl": {
            "$ifNull": ["$data.imageUrl", ""],
          },
          "vehicleDocuments": {
            "$ifNull": ["$data.vehicleDocuments", []],
          },
          "profileVerificationDocuments": {
            "$ifNull": ["$data.profileVerificationDocuments", []],
          },
          "commonDocuments": {
            "$ifNull": ["$data.commonDocuments", []],
          },
          "otherDocuments": {
            "$ifNull": ["$data.otherDocuments", {}],
          },
          "guidelines": {
            "$ifNull": ["$data.guidelines", { "title": "", "points": [] }],
          },
        },
      },
      { "$sort": { "createdAt": -1 } },
    ]);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData || [],
    "timestamp": generalSettings.data.registrationSetupUpdateTime || Date.now(),
    // "data": {
    //   "response": responseData || [],
    //   "timestamp":
    //     generalSettings.data.registrationSetupUpdateTime || Date.now(),
    // },
  };
};

adminAction.getRegistrationDocumentIdUsingServiceAreaIdAndServiceType =
  async function (context) {
    let responseData = [],
      errorCode = 200,
      message = "",
      languageCode = "";
    const clientId = context.params?.clientId || context.meta?.clientId;

    languageCode = context.meta?.professionalDetails?.languageCode;
    const { SOMETHING_WENT_WRONG } =
      notifyMessage.setNotifyLanguage(languageCode);
    try {
      responseData = await this.adapter.model.aggregate([
        {
          "$match": {
            "name": constantUtil.DOCUMENTS, //  "type": constantUtil.DOCUMENTS,
            "data.status": constantUtil.ACTIVE,
            "data.serviceAreaId": mongoose.Types.ObjectId(
              context.params.serviceAreaId?.toString()
            ),
            "data.docsServiceType": context.params.serviceType, // serviceType --> RIDE/DELIVERY/ALL
            "data.docsServiceTypeId": mongoose.Types.ObjectId(
              context.params.serviceTypeId?.toString()
            ),
            "data.clientId": mongoose.Types.ObjectId(clientId),
          },
        },
        {
          "$project": {
            "_id": 0,
            "documentId": "$_id",
            "docsServiceType": "$data.docsServiceType",
            "docsFor": {
              "$cond": {
                "if": {
                  "$eq": ["$data.docsFor", constantUtil.PROFILEDOCUMENTS],
                },
                "then": constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS,
                "else": constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS,
              },
            },
            "docsName": "$data.docsName",
            "docsDetail": "$data.docsDetail",
            "description": "$data.description",
          },
        },
        { "$sort": { "createdAt": -1 } },
      ]);
    } catch (e) {
      errorCode = e.code || 400;
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
      responseData = [];
    }
    return {
      "code": errorCode,
      "message": message,
      "data": responseData || [],
    };
  };
//#endregion  Registration Setup

//#region Incentive Setup
adminAction.getIncentiveList = async function (context) {
  let responseData = [],
    errorCode = 200,
    message = "",
    languageCode,
    count = 0;
  try {
    const search = context.params?.search ?? "";
    const clientId = context.params?.clientId || context.meta?.clientId;
    const query = {
      "type": constantUtil.COMMON,
      "name": constantUtil.CONST_INCENTIVE,
      "data.clientId": mongoose.Types.ObjectId(clientId),
    };
    if (!!context.params.serviceArea === true) {
      query["data.city"] = mongoose.Types.ObjectId(
        context.params.serviceArea.toString()
      );
    }
    if (context.params.userType?.toUpperCase() !== constantUtil.ALL) {
      query["data.userType"] = context.params.userType?.toUpperCase();
    }
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    const searchCity = {
      "$match": {
        "data.serviceAreaId": context.params.city
          ? mongoose.Types.ObjectId(context.params.city)
          : { "$ne": null },
      },
    };
    const searchQuery = {
      "$match": {
        "$or": [
          {
            "data.userType": {
              "$regex": search + ".*",
              "$options": "si",
            },
          },
          {
            "data.serviceType": {
              "$regex": search + ".*",
              "$options": "si",
            },
          },
          {
            "data.title": {
              "$regex": search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [searchQuery, searchCity, { "$count": "all" }],
          "response": [
            // {
            //   "$lookup": {
            //     "from": "categories",
            //     "localField": "data.serviceAreaId",
            //     "foreignField": "_id",
            //     "as": "serviceAreaList",
            //   },
            // },
            // {
            //   "$unwind": {
            //     "path": "$serviceAreaList",
            //     "preserveNullAndEmptyArrays": true,
            //   },
            // },
            // searchCity,
            {
              "$project": {
                "_id": "$_id",
                "title": "$data.title",
                "serviceAreaId": "$data.serviceAreaId",
                "userType": "$data.userType",
                "serviceType": "$data.serviceType",
                "campaignType": "$data.campaignType",
                "totalNoOfRides": "$data.totalNoOfRides",
                "amount": "$data.amount",
                "validFrom": "$data.validFrom",
                "validTo": "$data.validTo",
                "updatedAt": "$updatedAt",
                "isActive": "$isActive",
              },
            },
            { "$sort": { "updatedAt": -1 } },
            // searchQuery,
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);
    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};

adminAction.manageIncentiveSetup = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    languageCode = "";
  languageCode = context.meta?.adminDetails?.languageCode;
  const action = context.params.action;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const {
    SOMETHING_WENT_WRONG,
    INFO_ALREADY_EXISTS,
    INFO_INVALID_DETAILS,
    ALERT_CHECK_FROM_OR_TO_DATE,
  } = notifyMessage.setNotifyLanguage(languageCode);
  try {
    // try {
    //   context.params.guidelines = JSON.parse(context.params.guidelines);
    // } catch (error) {
    //   context.params.guidelines = [];
    // }
    // count
    const validFrom = context.params.validFrom;
    const validTo = context.params.validTo;
    const checkDuplicateRecord = {
      "type": constantUtil.COMMON,
      "name": constantUtil.CONST_INCENTIVE,
      // "data.name": context.params.name,
      "data.userType": context.params.userType,
      "data.serviceAreaId": mongoose.Types.ObjectId(
        context.params.serviceAreaId
      ),
      //"data.clientId": mongoose.Types.ObjectId(clientId),
      // "data.validTo": {
      //   // "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
      //   "$gte": new Date(validTo),
      // },
      "$or": [
        {
          "$and": [
            {
              "data.validTo": {
                // "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
                "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
              },
            },
            {
              "data.validFrom": {
                // "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
                "$lte": new Date(validFrom.setHours(0, 0, 0, 0)),
              },
            },
          ],
        },
        {
          "$and": [
            {
              "data.validTo": {
                // "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
                "$gte": new Date(validTo.setHours(0, 0, 0, 0)),
              },
            },
            {
              "data.validFrom": {
                // "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
                "$lte": new Date(validTo.setHours(0, 0, 0, 0)),
              },
            },
          ],
        },
      ],
      // "data.validTo": {
      //   // "$lt": new Date(validTo.setHours(23, 59, 59, 999)),
      //   "$lte": new Date(validTo),
      // },
      // "$or": [
      //   // {
      //   //   "data.validFrom": {
      //   //     "$lte": new Date(validFrom),
      //   //     "$gte": new Date(validTo),
      //   //   },
      //   // },
      //   {
      //     "data.validFrom": {
      //       "$gte": new Date(validFrom),
      //       "$lte": new Date(validTo),
      //     },
      //   },
      //   // {
      //   //   "data.validTo": {
      //   //     "$lte": new Date(validFrom),
      //   //     "$gte": new Date(validTo),
      //   //   },
      //   // },
      //   {
      //     "data.validTo": {
      //       "$gte": new Date(validFrom),
      //       "$lte": new Date(validTo),
      //     },
      //   },
      // ],
    };
    if (action === constantUtil.ACTION_EDIT) {
      checkDuplicateRecord["_id"] = {
        "$ne": mongoose.Types.ObjectId(context.params.id),
      };
    }
    const alredyExists = await this.adapter.model
      .find(checkDuplicateRecord, { "data.validFrom": 1, "data.validTo": 1 })
      .lean();
    // if (alredyExists && context.params.action === "add") {
    if (alredyExists.length > 0) {
      throw new MoleculerClientError(
        INFO_ALREADY_EXISTS + ". " + ALERT_CHECK_FROM_OR_TO_DATE
      ); // create the new coupon with existing code code is invaid so throw error
    } else if (action === constantUtil.ACTION_EDIT) {
      if (!!context.params.id === false) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
    if (action === constantUtil.ACTION_EDIT) {
      const updateResponse = await this.adapter.model.updateOne(
        { "_id": mongoose.Types.ObjectId(context.params.id) },
        {
          "$set": {
            "data.serviceAreaId": context.params.serviceAreaId,
            "data.name": context.params.name,
            "data.title": context.params.title,
            "data.description": context.params.description,
            "data.userType": context.params.userType,
            "data.serviceType": context.params.serviceType,
            "data.campaignType": context.params.campaignType,
            "data.totalNoOfRides": context.params.totalNoOfRides,
            "data.minimumReviewRating": context.params.minimumReviewRating,
            "data.rideAcceptanceRate": context.params.rideAcceptanceRate,
            "data.days": context.params.days?.split(","),
            "data.onlineTime": context.params.onlineTime,
            "data.minimumEligibleAmount": context.params.minimumEligibleAmount,
            "data.validFrom": context.params.validFrom.setHours(0, 0, 0, 0),
            "data.validTo": context.params.validTo.setHours(23, 59, 59, 999),
            "data.startTime": context.params.startTime,
            "data.startTime1": context.params.startTime1,
            "data.startTime2": context.params.startTime2,
            "data.endTime": context.params.endTime,
            "data.endTime1": context.params.endTime1,
            "data.endTime2": context.params.endTime2,
            "data.amount": context.params.amount,
            "data.incentiveType": context.params.incentiveType,
          },
        }
      );
      if (!updateResponse.nModified) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          INFO_INVALID_DETAILS,
          context.params
        );
      }
    } else if (action === constantUtil.ACTION_ADD) {
      const newRecord = await this.adapter.model.create({
        "dataType": constantUtil.CLIENTDATA,
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "data": {
          "clientId": clientId,
          "serviceAreaId": context.params.serviceAreaId,
          "name": context.params.name,
          "title": context.params.title,
          "description": context.params.description,
          "userType": context.params.userType,
          "serviceType": context.params.serviceType,
          "campaignType": context.params.campaignType,
          "totalNoOfRides": context.params.totalNoOfRides,
          "minimumReviewRating": context.params.minimumReviewRating,
          "rideAcceptanceRate": context.params.rideAcceptanceRate,
          "days": context.params.days?.split(","),
          "onlineTime": context.params.onlineTime,
          "minimumEligibleAmount": context.params.minimumEligibleAmount,
          "validFrom": context.params.validFrom.setHours(0, 0, 0, 0),
          "validTo": context.params.validTo.setHours(23, 59, 59, 999),
          "startTime": context.params.startTime,
          "startTime1": context.params.startTime1,
          "startTime2": context.params.startTime2,
          "endTime": context.params.endTime,
          "endTime1": context.params.endTime1,
          "endTime2": context.params.endTime2,
          "amount": context.params.amount,
          "incentiveType": context.params.incentiveType,
        },
        "isActive": true,
      });
      if (!newRecord) {
        throw new MoleculerError(
          INFO_INVALID_DETAILS,
          500,
          SOMETHING_WENT_WRONG,
          context.params
        );
      }
      context.params["id"] = newRecord._id?.toString();
    }
    // // await this.broker.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
    // //   "type": constantUtil.CONST_SERVICETYPE,
    // // });
    // await this.broker.emit("admin.updateDataInRedis", {
    //   "type": constantUtil.COMMON,
    //   "category": context.params.typeName,
    // });
    message = constantUtil.SUCCESS;
    responseData = context.params;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

adminAction.getProfessionalIncentiveDashboard = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;

  const validFrom = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000); // Last 30 Day Before
  const validTo = new Date(); // Today
  languageCode = context.meta?.professionalDetails?.languageCode;
  const action = context.params.action;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG, INFO_ALREADY_EXISTS, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    let incentive, onlineMinutesData, professionalRideData;
    incentive = await this.adapter.model
      .findOne({
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "isActive": true,
        "data.userType": constantUtil.PROFESSIONAL,
        "data.serviceAreaId": mongoose.Types.ObjectId(
          context.params.serviceAreaId
        ),
        "data.clientId": mongoose.Types.ObjectId(clientId),
        // "data.validFrom": {
        //   "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
        // },
        // "data.validTo": {
        //   "$lte": new Date(validTo.setHours(23, 59, 59, 999)),
        // },
        "data.validFrom": {
          "$lte": new Date(),
        },
        "data.validTo": {
          "$gte": new Date(),
        },
        // "$or": [
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        // ],
      })
      .populate("data.serviceAreaId", { "_id": 1, "locationName": 1 })
      .sort({ "_id": -1 })
      .lean();
    // if (incentiveSetupList?.length === 1) {
    if (incentive) {
      // await incentiveSetupList?.map(async (incentive) => {
      //--------- incentive StartDate & EndDate --> CAMPAIGN / DAILY ------
      const incentiveStartDate =
        incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
          ? new Date(incentive.data.validFrom.setHours(0, 0, 0, 0))
          : new Date(new Date().setHours(0, 0, 0, 0));
      const incentiveEndDate =
        incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
          ? new Date(incentive.data.validTo.setHours(23, 59, 59, 999))
          : new Date(new Date().setHours(23, 59, 59, 999));
      //------------------------------------
      let todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)); // Today night 23:59:59
      let tomorrowStartTime = new Date(
        new Date(Date.now() + 1 * 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
      ); // Tomorrow Morning 00:00:00
      //------------------------------------
      const incentiveEndTime = incentive.data.endTime;
      const incentiveStartTime = incentive.data.startTime;
      // const incentiveOnlineMinutes = incentive.data.onlineTime * 60; // Original --> Hours to Minutes
      const incentiveOnlineMinutes = incentive.data.onlineTime; // For Testing
      // incentive.data.endTime.getHours();
      // If the Incentive ValidTo is Expired and start next Day(new Day) then Incentive Run Automatically
      // if (
      //   // incentiveEndDate >= todayEndTime &&
      //   // incentiveEndDate <= tomorrowStartTime
      //   incentiveEndDate >= new Date() &&
      //   incentiveEndDate <= new Date()
      // ) {
      //---------------- Calculate Online Minitues -----------------
      onlineMinutesData = await this.broker.emit("log.calculateOnlineMinutes", {
        "clientId": clientId,
        "userType": constantUtil.PROFESSIONAL,
        "professionalId": context.params.professionalId,
        "startDate": incentiveStartDate,
        "endDate": incentiveEndDate,
      });
      onlineMinutesData = onlineMinutesData && onlineMinutesData[0];
      //---------------- Calculate Online Minitues -----------------
      //---------------- Count Accepted Ride & Ended Ride -----------
      professionalRideData = await this.broker.emit(
        "booking.getRideListByProfessionalId",
        {
          "clientId": clientId,
          "professionalId": context.params.professionalId,
          "startDate": incentiveStartDate,
          "endDate": incentiveEndDate,
          "minimumReviewRating": incentive.data.minimumReviewRating,
          "rideType": incentive.data.serviceType, // Need to Check
        }
      );
      professionalRideData = professionalRideData && professionalRideData[0];
      //---------------- Count Accepted Ride & Ended Ride -----------
      //---------------- Update Professional Incentive Amount -------
      if (
        onlineMinutesData?.onlineMinutes > 0 &&
        professionalRideData?.allRideCount > 0
      ) {
        // const rideAcceptanceRate =
        //   professionalrideData?.allRideCount /
        //   professionalrideData?.endedRideCount;
        let incentiveAmount = incentive.data?.amount || 0;
        const campaignType = incentive.data?.campaignType; // --> GUARANTEE / BONUS
        if (campaignType === constantUtil.CONST_GUARANTEE) {
          incentiveAmount =
            incentiveAmount - professionalRideData.professionalCommision;
        }
        // if (
        //   onlineMinutesData?.onlineMinutes >= incentiveOnlineMinutes &&
        //   professionalRideData?.allRideCount >=
        //     incentive.data.totalNoOfRides &&
        //   professionalRideData?.endedRideCount >=
        //     incentive.data.rideAcceptanceRate &&
        //   incentiveAmount > 0
        // ) {
        //   // this.broker.emit(
        //   //   "professional.calculateAndUpdateIncentiveAmount",
        //   //   {
        //   //     "professionalId": professional._id?.toString(),
        //   //     "onlineMinutes": onlineMinutesData?.onlineMinutes || 0,
        //   //     "allRideCount": onlineMinutesData?.onlineMinutes || 0,
        //   //     "availableAmount":
        //   //       professional.wallet.availableAmount || 0,
        //   //     "amount": incentiveAmount,
        //   //     "incentiveId": incentive._id?.toString(),
        //   //   }
        //   // );
        // }
      }
      //---------------- Update Professional Incentive Amount -------
      // }
      // });
    }
    const serviceAreaId =
      incentive?.data?.serviceAreaId?._id?.toString() || null;
    const serviceAreaName =
      incentive?.data?.serviceAreaId?.locationName || null;
    responseData["config"] = incentive?.data || {};
    responseData["config"]["serviceAreaId"] = serviceAreaId || "";
    responseData["config"]["serviceAreaName"] = serviceAreaName || "";
    // responseData["onlineTime"] = parseFloat(
    //   ((onlineMinutesData?.onlineMinutes || 0) / 60).toFixed(2)
    // );
    responseData["onlineMinutes"] = onlineMinutesData?.onlineMinutes || 0;
    responseData["totalNoOfRides"] = professionalRideData?.allRideCount || 0;
    responseData["rideAcceptanceRate"] =
      professionalRideData?.endedRideCount || 0;
    responseData["earnedAmount"] = parseFloat(
      (professionalRideData?.professionalCommision || 0).toFixed(2)
    );
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

adminAction.getUserIncentiveDashboard = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;

  const validFrom = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000); // Last 30 Day Before
  const validTo = new Date(); // Today
  languageCode = context.meta?.professionalDetails?.languageCode;
  const action = context.params.action;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { SOMETHING_WENT_WRONG, INFO_ALREADY_EXISTS, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    let incentive, userRideData;
    //#region User Current Service Area
    let userCurrentServiceCategory = await this.broker.emit(
      "category.getServieCategory",
      {
        "clientId": clientId,
        "lat": context.params.pickupLat,
        "lng": context.params.pickupLng,
        "categoryName": constantUtil.CONST_RIDE.toLowerCase(), //Must be Lowercase
      }
    );
    userCurrentServiceCategory =
      userCurrentServiceCategory && userCurrentServiceCategory[0];
    const userCurrentServiceAreaId =
      userCurrentServiceCategory?._id?.toString() || null;
    //#endregion User Current Service Area
    incentive = await this.adapter.model
      .findOne({
        "type": constantUtil.COMMON,
        "name": constantUtil.CONST_INCENTIVE,
        "isActive": true,
        "data.userType": constantUtil.USER,
        "data.serviceAreaId": mongoose.Types.ObjectId(userCurrentServiceAreaId),
        "data.clientId": mongoose.Types.ObjectId(clientId),
        // "data.validFrom": {
        //   "$gte": new Date(validFrom.setHours(0, 0, 0, 0)),
        // },
        // "data.validTo": {
        //   "$lte": new Date(validTo.setHours(23, 59, 59, 999)),
        // },
        "data.validFrom": {
          "$lte": new Date(),
        },
        "data.validTo": {
          "$gte": new Date(),
        },
        // "$or": [
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        //   {
        //     "$and": [
        //       {
        //         "data.validTo": {
        //           "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //       {
        //         "data.validFrom": {
        //           "$lte": new Date(new Date().setHours(0, 0, 0, 0)),
        //         },
        //       },
        //     ],
        //   },
        // ],
      })
      .populate("data.serviceAreaId", { "_id": 1, "locationName": 1 })
      .sort({ "_id": -1 })
      .lean();
    // if (incentiveSetupList?.length === 1) {
    if (incentive) {
      // await incentiveSetupList?.map(async (incentive) => {
      //--------- incentive StartDate & EndDate --> CAMPAIGN / DAILY ------
      const incentiveStartDate =
        incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
          ? new Date(incentive.data.validFrom.setHours(0, 0, 0, 0))
          : new Date(new Date().setHours(0, 0, 0, 0));
      const incentiveEndDate =
        incentive.data.incentiveType === constantUtil.CONST_CAMPAIGN
          ? new Date(incentive.data.validTo.setHours(23, 59, 59, 999))
          : new Date(new Date().setHours(23, 59, 59, 999));
      //------------------------------------
      let todayEndTime = new Date(new Date().setHours(23, 59, 59, 999)); // Today night 23:59:59
      let tomorrowStartTime = new Date(
        new Date(Date.now() + 1 * 24 * 60 * 60 * 1000).setHours(0, 0, 0, 0)
      ); // Tomorrow Morning 00:00:00
      //------------------------------------
      const incentiveEndTime = incentive.data.endTime;
      const incentiveStartTime = incentive.data.startTime;
      // const incentiveOnlineMinutes = incentive.data.onlineTime * 60; // Original --> Hours to Minutes
      const incentiveOnlineMinutes = incentive.data.onlineTime; // For Testing
      // incentive.data.endTime.getHours();
      // If the Incentive ValidTo is Expired and start next Day(new Day) then Incentive Run Automatically
      // if (
      //   // incentiveEndDate >= todayEndTime &&
      //   // incentiveEndDate <= tomorrowStartTime
      //   incentiveEndDate >= new Date() &&
      //   incentiveEndDate <= new Date()
      // ) {
      // //---------------- Calculate Online Minitues -----------------
      // onlineMinutesData = await this.broker.emit("log.calculateOnlineMinutes", {
      //   "clientId": clientId,
      //   "userType": constantUtil.USER,
      //   "userId": context.params.userId,
      //   "startDate": incentiveStartDate,
      //   "endDate": incentiveEndDate,
      // });
      // onlineMinutesData = onlineMinutesData && onlineMinutesData[0];
      // //---------------- Calculate Online Minitues -----------------
      //---------------- Count Accepted Ride & Ended Ride -----------
      userRideData = await this.broker.emit("booking.getRideListByUserId", {
        "clientId": clientId,
        "userId": context.params.userId,
        "startDate": incentiveStartDate,
        "endDate": incentiveEndDate,
        "minimumReviewRating": incentive.data.minimumReviewRating,
        "rideType": incentive.data.serviceType, // Need to Check
        "minimumEligibleAmount": parseFloat(
          incentive?.data?.minimumEligibleAmount || 0
        ),
      });
      userRideData = userRideData && userRideData[0];
      //---------------- Count Accepted Ride & Ended Ride -----------
      //---------------- Update Professional Incentive Amount -------
      if (
        // onlineMinutesData?.onlineMinutes > 0 &&
        userRideData?.allRideCount > 0
      ) {
        // const rideAcceptanceRate =
        //   userRideData?.allRideCount /
        //   userRideData?.endedRideCount;
        let incentiveAmount = incentive.data?.amount || 0;
        const campaignType = incentive.data?.campaignType; // --> GUARANTEE / BONUS
        // if (campaignType === constantUtil.CONST_GUARANTEE) {
        //   incentiveAmount = incentiveAmount;
        // }
        // if (
        //   onlineMinutesData?.onlineMinutes >= incentiveOnlineMinutes &&
        //   userRideData?.allRideCount >=
        //     incentive.data.totalNoOfRides &&
        //   userRideData?.endedRideCount >=
        //     incentive.data.rideAcceptanceRate &&
        //   incentiveAmount > 0
        // ) {
        //   // this.broker.emit(
        //   //   "professional.calculateAndUpdateIncentiveAmount",
        //   //   {
        //   //     "professionalId": professional._id?.toString(),
        //   //     "onlineMinutes": onlineMinutesData?.onlineMinutes || 0,
        //   //     "allRideCount": onlineMinutesData?.onlineMinutes || 0,
        //   //     "availableAmount":
        //   //       professional.wallet.availableAmount || 0,
        //   //     "amount": incentiveAmount,
        //   //     "incentiveId": incentive._id?.toString(),
        //   //   }
        //   // );
        // }
      }
      //---------------- Update Professional Incentive Amount -------
      // }
      // });
    }
    const serviceAreaId =
      incentive?.data?.serviceAreaId?._id?.toString() || null;
    const serviceAreaName =
      incentive?.data?.serviceAreaId?.locationName || null;
    responseData["config"] = incentive?.data || {};
    responseData["config"]["serviceAreaId"] = serviceAreaId || "";
    responseData["config"]["serviceAreaName"] = serviceAreaName || "";
    // responseData["onlineTime"] = parseFloat(
    //   ((onlineMinutesData?.onlineMinutes || 0) / 60).toFixed(2)
    // );
    //  responseData["onlineMinutes"] = onlineMinutesData?.onlineMinutes || 0;
    responseData["totalNoOfRides"] = userRideData?.allRideCount || 0;
    responseData["totalNoOfEligibleRides"] =
      userRideData?.totalNoOfEligibleRides || 0;
    responseData["rideAcceptanceRate"] = userRideData?.endedRideCount || 0;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

//#endregion Incentive Setup

//#region ThirdParty API Authorization
adminAction.getThirdPartyAPIAuthorization = async function (context) {
  let access_token = null,
    token_type = "",
    expires_in = 0,
    errorCode = 200,
    responseJson = {};
  const gatewayName = context.params.gatewayName;
  try {
    const secretKey = context.params?.secretKey || "";
    const generalConfigList = await storageUtil.read(
      constantUtil.GENERALSETTING
    );
    // //------------
    if (secretKey) {
      responseJson = generalConfigList;
      //   if (generalConfigList.length > 0) {
      //     Object.values(generalConfigList)
      //       .filter(
      //         (each) =>
      //           each.name === constantUtil.GENERALSETTING &&
      //           each.data.status === constantUtil.ACTIVE &&
      //           each.data.secretKey === secretKey
      //       )
      //       .forEach((item) => {
      //         responseJson = item;
      //       });
      //   }
      // } else {
      //   if (generalConfigList.length > 0) {
      //     Object.values(generalConfigList)
      //       .filter(
      //         (each) =>
      //           each.name === constantUtil.GENERALSETTING &&
      //           each.dataType === constantUtil.BASEDATA &&
      //           each.data.status === constantUtil.ACTIVE
      //       )
      //       .forEach((item) => {
      //         responseJson = item;
      //       });
      //   }
    }
    access_token = responseJson?.data?.firebaseAdmin?.androidUser || "";
    expires_in = 3600;
    token_type = "Bearer";
  } catch (e) {
    errorCode = 400;
  }
  return {
    "code": errorCode,
    "access_token": access_token,
    "expires_in": expires_in,
    "token_type": token_type,
  };
};
//#endregion ThirdParty API Authorization

// #region Check Unique Referral Code
adminAction.checkUniqueReferralCode = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;
  const clientId = context.params?.clientId || context.meta?.clientId;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    await notifyMessage.setNotifyLanguage(languageCode);

  try {
    if (userType === constantUtil.USER) {
      responseData = await this.broker.emit("user.checkUniqueReferralCode", {
        ...context.params,
        "clientId": clientId,
      });
      //{ "isValidUniqueCode": false };
    } else if (userType === constantUtil.PROFESSIONAL) {
      responseData = await this.broker.emit(
        "professional.checkUniqueReferralCode",
        {
          ...context.params,
          "clientId": clientId,
        }
      );
      // { "isValidUniqueCode": false };
    }
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
// #endregion Check Unique Referral Code

//------------------------------------
module.exports = adminAction;
