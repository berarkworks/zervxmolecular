const { customAlphabet } = require("nanoid");
const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const _ = require("lodash");
const QRCode = require("qrcode");
const CircularJSON = require("circular-json");

const cryptoUtils = require("../utils/crypto.util");
const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const {
  userObject,
  userObjectForAdmin,
  otpMailObject,
  userWelcomeMailObject,
} = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const {
  convertToFloat,
  convertToInt,
  trimPrefixZero,
} = require("../utils/common.util");

const userAction = {};
const otpLength = 6;

// userAction.validateLoginUser = async function (context) {
//   let generalSettings = null,
//     clientId = null,
//     userData = null;
//   const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
//   const clientIp = context.meta?.clientIp;
//   const secretKey = context.params.secretKey?.trim();

//   const {
//     INVALID_USER_DATA,
//     INFO_OTP_MESSAGE,
//     INFO_IP_BLOCK,
//     ERROR_GENEREL_SETTINGS,
//     INFO_SUCCESS,
//   } = notifyMessage.setNotifyLanguage(context.params.langCode);

//   // #region IP Validation
//   let isValidIP = false,
//     isValidatedPhoneNo = false,
//     message = "";
//   //
//   const WINDOW_DURATION_IN_HOURS = 1; // Hour
//   const MAX_WINDOW_REQUEST_COUNT = 10; // 5; // Max Allowed request Count
//   const WINDOW_LOG_DURATION_IN_HOURS = 1;

//   const currentTime = Date.now();
//   const phoneCode = context.params.phoneCode.trim();
//   const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());
//   if (osType === constantUtil.CONST_ANDROID_OS) {
//     if (context.params.userName) {
//       try {
//         const decodedPhoneNo = cryptoUtils.cryptoAESDecode(
//           context.params.userName
//         );
//         if (decodedPhoneNo === phoneCode + phoneNumber) {
//           isValidatedPhoneNo = true;
//         } else {
//           isValidatedPhoneNo = false;
//         }
//       } catch (e) {
//         isValidatedPhoneNo = false;
//       }
//     } else {
//       isValidatedPhoneNo = true;
//     }
//   } else {
//     isValidatedPhoneNo = true;
//   }
//   // For Initial App Loading
//   const generalConfigList = await storageUtil.read(constantUtil.GENERALSETTING);
//   // if (!clientId) {
//   if (secretKey && generalConfigList.length > 0) {
//     Object.values(generalConfigList)
//       .filter(
//         (each) =>
//           each.name === constantUtil.GENERALSETTING &&
//           // each.dataType === constantUtil.BASEDATA &&
//           // each.data.status === constantUtil.ACTIVE
//           each.data.secretKey === secretKey
//       )
//       .forEach((item) => {
//         generalSettings = item;
//       });
//     // Client Id
//     clientId = generalSettings.data.clientId;
//   } else {
//     return {
//       "code": 600,
//       "message": "Invalid App SecretKey.",
//       "data": {},
//     };
//   }
//   if (generalSettings.data.isEnableIPRestriction && clientIp) {
//     // if (clientIp) {
//     try {
//       let recordData = await storageUtil.read(clientIp);
//       //When there is no user record then a new record is created for the user and stored in the Redis storage
//       if (recordData === null) {
//         const newRecord = [];
//         const requestLog = {
//           "requestTimeStamp": currentTime,
//           "userType": constantUtil.USER,
//           "requestCount": 1,
//         };
//         newRecord.push(requestLog);
//         await storageUtil.write(clientIp, newRecord);
//         recordData = newRecord;
//       }
//       //When the record is found then its value is parsed and the number of requests the user has made within the last window is calculated
//       const windowBeginTimestamp = new Date()
//         .setHours(new Date().getHours() - WINDOW_DURATION_IN_HOURS)
//         .valueOf();
//       const newRecordData = recordData?.filter((entry) => {
//         return entry.requestTimeStamp > windowBeginTimestamp;
//       });
//       const requestsinWindow = recordData?.filter((entry) => {
//         return (
//           entry.requestTimeStamp > windowBeginTimestamp &&
//           entry.userType === constantUtil.USER
//         );
//       });
//       // console.log("requestsinWindow", requestsinWindow);
//       const totalWindowRequestsCount = requestsinWindow?.reduce(
//         (accumulator, entry) => {
//           return accumulator + entry.requestCount;
//         },
//         0
//       );
//       //if maximum number of requests is exceeded then an error is returned
//       if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
//         isValidIP = false;
//         message = INFO_IP_BLOCK;
//         // "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
//       } else {
//         isValidIP = true;
//         //When the number of requests made are less than the maximum the a new entry is logged
//         const lastRequestLog = recordData[recordData?.length - 1];

//         const potentialCurrentWindowIntervalStartTimeStamp = new Date()
//           .setHours(new Date().getHours() - WINDOW_LOG_DURATION_IN_HOURS)
//           .valueOf();
//         //When the interval has not passed from the last request, then the counter increments
//         if (
//           lastRequestLog.requestTimeStamp ===
//           potentialCurrentWindowIntervalStartTimeStamp
//         ) {
//           lastRequestLog.requestCount++;
//           recordData[recordData.length - 1] = lastRequestLog;
//         } else {
//           //When the interval has passed, a new entry for current user and timestamp is logged
//           newRecordData.push({
//             "requestTimeStamp": currentTime,
//             "userType": constantUtil.USER,
//             "requestCount": 1,
//           });
//           recordData = newRecordData;
//         }
//         await storageUtil.write(clientIp, recordData);
//       }
//     } catch (error) {
//       isValidIP = false;
//       message = INVALID_USER_DATA;
//     }
//   } else {
//     isValidIP = true;
//   }
//   // #endregion IP Validation
//   if (
//     isValidIP &&
//     isValidatedPhoneNo &&
//     (osType === constantUtil.CONST_ANDROID_OS ||
//       osType === constantUtil.CONST_IOS ||
//       osType === constantUtil.CONST_WEB_OS)
//   ) {
//     userData = await this.adapter.model
//       .findOne(
//         {
//           "phone.code": phoneCode,
//           "phone.number": phoneNumber,
//           "clientId": mongoose.Types.ObjectId(clientId),
//         },
//         { "_id": 1, "clientId": 1 }
//       )
//       .lean();
//     if (userData) {
//       // if (userData?.clientId) {
//       return {
//         "code": 200,
//         "message": INFO_SUCCESS,
//         "data": {}, // "data": { "clientId": userData?.clientId?.toString() },
//       };
//       // } else {
//       //   //Update & Get Client Data
//       //   userData = await this.adapter.model
//       //     .findOneAndUpdate(
//       //       { "_id": mongoose.Types.ObjectId(userData?._id?.toString()) },
//       //       { "$set": { "clientId": clientId } },
//       //       { "new": true, "fields": { "_id": 1, "clientId": 1 } }
//       //     )
//       //     .lean();
//       //   return {
//       //     "code": 200,
//       //     "message": INFO_SUCCESS,
//       //     "data": {}, // "data": { "clientId": clientId },
//       //   };
//       // }
//     } else {
//       userData = await this.adapter.model
//         .findOne(
//           {
//             "phone.code": phoneCode,
//             "phone.number": phoneNumber,
//             // "clientId": { "$ne": [null, "", undefined] },
//             // "clientId": { $exists: false },
//             "$and": [{ "clientId": { "$exists": false }, "clientId": null }],
//           },
//           { "_id": 1, "clientId": 1 }
//         )
//         .lean();
//       if (userData) {
//         //Update & Get Client Data
//         userData = await this.adapter.model
//           .findOneAndUpdate(
//             { "_id": mongoose.Types.ObjectId(userData?._id?.toString()) },
//             { "$set": { "clientId": clientId } },
//             { "new": true, "fields": { "_id": 1, "clientId": 1 } }
//           )
//           .lean();
//         // return {
//         //   "code": 200,
//         //   "message": INFO_SUCCESS,
//         //   "data": {}, // "data": { "clientId": clientId },
//         // };
//       }
//       return {
//         "code": 200,
//         "message": INFO_SUCCESS,
//         "data": {},
//       };
//     }
//   } else {
//     return {
//       "code": 600,
//       "message": message,
//       "data": {},
//     };
//   }
// };

userAction.login = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
  const clientIp = context.meta?.clientIp;
  const clientId = context.params?.clientId || context.meta?.clientId;
  context.params.otp = customAlphabet("1234567890", otpLength)();
  const emailOTP = customAlphabet("1234567890", otpLength)();

  const { INVALID_USER_DATA, INFO_OTP_MESSAGE, INFO_IP_BLOCK } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  // #region IP Validation
  let isValidIP = false,
    isValidatedPhoneNo = false,
    message = "";
  //
  const WINDOW_DURATION_IN_HOURS = 1; // Hour
  const MAX_WINDOW_REQUEST_COUNT = 5; // Max Allowed request Count
  const WINDOW_LOG_DURATION_IN_HOURS = 1;

  const currentTime = Date.now();
  const phoneCode = context.params.phoneCode.trim();
  const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());
  if (osType === constantUtil.CONST_ANDROID_OS) {
    if (context.params.userName) {
      try {
        const decodedPhoneNo = cryptoUtils.cryptoAESDecode(
          context.params.userName
        );
        if (decodedPhoneNo === phoneCode + phoneNumber) {
          isValidatedPhoneNo = true;
        } else {
          isValidatedPhoneNo = false;
        }
      } catch (e) {
        isValidatedPhoneNo = false;
      }
    } else {
      isValidatedPhoneNo = true;
    }
  } else {
    isValidatedPhoneNo = true;
  }
  if (generalSettings.data.isEnableIPRestriction && clientIp) {
    try {
      let recordData = await storageUtil.read(clientIp);
      //When there is no user record then a new record is created for the user and stored in the Redis storage
      if (recordData === null) {
        const newRecord = [];
        const requestLog = {
          "requestTimeStamp": currentTime,
          "userType": constantUtil.USER,
          "requestCount": 1,
        };
        newRecord.push(requestLog);
        await storageUtil.write(clientIp, newRecord);
        recordData = newRecord;
      }
      //When the record is found then its value is parsed and the number of requests the user has made within the last window is calculated
      const windowBeginTimestamp = new Date()
        .setHours(new Date().getHours() - WINDOW_DURATION_IN_HOURS)
        .valueOf();
      const newRecordData = recordData?.filter((entry) => {
        return entry.requestTimeStamp > windowBeginTimestamp;
      });
      const requestsinWindow = recordData?.filter((entry) => {
        return (
          entry.requestTimeStamp > windowBeginTimestamp &&
          entry.userType === constantUtil.USER
        );
      });
      // console.log("requestsinWindow", requestsinWindow);
      const totalWindowRequestsCount = requestsinWindow?.reduce(
        (accumulator, entry) => {
          return accumulator + entry.requestCount;
        },
        0
      );
      //if maximum number of requests is exceeded then an error is returned
      if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
        isValidIP = false;
        message = INFO_IP_BLOCK;
        // "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
      } else {
        isValidIP = true;
        //When the number of requests made are less than the maximum the a new entry is logged
        const lastRequestLog = recordData[recordData?.length - 1];

        const potentialCurrentWindowIntervalStartTimeStamp = new Date()
          .setHours(new Date().getHours() - WINDOW_LOG_DURATION_IN_HOURS)
          .valueOf();
        //When the interval has not passed from the last request, then the counter increments
        if (
          lastRequestLog.requestTimeStamp ===
          potentialCurrentWindowIntervalStartTimeStamp
        ) {
          lastRequestLog.requestCount++;
          recordData[recordData.length - 1] = lastRequestLog;
        } else {
          //When the interval has passed, a new entry for current user and timestamp is logged
          newRecordData.push({
            "requestTimeStamp": currentTime,
            "userType": constantUtil.USER,
            "requestCount": 1,
          });
          recordData = newRecordData;
        }
        await storageUtil.write(clientIp, recordData);
      }
    } catch (error) {
      isValidIP = false;
      message = INVALID_USER_DATA;
    }
  } else {
    isValidIP = true;
  }
  // #endregion IP Validation

  if (
    isValidIP &&
    isValidatedPhoneNo &&
    (osType === constantUtil.CONST_ANDROID_OS ||
      osType === constantUtil.CONST_IOS ||
      osType === constantUtil.CONST_WEB_OS)
  ) {
    let isEmailVerified = false;
    let userData = await this.adapter.model.countDocuments({
      "phone.code": context.params.phoneCode.trim(),
      "phone.number": phoneNumber,
    });

    if (userData) {
      userData = await this.adapter.model.findOneAndUpdate(
        {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "phone.code": phoneCode,
          "phone.number": phoneNumber,
        }, // No need to Add Status Based Condition Will handle in APP
        {
          "otp": cryptoUtils.hashSecret(context.params.otp),
          "emailOTP": cryptoUtils.hashSecret(emailOTP),
          "ipAddress": clientIp,
        },
        { "new": true }
      );
      isEmailVerified = userData.isEmailVerified || false;
    } else {
      const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
        "quality": 50,
        "width": 500,
      });
      let finalImageData = await this.broker.emit(
        "admin.uploadStaticMapImageInSpaces",
        {
          "files": qrImage,
          "fileName": `professional${phoneCode + "" + phoneNumber}QrImage`,
        }
      );
      finalImageData = finalImageData && finalImageData[0];

      userData = await this.adapter.model.create({
        "clientId": clientId,
        "isNewUser": true,
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "nationalIdNo": context.params.nationalIdNo,
        "dob": context.params.dob,
        "otp": cryptoUtils.hashSecret(context.params.otp),
        "emailOTP": cryptoUtils.hashSecret(emailOTP),
        "password": cryptoUtils.passwordEncode(context.params.password),
        "scanAndPayQR": finalImageData,
        "defaultPayment": generalSettings.data.defaultPayment,
        "ipAddress": clientIp,
        "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
        "lastCurrencyCode": generalSettings.data.currencyCode.toUpperCase(),
        "firstName":
          context.params.bookingFrom === constantUtil.CONST_WEBAPP
            ? "new"
            : null,
        "lastName":
          context.params.bookingFrom === constantUtil.CONST_WEBAPP
            ? "user"
            : null,
        "email":
          context.params.bookingFrom === constantUtil.CONST_WEBAPP
            ? Date.now().toString() + "@email.com"
            : null,
        "isEmailVerified": context.params.isEmailVerified || false,
        "status":
          context.params.bookingFrom === constantUtil.CONST_WEBAPP
            ? constantUtil.ACTIVE
            : constantUtil.INCOMPLETE,
        "createdFrom":
          context.params.bookingFrom === constantUtil.CONST_WEBAPP
            ? constantUtil.CONST_WEBAPP
            : context.params?.createdFrom || constantUtil.CONST_APP,
      });
      // isEmailVerified = true;
    }
    if (!userData) {
      throw new MoleculerError(INVALID_USER_DATA);
    }
    // send mail
    if (
      generalSettings.data.emailConfigurationEnable &&
      userData.email &&
      isEmailVerified
    ) {
      await this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "OtpForVerification",
        ...otpMailObject({
          ...userData.toJSON(),
          "otp": context.params.otp,
          "emailOTP": emailOTP,
        }),
      });
    }
    // send sms
    if (generalSettings.data.smsConfigurationEnable) {
      let otpNotifyMessage = smsSetting.data.notifyMessage || INFO_OTP_MESSAGE;
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{siteTitle}}/g,
        generalSettings.data.siteTitle
      );
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{OTP}}/g,
        context.params.otp
      );
      await this.broker.emit("admin.sendSMS", {
        "clientId": clientId,
        "phoneNumber": `${phoneCode}${phoneNumber}`,
        // "message": context.params.otp,
        "message": otpNotifyMessage,
        "notifyType": constantUtil.OTP,
        "otpReceiverAppName": constantUtil.SMS,
      });
    }
    return {
      "code": 200,
      "message": "OTP sended successfully",
      "OTP":
        smsSetting.data.mode === constantUtil.DEVELOPMENT
          ? context.params.otp
          : "",
      "otpLength": otpLength,
      "isOtpByPass": userData.isOtpBypass,
      "mode": smsSetting.data.mode,
      "smsType":
        smsSetting.data.mode === constantUtil.PRODUCTION
          ? smsSetting.data.smsType
          : "",
    };
  } else {
    context.params.otp = customAlphabet("1234567890", otpLength)(); //For Invalid OTP Purpose
    return {
      "code": 400,
      "message": message,
      "OTP":
        smsSetting.data.mode === constantUtil.DEVELOPMENT
          ? context.params.otp
          : "",
      "otpLength": otpLength,
      "isOtpByPass": false,
      "mode": smsSetting.data.mode,
      "smsType":
        smsSetting.data.mode === constantUtil.PRODUCTION
          ? smsSetting.data.smsType
          : "",
    };
  }
};

userAction.verifyOtp = async function (context) {
  // const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const clientId = context.params.clientId || context.meta?.clientId;
  const { INVALID_USER_DATA } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.userDetails?.languageCode
  );
  let checkData = {};
  let smsType = (smsSetting?.data?.smsType || "").toUpperCase();
  const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());

  if (smsSetting?.data?.isEnableSandboxMode) {
    const sandboxPhoneCode = smsSetting?.data?.sandboxPhone?.code || "";
    const sandboxPhoneNumber = smsSetting?.data?.sandboxPhone?.number || "";
    if (
      sandboxPhoneCode === context.params.phoneCode &&
      sandboxPhoneNumber === phoneNumber
    ) {
      smsType = constantUtil.SANDBOX;
    }
  }
  switch (smsType) {
    case constantUtil.TWILIO:
    case constantUtil.SENDIFY:
    case constantUtil.TUNISIESMS:
      checkData = {
        "phone.code": context.params.phoneCode,
        "phone.number": phoneNumber,
        "otp": cryptoUtils.hashSecret(context.params.otp),
        //"clientId": clientId,
      };
      break;

    // case constantUtil.SANDBOX:
    //   checkData = {
    //     "phone.code": context.params.phoneCode,
    //     "phone.number": context.params.phoneNumber,
    //   };
    //   break;

    default:
      checkData = {
        "phone.code": context.params.phoneCode,
        "phone.number": phoneNumber,
        //"clientId": clientId,
      };
      break;
  }
  const userData = await this.adapter.model.findOne(checkData).lean();
  if (!userData) {
    throw new MoleculerError(INVALID_USER_DATA, "401", "AUTH_FAILED");
  }
  const accessToken = cryptoUtils.jwtSign({
    "_id": userData._id,
    "type": constantUtil.USER,
    "deviceType": context.meta.userAgent.deviceType,
    "platform": context.meta.userAgent.platform,
  });

  const updatedUserData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(userData._id.toString()),
        //"clientId": clientId,
      },
      {
        "otp": "",
        "deviceInfo": [
          {
            "accessToken": accessToken,
            "deviceId": context.meta.userAgent.deviceId,
            "deviceType": context.meta.userAgent.deviceType,
            "platform": context.meta.userAgent.platform,
          },
        ],
        "status":
          // userData.email && userData.firstName && userData.lastName
          userData.firstName && userData.lastName
            ? userData.status
            : constantUtil.INCOMPLETE,
      },
      { "new": true }
    )
    .lean();
  if (!updatedUserData) {
    throw new MoleculerError(INVALID_USER_DATA);
  }
  // updatedUserData.newUser =
  //   updatedUserData.email &&
  //   updatedUserData.firstName &&
  //   updatedUserData.lastName
  //     ? false
  //     : true;
  updatedUserData.newUser =
    updatedUserData.firstName && updatedUserData.lastName ? false : true;
  updatedUserData.accessToken = accessToken;
  const userObjectData = await userObject(updatedUserData);
  return { "code": 200, "data": userObjectData };
};

userAction.verifyEmail = async function (context) {
  let userData = null,
    checkConditionData = {},
    updateFieldData = {},
    responseData,
    errorCode = 200,
    responseMessage = "";
  const action = context.params.action,
    userId = context.params.userId,
    clientId = context.params.clientId || context.meta?.clientId,
    emailOTP = customAlphabet("1234567890", otpLength)();
  //
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    INVALID_USER_DATA,
    OTP_INCORRECT,
    SOMETHING_WENT_WRONG,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.userDetails?.languageCode
  );
  try {
    if (action === constantUtil.OTP) {
      const checkEmailCount = await this.adapter.model.countDocuments({
        "_id": {
          "$ne": mongoose.Types.ObjectId(userId),
        },
        "email": context.params.email,
      });
      if (checkEmailCount > 0) {
        throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
      }
      checkConditionData = {
        "_id": mongoose.Types.ObjectId(userId),
      };
      updateFieldData = {
        "emailOTP": cryptoUtils.hashSecret(emailOTP),
        "email": context.params.email,
        "isEmailVerified": false,
      };
    } else if (action === constantUtil.CONST_VERIFY) {
      checkConditionData = {
        "_id": mongoose.Types.ObjectId(userId),
        "emailOTP": cryptoUtils.hashSecret(context.params.otp),
      };
      updateFieldData = {
        "isEmailVerified": true,
      };
    }
    userData = await this.adapter.model.findOne(checkConditionData).lean();
    if (!userData) {
      throw new MoleculerError(
        action === constantUtil.OTP ? INVALID_USER_DATA : OTP_INCORRECT,
        400
      );
    } else {
      userData = await this.adapter.model
        .findOneAndUpdate(
          { ...checkConditionData },
          { ...updateFieldData },
          { "new": true }
        )
        .lean();
    }
    // if (!userData) {
    //   throw new MoleculerError(INVALID_USER_DATA);
    // }
    // send mail
    if (
      generalSettings.data.emailConfigurationEnable &&
      action === constantUtil.OTP &&
      userData.email
    ) {
      await this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "emailVerification",
        ...otpMailObject({
          ...userData,
          "otp": "****" + emailOTP.substring(4, otpLength),
          "emailOTP": emailOTP,
        }),
      });
    }
    if (action === constantUtil.OTP) {
      responseData = { "OTP": emailOTP, "otpLength": otpLength };
    } else if (action === constantUtil.CONST_VERIFY) {
      responseData = await userObject(userData);
    }
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : INVALID_USER_DATA;
    responseData = {};
  }
  // return { "code": 200, "data": responseData };
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

userAction.profile = async function (context) {
  const userId = context.params?.userId || context.meta.userId;
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(userId.toString()))
    .lean();
  const userObjectData = await userObject(userData);
  return {
    "code": 200,
    "data": userObjectData,
  };
};

userAction.updateProfile = async function (context) {
  const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId = context.params.clientId || context.meta?.clientId;
  const userId = context.params.userId || context.meta.userId.toString();
  let userCheckData,
    referredByData = {},
    avgRatingData = {};

  //------------ Notification Message Start ---------
  const languageCode = context.meta?.userDetails?.languageCode;
  const {
    INFO_USER,
    INVALID_USER_DATA,
    INFO_INVALID_REFERRAL_CODE,
    INFO_INVALID_DETAILS,
    INFO_ALREADY_EXISTS,
    ALERT_CHECK_CPF,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------

  const referredBy = context.params.referredBy || "";
  if (referredBy) {
    userCheckData = await this.adapter.model
      .findOne(
        {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "uniqueCode": referredBy.toLowerCase(),
        },
        { "_id": 1, "uniqueCode": 1 }
      )
      .collation({ "locale": "en", "caseLevel": true });
    if (!userCheckData) {
      throw new MoleculerError(INFO_INVALID_REFERRAL_CODE);
    }
    referredByData = {
      "referredBy": referredBy.toLowerCase(),
    };
  }
  const checkEmailCount = await this.adapter.model.countDocuments({
    "_id": {
      "$ne": mongoose.Types.ObjectId(userId),
    },
    "email": context.params.email,
  });
  if (checkEmailCount > 0) {
    throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
  }
  const jsonData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(userId))
    .lean();
  if (!jsonData) {
    throw new MoleculerError(INVALID_USER_DATA);
  }
  if (context.params?.nationalIdNo) {
    const checkCPFNO = await this.adapter.model
      .find(
        {
          "_id": {
            "$ne": mongoose.Types.ObjectId(userId),
          },
          "nationalIdNo": context.params.nationalIdNo,
        },
        { "_id": 1, "nationalIdNo": 1, "phone": 1 }
      )
      .lean();
    // if (
    //   checkCPFNO.length > 1 ||
    //   (checkCPFNO.length === 1 &&
    //     checkCPFNO?.[0]?._id?.toString() !== context.meta.userId.toString())
    // )
    if (checkCPFNO.length > 0) {
      throw new MoleculerError(
        INFO_USER + " " + INFO_ALREADY_EXISTS + ". " + ALERT_CHECK_CPF
      );
    }
  }
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "clientId": clientId,
      "files": context.params.files,
      "fileName": `${jsonData.phone.number}${constantUtil.USER}`,
      "oldFileName": `${jsonData.avatar}`,
    });
    context.params.avatar = s3Image && s3Image[0];
  }
  // const dynamicUniqueCode = `${context.params.firstName.slice(
  //   0,
  //   4
  // )}${helperUtil.randomString(6, "a")}`;
  const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
    context.params.firstName,
    clientId
  );
  if (!!context.params.uniqueCode === false) {
    context.params.uniqueCode =
      !!jsonData.uniqueCode === true
        ? jsonData.uniqueCode
        : dynamicUniqueCode.toLowerCase();
  }
  if ((jsonData?.review?.avgRating ?? 0) === 0) {
    avgRatingData = {
      "review.avgRating": 5,
    };
  }
  // count
  const updatedUserData = await this.adapter.model.findOneAndUpdate(
    { "_id": mongoose.Types.ObjectId(userId) },
    {
      "isNewUser": false,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "email": context.params.email,
      "isEmailVerified": context.params.isEmailVerified || false,
      "nationalIdNo": context.params.nationalIdNo,
      "dob": context.params.dob,
      "gender": context.params.gender,
      "avatar": context.params.avatar,
      "uniqueCode": context.params?.uniqueCode || null,
      // "referredBy": (context.params?.referredBy || "").toUpperCase(),
      // "review.avgRating": 5,
      "status": context.params.status || constantUtil.ACTIVE,
      "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
      "lastCurrencyCode": generalSettings.data.currencyCode.toUpperCase(),
      "referralDetails": {
        "rideCount": inviteSettings.data.forUser.rideCount,
        "inviteAmount": inviteSettings.data.forUser.amountToInviter,
        "joinerAmount": inviteSettings.data.forUser.amountToJoiner,
      },
      ...avgRatingData,
      ...referredByData,
    },
    { "new": true }
  );
  if (!updatedUserData) {
    throw new MoleculerError(INFO_INVALID_DETAILS);
  }
  //----------- invite and earn Start --------------
  if (userCheckData) {
    const inviteAndEarnSettings = await storageUtil.read(
      constantUtil.INVITEANDEARN
    );
    if (inviteAndEarnSettings?.data?.forUser?.rideCount === 0) {
      await this.broker.emit("user.inviteAndEarn", {
        "clientId": clientId,
        "joinerId": userId,
      });
    } else {
      await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(userCheckData?._id?.toString()),
        },
        {
          "$inc": { "invitationCount": 1 },
          // "joinCount": 1,
        }
      );
    }
  }
  //----------- invite and earn End --------------
  // for send Mail
  if (
    generalSettings.data.emailConfigurationEnable &&
    updatedUserData.email &&
    updatedUserData.isEmailVerified &&
    jsonData.isNewUser
  ) {
    await this.broker.emit("admin.sendServiceMail", {
      "clientId": clientId,
      "templateName": "userWelcomeMail",
      ...userWelcomeMailObject(updatedUserData.toJSON()),
    });
  }
  // send sms
  const userObjectData = await userObject(updatedUserData);
  return { "code": 200, "data": userObjectData };
};

userAction.activateAndInactivateAccount = async function (context) {
  const updatedUserData = await this.adapter.model.findOneAndUpdate(
    {
      "_id": mongoose.Types.ObjectId(context.params.userId.toString()),
    },
    {
      "status": context.params.action.toUpperCase(),
      "inActivatedBy":
        context.params.action.toUpperCase() === constantUtil.INACTIVE
          ? constantUtil.USER
          : null,
      "inActivatedAt":
        context.params.action.toUpperCase() === constantUtil.INACTIVE
          ? new Date()
          : null,
    },
    { "new": true }
  );
  const userObjectData = await userObject(updatedUserData);
  return {
    "code": 200,
    "data": userObjectData,
  };
};

userAction.changePhoneNumber = async function (context) {
  const clientId = context?.params?.clientId || context.meta?.clientId;
  const phoneCode = context.params.phoneCode;
  const phoneNumber = context.params.phoneNumber;
  if (context.params.otp) {
    const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
      "quality": 50,
      "width": 500,
    });
    let finalImageData = await this.broker.emit(
      "admin.uploadStaticMapImageInSpaces",
      {
        "files": qrImage,
        "fileName": `user${phoneCode + "" + phoneNumber}QrImage`,
      }
    );
    finalImageData = finalImageData && finalImageData[0];
    const updatedUserData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
        "otp": cryptoUtils.hashSecret(context.params.otp),
      },
      {
        "otp": "",
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "scanAndPayQR": finalImageData,
      }
    );
    if (!updatedUserData?.nModified) {
      throw new MoleculerError("Invalid OTP");
    } else {
      return { "code": 200, "message": "Phone number changed successfully" };
    }
  } else {
    const otp = customAlphabet("1234567890", otpLength)();

    const updatedUserData = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.meta.userId.toString()),
      {
        "otp": cryptoUtils.hashSecret(otp),
      }
    );
    if (!updatedUserData) {
      throw new MoleculerError("Invalid user");
    } else {
      // this.broker.emit('admin.sendSMS', {
      //   'phoneNumber': `${context.params.phoneCode}${context.params.phoneNumber}`,
      //   'message': '',
      // })
      return { "code": 200, "message": "OTP sended successfully", "OTP": otp };
    }
  }
};

userAction.manageAddress = async function (context) {
  const clientId = context?.params?.clientId || context.meta?.clientId;
  switch (context.params.action) {
    case "add": {
      const updateUserData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
          "addressList.addressName": { "$ne": context.params.addressName },
        },
        {
          "$push": {
            "addressList": {
              "placeId": context.params.placeId,
              "addressName": context.params.addressName,
              "addressType": context.params.addressType,
              "fullAddress": context.params.fullAddress,
              "shortAddress": context.params.shortAddress,
              "lat": context.params.lat,
              "lng": context.params.lng,
            },
          },
        }
      );
      if (!updateUserData?.nModified) {
        throw new MoleculerError(
          "Invalid request, Address name already exists"
        );
      }
      const userData = await this.adapter.model
        .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
        .lean();
      if (!userData) {
        throw new MoleculerError("USER NOT FOUND");
      } else {
        return { "code": 200, "data": userData.addressList };
      }
    }
    case "update": {
      if (!context.params.addressId) {
        throw new MoleculerError("Invalid request, Address ID required");
      }
      const userData = await this.adapter.model
        .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
        .lean();
      if (!userData) {
        throw new MoleculerError("USER NOT FOUND");
      }
      const checkNameAlreadyExists = userData.addressList.filter(
        (address) =>
          address._id != context.params.addressId &&
          address.addressName === context.params.addressName
      );
      if (checkNameAlreadyExists.length > 0) {
        throw new MoleculerError(
          "Invalid request, Address name already exists"
        );
      }
      const updateUserData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
          "addressList._id": context.params.addressId,
        },
        {
          "addressList.$.placeId": context.params.placeId,
          "addressList.$.addressName": context.params.addressName,
          "addressList.$.addressType": context.params.addressType,
          "addressList.$.fullAddress": context.params.fullAddress,
          "addressList.$.shortAddress": context.params.shortAddress,
          "addressList.$.lat": context.params.lat,
          "addressList.$.lng": context.params.lng,
        }
      );
      if (!updateUserData?.nModified) {
        throw new MoleculerError(
          "Invalid request, Address name already exists"
        );
      } else {
        userData.addressList = userData.addressList.map((address) => {
          if (address._id.toString() === context.params.addressId) {
            address.placeId = context.params.placeId;
            address.addressName = context.params.addressName;
            address.addressType = context.params.addressType;
            address.fullAddress = context.params.fullAddress;
            address.shortAddress = context.params.shortAddress;
            address.lat = context.params.lat;
            address.lng = context.params.lng;
          }
          return address;
        });
        return { "code": 200, "data": userData.addressList };
      }
    }
    case "remove": {
      if (!context.params.addressId) {
        throw new MoleculerError("Invalid request, Address ID required");
      }
      const updateUserData = await this.adapter.updateById(
        mongoose.Types.ObjectId(context.meta.userId.toString()),
        {
          "$pull": {
            "addressList": { "_id": context.params.addressId },
          },
        }
      );
      if (!updateUserData) {
        throw new MoleculerError(
          "Invalid request, Address name already exists"
        );
      } else {
        return { "code": 200, "data": updateUserData.addressList };
      }
    }
    default: {
      throw new MoleculerError("Invalid Request");
    }
  }
};

userAction.manageCard = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  // trim card very essential
  context.params.cardNumber = context.params.cardNumber.replace(/ /g, "");
  //
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const currencyCode = context.params.currencyCode
    ? context.params.currencyCode.toUpperCase()
    : gerenalsetting.data.currencyCode.toUpperCase();
  //------------ Notification Message Start ---------
  const languageCode = context.meta?.userDetails?.languageCode;
  const {
    USER_NOT_FOUND,
    INFO_INVALID_DETAILS,
    INFO_CARD_ALREADY_EXIST,
    WARNING_CARD_VALIDATION_FAILED,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  switch (context.params.action) {
    case "add":
      {
        let userData = await this.adapter.model.findOne({
          "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
        });
        if (!userData) {
          throw new MoleculerError(USER_NOT_FOUND, 500);
        }
        const cardAlreadyExist = userData.cards.find(
          (card) => card.cardNumber === context.params.cardNumber.trim()
        );
        if (cardAlreadyExist?.isVerified === true) {
          throw new MoleculerError(INFO_CARD_ALREADY_EXIST, 500);
        }
        let validCard = await this.broker.emit("transaction.checkCardVaild", {
          "clientId": clientId,
          "cardNumber": context.params.cardNumber,
          "expiryYear": context.params.expiryYear,
          "expiryMonth": context.params.expiryMonth,
          "cvv": context.params.cvv,
          "type": context.params.type,
          "embedtoken": context.params.embedtoken,
          "userId": userData._id,
          "userType": constantUtil.USER,
          "pin": context.params.pin,
          "holderName": context.params.holderName,
          "paymentGateWayCustomerId": context.params.paymentGateWayCustomerId,
          "transactionId": context.params.transactionId,
          "transactionAmount": context.params.transactionAmount,
        });
        validCard = validCard && validCard[0];
        if (!validCard) {
          throw new MoleculerError(WARNING_CARD_VALIDATION_FAILED, 500);
        }
        if (validCard.code > 200) {
          //it means reuturn is error or more than 400
          return validCard;
        } else if (
          validCard.code === 200 &&
          validCard.data.authType === "NOAUTH"
        ) {
          // //Get Card Details
          // const updateDefaultCardStatus = await this.broker.emit(
          //   "user.updateDefaultCardStatusInActiveActive",
          //   {
          //     "userId": context.meta.userId.toString(),
          //     "cardId": null,
          //   }
          // );
          userData = await this.adapter.model.findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
            },
            {
              "$push": {
                "cards": {
                  "type": validCard.data?.type ?? context.params.type,
                  "cardId": context.params.cardId || validCard?.data?.cardId,
                  "cardNumber": context.params.cardNumber,
                  "expiryYear": context.params.expiryYear,
                  "expiryMonth": context.params.expiryMonth,
                  "holderName": context.params.holderName,
                  // token refs
                  "stripeCardToken": validCard.data.cardToken || undefined,
                  "embedtoken":
                    context.params.embedtoken ||
                    validCard.data?.embedtoken ||
                    undefined,
                  "isVerified": true,
                  "isDefault":
                    userData.defaultPayment !== constantUtil.CARD
                      ? false
                      : validCard.data.isDefault || false,
                  "isCVVRequired": validCard.data.isCVVRequired || false,
                  "securityCode": context.params.securityCode,
                },
              },
              // "stripeCustomerId": validCard.data.stripeCustomerId || null,
              "paymentGateWayCustomerId":
                validCard.data.paymentGateWayCustomerId || null,
            },
            { "new": true }
          );

          if (!userData) {
            throw new MoleculerError(
              "CAN'T UPDATE USER DATA IN MANAGECARD API",
              500
            );
          }
          //----------------------------
          let gatewayResponse = {};
          let params = {
            //"clientId": clientId,
            "transactionId": validCard.data.transactionId,
            "transactionAmount": validCard.data.transactionAmount,
            "paymentData": validCard.data.paymentData,
          };
          //
          switch (validCard.data.paymentGateWay) {
            case constantUtil.STRIPE:
              {
                // params["stripeCardToken"] = validCard.data.stripeCardToken;
                // params["stripeCustomerId"] =
                //   validCard.data.paymentGateWayCustomerId;
                //----------------------------
                gatewayResponse = await this.broker.emit(
                  "transaction.stripe.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;

            case constantUtil.RAZORPAY:
              {
                params["paymentGateWayCustomerId"] =
                  validCard.data.paymentGateWayCustomerId;
                //----------------------------
                gatewayResponse = await this.broker.emit(
                  "transaction.razorpay.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;

            case constantUtil.PEACH:
              {
                gatewayResponse = await this.broker.emit(
                  "transaction.peach.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;
          }
          if (!gatewayResponse) {
            throw new MoleculerError("Error Occured while Refund Amount", 500);
          }
          validCard.data.card = null;
          validCard.data.cvv = null;
          validCard.data.cardToken = null;

          // fluuterwave
          validCard.data.embedtoken = undefined;
          validCard.data.type = undefined;
        }
        return {
          "code": 200,
          "data": {
            ...validCard.data,
            "card": context.params,
          },
          "message": validCard.message,
        };
      }
      break;

    case "update":
      {
        if (!context.params.cardId) {
          throw new MoleculerError("Invalid request, Card ID required", 500);
        }
        const userData = await this.adapter.model
          .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
          .lean();
        const checkCardNumberAlreadyExists = userData.cards.find(
          (card) =>
            card?._id?.toString() !== context.params.cardId?.toString() &&
            card.cardNumber === context.params.cardNumber
        );
        if (checkCardNumberAlreadyExists) {
          throw new MoleculerError(INFO_CARD_ALREADY_EXIST, 500);
        }
        //  need to change => change the default card logic
        const updatedCardDetails = userData.cards.map((card) => {
          if (card._id.toString() === context.params.cardId.toString()) {
            card.isDefault = true;
          } else {
            card.isDefault = false;
          }
          return card;
        });

        const updateData = {
          "cards": updatedCardDetails,
          "defaultPayment": constantUtil.PAYMENTCARD,
        };

        const updateUserData = await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
          },
          updateData
        );
        if (!updateUserData?.nModified) {
          throw new MoleculerError(INFO_CARD_ALREADY_EXIST, 500);
        } else {
          return { "code": 200, "data": updatedCardDetails };
        }
      }
      break;

    case "remove":
      {
        if (!context.params.cardId) {
          throw new MoleculerError("Invalid request, Card ID required", 500);
        }
        // const updateUserData = await this.adapter.updateById(
        //   mongoose.Types.ObjectId(context.meta.userId.toString()),
        //   {
        //     "$pull": {
        //       "cards": { "_id": context.params.cardId, "isDefault": false },
        //     },
        //   }
        // );
        const updateUserData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
            },
            {
              "$pull": {
                "cards": { "_id": context.params.cardId, "isDefault": false },
              },
            },
            { "new": true }
          )
          .lean();
        if (!updateUserData) {
          throw new MoleculerError(
            "Invalid request, Address name already exists"
          );
        } else {
          return { "code": 200, "data": updateUserData.cards };
        }
      }
      break;

    default:
      {
        throw new MoleculerError("Invalid Request", 500);
      }
      break;
  }
};

userAction.setDefaultPaymentOption = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = await this.broker.emit("user.getById", {
    "id": context.meta.userId.toString(),
    "clientId": clientId,
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError("USER NOT FOUND");
  }
  const cards = await userData.cards.map((card) => {
    card.isDefault = false;
    return card;
  });
  const updatedUserData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.userId.toString()),
    {
      "defaultPayment": context.params.defaultPayment,
      "cards": cards,
    }
  );
  if (!updatedUserData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  // need to chage => this is temprory fix
  if (context.params.defaultPayment !== constantUtil.PAYMENTCARD) {
    await this.broker.emit("user.updateDefaultCardStatusInActiveActive", {
      "userId": userData._id.toString(),
      "clientId": clientId,
    });
  }
  return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
};

userAction.logout = async function (context) {
  const updatedUserData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.userId.toString()),
    {
      "deviceInfo": [],
    }
  );
  if (!updatedUserData) {
    throw new MoleculerError("User not found", "401", "AUTH_FAILED");
  } else {
    return { "code": 200, "message": "Device logout successfully" };
  }
};

userAction.updateLiveLocation = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.userId.toString()),
    {
      "location.coordinates": [context.params.lng, context.params.lat],
      "currentBearing": context.params.currentBearing,
      "altitude": context.params.altitude,
      "horizontalAccuracy": context.params.horizontalAccuracy,
      "verticalAccuracy": context.params.verticalAccuracy,
      "speed": context.params.speed,
      "isLocationUpdated": true,
      "locationUpdatedTime": new Date(),
    }
  );
  if (!updateData) {
    return { "code": 200, "data": {}, "message": "FAILED TO UPDATE LOCATION" };
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "LOCATION UPDATED SUCCESSFULLY",
    };
  }
};

userAction.trackUserLocation = async function (context) {
  const jsonData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.userId.toString()))
    .lean();
  if (!jsonData) {
    throw new MoleculerError("ERROR IN LOCATION DETAILS");
  } else {
    const locationData = {
      "lat": jsonData.location
        ? jsonData.location.coordinates[1]
          ? jsonData.location.coordinates[1]
          : 0
        : 0,
      "lng": jsonData.location
        ? jsonData.location.coordinates[0]
          ? jsonData.location.coordinates[0]
          : 0
        : 0,
      "currentBearing": jsonData.currentBearing
        ? jsonData.currentBearing
        : null,
      "altitude": jsonData.altitude ? jsonData.altitude : null,
      "speed": jsonData.speed ? jsonData.speed : null,
      "horizontalAccuracy": jsonData.horizontalAccuracy
        ? jsonData.horizontalAccuracy
        : null,
      "verticalAccuracy": jsonData.verticalAccuracy
        ? jsonData.verticalAccuracy
        : null,
      "isLocationUpdated": jsonData.isLocationUpdated
        ? jsonData.isLocationUpdated
        : null,
      "locationUpdatedTime": jsonData.locationUpdatedTime
        ? jsonData.locationUpdatedTime
        : null,
    };
    return { "code": 200, "data": locationData, "message": "" };
  }
};

userAction.getUserData = async function (context) {
  const todayCount = await this.adapter.count({
    "query": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
    },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
              constantUtil.ATTENDED,
            ],
          },
        }
      : {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
              constantUtil.ATTENDED,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        };
  // const matchData1 = {
  //   "$match":
  //     context.params.city === ""
  //       ? matchData
  //       : {
  //           ...matchData,
  //           "category": mongoose.Types.ObjectId(context.params.city),
  //         },
  // };
  const data = await this.adapter.model.find(
    // context.params.city === ""
    //   ? matchData
    //   : {
    //       ...matchData,
    //       "category": mongoose.Types.ObjectId(context.params.city),
    //     },
    matchData,
    {
      "status": 1,
      "createdAt": 1,
    }
  );
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0,
    triedCount = 0;

  _.map(data, async (data) => {
    switch (data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
      case constantUtil.INCOMPLETE:
        triedCount += 1;
        break;
      case constantUtil.ATTENDED:
        triedCount += 1;
        break;
    }
  });

  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });

  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "triedCount": triedCount,
    "todayCount": todayCount,
    "graphData": graphData,
  };
  return responseJson;
};

userAction.addBankDetails = async function (context) {
  const userId =
    context.params.userId.toString() || context.meta.userId.toString();
  let userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(userId))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  // userData = userData.toJSON();
  const query = {};
  const bankDetails = context.params?.bankDetails
    ? context.params.bankDetails
    : context.params;
  if (
    (Array.isArray(userData?.bankDetails) && userData?.bankDetails[0]?._id) ||
    typeof userData?.bankDetails[0] !== "object" ||
    !userData?.bankDetails[0]
  ) {
    // this means document is is array working properlys
    query["$push"] = {
      "bankDetails": { "_id": mongoose.Types.ObjectId(), ...bankDetails },
    };
  }
  // this happend only one time
  else {
    if (!Object.keys(userData?.bankDetails[0])[0]) {
      // it meas bankDetailes is empty Object
      query["bankDetails"] = [
        { "_id": mongoose.Types.ObjectId(), ...bankDetails },
      ];
    } else {
      // it meas bankDetailes is has value
      query["bankDetails"] = [
        { "_id": mongoose.Types.ObjectId(), ...userData.bankDetails[0] },
        { "_id": mongoose.Types.ObjectId(), ...bankDetails },
      ];
    }
  }
  const updateUserData = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.meta.userId.toString()) },
    query
  );
  if (!updateUserData?.nModified) {
    throw new MoleculerError("ERROR IN BANK DETAILS UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "BANK DETAILS UPDATED SUCESSFULLY",
    };
  }
};

userAction.removeBankDetails = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  const updateUserData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
    },
    {
      "$pull": {
        "bankDetails._id": context.params.id,
      },
    }
  );
  if (!updateUserData?.nModified) {
    throw new MoleculerError("ERROR IN BANK DETAILS UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "BANK DETAILS UPDATED SUCESSFULLY",
    };
  }
};

userAction.getBankDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let paymentData;
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      break;
    }
  }
  if (!paymentData) {
    return "ERROR IN PAYMENT OPTION LIST";
  }
  let userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID PROFESSIONAL DETAILS");
  } else {
    // userData = userData.toJSON();
    // reponse
    let responseDataResult = {};
    if (userData?.bankDetails) {
      // responseDataResult['isDefaultBankDisplayKeyInApp'] =
      // userData.bankDetails[0][paymentData.isDefaultBankDisplayKeyInApp]
      responseDataResult = userData.bankDetails.map((data) => ({
        "isDefaultBankDisplayKeyInApp":
          data[paymentData?.isDefaultBankDisplayKeyInApp],
      }));
    }
    // console.log('===============================')
    // console.log(responseDataResult)
    // console.log('===============================')
    return {
      "code": 200,
      "data": responseDataResult,
      "message": "",
    };
  }
};

userAction.checkUserAvail = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const userData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
      //"clientId": mongoose.Types.ObjectId(clientId),
    })
    .lean();
  if (!userData) {
    return {
      "code": 404,
      "data": {},
      "message": "INVALID USER DETAILS",
    };
  } else {
    return {
      "code": 200,
      "data": userObjectForAdmin(userData),
      "message": "",
    };
  }
};

userAction.checkUserAndProfessionalAvailableUsingPhoneNo = async function (
  context
) {
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = await this.broker.emit("user.checkUserAvail", {
    "clientId": clientId,
    "phoneCode": context.params.phoneCode,
    "phoneNumber": context.params.phoneNumber,
  });
  userData = userData && userData[0];
  //
  let professionalData = await this.broker.emit(
    "professional.checkProfessionalAvail",
    {
      "clientId": clientId,
      "phoneCode": context.params.phoneCode,
      "phoneNumber": context.params.phoneNumber,
    }
  );
  professionalData = professionalData && professionalData[0];
  return {
    "code": 200,
    "data": {
      "isAvailableInUser": userData ? true : false,
      "userDetails": userData || {},
      "isAvailableInProfessional": professionalData ? true : false,
      "professionalDetails": professionalData || {},
    },
    "message": "",
  };
};

userAction.adminCheckUserAvail = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
      //"clientId": mongoose.Types.ObjectId(clientId),
    })
    // .populate("bookingInfo.ongoingBooking")
    .lean();
  if (!userData) {
    return {
      "code": 100,
      "data": {},
      "message": "INVALID USER DETAILS",
    };
  } else {
    if (userData?.bookingInfo?.ongoingBooking) {
      // ----------- get ride Details Start -----------
      let bookingDetails = await this.broker.emit(
        "booking.getBookingDetailsById",
        {
          "bookingId": userData?.bookingInfo?.ongoingBooking?.toString(),
          "clientId": clientId,
        }
      );
      bookingDetails = bookingDetails && bookingDetails[0];
      // ----------- get ride Details End -----------
      switch (bookingDetails?.bookingStatus) {
        case constantUtil.REJECTED:
        case constantUtil.USERDENY:
        case constantUtil.USERCANCELLED:
        case constantUtil.PROFESSIONALCANCELLED:
        case constantUtil.EXPIRED:
        case constantUtil.ENDED:
          userData = await this.adapter.model
            .findOneAndUpdate(
              {
                "phone.code": context.params.phoneCode,
                "phone.number": context.params.phoneNumber,
                //"clientId": mongoose.Types.ObjectId(clientId),
              },
              {
                "bookingInfo.ongoingBooking": null,
                "bookingInfo.pendingReview":
                  userData?.bookingInfo?.ongoingBooking?.toString(),
              },
              { "new": true }
            )
            // .populate("bookingInfo.ongoingBooking")
            .lean();
      }
    }
    //
    const userCodeData = await this.adapter.model
      .findOne({
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        //"clientId": mongoose.Types.ObjectId(clientId),
      })
      .lean();
    if (!userCodeData) {
      return {
        "code": 101,
        "data": {},
        "message": "DUPLICATE USER DETAILS",
      };
    } else {
      const responseData = userObjectForAdmin(userData);
      responseData["notes"] = userData.notes || "";
      return {
        "code": 200,
        "data": responseData,
        "message": "",
      };
    }
  }
};

userAction.registerUserByBooking = async function (context) {
  const clientId = context.params.clientId;
  let responseJson = null;
  const {
    INFO_INVALID_DATE_FORMAT,
    ALERT_DUPLICATE_EMAIL,
    ALERT_CPF_NO_ALREADY_EXISTS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);
  const checkEmailCount = await this.adapter.model.countDocuments({
    // "clientId": mongoose.Types.ObjectId(clientId),
    "email": context.params.email,
  });
  if (checkEmailCount > 0) {
    // throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    responseJson = await this.adapter.model
      .findOne({
        //"clientId": mongoose.Types.ObjectId(clientId),
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        "email": context.params.email,
      })
      .lean();
  } else {
    responseJson = await this.adapter.insert({
      "clientId": clientId,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "email": context.params.email,
      "gender": context.params.gender,
      "phone": {
        "code": context.params.phoneCode,
        "number": context.params.phoneNumber,
      },
      "nationalIdNo": context.params.nationalIdNo,
      "dob": context.params.dob,
      "status": constantUtil.ACTIVE,
      "createdFrom": context.params.createdFrom,
    });
  }
  if (!responseJson) {
    // throw new MoleculerError("USER ALREADY EXISTS");
    return { "code": 400, "data": {}, "message": ALERT_DUPLICATE_EMAIL };
  } else {
    return { "code": 200, "data": userObjectForAdmin(responseJson) };
  }
};

userAction.viewDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.userId.toString()))
    .lean();
  if (!responseJson) {
    throw new MoleculerError("ERROR IN USER");
  }
  let rideData = await this.broker.emit("booking.getUserRideDetails", {
    "clientId": clientId,
    "userId": context.meta.userId.toString(),
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError("ERROR IN USER RIDE DETAILS");
  } else {
    return {
      "code": 200,
      "data": {
        "name": responseJson.firstName + responseJson.lastName,
        "image": responseJson.avatar,
        "registerDate": responseJson.createdAt,
        "avgRating": responseJson.review.avgRating,
        "totalRides": rideData.totalRides,
        "totalRidesFor30Days": rideData.totalRidesFor30Days,
      },
    };
  }
};

userAction.adminViewDetails = async function (context) {
  const responseJson = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
      {}
    )
    .populate("serviceAreaId", { "locationName": 1 })
    .lean();
  if (!responseJson) {
    throw new MoleculerError("ERROR IN USER");
  } else {
    return {
      "code": 200,
      "response": responseJson,
    };
  }
};

userAction.getRatings = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let rideData = await this.broker.emit("booking.getUserRatingList", {
    "userId": context.meta.userId.toString(),
    "skip": context.params.skip,
    "limit": context.params.limit,
    "clientId": clientId,
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError("ERROR IN USER RATING DETAILS");
  } else {
    return {
      "code": 200,
      "data": rideData,
    };
  }
};

userAction.checkContacts = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      "phone": { "$in": context.params.contacts },
    },
    {
      "_id": 0,
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
    }
  );

  if (!responseJson[0]) {
    return {
      "code": 200,
      "data": [],
      "message": "NO CONTACTS FOUND",
    };
  }
  return {
    "code": 200,
    "data": responseJson,
    "message": "",
  };
};
userAction.addWalletAmount = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "";
  const languageCode = context.meta?.userDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    INFO_NOT_FOUND,
    TRAN_REDEEM_REWARD_MIN,
    TRAN_REDEEM_REWARD_MAX,
    INFO_INVALID_DETAILS,
    SOMETHING_WENT_WRONG,
    WALLET_AMOUNT_LOADED,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const userData = await this.adapter.model
      .findOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.id),
        },
        {
          "_id": 1,
          "wallet": 1,
          "firstName": 1,
          "lastName": 1,
          "avatar": 1,
          "phone": 1,
          "serviceAreaId": 1,
        }
      )
      .lean();
    if (!userData) {
      throw new MoleculerError("ERROR IN USER");
    }
    // const updateData =
    //   context.params.type === constantUtil.CREDIT
    //     ? {
    //         "wallet.availableAmount":
    //           userData.wallet.availableAmount + context.params.amount,
    //       }
    //     : {
    //         "wallet.availableAmount":
    //           userData.wallet.availableAmount - context.params.amount,
    //       };
    let updateData = null,
      redeemPointToAmount = 0;
    switch (context.params.type) {
      case constantUtil.CREDIT:
        updateData = {
          "wallet.availableAmount":
            userData.wallet.availableAmount + context.params.amount,
        };
        break;

      case constantUtil.DEBIT:
        updateData = {
          "wallet.availableAmount":
            userData.wallet.availableAmount - context.params.amount,
          "lastWithdrawDate": new Date(),
        };
        break;

      case constantUtil.REWARDPOINT:
        updateData = {
          "wallet.rewardPoints":
            userData.wallet.rewardPoints + context.params.amount,
        };
        break;

      case constantUtil.REDEEMREWARDPOINT:
        {
          if (!userData.serviceAreaId) {
            throw new MoleculerError(INFO_INVALID_DETAILS);
          }
          //-------- Active Reward List (POINT) Start -----------
          let activeRewardsList = await this.broker.emit(
            "admin.redeemRewardsPointConfig",
            {
              "clientId": clientId,
              "lng": gerenalsetting.data.location.lng, // For Skip Required Field
              "lat": gerenalsetting.data.location.lat, // For Skip Required Field
              "userType": constantUtil.USER,
              "languageCode": languageCode,
              "serviceAreaId": userData.serviceAreaId?.toString(),
            }
          );
          activeRewardsList = activeRewardsList && activeRewardsList[0];
          if (!activeRewardsList) {
            throw new MoleculerError(INFO_NOT_FOUND);
          } else if (activeRewardsList.redeemLimitMin > context.params.amount) {
            //-------- Redeem Point Start ---------
            throw new MoleculerError(
              TRAN_REDEEM_REWARD_MIN + " " + activeRewardsList.redeemLimitMin
            );
          } else if (activeRewardsList.redeemLimitMax < context.params.amount) {
            throw new MoleculerError(
              TRAN_REDEEM_REWARD_MAX + " " + activeRewardsList.redeemLimitMax
            );
          } else {
            redeemPointToAmount =
              context.params.amount *
              convertToFloat(activeRewardsList.redeemPointValue);
          }
          updateData = {
            "wallet.rewardPoints":
              userData.wallet.rewardPoints - context.params.amount,
            "wallet.availableAmount":
              userData.wallet.availableAmount + redeemPointToAmount,
          };
        }
        break;
    }
    const responseJson = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
      updateData
    );
    if (!responseJson.nModified) {
      throw new MoleculerError("ERROR IN USER");
    }
    if (context.params.type === constantUtil.CREDIT) {
      this.broker.emit("transaction.updateWalletAdminRecharge", {
        "clientId": clientId,
        "amount": context.params.amount,
        "transactionType": constantUtil.ADMINRECHARGE,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance":
          userData.wallet.availableAmount + context.params.amount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.USER,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    } else if (context.params.type === constantUtil.DEBIT) {
      this.broker.emit("transaction.updateWalletAdminDebit", {
        "clientId": clientId,
        "amount": context.params.amount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance":
          userData.wallet.availableAmount - context.params.amount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.USER,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    } else if (context.params.type === constantUtil.REWARDPOINT) {
      await this.broker.emit("rewards.insertRewardByUserTypeFromAdmin", {
        "clientId": clientId,
        "userType": constantUtil.USER,
        "userId": context.params.id,
        "rewardPoints": context.params.amount,
        "transactionType": constantUtil.CREDIT,
        "createdId": context.params.loginUserId,
        "reason": context.params.reason,
      });
    } else if (context.params.type === constantUtil.REDEEMREWARDPOINT) {
      await this.broker.emit("rewards.insertRewardByUserTypeFromAdmin", {
        "clientId": clientId,
        "userType": constantUtil.USER,
        "userId": context.params.id,
        "rewardPoints": context.params.amount,
        "transactionType": constantUtil.DEBIT,
        "createdId": context.params.loginUserId,
        "reason": context.params.reason,
      });
      //----------- Transaction For Redeem Point Value ------------
      this.broker.emit("transaction.updateWalletAdminRecharge", {
        "clientId": clientId,
        "amount": redeemPointToAmount,
        "transactionType": constantUtil.REDEEMREWARDPOINT,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": userData.wallet.availableAmount + redeemPointToAmount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.USER,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    }
    message = WALLET_AMOUNT_LOADED;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  // return {
  //   "code": 200,
  //   "response": {},
  //   "message": "WALLET REACHARGE SUCCESSFULLY",
  // };
  return {
    "code": errorCode,
    "response": {},
    "message": message,
  };
};
userAction.updatePermissionData = async function (context) {
  const updateData = {
    "osType": context.params.osType ? context.params.osType : null,
    "brand": context.params.brand ? context.params.brand : null,
    "model": context.params.model ? context.params.model : null,
    "osVersion": context.params.osVersion ? context.params.osVersion : null,
    "screenSize": context.params.screenSize ? context.params.screenSize : null,
    "internetConnectivityMedium": context.params.internetConnectivityMedium
      ? context.params.internetConnectivityMedium
      : null,
    "token": context.params.token ? context.params.token : null,
    "fcmId": context.params.fcmId ? context.params.fcmId : null,
    "deviceLocationStatus": context.params.deviceLocationStatus
      ? context.params.deviceLocationStatus
      : null,
    "appLocationStatus": context.params.appLocationStatus
      ? context.params.appLocationStatus
      : null,
    "baseUrl": context.params.baseUrl ? context.params.baseUrl : null,
    "appVersionInServer": context.params.appVersionInServer
      ? context.params.appVersionInServer
      : null,
    "appVersionInStore": context.params.appVersionInStore
      ? context.params.appVersionInStore
      : null,
    "updatedTime": new Date(),
    "deviceTime": context.params.deviceTime ? context.params.deviceTime : null,
    "timezoneInNumber": context.params.timezoneInNumber
      ? context.params.timezoneInNumber
      : null,
    "timezoneInformat": context.params.timezoneInformat
      ? context.params.timezoneInformat
      : null,
    "connectivityMediumName": context.params.connectivityMediumName
      ? context.params.connectivityMediumName
      : null,
    "socketConnetivity": context.params.socketConnetivity
      ? context.params.socketConnetivity
      : null,
  };
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.userId.toString()),
    {
      "$set": {
        "permissionData": updateData,
        "location.coordinates": [
          context.params.currentLocation.lng,
          context.params.currentLocation.lat,
        ],
      },
    }
  );
  if (!responseJson) {
    throw new MoleculerError("ERROR IN UPDATE PERMISSION DATA");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "PERMISSION DATA IS UPDATED SUCCESSFULLY",
    };
  }
};
userAction.getInviteAndEarnDetail = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { USER_NOT_FOUND } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
  const responseJson = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  let uniqueCode = "";
  // const dynamicUniqueCode = `${responseJson.firstName.slice(
  //   0,
  //   4
  // )}${helperUtil.randomString(6, "a")}`;
  const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
    responseJson.firstName,
    clientId
  );
  if (responseJson.uniqueCode && responseJson.uniqueCode !== null) {
    uniqueCode = responseJson.uniqueCode;
  } else {
    uniqueCode = dynamicUniqueCode.toLowerCase();
    const updateData = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.meta.userId.toString()),
      { "$set": { "uniqueCode": uniqueCode } }
    );
    if (!updateData) {
      throw new MoleculerError("ERROR IN UPDATE UNIQUE CODE");
    }
  }
  const link = inviteSettings.data.forUser.messageText.replace(
    new RegExp(`{{code}}`, "g"),
    uniqueCode
  );
  return {
    "code": 200,
    "data": {
      "uniqueCode": uniqueCode.toUpperCase(),
      "promotionJson": { ...inviteSettings.data.forUser, "messageText": link },
      "link": link,
    },
    "message": "",
  };
};
userAction.getInviteAndEarnHistory = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);
  const clientId = context.params.clientId || context.meta.clientId;
  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "referredBy": (context.params.inviteCode || "").toLowerCase(),
      "joinAmount": { "$gte": 0 },
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        // { "$skip": parseInt(skip) },
        // { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "joinAmount": "$joinAmount",
            "date": "$createdAt",
            "currencyCode": "$currencyCode",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return {
    "code": 200,
    "data": jsonData?.[0]?.response || [],
    "message": "",
  };
};

userAction.getInviteAndEarnList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  if (context.params.isHasInviteCount) {
    query.push({
      "$match": {
        // "joinCount": { "$gte": 1 },
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "$or": [
          { "joinCount": { "$gte": 1 } },
          { "invitationCount": { "$gte": 1 } },
        ],
        // "$or": [
        //   { "joinCount": { "$gte": 1 } },
        //   { "joinerRideCount": { "$gte": 1 } },
        //   { "joinerRinviterRideCountideCount": { "$gte": 1 } },
        // ],
      },
    });
  } else {
    query.push({
      "$match": {
        "$or": [
          { "joinCount": { "$in": [0, null, undefined] } },
          { "invitationCount": { "$in": [0, null, undefined] } },
        ],
      },
    });
  }

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  // FOR EXPORT DATE FILTER

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        // { "$skip": parseInt(skip) },
        // { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "uniqueCode": "$uniqueCode",
            "joinCount": "$joinCount",
            "joinerRideCount": "$joinerRideCount",
            "inviterRideCount": "$inviterRideCount",
            "invitationCount": "$invitationCount",
            "referrelTotalAmount": "$referrelTotalAmount",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};
userAction.downloadInviteAndEarnList = async function (context) {
  // const skip = parseInt(context.params.skip ?? 0);
  // const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  let groupByJoinerList = {};
  if (context.params.isHasInviteCount) {
    query.push({
      "$match": {
        // "joinCount": { "$gte": 1 },
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
        "$or": [
          { "joinCount": { "$gte": 1 } },
          { "invitationCount": { "$gte": 1 } },
        ],
        // "$or": [
        //   { "joinCount": { "$gte": 1 } },
        //   { "joinerRideCount": { "$gte": 1 } },
        //   { "joinerRinviterRideCountideCount": { "$gte": 1 } },
        // ],
      },
    });
    groupByJoinerList = {
      "joinerList": {
        "$push": {
          "_id": "$referredByList._id",
          "fullName": {
            "$concat": [
              "$referredByList.firstName",
              " ",
              "$referredByList.lastName",
            ],
          },
          "email": "$referredByList.email",
          "phoneNumber": {
            "$concat": [
              "$referredByList.phone.code",
              " ",
              "$referredByList.phone.number",
            ],
          },
          "joinAmount": "$referredByList.joinAmount",
          "joinerRideCount": "$referredByList.joinerRideCount",
          "inviterRideCount": "$referredByList.inviterRideCount",
        },
      },
    };
  } else {
    query.push({
      "$match": {
        "$or": [
          { "joinCount": { "$in": [0, null, undefined] } },
          { "invitationCount": { "$in": [0, null, undefined] } },
          // { "joinCount": { "$in": [0, null] } },
          // { "invitationCount": { "$in": [0, null] } },
        ],
      },
    });
  }
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  // FOR EXPORT DATE FILTER
  query.push(
    {
      "$lookup": {
        "from": "users",
        "localField": "uniqueCode",
        "foreignField": "referredBy",
        "as": "referredByList",
      },
    },
    {
      "$unwind": {
        "path": "$referredByList",
        // "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$group": {
        "_id": "$_id",
        "firstName": { "$first": "$firstName" },
        "lastName": { "$first": "$lastName" },
        "email": { "$first": "$email" },
        "phone": { "$first": "$phone" },
        "uniqueCode": { "$first": "$uniqueCode" },
        "joinCount": { "$first": "$joinCount" },
        "joinerRideCount": { "$first": "$joinerRideCount" },
        "inviterRideCount": { "$first": "$inviterRideCount" },
        "invitationCount": { "$first": "$invitationCount" },
        "referrelTotalAmount": { "$first": "$referrelTotalAmount" },
        ...groupByJoinerList,
        // "joinerList": {
        //   "$push": {
        //     "_id": "$referredByList._id",
        //     "fullName": {
        //       "$concat": [
        //         "$referredByList.firstName",
        //         " ",
        //         "$referredByList.lastName",
        //       ],
        //     },
        //     "email": "$referredByList.email",
        //     "phoneNumber": {
        //       "$concat": [
        //         "$referredByList.phone.code",
        //         " ",
        //         "$referredByList.phone.number",
        //       ],
        //     },
        //     "joinAmount": "$referredByList.joinAmount",
        //     "joinerRideCount": "$referredByList.joinerRideCount",
        //     "inviterRideCount": "$referredByList.inviterRideCount",
        //   },
        // },
      },
    },
    {
      "$project": {
        "_id": "$_id",
        "firstName": "$firstName",
        "lastName": "$lastName",
        "email": "$email",
        "phone": "$phone",
        "uniqueCode": "$uniqueCode",
        "joinCount": "$joinCount",
        "joinerRideCount": "$joinerRideCount",
        "inviterRideCount": "$inviterRideCount",
        "invitationCount": "$invitationCount",
        "referrelTotalAmount": "$referrelTotalAmount",
        "joinerList": {
          "$ifNull": ["$joinerList", []],
        },
      },
    },
    {
      "$sort": { "_id": -1 },
    }
  );
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return {
    "code": 200,
    "response": jsonData || [],
    "message": "",
  };
};

userAction.downloadUserJoiningList = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    // CREATED DATE
    if (
      (context.params?.createdAtFrom || "") !== "" &&
      (context.params?.createdAtTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    // UPDATED DATE
    if (
      (context.params?.updatedFrom || "") !== "" &&
      (context.params?.updatedTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.updatedFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.updatedTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "updatedAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }

    // USER STATUS
    if (context.params.userStatus && context.params.userStatus !== "ALL") {
      query.push({
        "$match": { "status": context.params.userStatus },
      });
    }
    // USER GENDER
    if (context.params.gender && context.params.gender !== "ALL") {
      query.push({
        "$match": { "gender": context.params.gender },
      });
    }
    // USER CITY
    if (context.params.city && context.params.city !== "ALL") {
      query.push({
        "$match": {
          "serviceAreaId": mongoose.Types.ObjectId(context.params.city),
        },
      });
    }
    // USER PHONE NUMBER
    if (context.params.phoneNumber) {
      query.push({
        "$match": {
          "phone.number": {
            "$regex": context.params.phoneNumber,
            "$options": "si",
          },
        },
      });
    }
    // USER NAME
    if (context.params.name) {
      query.push({
        "$match": {
          "$or": [
            {
              "firstName": { "$regex": context.params.name, "$options": "si" },
            },
            {
              "lastName": { "$regex": context.params.name, "$options": "si" },
            },
          ],
        },
      });
    }
    // USER EMAIL
    if (context.params.email) {
      query.push({
        "$match": {
          "email": { "$regex": context.params.email, "$options": "si" },
        },
      });
    }

    // USER RATING AMOUNT
    if (context.params.rating.ratingAmount) {
      switch (context.params.rating.ratingCondition) {
        case ">=": {
          query.push({
            "$match": {
              "review.avgRating": {
                "$gte": context.params.rating.ratingAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "review.avgRating": {
                "$lte": context.params.rating.ratingAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "review.avgRating": context.params.rating.ratingAmount,
            },
          });
        }
      }
    }
    //USER WALLET AMOUNT
    if (context.params.wallet.walletAmount) {
      switch (context.params.wallet.WalletCondition) {
        case ">=": {
          query.push({
            "$match": {
              "wallet.availableAmount": {
                "$gte": context.params.wallet.walletAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "wallet.availableAmount": {
                "$lte": context.params.wallet.walletAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "wallet.availableAmount": context.params.wallet.walletAmount,
            },
          });
        }
      }
    }
    // USER REWARDS POINT AMOUNT
    if (context.params.rewards.rewardsAmount) {
      switch (context.params.rewards.rewardsCondition) {
        case ">=": {
          query.push({
            "$match": {
              "wallet.rewardPoints": {
                "$gte": context.params.rewards.rewardsAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "wallet.rewardPoints": {
                "$lte": context.params.rewards.rewardsAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "wallet.rewardPoints": context.params.rewards.rewardsAmount,
            },
          });
        }
      }
    }
    // USER INVITE EARNINGS AMOUNT
    if (context.params.inviteEarn.inviteEarnAmount) {
      switch (context.params.inviteEarn.inviteEarnCondition) {
        case ">=": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount": {
                "$gte": context.params.inviteEarn.inviteEarnAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount": {
                "$lte": context.params.inviteEarn.inviteEarnAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount":
                context.params.inviteEarn.inviteEarnAmount,
            },
          });
        }
      }
    }
    // USER DEVICE TYPE
    if (context.params.deviceType && context.params.deviceType !== "ALL") {
      query.push({
        "$match": { "permissionData.osType": context.params.deviceType },
      });
    }

    query.push(
      {
        "$lookup": {
          "from": "categories",
          "localField": "serviceAreaId",
          "foreignField": "_id",
          "as": "locationName",
        },
      },
      {
        "$unwind": {
          "path": "$locationName",
          "preserveNullAndEmptyArrays": true,
        },
      },

      {
        "$facet": {
          "totalCount": [
            {
              "$count": "count",
            },
          ],
          "responseData": [
            {
              "$project": {
                "_id": "$_id",
                "firstName": "$firstName",
                "lastName": "$lastName",
                "email": "$email",
                "phone": "$phone",
                "status": "$status",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                "isHaveRide": "$bookingList._id",
                "locationName": "$locationName.locationName",
                "serviceAreaId": "$serviceAreaId",

                "walletAmount": "$wallet.availableAmount",
                "rewardPoints": "$wallet.rewardPoints",
                "inviteAmount": "$referralDetails.inviteAmount",
                "gender": "$gender",
                "referredBy": "$referredBy",
                "defaultPayment": "$defaultPayment",
                "avgRating": "$review.avgRating",
                "totalReview": "$review.totalReview",
                "deviceType": "$permissionData.osType",
                "pushNotification": {
                  "$cond": {
                    "if": context.params.pushNotification,
                    "then": "$permissionData.fcmId",
                    "else": null,
                  },
                },
              },
            },
            // {
            //   "$group": {
            //     "_id": "$_id",
            //     "firstName": { "$first": "$firstName" },
            //     "lastName": { "$first": "$lastName" },
            //     "email": { "$first": "$email" },
            //     "gender": { "$first": "$gender" },
            //     "referredBy": { "$first": "$referredBy" },
            //     "phone": { "$first": "$phone" },
            //     "status": { "$first": "$status" },
            //     "createdAt": { "$first": "$createdAt" },
            //     "updatedAt": { "$first": "$updatedAt" },

            //     "locationName": { "$first": "$locationName" },
            //     "walletAmount": { "$first": "$walletAmount" },
            //     "rewardPoints": { "$first": "$rewardPoints" },
            //     "inviteAmount": { "$first": "$inviteAmount" },
            //     "defaultPayment": { "$first": "$defaultPayment" },
            //     "avgRating": { "$first": "$avgRating" },
            //     "totalReview": { "$first": "$totalReview" },
            //     "deviceType": { "$first": "$deviceType" },
            //     "pushNotification": { "$first": "$pushNotification" },
            //   },
            // },
            { "$sort": { "createdAt": -1 } },
            { "$skip": context.params.skip },
            { "$limit": context.params.limit },
          ],
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

userAction.searchUserByName = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const query = [];
    // FOR EXPORT DATE FILTER

    // USER STATUS
    query.push({
      "$match": { "status": constantUtil.ACTIVE },
    });
    // USER NAME
    if (context.params.name) {
      query.push(
        {
          "$addFields": {
            "fullName": {
              "$concat": ["$firstName", " ", "$lastName"],
            },
          },
        },
        {
          "$match": {
            "$or": [
              {
                "firstName": {
                  "$regex": context.params.name,
                  "$options": "si",
                },
              },
              {
                "lastName": { "$regex": context.params.name, "$options": "si" },
              },
              {
                "fullName": { "$regex": context.params.name, "$options": "si" },
              },
              {
                "phone.number": {
                  "$regex": context.params.name,
                  "$options": "si",
                },
              },
            ],
          },
        }
      );
    }
    query.push(
      {
        "$lookup": {
          "from": "categories",
          "localField": "serviceAreaId",
          "foreignField": "_id",
          "as": "locationName",
        },
      },
      {
        "$unwind": {
          "path": "$locationName",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$facet": {
          "totalCount": [
            {
              "$count": "count",
            },
          ],
          "responseData": [
            {
              "$project": {
                "_id": "$_id",
                "firstName": "$firstName",
                "lastName": "$lastName",
                "email": "$email",
                // "phone": "$phone",
                "phoneCode": "$phone.code",
                "phoneNumber": "$phone.number",
                "status": "$status",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                "locationName": "$locationName.locationName",
                "serviceAreaId": "$serviceAreaId",

                "walletAmount": "$wallet.availableAmount",
                "rewardPoints": "$wallet.rewardPoints",
                "gender": "$gender",
              },
            },
            { "$sort": { "createdAt": -1 } },
            { "$skip": 0 },
            { "$limit": 10 },
          ],
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

userAction.getUsersJoinerList = async function (context) {
  const userData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
    },
    {
      "uniqueCode": 1,
    }
  );
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "referredBy": userData.uniqueCode,
    },
  });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "joinAmount": "$joinAmount",
            "joinerRideCount": "$joinerRideCount",
            "inviterRideCount": "$inviterRideCount",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};
userAction.updatePasswordBypass = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  const updateUserData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    },
    {
      "$set": {
        "isOtpBypass": context.params.isOtpBypass,
      },
    }
  );
  if (!updateUserData.nModified) {
    throw new MoleculerError("ERROR IN UPDATE");
  } else {
    return {
      "code": 200,
      "response": {},
      "message": "OTP BYPASS UPDATED SUCESSFULLY",
    };
  }
};
userAction.getUserId = async function (context) {
  const { USER_NOT_FOUND } = notifyMessage.setNotifyLanguage(
    context.params?.langCode
  );
  const userData = await this.adapter.model
    .findOne({
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
    })
    .lean();
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  } else {
    return {
      "code": 200,
      "data": {
        "_id": userData._id,
        "firstName": userData.firstName,
        "lastName": userData.lastName,
        "email": userData.email,
        "wallet": userData.wallet,
        "lat": userData
          ? userData.location
            ? userData.location.coordinates[1]
            : null
          : null,
        "lng": userData
          ? userData.location
            ? userData.location.coordinates[0]
            : null
          : null,
        "locationUpdatedTime": userData ? userData.locationUpdatedTime : null,
      },
      "message": "",
    };
  }
};
userAction.getQrcode = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let finalImageData;
  const userId =
    context.params.userId?.toString() || context.meta.userId.toString();
  const { USER_NOT_FOUND } = notifyMessage.setNotifyLanguage(
    context.params?.langCode
  );
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(userId))
    .lean();
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  const phoneCode = userData.phone.code;
  const phoneNumber = userData.phone.number;
  if (userData.scanAndPayQR && userData.scanAndPayQR !== null) {
    // return {
    //   "code": 200,
    //   "data": {
    //     "qrImage": userData.scanAndPayQR,
    //   },
    //   "message": "",
    // };
    finalImageData = userData.scanAndPayQR;
  } else {
    const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
      "quality": 50,
      "width": 500,
    });
    finalImageData = await this.broker.emit(
      "admin.uploadStaticMapImageInSpaces",
      {
        "files": qrImage,
        "fileName": `user${phoneCode + "" + phoneNumber}QrImage`,
        "oldFileName": `user${phoneCode + "" + phoneNumber}QrImage`,
      }
    );
    finalImageData = finalImageData && finalImageData[0];
    //
    const updateUserData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.userId.toString()),
      },
      {
        "$set": {
          "scanAndPayQR": finalImageData,
        },
      }
    );
    if (!updateUserData?.nModified) {
      throw new MoleculerError("ERROR IN UPDATE");
    }
    // return {
    //   "code": 200,
    //   "data": {
    //     "qrImage": finalImageData,
    //   },
    //   "message": "",
    // };
  }
  return {
    "code": 200,
    "data": {
      "qrImage": finalImageData,
    },
    "message": "",
  };
};

userAction.setAppliedCoupons = async function (context) {
  const updatedUserData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.userId.toString()),
    { "appliedCoupon": context.params.couponId }
  );
  if (!updatedUserData) {
    throw new MoleculerError("INVALID USER DETAILS");
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};
// PAYMENT RELATED CARD AND BANK ACCOUNT
userAction.verifyCard = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.meta.userId),
    })
    .lean();
  if (!userData) {
    throw new MoleculerError("USER NOT FOUND");
  }
  // userData = userData.toJSON();
  let verifyResponse;
  if (context.params.mode === constantUtil.OTP) {
    if (!context.params.otp) {
      throw new MoleculerError("otp is required", 422, "validation error");
    } else {
      verifyResponse = await this.broker.emit("transaction.verifyCard", {
        "otp": context.params.otp,
        "transactionId": context.params.transactionId,
        "clientId": clientId,
      });
    }
  } else if (context.params.mode === constantUtil.PIN) {
    if (!context.params.pin) {
      throw new MoleculerError("pin is required", 422, "validation error");
    } else {
      verifyResponse = await this.broker.emit("transaction.verifyCard", {
        "pin": context.params.pin,
        "transactionId": context.params.transactionId,
        "clientId": clientId,
      });
    }
  }
  //
  verifyResponse = verifyResponse[0];
  if (verifyResponse.code > 200) {
    return verifyResponse;
  }
  const updatedCard = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(userData._id.toString()),
      "cards._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
    },
    { "$set": { "cards.$.isVerified": true } }
  );
  if (!updatedCard.nModified) {
    throw new MoleculerError("CANT UPDATED CARD IN VERIFY CARD OTP");
  } else {
    return {
      "code": 200,
      "data": {
        ...verifyResponse.data,
        "card": {
          ...userData.cards.find(
            (card) => card._id.toString() === context.params.cardId
          ),
          "isVerified": true,
        },
      },
      "message": verifyResponse.message,
    };
  }
  // update
};
// db migration purpose

userAction.dbMigration = async function (context) {
  console.log(context);
  const d = context.params.data;
  const errorOccured = [];
  for (const user of d) {
    let phoneCode, phoneNumber, firstName, lastName, email, availableAmount;
    const splitName = user["User Name"]?.split(" ");
    try {
      phoneCode = `+${user.Mobile.split(" ")[0]?.trim()}`;
      phoneNumber = user.Mobile.split(" ")[1]?.trim();
      firstName = splitName[0]?.trim();
      lastName = `${splitName[1]?.trim()} ${splitName[2]?.trim() ?? ""}`.trim();
      email = user.Email?.trim();
      availableAmount = user["Wallet Balance"]
        ? user["Wallet Balance"].split(" ")[1]?.trim()
        : 0;

      await this.adapter.model.create({
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "nationalIdNo": context.params.nationalIdNo,
        "dob": context.params.dob,
        "firstName": firstName ?? null,
        "lastName": lastName ?? "pamdrive",
        "email": email ?? undefined,
        "otp": cryptoUtils.hashSecret(context.params.otp),
        "password": cryptoUtils.passwordEncode(context.params.password),
        "wallet.availableAmount": parseFloat(
          parseFloat(availableAmount).toFixed(2)
        ),
        "status": user?.Status?.toUpperCase() ?? constantUtil.ACTIVE,
        "currencyCode": "USD",
        "lastCurrencyCode": "USD",
      });
      continue;
    } catch (error) {
      console.log("error", error);
      errorOccured.push({
        "alteredData": {
          "phone.code": phoneCode,
          "phone.number": phoneNumber,
          "firstName": firstName ?? null,
          "lastName": lastName ?? null,
          "email": email ?? undefined,
          "otp": cryptoUtils.hashSecret(context.params.otp),
          "password": cryptoUtils.passwordEncode(context.params.password),
          "wallet.availableAmount": parseFloat(
            parseFloat(availableAmount).toFixed(2)
          ),
        },
        "rawData": user,
        "error": error,
      });
      continue;
    }
  }

  const re = CircularJSON.stringify(errorOccured);
  console.log(errorOccured);
  return re;
};
//------------- Redeem Rewards Point Start ------------
userAction.redeemRewardsPoint = async function (context) {
  const redeemPoint = convertToFloat(context?.params?.redeemPoint || 0);
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    languageCode = "",
    redeemRewardsPointValue = 0;

  const responseJson = {};
  languageCode = context.meta?.userDetails?.languageCode;

  const {
    INFO_NOT_FOUND,
    TRAN_REDEEM_REWARD_MAX,
    TRAN_REDEEM_REWARD_MIN,
    TRAN_REDEEM_REWARD_POINT,
    SOMETHING_WENT_WRONG,
    ERROR_WALLET_WITHDRAW,
    INFO_PAYMENT_OPTIONS,
  } = notifyMessage.setNotifyLanguage(languageCode);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  try {
    //-------- Active Reward List (POINT) Start -----------
    let activeRewardsList = await this.broker.emit(
      "admin.redeemRewardsPointConfig",
      {
        "clientId": clientId,
        "lng": context.params?.lng,
        "lat": context.params?.lat,
        "userType": constantUtil.USER,
        "languageCode": languageCode,
      }
    );
    activeRewardsList = activeRewardsList && activeRewardsList[0];
    if (!activeRewardsList) {
      throw new MoleculerError(INFO_NOT_FOUND, 500);
    }
    //-------- Active Reward List (POINT) End -----------
    //-------- Redeem Point Start ---------
    if (activeRewardsList.redeemLimitMin > redeemPoint) {
      message = TRAN_REDEEM_REWARD_MIN + " " + activeRewardsList.redeemLimitMin;
      errorCode = 400;
    } else if (activeRewardsList.redeemLimitMax < redeemPoint) {
      message = TRAN_REDEEM_REWARD_MAX + " " + activeRewardsList.redeemLimitMax;
      errorCode = 400;
    } else {
      redeemRewardsPointValue =
        redeemPoint * convertToFloat(activeRewardsList.redeemPointValue);
      let transactionReason = TRAN_REDEEM_REWARD_POINT + " / " + redeemPoint;
      let transactionType = constantUtil.REDEEMREWARDPOINT;
      const isEnabledRedeemApi =
        generalSettings?.data?.redeemApp?.isEnabled || false;
      // const redeemApiPaymentGatway =
      //   generalSettings?.data?.redeemApp?.paymentGatway || undefined; //Need to Change
      const redeemApiPaymentGatway =
        context.params?.redeemPaymentGatway || undefined;
      let walletDetails = {},
        paymentData = {},
        gatewayResponse = {};

      //#region paymentOption / PaymentGatway
      const paymentOptionList = await storageUtil.read(
        constantUtil.PAYMENTGATEWAY
      );
      for (const prop in paymentOptionList) {
        if (
          paymentOptionList[prop].status === constantUtil.ACTIVE &&
          paymentOptionList[prop].name === constantUtil.REDEEM
        ) {
          paymentData = paymentOptionList[prop];
          break;
        }
      }
      if (!paymentOptionList) {
        throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
      }
      //#endregion paymentOption / PaymentGatway

      if (isEnabledRedeemApi) {
        switch (redeemApiPaymentGatway) {
          case constantUtil.CONST_TELEPORT:
            {
              gatewayResponse = await this.broker.emit(
                "transaction.teleport.redeemRewardsPointToTeleportAirtime",
                {
                  "clientId": clientId,
                  "redeemRewardsPointValue": redeemRewardsPointValue,
                  "phoneCode": context.params.phoneCode,
                  "phoneNumber": context.params.phoneNumber,
                  "generalSettings": generalSettings,
                  "transactionReason": transactionReason,
                  "paymentData": paymentData,
                }
              );
              gatewayResponse = gatewayResponse && gatewayResponse[0];
              if (!gatewayResponse) {
                throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
              }
              if (
                gatewayResponse.code === 200 &&
                gatewayResponse.data.status === constantUtil.COMPLETE
              ) {
                walletDetails = {
                  "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
                };
                transactionReason = "Mobile Recharge";
                transactionType = constantUtil.REDEEMREWARDAIRTIME;
              } else {
                throw new MoleculerError("Mobile Recharge Failed", 500);
              }
            }
            break;

          default:
            walletDetails = {
              "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
              "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
            };
            break;
        }
      } else {
        walletDetails = {
          "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
          "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
        };
      }

      //-------- Redeem Point End ---------
      const userDetails = await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.meta?.userId?.toString()),
        },
        {
          "$inc": {
            // "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
            // "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
            ...walletDetails,
          },
          "lastRedeemDate": new Date(),
        },
        { "new": true }
        // {
        //   "upsert": true,
        // }
      );
      //-------- Transaction Entry Start ---------------------
      const transactionDetails = await this.broker.emit(
        "transaction.insertTransactionBasedOnUserType",
        {
          "clientId": clientId,
          "transactionType": transactionType,
          "transactionAmount": redeemRewardsPointValue,
          "previousBalance":
            convertToFloat(userDetails.wallet.availableAmount) -
            convertToFloat(redeemRewardsPointValue),
          "currentBalance": userDetails.wallet.availableAmount,
          "transactionReason": transactionReason,
          "transactionStatus": constantUtil.SUCCESS,
          "paymentType": constantUtil.CREDIT,
          "paymentGateWay": redeemApiPaymentGatway,
          "gatewayResponse": gatewayResponse?.successResponse || "",
          "userType": constantUtil.USER,
          "userId": context.meta?.userId?.toString(),
          "professionalId": "",
        }
      );
      //-------- Transaction Entry End ---------------------
      message = TRAN_REDEEM_REWARD_POINT;
      responseJson["redeemPoint"] = redeemPoint;
      responseJson["wallet"] = userDetails?.wallet;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "error": errorMessage,
    "data": responseJson,
  };
};
// ! LOCATION

// * Categories

// $ RENTAL

userAction.getBookingLocationCategories = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  context.params.user = context.meta || {};
  const [response] = await this.broker.emit(
    "category.getBookingLocationCategories",
    { ...context.params, "clientId": clientId }
  );
  return response;
};

// $ RENTAL \\

// * Categories \\

// ! LOCATION \\

//------------- Redeem Rewards Point End ------------
userAction.getUsersActiveInactive = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model.aggregate([
      {
        "$project": {
          "_id": "$address.city",

          "activeStatus": {
            "$cond": {
              "if": { "$eq": ["$status", "ACTIVE"] },
              "then": 1,
              "else": 0,
            },
          },
          "inActiveStatus": {
            "$cond": {
              "if": {
                "$or": [
                  { "$eq": ["$status", "INCOMPLETE"] },
                  { "$eq": ["$status", "ATTENDED"] },
                  // { "$eq": ["$status", "ARCHIVE"] },
                ],
              },

              "then": 1,
              "else": 0,
            },
          },
          "status": "$status",

          "gender": {
            "$cond": {
              "if": { "$eq": ["$gender", null] },
              "then": "OTHER",
              "else": "$gender",
            },
          },
          // "userName": { "$substrCP": ["$users.firstName", 0, 2] },
          // "gender": "$gender",
        },
      },
      {
        "$group": {
          "_id": "$gender",

          "activeStatus": { "$sum": "$activeStatus" },

          "inActiveStatus": { "$sum": "$inActiveStatus" },
          "gender": { "$first": "$gender" },
          // "userName": { "$first": "$userName" },
          //           "activeStatus2":{ "$max" :{ "$sum": "$activeStatus" } },
        },
      },
      { "$sort": { "_id": 1 } },
    ]);

    return jsonData;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
//
userAction.manageBlockedProfessionalList = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    isBlocked = false,
    jsonData = {},
    responseData = [];
  try {
    switch (context.params?.action?.toUpperCase()) {
      case constantUtil.BLOCKED:
        jsonData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.userId),
            },
            {
              "$push": {
                "blockedProfessionalList": mongoose.Types.ObjectId(
                  context.params.blockedProfessionalId
                ),
              },
            },
            {
              "fields": {
                "blockedProfessionalList": 1,
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        if (jsonData) {
          isBlocked = true;
          jsonData["professionalId"] = context.params.blockedProfessionalId;
        }
        responseData.push(jsonData);
        break;

      case constantUtil.UNBLOCKED:
        jsonData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.userId),
            },
            {
              "$pull": {
                "blockedProfessionalList": mongoose.Types.ObjectId(
                  context.params.blockedProfessionalId
                ),
              },
            },
            {
              "fields": {
                "blockedProfessionalList": 1,
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        if (jsonData) {
          isBlocked = false;
          jsonData["professionalId"] = context.params.blockedProfessionalId;
        }
        responseData.push(jsonData);
        break;

      default: // CHECK --> Check Professional Status
        responseData = await this.adapter.model.aggregate([
          {
            "$match": { "_id": mongoose.Types.ObjectId(context.params.userId) },
          },
          {
            "$lookup": {
              "from": "professionals",
              "localField": "blockedProfessionalList",
              "foreignField": "_id",
              "as": "professionalsBlockedList",
            },
          },
          { "$unwind": "$professionalsBlockedList" },
          {
            "$match": {
              "professionalsBlockedList._id": mongoose.Types.ObjectId(
                context.params.blockedProfessionalId
              ),
            },
          },
          {
            "$project": {
              "_id": 1,
              "professionalId": "$professionalsBlockedList._id",
              "phone": "$professionalsBlockedList.phone",
              "firstName": "$professionalsBlockedList.firstName",
              "lastName": "$professionalsBlockedList.lastName",
              "blockedProfessionalList": 1,
            },
          },
        ]);
        if (responseData.length > 0) {
          isBlocked = true;
        }
        break;
    }
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
    isBlocked = false;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    // "data": responseData || [],
    "data": { "isBlocked": isBlocked, "userList": responseData },
  };
};

userAction.manageTrustedContact = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseData = [];
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    responseData = await this.broker.emit("user.updateTrustedContact", {
      ...context.params,
      "clientId": clientId,
    });
    responseData = responseData && responseData[0];
    errorCode = responseData.code;
    message = responseData.message;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData,
  };
};

userAction.updateProfileNotes = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    errorMessage = "",
    responseData = [];
  languageCode = context.meta?.userDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;

  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    responseData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.id),
        },
        {
          "notes": context.params.notes,
        },
        {
          "fields": {
            "_id": 1,
            "phone": 1,
            "firstName": 1,
            "lastName": 1,
            "notes": 1,
          },
          "new": true,
        }
      )
      .lean();
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
  };
};
//-------------------------------
module.exports = userAction;
