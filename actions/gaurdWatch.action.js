const QRCode = require("qrcode");
const mongoose = require("mongoose");
const { MoleculerError } = require("moleculer").Errors;

const uploadUtil = require("../utils/upload.util");
const constantUtil = require("../utils/constant.util");

const gaurdWatchAction = {};

gaurdWatchAction.createSiteLocation = async function (context) {
  const mongoId = mongoose.Types.ObjectId();
  let qrImage = await QRCode.toDataURL(mongoId.toString());
  qrImage = uploadUtil.uploadBase64(qrImage, context.params.placeId);
  const jsonData = await this.adapter.model.create({
    "_id": mongoId,
    "owner": context.meta.userId,
    "qrImage": qrImage,
    "locationName": context.params.addressName,
    "locationAddress": {
      "placeId": context.params.placeId,
      "addressName": context.params.addressName,
      "fullAddress": context.params.shortAddress,
      "shortAddress": context.params.fullAddress,
      "lat": context.params.lat,
      "lng": context.params.lng,
    },
    "locationStatus": constantUtil.ACTIVE,
    "gaurd": {},
  });
  if (!jsonData) {
    throw new MoleculerError("INVALID EVENT DATA");
  } else {
    return {
      "code": 200,
      "data": jsonData,
      "message": "",
    };
  }
};

gaurdWatchAction.changeSiteLocationStatus = async function (context) {
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
    },
    {
      "locationStatus": context.params.status,
    }
  );
  if (!jsonData || jsonData.nModified === 0) {
    throw new MoleculerError(
      "ERROR IN UPDATE STATUS OR YOU ARE NOT ALLOWED TO CHANGE STATUS"
    );
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "STATUS CHANGED SUCCESSFULLY",
    };
  }
};

gaurdWatchAction.deleteSiteLocation = async function (context) {
  const checkData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
    },
    { "gaurd": 1 }
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  }
  if (
    checkData ||
    (!checkData.gaurd && !checkData.gaurd.current) ||
    (checkData.gaurd &&
      checkData.gaurd.current &&
      checkData.gaurd.current.length === 0)
  ) {
    const jsonData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.locationId),
        "owner": mongoose.Types.ObjectId(context.meta.userId),
      },
      { "locationStatus": constantUtil.ARCHIVE }
    );
    if (!jsonData || jsonData.nModified === 0) {
      throw new MoleculerError(
        "ERROR IN UPDATE STATUS OR YOU ARE NOT ALLOWED TO CHANGE STATUS"
      );
    } else {
      return {
        "code": 200,
        "data": {},
        "message": "STATUS CHANGED SUCCESSFULLY",
      };
    }
  } else {
    throw new MoleculerError(
      "UNABLE TO DELETE YOUR SITE LOCATION BECAUSEOF GAURDS ARE AVALILABLE"
    );
  }
};

gaurdWatchAction.getSiteLocationList = async function (context) {
  const jsonData = await this.adapter.model.find(
    {
      "owner": mongoose.Types.ObjectId(context.meta.userId),
      "locationStatus": { "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE] },
    },
    {
      "_id": 1,
      "locationName": 1,
      "locationStatus": 1,
      "locationAddress": 1,
      "qrImage": 1,
      "gaurd": 1,
    }
  );
  if (!jsonData) {
    throw new MoleculerError("ERROR IN SITE LOCATION LIST");
  } else {
    return {
      "code": 200,
      "data": jsonData.map((data) => {
        return {
          "_id": data._id,
          "locationName": data.locationName,
          "locationStatus": data.locationStatus,
          "locationAddress": data.locationAddress,
          "qrImage": data.qrImage,
          "gaurdsCount": data.gaurd
            ? data.gaurd.active
              ? parseInt(data.gaurd.active.length)
              : 0
            : 0,
        };
      }),
      "message": "",
    };
  }
};

gaurdWatchAction.sendQrToMail = async function (context) {
  const checkData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
    },
    { "qrImage": 1 }
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  } else {
    // if (context.params.email) {
    //   this.broker.emit('admin.sendMail', {
    //     'email': context.params.email,
    //     'image': checkData.qrImage,
    //   })
    // }
    return {
      "code": 200,
      "data": {},
      "message": "MAIL IS SENDED TO YOUR MAIL ACCOUNT",
    };
  }
};

gaurdWatchAction.addGaurd = async function (context) {
  let gaurd = await this.broker.emit("user.getUserDetails", context.params);
  gaurd = gaurd[0];
  const updateData = {
    "_id": gaurd._id,
    "fromTime": context.params.fromTime,
    "workingHrs": context.params.workingHrs,
    "loginLogoutStatus": constantUtil.LOGOUT,
    "breakStatus": constantUtil.BREAKEND,
  };
  const checkData = await this.adapter.model.findOne({
    "gaurd.active._id": {
      "$eq": mongoose.Types.ObjectId(gaurd._id.toString()),
    },
  });
  if (checkData) {
    throw new MoleculerError("GUARD IS ALREADY WORK IN ANOTHER SITE LOCATION");
  }
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
      "gaurd.active._id": {
        "$ne": mongoose.Types.ObjectId(gaurd._id.toString()),
      },
    },
    {
      "$push": { "gaurd.active": updateData },
    }
  );
  if (!jsonData || jsonData.nModified === 0) {
    throw new MoleculerError(
      "ERROR IN UPDATE STATUS OR YOU ARE NOT ALLOWED TO CHANGE STATUS"
    );
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "GAURD ADDED SUCCESSFULLY",
    };
  }
};

gaurdWatchAction.getGaurdList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      "owner": mongoose.Types.ObjectId(context.meta.userId),
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd": { "$ne": null },
      "gaurd.active": { "$ne": null },
      "locationStatus": { "$ne": constantUtil.ARCHIVE },
    },
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$project": {
            "_id": "$_id",
            "locationId": "$_id",
            "locationName": "$locationName",
            "locationAddress": "$locationAddress",
            "locationStatus": "$locationStatus",
            "gaurds": "$gaurd.active",
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "gaurds._id",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": "$user",
        },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$locationId",
            "gaurd": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "code": "$user.phone.code",
              "number": "$user.phone.number",
              "avatar": "$user.avatar",
            },
            "locationStatus": "$locationStatus",
            "locationName": "$locationName",
            "locationAddress": "$locationAddress",
            "fromTime": { "$arrayElemAt": ["$gaurds.fromTime", 0] },
            "workingHrs": { "$arrayElemAt": ["$gaurds.workingHrs", 0] },
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query);

  return {
    "code": 200,
    "data": jsonData[0].response,
    "message": "",
  };
};

gaurdWatchAction.viewSiteLocation = async function (context) {
  const checkData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
    },
    { "gaurd": 0 }
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  } else {
    return {
      "code": 200,
      "data": checkData,
      "message": "",
    };
  }
};

gaurdWatchAction.loginAndLogout = async function (context) {
  const updateData =
    context.params.type === constantUtil.LOGIN
      ? {
          "gaurd.active.$.currentData.loginTime": new Date(),
          "gaurd.active.$.loginLogoutStatus": constantUtil.LOGIN,
        }
      : {
          "gaurd.active.$.currentData.logoutTime": new Date(),
          "gaurd.active.$.loginLogoutStatus": constantUtil.LOGOUT,
        };
  let checkData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
        },
      },
    },
    updateData
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  }
  if (context.params.type === constantUtil.LOGOUT) {
    const gaurdData = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
        },
      },
    });
    const data = await gaurdData.gaurd.active.filter(
      (data) => data._id.toString() === context.meta.userId.toString()
    );
    checkData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.locationId),
        "gaurd.active": {
          "$elemMatch": {
            "_id": mongoose.Types.ObjectId(context.meta.userId),
          },
        },
      },
      {
        "$push": { "gaurd.active.$.summary": data[0].currentData },
        "gaurd.active.$.currentData": {
          "loginTime": null,
          "breakData": [],
          "scanData": [],
          "logoutTime": null,
        },
      }
    );
    if (!checkData) {
      throw new MoleculerError("INVALID DATA");
    }
  }
  return {
    "code": 200,
    "data": {},
    "message": "UPDATED SUCESSFULLY",
  };
};

gaurdWatchAction.breakStatusChange = async function (context) {
  const updateData =
    context.params.type === constantUtil.BREAKSTART
      ? {
          "gaurd.active.$.breakData.breakStartTime": new Date(),
          "gaurd.active.$.breakStatus": constantUtil.BREAKSTART,
        }
      : {
          "gaurd.active.$.breakData.breakEndTime": new Date(),
          "gaurd.active.$.breakStatus": constantUtil.BREAKEND,
        };
  let checkData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
          "loginLogoutStatus": constantUtil.LOGIN,
        },
      },
    },
    updateData
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  }
  if (context.params.type === constantUtil.BREAKEND) {
    const gaurdData = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
        },
      },
    });
    const data = await gaurdData.gaurd.active.filter(
      (data) => data._id.toString() === context.meta.userId.toString()
    );
    checkData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.locationId),
        "gaurd.active": {
          "$elemMatch": {
            "_id": mongoose.Types.ObjectId(context.meta.userId),
            "loginLogoutStatus": constantUtil.LOGIN,
          },
        },
      },
      {
        "$push": {
          "gaurd.active.$.currentData.breakData": data[0].breakData,
        },
      }
    );
    if (!checkData) {
      throw new MoleculerError("INVALID DATA");
    }
  }
  return {
    "code": 200,
    "data": {},
    "message": "UPDATED SUCESSFULLY",
  };
};

gaurdWatchAction.getGaurdDetails = async function (context) {
  const siteData = await this.adapter.model.findOne({
    "gaurd.active": {
      "$elemMatch": {
        "_id": mongoose.Types.ObjectId(context.meta.userId),
      },
    },
  });
  if (!siteData) {
    throw new MoleculerError("SORRY... YOU ARE NOT GUARD AT THIS MOMENT ");
  }
  const gaurdData = await siteData.gaurd.active.filter(
    (data) => data._id.toString() === context.meta.userId.toString()
  );
  siteData.gaurd.active = gaurdData;
  if (!gaurdData) {
    throw new MoleculerError("SORRY... YOU ARE NOT GUARD AT THIS MOMENT");
  }
  let userData = await this.broker.emit("user.getById", {
    "id": siteData.gaurd.active[0]._id.toString(),
  });
  userData = userData && userData[0];
  if (!userData || userData.length === 0 || !userData[0]) {
    throw new MoleculerError("USER NOT FOUND");
  } else {
    const sendData = {
      "locationId": siteData._id,
      "locationAddress": siteData.locationAddress,
      "locationStatus": siteData.locationStatus,
      "locationName": siteData.locationName,
      "_id": siteData.gaurd.active[0]._id,
      "firstname": userData.firstName,
      "lastName": userData.lastName,
      "code": userData.phone.code,
      "number": userData.phone.number,
      "avatar": userData.avatar,
      "fromTime": siteData.gaurd.active[0].fromTime,
      "workingHrs": siteData.gaurd.active[0].workingHrs,
      "breakStatus": siteData.gaurd.active[0].breakStatus,
      "loginLogoutStatus": siteData.gaurd.active[0].loginLogoutStatus,
    };
    return {
      "code": 200,
      "data": sendData,
      "message": "UPDATED SUCESSFULLY",
    };
  }
};

gaurdWatchAction.scanAndUpdate = async function (context) {
  const updateData = {
    "scanDate": new Date(),
    "locationData": context.params.locationData,
  };
  const checkData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
          "loginLogoutStatus": constantUtil.LOGIN,
          "breakStatus": constantUtil.BREAKEND,
        },
      },
    },
    {
      "$push": {
        "gaurd.active.$.currentData.scanData": updateData,
      },
    }
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "UPDATED SUCESSFULLY",
    };
  }
};

gaurdWatchAction.getGaurdHistory = async function (context) {
  const checkData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.locationId),
    "gaurd.active": {
      "$elemMatch": {
        "_id": mongoose.Types.ObjectId(context.params.gaurdId),
      },
    },
  });
  const data = await checkData.gaurd.active.filter(
    (data) => data._id.toString() === context.params.gaurdId.toString()
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  }
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  let jsonData;
  if (data && data.length > 0) {
    query.push({
      "$match": {
        "_id": mongoose.Types.ObjectId(context.params.locationId),
        "gaurd.active": {
          "$elemMatch": {
            "_id": mongoose.Types.ObjectId(context.params.gaurdId),
          },
        },
      },
    });

    query.push({
      "$facet": {
        "response": [
          {
            "$project": {
              "_id": "$_id",
              "gaurd": {
                "$filter": {
                  "input": "$gaurd.active",
                  "as": "item",
                  "cond": {
                    "$eq": [
                      "$$item._id",
                      mongoose.Types.ObjectId(context.params.gaurdId),
                    ],
                  },
                },
              },
            },
          },
          {
            "$unwind": "$gaurd",
          },
          {
            "$project": {
              "summary": "$gaurd.summary",
            },
          },
          { "$unwind": "$summary" },
          { "$sort": { "summary.loginTime": -1 } },
          { "$skip": parseInt(skip) },
          { "$limit": parseInt(limit) },
          {
            "$project": {
              "_id": 0,
              "summary": "$summary",
            },
          },
        ],
      },
    });

    jsonData = await this.adapter.model.aggregate(query);
  }

  const responseJson = {};
  responseJson["totalDateCount"] =
    (data && data.length > 0 && data[0].summary && data[0].summary.length) || 0;

  responseJson["todayScanCount"] =
    (data &&
      data.length > 0 &&
      data[0].currentData &&
      data[0].currentData.scanData &&
      data[0].currentData.scanData.length) ||
    0;
  const dataSet =
    data &&
    data.length > 0 &&
    data[0].currentData &&
    data[0].currentData.scanData &&
    data[0].currentData.scanData.length > 0
      ? [{ "summary": data[0].currentData }]
      : [];
  responseJson["response"] =
    data && data.length > 0 && data[0].summary
      ? jsonData[0].response && jsonData[0].response.length > 0
        ? [...dataSet, ...jsonData[0].response]
        : []
      : [];

  return {
    "code": 200,
    "responseJson": responseJson,
    "message": "",
  };
};

gaurdWatchAction.removeGaurd = async function (context) {
  const gaurdData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.locationId),
    "gaurd.active": {
      "$elemMatch": {
        "_id": mongoose.Types.ObjectId(context.params.gaurdId),
      },
    },
  });
  if (!gaurdData) {
    throw new MoleculerError("INVALID DATA");
  }
  const data = await gaurdData.gaurd.active.filter(
    (data) => data._id.toString() === context.params.gaurdId.toString()
  );
  const checkData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "owner": mongoose.Types.ObjectId(context.meta.userId),
    },
    {
      "$push": { "gaurd.inactive": data[0] },
      "$pull": {
        "gaurd.active": {
          "_id": mongoose.Types.ObjectId(context.params.gaurdId),
        },
      },
    }
  );
  if (!checkData) {
    throw new MoleculerError("INVALID DATA");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "UPDATED SUCESSFULLY",
    };
  }
};

gaurdWatchAction.getSiteLocationBasedGuard = async function (context) {
  const jsonData = await this.adapter.model
    .find(
      {
        "$or": [
          {
            "gaurd.active": {
              "$elemMatch": {
                "_id": mongoose.Types.ObjectId(context.meta.userId),
              },
            },
          },
          {
            "gaurd.inactive": {
              "$elemMatch": {
                "_id": mongoose.Types.ObjectId(context.meta.userId),
              },
            },
          },
        ],
      },
      {
        "_id": 1,
        "locationName": 1,
        "locationStatus": 1,
        "locationAddress": 1,
        "qrImage": 1,
      }
    )
    .sort({ "_id": -1 });

  if (!jsonData) {
    throw new MoleculerError("INVALID DATA");
  } else {
    return {
      "code": 200,
      "data": jsonData.map((data) => {
        return {
          "_id": data._id,
          "locationName": data.locationName,
          "locationStatus": data.locationStatus,
          "locationAddress": data.locationAddress,
          "qrImage": data.qrImage,
        };
      }),
      "message": "",
    };
  }
};

gaurdWatchAction.getMyHistory = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  if (context.params.isCurrentSite === true) {
    const checkData = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.active": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
        },
      },
    });

    const data = await checkData.gaurd.active.filter(
      (data) => data._id.toString() === context.meta.userId.toString()
    );
    if (!checkData) {
      throw new MoleculerError("INVALID DATA");
    }
    const query = [];
    let jsonData;
    if (data && data.length > 0) {
      query.push({
        "$match": {
          "_id": mongoose.Types.ObjectId(context.params.locationId),
          "gaurd.active": {
            "$elemMatch": {
              "_id": mongoose.Types.ObjectId(context.meta.userId),
            },
          },
        },
      });

      query.push({
        "$facet": {
          "response": [
            {
              "$project": {
                "_id": "$_id",
                "gaurd": {
                  "$filter": {
                    "input": "$gaurd.active",
                    "as": "item",
                    "cond": {
                      "$eq": [
                        "$$item._id",
                        mongoose.Types.ObjectId(context.meta.userId),
                      ],
                    },
                  },
                },
              },
            },
            {
              "$unwind": "$gaurd",
            },
            {
              "$project": {
                "summary": "$gaurd.summary",
              },
            },
            { "$unwind": "$summary" },
            { "$sort": { "summary.loginTime": -1 } },
            { "$skip": parseInt(skip) },
            { "$limit": parseInt(limit) },
            {
              "$project": {
                "_id": 0,
                "summary": "$summary",
              },
            },
          ],
        },
      });

      jsonData = await this.adapter.model.aggregate(query);
    }

    const responseJson = {};
    responseJson["totalDateCount"] =
      (data && data.length > 0 && data[0].summary && data[0].summary.length) ||
      0;

    responseJson["todayScanCount"] =
      (data &&
        data.length > 0 &&
        data[0].currentData &&
        data[0].currentData.scanData &&
        data[0].currentData.scanData.length) ||
      0;
    const dataSet =
      data &&
      data.length > 0 &&
      data[0].currentData &&
      data[0].currentData.scanData &&
      data[0].currentData.scanData.length > 0
        ? [{ "summary": data[0].currentData }]
        : [];

    responseJson["response"] =
      data && data.length > 0 && data[0].summary
        ? jsonData[0].response && jsonData[0].response.length > 0
          ? [...dataSet, ...jsonData[0].response]
          : []
        : [];

    return {
      "code": 200,
      "responseJson": responseJson,
      "message": "",
    };
  } else {
    const checkData = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.locationId),
      "gaurd.inactive": {
        "$elemMatch": {
          "_id": mongoose.Types.ObjectId(context.meta.userId),
        },
      },
    });
    if (!checkData) {
      throw new MoleculerError("INVALID DATA");
    }
    const data = await checkData.gaurd.inactive.filter(
      (data) => data._id.toString() === context.meta.userId.toString()
    );
    const query = [];
    let jsonData;
    if (data && data.length > 0) {
      query.push({
        "$match": {
          "_id": mongoose.Types.ObjectId(context.params.locationId),
          "gaurd.inactive": {
            "$elemMatch": {
              "_id": mongoose.Types.ObjectId(context.meta.userId),
            },
          },
        },
      });

      query.push({
        "$facet": {
          "response": [
            {
              "$project": {
                "_id": "$_id",
                "gaurd": {
                  "$filter": {
                    "input": "$gaurd.inactive",
                    "as": "item",
                    "cond": {
                      "$eq": [
                        "$$item._id",
                        mongoose.Types.ObjectId(context.meta.userId),
                      ],
                    },
                  },
                },
              },
            },
            {
              "$unwind": "$gaurd",
            },
            {
              "$project": {
                "summary": "$gaurd.summary",
              },
            },
            { "$unwind": "$summary" },
            { "$sort": { "summary.loginTime": -1 } },
            { "$skip": parseInt(skip) },
            { "$limit": parseInt(limit) },
            {
              "$project": {
                "_id": 0,
                "summary": "$summary",
              },
            },
          ],
        },
      });

      jsonData = await this.adapter.model.aggregate(query);
    }

    const responseJson = {};
    responseJson["totalDateCount"] =
      (data && data.length > 0 && data[0].summary && data[0].summary.length) ||
      0;

    responseJson["todayScanCount"] = 0;
    responseJson["response"] =
      data && data.length > 0 && data[0].summary
        ? jsonData[0].response && jsonData[0].response.length > 0
          ? jsonData[0].response
          : []
        : [];

    return {
      "code": 200,
      "responseJson": responseJson,
      "message": "",
    };
  }
};
module.exports = gaurdWatchAction;
