const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { convertToFloat, convertToInt } = require("../utils/common.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const zmapAction = {};

zmapAction.getAutocomplete = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    userId,
    professionalId,
    languageCode;
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.longCode || languageCode);
  try {
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

zmapAction.getDirections = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    userId,
    professionalId,
    languageCode;
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.longCode || languageCode);
  try {
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

zmapAction.getGeocode = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    userId,
    professionalId,
    languageCode;
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.longCode || languageCode);
  try {
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

zmapAction.getDistanceMatrix = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    userId,
    professionalId,
    languageCode;
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.longCode || languageCode);
  try {
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

zmapAction.getSnapway = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    userId,
    professionalId,
    languageCode;
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.longCode || languageCode);
  try {
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

module.exports = zmapAction;
