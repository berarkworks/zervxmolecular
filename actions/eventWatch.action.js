const { customAlphabet } = require("nanoid");
const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");

const eventWatchAction = {};

eventWatchAction.createEvent = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let contactsData = [];
  if (
    context.params.eventType === constantUtil.PRIVATE &&
    context.params.participants
  ) {
    contactsData = await this.broker.emit("user.checkUserAvailable", {
      "contacts": context.params.participants,
    });
    contactsData = contactsData[0].data;
  }
  let jsonData = await this.adapter.model.create({
    "eventId": `${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}`,
    "user": context.meta.userId || null,
    "eventName": context.params.eventName,
    "description": context.params.description,
    "eventDate": context.params.eventDate,
    "eventLocation": {
      "addressName": context.params.eventLocationAddressName,
      "fullAddress": context.params.eventLocationFullAddress,
      "shortAddress": context.params.eventLocationShortAddress,
      "lat": context.params.eventLocationLat,
      "lng": context.params.eventLocationLng,
    },
    "eventType": context.params.eventType,
    "participants": contactsData,
    "eventStatus": context.params.eventStatus,
    "status": context.params.status,
  });
  if (!jsonData) {
    throw new MoleculerError("INVALID EVENT DATA", 500);
  }
  const value = {
    "_id": jsonData._id,
    "eventId": jsonData.eventId,
    "user": jsonData.userId,
    "eventName": jsonData.eventName,
    "description": jsonData.description,
    "eventDate": jsonData.eventDate,
    "eventLocation": {
      "addressName": jsonData.eventLocation.addressName,
      "fullAddress": jsonData.eventLocation.fullAddress,
      "shortAddress": jsonData.eventLocation.shortAddress,
      "lat": jsonData.eventLocation.lat,
      "lng": jsonData.eventLocation.lng,
    },
    "eventType": jsonData.eventType,
    "eventStatus": jsonData.eventStatus,
    "status": jsonData.status,
    "eventLink":
      generalSettings.data.siteUrl +
      "api/eventWatch/viewDetails/" +
      jsonData._id.toString(),
  };
  jsonData = await this.adapter.model.updateOne(
    {
      "_id": jsonData._id,
    },
    {
      "eventLink":
        generalSettings.data.siteUrl +
        "api/eventWatch/viewDetails/" +
        jsonData._id.toString(),
    }
  );
  if (!jsonData || jsonData.nModified === 0) {
    throw new MoleculerError("ERROR IN EVENT CREATE", 500);
  } else {
    return {
      "code": 200,
      "data": value,
      "message": "EVENT ADDED SUCCESSFULLY",
    };
  }
};

eventWatchAction.addPraticipants = async function (context) {
  const contactsData = {
    "_id": context.meta.userId,
    "status": context.params.status,
  };
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.eventId),
    },
    { "$push": { "participants": contactsData } }
  );
  if (jsonData.nModified === 0) {
    throw new MoleculerError("ERROR IN PARICIPANTS JOIN", 500);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "PARICIPANTS ADDED SUCCESSFULLY",
    };
  }
};

eventWatchAction.praticipantsStatusChange = async function (context) {
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.eventId),
      "participants._id": context.meta.userId,
    },
    {
      "$set": { "participants.$.status": context.params.status },
    }
  );
  if (jsonData.nModified === 0) {
    throw new MoleculerError("ERROR IN PARICIPANTS STATUS CHANGE", 500);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "PARICIPANTS STATUS CHANGED SUCCESSFULLY",
    };
  }
};

eventWatchAction.eventStatusChange = async function (context) {
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.eventId),
      "user": mongoose.Types.ObjectId(context.meta.userId),
    },
    {
      "$set": {
        "status": context.params.status,
        "eventStatus": context.params.status,
      },
    }
  );
  if (jsonData.nModified === 0) {
    throw new MoleculerError("ERROR IN EVENT STATUS CHANGE", 500);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "EVENT STATUS CHANGE SUCCESSFULLY",
    };
  }
};

eventWatchAction.editEvent = async function (context) {
  let contactsData = [];
  if (
    context.params.eventType === constantUtil.PRIVATE &&
    context.params.participants
  ) {
    contactsData = await this.broker.emit("user.checkUserAvailable", {
      "contacts": context.params.participants,
    });
    contactsData = contactsData[0].data;
  }
  const eventData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.eventId),
      "user": mongoose.Types.ObjectId(context.meta.userId),
    },
    { "participants": 1 }
  );
  if (!eventData) {
    throw new MoleculerError("ERROR IN EVENT EDIT", 500);
  }
  const contactJson = [...eventData.participants, ...contactsData];
  const jsonData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.eventId),
      "user": mongoose.Types.ObjectId(context.meta.userId),
    },
    {
      "$set": {
        "status": context.params.status,
        "eventStatus": context.params.eventStatus,
        "eventName": context.params.eventName,
        "description": context.params.description,
        "eventDate": context.params.eventDate,
        "eventLocation": {
          "addressName": context.params.eventLocationAddressName,
          "fullAddress": context.params.eventLocationFullAddress,
          "shortAddress": context.params.eventLocationShortAddress,
          "lat": context.params.eventLocationLat,
          "lng": context.params.eventLocationLng,
        },
        "participants": contactJson,
      },
    }
  );
  if (jsonData.nModified === 0) {
    throw new MoleculerError("ERROR IN EVENT EDIT", 500);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "EVENT EDIT SUCCESSFULLY",
    };
  }
};

eventWatchAction.viewDetails = async function (context) {
  let responseData;
  const jsonData = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.eventId),
  });
  if (!jsonData) {
    throw new MoleculerError("ERROR IN EVENT EDIT", 500);
  }
  if (jsonData.eventType === constantUtil.PRIVATE) {
    let checkData = false;
    if (jsonData.user.toString() === context.meta.userId.toString()) {
      checkData = true;
    }
    await jsonData.participants.map(async (contacts) => {
      if (contacts._id.toString() === context.meta.userId.toString()) {
        checkData = true;
      }
    });
    if (!checkData) {
      throw new MoleculerError("ACCESS DENIED", 500);
    }
    let userDataSet = await this.broker.emit("user.getById", {
      "id": jsonData.user.toString(),
    });
    userDataSet = userDataSet && userDataSet[0];
    if (!userDataSet) {
      throw new MoleculerError("USER NOT FOUND", 500);
    }
    let userData = await this.broker.emit("user.getUsers", {
      "ids": jsonData.participants,
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("USER NOT FOUND", 500);
    } else {
      responseData = {
        "eventLocation": jsonData.eventLocation,
        "eventType": jsonData.eventType,
        "eventStatus": jsonData.eventStatus,
        "status": jsonData.status,
        "_id": jsonData._id,
        "user": {
          "_id": userDataSet._id,
          "firstName": userDataSet.firstName,
          "lastName": userDataSet.lastName,
          "code": userDataSet.phone.code,
          "number": userDataSet.phone.number,
        },
        "eventName": jsonData.eventName,
        "description": jsonData.description,
        "eventDate": jsonData.eventDate,
        "eventLink": jsonData.eventLink,
        "participants": userData,
      };
      return {
        "code": 200,
        "data": responseData,
        "message": "",
      };
    }
  } else {
    let userDataSet = await this.broker.emit("user.getById", {
      "id": jsonData.user.toString(),
    });
    userDataSet = userDataSet && userDataSet[0];
    if (!userDataSet) {
      throw new MoleculerError("USER NOT FOUND", 500);
    }
    let userData = await this.broker.emit("user.getUsers", {
      "ids": jsonData.participants,
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("USER NOT FOUND", 500);
    } else {
      responseData = {
        "eventLocation": jsonData.eventLocation,
        "eventType": jsonData.eventType,
        "eventStatus": jsonData.eventStatus,
        "status": jsonData.status,
        "_id": jsonData._id,
        "user": {
          "_id": userDataSet._id,
          "firstName": userDataSet.firstName,
          "lastName": userDataSet.lastName,
          "code": userDataSet.phone.code,
          "number": userDataSet.phone.number,
        },
        "eventName": jsonData.eventName,
        "description": jsonData.description,
        "eventDate": jsonData.eventDate,
        "eventLink": jsonData.eventLink,
        "participants": userData,
      };
      return {
        "code": 200,
        "data": responseData,
        "message": "",
      };
    }
  }
};

eventWatchAction.getYourEventList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      "user": mongoose.Types.ObjectId(context.meta.userId),
    },
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "code": "$user.phone.code",
              "number": "$user.phone.number",
            },
            "eventType": "$eventType",
            "eventStatus": "$eventStatus",
            "eventName": "$eventName",
            "eventLink": "$eventLink",
            "description": "$description",
            "eventDate": "$eventDate",
            "eventLocation": "$eventLocation",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query);

  return {
    "code": 200,
    "data": jsonData[0].response,
    "message": "",
  };
};

eventWatchAction.getParticipatedEventList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      "participants._id": mongoose.Types.ObjectId(context.meta.userId),
    },
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "code": "$user.phone.code",
              "number": "$user.phone.number",
            },
            "eventType": "$eventType",
            "eventStatus": "$eventStatus",
            "eventName": "$eventName",
            "description": "$description",
            "eventDate": "$eventDate",
            "eventLink": "$eventLink",
            "eventLocation": "$eventLocation",
            "currentStatus": {
              "$reduce": {
                "input": "$participants",
                "initialValue": constantUtil.NOTASSIGNED,
                "in": {
                  "$cond": {
                    "if": {
                      "$eq": [
                        "$$this._id",
                        mongoose.Types.ObjectId(context.meta.userId),
                      ],
                    },
                    "then": "$$this.status",
                    "else": "$$value",
                  },
                },
              },
            },
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query);

  return {
    "code": 200,
    "data": jsonData[0].response,
    "message": "",
  };
};

module.exports = eventWatchAction;
