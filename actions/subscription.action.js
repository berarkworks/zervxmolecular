const mongoose = require("mongoose");
const { MoleculerError } = require("moleculer").Errors;

const constantUtil = require("../utils/constant.util");

const subscriptionAction = {};

subscriptionAction.getAllSubscription = async function (context) {
  const userType = context.params.userType.toUpperCase();
  let userData, professionalData;
  if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("INVALID USER DETAIL");
    }
  } else {
    professionalData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(),
    });
    professionalData = professionalData && professionalData[0];
    if (!professionalData) {
      throw new MoleculerError("INVALID PROFESSIONAL DETAIL");
    }
  }
  const subscriptionData = await this.adapter.model
    .find({
      "status": constantUtil.ACTIVE,
    })
    .sort({ "_id": -1 });
  if (subscriptionData && subscriptionData.length === 0)
    return {
      "code": 200,
      "data": [],
      "message": "THERE IS NO SUBSCRIPTION AVAILABLE",
    };

  const response = subscriptionData.map((respo) => ({
    "_id": respo._id,
    "name": respo.name,
    "hint": respo.hint,
    "description": respo.description,
    "expireInText": respo.expireInText,
    "expireDays": respo.expireDays,
    "price": respo.price,
    "status": respo.status,
    "currencySymbol": respo.currencySymbol,
    "currencyCode": respo.currencyCode,
    "markAsPopular": respo.markAsPopular,
    "subscribed":
      userType === constantUtil.USER
        ? userData.subscriptionInfo.planId &&
          userData.subscriptionInfo.planId.toString() == respo._id.toString()
          ? true
          : false
        : professionalData.subscriptionInfo.planId &&
          professionalData.subscriptionInfo.planId.toString() ==
            respo._id.toString()
        ? true
        : false,
    "services": respo.services,
  }));

  return {
    "code": 200,
    "data": response,
    "message": "",
  };
};

subscriptionAction.addSubscriptionPlan = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const subscriptionData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.subscriptionId),
      "status": constantUtil.ACTIVE,
    })
    .lean();
  if (!subscriptionData) {
    throw new MoleculerError("INVALID SUBSCRIPTION PLAN");
  }
  const subscribedEndAt =
    Date.now() + parseInt(subscriptionData.expireDays) * 1000 * 60 * 60 * 24;
  let responseData;
  let dataSet = {};
  if (userType === constantUtil.USER) {
    let userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("INVALID USER DETAIL");
    }
    dataSet = userData;
    if (
      parseFloat(userData.wallet.availableAmount) >
      parseFloat(subscriptionData.price)
    ) {
      responseData = await this.broker.emit("user.updateSubscriptionPlan", {
        "userId": context.meta.userId,
        "planId": subscriptionData._id.toString(),
        "subscribedAt": new Date().toISOString(),
        "subscribedEndAt": new Date(subscribedEndAt).toISOString(),
        "subscribedAtTimestamp": Date.now(),
        "subscribedEndAtTimestamp": subscribedEndAt,
        "price":
          parseFloat(userData.wallet.availableAmount) -
          parseFloat(subscriptionData.price),
      });
    } else {
      throw new MoleculerError(
        "KINDLY RECHARGE YOUR WALLET TO PROCED SUBSCRIPTION"
      );
    }
  } else {
    let professionalData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(),
    });
    professionalData = professionalData && professionalData[0];
    if (!professionalData) {
      throw new MoleculerError("INVALID PROFESSIONAL DETAIL");
    }
    dataSet = professionalData;
    if (
      parseFloat(professionalData.wallet.availableAmount) >
      parseFloat(subscriptionData.price)
    ) {
      responseData = await this.broker.emit(
        "professional.updateSubscriptionPlan",
        {
          "professionalId": context.meta.professionalId,
          "planId": subscriptionData._id.toString(),
          "subscribedAt": new Date().toISOString(),
          "subscribedEndAt": new Date(subscribedEndAt).toISOString(),
          "subscribedAtTimestamp": Date.now(),
          "subscribedEndAtTimestamp": subscribedEndAt,
          "price":
            parseFloat(professionalData.wallet.availableAmount) -
            parseFloat(subscriptionData.price),
        }
      );
    } else {
      throw new MoleculerError(
        "KINDLY RECHARGE YOUR WALLET TO PROCED SUBSCRIPTION"
      );
    }
  }
  this.broker.emit("transaction.subscriptionTransaction", {
    "clientId": context.params?.clientId,
    "amount": parseFloat(subscriptionData.price.toFixed(2)),
    "userType": userType,
    "data": {
      "id": dataSet._id.toString(),
      "firstName": dataSet.firstName,
      "lastName": dataSet.lastName,
      "avatar": dataSet.avatar,
      "phone": {
        "code": dataSet.phone.code,
        "number": dataSet.phone.number,
      },
    },
  });
  return { "code": 200, "data": responseData[0], "message": "" };
};

subscriptionAction.getSubscriptionPlanDetail = async function (context) {
  const subscriptionData = await this.adapter.findOne({
    "_id": mongoose.Types.ObjectId(context.params.subscriptionId),
    "status": constantUtil.ACTIVE,
  });
  if (!subscriptionData) {
    throw new MoleculerError("INVALID SUBSCRIPTION PLAN");
  } else {
    return { "code": 200, "data": subscriptionData, "message": "" };
  }
};

subscriptionAction.getAllServiceList = async function (context) {
  const responseData = await this.broker.call("admin.getSecurityServiceList", {
    "_id": mongoose.Types.ObjectId(context.params.subscriptionId),
    "status": constantUtil.ACTIVE,
  });
  if (!responseData) {
    throw new MoleculerError("INVALID SUBSCRIPTION PLAN");
  } else {
    return { "code": 200, "data": responseData, "message": "" };
  }
};

module.exports = subscriptionAction;
