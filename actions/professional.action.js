const { customAlphabet } = require("nanoid");
const { MoleculerError, MoleculerClientError, ValidationError } =
  require("moleculer").Errors;
const moment = require("moment");
const mongoose = require("mongoose");
const _ = require("lodash");
const QRCode = require("qrcode");

const cryptoUtils = require("../utils/crypto.util");
const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const helperUtil = require("../utils/helper.util");
const mappingUtils = require("../utils/mapping.util");
const {
  professionalObject,
  professionalWelcomeMailObject,
  otpMailObject,
} = require("../utils/mapping.util");
const { forEach, replace } = require("lodash");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const {
  convertToFloat,
  convertToInt,
  trimPrefixZero,
} = require("../utils/common.util");

const professionalAction = {};
const otpLength = 6;

// professionalAction.validateLoginProfessional = async function (context) {
//   let generalSettings = null,
//     clientId = null,
//     professionalData = null;
//   const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
//   const clientIp = context.meta?.clientIp;
//   const secretKey = context.params.secretKey?.trim();

//   const {
//     INVALID_PROFESSIONAL,
//     INFO_OTP_MESSAGE,
//     INFO_IP_BLOCK,
//     ERROR_GENEREL_SETTINGS,
//     INFO_SUCCESS,
//   } = notifyMessage.setNotifyLanguage(context.params.langCode);

//   // #region IP Validation
//   let isValidIP = false,
//     isValidatedPhoneNo = false,
//     message = "";
//   //
//   const WINDOW_DURATION_IN_HOURS = 1; // Hour
//   const MAX_WINDOW_REQUEST_COUNT = 100; // 5; // Max Allowed request Count
//   const WINDOW_LOG_DURATION_IN_HOURS = 1;

//   const currentTime = Date.now();
//   const phoneCode = context.params.phoneCode.trim();
//   const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());
//   if (osType === constantUtil.CONST_ANDROID_OS) {
//     if (context.params.userName) {
//       try {
//         const decodedPhoneNo = cryptoUtils.cryptoAESDecode(
//           context.params.userName
//         );
//         if (decodedPhoneNo === phoneCode + phoneNumber) {
//           isValidatedPhoneNo = true;
//         } else {
//           isValidatedPhoneNo = false;
//         }
//       } catch (e) {
//         isValidatedPhoneNo = false;
//       }
//     } else {
//       isValidatedPhoneNo = true;
//     }
//   } else {
//     isValidatedPhoneNo = true;
//   }
//   // For Initial App Loading
//   const generalConfigList = await storageUtil.read(constantUtil.GENERALSETTING);
//   // if (!clientId) {
//   if (secretKey && generalConfigList.length > 0) {
//     Object.values(generalConfigList)
//       .filter(
//         (each) =>
//           each.name === constantUtil.GENERALSETTING &&
//           each.data.secretKey === secretKey
//       )
//       .forEach((item) => {
//         generalSettings = item;
//       });
//     // Client Id
//     clientId = generalSettings.data.clientId;
//   } else {
//     return {
//       "code": 600,
//       "message": "Invalid App SecretKey.",
//       "data": {},
//     };
//   }
//   if (generalSettings.data.isEnableIPRestriction && clientIp) {
//     // if (clientIp) {
//     try {
//       let recordData = await storageUtil.read(clientIp);
//       //When there is no user record then a new record is created for the user and stored in the Redis storage
//       if (recordData === null) {
//         const newRecord = [];
//         const requestLog = {
//           "requestTimeStamp": currentTime,
//           "userType": constantUtil.USER,
//           "requestCount": 1,
//         };
//         newRecord.push(requestLog);
//         await storageUtil.write(clientIp, newRecord);
//         recordData = newRecord;
//       }
//       //When the record is found then its value is parsed and the number of requests the user has made within the last window is calculated
//       const windowBeginTimestamp = new Date()
//         .setHours(new Date().getHours() - WINDOW_DURATION_IN_HOURS)
//         .valueOf();
//       const newRecordData = recordData?.filter((entry) => {
//         return entry.requestTimeStamp > windowBeginTimestamp;
//       });
//       const requestsinWindow = recordData?.filter((entry) => {
//         return (
//           entry.requestTimeStamp > windowBeginTimestamp &&
//           entry.userType === constantUtil.USER
//         );
//       });
//       // console.log("requestsinWindow", requestsinWindow);
//       const totalWindowRequestsCount = requestsinWindow?.reduce(
//         (accumulator, entry) => {
//           return accumulator + entry.requestCount;
//         },
//         0
//       );
//       //if maximum number of requests is exceeded then an error is returned
//       if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
//         isValidIP = false;
//         message = INFO_IP_BLOCK;
//         // "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
//       } else {
//         isValidIP = true;
//         //When the number of requests made are less than the maximum the a new entry is logged
//         const lastRequestLog = recordData[recordData?.length - 1];

//         const potentialCurrentWindowIntervalStartTimeStamp = new Date()
//           .setHours(new Date().getHours() - WINDOW_LOG_DURATION_IN_HOURS)
//           .valueOf();
//         //When the interval has not passed from the last request, then the counter increments
//         if (
//           lastRequestLog.requestTimeStamp ===
//           potentialCurrentWindowIntervalStartTimeStamp
//         ) {
//           lastRequestLog.requestCount++;
//           recordData[recordData.length - 1] = lastRequestLog;
//         } else {
//           //When the interval has passed, a new entry for current user and timestamp is logged
//           newRecordData.push({
//             "requestTimeStamp": currentTime,
//             "userType": constantUtil.USER,
//             "requestCount": 1,
//           });
//           recordData = newRecordData;
//         }
//         await storageUtil.write(clientIp, recordData);
//       }
//     } catch (error) {
//       isValidIP = false;
//       message = INVALID_PROFESSIONAL;
//     }
//   } else {
//     isValidIP = true;
//   }
//   // #endregion IP Validation
//   if (
//     isValidIP &&
//     isValidatedPhoneNo &&
//     (osType === constantUtil.CONST_ANDROID_OS ||
//       osType === constantUtil.CONST_IOS ||
//       osType === constantUtil.CONST_WEB_OS)
//   ) {
//     professionalData = await this.adapter.model
//       .findOne(
//         {
//           "phone.code": phoneCode,
//           "phone.number": phoneNumber,
//           "clientId": mongoose.Types.ObjectId(clientId),
//         },
//         { "_id": 1, "clientId": 1 }
//       )
//       .lean();
//     if (professionalData) {
//       // if (professionalData?.clientId) {
//       return {
//         "code": 200,
//         "message": INFO_SUCCESS,
//         "data": {}, // "data": { "clientId": professionalData?.clientId?.toString() },
//       };
//       // } else {
//       //   //Update & Get Client Data
//       //   professionalData = await this.adapter.model
//       //     .findOneAndUpdate(
//       //       { "_id": mongoose.Types.ObjectId(professionalData?._id?.toString()) },
//       //       { "$set": { "clientId": clientId } },
//       //       { "new": true, "fields": { "_id": 1, "clientId": 1 } }
//       //     )
//       //     .lean();
//       //   return {
//       //     "code": 200,
//       //     "message": INFO_SUCCESS,
//       //     "data": {}, // "data": { "clientId": clientId },
//       //   };
//       // }
//     } else {
//       professionalData = await this.adapter.model
//         .findOne(
//           {
//             "phone.code": phoneCode,
//             "phone.number": phoneNumber,
//             // "clientId": { "$ne": [null, "", undefined] },
//             // "clientId": { $exists: false },
//             "$and": [{ "clientId": { "$exists": false }, "clientId": null }],
//           },
//           { "_id": 1, "clientId": 1 }
//         )
//         .lean();
//       if (professionalData) {
//         //Update & Get Client Data
//         professionalData = await this.adapter.model
//           .findOneAndUpdate(
//             {
//               "_id": mongoose.Types.ObjectId(professionalData?._id?.toString()),
//             },
//             { "$set": { "clientId": clientId } },
//             { "new": true, "fields": { "_id": 1, "clientId": 1 } }
//           )
//           .lean();
//         // return {
//         //   "code": 200,
//         //   "message": INFO_SUCCESS,
//         //   "data": {}, // "data": { "clientId": clientId },
//         // };
//       }
//       return {
//         "code": 200,
//         "message": INFO_SUCCESS,
//         "data": {},
//       };
//     }
//   } else {
//     return {
//       "code": 600,
//       "message": message,
//       "data": {},
//     };
//   }
// };

professionalAction.login = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const clientId = context.params.clientId || context.meta?.clientId;
  const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
  const clientIp = context.meta?.clientIp;
  context.params.otp = customAlphabet("1234567890", otpLength)();
  const paymentMode = generalSettings.data.paymentMode || [];
  const emailOTP = customAlphabet("1234567890", otpLength)();

  const {
    INVALID_PROFESSIONAL,
    INFO_OTP_MESSAGE,
    INFO_IP_BLOCK,
    ERROR_GENEREL_SETTINGS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode);

  // #region IP Validation
  let isValidIP = false,
    isValidatedPhoneNo = false,
    message = "";
  //
  const WINDOW_DURATION_IN_HOURS = 1; // Hour
  const MAX_WINDOW_REQUEST_COUNT = 5; // Max Allowed request Count
  const WINDOW_LOG_DURATION_IN_HOURS = 1;

  const currentTime = Date.now();
  const phoneCode = context.params.phoneCode.trim();
  const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());
  if (osType === constantUtil.CONST_ANDROID_OS) {
    if (context.params.userName) {
      try {
        const decodedPhoneNo = cryptoUtils.cryptoAESDecode(
          context.params.userName
        );
        if (decodedPhoneNo === phoneCode + phoneNumber) {
          isValidatedPhoneNo = true;
        } else {
          isValidatedPhoneNo = false;
        }
      } catch (e) {
        isValidatedPhoneNo = false;
      }
    } else {
      isValidatedPhoneNo = true;
    }
  } else {
    isValidatedPhoneNo = true;
  }
  if (generalSettings.data.isEnableIPRestriction && clientIp) {
    try {
      let recordData = await storageUtil.read(clientIp);
      //When there is no user record then a new record is created for the user and stored in the Redis storage
      if (recordData === null) {
        const newRecord = [];
        const requestLog = {
          "requestTimeStamp": currentTime,
          "userType": constantUtil.PROFESSIONAL,
          "requestCount": 1,
        };
        newRecord.push(requestLog);
        await storageUtil.write(clientIp, newRecord);
        recordData = newRecord;
      }
      //When the record is found then its value is parsed and the number of requests the user has made within the last window is calculated
      const windowBeginTimestamp = new Date()
        .setHours(new Date().getHours() - WINDOW_DURATION_IN_HOURS)
        .valueOf();
      const newRecordData = recordData?.filter((entry) => {
        return entry.requestTimeStamp > windowBeginTimestamp;
      });
      const requestsinWindow = recordData?.filter((entry) => {
        return (
          entry.requestTimeStamp > windowBeginTimestamp &&
          entry.userType === constantUtil.PROFESSIONAL
        );
      });
      // console.log("requestsinWindow", requestsinWindow);
      const totalWindowRequestsCount = requestsinWindow?.reduce(
        (accumulator, entry) => {
          return accumulator + entry.requestCount;
        },
        0
      );
      //if maximum number of requests is exceeded then an error is returned
      if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
        isValidIP = false;
        message = INFO_IP_BLOCK;
        // "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
      } else {
        isValidIP = true;
        //When the number of requests made are less than the maximum the a new entry is logged
        const lastRequestLog = recordData[recordData?.length - 1];

        const potentialCurrentWindowIntervalStartTimeStamp = new Date()
          .setHours(new Date().getHours() - WINDOW_LOG_DURATION_IN_HOURS)
          .valueOf();
        //When the interval has not passed from the last request, then the counter increments
        if (
          lastRequestLog.requestTimeStamp ===
          potentialCurrentWindowIntervalStartTimeStamp
        ) {
          lastRequestLog.requestCount++;
          recordData[recordData.length - 1] = lastRequestLog;
        } else {
          //When the interval has passed, a new entry for current user and timestamp is logged
          newRecordData.push({
            "requestTimeStamp": currentTime,
            "userType": constantUtil.PROFESSIONAL,
            "requestCount": 1,
          });
          recordData = newRecordData;
        }
        await storageUtil.write(clientIp, recordData);
      }
    } catch (error) {
      isValidIP = false;
      message = INVALID_PROFESSIONAL;
    }
  } else {
    isValidIP = true;
  }
  // #endregion IP Validation
  if (
    isValidIP &&
    isValidatedPhoneNo &&
    (osType === constantUtil.CONST_ANDROID_OS ||
      osType === constantUtil.CONST_IOS ||
      osType === constantUtil.CONST_WEB_OS)
  ) {
    let isEmailVerified = false;
    let professionalData = await this.adapter.model.countDocuments({
      //"clientId": mongoose.Types.ObjectId(clientId),
      "phone.code": phoneCode,
      "phone.number": phoneNumber,
    });

    if (professionalData) {
      professionalData = await this.adapter.model.findOneAndUpdate(
        {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "phone.code": phoneCode,
          "phone.number": phoneNumber,
        }, // No need to Add Status Based Condition Will handle in APP
        {
          "otp": cryptoUtils.hashSecret(context.params.otp),
          "emailOTP": cryptoUtils.hashSecret(emailOTP),
          "ipAddress": clientIp,
        },
        { "new": true }
      );
      isEmailVerified = professionalData.isEmailVerified || false;
    } else {
      const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
        "quality": 50,
        "width": 500,
      });

      let finalImageData = await this.broker.emit(
        "admin.uploadStaticMapImageInSpaces",
        {
          "files": qrImage,
          "fileName": `professional${phoneCode + "" + phoneNumber}QrImage`,
        }
      );
      finalImageData = finalImageData && finalImageData[0];

      professionalData = await this.adapter.model.create({
        "clientId": clientId,
        "isNewUser": true,
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "nationalIdNo": context.params.nationalIdNo,
        "nationalInsuranceNo": context.params.nationalInsuranceNo,
        "paymentMode": paymentMode,
        "knownLanguages": [],
        "otp": cryptoUtils.hashSecret(context.params.otp),
        "emailOTP": cryptoUtils.hashSecret(emailOTP),
        "isEmailVerified": context.params.isEmailVerified || false,
        "password": cryptoUtils.passwordEncode(context.params.password),
        "scanQr": finalImageData,
        "ipAddress": clientIp,
        "createdBy": context.params.loginUserId,
      });
      // notify to admin
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": constantUtil.NEW,
          "timestamp": Date.now(),
          "message": "NEW PROFESSIONAL",
          "details": {},
        },
        "registrationTokens": [
          {
            "token": "",
            "id": professionalData?._id?.toString(),
            "deviceType": "",
            "platform": "",
            "socketId": "",
            "userType": constantUtil.ADMIN,
          },
        ],
      };
      this.broker.emit(
        "socket.sendSecurityNotificationToAdmin",
        notificationObject
      );
    }
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    if (
      generalSettings.data.emailConfigurationEnable &&
      professionalData.email &&
      isEmailVerified
    ) {
      this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "OtpForVerification",
        ...otpMailObject({
          ...professionalData.toJSON(),
          "otp": context.params.otp,
          "emailOTP": emailOTP,
        }),
      });
    }
    // send message
    if (generalSettings.data.smsConfigurationEnable) {
      let otpNotifyMessage = smsSetting.data.notifyMessage || INFO_OTP_MESSAGE;
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{siteTitle}}/g,
        generalSettings.data.siteTitle
      );
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{OTP}}/g,
        context.params.otp
      );
      //send sms
      this.broker.emit("admin.sendSMS", {
        "clientId": clientId,
        "phoneNumber": `${phoneCode}${phoneNumber}`,
        // "message": context.params.otp,
        "message": otpNotifyMessage,
        "notifyType": constantUtil.OTP,
        "otpReceiverAppName": constantUtil.SMS,
      });
    }
    return {
      "code": 200,
      "message": "OTP sended successfully",
      "OTP":
        smsSetting.data.mode === constantUtil.DEVELOPMENT
          ? context.params.otp
          : "",
      "otpLength": otpLength,
      "isOtpByPass": professionalData.isOtpBypass,
    };
  } else {
    context.params.otp = customAlphabet("1234567890", otpLength)(); //For Invalid OTP Purpose
    return {
      "code": 400,
      "message": message,
      "OTP":
        smsSetting.data.mode === constantUtil.DEVELOPMENT
          ? context.params.otp
          : "",
      "otpLength": otpLength,
      "isOtpByPass": false,
    };
  }
};

professionalAction.loginWithPhoneNo = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
  const clientIp = context.meta?.clientIp;
  context.params.otp = customAlphabet("1234567890", otpLength)();
  const paymentMode = generalSettings.data.paymentMode || [];
  const emailOTP = customAlphabet("1234567890", otpLength)();

  const { INVALID_PROFESSIONAL, INFO_OTP_MESSAGE, INFO_IP_BLOCK } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  // #region IP Validation
  let isValidIP = false,
    isValidatedPhoneNo = false,
    message = "";
  //
  const WINDOW_DURATION_IN_HOURS = 1; // Hour
  const MAX_WINDOW_REQUEST_COUNT = 5; // Max Allowed request Count
  const WINDOW_LOG_DURATION_IN_HOURS = 1;

  const currentTime = Date.now();
  const phoneCode = context.params.phoneCode.trim();
  // const phoneNumber = trimPrefixZero(context.params.phoneNumber.trim());
  const phoneNumber = context.params.phoneNumber.trim();
  const otpReceiverAppName = context.params.otpReceiverAppName;
  if (osType === constantUtil.CONST_ANDROID_OS) {
    try {
      const decodedPhoneNo = cryptoUtils.cryptoAESDecode(
        context.params.userName
      );
      if (decodedPhoneNo === phoneCode + phoneNumber) {
        isValidatedPhoneNo = true;
      } else {
        isValidatedPhoneNo = false;
      }
    } catch (e) {
      isValidatedPhoneNo = false;
    }
  } else {
    isValidatedPhoneNo = true;
  }
  if (generalSettings.data.isEnableIPRestriction && clientIp) {
    try {
      let recordData = await storageUtil.read(clientIp);
      //When there is no user record then a new record is created for the user and stored in the Redis storage
      if (recordData === null) {
        const newRecord = [];
        const requestLog = {
          "requestTimeStamp": currentTime,
          "userType": constantUtil.PROFESSIONAL,
          "requestCount": 1,
        };
        newRecord.push(requestLog);
        await storageUtil.write(clientIp, newRecord);
        recordData = newRecord;
      }
      //When the record is found then its value is parsed and the number of requests the user has made within the last window is calculated
      const windowBeginTimestamp = new Date()
        .setHours(new Date().getHours() - WINDOW_DURATION_IN_HOURS)
        .valueOf();
      const newRecordData = recordData?.filter((entry) => {
        return entry.requestTimeStamp > windowBeginTimestamp;
      });
      const requestsinWindow = recordData?.filter((entry) => {
        return (
          entry.requestTimeStamp > windowBeginTimestamp &&
          entry.userType === constantUtil.PROFESSIONAL
        );
      });
      // console.log("requestsinWindow", requestsinWindow);
      const totalWindowRequestsCount = requestsinWindow?.reduce(
        (accumulator, entry) => {
          return accumulator + entry.requestCount;
        },
        0
      );
      //if maximum number of requests is exceeded then an error is returned
      if (totalWindowRequestsCount >= MAX_WINDOW_REQUEST_COUNT) {
        isValidIP = false;
        message = INFO_IP_BLOCK;
        // "Your account has been locked because you have reached the maximum number of invalid logon attempts.";
      } else {
        isValidIP = true;
        //When the number of requests made are less than the maximum the a new entry is logged
        const lastRequestLog = recordData[recordData?.length - 1];

        const potentialCurrentWindowIntervalStartTimeStamp = new Date()
          .setHours(new Date().getHours() - WINDOW_LOG_DURATION_IN_HOURS)
          .valueOf();
        //When the interval has not passed from the last request, then the counter increments
        if (
          lastRequestLog.requestTimeStamp ===
          potentialCurrentWindowIntervalStartTimeStamp
        ) {
          lastRequestLog.requestCount++;
          recordData[recordData.length - 1] = lastRequestLog;
        } else {
          //When the interval has passed, a new entry for current user and timestamp is logged
          newRecordData.push({
            "requestTimeStamp": currentTime,
            "userType": constantUtil.PROFESSIONAL,
            "requestCount": 1,
          });
          recordData = newRecordData;
        }
        await storageUtil.write(clientIp, recordData);
      }
    } catch (error) {
      isValidIP = false;
      message = INVALID_PROFESSIONAL;
    }
  } else {
    isValidIP = true;
  }
  // #endregion IP Validation

  if (
    isValidIP &&
    isValidatedPhoneNo &&
    (osType === constantUtil.CONST_ANDROID_OS ||
      osType === constantUtil.CONST_IOS ||
      osType === constantUtil.CONST_WEB_OS)
  ) {
    let isEmailVerified = false;
    let professionalData = await this.adapter.model.countDocuments({
      //"clientId": mongoose.Types.ObjectId(clientId),
      "phone.code": phoneCode,
      "phone.number": phoneNumber,
    });

    if (professionalData) {
      professionalData = await this.adapter.model.findOneAndUpdate(
        {
          "phone.code": phoneCode,
          "phone.number": phoneNumber,
          //"clientId": clientId,
        }, // No need to Add Status Based Condition Will handle in APP
        {
          "otpReceiverAppName": otpReceiverAppName,
          "otp": cryptoUtils.hashSecret(context.params.otp),
          "emailOTP": cryptoUtils.hashSecret(emailOTP),
          "ipAddress": clientIp,
        },
        { "new": true }
      );
      isEmailVerified = professionalData.isEmailVerified || false;
    } else {
      const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
        "quality": 50,
        "width": 500,
      });

      let finalImageData = await this.broker.emit(
        "admin.uploadStaticMapImageInSpaces",
        {
          "files": qrImage,
          "fileName": `professional${phoneCode + "" + phoneNumber}QrImage`,
        }
      );
      finalImageData = finalImageData && finalImageData[0];

      professionalData = await this.adapter.model.create({
        "clientId": clientId,
        "isNewUser": true,
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "otpReceiverAppName": otpReceiverAppName,
        "nationalIdNo": context.params.nationalIdNo,
        "nationalInsuranceNo": context.params.nationalInsuranceNo,
        "paymentMode": paymentMode,
        "knownLanguages": [],
        "otp": cryptoUtils.hashSecret(context.params.otp),
        "emailOTP": cryptoUtils.hashSecret(emailOTP),
        "isEmailVerified": context.params.isEmailVerified || false,
        "password": cryptoUtils.passwordEncode(context.params.password),
        "scanQr": finalImageData,
        "ipAddress": clientIp,
        "createdBy": context.params.loginUserId,
      });
      //  isEmailVerified = true;
      // notify to admin
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": constantUtil.NEW,
          "timestamp": Date.now(),
          "message": "NEW PROFESSIONAL",
          "details": {},
        },
        "registrationTokens": [
          {
            "token": "",
            "id": professionalData?._id?.toString(),
            "deviceType": "",
            "platform": "",
            "socketId": "",
            "userType": constantUtil.ADMIN,
          },
        ],
      };
      this.broker.emit(
        "socket.sendSecurityNotificationToAdmin",
        notificationObject
      );
    }
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    if (
      generalSettings.data.emailConfigurationEnable &&
      professionalData.email &&
      isEmailVerified
    ) {
      this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "OtpForVerification",
        ...otpMailObject({
          ...professionalData.toJSON(),
          "otp": context.params.otp,
          "emailOTP": emailOTP,
        }),
      });
    }
    // send message
    if (generalSettings.data.smsConfigurationEnable) {
      let otpNotifyMessage = smsSetting.data.notifyMessage || INFO_OTP_MESSAGE;
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{siteTitle}}/g,
        generalSettings.data.siteTitle
      );
      otpNotifyMessage = otpNotifyMessage.replace(
        /{{OTP}}/g,
        context.params.otp
      );
      //send sms
      this.broker.emit("admin.sendSMS", {
        "clientId": clientId,
        "phoneNumber": `${phoneCode}${phoneNumber}`,
        // "message": context.params.otp,
        "message": otpNotifyMessage,
        "notifyType": constantUtil.OTP,
        "otpReceiverAppName": otpReceiverAppName,
      });
    }
    return {
      "code": 200,
      "message": "OTP sended successfully",
      "data": {
        "message": "OTP sended successfully",
        "OTP":
          smsSetting.data.mode === constantUtil.DEVELOPMENT
            ? context.params.otp
            : "",
        "otpLength": otpLength,
        "otpReceiverAppName": otpReceiverAppName,
        "isOtpByPass": professionalData.isOtpBypass,
      },
    };
  } else {
    context.params.otp = customAlphabet("1234567890", otpLength)(); //For Invalid OTP Purpose
    return {
      "code": 400,
      "message": message,
      "data": {
        "message": message,
        "OTP":
          smsSetting.data.mode === constantUtil.DEVELOPMENT
            ? context.params.otp
            : "",
        "otpLength": otpLength,
        "otpReceiverAppName": otpReceiverAppName,
        "isOtpByPass": false,
      },
    };
  }
};

professionalAction.verifyOtp = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const { INVALID_PROFESSIONAL } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.professionalDetails?.languageCode
  );
  let checkData = {};
  let smsType = (smsSetting?.data?.smsType || "").toUpperCase();
  if (smsSetting?.data?.isEnableSandboxMode) {
    const sandboxPhoneCode = smsSetting?.data?.sandboxPhone?.code || "";
    const sandboxPhoneNumber = smsSetting?.data?.sandboxPhone?.number || "";
    if (
      sandboxPhoneCode === context.params.phoneCode &&
      sandboxPhoneNumber === context.params.phoneNumber
    ) {
      smsType = constantUtil.SANDBOX;
    }
  }
  switch (smsType) {
    case constantUtil.TWILIO:
    case constantUtil.WHATSUP:
    case constantUtil.SENDIFY:
    case constantUtil.TUNISIESMS:
      checkData = {
        //"clientId": clientId,
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
        "otp": cryptoUtils.hashSecret(context.params.otp),
      };
      break;

    // case constantUtil.SANDBOX:
    //   checkData = {
    //     "phone.code": context.params.phoneCode,
    //     "phone.number": context.params.phoneNumber,
    //   };
    //   break;

    default:
      checkData = {
        //"clientId": clientId,
        "phone.code": context.params.phoneCode,
        "phone.number": context.params.phoneNumber,
      };
      break;
  }
  const professionalData = await this.adapter.model.findOne(checkData).lean();
  if (!professionalData) {
    throw new MoleculerError(INVALID_PROFESSIONAL, "401", "AUTH_FAILED");
  }
  const accessToken = cryptoUtils.jwtSign({
    "_id": professionalData._id,
    "type": constantUtil.PROFESSIONAL,
    "deviceType": context.meta.userAgent.deviceType,
    "platform": context.meta.userAgent.platform,
  });
  let updatedProfessionalData = await this.adapter
    .updateById(mongoose.Types.ObjectId(professionalData._id.toString()), {
      "otp": "",
      "deviceInfo": [
        {
          "accessToken": accessToken,
          "deviceId": context.meta.userAgent.deviceId,
          "deviceType": context.meta.userAgent.deviceType,
          "platform": context.meta.userAgent.platform,
        },
      ],
      "status": professionalData.status || constantUtil.UNVERIFIED,
    })
    .populate("vehicles.hub");
  if (!updatedProfessionalData) {
    throw new MoleculerError(INVALID_PROFESSIONAL, 500);
  }
  updatedProfessionalData = updatedProfessionalData.toJSON();

  updatedProfessionalData.accessToken = accessToken;
  if (updatedProfessionalData.otpReceiverAppName) {
    updatedProfessionalData.newProfessional =
      updatedProfessionalData.firstName && updatedProfessionalData.lastName
        ? false
        : true;
  } else {
    updatedProfessionalData.newProfessional =
      updatedProfessionalData.email &&
      updatedProfessionalData.firstName &&
      updatedProfessionalData.lastName
        ? false
        : true;
  }

  // let driverDocumentList = await storageUtil.read(constantUtil.DRIVERDOCUMENTS);
  // if (!driverDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

  // driverDocumentList = await Object.keys(driverDocumentList)
  //   .map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     return el.status === constantUtil.ACTIVE;
  //   });

  // let profileDocumentList = await storageUtil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );
  // if (!profileDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  // profileDocumentList = Object.keys(profileDocumentList)
  //   .map((documentId) => ({
  //     ...profileDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     return el.status === constantUtil.ACTIVE;
  //   });

  // const nextStepDetails = {
  //   "profileVerification": true,
  //   "isVehicleDocumentsUploaded": true,
  //   "isHubDetailsPresent": true,
  //   "vehicleVerified": true,
  // };
  // const checkNextStepDetails = {
  //   "profileVerification": [],
  //   "isVehicleDocumentsUploaded": [],
  //   "vehicleVerified": [],
  // };

  // profileDocumentList.forEach(async (docs) => {
  //   if (
  //     profileDocumentList.length ===
  //     updatedProfessionalData.profileVerification.length
  //   ) {
  //     updatedProfessionalData.profileVerification.forEach((d) => {
  //       if (docs.docsName === d.documentName) {
  //         if (docs.docsExpiry === true) {
  //           const diffDays = parseInt(
  //             (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //           );
  //           if (
  //             d.uploadedStatus === constantUtil.UPLOADED &&
  //             d.status === constantUtil.VERIFIED
  //           ) {
  //             checkNextStepDetails.vehicleVerified.push("1");
  //           }
  //           if (diffDays <= 0) {
  //             checkNextStepDetails.profileVerification.push("1");
  //           }
  //         } else {
  //           if (
  //             d.uploadedStatus === constantUtil.UPLOADED &&
  //             d.status === constantUtil.VERIFIED
  //           ) {
  //             checkNextStepDetails.vehicleVerified.push("1");
  //           }
  //         }
  //       }
  //     });
  //   } else {
  //     checkNextStepDetails.profileVerification.push("1");
  //   }
  // });

  // const vehicleCategoryList = await storageUtil.read(
  //   constantUtil.VEHICLECATEGORY
  // );

  // await updatedProfessionalData.vehicles.map((vehicle) => {
  //   if (vehicle.status === constantUtil.INACTIVE)
  //     checkNextStepDetails.vehicleVerified.push("1");

  //   if (vehicle.vehicleDocuments.length > 0) {
  //     const docusvehi = vehicle.vehicleDocuments;
  //     driverDocumentList.forEach(async (docs) => {
  //       if (driverDocumentList.length === docusvehi.length) {
  //         docusvehi.forEach((c) => {
  //           if (docs.docsName === c.documentName) {
  //             if (docs.docsExpiry === true) {
  //               const diffDays = parseInt(
  //                 (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //               ); //gives day difference
  //               if (
  //                 c.uploadedStatus === constantUtil.UPLOADED &&
  //                 c.status === constantUtil.VERIFIED
  //               ) {
  //                 checkNextStepDetails.vehicleVerified.push("1");
  //               }
  //               if (diffDays <= 0) {
  //                 checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //               }
  //             } else {
  //               if (
  //                 c.uploadedStatus === constantUtil.UPLOADED &&
  //                 c.status === constantUtil.VERIFIED
  //               ) {
  //                 checkNextStepDetails.vehicleVerified.push("1");
  //               }
  //             }
  //           }
  //         });
  //       } else {
  //         checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //       }
  //     });
  //   } else {
  //     checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //   }

  //   // if (vehicle.hub && vehicle.scheduleDate)
  //   if (vehicle?.serviceCategory) nextStepDetails.isHubDetailsPresent = false;

  //   if (vehicle.vehicleCategoryId) {
  //     vehicle.vehicleCategoryName =
  //       vehicleCategoryList[vehicle.vehicleCategoryId].vehicleCategory;
  //   } else {
  //     vehicle.vehicleCategoryName = "";
  //   }
  // });

  // if (checkNextStepDetails.profileVerification.length === 0)
  //   nextStepDetails.profileVerification = false;

  // if (checkNextStepDetails.isVehicleDocumentsUploaded.length === 0)
  //   nextStepDetails.isVehicleDocumentsUploaded = false;

  // if (checkNextStepDetails.vehicleVerified.length === 0)
  //   nextStepDetails.vehicleVerified = false;

  // updatedProfessionalData.registrationNextStep =
  //   !updatedProfessionalData.newProfessional
  //     ? nextStepDetails.profileVerification
  //       ? constantUtil.REGISTRATIONSTEP2
  //       : updatedProfessionalData.vehicles.length === 0
  //       ? constantUtil.REGISTRATIONSTEP3
  //       : nextStepDetails.isVehicleDocumentsUploaded
  //       ? constantUtil.REGISTRATIONSTEP4
  //       : nextStepDetails.isHubDetailsPresent
  //       ? constantUtil.REGISTRATIONSTEP5
  //       : nextStepDetails.vehicleVerified
  //       ? constantUtil.REGISTRATIONSTEP6
  //       : constantUtil.REGISTRATIONCOMPLETED
  //     : constantUtil.REGISTRATIONSTEP1;

  // // if (updatedProfessionalData.newProfessional === true) {
  // //   updatedProfessionalData.registrationNextStep =
  // //     constantUtil.REGISTRATIONSTEP1;
  // // }
  // // // this menas old professionals
  // // if (updatedProfessionalData.newProfessional === false) {
  // //   // first registration step
  // //   if (nextStepDetails.profileVerification) {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONSTEP2;
  // //   }
  // //   // vehicle registration process
  // //   else if (!updatedProfessionalData.vehicles[0]) {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONSTEP1;
  // //   }
  // //   //
  // //   else if (nextStepDetails.isVehicleDocumentsUploaded) {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONSTEP4;
  // //   }
  // //   //
  // //   else if (nextStepDetails.isHubDetailsPresent) {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONSTEP5;
  // //   }
  // //   //
  // //   else if (nextStepDetails.vehicleVerified) {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONSTEP6;
  // //   }
  // //   //
  // //   else {
  // //     updatedProfessionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONCOMPLETED;
  // //   }
  // // }
  // // if (
  // //   professionalData.registrationNextStep === constantUtil.REGISTRATIONSTEP5
  // // ) {
  // //   if (gerenalsetting.data.isHubNeeded === false) {
  // //     professionalData.registrationNextStep = constantUtil.REGISTRATIONSTEP6;
  // //   }
  // // }

  // updatedProfessionalData.accessToken = accessToken;

  // updatedProfessionalData.vehicles = _.map(
  //   updatedProfessionalData.vehicles,
  //   (vehicle) => ({
  //     ...vehicle,
  //     "hubsName": vehicle?.hub?.data?.hubsName || "",
  //     "location": vehicle?.hub?.data?.location || "",
  //     "phone": vehicle?.hub?.data?.phone || "",
  //     "email": vehicle?.hub?.data?.email || "",
  //     "showAddress": vehicle?.hub?.data?.showAddress || "",
  //     "hub": vehicle?.hub?._id || vehicle?.hub,
  //   })
  // );
  // --------- Check Profile & Vehicle Document List Start -----------
  updatedProfessionalData = await this.broker.emit(
    "professional.checkProfessionalDocumentList",
    {
      ...updatedProfessionalData,
      "clientId": clientId,
    }
  );
  updatedProfessionalData =
    updatedProfessionalData && updatedProfessionalData[0];
  // --------- Check Profile & Vehicle Document List End -----------
  // ------------------ Log Entry Start --------------
  if (gerenalsetting?.data?.isEnableIncentive) {
    //---------------- Incentive List -----------------------
    let incentiveList = await this.broker.emit(
      "admin.checkAndValidateIncentiveRuleByServiceAreaId",
      {
        "userType": constantUtil.PROFESSIONAL,
        "clientId": clientId,
        "professionalId": professionalData._id.toString(),
        "serviceAreaId": professionalData?.serviceAreaId?.toString(),
      }
    );
    incentiveList = incentiveList && incentiveList[0];
    if (incentiveList) {
      //---------------- Incentive List -----------------------
      await this.broker.emit("log.insertSignupLog", {
        "clientId": clientId,
        "type": constantUtil.CONST_INCENTIVE,
        "name": constantUtil.LOGIN,
        "userType": constantUtil.PROFESSIONAL,
        "professionalId": professionalData._id.toString(),
      });
    }
    // ------------------ Log Entry End ----------------
  }
  const professionalObjectData = await professionalObject(
    updatedProfessionalData
  );
  return { "code": 200, "data": professionalObjectData };
};

professionalAction.verifyEmail = async function (context) {
  let professionalData = null,
    checkConditionData = {},
    updateFieldData = {},
    responseData,
    errorCode = 200,
    responseMessage = "";
  const action = context.params.action,
    professionalId = context.params.professionalId,
    clientId = context.params.clientId || context.meta?.clientId,
    emailOTP = customAlphabet("1234567890", otpLength)();
  //
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    INVALID_PROFESSIONAL,
    OTP_INCORRECT,
    SOMETHING_WENT_WRONG,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.userDetails?.languageCode
  );
  try {
    if (action === constantUtil.OTP) {
      const checkEmailCount = await this.adapter.model.countDocuments({
        "_id": {
          "$ne": mongoose.Types.ObjectId(professionalId),
        },
        "email": context.params.email,
      });
      if (checkEmailCount > 0) {
        throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
      }
      checkConditionData = {
        "_id": mongoose.Types.ObjectId(professionalId),
      };
      updateFieldData = {
        "emailOTP": cryptoUtils.hashSecret(emailOTP),
        "email": context.params.email,
        "isEmailVerified": false,
      };
    } else if (action === constantUtil.CONST_VERIFY) {
      checkConditionData = {
        "_id": mongoose.Types.ObjectId(professionalId),
        "emailOTP": cryptoUtils.hashSecret(context.params.otp),
      };
      updateFieldData = {
        "isEmailVerified": true,
      };
    }
    professionalData = await this.adapter.model
      .findOne(checkConditionData)
      .lean();
    if (!professionalData) {
      throw new MoleculerError(
        action === constantUtil.OTP ? INVALID_PROFESSIONAL : OTP_INCORRECT,
        400
      );
    } else {
      professionalData = await this.adapter.model
        .findOneAndUpdate(
          { ...checkConditionData },
          { ...updateFieldData },
          { "new": true }
        )
        .lean();
    }
    // if (!professionalData) {
    //   throw new MoleculerError(INVALID_PROFESSIONAL);
    // }
    // send mail
    if (
      generalSettings.data.emailConfigurationEnable &&
      action === constantUtil.OTP &&
      professionalData.email
    ) {
      await this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "emailVerification",
        ...otpMailObject({
          ...professionalData,
          "otp": "****" + emailOTP.substring(4, otpLength),
          "emailOTP": emailOTP,
        }),
      });
    }
    if (action === constantUtil.OTP) {
      responseData = { "OTP": emailOTP, "otpLength": otpLength };
    } else if (action === constantUtil.CONST_VERIFY) {
      responseData = await professionalObject(professionalData);
    }
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : INVALID_PROFESSIONAL;
    responseData = {};
  }
  // return { "code": 200, "data": responseData };
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

professionalAction.profile = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { INFO_PAYMENT_OPTIONS, INFO_INVALID_DETAILS } =
    notifyMessage.setNotifyLanguage(context.params.langCode);
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  let professionalData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .populate("vehicles.hub")
    .lean();

  // professionalData = professionalData.toJSON();

  // let driverDocumentList = await storageUtil.read(constantUtil.DRIVERDOCUMENTS);
  // if (!driverDocumentList)
  //   return { "code": 200, "data": {}, "message": "ERROR IN DRIVER DOCUMENTS" };

  // driverDocumentList = await Object.keys(driverDocumentList)
  //   .map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     return el.status === constantUtil.ACTIVE;
  //   });

  // let profileDocumentList = await storageUtil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );
  // if (!profileDocumentList)
  //   return { "code": 0, "data": {}, "message": "ERROR IN PROFILE DOCUMENTS" };

  // profileDocumentList = Object.keys(profileDocumentList)
  //   .map((documentId) => ({
  //     ...profileDocumentList[documentId],
  //     "id": documentId,
  //   }))
  //   .filter(function (el) {
  //     return el.status === constantUtil.ACTIVE;
  //   });

  // const nextStepDetails = {
  //   "profileVerification": true,
  //   "isVehicleDocumentsUploaded": true,
  //   "isHubDetailsPresent": true,
  //   "vehicleVerified": true,
  // };
  // const checkNextStepDetails = {
  //   "profileVerification": [],
  //   "isVehicleDocumentsUploaded": [],
  //   "vehicleVerified": [],
  // };

  // profileDocumentList.forEach(async (docs) => {
  //   if (
  //     profileDocumentList.length === professionalData.profileVerification.length
  //   ) {
  //     professionalData.profileVerification.forEach((d) => {
  //       if (docs.docsName === d.documentName) {
  //         if (docs.docsExpiry === true) {
  //           const diffDays = parseInt(
  //             (new Date(d.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //           );
  //           if (
  //             d.uploadedStatus === constantUtil.UPLOADED &&
  //             d.status === constantUtil.VERIFIED
  //           ) {
  //             checkNextStepDetails.vehicleVerified.push("1");
  //           }
  //           if (diffDays <= 0) {
  //             checkNextStepDetails.profileVerification.push("1");
  //           }
  //         } else {
  //           if (
  //             d.uploadedStatus === constantUtil.UPLOADED &&
  //             d.status === constantUtil.VERIFIED
  //           ) {
  //             checkNextStepDetails.vehicleVerified.push("1");
  //           }
  //         }
  //       }
  //     });
  //   } else {
  //     checkNextStepDetails.profileVerification.push("1");
  //   }
  // });

  // const vehicleCategoryList = await storageUtil.read(
  //   constantUtil.VEHICLECATEGORY
  // );

  // await professionalData.vehicles.map((vehicle) => {
  //   if (vehicle.status === constantUtil.INACTIVE)
  //     checkNextStepDetails.vehicleVerified.push("1");

  //   if (vehicle.vehicleDocuments.length > 0) {
  //     const docusvehi = vehicle.vehicleDocuments;
  //     driverDocumentList.forEach(async (docs) => {
  //       if (driverDocumentList.length === docusvehi.length) {
  //         docusvehi.forEach((c) => {
  //           if (docs.docsName === c.documentName) {
  //             if (docs.docsExpiry === true) {
  //               const diffDays = parseInt(
  //                 (new Date(c.expiryDate) - new Date()) / (1000 * 60 * 60 * 24)
  //               ); //gives day difference
  //               if (
  //                 c.uploadedStatus === constantUtil.UPLOADED &&
  //                 c.status === constantUtil.VERIFIED
  //               ) {
  //                 checkNextStepDetails.vehicleVerified.push("1");
  //               }
  //               if (diffDays <= 0) {
  //                 checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //               }
  //             } else {
  //               if (
  //                 c.uploadedStatus === constantUtil.UPLOADED &&
  //                 c.status === constantUtil.VERIFIED
  //               ) {
  //                 checkNextStepDetails.vehicleVerified.push("1");
  //               }
  //             }
  //           }
  //         });
  //       } else {
  //         checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //       }
  //     });
  //   } else {
  //     checkNextStepDetails.isVehicleDocumentsUploaded.push("1");
  //   }

  //   // if (vehicle.hub && vehicle.scheduleDate)
  //   if (vehicle?.serviceCategory) nextStepDetails.isHubDetailsPresent = false;

  //   if (vehicle.vehicleCategoryId) {
  //     vehicle.vehicleCategoryName =
  //       vehicleCategoryList[vehicle.vehicleCategoryId].vehicleCategory;
  //   } else {
  //     vehicle.vehicleCategoryName = "";
  //   }
  // });

  // if (checkNextStepDetails.profileVerification.length === 0)
  //   nextStepDetails.profileVerification = false;

  // if (checkNextStepDetails.isVehicleDocumentsUploaded.length === 0)
  //   nextStepDetails.isVehicleDocumentsUploaded = false;

  // if (checkNextStepDetails.vehicleVerified.length === 0)
  //   nextStepDetails.vehicleVerified = false;

  // professionalData.registrationNextStep = !professionalData.newProfessional
  //   ? nextStepDetails.profileVerification
  //     ? constantUtil.REGISTRATIONSTEP2
  //     : professionalData.vehicles.length === 0
  //     ? constantUtil.REGISTRATIONSTEP3
  //     : nextStepDetails.isVehicleDocumentsUploaded
  //     ? constantUtil.REGISTRATIONSTEP4
  //     : nextStepDetails.isHubDetailsPresent
  //     ? constantUtil.REGISTRATIONSTEP5
  //     : nextStepDetails.vehicleVerified
  //     ? constantUtil.REGISTRATIONSTEP6
  //     : constantUtil.REGISTRATIONCOMPLETED
  //   : constantUtil.REGISTRATIONSTEP1;

  // // if (
  // //   professionalData.registrationNextStep === constantUtil.REGISTRATIONSTEP5
  // // ) {
  // //   if (gerenalsetting.data.isHubNeeded === false) {
  // //     professionalData.registrationNextStep =
  // //       constantUtil.REGISTRATIONCOMPLETED;
  // //   }
  // // }
  // professionalData.vehicles = _.map(professionalData.vehicles, (vehicle) => ({
  //   ...vehicle,
  //   "hubsName": vehicle?.hub?.data?.hubsName || "",
  //   "location": vehicle?.hub?.data?.location || "",
  //   "phone": vehicle?.hub?.data?.phone || "",
  //   "email": vehicle?.hub?.data?.email || "",
  //   "showAddress": vehicle?.hub?.data?.showAddress || "",
  //   "hub": vehicle?.hub?._id || vehicle?.hub,
  // }));
  // --------- Check Profile & Vehicle Document List Start -----------
  professionalData = await this.broker.emit(
    "professional.checkProfessionalDocumentList",
    {
      ...professionalData,
      "clientId": clientId,
    }
  );
  professionalData = professionalData && professionalData[0];
  // --------- Check Profile & Vehicle Document List End -----------

  if (gerenalsetting.data.lastPriorityStatus === true) {
    if (
      professionalData?.isPriorityUpdatedTime !== null &&
      new Date(professionalData?.isPriorityUpdatedTime) <
        new Date(new Date().setHours(0, 0, 0, 0))
    ) {
      const updatedProfessionalData = await this.adapter.updateById(
        mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        {
          "$set": {
            "isPriorityUpdatedTime": null,
            "isPriorityCount": 0,
            "isPriorityStatus": false,
          },
        }
      );
      if (!updatedProfessionalData.nModified) {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    }
  }
  // const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  // if (!paymentOptionList) throw new MoleculerError(INFO_PAYMENT_OPTIONS);
  // let paymentGateWay = {};
  // for (const prop in paymentOptionList) {
  //   if (
  //     paymentOptionList[prop].status === constantUtil.ACTIVE &&
  //     paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
  //   ) {
  //     paymentGateWay = paymentOptionList[prop];
  //     paymentGateWay = {
  //       "mode": paymentGateWay.mode,
  //       "gateWay": paymentGateWay.gateWay,
  //       "testSecretKey": paymentGateWay.testSecretKey,
  //       "testPublicKey": paymentGateWay.testPublicKey,
  //       "livePublicKey": paymentGateWay.livePublicKey,
  //       "liveSecretKey": paymentGateWay.liveSecretKey,
  //       "paymentType": paymentGateWay.paymentType,
  //       "isPreAuthRequired": paymentGateWay.isPreAuthRequired,
  //     };
  //     break;
  //   }
  // }
  const professionalObjectData = await professionalObject(professionalData);
  return {
    "code": 200,
    "data": professionalObjectData,
  };
};

professionalAction.updateProfile = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId || context.meta.professionalId.toString();
  const {
    UPDATED,
    INVALID_PROFESSIONAL,
    // INFO_INVALID_REFERRAL_CODE,
    INFO_EMAIL_ALREADY_EXISTS,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
    INFO_INVALID_DATE_FORMAT,
    INFO_INVALID_AGE,
    INFO_PROFESSIONAL,
    INFO_ALREADY_EXISTS,
    ALERT_CHECK_CPF,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    const checkEmailCount = await this.adapter.model.countDocuments({
      "_id": {
        "$ne": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      },
      "email": context.params.email,
    });
    if (checkEmailCount > 0) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    // if (context.params.gender === "Masculino") {
    //   context.params.gender = constantUtil.MALE;
    // } else if (context.params.gender === "Feminino") {
    //   context.params.gender = constantUtil.FEMALE;
    // } else if (context.params.gender === "Outro") {
    //   context.params.gender = constantUtil.OTHER;
    // }
    // const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

    const paymentMode = generalSettings.data.paymentMode || [];

    const professionalData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(professionalId))
      .lean();
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    // finding age
    let age = 0;
    if (context.params.dob) {
      if (
        !moment(
          context.params.dob,
          ["MM-DD-YYYY", "MM/DD/YYYY"],
          true
        ).isValid()
      ) {
        throw new MoleculerError(INFO_INVALID_DATE_FORMAT);
      }
      age =
        moment().year() -
        moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"]).year();
      if (age < parseInt(generalSettings.data.driverMinAge))
        throw new MoleculerError(
          replace(
            INFO_INVALID_AGE,
            "{{minAgeKey}}",
            generalSettings.data.driverMinAge
          ),
          422 //Parameter Validation
        );
    }
    //
    // const dynamicUniqueCode = `${context.params.firstName.slice(
    //   0,
    //   4
    // )}${helperUtil.randomString(6, "a")}`;
    // if (!!context.params.uniqueCode === false) {
    //   if (!!professionalData.uniqueCode === false) {
    //     context.params.uniqueCode = dynamicUniqueCode.toUpperCase();
    //   } else {
    //     context.params.uniqueCode = professionalData.uniqueCode;
    //   }
    // }
    // let validReferreral,
    // referredByData = {},
    // avgRatingData = {};
    // const referredBy = context.params.referredBy || "";
    // if (!!referredBy === true) {
    //   validReferreral = await this.adapter.model
    //     .findOne(
    //       {
    //         "uniqueCode": referredBy.toLowerCase(),
    //       },
    //       { "_id": 1, "uniqueCode": 1 }
    //     )
    //     .collation({ "locale": "en", "caseLevel": true });
    //   if (!validReferreral)
    //     throw new MoleculerError(INFO_INVALID_REFERRAL_CODE);
    //   referredByData = {
    //     "referredBy": referredBy.toLowerCase(),
    //   };
    // }
    // if ((professionalData?.review?.avgRating ?? 0) === 0) {
    //   avgRatingData = {
    //     "review.avgRating": 5,
    //   };
    // }
    if (context.params?.nationalIdNo) {
      const checkCPFNO = await this.adapter.model
        .find(
          {
            "_id": {
              "$ne": mongoose.Types.ObjectId(professionalId),
            },
            "nationalIdNo": context.params.nationalIdNo,
          },
          { "_id": 1, "nationalIdNo": 1, "phone": 1 }
        )
        .lean();
      // if (
      //   checkCPFNO.length > 1 ||
      //   (checkCPFNO.length === 1 &&
      //     checkCPFNO?.[0]?._id?.toString() !==
      //       context.meta.professionalId.toString())
      // )
      if (checkCPFNO.length > 0) {
        throw new MoleculerError(
          INFO_PROFESSIONAL + " " + INFO_ALREADY_EXISTS + ". " + ALERT_CHECK_CPF
        );
      }
    }
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${constantUtil.AVATAR}${context.params.uniqueCode}${constantUtil.PROFESSIONAL}`,
          "oldFileName": `${professionalData.avatar}`,
        }
      );
      context.params.avatar = s3Image && s3Image[0];
    }
    const updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(professionalId),
        },
        {
          // "uniqueCode": context.params.uniqueCode || null,
          // "referredBy": (context.params.referredBy || "").toUpperCase(),
          "firstName": context.params.firstName,
          "lastName": context.params.lastName,
          "email": context.params.email,
          "isEmailVerified": context.params.isEmailVerified,
          "nationalIdNo": context.params.nationalIdNo,
          "nationalInsuranceNo": context.params.nationalInsuranceNo,
          "gender": context.params.gender,
          "avatar": context.params.avatar,
          "dob": context.params.dob,
          // "review.avgRating": 5,
          "age": age,
          "tin": context.params.tin,
          "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
          "lastCurrencyCode": generalSettings.data.currencyCode.toUpperCase(),
          "address": {
            "line": context.params.address,
            "city": context.params.city,
            "state": context.params.state,
            "country": context.params.country,
            "zipcode": context.params.zipcode,
          },
          "paymentMode": context.params.paymentMode || paymentMode,
          "knownLanguages": context.params.knownLanguages || [],
          // "referralDetails": {
          //   "rideCount": inviteSettings.data.forProfessional.rideCount,
          //   "inviteAmount": inviteSettings.data.forProfessional.amountToInviter,
          //   "joinerAmount": inviteSettings.data.forProfessional.amountToJoiner,
          // },
          // "licenceNo": context.params.licenceNo,
          // ...avgRatingData,
          // ...referredByData,
        },
        { "new": true }
      )
      .populate("vehicles.hub")
      .lean();
    if (!updatedProfessionalData) {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
    // //----------- invite and earn Start --------------
    // if (validReferreral) {
    //   const inviteAndEarnSettings = await storageUtil.read(
    //     constantUtil.INVITEANDEARN
    //   );
    //   if (inviteAndEarnSettings?.data?.forProfessional?.rideCount === 0) {
    //     await this.broker.emit("professional.inviteAndEarn", {
    //       "joinerId": context.meta.professionalId.toString(),
    //       // "id": validReferreral._id.toString(),
    //     });
    //   } else {
    //     await this.adapter.model.updateOne(
    //       {
    //         "_id": mongoose.Types.ObjectId(validReferreral?._id?.toString()),
    //       },
    //       {
    //         "$inc": { "invitationCount": 1 },
    //         // "joinCount": 1,
    //       }
    //     );
    //   }
    // }
    // //----------- invite and earn End --------------
    // // send mail
    // if (
    //   generalSettings.data.emailConfigurationEnable &&
    //   updatedProfessionalData.email
    // ) {
    //   this.broker.emit("admin.sendServiceMail", {
    //     "templateName": "professionalWelcomeMail",
    //     ...professionalWelcomeMailObject(updatedProfessionalData.toJSON()),
    //   });
    // }
    responseJson = await professionalObject(updatedProfessionalData);
    message = UPDATED;
    //send sms
  } catch (e) {
    errorCode = e.code || 400;
    if (e.code === "E11000" || e.code === "11000") {
      message = e.code ? e.message : INFO_EMAIL_ALREADY_EXISTS;
    } else {
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
    }
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

professionalAction.updateProfileImage = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId = context.params.professionalId;
  const {
    UPDATED,
    INVALID_PROFESSIONAL,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    const professionalData = await this.adapter.model
      .findOne(
        { "_id": mongoose.Types.ObjectId(professionalId) },
        { "_id": 1, "avatar": 1 }
      )
      .lean();
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${clientId}_${constantUtil.AVATAR}_${constantUtil.PROFESSIONAL}_${professionalId}`,
          "oldFileName": `${professionalData.avatar}`,
        }
      );
      context.params.avatar = s3Image && s3Image[0];
    }
    const updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        { "avatar": context.params.avatar },
        { "new": true }
      )
      .populate("vehicles.hub")
      .lean();
    if (!updatedProfessionalData) {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
    responseJson = await professionalObject(updatedProfessionalData);
    message = UPDATED;
    //send sms
  } catch (e) {
    errorCode = e.code || 400;
    if (e.code === "E11000" || e.code === "11000") {
      message = e.code ? e.message : INFO_EMAIL_ALREADY_EXISTS;
    } else {
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
    }
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

professionalAction.updateProfileAndVehicleDetails = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {},
    age;
  const vehicles = [];
  const clientId = context.params.clientId || context.meta.clientId;
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const {
    UPDATED,
    INVALID_PROFESSIONAL,
    INFO_INVALID_REFERRAL_CODE,
    INFO_EMAIL_ALREADY_EXISTS,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
    INFO_INVALID_DATE_FORMAT,
    INFO_INVALID_AGE,
    INFO_PROFESSIONAL,
    INFO_ALREADY_EXISTS,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    const checkEmailCount = await this.adapter.model.countDocuments({
      "_id": {
        "$ne": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      },
      "email": context.params.email,
    });
    if (checkEmailCount > 0) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    // if (context.params.gender === "Masculino") {
    //   context.params.gender = constantUtil.MALE;
    // } else if (context.params.gender === "Feminino") {
    //   context.params.gender = constantUtil.FEMALE;
    // } else if (context.params.gender === "Outro") {
    //   context.params.gender = constantUtil.OTHER;
    // }
    const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const paymentMode = generalSettings.data.paymentMode || [];

    const professionalData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
      .lean();
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    if (context.params.dob) {
      if (
        !moment(
          context.params.dob,
          ["MM-DD-YYYY", "MM/DD/YYYY"],
          true
        ).isValid()
      ) {
        // finding age
        throw new MoleculerError(INFO_INVALID_DATE_FORMAT);
      }
      age =
        moment().year() -
        moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"]).year();
      if (age < parseInt(generalSettings.data.driverMinAge)) {
        throw new MoleculerError(
          replace(
            INFO_INVALID_AGE,
            "{{minAgeKey}}",
            generalSettings.data.driverMinAge
          ),
          422 //Parameter Validation
        );
      }
    }
    if (context.params?.nationalIdNo) {
      const checkCPFNO = await this.adapter.model
        .find(
          {
            "nationalIdNo": context.params.nationalIdNo,
          },
          { "_id": 1, "nationalIdNo": 1, "phone": 1 }
        )
        .lean();
      if (
        checkCPFNO.length > 1 ||
        (checkCPFNO.length === 1 &&
          checkCPFNO?.[0]?._id?.toString() !==
            context.meta.professionalId.toString())
      ) {
        throw new MoleculerError(INFO_PROFESSIONAL + " " + INFO_ALREADY_EXISTS);
      }
    }
    // const dynamicUniqueCode = `${context.params.firstName.slice(
    //   0,
    //   4
    // )}${helperUtil.randomString(6, "a")}`;
    const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
      context.params.firstName,
      clientId
    );
    // if (!!context.params.uniqueCode === false) {
    //   if (!!professionalData.uniqueCode === false) {
    //     context.params.uniqueCode = dynamicUniqueCode.toUpperCase();
    //   } else {
    //     context.params.uniqueCode = professionalData.uniqueCode;
    //   }
    // }
    let validReferreral,
      referredByData = {},
      avgRatingData = {};
    const referredBy = context.params.referredBy || "";
    if (!!referredBy === true) {
      validReferreral = await this.adapter.model
        .findOne(
          {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "uniqueCode": referredBy.toLowerCase(),
          },
          { "_id": 1, "uniqueCode": 1 }
        )
        .collation({ "locale": "en", "caseLevel": true });
      if (!validReferreral) {
        throw new MoleculerError(INFO_INVALID_REFERRAL_CODE);
      }
      referredByData = {
        "referredBy": referredBy.toLowerCase(),
      };
    }
    if ((professionalData?.review?.avgRating ?? 0) === 0) {
      avgRatingData = {
        "review.avgRating": 5,
      };
    }
    // if (context.params.files && context.params.files.length > 0) {
    //   const s3Image = await this.broker.emit(
    //     "admin.uploadSingleImageInSpaces",
    //     {
    //       "files": context.params.files,
    //       "fileName": `${constantUtil.AVATAR}${context.params.uniqueCode}${constantUtil.PROFESSIONAL}`,
    //       "oldFileName": `${professionalData.avatar}`,
    //     }
    //   );
    //   context.params.avatar = s3Image[0];
    // }

    vehicles.push({
      "_id": mongoose.Types.ObjectId(),
      "professionalId": mongoose.Types.ObjectId(
        context.meta.professionalId.toString()
      ),
      "vinNumber": context.params.vinNumber || "",
      "type": context.params.type || "",
      "plateNumber": context.params.plateNumber || "",
      "maker": context.params.maker || "",
      "model": context.params.model || "",
      "year": context.params.year || "",
      "color": context.params.color || "",
      "noOfDoors": context.params.noOfDoors || "",
      "noOfSeats": context.params.noOfSeats || "",
      // "defaultVehicle": context.params.default ?? false,
      "defaultVehicle": true,
      "serviceCategory": context.params.serviceCategory,
      "serviceType": context.params.serviceType || professionalData.serviceType,
      "serviceTypeId": context.params.serviceTypeId
        ? mongoose.Types.ObjectId(context.params.serviceTypeId)
        : professionalData.serviceTypeId,
      "hub": context.params.hubId,
      "isSubCategoryAvailable": false,
      "handicapAvailable": false,
      "childseatAvailable": false,
      "isRentalSupported": false,
      "isCompanyVehicle": false,
      "isEnableShareRide": false,
      "isForceAppliedToProfessional": false,
      "status": constantUtil.INACTIVE,
    });
    // const emailCount = await this.adapter.model.count({
    //   "_id": {
    //     "$ne": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    //   },
    //   "email": context.params.email,
    // });
    // if (emailCount > 0) throw new MoleculerError(INFO_EMAIL_ALREADY_EXISTS);
    const defaultEmail = Date.now() + "@email.com";
    let updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.meta.professionalId.toString()
          ),
        },
        {
          "isNewUser": false,
          "firstName": context.params.firstName,
          "lastName": context.params.lastName,
          "uniqueCode": dynamicUniqueCode.toLowerCase(),
          "email": context.params.email || defaultEmail,
          "isEmailVerified": context.params.isEmailVerified || false,
          "nationalIdNo": context.params.nationalIdNo,
          "nationalInsuranceNo": context.params.nationalInsuranceNo,
          "gender": context.params.gender,
          // "avatar": context.params.avatar,
          "dob": context.params.dob,
          "age": age,
          "tin": context.params.tin,
          "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
          "lastCurrencyCode": generalSettings.data.currencyCode.toUpperCase(),
          "address": {
            "line": context.params.address,
            "city": context.params.city,
            "state": context.params.state,
            "country": context.params.country,
            "zipcode": context.params.zipcode,
          },
          "referralDetails": {
            "rideCount": inviteSettings.data.forProfessional.rideCount,
            "inviteAmount": inviteSettings.data.forProfessional.amountToInviter,
            "joinerAmount": inviteSettings.data.forProfessional.amountToJoiner,
          },
          "licenceNo": context.params.licenceNo,
          "serviceAreaId": context.params.serviceCategory,
          "vehicles": vehicles,
          "paymentMode": context.params.paymentMode || paymentMode,
          "knownLanguages": context.params.knownLanguages || [],
          "status": constantUtil.UNVERIFIED,
          ...avgRatingData,
          ...referredByData,
        },
        { "new": true }
      )
      .populate("vehicles.hub")
      .lean();
    if (!updatedProfessionalData) {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
    // --------- Check Profile & Vehicle Document List Start -----------
    updatedProfessionalData.accessToken =
      updatedProfessionalData?.deviceInfo[0]?.accessToken;
    updatedProfessionalData = await this.broker.emit(
      "professional.checkProfessionalDocumentList",
      {
        ...updatedProfessionalData,
        "clientId": clientId,
      }
    );
    updatedProfessionalData =
      updatedProfessionalData && updatedProfessionalData[0];
    // --------- Check Profile & Vehicle Document List End -----------
    //----------- invite and earn Start --------------
    if (validReferreral) {
      const inviteAndEarnSettings = await storageUtil.read(
        constantUtil.INVITEANDEARN
      );
      if (inviteAndEarnSettings?.data?.forProfessional?.rideCount === 0) {
        await this.broker.emit("professional.inviteAndEarn", {
          "clientId": clientId,
          "joinerId": context.meta.professionalId.toString(),
          // "id": validReferreral._id.toString(),
        });
      } else {
        await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(validReferreral?._id?.toString()),
          },
          {
            "$inc": { "invitationCount": 1 },
            // "joinCount": 1,
          }
        );
      }
    }
    //----------- invite and earn End --------------
    // send mail
    if (
      generalSettings.data.emailConfigurationEnable &&
      updatedProfessionalData.email &&
      updatedProfessionalData.isEmailVerified &&
      professionalData.isNewUser
    ) {
      this.broker.emit("admin.sendServiceMail", {
        "clientId": clientId,
        "templateName": "professionalWelcomeMail",
        ...professionalWelcomeMailObject(updatedProfessionalData),
      });
    }
    responseJson = await professionalObject(updatedProfessionalData);
    message = UPDATED;
    //send sms
  } catch (e) {
    errorCode = e.code || 400;
    if (e.code === "E11000" || e.code === "11000") {
      message = e.code ? e.message : INFO_EMAIL_ALREADY_EXISTS;
    } else {
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
    }
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

professionalAction.updateProfileCommonDetails = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};
  const commonDetails = {};
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const {
    UPDATED,
    INVALID_PROFESSIONAL,
    INFO_INVALID_REFERRAL_CODE,
    INFO_EMAIL_ALREADY_EXISTS,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
    INFO_INVALID_DATE_FORMAT,
    INFO_INVALID_AGE,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    if (context.params.email) {
      const checkEmailCount = await this.adapter.model.countDocuments({
        "_id": {
          "$ne": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        "email": context.params.email,
      });
      if (checkEmailCount > 0) {
        throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
      }
    }
    const professionalData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
      .lean();
    if (!professionalData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    if (context.params.files && context.params.files.length > 0) {
      const s3Image = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "clientId": clientId,
          "files": context.params.files,
          "fileName": `${constantUtil.AVATAR}${context.params.uniqueCode}${constantUtil.PROFESSIONAL}`,
          "oldFileName": `${professionalData.avatar}`,
        }
      );
      context.params.avatar = s3Image && s3Image[0];
    }
    if (context.params.avatar) {
      commonDetails["avatar"] = context.params.avatar;
    }
    if (context.params.email) {
      commonDetails["email"] = context.params.email;
    }
    if (context.params.gender) {
      commonDetails["gender"] = context.params.gender;
    }
    let updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        { ...commonDetails },
        { "new": true }
      )
      .populate("vehicles.hub")
      .lean();
    // --------- Check Profile & Vehicle Document List Start -----------
    updatedProfessionalData.accessToken =
      updatedProfessionalData?.deviceInfo[0]?.accessToken;
    updatedProfessionalData = await this.broker.emit(
      "professional.checkProfessionalDocumentList",
      {
        ...updatedProfessionalData,
        "clientId": clientId,
      }
    );
    updatedProfessionalData =
      updatedProfessionalData && updatedProfessionalData[0];
    // --------- Check Profile & Vehicle Document List End -----------
    responseJson = await professionalObject(updatedProfessionalData);
    message = UPDATED;
    //send sms
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

// Need to check & remove manageServiceType API after Jun 2024
professionalAction.manageServiceType = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};
  const vehicles = [];
  const clientId = context.params.clientId || context.meta.clientId;
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const {
    UPDATED,
    INVALID_PROFESSIONAL,
    INFO_INVALID_REFERRAL_CODE,
    INFO_EMAIL_ALREADY_EXISTS,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
    INFO_INVALID_DATE_FORMAT,
    INFO_INVALID_AGE,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    let updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        {
          "serviceTypeId": context.params.serviceTypeId,
          "serviceType": context.params.serviceType,
        },
        { "new": true }
      )
      .populate("vehicles.hub")
      .lean();
    if (!updatedProfessionalData) {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
    // // ---------- Update Vehicle Details  --------------
    // if (context.params.vehicleId) {
    //   await this.adapter.model.update(
    //     {
    //       "_id": mongoose.Types.ObjectId(
    //         context.params.professionalId.toString()
    //       ),
    //       "vehicles._id": mongoose.Types.ObjectId(
    //         context.params.vehicleId.toString()
    //       ),
    //     },
    //     {
    //       "vehicles.$.serviceType": context.params.serviceType,
    //       "vehicles.$.serviceTypeId": mongoose.Types.ObjectId(
    //         context.params.serviceTypeId
    //       ),
    //     }
    //   );
    // }
    // --------- Check Profile & Vehicle Document List Start -----------
    updatedProfessionalData.accessToken =
      updatedProfessionalData?.deviceInfo[0]?.accessToken;
    updatedProfessionalData = await this.broker.emit(
      "professional.checkProfessionalDocumentList",
      {
        ...updatedProfessionalData,
        "clientId": clientId,
      }
    );
    updatedProfessionalData =
      updatedProfessionalData && updatedProfessionalData[0];
    // --------- Check Profile & Vehicle Document List End -----------
    responseJson = await professionalObject(updatedProfessionalData);
    message = UPDATED;
    //send sms
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

professionalAction.activateAndInactivateAccount = async function (context) {
  const updatedProfessionalData = await this.adapter.model.findOneAndUpdate(
    {
      "_id": mongoose.Types.ObjectId(context.params.professionalId.toString()),
    },
    {
      "status": context.params.action.toUpperCase(),
      "inActivatedBy":
        context.params.action.toUpperCase() === constantUtil.INACTIVE
          ? constantUtil.PROFESSIONAL
          : null,
      "inActivatedAt":
        context.params.action.toUpperCase() === constantUtil.INACTIVE
          ? new Date()
          : null,
      "onlineStatus":
        context.params.action.toUpperCase() === constantUtil.INACTIVE
          ? false
          : true,
    },
    { "new": true }
  );
  const professionalObjectData = await professionalObject(
    updatedProfessionalData
  );
  return {
    "code": 200,
    "data": professionalObjectData,
  };
};

professionalAction.changePhoneNumber = async function (context) {
  const phoneCode = context.params.phoneCode;
  const phoneNumber = context.params.phoneNumber;
  const clientId = context.params.clientId || context.meta.clientId;
  if (context.params.otp) {
    const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
      "quality": 50,
      "width": 500,
    });

    let finalImageData = await this.broker.emit(
      "admin.uploadStaticMapImageInSpaces",
      {
        "files": qrImage,
        "fileName": `professional${phoneCode + "" + phoneNumber}QrImage`, //`professional${context.params.phoneNumber}QrImage`,
      }
    );
    finalImageData = finalImageData && finalImageData[0];
    const updatedProfessionalData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        "otp": cryptoUtils.hashSecret(context.params.otp),
      },
      {
        "otp": "",
        "phone.code": phoneCode,
        "phone.number": phoneNumber,
        "scanQr": finalImageData,
      }
    );
    if (!updatedProfessionalData.nModified) {
      throw new MoleculerError("Invalid OTP");
    } else {
      return { "code": 200, "message": "Phone number changed successfully" };
    }
  } else {
    const otp = customAlphabet("1234567890", otpLength)();

    const updatedProfessionalData = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      {
        "otp": cryptoUtils.hashSecret(otp),
      }
    );
    if (!updatedProfessionalData) {
      throw new MoleculerError("Invalid professional");
    } else {
      // this.broker.emit('admin.sendSMS', {
      //   'phoneNumber': `${context.params.phoneCode}${context.params.phoneNumber}`,
      //   'message': '',
      // })
      return { "code": 200, "message": "OTP sended successfully", "OTP": otp };
    }
  }
};

professionalAction.updateOnlineStatus = async function (context) {
  let errorCode = 200,
    responsemMssage = "",
    checkData = null,
    onlineStatus = context.params?.onlineStatus || false;
  const clientId = context.params.clientId || context.meta.clientId;
  const {
    UPDATED,
    PROFESSIONAL_INFO,
    INVALID_PROFESSIONAL,
    INFO_FAILED,
    SOMETHING_WENT_WRONG,
    ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    checkData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
      .lean();
    if (!checkData) {
      throw new MoleculerError(INVALID_PROFESSIONAL);
    }
    let countCheck = {};
    if (
      generalSettings.data.lastPriorityStatus === true &&
      onlineStatus === true
    ) {
      countCheck =
        checkData.isPriorityUpdatedTime === null
          ? { "isPriorityCount": 0 }
          : new Date(checkData.isPriorityUpdatedTime) <
            new Date(new Date().setHours(0, 0, 0, 0))
          ? { "isPriorityCount": 0 }
          : {};
    }
    const minimumwalletamount =
      generalSettings?.data?.minimumWalletAmountToOnline ?? 0;
    if (
      onlineStatus === true &&
      // generalSettings.data.paymentSection === constantUtil.CASHANDWALLET &&
      checkData.wallet.availableAmount < minimumwalletamount
    ) {
      // throw new MoleculerError(PROFESSIONAL_INFO);
      responsemMssage = PROFESSIONAL_INFO;
      errorCode = 600;
    } else {
      responsemMssage = UPDATED;
    }
    const updateData =
      context.params.lng && context.params.lat
        ? {
            "onlineStatus": onlineStatus,
            "onlineStatusBy": constantUtil.CONST_MANUAL,
            "onlineStatusUpdatedAt": new Date(),
            "location.coordinates": [context.params.lng, context.params.lat],
            "currentBearing": context.params.currentBearing,
            "altitude": context.params.altitude,
            "horizontalAccuracy": context.params.horizontalAccuracy,
            "verticalAccuracy": context.params.verticalAccuracy,
            "speed": context.params.speed,
            "isLocationUpdated": true,
            "locationUpdatedTime": new Date(),
            ...countCheck,
          }
        : {
            "onlineStatus": onlineStatus,
            "onlineStatusBy": constantUtil.CONST_MANUAL,
            "onlineStatusUpdatedAt": new Date(),
            ...countCheck,
          };
    // const updatedProfessionalData = await this.adapter.updateById(
    const updatedProfessionalData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        "status": constantUtil.ACTIVE,
      },
      updateData
    );
    if (!updatedProfessionalData) {
      throw new MoleculerError(ALERT_DOCUMENT_VERIFICATION_REVIEW_PENDING, 500); //INFO_FAILED
      // throw new MoleculerError("FAILED TO UPDATE ONLINE STATUS", 500);
    }
    // ------------------ Log Entry Start --------------
    if (generalSettings?.data?.isEnableIncentive) {
      //---------------- Incentive List -----------------------
      let incentiveList = await this.broker.emit(
        "admin.checkAndValidateIncentiveRuleByServiceAreaId",
        {
          "userType": constantUtil.PROFESSIONAL,
          "professionalId": context.meta.professionalId?.toString(),
          "serviceAreaId": checkData?.serviceAreaId?.toString(),
          "clientId": clientId,
        }
      );
      incentiveList = incentiveList && incentiveList[0];
      if (incentiveList) {
        if (onlineStatus) {
          //Insert Signup log
          await this.broker.emit("log.insertSignupLog", {
            "type": constantUtil.CONST_INCENTIVE,
            "name": constantUtil.LOGIN,
            "userType": constantUtil.PROFESSIONAL,
            "professionalId": context.meta.professionalId?.toString(),
            "clientId": clientId,
          });
        } else {
          // Insert Update logOutTime & Online Minutes
          await this.broker.emit("log.updateSignupLog", {
            "name": constantUtil.LOGOUT,
            "userType": constantUtil.PROFESSIONAL,
            "professionalId": context.meta.professionalId?.toString(),
            "clientId": clientId,
          });
        }
      }
      // ---------------- Incentive List -----------------------
    }
    // ------------------ Log Entry End --------------
    /* SOCKET PUSH NOTIFICATION FOR SATELIGHT VIEW */
    this.broker.emit(
      "socket.sendProfessionalStatusNotificationToAdminSatelightView",
      {
        "clientId": clientId,
        "data": {
          "clientId": clientId,
          "timestamp": Date.now(),
        },
      }
    );
  } catch (e) {
    errorCode = e.code || 200;
    responsemMssage = e.code ? e.message : SOMETHING_WENT_WRONG;
    onlineStatus = context.params.onlineStatus === true ? false : true;
  }
  return {
    "code": errorCode,
    "data": {
      "wallet": checkData?.wallet || {},
      "onlineStatus": onlineStatus,
    },
    "message": responsemMssage, // "STATUS UPDATED SUCCESSFULLY",
  };
};

professionalAction.updatePaymentMode = async function (context) {
  let errorCode = 200,
    responsemMssage = "",
    responseData = {};
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    await notifyMessage.setNotifyLanguage(
      context.params.langCode || context.meta?.professionalDetails?.languageCode
    );
  try {
    let updatedProfessionalData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.professionalId.toString()
          ),
        },
        {
          "paymentMode": context.params.paymentMode || [],
        },
        { "new": true }
      )
      .lean();
    responseData = await professionalObject(updatedProfessionalData);
    responsemMssage = INFO_SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    responsemMssage = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": responsemMssage,
  };
};

professionalAction.getDocumentListForProfileVerification = async function (
  context
) {
  // let profileDocumentList = await storageUtil.read(
  //   constantUtil.PROFILEDOCUMENTS
  // );

  // if (!profileDocumentList) profileDocumentList = [];

  // return {
  //   "code": 200,
  //   "data": Object.keys(profileDocumentList).map((documentId) => ({
  //     ...profileDocumentList[documentId],
  //     // "id": documentId,
  //   })),
  // };
  const documentList =
    await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
      context.params.serviceTypeId
    );
  return {
    "code": 200,
    "data": documentList?.profileDocumentRideList || [],
  };
};

professionalAction.updateProfileVerification = async function (context) {
  let expiryDate = "";
  const clientId = context.params.clientId || context.meta.clientId;
  if (
    context.params.expiryDate &&
    context.params.expiryDate != undefined &&
    context.params.expiryDate != ""
  ) {
    try {
      expiryDate = new Date(context.params.expiryDate).toISOString();
      const diffDays = parseInt(
        (new Date(context.params.expiryDate) - new Date()) /
          (1000 * 60 * 60 * 24)
      ); //gives day difference
      if (diffDays <= 0)
        // throw new MoleculerError(
        //   "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE"
        // );
        return {
          "code": 0,
          "data": {},
          "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
        };
    } catch (e) {
      expiryDate = new Date().toISOString();
    }
  }
  const professionalData = await this.adapter.model
    .findOne(
      {
        "_id": context.meta.professionalId,
      },
      {
        "uniqueCode": 1,
        "profileVerification": 1,
        "vehicles": 1,
        "status": 1,
      }
    )
    .populate("vehicles.hub")
    .lean();
  if (!professionalData) {
    throw new MoleculerError("ERROR IN PROFESSIONAL DETAILS");
  }
  if (context.params.files && context.params.files.length > 0) {
    context.params.documents = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `${context.meta.professionalId?.toString()}profile${
          constantUtil.PROFESSIONAL
        }`,
      }
    );
    if (context.params.documents && context.params.documents !== undefined) {
      context.params.documents = context.params.documents[0];
    }
  }
  const documentNameExists =
    professionalData.profileVerification.length > 0 &&
    professionalData.profileVerification.filter(
      (each) => each.documentName === context.params.documentName
    ).length > 0
      ? { "profileVerification.documentName": context.params.documentName }
      : {};

  const mongoId = mongoose.Types.ObjectId();
  let documentId = null;
  if (
    context.params.documentId?.toString() !== "null" &&
    context.params.documentId?.toString() !== "undefined"
  ) {
    documentId = context.params.documentId;
  }
  // const updateDocumentQuery =
  //   professionalData.profileVerification.length > 0 &&
  //   professionalData.profileVerification.filter(
  //     (each) => each.documentName === context.params.documentName
  //   ).length > 0
  //     ? professionalData.status === constantUtil.ACTIVE
  //       ? {
  //           "profileVerification.$.documentName": context.params.documentName,
  //           "profileVerification.$.expiryDate": expiryDate || "",
  //           "profileVerification.$.documents": context.params.documents || [],
  //           "profileVerification.$.uploadedStatus": constantUtil.UPLOADED,
  //           "profileVerification.$.reason": context.params.reason,
  //           "profileVerification.$.status": constantUtil.VERIFIED,
  //         }
  //       : {
  //           "profileVerification.$.documentName": context.params.documentName,
  //           "profileVerification.$.expiryDate": expiryDate || "",
  //           "profileVerification.$.documents": context.params.documents || [],
  //           "profileVerification.$.uploadedStatus": constantUtil.UPLOADED,
  //           "profileVerification.$.reason": context.params.reason,
  //           "profileVerification.$.status": constantUtil.UNVERIFIED,
  //           // "status": constantUtil.UNVERIFIED,
  //         }
  //     : professionalData.status === constantUtil.ACTIVE
  //     ? {
  //         "$push": {
  //           "profileVerification": {
  //             "_id": mongoId,
  //             "documentName": context.params.documentName,
  //             "expiryDate": expiryDate || "",
  //             "documents": context.params.documents || [],
  //             "uploadedStatus": constantUtil.UPLOADED,
  //             "reason": context.params.reason,
  //             "status": constantUtil.VERIFIED,
  //           },
  //         },
  //       }
  //     : {
  //         "$push": {
  //           "profileVerification": {
  //             "_id": mongoId,
  //             "documentName": context.params.documentName,
  //             "expiryDate": expiryDate || "",
  //             "documents": context.params.documents || [],
  //             "uploadedStatus": constantUtil.UPLOADED,
  //             "reason": context.params.reason,
  //             "status": constantUtil.UNVERIFIED,
  //           },
  //         },
  //         "status": constantUtil.UNVERIFIED,
  //       };
  const updateDocumentQuery =
    professionalData.profileVerification.length > 0 &&
    professionalData.profileVerification.filter(
      (each) => each.documentName === context.params.documentName
    ).length > 0
      ? {
          "profileVerification.$.documentId": documentId,
          "profileVerification.$.documentName": context.params.documentName,
          "profileVerification.$.expiryDate": expiryDate || "",
          "profileVerification.$.documents": context.params.documents || [],
          "profileVerification.$.uploadedStatus": constantUtil.UPLOADED,
          "profileVerification.$.reason": context.params.reason,
          "profileVerification.$.isMandatory": context.params.isMandatory,
          "profileVerification.$.status": constantUtil.UNVERIFIED,
          "status":
            professionalData.status === constantUtil.ACTIVE
              ? professionalData.status
              : constantUtil.UNVERIFIED,
        }
      : {
          "$push": {
            "profileVerification": {
              "_id": mongoId,
              "documentId": documentId,
              "documentName": context.params.documentName,
              "expiryDate": expiryDate || "",
              "documents": context.params.documents || [],
              "uploadedStatus": constantUtil.UPLOADED,
              "reason": context.params.reason,
              "isMandatory": context.params.isMandatory,
              "status": constantUtil.UNVERIFIED,
            },
          },
          "status":
            professionalData.status === constantUtil.ACTIVE
              ? professionalData.status
              : constantUtil.UNVERIFIED,
        };
  let updatedProfessionalData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        ...documentNameExists,
      },
      updateDocumentQuery,
      { "new": true }
    )
    .lean();

  // if (
  //   !updatedProfessionalData ||
  //   (updatedProfessionalData.n === 0 && updatedProfessionalData.nModified === 0)
  // )
  //   throw new MoleculerError("ERROR IN PROFILE DOCUMENT UPLOAD");
  // --------- Check Profile & Vehicle Document List Start -----------
  updatedProfessionalData = await this.broker.emit(
    "professional.checkProfessionalDocumentList",
    {
      ...updatedProfessionalData,
      "clientId": clientId,
    }
  );
  updatedProfessionalData =
    updatedProfessionalData && updatedProfessionalData[0];
  // --------- Check Profile & Vehicle Document List End -----------
  const professionalObjectData = await professionalObject(
    updatedProfessionalData
  );
  return {
    "code": 200,
    "message": "Document updated successfully",
    // "profileData": professionalObjectData,
    // "data": {
    //   "_id": context.params.id || mongoId,
    //   "documentName": context.params.documentName,
    //   "expiryDate": context.params.expiryDate,
    //   "documents": context.params.documents,
    //   "status": constantUtil.UNVERIFIED,
    // },
    "data": professionalObjectData,
  };
};

professionalAction.removeProfileVerification = async function (context) {
  const updatedProfessionalData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    {
      "$pull": {
        "profileVerification": {
          "_id": context.params.id,
        },
      },
    }
  );
  return { "code": 200, "data": updatedProfessionalData.profileVerification };
};

professionalAction.getDocumentListForVehicleVerification = async function (
  context
) {
  // let driverDocumentList = await storageUtil.read(constantUtil.DRIVERDOCUMENTS);

  // if (!driverDocumentList) driverDocumentList = [];

  // return {
  //   "code": 200,
  //   "data": Object.keys(driverDocumentList).map((documentId) => ({
  //     ...driverDocumentList[documentId],
  //     // "id": documentId,
  //   })),
  // };
  const documentList =
    await helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId(
      context.params.serviceTypeId
    );
  return {
    "code": 200,
    "data": documentList?.driverDocumentRideList || [],
  };
};

professionalAction.getServiceLocationList = async function (context) {
  let serviceLocationList;
  const clientId = context.params.clientId || context.meta.clientId;
  serviceLocationList = await this.broker.emit(
    "category.getServiceLocationList",
    {
      "requestFrom": context.params.requestFrom,
      "clientId": clientId,
    }
  );
  serviceLocationList = serviceLocationList && serviceLocationList[0];
  return { "code": 200, "data": serviceLocationList };
};

professionalAction.getHubList = async function (context) {
  let hubList;
  const clientId = context.params.clientId || context.meta.clientId;
  hubList = await this.broker.emit("admin.getHubListByLocation", {
    ...context.params,
    "clientId": clientId,
  });
  hubList = hubList && hubList[0];
  return { "code": 200, "data": hubList };
};

professionalAction.updateVehicleDetails = async function (context) {
  let isDefaultVehicle = false;
  const clientId = context.params.clientId || context.meta.clientId;
  if (!context.params.vinNumber) {
    context.params.vinNumber = "";
  }
  const professionalData = await this.adapter.model
    .findOne({
      "_id": context.meta.professionalId,
    })
    .lean();
  const documentNameExists = !context.params.id
    ? {}
    : { "vehicles._id": context.params.id };

  const mongoId = context.params.id
    ? context.params.id
    : mongoose.Types.ObjectId();

  let vehicles = [];

  if (!context.params.id) {
    if (professionalData.vehicles.length > 0) {
      vehicles = professionalData.vehicles;
    }
    vehicles.push({
      "_id": mongoId,
      "professionalId": mongoose.Types.ObjectId(
        context.params.professionalId.toString()
      ),
      "vinNumber": context.params.vinNumber || "",
      "type": context.params.type || "",
      "plateNumber": context.params.plateNumber || "",
      "maker": context.params.maker || "",
      "model": context.params.model || "",
      "year": context.params.year || "",
      "color": context.params.color || "",
      "noOfDoors": context.params.noOfDoors || "",
      "noOfSeats": context.params.noOfSeats || "",
      "serviceCategory": context.params.serviceCategoryId,
      "serviceType": context.params.serviceType,
      "serviceTypeId": context.params.serviceTypeId,
      "defaultVehicle": context.params.default ?? false,
      "isSubCategoryAvailable": false,
      "handicapAvailable": false,
      "childseatAvailable": false,
      "isRentalSupported": false,
      "isCompanyVehicle": false,
      "isEnableShareRide": false,
      "isForceAppliedToProfessional": false,
      "status": constantUtil.INACTIVE,
    });
  } else {
    vehicles = professionalData.vehicles.map((vehicle) => {
      if (vehicle._id.toString() === mongoId.toString()) {
        isDefaultVehicle = vehicle.defaultVehicle;
        const isVehicleValueChanged = () => {
          if (
            vehicle.vinNumber === context.params.vinNumber &&
            vehicle.type === context.params.type &&
            vehicle.plateNumber === context.params.plateNumber &&
            vehicle.maker === context.params.maker &&
            vehicle.model === context.params.model &&
            vehicle.year === context.params.year &&
            vehicle.color === context.params.color &&
            vehicle.noOfDoors === context.params.noOfDoors &&
            vehicle.noOfSeats === context.params.noOfSeats
          ) {
            return false;
          } else {
            return true;
          }
        };
        vehicle.vinNumber = context.params.vinNumber;
        vehicle.type = context.params.type;
        vehicle.plateNumber = context.params.plateNumber;
        vehicle.maker = context.params.maker;
        vehicle.model = context.params.model;
        vehicle.year = context.params.year;
        vehicle.color = context.params.color;
        vehicle.noOfDoors = context.params.noOfDoors;
        vehicle.noOfSeats = context.params.noOfSeats;
        vehicle.serviceCategory = context.params.serviceCategoryId;
        vehicle.serviceType = context.params.serviceType;
        vehicle.serviceTypeId = context.params.serviceTypeId;
        // vehicle.defaultVehicle = context.params.default ?? false;
        vehicle.status = isVehicleValueChanged
          ? constantUtil.INACTIVE
          : vehicle.status;
      }
      /* SET DEFAULT VEHICLE */
      if (context.params.default && vehicle.status !== constantUtil.INACTIVE) {
        if (vehicle._id.toString() === mongoId.toString()) {
          vehicle.defaultVehicle = true;
        } else {
          vehicle.defaultVehicle = false;
        }
      }
      return vehicle;
    });
  }
  //#region Update Professional Verification Service Type & TypeId
  if (!professionalData?.serviceTypeId) {
    await this.adapter.model.update(
      {
        "_id": mongoose.Types.ObjectId(
          context.params.professionalId.toString()
        ),
      },
      {
        "serviceTypeId": context.params.serviceTypeId,
        "serviceType": context.params.serviceType,
      }
    );
  }
  //#endregion Update Professional Verification Service Type & TypeId

  let updatedProfessionalData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        ...documentNameExists,
      },
      {
        "vehicles": vehicles,
      },
      { "new": true }
    )
    .populate("vehicles.hub")
    .lean();
  // if (updatedProfessionalData.nModified === 0)
  //   throw new MoleculerError("Vehicle not updated", "401", "AUTH_FAILED");
  // --------- Check Profile & Vehicle Document List Start -----------
  updatedProfessionalData.accessToken =
    updatedProfessionalData?.deviceInfo[0]?.accessToken;
  updatedProfessionalData.isDefaultVehicle = isDefaultVehicle;
  updatedProfessionalData = await this.broker.emit(
    "professional.checkProfessionalDocumentList",
    {
      ...updatedProfessionalData,
      "clientId": clientId,
    }
  );
  updatedProfessionalData =
    updatedProfessionalData && updatedProfessionalData[0];
  // --------- Check Profile & Vehicle Document List End -----------
  const professionalObjectData = await professionalObject(
    updatedProfessionalData
  );
  return {
    "code": 200,
    "message": "Vehicle updated successfully",
    // "profileData": professionalObjectData,
    // "data": vehicles.filter(
    //   (vehicle) => mongoId.toString() === vehicle._id.toString()
    // )[0],
    "data": professionalObjectData,
  };
};

professionalAction.updateVehicleDocuments = async function (context) {
  let expiryDate = "",
    isDefaultVehicle = false;
  const clientId = context.params.clientId || context.meta.clientId;
  if (
    context.params.expiryDate &&
    context.params.expiryDate != undefined &&
    context.params.expiryDate != ""
  ) {
    try {
      expiryDate = new Date(context.params.expiryDate).toISOString();
      const diffDays = parseInt(
        (new Date(context.params.expiryDate) - new Date()) /
          (1000 * 60 * 60 * 24)
      ); //gives day difference
      if (diffDays <= 0)
        return {
          "code": 0,
          "data": {},
          "message": "EXPIRE DATE MUST BE GREATER THAN CURRENT DATE",
        };
    } catch (e) {
      expiryDate = new Date().toISOString();
    }
  }
  const professionalData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (context.params.files && context.params.files.length > 0) {
    context.params.documents = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `${context.meta.professionalId.toString()}vehicle${
          constantUtil.PROFESSIONAL
        }`,
      }
    );
    if (context.params.documents && context.params.documents !== undefined) {
      context.params.documents = context.params.documents[0];
    }
  }
  let documentId = null;
  if (
    context.params.documentId?.toString() !== "null" &&
    context.params.documentId?.toString() !== "undefined"
  ) {
    documentId = context.params.documentId;
  }
  const vehicles = professionalData.vehicles.map((vehicle) => {
    if (vehicle._id.toString() === context.params.id.toString()) {
      isDefaultVehicle = vehicle.defaultVehicle; // For Change Professional Status ( UNVERIFIED/ACTIVE )
      if (professionalData.status === constantUtil.UNVERIFIED) {
        vehicle.status = constantUtil.INACTIVE;
      }
      if (
        vehicle.vehicleDocuments.length > 0 &&
        vehicle.vehicleDocuments.filter(
          (each) => each.documentName === context.params.documentName
        ).length > 0
      ) {
        // if (professionalData.status === constantUtil.ACTIVE) {
        //   vehicle.vehicleDocuments = vehicle.vehicleDocuments.map(
        //     (vehicleDocument) => {
        //       if (
        //         vehicleDocument.documentName === context.params.documentName
        //       ) {
        //         vehicleDocument.documentName = context.params.documentName;
        //         vehicleDocument.expiryDate = expiryDate || "";
        //         vehicleDocument.documents = context.params.documents;
        //         vehicleDocument.uploadedStatus = constantUtil.UPLOADED;
        //         vehicleDocument.reason = context.params.reason;
        //         vehicleDocument.status = constantUtil.VERIFIED;
        //       }
        //       return vehicleDocument;
        //     }
        //   );
        // } else {
        //   vehicle.vehicleDocuments = vehicle.vehicleDocuments.map(
        //     (vehicleDocument) => {
        //       if (
        //         vehicleDocument.documentName === context.params.documentName
        //       ) {
        //         vehicleDocument.documentName = context.params.documentName;
        //         vehicleDocument.expiryDate = expiryDate || "";
        //         vehicleDocument.documents = context.params.documents;
        //         vehicleDocument.uploadedStatus = constantUtil.UPLOADED;
        //         vehicleDocument.reason = context.params.reason;
        //         vehicleDocument.status = constantUtil.UNVERIFIED;
        //       }
        //       return vehicleDocument;
        //     }
        //   );
        // }

        vehicle.vehicleDocuments = vehicle.vehicleDocuments.map(
          (vehicleDocument) => {
            if (vehicleDocument.documentName === context.params.documentName) {
              vehicleDocument.documentId = documentId;
              vehicleDocument.documentName = context.params.documentName;
              vehicleDocument.expiryDate = expiryDate || "";
              vehicleDocument.documents = context.params.documents;
              vehicleDocument.uploadedStatus = constantUtil.UPLOADED;
              vehicleDocument.reason = context.params.reason;
              vehicleDocument.status =
                professionalData.status === constantUtil.ACTIVE
                  ? constantUtil.VERIFIED
                  : constantUtil.UNVERIFIED;
            }
            return vehicleDocument;
          }
        );
      } else {
        // if (professionalData.status === constantUtil.ACTIVE) {
        //   vehicle.vehicleDocuments.push({
        //     "_id": mongoose.Types.ObjectId(),
        //     "documentName": context.params.documentName,
        //     "expiryDate": expiryDate || "",
        //     "documents": context.params.documents,
        //     "uploadedStatus": constantUtil.UPLOADED,
        //     "reason": context.params.reason,
        //     "status": constantUtil.VERIFIED,
        //   });
        // } else {
        //   vehicle.vehicleDocuments.push({
        //     "_id": mongoose.Types.ObjectId(),
        //     "documentName": context.params.documentName,
        //     "expiryDate": expiryDate || "",
        //     "documents": context.params.documents,
        //     "uploadedStatus": constantUtil.UPLOADED,
        //     "reason": context.params.reason,
        //     "status": constantUtil.UNVERIFIED,
        //   });
        // }
        vehicle.vehicleDocuments.push({
          "_id": mongoose.Types.ObjectId(),
          "documentId": documentId,
          "documentName": context.params.documentName,
          "expiryDate": expiryDate || "",
          "documents": context.params.documents,
          "uploadedStatus": constantUtil.UPLOADED,
          "reason": context.params.reason,
          "status":
            professionalData.status === constantUtil.ACTIVE
              ? constantUtil.VERIFIED
              : constantUtil.UNVERIFIED,
        });
      }
    }
    return vehicle;
  });

  let updatedProfessionalData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      },
      { "vehicles": vehicles },
      { "new": true }
    )
    .lean();
  // if (!updatedProfessionalData)
  //   throw new MoleculerError("Documents not updated", "401", "AUTH_FAILED");
  // --------- Check Profile & Vehicle Document List Start -----------
  updatedProfessionalData.isDefaultVehicle = isDefaultVehicle;
  updatedProfessionalData = await this.broker.emit(
    "professional.checkProfessionalDocumentList",
    {
      ...updatedProfessionalData,
      "clientId": clientId,
    }
  );
  updatedProfessionalData =
    updatedProfessionalData && updatedProfessionalData[0];
  // --------- Check Profile & Vehicle Document List End -----------
  const professionalObjectData = await professionalObject(
    updatedProfessionalData
  );
  return {
    "code": 200,
    "message": "Document updated successfully",
    // "profileData": professionalObjectData,
    // "data": vehicles,
    "data": professionalObjectData,
  };
};

professionalAction.removeVehicleDetails = async function (context) {
  const updatedProfessionalData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    {
      "$pull": {
        "vehicles": {
          "_id": context.params.id,
        },
      },
    }
  );
  return { "code": 200, "data": updatedProfessionalData.vehicles };
};

professionalAction.setDefaultVehicle = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseJson = {};

  const { SOMETHING_WENT_WRONG } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.professionalDetails?.languageCode
  );
  try {
    const osType = context.params?.type || constantUtil.CONST_ANDROID_OS; //Need to remove default Const Value
    const vehicleId = mongoose.Types.ObjectId(context.params.id);
    const selectedDefaultVehicle = await this.adapter.model
      .findOne(
        {
          "_id": context.params.professionalId,
        },
        {
          "serviceType": 1,
          "serviceTypeId": 1,
          "vehicles": {
            "$elemMatch": { "_id": vehicleId, "status": constantUtil.ACTIVE },
          },
        }
      )
      .lean();
    const changeDefaultVehicleStatus = await this.adapter.model.update(
      {
        "_id": context.params.professionalId,
        "status": constantUtil.ACTIVE,
        "vehicles": {
          "$elemMatch": { "_id": vehicleId, "status": constantUtil.ACTIVE },
        },
      },
      {
        "$set": {
          "vehicles.$[list].defaultVehicle": true,
          "defaultVehicle": selectedDefaultVehicle.vehicles?.[0] || {},
          "serviceType":
            selectedDefaultVehicle.vehicles?.[0]?.serviceType ||
            selectedDefaultVehicle.serviceType,
          "serviceTypeId":
            selectedDefaultVehicle.vehicles?.[0]?.serviceTypeId?.toString() ||
            selectedDefaultVehicle.serviceTypeId?.toString(),
        },
      },
      { "arrayFilters": [{ "list._id": { "$in": vehicleId } }] }
    );
    //
    if (changeDefaultVehicleStatus.nModified) {
      responseJson = await this.adapter.model.updateMany(
        {
          "_id": context.params.professionalId,
          "vehicles": {
            "$elemMatch": { "_id": { "$nin": vehicleId } },
          },
        },
        {
          "$set": { "vehicles.$[list].defaultVehicle": false },
        },
        { "arrayFilters": [{ "list._id": { "$nin": vehicleId } }] }
      );
    }
    switch (osType) {
      case constantUtil.CONST_ANDROID_OS:
      case constantUtil.CONST_IOS:
        {
          if (!responseJson.nModified) {
            errorCode = 400;
            responseJson = {};
            message = "ERROR IN STATUS CHANGE";
          } else {
            errorCode = 200;
            message = "UPDATED SUCCESSFULLY";
            // --------------------------------------------
            const professionalData = await this.adapter.model
              .findOne({
                "_id": mongoose.Types.ObjectId(context.params.professionalId),
              })
              .lean();
            responseJson = await professionalObject(professionalData);
            // --------------------------------------------
          }
        }
        break;

      default:
        {
          if (!changeDefaultVehicleStatus.nModified) {
            errorCode = 0;
            responseJson = {};
            message = "ERROR IN STATUS CHANGE";
          } else {
            errorCode = 1;
            responseJson = {};
            message = "UPDATED SUCCESSFULLY";
          }
        }
        break;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "error": errorMessage,
    "data": responseJson,
  };
};

professionalAction.updateVehicleHubDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const validationError = [];
  if (generalSettings.data.isHubNeeded) {
    if (!!context.params.hubId === false || context.params.hubId.length < 12) {
      validationError.push({
        "type": "string",
        "expected": "string",
        "actual": context.params.hubId,
        "field": "hubId",
        "message": "Minimum 12 charactor required",
      });
    }
    if (!!context.params.scheduleDate === false) {
      validationError.push({
        "type": "string",
        "expected": "string",
        "actual": context.params.scheduleDate,
        "field": "scheduleDate",
        "message": "Required",
      });
    }
  }
  if (validationError[0]) {
    throw new ValidationError(validationError);
  }
  const updateQuey = {
    "vehicles.$.hub":
      !!context.params?.hubId === true ? context.params.hubId : undefined,
    "vehicles.$.scheduleDate":
      !!context.params?.scheduleDate === true
        ? context.params?.scheduleDate
        : undefined,
    "vehicles.$.serviceCategory": context.params.serviceCategory,
  };
  const professionalData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      "vehicles._id": context.params.id,
    },
    updateQuey
  );
  if (!professionalData.nModified) {
    throw new MoleculerError("HUB DETAILS FAILED TO UPDATE");
  }
  return {
    "code": 200,
    "data": {
      "hubId": context.params.hubId,
      "scheduleDate": context.params.scheduleDate,
      "serviceCategory": context.params.serviceCategory,
    },
  };
};

professionalAction.getDocumentListForHub = async function () {
  const clientId = context.params.clientId || context.meta.clientId;
  const hubVerificationDocumentList = await this.broker.emit(
    "admin.getDocumentListForHub",
    { "clientId": clientId }
  );

  return {
    "code": 200,
    "data": { "hubVerificationDocumentList": hubVerificationDocumentList[0] },
  };
};

professionalAction.logout = async function (context) {
  // const updatedProfessionalData = await this.adapter.updateById(
  //   mongoose.Types.ObjectId(context.meta.professionalId.toString()),
  //   {
  //     "deviceInfo": [],
  //     "onlineStatus": false,
  //     "onlineStatusBy": constantUtil.CONST_MANUAL,
  //     "onlineStatusUpdatedAt": new Date(),
  //   }
  // );
  const updatedProfessionalData = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()) },
    {
      "$set": {
        "deviceInfo": [],
        "onlineStatus": false,
        "onlineStatusBy": constantUtil.CONST_MANUAL,
        "onlineStatusUpdatedAt": new Date(),
      },
    }
  );
  if (!updatedProfessionalData.nModified) {
    throw new MoleculerError("Professional not found", "401", "AUTH_FAILED");
  } else {
    return { "code": 200, "message": "Device logout successfully" };
  }
};

professionalAction.updateLiveLocation = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const dataResult = await this.adapter.model
    .findOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId),
      },
      { "onlineStatus": 1, "locationUpdatedTime": 1 }
    )
    .lean();
  // console.log(dataResult);
  // const updatedProfessionalData = await this.adapter.updateById(
  //   mongoose.Types.ObjectId(context.meta.professionalId),
  //   {
  //     "location.coordinates": [
  //       parseFloat(context.params.lng),
  //       parseFloat(context.params.lat),
  //     ],
  //     "currentBearing": context.params.currentBearing,
  //     "altitude": context.params.altitude,
  //     "horizontalAccuracy": context.params.horizontalAccuracy,
  //     "verticalAccuracy": context.params.verticalAccuracy,
  //     "speed": context.params.speed,
  //     "isLocationUpdated": true,
  //     "locationUpdatedTime": new Date(),
  //     "onlineStatus": dataResult.onlineStatus,
  //   },
  //   { "returnOriginal": false }
  // );
  let onlineStatusData = {
    "onlineStatusBy": constantUtil.CONST_MANUAL,
  };
  if (context.params?.onlineStatus) {
    onlineStatusData = {
      "onlineStatus": context.params.onlineStatus,
      "onlineStatusBy": constantUtil.CONST_MANUAL,
      "onlineStatusUpdatedAt": new Date(),
    };
  }
  const updatedProfessionalData = await this.adapter.model
    .findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId),
      },
      {
        "location.coordinates": [
          parseFloat(context.params.lng),
          parseFloat(context.params.lat),
        ],
        "currentBearing": context.params.currentBearing,
        "altitude": context.params.altitude,
        "horizontalAccuracy": context.params.horizontalAccuracy,
        "verticalAccuracy": context.params.verticalAccuracy,
        "speed": context.params.speed,
        "isLocationUpdated": true,
        "locationUpdatedTime": new Date(),
        // "onlineStatus": dataResult.onlineStatus,
        ...onlineStatusData,
      },
      { "new": true }
    )
    .lean();
  if (
    dataResult?.onlineStatus === false &&
    context.params?.onlineStatus === true
  ) {
    //Insert Signup log
    await this.broker.emit("log.insertSignupLog", {
      "clientId": clientId,
      "type": constantUtil.CONST_INCENTIVE,
      "name": constantUtil.LOGIN,
      "userType": constantUtil.PROFESSIONAL,
      "professionalId": context.meta.professionalId?.toString(),
    });
  }
  console.log(
    "_____updateLiveLocation_Action=" + context.meta.professionalId + "______"
  );
  if (!updatedProfessionalData)
    return {
      "code": 0,
      "data": {},
      "message": "LOCATION UPDATE FAILED",
    };

  return {
    "code": 200,
    "data": {
      "onlineStatus": updatedProfessionalData.onlineStatus,
      "isPriorityStatus": updatedProfessionalData.isPriorityStatus,
      "bookingInfo": updatedProfessionalData.bookingInfo,
      "phone": updatedProfessionalData.phone.number,
    },
    "message": "LOCATION UPDATED SUCCESSFULLY",
  };
};

professionalAction.trackProfessionalLocation = async function (context) {
  const jsonData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.professionalId.toString()))
    .lean();
  if (!jsonData) {
    throw new MoleculerError("ERROR IN BOOKING DETAILS");
  } else {
    const locationData = {
      "lat": jsonData?.location?.coordinates?.[1] || 0,
      "lng": jsonData?.location?.coordinates?.[0] || 0,
      "currentBearing": jsonData.currentBearing
        ? jsonData.currentBearing
        : null,
      "altitude": jsonData.altitude ? jsonData.altitude : null,
      "speed": jsonData.speed ? jsonData.speed : null,
      "horizontalAccuracy": jsonData.horizontalAccuracy
        ? jsonData.horizontalAccuracy
        : null,
      "verticalAccuracy": jsonData.verticalAccuracy
        ? jsonData.verticalAccuracy
        : null,
      "isLocationUpdated": jsonData.isLocationUpdated
        ? jsonData.isLocationUpdated
        : null,
      "locationUpdatedTime": jsonData.locationUpdatedTime
        ? jsonData.locationUpdatedTime
        : null,
    };
    return { "code": 200, "data": locationData, "message": "" };
  }
};

professionalAction.getProfessionalData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const todayCount = await this.adapter.model.count({
    // "query": {
    // "clientId": mongoose.Types.ObjectId(clientId),
    "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
    // "vehicles.serviceCategory": {
    //   "$eq": mongoose.Types.ObjectId(context.params.city),
    // },
    // },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
              constantUtil.ATTENDED,
              constantUtil.UNVERIFIED,
            ],
          },
          "vehicles.serviceCategory": {
            "$in": [mongoose.Types.ObjectId(context.params.city)],
          },
        }
      : context.params.city === ""
      ? {
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
              constantUtil.ATTENDED,
              constantUtil.UNVERIFIED,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        }
      : {
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
              constantUtil.ATTENDED,
              constantUtil.UNVERIFIED,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
          "vehicles.serviceCategory": {
            "$in": [mongoose.Types.ObjectId(context.params.city)],
          },
        };

  const data = await this.adapter.model
    .find(matchData, {
      "status": 1,
      "createdAt": 1,
      "vehicles.serviceCategory": 1,
    })
    .lean();
  const dataForNotification = await this.adapter.model
    .find(
      {
        "status": {
          "$in": [
            constantUtil.INCOMPLETE,
            constantUtil.ATTENDED,
            constantUtil.UNVERIFIED,
          ],
        },
      },
      { "status": 1 }
    )
    .lean();
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0,
    triedCount = 0,
    unverifiedCount = 0,
    notifyTriedCount = 0,
    notifyUnverifiedCount = 0,
    notifyExpiredCount = 0;

  _.map(data, async (data) => {
    switch (data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
      case constantUtil.INCOMPLETE:
      case constantUtil.ATTENDED:
        triedCount += 1;
        break;
      // case constantUtil.ATTENDED:
      //   triedCount += 1;
      //   break;
      case constantUtil.UNVERIFIED:
        unverifiedCount += 1;
        totalCount += 1;
        break;
    }
  });
  _.map(dataForNotification, async (data) => {
    switch (data.status) {
      case constantUtil.INCOMPLETE:
      case constantUtil.ATTENDED:
        notifyTriedCount += 1;
        break;

      case constantUtil.UNVERIFIED:
        notifyUnverifiedCount += 1;
        break;
    }
  });

  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });

  //
  const notifyData = await this.adapter.model
    .aggregate([
      // {
      //   "$match": {
      //     "vehicles.serviceCategory": {
      //       "$in": [mongoose.Types.ObjectId(context.params.city)],
      //     },
      //   },
      // },
      {
        "$project": {
          "_id": "$status",
          "notifyTriedCount": {
            "$cond": {
              "if": {
                "$in": [
                  "$status",
                  [constantUtil.ATTENDED, constantUtil.INCOMPLETE],
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "notifyUnverifiedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$isExpired", false] },
                  { "$eq": ["$status", constantUtil.UNVERIFIED] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "notifyExpiredCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$isExpired", true] },
                  { "$eq": ["$status", constantUtil.UNVERIFIED] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": null,
          "notifyTriedCount": { "$sum": "$notifyTriedCount" },
          "notifyUnverifiedCount": { "$sum": "$notifyUnverifiedCount" },
          "notifyExpiredCount": { "$sum": "$notifyExpiredCount" },
        },
      },
      { "$sort": { "_id": 1 } },
    ])
    .allowDiskUse(true);
  //
  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "triedCount": triedCount,
    "unverifiedCount": unverifiedCount,
    "todayCount": todayCount,
    "graphData": graphData,
    "notifyTriedCount": notifyData?.[0].notifyTriedCount || 0, //notifyTriedCount,
    "notifyUnverifiedCount": notifyData?.[0].notifyUnverifiedCount || 0, //notifyUnverifiedCount,
    "notifyExpiredCount": notifyData?.[0].notifyExpiredCount || 0, //notifyExpiredCount,
  };
  return responseJson;
};

professionalAction.getVehiclesData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
            ],
          },
          // "category": mongoose.Types.ObjectId(context.params.city),
        }
      : {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
          // "category": mongoose.Types.ObjectId(context.params.city),
        };
  const data = await this.adapter.model.find(matchData, {
    "status": 1,
    "vehicles": 1,
    "createdAt": 1,
  });
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    verifiedCount = 0,
    unverifiedCount = 0;

  _.forEach(data, async (vehicleJson) => {
    if (vehicleJson.vehicles.length > 0) {
      _.map(vehicleJson.vehicles, async (data) => {
        switch (data.status) {
          case constantUtil.INACTIVE:
            inactiveCount += 1;
            totalCount += 1;
            break;
          case constantUtil.ACTIVE:
            activeCount += 1;
            totalCount += 1;
            break;
          case constantUtil.VERIFIED:
            verifiedCount += 1;
            totalCount += 1;
            break;
          case constantUtil.UNVERIFIED:
            unverifiedCount += 1;
            totalCount += 1;
            break;
        }
      });
    }
  });

  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "verifiedCount": verifiedCount,
    "unverifiedCount": unverifiedCount,
  };
  return responseJson;
};

professionalAction.getVehiclesDataBasedVehicleCategory = async function (
  context
) {
  const clientId = context.params.clientId || context.meta.clientId;
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": { "$lte": new Date() },
          "status": { "$ne": constantUtil.INCOMPLETE },
          // "category": mongoose.Types.ObjectId(context.params.city),
        }
      : {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
          "status": { "$ne": constantUtil.INCOMPLETE },
          // "category": mongoose.Types.ObjectId(context.params.city),
        };

  const query = [];
  query.push({
    "$match": matchData,
  });
  query.push(
    {
      "$project": {
        "vehicles": "$vehicles",
      },
    },
    { "$unwind": "$vehicles" },
    {
      "$group": {
        "_id": "$vehicles.vehicleCategoryId",
        "count": { "$sum": 1 },
      },
    }
  );
  const aggregateData = await this.adapter.model.aggregate(query);

  const vehicleCategoryList = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  const responseJson = [];
  _.forEach(aggregateData, async (vehicleJson) => {
    if (vehicleJson._id !== null) {
      vehicleJson["name"] =
        vehicleCategoryList[vehicleJson._id] &&
        vehicleCategoryList[vehicleJson._id].vehicleCategory;
      responseJson.push(vehicleJson);
    }
  });
  return responseJson;
};

professionalAction.addBankDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId || context.meta.professionalId.toString();
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  let paymentData;

  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentData = paymentOptionList[prop];
      // SOME TIME NOT FOUND SO WE NEED TO ADD
      if (!!paymentOptionList[prop]?.availableCurrencies === false) {
        throw new MoleculerError(
          "PLEASE ADD AVILABLECURRENCIES FILED IN  ACTIVE PARMANT GATEWAY"
        );
      }
    }
  }
  // // GET USERDATA
  // let userData;
  // if (context.params.userType === constantUtil.PROFESSIONAL) {
  //   userData = await this.broker.emit("professional.getById", {
  //     "id": context.meta.professionalId,
  //   });
  // }
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (!responseJson) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  // if (paymentData.gateWay === constantUtil.STRIPE) {
  //   let gatwayResponse = await this.broker.emit(
  //     'transaction.stripe.verifyBankAccount',
  //     {
  //       ...context.params,
  //       'email': responseJson.email,
  //       // 'currency': bankData.code,
  //       'stripeCustomerId': context.meta.professionalDetails.stripeCustomerId,
  //     }
  //   )
  //   if (gatwayResponse) throw new MoleculerError('ERROR IN BANK DETAILS')

  //   gatwayResponse = gatwayResponse && gatwayResponse[0]

  //   const transaction = await this.adapter.model.create({
  //     'transactionAmount': context.params.amount,
  //     'transactionReason': gatwayResponse.data.transactionReason,
  //     'from': {
  //       'userType': context.params.userType,
  //       'userId': userData._id.toString(),
  //       'name': userData.firstName + userData.lastName,
  //       'phoneNumber': userData.phoneNumber,
  //     },
  //     'to': {
  //       'userType': context.params.userType,
  //       'name': constantUtil.ADMIN,
  //     },
  //     'paymentType': constantUtil.CREDIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
  //     'gatewayName': paymentData.gateWay,
  //     'transactionType': constantUtil.WALLETRECHARGE,
  //     'transactionId': gatwayResponse.data.transactionId,
  //     'transactionDate': Date.now(),
  //     'gatewayDocId': gatwayResponse.data._id,
  //     'transactionStatus': constantUtil.SUCCESS,
  //     // 'cardDetailes': context.params.cardDetailes,
  //     'gatewayResponse': gatwayResponse,
  //   })

  //   return
  // }
  // ObjectId Added
  let bankDetails;
  if (context.params.bankDetails) {
    bankDetails = {
      "_id": mongoose.Types.ObjectId(),
      ...context.params.bankDetails,
    };
  } else {
    //Need to Remove Else Condition After Mar 2025
    bankDetails = { "_id": mongoose.Types.ObjectId(), ...context.params };
  }
  const updateJson = await this.adapter.model.updateOne(
    { "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()) },
    { "bankDetails": bankDetails }
  );
  if (!updateJson.nModified) {
    throw new MoleculerError("ERROR IN BANK DETAILS UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "BANK DETAILS UPDATED SUCESSFULLY",
    };
  }
};

professionalAction.getBankDetails = async function (context) {
  const { INFO_PAYMENT_OPTIONS, USER_NOT_FOUND } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) {
    throw new MoleculerError(INFO_PAYMENT_OPTIONS);
  }
  let paymentGateWay;
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentGateWay = paymentOptionList[prop];
      // paymentGateWay = {
      //   "mode": paymentGateWay.mode,
      //   "gateWay": paymentGateWay.gateWay,
      //   "testSecretKey": paymentGateWay.testSecretKey,
      //   "testPublicKey": paymentGateWay.testPublicKey,
      //   "livePublicKey": paymentGateWay.livePublicKey,
      //   "liveSecretKey": paymentGateWay.liveSecretKey,
      //   "paymentType": paymentGateWay.paymentType,
      //   "isPreAuthRequired": paymentGateWay.isPreAuthRequired,
      //   "isDefaultBankDisplayKeyInApp":
      //     paymentGateWay.isDefaultBankDisplayKeyInApp,
      // };
      break;
    }
  }

  let professionalData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (!professionalData) {
    throw new MoleculerError("INVALID PROFESSIONAL DETAILS");
  } else {
    // reponse
    const responseDataResult = {};
    if (professionalData?.bankDetails) {
      responseDataResult["isDefaultBankDisplayKeyInApp"] =
        professionalData.bankDetails[
          paymentGateWay?.isDefaultBankDisplayKeyInApp
        ];
    } else {
      responseDataResult["isDefaultBankDisplayKeyInApp"] = "";
    }
    return {
      "code": 200,
      "data": responseDataResult,
      // "paymentGateWay": paymentGateWay || {},
      "message": "",
    };
  }
};

professionalAction.manageCard = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  // trim card very essential
  context.params.cardNumber = context.params.cardNumber.replace(/ /g, "");
  //
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const currencyCode = context.params.currencyCode
    ? context.params.currencyCode.toUpperCase()
    : gerenalsetting.data.currencyCode.toUpperCase();
  //------------ Notification Message Start ---------
  const languageCode = context.meta?.userDetails?.languageCode;
  const {
    PROFESSIONAL_NOT_FOUND,
    INFO_INVALID_DETAILS,
    INFO_CARD_ALREADY_EXIST,
    WARNING_CARD_VALIDATION_FAILED,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  switch (context.params.action) {
    case "add":
      {
        let userData = await this.adapter.model.findOne({
          "_id": mongoose.Types.ObjectId(
            context.meta.professionalId.toString()
          ),
        });
        if (!userData) {
          throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
        }
        const cardAlreadyExist = userData.cards.find(
          (card) => card.cardNumber === context.params.cardNumber.trim()
        );
        if (cardAlreadyExist?.isVerified === true) {
          throw new MoleculerError(INFO_CARD_ALREADY_EXIST);
        }
        let validCard = await this.broker.emit("transaction.checkCardVaild", {
          "type": context.params.type,
          "cardNumber": context.params.cardNumber,
          "expiryYear": context.params.expiryYear,
          "expiryMonth": context.params.expiryMonth,
          "holderName": context.params.holderName,
          "cvv": context.params.cvv,
          "embedtoken": context.params.embedtoken,
          "userType": constantUtil.PROFESSIONAL,
          "userId": userData._id.toString(),
          "pin": context.params.pin,
          "paymentGateWayCustomerId": context.params.paymentGateWayCustomerId,
          "transactionId": context.params.transactionId,
          "transactionAmount": context.params.transactionAmount,
        });
        validCard = validCard && validCard[0];
        if (!validCard) {
          throw new MoleculerError(WARNING_CARD_VALIDATION_FAILED);
        }
        if (validCard.code > 200) {
          //it means reuturn is error or more than 400
          return validCard;
        } else if (
          validCard.code === 200 &&
          validCard.data.authType.toUpperCase() === "NOAUTH"
        ) {
          // const updateDefaultCardStatus = await this.broker.emit(
          //   "professional.updateDefaultCardStatusInActiveActive",
          //   {
          //     "professionalId": context.meta.professionalId.toString(),
          //     "cardId": null,
          //   }
          // );
          userData = await this.adapter.model.findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(
                context.meta.professionalId.toString()
              ),
              "cards.cardNumber": { "$ne": context.params.cardNumber },
            },
            {
              "$push": {
                "cards": {
                  "type": validCard.data?.type ?? context.params.type,
                  "cardId": context.params.cardId || validCard?.data?.cardId,
                  "cardNumber": context.params.cardNumber,
                  "expiryYear": context.params.expiryYear,
                  "expiryMonth": context.params.expiryMonth,
                  "holderName": context.params.holderName,
                  // token refs
                  "embedtoken":
                    context.params.embedtoken ||
                    validCard.data?.embedtoken ||
                    undefined,
                  "isVerified": true,
                  "isDefault":
                    userData.defaultPayment !== constantUtil.CARD
                      ? false
                      : validCard.data.isDefault || false,
                  "isCVVRequired": validCard.data.isCVVRequired || false,
                  "securityCode": context.params.securityCode,
                },
              },
              // "stripeCustomerId": validCard.data.stripeCustomerId || null,
              "paymentGateWayCustomerId":
                validCard.data.paymentGateWayCustomerId || null,
            },
            { "new": true }
          );
          //-------------
          let gatewayResponse = {};
          let params = {
            "clientId": clientId,
            "transactionId": validCard.data.transactionId,
            "transactionAmount": validCard.data.transactionAmount,
            "paymentData": validCard.data.paymentData,
          };
          //
          switch (validCard.data.paymentGateWay) {
            case constantUtil.STRIPE:
              {
                params["stripeCardToken"] = validCard.data.stripeCardToken;
                params["stripeCustomerId"] =
                  validCard.data.paymentGateWayCustomerId;
                //----------------------------
                gatewayResponse = await this.broker.emit(
                  "transaction.stripe.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;

            case constantUtil.RAZORPAY:
              {
                params["paymentGateWayCustomerId"] =
                  validCard.data.paymentGateWayCustomerId;
                //----------------------------
                gatewayResponse = await this.broker.emit(
                  "transaction.razorpay.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;

            case constantUtil.PEACH:
              {
                gatewayResponse = await this.broker.emit(
                  "transaction.peach.refundsAmount",
                  {
                    ...params,
                  }
                );
              }
              break;
          }
          if (!gatewayResponse) {
            throw new MoleculerError("Error Occured while Refund Amount");
          }
          //------------------------------
          validCard.data.card = null;
          validCard.data.cvv = null;
          validCard.data.cardToken = null;

          // fluuterwave
          validCard.data.embedtoken = undefined;
          validCard.data.type = undefined;
        }

        return {
          "code": 200,
          "data": {
            ...validCard.data,
            // 'card': context.params,
          },
          "message": validCard.message,
        };
      }
      break;

    case "remove":
      {
        if (!context.params.cardId) {
          throw new MoleculerError("Invalid request, Card ID required");
        }
        // const updateUserData = await this.adapter.updateById(
        //   mongoose.Types.ObjectId(context.meta.professionalId.toString()),
        //   {
        //     "$pull": {
        //       "cards": { "_id": context.params.cardId },
        //     },
        //   }
        // );
        const updateUserData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(
                context.meta.professionalId.toString()
              ),
            },
            {
              "$pull": {
                "cards": { "_id": context.params.cardId }, // No Need to Chack "isDefault" based Condition For Professional
              },
            },
            { "new": true }
          )
          .lean();
        if (!updateUserData) {
          throw new MoleculerError(
            "Invalid request, Address name already exists"
          );
        } else {
          return { "code": 200, "data": updateUserData.cards };
        }
      }
      break;

    default:
      {
        throw new MoleculerError("Invalid Request");
      }
      break;
  }
};

professionalAction.getCardsList = async function (context) {
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (!responseJson) {
    throw new MoleculerError("PROFESSIONAL NOT FOUND");
  } else {
    return {
      "code": 200,
      "data": responseJson.cards,
      "message": "",
    };
  }
};

professionalAction.getProfessionalViewDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId || context.meta.professionalId?.toString();
  const days = context.params.days || 30;
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  //
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(professionalId))
    .lean();
  if (!responseJson) {
    throw new MoleculerError("PROFESSIONAL NOT FOUND");
  }
  const defaultVehicle = responseJson?.vehicles?.find((item) => {
    return item.defaultVehicle === true;
  });
  let rideData = await this.broker.emit("booking.getProfessionalRideDetails", {
    "clientId": clientId,
    "professionalId": professionalId,
    "days": days,
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError("ERROR IN USER RIDE DETAILS");
  } else {
    vehicleCategoryData =
      vehicleCategoryData[defaultVehicle?.vehicleCategoryId?.toString()];
    return {
      "code": 200,
      "data": {
        "name":
          (responseJson.firstName || "") + " " + (responseJson.lastName || ""),
        "image": responseJson.avatar,
        "registerDate": responseJson.createdAt,
        "nationalIdNo": responseJson.nationalIdNo,
        "nationalInsuranceNo": responseJson.nationalInsuranceNo,
        "location": responseJson.location.coordinates,
        "avgRating": parseFloat(
          (responseJson?.review?.avgRating || 0).toFixed(1)
        ),
        // "vehicles": {
        //   "maker": responseJson.vehicles[0].maker,
        //   "model": responseJson.vehicles[0].model,
        //   "plateNumber": responseJson.vehicles[0].plateNumber,
        //   "vinNumber": responseJson.vehicles[0].vinNumber,
        // },
        "vehicles": {
          "maker": defaultVehicle?.maker || "",
          "model": defaultVehicle?.model || "",
          "color": defaultVehicle?.color || "",
          "plateNumber": defaultVehicle?.plateNumber || "",
          "vinNumber": defaultVehicle?.vinNumber || "",
          "noOfSeats": defaultVehicle?.noOfSeats || 0,
          "vehicleCategoryId": defaultVehicle?.vehicleCategoryId || "",
          "vehicleCategoryImage": vehicleCategoryData?.categoryImage || "",
          "rightImage": defaultVehicle?.rightImage || "",
          "leftImage": defaultVehicle?.leftImage || "",
          "backImage": defaultVehicle?.backImage || "",
          "frontImage": defaultVehicle?.frontImage || "",
        },
        "totalRides": rideData.totalRides,
        "totalEarnings": rideData.totalEarnings,
        "totalRidesForSelectedDays": rideData.totalRidesForSelectedDays,
        "acceptedRidesForSelectedDays": rideData.acceptedRidesForSelectedDays,
        "acceptedPercentage":
          (rideData.acceptedRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        "completedRidesForSelectedDays": rideData.completedRidesForSelectedDays,
        "completedPercentage":
          (rideData.completedRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        "cancelledRidesForSelectedDays": rideData.cancelledRidesForSelectedDays,
        "cancelledPercentage":
          (rideData.cancelledRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        //
        "userRidesNotReviewRating": rideData.userRidesNotReviewRating,
        "userRidesReviewRating1": rideData.userRidesReviewRating1,
        "userRidesReviewRating2": rideData.userRidesReviewRating2,
        "userRidesReviewRating3": rideData.userRidesReviewRating3,
        "userRidesReviewRating4": rideData.userRidesReviewRating4,
        "userRidesReviewRating5": rideData.userRidesReviewRating5,
      },
      "message": "",
    };
  }
};

professionalAction.getRatings = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId || context.meta.professionalId?.toString();
  let rideData = await this.broker.emit("booking.getProfessionalRatingList", {
    "clientId": clientId,
    "professionalId": professionalId,
    "skip": context.params.skip,
    "limit": context.params.limit,
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError("ERROR IN USER RATING DETAILS");
  } else {
    return { "code": 200, "data": rideData };
  }
};

professionalAction.getUploadedList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        "vehicles.hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;

    case constantUtil.HUBSEMPLOYEE:
      professionalVehicleHubQuery = {
        "createdBy": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      ...professionalVehicleHubQuery,
      "isExpired": true,
      // "status": constantUtil.UPLOADED,
      // "clientId": mongoose.Types.ObjectId(clientId),
      "$or": [
        { "profileVerification.uploadedStatus": constantUtil.UPLOADED },
        { "vehicles.vehicleDocuments.uploadedStatus": constantUtil.UPLOADED },
      ],
    },
  });

  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "firstName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "lastName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "email": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.code": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.number": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "status": "$status",
            "avgRating": "$avgRating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};

professionalAction.getExpiredProfessionalList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];
  let professionalVehicleHubQuery = {};
  switch (context.params.loginUserType) {
    case constantUtil.HUBS:
      // case constantUtil.DEVELOPER:
      professionalVehicleHubQuery = {
        "vehicles.hub": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;

    case constantUtil.HUBSEMPLOYEE:
      professionalVehicleHubQuery = {
        "createdBy": mongoose.Types.ObjectId(context.params.loginUserId),
      };
      break;
  }
  query.push({
    "$match": {
      ...professionalVehicleHubQuery,
      "isExpired": true,
      "status": constantUtil.UNVERIFIED,
      "clientId": mongoose.Types.ObjectId(clientId),
    },
  });
  if (context.params.search && context.params.search != "")
    query.push({
      "$match": {
        "$or": [
          {
            "firstName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "lastName": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "email": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.code": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
          {
            "phone.number": {
              "$regex": context.params.search + ".*",
              "$options": "si",
            },
          },
        ],
      },
    });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "status": "$status",
            "avgRating": "$avgRating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};

professionalAction.fetchBillingCycle = async function () {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("billings.fetchBillingCycle", {
    "clientId": clientId,
  });
  responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError("ERROR IN BILLING CYCLE");
  } else {
    return {
      "code": 200,
      "response": responseJson,
      "message": "",
    };
  }
};
professionalAction.getDriverBillingList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseJson = await this.broker.emit("booking.getDriverBillingList", {
    "clientId": clientId,
    "billingId": context.params.billingId,
    "professionalId": context.meta.professionalId,
  });
  responseJson = responseJson && responseJson[0];
  if (responseJson.code === 503) {
    throw new MoleculerError("ERROR IN DRIVER EARNINGS");
  } else {
    return {
      "code": 200,
      "response": responseJson.data,
      "message": "",
    };
  }
};
professionalAction.updateSubCategoryOption = async function (context) {
  const professionalData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    },
    {
      "$set": {
        "vehicles.$[list].applySubCategory":
          context.params.optionStatus || false,
        "vehicles.$[list].isEnableShareRide":
          context.params.isEnableShareRide || false,
      },
    },
    { "arrayFilters": [{ "list.defaultVehicle": true }] }
  );
  if (!professionalData.nModified) {
    throw new MoleculerError("ERROR IN CATEGORY OPTION");
  } else {
    return {
      "code": 200,
      "response": {},
      "message": "CATEGORY OPTION UPDATED SUCCESSFULLY",
    };
  }
};

professionalAction.addWalletAmount = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "";
  const languageCode = context.meta?.professionalDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    INFO_NOT_FOUND,
    TRAN_REDEEM_REWARD_MIN,
    TRAN_REDEEM_REWARD_MAX,
    INFO_INVALID_DETAILS,
    SOMETHING_WENT_WRONG,
    WALLET_AMOUNT_LOADED,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const userData = await this.adapter.model.findOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
      {
        "_id": 1,
        "wallet": 1,
        "firstName": 1,
        "lastName": 1,
        "avatar": 1,
        "phone": 1,
        "onlineStatus": 1,
        "serviceAreaId": 1,
      }
    );
    if (!userData) {
      throw new MoleculerError("ERROR IN PROFESSIONAL");
    }
    // const updateData =
    //   context.params.type === constantUtil.CREDIT
    //     ? userData.wallet.availableAmount + context.params.amount
    //     : userData.wallet.availableAmount - context.params.amount;
    // let onlineStatus = userData.onlineStatus;
    // if (
    //   generalSettings.data.paymentSection === constantUtil.CASHANDWALLET &&
    //   updateData < generalSettings.data.minimumWalletAmountToOnline
    // ) {
    //   onlineStatus = false;
    // }

    // const responseJson = await this.adapter.model.updateOne(
    //   {
    //     "_id": mongoose.Types.ObjectId(context.params.id),
    //   },
    //   {
    //     "wallet.availableAmount": updateData,
    //     "onlineStatus": onlineStatus,
    //     "onlineStatusUpdatedAt": new Date(),
    //   }
    // );

    let updateData = null,
      currentAvailableAmount = 0,
      redeemPointToAmount = 0;
    let onlineStatus = userData.onlineStatus;
    switch (context.params.type) {
      case constantUtil.CREDIT:
        {
          currentAvailableAmount =
            userData.wallet.availableAmount + context.params.amount;
          if (
            generalSettings.data.paymentSection ===
              constantUtil.CASHANDWALLET &&
            currentAvailableAmount <
              generalSettings.data.minimumWalletAmountToOnline
          ) {
            onlineStatus = false;
          }
          updateData = {
            "wallet.availableAmount": currentAvailableAmount,
            "onlineStatus": onlineStatus,
            "onlineStatusBy": constantUtil.CONST_MANUAL,
            "onlineStatusUpdatedAt": new Date(),
          };
        }
        break;

      case constantUtil.DEBIT:
        {
          currentAvailableAmount =
            userData.wallet.availableAmount - context.params.amount;
          if (
            generalSettings.data.paymentSection ===
              constantUtil.CASHANDWALLET &&
            currentAvailableAmount <
              generalSettings.data.minimumWalletAmountToOnline
          ) {
            onlineStatus = false;
          }
          updateData = {
            "wallet.availableAmount": currentAvailableAmount,
            "onlineStatus": onlineStatus,
            "onlineStatusBy": constantUtil.CONST_MANUAL,
            "onlineStatusUpdatedAt": new Date(),
            "lastWithdrawDate": new Date(),
          };
        }
        break;

      case constantUtil.REWARDPOINT:
        updateData = {
          "wallet.rewardPoints":
            userData.wallet.rewardPoints + context.params.amount,
        };
        break;

      case constantUtil.REDEEMREWARDPOINT:
        {
          if (!userData.serviceAreaId) {
            throw new MoleculerError(INFO_INVALID_DETAILS);
          }
          //-------- Active Reward List (POINT) Start -----------
          let activeRewardsList = await this.broker.emit(
            "admin.redeemRewardsPointConfig",
            {
              "clientId": clientId,
              "lng": generalSettings.data.location.lng, // For Skip Required Field
              "lat": generalSettings.data.location.lat, // For Skip Required Field
              "userType": constantUtil.PROFESSIONAL,
              "languageCode": languageCode,
              "serviceAreaId": userData.serviceAreaId?.toString(),
            }
          );
          activeRewardsList = activeRewardsList && activeRewardsList[0];
          if (!activeRewardsList) {
            throw new MoleculerError(INFO_NOT_FOUND);
          } else if (activeRewardsList.redeemLimitMin > context.params.amount) {
            //-------- Redeem Point Start ---------
            throw new MoleculerError(
              TRAN_REDEEM_REWARD_MIN + " " + activeRewardsList.redeemLimitMin
            );
          } else if (activeRewardsList.redeemLimitMax < context.params.amount) {
            throw new MoleculerError(
              TRAN_REDEEM_REWARD_MAX + " " + activeRewardsList.redeemLimitMax
            );
          } else {
            redeemPointToAmount =
              context.params.amount *
              convertToFloat(activeRewardsList.redeemPointValue);
          }
          currentAvailableAmount =
            userData.wallet.availableAmount + redeemPointToAmount;
          updateData = {
            "wallet.rewardPoints":
              userData.wallet.rewardPoints - context.params.amount,
            "wallet.availableAmount": currentAvailableAmount,
          };
        }
        break;
    }
    const responseJson = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.id),
      },
      updateData
    );
    if (!responseJson.nModified) {
      throw new MoleculerError("ERROR IN USER");
    }
    if (context.params.type === constantUtil.CREDIT) {
      this.broker.emit("transaction.updateWalletAdminRecharge", {
        "clientId": clientId,
        "amount": context.params.amount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": currentAvailableAmount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.PROFESSIONAL,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    } else if (context.params.type === constantUtil.DEBIT) {
      this.broker.emit("transaction.updateWalletAdminDebit", {
        "clientId": clientId,
        "amount": context.params.amount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": currentAvailableAmount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.PROFESSIONAL,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    } else if (context.params.type === constantUtil.REWARDPOINT) {
      await this.broker.emit("rewards.insertRewardByUserTypeFromAdmin", {
        "clientId": clientId,
        "userType": constantUtil.PROFESSIONAL,
        "professionalId": context.params.id,
        "rewardPoints": context.params.amount,
        "transactionType": constantUtil.CREDIT,
        "createdId": context.params.loginUserId,
        "reason": context.params.reason,
      });
    } else if (context.params.type === constantUtil.REDEEMREWARDPOINT) {
      await this.broker.emit("rewards.insertRewardByUserTypeFromAdmin", {
        "clientId": clientId,
        "userType": constantUtil.PROFESSIONAL,
        "professionalId": context.params.id,
        "rewardPoints": context.params.amount,
        "transactionType": constantUtil.DEBIT,
        "createdId": context.params.loginUserId,
        "reason": context.params.reason,
      });
      //----------- Transaction For Redeem Point Value ------------
      this.broker.emit("transaction.updateWalletAdminRecharge", {
        "clientId": clientId,
        "amount": redeemPointToAmount,
        "previousBalance": userData.wallet.availableAmount,
        "currentBalance": currentAvailableAmount,
        "reason": context.params.reason || "",
        "isReimbursement": context.params.isReimbursement || false,
        "userType": constantUtil.PROFESSIONAL,
        "data": {
          "id": userData._id.toString(),
          "firstName": userData.firstName,
          "lastName": userData.lastName,
          "avatar": userData.avatar,
          "phone": {
            "code": userData.phone.code,
            "number": userData.phone.number,
          },
        },
        "admin": {
          "id": context.meta.adminId.toString(),
          "firstName": context.meta.adminDetails.firstName,
          "lastName": context.meta.adminDetails.lastName,
          "avatar": context.meta.adminDetails.avatar,
          "phone": {
            "code": context.meta.adminDetails.phone.code,
            "number": context.meta.adminDetails.phone.number,
          },
        },
      });
    }
    message = WALLET_AMOUNT_LOADED;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  // return {
  //   "code": 200,
  //   "response": {},
  //   "message": "WALLET REACHARGE SUCCESSFULLY",
  // };
  return {
    "code": errorCode,
    "response": {},
    "message": message,
  };
};

professionalAction.updatePermissionData = async function (context) {
  const updateData = {
    "osType": context.params.osType ? context.params.osType : null,
    "brand": context.params.brand ? context.params.brand : null,
    "model": context.params.model ? context.params.model : null,
    "osVersion": context.params.osVersion ? context.params.osVersion : null,
    "screenSize": context.params.screenSize ? context.params.screenSize : null,
    "internetConnectivityMedium": context.params.internetConnectivityMedium
      ? context.params.internetConnectivityMedium
      : null,
    "token": context.params.token ? context.params.token : null,
    "fcmId": context.params.fcmId ? context.params.fcmId : null,
    "deviceLocationStatus": context.params.deviceLocationStatus
      ? context.params.deviceLocationStatus
      : null,
    "appLocationStatus": context.params.appLocationStatus
      ? context.params.appLocationStatus
      : null,
    "baseUrl": context.params.baseUrl ? context.params.baseUrl : null,
    "appVersionInServer": context.params.appVersionInServer
      ? context.params.appVersionInServer
      : null,
    "appVersionInStore": context.params.appVersionInStore
      ? context.params.appVersionInStore
      : null,
    "updatedTime": new Date(),
    "deviceTime": context.params.deviceTime ? context.params.deviceTime : null,
    "timezoneInNumber": context.params.timezoneInNumber
      ? context.params.timezoneInNumber
      : null,
    "timezoneInformat": context.params.timezoneInformat
      ? context.params.timezoneInformat
      : null,
    "connectivityMediumName": context.params.connectivityMediumName
      ? context.params.connectivityMediumName
      : null,
    "socketConnetivity": context.params.socketConnetivity
      ? context.params.socketConnetivity
      : null,
  };
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    {
      "$set": {
        "permissionData": updateData,
        "location.coordinates": [
          context.params.currentLocation.lng,
          context.params.currentLocation.lat,
        ],
      },
    }
  );
  if (!responseJson) {
    throw new MoleculerError("ERROR IN UPDATE PERMISSION DATA");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "PERMISSION DATA IS UPDATED SUCCESSFULLY",
    };
  }
};

professionalAction.getInviteAndEarnList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);
  const clientId = context.params.clientId || context.meta.clientId;

  const query = [];
  if (context.params.isHasInviteCount) {
    query.push({
      "$match": {
        // "joinCount": { "$gte": 1 },
        //"clientId": mongoose.Types.ObjectId(clientId),
        "$or": [
          { "joinCount": { "$gte": 1 } },
          { "invitationCount": { "$gte": 1 } },
        ],
        // "$or": [
        //   { "joinCount": { "$gte": 1 } },
        //   { "joinerRideCount": { "$gte": 1 } },
        //   { "joinerRinviterRideCountideCount": { "$gte": 1 } },
        // ],
      },
    });
  } else {
    query.push({
      "$match": {
        "$or": [
          { "joinCount": { "$in": [0, null, undefined] } },
          { "invitationCount": { "$in": [0, null, undefined] } },
        ],
      },
    });
  }
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  // FOR EXPORT DATE FILTER
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        // { "$skip": parseInt(skip) },
        // { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "uniqueCode": "$uniqueCode",
            "joinCount": "$joinCount",
            "joinerRideCount": "$joinerRideCount",
            "inviterRideCount": "$inviterRideCount",
            "invitationCount": "$invitationCount",
            "referrelTotalAmount": "$referrelTotalAmount",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};

professionalAction.downloadInviteAndEarnList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);
  const clientId = context.params.clientId || context.meta.clientId;

  const query = [];
  if (context.params.isHasInviteCount) {
    query.push({
      "$match": {
        // "joinCount": { "$gte": 1 },
        //"clientId": mongoose.Types.ObjectId(clientId),
        "$or": [
          { "joinCount": { "$gte": 1 } },
          { "invitationCount": { "$gte": 1 } },
        ],
        // "$or": [
        //   { "joinCount": { "$gte": 1 } },
        //   { "joinerRideCount": { "$gte": 1 } },
        //   { "joinerRinviterRideCountideCount": { "$gte": 1 } },
        // ],
      },
    });
  } else {
    query.push({
      "$match": {
        "$or": [
          { "joinCount": { "$in": [0, null, undefined] } },
          { "invitationCount": { "$in": [0, null, undefined] } },
        ],
      },
    });
  }
  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    query.push({
      "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  // FOR EXPORT DATE FILTER
  query.push(
    {
      "$lookup": {
        "from": "professionals",
        "localField": "uniqueCode",
        "foreignField": "referredBy",
        "as": "referredByList",
      },
    },
    {
      "$unwind": {
        "path": "$referredByList",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$group": {
        "_id": "$_id",
        "firstName": { "$first": "$firstName" },
        "lastName": { "$first": "$lastName" },
        "email": { "$first": "$email" },
        "phone": { "$first": "$phone" },
        "uniqueCode": { "$first": "$uniqueCode" },
        "joinCount": { "$first": "$joinCount" },
        "joinerRideCount": { "$first": "$joinerRideCount" },
        "inviterRideCount": { "$first": "$inviterRideCount" },
        "invitationCount": { "$first": "$invitationCount" },
        "referrelTotalAmount": { "$first": "$referrelTotalAmount" },
        "joinerList": {
          "$push": {
            "_id": "$referredByList._id",
            "fullName": {
              "$concat": [
                "$referredByList.firstName",
                " ",
                "$referredByList.lastName",
              ],
            },
            "email": "$referredByList.email",
            "phoneNumber": {
              "$concat": [
                "$referredByList.phone.code",
                " ",
                "$referredByList.phone.number",
              ],
            },
            "joinAmount": "$referredByList.joinAmount",
            "joinerRideCount": "$referredByList.joinerRideCount",
            "inviterRideCount": "$referredByList.inviterRideCount",
          },
        },
      },
    },
    {
      "$project": {
        "_id": "$_id",
        "firstName": "$firstName",
        "lastName": "$lastName",
        "email": "$email",
        "phone": "$phone",
        "uniqueCode": "$uniqueCode",
        "joinCount": "$joinCount",
        "joinerRideCount": "$joinerRideCount",
        "inviterRideCount": "$inviterRideCount",
        "invitationCount": "$invitationCount",
        "referrelTotalAmount": "$referrelTotalAmount",
        "joinerList": "$joinerList",
      },
    },
    {
      "$sort": { "_id": -1 },
    }
  );
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return {
    "code": 200,
    "response": jsonData || [],
    "message": "",
  };
};
professionalAction.downloadProfessionalJoiningList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const query = [];
    // query.push({
    //   "$match": {
    //     "clientId": mongoose.Types.ObjectId(context.params.clientId),
    //   },
    // });
    // FOR EXPORT DATE FILTER
    // CREATED DATE
    if (
      (context.params?.createdAtFrom || "") !== "" &&
      (context.params.createdAtTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    // UPDATED DATE
    if (
      (context.params?.updatedFrom || "") !== "" &&
      (context.params?.updatedTo || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.updatedFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.updatedTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "updatedAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    // PROFESSIONAL STATUS
    if (context.params.userStatus && context.params.userStatus !== "ALL") {
      query.push({
        "$match": { "status": context.params.userStatus },
      });
    }
    // PROFESSIONAL CITY
    if (context.params.city && context.params.city !== "ALL") {
      query.push({
        "$match": {
          "vehicles.serviceCategory": mongoose.Types.ObjectId(
            context.params.city
          ),
        },
      });
    }
    // PROFESSIONAL PHONENUMBER
    if (context.params.phoneNumber) {
      query.push({
        "$match": { "phone.number": { "$regex": context.params.phoneNumber } },
      });
    }
    // PROFESSIONAL AGE
    if (context.params.age) {
      query.push({
        "$match": { "age": Number(context.params.age) },
      });
    }
    // PROFESSIONAL GENDER
    if (context.params.gender && context.params.gender !== "ALL") {
      query.push({
        "$match": { "gender": context.params.gender },
      });
    }
    // PROFESSIONAL VECHILE TYPE
    if (context.params.vehicleType && context.params.vehicleType !== "ALL") {
      query.push({
        "$match": { "vehicles.type": context.params.vehicleType },
      });
    }
    // PROFESSIONAL DEVICE TYPE
    if (context.params.deviceType && context.params.deviceType !== "ALL") {
      query.push({
        "$match": { "permissionData.osType": context.params.deviceType },
      });
    }
    // PROFESSIONAL NAME
    if (context.params.name) {
      query.push({
        "$match": {
          "$or": [
            {
              "firstName": { "$regex": context.params.name, "$options": "si" },
            },
            {
              "lastName": { "$regex": context.params.name, "$options": "si" },
            },
          ],
        },
      });
    }
    // PROFESSIONAL EMAIL
    if (context.params.email) {
      query.push({
        "$match": {
          "email": { "$regex": context.params.email, "$options": "si" },
        },
      });
    }
    // PROFESSIONAL AGE

    if (context.params.Professionalage.age) {
      switch (context.params.Professionalage.ageCondition) {
        case ">=": {
          query.push({
            "$match": {
              "age": { "$gte": context.params.Professionalage.age },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "age": { "$lte": context.params.Professionalage.age },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "age": context.params.Professionalage.age,
            },
          });
        }
      }
    }
    // PROFESSIONAL WALLET AMOUNT
    if (context.params.wallet.walletAmount) {
      switch (context.params.wallet.WalletCondition) {
        case ">=": {
          query.push({
            "$match": {
              "wallet.availableAmount": {
                "$gte": context.params.wallet.walletAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "wallet.availableAmount": {
                "$lte": context.params.wallet.walletAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "wallet.availableAmount": context.params.wallet.walletAmount,
            },
          });
        }
      }
    }
    // PROFESSIONAL REWARD AMOUNT
    if (context.params.rewards.rewardsAmount) {
      switch (context.params.rewards.rewardsCondition) {
        case ">=": {
          query.push({
            "$match": {
              "wallet.rewardPoints": {
                "$gte": context.params.rewards.rewardsAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "wallet.rewardPoints": {
                "$lte": context.params.rewards.rewardsAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "wallet.rewardPoints": context.params.rewards.rewardsAmount,
            },
          });
        }
      }
    }
    // PROFESSIONAL INVITE EARN AMOUNT
    if (context.params.inviteEarn.inviteEarnAmount) {
      switch (context.params.inviteEarn.inviteEarnCondition) {
        case ">=": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount": {
                "$gte": context.params.inviteEarn.inviteEarnAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount": {
                "$lte": context.params.inviteEarn.inviteEarnAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "referralDetails.inviteAmount":
                context.params.inviteEarn.inviteEarnAmount,
            },
          });
        }
      }
    }
    // PROFESSIONAL RATING AMOUNT
    if (context.params.rating.ratingAmount) {
      switch (context.params.rating.ratingCondition) {
        case ">=": {
          query.push({
            "$match": {
              "review.avgRating": {
                "$gte": context.params.rating.ratingAmount,
              },
            },
          });
          break;
        }
        case "<=": {
          query.push({
            "$match": {
              "review.avgRating": {
                "$lte": context.params.rating.ratingAmount,
              },
            },
          });
          break;
        }
        case "===": {
          query.push({
            "$match": {
              "review.avgRating": context.params.rating.ratingAmount,
            },
          });
        }
      }
    }
    // PROFESSIONAL PLATE NUMBER
    if (context.params.plateNumber) {
      query.push({
        "$match": {
          "vehicles.plateNumber": {
            "$regex": context.params.plateNumber,
            "$options": "si",
          },
        },
      });
    }
    // PROFESSIONAL MAKING YEAR
    if (context.params.makingYear) {
      query.push({
        "$match": {
          "vehicles.year": {
            "$regex": context.params.makingYear,
            "$options": "si",
          },
        },
      });
    }
    // PROFESSIONAL VECHILE SEAT COUNT
    if (context.params.seatCount) {
      query.push({
        "$match": {
          "vehicles.noOfSeats": {
            "$regex": context.params.seatCount,
            "$options": "si",
          },
        },
      });
    }
    // LINK TABLES
    query.push(
      {
        "$lookup": {
          "from": "admins",
          "localField": "vehicles.vehicleCategoryId",
          "foreignField": "_id",
          "as": "vehicleCategory",
        },
      },
      {
        "$unwind": {
          "path": "$vehicleCategory",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );
    // PROFESSIONAL VEHICLE CATEGORIY
    if (
      context.params.vehicleCategory &&
      context.params.vehicleCategory !== "ALL"
    ) {
      query.push({
        "$match": {
          "vehicleCategory._id": mongoose.Types.ObjectId(
            context.params.vehicleCategory
          ),
        },
      });
    }

    query.push(
      {
        "$lookup": {
          "from": "categories",
          "localField": "vehicles.serviceCategory",
          "foreignField": "_id",
          "as": "locationName",
        },
      },
      {
        "$unwind": {
          "path": "$locationName",
          "preserveNullAndEmptyArrays": true,
        },
      },

      {
        "$facet": {
          "totalCount": [
            {
              "$count": "count",
            },
          ],
          "responseData": [
            {
              "$project": {
                "_id": "$_id",

                "name": { "$concat": ["$firstName", "$lastName"] },
                "email": "$email",
                "phoneNumber": { "$concat": ["$phone.code", "$phone.number"] },
                "status": "$status",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                "locationName": "$locationName.locationName",

                "age": "$age",
                "DateOfBirth": "$dob",
                "gender": "$gender",
                "plateNumber": "$vehicles.plateNumber",
                "vinNumber": "$vehicles.vinNumber",
                "makingYear": "$vehicles.year",
                "type": "$vehicles.type",
                "noOfSeats": "$vehicles.noOfSeats",
                "onlineStatusUpdatedAt": "$onlineStatusUpdatedAt",
                "location": "$location",
                "availableAmount": "$wallet.availableAmount",
                "appVersionInStore": "$permissionData.appVersionInStore",
                "model": "$permissionData.model",
                "brand": "$permissionData.brand",
                "deviceType": "$permissionData.osType",
                "osVersion": "$permissionData.osVersion",
                "rewardPoints": "$wallet.rewardPoints",
                "inviteAmount": "$referralDetails.inviteAmount",
                "permissionData": {
                  "$cond": {
                    "if": context.params.pushNotification === true,
                    "then": "$permissionData.fcmId",
                    "else": 0,
                  },
                },
                "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
                "avgRating": "$review.avgRating",
              },
            },
            // {
            //   "$group": {
            //     "_id": "$_id",

            //     "name": { "$first": "$name" },
            //     "email": { "$first": "$email" },
            //     "phoneNumber": { "$first": "$phoneNumber" },
            //     "status": { "$first": "$status" },
            //     "createdAt": { "$first": "$createdAt" },
            //     "updatedAt": { "$first": "$updatedAt" },

            //     "locationName": { "$first": "$locationName" },
            //     "age": { "$first": "$age" },
            //     "DateOfBirth": { "$first": "$dob" },
            //     "gender": { "$first": "$gender" },
            //     "plateNumber": { "$first": "$plateNumber" },
            //     "vinNumber": { "$first": "$vinNumber" },
            //     "makingYear": { "$first": "$year" },
            //     "type": { "$first": "$type" },
            //     "noOfSeats": { "$first": "$noOfSeats" },
            //     "onlineStatusUpdatedAt": { "$first": "$onlineStatusUpdatedAt" },
            //     "location": { "$first": "$location" },
            //     "availableAmount": { "$first": "$availableAmount" },
            //     "currencySymbol": { "$first": "$currencySymbol" },
            //     "appVersionInStore": { "$first": "$appVersionInStore" },
            //     "model": { "$first": "$model" },
            //     "brand": { "$first": "$brand" },
            //     "deviceType": { "$first": "$osType" },
            //     "osVersion": { "$first": "$osVersion" },
            //     "rewardPoints": { "$first": "$rewardPoints" },
            //     "inviteAmount": { "$first": "$inviteAmount" },
            //     "permissionData": { "$first": "$permissionData" },
            //     "vehicleCategory": { "$first": "$vehicleCategory" },
            //     "avgRating": { "$first": "$avgRating" },
            //   },
            // },
            { "$sort": { "createdAt": -1 } },
            { "$skip": context.params.skip },
            { "$limit": context.params.limit },
          ],
        },
      }
      // { "$sort": { "createdAt": 1 } }
    );
    // if (context.params.seatCount) {
    //   query.push({
    //     "$match": {
    //       "vehicles.noOfSeats": { "$regex": context.params.seatCount },
    //     },
    //   });
    // }

    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

professionalAction.getUsersJoinerList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const userData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id),
    },
    {
      "uniqueCode": 1,
    }
  );
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);

  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "referredBy": userData.uniqueCode,
    },
  });

  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "joinAmount": "$joinAmount",
            "joinerRideCount": "$joinerRideCount",
            "inviterRideCount": "$inviterRideCount",
          },
        },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseJson = {};
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];

  return responseJson;
};
professionalAction.updatePasswordBypass = async function (context) {
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  const updateUserData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.id.toString()),
    },
    {
      "$set": {
        "isOtpBypass": context.params.isOtpBypass,
      },
    }
  );
  if (!updateUserData.nModified) {
    throw new MoleculerError("ERROR IN UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "OTP BYPASS UPDATED SUCESSFULLY",
    };
  }
};
professionalAction.getInviteAndEarnDetail = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { PROFESSIONAL_NOT_FOUND } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  const inviteSettings = await storageUtil.read(constantUtil.INVITEANDEARN);
  const responseJson = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    })
    .lean();
  if (!responseJson) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  let uniqueCode = "";
  // const dynamicUniqueCode = `${responseJson.firstName.slice(
  //   0,
  //   4
  // )}${helperUtil.randomString(6, "a")}`;
  const dynamicUniqueCode = await helperUtil.getDynamicUniqueCode(
    responseJson.firstName,
    clientId
  );
  if (responseJson.uniqueCode && responseJson.uniqueCode !== null) {
    uniqueCode = responseJson.uniqueCode;
  } else {
    uniqueCode = dynamicUniqueCode.toLowerCase();
    const updateData = await this.adapter.updateById(
      mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      {
        "$set": { "uniqueCode": uniqueCode },
      }
    );
    if (!updateData) {
      throw new MoleculerError("ERROR IN UPDATE UNIQUE CODE");
    }
  }
  // const link=  mailData.content = mailData.content.replace(
  //   new RegExp(`{{${key}}}`, 'g'),
  //   context.params[key]
  // )
  // link=inviteAndEarn.
  const link = inviteSettings.data.forProfessional.messageText.replace(
    new RegExp(`{{code}}`, "g"),
    uniqueCode
  );
  return {
    "code": 200,
    "data": {
      "uniqueCode": uniqueCode.toUpperCase(),
      "promotionJson": {
        ...inviteSettings.data.forProfessional,
        "messageText": link,
      },
      "link": link,
    },
    "message": "",
  };
};
professionalAction.getInviteAndEarnHistory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 10);
  const query = [];

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "referredBy": (context.params.inviteCode || "").toLowerCase(),
      "joinAmount": { "$gte": 0 },
    },
  });

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        // { "$skip": parseInt(skip) },
        // { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "email": "$email",
            "phone": "$phone",
            "joinAmount": "$joinAmount",
            "date": "$createdAt",
            "currencyCode": "$currencyCode",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  return {
    "code": 200,
    "data": jsonData?.[0]?.response || [],
    "message": "",
  };
};
professionalAction.updateLastPriorityAddress = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const checkDate = new Date().setHours(0, 0, 0, 0);
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  const updateDataSet =
    (userData && userData.isPriorityUpdatedTime === null) ||
    (userData && userData.isPriorityCount === null) ||
    (userData &&
      userData.isPriorityUpdatedTime !== "undefined" &&
      Date.now(userData.isPriorityUpdatedTime) < checkDate &&
      userData.isPriorityCount !== "undefined" &&
      userData.isPriorityCount < gerenalsetting.data.lastPriorityCount)
      ? {
          "isPriorityUpdatedTime": new Date(),
          "isPriorityStatus": context.params.isPriorityStatus,
          "isPriorityLocation": context.params.isPriorityLocation,
        }
      : {
          "isPriorityLocation": context.params.isPriorityLocation,
        };
  const updateData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    },
    {
      "$set": updateDataSet,
    }
  );
  if (!updateData.nModified) {
    throw new MoleculerError("ERROR IN UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "LAST PRIORTY RIDE UPDATED SUCESSFULLY",
    };
  }
};
professionalAction.updateLastPriorityStatus = async function (context) {
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId = context.params.clientId || context.meta.clientId;
  const userData = await this.adapter.findById(
    mongoose.Types.ObjectId(context.meta.professionalId.toString())
  );
  if (!userData) {
    throw new MoleculerError("INVALID USER DETAILS");
  }
  let isPriorityCount = userData.isPriorityCount;
  if (context.params.isPriorityStatus === true) {
    isPriorityCount = userData.isPriorityCount + 1;
  }
  if (
    context.params.isPriorityStatus === false ||
    (userData && userData.isPriorityCount === null) ||
    (userData &&
      userData.isPriorityCount !== "undefined" &&
      userData.isPriorityCount < gerenalsetting.data.lastPriorityCount)
  ) {
    const updateData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      },
      {
        "$set": {
          //"clientId": clientId,
          "isPriorityStatus": context.params.isPriorityStatus,
          "isPriorityUpdatedTime": new Date(),
          "isPriorityCount": isPriorityCount,
        },
      }
    );
    if (!updateData.nModified) {
      throw new MoleculerError("ERROR IN UPDATE");
    } else {
      return {
        "code": 200,
        "data": {},
        "message": "LAST PRIORTY RIDE UPDATED SUCESSFULLY",
      };
    }
  } else {
    throw new MoleculerError("LAST PRIORITY LIMIT IS EXISTED");
  }
};

professionalAction.deleteLastPriorityAddress = async function (context) {
  const updateData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
    },
    {
      "$set": {
        "isPriorityLocation": {},
      },
    }
  );
  if (!updateData.nModified) {
    throw new MoleculerError("ERROR IN UPDATE");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "LAST PRIORTY RIDE UPDATED SUCESSFULLY",
    };
  }
};

professionalAction.deleteProfessionalUsingProfessionalId = async function (
  context
) {
  let updateData = null;
  const { DELETED, INFO_FAILED } = notifyMessage.setNotifyLanguage(
    context.params?.langCode
  );
  if (
    context.params.action === constantUtil.ACTION_DELETE &&
    context.params.ids
  ) {
    const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
    updateData = await this.adapter.model.deleteMany({
      "_id": { "$in": ids },
      // "status": { "$in": [constantUtil.INCOMPLETE, constantUtil.ATTENDED] },
    });
  } else if (context.params.id) {
    updateData = await this.adapter.model.deleteOne({
      "_id": mongoose.Types.ObjectId(context.params.id?.toString()),
    });
  }
  if (!updateData.deletedCount) {
    throw new MoleculerError(INFO_FAILED);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": DELETED,
    };
  }
};

professionalAction.getLastPriorityDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const { INVALID_PROFESSIONAL } = notifyMessage.setNotifyLanguage(
    context.params?.langCode
  );
  const userData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.professionalId.toString()))
    .lean();
  if (!userData) {
    throw new MoleculerError(INVALID_PROFESSIONAL);
  } else {
    return {
      "code": 200,
      "data": {
        "clientId": clientId,
        "lastPriorityUpdatedTime": userData.isPriorityUpdatedTime,
        "lastPriorityStatus": userData.isPriorityStatus,
        "lastPriorityLocation": userData.isPriorityLocation,
        "lastPriorityCount": userData.isPriorityCount,
        "lastPriorityTotalCount": gerenalsetting.data.lastPriorityCount,
      },
      "message": "",
    };
  }
};
professionalAction.getQrcode = async function (context) {
  let finalImageData;
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId?.toString() ||
    context.meta.professionalId.toString();
  const { INVALID_PROFESSIONAL } = notifyMessage.setNotifyLanguage(
    context.params?.langCode
  );
  const responseData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(professionalId))
    .lean();
  if (!responseData) {
    throw new MoleculerError(INVALID_PROFESSIONAL);
  }
  const phoneCode = responseData.phone.code;
  const phoneNumber = responseData.phone.number;
  if (responseData.scanQr && responseData.scanQr !== null) {
    // return {
    //   "code": 200,
    //   "data": {
    //     "qrImage": responseData.scanQr,
    //   },
    //   "message": "",
    // };
    finalImageData = responseData.scanQr;
  } else {
    const qrImage = await QRCode.toBuffer(`${phoneCode}-${phoneNumber}`, {
      "quality": 50,
      "width": 500,
    });
    finalImageData = await this.broker.emit(
      "admin.uploadStaticMapImageInSpaces",
      {
        "files": qrImage,
        "fileName": `professional${phoneCode + "" + phoneNumber}QrImage`,
        "oldFileName": `professional${phoneCode + "" + phoneNumber}QrImage`,
      }
    );
    finalImageData = finalImageData && finalImageData[0];
    //
    const updateUserData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.meta.professionalId.toString()),
      },
      {
        "$set": {
          "scanQr": finalImageData,
        },
      }
    );
    if (!updateUserData.nModified) {
      throw new MoleculerError("ERROR IN UPDATE");
    }
    // return {
    //   "code": 200,
    //   "data": {
    //     "qrImage": finalImageData,
    //   },
    //   "message": "",
    // };
  }
  return {
    "code": 200,
    "data": {
      "qrImage": finalImageData,
    },
    "message": "",
  };
};

professionalAction.getProfessionalDetails = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const professionalId =
    context.params.professionalId || context.meta.professionalId?.toString();
  const days = context.params.days || 30;
  const { PROFESSIONAL_NOT_FOUND, INVALID_BOOKING } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.userDetails?.languageCode
    );
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  //
  let phone = [];
  const query = {};
  if (professionalId) {
    query._id = professionalId;
  } else {
    phone = context.params.phoneNumber.split("-");
    query["phone.code"] = phone[0] || 0;
    query["phone.number"] = phone[1] || 0;
  }
  const responseJson = await this.adapter.model.findOne(query).lean();
  // responseJson = responseJson && responseJson[0];
  if (!responseJson) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  const defaultVehicle = responseJson?.vehicles?.find((item) => {
    return item.defaultVehicle === true;
  });
  let rideData = await this.broker.emit("booking.getProfessionalRideDetails", {
    "clientId": clientId,
    "professionalId": responseJson?._id?.toString(),
    "days": days,
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError(INVALID_BOOKING);
  } else {
    vehicleCategoryData =
      vehicleCategoryData[defaultVehicle?.vehicleCategoryId?.toString()];
    return {
      "code": 200,
      "data": {
        "_id": responseJson._id,
        "name":
          (responseJson.firstName || "") + " " + (responseJson.lastName || ""),
        "phone": responseJson.phone,
        "image": responseJson.avatar,
        "onlineStatus": responseJson.onlineStatus,
        "status": responseJson.status,
        "wallet": responseJson.wallet,
        "nationalIdNo": responseJson.nationalIdNo,
        "nationalInsuranceNo": responseJson.nationalInsuranceNo,
        "location": responseJson.location.coordinates,
        "paymentMode": responseJson.paymentMode,
        "registerDate": responseJson.createdAt,
        "avgRating": parseFloat(
          (responseJson?.review?.avgRating || 0).toFixed(1)
        ),
        // "vehicles": {
        //   "maker": responseJson?.vehicles[0]?.maker || "",
        //   "model": responseJson?.vehicles[0]?.model || "",
        //   "plateNumber": responseJson?.vehicles[0]?.plateNumber || "",
        //   "vinNumber": responseJson?.vehicles[0]?.vinNumber || "",
        //   "noOfSeats": responseJson?.vehicles[0]?.noOfSeats || 0,
        //   "vehicleCategoryId": responseJson?.vehicles[0]?.vehicleCategoryId || "",
        //   "rightImage": responseJson?.vehicles[0]?.rightImage || "",
        //   "leftImage": responseJson?.vehicles[0]?.leftImage || "",
        //   "backImage": responseJson?.vehicles[0]?.backImage || "",
        //   "frontImage": responseJson?.vehicles[0]?.frontImage || "",
        // },
        "vehicles": {
          "maker": defaultVehicle?.maker || "",
          "model": defaultVehicle?.model || "",
          "color": defaultVehicle?.color || "",
          "plateNumber": defaultVehicle?.plateNumber || "",
          "vinNumber": defaultVehicle?.vinNumber || "",
          "noOfSeats": defaultVehicle?.noOfSeats || 0,
          "vehicleCategoryId": defaultVehicle?.vehicleCategoryId || "",
          "vehicleCategoryImage": vehicleCategoryData?.categoryImage || "",
          "rightImage": defaultVehicle?.rightImage || "",
          "leftImage": defaultVehicle?.leftImage || "",
          "backImage": defaultVehicle?.backImage || "",
          "frontImage": defaultVehicle?.frontImage || "",
        },
        "totalRides": rideData.totalRides,
        "totalEarnings": rideData.totalEarnings,
        "totalRidesForSelectedDays": rideData.totalRidesForSelectedDays,
        "acceptedRidesForSelectedDays": rideData.acceptedRidesForSelectedDays,
        "acceptedPercentage":
          (rideData.acceptedRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        "completedRidesForSelectedDays": rideData.completedRidesForSelectedDays,
        "completedPercentage":
          (rideData.completedRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        "cancelledRidesForSelectedDays": rideData.cancelledRidesForSelectedDays,
        "cancelledPercentage":
          (rideData.cancelledRidesForSelectedDays /
            rideData.totalRidesForSelectedDays) *
          100,
        //
        "userRidesNotReviewRating": rideData.userRidesNotReviewRating,
        "userRidesReviewRating1": rideData.userRidesReviewRating1,
        "userRidesReviewRating2": rideData.userRidesReviewRating2,
        "userRidesReviewRating3": rideData.userRidesReviewRating3,
        "userRidesReviewRating4": rideData.userRidesReviewRating4,
        "userRidesReviewRating5": rideData.userRidesReviewRating5,
      },
      "message": "",
    };
  }
};

professionalAction.checkProfessionalAvail = async function (context) {
  const responseData = await this.adapter.model.findOne({
    "phone.number": context.params.phoneNumber,
    "phone.code": context.params.phoneCode,
    "status": "ACTIVE",
  });
  if (!responseData) {
    return { "code": 404, "data": {}, "message": "recipirant not available" };
  } else {
    const professionalObjectData = await professionalObject(responseData);
    return {
      "code": 200,
      "data": professionalObjectData,
      "message": "INVALID USER DETAILS",
    };
  }
};

professionalAction.checkUserAndProfessionalAvailableUsingPhoneNo =
  async function (context) {
    const clientId = context.params.clientId || context.meta.clientId;
    let userData = await this.broker.emit("user.checkUserAvail", {
      "clientId": clientId,
      "phoneCode": context.params.phoneCode,
      "phoneNumber": context.params.phoneNumber,
    });
    userData = userData && userData[0];
    //
    let professionalData = await this.broker.emit(
      "professional.checkProfessionalAvail",
      {
        "clientId": clientId,
        "phoneCode": context.params.phoneCode,
        "phoneNumber": context.params.phoneNumber,
      }
    );
    professionalData = professionalData && professionalData[0];
    return {
      "code": 200,
      "data": {
        "isAvailableInUser": userData ? true : false,
        "userDetails": userData || {},
        "isAvailableInProfessional": professionalData ? true : false,
        "professionalDetails": professionalData || {},
      },
      "message": "",
    };
  };
professionalAction.checkContacts = async function (context) {
  const responseJson = await this.adapter.model.find(
    {
      "phone": { "$in": context.params.contacts },
    },
    {
      "_id": 0,
      "firstName": 1,
      "lastName": 1,
      "phone": 1,
    }
  );

  if (!responseJson[0]) {
    return { "code": 200, "message": "NO CONTACES FOUND", "data": [] };
  }
  return {
    "code": 200,
    "data": responseJson,
    "message": "",
  };
};
// PAYMENT RELATED CARD AND BANK ACCOUNT
professionalAction.verifyCard = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let userData = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.meta.professionalId),
    })
    .lean();
  if (!userData) {
    throw new MoleculerError("PROFESSIONAL NOT FOUND");
  }
  let verifyResponse;
  if (context.params.mode === constantUtil.OTP) {
    if (!context.params.otp) {
      throw new MoleculerError("otp is required", 422, "validation error");
    }
    verifyResponse = await this.broker.emit("transaction.verifyCard", {
      "otp": context.params.otp,
      "transactionId": context.params.transactionId,
      "clientId": clientId,
    });
  }
  if (context.params.mode === constantUtil.PIN) {
    if (!context.params.pin) {
      throw new MoleculerError("pin is required", 422, "validation error");
    }
    verifyResponse = await this.broker.emit("transaction.verifyCard", {
      "pin": context.params.pin,
      "transactionId": context.params.transactionId,
      "clientId": clientId,
    });
  }
  //
  verifyResponse = verifyResponse[0];
  if (verifyResponse.code > 200) {
    return verifyResponse;
  }
  const updatedCard = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(userData._id.toString()),
      "cards._id": mongoose.Types.ObjectId(context.params.cardId.toString()),
    },
    { "$set": { "cards.$.isVerified": true } }
  );
  if (!updatedCard.nModified) {
    throw new MoleculerError("CANT UPDATED CARD IN VERIFY CARD OTP");
  }
  return {
    "code": 200,
    "data": {
      ...verifyResponse.data,
      "card": {
        ...userData.cards.find(
          (card) => card._id.toString() === context.params.cardId
        ),
        "isVerified": true,
      },
    },
    "message": verifyResponse.message,
  };

  // update
};
professionalAction.activateMembershipPlan = async function (context) {
  const { [context.params.membershipPlanId]: plan } = await storageUtil.read(
    constantUtil.PROFESSIONALMEMBERSHIPPLAN
  );
  const response = await this.adapter.model.update(
    { "_id": context.params?.professionalId ?? context.meta?.professionalId },
    {
      "membershipDetails.membershipPlanId": plan._id,
      "membershipDetails.totalNoOfRides": plan.totalNoOfRides,
      "membershipDetails.usedRides": 0,
      "membershipDetails.startDate": new Date(),
      "membershipDetails.endDate": new Date(
        new Date().getTime() + plan.validityDays * 24 * 60 * 60 * 1000
      ),
    }
  );
  if (!response.nModified) {
    throw new MoleculerError("SOMETHING WENT WRONG");
  } else {
    return {
      "code": 200,
      "message": "Success",
    };
  }
};
//------------- Redeem Rewards Point Start ------------
professionalAction.redeemRewardsPoint = async function (context) {
  const redeemPoint = convertToFloat(context?.params?.redeemPoint || 0);
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    languageCode = "",
    redeemRewardsPointValue = 0;

  const responseJson = {};
  languageCode = context.meta?.professionalDetails?.languageCode;

  const {
    INFO_NOT_FOUND,
    TRAN_REDEEM_REWARD_MAX,
    TRAN_REDEEM_REWARD_MIN,
    TRAN_REDEEM_REWARD_POINT,
    SOMETHING_WENT_WRONG,
    ERROR_WALLET_WITHDRAW,
    INFO_PAYMENT_OPTIONS,
  } = notifyMessage.setNotifyLanguage(languageCode);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  try {
    //-------- Active Reward List (POINT) Start -----------
    let activeRewardsList = await this.broker.emit(
      "admin.redeemRewardsPointConfig",
      {
        "clientId": clientId,
        "lng": context.params?.lng,
        "lat": context.params?.lat,
        "userType": constantUtil.PROFESSIONAL,
        "languageCode": languageCode,
      }
    );
    activeRewardsList = activeRewardsList && activeRewardsList[0];
    if (!activeRewardsList) {
      throw new MoleculerError(INFO_NOT_FOUND);
    }
    //-------- Active Reward List (POINT) End -----------
    //-------- Redeem Point Start ---------
    if (activeRewardsList.redeemLimitMin > redeemPoint) {
      message = TRAN_REDEEM_REWARD_MIN + " " + activeRewardsList.redeemLimitMin;
      errorCode = 400;
    } else if (activeRewardsList.redeemLimitMax < redeemPoint) {
      message = TRAN_REDEEM_REWARD_MAX + " " + activeRewardsList.redeemLimitMax;
      errorCode = 400;
    } else {
      redeemRewardsPointValue =
        redeemPoint * convertToFloat(activeRewardsList.redeemPointValue);

      let transactionReason = TRAN_REDEEM_REWARD_POINT + " / " + redeemPoint;
      let transactionType = constantUtil.REDEEMREWARDPOINT;
      const isEnabledRedeemApi =
        generalSettings?.data?.redeemApp?.isEnabled || false;
      // const redeemApiPaymentGatway =
      //   generalSettings?.data?.redeemApp?.paymentGatway || undefined; //Need to Change
      const redeemApiPaymentGatway =
        context.params?.redeemPaymentGatway || undefined;
      let walletDetails = {},
        paymentData = {},
        gatewayResponse = {};

      //#region paymentOption / PaymentGatway
      const paymentOptionList = await storageUtil.read(
        constantUtil.PAYMENTGATEWAY
      );
      for (const prop in paymentOptionList) {
        if (
          paymentOptionList[prop].status === constantUtil.ACTIVE &&
          paymentOptionList[prop].name === constantUtil.REDEEM
        ) {
          paymentData = paymentOptionList[prop];
          break;
        }
      }
      if (!paymentOptionList) {
        throw new MoleculerError(INFO_PAYMENT_OPTIONS, 500);
      }
      //#endregion paymentOption / PaymentGatway

      if (isEnabledRedeemApi) {
        switch (redeemApiPaymentGatway) {
          case constantUtil.CONST_TELEPORT:
            {
              gatewayResponse = await this.broker.emit(
                "transaction.teleport.redeemRewardsPointToTeleportAirtime",
                {
                  "clientId": clientId,
                  "redeemRewardsPointValue": redeemRewardsPointValue,
                  "phoneCode": context.params.phoneCode,
                  "phoneNumber": context.params.phoneNumber,
                  "generalSettings": generalSettings,
                  "transactionReason": transactionReason,
                  "paymentData": paymentData,
                }
              );
              gatewayResponse = gatewayResponse && gatewayResponse[0];
              if (!gatewayResponse) {
                throw new MoleculerError(ERROR_WALLET_WITHDRAW, 500);
              }
              if (
                gatewayResponse.code === 200 &&
                gatewayResponse.data.status === constantUtil.COMPLETE
              ) {
                walletDetails = {
                  "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
                };
                transactionReason = "Mobile Recharge";
                transactionType = constantUtil.REDEEMREWARDAIRTIME;
              } else {
                throw new MoleculerError("Mobile Recharge Failed", 500);
              }
            }
            break;

          default:
            walletDetails = {
              "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
              "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
            };
            break;
        }
      } else {
        walletDetails = {
          "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
          "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
        };
      }
      //-------- Redeem Point End ---------
      const professionalDetails = await this.adapter.model.findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(
            context.meta?.professionalId?.toString()
          ),
        },
        {
          "$inc": {
            // "wallet.rewardPoints": -convertToFloat(redeemPoint || 0),
            // "wallet.availableAmount": convertToFloat(redeemRewardsPointValue),
            ...walletDetails,
          },
          "lastRedeemDate": new Date(),
        },
        { "new": true }
        // {
        //   "upsert": true,
        // }
      );
      //-------- Transaction Entry Start ---------------------
      const transactionDetails = await this.broker.emit(
        "transaction.insertTransactionBasedOnUserType",
        {
          "clientId": clientId,
          "transactionType": transactionType,
          "transactionAmount": redeemRewardsPointValue,
          "previousBalance":
            convertToFloat(professionalDetails.wallet.availableAmount) -
            convertToFloat(redeemRewardsPointValue),
          "currentBalance": professionalDetails.wallet.availableAmount,
          "transactionReason": transactionReason,
          "transactionStatus": constantUtil.SUCCESS,
          "paymentType": constantUtil.CREDIT,
          "paymentGateWay": redeemApiPaymentGatway,
          "gatewayResponse": gatewayResponse?.successResponse || "",
          "userType": constantUtil.PROFESSIONAL,
          "userId": "",
          "professionalId": context.meta?.professionalId?.toString(),
        }
      );
      //-------- Transaction Entry End ---------------------
      message = TRAN_REDEEM_REWARD_POINT;
      responseJson["redeemPoint"] = redeemPoint;
      responseJson["wallet"] = professionalDetails?.wallet;
    }
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "error": errorMessage,
    "data": responseJson,
  };
};
//------------- Redeem Rewards Point End ------------

// Professional Active Inactive..........
professionalAction.getProfessionalActiveInactive = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model.aggregate([
      {
        "$lookup": {
          "from": "categories",
          "localField": "vehicles.serviceCategory",
          "foreignField": "_id",
          "as": "serviceAreaList",
        },
      },
      { "$unwind": "$serviceAreaList" },
      {
        "$project": {
          // "_id": "$address.city",
          "_id": "$_id",

          "activeStatus": {
            "$cond": {
              "if": { "$eq": ["$status", "ACTIVE"] },
              "then": 1,
              "else": 0,
            },
          },
          "inActiveStatus": {
            "$cond": {
              "if": { "$eq": ["$status", "INCOMPLETE"] },
              "then": 1,
              "else": 0,
            },
          },
          "status": "$status",
          // "city": "$address.city",
          // "city": { "$substrCP": ["$address.city", 0, 4] },
          "city": {
            "$cond": {
              "if": { "$eq": ["$serviceAreaList.locationName", null || " "] },
              "then": "OTHERS",
              "else": "$serviceAreaList.locationName",
            },
          },
        },
      },
      {
        "$group": {
          "_id": "$city",
          // "activeStatus": { "$first": "$_id" },
          "activeStatus": { "$sum": "$activeStatus" },

          "inActiveStatus": { "$sum": "$inActiveStatus" },
          //           "activeStatus2":{ "$max" :{ "$sum": "$activeStatus" } },
        },
      },
      { "$sort": { "_id": 1 } },
    ]);

    return jsonData;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
professionalAction.manageBlockedUserList = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    isBlocked = false,
    jsonData = {},
    responseData = [];
  try {
    switch (context.params?.action?.toUpperCase()) {
      case constantUtil.BLOCKED:
        jsonData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "$push": {
                "blockedUserList": mongoose.Types.ObjectId(
                  context.params.blockedUserId
                ),
              },
            },
            {
              "fields": {
                "blockedUserList": 1,
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        if (jsonData) {
          isBlocked = true;
          jsonData["userId"] = context.params.blockedUserId;
        }
        responseData.push(jsonData);

        break;

      case constantUtil.UNBLOCKED:
        jsonData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "$pull": {
                "blockedUserList": mongoose.Types.ObjectId(
                  context.params.blockedUserId
                ),
              },
            },
            {
              "fields": {
                "blockedUserList": 1,
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        if (jsonData) {
          isBlocked = false;
          jsonData["userId"] = context.params.blockedUserId;
        }
        responseData.push(jsonData);
        break;

      default: //CHECK --> Check User Status
        responseData = await this.adapter.model.aggregate([
          {
            "$match": {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
          },
          {
            "$lookup": {
              "from": "users",
              "localField": "blockedUserList",
              "foreignField": "_id",
              "as": "userBlockedList",
            },
          },
          { "$unwind": "$userBlockedList" },
          {
            "$match": {
              "userBlockedList._id": mongoose.Types.ObjectId(
                context.params.blockedUserId
              ),
            },
          },
          {
            "$project": {
              "_id": 1,
              "userId": "$userBlockedList._id",
              "phone": "$userBlockedList.phone",
              "firstName": "$userBlockedList.firstName",
              "lastName": "$userBlockedList.lastName",
              "blockedUserList": 1,
            },
          },
        ]);
        if (responseData.length > 0) {
          isBlocked = true;
        }
        break;
    }
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
    isBlocked = false;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    // "data": responseData || [],
    "data": { "isBlocked": isBlocked, "userList": responseData },
  };
};
professionalAction.manageBlockedProfessional = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    languageCode,
    responseData = [];

  languageCode = context.meta?.professionalDetails?.languageCode;

  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  try {
    switch (context.params?.action?.toUpperCase()) {
      case constantUtil.BLOCKED:
        responseData = await this.adapter.model
          .findOneAndUpdate(
            {
              "_id": mongoose.Types.ObjectId(context.params.professionalId),
            },
            {
              "status": constantUtil.BLOCKED,
              "blockedTill": context.params.blockDate,
              "unblockedTill": null,
            },
            {
              "fields": {
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        // if (jsonData) {
        //   isBlocked = true;
        //   jsonData["userId"] = context.params.blockedUserId;
        // }
        // responseData.push(jsonData);

        break;

      case constantUtil.UNBLOCKED:
        const unblockedTillDate = new Date(
          new Date(context.params.blockDate).setHours(23, 59, 59, 999)
        );
        responseData = await this.adapter.model
          .findOneAndUpdate(
            { "_id": mongoose.Types.ObjectId(context.params.professionalId) },
            {
              "status": constantUtil.ACTIVE,
              "blockedTill": null,
              "unblockedTill": unblockedTillDate,
            },
            {
              "fields": {
                "_id": 1,
                "phone": 1,
                "firstName": 1,
                "lastName": 1,
              },
              "new": true,
            }
          )
          .lean();
        // if (jsonData) {
        //   isBlocked = false;
        //   jsonData["userId"] = context.params.blockedUserId;
        // }
        // responseData.push(jsonData);
        break;

      // default: //CHECK --> Check User Status
      //   responseData = await this.adapter.model.aggregate([
      //     {
      //       "$match": {
      //         "_id": mongoose.Types.ObjectId(context.params.professionalId),
      //       },
      //     },
      //     {
      //       "$project": {
      //         "_id": 1,
      //         "userId": "$userBlockedList._id",
      //         "phone": "$userBlockedList.phone",
      //         "firstName": "$userBlockedList.firstName",
      //         "lastName": "$userBlockedList.lastName",
      //         "blockedUserList": 1,
      //       },
      //     },
      //   ]);
      //   // if (responseData.length > 0) {
      //   //   isBlocked = true;
      //   // }
      //   break;
    }
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    // "data": responseData || [],
    "data": responseData,
  };
};
//#region REPORT
professionalAction.getOnlineOfflineList = async function (context) {
  // const skip = parseInt(context.params.skip ?? 0);
  // const limit = parseInt(context.params.limit ?? 20);
  const clientId = context.params.clientId || context.meta.clientId;
  const search = context.params?.search || "";
  const query = [];
  // query["status"] =
  //   context.params.filter && context.params.filter != ""
  //     ? { "$eq": context.params.filter }
  //     : {
  //         "$in": [constantUtil.ACTIVE, constantUtil.INACTIVE],
  //       };
  // query["status"] = constantUtil.ACTIVE;
  query.push({
    "$match": {
      "status": constantUtil.ACTIVE,
      //"clientId": mongoose.Types.ObjectId(clientId),
    },
  });
  if (context.params.onlineStatus) {
    // query["onlineStatus"] = context.params.onlineStatus;
    query.push({
      "$match": {
        "onlineStatus":
          context.params.onlineStatus === constantUtil.ACTIVE ? true : false,
      },
    });
  }
  if (context.params.serviceAreaId) {
    // query["vehicles.serviceCategory"] = mongoose.Types.ObjectId(
    //   context.params.serviceAreaId.toString()
    // );
    query.push({
      "$match": {
        "vehicles.serviceCategory": mongoose.Types.ObjectId(
          context.params.serviceAreaId.toString()
        ),
      },
    });
  }
  // query["locationUpdatedTime"] = {
  //   "$gte": new Date(new Date().setHours(0, 0, 0, 0)),
  // };

  // FOR EXPORT DATE FILTER
  if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    // query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    query.push({
      "$match": {
        "createdAt": { "$gte": fromDate, "$lt": toDate },
      },
    });
  }
  // FOR EXPORT DATE FILTER

  const responseJson = {};
  // const count = await this.adapter.count({
  //   "query": query,
  //   "search": search,
  //   "searchFields": [
  //     "data.firstName",
  //     "data.lastName",
  //     "data.phone.code",
  //     "data.phone.number",
  //   ],
  // });
  // const jsonData = await this.adapter.find({
  //   "query": query,
  //   // "limit": limit,
  //   // "offset": skip,
  //   "sort": "-locationUpdatedTime",
  //   "search": search,
  //   "searchFields": [
  //     "data.firstName",
  //     "data.lastName",
  //     "data.phone.code",
  //     "data.phone.number",
  //   ],
  // });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],

      "response": [
        // { "$match": { "userType": { "$ne": null } } },
        // { "$sort": { "createdAt": -1 } },
        // { "$skip": parseInt(skip) },
        // { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "categories",
            "localField": "vehicles.serviceCategory",
            "foreignField": "_id",
            "as": "serviceCategoryList",
          },
        },
        {
          "$unwind": {
            "path": "$serviceCategoryList",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "firstName": "$firstName",
            "lastName": "$lastName",
            "serviceArea": "$serviceCategoryList.locationName",
            "phone": "$phone",
            "locationUpdatedTime": "$locationUpdatedTime",
            "onlineStatus": "$onlineStatus",
            "status": "$status",
          },
        },
        { "$sort": { "locationUpdatedTime": -1 } }, // Need to check& change
        // { "$skip": parseInt(skip) }, // Need to check& change
        // { "$limit": parseInt(limit) }, // Need to check& change
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  // responseJson["count"] = count;
  // responseJson["response"] = jsonData.map((respo) => ({
  //   "_id": respo._id,
  //   "data": {
  //     "firstName": respo.firstName,
  //     "lastName": respo.lastName,
  //     "phone": respo.phone,
  //     "locationUpdatedTime": respo.locationUpdatedTime,
  //     "onlineStatus": respo.onlineStatus,
  //     "status": respo.status,
  //   },
  // }));
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || {};
  return responseJson;
};
//#endregion REPORT

professionalAction.manageTrustedContact = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    responseData = [];
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    responseData = await this.broker.emit("professional.updateTrustedContact", {
      ...context.params,
      "clientId": clientId,
    });
    responseData = responseData && responseData[0];
    errorCode = responseData.code;
    message = responseData.message;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = [];
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData,
  };
};

//-------------------------------
module.exports = professionalAction;
