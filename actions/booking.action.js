const { customAlphabet } = require("nanoid");
const { MoleculerError, MoleculerClientError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const _ = require("lodash");
const axios = require("axios");
const turf = require("@turf/turf");
const currencyCodeUtil = require("../utils/currenyCode.util");
// const decodePolyline = require('decode-google-map-polyline')
const googleApiUtil = require("../utils/googleApi.util");

const constantUtil = require("../utils/constant.util");
const helperUtil = require("../utils/helper.util");
const storageUtil = require("../utils/storage.util");
// Collection Model
// const adminModel = require("../models/admin.model");
// const professionalModel = require("../models/professional.model");

const {
  bookingObject,
  bookingObjectDetails,
} = require("../utils/mapping.util");
const mappingUtil = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");
const {
  convertToInt,
  setDayStartHours,
  setDayEndHours,
} = require("../utils/common.util");
const {
  lzStringEncode,
  lzStringDecode,
  cryptoAESEncode,
  cryptoAESDecode,
} = require("../utils/crypto.util");
const {
  addDaysToRegionalUTC,
  getDbQueryString,
} = require("../utils/common.util");
const { CONST_RIDE } = require("../utils/constant.util");
const { parseBooleans } = require("xml2js/lib/processors");

const bookingAction = {};

// GET USER PAYMENT OPTIONS LIST
bookingAction.userGetPaymentOptions = async function (context) {
  const clientId = context.params?.clientId || context.meta?.clientId;
  const { INFO_PAYMENT_OPTIONS, USER_NOT_FOUND } =
    notifyMessage.setNotifyLanguage(context.params.langCode);

  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  if (!paymentOptionList) {
    throw new MoleculerError(INFO_PAYMENT_OPTIONS);
  }
  let userData = await this.broker.emit("user.getById", {
    "clientId": clientId,
    "id": context.meta.userId.toString(),
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  //
  let anotherDefaultCard = false;
  if (userData?.cards && userData.cards[0]) {
    userData.cards = userData.cards.map((card) => {
      if (anotherDefaultCard === true && card.isDefault === true) {
        this.broker.emit("user.changeCardDefaultStatus", {
          "clientId": clientId,
          "userId": userData._id.toString(),
          "cardId": card._id.toString(),
          "status": false,
        });
        card.isDefault = false;
      }
      if (card.isDefault) {
        anotherDefaultCard = true;
      }
      return card;
    });
  }
  const responseData = [];
  Object.values(paymentOptionList)
    .filter((item) => item.status === constantUtil.ACTIVE)
    .forEach((option) => {
      // if (option.status === constantUtil.ACTIVE) {
      if (
        option.status === constantUtil.ACTIVE &&
        option.paymentType !== constantUtil.PAYMENTCREDIT
      ) {
        // if (option.paymentType !== constantUtil.PAYMENTCARD) {
        //   const jsonOutput = {
        //     "_id": option._id,
        //     "name": option.name,
        //     "cards":
        //       option.paymentType === constantUtil.PAYMENTCARD
        //         ? userData.cards
        //         : [],
        //     "isDefaultPayment":
        //       userData.defaultPayment === option.name ? true : false,
        //     "status": option.status,
        //   };
        //   responseData.push(jsonOutput);
        // } else if (option.paymentType === constantUtil.PAYMENTCARD) {
        //   let isCard = false;

        //   responseData.forEach((cardData) => {
        //     isCard =
        //       cardData.paymentType !== constantUtil.PAYMENTCARD ? true : false;
        //   });
        //   const cardOjtect = {
        //     "_id": option._id,
        //     "name": option.name,
        //     "cards": userData.cards || [],
        //     "isDefaultPayment":
        //       userData.defaultPayment === option.name ? true : false,
        //     "status": option.status,
        //   };
        //   if (isCard) {
        //     responseData.push(cardOjtect);
        //   }
        // }
        const jsonOutput = {
          "_id": option._id,
          "name": option.name,
          "cards":
            option.paymentType === constantUtil.PAYMENTCARD
              ? userData?.cards || []
              : [],
          "isDefaultPayment":
            userData.defaultPayment === option.paymentType ? true : false,
          "status": option.status,
        };
        responseData.push(jsonOutput);
      }
    });
  const pendingPaymentAmount =
    userData.wallet.availableAmount < 0
      ? parseFloat(Math.abs(userData.wallet.availableAmount).toFixed(2))
      : 0;
  // let paymentGateWay = {};

  // for (const prop in paymentOptionList) {
  //   if (
  //     paymentOptionList[prop].status === constantUtil.ACTIVE &&
  //     paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
  //   ) {
  //     paymentGateWay = paymentOptionList[prop];
  //     paymentGateWay = {
  //       "mode": paymentGateWay.mode,
  //       "gateWay": paymentGateWay.gateWay,
  //       "testSecretKey": paymentGateWay.testSecretKey,
  //       "testPublicKey": paymentGateWay.testPublicKey,
  //       "livePublicKey": paymentGateWay.livePublicKey,
  //       "liveSecretKey": paymentGateWay.liveSecretKey,
  //       "paymentType": paymentGateWay.paymentType,
  //       "isPreAuthRequired": paymentGateWay.isPreAuthRequired,
  //     };
  //     break;
  //   }
  // }
  return {
    "code": 200,
    "data": responseData,
    "pendingPaymentAmount": pendingPaymentAmount,
    // "paymentGateWay": paymentGateWay || {},
  };
};

// NEW RIDE BOOKING
bookingAction.userRideBooking = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseData = {},
    errorCode = 200,
    responseMessage = "";

  const {
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    USER_NOT_FOUND,
    USER_WALLET_PAYMENT_INFO,
    PROFESSIONAL_NOT_FOUND,
    USER_PAYMENT_DECLINED,
    INVALID_BOOKING,
    PROFESSIONAL_NEW_RIDE,
    PROFESSIONAL_NOTIFICATION,
    BOOKING_SCHEDULED_RIDE,
    SOMETHING_WENT_WRONG,
  } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.userDetails?.languageCode
  );
  try {
    const bookingBy = context.params.bookingBy || constantUtil.USER;
    const bookingFrom = context.params.bookingFrom || constantUtil.CONST_APP;
    // const guestType = context.params.guestType || constantUtil.USER;
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const callSettings = await storageUtil.read(constantUtil.CALLSETTING);

    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND, 500);
    }
    let serviceCategoryData = await this.broker.emit(
      "category.getCategoryById",
      {
        "id": context.params.categoryId,
      }
    );
    serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
    if (!serviceCategoryData) {
      throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND, 500);
    }
    serviceCategoryData.categoryData.vehicles.map((vehicle) => {
      if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
        vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
      }
    });

    const clientId =
      context.params.clientId ||
      context.meta.clientId ||
      generalSettings.data.clientId;
    //
    switch (context.params.type.toUpperCase()) {
      case constantUtil.INSTANT: {
        context.params.bookingType = constantUtil.INSTANT;
        break;
      }
      case constantUtil.SCHEDULE: {
        context.params.bookingType = constantUtil.SCHEDULE;
        break;
      }
      case constantUtil.SHARE: {
        context.params.bookingType = constantUtil.SHARE;
        break;
      }
      default: {
        context.params.bookingType = context.params.type?.toUpperCase();
        break;
      }
    }
    const bookingId =
      context.params?.bookingId ||
      `${generalSettings.data.bookingPrefix}-${customAlphabet(
        "1234567890",
        9
      )()}`;
    let paymentData,
      paymentInitId = null,
      paymentInitAmount = 0;
    let userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError(USER_NOT_FOUND, 500);
    }
    let professionalRetryData = [];
    if (context.params.bookingType === constantUtil.INSTANT) {
      professionalRetryData = await this.getProfessionalByLocation(
        //clientId,
        context.params.pickUpLat,
        context.params.pickUpLng,
        context.params.dropLat,
        context.params.dropLng,
        context.params.vehicleCategoryId,
        constantUtil.BOOKING, // actionType
        context.params.rideType,
        context.params?.radius || generalSettings.data.requestDistance,
        // context.params?.maxRadius || generalSettings.data.requestDistance,
        false,
        true,
        generalSettings.data.isTailRideNeeded,
        vehicleCategoryData.isSubCategoryAvailable,
        vehicleCategoryData.isForceAppliedToProfessional,
        context.params.isGenderAvailable || false,
        context.params.childseatAvailable || false,
        context.params.handicapAvailable || false,
        !!(
          userData?.gender === constantUtil.MALE &&
          serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
        ),
        context.params.tripType === constantUtil.RENTAL, //isRentalSupported
        context.params?.serviceAreaId,
        context.params?.isShareRide || false,
        context.params?.paymentOption || null,
        context.params?.languageCode || null,
        context.params?.isPetAllowed || false
      );
      professionalRetryData = professionalRetryData && professionalRetryData[0];
      if (!professionalRetryData?.length) {
        professionalRetryData = await this.getProfessionalByLocation(
          //clientId,
          context.params.pickUpLat,
          context.params.pickUpLng,
          context.params.dropLat,
          context.params.dropLng,
          context.params.vehicleCategoryId,
          constantUtil.BOOKING, // actionType
          context.params.rideType,
          context.params?.radius || generalSettings.data.requestDistance,
          // context.params?.maxRadius || generalSettings.data.requestDistance,
          false,
          true,
          generalSettings.data.isTailRideNeeded,
          vehicleCategoryData.isSubCategoryAvailable,
          vehicleCategoryData.isForceAppliedToProfessional,
          context.params.isGenderAvailable || false,
          context.params.childseatAvailable || false,
          context.params.handicapAvailable || false,
          !!(
            userData?.gender === constantUtil.MALE &&
            serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
          ),
          context.params.tripType === constantUtil.RENTAL, //isRentalSupported
          context.params?.serviceAreaId,
          context.params?.isShareRide || false,
          context.params?.paymentOption || null,
          context.params?.languageCode || null,
          context.params?.isPetAllowed || false
        );
        professionalRetryData =
          professionalRetryData && professionalRetryData[0];
      }
      //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
      professionalRetryData =
        await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
          professionalRetryData,
          null, //bookingData
          userData
        );
      // if (
      //   !professionalRetryData ||
      //   (professionalRetryData && professionalRetryData.length === 0)
      // )
      //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
      //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
      // if (!professionalRetryData?.length)
      //   return {
      //     "code": 503,
      //     "data": {},
      //     "message": PROFESSIONAL_NOT_FOUND,
      //   };
    }

    if (context.params.paymentOption === constantUtil.PAYMENTWALLET) {
      /* @TODO CARD PAYMENT PENDING*/
      if (
        parseFloat(userData.wallet.availableAmount) <
        parseFloat(context.params.estimationAmount)
      ) {
        throw new MoleculerError(USER_WALLET_PAYMENT_INFO, 500);
      }
      if (context.params.bookingType === constantUtil.SCHEDULE) {
        const updateValue =
          parseFloat(userData.wallet.availableAmount) -
          parseFloat(context.params.estimationAmount);
        const scheduleFreezedAmount =
          parseFloat(userData.wallet.scheduleFreezedAmount) +
          parseFloat(context.params.estimationAmount);
        this.broker.emit("user.scheduleWalletUpdate", {
          "userId": context.meta.userId,

          "finalAmount": updateValue,
          "amount": scheduleFreezedAmount,
        });
      }
    }
    if (context.params.paymentOption === constantUtil.PAYMENTCARD) {
      let cardPayment = await this.broker.emit(
        "transaction.newBookingCardPayment",
        {
          ...context.params, // it contains all data
          "bookingId": bookingId,
          "userData": userData,
          "cardId": context.params.card.cardId,
          "estimationAmount": context.params?.estimationAmount,
        }
      );
      cardPayment = cardPayment && cardPayment[0];
      if (!cardPayment || cardPayment?.code > 200) {
        throw new MoleculerClientError(
          USER_PAYMENT_DECLINED,
          400,
          "CARD ERROR",
          cardPayment?.data
        );
      }
      paymentInitId =
        context.params?.paymentIntentId || cardPayment?.data?.paymentInitId;
      paymentInitAmount =
        context.params?.paymentIntentAmount || context.params.estimationAmount;
    }

    let newDate = helperUtil.toUTC(new Date().toISOString());
    let newRegionalDate = helperUtil.toRegionalUTC(new Date());
    if (context.params.bookingType === constantUtil.SCHEDULE) {
      newDate = helperUtil.toUTC(context.params.bookingDate);
      newRegionalDate = helperUtil.toRegionalUTC(context.params.bookingDate);
    }
    //#region algorithm part
    const requestSendProfessionals = [];
    if (
      professionalRetryData.length > 0 &&
      context.params.bookingType === constantUtil.INSTANT &&
      (generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
        generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST)
    ) {
      requestSendProfessionals.push({
        "professionalId": professionalRetryData[0]?._id?.toString(),
        "requestReceiveDateTime": new Date().toISOString(),
        "count": 1,
        "location.coordinates": professionalRetryData[0]?.location?.coordinates,
        "duration": professionalRetryData[0]?.duration,
        "distance": professionalRetryData[0]?.distance,
      });
      professionalRetryData = [professionalRetryData[0]];
    }
    //#endregion algorithm part
    context.params["vehicleCategoryData"] = vehicleCategoryData; // Vehicle Category Details
    //------------ Multiple Collection calling Sample Start ----------------
    // const bookingDataxxx = await adminModel.create({
    //   "name": "DEVELOPER_R",
    //   "age": 30,
    //   "data.defaultPayment": constantUtil.WALLET,
    // });
    // if (bookingDataxxx) throw new MoleculerError(INVALID_BOOKING, 500);
    //------------ Multiple Collection calling Sample End ----------------
    //--------------------------------------------------------------------
    //#region Coupon Issue fix Temporary Need to Remove
    //#region couponList
    if (
      context.meta.userId &&
      context.params.couponApplied &&
      (context.params.couponId === null || context.params.couponId === "")
    ) {
      // this.broker.emit("user.updateServiceAreaId", {
      //   "userId":
      //     context.params.userId?.toString() || context.meta.userId?.toString(),
      //   "serviceAreaId": serviceCategoryData?._id?.toString(),
      // });
      let couponList = await this.broker.emit("admin.getUserBasedCouponList", {
        "lat": context.params.pickUpLat,
        "lng": context.params.pickUpLng,
        "userId":
          context.params.userId?.toString() || context.meta.userId?.toString(),
        // "serviceAreaId": serviceCategoryData?._id?.toString(),
      });
      couponList = (couponList && couponList[0]) || [];
      if (couponList.length > 0) {
        // categoryMappingJSON["couponList"] = couponList;
        context.params["couponId"] = couponList[0]?._id;
        context.params["couponValue"] = couponList[0]?.couponAmount || 0;
        context.params["couponType"] = couponList[0]?.couponType;
        const couponMaxAmount = couponList[0]?.couponMaxAmount || 0;
        const isCouponCheckWithMaxTripAmount =
          couponList[0]?.isCouponCheckWithMaxTripAmount || false;
        let amount =
          couponList[0]?.couponType === constantUtil.PERCENTAGE
            ? parseFloat(
                parseFloat(
                  parseFloat(context.params.travelCharge) *
                    ((couponList[0]?.couponAmount || 0) / 100)
                ).toFixed(2)
              )
            : parseFloat(couponList[0]?.couponAmount.toFixed(2));
        // if amount is more than specified amount so set max amount in coupon
        if (isCouponCheckWithMaxTripAmount) {
          if (
            // parseFloat(data.deliveryCharge) - parseFloat(data.amount) > couponMaxAmount
            parseFloat(context.params.travelCharge) >= couponMaxAmount
          ) {
            amount = 0;
          }
          //} else {
          //   if (amount > couponMaxAmount) {
          //     amount = couponMaxAmount;
          //   }
        } else {
          if (amount > couponMaxAmount) {
            amount = couponMaxAmount;
          }
        }
        context.params["couponAmount"] = amount || 0;
      }
    }
    //#endregion couponList
    //#endregion Coupon Issue fix Temporary Need to Remove
    //--------------------------------------------------------------------
    // let encodedRideCoordinates = null;
    // if (context.params.rawRideCoordinates) {
    //   encodedRideCoordinates = cryptoAESEncode(
    //     context.params.rawRideCoordinates
    //   ); // From Admin Booking
    //   encodedRideCoordinates = encodedRideCoordinates?.toString();
    // }
    let roundTripDetails = {};
    const rideCoordinatesEncoded =
      context.params.rideCoordinatesEncoded ||
      context.params.rawRideCoordinates;
    const isRoundTrip = context.params.isRoundTrip || false;
    if (isRoundTrip) {
      roundTripDetails = {
        "roundTripId": null,
        "roundTripType": constantUtil.CONST_PARENT,
      };
    }
    //------------------------------------------
    let bookingData = await this.adapter.model.create({
      "clientId": clientId,
      "bookingId": bookingId,
      "bookingOTP": customAlphabet("1234567890", 4)().toString(),
      "bookingType": context.params.bookingType,
      "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
      "guestType": constantUtil.USER,
      "bookingDate": newDate,
      "regionalData": {
        "bookingDate": newRegionalDate,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
      },
      "bookingFor": context.params.bookingFor,
      "bookingBy": bookingBy,
      "bookingFrom": bookingFrom,
      "tripType": context.params.tripType,
      "paymentSection": generalSettings.data.paymentSection,
      "admin": null,
      "user": context.meta.userId,
      "corporateEmailId": context.params.corporateEmailId || null,
      "category": context.params.categoryId,
      "couponId": context.params.couponId || null,
      "isGenderAvailable": context.params.isGenderAvailable || false,
      "childseatAvailable": context.params.childseatAvailable || false,
      "handicapAvailable": context.params.handicapAvailable || false,
      "isShareRide": context.params.isShareRide || false,
      "isPetAllowed": context.params.isPetAllowed || false,
      "isEnableLuggage": context.params?.isEnableLuggage || false,
      "vehicle": {
        "vehicleCategoryId": context.params.vehicleCategoryId,
      },
      "origin": {
        "addressName": context.params.pickUpAddressName,
        "fullAddress": context.params.pickUpFullAddress,
        "shortAddress": context.params.pickUpShortAddress,
        "lat": context.params.pickUpLat,
        "lng": context.params.pickUpLng,
      },
      "destination": {
        "addressName": context.params.dropAddressName,
        "fullAddress": context.params.dropFullAddress,
        "shortAddress": context.params.dropShortAddress,
        "lat": context.params.dropLat,
        "lng": context.params.dropLng,
      },
      "estimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "bookedEstimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "waitingTime": {
        "waitingStartTime": null,
        "waitingEndTime": null,
      },
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceUnit": serviceCategoryData.distanceType,
      "invoice": mappingUtil.calculateInvoiceObject(context.params),
      "serviceCategory": context.params.serviceCategory,
      "activity": {
        "noOfUsersInVehicle": null,
        "noOfSeatsAvailable": null,
        "isUserPickedUp": false,
        "isUserDropped": false,
        "waitingDuration": null,
        "actualBookingTime": new Date(),
        "bookingTime": new Date(),
        "denyOrExpireTime": null,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
        "rideStops": context.params.bookingLocations || [],
      },
      "rideStops": context.params.bookingLocations || [],
      "notes": context.params.notes || null,
      "payment": {
        "option": context.params.paymentOption,
        "details": paymentData?.data || null,
        "card":
          context.params.paymentOption === constantUtil.PAYMENTCARD
            ? context.params.card
            : {},
        "paid": true,
      },
      // for strip Payment wave
      "paymentInitId": paymentInitId || null,
      "paymentInitAmount": paymentInitAmount || 0,
      // for algorithm
      "requestSendProfessionals": requestSendProfessionals,
      "requestRetryCount": 1,
      "isRestrictMaleBookingToFemaleProfessional":
        !!serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional,
      "rental": {
        "package": context.params.rentalPackage || null,
      },
      "totalPassengerCount": vehicleCategoryData?.seatCount || 0,
      "currentPassengerCount": context.params.passengerCount || 0,
      "passengerCount": context.params.passengerCount || 0,
      // "location": [],
      "pickupLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "dropLocation.coordinates": [
        [context.params.dropLng, context.params.dropLat],
      ],
      "upcomingLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "rideCoordinatesEncoded": rideCoordinatesEncoded,
      "flightNumber": context.params.flightNumber,
      "welcomeSignboardText": context.params.welcomeSignboardText,
      "isRoundTrip": isRoundTrip,
      "roundTripDetails": roundTripDetails,
      "isHasUSBChargePort": context.params.isHasUSBChargePort,
      "isHasWifi": context.params.isHasWifi,
      "knownLanguages": context.params.knownLanguages,
      "isHasAC": context.params.isHasAC,
    });

    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING, 500);
    } else {
      //#region Packages & Share Ride
      let shareRideProfessional;
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_PACKAGES.toLowerCase():
          this.broker.emit("booking.updatePackageDetailsById", {
            "action": constantUtil.ADD,
            "uploadedBy": constantUtil.USER,
            "packageData": context.params.packageData || {},
            "bookingId": bookingData?._id?.toString(),
          });
          break;

        case constantUtil.CONST_SHARERIDE.toLowerCase():
          {
            const earthRadious =
              bookingData.distanceUnit === constantUtil.KM ? 6371 : 3959; // 3959 --> Default Earth Radious in Miles, 6371 --> Default Earth Radious in KM, 1000 --> KM Value ( Custom Meter / KM / Radious)
            const sharrideSearchRadius =
              generalSettings.data.shareRideMaxThershold / 1000 / earthRadious;
            //update Location
            if (
              // bookingData.location === null ||
              bookingData?.rideLocation?.coordinates?.length <= 1
            ) {
              await this.adapter.model.updateOne(
                {
                  "_id": mongoose.Types.ObjectId(bookingData?._id),
                },
                {
                  "rideLocation.coordinates": context.params?.rideLocation,
                }
              );
            }
            // check pickup
            // const ridePickupData = await this.adapter.model.findOne({
            //   "parentShareRideId": null,
            //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //   "bookingStatus": { "$ne": constantUtil.ENDED },
            //   "location": {
            //     "$geoIntersects": {
            //       "$geometry": {
            //         "type": "Point",
            //         "coordinates": [
            //           context.params.pickUpLng,
            //           context.params.pickUpLat,
            //         ],
            //       },
            //     },
            //     "$minDistance": 0,
            //     "$maxDistance": 500, //need to change dynamic
            //   },
            // });
            //------------ Test -------------
            // const checkRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       "$geoIntersects": {
            //         "$geometry": {
            //           // "type": "Polygon",
            //           "type": "MultiPoint",
            //           "coordinates": [
            //             // [
            //             [context.params.pickUpLng, context.params.pickUpLat],
            //             [context.params.dropLng, context.params.dropLat],
            //             [context.params.dropLng, context.params.dropLat],
            //             [context.params.pickUpLng, context.params.pickUpLat],
            //             // ],
            //           ],
            //         },
            //       },
            //     },
            //   })
            //   .lean();
            // const checkBoxRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation.coordinates": {
            //       "$geoWithin": {
            //         "$box": [
            //           [context.params.pickUpLng, context.params.pickUpLat],
            //           [context.params.dropLng, context.params.dropLat],
            //         ],
            //       },
            //     },
            //   })
            //   .lean();

            // const checkCenterRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation.coordinates": {
            //       "$geoWithin": {
            //         "$centerSphere": [
            //           [context.params.pickUpLng, context.params.pickUpLat],
            //           // [context.params.dropLng, context.params.dropLat],
            //           searchRadius,
            //         ],
            //       },
            //     },
            //   })
            //   .lean();
            //----------- ridePickupData Old ---------
            // const ridePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       // "$nearSphere": {
            //       "$near": {
            //         // "$geoIntersects": {
            //         "$geometry": {
            //           "type": "Point",
            //           "coordinates": [
            //             context.params.pickUpLng,
            //             context.params.pickUpLat,
            //           ],
            //         },
            //         "$minDistance": 0,
            //         "$maxDistance": 2000, //need to change dynamic
            //       },
            //     },
            //   })
            //   .lean();
            //----------- ridePickupData New ---------
            const ridePickupData = await this.adapter.model
              .findOne({
                "_id": {
                  "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                "parentShareRideId": null,
                "isShareRide": true,
                "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
                "bookingStatus": {
                  // "$ne": constantUtil.ENDED,
                  "$in": [
                    // constantUtil.AWAITING,
                    constantUtil.ACCEPTED,
                    constantUtil.ARRIVED,
                    constantUtil.STARTED,
                  ],
                },
                "rideLocation.coordinates": {
                  "$geoWithin": {
                    "$centerSphere": [
                      [context.params.pickUpLng, context.params.pickUpLat],
                      sharrideSearchRadius,
                    ],
                  },
                },
              })
              .populate(
                "professional",
                { "_id": 1 },
                {
                  "location.coordinates": {
                    "$geoWithin": {
                      "$centerSphere": [
                        [context.params.pickUpLng, context.params.pickUpLat],
                        sharrideSearchRadius,
                      ],
                    },
                  },
                }
              )
              .lean();
            // if (
            //   ridePickupData ||
            //   checkCenterRidePickupData ||
            //   checkBoxRidePickupData
            // )
            //   throw new MoleculerError("Success", 500);
            // else throw new MoleculerError("Success", 500);
            // // check drop
            // // const rideDropData = await this.adapter.model.findOne({
            // //   "parentShareRideId": null,
            // //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            // //   "bookingStatus": { "$ne": constantUtil.ENDED },
            // //   "location": {
            // //     "$geoIntersects": {
            // //       "$geometry": {
            // //         "type": "Point",
            // //         "coordinates": [
            // //           context.params.dropLng,
            // //           context.params.dropLat,
            // //         ],
            // //       },
            // //     },
            // //     "$minDistance": 0,
            // //     "$maxDistance": 500, //need to change dynamic
            // //   },
            // // });
            //----------- rideDropData Old ---------
            // const rideDropData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       // "$nearSphere": {
            //       "$near": {
            //         // "$geoIntersects": {
            //         "$geometry": {
            //           "type": "Point",
            //           "coordinates": [
            //             context.params.dropLng,
            //             context.params.dropLat,
            //           ],
            //         },
            //         "$minDistance": 0,
            //         "$maxDistance": 2000, //need to change dynamic
            //       },
            //     },
            //   })
            //   .lean();
            //----------- rideDropData New ---------
            const rideDropData = await this.adapter.model
              .findOne({
                "_id": {
                  "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                "parentShareRideId": null,
                "isShareRide": true,
                "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
                "bookingStatus": {
                  // "$ne": constantUtil.ENDED,
                  "$in": [
                    // constantUtil.AWAITING,
                    constantUtil.ACCEPTED,
                    constantUtil.ARRIVED,
                    constantUtil.STARTED,
                  ],
                },
                "rideLocation.coordinates": {
                  "$geoWithin": {
                    "$centerSphere": [
                      [context.params.dropLng, context.params.dropLat],
                      sharrideSearchRadius,
                    ],
                  },
                },
              })
              .lean();
            // if (ridePickupData || rideDropData)
            //   throw new MoleculerError("Success", 500);
            // else throw new MoleculerError("Success", 500);
            //---------- Sample code ------------
            // await this.adapter.model.findOne({
            //   "parentShareRideId": null,
            //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //   "bookingStatus": { "$ne": constantUtil.ENDED },
            //   "location": {
            //     // "$nearSphere": {
            //     "$near": {
            //       "$geometry": {
            //         "type": "Point",
            //         //"coordinates": [77.3266,8.2506],
            //         "coordinates": [77.3124073, 8.248572],
            //       },
            //       "$minDistance": 0,
            //       "$maxDistance": 2400, //need to change dynamic
            //     },
            //   },
            // });
            //-------
            // -------------------------
            const currentPassengerCount =
              parseInt(ridePickupData?.currentPassengerCount || 0) +
              parseInt(context.params?.passengerCount || 0);
            if (
              ridePickupData &&
              rideDropData &&
              ridePickupData?.professional?._id && // Is professional Active
              ridePickupData?._id?.toString() !==
                bookingData?._id?.toString() &&
              rideDropData?._id?.toString() !== bookingData?._id?.toString() &&
              currentPassengerCount <=
                parseInt(ridePickupData.totalPassengerCount)
            ) {
              // Find & Select Current Share Ride Professional
              professionalRetryData =
                await this.findCurrentShareRideProfessional(
                  constantUtil.ADD, // Action
                  professionalRetryData,
                  ridePickupData?.professional?._id?.toString() // --> currentShareRideProfessionalId
                );
              // //update currentPassengerCount in Parent Share Ride
              // await this.adapter.model.updateOne(
              //   {
              //     "_id": mongoose.Types.ObjectId(ridePickupData?._id),
              //     "parentShareRideId": null,
              //   },
              //   {
              //     "$inc": {
              //       "currentPassengerCount": parseInt(
              //         context.params?.passengerCount || 0
              //       ),
              //     },
              //   }
              // );
              //update Child Share Ride
              await this.adapter.model.updateOne(
                {
                  "_id": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                {
                  "parentShareRideId": ridePickupData?._id?.toString(),
                  "parentShareRideProfessional":
                    ridePickupData?.professional?._id?.toString(),
                  // "rideLocation.coordinates": [
                  //   [context.params.pickUpLng, context.params.pickUpLat],
                  //   [context.params.dropLng, context.params.dropLat],
                  // ],
                  "requestSendProfessionals":
                    generalSettings.data.bookingAlgorithm ===
                      constantUtil.NEAREST ||
                    generalSettings.data.bookingAlgorithm ===
                      constantUtil.SHORTEST
                      ? {
                          "$push": {
                            "professionalId":
                              professionalRetryData[0]?._id?.toString(),
                            "requestReceiveDateTime": new Date().toISOString(),
                            "count": 1,
                            "location.coordinates":
                              professionalRetryData[0]?.location?.coordinates,
                            "duration": professionalRetryData[0]?.duration,
                            "distance": professionalRetryData[0]?.distance,
                          },
                        }
                      : null,
                }
              );
              // }
            } else {
              // Find and Remove Current Share Ride Professional
              const parentShareRideProfessional =
                ridePickupData?.parentShareRideProfessional?.toString() ||
                rideDropData?.parentShareRideProfessional?.toString();
              professionalRetryData =
                await this.findCurrentShareRideProfessional(
                  constantUtil.REMOVE, // Action
                  professionalRetryData,
                  parentShareRideProfessional // --> currentShareRideProfessionalId
                );
            }
          }
          break;
      }
      //#endregion Packages & Share Ride
      // Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": bookingData?._id?.toString(),
      }); // Get booking Details
    }
    // bookingData = bookingData.toJSON();
    if (isRoundTrip) {
      roundTripDetails = {
        "roundTripId": bookingData?._id?.toString(),
        "roundTripType": constantUtil.CONST_CHILD,
      };
      // Insert Round Trip ( Return Trip )
      await this.broker.emit("booking.rideBookingDetails", {
        ...context.params, // it contains all data
        "bookingPrefix": generalSettings.data.bookingPrefix,
        "guestType": constantUtil.USER,
        "bookingBy": bookingBy,
        "bookingFrom": bookingFrom,
        "currencySymbol": serviceCategoryData.currencySymbol,
        "currencyCode": serviceCategoryData.currencyCode,
        "distanceUnit": serviceCategoryData.distanceType,
        "pickUpLng": context.params.dropLng, // For Return Trip --> dropLng as pickUpLng
        "pickUpLat": context.params.dropLat, // For Return Trip --> dropLat as pickUpLat
        "dropLng": context.params.pickUpLng, // For Return Trip --> pickUpLng as dropLng
        "dropLat": context.params.pickUpLat, // For Return Trip --> pickUpLat as dropLat
        "paymentSection": generalSettings.data.paymentSection,
        "payment": {
          "option": context.params.paymentOption,
          "details": paymentData?.data || null,
          "card":
            context.params.paymentOption === constantUtil.PAYMENTCARD
              ? context.params.card
              : {},
          "paid": true,
        },
        "paymentInitId": null,
        "paymentInitAmount": null,
        "userId": context.params.userId || context.meta.userId,
        "newDate": newDate,
        "newRegionalDate": newRegionalDate,
        "rideCoordinatesEncoded": null,
        "isRoundTrip": isRoundTrip,
        "roundTripDetails": roundTripDetails,
      });
    }
    // track link
    const trackLink = `${generalSettings.data.redirectUrls.trackBooking}${bookingData.bookingId}`;

    bookingData.trackLink = trackLink;

    bookingData.user = {
      "firstName": context.meta.userDetails.firstName,
      "lastName": context.meta.userDetails.lastName,
      "phone": context.meta.userDetails.phone,
      "avatar": context.meta.userDetails.avatar,
      "review": context.meta.userDetails.review,
      "blockedProfessionalList": bookingData.user.blockedProfessionalList,
    };
    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    bookingData.vehicle.isShowProfessionalList =
      vehicleCategoryData.isShowProfessionalList;

    bookingData.retryTime = generalSettings.data.driverRequestTimeout;
    bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
    // bookingData.distanceUnit = serviceCategoryData.distanceType;
    bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;

    bookingData["category"]["isNeedSecurityImageUpload"] =
      serviceCategoryData?.isNeedSecurityImageUpload || false;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    responseData = bookingObject(bookingData);
    if (context.params.bookingType === constantUtil.INSTANT) {
      if (professionalRetryData.length > 0) {
        const notificationObject = {
          "temprorySound": true,
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.PROFESSIONAL,
            "action": constantUtil.ACTION_BOOKINGREQUEST,
            "timestamp": Date.now(),
            "message": PROFESSIONAL_NEW_RIDE, //"YOU GOT A NEW RIDE",
            "details": lzStringEncode(responseData),
          },
          "registrationTokens": professionalRetryData.map((professional) => {
            if (
              // parseFloat(professional.wallet.availableAmount) <  parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
              bookingData.payment.option === constantUtil.WALLET ||
              bookingData.payment.option === constantUtil.PAYMENTCARD ||
              // bookingData.payment.option !== constantUtil.CASH &&
              // bookingData.payment.option !== constantUtil.CONST_PIX &&
              // bookingData.payment.option !== constantUtil.CONST_POS &&
              // bookingData.payment.option !== constantUtil.CREDIT) ||
              parseFloat(professional.wallet.availableAmount) >=
                parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
            ) {
              return {
                "token": professional?.deviceInfo[0]?.deviceId || "",
                "id": professional?._id?.toString(),
                "deviceType": professional?.deviceInfo[0]?.deviceType || "",
                "platform": professional?.deviceInfo[0]?.platform || "",
                "socketId": professional?.deviceInfo[0]?.socketId || "",
                "duration": professional?.duration,
                "distance": professional?.distance,
              };
            }
          }),
        };
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
        responseMessage = PROFESSIONAL_NOTIFICATION;
      } else {
        if (
          parseInt(context.params.radius) < parseInt(context.params.maxRadius)
        ) {
          errorCode = 600; // 600 --> For Retry Booking Purpose
          responseMessage = PROFESSIONAL_NOTIFICATION;
        } else {
          errorCode = 500;
          responseMessage = PROFESSIONAL_NOT_FOUND;
        }
      }
      //#region SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE
      if (generalSettings.data?.notifyAdmin?.isEnableNewRideBook) {
        const adminNotificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.ADMIN,
            "action": constantUtil.ACTION_BOOKINGREQUEST,
            "timestamp": Date.now(),
            "message": PROFESSIONAL_NEW_RIDE,
            "details": responseData,
          },
          "registrationTokens": [{ "userType": constantUtil.ADMIN }],
        };
        this.broker.emit(
          "socket.sendAdminNotificationForRide",
          adminNotificationObject
        );
      }
      //#endregion SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE

      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": PROFESSIONAL_NOTIFICATION, //"NOTIFICATION SEND TO NEARBY PROFESSIONALS",
      // };
    }
    if (context.params.bookingType === constantUtil.SCHEDULE) {
      let cachedBookingData = await storageUtil.read(constantUtil.SCHEDULE);
      if (!cachedBookingData) {
        cachedBookingData = [];
      }
      cachedBookingData.push({
        "_id": bookingData._id,
        "bookingId": bookingData.bookingId,
        "bookingDate": bookingData.bookingDate,
      });
      storageUtil.write(constantUtil.SCHEDULE, cachedBookingData);

      this.broker.emit("user.updateScheduleRide", {
        "userId": context.meta.userId,
        "bookingId": bookingData._id.toString(),
        "bookingDate": bookingData.bookingDate,
        "estimationTime": context.params.estimationTime,
      });
      responseMessage = BOOKING_SCHEDULED_RIDE;
      // console.log("this is bookingData", bookingData);
      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": BOOKING_SCHEDULED_RIDE, //"BOOKING SCHEDULED SUCCESSFULLY",
      // };
    }
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

// USER RETRY BOOKING
bookingAction.userRetryBooking = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  let responseData,
    errorCode = 200,
    responseMessage = "",
    languageCode;

  const userType = (context.params?.userType ?? "").toUpperCase();
  let userData = null;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    PROFESSIONAL_NEW_RIDE,
    PROFESSIONAL_NOTIFICATION,
    BOOKING_STATUS_IS,
    SOMETHING_WENT_WRONG,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
    const clientId =
      context.params.clientId ||
      context.meta.clientId ||
      generalSettings.data.clientId;
    //
    const bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    vehicleCategoryData =
      vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

    let serviceCategoryData = await this.broker.emit(
      "category.getCategoryById",
      {
        "id": bookingData.category._id.toString(),
      }
    );
    serviceCategoryData = serviceCategoryData[0];
    if (!serviceCategoryData) {
      throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
    }
    serviceCategoryData.categoryData.vehicles.map((vehicle) => {
      if (
        vehicle.categoryId.toString() ===
        bookingData.vehicle.vehicleCategoryId.toString()
      ) {
        vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
      }
    });
    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    bookingData.vehicle.isShowProfessionalList =
      vehicleCategoryData.isShowProfessionalList;

    bookingData.retryTime = generalSettings.data.driverRequestTimeout;
    bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
    bookingData["category"]["isNeedSecurityImageUpload"] =
      serviceCategoryData?.isNeedSecurityImageUpload || false;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;
    if (bookingData.bookingStatus === constantUtil.USERCANCELLED) {
      bookingData["bookingStatus"] = constantUtil.USERDENY;
    }
    responseData = bookingObject(bookingData); //Response Data
    if (bookingData.bookingStatus === constantUtil.AWAITING) {
      let professionalData = {};
      switch ((context?.params?.retryType ?? "").toUpperCase()) {
        case constantUtil.PROFESSIONALSELECT:
          professionalData = await this.broker.emit("professional.getById", {
            "id": context.params.professionalId.toString(),
          });
          break;

        default:
          professionalData = await this.getProfessionalByLocation(
            //clientId,
            bookingData.origin.lat,
            bookingData.origin.lng,
            bookingData.destination.lat,
            bookingData.destination.lng,
            bookingData.vehicle.vehicleCategoryId.toString(),
            constantUtil.BOOKING, // actionType
            context.params.rideType,
            context.params?.radius || generalSettings.data.requestDistance,
            // context.params?.maxRadius || generalSettings.data.requestDistance,
            false,
            true,
            generalSettings.data.isTailRideNeeded,
            vehicleCategoryData.isSubCategoryAvailable,
            vehicleCategoryData.isForceAppliedToProfessional,
            bookingData.isGenderAvailable,
            bookingData.childseatAvailable,
            bookingData.handicapAvailable,
            !!(
              bookingData?.user?.gender === constantUtil.MALE &&
              serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
            ),
            bookingData.tripType === constantUtil.RENTAL, //isRentalSupported
            context.params?.serviceAreaId,
            bookingData.isShareRide,
            bookingData?.payment.option || constantUtil.CASH,
            bookingData?.languageCode || null,
            bookingData?.isPetAllowed || false
          );
          break;
      }
      professionalData = professionalData && professionalData[0];
      // if (professionalData.length === 0) {
      //   return {
      //     "code": 500,
      //     "data": {},
      //     "message": PROFESSIONAL_NOT_FOUND,
      //   };
      // }
      // throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
      if (professionalData.length > 0) {
        professionalData =
          await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
            professionalData,
            bookingData,
            userData
          );
      }
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_SHARERIDE.toLowerCase():
          {
            const earthRadious =
              bookingData.distanceUnit === constantUtil.KM ? 6371 : 3959; // 3959 --> Default Earth Radious in Miles, 6371 --> Default Earth Radious in KM, 1000 --> KM Value ( Custom Meter / KM / Radious)
            const sharrideSearchRadius =
              generalSettings.data.shareRideMaxThershold / 1000 / earthRadious;
            // //update Location
            // if (
            //   // bookingData.location === null ||
            //   bookingData?.rideLocation?.coordinates?.length <= 1
            // ) {
            //   await this.adapter.model.updateOne(
            //     {
            //       "_id": mongoose.Types.ObjectId(bookingData?._id),
            //     },
            //     {
            //       "pickupLocation.coordinates": [
            //         [context.params.pickUpLng, context.params.pickUpLat],
            //       ],
            //       "dropLocation.coordinates": [
            //         [context.params.dropLng, context.params.dropLat],
            //       ],
            //       "rideLocation.coordinates": context.params?.rideLocation,
            //     }
            //   );
            // }
            // check pickup
            // const ridePickupData = await this.adapter.model.findOne({
            //   "parentShareRideId": null,
            //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //   "bookingStatus": { "$ne": constantUtil.ENDED },
            //   "location": {
            //     "$geoIntersects": {
            //       "$geometry": {
            //         "type": "Point",
            //         "coordinates": [
            //           context.params.pickUpLng,
            //           context.params.pickUpLat,
            //         ],
            //       },
            //     },
            //     "$minDistance": 0,
            //     "$maxDistance": 500, //need to change dynamic
            //   },
            // });
            //------------ Test -------------
            // const checkRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       "$geoIntersects": {
            //         "$geometry": {
            //           // "type": "Polygon",
            //           "type": "MultiPoint",
            //           "coordinates": [
            //             // [
            //             [context.params.pickUpLng, context.params.pickUpLat],
            //             [context.params.dropLng, context.params.dropLat],
            //             [context.params.dropLng, context.params.dropLat],
            //             [context.params.pickUpLng, context.params.pickUpLat],
            //             // ],
            //           ],
            //         },
            //       },
            //     },
            //   })
            //   .lean();
            // const checkBoxRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation.coordinates": {
            //       "$geoWithin": {
            //         "$box": [
            //           [context.params.pickUpLng, context.params.pickUpLat],
            //           [context.params.dropLng, context.params.dropLat],
            //         ],
            //       },
            //     },
            //   })
            //   .lean();

            // const checkCenterRidePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation.coordinates": {
            //       "$geoWithin": {
            //         "$centerSphere": [
            //           [context.params.pickUpLng, context.params.pickUpLat],
            //           // [context.params.dropLng, context.params.dropLat],
            //           searchRadius,
            //         ],
            //       },
            //     },
            //   })
            //   .lean();
            //----------- ridePickupData Old ---------
            // const ridePickupData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       // "$nearSphere": {
            //       "$near": {
            //         // "$geoIntersects": {
            //         "$geometry": {
            //           "type": "Point",
            //           "coordinates": [
            //             context.params.pickUpLng,
            //             context.params.pickUpLat,
            //           ],
            //         },
            //         "$minDistance": 0,
            //         "$maxDistance": 2000, //need to change dynamic
            //       },
            //     },
            //   })
            //   .lean();
            //----------- ridePickupData New ---------
            const ridePickupData = await this.adapter.model
              .findOne({
                "_id": {
                  "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                "parentShareRideId": null,
                "isShareRide": true,
                "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
                "bookingStatus": {
                  // "$ne": constantUtil.ENDED,
                  "$in": [
                    // constantUtil.AWAITING,
                    constantUtil.ACCEPTED,
                    constantUtil.ARRIVED,
                    constantUtil.STARTED,
                  ],
                },
                "rideLocation.coordinates": {
                  "$geoWithin": {
                    "$centerSphere": [
                      bookingData.pickupLocation?.coordinates[0],
                      sharrideSearchRadius,
                    ],
                  },
                },
              })
              .populate(
                "professional",
                { "_id": 1 },
                {
                  "location.coordinates": {
                    "$geoWithin": {
                      "$centerSphere": [
                        bookingData.pickupLocation?.coordinates[0],
                        sharrideSearchRadius,
                      ],
                    },
                  },
                }
              )
              .lean();
            // if (
            //   ridePickupData ||
            //   checkCenterRidePickupData ||
            //   checkBoxRidePickupData
            // )
            //   throw new MoleculerError("Success", 500);
            // else throw new MoleculerError("Success", 500);
            // // check drop
            // // const rideDropData = await this.adapter.model.findOne({
            // //   "parentShareRideId": null,
            // //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            // //   "bookingStatus": { "$ne": constantUtil.ENDED },
            // //   "location": {
            // //     "$geoIntersects": {
            // //       "$geometry": {
            // //         "type": "Point",
            // //         "coordinates": [
            // //           context.params.dropLng,
            // //           context.params.dropLat,
            // //         ],
            // //       },
            // //     },
            // //     "$minDistance": 0,
            // //     "$maxDistance": 500, //need to change dynamic
            // //   },
            // // });
            //----------- rideDropData Old ---------
            // const rideDropData = await this.adapter.model
            //   .findOne({
            //     "_id": {
            //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
            //     },
            //     "parentShareRideId": null,
            //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //     "bookingStatus": {
            //       // "$ne": constantUtil.ENDED,
            //       "$in": [
            //         // constantUtil.AWAITING,
            //         constantUtil.ACCEPTED,
            //         constantUtil.ARRIVED,
            //       ],
            //     },
            //     "rideLocation": {
            //       // "$nearSphere": {
            //       "$near": {
            //         // "$geoIntersects": {
            //         "$geometry": {
            //           "type": "Point",
            //           "coordinates": [
            //             context.params.dropLng,
            //             context.params.dropLat,
            //           ],
            //         },
            //         "$minDistance": 0,
            //         "$maxDistance": 2000, //need to change dynamic
            //       },
            //     },
            //   })
            //   .lean();
            //----------- rideDropData New ---------
            const rideDropData = await this.adapter.model
              .findOne({
                "_id": {
                  "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                "parentShareRideId": null,
                "isShareRide": true,
                "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
                "bookingStatus": {
                  // "$ne": constantUtil.ENDED,
                  "$in": [
                    // constantUtil.AWAITING,
                    constantUtil.ACCEPTED,
                    constantUtil.ARRIVED,
                    constantUtil.STARTED,
                  ],
                },
                "rideLocation.coordinates": {
                  "$geoWithin": {
                    "$centerSphere": [
                      bookingData.dropLocation?.coordinates[0],
                      sharrideSearchRadius,
                    ],
                  },
                },
              })
              .lean();
            // if (ridePickupData || rideDropData)
            //   throw new MoleculerError("Success", 500);
            // else throw new MoleculerError("Success", 500);
            //---------- Sample code ------------
            // await this.adapter.model.findOne({
            //   "parentShareRideId": null,
            //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
            //   "bookingStatus": { "$ne": constantUtil.ENDED },
            //   "location": {
            //     // "$nearSphere": {
            //     "$near": {
            //       "$geometry": {
            //         "type": "Point",
            //         //"coordinates": [77.3266,8.2506],
            //         "coordinates": [77.3124073, 8.248572],
            //       },
            //       "$minDistance": 0,
            //       "$maxDistance": 2400, //need to change dynamic
            //     },
            //   },
            // });
            //-------
            // -------------------------
            const currentPassengerCount =
              parseInt(ridePickupData?.currentPassengerCount || 0) +
              parseInt(bookingData?.passengerCount || 0);
            if (
              ridePickupData &&
              rideDropData &&
              ridePickupData?.professional?._id && // Is professional Active
              ridePickupData?._id?.toString() !==
                bookingData?._id?.toString() &&
              rideDropData?._id?.toString() !== bookingData?._id?.toString() &&
              currentPassengerCount <=
                parseInt(ridePickupData.totalPassengerCount)
            ) {
              // Find Current Share Ride Professional
              const professionalDataNewList =
                await this.findCurrentShareRideProfessional(
                  constantUtil.NEXT, // Action
                  professionalData,
                  ridePickupData?.professional?._id?.toString() // --> currentShareRideProfessionalId
                );
              if (professionalDataNewList?.length === 0) {
                professionalData = await this.findCurrentShareRideProfessional(
                  constantUtil.REMOVE, // Action
                  professionalData,
                  ridePickupData?.professional?._id?.toString() // --> currentShareRideProfessionalId
                );
              } else {
                professionalData = professionalDataNewList;
              }
              // //update currentPassengerCount in Parent Share Ride
              // await this.adapter.model.updateOne(
              //   {
              //     "_id": mongoose.Types.ObjectId(ridePickupData?._id),
              //     "parentShareRideId": null,
              //   },
              //   {
              //     "$inc": {
              //       "currentPassengerCount": parseInt(
              //         context.params?.passengerCount || 0
              //       ),
              //     },
              //   }
              // );
              // //update Child Share Ride
              // await this.adapter.model.updateOne(
              //   {
              //     "_id": mongoose.Types.ObjectId(bookingData?._id?.toString()),
              //   },
              //   {
              //     "parentShareRideId": ridePickupData?._id?.toString(),
              //     "parentShareRideProfessional":
              //       ridePickupData?.professional?.toString(),
              //     // "rideLocation.coordinates": [
              //     //   [context.params.pickUpLng, context.params.pickUpLat],
              //     //   [context.params.dropLng, context.params.dropLat],
              //     // ],
              //     "requestSendProfessionals":
              //       generalSettings.data.bookingAlgorithm ===
              //       constantUtil.NEAREST
              //         ? {
              //             "$push": {
              //               "professionalId":
              //                 professionalRetryData[0]._id.toString(),
              //               "requestReceiveDateTime": new Date().toISOString(),
              //               "count": 1,
              //             },
              //           }
              //         : null,
              //   }
              // );
              // // }
            } else {
              // Find and Remove Current Share Ride Professional
              const parentShareRideProfessional =
                ridePickupData?.parentShareRideProfessional?.toString() ||
                rideDropData?.parentShareRideProfessional?.toString();
              professionalData = await this.findCurrentShareRideProfessional(
                constantUtil.REMOVE, // Action
                professionalData,
                parentShareRideProfessional // --> currentShareRideProfessionalId
              );
            }
          }
          break;
      }
      let registrationTokens = [];
      if (professionalData.length > 0) {
        switch ((context?.params?.retryType ?? "").toUpperCase()) {
          case constantUtil.PROFESSIONALSELECT:
            // Asign Professional device Details
            registrationTokens = [
              {
                "id": professionalData?._id?.toString(),
                "token": professionalData?.deviceInfo[0]?.deviceId,
                "deviceType": professionalData?.deviceInfo[0]?.deviceType,
                "platform": professionalData?.deviceInfo[0]?.platform,
                "socketId": professionalData?.deviceInfo[0]?.socketId,
                "duration": professionalData?.duration,
                "distance": professionalData?.distance,
              },
            ];
            break;

          default:
            //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
            // userData = await this.broker.emit("user.getById", {
            //   "id": context.meta.userId.toString(),
            // });
            userData = bookingData.user;
            // professionalData =
            //   await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
            //     professionalData,
            //     bookingData,
            //     userData
            //   );
            // if (
            //   !professionalData ||
            //   (professionalData && professionalData.length === 0)
            // )
            //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
            //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
            // Asign Professional device Details
            // if (professionalData.length > 0) {
            registrationTokens = professionalData.map((professional) => {
              if (
                // parseFloat(professional.wallet.availableAmount) <
                //   parseFloat(
                //     generalSettings.data?.minimumWalletAmountToOnline
                //   ) &&
                bookingData.payment.option === constantUtil.WALLET ||
                bookingData.payment.option === constantUtil.PAYMENTCARD ||
                // bookingData.payment.option !== constantUtil.CASH &&
                // bookingData.payment.option !== constantUtil.CREDIT) ||
                parseFloat(professional.wallet.availableAmount) >=
                  parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
              ) {
                return {
                  "id": professional?._id?.toString(),
                  "token": professional?.deviceInfo[0]?.deviceId || "",
                  "deviceType": professional?.deviceInfo[0]?.deviceType || "",
                  "platform": professional?.deviceInfo[0]?.platform || "",
                  "socketId": professional?.deviceInfo[0]?.socketId || "",
                  "duration": professional?.duration,
                  "distance": professional?.distance,
                };
              }
            });
            // } else {
            //   if (
            //     parseInt(context.params.radius) <
            //     parseInt(context.params.maxRadius)
            //   ) {
            //     errorCode = 600; // 600 --> For Retry Booking Purpose
            //      message = PROFESSIONAL_NOTIFICATION;
            //   }else {
            //    errorCode = 500;
            //    message = PROFESSIONAL_NOT_FOUND;
            //  }
            // }
            break;
        }
        const notificationObject = {
          "temprorySound": true,
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.PROFESSIONAL,
            "action": constantUtil.ACTION_BOOKINGREQUEST,
            "timestamp": Date.now(),
            "message": PROFESSIONAL_NEW_RIDE, //"YOU GOT A NEW RIDE",
            "details": lzStringEncode(responseData),
          },
          "registrationTokens": registrationTokens,
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
        //
        responseMessage = PROFESSIONAL_NOTIFICATION;
      } else {
        if (
          parseInt(context.params.radius) < parseInt(context.params.maxRadius)
        ) {
          errorCode = 600; // 600 --> For Retry Booking Purpose
          responseMessage = PROFESSIONAL_NOTIFICATION;
        } else {
          errorCode = 500;
          responseMessage = PROFESSIONAL_NOT_FOUND;
        }
      }
      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": PROFESSIONAL_NOTIFICATION, //"NOTIFICATION SEND TO NEARBY PROFESSIONALS",
      // };
    } else {
      responseMessage = BOOKING_STATUS_IS + " " + bookingData.bookingStatus;
    }

    // return {
    //   "code": 200,
    //   "data": bookingObject(bookingData),
    //   "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
    // };
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

// RIDE BOOKING TRACKING
bookingAction.trackBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  const { INVALID_BOOKING, VEHICLE_CATEGORY_NOT_FOUND, BOOKING_STATUS_IS } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.userDetails?.languageCode
    );

  // const bookingData = await this.adapter.model
  //   .findOne({
  //     "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
  //   })
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")

  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();

  // Get booking Details
  const bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId.toString(),
  });
  // Get booking Details
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();
  // track link
  const trackLink = `${generalSettings.data.redirectUrls.trackBooking}${bookingData.bookingId}`;

  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = bookingData.category.distanceType;
  bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
  bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;
  return {
    "code": 200,
    "data": bookingObject({ ...bookingData, "trackLink": trackLink }),
    "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
  };
};

// EXPIRY OR CANCEL FOR USER AND PROFESSIONAL
bookingAction.bookingExpiryOrCancel_Old = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    INVALID_ACCESS,
    INVALID_BOOKING,
    BOOKING_STATUS_INFO,
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_STATUS_IS,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  if (userType === constantUtil.USER) {
    if (
      !(
        context.meta.userId &&
        (context.params.denyType === constantUtil.EXPIRED ||
          context.params.denyType === constantUtil.USERCANCELLED)
      )
    ) {
      throw new MoleculerError(INVALID_ACCESS);
    }
  }
  if (userType === constantUtil.PROFESSIONAL) {
    if (
      !(
        context.meta.professionalId &&
        (context.params.denyType === constantUtil.EXPIRED ||
          context.params.denyType === constantUtil.PROFESSIONALCANCELLED)
      )
    ) {
      throw new MoleculerError(INVALID_ACCESS);
    }
  }
  let bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();
  const paymentInitId = bookingData?.paymentInitId?.toString();
  const paymentInitAmount = bookingData?.paymentInitAmount?.toString();
  const cardId = bookingData?.payment?.card?.cardId?.toString();
  const refBookingId = bookingData?.bookingId?.toString();
  const bookingId = bookingData?._id;

  if (bookingData.bookingStatus === constantUtil.AWAITING) {
    const updateData =
      userType === constantUtil.USER
        ? context.params.denyType === constantUtil.EXPIRED
          ? {
              "bookingStatus": constantUtil.EXPIRED,
              "activity.denyOrExpireTime": new Date(),
            }
          : {
              "bookingStatus": constantUtil.USERDENY,
              "activity.denyOrExpireTime": new Date(),
            }
        : bookingData.bookingType === constantUtil.SCHEDULE
        ? {
            "$push": {
              "denyDriverList": context.meta.professionalId,
              "denyProfessionals": {
                "professionalId": context.meta.professionalId,
                "requestDenyDateTime": new Date(),
              },
            },
            "$pull": { "wishList": context.meta.professionalId },
          }
        : {
            "$push": {
              "denyDriverList": context.meta.professionalId,
              "denyProfessionals": {
                "professionalId": context.meta.professionalId,
                "requestDenyDateTime": new Date(),
              },
            },
          };

    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "bookingStatus": constantUtil.AWAITING,
      },
      updateData
    );
    if (!updatedBookingData.nModified) {
      throw new MoleculerError(BOOKING_STATUS_INFO);
    }
    // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
    if (bookingData.user) {
      if (bookingData.bookingType === constantUtil.SCHEDULE)
        this.broker.emit("user.removeScheduleBooking", {
          "userId": bookingData.user._id.toString(),
          "bookingType": bookingData.bookingType,
          "bookingId": context.params.bookingId,
        });
      //------------ Refund-Ajith End -------------
      //Refund
      if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
        await this.broker.emit("transaction.newBookingCancelRefund", {
          "paymentTokenId": bookingData?.payment?.details?.id,
          "userId": bookingData?.user?._id?.toString(),
          "amount": 0,
          "currencyCode": bookingData.currencyCode,
          "paymentInitId": paymentInitId,
          "paymentInitAmount": paymentInitAmount,
          "cardId": cardId,
          "bookingId": bookingId,
          "refBookingId": refBookingId,
          "transactionType": constantUtil.CANCELCAPTURE,
        });
        // if (bookingData.bookingType === constantUtil.INSTANT) {
        //   await this.broker.emit('user.updateWalletAmountInBooking', {
        //     'userId': context.meta.userId,
        //     'amount': 0,
        //     'finalAmount': parseFloat(
        //       bookingData.user.wallet.availableAmount.toFixed(2)
        //     ),
        //   })
        //   let paymentData = await this.broker.emit(
        //     'transaction.newBookingCancelRefund',
        //     {
        //       'paymentTokenId': bookingData.payment.details.id,
        //       'userId': context.meta.userId,
        //       'amount': bookingData.invoice.estimationAmount.toFixed(2),
        //       'currencyCode': bookingData.currencyCode,
        //     }
        //   )
        //   paymentData = paymentData[0]
        //   if (
        //     !paymentData ||
        //     paymentData === undefined ||
        //     paymentData === 'undefined' ||
        //     paymentData.code === 0
        //   )
        //     throw new MoleculerError(
        //       'ERROR IN CARD PAYMENT, KINDLY TRY AFTER SOMETIME'
        //     )
        // }
      }
      //------------ Refund-Ajith End -------------
    }

    bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();
    // bookingData = bookingData.toJSON();
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    vehicleCategoryData =
      vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
  }

  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
  };
};
// booking expiry or cancel
// EXPIRY OR CANCEL FOR USER AND PROFESSIONAL
bookingAction.bookingExpiryOrCancel2 = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    INVALID_BOOKING,
    BOOKING_STATUS_IS,
    BOOKING_CANCELLED_BY,
    RIDE_CANCELLED,
  } = await notifyMessage.setNotifyLanguage(languageCode);

  // let bookingData = await this.adapter.model
  //   .findOne({
  //     "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //   })
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();

  // Get booking Details
  let bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId?.toString(),
  }); // Get booking Details

  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();
  // if (bookingData.bookingStatus !== constantUtil.AWAITING) {
  //   return {
  //     "code": 200,
  //     "data": bookingObject(bookingData),
  //     "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
  //   };
  // }
  let query;
  // user deny types
  if (userType === constantUtil.USER && context.meta.userId) {
    // user contains two deny types
    if (
      context.params.denyType !== constantUtil.EXPIRED &&
      context.params.denyType !== constantUtil.USERCANCELLED
    ) {
      throw new MoleculerError(
        `IN USER ${context.params.denyType} IS NOT VALID`
      );
    }
    if (context.params.denyType === constantUtil.EXPIRED) {
      query = {
        "bookingStatus": constantUtil.EXPIRED,
        "activity.denyOrExpireTime": new Date(),
      };
    }
    if (context.params.denyType === constantUtil.USERCANCELLED) {
      query = {
        "bookingStatus": constantUtil.USERDENY,
        "activity.denyOrExpireTime": new Date(),
      };
    }

    // update bookingData
    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "bookingStatus": constantUtil.AWAITING,
      },
      query
    );
    // if (!updatedBookingData.nModified) {
    //   throw new MoleculerError(BOOKING_STATUS_INFO);
    // }
    if (updatedBookingData.nModified) {
      if (bookingData.guestType === constantUtil.USER && bookingData.user) {
        // if schedule remove schedule booking
        if (bookingData.bookingType === constantUtil.SCHEDULE) {
          this.broker.emit("user.removeScheduleBooking", {
            "clientId": clientId,
            "userId": bookingData.user._id.toString(),
            "bookingType": bookingData.bookingType,
            "bookingId": context.params.bookingId,
          });
        }
        // if user cancelled  need to reund it
        if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
          // needed keys
          const paymentInitId = bookingData?.paymentInitId?.toString();
          const paymentInitAmount = bookingData?.paymentInitAmount?.toString();
          const cardId = bookingData?.payment?.card?.cardId?.toString();
          const refBookingId = bookingData?.bookingId?.toString();
          const bookingId = bookingData?._id;

          await this.broker.emit("transaction.newBookingCancelRefund", {
            "clientId": clientId,
            "paymentTokenId": bookingData?.payment?.details?.id,
            "userId": bookingData?.user?._id?.toString(),
            "amount": 0,
            "currencyCode": bookingData.currencyCode,
            "paymentInitId": paymentInitId,
            "paymentInitAmount": paymentInitAmount,
            "cardId": cardId,
            "bookingId": bookingId,
            "refBookingId": refBookingId,
            "transactionType": constantUtil.CANCELCAPTURE,
          });
          // if (bookingData.bookingType === constantUtil.INSTANT) {
          //   await this.broker.emit('user.updateWalletAmountInBooking', {
          //     'userId': context.meta.userId,
          //     'amount': 0,
          //     'finalAmount': parseFloat(
          //       bookingData.user.wallet.availableAmount.toFixed(2)
          //     ),
          //   })
          //   let paymentData = await this.broker.emit(
          //     'transaction.newBookingCancelRefund',
          //     {
          //       'paymentTokenId': bookingData.payment.details.id,
          //       'userId': context.meta.userId,
          //       'amount': bookingData.invoice.estimationAmount.toFixed(2),
          //       'currencyCode': bookingData.currencyCode,
          //     }
          //   )
          //   paymentData = paymentData[0]
          //   if (
          //     !paymentData ||
          //     paymentData === undefined ||
          //     paymentData === 'undefined' ||
          //     paymentData.code === 0
          //   )
          //     throw new MoleculerError(
          //       'ERROR IN CARD PAYMENT, KINDLY TRY AFTER SOMETIME'
          //     )
          // }
        }
        if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
          let availableAmount = 0,
            scheduleFreezedAmount = 0;
          if (bookingData.bookingType === constantUtil.SCHEDULE) {
            availableAmount =
              bookingData.user.wallet.availableAmount +
              bookingData.invoice.estimationAmount;
            scheduleFreezedAmount =
              bookingData.user.wallet.scheduleFreezedAmount -
              bookingData.invoice.estimationAmount;
          } else {
            availableAmount = bookingData.user.wallet.availableAmount;
            scheduleFreezedAmount =
              bookingData.user.wallet.scheduleFreezedAmount;
          }
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount": availableAmount,
            "freezedAmount": bookingData.user.wallet.freezedAmount,
            "scheduleFreezedAmount": scheduleFreezedAmount,
          });
        }
        // // IF PAYMENT IS cash so we have take canel penality amount from wallet
        // if (bookingData.payment.option === constantUtil.PAYMENTCASH) {
        //   // only take money is payement amount is take more than 0
        //   if (penalityAmount) {
        //     this.broker.emit("user.updateWalletAmount", {
        //       "userId": bookingData.user._id.toString(),
        //       "availableAmount":
        //         bookingData.user.wallet.availableAmount - penalityAmount,
        //       "freezedAmount": bookingData.user.wallet.freezedAmount,
        //       "scheduleFreezedAmount":
        //         bookingData.user.wallet.scheduleFreezedAmount,
        //     });
        //   }
        // }
      }
      // Remove ongoing booking
      if (bookingData.bookingType === constantUtil.INSTANT) {
        await this.broker.emit("user.removeOngoingBooking", {
          "userId": bookingData.user?._id?.toString(),
          "bookingId": context.params.bookingId.toString(),
        });
      }
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_CANCELBOOKING,
          "timestamp": Date.now(),
          "message": BOOKING_CANCELLED_BY + " " + userType.toUpperCase(),
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": [
          {
            "id": bookingData?.professional?._id?.toString(),
            "token": bookingData?.professional?.deviceInfo[0]?.deviceId,
            "deviceType": bookingData?.professional?.deviceInfo[0]?.deviceType,
            "platform": bookingData?.professional?.deviceInfo[0]?.platform,
            "socketId": bookingData?.professional?.deviceInfo[0]?.socketId,
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
  }
  // professional Deny types
  if (userType === constantUtil.PROFESSIONAL && context.meta.professionalId) {
    if (
      context.params.denyType !== constantUtil.EXPIRED &&
      context.params.denyType !== constantUtil.PROFESSIONALCANCELLED
    ) {
      throw new MoleculerError(
        `IN professional ${context.params.denyType} IS NOT VALID`
      );
    }
    // in professional two deny type but this api carry professional cancelled
    if (bookingData.bookingType === constantUtil.SCHEDULE) {
      query = {
        "$push": {
          "denyDriverList": context.meta.professionalId,
          "denyProfessionals": {
            "professionalId": context.meta.professionalId,
            "requestDenyDateTime": new Date(),
          },
        },
        "$pull": { "wishList": context.meta.professionalId },
      };
    } else if (bookingData.bookingType === constantUtil.INSTANT) {
      query = {
        "$push": {
          "denyDriverList": context.meta.professionalId,
          "denyProfessionals": {
            "professionalId": context.meta.professionalId,
            "requestDenyDateTime": new Date(),
          },
        },
      };
    }
    // update bookingData
    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "bookingStatus": constantUtil.AWAITING,
      },
      { ...query }
    );
    // if (!updatedBookingData.nModified) {
    //   throw new MoleculerError(BOOKING_STATUS_INFO);
    // }
    if (updatedBookingData.nModified) {
      // SEND SILENET NOTIFICATION TO USER .PROFESSIONAL IS DENIED
      const generalSettings = await storageUtil.read(
        constantUtil.GENERALSETTING
      );

      if (
        (generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
          generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST) &&
        bookingData.user
      ) {
        bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
        bookingData["imageURLPath"] =
          generalSettings.data.spaces.spacesBaseUrl +
          "/" +
          generalSettings.data.spaces.spacesObjectName;
        const notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": "PROFESSIONALDENIED",
            "timestamp": Date.now(),
            "message": "",
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "id": bookingData?.user?._id.toString(),
              "token": bookingData?.user?.deviceInfo[0]?.deviceId,
              "deviceType": bookingData?.user?.deviceInfo[0]?.deviceType,
              "platform": bookingData?.user?.deviceInfo[0]?.platform,
              "socketId": bookingData?.user?.deviceInfo[0]?.socketId,
            },
          ],
        };
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }

      // UPDATE data
      // alogrithim works
      // suddenly send request to next professional
      // await this.broker.emit('booking.sendBookingRequestToProfessionals', {
      //   'bookingId': context.params.bookingId,
      //   'requestFor': 'professionalDeny',
      // })
      //Remove OngoingBooking
      await this.broker.emit("professional.updateOnGoingBooking", {
        "professionalId": context.meta.professionalId.toString(),
      });
    } else {
      await this.adapter.model.updateOne(
        { "_id": mongoose.Types.ObjectId(context.params.bookingId) },
        { ...query }
      );
      throw new MoleculerError(RIDE_CANCELLED);
    }
  }
  switch (bookingData.serviceCategory?.toLowerCase()) {
    case constantUtil.CONST_SHARERIDE.toLowerCase():
      {
        await this.broker.emit("professional.updateProfessionalShareRide", {
          "actionStatus":
            userType === constantUtil.PROFESSIONAL
              ? constantUtil.PROFESSIONALCANCELLED
              : constantUtil.USERCANCELLED,
          "bookingId": context.params.bookingId,
          "professionalId": context.meta.professionalId,
          // "pickUpLat": context.params.pickUpLat,
          // "pickUpLng": context.params.pickUpLng,
          // "dropLat": context.params.dropLat,
          // "dropLng": context.params.dropLng,
        });
        await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
          "actionStatus": constantUtil.CONST_DROP,
          "bookingId": null,
          "professionalId": context.meta.professionalId,
          "serviceCategory": bookingData.serviceCategory,
          "lastRidePassengerCount": 0, // bookingData.passengerCount,
          "lat": context.params?.latLng?.lat,
          "lng": context.params?.latLng?.lng,
          "parentShareRideId": null, // bookingData?.parentShareRideId,
          // "passengerCount": bookingData?.passengerCount || 0,
        });
        // await this.adapter.model.updateOne(
        //   {
        //     "_id": mongoose.Types.ObjectId(
        //       bookingData?.parentShareRideId?.toString()
        //     ),
        //   },
        //   {
        //     "$inc": {
        //       "currentPassengerCount": -parseInt(
        //         bookingData?.passengerCount || 0
        //       ),
        //     },
        //   }
        // );
      }
      break;
  }
  /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
  // if (generalSettings.data.isEnableWebAppBooking) {
  this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
    "bookingId": bookingData._id,
    "bookingStatus": bookingData.bookingStatus,
    "dateTime": bookingData.updatedAt,
    "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
    "bookingInfo": bookingData?.user?.bookingInfo || {},
  });
  // }

  // bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "trustedContacts": 0,
  //     "cards": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "cards": 0,
  //     "bankDetails": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();

  // Get booking Details
  bookingData = await this.getBookingDetailsById({
    "bookingId": bookingData?._id?.toString(),
  }); // Get booking Details

  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
  };
};

// GET CANCELLATION REASONS FOR USER AND PROFESSIONAL
bookingAction.getCancellationReason = async function (context) {
  const userType = context.params?.userType?.toLowerCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { CANCELLATION_REASON_NOT_FOUND } =
    await notifyMessage.setNotifyLanguage(languageCode);
  const cancellationReason = await storageUtil.read(
    constantUtil.CANCELLATIONREASON
  );
  if (!cancellationReason) {
    throw new MoleculerError(CANCELLATION_REASON_NOT_FOUND);
  }
  // console.log(cancellationReason);
  const response = [];
  for (const prop in cancellationReason) {
    if (
      cancellationReason[prop].status === constantUtil.ACTIVE &&
      cancellationReason[prop]?.reasonFor?.toLowerCase() ===
        context.params?.userType?.toLowerCase()
    ) {
      response.push(cancellationReason[prop]);
    }
  }
  return {
    "code": 200,
    "data": response || [],
    "message": "",
  };
};

// CANCEL BOOKING FOR USER AND PROFESSIONAL
bookingAction.cancelBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context.params.userType.toUpperCase();
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  if (userType === constantUtil.USER && !!context.meta?.userId === false) {
    throw new MoleculerError(
      "CANCELLED BY USER BUT ID IS PROFESSIOAL PLEASE CHECK"
    );
  }
  if (
    userType === constantUtil.PROFESSIONAL &&
    !!context.meta.professionalId === false
  ) {
    throw new MoleculerError(
      "CANCELLED BY PROFESSIONAL BUT ID IS USER PLEASE CHECK"
    );
  }
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const {
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_CATEGORY_NOT_FOUND,
    SOMETHING_WENT_WRONG,
    RIDE_CANCELLED,
    RIDE_CANCELLATION_CHARGE,
    BOOKING_CANCELLED_BY,
    INFO_FELLOW_PASSENGER,
    RIDE_IS,
    BOOKING_CANCELLED,
    PROFESSIONAL_NEW_RIDE,
    BOOKING_CANCELLED_BY_PROFESSIONAL,
    PROFESSIONAL_NOTIFICATION,
    PROFESSIONAL_RIDE_CANCEL,
    USER_RIDE_CANCEL_FAILD,
    BOOKING_UPDATE_FAILED,
  } = await notifyMessage.setNotifyLanguage(languageCode);

  // let bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();
  // if (!bookingData)
  //   throw new MoleculerError("REQUESTED CANCELLATION RIDE NOT FOUND");

  // bookingData = bookingData.toJSON();
  //---------------------------------
  // Get booking Details
  let bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId.toString(),
  }); // Get booking Details
  if (!bookingData) {
    throw new MoleculerError("REQUESTED CANCELLATION RIDE NOT FOUND");
  }
  // GET REQUESTER DATA
  // const stringTransactionReason =
  //   'Trip Canceled/Trip Number : ' + bookingData?.bookingId
  const paymentInitId = bookingData?.paymentInitId?.toString();
  const paymentInitAmount = bookingData?.paymentInitAmount?.toString();
  const cardId = bookingData?.payment?.card?.cardId?.toString();
  const refBookingId = bookingData?.bookingId?.toString();
  const bookingId = bookingData?._id;
  const transactionId = `${helperUtil.randomString(8, "a")}`;
  const stringTransactionReasonCredit = `${"Trip Cancellation Credit"}/${
    "Trip Number : " + refBookingId
  }`;

  // vehicle category from redis
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId?.toString()];
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  //  service Category
  if (!bookingData.category._id) {
    throw new MoleculerError(BOOKING_CATEGORY_NOT_FOUND);
  }
  vehicleCategoryData = {
    ...vehicleCategoryData,
    ...bookingData.category.categoryData.vehicles.find(
      (vehicle) =>
        vehicle.categoryId?.toString() ===
        bookingData.vehicle.vehicleCategoryId?.toString()
    ),
  };
  // data initialization
  if (!generalSettings) {
    throw new MoleculerError(SOMETHING_WENT_WRONG);
  }
  if (
    (bookingData.bookingStatus === constantUtil.AWAITING &&
      bookingData.bookingType === constantUtil.SCHEDULE) || // ((
    (bookingData.bookingStatus === constantUtil.AWAITING &&
      bookingData.professional === null) ||
    bookingData.bookingStatus === constantUtil.ACCEPTED ||
    bookingData.bookingStatus === constantUtil.ARRIVED
    //) && bookingData.bookingStatus === context.params.bookingStatus) // Need To Work Based on Condition --> context.params.bookingStatus
  ) {
    // context changeing
    context.params.userType = context.params.userType?.toUpperCase();
    const cancellationReason = context.params?.cancellationReason || "";
    const cancellationReasonId = context.params?.reasonId || null;
    const cancelReasonDescription =
      context.params?.cancelReasonDescription || "";
    const isCancelToNextProfessional =
      context.params?.isCancelToNextProfessional || false;
    const isHasPenalty =
      context.params?.isHasPenalty ||
      context.params?.selectedHasPenalty ||
      false; // Need to Remove selectedHasPenalty Key After Mar 2025
    // const isUserRideCancellationPenalty =
    //   context.params?.isUserRideCancellationPenalty || false;
    // const isProfessionalRideCancellationPenalty =
    //   context.params?.isProfessionalRideCancellationPenalty || false;
    // two types of cancelation professional and user
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      let cancellationAmountCreditToUser = 0;
      // needd for many process check carefully for penality
      const cancellationMinute = helperUtil.getMinutesDiff(
        new Date(),
        (bookingData.activity.acceptTime == null
          ? ""
          : bookingData.activity.acceptTime
        ).toString() // professional accept time
      );
      const penalityStatus =
        cancellationMinute >=
        vehicleCategoryData.professionalCancellation.thresholdMinute;

      const penalityAmount =
        vehicleCategoryData.professionalCancellation.status === true && // only amount is applicable when this condition is satifies many buffer function is used so this process very useful check entirely the amount is a paras of many events so this a comfortable solution for this
        penalityStatus === true &&
        isHasPenalty === true
          ? vehicleCategoryData.professionalCancellation.amount
          : 0;
      const nextProfessioanlStatus =
        bookingData.cancelledProfessionalList.length > 0 ? true : false; // if professional alredy cancelled then another professional is calling this ride is isCancelToNearbyProfessionals ride
      //
      // first take amount if canceled time exceeded beyond buffer time from accepted time
      // if user type is professional so which booking is accepted so does not need condition

      // calcualate if cancel time is exceed buffer time
      if (vehicleCategoryData.professionalCancellation.status === true) {
        // below condition check accepted time and cancel time difference is exceed cancelation taking bufferTime
        if (penalityStatus && penalityAmount > 0) {
          // some times penality amount is 0 so we dont need to make transation
          // take amount from prfessional
          this.broker.emit("professional.updateWalletAmount", {
            "professionalId": context.meta.professionalId.toString(),
            "availableAmount":
              bookingData.professional.wallet.availableAmount - penalityAmount, //professional cancellation amount detection
            "freezedAmount": bookingData.professional.wallet.freezedAmount,
          });
          //create transaction
          this.broker.emit("transaction.profesionalCancelCharge", {
            "clientId": bookingData?.clientId?.toString(),
            "id": context.meta.professionalId, //professional id
            "bookingId": bookingData._id.toString(), //bookingId
            "amount": penalityAmount,
            "refBookingId": bookingData.bookingId,
            "previousBalance": bookingData.professional.wallet.availableAmount,
            "currentBalance":
              bookingData.professional.wallet.availableAmount - penalityAmount,
          });
          //#region  Cancel Amount Credit to Professional Start
          if (penalityAmount > 0 && bookingData?.user?._id) {
            cancellationAmountCreditToUser =
              penalityAmount *
              ((bookingData.invoice?.cancellationFeeCreditToUserPrecentage ||
                0) /
                100);
            if (cancellationAmountCreditToUser > 0) {
              // Credit amount to User
              this.broker.emit("user.updateWalletAmount", {
                "userId": bookingData.user._id.toString(),
                "availableAmount":
                  bookingData.user.wallet.availableAmount +
                  cancellationAmountCreditToUser,
                "freezedAmount": bookingData.user.wallet.freezedAmount,
                "scheduleFreezedAmount":
                  bookingData.user.wallet.scheduleFreezedAmount,
              });
              //create transaction
              this.broker.emit("transaction.insertTransactionBasedOnUserType", {
                "transactionType": constantUtil.CANCELLATIONCREDIT,
                "transactionAmount": cancellationAmountCreditToUser.toFixed(2),
                "previousBalance": bookingData.user.wallet.availableAmount,
                "currentBalance":
                  bookingData.user.wallet.availableAmount +
                  cancellationAmountCreditToUser,
                "transactionReason": stringTransactionReasonCredit,
                "refBookingId": refBookingId,
                "transactionStatus": constantUtil.SUCCESS,
                "transactionId": transactionId,
                "paymentType": constantUtil.CREDIT,
                "paymentGateWay": constantUtil.NONE,
                "gatewayResponse": {},
                "userType": constantUtil.ADMIN, // Debit From
                "userId": null, // Debit From
                "professionalId": bookingData.professional._id.toString(), // Debit From
                "paymentToUserType": constantUtil.USER, // Credit To
                "paymentToUserId": bookingData.user._id.toString(), // Credit To
              });
            }
          }
          //#endregion  Cancel Amount Credit to Professional Start
        }
      }
      // this is condition check this ride can assign next professionl when in generalsettings key is on
      let professionalsData = []; // =>this is used for when isCancelToNextProfessional key false so professional also 0 length use same code => check clerly and it will be got
      if (
        bookingData.serviceCategory?.toLowerCase() !==
          constantUtil.CONST_SHARERIDE.toLowerCase() &&
        generalSettings.data.isCancelToNextProfessional === true &&
        isCancelToNextProfessional === true &&
        bookingData.bookingBy !== constantUtil.PROFESSIONAL
      ) {
        professionalsData = await this.getProfessionalByLocation(
          //clientId,
          bookingData.origin.lat,
          bookingData.origin.lng,
          bookingData.destination.lat,
          bookingData.destination.lng,
          bookingData.vehicle.vehicleCategoryId.toString(),
          constantUtil.BOOKING, // actionType
          context.params.rideType,
          context.params?.radius || generalSettings.data.requestDistance,
          // context.params?.maxRadius || generalSettings.data.requestDistance,
          false,
          true,
          generalSettings.data.isTailRideNeeded,
          vehicleCategoryData.isSubCategoryAvailable,
          vehicleCategoryData.isForceAppliedToProfessional,
          bookingData.isGenderAvailable,
          bookingData.childseatAvailable,
          bookingData.handicapAvailable,
          !!(
            bookingData?.user?.gender === constantUtil.MALE &&
            bookingData?.category?.isRestrictMaleBookingToFemaleProfessional
          ),
          bookingData.tripType === constantUtil.RENTAL, //isRentalSupported
          context.params?.serviceAreaId,
          bookingData.isShareRide,
          bookingData?.payment.option || constantUtil.CASH,
          bookingData?.languageCode || null,
          bookingData?.isPetAllowed || false
        );
        professionalsData = professionalsData && professionalsData[0];

        // filtered professional data whithout deny professionals data
        if (professionalsData[0]) {
          professionalsData = professionalsData.filter(
            (professional) =>
              !bookingData.denyDriverList.includes(professional._id.toString())
          );
        }
        switch (bookingData.serviceCategory?.toLowerCase()) {
          case constantUtil.CONST_SHARERIDE.toLowerCase():
            {
              // Find Current Share Ride Professional
              const professionalDataNewList =
                await this.findCurrentShareRideProfessional(
                  constantUtil.NEXT, // Action
                  professionalsData,
                  context.meta.professionalId // --> currentShareRideProfessionalId
                );
              if (professionalDataNewList?.length === 0) {
                professionalsData = await this.findCurrentShareRideProfessional(
                  constantUtil.REMOVE, // Action
                  professionalsData,
                  context.meta.professionalId // --> currentShareRideProfessionalId
                );
              } else {
                professionalsData = professionalDataNewList;
              }
            }
            break;
        }
        if (professionalsData[0]) {
          const updatedBookingData = await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(context.params.bookingId),
            },
            {
              // "professional": null,
              "bookingStatus": constantUtil.AWAITING,
              "bookingDate": new Date(),
              "isCancelToNearbyProfessionals": true,
              "isadminForceAssign": false,
              "$push": {
                "denyDriverList": context.meta.professionalId,
                // "denyProfessionals": {
                //   "professionalId": context.meta.professionalId,
                //   "requestDenyDateTime": new Date(),
                // },
                "cancelledProfessionalList": {
                  "professionalId": mongoose.Types.ObjectId(
                    context.meta.professionalId
                  ),
                  "cancelledTime": new Date(),
                  "cancellationReasonId": cancellationReasonId,
                  "cancellationReason": cancellationReason,
                  "cancellationMinute": cancellationMinute,
                  "cancellationPenalityStatus": penalityStatus,
                  "cancellationAmount": penalityAmount,
                  "isHasPenalty": isHasPenalty,
                },
              },
            }
          );

          if (!updatedBookingData.nModified) {
            throw new MoleculerError(SOMETHING_WENT_WRONG);
          }
          // professional can only cancel ongoing  booking
          // remove professional ongoing list
          await this.broker.emit("professional.updateOnGoingAndTailBooking", {
            // this is emiter need to change  in working
            "professionalId": context.meta.professionalId.toString(),
            "bookingId": context.params.bookingId.toString(),
          });
          // data adding for bookingObject
          // Get booking Details
          bookingData = await this.getBookingDetailsById({
            "bookingId": context.params.bookingId.toString(),
          }); // Get booking Details
          bookingData.vehicle.vehicleCategoryName =
            vehicleCategoryData.vehicleCategory;
          bookingData.vehicle.vehicleCategoryImage =
            vehicleCategoryData.categoryImage;
          bookingData.vehicle.vehicleCategoryMapImage =
            vehicleCategoryData.categoryMapImage;
          bookingData.retryTime = generalSettings.data.driverRequestTimeout;
          bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
          // bookingData.distanceUnit = bookingData.category.distanceType;
          bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
          bookingData["imageURLPath"] =
            generalSettings.data.spaces.spacesBaseUrl +
            "/" +
            generalSettings.data.spaces.spacesObjectName;

          let notificationObject = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.PROFESSIONAL,
              "action": constantUtil.ACTION_BOOKINGREQUEST,
              "timestamp": Date.now(),
              "message": PROFESSIONAL_NEW_RIDE, //"YOU HAVE NEW RIDE",
              "details": lzStringEncode(bookingObject(bookingData)),
            },
            "registrationTokens": await professionalsData.map(
              (professional) => {
                if (
                  // parseFloat(professional.wallet.availableAmount) <
                  // parseFloat(
                  //   generalSettings.data?.minimumWalletAmountToOnline
                  // ) &&
                  bookingData.payment.option === constantUtil.WALLET ||
                  bookingData.payment.option === constantUtil.PAYMENTCARD ||
                  // bookingData.payment.option !== constantUtil.CASH &&
                  // bookingData.payment.option !== constantUtil.CREDIT) ||
                  parseFloat(professional.wallet.availableAmount) >=
                    parseFloat(
                      generalSettings.data?.minimumWalletAmountToOnline
                    )
                ) {
                  return {
                    "id": professional?._id?.toString(),
                    "token": professional?.deviceInfo[0]?.deviceId || "",
                    "deviceType": professional?.deviceInfo[0]?.deviceType || "",
                    "platform": professional?.deviceInfo[0]?.platform || "",
                    "socketId": professional?.deviceInfo[0]?.socketId || "",
                    "duration": professional?.duration,
                    "distance": professional?.distance,
                  };
                }
              }
            ),
          };
          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          // this.broker.emit('socket.statusChangeEvent', notificationObject)
          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
          //
          // // create notification user Object
          // notificationObject = {
          //   "clientId":  clientId,
          //   "data": {
          //     "type": constantUtil.NOTIFICATIONTYPE,
          //     "usertype": constantUtil.USER,
          //     "action":
          //       constantUtil.ACTION_BOOKINGCANCELCHECKINGFORANOTHERDRIVER,
          //     "timestamp": Date.now(),
          //     "message": BOOKING_CANCELLED_BY_PROFESSIONAL,
          //     "details": lzStringEncode(bookingObject(bookingData)),
          //   },
          //   "registrationTokens": [
          //     {
          //       "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
          //       "id": bookingData.user._id.toString(),
          //       "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
          //       "platform": bookingData.user?.deviceInfo[0]?.platform || "",
          //       "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
          //     },
          //   ],
          // };
          // // socket push notificaton

          // this.broker.emit("socket.sendNotification", notificationObject);
          // /* FCM */
          // this.broker.emit("admin.sendFCM", notificationObject);
          if (
            bookingData.guestType === constantUtil.USER &&
            bookingData.user === null //bookingData.user !== null
          ) {
            // timing for if next professional never accept
            this.broker.emit("booking.nextProfessionalBuffer", {
              "professionalId": context.meta.professionalId,
              "bookingId": bookingData._id.toString(),
              "cancelledTime": new Date(),
              "cancellationReason": cancellationReason,
              "cancellationAmount": penalityAmount,
              "cancellationPenalityStatus": true,
              "bookingDate": bookingData.bookingDate,
            });
          } else {
            await this.adapter.model.updateOne(
              { "_id": mongoose.Types.ObjectId(context.params.bookingId) },
              { "professional": null }
            );
            // bookingData.bookingStatus = constantUtil.AWAITINGNEXTPROFESSIONAL;
          }
          /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
          // if (generalSettings.data.isEnableWebAppBooking) {
          this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
            "bookingId": bookingData._id,
            "bookingStatus": bookingData.bookingStatus,
            "dateTime": bookingData.updatedAt,
            "userAccessToken":
              bookingData.user?.deviceInfo[0]?.accessToken || "",
            "bookingInfo": bookingData?.user?.bookingInfo || {},
          });
          // }
          // professionals is found so this api returns
          return {
            "code": 200,
            "data": bookingObject(bookingData),
            "message": PROFESSIONAL_NOTIFICATION,
          };
        }
      }
      if (
        bookingData.serviceCategory?.toLowerCase() ===
          constantUtil.CONST_SHARERIDE.toLowerCase() ||
        generalSettings.data.isCancelToNextProfessional === false ||
        isCancelToNextProfessional === false ||
        !professionalsData[0] // this condition check if nearby porfessionals does not exist
      ) {
        const bookingPayableAmount = parseFloat(
          bookingData?.invoice?.payableAmount || 0
        );
        // differed TwoTypes instant and schedule
        if (bookingData.bookingType === constantUtil.INSTANT) {
          // update ride data
          const updatedBookingData = await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(context.params.bookingId),
            },
            {
              "professional": mongoose.Types.ObjectId(
                context.meta.professionalId.toString()
              ),
              "cancellationReasonId": cancellationReasonId,
              "cancellationReason": cancellationReason,
              "isHasPenalty": isHasPenalty,
              "bookingStatus": constantUtil.PROFESSIONALCANCELLED,
              "isCancelToNearbyProfessionals": nextProfessioanlStatus,

              "$push": {
                "denyDriverList": context.meta.professionalId,
                // "denyProfessionals": {
                //   "professionalId": context.meta.professionalId,
                //   "requestDenyDateTime": new Date(),
                // },
                "cancelledProfessionalList": {
                  "professionalId": mongoose.Types.ObjectId(
                    context.meta.professionalId
                  ),
                  "cancelledTime": new Date(),
                  "cancellationReasonId": cancellationReasonId,
                  "cancellationReason": cancellationReason,
                  "cancellationPenality": penalityStatus,
                  "cancellationMinute": cancellationMinute,
                  "cancellationAmount": penalityAmount,
                  "isHasPenalty": isHasPenalty,
                },
              },
              "invoice.professionalCancellationStatus": penalityStatus,
              "invoice.professionalCancellationMinute": cancellationMinute,
              "invoice.professionalCancellationAmount": penalityAmount,
              "invoice.payableAmount": 0,
              "invoice.siteCommission":
                penalityAmount - cancellationAmountCreditToUser,
              "activity.cancelTime": new Date(),
              "regionalData.cancelTime": helperUtil.toRegionalUTC(new Date()),
            }
          );
          if (!updatedBookingData.nModified) {
            throw new MoleculerError(SOMETHING_WENT_WRONG);
          }
          // update cancelled professional Ongoing ride
          this.broker.emit("professional.updateOnGoingBooking", {
            "professionalId": context.meta.professionalId.toString(),
          });
          switch (bookingData.serviceCategory?.toLowerCase()) {
            case constantUtil.CONST_SHARERIDE.toLowerCase():
              {
                await this.broker.emit(
                  "professional.updateProfessionalShareRide",
                  {
                    "actionStatus": constantUtil.PROFESSIONALCANCELLED,
                    "bookingId": context.params.bookingId,
                    "professionalId": context.meta.professionalId,
                    // "pickUpLat": context.params.pickUpLat,
                    // "pickUpLng": context.params.pickUpLng,
                    // "dropLat": context.params.dropLat,
                    // "dropLng": context.params.dropLng,
                  }
                );
                await this.broker.emit(
                  "booking.checkAndUpdateOngoingShareRide",
                  {
                    "actionStatus": constantUtil.CONST_DROP,
                    "bookingId": null,
                    "professionalId": context.meta.professionalId,
                    "serviceCategory": bookingData.serviceCategory,
                    "lastRidePassengerCount": bookingData?.passengerCount || 0,
                    "lat": context.params?.latLng?.lat,
                    "lng": context.params?.latLng?.lng,
                    "parentShareRideId": bookingData?.parentShareRideId,
                    // "passengerCount": bookingData?.passengerCount || 0,
                  }
                );
                // await this.adapter.model.updateOne(
                //   {
                //     "_id": mongoose.Types.ObjectId(
                //       bookingData?.parentShareRideId?.toString()
                //     ),
                //   },
                //   {
                //     "$inc": {
                //       "currentPassengerCount": -parseInt(
                //         bookingData?.passengerCount || 0
                //       ),
                //     },
                //   }
                // );
              }
              break;
          }
          if (
            bookingData?.user?.bookingInfo?.ongoingBooking?.toString() ===
            context.params.bookingId.toString()
          ) {
            // udate ongoing booking
            this.broker.emit("user.removeOngoingBooking", {
              "userId": bookingData.user._id.toString(),
              "bookingId": context.params.bookingId.toString(),
            });
          }
          // refund money
          if (bookingData.guestType === constantUtil.USER && bookingData.user) {
            // in instant ride ,ride must be inside the ongoing ride
            if (
              bookingData.user.bookingInfo.ongoingBooking?.toString() ===
              context.params.bookingId.toString()
            ) {
              // // udate ongoing booking
              // this.broker.emit("user.removeOngoingBooking", {
              //   "userId": bookingData.user._id.toString(),
              //   "bookingId": context.params.bookingId.toString(),
              // });

              // refund
              if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
                await this.broker.emit("transaction.newBookingCancelRefund", {
                  "paymentTokenId": bookingData?.payment?.details?.id,
                  "userId": bookingData?.user?._id?.toString(),
                  "amount": 0,
                  "currencyCode": bookingData.currencyCode,
                  "paymentInitId": paymentInitId,
                  "paymentInitAmount": paymentInitAmount,
                  "cardId": cardId,
                  "bookingId": bookingId,
                  "refBookingId": refBookingId,
                  "transactionType": constantUtil.CANCELCAPTURE,
                });
              }
              //#region Get Penality From User
              const userCancelpenalityAmount = parseFloat(
                vehicleCategoryData.cancellation.amount || 0
              ); // Need to Change Condition Based On Admin Panel Configuration
              if (
                bookingData.bookingStatus === constantUtil.ARRIVED &&
                userCancelpenalityAmount > 0
              ) {
                this.broker.emit("user.updateWalletAmount", {
                  "userId": bookingData.user._id.toString(),
                  "availableAmount":
                    parseFloat(bookingData.user.wallet.availableAmount) +
                    parseFloat(bookingData.user.wallet.freezedAmount) -
                    userCancelpenalityAmount,
                  "freezedAmount": 0,
                  "scheduleFreezedAmount":
                    bookingData.user.wallet.scheduleFreezedAmount,
                });
                //
                this.broker.emit("transaction.cancelCharge", {
                  "clientId": bookingData?.clientId?.toString(),
                  "userId": bookingData.user._id.toString(),
                  "bookingId": bookingData._id.toString(),
                  "amount": userCancelpenalityAmount,
                  "previousBalance":
                    parseFloat(bookingData.user.wallet.availableAmount) +
                    parseFloat(bookingData.user.wallet.freezedAmount),
                  "currentBalance":
                    parseFloat(bookingData.user.wallet.availableAmount) +
                    parseFloat(bookingData.user.wallet.freezedAmount) -
                    userCancelpenalityAmount,
                  "refBookingId": bookingData.bookingId,
                });
                //
                cancellationAmountCreditToProfessional =
                  userCancelpenalityAmount *
                  ((bookingData.invoice
                    ?.cancellationFeeCreditToProfessionalPrecentage || 0) /
                    100);
                // #region Cancellation Amount Credit To Professional
                if (cancellationAmountCreditToProfessional > 0) {
                  this.broker.emit("professional.updateWalletAmount", {
                    "professionalId": bookingData.professional?._id?.toString(),
                    "availableAmount":
                      bookingData?.professional?.wallet?.availableAmount +
                      cancellationAmountCreditToProfessional, //From User cancellation amount detection
                    "freezedAmount":
                      bookingData.professional.wallet.freezedAmount,
                  });
                  //create transaction
                  this.broker.emit(
                    "transaction.insertTransactionBasedOnUserType",
                    {
                      "transactionType": constantUtil.CANCELLATIONCREDIT,
                      "transactionAmount":
                        cancellationAmountCreditToProfessional.toFixed(2),
                      "previousBalance":
                        bookingData?.professional?.wallet?.availableAmount,
                      "currentBalance":
                        bookingData?.professional?.wallet?.availableAmount +
                        cancellationAmountCreditToProfessional,
                      "transactionReason": stringTransactionReasonCredit,
                      "refBookingId": refBookingId,
                      "transactionStatus": constantUtil.SUCCESS,
                      "transactionId": transactionId,
                      "paymentType": constantUtil.CREDIT,
                      "paymentGateWay": constantUtil.NONE,
                      "gatewayResponse": {},
                      "userType": constantUtil.ADMIN, // Debit From
                      "userId": null, //bookingData.user._id.toString(), // Debit From
                      "professionalId": null, // Debit From
                      "paymentToUserType": constantUtil.PROFESSIONAL, // Credit To
                      "paymentToUserId":
                        bookingData.professional._id.toString(), // Credit To
                    }
                  );
                }
                // #endregion Cancellation Amount Credit To Professional
              } else {
                if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
                  this.broker.emit("user.updateWalletAmount", {
                    "userId": bookingData.user._id.toString(),
                    "availableAmount":
                      parseFloat(bookingData.user.wallet.availableAmount) +
                      parseFloat(bookingData.invoice.estimationAmount),
                    "freezedAmount":
                      parseFloat(bookingData.user.wallet.freezedAmount) -
                      parseFloat(bookingData.invoice.estimationAmount),
                    "scheduleFreezedAmount":
                      bookingData.user.wallet.scheduleFreezedAmount,
                  });
                }
              }
              //#endregion Get Penality From User
            }
            //-------- Send SMS Alert Start --------------------------
            // if (
            //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
            //   bookingData.bookingBy === constantUtil.ADMIN ||
            //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
            //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
            // )
            {
              this.broker.emit("booking.sendSMSAlertByRideId", {
                "rideId": context.params.bookingId.toString(),
                "bookingFrom": bookingData.bookingFrom,
                "notifyType": constantUtil.PROFESSIONALCANCELLED,
                "notifyFor": constantUtil.USER,
              });
            }
            //-------- Send SMS Alert End --------------------------
          }
          // // esstional data intialattion after updation
          // bookingData = await this.adapter.model
          //   .findById(
          //     mongoose.Types.ObjectId(context.params.bookingId.toString())
          //   )
          //   .populate("user", {
          //     "password": 0,
          //     "bankDetails": 0,
          //     "cards": 0,
          //     "trustedContacts": 0,
          //   })
          //   .populate("professional", {
          //     "password": 0,
          //     "bankDetails": 0,
          //     "cards": 0,
          //     "trustedContacts": 0,
          //   })
          //   .populate("category")
          //   .populate("security")
          //   .populate("officer", { "password": 0, "preferences": 0 })
          //   .lean();
          // bookingData = bookingData.toJSON();
          //Get Booking Details
          bookingData = await this.getBookingDetailsById({
            "bookingId": context.params.bookingId.toString(),
          }); // Get booking Details

          // some another mapping data for booking object
          bookingData.vehicle.vehicleCategoryName =
            vehicleCategoryData.vehicleCategory;
          bookingData.vehicle.vehicleCategoryImage =
            vehicleCategoryData.categoryImage;
          bookingData.vehicle.vehicleCategoryMapImage =
            vehicleCategoryData.categoryMapImage;
          bookingData.retryTime = generalSettings.data.driverRequestTimeout;
          bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
          // bookingData.distanceUnit = bookingData.category.distanceType;
          bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
          bookingData["imageURLPath"] =
            generalSettings.data.spaces.spacesBaseUrl +
            "/" +
            generalSettings.data.spaces.spacesObjectName;

          // send notification
          if (bookingData?.user?._id) {
            const notificationObject = {
              "clientId": clientId,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.USER,
                "action": constantUtil.ACTION_CANCELBOOKING,
                "timestamp": Date.now(),
                "message": PROFESSIONAL_RIDE_CANCEL,
                "details": lzStringEncode(bookingObject(bookingData)),
              },
              "registrationTokens": [
                {
                  "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
                  "id": bookingData.user?._id?.toString(),
                  "deviceType":
                    bookingData.user?.deviceInfo[0]?.deviceType || "",
                  "platform": bookingData.user?.deviceInfo[0]?.platform || "",
                  "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
                },
              ],
            };

            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
            /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
            // if (generalSettings.data.isEnableWebAppBooking) {
            this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
              "bookingId": bookingData._id,
              "bookingStatus": bookingData.bookingStatus,
              "dateTime": bookingData.updatedAt,
              "userAccessToken":
                bookingData.user?.deviceInfo[0]?.accessToken || "",
              "bookingInfo": bookingData?.user?.bookingInfo || {},
            });
            // }
          }
          switch (bookingData.serviceCategory?.toLowerCase()) {
            case constantUtil.CONST_SHARERIDE.toLowerCase():
              {
                this.broker.emit(
                  "booking.sendNotificationActionShareRideStatus",
                  {
                    "bookingId": context.params.bookingId.toString(),
                    "parentShareRideId":
                      bookingData.parentShareRideId?.toString(),
                    "userType": constantUtil.USER,
                    "actionStatus": constantUtil.ACTION_CANCELBOOKING,
                    "notificationMessage":
                      BOOKING_CANCELLED_BY + " " + INFO_FELLOW_PASSENGER,
                  }
                );
              }
              break;
          }
          // return {
          //   "code": 200,
          //   "data": bookingObject(bookingData),
          //   "message": RIDE_CANCELLED,
          // };
        }
        if (bookingData.bookingType === constantUtil.SCHEDULE) {
          const professionalData = bookingData.professional;
          // update Booking data
          bookingData = await this.adapter.model
            .findOneAndUpdate(
              {
                "_id": mongoose.Types.ObjectId(context.params.bookingId),
              },
              {
                "professional": mongoose.Types.ObjectId(
                  context.meta.professionalId.toString()
                ),

                // "vehicle": {
                //   "vehicleCategoryId":
                //     bookingData.vehicle.vehicleCategoryId.toString(),
                // },
                // "bookingStatus": constantUtil.AWAITING,
                // "bookingDate": new Date(),
                "bookingStatus": constantUtil.PROFESSIONALCANCELLED,
                "cancellationReasonId": cancellationReasonId,
                "cancellationReason": cancellationReason,
                "isHasPenalty": isHasPenalty,

                "isCancelToNearbyProfessionals": false,
                "isadminForceAssign": false,
                "$push": {
                  "denyDriverList": context.meta.professionalId,
                  // "denyProfessionals": {
                  //   "professionalId": context.meta.professionalId,
                  //   "requestDenyDateTime": new Date(),
                  // },
                  "cancelledProfessionalList": {
                    "professionalId": mongoose.Types.ObjectId(
                      context.meta.professionalId
                    ),
                    "cancellationMinute": cancellationMinute,
                    "cancelledTime": new Date(),
                    "cancellationReasonId": cancellationReasonId,
                    "cancellationReason": cancellationReason,
                    "cancellationPenality": penalityStatus,
                    "cancellationAmount": penalityAmount,
                    "isHasPenalty": isHasPenalty,
                  },
                },
                "invoice.professionalCancellationStatus": penalityStatus,
                "invoice.professionalCancellationMinute": cancellationMinute,
                "invoice.professionalCancellationAmount": penalityAmount,
                "invoice.payableAmount": 0,
                "invoice.siteCommission":
                  penalityAmount - cancellationAmountCreditToUser,
                "activity.cancelTime": new Date(),
                "regionalData.cancelTime": helperUtil.toRegionalUTC(new Date()),
              },
              { "new": true }
            )
            .populate("user", {
              "password": 0,
              "bankDetailes": 0,
              "trustedContacts": 0,
              "cards": 0,
            })
            .populate("professional", {
              "password": 0,
              "cards": 0,
              "bankDetails": 0,
              "trustedContacts": 0,
            })
            .populate("category")
            .populate("security")
            .populate("officer", { "password": 0, "preferences": 0 })
            .lean();
          if (!bookingData) {
            throw new MoleculerError(BOOKING_UPDATE_FAILED);
          }
          // update cancelled professional Ongoing ride
          this.broker.emit("professional.updateOnGoingBooking", {
            "professionalId": context.meta.professionalId.toString(),
          });
          // update ongoing of user (remove Onging Schedule ride)
          this.broker.emit("user.updateOnGoingBooking", {
            "userId": bookingData?.user?._id?.toString(),
            "bookingType": bookingData.bookingType,
            "bookingId": null,
          });
          //Get Booking Details
          bookingData = await this.getBookingDetailsById({
            "bookingId": context.params.bookingId.toString(),
          }); // Get booking Details
          if (bookingData.user?._id) {
            const notificationObject = {
              "clientId": clientId,
              "data": {
                "type": constantUtil.NOTIFICATIONTYPE,
                "userType": constantUtil.USER,
                "action": constantUtil.ACTION_CANCELBOOKING,
                "timestamp": Date.now(),
                "message": PROFESSIONAL_RIDE_CANCEL,
                "details": lzStringEncode(bookingObject(bookingData)),
              },
              "registrationTokens": [
                {
                  "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
                  "id": bookingData.user?._id?.toString(),
                  "deviceType":
                    bookingData.user?.deviceInfo[0]?.deviceType || "",
                  "platform": bookingData.user?.deviceInfo[0]?.platform || "",
                  "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
                },
              ],
            };
            /* SOCKET PUSH NOTIFICATION */
            this.broker.emit("socket.sendNotification", notificationObject);
            /* FCM */
            this.broker.emit("admin.sendFCM", notificationObject);
          }
          // notify to admin
          const notificationObjectAdmin = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.USER,
              "action": constantUtil.ACTION_CANCELBOOKING,
              "timestamp": Date.now(),
              "message": PROFESSIONAL_RIDE_CANCEL,
              "details": bookingObject(bookingData),
            },
            "registrationTokens": [
              {
                "token": professionalData?.deviceInfo[0]?.deviceId || "",
                "id": professionalData?._id?.toString() || "",
                "deviceType": "",
                "platform": "",
                "socketId": "",
              },
            ],
          };
          this.broker.emit(
            "socket.sendSecurityNotificationToAdmin",
            notificationObjectAdmin
          );
          // after if admin not assiging the bookingneedtoCancel
          this.broker.emit(
            "booking.adminCannotAssignProfessionalCanceledRide",
            {
              "cancellationReason": cancellationReason,
              "professsionalId": context.meta.professionalId.toString(),
              "cancelledTime": new Date(),
              "bookingId": bookingData._id.toString(),
              "professionalCancellationAmount": penalityAmount,
              "isCancelToNearbyProfessionals": false,
            }
          );
          /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
          // if (generalSettings.data.isEnableWebAppBooking) {
          this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
            "bookingId": bookingData._id,
            "bookingStatus": bookingData.bookingStatus,
            "dateTime": bookingData.updatedAt,
            "userAccessToken":
              bookingData.user?.deviceInfo[0]?.accessToken || "",
            "bookingInfo": bookingData?.user?.bookingInfo || {},
          });
          // }
          // return {
          //   "code": 200,
          //   "data": bookingObject(bookingData),
          //   "message": BOOKING_CANCELLED,
          // };
        }
        if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
          if (
            generalSettings?.data?.isEnableCorporateRideWalletPay &&
            bookingData.coorperate
          ) {
            const updateCorporateRideWalletPay = "";
            // let corporateData = await
            await this.broker.emit("admin.updateWalletAmountTripEnded", {
              "corporateId": bookingData.coorperate?._id?.toString(),
              "amount": bookingPayableAmount,
              "paymentType": constantUtil.CREDIT,
              "paymentMethod": bookingData.payment.option,
              "bookingId": bookingData._id?.toString(),
              "refBookingId": bookingData.bookingId,
              "transactionType": constantUtil.CANCELLATIONCHARGE,
              "transactionReason":
                RIDE_CANCELLATION_CHARGE + " / " + bookingData.bookingId,
            });
          }
        }
        return {
          "code": 200,
          "data": bookingObject(bookingData),
          "message": RIDE_CANCELLED,
        };
      }
      // here is the place all datas are updates when professional hit cancellbooking send
      // we take money so send a mail
      // sendMail
      if (
        generalSettings.data.emailConfigurationEnable &&
        bookingData?.professional?.email &&
        bookingData?.professional?.isEmailVerified
      ) {
        // sometimes some key missed so assign mannualy very important
        bookingData.activity.cancelTime = new Date();
        bookingData.cancellationReason = cancellationReason;
        // onely sendmail if penality amount is taken
        if (penalityStatus && penalityAmount > 0) {
          // sending to professional
          this.broker.emit("admin.sendServiceMail", {
            "templateName":
              "professionalCancellationEmailWithCancellationAmount",
            ...mappingUtil.professionalCancelBookingEmailObjectWithCancelAmount(
              bookingData
            ),
          });
        }
      }
    }
    if (context.params.userType === constantUtil.USER) {
      //only take money from user booking is since accepted to arrived
      const bookingPayableAmount = parseFloat(
        bookingData?.invoice?.payableAmount || 0
      );
      let refundAmount = bookingData.invoice.estimationAmount;
      let payableAmount = 0,
        cancellationAmountCreditToProfessional = 0;
      const cancellationMinute = helperUtil.getMinutesDiff(
        new Date(),
        bookingData.activity.acceptTime // professional accept time
      );
      const penalityStatus =
        cancellationMinute >= vehicleCategoryData.cancellation.thresholdMinute;

      const penalityAmount =
        vehicleCategoryData.cancellation.status === true && // only update cancellation money when city cancell fee applied // here why i am using this type of condition it is very easy update db other wise tow update query need to write ok.
        penalityStatus === true &&
        isHasPenalty === true
          ? vehicleCategoryData.cancellation.amount
          : 0;

      const nextProfessioanlStatus =
        bookingData.cancelledProfessionalList.length > 0 ? true : false; // if professional alredy cancelled then another professional is calling this ride is isCancelToNearbyProfessionals ride
      if (
        bookingData.bookingStatus === constantUtil.ACCEPTED ||
        bookingData.bookingStatus === constantUtil.ARRIVED
      ) {
        if (vehicleCategoryData.cancellation.status === true) {
          //exceed buffer time
          if (
            penalityStatus === true && //CHECK PENALITY STATUS
            penalityAmount > 0 // SOME TIMES CANCELLATION AMOUNT IS 0 SO WE DON'T NEEDD TO TAKE MONEY
          ) {
            // if (
            //   // bookingData.payment.option === constantUtil.PAYMENTCARD ||
            //   // bookingData.payment.option === constantUtil.PAYMENTCASH
            //   bookingData.payment.option !== constantUtil.PAYMENTWALLET
            // ) {
            //   // BLOW CODE WE WILL TAKE AMOUNT SO CREATE TRANSACTION HERE
            //   this.broker.emit("transaction.cancelCharge", {
            //     // "userId": context.meta.userId.toString(),
            //     "clientId": bookingData?.clientId?.toString(),
            //     "userId": bookingData.user._id.toString(),
            //     "bookingId": bookingData._id.toString(),
            //     "amount": penalityAmount,
            //     "previousBalance": bookingData.user.wallet.availableAmount,
            //     "currentBalance":
            //       parseFloat(bookingData.user.wallet.availableAmount) -
            //       parseFloat(penalityAmount),
            //     "refBookingId": bookingData.bookingId,
            //   });
            // }
            refundAmount =
              refundAmount - vehicleCategoryData.cancellation.amount;
            payableAmount = vehicleCategoryData.cancellation.amount;
          }
        }
      }
      if (bookingData.professional) {
        // update professioanl ongoing and tail booking
        this.broker.emit("professional.updateOnGoingAndTailBooking", {
          "professionalId": bookingData.professional._id.toString(),
          "bookingId": bookingData._id.toString(),
        });
      }
      // refund
      if (
        bookingData.user?.bookingInfo?.ongoingBooking?.toString() ===
          context.params.bookingId ||
        bookingData.bookingType === constantUtil.INSTANT
      ) {
        // udate ongoing booking
        this.broker.emit("user.removeOngoingBooking", {
          "userId": bookingData.user._id.toString(),
          "bookingId": context.params.bookingId.toString(),
        });
        //--------------- Refund-Ajith Start -----------------
        //Refund
        if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
          await this.broker.emit("transaction.newBookingCancelRefund", {
            "paymentTokenId": bookingData?.payment?.details?.id,
            "userId": bookingData?.user?._id?.toString(),
            "amount": penalityAmount.toString(),
            "currencyCode": bookingData.currencyCode,
            "paymentInitId": paymentInitId,
            "paymentInitAmount": paymentInitAmount,
            "cardId": cardId,
            "bookingId": bookingId,
            "refBookingId": refBookingId,
            "transactionType": constantUtil.PENALITY,
          });

          // // update wallet amount here beacuse if profesional accept schedule ride  is ongoing so amount is freezedAmount
          // await this.broker.emit('user.updateWalletAmount', {
          //   'userId': bookingData.user._id.toString(),
          //   'availableAmount': bookingData.user.wallet.availableAmount,
          //   'freezedAmount':
          //     bookingData.user.wallet.freezedAmount -
          //     bookingData.invoice.estimationAmount,
          //   'scheduleFreezedAmount':
          //     bookingData.user.wallet.scheduleFreezedAmount,
          // })
        }
        //---------- Refund-Ajith End --------------
        if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount":
              bookingData.user.wallet.availableAmount + refundAmount, // refundAmount = rideFreezedAmount - payableAmount(cancellationAmount)
            "freezedAmount":
              bookingData.user.wallet.freezedAmount -
              bookingData.invoice.estimationAmount,
            "scheduleFreezedAmount":
              bookingData.user.wallet.scheduleFreezedAmount,
          });
          if (payableAmount > 0) {
            //create transaction
            this.broker.emit("transaction.insertTransactionBasedOnUserType", {
              "transactionType": constantUtil.CANCELLATIONCHARGE,
              "transactionAmount": payableAmount.toFixed(2), //refundAmount // payableAmount means cancellationAmount
              "previousBalance":
                parseFloat(bookingData.user.wallet.availableAmount) +
                parseFloat(bookingData.user.wallet.freezedAmount),
              "currentBalance":
                parseFloat(bookingData.user.wallet.availableAmount) +
                parseFloat(bookingData.user.wallet.freezedAmount) +
                parseFloat(payableAmount), //refundAmount // payableAmount means cancellationAmount
              "transactionReason":
                constantUtil.CANCELLATIONCHARGE + " / " + bookingId,
              "refBookingId": refBookingId,
              "transactionStatus": constantUtil.SUCCESS,
              "transactionId": transactionId,
              "paymentType": constantUtil.DEBIT,
              "paymentGateWay": constantUtil.NONE,
              "gatewayResponse": {},
              "userType": constantUtil.USER, // Debit From
              "userId": bookingData.user._id.toString(), // Debit From
              "professionalId": null, // Debit From
              "paymentToUserType": constantUtil.ADMIN, // Credit To
              "paymentToUserId": null, // Credit To
            });
          }
        }
        // // IF PAYMENT IS cash so we have take canel penality amount from wallet
        // if (bookingData.payment.option === constantUtil.PAYMENTCASH) {
        //   // only take money is payement amount is take more than 0
        //   if (penalityAmount) {
        //     if (bookingData.payment.option === constantUtil.PAYMENTCASH) {
        //       this.broker.emit("user.updateWalletAmount", {
        //         "userId": bookingData.user._id.toString(),
        //         "availableAmount":
        //           bookingData.user.wallet.availableAmount - penalityAmount,
        //         "freezedAmount": bookingData.user.wallet.freezedAmount,
        //         "scheduleFreezedAmount":
        //           bookingData.user.wallet.scheduleFreezedAmount,
        //       });
        //     }
        //   }
        // }
      }
      if (
        bookingData.bookingType === constantUtil.SCHEDULE &&
        bookingData.user?.bookingInfo?.scheduledBooking?.some(
          (element) =>
            element?.bookingId?.toString() === context.params.bookingId
        )
      ) {
        // REMOVE SCHEDULE RIDE
        this.broker.emit("user.removeScheduleBooking", {
          "userId": bookingData.user._id.toString(),
          "bookingType": bookingData.bookingType,
          "bookingId": context.params.bookingId,
        });
        //-------------Refund-Ajith Start ----------------
        //Refund
        if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
          await this.broker.emit("transaction.newBookingCancelRefund", {
            "paymentTokenId": bookingData?.payment?.details?.id,
            "userId": bookingData?.user?._id?.toString(),
            "amount": 0,
            "currencyCode": bookingData.currencyCode,
            "paymentInitId": paymentInitId,
            "paymentInitAmount": paymentInitAmount,
            "cardId": cardId,
            "bookingId": bookingId,
            "refBookingId": refBookingId,
            "transactionType": constantUtil.CANCELCAPTURE,
          });
          // // update wallet amount here beacuse if profesional NOT accept schedule ride  is SCHEDULE so amount is schedulefreezedAmount
          // await this.broker.emit('user.updateWalletAmount', {
          //   'userId': bookingData.user._id.toString(),
          //   'availableAmount': bookingData.user.wallet.availableAmount,
          //   'freezedAmount': bookingData.user.wallet.freezedAmount,
          //   'scheduleFreezedAmount':
          //     bookingData.user.wallet.scheduleFreezedAmount -
          //     bookingData.invoice.estimationAmount,
          // })
        }
        //----------Refund-Ajith End ------------
        if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount":
              bookingData.user.wallet.availableAmount + refundAmount,
            "freezedAmount": bookingData.user.wallet.freezedAmount,
            "scheduleFreezedAmount":
              bookingData.user.wallet.scheduleFreezedAmount -
              bookingData.invoice.estimationAmount,
          });
        }
        // // IF PAYMENT IS cash so we have take canel penality amount from wallet
        // if (bookingData.payment.option === constantUtil.PAYMENTCASH) {
        //   // only take money is payement amount is take more than 0
        //   if (penalityAmount) {
        //     this.broker.emit("user.updateWalletAmount", {
        //       "userId": bookingData.user._id.toString(),
        //       "availableAmount":
        //         bookingData.user.wallet.availableAmount - penalityAmount,
        //       "freezedAmount": bookingData.user.wallet.freezedAmount,
        //       "scheduleFreezedAmount":
        //         bookingData.user.wallet.scheduleFreezedAmount,
        //     });
        //   }
        // }
      }
      // IF PAYMENT IS cash so we have take canel penality amount from wallet
      if (
        bookingData.payment.option === constantUtil.PAYMENTCASH ||
        bookingData.payment.option === constantUtil.CONST_PIX ||
        bookingData.payment.option === constantUtil.CONST_POS
      ) {
        // only take money is payement amount is take more than 0
        if (penalityAmount > 0) {
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount":
              bookingData.user.wallet.availableAmount - penalityAmount,
            "freezedAmount": bookingData.user.wallet.freezedAmount,
            "scheduleFreezedAmount":
              bookingData.user.wallet.scheduleFreezedAmount,
          });
          //create transaction
          await this.broker.emit(
            "transaction.insertTransactionBasedOnUserType",
            {
              "transactionType": constantUtil.CANCELLATIONCHARGE,
              "transactionAmount": penalityAmount.toFixed(2),
              "previousBalance": bookingData.user.wallet.availableAmount,
              "currentBalance":
                bookingData.user.wallet.availableAmount - penalityAmount,
              "transactionReason":
                constantUtil.CANCELLATIONCHARGE + " " + refBookingId,
              "transactionStatus": constantUtil.SUCCESS,
              "transactionId": transactionId,
              "paymentType": constantUtil.DEBIT,
              "paymentGateWay": constantUtil.NONE,
              "gatewayResponse": {},
              "userType": constantUtil.USER, // Debit From
              "userId": bookingData.user._id.toString(), // Debit From
              "professionalId": null, // Debit From
              "paymentToUserType": constantUtil.ADMIN, // Credit To
              "paymentToUserId": null, // Credit To
            }
          );
        }
      }
      //#region  Cancel Amount Credit to Professional Start
      if (penalityAmount > 0 && bookingData?.professional?._id) {
        // some times penality amount is 0 so we dont need to make transation
        // take amount from prfessional
        cancellationAmountCreditToProfessional =
          penalityAmount *
          ((bookingData.invoice
            ?.cancellationFeeCreditToProfessionalPrecentage || 0) /
            100);
        //
        if (cancellationAmountCreditToProfessional > 0) {
          this.broker.emit("professional.updateWalletAmount", {
            "professionalId": bookingData.professional?._id?.toString(),
            "availableAmount":
              bookingData?.professional?.wallet?.availableAmount +
              cancellationAmountCreditToProfessional, //professional cancellation amount detection
            "freezedAmount": bookingData.professional.wallet.freezedAmount,
          });
          //create transaction
          this.broker.emit("transaction.insertTransactionBasedOnUserType", {
            "transactionType": constantUtil.CANCELLATIONCREDIT,
            "transactionAmount":
              cancellationAmountCreditToProfessional.toFixed(2),
            "previousBalance":
              bookingData?.professional?.wallet?.availableAmount,
            "currentBalance":
              bookingData?.professional?.wallet?.availableAmount +
              cancellationAmountCreditToProfessional,
            "transactionReason": stringTransactionReasonCredit,
            "refBookingId": refBookingId,
            "transactionStatus": constantUtil.SUCCESS,
            "transactionId": transactionId,
            "paymentType": constantUtil.CREDIT,
            "paymentGateWay": constantUtil.NONE,
            "gatewayResponse": {},
            "userType": constantUtil.ADMIN, // Debit From
            "userId": null, //bookingData.user._id.toString(), // Debit From
            "professionalId": null, // Debit From
            "paymentToUserType": constantUtil.PROFESSIONAL, // Credit To
            "paymentToUserId": bookingData.professional._id.toString(), // Credit To
          });
          // this.broker.emit("transaction.professionalSiteCommissionTransaction", {
          //   "professionalId": bookingData.professional._id.toString(),
          //   "amount": cancellationAmountCreditToProfessional,
          //   "bookingId": bookingData._id.toString(),
          //   "refBookingId": bookingData.bookingId,
          //   "paymentType": constantUtil.CREDIT,
          // });
        }
      }
      //#endregion  Cancel Amount Credit to Professional End
      bookingData = await this.adapter.model
        .findOneAndUpdate(
          { "_id": mongoose.Types.ObjectId(context.params.bookingId) },
          {
            "bookingStatus": constantUtil.USERCANCELLED,
            "cancellationReasonId": cancellationReasonId,
            "cancellationReason": cancellationReason,
            "isHasPenalty": isHasPenalty,
            "activity.cancelTime": new Date(),
            "invoice.professionalCommision":
              cancellationAmountCreditToProfessional,
            "invoice.payableAmount": payableAmount,
            "invoice.cancellationStatus": penalityStatus,
            "invoice.cancellationAmount": penalityAmount,
            "invoice.cancellationMinute": cancellationMinute,
            "invoice.siteCommission":
              payableAmount - cancellationAmountCreditToProfessional,
            // only user cancellation  charge is applied here
            "activity.cancelledBy": {
              "id": context.meta.userId.toString(),
              "userType": constantUtil.USER,
            },
            "isCancelToNearbyProfessionals": nextProfessioanlStatus,
            "regionalData.cancelTime": helperUtil.toRegionalUTC(new Date()),
          },
          { "new": true }
        )
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
        })
        .populate("category")
        .populate("security")
        .populate("officer", { "password": 0, "preferences": 0 })
        .lean();
      if (!bookingData) {
        throw new MoleculerError(USER_RIDE_CANCEL_FAILD);
      }
      // bookingData = bookingData.toJSON();
      if (bookingData.professional) {
        switch (bookingData.serviceCategory?.toLowerCase()) {
          case constantUtil.CONST_SHARERIDE.toLowerCase():
            {
              await this.broker.emit(
                "professional.updateProfessionalShareRide",
                {
                  "actionStatus": constantUtil.USERCANCELLED,
                  "bookingId": context.params.bookingId,
                  "professionalId": bookingData?.professional?._id?.toString(),
                  // "pickUpLat": context.params.pickUpLat,
                  // "pickUpLng": context.params.pickUpLng,
                  // "dropLat": context.params.dropLat,
                  // "dropLng": context.params.dropLng,
                }
              );
              await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
                "actionStatus": constantUtil.CONST_DROP,
                "bookingId": null,
                "professionalId": bookingData?.professional?._id?.toString(),
                "serviceCategory": bookingData.serviceCategory,
                "lastRidePassengerCount": bookingData.passengerCount || 0,
                "lat": context.params?.latLng?.lat,
                "lng": context.params?.latLng?.lng,
                "parentShareRideId": bookingData?.parentShareRideId,
                // "passengerCount": bookingData?.passengerCount || 0,
              });
              // await this.adapter.model.updateOne(
              //   {
              //     "_id": mongoose.Types.ObjectId(
              //       bookingData?.parentShareRideId?.toString()
              //     ),
              //   },
              //   {
              //     "$inc": {
              //       "currentPassengerCount": -parseInt(
              //         bookingData?.passengerCount || 0
              //       ),
              //     },
              //   }
              // );
            }
            break;
        }
      }
      // if (bookingData?.couponId) {
      //   await this.broker.emit("admin.updateCouponCount", {
      //     "userId": bookingData.user._id.toString(),
      //     "couponId": bookingData?.couponId?.toString(),
      //     "actionType": constantUtil.USERCANCELLED,
      //   });
      // }
      //Get Booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      }); // Get booking Details
      const actualBookingStatus = bookingData.bookingStatus;
      // data adding for bookingObject
      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage;
      bookingData.retryTime = generalSettings.data.driverRequestTimeout;
      bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
      bookingData["imageURLPath"] =
        generalSettings.data.spaces.spacesBaseUrl +
        "/" +
        generalSettings.data.spaces.spacesObjectName;

      if (bookingData.professional) {
        //  send notification to professional
        const notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.PROFESSIONAL,
            "action": constantUtil.ACTION_CANCELBOOKING,
            "timestamp": Date.now(),
            "message":
              BOOKING_CANCELLED_BY +
              " " +
              context.params.userType.toUpperCase(),
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "id": bookingData.professional?._id?.toString(),
              "token": bookingData?.professional?.deviceInfo[0]?.deviceId || "",
              "deviceType":
                bookingData?.professional?.deviceInfo[0]?.deviceType || "",
              "platform":
                bookingData?.professional?.deviceInfo[0]?.platform || "",
              "socketId":
                bookingData?.professional?.deviceInfo[0]?.socketId || "",
            },
          ],
        };
        console.log("i am heere");
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // send notification to admin this booking is cancelled and rebooking trying

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
      if (bookingData.user) {
        if (bookingData.bookingStatus === actualBookingStatus) {
          bookingData["bookingStatus"] = constantUtil.USERDENY;
        } // Need to Remove After Mar 2025
        const notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": constantUtil.ACTION_CANCELBOOKING,
            "timestamp": Date.now(),
            "message": RIDE_CANCELLED,
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
              "id": bookingData.user?._id?.toString(),
              "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
              "platform": bookingData.user?.deviceInfo[0]?.platform || "",
              "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
            },
          ],
        };
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
      // send notifications
      if (bookingData.bookingStatus === actualBookingStatus) {
        bookingData["bookingStatus"] = actualBookingStatus;
      } // Need to Remove After Mar 2025
      // send mail
      if (
        generalSettings.data.emailConfigurationEnable &&
        bookingData?.user?.email &&
        bookingData?.user?.isEmailVerified
      ) {
        // only send notification if cancellation amount is taken
        if (penalityStatus && penalityAmount > 0) {
          this.broker.emit("admin.sendServiceMail", {
            "templateName": "userCancellationEmailWithCancelAmount",
            ...mappingUtil.userCancelBookingEmailObjectWithCancellationAmount(
              bookingData
            ),
          });
        }
      }
      /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
      // if (generalSettings.data.isEnableWebAppBooking) {
      this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
        "bookingId": bookingData._id,
        "bookingStatus": bookingData.bookingStatus,
        "dateTime": bookingData.updatedAt,
        "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
        "bookingInfo": bookingData?.user?.bookingInfo || {},
      });
      // }
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_SHARERIDE.toLowerCase():
          {
            this.broker.emit("booking.sendNotificationActionShareRideStatus", {
              "bookingId": context.params.bookingId.toString(),
              "parentShareRideId": bookingData.parentShareRideId?.toString(),
              "userType": constantUtil.PROFESSIONAL,
              "actionStatus": constantUtil.ACTION_CANCELBOOKING,
              "notificationMessage":
                BOOKING_CANCELLED_BY + " " + INFO_FELLOW_PASSENGER,
            });
          }
          break;
      }
      if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
        if (
          generalSettings?.data?.isEnableCorporateRideWalletPay &&
          bookingData.coorperate
        ) {
          const updateCorporateRideWalletPay = "";
          // let corporateData = await
          await this.broker.emit("admin.updateWalletAmountTripEnded", {
            "corporateId": bookingData.coorperate?._id?.toString(),
            "amount": bookingPayableAmount,
            "paymentType": constantUtil.CREDIT,
            "paymentMethod": bookingData.payment.option,
            "bookingId": bookingData._id?.toString(),
            "refBookingId": bookingData.bookingId,
            "transactionType": constantUtil.CANCELLATIONCHARGE,
            "transactionReason":
              RIDE_CANCELLATION_CHARGE + " / " + bookingData.bookingId,
          });
        }
      }
      return {
        "code": 200,
        "data": bookingObject(bookingData),
        "message": BOOKING_CANCELLED,
      };
    }
    // #region Penalty
    // #endregion Penalty
  } else {
    // return means booking Data ride is start or anothor booking status
    return {
      "code": 403,
      "data": bookingObject(bookingData),
      "message": RIDE_IS + " " + bookingData.bookingStatus,
    };
  }
};
// ACCEPT BOOKING
bookingAction.professionalAcceptBooking = async function (context) {
  let responseData = {},
    errorCode = 200,
    responseMessage = "";
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  const {
    INVALID_BOOKING,
    PROFESSIONAL_NOT_FOUND,
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_ACCEPTED_BY_OTHERS,
    BOOKING_UPDATE_FAILED,
    USER_NOT_FOUND,
    BOOKING_ACCEPTED,
    SOMETHING_WENT_WRONG,
    INFO_FELLOW_PASSENGER,
  } = await notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.professionalDetails?.languageCode
  );
  try {
    // let bookingData = await this.adapter.model
    //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    //   .populate("admin", { "data.accessToken": 0, "data.password": 0 })
    //   .populate("user", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "cards": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("professional", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "cards": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("category")
    //   .lean();

    // Get booking Details
    let bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    if (
      bookingData.professional &&
      bookingData.professional !== undefined &&
      bookingData.professional.toString() !==
        context.meta.professionalId.toString()
    ) {
      throw new MoleculerError(BOOKING_ACCEPTED_BY_OTHERS);
    }
    //
    let vehicleCategoryData;
    const vehicleCategoryDataSet = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryDataSet) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    if (bookingData.bookingStatus === constantUtil.AWAITING) {
      let professionalData = await this.broker.emit("professional.getById", {
        "id": context.meta.professionalId.toString(),
      });
      professionalData = professionalData && professionalData[0];
      if (!professionalData) {
        throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
      }
      const vehicleDetails = {};
      professionalData.vehicles.forEach((vehicle) => {
        if (
          vehicle.status === constantUtil.ACTIVE &&
          vehicle.defaultVehicle === true
        ) {
          vehicleDetails["vehicleCategoryId"] =
            bookingData.vehicle.vehicleCategoryId;
          vehicleDetails["vinNumber"] = vehicle.vinNumber;
          vehicleDetails["type"] = vehicle.type;
          vehicleDetails["plateNumber"] = vehicle.plateNumber;
          vehicleDetails["model"] = vehicle.model;
          vehicleDetails["year"] = vehicle.year;
          vehicleDetails["color"] = vehicle.color;
          vehicleDetails["noOfDoors"] = vehicle.noOfDoors;
          vehicleDetails["noOfSeats"] = vehicle.noOfSeats;
        }
      });
      if (Object.keys(vehicleDetails).length === 0) {
        throw new MoleculerError("NO VEHICLE IS AVAILABLE");
      }
      const updatedBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
          "bookingStatus": constantUtil.AWAITING,
        },
        {
          "professional": context.meta.professionalId,
          "vehicle": vehicleDetails,
          "bookingStatus": constantUtil.ACCEPTED,
          "activity.acceptLatLng": context.params.latLng,
          "activity.acceptTime": new Date(),
          "isCancelToNearbyProfessionals": false,
          "regionalData.acceptTime": helperUtil.toRegionalUTC(new Date()),
        }
      );
      if (!updatedBookingData.nModified) {
        throw new MoleculerError(BOOKING_UPDATE_FAILED);
      }
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_SHARERIDE.toLowerCase(): // no need to update ongoing & Tail ride
          break;

        default:
          if (professionalData.bookingInfo.ongoingBooking === null) {
            await this.broker.emit("professional.updateOnGoingBooking", {
              "professionalId": context.meta.professionalId,
              "bookingId": context.params.bookingId,
            });
          } else if (generalSettings.data.isTailRideNeeded) {
            await this.broker.emit("professional.updateTailOnGoingBooking", {
              "professionalId": context.meta.professionalId,
              "bookingId": context.params.bookingId,
            });
          }
          break;
      }

      // bookingData = await this.adapter.model
      //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      //   // .populate("admin", { "data.accessToken": 0, "data.password": 0 })
      //   .populate("admin", { "data.password": 0 })
      //   .populate("user", {
      //     "password": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //     "cards": 0,
      //   })
      //   .populate("professional", {
      //     "password": 0,
      //     "cards": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //   })
      //   .populate("category")
      //   .populate("security")
      //   .populate("officer", { "password": 0, "preferences": 0 })
      //   .lean();

      // Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      }); // Get booking Details
      //Update Professional Hub
      const updatedProfessionalHub = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
        },
        {
          "hub": bookingData.professional?.vehicles[0]?.hub?.toString() || null,
        }
      );
      bookingData.vehicle = vehicleDetails;

      vehicleCategoryData =
        vehicleCategoryDataSet[
          bookingData.vehicle.vehicleCategoryId.toString()
        ];
      // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
      if (bookingData.user) {
        // For update and wallet and card payment
        let userData = await this.broker.emit("user.getById", {
          "id": bookingData.user._id.toString(),
        });
        userData = userData && userData[0];
        if (!userData) {
          throw new MoleculerError(USER_NOT_FOUND);
        }
        if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
          // SCHEDULE
          if (bookingData.bookingType === constantUtil.SCHEDULE) {
            const finalAmount =
              parseFloat(userData.wallet.scheduleFreezedAmount) -
              parseFloat(bookingData.invoice.estimationAmount);
            this.broker.emit("user.updateWalletAmountInScheduleBooking", {
              "userId": bookingData.user._id.toString(),
              "amount": parseFloat(
                bookingData.invoice.estimationAmount.toFixed(2)
              ),
              "finalAmount": parseFloat(finalAmount.toFixed(2)),
            });
          }
          // INSTANT
          if (bookingData.bookingType === constantUtil.INSTANT) {
            const finalAmount =
              parseFloat(userData.wallet.availableAmount) -
              parseFloat(bookingData.invoice.estimationAmount);
            this.broker.emit("user.updateWalletAmountInBooking", {
              "userId": bookingData.user._id.toString(),
              "amount": parseFloat(
                bookingData.invoice.estimationAmount.toFixed(2)
              ),
              "finalAmount": parseFloat(finalAmount.toFixed(2)),
            });
          }
        }
        // if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
        //   // SCHEDULE
        //   if (bookingData.bookingType === constantUtil.SCHEDULE) {
        //     const finalAmount =
        //       parseFloat(userData.wallet.scheduleFreezedAmount) -
        //       parseFloat(bookingData.invoice.estimationAmount)
        //     this.broker.emit('user.updateWalletAmountInScheduleBooking', {
        //       'userId': bookingData.user._id.toString(),
        //       'amount': parseFloat(
        //         bookingData.invoice.estimationAmount.toFixed(2)
        //       ),
        //       'finalAmount': parseFloat(finalAmount.toFixed(2)),
        //     })
        //   }
        //   // INSTANT
        //   if (bookingData.payment.option === constantUtil.INSTANT) {
        //     //here the take amount if user book a ride using card

        //     updatedBookingData = await this.adapter.model.updateOne(
        //       {
        //         '_id': mongoose.Types.ObjectId(context.params.bookingId),
        //         'bookingStatus': constantUtil.ACCEPTED,
        //       },
        //       {
        //         'bookingData.payment.card.cvv': null,
        //       }
        //     )

        //     if (updatedBookingData.nModified === 0)
        //       throw new MoleculerError('BOOKING DETAILS FAILED TO UPDATE')

        //     bookingData = await this.adapter.model
        //       .findById(
        //         mongoose.Types.ObjectId(context.params.bookingId.toString())
        //       )
        //       .populate('admin', { 'data.accessToken': 0, 'data.password': 0 })
        //       .populate('user', {
        //         'password': 0,
        //         'bankDetails': 0,
        //         'trustedContacts': 0,
        //         'cards': 0,
        //       })
        //       .populate('professional', {
        //         'password': 0,
        //         'cards': 0,
        //         'bankDetails': 0,
        //         'trustedContacts': 0,
        //       })
        //       .populate('category')
        //       .populate('security')
        //       .populate('officer', { 'password': 0, 'preferences': 0 })

        //     bookingData = bookingData.toJSON()
        //   }
        // }
        if (bookingData.user.bookingInfo.pendingReview) {
          await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(
                bookingData.user.bookingInfo.pendingReview?.toString()
              ),
              "bookingStatus": constantUtil.ENDED,
            },
            {
              "$set": {
                "bookingReview.userReview.rating": 5,
                "bookingReview.isUserReviewed": true,
              },
            }
          );
          await this.broker.emit("user.updateRating", {
            "userId": bookingData.user._id.toString(),
            "rating": 5,
          });
        }
        this.broker.emit("user.updateOnGoingBooking", {
          "userId": bookingData.user._id.toString(),
          "bookingId": context.params.bookingId,
          "bookingType": bookingData.bookingType,
        });
        // send mail and
        // const rideLocation = [];
        switch (bookingData.serviceCategory?.toLowerCase()) {
          case constantUtil.CONST_SHARERIDE.toLowerCase():
            {
              await this.broker.emit(
                "professional.updateProfessionalShareRide",
                {
                  "actionStatus": constantUtil.STATUS_UPCOMING,
                  "bookingId": context.params.bookingId,
                  "professionalId": context.meta.professionalId,
                  // "pickUpLat": context.params.pickUpLat,
                  // "pickUpLng": context.params.pickUpLng,
                  // "dropLat": context.params.dropLat,
                  // "dropLng": context.params.dropLng,
                }
              );
              await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
                // "actionStatus": constantUtil.ACCEPTED,
                "actionStatus": constantUtil.STATUS_UPCOMING,
                "bookingId": context.params.bookingId, // If no Shortest path ride  then currnent accepted booking is Default
                "professionalId": context.meta.professionalId,
                "serviceCategory": bookingData.serviceCategory,
                "lastRidePassengerCount": 0,
                "lat": context.params?.latLng?.lat,
                "lng": context.params?.latLng?.lng,
                "parentShareRideId": bookingData?.parentShareRideId,
                // "passengerCount": 0,
              });
              // Check and Update parentShareRideId & rideLocation coordinates (if ongoing Shared Ride )
              // // ---------------- rideLocation Start ------------
              // rideLocation.push(bookingData?.pickupLocation?.coordinates[0]);
              // rideLocation.push(bookingData?.dropLocation?.coordinates[0]);
              // // ---------------- rideLocation End ------------
              await this.adapter.model.updateOne(
                {
                  "_id": mongoose.Types.ObjectId(bookingData?._id?.toString()),
                },
                {
                  "parentShareRideId":
                    bookingData?.parentShareRideProfessional?.toString() ===
                    context.meta.professionalId?.toString()
                      ? bookingData?.parentShareRideId?.toString()
                      : null,
                  "parentShareRideProfessional": context.meta.professionalId,
                  "totalPassengerCount":
                    bookingData?.professional?.vehicles[0]?.noOfSeats ||
                    vehicleCategoryData?.seatCount ||
                    0,
                  "rideLocation.coordinates":
                    bookingData?.parentShareRideProfessional?.toString() ===
                    context.meta.professionalId?.toString()
                      ? // ? [rideLocation]
                        [
                          bookingData?.pickupLocation?.coordinates[0],
                          bookingData?.dropLocation?.coordinates[0],
                        ]
                      : bookingData?.rideLocation?.coordinates,
                }
              );
              //update currentPassengerCount in Parent Share Ride
              await this.adapter.model.updateOne(
                {
                  "_id": mongoose.Types.ObjectId(
                    bookingData?.parentShareRideId?.toString()
                  ),
                  "parentShareRideId": null,
                },
                {
                  // // "totalPassengerCount": vehicleCategoryData?.seatCount || 0,
                  // "totalPassengerCount":
                  //   bookingData?.professional?.vehicles[0]?.noOfSeats ||
                  //   vehicleCategoryData?.seatCount ||
                  //   0,
                  "$inc": {
                    "currentPassengerCount": parseInt(
                      bookingData?.passengerCount || 0
                    ),
                  },
                }
              );
            }
            break;
        }
      }

      // if (bookingData.invoice.couponApplied === true) {
      //   await this.broker.emit("admin.updateCouponCount", {
      //     "userId": bookingData.user._id.toString(),
      //     "couponId": bookingData.couponId.toString(),
      //   });
      // }
      // bookingData.professional = professionalData;
      // bookingData.vehicle = vehicleDetails;
      // // bookingData.bookingStatus = constantUtil.ACCEPTED;

      // vehicleCategoryData =
      //   vehicleCategoryDataSet[
      //     bookingData.vehicle.vehicleCategoryId.toString()
      //   ];

      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage;
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
      bookingData["imageURLPath"] =
        generalSettings.data.spaces.spacesBaseUrl +
        "/" +
        generalSettings.data.spaces.spacesObjectName;

      // if (bookingData.bookingBy === constantUtil.USER) {
      if (bookingData?.user?._id && bookingData.user?.deviceInfo[0]) {
        const notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": constantUtil.ACTION_ACCEPTBOOKING,
            "timestamp": Date.now(),
            "message": BOOKING_ACCEPTED,
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "token": bookingData.user?.deviceInfo[0]?.deviceId,
              "id": bookingData.user?._id?.toString(),
              "deviceType": bookingData.user?.deviceInfo[0]?.deviceType,
              "platform": bookingData.user?.deviceInfo[0]?.platform,
              "socketId": bookingData.user?.deviceInfo[0]?.socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_SHARERIDE.toLowerCase():
          {
            this.broker.emit("booking.sendNotificationActionShareRideStatus", {
              "bookingId": context.params.bookingId.toString(),
              "parentShareRideId": bookingData.parentShareRideId?.toString(),
              "userType": constantUtil.USER,
              "actionStatus": constantUtil.ACCEPTED,
              "notificationMessage":
                // BOOKING_ACCEPTED + " " + INFO_FELLOW_PASSENGER,
                "Another passenger has booked a ride along your route",
            });
          }
          break;
      }

      if (bookingData.bookingBy === constantUtil.ADMIN) {
        const adminNotificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.ADMIN,
            "action": constantUtil.ACTION_ACCEPTBOOKING,
            "timestamp": Date.now(),
            "message": BOOKING_ACCEPTED,
            "details": bookingObject(bookingData),
          },
          "registrationTokens": [
            {
              "token": bookingData.admin.data.accessToken,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit(
          "socket.sendAdminNotification",
          adminNotificationObject
        );
        // this.broker.emit('socket.statusChangeEvent', adminNotificationObject)
      }

      professionalData = await this.broker.emit("professional.getById", {
        "id": context.meta.professionalId.toString(),
      });
      professionalData = professionalData && professionalData[0];
      if (professionalData.bookingInfo.ongoingTailBooking !== null) {
        // bookingData = await this.adapter.model
        //   .findById(
        //     mongoose.Types.ObjectId(
        //       professionalData.bookingInfo.ongoingBooking.toString()
        //     )
        //   )
        //   .populate("admin", { "data.accessToken": 0, "data.password": 0 })
        //   .populate("user", {
        //     "password": 0,
        //     "bankDetails": 0,
        //     "trustedContacts": 0,
        //     "cards": 0,
        //   })
        //   .populate("professional", {
        //     "password": 0,
        //     "cards": 0,
        //     "bankDetails": 0,
        //     "trustedContacts": 0,
        //   })
        //   .populate("category")
        //   .populate("security")
        //   .populate("officer", { "password": 0, "preferences": 0 })
        //   .lean();

        // bookingData = bookingData.toJSON();

        // Get ongoingBooking booking Details
        bookingData = await this.getBookingDetailsById({
          "bookingId":
            professionalData?.bookingInfo?.ongoingBooking?.toString(),
        }); // Get ongoingBooking booking Details

        vehicleCategoryData =
          vehicleCategoryDataSet[
            bookingData.vehicle.vehicleCategoryId.toString()
          ];
        bookingData.vehicle.vehicleCategoryName =
          vehicleCategoryData.vehicleCategory;
        bookingData.vehicle.vehicleCategoryImage =
          vehicleCategoryData.categoryImage;
        bookingData.vehicle.vehicleCategoryMapImage =
          vehicleCategoryData.categoryMapImage;
        // bookingData.distanceUnit = bookingData.category.distanceType;
      } else {
        // Get booking Details
        bookingData = await this.getBookingDetailsById({
          "bookingId": context.params.bookingId.toString(),
        }); // Get booking Details
      }
      //-------- Send SMS Alert Start --------------------------
      // if (
      //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
      //   bookingData.bookingBy === constantUtil.ADMIN ||
      //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
      //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
      // )
      {
        this.broker.emit("booking.sendSMSAlertByRideId", {
          "rideId": context.params.bookingId.toString(),
          "bookingFrom": bookingData.bookingFrom,
          "notifyType": constantUtil.ACCEPTED,
          "notifyFor": constantUtil.USER,
        });
      }
      // Send Message To Package Receiver
      // if (
      //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
      //   bookingData.bookingBy === constantUtil.ADMIN ||
      //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
      //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
      // )
      if (
        bookingData.serviceCategory ===
        constantUtil.CONST_PACKAGES.toLowerCase()
      ) {
        this.broker.emit("booking.sendSMSAlertByRideId", {
          "rideId": context.params.bookingId.toString(),
          "bookingFrom": bookingData.bookingFrom,
          "notifyType": constantUtil.ACCEPTED,
          "notifyFor": constantUtil.PACKAGERECEIVER,
        });
      }
      //-------- Send SMS Alert End --------------------------
      /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
      // if (generalSettings.data.isEnableWebAppBooking) {
      this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
        "bookingId": bookingData._id,
        "bookingStatus": bookingData.bookingStatus,
        "dateTime": bookingData.updatedAt,
        "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
        "bookingInfo": bookingData?.user?.bookingInfo || {},
      });
      // }
    }
    responseData = bookingObject(bookingData);
    responseMessage = BOOKING_ACCEPTED;
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": responseMessage,
  };
};

// BORKING STATUS CHANGE (ARRIVED, STARTED, ENDED)
bookingAction.updateProfessionalBookingStatus = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const {
    OTP_INCORRECT,
    VEHICLE_CATEGORY_NOT_FOUND,
    INVALID_BOOKING,
    BOOKING_UPDATE_STATUS_FAILED,
    PROFESSIONAL_ARRIVED,
    PROFESSIONAL_STARTED,
    TRIP_END,
    INFO_FELLOW_PASSENGER,
    BOOKING_PAID,
    REFUND_TO_WALLET,
    INFO_INCENTIVE_CREDIT,
    INFO_REFERREL_CHARGE,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  // const notifyMsg = await notifyMessage.setNotifyLanguage(
  //   context.meta?.userDetails?.languageCode
  // );
  let snapWayData = [],
    manualWayData = [],
    snapErrorData = {},
    finalWayDataSet = [],
    wayPointsSet = [],
    checkProfessionalRideCount = 0,
    checkUserRideCount = 0;

  const directionsData = {},
    finalEncodedPolyline = [],
    traveledDataSet = {};

  const professionalStatus = context.params.professionalStatus.toUpperCase();
  if (
    context.params.bookingOTP &&
    professionalStatus === constantUtil.STARTED
  ) {
    const checkJson = await this.adapter.model.findOne({
      "_id": context.params.bookingId,
      "bookingOTP": context.params.bookingOTP.toString(),
    });
    if (!checkJson) {
      throw new MoleculerError(OTP_INCORRECT);
    }
  }
  // let bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .populate("couponId")
  //   .populate("coorperate")
  //   .populate("couponId")
  //   .lean();

  // Get booking Details
  let bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId.toString(),
  }); // Get booking Details
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  let vehicleCategoryData;
  const vehicleCategoryDataSet = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryDataSet) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  // let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
  //   "id": bookingData.category.toString(),
  // });
  // serviceCategoryData = serviceCategoryData[0];

  // bookingData.isNeedSecurityImageUpload =
  //   serviceCategoryData.isNeedSecurityImageUpload;

  if (
    (bookingData.bookingStatus === constantUtil.ACCEPTED &&
      professionalStatus === constantUtil.ARRIVED) ||
    (bookingData.bookingStatus === constantUtil.ARRIVED &&
      professionalStatus === constantUtil.STARTED) ||
    (bookingData.bookingStatus === constantUtil.STARTED &&
      professionalStatus === constantUtil.ENDED)
  ) {
    let mapLocationData,
      formatted_address = "";
    if (professionalStatus === constantUtil.ENDED) {
      mapLocationData = await googleApiUtil.geocode({
        "clientId": clientId,
        "lat": context.params.lat,
        "lng": context.params.lng,
      });
      formatted_address = await googleApiUtil.formattedAddress(
        mapLocationData.data.results
      );
      // const mapData = await googleApiUtil.directions({
      //   "from": `${bookingData.origin.lat},${bookingData.origin.lng}`,
      //   "to": `${context.params.lat},${context.params.lat}`,
      // });
    }
    bookingData.activity.rideStops = bookingData.activity.rideStops.map(
      (stop) => {
        if (
          professionalStatus === constantUtil.STARTED &&
          stop.type === constantUtil.ORIGIN
        ) {
          stop.lat = context.params.lat;
          stop.lng = context.params.lng;
          stop.status = constantUtil.ENDED;
        } else if (
          professionalStatus === constantUtil.ENDED &&
          stop.type === constantUtil.DESTINATION
        ) {
          const address = formatted_address;
          // mapLocationData.data.results[0].formatted_address || null;
          stop.lat = context.params.lat;
          stop.lng = context.params.lng;
          stop.status = constantUtil.ENDED;
          if (address) {
            stop.addressName = address;
            stop.fullAddress = address;
            stop.shortAddress = address;
          }
        }
        if (stop.type === constantUtil.ORIGIN) {
          directionsData["from1"] = [stop.lat + "," + stop.lng];
          directionsData["turfFrom"] = turf.point([
            parseFloat(stop.lat),
            parseFloat(stop.lng),
          ]);
        }
        if (stop.type === constantUtil.DESTINATION) {
          directionsData["to1"] = [stop.lat + "," + stop.lng];
          directionsData["turfTo"] = turf.point([
            parseFloat(stop.lat),
            parseFloat(stop.lng),
          ]);
        }
        return stop;
      }
    );
    let workedMins;
    let waitingMins;
    // if (professionalStatus === constantUtil.ENDED) {
    //   waitingMins = helperUtil.calculateWaitingMins(
    //     bookingData.waitingTimesArray
    //   );
    //   workedMins = parseInt(
    //     (new Date() - new Date(bookingData.activity.pickUpTime)) / (1000 * 60)
    //   );
    // }
    // const timeData =
    //   professionalStatus === constantUtil.ARRIVED
    //     ? { "activity.arriveTime": new Date() }
    //     : professionalStatus === constantUtil.STARTED
    //     ? {
    //         "activity.pickUpTime": new Date(),
    //         "activity.isUserPickedUp": true,
    //       }
    //     : professionalStatus === constantUtil.ENDED
    //     ? {
    //         "activity.dropTime": new Date(),
    //         "activity.isUserDropped": true,
    //         "activity.workedMins": workedMins,
    //         "activity.waitingMins": waitingMins,
    //         "activity.endedBy": {
    //           "id": context.meta.professionalId.toString(),
    //           "userType": constantUtil.PROFESSIONAL,
    //         },
    //       }
    //     : "";
    let timeData = "",
      trackData = {},
      wayData = {};
    switch (professionalStatus) {
      case constantUtil.ARRIVED:
        timeData = {
          "activity.arriveTime": new Date(),
          "regionalData.arriveTime": helperUtil.toRegionalUTC(new Date()),
          "activity.arriveLatLng": context.params.latLng,
        };
        break;

      case constantUtil.STARTED:
        {
          timeData = {
            "activity.pickUpLatLng": context.params.latLng,
            "activity.pickUpTime": new Date(),
            "activity.isUserPickedUp": true,
            "regionalData.pickUpTime": helperUtil.toRegionalUTC(new Date()),
            "upcomingLocation.coordinates":
              bookingData?.dropLocation?.coordinates || [],
          };
          wayData = { "pickupWayData": context.params.pickupWayData };
        }
        break;

      case constantUtil.ENDED:
        {
          waitingMins = helperUtil.calculateWaitingMins(
            bookingData.waitingTimesArray
          );
          workedMins = parseInt(
            (new Date() - new Date(bookingData.activity.pickUpTime)) /
              (1000 * 60)
          );
          timeData = {
            "activity.dropLatLng": context.params.latLng,
            "activity.dropTime": new Date(),
            "activity.isUserDropped": true,
            "activity.workedMins": workedMins,
            "activity.waitingMins": waitingMins,
            "activity.endedBy": {
              "id": context.meta.professionalId.toString(),
              "userType": constantUtil.PROFESSIONAL,
            },
            "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
          };
          trackData = { "trackData": context.params?.trackData || {} };
          wayData = {
            "wayData": context.params.wayData,
            "snapWayData": context.params.snapWayData,
            "manualWayData": context.params.manualWayData,
          };
        }
        break;
    }
    const updateBookingData = await this.adapter.model.updateOne(
      {
        "_id": context.params.bookingId,
        "bookingStatus":
          professionalStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : professionalStatus === constantUtil.STARTED
            ? constantUtil.ARRIVED
            : professionalStatus === constantUtil.ENDED
            ? constantUtil.STARTED
            : "",
      },
      {
        "$set": {
          "hub": bookingData.professional?.vehicles[0]?.hub?.toString() || null,
          "bookingStatus": professionalStatus,
          // "wayData": context.params.wayData,
          // "snapWayData": context.params.snapWayData,
          // "manualWayData": context.params.manualWayData,
          ...wayData,
          "activity.rideStops": bookingData.activity.rideStops,
          "invoice.tollFareAmount": context.params?.tollFareAmount || 0,
          "invoice.airportFareAmount": context.params?.airportFareAmount || 0,
          ...timeData,
          // ...trackData,
        },
      }
    );
    if (!updateBookingData.nModified) {
      throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
    }
    if (
      professionalStatus === constantUtil.STARTED &&
      bookingData.invoice.couponApplied === true &&
      bookingData?.couponId
    ) {
      await this.broker.emit("admin.updateCouponCount", {
        "userId": bookingData.user._id.toString(),
        "couponId": bookingData?.couponId?._id.toString(),
        "actionType": constantUtil.STARTED,
      });
    }
    // bookingData = await this.adapter.model
    //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    //   .populate("user", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "trustedContacts": 0,
    //     "cards": 0,
    //   })
    //   .populate("professional", {
    //     "password": 0,
    //     "cards": 0,
    //     "bankDetails": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("category")
    //   .populate("security")
    //   .populate("officer", { "password": 0, "preferences": 0 })
    //   .populate("couponId")
    //   .populate("coorperate")
    //   .populate("couponId")
    //   .lean();
    // // bookingData = bookingData.toJSON();

    // Get booking Details
    bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details

    vehicleCategoryData =
      vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
      context.params?.afterRideWaitingMinsWithGracePeriod || 0;

    if (professionalStatus === constantUtil.ENDED) {
      const inputConstMeterReadingType =
        context.params?.meterReadingType || constantUtil.CONST_SNAPWAY;
      let meterReadingType = context.params.meterReadingType;
      const liveMeterMinInterruptThresholdTime =
        generalSettings.data.liveMeterMinInterruptThresholdTime;
      const liveMeterMaxInterruptThresholdTime =
        generalSettings.data.liveMeterMaxInterruptThresholdTime;
      const isEnableLiveMeter = generalSettings.data.isEnableLiveMeter;
      //#region intercity ride
      traveledDataSet["pickupDistance"] = 0;
      traveledDataSet["pickupTimeDuration"] = 0;
      traveledDataSet["dropDistance"] = 0;
      traveledDataSet["dropTimeDuration"] = 0;

      const distanceTypeUnitValue =
        (bookingData?.distanceUnit ?? constantUtil.KM).toUpperCase() ===
        constantUtil.KM
          ? constantUtil.KM_VALUE
          : constantUtil.MILES_VALUE;

      if (
        bookingData.serviceCategory ===
        constantUtil.CONST_INTERCITYRIDE.toLowerCase()
      ) {
        //---- Pickup Time
        traveledDataSet["pickupTimeDuration"] = parseInt(
          (new Date(bookingData?.activity?.pickUpTime) -
            new Date(bookingData?.activity?.acceptTime)) /
            1000
        );
        if (traveledDataSet["pickupTimeDuration"] < 60) {
          traveledDataSet["pickupTimeDuration"] = 60;
        }
        //---- Pickup Distance
        // const distanceData = await googleApiUtil.distancematrix({
        //   "lat": bookingData.professional.location.coordinates[1],
        //   "lng": bookingData.professional.location.coordinates[0],
        // });
        // traveledDataSet["pickupDistance"] = distanceData?.distance || 0;
        const pickupLatLngData = [];
        pickupLatLngData.push(bookingData.activity.acceptLatLng);
        pickupLatLngData.push(bookingData.activity.arriveLatLng);
        // const distanceData = await helperUtil.snapWayPath(pickupLatLngData);
        // const distanceData = await helperUtil.snapWayPath([
        //   {
        //     "lat": -28.6717216,
        //     "lng": -49.3777503,
        //     // "timeStamp": 1642342745,
        //     // "isStop": false,
        //   },
        //   {
        //     "lat": -28.6717997,
        //     "lng": -49.377989,
        //     // "timeStamp": 1642344362,
        //     // "isStop": false,
        //   },
        // ]);
        const mapDataPickup = await googleApiUtil.directions({
          "clientId": clientId,
          "from":
            bookingData.activity.acceptLatLng.lat +
            "," +
            bookingData.activity.acceptLatLng.lng,
          "to":
            bookingData.activity.pickUpLatLng.lat +
            "," +
            bookingData.activity.pickUpLatLng.lng,
        });
        traveledDataSet["pickupDistance"] =
          mapDataPickup.data?.routes[0]?.legs[0]?.distance?.value || 0;
        //-------- For RoundTrip Calculation Start --------------
        const mapDataDrop = await googleApiUtil.directions({
          "clientId": clientId,
          "from":
            bookingData.activity.acceptLatLng.lat +
            "," +
            bookingData.activity.acceptLatLng.lng,
          "to": context.params.latLng.lat + "," + context.params.latLng.lng,
        });
        traveledDataSet["dropDistance"] =
          mapDataDrop.data?.routes[0]?.legs[0]?.distance?.value || 0;
        //---- Pickup Time
        traveledDataSet["dropTimeDuration"] = parseInt(
          (new Date(bookingData?.activity?.pickUpTime) -
            new Date(bookingData?.activity?.dropTime)) /
            1000
        );
        // traveledDataSet["dropTimeDuration"] =
        //   parseInt(
        //     (
        //       (traveledDataSet["dropDistance"] / distanceTypeUnitValue) %
        //       50
        //     ).toFixed(2)
        //   ) * 60; //(time = distance ÷ speed), 50 --> Speed
        //-------- For RoundTrip Calculation End --------------;
      }
      //#endregion intercity ride

      //#region Traveled Time
      traveledDataSet["timeDuration"] = parseInt(
        (new Date(bookingData.activity.dropTime) -
          new Date(bookingData.activity.pickUpTime)) /
          1000
      );

      //-------- For Live Meter Start ------------
      if (
        isEnableLiveMeter &&
        inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
      ) {
        // bookingData["estimation"]["time"] = parseInt(
        //   context.params.traveledTime
        // );
        if (
          parseInt(context.params.traveledTime) >=
            parseInt(traveledDataSet["timeDuration"]) -
              liveMeterMinInterruptThresholdTime &&
          parseInt(context.params.traveledTime) <=
            parseInt(traveledDataSet["timeDuration"]) +
              liveMeterMaxInterruptThresholdTime
        ) {
          traveledDataSet["timeDuration"] = parseInt(
            context.params.traveledTime
          );
        } else {
          meterReadingType = constantUtil.CONST_TIMEINTERRUPT;
        }
      }
      //-------- For Live Meter Start ------------
      if (traveledDataSet["timeDuration"] < 60) {
        traveledDataSet["timeDuration"] = 60;
      }
      //#endregion Traveled Time

      let finalData = {};
      let calculatedData = {};
      const finalWayPolydata = [];
      snapWayData =
        context.params.snapWayData &&
        Array.isArray(context.params.snapWayData) &&
        context.params.snapWayData.length > 1
          ? context.params.snapWayData
          : [];
      manualWayData =
        context.params.manualWayData &&
        Array.isArray(context.params.manualWayData) &&
        context.params.manualWayData.length > 1
          ? context.params.manualWayData
          : [];

      //#region Traveled Distance
      traveledDataSet["km"] = 0;
      const liveMeterMinInterruptThresholdDistance =
        generalSettings.data.liveMeterMinInterruptThresholdDistance;
      const liveMeterMaxInterruptThresholdDistance =
        generalSettings.data.liveMeterMaxInterruptThresholdDistance;
      if (generalSettings.data.distanceCalculateType === constantUtil.ROUTE) {
        if (
          snapWayData &&
          snapWayData !== null &&
          snapWayData !== undefined &&
          Array.isArray(snapWayData) &&
          snapWayData.length > 0
        ) {
          const finalSnapWayData = await helperUtil.snapWayPath(snapWayData);
          snapErrorData = finalSnapWayData.snapErrorData;
          wayPointsSet = finalSnapWayData.finalSnapWayData;
          finalWayDataSet = finalSnapWayData.finalWayDataSet;
          traveledDataSet["km"] = finalSnapWayData.distance;
          //-------- For Live Meter Start ------------
          if (
            isEnableLiveMeter &&
            inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
          ) {
            // bookingData["estimation"]["distance"] = parseInt(
            //   context.params.traveledDistance
            // );
            if (
              parseInt(context.params.traveledDistance) >=
                parseInt(traveledDataSet["km"]) -
                  liveMeterMinInterruptThresholdDistance &&
              parseInt(context.params.traveledDistance) <=
                parseInt(traveledDataSet["km"]) +
                  liveMeterMaxInterruptThresholdDistance
            ) {
              traveledDataSet["km"] = parseFloat(
                context.params.traveledDistance
              );
            } else {
              meterReadingType = constantUtil.CONST_DISTANCEINTERRUPT;
            }
          }
          //-------- For Live Meter Start ------------
          if (snapErrorData != {}) {
            calculatedData = await helperUtil.calculateEstimateAmount({
              "clientId": clientId,
              "bookingData": bookingData,
              //
              "tollFareAmount": context.params?.tollFareAmount || 0,
              "airportFareAmount": context.params?.airportFareAmount || 0,
              //
              "estimationDistance": traveledDataSet["km"],
              "estimationTime": traveledDataSet["timeDuration"],
              "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              "calculatedDropDistance": traveledDataSet["dropDistance"],
              "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              "beforeRideAssistanceCareProvidesMins": parseInt(
                context.params.beforeRideAssistanceCareProvidesMins || 0
              ),
              "afterRideAssistanceCareProvidesMins": parseInt(
                context.params.afterRideAssistanceCareProvidesMins || 0
              ),
            });

            finalData = await helperUtil.endTripCalculation({
              "bookingData": bookingData,
              "paymentOption": bookingData.payment.option,
              "calculatedData": calculatedData,
              "estimationDistance": traveledDataSet["km"],
              "estimationTime": traveledDataSet["timeDuration"],
              "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              "calculatedDropDistance": traveledDataSet["dropDistance"],
              "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            });
          } else {
            if (
              manualWayData &&
              manualWayData !== null &&
              manualWayData !== undefined &&
              Array.isArray(manualWayData) &&
              manualWayData.length > 1
            ) {
              const finalMaunalCalc =
                await helperUtil.manualWayDistanceCalculate(manualWayData);
              wayPointsSet = finalMaunalCalc.finalWayPoint;
              finalWayDataSet = [finalMaunalCalc.finalWayPoint];
              traveledDataSet["km"] = finalMaunalCalc.distance;
            } else {
              traveledDataSet["km"] =
                manualWayData.length == 1
                  ? context.params.manualDistance
                    ? context.params.manualDistance
                    : 0
                  : bookingData.estimation.distance;
            }
            //-------- For Live Meter Start ------------
            if (
              isEnableLiveMeter &&
              inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
            ) {
              // bookingData["estimation"]["distance"] = parseInt(
              //   context.params.traveledDistance
              // );
              if (
                parseInt(context.params.traveledDistance) >=
                  parseInt(traveledDataSet["km"]) -
                    liveMeterMinInterruptThresholdDistance &&
                parseInt(context.params.traveledDistance) <=
                  parseInt(traveledDataSet["km"]) +
                    liveMeterMaxInterruptThresholdDistance
              ) {
                traveledDataSet["km"] = parseFloat(
                  context.params.traveledDistance
                );
              } else {
                meterReadingType = constantUtil.CONST_DISTANCEINTERRUPT;
              }
            }
            //-------- For Live Meter End ------------
            calculatedData = await helperUtil.calculateEstimateAmount({
              "clientId": clientId,
              "bookingData": bookingData,
              //
              "tollFareAmount": context.params?.tollFareAmount || 0,
              "airportFareAmount": context.params?.airportFareAmount || 0,
              //
              "estimationDistance": traveledDataSet["km"],
              "estimationTime": traveledDataSet["timeDuration"],
              "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              "calculatedDropDistance": traveledDataSet["dropDistance"],
              "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              "beforeRideAssistanceCareProvidesMins": parseInt(
                context.params.beforeRideAssistanceCareProvidesMins || 0
              ),
              "afterRideAssistanceCareProvidesMins": parseInt(
                context.params.afterRideAssistanceCareProvidesMins || 0
              ),
            });
            finalData = await helperUtil.endTripCalculation({
              "bookingData": bookingData,
              "paymentOption": bookingData.payment.option,
              "calculatedData": calculatedData,
              "estimationDistance": traveledDataSet["km"],
              "estimationTime": traveledDataSet["timeDuration"],
              "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              "calculatedDropDistance": traveledDataSet["dropDistance"],
              "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            });
          }
        } else {
          if (
            manualWayData &&
            manualWayData !== null &&
            manualWayData !== undefined &&
            Array.isArray(manualWayData) &&
            manualWayData.length > 1
          ) {
            const finalMaunalCalc = await helperUtil.manualWayDistanceCalculate(
              manualWayData
            );
            wayPointsSet = finalMaunalCalc.finalWayPoint;
            finalWayDataSet = [finalMaunalCalc.finalWayPoint];
            traveledDataSet["km"] = finalMaunalCalc.distance;
          } else {
            traveledDataSet["km"] =
              manualWayData.length == 1
                ? context.params.manualDistance
                  ? context.params.manualDistance
                  : 0
                : bookingData.estimation.distance;
          }
          //-------- For Live Meter Start ------------
          if (
            isEnableLiveMeter &&
            inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
          ) {
            // bookingData["estimation"]["distance"] = parseInt(
            //   context.params.traveledDistance
            // );
            if (
              parseInt(context.params.traveledDistance) >=
                parseInt(traveledDataSet["km"]) -
                  liveMeterMinInterruptThresholdDistance &&
              parseInt(context.params.traveledDistance) <=
                parseInt(traveledDataSet["km"]) +
                  liveMeterMaxInterruptThresholdDistance
            ) {
              traveledDataSet["km"] = parseFloat(
                context.params.traveledDistance
              );
            } else {
              meterReadingType = constantUtil.CONST_DISTANCEINTERRUPT;
            }
          }
          //-------- For Live Meter End ------------
          calculatedData = await helperUtil.calculateEstimateAmount({
            "clientId": clientId,
            "bookingData": bookingData,
            //
            "tollFareAmount": context.params?.tollFareAmount || 0,
            "airportFareAmount": context.params?.airportFareAmount || 0,
            //
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            "beforeRideAssistanceCareProvidesMins": parseInt(
              context.params.beforeRideAssistanceCareProvidesMins || 0
            ),
            "afterRideAssistanceCareProvidesMins": parseInt(
              context.params.afterRideAssistanceCareProvidesMins || 0
            ),
          });
          finalData = await helperUtil.endTripCalculation({
            "bookingData": bookingData,
            "paymentOption": bookingData.payment.option,
            "calculatedData": calculatedData,
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
          });
        }
      } else if (
        generalSettings.data.distanceCalculateType === constantUtil.MANUAL
      ) {
        if (
          manualWayData &&
          manualWayData !== null &&
          manualWayData !== undefined &&
          Array.isArray(manualWayData) &&
          manualWayData.length > 1
        ) {
          const finalMaunalCalc = await helperUtil.manualWayDistanceCalculate(
            manualWayData
          );
          wayPointsSet = finalMaunalCalc.finalWayPoint;
          finalWayDataSet = [finalMaunalCalc.finalWayPoint];
          traveledDataSet["km"] = finalMaunalCalc.distance;
        } else {
          traveledDataSet["km"] =
            manualWayData.length == 1
              ? context.params.manualDistance
                ? context.params.manualDistance
                : 0
              : bookingData.estimation.distance;
        }
        //-------- For Live Meter Start ------------
        if (
          isEnableLiveMeter &&
          inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
        ) {
          // bookingData["estimation"]["distance"] = parseInt(
          //   context.params.traveledDistance
          // );
          if (
            parseInt(context.params.traveledDistance) >=
              parseInt(traveledDataSet["km"]) -
                liveMeterMinInterruptThresholdDistance &&
            parseInt(context.params.traveledDistance) <=
              parseInt(traveledDataSet["km"]) +
                liveMeterMaxInterruptThresholdDistance
          ) {
            traveledDataSet["km"] = parseFloat(context.params.traveledDistance);
          } else {
            meterReadingType = constantUtil.CONST_DISTANCEINTERRUPT;
          }
        }
        //-------- For Live Meter End ------------
        calculatedData = await helperUtil.calculateEstimateAmount({
          "clientId": clientId,
          "bookingData": bookingData,
          //
          "tollFareAmount": context.params?.tollFareAmount || 0,
          "airportFareAmount": context.params?.airportFareAmount || 0,
          //
          "estimationDistance": traveledDataSet["km"],
          "estimationTime": traveledDataSet["timeDuration"],
          "calculatedPickupDistance": traveledDataSet["pickupDistance"],
          "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
          "calculatedDropDistance": traveledDataSet["dropDistance"],
          "calculatedDropTime": traveledDataSet["dropTimeDuration"],
          "beforeRideAssistanceCareProvidesMins": parseInt(
            context.params.beforeRideAssistanceCareProvidesMins || 0
          ),
          "afterRideAssistanceCareProvidesMins": parseInt(
            context.params.afterRideAssistanceCareProvidesMins || 0
          ),
        });
        finalData = await helperUtil.endTripCalculation({
          "bookingData": bookingData,
          "paymentOption": bookingData.payment.option,
          "calculatedData": calculatedData,
          "estimationDistance": traveledDataSet["km"],
          "estimationTime": traveledDataSet["timeDuration"],
          "calculatedPickupDistance": traveledDataSet["pickupDistance"],
          "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
          "calculatedDropDistance": traveledDataSet["dropDistance"],
          "calculatedDropTime": traveledDataSet["dropTimeDuration"],
        });
      }
      //#endregion Traveled Distance

      let paidCalcType = constantUtil.EQUALTOPAID;
      if (finalData.payableAmount < bookingData.invoice.estimationAmount) {
        paidCalcType = constantUtil.LESSTHENPAID;
      } else if (
        finalData.payableAmount > bookingData.invoice.estimationAmount
      ) {
        paidCalcType = constantUtil.GREATERTHENPAID;
      }
      const staticMapData = {
        "clientId": clientId,
        "km": traveledDataSet["km"],
        "from": directionsData["from1"],
        "to": directionsData["to1"],
        "waypoints": "",
      };

      // const wayValueSet = [];
      // if (finalWayPolydata && finalWayPolydata.length > 0) {
      //   const wayPointsValue = await helperUtil.randomslice(
      //     finalWayPolydata,
      //     30
      //   );
      //   await wayPointsValue.forEach((w) => {
      //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
      //   });
      //   staticMapData["waypoints"] = wayValueSet.join("");
      // } else {
      //   const wayPointsValue = await helperUtil.randomslice(
      //     manualWayData,
      //     30
      //   );
      //   await wayPointsValue.forEach((w) => {
      //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
      //   });
      //   staticMapData["waypoints"] = wayValueSet.join("");
      // }
      const wayValueSet = [];
      if (finalWayPolydata && finalWayPolydata.length > 0) {
        const wayPointsValue = await helperUtil.randomslice(
          finalWayPolydata,
          30
        );
        await wayPointsValue.forEach((w) => {
          wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
        });
        // staticMapData["waypoints"] = wayValueSet.join("");
      } else {
        const wayPointsValue = await helperUtil.randomslice(manualWayData, 30);
        await wayPointsValue.forEach((w) => {
          wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
        });
        // staticMapData["waypoints"] = wayValueSet.join("");
      }
      staticMapData["waypoints"] = wayValueSet.join("");
      const value = await helperUtil.getStaticImage(staticMapData);
      const config = {
        "method": "get",
        "url": value,
        "headers": {},
        "responseType": "arraybuffer",
      };

      const imageData = await axios(config);
      let finalImageData = await this.broker.emit(
        "admin.uploadStaticMapImageInSpaces",
        {
          "files": imageData.data,
          "fileName": `static${directionsData["from1"]}Map${directionsData["to1"]}Image`,
        }
      );
      finalImageData = finalImageData && finalImageData[0];
      const updateBookingData = await this.adapter.model.updateOne(
        {
          "_id": context.params.bookingId,
          "bookingStatus": constantUtil.ENDED,
        },
        {
          "hub": bookingData.professional?.vehicles[0]?.hub?.toString() || null,
          "finalWayData": finalWayDataSet,
          "manualDistance": context.params.manualDistance || 0,
          "manualWayData": context.params.manualWayData || [],
          "totalTollPassed": context.params.totalTollPassed || 0,
          "passedTollList": context.params.passedTollList || [],
          "invoice.isMinimumChargeApplied": finalData["isMinimumChargeApplied"],
          "invoice.serviceTax": finalData["serviceTax"],
          "invoice.tollFareAmount": finalData["tollFareAmount"],
          "invoice.airportFareAmount": finalData["airportFareAmount"],
          "invoice.amountInSite": finalData["amountInSite"],
          "invoice.amountInDriver": finalData["amountInDriver"],
          "invoice.travelCharge": finalData["travelCharge"],
          "invoice.distanceFare": finalData["distanceFare"],
          "invoice.timeFare": finalData["timeFare"],
          "invoice.siteCommissionWithoutTax":
            finalData["siteCommissionWithoutTax"],
          "invoice.siteCommission": finalData["siteCommission"],
          "invoice.professionalCommision": finalData["professionalCommision"],
          "invoice.professionalCommisionWithoutTips":
            finalData["professionalCommision"],
          "invoice.estimationPayableAmount": finalData["estimationAmount"],
          "invoice.payableAmount": finalData["payableAmount"],
          "invoice.couponAmount": finalData["couponAmount"],
          "invoice.professionalTolerenceAmount":
            finalData["professionalTolerenceAmount"],
          "estimation.distance": traveledDataSet["km"],
          "estimation.time": traveledDataSet["timeDuration"],
          "estimation.pickupDistance": traveledDataSet["pickupDistance"],
          "estimation.pickupTime": traveledDataSet["pickupTimeDuration"],
          "estimation.dropDistance": traveledDataSet["dropDistance"],
          "estimation.dropTime": traveledDataSet["dropTimeDuration"],
          "bookedEstimation.distance": bookingData.estimation.distance,
          "bookedEstimation.time": bookingData.estimation.time,
          "bookedEstimation.pickupDistance":
            bookingData.estimation.pickupDistance,
          "bookedEstimation.pickupTime": bookingData.estimation.pickupTime,
          "rideMapRouteImage": finalImageData,
          "payment.paid": true,
          "encodedPolyline": finalEncodedPolyline,
          "mapErrorData": "",
          "snapErrorData": snapErrorData,
          "originalWayData": wayPointsSet,
          "distanceCalculateType": generalSettings.data.distanceCalculateType,
          "invoice.peakFareCharge": finalData["peakFareCharge"],
          "invoice.nightFareCharge": finalData["nightFareCharge"],
          "invoice.multiStopCharge": finalData["multiStopCharge"],
          "invoice.waitingCharge": finalData["waitingCharge"],
          "invoice.beforeRideWaitingCharge":
            finalData["beforeRideWaitingCharge"],
          "invoice.beforeRideWaitingMins": finalData["beforeRideWaitingMins"],
          "invoice.afterRideWaitingCharge": finalData["afterRideWaitingCharge"],
          "invoice.afterRideWaitingMins": finalData["afterRideWaitingMins"],
          "meterReadingType": meterReadingType,
          "liveMeter": {
            "isEnableLiveMeter": isEnableLiveMeter,
            "traveledDistance": context.params.traveledDistance,
            "traveledTime": context.params.traveledTime,
          },
          "invoice.intercityPickupTimeFare":
            finalData["intercityPickupTimeFare"],
          "invoice.intercityPickupDistanceFare":
            finalData["intercityPickupDistanceFare"],
          "invoice.intercityPickupCharge": finalData["intercityPickupCharge"],
          "invoice.intercityDropCharge": finalData["intercityDropCharge"],
          "invoice.roundingAmount": finalData["roundingAmount"],
          "invoice.discountAmount": finalData["discountAmount"],
          //
          "invoice.beforeRideAssistanceCareProvidesMins": parseInt(
            context.params.beforeRideAssistanceCareProvidesMins || 0
          ),
          "invoice.beforeRideAssistanceCareAmount":
            finalData["beforeRideAssistanceCareAmount"],
          "invoice.afterRideAssistanceCareProvidesMins": parseInt(
            context.params.afterRideAssistanceCareProvidesMins || 0
          ),
          "invoice.afterRideAssistanceCareAmount":
            finalData["afterRideAssistanceCareAmount"],
          "invoice.assistanceCareAmount": finalData["assistanceCareAmount"],
        }
      );
      if (!updateBookingData.nModified) {
        throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
      }
      this.broker.emit("professional.updatependingReview", {
        "professionalId": bookingData.professional._id.toString(),
        "bookingId": context.params.bookingId,
        "lastCurrencyCode": bookingData.currencyCode,
      });

      //#region Franchising (site commition to Hub)
      if (generalSettings.data.isEnableFranchising) {
        let hubData = await this.broker.emit("admin.updateHubSiteCommission", {
          // "professionalId": bookingData.professional?._id?.toString(),
          "bookingId": bookingData?._id?.toString(),
          "refBookingId": bookingData.bookingId,
          "hubId": bookingData.professional?.vehicles[0]?.hub?.toString(),
          "siteCommissionWithoutTax": finalData["siteCommissionWithoutTax"],
        });
        hubData = hubData && hubData[0];
        console.log(JSON.stringify(hubData));
        await this.adapter.model.updateOne(
          {
            "_id": mongoose.Types.ObjectId(context.params.bookingId),
            "bookingStatus": constantUtil.ENDED,
          },
          {
            "invoice.hubSiteCommission": hubData?.hubSiteCommission || 0,
            "invoice.hubSiteCommissionPercentage":
              hubData?.hubSiteCommissionPercentage || 0,
          }
        );
      }
      //#endregion Franchising (site commition to Hub)

      // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
      if (bookingData.user) {
        // pending amount
        if (bookingData.invoice.pendingPaymentAmount > 0) {
          await this.broker.emit("user.updateWalletForPendingAmount", {
            "userId": bookingData.user._id.toString(),
          });
        }
        // upate review
        this.broker.emit("user.updatependingReview", {
          "userId": bookingData.user._id.toString(),
          "bookingId": context.params.bookingId,
          "lastCurrencyCode": bookingData.currencyCode,
        });
        if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
          this.broker.emit("user.updateWalletAmountTripEnded", {
            "userId": bookingData.user._id.toString(),
            "amount": parseFloat(finalData.payableAmount.toFixed(2)),
            "transStatus": paidCalcType,
            "paymentMethod": bookingData.payment.option,
          });
          const userAvailableAmount =
            parseFloat(bookingData?.user?.wallet?.availableAmount) +
            parseFloat(bookingData?.user?.wallet?.freezedAmount);
          this.broker.emit("transaction.newBookingTransaction", {
            "clientId": clientId,
            "userId": bookingData.user._id.toString(),
            "amount": finalData.payableAmount.toFixed(2),
            "previousBalance": userAvailableAmount,
            "currentBalance":
              parseFloat(userAvailableAmount) -
              parseFloat(finalData.payableAmount.toFixed(2)),
            "refBookingId": bookingData?.bookingId,
          });
        }
        if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
          // take amount
          await this.broker.emit("transaction.tripEndCardCharge", {
            "clientId": clientId,
            "userId": bookingData.user._id.toString(),
            "amount": parseFloat(finalData.payableAmount.toFixed(2)),
            "cardId": bookingData.payment.card.cardId.toString(),
            "bookingId": bookingData._id.toString(),
            "refBookingId": bookingData.bookingId,
            "paymentInitId": bookingData.paymentInitId,
            "paymentInitAmount": bookingData.paymentInitAmount,
          });
        }
      }

      // professsional transaction
      if (generalSettings.data.paymentSection === constantUtil.CASHANDWALLET) {
        if (
          bookingData.payment.option === constantUtil.PAYMENTWALLET ||
          bookingData.payment.option === constantUtil.PAYMENTCARD
        ) {
          this.broker.emit("professional.updateWalletAmountTripEnded", {
            "clientId": clientId,
            "professionalId": bookingData.professional._id.toString(),
            "amount": parseFloat(finalData.professionalCommision.toFixed(2)),
            "paymentMethod": bookingData.payment.option,
          });
          this.broker.emit("transaction.professionalBookingEndTransaction", {
            "clientId": clientId,
            "professionalId": bookingData.professional._id.toString(),
            "amount": finalData.professionalCommision.toFixed(2),
            "bookingId": bookingData._id.toString(),
            "refBookingId": bookingData.bookingId,
            "previousBalance":
              bookingData?.professional?.wallet?.availableAmount,
            "currentBalance":
              parseFloat(bookingData?.professional?.wallet?.availableAmount) +
              parseFloat(finalData.professionalCommision.toFixed(2)),
          });
        }
        if (
          bookingData.payment.option === constantUtil.PAYMENTCASH ||
          bookingData.payment.option === constantUtil.CONST_PIX ||
          bookingData.payment.option === constantUtil.CONST_POS ||
          bookingData.payment.option === constantUtil.PAYMENTCREDIT //=>this for corporate beacuse in corpporate payment options use paymentcredit key word
        ) {
          // make transaction
          let paymentType = constantUtil.DEBIT;
          let professionalCommision = (
            parseFloat(finalData.payableAmount) -
            parseFloat(finalData.professionalCommision)
          ).toFixed(2);
          if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
            if (
              generalSettings?.data?.isEnableCorporateRideWalletPay &&
              bookingData.coorperate
            ) {
              const updateCorporateRideWalletPay = "";
              // let corporateData = await
              let finalPayableAmount =
                parseFloat(finalData.payableAmount.toFixed(2)) -
                parseFloat(bookingData?.invoice?.payableAmount || 0);
              if (finalPayableAmount > 0) {
                await this.broker.emit("admin.updateWalletAmountTripEnded", {
                  "clientId": clientId,
                  "corporateId": bookingData.coorperate._id.toString(),
                  "amount": finalPayableAmount,
                  "paymentType": constantUtil.DEBIT,
                  "paymentMethod": bookingData.payment.option,
                  "bookingId": bookingData._id?.toString(),
                  "refBookingId": bookingData.bookingId,
                  "transactionType": constantUtil.BOOKINGCHARGE,
                  "transactionReason":
                    BOOKING_PAID + " / " + bookingData.bookingId,
                });
              } else if (finalPayableAmount < 0) {
                finalPayableAmount =
                  parseFloat(bookingData?.invoice?.payableAmount || 0) -
                  parseFloat(finalData.payableAmount.toFixed(2));
                await this.broker.emit("admin.updateWalletAmountTripEnded", {
                  "clientId": clientId,
                  "corporateId": bookingData.coorperate?._id?.toString(),
                  "amount": finalPayableAmount,
                  "paymentType": constantUtil.CREDIT,
                  "paymentMethod": bookingData.payment.option,
                  "bookingId": bookingData._id?.toString(),
                  "refBookingId": bookingData.bookingId,
                  "transactionType": constantUtil.BOOKINGCHARGE,
                  "transactionReason":
                    REFUND_TO_WALLET + " / " + bookingData.bookingId,
                });
              }
              // corporateData = corporateData && corporateData[0];
              // //-------- Transaction Entry Start ---------------------
              // if (corporateData) {
              //   this.broker.emit(
              //     "transaction.corporateRideWalletPayTransaction",
              //     {
              //       "bookingId": context.params.bookingId?.toString(),
              //       "refBookingId": bookingData.bookingId,
              //       "corporateId": corporateData?._id?.toString(),
              //       "corporateData": corporateData.data,
              //       "amount": finalData.payableAmount,
              //       "previousBalance": corporateData?.data?.amount || 0,
              //       "currentBalance":
              //         parseFloat(corporateData?.data?.amount || 0) -
              //         parseFloat(finalData.payableAmount || 0),
              //     }
              //   );
              // }
              // //-------- Transaction Entry End ---------------------
            }
            if (
              generalSettings.data
                .isProfessionalsCorporateSiteCommissionCreditToWallet
            ) {
              paymentType = constantUtil.CREDIT; //need to Revert
              professionalCommision =
                finalData.professionalCommision.toFixed(2); //need to Revert
            } else {
              // ----------- Zayride Start ------------
              paymentType = constantUtil.DEBIT; //need to Revert
              professionalCommision = (
                parseFloat(finalData.payableAmount) -
                parseFloat(finalData.professionalCommision)
              ).toFixed(2); //need to Revert
              // ----------- Zayride End ------------
            }
          }
          if (finalData.payableAmount > finalData.professionalCommision) {
            this.broker.emit("professional.updateWalletAmountTripEnded", {
              "clientId": clientId,
              "professionalId": bookingData.professional._id.toString(),
              "amount": professionalCommision,
              "paymentMethod": bookingData.payment.option,
            });
            this.broker.emit(
              "transaction.professionalSiteCommissionTransaction",
              {
                "clientId": clientId,
                "professionalId": bookingData.professional._id.toString(),
                "amount": professionalCommision,
                "bookingId": bookingData._id.toString(),
                "refBookingId": bookingData.bookingId,
                "paymentType": paymentType,
                "previousBalance":
                  bookingData?.professional?.wallet?.availableAmount,
                "currentBalance":
                  parseFloat(
                    bookingData?.professional?.wallet?.availableAmount
                  ) + parseFloat(professionalCommision),
              }
            );
          }

          if (finalData.payableAmount < finalData.professionalCommision) {
            // in cash payment user paied amount for a ride is lesser than professioanl commission so menas some coupons or some discount is applied so have reimpersed this differce amount

            this.broker.emit("professional.updateWalletAmountTripEnded", {
              "clientId": clientId,
              "professionalId": bookingData.professional._id.toString(),
              "amount": parseFloat(
                finalData.professionalCommision - finalData.payableAmount
              ),
              "paymentMethod": constantUtil.PAYMENTWALLET,
            });
            this.broker.emit(
              "transaction.professionalTolerenceAmountTransaction",
              {
                "clientId": clientId,
                "professionalId": bookingData.professional._id.toString(),
                "amount": parseFloat(
                  finalData.professionalCommision - finalData.payableAmount
                ).toFixed(2),
                "bookingId": bookingData._id.toString(),
                "refBookingId": bookingData.bookingId,
                "paymentType": paymentType,
                "previousBalance":
                  bookingData?.professional?.wallet?.availableAmount,
                "currentBalance":
                  parseFloat(
                    bookingData?.professional?.wallet?.availableAmount
                  ) +
                  parseFloat(
                    finalData.professionalCommision - finalData.payableAmount
                  ),
              }
            );
          }
        }
      }
      checkProfessionalRideCount = await this.adapter.model.count({
        "professional": mongoose.Types.ObjectId(
          bookingData.professional._id.toString()
        ),
        "bookingStatus": constantUtil.ENDED,
      });
      if (bookingData.guestType === constantUtil.USER && bookingData.user) {
        checkUserRideCount = await this.adapter.model.count({
          "user": mongoose.Types.ObjectId(bookingData.user._id.toString()),
          "bookingStatus": constantUtil.ENDED,
        });
      }

      // invite and earn
      const inviteAndEarnSettings = await storageUtil.read(
        constantUtil.INVITEANDEARN
      );
      // user
      if (bookingData?.user?.referredBy) {
        if (
          parseInt(checkUserRideCount) ===
          inviteAndEarnSettings?.data?.forUser?.rideCount
        ) {
          this.broker.emit("user.inviteAndEarn", {
            "clientId": clientId,
            "joinerId": bookingData.user._id.toString(),
          });
        } else if (
          parseInt(checkUserRideCount) <
          inviteAndEarnSettings?.data?.forUser?.rideCount
        ) {
          this.broker.emit("user.updateJoinerRideCount", {
            "clientId": clientId,
            "joinerId": bookingData.user._id.toString(),
          });
        }
        if (inviteAndEarnSettings?.data?.forUser?.isEnableReferralRideEarning) {
          const inviterRideCommission =
            ((finalData["travelCharge"] + bookingData.invoice.baseFare) / 100) *
            (inviteAndEarnSettings?.data?.forUser
              ?.referralRideEarningPercentage || 0);
          await this.broker.emit("user.inviterRideEarnings", {
            "clientId": clientId,
            "inviterUniqueCode": bookingData?.user?.referredBy,
            // "joinerId": bookingData.user._id.toString(),
            "amount": inviterRideCommission.toFixed(2),
            "bookingId": bookingData?._id?.toString(),
            "refBookingId": bookingData.bookingId,
            "transactionReason":
              INFO_INCENTIVE_CREDIT +
              " / " +
              INFO_REFERREL_CHARGE +
              " / " +
              bookingData.bookingId,
          });
        }
      }

      //professional
      if (bookingData?.professional?.referredBy) {
        if (
          parseInt(checkProfessionalRideCount) ===
          inviteAndEarnSettings?.data?.forProfessional?.rideCount
        ) {
          this.broker.emit("professional.inviteAndEarn", {
            "clientId": clientId,
            "joinerId": bookingData.professional._id.toString(),
          });
        } else if (
          parseInt(checkProfessionalRideCount) <
          inviteAndEarnSettings?.data?.forProfessional?.rideCount
        ) {
          this.broker.emit("professional.updateJoinerRideCount", {
            "clientId": clientId,
            "joinerId": bookingData.professional._id.toString(),
          });
        }
        if (
          inviteAndEarnSettings?.data?.forProfessional
            ?.isEnableReferralRideEarning
        ) {
          const inviterRideCommission =
            ((finalData["travelCharge"] + bookingData.invoice.baseFare) / 100) *
            (inviteAndEarnSettings?.data?.forProfessional
              ?.referralRideEarningPercentage || 0);
          await this.broker.emit("professional.inviterRideEarnings", {
            "clientId": clientId,
            "inviterUniqueCode": bookingData?.professional?.referredBy,
            // "joinerId": bookingData.professional._id.toString(),
            "amount": inviterRideCommission.toFixed(2),
            "bookingId": bookingData?._id?.toString(),
            "refBookingId": bookingData.bookingId,
            "transactionReason":
              INFO_INCENTIVE_CREDIT +
              " / " +
              INFO_REFERREL_CHARGE +
              " / " +
              bookingData.bookingId,
          });
        }
      }

      // bookingData = await this.adapter.model
      //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      //   .populate("user", {
      //     "password": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //     "cards": 0,
      //   })
      //   .populate("professional", {
      //     "password": 0,
      //     "cards": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //   })
      //   .populate("category")
      //   .populate("security")
      //   .populate("officer", { "password": 0, "preferences": 0 })
      //   .populate("coorperate")
      //   .populate("couponId")
      //   .lean();

      // Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      }); // Get booking Details

      // bookingData = bookingData.toJSON();
      vehicleCategoryData = await storageUtil.read(
        constantUtil.VEHICLECATEGORY
      );
      if (!vehicleCategoryData) {
        throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
      }
      // serviceCategoryData = await this.broker.emit("category.getCategoryById", {
      //   "id": bookingData.category.toString(),
      // });
      // serviceCategoryData = serviceCategoryData[0];

      vehicleCategoryData =
        vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage;
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
      bookingData["imageURLPath"] =
        generalSettings.data.spaces.spacesBaseUrl +
        "/" +
        generalSettings.data.spaces.spacesObjectName;
      // bookingData.isNeedSecurityImageUpload =
      //   serviceCategoryData.isNeedSecurityImageUpload;

      // send mail
      if (generalSettings.data.emailConfigurationEnable === true) {
        // SEND TO PROFESSIONAL
        if (
          bookingData?.professional?.email &&
          bookingData?.professional?.isEmailVerified
        ) {
          this.broker.emit("admin.sendServiceMail", {
            "clientId": clientId,
            "templateName": "professionalInvoiceMailTemplate",
            ...mappingUtil.professionalInvoiceMailObject(bookingData),
          });
        }
        // SEND TO USER
        if (bookingData?.user?.email && bookingData?.user?.isEmailVerified) {
          this.broker.emit("admin.sendServiceMail", {
            "clientId": clientId,
            "templateName": "userInvoiceMailTemplate",
            ...mappingUtil.userInvoiceMailObject(bookingData),
          });
        }
        if (
          bookingData?.coorperate?.data?.email &&
          bookingData?.coorperate?.data?.isEmailVerified
        ) {
          // SEND TO CORPORATE
          this.broker.emit("admin.sendServiceMail", {
            "clientId": clientId,
            "templateName": "corporateInvoiceMailTemplate",
            ...mappingUtil.corporateInvoiceMailObject(bookingData),
          });
        }
      }
    }
    switch (bookingData.serviceCategory?.toLowerCase()) {
      case constantUtil.CONST_SHARERIDE.toLowerCase():
        {
          await this.broker.emit("professional.updateProfessionalShareRide", {
            "clientId": clientId,
            "actionStatus": professionalStatus, // constantUtil.ENDED,
            "bookingId": bookingData?._id?.toString(),
            "professionalId": bookingData?.professional?._id?.toString(),
            // "pickUpLat": context.params.pickUpLat,
            // "pickUpLng": context.params.pickUpLng,
            // "dropLat": context.params.dropLat,
            // "dropLng": context.params.dropLng,
          });
          // const ongoingShareRideAction =
          //   professionalStatus === constantUtil.ARRIVED
          //     ? constantUtil.CONST_PICKUP
          //     : professionalStatus === constantUtil.ENDED
          //     ? constantUtil.CONST_DROP
          //     : constantUtil.STARTED;
          const ongoingShareRideAction =
            professionalStatus === constantUtil.ENDED
              ? constantUtil.CONST_DROP
              : constantUtil.STATUS_UPCOMING;
          await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
            "clientId": clientId,
            "actionStatus": ongoingShareRideAction,
            "bookingId":
              ongoingShareRideAction === constantUtil.CONST_DROP
                ? null
                : bookingData?._id?.toString(),
            "professionalId": bookingData.professional._id.toString(),
            "serviceCategory": bookingData.serviceCategory,
            "lastRidePassengerCount":
              ongoingShareRideAction === constantUtil.CONST_DROP
                ? bookingData?.passengerCount
                : 0,
            "lat": context.params?.latLng?.lat,
            "lng": context.params?.latLng?.lng,
            "parentShareRideId": bookingData?.parentShareRideId,
            // "passengerCount":
            //   ongoingShareRideAction === constantUtil.CONST_DROP
            //     ? bookingData?.passengerCount
            //     : 0,
          });
          // Get booking Details
          bookingData = await this.getBookingDetailsById({
            "bookingId": context.params.bookingId.toString(),
          }); // Get booking Details
        }
        break;
    }

    const BookingStatus =
      bookingData.bookingStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_PROFESSIONALARRIVED
        : bookingData.bookingStatus === constantUtil.STARTED
        ? constantUtil.ACTION_PROFESSIONALSTARTED
        : bookingData.bookingStatus === constantUtil.ENDED
        ? constantUtil.ACTION_BOOKINGENDED
        : "";
    //-------- Send SMS Alert Start --------------------------
    // if (
    //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
    //   bookingData.bookingBy === constantUtil.ADMIN ||
    //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
    //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
    // )
    {
      await this.broker.emit("booking.sendSMSAlertByRideId", {
        "clientId": clientId,
        "rideId": context.params.bookingId.toString(),
        "bookingFrom": bookingData.bookingFrom,
        "notifyType": bookingData.bookingStatus,
        "notifyFor": constantUtil.USER,
      });
    }
    //-------- Send SMS Alert End --------------------------
    // if (bookingData.bookingBy === constantUtil.USER) {
    if (bookingData?.user?._id) {
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": BookingStatus,
          "timestamp": Date.now(),
          "message":
            BookingStatus === constantUtil.ACTION_PROFESSIONALARRIVED
              ? PROFESSIONAL_ARRIVED
              : BookingStatus === constantUtil.ACTION_PROFESSIONALSTARTED
              ? PROFESSIONAL_STARTED
              : TRIP_END,
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": [
          {
            "token": bookingData?.user?.deviceInfo[0]?.deviceId || "",
            "id": bookingData?.user?._id?.toString() || "",
            "deviceType": bookingData?.user?.deviceInfo[0]?.deviceType || "",
            "platform": bookingData?.user?.deviceInfo[0]?.platform || "",
            "socketId": bookingData?.user?.deviceInfo[0]?.socketId || "",
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      await this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)

      /* FCM */
      await this.broker.emit("admin.sendFCM", notificationObject);
      /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
      // if (generalSettings.data.isEnableWebAppBooking) {
      this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
        "bookingId": bookingData._id,
        "bookingStatus": bookingData.bookingStatus,
        "dateTime": bookingData.updatedAt,
        "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
        "bookingInfo": bookingData?.user?.bookingInfo || {},
      });
      // }
    }
    switch (bookingData.serviceCategory?.toLowerCase()) {
      case constantUtil.CONST_SHARERIDE.toLowerCase():
        {
          this.broker.emit("booking.sendNotificationActionShareRideStatus", {
            "bookingId": context.params.bookingId.toString(),
            "parentShareRideId": bookingData.parentShareRideId?.toString(),
            "userType": constantUtil.USER,
            "actionStatus": bookingData.bookingStatus,
            "notificationMessage":
              // BookingStatus === constantUtil.ACTION_PROFESSIONALARRIVED
              //   ? PROFESSIONAL_ARRIVED
              //   : BookingStatus === constantUtil.ACTION_PROFESSIONALSTARTED
              //   ? PROFESSIONAL_STARTED
              //   : TRIP_END + " " + INFO_FELLOW_PASSENGER,
              BookingStatus === constantUtil.ACTION_PROFESSIONALARRIVED
                ? "Your fellow passenger has arrived at the pickup location"
                : BookingStatus === constantUtil.ACTION_PROFESSIONALSTARTED
                ? "Your fellow passenger has started the ride"
                : "The ride has come to an end for your fellow passenger.",
          });
        }
        break;
    }
  } else {
    vehicleCategoryData =
      vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
  }
  //#region Packages
  if (
    bookingData.serviceCategory === constantUtil.CONST_PACKAGES.toLowerCase()
  ) {
    this.broker.emit("booking.updatePackageDetailsById", {
      "clientId": clientId,
      "action": constantUtil.UPDATE,
      "uploadedBy": context.params.uploadedBy,
      "packageData": context.params.packageData || {},
      "bookingId": bookingData?._id?.toString(),
    });
  }
  //#endregion Packages
  return { "code": 200, "data": bookingObject(bookingData), "message": "" };
};

// UPDATE BOOKING STOP STATUS
bookingAction.updatedProfessionalBookingStopStatus = async function (context) {
  let responseData,
    errorCode = 200,
    message = "",
    bookingData = null,
    multiStopChargeObj = {};
  const clientId = context.params.clientId || context.meta.clientId;
  const stopStatus = context.params.stopStatus.toUpperCase();
  const languageCode = context.meta?.userDetails?.languageCode;
  //
  const {
    RIDE_STATUS_INFO,
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_RIDE_STOP_NOTIFICATION,
    PROFESSIONAL_RIDE_START_NOTIFICATION,
    SOMETHING_WENT_WRONG,
  } = await notifyMessage.setNotifyLanguage(languageCode);
  try {
    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const vehicleCategoryDataList = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryDataList) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    // let bookingData = await this.adapter.model
    //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    //   .populate("user", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "cards": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("professional", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "cards": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("category")
    //   .populate("security")
    //   .populate("officer", { "password": 0, "preferences": 0 })
    //   .populate("couponId")
    //   .lean();
    // Get booking Details
    bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    const vehicleCategoryData =
      vehicleCategoryDataList[
        bookingData.vehicle.vehicleCategoryId?.toString()
      ];
    // bookingData = bookingData.toJSON();
    if (bookingData.bookingStatus === constantUtil.STARTED) {
      bookingData.activity.rideStops = bookingData.activity.rideStops.map(
        (stop) => {
          if (
            stop._id.toString() === context.params.stopId &&
            (stop.status === null || stop.status === "")
          ) {
            stop.lat = context.params.lat;
            stop.lng = context.params.lng;
            stop.status = constantUtil.ARRIVED;
          } else if (
            stop._id.toString() === context.params.stopId &&
            stop.status == constantUtil.ARRIVED
          ) {
            stop.lat = context.params.lat;
            stop.lng = context.params.lng;
            stop.status = constantUtil.ENDED;
          }
          return stop;
        }
      );
      if (stopStatus === constantUtil.ENDED) {
        multiStopChargeObj = {
          "$push": {
            "invoice.multiStopChargeList": {
              "amount": bookingData.invoice.stopCharge || 0,
              "stopedLatLng": {
                "lat": context.params.lat,
                "lng": context.params.lng,
              },
              "stopedTime": new Date(),
            },
          },
        };
      }
      const updatedBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
          "bookingStatus": constantUtil.STARTED,
        },
        {
          "activity.rideStops": bookingData.activity.rideStops,
          "$push": { "activity.stopTime": new Date() },
          ...multiStopChargeObj,
        }
      );
      if (!updatedBookingData.nModified) {
        throw new MoleculerError(RIDE_STATUS_INFO);
      }
      // bookingData = await this.adapter.model
      //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      //   .populate("user", {
      //     "password": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //     "cards": 0,
      //   })
      //   .populate("professional", {
      //     "password": 0,
      //     "cards": 0,
      //     "bankDetails": 0,
      //     "trustedContacts": 0,
      //   })
      //   .populate("category")
      //   .populate("security")
      //   .populate("officer", { "password": 0, "preferences": 0 })
      //   .populate("couponId")
      //   .lean();
      // Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      }); // Get booking Details

      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage;
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
      bookingData["imageURLPath"] =
        generalSettings.data.spaces.spacesBaseUrl +
        "/" +
        generalSettings.data.spaces.spacesObjectName;

      const stopAction =
        stopStatus === constantUtil.ARRIVED
          ? constantUtil.ACTION_BOOKINGSTOPARRIVED
          : constantUtil.ACTION_BOOKINGSTOPENDED;

      // if (bookingData.bookingBy === constantUtil.USER) {
      if (bookingData?.user?._id) {
        const notificationObject = {
          "clientId": clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": stopAction,
            "timestamp": Date.now(),
            "message":
              stopAction === constantUtil.ACTION_BOOKINGSTOPARRIVED
                ? PROFESSIONAL_RIDE_STOP_NOTIFICATION
                : PROFESSIONAL_RIDE_START_NOTIFICATION,
            "details": lzStringEncode(bookingObject(bookingData)),
          },
          "registrationTokens": [
            {
              "token": bookingData?.user?.deviceInfo[0]?.deviceId || "",
              "id": bookingData?.user?._id?.toString() || "",
              "deviceType": bookingData?.user?.deviceInfo[0]?.deviceType || "",
              "platform": bookingData?.user?.deviceInfo[0]?.platform || "",
              "socketId": bookingData?.user?.deviceInfo[0]?.socketId || "",
            },
          ],
        };
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);
        // this.broker.emit('socket.statusChangeEvent', notificationObject)
        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }
    }
    responseData = bookingObject(bookingData);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

// SUBMIT RATING FOR USER AND PROFESSIONAL
bookingAction.submitRating = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  const responseData = {};
  let languageCode,
    userId,
    professionalId,
    userData = {},
    rewardsData = {},
    rewardType,
    rewardPoints = 0;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
    userId = context.meta.userId;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
    professionalId = context.meta.professionalId;
  }
  //-------------- Notification Msg Start -----------------
  const { INVALID_BOOKING, RATING_UPDATE_FAILED, RATING_SUBMITED } =
    notifyMessage.setNotifyLanguage(languageCode);
  //-------------- Notification Msg End -------------------
  try {
    // const bookingData = await this.adapter.model
    //   .findOne(
    //     { "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()) },
    //     { "bookingStatus": 1 }
    //   )
    //   .lean();

    // Get booking Details
    const bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    // bookingData = bookingData.toJSON();

    if (bookingData.bookingStatus === constantUtil.ENDED) {
      const updateData =
        userType === constantUtil.USER
          ? {
              "bookingReview.userReview.rating": context.params.rating,
              "bookingReview.userReview.comment": context.params.comment,
              "bookingReview.isUserReviewed": true,
              "trackData.user": context.params?.trackData || {},
            }
          : {
              "bookingReview.professionalReview.rating": context.params.rating,
              "bookingReview.professionalReview.comment":
                context.params.comment,
              "bookingReview.isProfessionalReviewed": true,
              "trackData.professional": context.params?.trackData || {},
            };
      const updateBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
          "bookingStatus": constantUtil.ENDED,
        },
        updateData
      );
      if (!updateBookingData.nModified) {
        throw new MoleculerError(RATING_UPDATE_FAILED);
      }
      if (userType === constantUtil.USER) {
        if (bookingData.user.bookingInfo.pendingReview) {
          userData = await this.broker.emit("user.updateRating", {
            "clientId": clientId,
            "userId": userId,
            "rating": context.params.rating,
          });
        } else {
          // For ongoingBooking Track
          userData = [
            {
              bookingInfo: {
                ongoingBooking: bookingData.user.bookingInfo.ongoingBooking,
              },
            },
          ];
        }
      } else {
        userData = await this.broker.emit("professional.updateRating", {
          "clientId": clientId,
          "professionalId": professionalId,
          "rating": context.params.rating,
        });
      }
      userData = userData && userData?.[0];
      //------------------ Rewards Start -------------------------
      // const randomValue = Math.floor(Math.random() * 10 + 1);
      // if (randomValue === 10) {
      //   rewardType = constantUtil.PARTNERDEALS;
      // } else {
      //   rewardType = constantUtil.POINTS;
      // }
      rewardType = constantUtil.PARTNERDEALS;
      rewardsData = await this.broker.emit(
        "rewards.getRewardsForRideByUserType",
        {
          "clientId": clientId,
          "lng": context.params?.lng,
          "lat": context.params?.lat,
          "userType": userType,
          "rideId": context.params.bookingId.toString(),
          "professionalId": professionalId,
          "userId": userId,
          "rewardType": rewardType,
        }
      );
      rewardsData = rewardsData && rewardsData[0];
      // ---------- Partner Deals Not Applied then Applay Point Start ----------------
      if (rewardType === constantUtil.PARTNERDEALS) {
        if (!rewardsData) {
          rewardType = constantUtil.POINTS;
          rewardsData = await this.broker.emit(
            "rewards.getRewardsForRideByUserType",
            {
              "clientId": clientId,
              "lng": context.params?.lng,
              "lat": context.params?.lat,
              "userType": userType,
              "rideId": context.params.bookingId.toString(),
              "professionalId": professionalId,
              "userId": userId,
              "rewardType": rewardType,
            }
          );
          rewardsData = rewardsData && rewardsData[0];
        }
      }
      // ---------- Partner Deals Not Applied then Applay Point End ----------------
      //------------ Auto Scratch Reward For POINTS (rewardType) Start ---------
      if (rewardType === constantUtil.POINTS) {
        if (rewardsData) {
          rewardsData = await this.broker.emit("rewards.scratchRewardsById", {
            "clientId": clientId,
            "userType": userType,
            "rewardId": rewardsData?._id?.toString(),
            "professionalId": professionalId,
            "userId": userId,
          });
          rewardsData = rewardsData && rewardsData[0];
          rewardPoints = rewardsData.rewardPoints;
        } else {
          rewardType = "";
        }
      }
      //------------ Auto Scratch Reward For POINTS (rewardType) End ---------
      //------------------ Rewards End -------------------------
    }
  } catch (e) {
    rewardType = "";
  }
  //----------- Response Data Start ------------
  responseData["rewardType"] = rewardType;
  responseData["points"] = convertToInt(rewardPoints);
  responseData["nextOngoingBooking"] =
    userData?.bookingInfo?.ongoingBooking || null;
  //----------- Response Data End ------------
  return {
    "code": 200,
    // "data": bookingObject(bookingData),
    "data": responseData,
    "message": RATING_SUBMITED,
  };
};

bookingAction.professionalGetScheduleList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { LOCATION_ACCESS_FAILED, VEHICLE_CATEGORY_NOT_FOUND } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.professionalDetails?.languageCode
    );
  let serviceCategory = { "_id": context.params.serviceCategoryId }; // i used object if serviceCategoryId not gived by client i take from database by lat lng
  if (!serviceCategory._id) {
    serviceCategory = await this.broker.emit("category.getServieCategory", {
      "clientId": clientId,
      "lat": context.params.lat,
      "lng": context.params.lng,
      "categoryName": null, // null --> get near by One serviceArea based on Location,
    });
    serviceCategory = serviceCategory && serviceCategory[0];
    console.log("this is consoleing from", serviceCategory);
  }

  console.log("this is service Category data", serviceCategory);
  if (!serviceCategory) {
    return {
      "code": 422,
      "message": LOCATION_ACCESS_FAILED,
      "data": {},
    };
  }
  const categoriesIds = context.params.categoryId.map((ids) =>
    mongoose.Types.ObjectId(ids)
  );
  const skip = context.params.skip ?? 0;
  const limit = context.params.limit ?? 20;

  const query = [];
  query.push({
    "$match": {
      // "$or": [ // Or Condition Moved below (inside the Loop)
      //   { "category": mongoose.Types.ObjectId(serviceCategory._id.toString()) },
      //   { "serviceCategory": "intercityride" },
      // ],
      //"clientId": mongoose.Types.ObjectId(clientId),
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.SCHEDULE,
      "vehicle.vehicleCategoryId": { "$in": categoriesIds },
      "isAdminForceAssign": false,
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$limit": parseInt(limit) },
        { "$skip": parseInt(skip) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "categoryData",
          },
        },
        { "$unwind": "$categoryData" },
        // {
        //   "$match": {
        //     "$or": [
        //       {
        //         "$categoryData._id": mongoose.Types.ObjectId(
        //           serviceCategory._id.toString()
        //         ),
        //       },
        //       {
        //         "$categoryData.serviceAreaId": mongoose.Types.ObjectId(
        //           serviceCategory._id.toString()
        //         ),
        //       },
        //       { "$serviceCategory": "intercityride" },
        //     ],
        //   },
        // },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "tripType": "$tripType",
            "vehicle": "$vehicle",
            "origin": "$origin",
            "destination": "$destination",
            "estimation": "$estimation",
            "activity": "$activity",
            "invoice": "$invoice",
            "payment": "$payment",
            "isAdminForceAssign": "$isAdminForceAssign",
            "isAcceptOtherCategory": "$isAcceptOtherCategory",
            "bookingStatus": "$bookingStatus",
            "currencySymbol": "$currencySymbol",
            "currencyCode": "$currencyCode",
            "serviceCategory": "$serviceCategory",
            "user": "$user",
            "category": "$categoryData",
            "isWished": {
              "$reduce": {
                "input": "$wishList",
                "initialValue": false,
                "in": {
                  "$cond": {
                    "if": {
                      "$eq": [
                        mongoose.Types.ObjectId(context.meta.professionalId),
                        "$$this",
                      ],
                    },
                    "then": true,
                    "else": false,
                  },
                },
              },
            },
          },
        },
        { "$sort": { "bookingDate": 1 } },
      ],
    },
  });

  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  console.log("this is data from  user schecule list", jsonData);
  const responseData = [];
  const vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  const vehicleCategoryDataset =
    vehicleCategoryData[context.params.categoryId.toString()];

  jsonData[0].response.forEach((data) => {
    if (vehicleCategoryDataset) {
      data.vehicle.vehicleCategoryName =
        vehicleCategoryDataset.vehicleCategory || "";
      data.vehicle.vehicleCategoryImage =
        vehicleCategoryDataset.categoryImage || "";
      data.vehicle.vehicleCategoryMapImage =
        vehicleCategoryDataset.categoryMapImage || "";
      data.distanceUnit = data.category.distanceType;
    }
    if (
      data.category._id?.toString() === serviceCategory._id.toString() ||
      data.category.serviceAreaId?.toString() ===
        serviceCategory._id.toString() ||
      data.serviceCategory === "intercityride"
    ) {
      responseData.push(bookingObject(data));
    }
  });
  return {
    "code": 200,
    "data": {
      "serviceCategoryId": serviceCategory._id,
      "responseData": responseData,
    },
    "message": "",
  };
};

// ADD PROFESSIONAL WISHLIST
bookingAction.professionalAddWishList = async function (context) {
  const action = context.params.action.toUpperCase();
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  const {
    UPDATED,
    WISHLIST_ACTION_FAILED,
    INVALID_PROFESSIONAL,
    PROFESSIONAL_INFO,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  let professionalData = await this.broker.emit("professional.getById", {
    "id": context.meta?.professionalId?.toString(),
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData) {
    throw new MoleculerError(INVALID_PROFESSIONAL);
  }
  const bookingData = await this.adapter.model.findOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.bookingId),
    },
    { "payment": 1 }
  );
  if (
    // parseFloat(professionalData.wallet.availableAmount) <
    // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
    bookingData.payment.option === constantUtil.WALLET ||
    bookingData.payment.option === constantUtil.PAYMENTCARD ||
    // bookingData.payment.option !== constantUtil.CASH &&
    // bookingData.payment.option !== constantUtil.CREDIT) ||
    parseFloat(professionalData.wallet.availableAmount) >=
      parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
  ) {
    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "bookingType": constantUtil.SCHEDULE,
      },
      {
        [action === constantUtil.ADD ? "$push" : "$pull"]: {
          "wishList": context.meta.professionalId,
        },
      }
    );
    if (!updatedBookingData.nModified) {
      throw new MoleculerError(
        WISHLIST_ACTION_FAILED.replace("{{action}}", action)
      );
    }
  } else {
    return { "code": 600, "data": {}, "message": PROFESSIONAL_INFO };
  }
  return { "code": 200, "data": {}, "message": UPDATED };
};

// GET PROFESSIONAL WISHLIST
bookingAction.professionalGetWishList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { VEHICLE_CATEGORY_NOT_FOUND } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  query.push({
    "$match": {
      // "clientId": mongoose.Types.ObjectId(clientId),
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.SCHEDULE,
      "wishList": {
        "$in": [mongoose.Types.ObjectId(context.meta.professionalId)],
      },
      "isAdminForceAssign": context.params?.isAdminAssign,
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        { "$unwind": "$category" },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "vehicle": "$vehicle",
            "origin": "$origin",
            "destination": "$destination",
            "estimation": "$estimation",
            "activity": "$activity",
            "invoice": "$invoice",
            "payment": "$payment",
            "isAdminForceAssign": "$isAdminForceAssign",
            "isAcceptOtherCategory": "$isAcceptOtherCategory",
            "bookingStatus": "$bookingStatus",
            "currencySymbol": "$currencySymbol",
            "currencyCode": "$currencyCode",
            "serviceCategory": "$serviceCategory",
            "user": "$user",
            "category": "$category",
            "isWished": {
              "$reduce": {
                "input": "$wishList",
                "initialValue": false,
                "in": {
                  "$cond": {
                    "if": {
                      "$eq": [
                        mongoose.Types.ObjectId(context.meta.professionalId),
                        "$$this",
                      ],
                    },
                    "then": true,
                    "else": false,
                  },
                },
              },
            },
          },
        },
        { "$sort": { "bookingDate": 1 } },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseData = [];
  const vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  jsonData[0].response.forEach((data) => {
    const vehicleCategoryDataset =
      vehicleCategoryData[data.vehicle.vehicleCategoryId.toString()];
    if (vehicleCategoryDataset) {
      data.vehicle.vehicleCategoryName =
        vehicleCategoryDataset.vehicleCategory || "";
      data.vehicle.vehicleCategoryImage =
        vehicleCategoryDataset.categoryImage || "";
      data.vehicle.vehicleCategoryMapImage =
        vehicleCategoryDataset.categoryMapImage || "";
      data.distanceUnit = data.category.distanceType;
    }
    data["userType"] = constantUtil.PROFESSIONAL;
    responseData.push(bookingObject(data));
  });
  return {
    "code": 200,
    "data": responseData,
    "message": "",
  };
};

bookingAction.userGetScheduleList = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { VEHICLE_CATEGORY_NOT_FOUND } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.SCHEDULE,
      "user": mongoose.Types.ObjectId(context.meta.userId),
    },
  });
  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$limit": parseInt(limit) },
        { "$skip": parseInt(skip) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        { "$unwind": "$category" },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "vehicle": "$vehicle",
            "origin": "$origin",
            "destination": "$destination",
            "estimation": "$estimation",
            "activity": "$activity",
            "invoice": "$invoice",
            "payment": "$payment",
            "isAdminForceAssign": "$isAdminForceAssign",
            "isAcceptOtherCategory": "$isAcceptOtherCategory",
            "bookingStatus": "$bookingStatus",
            "currencySymbol": "$currencySymbol",
            "currencyCode": "$currencyCode",
            "serviceCategory": "$serviceCategory",
            "user": "$user",
            "category": "$category",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseData = [];
  const vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  jsonData[0].response.forEach((data) => {
    const vehicleCategoryDataset =
      vehicleCategoryData[data.vehicle.vehicleCategoryId.toString()];
    if (vehicleCategoryDataset) {
      data.vehicle.vehicleCategoryName =
        vehicleCategoryDataset.vehicleCategory || "";
      data.vehicle.vehicleCategoryImage =
        vehicleCategoryDataset.categoryImage || "";
      data.vehicle.vehicleCategoryMapImage =
        vehicleCategoryDataset.categoryMapImage || "";
      data.distanceUnit = data.category.distanceType;
    }
    data["userType"] = constantUtil.USER;
    responseData.push(bookingObject(data));
  });
  return {
    "code": 200,
    "data": responseData,
    "message": "",
  };
};

bookingAction.getOrderAndBookingHistory = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { VEHICLE_CATEGORY_NOT_FOUND } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  const userType = context.params.userType.toUpperCase();
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  const condition =
    userType === constantUtil.USER
      ? { "user": mongoose.Types.ObjectId(context.meta.userId) }
      : {
          "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        };

  query.push({
    "$match": {
      //"clientId": mongoose.Types.ObjectId(clientId),
      "bookingStatus": {
        "$in": [
          constantUtil.ENDED,
          constantUtil.USERCANCELLED,
          constantUtil.PROFESSIONALCANCELLED,
        ],
      },
      ...condition,
    },
  });

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$lookup": {
            "from": "categories",
            "localField": "category",
            "foreignField": "_id",
            "as": "category",
          },
        },
        { "$unwind": "$category" },
        {
          "$lookup": {
            "from": "securityescorts",
            "localField": "security",
            "foreignField": "_id",
            "as": "security",
          },
        },
        {
          "$unwind": {
            "path": "$security",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$unwind": {
            "path": "$officer",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "clientId": "$clientId",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "vehicle": "$vehicle",
            "origin": "$origin",
            "destination": "$destination",
            "estimation": "$estimation",
            "activity": "$activity",
            "invoice": "$invoice",
            "payment": "$payment",
            "isSecurityAvail": "$isSecurityAvail",
            "isAdminForceAssign": "$isAdminForceAssign",
            "isAcceptOtherCategory": "$isAcceptOtherCategory",
            "bookingStatus": "$bookingStatus",
            "currencySymbol": "$currencySymbol",
            "currencyCode": "$currencyCode",
            "user": "$user",
            "professional": "$professional",
            "category": "$category",
            "security": "$security",
            "officer": "$officer",
            "bookingReview": "$bookingReview",
            "rideMapRouteImage": "$rideMapRouteImage",
            "serviceCategory": "$serviceCategory",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const responseData = [];
  const vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  jsonData[0].response.forEach((data) => {
    const vehicleCategoryDataset =
      vehicleCategoryData[data.vehicle.vehicleCategoryId.toString()];
    if (vehicleCategoryDataset) {
      data.vehicle.vehicleCategoryName =
        vehicleCategoryDataset.vehicleCategory || "";
      data.vehicle.vehicleCategoryImage =
        vehicleCategoryDataset.categoryImage || "";
      data.vehicle.vehicleCategoryMapImage =
        vehicleCategoryDataset.categoryMapImage || "";
      data.distanceUnit = data.category.distanceType;
    }
    data["userType"] = userType;
    responseData.push(bookingObjectDetails(data));
  });
  return {
    "code": 200,
    "data": responseData,
    "message": "",
  };
};
// RIDE BOOKING LOCATION CHANGE
bookingAction.changeRideLocation = async function (context) {
  let errorCode = 200,
    message = "",
    responseData = {},
    languageCode = null;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  languageCode =
    context.params.userType === constantUtil.USER
      ? context.meta?.userDetails?.languageCode
      : context.meta?.professionalDetails?.languageCode;
  //
  const {
    SOMETHING_WENT_WRONG,
    RIDE_LOCATION_UPDATE_FAILED,
    LOCATION_CHANGED_SUCCESSFULLY,
    VEHICLE_CATEGORY_NOT_FOUND,
    RIDE_ADDRESS_UPDATE_BY_USER,
    RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL,
  } = await notifyMessage.setNotifyLanguage(
    context.params.langCode || languageCode
  );
  // const updatedBookingData = await this.adapter.model.updateOne(
  //   {
  //     "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //   },
  //   {
  //     "activity.rideStops": context.params.bookingLocations,
  //     "activity.rideStopsUpdatedBy": context.meta.userType,
  //     "activity.rideStopsUpdatedTime": new Date(),
  //   }
  // );

  // if (!updatedBookingData.nModified)
  //   throw new MoleculerError(RIDE_LOCATION_UPDATE_FAILED);

  // const bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();
  try {
    let bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .lean();

    let directionsData = {},
      traveledDataSet = {};
    context.params.bookingLocations?.map((stop) => {
      if (stop.type === constantUtil.ORIGIN) {
        directionsData["from"] = [stop.lat + "," + stop.lng];
      }
      if (stop.type === constantUtil.DESTINATION) {
        directionsData["to"] = [stop.lat + "," + stop.lng];
      }
      return stop;
    });
    //----------
    if (context.params.dropTimeDuration || context.params.dropDistance) {
      traveledDataSet["dropTimeDuration"] = parseInt(
        context.params.dropTimeDuration || 0
      );
      traveledDataSet["dropDistance"] = parseInt(
        context.params.dropDistance || 0
      );
    } else {
      const mapDataDrop = await googleApiUtil.directions({
        "clientId": clientId,
        "from": directionsData["from"],
        "to": directionsData["to"],
      });
      traveledDataSet["dropTimeDuration"] =
        mapDataDrop.data?.routes[0]?.legs[0]?.duration?.value || 0;
      traveledDataSet["dropDistance"] =
        mapDataDrop.data?.routes[0]?.legs[0]?.distance?.value || 0;
      // traveledDataSet["km"] = traveledDataSet["dropDistance"] / 1000;
      // traveledDataSet["timeDuration"] = traveledDataSet["dropTimeDuration"] / 60;
    }
    //----------
    const calculatedData = await helperUtil.calculateEstimateAmount({
      "clientId": clientId,
      "bookingData": bookingData,
      //
      "tollFareAmount": context.params?.tollFareAmount || 0,
      "airportFareAmount": context.params?.airportFareAmount || 0,
      //
      "estimationDistance": traveledDataSet["dropDistance"], // traveledDataSet["km"],
      "estimationTime": traveledDataSet["dropTimeDuration"], // traveledDataSet["timeDuration"],
      "calculatedPickupDistance": bookingData.estimation.pickupDistance, // traveledDataSet["pickupDistance"],
      "calculatedPickupTime": bookingData.estimation.pickupTime, // traveledDataSet["pickupTimeDuration"],
      "calculatedDropDistance": traveledDataSet["dropDistance"],
      "calculatedDropTime": traveledDataSet["dropTimeDuration"],
      "beforeRideAssistanceCareProvidesMins": parseInt(
        bookingData.invoice.beforeRideAssistanceCareProvidesMins || 0
      ),
      "afterRideAssistanceCareProvidesMins": parseInt(
        bookingData.invoice.afterRideAssistanceCareProvidesMins || 0
      ),
    });
    const finalData = await helperUtil.endTripCalculation({
      "bookingData": bookingData,
      "paymentOption": bookingData.payment.option,
      "calculatedData": calculatedData,
      "estimationDistance": traveledDataSet["dropDistance"], //  traveledDataSet["km"],
      "estimationTime": traveledDataSet["dropTimeDuration"], // traveledDataSet["timeDuration"],
      "calculatedPickupDistance": bookingData.estimation.pickupDistance, // traveledDataSet["pickupDistance"],
      "calculatedPickupTime": bookingData.estimation.pickupTime, // traveledDataSet["pickupTimeDuration"],
      "calculatedDropDistance": traveledDataSet["dropDistance"],
      "calculatedDropTime": traveledDataSet["dropTimeDuration"],
    });
    //---------
    bookingData = await this.adapter.model
      .findOneAndUpdate(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
        },
        {
          "activity.rideStops": context.params.bookingLocations,
          "activity.rideStopsUpdated": true,
          "activity.rideStopsUpdatedBy": context.params.userType,
          "activity.rideStopsUpdatedTime": new Date(),
          //
          "invoice.isMinimumChargeApplied":
            finalData.isMinimumChargeApplied || false,
          "invoice.serviceTax": finalData.serviceTax || 0,
          "invoice.tollFareAmount": finalData.tollFareAmount || 0,
          "invoice.airportFareAmount": finalData.airportFareAmount || 0,
          "invoice.amountInSite": finalData.amountInSite || 0,
          "invoice.amountInDriver": finalData.amountInDriver || 0,
          "invoice.siteCommission": finalData.siteCommission || 0,
          "invoice.travelCharge": finalData.travelCharge || 0,
          "invoice.distanceFare": finalData.distanceFare || 0,
          "invoice.timeFare": finalData.timeFare || 0,
          "invoice.siteCommissionWithoutTax":
            finalData.siteCommissionWithoutTax || 0,
          "invoice.professionalCommision": finalData.professionalCommision || 0,
          "invoice.professionalCommisionWithoutTips":
            finalData.professionalCommision || 0,
          "invoice.peakFareCharge": finalData["peakFareCharge"],
          "invoice.nightFareCharge": finalData["nightFareCharge"],
          "invoice.multiStopCharge": finalData["multiStopCharge"],
          "invoice.waitingCharge": finalData["waitingCharge"],
          "invoice.beforeRideWaitingCharge":
            finalData["beforeRideWaitingCharge"],
          "invoice.beforeRideWaitingMins": finalData["beforeRideWaitingMins"],
          "invoice.afterRideWaitingCharge": finalData["afterRideWaitingCharge"],
          "invoice.afterRideWaitingMins": finalData["afterRideWaitingMins"],
          "invoice.intercityPickupTimeFare":
            finalData["intercityPickupTimeFare"],
          "invoice.intercityPickupDistanceFare":
            finalData["intercityPickupDistanceFare"],
          "invoice.intercityPickupCharge": finalData["intercityPickupCharge"],
          "invoice.intercityDropCharge": finalData["intercityDropCharge"],
          "invoice.roundingAmount": finalData["roundingAmount"],
          "invoice.discountAmount": finalData["discountAmount"],
          "invoice.beforeRideAssistanceCareAmount":
            finalData["beforeRideAssistanceCareAmount"],
          "invoice.afterRideAssistanceCareAmount":
            finalData["afterRideAssistanceCareAmount"],
          "invoice.assistanceCareAmount": finalData["assistanceCareAmount"],
          "invoice.estimationAmount": finalData["estimationAmount"],
          "invoice.actualEstimationAmount": finalData["estimationAmount"],
          "invoice.estimationPayableAmount": finalData.payableAmount || 0,
          "invoice.payableAmount": finalData.payableAmount || 0,
          //
          "estimation.distance": traveledDataSet["dropDistance"] || 0,
          "estimation.time": traveledDataSet["dropTimeDuration"] || 0,
          "bookedEstimation.distance": traveledDataSet["dropDistance"],
          "bookedEstimation.time": traveledDataSet["dropTimeDuration"],
          // "bookedEstimation.distance": bookingData.estimation.distance,
          // "bookedEstimation.time": bookingData.estimation.acceptTime,
          // "bookedEstimation.pickupDistance":
          //   bookingData.estimation.pickupDistance,
          // "bookedEstimation.pickupTime": bookingData.estimation.pickupTime,
        },
        { "new": true }
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();

    if (!bookingData) {
      throw new MoleculerError(RIDE_LOCATION_UPDATE_FAILED);
    }
    // bookingData = bookingData.toJSON();
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    vehicleCategoryData =
      vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;
    //
    let registrationTokensData = null,
      notifyUserType = null,
      notifyMessage = null;
    if (context.params.userType === constantUtil.PROFESSIONAL) {
      notifyUserType = constantUtil.USER;
      notifyMessage = RIDE_ADDRESS_UPDATE_BY_PROFESSIONAL;
      registrationTokensData = [
        {
          "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
          "id": bookingData.user?._id?.toString(),
          "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
          "platform": bookingData.user?.deviceInfo[0]?.platform || "",
          "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
        },
      ];
    } else {
      notifyUserType = constantUtil.PROFESSIONAL;
      notifyMessage = RIDE_ADDRESS_UPDATE_BY_USER;
      registrationTokensData = [
        {
          "token": bookingData.professional?.deviceInfo[0]?.deviceId || "",
          "id": bookingData.professional?._id?.toString(),
          "deviceType":
            bookingData.professional?.deviceInfo[0]?.deviceType || "",
          "platform": bookingData.professional?.deviceInfo[0]?.platform || "",
          "socketId": bookingData.professional?.deviceInfo[0]?.socketId || "",
        },
      ];
    }
    const notificationObject = {
      "clientId": clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": notifyUserType,
        "action": constantUtil.ACTION_BOOKINGLOCATIONCHANGE,
        "timestamp": Date.now(),
        "message": notifyMessage,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": registrationTokensData,
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
    //
    responseData = bookingObject(bookingData);
    message = LOCATION_CHANGED_SUCCESSFULLY;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

// TRUSTED CONTACTS LIST
bookingAction.getTrustedContactsReasons = async function (context) {
  const clientId = context.params.clientId || context.meta?.clientId;
  let jsonData = await this.broker.emit("admin.getTrustedContactsReasons", {
    ...context.params,
    "clientId": clientId,
  });
  jsonData = jsonData && jsonData[0];
  if (jsonData.code === 0) {
    throw new MoleculerError(jsonData.message);
  } else {
    return jsonData || {};
  }
};

bookingAction.addAndRemoveTrustedContacts = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta?.clientId;
  switch (context.params.action) {
    case "add": {
      let jsonData = await this.broker.emit("admin.getReasonData", {
        ...context.params,
        "clientId": clientId,
      });
      jsonData = jsonData && jsonData[0];
      if (jsonData.code === 0) {
        throw new MoleculerError(jsonData.message);
      }
      const contacts = { "phone": {} };
      contacts["_id"] = mongoose.Types.ObjectId();
      contacts["name"] = context.params.name;
      contacts["phone"]["code"] = context.params.phoneCode;
      contacts["phone"]["number"] = context.params.phoneNumber;
      contacts["reasons"] = {};
      if (jsonData.code === 1)
        contacts["reasons"] = {
          "_id": jsonData.data._id,
          "reason": jsonData.data.data.reason,
          "isDefaultReason": jsonData.data.data.isDefaultReason,
          "action": jsonData.data.data.action,
          "isTimings": jsonData.data.data.isTimings,
          "fromTime": jsonData.data.data.fromTime || "",
          "toTime": jsonData.data.data.toTime || "",
          "status": jsonData.data.data.status,
        };
      context.params.contacts = contacts;
      if (userType === constantUtil.USER) {
        context.params.userId = context.meta.userId;
        let userContactData = await this.broker.emit(
          "user.updateTrustedContact",
          { ...context.params, "clientId": clientId }
        );
        userContactData = userContactData && userContactData[0];
        if (userContactData.code === 0) {
          throw new MoleculerError(userContactData.message);
        }
        userContactData.data = {
          "_id": contacts._id,
          "name": contacts.name,
          "phone": contacts.phone,
          "reason": {
            "_id": contacts.reasons._id,
            "reason": contacts.reasons.reason,
          },
        };
        return userContactData;
      } else {
        context.params.professionalId = context.meta.professionalId;
        let professionalContactData = await this.broker.emit(
          "professional.updateTrustedContact",
          { ...context.params, "clientId": clientId }
        );
        professionalContactData =
          professionalContactData && professionalContactData[0];
        if (professionalContactData.code === 0) {
          throw new MoleculerError(professionalContactData.message);
        }
        professionalContactData.data = {
          "_id": contacts._id,
          "name": contacts.name,
          "phone": contacts.phone,
          "reason": {
            "_id": contacts.reasons._id,
            "reason": contacts.reasons.reason,
          },
        };
        return professionalContactData;
      }
    }
    case "remove": {
      if (userType == constantUtil.USER) {
        context.params.userId = context.meta.userId;
        let userContactData = await this.broker.emit(
          "user.updateTrustedContact",
          { ...context.params, "clientId": clientId }
        );
        userContactData = userContactData && userContactData[0];
        if (userContactData.code === 0) {
          throw new MoleculerError(userContactData.message);
        }
        return userContactData;
      } else {
        context.params.professionalId = context.meta.professionalId;
        let professionalContactData = await this.broker.emit(
          "professional.updateTrustedContact",
          { ...context.params, "clientId": clientId }
        );
        professionalContactData =
          professionalContactData && professionalContactData[0];
        if (professionalContactData.code === 0) {
          throw new MoleculerError(professionalContactData.message);
        }
        return professionalContactData;
      }
    }
    case "update": {
      let jsonData = await this.broker.emit("admin.getReasonData", {
        ...context.params,
        "clientId": clientId,
      });
      jsonData = jsonData && jsonData[0];
      if (jsonData.code === 0) {
        throw new MoleculerError(jsonData.message);
      }
      const contacts = { "phone": {} };
      contacts["_id"] = context.params.id;
      contacts["name"] = context.params.name;
      contacts["phone"]["code"] = context.params.phoneCode;
      contacts["phone"]["number"] = context.params.phoneNumber;
      contacts["reasons"] = {};
      if (jsonData.code === 1)
        contacts["reasons"] = {
          "_id": jsonData.data._id,
          "reason": jsonData.data.data.reason,
          "isDefaultReason": jsonData.data.data.isDefaultReason,
          "action": jsonData.data.data.action,
          "isTimings": jsonData.data.data.isTimings,
          "fromTime": jsonData.data.data.fromTime || "",
          "toTime": jsonData.data.data.toTime || "",
          "status": jsonData.data.data.status,
        };
      context.params.contacts = contacts;
      if (userType == constantUtil.USER) {
        context.params.userId = context.meta.userId;
        let userContactData = await this.broker.emit(
          "user.changeTrustedContactReason",
          { ...context.params, "clientId": clientId }
        );
        userContactData = userContactData && userContactData[0];
        if (userContactData.code === 0) {
          throw new MoleculerError(userContactData.message);
        }
        userContactData.data = {
          "_id": contacts._id,
          "name": contacts.name,
          "phone": contacts.phone,
          "reason": {
            "_id": contacts.reasons._id,
            "reason": contacts.reasons.reason,
          },
        };
        return userContactData;
      } else {
        context.params.professionalId = context.meta.professionalId;
        let professionalContactData = await this.broker.emit(
          "professional.changeTrustedContactReason",
          { ...context.params, "clientId": clientId }
        );
        professionalContactData =
          professionalContactData && professionalContactData[0];
        if (professionalContactData.code === 0) {
          throw new MoleculerError(professionalContactData.message);
        }
        professionalContactData.data = {
          "_id": contacts._id,
          "name": contacts.name,
          "phone": contacts.phone,
          "reason": {
            "_id": contacts.reasons._id,
            "reason": contacts.reasons.reason,
          },
        };
        return professionalContactData;
      }
    }
  }
};

bookingAction.getTrustedContactsList = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta?.clientId;
  if (userType == constantUtil.USER) {
    context.params.userId = context.meta.userId;
    let userContactData = await this.broker.emit(
      "user.getTrustedContactsList",
      {
        ...context.params,
        "clientId": clientId,
      }
    );
    userContactData = userContactData && userContactData[0];
    if (userContactData.code === 0) {
      throw new MoleculerError(userContactData.message);
    }
    return userContactData;
  } else {
    context.params.professionalId = context.meta.professionalId;
    let professionalContactData = await this.broker.emit(
      "professional.getTrustedContactsList",
      { ...context.params, "clientId": clientId }
    );
    professionalContactData =
      professionalContactData && professionalContactData[0];
    if (professionalContactData.code === 0) {
      throw new MoleculerError(professionalContactData.message);
    } else {
      return professionalContactData;
    }
  }
};

bookingAction.shareTripToTrustedContact = async function (context) {
  const { INVALID_BOOKING, TRIP_SHARED } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.userDetails?.languageCode
    );
  const bookingData = await this.adapter.model
    .findOne({
      "_id": context.params.bookingId,
    })
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  context.params.professionalId = bookingData.professional;
  // const messageData = {
  //   'professionalId': context.params.professionalId,
  // }
  // context.params.contacts.forEach((contact) => {
  //   // this.broker.emit('admin.sendSMS', {
  //   //   'phoneNumber': `${contact.phoneCode}${contact.phoneNumber}`,
  //   //   'message': '',
  //   // })
  // })

  return { "code": 200, "data": {}, "message": TRIP_SHARED };
};

// VIEW BOOKING RECIPT
bookingAction.viewRecipt = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const clientId = context.params.clientId || context.meta.clientId;
  let languageCode,
    userId,
    professionalId,
    reciptFields = [];
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  //  #endregion Get Config Settings
  if (userType === constantUtil.USER) {
    //   userId= context.meta.userId?.toString() === bookingData.user?.toString()

    languageCode = context.meta?.userDetails?.languageCode;
    reciptFields = [
      "baseFare", // Client Added
      "timeFare", // Client Added
      "distanceFare", // Client Added
      "peakFareCharge", // Client Added
      "nightFareCharge", // Client Added
      "multiStopCharge",
      // "tripFeeUser",
      "WaitingCharge",
      "assistanceCareAmount",
      "surCharge",
      "serviceTax",
      "tipsAmount",
      "couponSavings",
      "tollFee",
      "airportFee",
      "pendingAmount",
      "intercityBoundaryFare",
      "roundingAmount",
      "discountAmount",
      "totalBill",
    ];
  } else if (userType === constantUtil.PROFESSIONAL) {
    // professionalId =
    //   context.meta.professionalId?.toString() ===
    //   bookingData.professional?.toString();
    languageCode = context.meta?.professionalDetails?.languageCode;
    reciptFields = [
      "baseFare", // Client Added
      "timeFare", // Client Added
      "distanceFare", // Client Added
      "peakFareCharge", // Client Added
      "nightFareCharge", // Client Added
      "multiStopCharge",
      // "tripFeeProfessional",
      "surCharge",
      "discountApplied",
      "WaitingCharge",
      "assistanceCareAmount",
      // "professionalCommission",
      "intercityBoundaryFare",
      "tollFee",
      "airportFee",
      "pendingAmount",
      "roundingAmount",
      "totalBill",
      "amountInDriver",
      // "siteCommission",
      "refundtoWallet",
      "professionalEarnings",
    ];
  }
  const {
    INVALID_BOOKING,
    TRIP_FEE,
    WAITING_CHARGE,
    SUR_CHARGE,
    TIPS_AMOUNT,
    SERVICE_TAX,
    COUPON_SAVINGS,
    PENDING_AMOUNT,
    TOTAL_BILL,
    TOLL_FEE,
    AIRPORT_FEE,
    PROFESSIONAL_COMMISSION,
    SOMETHING_WENT_WRONG,
    DISCOUNT_APPLIED,
    EARNINGS,
    CASH_IN_HAND,
    REFUND_TO_WALLET,
    INFO_INTERCITY_RIDE,
    INFO_SITE_COMMISSION,
    INFO_ROUNDING,
    INFO_ASSISTANCE_CARE_FEE,
    INFO_BASE_FARE,
    INFO_TIME_FARE,
    INFO_DISTANCE_FARE,
    INFO_PEAK_FARE,
    INFO_NIGHT_FARE,
  } = notifyMessage.setNotifyLanguage(languageCode || context.params.langCode);
  const bookingData = await this.adapter.model
    .findOne({
      "_id": context.params.bookingId,
    })
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();

  // first assign basic detailes
  const responseJson = {
    "bookingId": bookingData.bookingId,
    "bookingType": bookingData.bookingType,
    "bookingDate": bookingData.bookingDate,
    "payment": bookingData.payment,
    "fareSummary": [],
  };

  // Waiting Charge
  const waitingCharge =
    bookingData.invoice.beforeRideWaitingCharge +
    bookingData.invoice.afterRideWaitingCharge;
  // userData

  //------------------
  const minimumChargeStatus =
    bookingData?.invoice?.minimumChargeStatus || false;
  const minimumChargeAmount = bookingData.invoice.minimumChargeAmount;
  const baseFare = bookingData.invoice.baseFare;
  const timeFare = bookingData.invoice.timeFare;
  const distanceFare = bookingData.invoice.distanceFare || 0;
  const peakFareCharge = bookingData.invoice?.peakFareCharge || 0;
  const nightFareCharge = bookingData.invoice?.nightFareCharge || 0;
  const multiStopCharge = bookingData.invoice?.multiStopCharge || 0;
  const travelCharge = bookingData.invoice.travelCharge;
  const serviceTax = bookingData.invoice.serviceTax;
  const currencyCode = bookingData.currencyCode;
  const currencySymbol = bookingData.currencySymbol;
  const surchargeFee = bookingData.invoice.surchargeFee;
  const tipsAmount = bookingData.invoice.tipsAmount;
  const couponAmount = bookingData.invoice.couponAmount;
  const tollFareAmount = bookingData.invoice.tollFareAmount;
  const airportFareAmount = bookingData.invoice.airportFareAmount;
  const pendingPaymentAmount = bookingData.invoice.pendingPaymentAmount;
  const payableAmount = bookingData.invoice.payableAmount;
  const professionalCommision = bookingData.invoice.professionalCommision;
  const couponApplied = bookingData.invoice.couponApplied;
  const isProfessionalTolerance = bookingData.invoice.isProfessionalTolerance;
  const intercityBoundaryFare = bookingData.invoice.intercityBoundaryFare;
  // + bookingData.invoice.intercityPickupCharge;
  const siteCommissionData = bookingData.invoice.siteCommission;
  const discountAmount = bookingData.invoice.discountAmount;
  const assistanceCareAmount = bookingData.invoice.assistanceCareAmount;
  //-------------------
  const isWaitingChargeApplied =
    bookingData.invoice.isBeforeRideWaitingCharge === true ||
    bookingData.invoice.isAfterRideWaitingCharge === true
      ? true
      : false;
  const const_intercityride = constantUtil.CONST_INTERCITYRIDE.toLowerCase(); //Must be Lowercase
  //------------------
  let tripFee = 0;

  reciptFields.map((field, index) => {
    const orderNo = index + 1;
    {
      switch (field) {
        case "baseFare":
          responseJson.fareSummary.push({
            "title": INFO_BASE_FARE,
            "key": "baseFare",
            "value": baseFare,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": false,
            "orderBy": orderNo,
          });
          break;

        case "timeFare":
          responseJson.fareSummary.push({
            "title": INFO_TIME_FARE,
            "key": "timeFare",
            "value": timeFare,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": false,
            "orderBy": orderNo,
          });
          break;

        case "distanceFare":
          responseJson.fareSummary.push({
            "title": INFO_DISTANCE_FARE,
            "key": "distanceFare",
            "value": distanceFare,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": false,
            "orderBy": orderNo,
          });
          break;

        case "peakFareCharge":
          if (peakFareCharge > 0) {
            responseJson.fareSummary.push({
              "title": INFO_PEAK_FARE,
              "key": "peakFareCharge",
              "value": peakFareCharge,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "nightFareCharge":
          if (nightFareCharge) {
            responseJson.fareSummary.push({
              "title": INFO_NIGHT_FARE,
              "key": "nightFareCharge",
              "value": nightFareCharge,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "multiStopCharge":
          if (multiStopCharge) {
            responseJson.fareSummary.push({
              "title": INFO_NIGHT_FARE,
              "key": "multiStopCharge",
              "value": multiStopCharge,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "tripFeeUser":
          tripFee = baseFare + travelCharge;
          if (minimumChargeStatus === true && minimumChargeAmount > tripFee) {
            tripFee = minimumChargeAmount;
          }
          // Intercity Boundary Fare
          if (bookingData.serviceCategory === const_intercityride) {
            tripFee = tripFee + intercityBoundaryFare;
          }
          // Waiting Charge
          if (isWaitingChargeApplied && waitingCharge > 0) {
            tripFee = tripFee - waitingCharge;
          }

          responseJson.fareSummary.push({
            "title": TRIP_FEE, //"Trip Fee",
            "key": "tripFee",
            "value": tripFee,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": true, // false,
            "orderBy": orderNo,
          });
          break;

        case "tripFeeProfessional":
          tripFee = baseFare + travelCharge + serviceTax;
          if (minimumChargeStatus === true && minimumChargeAmount > tripFee) {
            tripFee = minimumChargeAmount + serviceTax;
          }
          // Coupon Applied
          if (
            couponApplied === true &&
            couponAmount > 0 &&
            isProfessionalTolerance === true
          ) {
            tripFee = tripFee - couponAmount;
          }
          // Toll Fare to Driver / Professional
          if (
            tollFareAmount > 0 &&
            generalSettings.data.isTollToDriver === true
          ) {
            tripFee = tripFee + tollFareAmount;
          }
          // Airport Fare
          if (airportFareAmount > 0) {
            tripFee = tripFee + airportFareAmount;
          }
          // Intercity Boundary Fare to Driver / Professional
          if (
            intercityBoundaryFare > 0 &&
            bookingData.serviceCategory === const_intercityride &&
            generalSettings?.data?.isEnableIntercityBoundaryFareToProfessional
          ) {
            tripFee = tripFee + intercityBoundaryFare;
          }
          // Waiting Charge
          if (isWaitingChargeApplied && waitingCharge > 0) {
            tripFee = tripFee - waitingCharge;
          }
          // Discount
          if (discountAmount > 0) {
            tripFee = tripFee - discountAmount;
          }

          responseJson.fareSummary.push({
            "title": TRIP_FEE, //"Trip Fee",
            "key": "tripFee",
            "value": tripFee,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": false,
            "orderBy": orderNo,
          });
          break;

        case "WaitingCharge":
          if (isWaitingChargeApplied && waitingCharge > 0) {
            responseJson.fareSummary.push({
              "title": WAITING_CHARGE, //"Waiting Charge",
              "key": "waitingCharge",
              "value": waitingCharge,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "assistanceCareAmount":
          if (assistanceCareAmount > 0) {
            responseJson.fareSummary.push({
              "title": INFO_ASSISTANCE_CARE_FEE,
              "key": "assistanceCareAmount",
              "value": assistanceCareAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "surCharge":
          // if (!!surchargeFee === true) {
          if (surchargeFee > 0) {
            responseJson.fareSummary.push({
              "title": bookingData?.invoice?.surchargeTitle || SUR_CHARGE,
              "key": "surCharge",
              "value": surchargeFee,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "serviceTax":
          if (serviceTax > 0) {
            responseJson.fareSummary.push({
              "title": SERVICE_TAX,
              "key": "serviceTax",
              "value": serviceTax,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "tipsAmount":
          if (tipsAmount > 0) {
            responseJson.fareSummary.push({
              "title": TIPS_AMOUNT,
              "key": "tipsAmount",
              "value": tipsAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "couponSavings":
          if (bookingData.invoice.couponApplied === true && couponAmount > 0) {
            responseJson.fareSummary.push({
              "title": COUPON_SAVINGS,
              "key": "couponSavings",
              "value": -couponAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "tollFee":
          if (tollFareAmount > 0) {
            responseJson.fareSummary.push({
              "title": TOLL_FEE,
              "key": "tollFee",
              "value": tollFareAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "airportFee":
          if (airportFareAmount > 0) {
            responseJson.fareSummary.push({
              "title": AIRPORT_FEE,
              "key": "airportFee",
              "value": airportFareAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "pendingAmount":
          if (pendingPaymentAmount > 0) {
            responseJson.fareSummary.push({
              "title": PENDING_AMOUNT, //"Pending Amount",
              "key": "pendingAmount",
              "value": pendingPaymentAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
        case "totalBill":
          responseJson.fareSummary.push({
            "title": TOTAL_BILL, //"Total Bill",
            "key": "totalBill",
            "value": payableAmount >= 0 ? payableAmount : 0,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": true,
            "orderBy": orderNo,
          });
          break;
        case "discountApplied":
          if (
            bookingData.invoice.couponApplied === true &&
            couponAmount > 0 &&
            bookingData.invoice.isProfessionalTolerance === false
          ) {
            responseJson.fareSummary.push({
              "title": DISCOUNT_APPLIED,
              "key": "discountApplied",
              "value": -couponAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
        case "intercityBoundaryFare":
          if (
            intercityBoundaryFare > 0 &&
            bookingData.serviceCategory === const_intercityride &&
            !generalSettings?.data?.isEnableIntercityBoundaryFareToProfessional
          ) {
            responseJson.fareSummary.push({
              "title": INFO_INTERCITY_RIDE,
              "key": "intercityBoundaryFare",
              "value": intercityBoundaryFare,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
        case "siteCommission":
          if (siteCommissionData > 0) {
            responseJson.fareSummary.push({
              "title": INFO_SITE_COMMISSION,
              "key": "siteCommission",
              "value": siteCommissionData,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
        case "professionalCommission":
        case "professionalEarnings":
          responseJson.fareSummary.push({
            "title":
              field === "professionalCommission"
                ? PROFESSIONAL_COMMISSION //"Professional Commission",
                : EARNINGS, //"Your Earnings",
            "key": field, // "professionalCommission" (or) professionalEarnings,
            "value": bookingData.invoice.professionalCommision,
            "currencyCode": currencyCode,
            "currencySymbol": currencySymbol,
            "isBold": field === "professionalCommission" ? false : true,
            "orderBy": orderNo,
          });
          break;
        // case "professionalEarnings":
        //   responseJson.fareSummary.push({
        //     "title": EARNINGS, // "Your Earnings",
        //     "key": "professionalEarnings",
        //     "value": bookingData.invoice.professionalCommision,
        //     "currencyCode": currencyCode,
        //     "currencySymbol": currencySymbol,
        //     "isBold": true,
        //     "orderBy": 7,
        //   });
        //   break;
        case "amountInDriver":
          if (bookingData.invoice.amountInDriver) {
            responseJson.fareSummary.push({
              "title": CASH_IN_HAND, // "Cash In Hand",
              "key": "amountInDriver",
              "value": bookingData.invoice.amountInDriver,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
        case "refundtoWallet":
          if (payableAmount < professionalCommision) {
            responseJson.fareSummary.push({
              "title": REFUND_TO_WALLET,
              "key": "refundtoWallet",
              "value": payableAmount - professionalCommision,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "roundingAmount":
          if (bookingData.invoice.roundingAmount) {
            responseJson.fareSummary.push({
              "title": INFO_ROUNDING,
              "key": "roundingAmount",
              "value": bookingData.invoice.roundingAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;

        case "discountAmount":
          if (discountAmount > 0) {
            responseJson.fareSummary.push({
              "title": "Discount", // INFO_DISCOUNT,
              "key": "discountAmount",
              "value": discountAmount,
              "currencyCode": currencyCode,
              "currencySymbol": currencySymbol,
              "isBold": false,
              "orderBy": orderNo,
            });
          }
          break;
      }
    }
  });

  return { "code": 200, "data": responseJson, "message": "" };
};

// VIEW BOOKING RECIPT
// bookingAction.viewRecipt_BackUP = async function (context) {
//   const userType = context.params.userType.toUpperCase();
//   let languageCode;
//   if (userType === constantUtil.USER) {
//     languageCode = context.meta?.userDetails?.languageCode;
//   } else if (userType === constantUtil.PROFESSIONAL) {
//     languageCode = context.meta?.professionalDetails?.languageCode;
//   }
//   const {
//     INVALID_BOOKING,
//     TRIP_FEE,
//     WAITING_CHARGE,
//     SUR_CHARGE,
//     TIPS_AMOUNT,
//     SERVICE_TAX,
//     COUPON_SAVINGS,
//     PENDING_AMOUNT,
//     TOTAL_BILL,
//     TOLL_FEE,
//     PROFESSIONAL_COMMISSION,
//     SOMETHING_WENT_WRONG,
//     DISCOUNT_APPLIED,
//     EARNINGS,
//     CASH_IN_HAND,
//     REFUND_TO_WALLET,
//   } = notifyMessage.setNotifyLanguage(languageCode || context.params.langCode);
//   let bookingData = await this.adapter.model.findOne({
//     "_id": context.params.bookingId,
//   });

//   if (!bookingData) throw new MoleculerError(INVALID_BOOKING);
//   bookingData = bookingData.toJSON();

//   // first assign basic detailes
//   const responseJson = {
//     "bookingId": bookingData.bookingId,
//     "bookingType": bookingData.bookingType,
//     "bookingDate": bookingData.bookingDate,
//     "payment": bookingData.payment,
//     "fareSummary": [],
//   };

//   // Waiting Charge
//   const waitingCharge =
//     bookingData.invoice.beforeRideWaitingCharge +
//     bookingData.invoice.afterRideWaitingCharge;
//   // userData

//   if (
//     userType === constantUtil.USER //&&
//     //context.meta.userId?.toString() === bookingData.user?.toString()
//   ) {
//     // below is code step listern carefully response based on code exution

//     // these key are same in professioal and user
//     if (
//       bookingData.invoice.minimumChargeStatus === true &&
//       bookingData.invoice.minimumChargeAmount >
//         bookingData.invoice.baseFare + bookingData.invoice.travelCharge
//     ) {
//       responseJson.fareSummary.push({
//         "title": TRIP_FEE, //"Trip Fee",
//         "key": "tripFee",
//         "value": bookingData.invoice.minimumChargeAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 1,
//       });
//     } else {
//       responseJson.fareSummary.push({
//         "title": TRIP_FEE, //"Trip Fee",
//         "key": "tripFee",
//         "value":
//           bookingData.invoice.baseFare + bookingData.invoice.travelCharge,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 1,
//       });
//     }
//     // Waiting Charge
//     if (
//       (bookingData.invoice.isBeforeRideWaitingCharge === true ||
//         bookingData.invoice.isAfterRideWaitingCharge === true) &&
//       waitingCharge > 0
//     ) {
//       responseJson.fareSummary.push({
//         "title": WAITING_CHARGE,
//         "key": "waitingCharge",
//         "value": waitingCharge,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 1,
//       });
//     }
//     // SUR CHARGE
//     if (!!bookingData.invoice?.surchargeFee === true) {
//       responseJson.fareSummary.push({
//         "title": bookingData?.invoice?.surchargeTitle || SUR_CHARGE,
//         "key": "surCharge",
//         "value": bookingData.invoice.surchargeFee,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 2,
//       });
//     }
//     // service tax
//     responseJson.fareSummary.push({
//       "title": SERVICE_TAX,
//       "key": "serviceTax",
//       "value": bookingData.invoice.serviceTax,
//       "currencyCode": bookingData.currencyCode,
//       "currencySymbol": bookingData.currencySymbol,
//       "isBold": false,
//       "orderBy": 2,
//     });
//     // tips amount
//     if (!!bookingData.invoice.tipsAmount === true) {
//       responseJson.fareSummary.push({
//         "title": TIPS_AMOUNT,
//         "key": "tipsAmount",
//         "value": bookingData.invoice.tipsAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 3,
//       });
//     }
//     // couponAmont
//     if (
//       bookingData.invoice.couponApplied === true &&
//       !!bookingData.invoice.couponAmount === true
//     ) {
//       responseJson.fareSummary.push({
//         "title": COUPON_SAVINGS,
//         "key": "couponSavings",
//         "value": -bookingData.invoice.couponAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 4,
//       });
//     }
//     // toll
//     if (!!bookingData.invoice.tollFareAmount === true) {
//       //
//       responseJson.fareSummary.push({
//         "title": TOLL_FEE,
//         "key": "tollFee",
//         "value": bookingData.invoice.tollFareAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 5,
//       });
//     }
//     // pending amount
//     if (bookingData.invoice.pendingPaymentAmount) {
//       responseJson.fareSummary.push({
//         "title": PENDING_AMOUNT, //"Pending Amount",
//         "key": "pendingAmount",
//         "value": bookingData.invoice.pendingPaymentAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 7,
//       });
//     }

//     // finally paybable Amount
//     responseJson.fareSummary.push({
//       "title": TOTAL_BILL, //"Total Bill",
//       "key": "totalBill",
//       "value": bookingData.invoice.payableAmount,
//       "currencyCode": bookingData.currencyCode,
//       "currencySymbol": bookingData.currencySymbol,
//       "isBold": true,
//       "orderBy": 8,
//     });
//   }

//   if (
//     userType === constantUtil.PROFESSIONAL //&&
//     // context.meta.professionalId?.toString() ===
//     //   bookingData.professional?.toString()
//   ) {
//     if (
//       bookingData.invoice.minimumChargeStatus === true &&
//       bookingData.invoice.minimumChargeAmount >
//         bookingData.invoice.baseFare + bookingData.invoice.travelCharge
//     ) {
//       responseJson.fareSummary.push({
//         "title": TRIP_FEE, //"Trip Fee",
//         "key": "tripFee",
//         "value":
//           bookingData.invoice.minimumChargeAmount +
//           bookingData.invoice.serviceTax,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 1,
//       });
//     } else {
//       responseJson.fareSummary.push({
//         "title": TRIP_FEE, //"Trip Fee",
//         "key": "tripFee",
//         "value":
//           bookingData.invoice.baseFare +
//           bookingData.invoice.travelCharge +
//           bookingData.invoice.serviceTax,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 1,
//       });
//     }
//     // // service tax
//     // responseJson.fareSummary.push({
//     //   'title': 'Service Tax',
//     //   'key': 'serviceTax',
//     //   'value': bookingData.invoice.serviceTax,
//     //   'currencyCode': bookingData.currencyCode,
//     //   'currencySymbol': bookingData.currencySymbol,
//     //   'isBold': false,
//     //   'orderBy': 2,
//     // })
//     // Waiting Charge
//     if (
//       (bookingData.invoice.isBeforeRideWaitingCharge === true ||
//         bookingData.invoice.isAfterRideWaitingCharge === true) &&
//       waitingCharge > 0
//     ) {
//       responseJson.fareSummary.push({
//         "title": WAITING_CHARGE,
//         "key": "waitingCharge",
//         "value": waitingCharge,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 2,
//       });
//     }
//     // SUR CHARGE
//     if (!!bookingData.invoice.surchargeFee === true) {
//       responseJson.fareSummary.push({
//         "title": bookingData?.invoice?.surchargeTitle || SUR_CHARGE,
//         "key": "surCharge",
//         "value": bookingData.invoice.surchargeFee,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 2,
//       });
//     }

//     // professional commission
//     responseJson.fareSummary.push({
//       "title": PROFESSIONAL_COMMISSION, //"Professional Commission",
//       "key": "professionalCommission",
//       "value": bookingData.invoice.professionalCommision,
//       "currencyCode": bookingData.currencyCode,
//       "currencySymbol": bookingData.currencySymbol,
//       "isBold": false,
//       "orderBy": 3,
//     });

//     // coupon added if poressional tolerence
//     // need general Settings
//     const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//     if (!generalSettings) {
//       throw new MoleculerError(SOMETHING_WENT_WRONG + "/REDIS");
//     }
//     //
//     //  if driver tollerence applied substract the trip amount
//     if (
//       bookingData.invoice.couponApplied === true &&
//       !!bookingData.invoice.couponAmount === true &&
//       bookingData.invoice.isProfessionalTolerance === true
//     ) {
//       responseJson.fareSummary = responseJson.fareSummary.map((summery) => {
//         if (summery.key === "tripFee") {
//           summery.value = summery.value - bookingData.invoice.couponAmount;
//         }
//         return summery;
//       });
//     }

//     // coupon amount
//     if (
//       bookingData.invoice.couponApplied === true &&
//       !!bookingData.invoice.couponAmount === true &&
//       bookingData.invoice.isProfessionalTolerance === false
//     ) {
//       responseJson.fareSummary.push({
//         "title": DISCOUNT_APPLIED,
//         "key": "discountApplied",
//         "value": -bookingData.invoice.couponAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 2,
//       });
//     }

//     //toll
//     if (
//       generalSettings.data.isTollToDriver === true &&
//       bookingData.invoice.tollFareAmount
//     ) {
//       responseJson.fareSummary.push({
//         "title": TOLL_FEE,
//         "key": "toll",
//         "value": bookingData.invoice.tollFareAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 3,
//       });
//     }
//     // toll false
//     if (
//       generalSettings.data.isTollToDriver === false &&
//       bookingData.invoice.tollFareAmount
//     ) {
//       // add  toll amount to trip Fee
//       responseJson.fareSummary = responseJson.fareSummary.map((summery) => {
//         if (summery.key === "tripFee") {
//           summery.value = summery.value + bookingData.invoice.tollFareAmount;
//         }
//         return summery;
//       });
//     }
//     // pending amount
//     if (bookingData.invoice.pendingPaymentAmount) {
//       responseJson.fareSummary.push({
//         "title": PENDING_AMOUNT, //"Pending Amount",
//         "key": "pendingAmount",
//         "value": bookingData.invoice.pendingPaymentAmount,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 5,
//       });
//     }
//     // payable Amount
//     responseJson.fareSummary.push({
//       "title": TOTAL_BILL, //"Total Bill",
//       "key": "totalBill",
//       "value": bookingData.invoice.payableAmount,
//       "currencyCode": bookingData.currencyCode,
//       "currencySymbol": bookingData.currencySymbol,
//       "isBold": true,
//       "orderBy": 6,
//     });

//     // professional Earnings
//     responseJson.fareSummary.push({
//       "title": EARNINGS, // "Your Earnings",
//       "key": "professionalEarnings",
//       "value": bookingData.invoice.professionalCommision,
//       "currencyCode": bookingData.currencyCode,
//       "currencySymbol": bookingData.currencySymbol,
//       "isBold": true,
//       "orderBy": 7,
//     });
//     // amount in driver

//     if (bookingData.invoice.amountInDriver) {
//       responseJson.fareSummary.push({
//         "title": CASH_IN_HAND, // "Cash In Hand",
//         "key": "amountInDriver",
//         "value": bookingData.invoice.amountInDriver,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 8,
//       });
//     }

//     // reimpurseable amount
//     if (
//       bookingData.invoice.payableAmount <
//       bookingData.invoice.professionalCommision
//     ) {
//       responseJson.fareSummary.push({
//         "title": REFUND_TO_WALLET,
//         "key": "refundtoWallet",
//         "value":
//           bookingData.invoice.payableAmount -
//           bookingData.invoice.professionalCommision,
//         "currencyCode": bookingData.currencyCode,
//         "currencySymbol": bookingData.currencySymbol,
//         "isBold": false,
//         "orderBy": 9,
//       });
//     }
//   }
//   // order the fare summery
//   responseJson["fareSummary"].sort(function (a, b) {
//     return a.orderBy - b.orderBy;
//   });
//   // return
//   return { "code": 200, "data": responseJson, "message": "" };
// };

bookingAction.scheduleBookingCategoryChange = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const updateData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.bookingId),
      //"clientId": mongoose.Types.ObjectId(clientId),
      "bookingStatus": constantUtil.AWAITING,
      "bookingType": constantUtil.SCHEDULE,
    },
    {
      "isAcceptOtherCategory": true,
    }
  );
  if (!updateData.nModified) {
    throw new MoleculerError("ERROR IN UPDATE CATEGORY");
  } else {
    return { "code": 200, "data": {}, "message": "UPDATED SUCCESSFULLY" };
  }
};

bookingAction.getBookingDetail = async function (context) {
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  //  #endregion Get Config Settings
  // const bookingData = await this.adapter.model
  //   .findOne({
  //     "_id": context.params.bookingId,
  //   })
  //   .lean();
  const bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId.toString(),
  }); // Get booking Details
  if (!bookingData) {
    throw new MoleculerError("ERROR IN BOOKING DETAILS");
  }
  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "clientId": clientId,
    "id": bookingData.category.toString(),
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];

  bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
  bookingData.trackLink = `${generalSettings.data.redirectUrls.trackBooking}${bookingData.bookingId}`;
  bookingData["category"]["isNeedSecurityImageUpload"] =
    serviceCategoryData?.isNeedSecurityImageUpload || false;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;
  //
  return { "code": 200, "data": bookingObject(bookingData), "message": "" };
};

bookingAction.getBookingDetailById = async function (context) {
  let responseData = {},
    bookingData = {};
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  try {
    bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details
    if (!bookingData) {
      throw new MoleculerError("ERROR IN BOOKING DETAILS", 500);
    }
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    vehicleCategoryData =
      vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];
    responseData = bookingData;
    // responseData.vehicle["vehicleCategory"] = vehicleCategoryData;
    responseData.vehicle["vehicleCategoryName"] =
      vehicleCategoryData?.vehicleCategory || "";
    responseData.vehicle["vehicleCategoryImage"] =
      vehicleCategoryData?.categoryImage || "";
    responseData.vehicle["vehicleCategoryMapImage"] =
      vehicleCategoryData?.categoryMapImage || "";
    responseData.trackLink = `${generalSettings.data.redirectUrls.trackBooking}${bookingData.bookingId}`;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;
  } catch (e) {
    responseData = {};
  }
  if (context.params.action?.toUpperCase() === constantUtil.CONST_MANUALWAY) {
    return {
      "code": 200,
      "data": {
        "_id": responseData?._id?.toString(),
        "bookingId": responseData.bookingId,
        "activity": responseData.activity,
        "wayData": responseData.wayData || [],
        "manualWayData": responseData.manualWayData || [],
        // "snapWayData": responseData.snapWayData || [],
        "manualDistance": responseData.manualDistance || 0,
        "professionalCurrentLocation": responseData.professional?.location
          ?.coordinates || [0, 0],
        "locationUpdatedTime": responseData.professional?.locationUpdatedTime,
      },
      "message": "",
    };
  } else {
    return {
      "code": 200,
      "data": bookingObjectDetails(responseData),
      "message": "",
    };
  }
};

bookingAction.trackBookingDetailsByIdFromWebBookingApp = async function (
  context
) {
  let responseData = {},
    bookingData = {};
  try {
    if (context.params.bookingStatus !== "ENDED") {
      bookingData = await this.adapter.model
        .findOne({
          "_id": context.params.bookingId,
        })
        .lean();
    }
    if (!bookingData) {
      throw new MoleculerError("ERROR IN BOOKING DETAILS", 500);
    } else {
      responseData = bookingObject(bookingData);
    }
  } catch (e) {
    responseData = {};
  }
  return { "code": 200, "data": responseData, "message": "" };
};
bookingAction.trackBookingDetailsByIdForDebug = async function (context) {
  let responseData = {};
  try {
    responseData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId?.toString(),
    });
    if (!responseData) {
      throw new MoleculerError("ERROR IN BOOKING DETAILS", 500);
    }
  } catch (e) {
    responseData = {};
  }
  return { "code": 200, "data": responseData, "message": "" };
};

bookingAction.getCityBasedRidesData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": { "$lte": new Date() },
          // "category": mongoose.Types.ObjectId(context.params.city),
        }
      : {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
          // "category": mongoose.Types.ObjectId(context.params.city),
        };

  const query = [];
  query.push({
    "$match": matchData,
  });
  query.push(
    {
      "$lookup": {
        "from": "categories",
        "localField": "category",
        "foreignField": "_id",
        "as": "category",
      },
    },
    { "$unwind": "$category" },
    {
      "$project": {
        "_id": 1,
        "category": "$category.locationName",
      },
    },
    {
      "$group": {
        "_id": "$category",
        "count": { "$sum": 1 },
      },
    }
  );
  const aggregateData = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  return aggregateData;
};

bookingAction.getGraphBookingData = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": { "$lte": new Date() },
        }
      : {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
        };

  const query = [];
  query.push({
    "$match":
      context.params.city === ""
        ? matchData
        : {
            ...matchData,
            "category": mongoose.Types.ObjectId(context.params.city),
          },
  });
  query.push(
    {
      "$project": {
        "_id": 1,
        "createdAt": {
          "$dateToString": { "format": "%Y-%m-%d", "date": "$createdAt" },
        },
        "bookingDetails": {
          "bookingStatus": "$bookingStatus",
          "bookingType": "$bookingType",
        },
      },
    },
    {
      "$group": {
        "_id": "$createdAt",
        "bookingDetails": {
          "$push": "$bookingDetails",
        },
        "createdAt": { "$first": "$createdAt" },
      },
    }
  );
  const aggregateData = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  const responseJson = [];

  _.forEach(aggregateData, async (data) => {
    const finalData = {
      "date": data.createdAt,
      "data": {
        "totalCount": 0,
        "newCount": 0,
        "scheduleCount": 0,
        "userdenyCount": 0,
        "ongoingCount": 0,
        "expiredCount": 0,
        "cancelCount": 0,
        "endedCount": 0,
      },
    };
    _.map(data.bookingDetails, async (status) => {
      switch (status.bookingStatus) {
        case constantUtil.AWAITING:
          finalData.data.totalCount += 1;
          if (status.bookingType === constantUtil.SCHEDULE) {
            finalData.data.scheduleCount += 1;
          } else {
            finalData.data.newCount += 1;
          }
          break;
        case constantUtil.ACCEPTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERDENY:
          finalData.data.userdenyCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.PROFESSIONALCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.EXPIRED:
          finalData.data.expiredCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.STARTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.ENDED:
          finalData.data.endedCount += 1;
          finalData.data.totalCount += 1;
          break;
      }
    });
    responseJson.push(finalData);
  });

  return responseJson;
};
// Admin Dashboard.................
bookingAction.getAdminDashboardAllRides = async function (context) {
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "createdAt": { "$lte": new Date() },
        }
      : {
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
        };

  const query = [];
  query.push({
    "$match":
      context.params.city === ""
        ? matchData
        : {
            ...matchData,
            "category": mongoose.Types.ObjectId(context.params.city),
          },
  });
  query.push(
    {
      "$project": {
        "_id": 1,
        "createdAt": {
          "$dateToString": { "format": "%Y-%m-%d", "date": "$createdAt" },
        },
        "bookingDetails": {
          "bookingStatus": "$bookingStatus",
          "bookingType": "$bookingType",
        },
      },
    },
    {
      "$group": {
        "_id": "$createdAt",
        "bookingDetails": {
          "$push": "$bookingDetails",
        },
        "createdAt": { "$first": "$createdAt" },
      },
    }
  );
  const aggregateData = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  const responseJson = [];

  _.forEach(aggregateData, async (data) => {
    const finalData = {
      "date": data.createdAt,
      "data": {
        "totalCount": 0,
        "newCount": 0,
        "scheduleCount": 0,
        "userdenyCount": 0,
        "ongoingCount": 0,
        "expiredCount": 0,
        "cancelCount": 0,
        "endedCount": 0,
      },
    };
    _.map(data.bookingDetails, async (status) => {
      switch (status.bookingStatus) {
        case constantUtil.AWAITING:
          finalData.data.totalCount += 1;
          if (status.bookingType === constantUtil.SCHEDULE) {
            finalData.data.scheduleCount += 1;
          } else {
            finalData.data.newCount += 1;
          }
          break;
        case constantUtil.ACCEPTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERDENY:
          finalData.data.userdenyCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.PROFESSIONALCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.EXPIRED:
          finalData.data.expiredCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.STARTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.ENDED:
          finalData.data.endedCount += 1;
          finalData.data.totalCount += 1;
          break;
      }
    });
    responseJson.push(finalData);
  });

  return responseJson;
};
bookingAction.getAvailableProfessionals = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  try {
    const responseData = [];
    //  #region Get Config Settings
    //  #endregion Get Config Settings
    const radius =
      parseInt(generalSettings.data.requestDistance) +
      parseInt(generalSettings.data.retryRequestDistance);
    const checkData = {
      ...context.params,
      "clientId": clientId,
      "vehicleCategoryId": null,
      "professionalOnlineStatus": true,
      "actionType": constantUtil.CONST_ONLINE,
      "rideType": context.params.rideType,
      "radius": context.params?.radius || radius,
      "checkOngoingBooking": false,
      "checkLastPriority": false,
      "forTailRide": generalSettings.data.isTailRideNeeded,
      "isSubCategoryAvailable": true,
      "isForceAppliedToProfessional": true,
      "isGenderAvailable": false,
      "childseatAvailable": false,
      "handicapAvailable": false,
      "isRestrictMaleBookingToFemaleProfessional": false,
      "serviceAreaId": context.params?.serviceAreaId || null,
      "isShareRide": context.params?.isShareRide ?? null, // null --> Default For get Online Professional
      "paymentOption": context.params?.paymentOption || null, // null --> Default For get All PaymentMode Professional
      "languageCode": context.params?.languageCode || null, // null --> Default For get All language Known Professional
      "isPetAllowed": context.params?.isPetAllowed || false,
      "isEnableLuggage": context.params?.isEnableLuggage || false,
    };
    let professionalList = await this.broker.emit(
      // "professional.getProfessionalByLocation",
      "booking.getOnlineProfessionals",
      checkData
    );
    professionalList = professionalList && professionalList[0];
    if (professionalList.length === 0) {
      throw new MoleculerError("SORRY NO PROFESSIONAL AVAILABLE", 500);
    }
    const vehicleCategoryJson = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    // professionalList = professionalList.map((professional) => {
    professionalList?.forEach((professional) => {
      const defaultVehicle = professional.vehicles.filter((vehicle) => {
        vehicle = JSON.parse(JSON.stringify(vehicle));
        return (
          vehicle.defaultVehicle === true &&
          vehicle.status === constantUtil.ACTIVE
        );
      });
      if (
        defaultVehicle[0]?.vehicleCategoryId &&
        parseFloat(professional.wallet.availableAmount) >=
          parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
      ) {
        const vehicalCategoryMetaData =
          vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];
        responseData.push({
          "currentBearing": professional.currentBearing,
          "driverId": professional._id,
          "gender": professional.gender,
          "serviceCategory":
            professional?.bookingInfo?.ongoingBooking?.serviceCategory || null,
          "ongoingBooking":
            professional?.bookingInfo?.ongoingBooking?._id || null,
          "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
          "vehicleCategoryId": defaultVehicle[0].vehicleCategoryId,
          "isSubCategoryAvailable": defaultVehicle[0].isSubCategoryAvailable,
          "applySubCategory": defaultVehicle[0].applySubCategory,
          "subCategoryIds": defaultVehicle[0].subCategoryIds,
          "isGenderAvailable": defaultVehicle[0].isGenderAvailable,
          "childseatAvailable": defaultVehicle[0].childseatAvailable,
          "handicapAvailable": defaultVehicle[0].handicapAvailable,
          "isPetAllowed": defaultVehicle[0].isPetAllowed,
          "isEnableLuggage": defaultVehicle[0].isEnableLuggage,
          // "isHaveShare": vehicalCategoryMetaData?.mandatoryShare || false,
          "isHaveShare": defaultVehicle[0]?.isEnableShareRide || false,
          "isTailRideBooking": professional?.isTailRideBooking || false,
          "vehicleCategoryMapImage":
            vehicalCategoryMetaData?.categoryMapImage || "",
          "lat": professional.location.coordinates[1],
          "lng": professional.location.coordinates[0],
          "serviceType": vehicalCategoryMetaData?.vehicleCategory || "",
          "fullName":
            (professional?.firstName || "") +
            " " +
            (professional?.lastName || ""),
          "phoneNumber":
            (professional?.phone.code || "") +
            " " +
            (professional?.phone.number || ""),
          "avatar": professional?.avatar || "",
          "reviewRating": professional?.review?.avgRating || 0,
          "paymentMode": professional?.paymentMode || [],
          ...professional,
        });
      }
      //  else {
      //   return [];
      // }
    });
    // professionalList = professionalList?.filter((each) => {
    //   if (each?.vehicleCategoryId) {
    //     return each;
    //   }
    // });
    // return professionalList;
    return responseData;
  } catch (e) {
    return null;
  }
};

bookingAction.getOnlineProfessionals = async function (context) {
  const responseData = [];
  //  #region Get Config Settings
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta?.clientId ||
    generalSettings.data.clientId;
  //  #endregion Get Config Settings
  const radius =
    parseInt(generalSettings.data.requestDistance) +
    parseInt(generalSettings.data.retryRequestDistance);
  const checkData = {
    ...context.params,
    "clientId": clientId,
    "vehicleCategoryId": null,
    "professionalOnlineStatus": true,
    "actionType": constantUtil.CONST_ONLINE,
    "rideType": context.params.rideType,
    "radius": context.params?.radius || radius,
    "checkOngoingBooking": false,
    "checkLastPriority": false,
    "forTailRide": generalSettings.data.isTailRideNeeded,
    "isSubCategoryAvailable": true,
    "isForceAppliedToProfessional": true,
    "isGenderAvailable": false,
    "childseatAvailable": false,
    "handicapAvailable": false,
    "isRestrictMaleBookingToFemaleProfessional": false,
    "serviceAreaId": context.params?.serviceAreaId || null,
    "isShareRide": context.params?.isShareRide ?? null, // null --> Default For get Online Professional
    "paymentOption": context.params?.paymentOption || null, // null --> Default For get All PaymentMode Professional
    "languageCode": context.params?.languageCode || null, // null --> Default For get All language Known Professional
    "isPetAllowed": context.params?.isPetAllowed || false,
    "isEnableLuggage": context.params?.isEnableLuggage || false,
  };
  let professionalDataResponse = await this.broker.emit(
    // "professional.getProfessionalByLocation",
    "booking.getOnlineProfessionals",
    checkData
  );
  professionalDataResponse =
    professionalDataResponse && professionalDataResponse[0];

  if (professionalDataResponse?.length === 0)
    return {
      "code": 200,
      "data": responseData,
      "message": "",
    };

  const vehicleCategoryJson = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  // professionalDataResponse = professionalDataResponse.map((professional) => {
  professionalDataResponse?.forEach((professional) => {
    const defaultVehicle = professional.vehicles.filter((vehicle) => {
      vehicle = JSON.parse(JSON.stringify(vehicle));
      return (
        vehicle.defaultVehicle === true &&
        vehicle.status === constantUtil.ACTIVE
      );
    });
    if (
      defaultVehicle[0]?.vehicleCategoryId &&
      parseFloat(professional.wallet.availableAmount) >=
        parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
    ) {
      const vehicalCategoryMetaData =
        vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];
      responseData.push({
        "currentBearing": professional.currentBearing,
        "driverId": professional._id,
        "gender": professional.gender,
        "serviceCategory":
          professional?.bookingInfo?.ongoingBooking?.serviceCategory || null,
        "ongoingBooking":
          professional?.bookingInfo?.ongoingBooking?._id || null,
        "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
        "vehicleCategoryId": defaultVehicle[0].vehicleCategoryId,
        "isSubCategoryAvailable": defaultVehicle[0].isSubCategoryAvailable,
        "applySubCategory": defaultVehicle[0].applySubCategory,
        "subCategoryIds": defaultVehicle[0].subCategoryIds,
        "isGenderAvailable": defaultVehicle[0].isGenderAvailable,
        "childseatAvailable": defaultVehicle[0].childseatAvailable,
        "handicapAvailable": defaultVehicle[0].handicapAvailable,
        "isPetAllowed": defaultVehicle[0].isPetAllowed,
        "isEnableLuggage": defaultVehicle[0].isEnableLuggage,
        // "isHaveShare": vehicalCategoryMetaData?.mandatoryShare || false,
        "isHaveShare": defaultVehicle[0]?.isEnableShareRide || false,
        "isTailRideBooking": professional?.isTailRideBooking || false,
        "vehicleCategoryMapImage":
          vehicalCategoryMetaData?.categoryMapImage || "",
        "lat": professional.location.coordinates[1],
        "lng": professional.location.coordinates[0],
        "serviceType": vehicalCategoryMetaData?.vehicleCategory || "",
        "fullName":
          (professional?.firstName || "") +
          " " +
          (professional?.lastName || ""),
        "avatar": professional?.avatar || "",
        "reviewRating": professional?.review?.avgRating || 0,
        "paymentMode": professional?.paymentMode || [],
        // ...professional,
      });
    }
    // else {
    //   return [];
    // }
  });
  // professionalDataResponse = professionalDataResponse?.filter((each) => {
  //   if (each?.vehicleCategoryId) {
  //     return each;
  //   }
  // });
  return {
    "code": 200,
    // "data": professionalDataResponse,
    "data": responseData,
    "message": "",
  };
};

bookingAction.newRideBookingFromAdmin = async function (context) {
  let responseData,
    errorCode = 200,
    responseMessage = "",
    languageCode;
  const limit = parseInt(context?.params?.limit || 20);
  const skip = parseInt(context?.params?.skip || 0);
  const clientId = context.params.clientId || context.meta.clientId;

  try {
    const bookingBy = (context.params?.bookingBy ?? "").toUpperCase();
    const bookingFrom = (
      context.params?.bookingFrom || constantUtil.ADMIN
    ).toUpperCase();
    let languageCode;
    if (
      bookingBy === constantUtil.USER ||
      bookingBy === constantUtil.CORPORATEOFFICER //Officer / User
    ) {
      languageCode = context.meta?.userDetails?.languageCode;
    } else if (
      bookingBy === constantUtil.ADMIN //Office  / Admin
    ) {
      languageCode = context.meta?.adminDetails?.languageCode;
    }
    const {
      USER_HAVE_RIDE,
      USER_NOT_FOUND,
      USER_WALLET_PAYMENT_INFO,
      VEHICLE_CATEGORY_NOT_FOUND,
      SERVICE_CATEGORY_NOT_FOUND,
      PROFESSIONAL_NOT_FOUND,
      INVALID_BOOKING,
      BOOKING_SCHEDULED_RIDE,
      PROFESSIONAL_NOTIFICATION,
      PROFESSIONAL_NEW_RIDE,
    } = notifyMessage.setNotifyLanguage(
      languageCode || context.params?.langCode
    );
    switch (context.params.type.toUpperCase()) {
      case constantUtil.INSTANT: {
        context.params.bookingType = constantUtil.INSTANT;
        break;
      }
      case constantUtil.SCHEDULE: {
        context.params.bookingType = constantUtil.SCHEDULE;
        break;
      }
      default: {
        break;
      }
    }
    if (context.params.bookingType === constantUtil.INSTANT) {
      const userCheckData = await this.adapter.model.findOne({
        //"clientId": mongoose.Types.ObjectId(clientId),
        "bookingFor.phoneNumber": context.params.bookingFor.phoneNumber,
        "bookingStatus": {
          "$nin": [
            constantUtil.ENDED,
            constantUtil.EXPIRED,
            constantUtil.USERDENY,
            constantUtil.CANCELLED,
            constantUtil.USERCANCELLED,
            constantUtil.PROFESSIONALCANCELLED,
          ],
        },
        "bookingType": constantUtil.INSTANT,
        "bookingDate": { "$gte": setDayStartHours(new Date()) },
      });
      if (userCheckData) {
        throw new MoleculerError(USER_HAVE_RIDE, 500);
      }
    }
    let userData;
    if (context.params.userId) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId.toString(),
      });
      userData = userData && userData[0];
      if (!userData) {
        throw new MoleculerError(USER_NOT_FOUND);
      }
      if (context.params.paymentOption === constantUtil.PAYMENTWALLET) {
        /* @TODO CARD PAYMENT PENDING*/
        if (
          parseFloat(userData.wallet.availableAmount) <
          parseFloat(context.params.estimationAmount)
        ) {
          throw new MoleculerError(USER_WALLET_PAYMENT_INFO);
        }
      }
    }
    //  #region Get Config Settings

    const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
    const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

    //  #endregion Get Config Settings
    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    let serviceCategoryData = await this.broker.emit(
      "category.getCategoryById",
      {
        "id": context.params.categoryId,
        "clientId": clientId,
      }
    );
    serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
    if (!serviceCategoryData) {
      throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
    }
    serviceCategoryData.categoryData.vehicles.map((vehicle) => {
      if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
        vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
      }
    });

    const radius = parseInt(
      context.params?.radius || generalSettings.data.requestDistance
    );
    let professionalRetryData = [];
    if (context.params.bookingType === constantUtil.INSTANT) {
      professionalRetryData = await this.getProfessionalByLocation(
        //clientId,
        context.params.pickUpLat,
        context.params.pickUpLng,
        context.params.dropLat,
        context.params.dropLng,
        context.params.vehicleCategoryId,
        constantUtil.BOOKING, // actionType
        context.params.rideType,
        radius,
        // radius + generalSettings.data.requestDistance, // maxRadius
        true,
        true,
        false,
        vehicleCategoryData.isSubCategoryAvailable,
        vehicleCategoryData.isForceAppliedToProfessional,
        context.params.isGenderAvailable || false,
        context.params.childseatAvailable || false,
        context.params.handicapAvailable || false,
        !!(
          userData?.gender === constantUtil.MALE &&
          serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
        ),
        false, //isRentalSupported Need to Change
        context.params?.serviceAreaId,
        context.params?.isShareRide || false,
        context.params?.paymentOption || null,
        context.params?.languageCode || null,
        context.params?.isPetAllowed || false
      );
      professionalRetryData = professionalRetryData && professionalRetryData[0];

      if (!professionalRetryData?.length) {
        professionalRetryData = await this.getProfessionalByLocation(
          //clientId,
          context.params.pickUpLat,
          context.params.pickUpLng,
          context.params.dropLat,
          context.params.dropLng,
          context.params.vehicleCategoryId,
          constantUtil.BOOKING, // actionType
          context.params.rideType,
          radius + parseInt(generalSettings.data.retryRequestDistance),
          // radius +
          //   generalSettings.data.requestDistance +
          // +parseInt(generalSettings.data.retryRequestDistance), // maxRadius
          false,
          true,
          generalSettings.data.isTailRideNeeded,
          vehicleCategoryData.isSubCategoryAvailable,
          vehicleCategoryData.isForceAppliedToProfessional,
          context.params.isGenderAvailable || false,
          context.params.childseatAvailable || false,
          context.params.handicapAvailable || false,
          !!(
            userData?.gender === constantUtil.MALE &&
            serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
          ),
          false, //isRentalSupported Need to Change
          context.params?.serviceAreaId,
          context.params?.isShareRide || false,
          context.params?.paymentOption || null,
          context.params?.languageCode || null,
          context.params?.isPetAllowed || false
        );
        professionalRetryData =
          professionalRetryData && professionalRetryData[0];
      }
      // if (!professionalRetryData?.length) {
      //   return {
      //     "code": 503,
      //     "data": {},
      //     "message": PROFESSIONAL_NOT_FOUND,
      //   };
      // }
    }

    let newDate = helperUtil.toUTC(new Date().toISOString());
    let newRegionalDate = helperUtil.toRegionalUTC(new Date());
    if (context.params.bookingType === constantUtil.SCHEDULE) {
      newDate = helperUtil.toUTC(context.params.bookingDate);
      newRegionalDate = helperUtil.toRegionalUTC(context.params.bookingDate);
    }
    // ------- algorithm part Start -------
    const requestSendProfessionals = [];
    if (
      professionalRetryData.length > 0 &&
      context.params.isNotify === true &&
      context.params.bookingType === constantUtil.INSTANT &&
      (generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
        generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST)
    ) {
      requestSendProfessionals.push({
        "professionalId": professionalRetryData[0]?._id?.toString(),
        "requestReceiveDateTime": new Date().toISOString(),
        "count": 1,
        "location.coordinates": professionalRetryData[0]?.location?.coordinates,
        "duration": professionalRetryData[0]?.duration,
        "distance": professionalRetryData[0]?.distance,
      });
      professionalRetryData = [professionalRetryData[0]];
    }
    // ------- algorithm part End -------
    context.params["vehicleCategoryData"] = vehicleCategoryData; // Vehicle Category Details
    //--------------------------------------------------------------------
    // let encodedRideCoordinates = null;
    // if (context.params.rawRideCoordinates) {
    //   encodedRideCoordinates = cryptoAESEncode(
    //     context.params.rawRideCoordinates
    //   ); // From Admin Booking
    //   encodedRideCoordinates = encodedRideCoordinates?.toString();
    // }
    const rideCoordinatesEncoded =
      context.params.rideCoordinatesEncoded ||
      context.params.rawRideCoordinates;
    //------------------------
    let bookingData = await this.adapter.model.create({
      "clientId": clientId,
      "bookingId": `${generalSettings.data.bookingPrefix}-${customAlphabet(
        "1234567890",
        9
      )()}`,
      "bookingOTP": customAlphabet("1234567890", 4)().toString(),
      "bookingType": context.params.bookingType,
      "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
      "bookingDate": newDate,
      "regionalData": {
        "bookingDate": newRegionalDate,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
      },
      "bookingFor": context.params.bookingFor,
      "bookingBy": bookingBy,
      "bookingFrom": bookingFrom,
      "tripType": constantUtil.DAILYTRIP,
      "paymentSection": generalSettings.data.paymentSection,
      "admin": context.meta.adminId,
      "user": context.params.userId || null,
      "couponId": null,
      "guestType": context.params.userId
        ? constantUtil.USER
        : constantUtil.GUESTUSER,
      "category": context.params.categoryId,
      "isGenderAvailable": context.params.isGenderAvailable || false,
      "childseatAvailable": context.params.childseatAvailable || false,
      "handicapAvailable": context.params.handicapAvailable || false,
      "isShareRide": false,
      "isPetAllowed": context.params.isPetAllowed || false,
      "isEnableLuggage": context.params?.isEnableLuggage || false,
      "vehicle": {
        "vehicleCategoryId": context.params.vehicleCategoryId,
      },
      "origin": {
        "addressName": context.params.pickUpAddressName,
        "fullAddress": context.params.pickUpFullAddress,
        "shortAddress": context.params.pickUpShortAddress,
        "lat": context.params.pickUpLat,
        "lng": context.params.pickUpLng,
      },
      "destination": {
        "addressName": context.params.dropAddressName,
        "fullAddress": context.params.dropFullAddress,
        "shortAddress": context.params.dropShortAddress,
        "lat": context.params.dropLat,
        "lng": context.params.dropLng,
      },
      "estimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "bookedEstimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "waitingTime": {
        "waitingStartTime": null,
        "waitingEndTime": null,
      },
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceUnit": serviceCategoryData.distanceType,
      "invoice": mappingUtil.calculateInvoiceObject(context.params),
      "serviceCategory": context.params.serviceCategory,
      "activity": {
        "noOfUsersInVehicle": null,
        "noOfSeatsAvailable": null,
        "isUserPickedUp": false,
        "isUserDropped": false,
        "waitingDuration": null,
        "actualBookingTime": new Date(),
        "bookingTime": new Date(),
        "denyOrExpireTime": null,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
        "rideStops": context.params.bookingLocations || [],
      },
      "rideStops": context.params.bookingLocations || [],
      "notes": context.params.notes?.toString("utf8") || null,
      "payment": {
        "option": context.params.paymentOption,
        "details": null,
        "card":
          context.params.paymentOption === constantUtil.PAYMENTCARD
            ? context.params.card
            : {},
        "paid": true,
        "isPrepaidRide": context.params?.isPrepaidRide || false,
      },
      // ---  for alogorithim Start --------
      "requestSendProfessionals": requestSendProfessionals,
      "requestRetryCount": 1,
      // ---  for alogorithim End --------
      "isRestrictMaleBookingToFemaleProfessional":
        !!serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional,
      "pickupLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "dropLocation.coordinates": [
        [context.params.dropLng, context.params.dropLat],
      ],
      "upcomingLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "rideCoordinatesEncoded": rideCoordinatesEncoded,
    });
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    bookingData = await this.getBookingDetailsById({
      "bookingId": bookingData._id.toString(),
    });
    if (context.params.userId) {
      bookingData.user = {
        "clientId": userData.clientId,
        "firstName": userData.firstName,
        "lastName": userData.lastName,
        "phone": userData.phone,
        "avatar": userData.avatar,
        "review": userData.review,
      };
    }
    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    bookingData.vehicle.isShowProfessionalList =
      vehicleCategoryData.isShowProfessionalList;

    bookingData.retryTime = generalSettings.data.driverRequestTimeout;
    bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
    // bookingData.distanceUnit = serviceCategoryData.distanceType;
    bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
    bookingData["category"]["isNeedSecurityImageUpload"] =
      serviceCategoryData?.isNeedSecurityImageUpload || false;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;
    //#region Packages
    if (
      bookingData.serviceCategory === constantUtil.CONST_PACKAGES.toLowerCase()
    ) {
      this.broker.emit("booking.updatePackageDetailsById", {
        "action": constantUtil.ADD,
        "uploadedBy": constantUtil.ADMIN,
        "packageData": context.params.packageData || {},
        "bookingId": bookingData?._id?.toString(),
      });
    }
    //#endregion Packages
    responseData = bookingObject(bookingData);
    const pushNotifyMessage =
      bookingData.serviceCategory === constantUtil.CONST_PACKAGES.toLowerCase()
        ? PROFESSIONAL_NEW_RIDE
        : PROFESSIONAL_NEW_RIDE;
    if (context.params.bookingType === constantUtil.INSTANT) {
      if (professionalRetryData.length > 0) {
        if (context.params.isNotify === true) {
          const notificationObject = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.PROFESSIONAL,
              "action": constantUtil.ACTION_BOOKINGREQUEST,
              "timestamp": Date.now(),
              "message": PROFESSIONAL_NEW_RIDE,
              "details": lzStringEncode(responseData),
            },
            "registrationTokens": professionalRetryData.map((professional) => {
              if (
                // parseFloat(professional.wallet.availableAmount) <
                // parseFloat(
                //   generalSettings.data?.minimumWalletAmountToOnline
                // ) &&
                bookingData.payment.option === constantUtil.WALLET ||
                bookingData.payment.option === constantUtil.PAYMENTCARD ||
                // bookingData.payment.option !== constantUtil.CASH &&
                // bookingData.payment.option !== constantUtil.CREDIT) ||
                parseFloat(professional.wallet.availableAmount) >=
                  parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
              ) {
                return {
                  "token": professional?.deviceInfo[0]?.deviceId || "",
                  "id": professional?._id?.toString(),
                  "deviceType": professional?.deviceInfo[0]?.deviceType || "",
                  "platform": professional?.deviceInfo[0]?.platform || "",
                  "socketId": professional?.deviceInfo[0]?.socketId || "",
                  "duration": professional?.duration,
                  "distance": professional?.distance,
                };
              }
            }),
          };

          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          // this.broker.emit('socket.statusChangeEvent', notificationObject)

          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
        }
        responseMessage = PROFESSIONAL_NOTIFICATION;
      } else {
        if (
          parseInt(context.params.radius) < parseInt(context.params.maxRadius)
        ) {
          errorCode = 600; // 600 --> For Retry Booking Purpose
          responseMessage = PROFESSIONAL_NOTIFICATION;
        } else {
          errorCode = 500;
          responseMessage = PROFESSIONAL_NOT_FOUND;
        }
      }
      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": PROFESSIONAL_NOTIFICATION,
      // };
    } else if (context.params.bookingType === constantUtil.SCHEDULE) {
      let cachedBookingData = await storageUtil.read(constantUtil.SCHEDULE);
      if (!cachedBookingData) {
        cachedBookingData = [];
      }
      cachedBookingData.push({
        "_id": bookingData._id,
        "bookingId": bookingData.bookingId,
        "bookingDate": bookingData.bookingDate,
      });
      storageUtil.write(constantUtil.SCHEDULE, cachedBookingData);

      if (context.params.userId)
        this.broker.emit("user.updateScheduleRide", {
          "userId": context.params.userId,
          "bookingId": bookingData._id.toString(),
          "bookingDate": bookingData.bookingDate,
          "estimationTime": context.params.estimationTime,
        });

      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": BOOKING_SCHEDULED_RIDE,
      // };
      responseMessage = BOOKING_SCHEDULED_RIDE;
    }
    //#region SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE
    if (generalSettings.data?.notifyAdmin?.isEnableNewRideBook) {
      const adminNotificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": constantUtil.ACTION_BOOKINGREQUEST,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_NEW_RIDE,
          "details": responseData,
        },
        "registrationTokens": [{ "userType": constantUtil.ADMIN }],
      };
      this.broker.emit(
        "socket.sendAdminNotificationForRide",
        adminNotificationObject
      );
    }
    //#endregion SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE
    // return {
    //   "code": 200,
    //   "data": bookingObject(bookingData),
    //   "message": responseMessage,
    // };
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

bookingAction.bookingStatusChange = async function (context) {
  const professionalStatus = context.params.professionalStatus.toUpperCase();
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //------------- Notification Msg Start ---------
  const {
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_UPDATE_STATUS_FAILED,
    PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION,
    INFO_OPERATOR_STATUS_ARRIVED,
    INFO_OPERATOR_STATUS_START,
    PROFESSIONAL_RIDE_START_INFO,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  //  #region Get Config Settings
  //  #endregion Get Config Settings
  //------------- Notification Msg End ---------
  let bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .lean();

  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  let vehicleCategoryData;
  const vehicleCategoryDataSet = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryDataSet) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  if (
    (bookingData.bookingStatus === constantUtil.ACCEPTED &&
      professionalStatus === constantUtil.ARRIVED) ||
    (bookingData.bookingStatus === constantUtil.ARRIVED &&
      professionalStatus === constantUtil.STARTED)
  ) {
    bookingData.activity.rideStops = bookingData.activity.rideStops.map(
      (stop) => {
        if (
          professionalStatus === constantUtil.STARTED &&
          stop.type === constantUtil.ORIGIN
        ) {
          stop.status = constantUtil.ENDED;
        }
        return stop;
      }
    );
    const timeData =
      professionalStatus === constantUtil.ARRIVED
        ? {
            "activity.arriveTime": new Date(),
            "regionalData.arriveTime": helperUtil.toRegionalUTC(new Date()),
          }
        : professionalStatus === constantUtil.STARTED
        ? {
            "activity.pickUpTime": new Date(),
            "activity.isUserPickedUp": true,
            "regionalData.pickUpTime": helperUtil.toRegionalUTC(new Date()),
          }
        : "";
    const updateBookingData = await this.adapter.model.updateOne(
      {
        "_id": context.params.bookingId,
        "bookingStatus":
          professionalStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : professionalStatus === constantUtil.STARTED
            ? constantUtil.ARRIVED
            : "",
      },
      {
        "bookingStatus": professionalStatus,
        "activity.rideStops": bookingData.activity.rideStops,
        ...timeData,
      }
    );
    if (!updateBookingData.nModified) {
      throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
    }
    bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();

    // bookingData = bookingData.toJSON();

    vehicleCategoryData =
      vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory || "";
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage || "";
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage || "";
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    const bookingAction =
      bookingData.bookingStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_PROFESSIONALARRIVED
        : bookingData.bookingStatus === constantUtil.STARTED
        ? constantUtil.ACTION_PROFESSIONALSTARTED
        : "";

    // if (bookingData.bookingBy === constantUtil.USER) {
    if (bookingData?.user?._id) {
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_PROFESSIONALARRIVED
              ? PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION
              : PROFESSIONAL_RIDE_START_INFO,
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": [
          {
            "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
            "id": bookingData.user?._id?.toString(),
            "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
            "platform": bookingData.user?.deviceInfo[0]?.platform || "",
            "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
      /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
      // if (generalSettings.data.isEnableWebAppBooking) {
      this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
        "bookingId": bookingData._id,
        "bookingStatus": bookingData.bookingStatus,
        "dateTime": bookingData.updatedAt,
        "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
        "bookingInfo": bookingData?.user?.bookingInfo || {},
      });
      // }
    }
    if (context.meta.adminId) {
      const notificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_TRIPSTATUSCHANGEDBYADMIN,
          "timestamp": Date.now(),
          "message":
            professionalStatus === constantUtil.ARRIVED
              ? INFO_OPERATOR_STATUS_ARRIVED
              : INFO_OPERATOR_STATUS_START,
          "details": lzStringEncode(bookingObject(bookingData)),
        },
        "registrationTokens": [
          {
            "token": bookingData.professional?.deviceInfo[0]?.deviceId || "",
            "id": bookingData.professional?._id?.toString(),
            "deviceType":
              bookingData.professional?.deviceInfo[0]?.deviceType || "",
            "platform": bookingData.professional?.deviceInfo[0]?.platform || "",
            "socketId": bookingData.professional?.deviceInfo[0]?.socketId || "",
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);
    }
  } else {
    vehicleCategoryData =
      vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;
  }

  return { "code": 200, "data": bookingData, "message": "" };
};
// admin change professional status
bookingAction.bookingStatusChangeEnded = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    bookingData;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;

  // const transactionOptions = {
  //   "readConcern": { "level": "snapshot" },
  //   "writeConcern": { "w": "majority" },
  //   "readPreference": "primary",
  // };
  // const session = mongoose.startSession();

  try {
    // session.startTransaction();
    const {
      INVALID_BOOKING,
      VEHICLE_CATEGORY_NOT_FOUND,
      BOOKING_UPDATE_STATUS_FAILED,
      BOOKING_PAID,
      TRIP_END_PROFESSIONAL,
      OPERATOR_STATUS_END,
      REFUND_TO_WALLET,
      INFO_INCENTIVE_CREDIT,
      INFO_REFERREL_CHARGE,
    } = await notifyMessage.setNotifyLanguage(
      context.meta?.professionalDetails?.languageCode
    );
    let snapWayData = [],
      manualWayData = [],
      snapErrorData = {},
      finalWayDataSet = [],
      wayPointsSet = [],
      checkProfessionalRideCount = 0,
      checkUserRideCount = 0;

    const directionsData = {},
      finalEncodedPolyline = [],
      traveledDataSet = {};

    let bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("professional", { "location.coordinates": 1 })
      .lean();
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    let vehicleCategoryData;
    const vehicleCategoryDataSet = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryDataSet) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    let mapLocationData,
      locationData,
      formatted_address = "";
    if (context.params.isDistanceCalculate) {
      // locationData = this.adapter.model.aggregate([
      //   {
      //     "$match": {
      //       "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
      //     },
      //   },
      //   {
      //     "$project": {
      //       "_id": 0,
      //       "snapWayData": { "$slice": ["$snapWayData", -1] },
      //       "finalWayData": { "$slice": ["$finalWayData", -1] },
      //     },
      //   },
      // ]);
      locationData = {
        "snapWayData":
          bookingData.snapWayData[bookingData.snapWayData.length - 1],
        "finalWayData":
          bookingData.finalWayData[bookingData.finalWayData.length - 1],
      };
      mapLocationData = await googleApiUtil.geocode({
        "clientId": clientId,
        "lat":
          locationData?.snapWayData?.lat || locationData?.finalWayData?.lat,
        "lng":
          locationData?.snapWayData?.lng || locationData?.finalWayData?.lng,
      });
      formatted_address = await googleApiUtil.formattedAddress(
        mapLocationData.data.results
      );
    }
    bookingData.activity.rideStops = bookingData.activity.rideStops.map(
      (stop) => {
        if (stop.type === constantUtil.DESTINATION) {
          stop.status = constantUtil.ENDED;
          if (context.params.isDistanceCalculate) {
            stop.lat =
              locationData?.snapWayData?.lat || locationData?.finalWayData?.lat;
            stop.lng =
              locationData?.snapWayData?.lng || locationData?.finalWayData?.lng;
            const address = formatted_address;
            // mapLocationData.data.results[0].formatted_address || null;
            if (address) {
              stop.addressName = address;
              stop.fullAddress = address;
              stop.shortAddress = address;
            }
          }
        }
        if (stop.type === constantUtil.ORIGIN) {
          directionsData["from1"] = [stop.lat + "," + stop.lng];
        }
        if (stop.type === constantUtil.DESTINATION) {
          directionsData["to1"] = [stop.lat + "," + stop.lng];
        }
        return stop;
      }
    );
    if (bookingData.bookingStatus === constantUtil.STARTED) {
      const workedMins = parseInt(
        (new Date() - new Date(bookingData.activity.pickUpTime)) / (1000 * 60)
      );
      //----- check ------
      // const updateBookingDataCheck = await this.adapter.model.updateOne(
      //   {
      //     "_id": context.params.bookingId,
      //     "bookingStatus": constantUtil.STARTED,
      //   },
      //   {
      //     "bookingStatus": constantUtil.ENDED,
      //     "activity.rideStops": bookingData.activity.rideStops,
      //     "activity.endedBy": {
      //       "id": context.meta.adminId.toString(),
      //       "userType": constantUtil.ADMIN,
      //     },
      //     "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
      //   },
      //   { session }
      // );

      const updateBookingData = await this.adapter.model.updateOne(
        {
          "_id": context.params.bookingId,
          "bookingStatus": constantUtil.STARTED,
        },
        {
          "bookingStatus": constantUtil.ENDED,
          "activity.rideStops": bookingData.activity.rideStops,
          "activity.dropLatLng": context.params.latLng,
          "activity.dropTime": new Date(),
          "activity.isUserDropped": true,
          "activity.workedMins": workedMins,
          "activity.endedBy": {
            "id": context.meta.adminId.toString(),
            "userType": constantUtil.ADMIN,
          },
          "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
        }
        // { session }
      );
      if (!updateBookingData.nModified) {
        throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
      }
      checkProfessionalRideCount = await this.adapter.model.count({
        "professional": mongoose.Types.ObjectId(
          bookingData.professional._id.toString()
        ),
        "bookingStatus": constantUtil.ENDED,
      });
      if (bookingData.guestType === constantUtil.USER && bookingData.user) {
        checkUserRideCount = await this.adapter.model.count({
          "user": mongoose.Types.ObjectId(bookingData.user._id.toString()),
          "bookingStatus": constantUtil.ENDED,
        });
      }
      bookingData = await this.adapter.model
        .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
        })
        .populate("category")
        .populate("security")
        .populate("officer", { "password": 0, "preferences": 0 })
        .populate("coorperate")
        .populate("couponId")
        .lean();
      // bookingData = bookingData.toJSON();

      if (bookingData.bookingStatus === constantUtil.ENDED) {
        traveledDataSet["timeDuration"] = parseInt(
          (new Date(bookingData.activity.dropTime) -
            new Date(bookingData.activity.pickUpTime)) /
            1000
        );
        if (traveledDataSet["timeDuration"] < 60) {
          traveledDataSet["timeDuration"] = 60;
        }
        let finalData = {};
        let calculatedData = {};
        const finalWayPolydata = [];
        snapWayData =
          bookingData && bookingData.snapWayData ? bookingData.snapWayData : [];
        manualWayData =
          bookingData && bookingData.manualWayData
            ? bookingData.manualWayData
            : [];
        bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
          context.params?.afterRideWaitingMinsWithGracePeriod || 0;
        // if (
        //   parseInt(dataSet['timeDuration']) <=
        //   parseInt(generalSettings.data.timeDurationThershold)
        // ) {
        //   dataSet['km'] = 0
        //   calculatedData = await helperUtil.calculateEstimateAmount({
        //     'bookingData': bookingData,
        //     'calcType': constantUtil.MINIMUMDISTANCE,
        //     'estimationDistance': dataSet['km'],
        //     'estimationTime': dataSet['timeDuration'],
        //   })
        //   finalData = await helperUtil.endTripCalculation({
        //     'bookingData': bookingData,
        //     'calculatedData': calculatedData,
        //     'estimationDistance': dataSet['km'],
        //     'estimationTime': dataSet['timeDuration'],
        //   })
        // } else {
        traveledDataSet["km"] = 0;
        if (generalSettings.data.distanceCalculateType === constantUtil.ROUTE) {
          if (
            snapWayData &&
            snapWayData !== null &&
            snapWayData !== undefined &&
            Array.isArray(snapWayData) &&
            snapWayData.length > 0
          ) {
            const finalSnapWayData = await helperUtil.snapWayPath(snapWayData);
            snapErrorData = finalSnapWayData.snapErrorData;
            wayPointsSet = finalSnapWayData.finalSnapWayData;
            finalWayDataSet = finalSnapWayData.finalWayDataSet;
            traveledDataSet["km"] = finalSnapWayData.distance;
            // calculatedData = await helperUtil.calculateEstimateAmount({
            //   "bookingData": bookingData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
            // finalData = await helperUtil.endTripCalculation({
            //   "bookingData": bookingData,
            //   "calculatedData": calculatedData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
          } else {
            if (
              manualWayData &&
              manualWayData !== null &&
              manualWayData !== undefined &&
              Array.isArray(manualWayData) &&
              manualWayData.length > 0
            ) {
              const finalMaunalCalc =
                await helperUtil.manualWayDistanceCalculate(manualWayData);
              wayPointsSet = finalMaunalCalc.finalWayPoint;
              finalWayDataSet = [finalMaunalCalc.finalWayPoint];
              traveledDataSet["km"] = finalMaunalCalc.distance;
              // calculatedData = await helperUtil.calculateEstimateAmount({
              //   "bookingData": bookingData,
              //   "estimationDistance": traveledDataSet["km"],
              //   "estimationTime": traveledDataSet["timeDuration"],
              //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              //   "calculatedDropDistance": traveledDataSet["dropDistance"],
              //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              // });
              // finalData = await helperUtil.endTripCalculation({
              //   "bookingData": bookingData,
              //   "calculatedData": calculatedData,
              //   "estimationDistance": traveledDataSet["km"],
              //   "estimationTime": traveledDataSet["timeDuration"],
              //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              //   "calculatedDropDistance": traveledDataSet["dropDistance"],
              //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              // });
            } else {
              traveledDataSet["km"] = bookingData.estimation.distance;
              // calculatedData = await helperUtil.calculateEstimateAmount({
              //   "bookingData": bookingData,
              //   "estimationDistance": traveledDataSet["km"],
              //   "estimationTime": traveledDataSet["timeDuration"],
              //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              //   "calculatedDropDistance": traveledDataSet["dropDistance"],
              //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              // });
              // finalData = await helperUtil.endTripCalculation({
              //   "bookingData": bookingData,
              //   "calculatedData": calculatedData,
              //   "estimationDistance": traveledDataSet["km"],
              //   "estimationTime": traveledDataSet["timeDuration"],
              //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
              //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
              //   "calculatedDropDistance": traveledDataSet["dropDistance"],
              //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
              // });
            }
          }
          calculatedData = await helperUtil.calculateEstimateAmount({
            "clientId": clientId,
            "bookingData": bookingData,
            //
            "tollFareAmount": context.params?.tollFareAmount || 0,
            "airportFareAmount": context.params?.airportFareAmount || 0,
            //
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            "beforeRideAssistanceCareProvidesMins": parseInt(
              context.params.beforeRideAssistanceCareProvidesMins || 0
            ),
            "afterRideAssistanceCareProvidesMins": parseInt(
              context.params.afterRideAssistanceCareProvidesMins || 0
            ),
          });
          finalData = await helperUtil.endTripCalculation({
            "bookingData": bookingData,
            "paymentOption": bookingData.payment.option,
            "calculatedData": calculatedData,
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
          });
        }
        if (
          generalSettings.data.distanceCalculateType === constantUtil.MANUAL
        ) {
          if (
            manualWayData &&
            manualWayData !== null &&
            manualWayData !== undefined &&
            Array.isArray(manualWayData) &&
            manualWayData.length > 0
          ) {
            const finalMaunalCalc = await helperUtil.manualWayDistanceCalculate(
              manualWayData
            );
            wayPointsSet = finalMaunalCalc.finalWayPoint;
            finalWayDataSet = [finalMaunalCalc.finalWayPoint];
            traveledDataSet["km"] = finalMaunalCalc.distance;
            // calculatedData = await helperUtil.calculateEstimateAmount({
            //   "bookingData": bookingData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
            // finalData = await helperUtil.endTripCalculation({
            //   "bookingData": bookingData,
            //   "calculatedData": calculatedData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
          } else {
            traveledDataSet["km"] = bookingData.estimation.distance;
            // calculatedData = await helperUtil.calculateEstimateAmount({
            //   "bookingData": bookingData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
            // finalData = await helperUtil.endTripCalculation({
            //   "bookingData": bookingData,
            //   "calculatedData": calculatedData,
            //   "estimationDistance": traveledDataSet["km"],
            //   "estimationTime": traveledDataSet["timeDuration"],
            //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            //   "calculatedDropDistance": traveledDataSet["dropDistance"],
            //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            // });
          }
          calculatedData = await helperUtil.calculateEstimateAmount({
            "clientId": clientId,
            "bookingData": bookingData,
            //
            "tollFareAmount": context.params?.tollFareAmount || 0,
            "airportFareAmount": context.params?.airportFareAmount || 0,
            //
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            "beforeRideAssistanceCareProvidesMins": parseInt(
              context.params.beforeRideAssistanceCareProvidesMins || 0
            ),
            "afterRideAssistanceCareProvidesMins": parseInt(
              context.params.afterRideAssistanceCareProvidesMins || 0
            ),
          });
          finalData = await helperUtil.endTripCalculation({
            "bookingData": bookingData,
            "paymentOption": bookingData.payment.option,
            "calculatedData": calculatedData,
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
          });
        }

        let paidCalcType = constantUtil.EQUALTOPAID;
        if (finalData.payableAmount < bookingData.invoice.payableAmount) {
          paidCalcType = constantUtil.LESSTHENPAID;
        } else if (
          finalData.payableAmount > bookingData.invoice.payableAmount
        ) {
          paidCalcType = constantUtil.GREATERTHENPAID;
        }
        const staticMapData = {
          "clientId": clientId,
          "km": traveledDataSet["km"],
          "from": directionsData["from1"],
          "to": directionsData["to1"],
          "waypoints": "",
        };

        // const wayValueSet = [];
        // if (finalWayPolydata && finalWayPolydata.length > 0) {
        //   const wayPointsValue = await helperUtil.randomslice(
        //     finalWayPolydata,
        //     30
        //   );
        //   await wayPointsValue.forEach((w) => {
        //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
        //   });
        //   staticMapData["waypoints"] = wayValueSet.join("");
        // } else {
        //   const wayPointsValue = await helperUtil.randomslice(
        //     manualWayData,
        //     30
        //   );
        //   await wayPointsValue.forEach((w) => {
        //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
        //   });
        //   staticMapData["waypoints"] = wayValueSet.join("");
        // }
        const wayValueSet = [];
        if (finalWayPolydata && finalWayPolydata.length > 0) {
          const wayPointsValue = await helperUtil.randomslice(
            finalWayPolydata,
            30
          );
          await wayPointsValue.forEach((w) => {
            wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
          });
          // staticMapData["waypoints"] = wayValueSet.join("");
        } else {
          const wayPointsValue = await helperUtil.randomslice(
            manualWayData,
            30
          );
          await wayPointsValue.forEach((w) => {
            wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
          });
          // staticMapData["waypoints"] = wayValueSet.join("");
        }
        staticMapData["waypoints"] = wayValueSet.join("");
        const value = await helperUtil.getStaticImage(staticMapData);

        const config = {
          "method": "get",
          "url": value,
          "headers": {},
          "responseType": "arraybuffer",
        };

        const imageData = await axios(config);

        let finalImageData = await this.broker.emit(
          "admin.uploadStaticMapImageInSpaces",
          {
            "files": imageData.data,
            "fileName": `static${directionsData["from1"]}Map${directionsData["to1"]}Image`,
          }
        );
        finalImageData = finalImageData && finalImageData[0];
        const updateBookingData = await this.adapter.model.updateOne(
          {
            "_id": context.params.bookingId,
            "bookingStatus": constantUtil.ENDED,
          },
          {
            "hub":
              bookingData.professional?.vehicles[0]?.hub?.toString() || null,
            "finalWayData": finalWayDataSet,
            "totalTollPassed": context.params.totalTollPassed || 0,
            "passedTollList": context.params.passedTollList || [],
            "invoice.isMinimumChargeApplied":
              finalData.isMinimumChargeApplied || false,
            "invoice.serviceTax": finalData.serviceTax || 0,
            "invoice.tollFareAmount": finalData.tollFareAmount || 0,
            "invoice.airportFareAmount": finalData.airportFareAmount || 0,
            "invoice.amountInSite": finalData.amountInSite || 0,
            "invoice.amountInDriver": finalData.amountInDriver || 0,
            "invoice.payableAmount": finalData.payableAmount || 0,
            "invoice.siteCommission": finalData.siteCommission || 0,
            "invoice.travelCharge": finalData.travelCharge || 0,
            "invoice.distanceFare": finalData.distanceFare || 0,
            "invoice.timeFare": finalData.timeFare || 0,
            "invoice.siteCommissionWithoutTax":
              finalData.siteCommissionWithoutTax || 0,
            "invoice.professionalCommision":
              finalData.professionalCommision || 0,
            "invoice.professionalCommisionWithoutTips":
              finalData.professionalCommision || 0,
            "estimation.distance": traveledDataSet["km"] || 0,
            "estimation.time": traveledDataSet["timeDuration"] || 0,
            "estimation.pickupDistance": traveledDataSet["pickupDistance"] || 0,
            "estimation.pickupTime": traveledDataSet["pickupTimeDuration"] || 0,
            "estimation.dropDistance": traveledDataSet["dropDistance"],
            "estimation.dropTime": traveledDataSet["dropTimeDuration"],
            "bookedEstimation.distance": bookingData.estimation.distance,
            "bookedEstimation.time": bookingData.estimation.acceptTime,
            "bookedEstimation.pickupDistance":
              bookingData.estimation.pickupDistance,
            "bookedEstimation.pickupTime": bookingData.estimation.pickupTime,
            "rideMapRouteImage": finalImageData,
            "payment.paid": true,
            "encodedPolyline": finalEncodedPolyline,
            "mapErrorData": "",
            "snapErrorData": snapErrorData,
            "originalWayData": wayPointsSet,
            "distanceCalculateType": generalSettings.data.distanceCalculateType,
            "invoice.peakFareCharge": finalData["peakFareCharge"],
            "invoice.nightFareCharge": finalData["nightFareCharge"],
            "invoice.multiStopCharge": finalData["multiStopCharge"],
            "invoice.waitingCharge": finalData["waitingCharge"],
            "invoice.beforeRideWaitingCharge":
              finalData["beforeRideWaitingCharge"],
            "invoice.beforeRideWaitingMins": finalData["beforeRideWaitingMins"],
            "invoice.afterRideWaitingCharge":
              finalData["afterRideWaitingCharge"],
            "invoice.afterRideWaitingMins": finalData["afterRideWaitingMins"],
            // "trackData": context.params?.trackData || {},
            "invoice.intercityPickupTimeFare":
              finalData["intercityPickupTimeFare"],
            "invoice.intercityPickupDistanceFare":
              finalData["intercityPickupDistanceFare"],
            "invoice.intercityPickupCharge": finalData["intercityPickupCharge"],
            "invoice.intercityDropCharge": finalData["intercityDropCharge"],
            "invoice.roundingAmount": finalData["roundingAmount"],
            "invoice.discountAmount": finalData["discountAmount"],
            //
            "invoice.beforeRideAssistanceCareProvidesMins": parseInt(
              context.params.beforeRideAssistanceCareProvidesMins || 0
            ),
            "invoice.beforeRideAssistanceCareAmount":
              finalData["beforeRideAssistanceCareAmount"],
            "invoice.afterRideAssistanceCareProvidesMins": parseInt(
              context.params.afterRideAssistanceCareProvidesMins || 0
            ),
            "invoice.afterRideAssistanceCareAmount":
              finalData["afterRideAssistanceCareAmount"],
            "invoice.assistanceCareAmount": finalData["assistanceCareAmount"],
          }
        );
        if (!updateBookingData.nModified) {
          throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
        }
        this.broker.emit("professional.updatependingReview", {
          "professionalId": bookingData.professional._id.toString(),
          "bookingId": context.params.bookingId,
        });

        //#region Franchising (site commition to Hub)
        if (generalSettings.data.isEnableFranchising) {
          let hubData = await this.broker.emit(
            "admin.updateHubSiteCommission",
            {
              // "professionalId": bookingData.professional?._id?.toString(),
              "bookingId": bookingData?._id?.toString(),
              "refBookingId": bookingData.bookingId,
              "hubId": bookingData.professional?.vehicles[0]?.hub?.toString(),
              "siteCommissionWithoutTax": finalData["siteCommissionWithoutTax"],
            }
          );
          hubData = hubData && hubData[0];
          console.log(JSON.stringify(hubData));
          await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(context.params.bookingId),
              "bookingStatus": constantUtil.ENDED,
            },
            {
              "invoice.hubSiteCommission": hubData?.hubSiteCommission || 0,
              "invoice.hubSiteCommissionPercentage":
                hubData?.hubSiteCommissionPercentage || 0,
            }
          );
        }
        //#endregion Franchising (site commition to Hub)

        // user payment
        if (bookingData.guestType === constantUtil.USER && bookingData.user) {
          this.broker.emit("user.updatependingReview", {
            "userId": bookingData.user._id.toString(),
            "bookingId": context.params.bookingId,
          });

          if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
            this.broker.emit("user.updateWalletAmountTripEnded", {
              "userId": bookingData.user._id.toString(),
              "amount": parseFloat(finalData.payableAmount.toFixed(2)),
              "transStatus": paidCalcType,
              "paymentMethod": bookingData.payment.option,
            });
            const userAvailableAmount =
              parseFloat(bookingData?.user?.wallet?.availableAmount) +
              parseFloat(bookingData?.user?.wallet?.freezedAmount);
            this.broker.emit("transaction.newBookingTransaction", {
              "clientId": bookingData.clientId.toString(),
              "userId": bookingData.user._id.toString(),
              "amount": finalData.payableAmount.toFixed(2),
              "previousBalance": userAvailableAmount,
              "currentBalance":
                parseFloat(userAvailableAmount) -
                parseFloat(finalData.payableAmount.toFixed(2)),
              "refBookingId": bookingData?.bookingId,
            });
          }
          if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
            // take amount
            await this.broker.emit("transaction.tripEndCardCharge", {
              "userId": bookingData.user._id.toString(),
              "amount": parseFloat(finalData.payableAmount.toFixed(2)),
              "cardId": bookingData.payment.card.cardId.toString(),
              "bookingId": bookingData._id.toString(),
              "refBookingId": bookingData.bookingId,
              "paymentInitId": bookingData.paymentInitId,
              "paymentInitAmount": bookingData.paymentInitAmount,
            });
          }
        }

        // professsional transaction
        if (
          generalSettings.data.paymentSection === constantUtil.CASHANDWALLET
        ) {
          if (
            bookingData.payment.option === constantUtil.PAYMENTWALLET ||
            bookingData.payment.option === constantUtil.PAYMENTCARD
          ) {
            this.broker.emit("professional.updateWalletAmountTripEnded", {
              "professionalId": bookingData.professional._id.toString(),
              "amount": parseFloat(finalData.professionalCommision.toFixed(2)),
              "paymentMethod": bookingData.payment.option,
            });
            this.broker.emit("transaction.professionalBookingEndTransaction", {
              "clientId": bookingData.clientId.toString(),
              "professionalId": bookingData.professional._id.toString(),
              "amount": finalData.professionalCommision.toFixed(2),
              "bookingId": bookingData._id.toString(),
              "refBookingId": bookingData.bookingId,
              "previousBalance":
                bookingData?.professional?.wallet?.availableAmount,
              "currentBalance":
                parseFloat(bookingData?.professional?.wallet?.availableAmount) +
                parseFloat(finalData.professionalCommision.toFixed(2)),
            });
          }
          if (
            bookingData.payment.option === constantUtil.PAYMENTCASH ||
            bookingData.payment.option === constantUtil.CONST_PIX ||
            bookingData.payment.option === constantUtil.CONST_POS ||
            bookingData.payment.option === constantUtil.PAYMENTCREDIT //=>this for corporate beacuse in corpporate payment options use paymentcredit key word
          ) {
            // make transaction
            let paymentType = constantUtil.DEBIT;
            let professionalCommision = (
              parseFloat(finalData.payableAmount) -
              parseFloat(finalData.professionalCommision)
            ).toFixed(2);
            if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
              if (
                generalSettings?.data?.isEnableCorporateRideWalletPay &&
                bookingData.coorperate
              ) {
                const updateCorporateRideWalletPay = "";
                let finalPayableAmount =
                  parseFloat(finalData.payableAmount.toFixed(2)) -
                  parseFloat(bookingData?.invoice?.payableAmount || 0);
                if (finalPayableAmount > 0) {
                  await this.broker.emit("admin.updateWalletAmountTripEnded", {
                    "corporateId": bookingData.coorperate?._id?.toString(),
                    "amount": finalPayableAmount,
                    "paymentType": constantUtil.DEBIT,
                    "paymentMethod": bookingData.payment.option,
                    "bookingId": bookingData._id?.toString(),
                    "refBookingId": bookingData.bookingId,
                    "transactionType": constantUtil.BOOKINGCHARGE,
                    "transactionReason":
                      BOOKING_PAID + " / " + bookingData.bookingId,
                  });
                } else if (finalPayableAmount < 0) {
                  finalPayableAmount =
                    parseFloat(bookingData?.invoice?.payableAmount || 0) -
                    parseFloat(finalData.payableAmount.toFixed(2));
                  await this.broker.emit("admin.updateWalletAmountTripEnded", {
                    "corporateId": bookingData.coorperate?._id?.toString(),
                    "amount": finalPayableAmount,
                    "paymentType": constantUtil.CREDIT,
                    "paymentMethod": bookingData.payment.option,
                    "bookingId": bookingData._id?.toString(),
                    "refBookingId": bookingData.bookingId,
                    "transactionType": constantUtil.BOOKINGCHARGE,
                    "transactionReason":
                      REFUND_TO_WALLET + " / " + bookingData.bookingId,
                  });
                }
              }
              if (
                generalSettings.data
                  .isProfessionalsCorporateSiteCommissionCreditToWallet
              ) {
                paymentType = constantUtil.CREDIT; //need to Revert
                professionalCommision =
                  finalData.professionalCommision.toFixed(2); //need to Revert
              } else {
                // ----------- Zayride Start ------------
                paymentType = constantUtil.DEBIT; //need to Revert
                professionalCommision = (
                  parseFloat(finalData.payableAmount) -
                  parseFloat(finalData.professionalCommision)
                ).toFixed(2); //need to Revert
                // ----------- Zayride End ------------
              }
            }
            if (finalData.payableAmount > finalData.professionalCommision) {
              this.broker.emit("professional.updateWalletAmountTripEnded", {
                "professionalId": bookingData.professional._id.toString(),
                "amount": professionalCommision,
                "paymentMethod": bookingData.payment.option,
              });
              this.broker.emit(
                "transaction.professionalSiteCommissionTransaction",
                {
                  "clientId": bookingData?.clientId?.toString(),
                  "professionalId": bookingData.professional._id.toString(),
                  "amount": professionalCommision,
                  "bookingId": bookingData._id.toString(),
                  "refBookingId": bookingData.bookingId,
                  "paymentType": paymentType,
                  "previousBalance":
                    bookingData?.professional?.wallet?.availableAmount,
                  "currentBalance":
                    parseFloat(
                      bookingData?.professional?.wallet?.availableAmount
                    ) + parseFloat(professionalCommision),
                }
              );
            }

            if (finalData.payableAmount < finalData.professionalCommision) {
              // in cash payment user paied amount for a ride is lesser than professioanl commission so menas some coupons or some discount is applied so have reimpersed this differce amount

              await this.broker.emit(
                "professional.updateWalletAmountTripEnded",
                {
                  "professionalId": bookingData.professional._id.toString(),
                  "amount": parseFloat(
                    finalData.professionalCommision - finalData.payableAmount
                  ).toFixed(2),
                  "paymentMethod": constantUtil.PAYMENTWALLET,
                }
              );
              await this.broker.emit(
                "transaction.professionalTolerenceAmountTransaction",
                {
                  "clientId": bookingData?.clientId?.toString(),
                  "professionalId": bookingData.professional._id.toString(),
                  "amount": parseFloat(
                    finalData.professionalCommision - finalData.payableAmount
                  ).toFixed(2),
                  "bookingId": bookingData._id?.toString(),
                  "refBookingId": bookingData.bookingId,
                  "paymentType": paymentType,
                  "previousBalance":
                    bookingData?.professional?.wallet?.availableAmount,
                  "currentBalance":
                    parseFloat(
                      bookingData?.professional?.wallet?.availableAmount
                    ) +
                    parseFloat(
                      finalData.professionalCommision - finalData.payableAmount
                    ),
                }
              );
            }
          }
        }
        switch (bookingData.serviceCategory?.toLowerCase()) {
          case constantUtil.CONST_SHARERIDE.toLowerCase():
            {
              await this.broker.emit(
                "professional.updateProfessionalShareRide",
                {
                  "actionStatus": constantUtil.ENDED,
                  "bookingId": bookingData?._id?.toString(),
                  "professionalId": bookingData?.professional?._id?.toString(),
                  // "pickUpLat": context.params.pickUpLat,
                  // "pickUpLng": context.params.pickUpLng,
                  // "dropLat": context.params.dropLat,
                  // "dropLng": context.params.dropLng,
                }
              );
              await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
                "actionStatus": constantUtil.CONST_DROP,
                "bookingId": null,
                "professionalId": bookingData.professional._id.toString(),
                "serviceCategory": bookingData.serviceCategory,
                "lastRidePassengerCount": bookingData?.passengerCount,
                "lat":
                  context.params?.latLng?.lat ||
                  bookingData?.professional.location.coordinates[1],
                "lng":
                  context.params?.latLng?.lng ||
                  bookingData?.professional.location.coordinates[0],
                "parentShareRideId": bookingData?.parentShareRideId,
                // "passengerCount":
                //   ongoingShareRideAction === constantUtil.CONST_DROP
                //     ? bookingData?.passengerCount
                //     : 0,
              });
            }
            break;
        }
        // // invite and earning
        // //xp
        // bookingData = await this.adapter.model
        //   .findById(
        //     mongoose.Types.ObjectId(context.params.bookingId.toString())
        //   )
        //   .populate("user", {
        //     "password": 0,
        //     "bankDetails": 0,
        //     "trustedContacts": 0,
        //     "cards": 0,
        //   })
        //   .populate("professional", {
        //     "password": 0,
        //     "cards": 0,
        //     "bankDetails": 0,
        //     "trustedContacts": 0,
        //   })
        //   .populate("category")
        //   .populate("security")
        //   .populate("officer", { "password": 0, "preferences": 0 })
        //   .populate("coorperate")
        //   .populate("couponId")
        //   .lean();

        // Get booking Details
        bookingData = await this.getBookingDetailsById({
          "bookingId": context.params.bookingId.toString(),
        }); // Get booking Details

        // bookingData = bookingData.toJSON();
        vehicleCategoryData =
          vehicleCategoryDataSet[
            bookingData.vehicle.vehicleCategoryId.toString()
          ];

        bookingData.vehicle.vehicleCategoryName =
          vehicleCategoryData.vehicleCategory || "";
        bookingData.vehicle.vehicleCategoryImage =
          vehicleCategoryData.categoryImage || "";
        bookingData.vehicle.vehicleCategoryMapImage =
          vehicleCategoryData.categoryMapImage || "";
        // bookingData.distanceUnit = bookingData.category.distanceType;
        bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
          context.params?.afterRideWaitingMinsWithGracePeriod || 0;
        bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
        bookingData["imageURLPath"] =
          generalSettings.data.spaces.spacesBaseUrl +
          "/" +
          generalSettings.data.spaces.spacesObjectName;

        // invite and earn
        const inviteAndEarnSettings = await storageUtil.read(
          constantUtil.INVITEANDEARN
        );
        // user
        if (bookingData?.user?.referredBy) {
          if (
            parseInt(checkUserRideCount) ===
            inviteAndEarnSettings?.data?.forUser?.rideCount
          ) {
            this.broker.emit("user.inviteAndEarn", {
              "joinerId": bookingData.user._id.toString(),
            });
          } else if (
            parseInt(checkUserRideCount) <
            inviteAndEarnSettings?.data?.forUser?.rideCount
          ) {
            this.broker.emit("user.updateJoinerRideCount", {
              "joinerId": bookingData.user._id.toString(),
            });
          }
          if (
            inviteAndEarnSettings?.data?.forUser?.isEnableReferralRideEarning
          ) {
            const inviterRideCommission =
              ((bookingData.invoice.travelCharge +
                bookingData.invoice.baseFare) /
                100) *
              (inviteAndEarnSettings?.data?.forUser
                ?.referralRideEarningPercentage || 0);
            await this.broker.emit("user.inviterRideEarnings", {
              "inviterUniqueCode": bookingData?.user?.referredBy,
              // "joinerId": bookingData.user._id.toString(),
              "amount": inviterRideCommission.toFixed(2),
              "bookingId": bookingData?._id?.toString(),
              "refBookingId": bookingData.bookingId,
              "transactionReason":
                INFO_INCENTIVE_CREDIT +
                " / " +
                INFO_REFERREL_CHARGE +
                " / " +
                bookingData.bookingId,
            });
          }
        }
        //professional
        if (bookingData?.professional?.referredBy) {
          if (
            parseInt(checkProfessionalRideCount) ===
            inviteAndEarnSettings?.data?.forProfessional?.rideCount
          ) {
            this.broker.emit("professional.inviteAndEarn", {
              "joinerId": bookingData.professional._id.toString(),
            });
          } else if (
            parseInt(checkProfessionalRideCount) <
            inviteAndEarnSettings?.data?.forProfessional?.rideCount
          ) {
            this.broker.emit("professional.updateJoinerRideCount", {
              "joinerId": bookingData.professional._id.toString(),
            });
          }
          if (
            inviteAndEarnSettings?.data?.forProfessional
              ?.isEnableReferralRideEarning
          ) {
            const inviterRideCommission =
              ((bookingData.invoice.travelCharge +
                bookingData.invoice.baseFare) /
                100) *
              (inviteAndEarnSettings?.data?.forProfessional
                ?.referralRideEarningPercentage || 0);
            await this.broker.emit("professional.inviterRideEarnings", {
              "inviterUniqueCode": bookingData?.professional?.referredBy,
              // "joinerId": bookingData.professional._id.toString(),
              "amount": inviterRideCommission.toFixed(2),
              "bookingId": bookingData?._id?.toString(),
              "refBookingId": bookingData.bookingId,
              "transactionReason":
                INFO_INCENTIVE_CREDIT +
                " / " +
                INFO_REFERREL_CHARGE +
                " / " +
                bookingData.bookingId,
            });
          }
        }
        //-------- Send SMS Alert Start --------------------------
        // if (
        //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
        //   bookingData.bookingBy === constantUtil.ADMIN ||
        //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
        //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
        // )
        {
          this.broker.emit("booking.sendSMSAlertByRideId", {
            "rideId": context.params.bookingId.toString(),
            "bookingFrom": bookingData.bookingFrom,
            "notifyType": constantUtil.ENDED,
            "notifyFor": constantUtil.USER,
          });
        }
        //-------- Send SMS Alert End --------------------------
        // if (bookingData.bookingBy === constantUtil.USER) {
        if (bookingData?.user?._id) {
          const notificationObject = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.USER,
              "action": constantUtil.ACTION_BOOKINGENDED,
              "timestamp": Date.now(),
              "message": TRIP_END_PROFESSIONAL,
              "details": lzStringEncode(bookingObject(bookingData)),
            },
            "registrationTokens": [
              {
                "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
                "id": bookingData.user?._id?.toString(),
                "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
                "platform": bookingData.user?.deviceInfo[0]?.platform || "",
                "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
              },
            ],
          };

          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          // this.broker.emit('socket.statusChangeEvent', notificationObject)

          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
          /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
          // if (generalSettings.data.isEnableWebAppBooking) {
          this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
            "bookingId": bookingData._id,
            "bookingStatus": bookingData.bookingStatus,
            "dateTime": bookingData.updatedAt,
            "userAccessToken":
              bookingData.user?.deviceInfo[0]?.accessToken || "",
            "bookingInfo": bookingData?.user?.bookingInfo || {},
          });
          // }
        }
        if (bookingData.professional) {
          const notificationObject = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.PROFESSIONAL,
              "action": constantUtil.ACTION_TRIPSTATUSCHANGEDBYADMIN,
              "timestamp": Date.now(),
              "message": OPERATOR_STATUS_END,
              "details": lzStringEncode(bookingObject(bookingData)),
            },
            "registrationTokens": [
              {
                "token":
                  bookingData.professional?.deviceInfo[0]?.deviceId || "",
                "id": bookingData.professional?._id?.toString(),
                "deviceType":
                  bookingData.professional?.deviceInfo[0]?.deviceType || "",
                "platform":
                  bookingData.professional?.deviceInfo[0]?.platform || "",
                "socketId":
                  bookingData.professional?.deviceInfo[0]?.socketId || "",
              },
            ],
          };
          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          // this.broker.emit('socket.statusChangeEvent', notificationObject)

          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
          // sendEamail

          if (generalSettings.data.emailConfigurationEnable) {
            // SEND TO PROFESSIONAL
            if (
              bookingData?.professional?.email &&
              bookingData?.professional?.isEmailVerified
            ) {
              this.broker.emit("admin.sendServiceMail", {
                "templateName": "professionalInvoiceMailTemplate",
                ...mappingUtil.professionalInvoiceMailObject(bookingData),
              });
            }
            // SEND TO USER
            if (
              bookingData?.user?.email &&
              bookingData?.user?.isEmailVerified
            ) {
              this.broker.emit("admin.sendServiceMail", {
                "templateName": "userInvoiceMailTemplate",
                ...mappingUtil.userInvoiceMailObject(bookingData),
              });
            }
            if (
              bookingData?.coorperate?.data?.email &&
              bookingData?.coorperate?.data?.isEmailVerified
            ) {
              // SEND TO CORPORATE
              this.broker.emit("admin.sendServiceMail", {
                "templateName": "corporateInvoiceMailTemplate",
                ...mappingUtil.corporateInvoiceMailObject(bookingData),
              });
            }
            if (bookingData?.corporateEmailId) {
              // SEND COPY INVOICE TO CORPORATE (For CORPORATE USER Ride)
              this.broker.emit("admin.sendServiceMail", {
                "templateName": "corporateInvoiceMailTemplate",
                ...mappingUtil.corporateInvoiceMailObject(bookingData),
              });
            }
          }
        }
      }
    } else {
      vehicleCategoryData =
        vehicleCategoryDataSet[
          bookingData.vehicle.vehicleCategoryId.toString()
        ];

      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory || "";
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage || null;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage || null;
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
        context.params?.afterRideWaitingMinsWithGracePeriod || 0;
    }
    // await session.commitTransaction();
    return bookingData;
    // return { "code": 200, "data": bookingData, "message": "" };
  } catch (e) {
    // await session.abortTransaction();
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
  }
  // finally {
  //   await session.endSession();
  // }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": bookingData,
  };
};

bookingAction.adminCancelBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //---------- Notification Msg Start ---------
  const {
    INFO_PROFESSIONAL,
    INFO_ADMIN,
    INFO_RIDE_CANCELLED_BY,
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    SOMETHING_WENT_WRONG,
    BOOKING_STATUS_INFO,
    BOOKING_CANCELLED,
    BOOKING_CANCELLED_BY,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  //---------- Notification Msg End ---------
  // let bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();
  let bookingData = await this.getBookingDetailsById({
    "bookingId": context.params.bookingId.toString(),
  }); // Get booking Details
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();
  const paymentInitId = bookingData?.paymentInitId?.toString();
  const paymentInitAmount = bookingData?.paymentInitAmount?.toString();
  const cardId = bookingData?.payment?.card?.cardId?.toString();
  const refBookingId = bookingData?.bookingId?.toString();
  const bookingId = bookingData?._id;

  // vehicle category from redis
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(
      VEHICLE_CATEGORY_NOT_FOUND + "" + SOMETHING_WENT_WRONG + "/REDIS"
    );
  }
  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];
  if (!vehicleCategoryData) {
    throw new MoleculerError(
      VEHICLE_CATEGORY_NOT_FOUND + "" + SOMETHING_WENT_WRONG
    );
  }
  //  service Category
  if (!bookingData.category._id) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  vehicleCategoryData = {
    ...vehicleCategoryData,
    ...bookingData.category.categoryData.vehicles.find(
      (vehicle) =>
        vehicle.categoryId.toString() ===
        bookingData.vehicle.vehicleCategoryId.toString()
    ),
  };
  if (
    // (bookingData.bookingStatus === constantUtil.AWAITING &&
    //   bookingData.bookingType === constantUtil.SCHEDULE) ||
    bookingData.bookingStatus === constantUtil.AWAITING ||
    (bookingData.bookingStatus === constantUtil.ACCEPTED &&
      bookingData.bookingType === constantUtil.SCHEDULE) ||
    bookingData.bookingStatus === constantUtil.ACCEPTED ||
    (bookingData.bookingStatus === constantUtil.ARRIVED &&
      bookingData.bookingType === constantUtil.SCHEDULE) ||
    bookingData.bookingStatus === constantUtil.ARRIVED
  ) {
    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
      },
      {
        "bookingStatus": constantUtil.PROFESSIONALCANCELLED,
        "activity.cancelTime": new Date(),
        "activity.cancelledBy": {
          "id": context.meta.adminId.toString(),
          "userType": constantUtil.ADMIN,
        },
        "invoice.payableAmount": 0,
        "regionalData.cancelTime": helperUtil.toRegionalUTC(new Date()),
      }
    );
    if (!updatedBookingData.nModified) {
      throw new MoleculerError(BOOKING_STATUS_INFO);
    }
    //  professional datas update
    if (bookingData.professional) {
      // update ongoing  ride booking
      switch (bookingData.serviceCategory?.toLowerCase()) {
        case constantUtil.CONST_SHARERIDE.toLowerCase():
          {
            await this.broker.emit("professional.updateProfessionalShareRide", {
              "actionStatus": constantUtil.PROFESSIONALCANCELLED,
              "bookingId": context.params.bookingId,
              "professionalId": bookingData?.professional?._id?.toString(),
              // "pickUpLat": context.params.pickUpLat,
              // "pickUpLng": context.params.pickUpLng,
              // "dropLat": context.params.dropLat,
              // "dropLng": context.params.dropLng,
            });
            await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
              "actionStatus": constantUtil.CONST_DROP,
              "bookingId": null,
              "professionalId": bookingData?.professional?._id?.toString(),
              "serviceCategory": bookingData.serviceCategory,
              "lastRidePassengerCount": bookingData?.passengerCount || 0,
              "lat":
                context.params?.latLng?.lat ||
                bookingData?.professional.location.coordinates[1],
              "lng":
                context.params?.latLng?.lng ||
                bookingData?.professional.location.coordinates[0],
              "parentShareRideId": bookingData?.parentShareRideId,
              // "passengerCount": bookingData?.passengerCount || 0,
            });
            // await this.adapter.model.updateOne(
            //   {
            //     "_id": mongoose.Types.ObjectId(
            //       bookingData?.parentShareRideId?.toString()
            //     ),
            //   },
            //   {
            //     "$inc": {
            //       "currentPassengerCount": -parseInt(
            //         bookingData?.passengerCount || 0
            //       ),
            //     },
            //   }
            // );
          }
          break;

        default:
          this.broker.emit("professional.updateOnGoingAndTailBooking", {
            "professionalId": bookingData?.professional?._id?.toString(),
            "bookingId": bookingData?._id?.toString(),
          });
          break;
      }

      //  penality amount taken if exist

      // needd for many process check carefully for penality
      const cancellationMinute = helperUtil.getMinutesDiff(
        new Date(),
        (bookingData.activity.acceptTime == null
          ? ""
          : bookingData.activity.acceptTime
        ).toString() // professional accept time
      );
      const penalityStatus =
        cancellationMinute >=
        vehicleCategoryData.professionalCancellation.thresholdMinute;

      const penalityAmount =
        vehicleCategoryData.professionalCancellation.status === true && // only amount is applicable when this condition is satifies many buffer function is used so this process very useful check entirly the amount is a paras of many events so this a confortable sollution for this
        penalityStatus === true //&& isHasPenalty === true
          ? vehicleCategoryData.professionalCancellation.amount
          : 0;

      //
      // first take amount if canceled time exceeded beyond buffer time from accepted time
      // if user type is professional so which booking is accepted so does not need condition

      // calcualate if cancel time is exceed buffer time
      //  BELOW CODE IS REMOVED ACCORDTO PROJECT MANAGER LOGIC BEACUSE WHEN CANCELL A RIDE FROM ADMIN PANEL NOT NEED TO CANCEL AMOUNT
      // if (vehicleCategoryData.professionalCancellation.status === true) {
      //   // below condition check accepted time and cancel time difference is exceed cancelation taking bufferTime

      //   if (penalityStatus && penalityAmount) {
      //     // some times penality amount is 0 so we dont need to make transation
      //     // take amount from prfessional
      //     this.broker.emit("professional.updateWalletAmount", {
      //       "professionalId": bookingData.professional._id.toString(),
      //       "availableAmount":
      //         bookingData.professional.wallet.availableAmount - penalityAmount, //professional cancellation amount detection
      //       "freezedAmount": bookingData.professional.wallet.freezedAmount,
      //     });
      //     //create transaction
      //     this.broker.emit("transaction.profesionalCancelCharge", {
      //       "id": bookingData.professional._id.toString(), //professional id
      //       "bookingId": bookingData._id.toString(), //bookingId
      //       "amount": penalityAmount,
      //       "refBookingId": bookingData.bookingId,
      //     });
      //   }
      // }
    }

    // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
    if (bookingData.user) {
      // update ongoing of user
      this.broker.emit("user.updateOnGoingBooking", {
        "userId": bookingData.user._id.toString(),
        "bookingType": bookingData.bookingType,
      });
      //  schedule ride
      if (bookingData.bookingType === constantUtil.SCHEDULE) {
        this.broker.emit("user.removeScheduleBooking", {
          "userId": bookingData.user._id.toString(),
          "bookingType": bookingData.bookingType,
          "bookingId": context.params.bookingId,
        });
      }
      // refund amount
      if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
        // REFUND THE WHOLE AMOUNT
        if (bookingData.bookingType === constantUtil.INSTANT) {
          // update wallet amount and update freezed amount
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount":
              bookingData.user.wallet.availableAmount +
              bookingData.invoice.estimationAmount,
            "freezedAmount":
              bookingData.user.wallet.availableAmount -
              bookingData.invoice.estimationAmount,
            "scheduleFreezedAmount": bookingData.user.wallet.availableAmount,
          });
        }
        if (bookingData.bookingType === constantUtil.SCHEDULE) {
          // update wallet amount and update freezed amount
          this.broker.emit("user.updateWalletAmount", {
            "userId": bookingData.user._id.toString(),
            "availableAmount":
              bookingData.user.wallet.availableAmount +
              bookingData.invoice.estimationAmount,
            "freezedAmount": bookingData.user.wallet.availableAmount,
            "scheduleFreezedAmount":
              bookingData.user.wallet.availableAmount -
              bookingData.invoice.estimationAmount,
          });
        }
      }
      // PAYEMNT CARD
      if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
        // REFUND THE AMOUNT
        this.broker.emit("transaction.newBookingCancelRefund", {
          "paymentTokenId": bookingData?.payment?.details?.id,
          "userId": bookingData?.user?._id?.toString(),
          "amount": 0,
          "currencyCode": bookingData.currencyCode,
          "paymentInitId": paymentInitId,
          "paymentInitAmount": paymentInitAmount,
          "cardId": cardId,
          "bookingId": bookingId,
          "refBookingId": refBookingId,
          "transactionType": constantUtil.CANCELCAPTURE,
        });
      }
    }
    if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
      if (
        generalSettings?.data?.isEnableCorporateRideWalletPay &&
        bookingData.coorperate
      ) {
        const updateCorporateRideWalletPay = "";
        // let corporateData = await
        await this.broker.emit("admin.updateWalletAmountTripEnded", {
          "corporateId": bookingData.coorperate?._id?.toString(),
          "amount": parseFloat(bookingData?.invoice?.payableAmount || 0),
          "paymentType": constantUtil.CREDIT,
          "paymentMethod": bookingData.payment.option,
          "bookingId": bookingData._id?.toString(),
          "refBookingId": bookingData.bookingId,
          "transactionType": constantUtil.CANCELLATIONCHARGE,
          "transactionReason":
            constantUtil.CANCELLATIONCHARGE + " / " + bookingData.bookingId,
        });
      }
    }
    // bookingData = await this.adapter.model
    //   .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    //   .populate("user", {
    //     "password": 0,
    //     "bankDetails": 0,
    //     "trustedContacts": 0,
    //     "cards": 0,
    //   })
    //   .populate("professional", {
    //     "password": 0,
    //     "cards": 0,
    //     "bankDetails": 0,
    //     "trustedContacts": 0,
    //   })
    //   .populate("category")
    //   .populate("security")
    //   .populate("officer", { "password": 0, "preferences": 0 })
    //   .lean();

    // bookingData = bookingData.toJSON();

    bookingData = await this.getBookingDetailsById({
      "bookingId": context.params.bookingId.toString(),
    }); // Get booking Details
  }

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  // bookingData.distanceUnit = bookingData.category.distanceType;
  bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;

  if (bookingData.user) {
    const deviceId = {
      "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
      "id": bookingData?.user?._id?.toString(),
      "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
      "platform": bookingData.user?.deviceInfo[0]?.platform || "",
      "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
    };
    const notificationObject = {
      "clientId": clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_CANCELBOOKING,
        "timestamp": Date.now(),
        "message": INFO_RIDE_CANCELLED_BY + " " + INFO_PROFESSIONAL,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": [deviceId],
    };

    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
    /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
    // if (generalSettings.data.isEnableWebAppBooking) {
    this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
      "bookingId": bookingData._id,
      "bookingStatus": bookingData.bookingStatus,
      "dateTime": bookingData.updatedAt,
      "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
      "bookingInfo": bookingData?.user?.bookingInfo || {},
    });
    // }
  }

  if (bookingData.professional) {
    const deviceId = {
      "token": bookingData.professional?.deviceInfo[0]?.deviceId || "",
      "id": bookingData.professional?._id?.toString(),
      "deviceType": bookingData.professional?.deviceInfo[0]?.deviceType || "",
      "platform": bookingData.professional?.deviceInfo[0]?.platform || "",
      "socketId": bookingData.professional?.deviceInfo[0]?.socketId || "",
    };
    const notificationObject = {
      "clientId": clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_CANCELBOOKING,
        "timestamp": Date.now(),
        "message": BOOKING_CANCELLED_BY + " " + INFO_ADMIN,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": [deviceId],
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }

  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_CANCELLED,
  };
};

bookingAction.adminBookingExpiryOrCancel = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const {
    INVALID_BOOKING,
    BOOKING_STATUS_INFO,
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_STATUS_IS,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  if (
    context.params.denyType === constantUtil.EXPIRED ||
    context.params.denyType === constantUtil.USERCANCELLED
  ) {
    let bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();

    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    // bookingData = bookingData.toJSON();
    const paymentInitId = bookingData?.paymentInitId?.toString();
    const paymentInitAmount = bookingData?.paymentInitAmount?.toString();
    const cardId = bookingData?.payment?.card?.cardId?.toString();
    const refBookingId = bookingData?.bookingId?.toString();
    const bookingId = bookingData?._id;

    if (bookingData.bookingStatus === constantUtil.AWAITING) {
      const updateData =
        context.params.denyType === constantUtil.EXPIRED
          ? {
              "bookingStatus": constantUtil.EXPIRED,
              "activity.denyOrExpireTime": new Date(),
            }
          : {
              "bookingStatus": constantUtil.USERDENY,
              "activity.denyOrExpireTime": new Date(),
            };

      const updatedBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
          "bookingStatus": constantUtil.AWAITING,
        },
        updateData
      );

      if (!updatedBookingData.nModified) {
        throw new MoleculerError(BOOKING_STATUS_INFO);
      }
      if (bookingData.bookingType === constantUtil.SCHEDULE)
        if (bookingData.user) {
          // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
          this.broker.emit("user.removeScheduleBooking", {
            "userId": bookingData.user._id.toString(),
            "bookingType": bookingData.bookingType,
            "bookingId": context.params.bookingId,
          });
          //---------Refund-Ajith Start-----------
          if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
            await this.broker.emit("transaction.newBookingCancelRefund", {
              "paymentTokenId": bookingData?.payment?.details?.id,
              "userId": bookingData?.user?._id?.toString(),
              "amount": 0,
              "currencyCode": bookingData.currencyCode,
              "paymentInitId": paymentInitId,
              "paymentInitAmount": paymentInitAmount,
              "cardId": cardId,
              "bookingId": bookingId,
              "refBookingId": refBookingId,
              "transactionType": constantUtil.CANCELCAPTURE,
            });
          }
          //---------Refund-Ajith End-----------
        }
      bookingData = await this.adapter.model
        .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
        })
        .populate("category")
        .populate("security")
        .populate("officer", { "password": 0, "preferences": 0 })
        .lean();

      // bookingData = bookingData.toJSON();
      let vehicleCategoryData = await storageUtil.read(
        constantUtil.VEHICLECATEGORY
      );
      if (!vehicleCategoryData) {
        throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
      }
      vehicleCategoryData =
        vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

      bookingData.vehicle.vehicleCategoryName =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle.vehicleCategoryImage =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle.vehicleCategoryMapImage =
        vehicleCategoryData.categoryMapImage;
      // bookingData.distanceUnit = bookingData.category.distanceType;
    }
    return {
      "code": 200,
      "data": bookingObject(bookingData),
      "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
    };
  }
};
// Check And remove After June 2015 --> API Name --> getCommonConfig
bookingAction.getCommonConfig = async function (context) {
  let responseJson = null;
  // let clientId = context.params.clientId || context.meta?.clientId;
  let secretKey = context.params.secretKey?.trim();
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const callSetting = await storageUtil.read(constantUtil.CALLSETTING);
  const mapSetting = await storageUtil.read(constantUtil.MAPSETTING);
  const { ERROR_GENEREL_SETTINGS } = notifyMessage.setNotifyLanguage(
    constantUtil.DEFAULT_LANGUAGE
  );
  if (!generalSettings || !generalSettings.data) {
    throw new MoleculerError(ERROR_GENEREL_SETTINGS);
  }
  const selectedAllServiceType =
      generalSettings.data?.selectedAllServiceType || [],
    clientId = generalSettings.data?.clientId;
  const imageURLPath =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;
  const userType = context.params.userType;
  let responseData = {
    "id": generalSettings._id,
    "clientId": generalSettings.data.clientId,
    "clientCode": generalSettings.data.clientCode,
    "companyAddress": generalSettings.data.address,
    // // "secretKey":  "DoNotDisplay", //generalSettings.data.secretKey, // Must will not send this Key Value
    "tipStatus": generalSettings.data.tipStatus,
    "minimumWalletAmountToOnline":
      generalSettings?.data?.minimumWalletAmountToOnline,
    "paymentSection": generalSettings.data.paymentSection,
    "tipsMinAmount": generalSettings.data.tipsMinAmount,
    "tipsMaxAmount": generalSettings.data.tipsMaxAmount,
    "emailAddress": generalSettings.data.emailAddress,
    "mapApi": generalSettings.data.mapApi,
    "siteTitle": generalSettings.data.siteTitle,
    "siteUrl": generalSettings.data.siteUrl,
    "currencyCode": generalSettings.data.currencyCode,
    "currencySymbol": generalSettings.data.currencySymbol,
    "smsType": smsSetting.data.smsType,
    "mode": smsSetting.data.mode,
    // "smsSettings": smsSetting.data,
    "callSettings": callSetting.data,
    "isEnableZervxMap": generalSettings.data.isEnableZervxMap,
    "mapSettings": {
      "autocomplete": mapSetting.data.autocomplete || {},
      "direction": mapSetting.data.direction || {},
      "locationMap": mapSetting.data.locationMap || {},
      "place": mapSetting.data.place || {},
      "snap": mapSetting.data.snap || {},
    },
    "redirectUrls": generalSettings.data.redirectUrls,
    "appUpdateStatus": generalSettings?.data?.appUpdateStatus,
    "androidUserAppUpdateStatus":
      generalSettings?.data?.androidUserAppUpdateStatus,
    "androidDriverAppUpdateStatus":
      generalSettings?.data?.androidDriverAppUpdateStatus,
    "iosDriverAppUpdateStatus": generalSettings?.data?.iosDriverAppUpdateStatus,
    "iosUserAppUpdateStatus": generalSettings?.data?.iosUserAppUpdateStatus,
    "androidUserVersionKey": generalSettings?.data?.androidUserVersionKey,
    "androidDriverVersionKey": generalSettings?.data?.androidDriverVersionKey,
    "iosUserVersionKey": generalSettings?.data?.iosUserVersionKey,
    "iosDriverVersionKey": generalSettings?.data?.iosDriverVersionKey,
    "androidVersionKey": generalSettings.data.androidVersionKey,
    "iosVersionKey": generalSettings.data.iosVersionKey,
    "isSocketPolling": generalSettings.data.isSocketPolling,
    "isReferralNeeded": generalSettings.data.isReferralNeeded,
    "stopLimit": generalSettings.data.stopLimit,
    "isManualMeterNeeded": generalSettings.data.isManualMeterNeeded,
    "isOtpNeeded": generalSettings.data.isOtpNeeded,
    "isTollNeeded": generalSettings.data.isTollNeeded,
    "isTollManual": generalSettings.data.isTollManual,
    "isProduction": generalSettings.data.isProduction,
    "isCancelToNextProfessional":
      generalSettings.data.isCancelToNextProfessional,
    // "isWaitingEnable": generalSettings.data.isWaitingEnable,
    "siteStatus": generalSettings.data.siteStatus,
    "siteStatusMessage": generalSettings.data.siteStatusMessage,
    "lastPriorityStatus": generalSettings.data.lastPriorityStatus,
    "isRerouteEnable": generalSettings.data.isRerouteEnable,
    "isAddressChangeEnable": generalSettings.data.isAddressChangeEnable,
    //--------- Update Time Start ----
    "tollUpdateTime": generalSettings.data.tollUpdateTime,
    "couponUpdateTime": generalSettings.data.couponUpdateTime,
    "airportUpdateTime": generalSettings.data.airportUpdateTime,
    "popularplaceUpdateTime": generalSettings.data.popularplaceUpdateTime,
    "packageUpdateTime": generalSettings.data.packageUpdateTime,
    "mapKeyUpdateTime": generalSettings.data.mapKeyUpdateTime,
    "registrationSetupUpdateTime":
      generalSettings.data.registrationSetupUpdateTime,
    "callCenterUpdateTime": generalSettings.data.callCenterUpdateTime,
    "cancelReasonUpdateTime": generalSettings.data.cancelReasonUpdateTime,
    //--------- Update Time End ----
    "isQuickTripNeeded": generalSettings.data.isQuickTripNeeded,
    "isDailyTripNeeded": generalSettings.data.isDailyTripNeeded,
    "isOutstationNeeded": generalSettings.data.isOutstationNeeded,
    "isRentalNeeded": generalSettings.data.isRentalNeeded,
    "isAirportTripNeeded": generalSettings.data.isAirportTripNeeded,
    "isCarpoolTripNeeded": generalSettings.data.isCarpoolTripNeeded,
    "isAmbulanceNeeded": generalSettings.data.isAmbulanceNeeded,
    "isSmallPackageNeeded": generalSettings.data.isSmallPackageNeeded,
    "isHandicapNeeded": generalSettings.data.isHandicapNeeded,
    "isChildseatNeeded": generalSettings.data.isChildseatNeeded,
    "isgenderTripNeeded": generalSettings.data.isgenderTripNeeded,
    "isPetAllowed": generalSettings.data.isPetAllowed,
    "distanceCalculateType": generalSettings.data.distanceCalculateType,
    "walletSendMinAmount": generalSettings.data.walletSendMinAmount,
    "walletSendMaxAmount": generalSettings.data.walletSendMaxAmount,
    "callCenterPhone": generalSettings.data.callCenterPhone,
    "walletMinAmountThreshold": generalSettings.data.walletMinAmountThreshold,
    "withDrawMinAmount": generalSettings.data.withDrawMinAmount,
    "withDrawMaxAmount": generalSettings.data.withDrawMaxAmount,
    "walletRechargeMinAmount": generalSettings.data.walletRechargeMinAmount,
    "walletRechargeMaxAmount": generalSettings.data.walletRechargeMaxAmount,
    "emailConfigurationEnable": generalSettings.data.emailConfigurationEnable,
    "smsConfigurationEnable": generalSettings.data.smsConfigurationEnable,
    "defaultCountryCode": generalSettings.data.defaultCountryCode,
    "defaultDialCode": generalSettings.data.defaultDialCode,
    "sosEmergencyNumber": generalSettings.data.sosEmergencyNumber,
    "isVinNeeded": generalSettings.data.isVinNeeded,
    "androidUserServerKey": generalSettings.data.firebaseAdmin.androidUser,
    "androidDriverServerKey": generalSettings.data.firebaseAdmin.androidDriver,
    "iosUserServerKey": generalSettings.data.firebaseAdmin.iosUser,
    "iosDriverServerKey": generalSettings.data.firebaseAdmin.iosDriver,
    "documentExpireThreshold": generalSettings.data.documentExpireThreshold,
    "isMapOptimized": generalSettings.data.isMapOptimized,
    "isUserReroute": generalSettings.data.isUserReroute,
    "isDriverReroute": generalSettings.data.isDriverReroute,
    "isShowRouteInfoInMap": generalSettings.data.isShowRouteInfoInMap,
    "algorithimType": generalSettings.data.bookingAlgorithm,
    "isHubNeeded": generalSettings.data?.isHubNeeded,
    // "developmentMode": generalSettings.data.mode,
    // "zervxServices": generalSettings.data?.zervxServices,
    "zervxServices": selectedAllServiceType,
    "imageURLPath": imageURLPath,
    "brandImage": generalSettings.data.brandImage,
    "islicenceNoNeeded": generalSettings?.data?.islicenceNoNeeded, //need to remove
    "isEnableElectricVehicle": generalSettings?.data?.isEnableElectricVehicle,
    "isEnableLiveMeter": generalSettings?.data?.isEnableLiveMeter,
    "userAppSettings": generalSettings?.data?.userApp,
    "driverAppSettings": generalSettings?.data?.driverApp,
    "oneSignalAppSettings": generalSettings?.data?.oneSignalApp,
    "redeemAppSettings": generalSettings?.data?.redeemApp,
    "dispatchRideSettings": generalSettings?.data?.dispatchRide,
    "driverArrivalThresholdDistance":
      generalSettings?.data?.driverArrivalThresholdDistance,
    "driverCancelThresholdMins":
      generalSettings?.data?.driverCancelThresholdMins,
    "requestDistance": generalSettings?.data?.requestDistance,
    "isEnableShareRide": generalSettings?.data?.isEnableShareRide,
    "isEnableHeatmap": generalSettings?.data?.isEnableHeatmap,
    "isEnableCorporateRideWalletPay":
      generalSettings?.data?.isEnableCorporateRideWalletPay,
    "isEnableIncentive": generalSettings?.data?.isEnableIncentive,
    "isEnableMultiRideRequestPopup":
      generalSettings?.data?.isEnableMultiRideRequestPopup,
    "onlineStatusThreshold": generalSettings?.data?.onlineStatusThreshold,
    "nationalIdCheckAPI": generalSettings?.data?.nationalIdCheck,
    "paymentMode": generalSettings?.data?.paymentMode,
    "voiceAssistant": {
      "isEnableLocationsSearch":
        generalSettings?.data?.voiceAssistant?.isEnableLocationsSearch || false,
      "isEnableActionChange": false,
    },
    "isEnableFixedChangeRide":
      generalSettings.data?.dispatchRide?.isEnableFixedChangeRide || false,
    "isProfessionalsCorporateSiteCommissionCreditToWallet":
      generalSettings.data
        ?.isProfessionalsCorporateSiteCommissionCreditToWallet || false,
  };
  if (context.params.appVersion) {
    responseData = cryptoAESEncode(JSON.stringify(responseData));
    responseData = responseData?.toString();
  }
  return {
    "code": 200,
    "data": responseData,
    "message": "",
  };
};
bookingAction.giveTips = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  let tipsAmount = parseFloat(context.params.amount || 0);
  const {
    ERROR_GENEREL_SETTINGS,
    TRIP_NOT_AVAILABLE,
    TIPS_MINIMUM,
    TIPS_MAXIMUM,
    INVALID_BOOKING,
    ERROR_PAYMENT_CARD,
    BOOKING_UPDATE_FAILED,
    TIPS_ADDED,
    TIPS_AMOUNT,
    INFO_TIPS,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  if (!generalSettings || !generalSettings.data) {
    throw new MoleculerError(ERROR_GENEREL_SETTINGS);
  }
  if (generalSettings.data.tipStatus === "0") {
    throw new MoleculerError(TRIP_NOT_AVAILABLE);
  }
  if (generalSettings.data.tipsMinAmount > context.params.amount) {
    throw new MoleculerError(
      TIPS_MINIMUM + " " + generalSettings.data.tipsMinAmount
    );
  }
  if (generalSettings.data.tipsMaxAmount < context.params.amount) {
    throw new MoleculerError(
      TIPS_MAXIMUM + " " + generalSettings.data.tipsMaxAmount
    );
  }
  const bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "_id": 1,
      "wallet": 1,
    })
    .populate("professional", {
      "_id": 1,
      "wallet": 1,
    })
    // .populate("category")
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // let paymentData = await this.broker.emit("transaction.newTipsCharge", {
  //   "card": context.params.card,
  //   // "userId": bookingData.user._id.toString(),
  //   "userId": bookingData.user.toString(),
  //   "amount": parseFloat(context.params.amount).toFixed(2),
  //   "currencyCode": bookingData.currencyCode,
  // });
  // paymentData = paymentData && paymentData[0];
  // if (!paymentData) {
  //   throw new MoleculerError(ERROR_PAYMENT_CARD);
  // }

  // //create transaction
  // this.broker.emit("transaction.profesionalCancelCharge", {
  //   "id": context.meta.professionalId, //professional id
  //   "bookingId": bookingData._id.toString(), //bookingId
  //   "amount": penalityAmount,
  //   "refBookingId": bookingData.bookingId,
  //   "previousBalance": bookingData.professional.wallet.availableAmount,
  //   "currentBalance":
  //     bookingData.professional.wallet.availableAmount - penalityAmount,
  // });

  // const commisionWithTip =
  //   parseFloat(bookingData.invoice.professionalCommision.toFixed(2)) +
  //   parseFloat(context.params.amount);
  const updatedBookingData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.bookingId),
    },
    {
      "$set": {
        "tipsInfo.status": true,
        "tipsInfo.paymentType": context.params.paymentType,
        "tipsInfo.amount": parseFloat(context.params.amount),
        // "invoice.professionalCommision": commisionWithTip,
        "invoice.tipsAmount": parseFloat(context.params.amount),
      },
    }
  );
  if (!updatedBookingData.nModified) {
    throw new MoleculerError(BOOKING_UPDATE_FAILED);
  }
  if (context.params.paymentType === constantUtil.PAYMENTWALLET) {
    // update User Wallet Amount
    this.broker.emit("user.updateWalletAmount", {
      "userId": bookingData.user._id.toString(),
      "availableAmount": bookingData.user.wallet.availableAmount - tipsAmount,
      "freezedAmount": bookingData.user.wallet.freezedAmount,
      "scheduleFreezedAmount": bookingData.user.wallet.scheduleFreezedAmount,
    });
    //create transaction
    this.broker.emit("transaction.insertTransactionBasedOnUserType", {
      "transactionType": constantUtil.TIPSCHARGE, // Tips Amount Debit
      "transactionAmount": tipsAmount,
      "previousBalance": bookingData.user.wallet.availableAmount,
      "currentBalance": bookingData.user.wallet.availableAmount - tipsAmount,
      "transactionReason": INFO_TIPS + " / " + bookingData.bookingId,
      "refBookingId": bookingData.bookingId,
      "transactionStatus": constantUtil.SUCCESS,
      "transactionId": context.params.transactionId,
      "paymentType": constantUtil.DEBIT,
      "paymentGateWay": constantUtil.NONE,
      "gatewayResponse": {},
      "userType": constantUtil.USER, // Debit From
      "userId": bookingData.user._id.toString(), // Debit From
      "professionalId": null, // Debit From
      "paymentToUserType": constantUtil.ADMIN, // Credit To
      "paymentToUserId": null, // Credit To
    });
    // update professional Wallet Amount
    this.broker.emit("professional.updateWalletAmount", {
      "professionalId": bookingData.professional._id.toString(),
      "availableAmount":
        bookingData.professional.wallet.availableAmount + tipsAmount,
      "freezedAmount": bookingData.professional.wallet.freezedAmount,
    });
    //create transaction
    this.broker.emit("transaction.insertTransactionBasedOnUserType", {
      "transactionType": constantUtil.TIPSCHARGE, // Tips Amount Credit
      "transactionAmount": tipsAmount,
      "previousBalance": bookingData.professional.wallet.availableAmount,
      "currentBalance":
        bookingData.professional.wallet.availableAmount + tipsAmount,
      "transactionReason": TIPS_AMOUNT + " / " + bookingData.bookingId,
      "refBookingId": bookingData.bookingId,
      "transactionStatus": constantUtil.SUCCESS,
      "transactionId": context.params.transactionId,
      "paymentType": constantUtil.CREDIT,
      "paymentGateWay": constantUtil.NONE,
      "gatewayResponse": {},
      "userType": constantUtil.ADMIN, // Debit From
      "userId": null, // Debit From
      "professionalId": null, // Debit From
      "paymentToUserType": constantUtil.PROFESSIONAL, // Credit To
      "paymentToUserId": bookingData.professional._id.toString(), // Credit To
    });
  } else if (context.params.paymentType === constantUtil.PAYMENTCARD) {
    let paymentData = await this.broker.emit("transaction.newTipsCharge", {
      "card": context.params.card,
      // "userId": bookingData.user._id.toString(),
      "userId": bookingData.user._id.toString(),
      "professionalId": bookingData.professional._id.toString(),
      "bookingId": bookingData.bookingId,
      "amount": tipsAmount,
      "transactionId": context.params.transactionId,
      "gatewayResponse": context.params?.gatewayResponse,
      "currencyCode": bookingData.currencyCode,
    });
    paymentData = paymentData && paymentData[0];
    if (!paymentData) {
      throw new MoleculerError(ERROR_PAYMENT_CARD);
    }
    tipsAmount = 0;
  }
  return {
    "code": 200,
    "data": {
      "wallet": {
        ...bookingData.user.wallet,
        "availableAmount": bookingData.user.wallet.availableAmount - tipsAmount,
      },
    },
    "message": TIPS_ADDED,
  };
};
// professional earnings
bookingAction.getDriversBillingEarnings = async function (context) {
  const clientId = context.params.clientId || context.meta.clientId;
  const { ERROR_EARNINGS } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  let earnIds = await this.broker.emit("billings.getBillingIds", {
    "clientId": clientId,
    "professionalId": context.meta.professionalId,
    "skip": skip,
    "limit": limit,
  });
  earnIds = earnIds && earnIds[0];
  if (!earnIds) {
    throw new MoleculerError(ERROR_EARNINGS);
  }
  const ids = _.map(earnIds, (e) => mongoose.Types.ObjectId(e._id));
  const query = [
    {
      "$match": {
        "billingId": { "$in": ids },
        "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        //"clientId": clientId,
      },
    },
    {
      "$project": {
        "billingId": 1,
        "currencySymbol": 1,
        "currencyCode": 1,
        "invoice": 1,
        "workedMins": "$activity.workedMins",
        "time": "$activity.arriveTime",
      },
    },
    {
      "$group": {
        "_id": "$billingId",
        "totalBookingCounts": { "$sum": 1 },
        "totalProfessionalEarnings": {
          "$sum": "$invoice.professionalCommision",
        },
        "totalCashEarnings": { "$sum": "$invoice.amountInDriver" },
        "totalTipsAmount": { "$sum": "$invoice.tipsAmount" },
        "totalWorkedMins": { "$sum": "$workedMins" },
        "currencySymbol": { "$first": "$currencySymbol" },
        "currencyCode": { "$first": "$currencyCode" },
      },
    },
  ];

  const query1 = [
    {
      "$match": {
        "billingId": { "$in": ids },
        "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        //"clientId": clientId,
      },
    },
    {
      "$project": {
        "billingId": 1,
        "invoice": 1,
        "time": "$activity.arriveTime",
      },
    },
    {
      "$group": {
        "_id": { "$dateToString": { "format": "%Y-%m-%d", "date": "$time" } },
        "billingId": { "$first": "$billingId" },
        "date": { "$first": "$time" },
        "bookingCount": { "$sum": 1 },
        "professionalEarningsPerDay": {
          "$sum": "$invoice.professionalCommision",
        },
      },
    },
  ];
  const rides = await this.adapter.model.aggregate(query).allowDiskUse(true);
  if (!rides || (rides && rides.length == 0)) {
    return {
      "code": 200,
      "message": "",
      "data": [],
    };
  } else {
    const dayRides = await this.adapter.model
      .aggregate(query1)
      .allowDiskUse(true);
    if (!dayRides || (dayRides && dayRides.length == 0)) {
      return {
        "code": 200,
        "message": "",
        "data": [],
      };
    } else {
      const dataRespo = rides.map((event) =>
        Object.assign(event, {
          "daysEarnings": dayRides
            .map((day) => day)
            .filter((day) => day.billingId.toString() === event._id.toString()),
        })
      );
      const earningsRespo = [];
      await _.forEach(dataRespo, async (r) => {
        const responseData = {};
        responseData["billingId"] = r._id;
        responseData["totalBookingCounts"] = r.totalBookingCounts;
        responseData["totalProfessionalEarnings"] = parseFloat(
          r.totalProfessionalEarnings.toFixed(2)
        );
        responseData["totalCashEarnings"] = parseFloat(
          r.totalCashEarnings.toFixed(2)
        );
        responseData["totalBalanceAmount"] = parseFloat(
          (r.totalProfessionalEarnings - r.totalCashEarnings).toFixed(2)
        );
        responseData["totalTipsAmount"] = parseFloat(
          r.totalTipsAmount.toFixed(2)
        );
        responseData["totalWorkedMins"] = r.totalWorkedMins;
        responseData["currencySymbol"] = r.currencySymbol;
        responseData["currencyCode"] = r.currencyCode;
        await _.forEach(earnIds, async (cycle) => {
          if (cycle._id.toString() === r._id.toString()) {
            responseData["billingsCycles"] = cycle.billingcycle;
            let from, to;
            if (cycle.billingcycle.split(" - ").length > 1) {
              from =
                cycle.billingcycle.split(" - ")[0].split("/")[1] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[0] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[2];
              to =
                cycle.billingcycle.split(" - ")[1].split("/")[1] +
                "/" +
                cycle.billingcycle.split(" - ")[1].split("/")[0] +
                "/" +
                cycle.billingcycle.split(" - ")[1].split("/")[2];
            } else {
              from =
                cycle.billingcycle.split(" - ")[0].split("/")[1] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[0] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[2];
              to =
                cycle.billingcycle.split(" - ")[0].split("/")[1] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[0] +
                "/" +
                cycle.billingcycle.split(" - ")[0].split("/")[2];
            }
            responseData["payoutDate"] = new Date(cycle.payoutDate);
            responseData["fromDate"] = new Date(from);
            responseData["toDate"] = new Date(to);
          }
        });
        responseData["daysEarnings"] = [];
        await _.forEach(r.daysEarnings, async (day) => {
          const dayEarnings = {};
          dayEarnings["date"] = day.date;
          dayEarnings["bookingCount"] = day.bookingCount;
          dayEarnings["professionalEarningsPerDay"] =
            day.professionalEarningsPerDay;
          responseData["daysEarnings"].push(dayEarnings);
        });
        earningsRespo.push(responseData);
      });
      return {
        "code": 200,
        "message": "",
        "data": earningsRespo,
      };
    }
  }
};
bookingAction.getProfessionalInstantPaymentEarnings = async function (context) {
  return {
    "code": 200,
    "message": "FETCHED SUCCESSFULLY",
    "data": {
      "balacnceAmount":
        context.meta.professionalDetails.wallet.availableAmount < 0
          ? context.meta.professionalDetails.wallet.availableAmount
          : 0,
      "data": await this.adapter.model.aggregate([
        {
          "$match": {
            "professional": mongoose.Types.ObjectId(
              context.meta.professionalId
            ),
            "bookingStatus": constantUtil.ENDED,
            "regionalData.bookingDate": { "$ne": null },
            // "createdAt": {
            //   "$lte": new Date(),
            // },
            // "regionalData.bookingDate": {
            //   "$lte": new Date(),
            // },
          },
        },
        {
          "$group": {
            "_id": {
              // "$dateToString": { "format": "%Y-%m-%d", "date": "$createdAt" },
              "$dateToString": {
                "format": "%Y-%m-%d",
                "date": "$regionalData.bookingDate",
              },
            },
            "distanceUnit": { "$first": "$distanceUnit" },
            "totalBookingCounts": { "$sum": 1 },
            "totalProfessionalEarnings": {
              "$sum": "$invoice.professionalCommision",
            },
            "totalCashEarnings": {
              "$sum": "$invoice.amountInDriver",
            },
            "totalBalanceAmount": {
              "$sum": "$invoice.amountInDriver",
            },
            "totalTipsAmount": { "$sum": "$invoice.tipsAmount" },
            "totalWorkedMins": {
              "$sum": {
                "$divide": [
                  {
                    "$subtract": ["$activity.dropTime", "$activity.acceptTime"],
                  },
                  1000,
                ],
              },
            },
            "totalWorkedDistance": {
              "$sum": "$estimation.distance",
            },
          },
        },
        { "$sort": { "_id": -1 } },
        { "$skip": context.params.skip },
        { "$limit": context.params.limit },
        {
          "$project": {
            "date": {
              "$dateFromString": {
                "dateString": "$_id",
              },
            },
            "distanceUnit": "$distanceUnit",
            "totalBookingCounts": "$totalBookingCounts",
            "totalProfessionalEarnings": "$totalProfessionalEarnings",
            "totalCashEarnings": "$totalCashEarnings",
            // "totalBalanceAmount": "$totalBalanceAmount",
            "totalTipsAmount": "$totalTipsAmount",
            "totalWorkedMins": "$totalWorkedMins",
            "totalWorkedDistance": "$totalWorkedDistance",
            "_id": 0,
          },
        },
      ]),
    },
  };
};

// "billingId": null,
//         "totalBookingCounts": "$_id.totalProfessionalEarnings",
//         "totalProfessionalEarnings": "$_id.totalProfessionalEarnings",
//         "totalCashEarnings": "$_id.totalCashEarnings",
//         "totalBalanceAmount": {
//           "$subtract": [
//             "$_id.totalProfessionalEarnings",
//             "$_id.totalCashEarnings",
//           ],
//         },
//         "totalTipsAmount": "$_id.totalTipsAmount",
//         "totalWorkedMins": "$_id.totalWorkedMins",
//         "currencySymbol": "$_id.currencySymbol",
//         "currencyCode": "$_id.currencyCode",
//         "billingsCycles": null,
//         "payoutDate": null,
//         "fromDate": "$_id.date",
//         "toDate": "$_id.date",
//         "daysEarnings": [
//           {
//             "date": null,
//             "bookingCount": "$_id.totalBookingCounts",
//             "professionalEarningsPerDay": "$_id.totalProfessionalEarnings",
//           },
//         ],
//
bookingAction.getDayEarnings = async function (context) {
  let fromDate,
    toDate,
    userDetails = null,
    condition = {};
  const actionType = (
    context.params.actionType || constantUtil.ALL
  ).toUpperCase();
  const { INFO_NOT_FOUND } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.professionalDetails?.languageCode
  );
  // const fromDate = new Date(
  //   new Date(context.params.billedDate).setHours(0, 0, 0, 0)
  // );
  // const toDate = new Date(
  //   new Date(context.params.billedDate).setDate(fromDate.getDate() + 1)
  // );
  // const billedDate = helperUtil.toRegionalUTC(context.params.billedDate);
  if (context.params.userType === constantUtil.USER) {
    userDetails = {
      "user": mongoose.Types.ObjectId(context.params.userId),
    };
  } else {
    userDetails = {
      "professional": mongoose.Types.ObjectId(
        context.params.professionalId || context.meta.professionalId
      ),
    };
  }
  if (actionType === constantUtil.CONST_INCENTIVE) {
    fromDate = helperUtil.toUTC(context.params.billedDate);
    // fromDate = helperUtil.toUTC(context.params.startDate); // Future Use
    toDate = helperUtil.toUTC(
      new Date(context.params.endDate).setDate(
        context.params.endDate.getDate() + 1
      )
    );
    condition = {
      // "serviceCategory": { "$in": context.params.rideType }, // Need to Check
      "serviceCategory": {
        "$regex": ".*" + context.params.rideType?.toLowerCase() + ".*",
      },
    };
    if (context.params.userType === constantUtil.USER) {
      condition["bookingReview.professionalReview.rating"] = {
        "$gte": context.params.minimumReviewRating,
      };
      condition["invoice.payableAmount"] = {
        "$gte": context.params.minimumEligibleAmount,
      };
    } else {
      condition["bookingReview.userReview.rating"] = {
        "$gte": context.params.minimumReviewRating,
      };
    }
  } else {
    fromDate = helperUtil.toUTC(context.params.billedDate);
    toDate = helperUtil.toUTC(
      new Date(context.params.billedDate).setDate(
        context.params.billedDate.getDate() + 1
      )
    );
  }
  // const fromDate = helperUtil
  //   .toRegionalUTC(context.params.billedDate)
  //   .substring(0, 10);
  // const toDate = helperUtil
  //   .toRegionalUTC(
  //     new Date(
  //       new Date(context.params.billedDate).setDate(
  //         context.params.billedDate.getDate() + 1
  //       )
  //     )
  //   )
  //   .substring(0, 10);

  const rides = await this.adapter.model
    .aggregate([
      {
        "$match": {
          ...userDetails,
          "bookingStatus": constantUtil.ENDED,
          // "activity.arriveTime": {
          //   "$gte": fromDate,
          //   "$lt": toDate,
          // },
          // "createdAt": {
          //   "$gte": fromDate,
          //   "$lt": toDate,
          // },
          // "regionalData.bookingDate": {
          //   "$gte": fromDate,
          //   "$lt": toDate,
          // },
          "$expr": {
            "$and": [
              {
                "$gte": [
                  "$regionalData.bookingDate",
                  {
                    "$dateFromString": {
                      "dateString": fromDate,
                      // "format": "%Y-%m-%d",
                    },
                  },
                ],
              },
              {
                "$lte": [
                  "$regionalData.bookingDate",
                  {
                    "$dateFromString": {
                      "dateString": toDate,
                      // "format": "%Y-%m-%d",
                    },
                  },
                ],
              },
            ],
          },
          ...condition,
        },
      },
      {
        "$project": {
          "bookingId": 1,
          "bookingType": 1,
          // "bookingDate": 1,
          "bookingDate": "$regionalData.bookingDate",
          "vehicleCategory": "$vehicle.vehicleCategoryId",
          "vehiclePlateNumber": "$vehicle.plateNumber",
          "vehicleModel": "$vehicle.model",
          "vehicleColor": "$vehicle.color",
          "vehicleYear": "$vehicle.year",
          "paymentOption": "$payment.option",
          "currencyCode": 1,
          "currencySymbol": 1,
          "totalAmount": "$invoice.professionalCommision",
          "payableAmount": "$invoice.payableAmount",
        },
      },
      { "$sort": { "_id": -1 } },
    ])
    .allowDiskUse(true);
  if (!rides?.length) {
    throw new MoleculerError(INFO_NOT_FOUND);
  } else {
    return {
      "code": 200,
      "message": "",
      "data": rides,
    };
  }
};
// take lat 30 day earning for professional

//
bookingAction.getProfessionalDashboard = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  // const minimumWalletAmountToOnline =
  //   generalSettings?.data?.minimumWalletAmountToOnline || 0;
  // if (
  //   parseFloat(context.meta.professionalDetails.wallet.availableAmount) <
  //   parseFloat(minimumWalletAmountToOnline)
  // ) {
  //   console.log(
  //     parseFloat(context.meta.professionalDetails.wallet.availableAmount) +
  //       " " +
  //       parseFloat(minimumWalletAmountToOnline)
  //   );
  //   console.log(
  //     Math.abs(context.meta.professionalDetails.wallet.availableAmount) +
  //       " " +
  //       Math.abs(minimumWalletAmountToOnline)
  //   );
  //   this.broker.emit("professional.updateOnlineStatus", {
  //     "id": context.meta.professionalId.toString(),
  //     "status": false,
  //   });
  // }
  let professionalData = await this.broker.emit("professional.getById", {
    "clientId": clientId,
    "id": context.meta.professionalId,
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData) {
    throw new MoleculerClientError("REQUESTED PROFESSIONAL NOT FOUND");
  }
  const data = {};

  const fromTodayDate = new Date().setHours(0, 0, 0, 0);
  const fromThisBillingDate = new Date(
    generalSettings.data.lastBilledDate
  ).setHours(0, 0, 0, 0);
  const query = [
    {
      "$match": {
        //"clientId": mongoose.Types.ObjectId(clientId),
        "bookingStatus": { "$in": [constantUtil.ENDED] },
        "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        "bookingDate": { "$gte": new Date(fromThisBillingDate) },
      },
    },
    {
      "$facet": {
        "all": [{ "$count": "all" }],
        "documentData": [
          {
            "$project": {
              "currencySymbol": 1,
              "currencyCode": 1,
              "invoice": 1,
              "workedMins": "$activity.workedMins",
            },
          },
          {
            "$group": {
              "_id": "$professional",
              "currencySymbol": { "$first": "$currencySymbol" },
              "currencyCode": { "$first": "$currencyCode" },
              "totalBookings": { "$sum": 1 },
              "tipsAmount": { "$sum": "$invoice.tipsAmount" },
              "professionalEarnings": {
                "$sum": "$invoice.professionalCommision",
              },
              "professionalEarningsInCash": {
                "$sum": "$invoice.amountInDriver",
              },
              "workedMins": { "$sum": "$workedMins" },
            },
          },
        ],
      },
    },
  ];
  const todayQuery = [
    {
      "$match": {
        //"clientId": mongoose.Types.ObjectId(clientId),
        "bookingStatus": { "$in": [constantUtil.ENDED] },
        "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        "bookingDate": { "$gte": new Date(fromTodayDate) },
      },
    },
    {
      "$facet": {
        "documentData": [
          {
            "$project": {
              "invoice": 1,
              "workedMins": "$activity.workedMins",
            },
          },
          {
            "$group": {
              "_id": "$professional",
              "todayTotalBooking": { "$sum": 1 },
              "todayTipsAmount": { "$sum": "$invoice.tipsAmount" },
              "todayProfessionalEarnings": {
                "$sum": "$invoice.professionalCommision",
              },
              "todayProfessionalEarningsInCash": {
                "$sum": "$invoice.amountInDriver",
              },
              "todayWorkedMins": { "$sum": "$workedMins" },
            },
          },
        ],
      },
    },
  ];
  const rides = await this.adapter.model.aggregate(query).allowDiskUse(true);
  if (
    !rides ||
    (rides && rides[0]?.documentData && rides[0]?.documentData.length === 0)
  ) {
    data["totalBookings"] = 0;
    data["currencyCode"] = professionalData?.currencyCode || "";
    data["currencySymbol"] = currencyCodeUtil[professionalData?.currencyCode]
      ? currencyCodeUtil[professionalData?.currencyCode]["symbol-alt-narrow"]
      : "";
    data["tipsAmount"] = 0;
    data["workedMins"] = 0;
    data["professionalEarnings"] = 0;
    data["professionalEarningsInCash"] = 0;
    data["todayTotalBooking"] = 0;
    data["todayTipsAmount"] = 0;
    data["todayWorkedMins"] = 0;
    data["todayProfessionalEarnings"] = 0;
    data["todayProfessionalEarningsInCash"] = 0;
    // return {
    //   "code": 200,
    //   "message": "",
    //   "data": data,
    // };
  } else {
    const todayBookingData = {
      "todayTotalBooking": 0,
      "todayTipsAmount": 0,
      "todayWorkedMins": 0,
      "todayProfessionalEarnings": 0,
      "todayProfessionalEarningsInCash": 0,
    };
    const todayRides = await this.adapter.model
      .aggregate(todayQuery)
      .allowDiskUse(true);
    if (
      !todayRides ||
      (todayRides &&
        todayRides[0]?.documentData &&
        todayRides[0].documentData.length === 0)
    ) {
      const respoData = { ...rides[0].documentData[0], ...todayBookingData };
      data["currencyCode"] = respoData.currencyCode;
      data["currencySymbol"] = respoData.currencySymbol;
      data["totalBookings"] = respoData.totalBookings;
      data["tipsAmount"] = respoData.tipsAmount;
      data["workedMins"] = respoData.workedMins;
      data["professionalEarnings"] = respoData.professionalEarnings;
      data["professionalEarningsInCash"] = respoData.professionalEarningsInCash;
      data["todayTotalBooking"] = respoData.todayTotalBooking;
      data["todayTipsAmount"] = respoData.todayTipsAmount;
      data["todayWorkedMins"] = respoData.todayWorkedMins;
      data["todayProfessionalEarnings"] = respoData.todayProfessionalEarnings;
      data["todayProfessionalEarningsInCash"] =
        respoData.todayProfessionalEarningsInCash;
      // return {
      //   "code": 200,
      //   "message": "",
      //   "data": data,
      // };
    } else {
      if (rides[0].documentData && rides[0].documentData.length > 0) {
        if (
          todayRides[0].documentData &&
          todayRides[0].documentData.length > 0
        ) {
          const respoData = {
            ...rides[0].documentData[0],
            ...todayRides[0].documentData[0],
          };
          data["currencyCode"] = respoData.currencyCode;
          data["currencySymbol"] = respoData.currencySymbol;
          data["totalBookings"] = respoData.totalBookings;
          data["tipsAmount"] = respoData.tipsAmount;
          data["workedMins"] = respoData.workedMins;
          data["professionalEarnings"] = respoData.professionalEarnings;
          data["professionalEarningsInCash"] =
            respoData.professionalEarningsInCash;
          data["todayTotalBooking"] = respoData.todayTotalBooking;
          data["todayTipsAmount"] = respoData.todayTipsAmount;
          data["todayWorkedMins"] = respoData.todayWorkedMins;
          data["todayProfessionalEarnings"] =
            respoData.todayProfessionalEarnings;
          data["todayProfessionalEarningsInCash"] =
            respoData.todayProfessionalEarningsInCash;

          // return {
          //   "code": 200,
          //   "message": "",
          //   "data": data,
          // };
        } else {
          const respoData = {
            ...rides[0].documentData[0],
            ...todayBookingData,
          };
          data["currencyCode"] = respoData.currencyCode;
          data["currencySymbol"] = respoData.currencySymbol;
          data["totalBookings"] = respoData.totalBookings;
          data["tipsAmount"] = respoData.tipsAmount;
          data["workedMins"] = respoData.workedMins;
          data["professionalEarnings"] = respoData.professionalEarnings;
          data["professionalEarningsInCash"] =
            respoData.professionalEarningsInCash;
          data["todayTotalBooking"] = respoData.todayTotalBooking;
          data["todayTipsAmount"] = respoData.todayTipsAmount;
          data["todayWorkedMins"] = respoData.todayWorkedMins;
          data["todayProfessionalEarnings"] =
            respoData.todayProfessionalEarnings;
          data["todayProfessionalEarningsInCash"] =
            respoData.todayProfessionalEarningsInCash;
          // return {
          //   "code": 200,
          //   "message": "",
          //   "data": data,
          // };
        }
      } else {
        data["totalBookings"] = 0;
        data["currencyCode"] = "USD";
        data["currencySymbol"] = "$";
        data["tipsAmount"] = 0;
        data["workedMins"] = 0;
        data["professionalEarnings"] = 0;
        data["professionalEarningsInCash"] = 0;
        data["todayTotalBooking"] = 0;
        data["todayTipsAmount"] = 0;
        data["todayWorkedMins"] = 0;
        data["todayProfessionalEarnings"] = 0;
        data["todayProfessionalEarningsInCash"] = 0;
        // return {
        //   "code": 200,
        //   "message": "",
        //   "data": data,
        // };
      }
    }
  }
  return {
    "code": 200,
    "message": "",
    "data": data,
  };
};

bookingAction.getSiteEarnings = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const fromTodayDate = new Date(
    new Date(generalSettings.data.lastBilledDate).setHours(0, 0, 0, 0)
  );
  let hubSiteCommissionQuery = null;
  const query = [],
    query1 = [],
    match = {};

  match["$and"] = [];
  match["$and"].push({
    //"clientId": mongoose.Types.ObjectId(clientId),
    "bookingStatus": {
      "$in": [
        constantUtil.USERCANCELLED,
        constantUtil.PROFESSIONALCANCELLED,
        constantUtil.ENDED,
      ],
    },
    "invoice.payableAmount": { "$gt": 0 },
  });

  if ((context.params?.billingId || "") !== "") {
    match["$and"].push({
      "billingId": mongoose.Types.ObjectId(context.params.billingId),
    });
  } else if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({ "bookingDate": { "$gte": fromDate, "$lt": toDate } });
  } else {
    match["$and"].push({ "bookingDate": { "$gte": fromTodayDate } });
  }
  if (context.params.city && context.params.city != "") {
    match["$and"].push({
      "category": mongoose.Types.ObjectId(context.params.city),
    });
  }
  if (context.params.vehicleCategory && context.params.vehicleCategory != "") {
    match["$and"].push({
      "vehicle.vehicleCategoryId": mongoose.Types.ObjectId(
        context.params.vehicleCategory
      ),
    });
  }
  if ((context.params?.paymentSectionFilter || "") !== "") {
    match["$and"].push({
      "paymentSection": context.params.paymentSectionFilter,
    });
  }
  if (
    context.params.isEnableFranchising &&
    context.params.loginUserType !== constantUtil.ADMIN &&
    context.params.loginUserType !== constantUtil.DEVELOPER
  ) {
    hubSiteCommissionQuery = {
      "$match": {
        "invoice.hubSiteCommission": { "$gt": 0 },
        "professional.vehicles.hub": mongoose.Types.ObjectId(
          context.params.hubId
        ),
      },
    };
  } else {
    hubSiteCommissionQuery = {
      "$match": {},
    };
  }
  // if (context.params.search && context.params.search != '') {
  //   const professionalObject = await this.broker.emit(
  //     'professional.searchRequest',
  //     { 'search': context.params.search }
  //   )
  //   match['$and'].push({
  //     'professional': mongoose.Types.ObjectId(professionalObject._id),
  //   })
  // }
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          ...hubSiteCommissionQuery,
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "invoice": "$invoice",
            "professionalCommisionWithSiteCommission": {
              "$add": [
                "$invoice.professionalCommision",
                "$invoice.siteCommission",
              ],
            },
            "siteCommission": "$invoice.siteCommission",
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phone": "$professional.phone",
            },
            "plateNumber": "$professional.vehicles.plateNumber",
            //
            // "estimationPayableAmount": {
            //   "$cond": {
            //     "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
            //     "then": "$invoice.estimationPayableAmount",
            //     "else": 0,
            //   },
            // },
            "payableAmount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": "$invoice.payableAmount",
                "else": 0,
              },
            },
            "couponAmount": {
              "$cond": {
                "if": {
                  $and: [
                    { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                    // {
                    //   "$ne": [
                    //     "$serviceCategory",
                    //     constantUtil.CONST_SHARERIDE.toLowerCase(),
                    //   ],
                    // },
                  ],
                },
                "then": "$invoice.couponAmount",
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": "$professional._id",
            "totalBookings": { "$sum": 1 },
            "professional": { "$first": "$professional" },
            "tipAmount": {
              "$sum": "$invoice.tipsAmount",
            },
            "discountAmount": {
              "$sum": "$couponAmount",
            },
            "estimationPayableAmount": {
              "$sum": "$invoice.estimationPayableAmount",
            },
            "totalAmount": {
              "$sum": "$payableAmount",
            },
            "professionalEarnings": {
              "$sum": "$invoice.professionalCommision",
            },
            "siteEarnings": {
              "$sum": "$invoice.siteCommission",
            },
            "totalCashInProfessional": {
              "$sum": "$invoice.amountInDriver",
            },
            "totalCashInSite": {
              "$sum": "$invoice.amountInSite",
            },
            "payoutAmount": {
              "$sum": "$invoice.payableAmount",
            },
            "professionalTolerenceAmount": {
              "$sum": "$invoice.professionalTolerenceAmount",
            },
            "roundingAmount": {
              "$sum": "$invoice.roundingAmount",
            },
            "hubSiteCommission": {
              "$sum": "$invoice.hubSiteCommission",
            },
            "surchargeFee": {
              "$sum": "$invoice.surchargeFee",
            },
          },
        },
      ],
    },
  });
  query1.push({
    "$match": match,
  });
  query1.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          ...hubSiteCommissionQuery,
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "invoice": "$invoice",
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phone": "$professional.phone",
            },
            "userCanceled": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.USERCANCELLED] },
                "then": 1,
                "else": 0,
              },
            },
            "professionalCanceled": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingStatus", constantUtil.PROFESSIONALCANCELLED],
                },
                "then": 1,
                "else": 0,
              },
            },
            "endedRide": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": 1,
                "else": 0,
              },
            },
            //
            "estimationPayableAmount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": "$invoice.estimationPayableAmount",
                "else": "$invoice.payableAmount", // For Canceled Ride
              },
            },
            // "payableAmount": {
            //   "$cond": {
            //     "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
            //     "then": "$invoice.payableAmount",
            //     "else": 0,
            //   },
            // },
            "couponAmount": {
              "$cond": {
                "if": {
                  $and: [
                    { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                    // {
                    //   "$ne": [
                    //     "$serviceCategory",
                    //     constantUtil.CONST_SHARERIDE.toLowerCase(),
                    //   ],
                    // },
                  ],
                },
                "then": "$invoice.couponAmount",
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": "",
            "totalBookings": { "$sum": 1 },
            "totalEndedBookings": { "$sum": "$endedRide" },
            "totalUserCanceledBookings": {
              "$sum": "$userCanceled",
            },
            "totalProfessionalCanceledBookings": {
              "$sum": "$professionalCanceled",
            },
            "totalEstimationPayableAmount": {
              "$sum": "$estimationPayableAmount",
            },
            "totalTripRevenue": {
              "$sum": "$invoice.payableAmount",
            },
            "totalProfessionalTolerenceAmount": {
              "$sum": "$invoice.professionalTolerenceAmount",
            },
            "totalProfessionalEarnings": {
              "$sum": "$invoice.professionalCommision",
            },
            "totalSiteEarnings": {
              "$sum": "$invoice.siteCommission",
            },
            "totalRoundingAmount": {
              "$sum": "$invoice.roundingAmount",
            },
            "totalDiscountAmount": {
              "$sum": "$couponAmount",
            },
            "totalHubSiteCommission": {
              "$sum": "$invoice.hubSiteCommission",
            },
            "totalSurchargeFee": {
              "$sum": "$invoice.surchargeFee",
            },
            "totalCashInProfessional": {
              "$sum": "$invoice.amountInDriver",
            },
            "totalCashInSite": {
              "$sum": "$invoice.amountInSite",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const jsonData1 = await this.adapter.model
    .aggregate(query1)
    .allowDiskUse(true);

  // responseJson['count'] =
  //   jsonData[0].response && jsonData[0].response.length > 0
  //     ? jsonData[0].response.length
  //     : 0
  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  responseJson["dashboardData"] =
    jsonData1[0].response && jsonData1[0].response.length > 0
      ? jsonData1[0].response[0]
      : {
          "_id": "",
          "totalBookings": 0,
          "totalEndedBookings": 0,
          "totalUserCanceledBookings": 0,
          "totalProfessionalCanceledBookings": 0,
          "totalEstimationPayableAmount": 0,
          "totalTripRevenue": 0,
          "totalProfessionalTolerenceAmount": 0,
          "totalProfessionalEarnings": 0,
          "totalSiteEarnings": 0,
          "totalRoundingAmount": 0,
          "totalDiscountAmount": 0,
          "totalHubSiteCommission": 0,
          "totalSurchargeFee": 0,
          "totalCashInProfessional": 0,
          "totalCashInSite": 0,
        };

  return responseJson;
};

bookingAction.getSiteEarningsSummary = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const fromTodayDate = new Date(
    new Date(generalSettings.data.lastBilledDate).setHours(0, 0, 0, 0)
  );

  // const skip =
  //   context.params.skip != 'undefined' && context.params.skip >= 0
  //     ? parseInt(context.params.skip)
  //     : 0
  // const limit =
  //   parseInt(context.params.limit ?? 20)
  let hubSiteCommissionQuery = null;
  const query = [],
    query1 = [],
    match = {};
  match["$and"] = [];
  match["$and"].push(
    // { "bookingStatus": constantUtil.ENDED },
    {
      "bookingStatus": {
        "$in": [
          constantUtil.USERCANCELLED,
          constantUtil.PROFESSIONALCANCELLED,
          constantUtil.ENDED,
        ],
      },
      "invoice.payableAmount": { "$gt": 0 },
    },
    { "professional": mongoose.Types.ObjectId(context.params.professionalId) }
  );

  if (context.params.billingId && context.params.billingId != "") {
    match["$and"].push({
      "billingId": mongoose.Types.ObjectId(context.params.billingId),
    });
  } else if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({ "bookingDate": { "$gte": fromDate, "$lt": toDate } });
  } else {
    match["$and"].push({ "bookingDate": { "$gte": fromTodayDate } });
  }
  if (context.params.city && context.params.city != "") {
    match["$and"].push({
      "category": mongoose.Types.ObjectId(context.params.city),
    });
  }
  if (context.params.vehicleCategory && context.params.vehicleCategory != "") {
    match["$and"].push({
      "vehicle.vehicleCategoryId": mongoose.Types.ObjectId(
        context.params.vehicleCategory
      ),
    });
  }
  if (
    (context.params.paymentSectionFilter || "") === constantUtil.CASHANDWALLET
  ) {
    match["$and"].push({
      "paymentSection": context.params.paymentSectionFilter,
    });
  }
  if (
    parseBooleans(context.params.isEnableFranchising) &&
    context.params.loginUserType !== constantUtil.ADMIN &&
    context.params.loginUserType !== constantUtil.DEVELOPER
  ) {
    hubSiteCommissionQuery = {
      "$match": {
        "invoice.hubSiteCommission": { "$gt": 0 },
        "professional.vehicles.hub": mongoose.Types.ObjectId(
          context.params.hubId
        ),
      },
    };
  } else {
    hubSiteCommissionQuery = {
      "$match": {},
    };
  }
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          ...hubSiteCommissionQuery,
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingStatus": "$bookingStatus",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "paymentOption": "$payment.option",
            "estimationPayableAmount": "$invoice.estimationPayableAmount",
            "totalFare": "$invoice.payableAmount",
            "payableAmount": "$invoice.payableAmount",
            "tipsAmount": "$invoice.tipsAmount",
            // "discountAmount": "$invoice.couponAmount",
            "discountAmount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": "$invoice.couponAmount",
                "else": 0,
              },
            },
            "amountInSite": "$invoice.amountInSite",
            "amountInDriver": "$invoice.amountInDriver",
            "professionalCommision": "$invoice.professionalCommision",
            "siteCommission": "$invoice.siteCommission",
            "hubSiteCommission": "$invoice.hubSiteCommission",
            "professionalTolerenceAmount":
              "$invoice.professionalTolerenceAmount",
            "surchargeFee": "$invoice.surchargeFee",
            "roundingAmount": "$invoice.roundingAmount",
          },
        },
      ],
    },
  });

  query1.push({
    "$match": match,
  });
  query1.push({
    "$facet": {
      "all": [{ "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          ...hubSiteCommissionQuery,
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingDate": "$bookingDate",
            "paymentOption": "$payment.option",
            "invoice": "$invoice",
            "couponAmount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": "$invoice.couponAmount",
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": "$professional._id",
            "tipsAmount": {
              "$sum": "$invoice.tipsAmount",
            },
            "discountAmount": {
              "$sum": "$couponAmount",
            },
            "estimationPayableAmount": {
              "$sum": "$invoice.estimationPayableAmount",
            },
            "totalFare": {
              "$sum": "$invoice.payableAmount",
            },
            "professionalCommision": {
              "$sum": "$invoice.professionalCommision",
            },
            "siteCommission": {
              "$sum": "$invoice.siteCommission",
            },
            "hubSiteCommission": {
              "$sum": "$invoice.hubSiteCommission",
            },
            "roundingAmount": {
              "$sum": "$invoice.roundingAmount",
            },
            "amountInDriver": {
              "$sum": "$invoice.amountInDriver",
            },
            "amountInSite": {
              "$sum": "$invoice.amountInSite",
            },
            "payableAmount": {
              "$sum": "$invoice.payableAmount",
            },
            "professionalTolerenceAmount": {
              "$sum": "$invoice.professionalTolerenceAmount",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  const jsonData1 = await this.adapter.model
    .aggregate(query1)
    .allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  responseJson["summaryResponse"] = jsonData1[0]?.response[0] || [];
  return responseJson;
};

bookingAction.adminRideList = async function (context) {
  const wishedTime = new Date();
  const wishedTimeStamp = 25 * 60 * 1000;
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const clientId = context.params.clientId || context.meta.clientId;
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "admin": mongoose.Types.ObjectId(context.meta.adminId.toString()),
    //"clientId": mongoose.Types.ObjectId(clientId.toString()),
  });
  if (context.params.filter !== "undefined") {
    const filterData =
      context.params.filter === constantUtil.AWAITING
        ? { "bookingStatus": constantUtil.AWAITING }
        : context.params.filter === constantUtil.ARRIVED
        ? { "bookingStatus": constantUtil.ARRIVED }
        : context.params.filter === constantUtil.ACCEPTED
        ? { "bookingStatus": constantUtil.ACCEPTED }
        : context.params.filter === constantUtil.USERDENY
        ? { "bookingStatus": constantUtil.USERDENY }
        : context.params.filter === constantUtil.USERCANCELLED
        ? { "bookingStatus": constantUtil.USERCANCELLED }
        : context.params.filter === constantUtil.CANCELLED
        ? { "bookingStatus": constantUtil.CANCELLED }
        : context.params.filter === constantUtil.PROFESSIONALCANCELLED
        ? { "bookingStatus": constantUtil.PROFESSIONALCANCELLED }
        : context.params.filter === constantUtil.EXPIRED
        ? { "bookingStatus": constantUtil.EXPIRED }
        : context.params.filter === constantUtil.STARTED
        ? { "bookingStatus": constantUtil.STARTED }
        : context.params.filter === constantUtil.ENDED
        ? { "bookingStatus": constantUtil.ENDED }
        : {};
    match["$and"].push(filterData);
  }

  query.push({
    "$match": match,
  });
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.phoneNumber": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.name": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "origin": "$origin",
            "destination": "$destination",
            "vehicle": "$vehicle",
            "user": {
              "_id": "$user._id",
              "firstName": "$user.firstName",
              "lastName": "$user.lastName",
              "email": "$user.email",
              "phone": "$user.phone",
            },
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phone": "$professional.phone",
            },
            "isAssignAllowed": {
              "$cond": {
                "if": {
                  "$lte": [
                    { "$subtract": ["$bookingDate", wishedTime] },
                    wishedTimeStamp,
                  ],
                },
                "then": true,
                "else": false,
              },
            },
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};

bookingAction.getUserRideList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "user": mongoose.Types.ObjectId(context.params.id),
  });
  // if (context.params.filter != 'undefined' && context.params.filter === true)
  //   match['$and'].push({ 'bookingStatus': constantUtil.AWAITING })

  query.push({
    "$match": match,
  });
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        // {
        //   "$lookup": {
        //     "from": "categories",
        //     "localField": "vehicles.vehicleCategoryId",
        //     "foreignField": "_id",
        //     "as": "vehicleCategory",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$vehicleCategory",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        // {
        //   "$project": {
        //     "_id": "$_id",
        //     "bookingId": "$bookingId",
        //     "bookingType": "$bookingType",
        //     "bookingStatus": "$bookingStatus",
        //     "bookingDate": "$bookingDate",
        //     "bookingFor": "$bookingFor",
        //     "origin": "$origin",
        //     "destination": "$destination",
        //     "vehicle": "$vehicle",
        //     "professional": {
        //       "_id": "$professional._id",
        //       "firstName": "$professional.firstName",
        //       "lastName": "$professional.lastName",
        //       "email": "$professional.email",
        //       "phone": "$professional.phone",
        //     },
        //   },
        // },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "origin": "$origin.fullAddress",
            "destination": "$destination.fullAddress",
            "vehicle": "$vehicle",
            //             "serviceArea": "$serviceArea",
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phone": "$professional.phone",
            },
            "professionalName": {
              "$concat": [
                "$professional.firstName",
                " ",
                "$professional.lastName",
              ],
            },
            "serviceCategory": "$serviceCategory",
            "vehicleCategory": "vehicleCategory",
            // "acceptTime": "$regionalData.acceptTime",
            // "pickUpTime": "$regionalData.pickUpTime",
            "acceptTime": "$activity.acceptTime",
            "pickUpTime": "$activity.pickUpTime",
            "arriveTime": "$activity.arriveTime",
            "dropTime": "$activity.dropTime",
            "cancelTime": "$activity.cancelTime",
            "denyOrExpireTime": "$activity.denyOrExpireTime",
            "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
            "rideStops": "$rideStops.fullAddress",
            "distanceUnit": "$distanceUnit",
            "payment": "$payment.option",
            "estimateDistance": "$bookedEstimation.distance",
            "estimateTime": "$bookedEstimation.time",
            "estimateFare": "$invoice.estimationAmount",
            "actualDistance": "$estimation.distance",
            "actualTime": "$estimation.time",
            "actualFare": "$invoice.payableAmount",
            "discount": "$invoice.couponAmount",
            "cancelledBy": "$activity.cancelledBy",
            "cancellationReason":
              "$cancelledProfessionalList.cancellationReason",
            "cancellationAmount": "$invoice.cancellationAmount",
            "rating": "$bookingReview.professionalReview.rating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};

bookingAction.getUserRideReportList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "user": mongoose.Types.ObjectId(context.params.id),
  });
  // if (context.params.filter != 'undefined' && context.params.filter === true)
  //   match['$and'].push({ 'bookingStatus': constantUtil.AWAITING })

  query.push({
    "$match": match,
  });
  if (context.params.bookingID) {
    query.push({
      "$match": {
        "bookingId": {
          "$regex": context.params.bookingID?.trim(),
          "$options": "si",
        },
      },
    });
  }

  // if (context.params.bookingStatus && context.params.bookingStatus !== "ALL") {
  //   query.push({
  //     "$match": {
  //       "bookingStatus": context.params.bookingStatus,
  //     },
  //   });
  // }
  if (
    context.params.bookingStatus &&
    context.params.bookingStatus !== constantUtil.ALL &&
    context.params.bookingStatus !== constantUtil.CANCELLED &&
    context.params.bookingStatus !== constantUtil.EXPIRED
  ) {
    query.push({
      "$match": {
        "bookingStatus": context.params.bookingStatus,
      },
    });
  } else if (context.params.bookingStatus === constantUtil.CANCELLED) {
    query.push({
      "$match": {
        "bookingStatus": {
          "$in": [
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.USERCANCELLED,
            constantUtil.CANCELLED,
          ],
        },
      },
    });
  } else if (context.params.bookingStatus === constantUtil.EXPIRED) {
    query.push({
      "$match": {
        "bookingStatus": {
          "$in": [
            constantUtil.EXPIRED,
            constantUtil.USERDENY,
            constantUtil.PROFESSIONALDENY,
          ],
        },
      },
    });
  }
  if (
    context.params.serviceCategory &&
    context.params.serviceCategory !== "ALL"
  ) {
    query.push({
      "$match": {
        "serviceCategory": context.params.serviceCategory,
      },
    });
  }
  if (context.params.bookingType && context.params.bookingType !== "ALL") {
    query.push({
      "$match": {
        "bookingType": context.params.bookingType,
      },
    });
  }
  if (context.params.paymentType && context.params.paymentType !== "ALL") {
    query.push({
      "$match": {
        "payment.option": context.params.paymentType,
      },
    });
  }
  if (
    context.params.customDatesOne &&
    context.params.customDatesOne != "" &&
    context.params.customDatesTwo &&
    context.params.customDatesTwo != ""
  ) {
    const fromDate = new Date(
      new Date(context.params.customDatesOne).setHours(0, 0, 0, 0)
      // new Date(context.params.customDatesOne)
    );
    const toDate = new Date(
      new Date(context.params.customDatesTwo).setHours(23, 59, 59, 999)
      // new Date(context.params.customDatesTwo)
    );
    query.push({
      "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push(
    {
      "$lookup": {
        "from": "professionals",
        "localField": "professional",
        "foreignField": "_id",
        "as": "professional",
      },
    },
    {
      "$unwind": {
        "path": "$professional",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "vehicle.vehicleCategoryId",
        "foreignField": "_id",
        "as": "vehicleCategory",
      },
    },
    {
      "$unwind": {
        "path": "$vehicleCategory",
        "preserveNullAndEmptyArrays": true,
      },
    }
  );
  if (
    context.params.vehicleCategory &&
    context.params.vehicleCategory !== "ALL"
  ) {
    query.push({
      "$match": {
        "vehicleCategory._id": mongoose.Types.ObjectId(
          context.params.vehicleCategory
        ),
      },
    });
  }
  if (context.params.name) {
    query.push({
      "$match": {
        "$or": [
          {
            "user.firstName": {
              "$regex": context.params.name?.trim(),
              "$options": "si",
            },
          },
          {
            "user.lastName": {
              "$regex": context.params.name?.trim(),
              "$options": "si",
            },
          },
        ],
      },
    });
  }
  if (context.params.phoneNumber) {
    query.push({
      "$match": {
        "user.phone.number": {
          "$regex": context.params.phoneNumber?.trim(),
          "$options": "si",
        },
      },
    });
  }
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },

        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        // {
        //   "$project": {
        //     "_id": "$_id",
        //     "bookingId": "$bookingId",
        //     "bookingType": "$bookingType",
        //     "bookingStatus": "$bookingStatus",
        //     "bookingDate": "$bookingDate",
        //     "bookingFor": "$bookingFor",
        //     "origin": "$origin",
        //     "destination": "$destination",
        //     "vehicle": "$vehicle",
        //     "professional": {
        //       "_id": "$professional._id",
        //       "firstName": "$professional.firstName",
        //       "lastName": "$professional.lastName",
        //       "email": "$professional.email",
        //       "phone": "$professional.phone",
        //     },
        //   },
        // },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            // "bookingDate": "$bookingDate",
            "bookingDate": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingType", constantUtil.SCHEDULE],
                },
                "then": "$createdAt", // For Schedule Booking Only, (createdAt as a bookingDate)
                "else": "$bookingDate",
              },
            },
            "scheduleTime": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingType", constantUtil.SCHEDULE],
                },
                "then": "$bookingDate",
                "else": "",
              },
            },
            "bookingFor": "$bookingFor",
            "origin": "$origin.fullAddress",
            "destination": "$destination.fullAddress",
            "vehicle": "$vehicle",
            //             "serviceArea": "$serviceArea",
            // "professional": {
            //   "_id": "$professional._id",
            //   "firstName": "$professional.firstName",
            //   "lastName": "$professional.lastName",
            //   "email": "$professional.email",
            //   "phone": "$professional.phone",
            // },
            "professionalName": {
              "$concat": [
                "$professional.firstName",
                " ",
                "$professional.lastName",
              ],
            },
            "professionalNumber": {
              "$concat": [
                "$professional.phone.code",
                " ",
                "$professional.phone.number",
              ],
            },
            "serviceCategory": "$serviceCategory",
            "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
            "bookingBy": "$bookingBy",
            // "acceptTime": "$regionalData.acceptTime",
            // "pickUpTime": "$regionalData.pickUpTime",
            // "scheduleTime": "$bookingDate",
            "acceptTime": "$activity.acceptTime",
            "pickUpTime": "$activity.pickUpTime",
            "arriveTime": "$activity.arriveTime",
            "dropTime": "$activity.dropTime",
            "cancelTime": "$activity.cancelTime",
            "denyOrExpireTime": "$activity.denyOrExpireTime",
            "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
            // "rideStops": "$rideStops.fullAddress",
            "rideStops": "$activity.rideStops",
            "distanceUnit": "$distanceUnit",
            "payment": "$payment.option",
            "estimateDistance": "$bookedEstimation.distance",
            "estimateTime": "$bookedEstimation.time",
            "estimateFare": "$invoice.estimationAmount",
            "actualDistance": "$estimation.distance",
            "actualTime": "$estimation.time",
            "actualFare": "$invoice.payableAmount",
            "discount": "$invoice.couponAmount",
            "cancelledBy": "$activity.cancelledBy.userType",
            "cancellationReason":
              "$cancelledProfessionalList.cancellationReason",
            "cancellationAmount": "$invoice.cancellationAmount",
            "rating": "$bookingReview.professionalReview.rating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};

bookingAction.getProfessionalRideList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "professional": mongoose.Types.ObjectId(context.params.id),
  });
  // if (context.params.filter != 'undefined' && context.params.filter === true)
  //   match['$and'].push({ 'bookingStatus': constantUtil.AWAITING })

  query.push({
    "$match": match,
  });
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push(
    {
      "$lookup": {
        "from": "users",
        "localField": "user",
        "foreignField": "_id",
        "as": "user",
      },
    },
    {
      "$unwind": {
        "path": "$user",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$lookup": {
        "from": "admins",
        "localField": "vehicle.vehicleCategoryId",
        "foreignField": "_id",
        "as": "vehicleCategory",
      },
    },
    {
      "$unwind": {
        "path": "$vehicleCategory",
        "preserveNullAndEmptyArrays": true,
      },
    }
  );
  if (
    context.params.vehicleCategory &&
    context.params.vehicleCategory !== "ALL"
  ) {
    query.push({
      "$match": {
        "vehicleCategory._id": mongoose.Types.ObjectId(
          context.params.vehicleCategory
        ),
      },
    });
  }
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        // {
        //   "$project": {
        //     "_id": "$_id",
        //     "bookingId": "$bookingId",
        //     "bookingType": "$bookingType",
        //     "bookingStatus": "$bookingStatus",
        //     "bookingDate": "$bookingDate",
        //     "bookingFor": "$bookingFor",
        //     "origin": "$origin",
        //     "destination": "$destination",
        //     "vehicle": "$vehicle",
        //   },
        // },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "origin": "$origin.fullAddress",
            "destination": "$destination.fullAddress",
            "vehicle": "$vehicle",
            //             "serviceArea": "$serviceArea",
            // "professional": {
            //   "_id": "$professional._id",
            //   "firstName": "$professional.firstName",
            //   "lastName": "$professional.lastName",
            //   "email": "$professional.email",
            //   "phone": "$professional.phone",
            // },
            "userName": {
              "$concat": ["$user.firstName", " ", "$user.lastName"],
            },
            "userPhoneNumber": {
              "$concat": ["$user.phone.code", " ", "$user.phone.number"],
            },
            "serviceCategory": "$serviceCategory",
            "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
            "bookingBy": "$bookingBy",
            // "acceptTime": "$regionalData.acceptTime",
            // "pickUpTime": "$regionalData.pickUpTime",
            "scheduleTime": "$regionalData.scheduleTime",
            "acceptTime": "$activity.acceptTime",
            "pickUpTime": "$activity.pickUpTime",
            "arriveTime": "$activity.arriveTime",
            "dropTime": "$activity.dropTime",
            "cancelTime": "$activity.cancelTime",
            "denyOrExpireTime": "$activity.denyOrExpireTime",
            "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
            "rideStops": "$rideStops.fullAddress",
            "distanceUnit": "$distanceUnit",
            "payment": "$payment.option",
            "estimateDistance": "$bookedEstimation.distance",
            "estimateTime": "$bookedEstimation.time",
            "estimateFare": "$invoice.estimationAmount",
            "actualDistance": "$estimation.distance",
            "actualTime": "$estimation.time",
            "actualFare": "$invoice.payableAmount",
            "professionalCommision": "$invoice.professionalCommision",
            "discount": "$invoice.couponAmount",
            "cancelledBy": "$activity.cancelledBy",
            "cancellationReason":
              "$cancelledProfessionalList.cancellationReason",
            "cancellationAmount": "$invoice.cancellationAmount",
            "rating": "$bookingReview.professionalReview.rating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};
bookingAction.getProfessionalRideReportList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "professional": mongoose.Types.ObjectId(context.params.id),
  });
  // if (context.params.filter != 'undefined' && context.params.filter === true)
  //   match['$and'].push({ 'bookingStatus': constantUtil.AWAITING })

  query.push({
    "$match": match,
  });
  if (context.params.bookingID) {
    query.push({
      "$match": {
        "bookingId": { "$regex": context.params.bookingID, "$options": "si" },
      },
    });
  }
  if (context.params.name) {
    query.push({
      "$match": {
        "bookingFor.name": { "$regex": context.params.name, "$options": "si" },
      },
    });
  }
  if (context.params.phoneNumber) {
    query.push({
      "$match": {
        "bookingFor.phoneNumber": {
          "$regex": context.params.phoneNumber?.trim(),
          "$options": "si",
        },
      },
    });
  }
  // if (context.params.bookingStatus && context.params.bookingStatus !== "ALL") {
  //   query.push({
  //     "$match": {
  //       "bookingStatus": context.params.bookingStatus,
  //     },
  //   });
  // }
  if (
    context.params.bookingStatus &&
    context.params.bookingStatus !== constantUtil.ALL &&
    context.params.bookingStatus !== constantUtil.CANCELRIDE &&
    context.params.bookingStatus !== constantUtil.EXPIRED
  ) {
    query.push({
      "$match": {
        "bookingStatus": context.params.bookingStatus,
      },
    });
  } else if (context.params.bookingStatus === constantUtil.CANCELLED) {
    query.push({
      "$match": {
        "bookingStatus": {
          "$in": [
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.USERCANCELLED,
            constantUtil.CANCELLED,
          ],
        },
      },
    });
  } else if (context.params.bookingStatus === constantUtil.EXPIRED) {
    query.push({
      "$match": {
        "bookingStatus": {
          "$in": [
            constantUtil.EXPIRED,
            constantUtil.USERDENY,
            constantUtil.PROFESSIONALDENY,
          ],
        },
      },
    });
  }
  if (
    context.params.serviceCategory &&
    context.params.serviceCategory !== "ALL"
  ) {
    query.push({
      "$match": {
        "serviceCategory": context.params.serviceCategory,
      },
    });
  }
  // if (context.params.vehicleCategory) {
  //   query.push({
  //     "$match": {
  //       "vehicleCategory": context.params.vehicleCategory,
  //     },
  //   });
  // }

  if (context.params.bookingType && context.params.bookingType !== "ALL") {
    query.push({
      "$match": {
        "bookingType": context.params.bookingType,
      },
    });
  }
  if (context.params.paymentType && context.params.paymentType !== "ALL") {
    query.push({
      "$match": {
        "payment.option": context.params.paymentType,
      },
    });
  }
  if (
    context.params.customDatesOne &&
    context.params.customDatesOne != "" &&
    context.params.customDatesTwo &&
    context.params.customDatesTwo != ""
  ) {
    const fromDate = new Date(
      new Date(context.params.customDatesOne).setHours(0, 0, 0, 0)
      // new Date(context.params.customDatesOne)
    );
    const toDate = new Date(
      new Date(context.params.customDatesTwo).setHours(23, 59, 59, 999)
      // new Date(context.params.customDatesTwo)
    );
    query.push({
      "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
    });
  }
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push(
    {
      "$lookup": {
        "from": "admins",
        "localField": "vehicle.vehicleCategoryId",
        "foreignField": "_id",
        "as": "vehicleCategory",
      },
    },
    {
      "$unwind": {
        "path": "$vehicleCategory",
        "preserveNullAndEmptyArrays": true,
      },
    }
  );
  if (
    context.params.vehicleCategory &&
    context.params.vehicleCategory !== "ALL"
  ) {
    query.push({
      "$match": {
        "vehicleCategory._id": mongoose.Types.ObjectId(
          context.params.vehicleCategory
        ),
      },
    });
  }
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        // {
        //   "$project": {
        //     "_id": "$_id",
        //     "bookingId": "$bookingId",
        //     "bookingType": "$bookingType",
        //     "bookingStatus": "$bookingStatus",
        //     "bookingDate": "$bookingDate",
        //     "bookingFor": "$bookingFor",
        //     "origin": "$origin",
        //     "destination": "$destination",
        //     "vehicle": "$vehicle",
        //   },
        // },
        // {
        //   "$lookup": {
        //     "from": "categories",
        //     "localField": "category",
        //     "foreignField": "_id",
        //     "as": "serviceArea",
        //   },
        // },
        // {
        //   "$unwind": {
        //     "path": "$serviceArea",
        //     "preserveNullAndEmptyArrays": true,
        //   },
        // },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            // "bookingDate": "$bookingDate",
            "bookingDate": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingType", constantUtil.SCHEDULE],
                },
                "then": "$createdAt", // For Schedule Booking Only, (createdAt as a bookingDate)
                "else": "$bookingDate",
              },
            },
            "scheduleTime": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingType", constantUtil.SCHEDULE],
                },
                "then": "$bookingDate",
                "else": "",
              },
            },
            "bookingFor": "$bookingFor",
            "origin": "$origin.fullAddress",
            "destination": "$destination.fullAddress",
            "vehicle": "$vehicle",
            "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
            //             "serviceArea": "$serviceArea",
            // "professional": {
            //   "_id": "$professional._id",
            //   "firstName": "$professional.firstName",
            //   "lastName": "$professional.lastName",
            //   "email": "$professional.email",
            //   "phone": "$professional.phone",
            // },
            "professionalName": {
              "$concat": [
                "$professional.firstName",
                " ",
                "$professional.lastName",
              ],
            },
            "serviceCategory": "$serviceCategory",
            // "acceptTime": "$regionalData.acceptTime",
            // "pickUpTime": "$regionalData.pickUpTime",
            "acceptTime": "$activity.acceptTime",
            "pickUpTime": "$activity.pickUpTime",
            "arriveTime": "$activity.arriveTime",
            "dropTime": "$activity.dropTime",
            "cancelTime": "$activity.cancelTime",
            "denyOrExpireTime": "$activity.denyOrExpireTime",
            "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
            // "rideStops": "$rideStops.fullAddress",
            "rideStops": "$activity.rideStops",
            //  "distanceUnit": "$serviceArea.distanceType",
            "distanceUnit": "$distanceUnit",
            "payment": "$payment.option",
            "estimateDistance": "$bookedEstimation.distance",
            "estimateTime": "$bookedEstimation.time",
            "estimateFare": "$invoice.estimationAmount",
            "actualDistance": "$estimation.distance",
            "actualTime": "$estimation.time",
            "actualFare": "$invoice.payableAmount",
            "discount": "$invoice.couponAmount",
            "cancelledBy": "$activity.cancelledBy.userType",
            "cancellationReason":
              "$cancelledProfessionalList.cancellationReason",
            "cancellationAmount": "$invoice.cancellationAmount",
            "rating": "$bookingReview.professionalReview.rating",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};

bookingAction.getAppUsageList = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    if (
      context.params.createdAtFrom &&
      context.params.createdAtFrom != "" &&
      context.params.createdAtTo &&
      context.params.createdAtTo != ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    // if (context.params.createdAtFrom && context.params.createdAtTo) {
    //   query.push({
    //     "$match": {
    //       "bookingDate": {
    //         "$gte": context.params.createdAtFrom,
    //         "$lt": context.params.createdAtTo,
    //       },
    //     },
    //   });
    // }
    if (
      context.params.bookingStatus &&
      context.params.bookingStatus !== constantUtil.ALL &&
      context.params.bookingStatus !== constantUtil.CANCELRIDE &&
      context.params.bookingStatus !== constantUtil.EXPIRED
    ) {
      query.push({
        "$match": {
          "bookingStatus": context.params.bookingStatus,
        },
      });
    } else if (context.params.bookingStatus === constantUtil.CANCELRIDE) {
      query.push({
        "$match": {
          "bookingStatus": {
            "$in": [
              constantUtil.PROFESSIONALCANCELLED,
              constantUtil.USERCANCELLED,
              constantUtil.CANCELLED,
            ],
          },
        },
      });
    } else if (context.params.bookingStatus === constantUtil.EXPIRED) {
      query.push({
        "$match": {
          "bookingStatus": {
            "$in": [
              constantUtil.EXPIRED,
              constantUtil.USERDENY,
              constantUtil.PROFESSIONALDENY,
            ],
          },
        },
      });
    }
    if (context.params.bookingBy && context.params.bookingBy !== "ALL") {
      query.push({
        "$match": { "bookingBy": context.params.bookingBy },
      });
    }
    if (context.params.phoneNumber) {
      query.push({
        "$match": {
          "phone.number": {
            "$regex": context.params.phoneNumber?.trim(),
            "$options": "si",
          },
        },
      });
    }

    query.push(
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$project": {
          "_id": "$_id",
          "bookingDate": "$bookingDate",
          "professionalName": {
            "$concat": [
              "$professional.firstName",
              " ",
              "$professional.lastName",
            ],
          },
          "professionalID": "$professional._id",
          "professionalNumber": {
            "$concat": [
              "$professional.phone.code",
              " ",
              "$professional.phone.number",
            ],
          },
          "plateNumber": "$professional.vehicles.plateNumber",
          "bookingStatus": "$bookingStatus",
          "guestType": "$guestType",
          "avgRating": "$professional.review.avgRating",
          "payableAmount": "$invoice.payableAmount",
          "completedTrip": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "ENDED"] },
              "then": 1,
              "else": 0,
            },
          },
          "professionalCanceledTrip": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "PROFESSIONALCANCELLED"] },
              "then": 1,
              "else": 0,
            },
          },
          "userCanceledTrip": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "USERCANCELLED"] },
              "then": 1,
              "else": 0,
            },
          },
          "Expired": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "EXPIRED"] },
              "then": 1,
              "else": 0,
            },
          },
          "userDeny": {
            "$cond": {
              "if": { "$eq": ["$bookingStatus", "USERDENY"] },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": "$professionalID",
          "bookingDate": { "$first": "$bookingDate" },
          "professionalName": { "$first": "$professionalName" },
          "professionalNumber": { "$first": "$professionalNumber" },
          "plateNumber": { "$first": "$plateNumber" },
          "bookingStatus": { "$first": "$bookingStatus" },
          "guestType": { "$first": "$guestType" },
          "avgRating": { "$first": "$avgRating" },
          "totalTrip": { "$sum": 1 },
          "completedTrip": { "$sum": "$completedTrip" },
          "professionalCanceledTrip": { "$sum": "$professionalCanceledTrip" },
          "userCanceledTrip": { "$sum": "$userCanceledTrip" },
          "Expired": { "$sum": "$Expired" },
          "userDeny": { "$sum": "$userDeny" },
          "payableAmount": { "$sum": "$payableAmount" },
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};

bookingAction.getProfessionalCommissionReport = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    if (
      context.params.createdAtFrom &&
      context.params.createdAtFrom != "" &&
      context.params.createdAtTo &&
      context.params.createdAtTo != ""
    ) {
      const fromDate = new Date(
        new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
      );

      // const fromDate = new Date(new Date("1 jul 2023").setHours(0, 0, 0, 0));
      // const toDate = new Date(
      //   new Date("30 sep 2023").setHours(23, 59, 59, 999)
      // );
      query.push({
        "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
      });
    }

    // if (
    //   context.params.bookingStatus &&
    //   context.params.bookingStatus !== constantUtil.ALL &&
    //   context.params.bookingStatus !== constantUtil.CANCELRIDE &&
    //   context.params.bookingStatus !== constantUtil.EXPIRED
    // ) {
    //   query.push({
    //     "$match": {
    //       "bookingStatus": context.params.bookingStatus,
    //     },
    //   });
    // } else if (context.params.bookingStatus === constantUtil.CANCELRIDE) {
    //   query.push({
    //     "$match": {
    //       "bookingStatus": {
    //         "$in": [
    //           constantUtil.PROFESSIONALCANCELLED,
    //           constantUtil.USERCANCELLED,
    //           constantUtil.CANCELLED,
    //         ],
    //       },
    //     },
    //   });
    // } else if (context.params.bookingStatus === constantUtil.EXPIRED) {
    //   query.push({
    //     "$match": {
    //       "bookingStatus": {
    //         "$in": [
    //           constantUtil.EXPIRED,
    //           constantUtil.USERDENY,
    //           constantUtil.PROFESSIONALDENY,
    //         ],
    //       },
    //     },
    //   });
    // }
    query.push({
      "$match": {
        "bookingStatus": constantUtil.ENDED,
      },
    });
    // if (context.params.bookingBy && context.params.bookingBy !== "ALL") {
    //   query.push({
    //     "$match": { "bookingBy": context.params.bookingBy },
    //   });
    // }
    // if (context.params.phoneNumber) {
    //   query.push({
    //     "$match": {
    //       "phone.number": {
    //         "$regex": context.params.phoneNumber,
    //         "$options": "si",
    //       },
    //     },
    //   });
    // }

    query.push(
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$project": {
          "_id": "$_id",
          "bookingDate": "$bookingDate",
          "professionalName": {
            "$concat": [
              "$professional.firstName",
              " ",
              "$professional.lastName",
            ],
          },
          "professionalID": "$professional._id",
          "professionalNumber": {
            "$concat": [
              "$professional.phone.code",
              " ",
              "$professional.phone.number",
            ],
          },
          "plateNumber": "$professional.vehicles.plateNumber",
          "tinNumber": "$professional.tin",
          // "bookingStatus": "$bookingStatus",
          // "guestType": "$guestType",
          // "avgRating": "$professional.review.avgRating",
          "professionalCommision": "$invoice.professionalCommision",
          "payableAmount": "$invoice.payableAmount",
          // "completedTrip": {
          //   "$cond": {
          //     "if": { "$eq": ["$bookingStatus", "ENDED"] },
          //     "then": 1,
          //     "else": 0,
          //   },
          // },
          // "professionalCanceledTrip": {
          //   "$cond": {
          //     "if": { "$eq": ["$bookingStatus", "PROFESSIONALCANCELLED"] },
          //     "then": 1,
          //     "else": 0,
          //   },
          // },
          // "userCanceledTrip": {
          //   "$cond": {
          //     "if": { "$eq": ["$bookingStatus", "USERCANCELLED"] },
          //     "then": 1,
          //     "else": 0,
          //   },
          // },
          // "Expired": {
          //   "$cond": {
          //     "if": { "$eq": ["$bookingStatus", "EXPIRED"] },
          //     "then": 1,
          //     "else": 0,
          //   },
          // },
          // "userDeny": {
          //   "$cond": {
          //     "if": { "$eq": ["$bookingStatus", "USERDENY"] },
          //     "then": 1,
          //     "else": 0,
          //   },
          // },
        },
      },
      {
        "$group": {
          "_id": "$professionalID",
          // "bookingDate": { "$first": "$bookingDate" },
          "professionalName": { "$first": "$professionalName" },
          "professionalNumber": { "$first": "$professionalNumber" },
          "plateNumber": { "$first": "$plateNumber" },
          "tinNumber": { "$first": "$tinNumber" },
          // "bookingStatus": { "$first": "$bookingStatus" },
          // "guestType": { "$first": "$guestType" },
          // "avgRating": { "$first": "$avgRating" },
          "totalTrip": { "$sum": 1 },
          // "completedTrip": { "$sum": "$completedTrip" },
          // "professionalCanceledTrip": { "$sum": "$professionalCanceledTrip" },
          // "userCanceledTrip": { "$sum": "$userCanceledTrip" },
          // "Expired": { "$sum": "$Expired" },
          // "userDeny": { "$sum": "$userDeny" },
          "professionalCommision": { "$sum": "$professionalCommision" },
          "payableAmount": { "$sum": "$payableAmount" },
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};

// bookingAction.getProfessionalRankingReport_backup = async function (context) {
//   let errorCode = 200,
//     message = "",
//     errorMessage = "",
//     jsonData = {},
//     matchPhoneNumberQuery = {},
//     matchServiceAreaQuery = {};
//   try {
//     const query = [];
//     // FOR EXPORT DATE FILTER
//     if (
//       (context.params?.createdAtFrom || "") !== "" &&
//       (context.params?.createdAtTo || "") !== ""
//     ) {
//       const fromDate = new Date(
//         new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
//       );
//       const toDate = new Date(
//         new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
//       );
//       query.push({
//         "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
//       });
//     }
//     //
//     if ((context.params?.phoneNumber || "") !== "") {
//       matchPhoneNumberQuery = {
//         "$match": {
//           "allProfessional.phone.number": {
//             "$regex": context.params.phoneNumber?.trim(),
//             "$options": "si",
//           },
//         },
//       };
//     } else {
//       matchPhoneNumberQuery = {
//         "$match": {
//           "allProfessional.phone.number": { "$ne": null },
//         },
//       };
//     }
//     //
//     if (
//       (context.params.city || "") !== "" &&
//       context.params.city !== constantUtil.ALL
//     ) {
//       // query.push({
//       //   "$match": {
//       //     "category": mongoose.Types.ObjectId(context.params.city),
//       //   },
//       // });
//       matchServiceAreaQuery = {
//         "$match": {
//           "allProfessional.serviceAreaId": mongoose.Types.ObjectId(
//             context.params.city
//           ),
//         },
//       };
//     } else {
//       matchServiceAreaQuery = {
//         "$match": {
//           "allProfessional.serviceAreaId": { "$ne": null },
//         },
//       };
//     }
//     query.push({
//       "$match": {
//         "bookingStatus": {
//           $in: [
//             constantUtil.ENDED,
//             // constantUtil.PROFESSIONALDENY,
//             constantUtil.PROFESSIONALCANCELLED,
//           ],
//         },
//       },
//     });
//     query.push(
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "requestSendProfessionals.professionalId",
//           "foreignField": "_id",
//           "as": "requestSendProfessionals",
//         },
//       },
//       {
//         "$unwind": {
//           "path": "$requestSendProfessionals",
//           "preserveNullAndEmptyArrays": true,
//         },
//       },
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "denyProfessionals.professionalId",
//           "foreignField": "_id",
//           "as": "denyProfessionals",
//         },
//       },
//       {
//         "$unwind": {
//           "path": "$denyProfessionals",
//           "preserveNullAndEmptyArrays": true,
//         },
//       },
//       {
//         "$project": {
//           "_id": "$_id",
//           "bookingDate": "$bookingDate",
//           "bookingId": "$bookingId",
//           "bookingStatus": "$bookingStatus",
//           "professional": "$professional",
//           "denyProfessionals": "$denyProfessionals",
//           "requestSendProfessionals": "$requestSendProfessionals",
//         },
//       },
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "denyProfessionals._id",
//           "foreignField": "_id",
//           "as": "newdenyProfessionals",
//         },
//       },
//       {
//         "$unwind": {
//           "path": "$newdenyProfessionals",
//           "preserveNullAndEmptyArrays": true,
//         },
//       },
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "requestSendProfessionals._id",
//           "foreignField": "_id",
//           "as": "newRequestSendProfessionals",
//         },
//       },
//       {
//         "$unwind": {
//           "path": "$newRequestSendProfessionals",
//           "preserveNullAndEmptyArrays": true,
//         },
//       },
//       {
//         "$project": {
//           "_id": "$_id",
//           "bookingDate": "$bookingDate",
//           "bookingId": "$bookingId",
//           "bookingStatus": "$bookingStatus",
//           "professional": "$professional",
//           // "allProfessional": {
//           //   "$cond": {
//           //     "if": { "$eq": ["$newdenyProfessionals._id", "$professional"] },
//           //     "then": "$newdenyProfessionals._id",
//           //     "else": "$newRequestSendProfessionals._id",
//           //   },
//           // },
//           "allProfessional": {
//             "$cond": {
//               "if": {
//                 "$eq": ["$newRequestSendProfessionals._id", "$professional"],
//                 "$eq": ["$bookingStatus", constantUtil.ENDED],
//               },
//               "then": "$newRequestSendProfessionals._id",
//               "else": "$professional",
//             },
//           },
//           // "allProfessional": {
//           //   "$cond": {
//           //     "if": {
//           //       "$eq": ["$newRequestSendProfessionals._id", "$professional"],
//           //     },
//           //     "then": {
//           //       "$cond": {
//           //         "if": {
//           //           "$eq": ["$bookingStatus", constantUtil.ENDED],
//           //         },
//           //         "then": "$newRequestSendProfessionals._id",
//           //         "else": "$professional",
//           //       },
//           //     },
//           //     "else": "$professional",
//           //   },
//           // },
//         },
//       },
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "allProfessional",
//           "foreignField": "_id",
//           "as": "allProfessional",
//         },
//       },
//       {
//         "$unwind": {
//           "path": "$allProfessional",
//           "preserveNullAndEmptyArrays": true,
//         },
//       },
//       matchPhoneNumberQuery,
//       matchServiceAreaQuery
//     );

//     query.push({
//       "$facet": {
//         "totalCount": [
//           {
//             "$count": "count",
//           },
//         ],
//         "responseData": [
//           {
//             "$project": {
//               "_id": 0,
//               "bookingDate": "$bookingDate",
//               "bookingId": "$bookingId",
//               "bookingStatus": "$bookingStatus",
//               "professionalName": {
//                 "$concat": [
//                   "$allProfessional.firstName",
//                   " ",
//                   "$allProfessional.lastName",
//                 ],
//               },
//               "professionalNumber": {
//                 "$concat": [
//                   "$allProfessional.phone.code",
//                   " ",
//                   "$allProfessional.phone.number",
//                 ],
//               },
//               "professionalId": "$allProfessional._id",
//               "professionalStatus": "$allProfessional.status",
//               "blockedTill": "$allProfessional.blockedTill",
//               "completedTrip": {
//                 "$cond": {
//                   "if": {
//                     "$eq": ["$allProfessional._id", "$professional"],
//                     "$eq": ["$bookingStatus", constantUtil.ENDED],
//                   },
//                   "then": 1,
//                   "else": 0,
//                 },
//               },
//               "deniedTrip": {
//                 "$cond": {
//                   "if": {
//                     "$ne": ["$allProfessional._id", "$professional"],
//                     "$ne": ["$bookingStatus", constantUtil.ENDED],
//                   },
//                   "then": 1,
//                   "else": 0,
//                 },
//               },
//             },
//           },
//           {
//             "$group": {
//               "_id": "$professionalId",
//               // "bookingDate": { "$first": "$bookingDate" },
//               // "bookingId": { "$first": "$bookingId" },
//               "professionalName": { "$first": "$professionalName" },
//               "professionalNumber": { "$first": "$professionalNumber" },
//               "professionalStatus": { "$first": "$professionalStatus" },
//               "blockedTill": { "$first": "$blockedTill" },
//               "totalTrip": { "$sum": 1 },
//               "completedTrip": { "$sum": "$completedTrip" },
//               "completedRatio": {
//                 "$avg": { "$multiply": [{ "$sum": 1 }, "$completedTrip"] },
//               },
//               "deniedTrip": { "$sum": "$deniedTrip" },
//               "deniedRatio": {
//                 "$avg": { "$multiply": [{ "$sum": 1 }, "$deniedTrip"] },
//               },
//             },
//           },
//           // {
//           //   "$sort":
//           //     context.params.sortBy === "acceptanceRate"
//           //       ? { "completedRatio": -1 }
//           //       : { "deniedRatio": -1 },
//           // },
//           { "$sort": { "deniedRatio": -1 } },
//           { "$skip": context.params.skip },
//           { "$limit": context.params.limit },
//           // { "$sort": { "deniedRatio": -1 } },
//         ],
//       },
//     });
//     jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
//     //const totalCount = jsonData?.[0]?.responseData?.length;
//     // if (totalCount <= context.params.limit) {
//     //   jsonData[0]["totalCount"][0]["count"] = totalCount;
//     // }
//     const xewrqr = "43425";
//   } catch (e) {
//     errorCode = 400;
//     errorMessage = e.message;
//     message = "SOMTHING WENT WORNG";
//   }
//   return {
//     "errorCode": errorCode,
//     "message": message,
//     "errorMessage": errorMessage,
//     "data": jsonData,
//   };
// };

bookingAction.getProfessionalRankingReport = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = [];
  // matchPhoneNumberQuery = {},
  // matchServiceAreaQuery = {};
  try {
    // const query = [];
    // // FOR EXPORT DATE FILTER
    // if (
    //   (context.params?.createdAtFrom || "") !== "" &&
    //   (context.params?.createdAtTo || "") !== ""
    // ) {
    //   const fromDate = new Date(
    //     new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
    //   );
    //   const toDate = new Date(
    //     new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
    //   );
    //   query.push({
    //     "$match": { "bookingDate": { "$gte": fromDate, "$lt": toDate } },
    //   });
    // }
    // //
    // if ((context.params?.phoneNumber || "") !== "") {
    //   matchPhoneNumberQuery = {
    //     "$match": {
    //       "allProfessional.phone.number": {
    //         "$regex": context.params.phoneNumber?.trim(),
    //         "$options": "si",
    //       },
    //     },
    //   };
    // } else {
    //   matchPhoneNumberQuery = {
    //     "$match": {
    //       "allProfessional.phone.number": { "$ne": null },
    //     },
    //   };
    // }
    // //
    // if (
    //   (context.params.city || "") !== "" &&
    //   context.params.city !== constantUtil.ALL
    // ) {
    //   // query.push({
    //   //   "$match": {
    //   //     "category": mongoose.Types.ObjectId(context.params.city),
    //   //   },
    //   // });
    //   matchServiceAreaQuery = {
    //     "$match": {
    //       "allProfessional.serviceAreaId": mongoose.Types.ObjectId(
    //         context.params.city
    //       ),
    //     },
    //   };
    // } else {
    //   matchServiceAreaQuery = {
    //     "$match": {
    //       "allProfessional.serviceAreaId": { "$ne": null },
    //     },
    //   };
    // }
    // // query.push({
    // //   "$match": {
    // //     "bookingStatus": {
    // //       $in: [
    // //         constantUtil.ENDED,
    // //         // constantUtil.PROFESSIONALDENY,
    // //         constantUtil.PROFESSIONALCANCELLED,
    // //       ],
    // //     },
    // //   },
    // // });
    // query.push(
    //   // {
    //   //   "$lookup": {
    //   //     "from": "professionals",
    //   //     "localField": "requestSendProfessionals.professionalId",
    //   //     "foreignField": "_id",
    //   //     "as": "requestSendProfessionals",
    //   //   },
    //   // },
    //   // {
    //   //   "$unwind": {
    //   //     "path": "$requestSendProfessionals",
    //   //     "preserveNullAndEmptyArrays": true,
    //   //   },
    //   // },
    //   // {
    //   //   "$lookup": {
    //   //     "from": "professionals",
    //   //     "localField": "denyProfessionals.professionalId",
    //   //     "foreignField": "_id",
    //   //     "as": "denyProfessionals",
    //   //   },
    //   // },
    //   // {
    //   //   "$unwind": {
    //   //     "path": "$denyProfessionals",
    //   //     "preserveNullAndEmptyArrays": true,
    //   //   },
    //   // },
    //   {
    //     "$project": {
    //       "_id": "$_id",
    //       "bookingDate": "$bookingDate",
    //       "bookingId": "$bookingId",
    //       "bookingStatus": "$bookingStatus",
    //       "professional": "$professional",
    //       "denyProfessionals": "$denyProfessionals",
    //       "cancelledProfessional": "$cancelledProfessionalList",
    //       "requestSendProfessionals": "$requestSendProfessionals",
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "professionals",
    //       "localField": "denyProfessionals.professionalId",
    //       "foreignField": "_id",
    //       "as": "newdenyProfessionals",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$newdenyProfessionals",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "professionals",
    //       "localField": "cancelledProfessional.professionalId",
    //       "foreignField": "_id",
    //       "as": "newCancelProfessionals",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$newCancelProfessionals",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "professionals",
    //       "localField": "requestSendProfessionals.professionalId",
    //       "foreignField": "_id",
    //       "as": "newRequestSendProfessionals",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$newRequestSendProfessionals",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$project": {
    //       "_id": "$_id",
    //       "bookingDate": "$bookingDate",
    //       "bookingId": "$bookingId",
    //       "bookingStatus": "$bookingStatus",
    //       "professional": "$professional",
    //       // "allProfessional": {
    //       //   "$cond": {
    //       //     "if": { "$eq": ["$newdenyProfessionals._id", "$professional"] },
    //       //     "then": "$newdenyProfessionals._id",
    //       //     "else": "$newRequestSendProfessionals._id",
    //       //   },
    //       // },
    //       "allProfessional": "$newRequestSendProfessionals._id",
    //       // "denyProfessional": "$newdenyProfessionals",
    //       "denyProfessional": {
    //         "$cond": {
    //           "if": {
    //             "$eq": [
    //               "$newRequestSendProfessionals._id",
    //               "$newdenyProfessionals._id",
    //             ],
    //           },
    //           "then": "$newdenyProfessionals._id",
    //           "else": null,
    //         },
    //       },
    //       "cancelProfessional": {
    //         "$cond": {
    //           "if": {
    //             "$eq": [
    //               "$newRequestSendProfessionals._id",
    //               "$newCancelProfessionals._id",
    //             ],
    //           },
    //           "then": "$newCancelProfessionals._id",
    //           "else": null,
    //         },
    //       },
    //       // "allProfessional": {
    //       //   "$cond": {
    //       //     "if": {
    //       //       "$eq": ["$newRequestSendProfessionals._id", "$professional"],
    //       //     },
    //       //     "then": {
    //       //       "$cond": {
    //       //         "if": {
    //       //           "$eq": ["$bookingStatus", constantUtil.ENDED],
    //       //         },
    //       //         "then": "$newRequestSendProfessionals._id",
    //       //         "else": "$professional",
    //       //       },
    //       //     },
    //       //     "else": "$professional",
    //       //   },
    //       // },
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "professionals",
    //       "localField": "allProfessional",
    //       "foreignField": "_id",
    //       "as": "allProfessional",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$allProfessional",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   matchPhoneNumberQuery,
    //   matchServiceAreaQuery
    // );

    // query.push({
    //   "$facet": {
    //     "totalCount": [
    //       {
    //         "$count": "count",
    //       },
    //     ],
    //     "responseData": [
    //       {
    //         "$project": {
    //           "_id": 0,
    //           "bookingDate": "$bookingDate",
    //           "bookingId": "$bookingId",
    //           "bookingStatus": "$bookingStatus",
    //           "professionalName": {
    //             "$concat": [
    //               "$allProfessional.firstName",
    //               " ",
    //               "$allProfessional.lastName",
    //             ],
    //           },
    //           "professionalNumber": {
    //             "$concat": [
    //               "$allProfessional.phone.code",
    //               " ",
    //               "$allProfessional.phone.number",
    //             ],
    //           },
    //           "professionalId": "$allProfessional._id",
    //           "professionalStatus": "$allProfessional.status",
    //           "blockedTill": "$allProfessional.blockedTill",
    //           "completedTrip": {
    //             "$cond": {
    //               "if": {
    //                 "$eq": ["$allProfessional._id", "$professional"],
    //                 "$eq": ["$bookingStatus", constantUtil.ENDED],
    //               },
    //               "then": 1,
    //               "else": 0,
    //             },
    //           },
    //           // "deniedTrip": {
    //           //   "$cond": {
    //           //     "if": {
    //           //       "$eq": ["$allProfessional._id", "$denyProfessional"],
    //           //     },
    //           //     "then": 1,
    //           //     "else": 0,
    //           //   },
    //           // },
    //           "cancelledTrip": {
    //             "$cond": {
    //               "if": {
    //                 "$eq": ["$allProfessional._id", "$cancelProfessional"],
    //               },
    //               "then": 1,
    //               "else": 0,
    //             },
    //           },
    //           "notAttemptTrip": {
    //             "$cond": {
    //               "if": {
    //                 "$or": [
    //                   // { "$eq": ["$allProfessional._id", "$denyProfessional"] },
    //                   {
    //                     "$eq": ["$allProfessional._id", "$cancelProfessional"],
    //                   },
    //                   {
    //                     "$eq": ["$allProfessional._id", "$professional"],
    //                     "$eq": ["$bookingStatus", constantUtil.ENDED],
    //                   },
    //                 ],
    //               },
    //               "then": 0,
    //               "else": 1,
    //             },
    //           },
    //         },
    //       },
    //       {
    //         "$group": {
    //           "_id": "$professionalId",
    //           // "bookingDate": { "$first": "$bookingDate" },
    //           // "bookingId": { "$first": "$bookingId" },
    //           "professionalName": { "$first": "$professionalName" },
    //           "professionalNumber": { "$first": "$professionalNumber" },
    //           "professionalStatus": { "$first": "$professionalStatus" },
    //           "blockedTill": { "$first": "$blockedTill" },
    //           "totalTrip": { "$sum": 1 },
    //           "completedTrip": { "$sum": "$completedTrip" },
    //           "completedRatio": {
    //             "$avg": { "$multiply": [{ "$sum": 1 }, "$completedTrip"] },
    //           },
    //           // "deniedTrip": { "$sum": "$deniedTrip" },
    //           // "deniedRatio": {
    //           //   "$avg": { "$multiply": [{ "$sum": 1 }, "$deniedTrip"] },
    //           // },
    //           "cancelledTrip": { "$sum": "$cancelledTrip" },
    //           "cancelledRation": {
    //             "$avg": { "$multiply": [{ "$sum": 1 }, "$cancelledTrip"] },
    //           },
    //           "notAttemptTrip": { "$sum": "$notAttemptTrip" },
    //           "notAttemptRatio": {
    //             "$avg": { "$multiply": [{ "$sum": 1 }, "$notAttemptTrip"] },
    //           },
    //         },
    //       },
    //       {
    //         "$sort":
    //           context.params.sortBy === "acceptanceRate"
    //             ? { "notAttemptRatio": -1 }
    //             : { "deniedRatio": -1 },
    //       },
    //       // { "$sort": { "deniedRatio": -1 } },
    //       { "$skip": context.params.skip },
    //       { "$limit": context.params.limit },
    //       // { "$sort": { "deniedRatio": -1 } },
    //     ],
    //   },
    // });
    // jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
    // //const totalCount = jsonData?.[0]?.responseData?.length;
    // // if (totalCount <= context.params.limit) {
    // //   jsonData[0]["totalCount"][0]["count"] = totalCount;
    // // }
    // const xewrqr = "43425";
    jsonData = await this.broker.emit("booking.getProfessionalRankingReport", {
      ...context.params,
    });
    jsonData = jsonData && jsonData[0];
    if (!jsonData) {
      throw new MoleculerError("SOMTHING WENT WORNG");
    }
    jsonData?.[0]?.responseData?.map((item) => {
      item.deviceInfo = null;
    });
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
    jsonData = [];
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};
bookingAction.getRideReviewRatingReport = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = [];
  // matchPhoneNumberQuery = {},
  // matchServiceAreaQuery = {};
  try {
    jsonData = await this.broker.emit("booking.getRideReviewRatingReport", {
      ...context.params,
    });
    jsonData = jsonData && jsonData[0];
    if (!jsonData) {
      throw new MoleculerError("SOMTHING WENT WORNG");
    }
    jsonData?.[0]?.responseData?.map((item) => {
      item.deviceInfo = null;
    });
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
    jsonData = [];
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};

bookingAction.downloadRideHistoryData = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    if (
      (context.params?.fromDate || "") !== "" &&
      (context.params?.toDate || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    // FOR EXPORT DATE FILTER
    query.push(
      { "$sort": { "bookingDate": -1 } },

      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      //       {
      //         "$lookup": {
      //           "from": "categories",
      //           "localField": "category",
      //           "foreignField": "_id",
      //           "as": "serviceArea",
      //         },
      //       },
      //       {
      //         "$unwind": {
      //           "path": "$serviceArea",
      //           "preserveNullAndEmptyArrays": true,
      //         },
      //       },
      {
        "$project": {
          "_id": "$_id",
          "bookingId": "$bookingId",
          "bookingType": "$bookingType",
          "bookingStatus": "$bookingStatus",
          "bookingDate": "$bookingDate",
          "bookingFor": "$bookingFor",
          "origin": "$origin.fullAddress",
          "destination": "$destination.fullAddress",
          "vehicle": "$vehicle",
          "serviceArea": "$serviceArea",
          "professional": {
            "_id": "$professional._id",
            "firstName": "$professional.firstName",
            "lastName": "$professional.lastName",
            "email": "$professional.email",
            "phone": "$professional.phone",
          },
          "professionalName": {
            "$concat": [
              "$professional.firstName",
              " ",
              "$professional.lastName",
            ],
          },
          "serviceCategory": "$serviceCategory",
          "acceptTime": "$regionalData.acceptTime",
          "pickUpTime": "$regionalData.pickUpTime",
          "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
          //             "rideStops": "$rideStops.fullAddress",
          //             "origin": "$origin.addressName",
          //             "destination": "$destination.addressName",
          "distanceUnit": "$distanceUnit",
          "payment": "$payment.option",
          "cancelledBy": "$activity.cancelledBy",
          "cancellationReason": "$cancelledProfessionalList.cancellationReason",
          "rating": "$bookingReview.professionalReview.rating",
        },
      },
      {
        "$group": {
          "_id": "$professional._id",
          "professionalDeatils": { "$first": "$professional" },
          "vehicle": { "$first": "$vehicle" },
          "totalBookings": { "$sum": 1 },
          "rideDeatils": {
            "$push": {
              "_id": "$_id",
              "bookingId": "$bookingId",
              "bookingType": "$bookingType",
              "bookingStatus": "$bookingStatus",
              "bookingDate": "$bookingDate",
              "bookingFor": "$bookingFor",
              "origin": "$origin",
              "destination": "$destination",
              "serviceCategory": "$serviceCategory",
              "serviceArea": "$serviceArea",
              "acceptTime": "$acceptTime",
              "pickUpTime": "$pickUpTime",
              "bookedEstimationPickUpTime": "$bookedEstimationPickUpTime",
              "distanceUnit": "$distanceUnit",
              "payment": "$payment",
              "cancelledBy": "$cancelledBy",
              "cancellationReason": "$cancellationReason",
              "rating": "$rating",
            },
          },
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};
bookingAction.getRidesReport = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const query = [];
    // FOR EXPORT DATE FILTER
    if (
      (context.params?.fromDate || "") !== "" &&
      (context.params?.toDate || "") !== ""
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": { "createdAt": { "$gte": fromDate, "$lt": toDate } },
      });
    }
    if (
      context.params.bookingBy &&
      context.params.bookingBy !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "bookingBy": context.params.bookingBy,
        },
      });
    }
    if (
      context.params.serviceCategorys &&
      context.params.serviceCategorys !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "serviceCategory": context.params.serviceCategorys,
        },
      });
    }
    if (
      context.params.vehicleCategory &&
      context.params.vehicleCategory !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "vehicleCategory": context.params.vehicleCategory,
        },
      });
    }
    if (
      context.params.bookingStatus &&
      context.params.bookingStatus !== constantUtil.ALL &&
      context.params.bookingStatus !== constantUtil.CANCELRIDE &&
      context.params.bookingStatus !== constantUtil.EXPIRED
    ) {
      query.push({
        "$match": {
          "bookingStatus": context.params.bookingStatus,
        },
      });
    } else if (context.params.bookingStatus === constantUtil.CANCELRIDE) {
      query.push({
        "$match": {
          "bookingStatus": {
            "$in": [
              constantUtil.PROFESSIONALCANCELLED,
              constantUtil.USERCANCELLED,
              constantUtil.CANCELLED,
            ],
          },
        },
      });
    } else if (context.params.bookingStatus === constantUtil.EXPIRED) {
      query.push({
        "$match": {
          "bookingStatus": {
            "$in": [
              constantUtil.EXPIRED,
              constantUtil.USERDENY,
              constantUtil.PROFESSIONALDENY,
            ],
          },
        },
      });
    }

    if (context.params.bookingId) {
      query.push({
        "$match": {
          "bookingId": {
            "$regex": context.params.bookingId?.trim(),
            "$options": "si",
          },
        },
      });
    }
    // if (context.params.distanceType && context.params.distanceType !== "ALL") {
    //   query.push({
    //     "$match": {
    //       "distanceUnit": context.params.distanceType,
    //     },
    //   });
    // }
    if (context.params.plateNumber) {
      query.push({
        "$match": {
          "vehicle.plateNumber": {
            "$regex": context.params.plateNumber?.trim(),
            "$options": "si",
          },
        },
      });
    }
    if (
      context.params.cancelledBy &&
      context.params.cancelledBy !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "activity.cancelledBy.userType": context.params.cancelledBy,
        },
      });
    }
    if (
      context.params.bookingType &&
      context.params.bookingType !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "bookingType": context.params.bookingType,
        },
      });
    }
    if (
      context.params.paymentType &&
      context.params.paymentType !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "payment.option": context.params.paymentType,
        },
      });
    }

    // if (context.params.limit) {
    //   query.push({ "$limit": context.params.limit });
    // }
    // if (context.params.skip) {
    //   query.push({ "$skip": context.params.skip });
    // }
    query.push(
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "users",
          "localField": "user",
          "foreignField": "_id",
          "as": "userDetails",
        },
      },
      {
        "$unwind": {
          "path": "$userDetails",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "categories",
          "localField": "category",
          "foreignField": "_id",
          "as": "serviceArea",
        },
      },
      {
        "$unwind": {
          "path": "$serviceArea",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "vehicle.vehicleCategoryId",
          "foreignField": "_id",
          "as": "vehicleCategory",
        },
      },
      {
        "$unwind": {
          "path": "$vehicleCategory",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );
    if (context.params.userNumber) {
      query.push({
        "$match": {
          "userDetails.phone.number": {
            "$regex": context.params.userNumber?.trim(),
            "$options": "si",
          },
        },
      });
    }
    if (context.params.professionalNumber) {
      query.push({
        "$match": {
          "professional.phone.number": {
            "$regex": context.params.professionalNumber?.trim(),
            "$options": "si",
          },
        },
      });
    }
    if (
      context.params.serviceArea &&
      context.params.serviceArea !== constantUtil.ALL
    ) {
      query.push({
        "$match": {
          "serviceArea._id": mongoose.Types.ObjectId(
            context.params.serviceArea
          ),
        },
      });
    }
    // FOR EXPORT DATE FILTER
    query.push({
      "$facet": {
        "totalCount": [
          {
            "$count": "count",
          },
        ],

        "responseData": [
          // context.params.limit && { "$limit": context.params.limit },
          // context.params.skip !== 0 && { "$skip": context.params.skip },
          {
            "$project": {
              "_id": "$_id",
              "professionalName": {
                "$concat": [
                  "$professional.firstName",
                  " ",
                  "$professional.lastName",
                ],
              },
              "professionalPhone": {
                "$concat": [
                  "$professional.phone.code",
                  " ",
                  "$professional.phone.number",
                ],
              },
              "userName": {
                "$concat": [
                  "$userDetails.firstName",
                  " ",
                  "$userDetails.lastName",
                ],
              },
              "userPhoneNumber": {
                "$concat": [
                  "$userDetails.phone.code",
                  " ",
                  "$userDetails.phone.number",
                ],
              },
              "serviceArea": "$serviceArea.locationName",
              "bookingBy": "$bookingBy",
              "bookingType": "$bookingType",
              "serviceCategory": "$serviceCategory",
              //             "vehicleCategory": "$vehicleCategory",
              "vehicleCategory": {
                "$cond": {
                  "if": "$vehicleCategory",
                  "then": "$vehicleCategory.data.vehicleCategory",
                  "else": null,
                },
              },
              "pickUpLocation": "$origin.fullAddress",
              "dropLocation": "$destination.fullAddress",
              "bookingStatus": "$bookingStatus",
              // "cancelTime": "$regionalData.cancelTime",
              // "acceptTime": "$regionalData.acceptTime",
              // "arriveTime": "$regionalData.arriveTime",
              // "pickUpTime": "$regionalData.pickUpTime",
              // "dropTime": "$regionalData.dropTime",
              "cancelTime": "$activity.cancelTime",
              "acceptTime": "$activity.acceptTime",
              "arriveTime": "$activity.arriveTime",
              "pickUpTime": "$activity.pickUpTime",
              "dropTime": "$activity.dropTime",
              "distanceUnit": "$distanceUnit",
              "bookingFrom": "$bookingFrom",
              "bookingId": "$bookingId",
              "bookingDate": "$bookingDate",
              "bookingFor": "$bookingFor",
              "vehicle": "$vehicle",
              "bookedEstimationPickUpTime": "$bookedEstimation.pickupTime",
              "payment": "$payment.option",
              "cancelledBy": "$activity.cancelledBy.userType",
              "cancellationReason": "$cancellationReason",
              "rating": "$bookingReview.professionalReview.rating",
              //             "userDetails": "$userDetails",

              //               "professional": {
              //               "_id": "$professional._id",
              //               "firstName": "$professional.firstName",
              //               "lastName": "$professional.lastName",
              //               "email": "$professional.email",
              //               "phone": "$professional.phone",
              //             },

              //             "passengerName": { "$cond": {"if":"user","then":{ "$concat": [ "$userDetails.firstName","$userDetails.lastName"]},"else":null}},
            },
          },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
          { "$sort": { "bookingDate": -1 } },
        ],
      },
    });
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMTHING WENT WORNG";
  }
  return {
    "errorCode": errorCode,
    "message": message,
    "errorMessage": errorMessage,
    "data": jsonData,
  };
};

bookingAction.getHubsRideList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  // let matchVehicleHubQuery = {};
  const query = [],
    match = {};

  // switch (context.params.userType) {
  //   case constantUtil.HUBS:
  //     // match["$and"].push({
  //     //   "professional": mongoose.Types.ObjectId(context.params.id),
  //     // });
  //     matchVehicleHubQuery = {
  //       "$match": {
  //         "professional.vehicles.hub": mongoose.Types.ObjectId(
  //           context.params.id
  //         ),
  //       },
  //     };
  //     break;

  //   default:
  //     // match["$and"] = [];
  //     // match["$and"].push({
  //     //   "professional": mongoose.Types.ObjectId(context.params.id),
  //     // });
  //     // query.push({
  //     //   "$match": match,
  //     // });
  //     matchVehicleHubQuery = {
  //       "$match": {
  //         // "professional": mongoose.Types.ObjectId(context.params.id),
  //       },
  //     };
  //     break;
  // }
  // // if (context.params.filter != 'undefined' && context.params.filter === true)
  // //   match['$and'].push({ 'bookingStatus': constantUtil.AWAITING })
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
      "$and": [
        {
          "professional.vehicles.hub": mongoose.Types.ObjectId(
            context.params.id
          ),
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$match": {
            "professional.vehicles.hub": mongoose.Types.ObjectId(
              context.params.id
            ),
          },
        },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "origin": "$origin",
            "destination": "$destination",
            "vehicle": "$vehicle",
            "hub": "$professional.vehicles.hub",
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};

bookingAction.expiredRetryBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const {
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    BOOKING_UPDATE_STATUS_FAILED,
    PROFESSIONAL_NEW_RIDE,
    PROFESSIONAL_NOTIFICATION,
    BOOKING_ALREADY,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );

  let bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  vehicleCategoryData =
    vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "id": bookingData.category._id.toString(),
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
  if (!serviceCategoryData) {
    throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
  }
  serviceCategoryData.categoryData.vehicles.map((vehicle) => {
    if (
      vehicle.categoryId.toString() ===
      bookingData.vehicle.vehicleCategoryId.toString()
    ) {
      vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
    }
  });
  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = bookingData.category.distanceType;
  const radius =
    parseInt(generalSettings.data.requestDistance) +
    parseInt(generalSettings.data.retryRequestDistance);
  if (bookingData.bookingStatus === constantUtil.EXPIRED) {
    let professionalData = await this.getProfessionalByLocation(
      //clientId,
      bookingData.origin.lat,
      bookingData.origin.lng,
      bookingData.destination.lat,
      bookingData.destination.lng,
      bookingData.vehicle.vehicleCategoryId.toString(),
      constantUtil.BOOKING, // actionType
      context.params.rideType,
      context.params?.radius || radius,
      // context.params?.maxRadius ||
      //   radius + parseInt(generalSettings.data.requestDistance), // maxRadius
      false,
      true,
      generalSettings.data.isTailRideNeeded,
      vehicleCategoryData.isSubCategoryAvailable,
      vehicleCategoryData.isForceAppliedToProfessional,
      bookingData.isGenderAvailable,
      bookingData.childseatAvailable,
      bookingData.handicapAvailable,
      !!(
        bookingData?.user?.gender === constantUtil.MALE &&
        serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
      ),
      false, //isRentalSupported Need to Change
      context.params?.serviceAreaId,
      bookingData.isShareRide,
      bookingData?.payment.option || constantUtil.CASH,
      bookingData?.languageCode || null,
      bookingData?.isPetAllowed || false
    );
    professionalData = professionalData && professionalData[0];
    if (professionalData.length === 0) {
      throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
    }
    //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    const userType = (context.params.userType || "").toUpperCase();
    let userData = null;
    if (userType === constantUtil.USER) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params?.userId?.toString(),
      });
      userData = userData && userData[0];
    }
    professionalData =
      await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
        professionalData,
        bookingData,
        userData
      );
    if (
      !professionalData ||
      (professionalData && professionalData.length === 0)
    ) {
      throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
    }
    //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
    const updateData = {
      "activity": {
        "noOfUsersInVehicle": null,
        "noOfSeatsAvailable": null,
        "isUserPickedUp": false,
        "isUserDropped": false,
        "waitingDuration": null,
        "actualBookingTime": new Date(),
        "bookingTime": new Date(),
        "denyOrExpireTime": null,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
        "rideStops": bookingData.activity.rideStops || [],
      },
      "rideStops": bookingData.activity.rideStops || [],
      "vehicle": {
        "vehicleCategoryId": bookingData.vehicle.vehicleCategoryId.toString(),
      },
      "bookingStatus": constantUtil.AWAITING,
    };
    const updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
      },
      updateData
    );
    if (!updatedBookingData.nModified) {
      throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
    }
    bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    // bookingData = bookingData.toJSON();

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    bookingData.retryTime = generalSettings.data.driverRequestTimeout;
    bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
    // bookingData.distanceUnit = bookingData.category.distanceType;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    const notificationObject = {
      "clientId": clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.PROFESSIONAL,
        "action": constantUtil.ACTION_BOOKINGREQUEST,
        "timestamp": Date.now(),
        "message": PROFESSIONAL_NEW_RIDE,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": professionalData.map((professional) => {
        if (
          // parseFloat(professional.wallet.availableAmount) <
          // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
          bookingData.payment.option === constantUtil.WALLET ||
          bookingData.payment.option === constantUtil.PAYMENTCARD ||
          // bookingData.payment.option !== constantUtil.CASH &&
          // bookingData.payment.option !== constantUtil.CREDIT) ||
          parseFloat(professional.wallet.availableAmount) >=
            parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
        ) {
          return {
            "id": professional?._id?.toString(),
            "token": professional?.deviceInfo[0]?.deviceId || "",
            "deviceType": professional?.deviceInfo[0]?.deviceType || "",
            "platform": professional?.deviceInfo[0]?.platform || "",
            "socketId": professional?.deviceInfo[0]?.socketId || "",
            "duration": professional?.duration,
            "distance": professional?.distance,
          };
        }
      }),
    };

    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);

    return {
      "code": 200,
      "data": bookingObject(bookingData),
      "message": PROFESSIONAL_NOTIFICATION,
    };
  }

  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_ALREADY + " " + bookingData.bookingStatus,
  };
};
bookingAction.manualMeterBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const {
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    VEHICLE_NOT_AVAILABLE,
    INVALID_BOOKING,
    BOOKING_ACCEPTED,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );

  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "id": context.params.categoryId,
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
  if (!serviceCategoryData) {
    throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
  }
  serviceCategoryData.categoryData.vehicles.map((vehicle) => {
    if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
      vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
    }
  });

  const [user] = await this.broker.emit("user.getUserDetails", {
    "code": context.params.bookingFor.phoneCode,
    "number": context.params.bookingFor.phoneNumber,
  });

  let professionalData = await this.broker.emit("professional.getById", {
    "id": context.meta.professionalId.toString(),
  });
  professionalData = professionalData && professionalData[0];
  // //#region Remove/Exclude Professional Deny, Professional BlockList & User BlockList
  // professionalData =
  //   await this.checkAndRemoveProfessionalAndUserDenyAndBlockList(
  //     professionalData,
  //     null, //bookingData
  //     user
  //   );
  // //#endregion Remove/Exclude Professional Deny, Professional BlockList & User BlockList
  // if (
  //   !professionalData ||
  //   (professionalData && professionalData.length === 0)
  // ) {
  //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
  // }
  if (!professionalData) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND, 500);
  }

  const vehicleDetails = {};
  professionalData.vehicles.forEach((vehicle) => {
    if (
      vehicle.status === constantUtil.ACTIVE &&
      vehicle.defaultVehicle === true
    ) {
      vehicleDetails.vehicleCategoryId = context.params.vehicleCategoryId;
      vehicleDetails.vinNumber = vehicle.vinNumber;
      vehicleDetails.type = vehicle.type;
      vehicleDetails.plateNumber = vehicle.plateNumber;
      vehicleDetails.model = vehicle.model;
      vehicleDetails.year = vehicle.year;
      vehicleDetails.color = vehicle.color;
      vehicleDetails.noOfDoors = vehicle.noOfDoors;
      vehicleDetails.noOfSeats = vehicle.noOfSeats;
    }
  });
  if (Object.keys(vehicleDetails).length === 0) {
    throw new MoleculerError(VEHICLE_NOT_AVAILABLE);
  }
  const newDate = helperUtil.toUTC(new Date().toISOString());
  const newRegionalDate = helperUtil.toRegionalUTC(new Date());
  //#region Vehicle Category Details Start
  context.params["vehicleCategoryData"] = vehicleCategoryData; // Vehicle Category Details
  context.params["vehicleCategoryData"]["siteCommission"] =
    vehicleCategoryData.siteCommissionManualMeter || 0; // For Professional Only (Manual Meter)
  //#endregion  Vehicle Category Details End
  if (!context.params.bookingBy) {
    context.params["bookingBy"] = constantUtil.PROFESSIONAL;
  }
  const rideCoordinatesEncoded = context.params.rideCoordinatesEncoded;
  //-----------------------
  let bookingData = await this.adapter.model.create({
    "clientId": clientId,
    "bookingId": `${generalSettings.data.bookingPrefix}-${customAlphabet(
      "1234567890",
      9
    )()}`,
    "bookingOTP": customAlphabet("1234567890", 4)().toString(),
    "bookingType": constantUtil.INSTANT,
    "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
    "bookingDate": newDate,
    "regionalData": {
      "bookingDate": newRegionalDate,
      "acceptTime": newRegionalDate,
      "arriveTime": newRegionalDate,
      "cancelTime": null,
      "pickUpTime": null,
      "dropTime": null,
    },
    "bookingFor": context.params.bookingFor,
    "bookingBy": context.params.bookingBy,
    "bookingFrom": constantUtil.CONST_APP,
    "tripType": constantUtil.DAILYTRIP,
    "paymentSection": generalSettings.data.paymentSection,
    "bookingStatus": constantUtil.ARRIVED,
    "admin": null,
    "user": user?._id ? mongoose.Types.ObjectId(user._id.toString()) : null,
    "professional": context.meta.professionalId,
    "guestType": constantUtil.PROFESSIONALGUEST,
    "category": context.params.categoryId,
    "vehicle": vehicleDetails,
    "origin": {
      "addressName": context.params.pickUpAddressName,
      "fullAddress": context.params.pickUpFullAddress,
      "shortAddress": context.params.pickUpShortAddress,
      "lat": context.params.pickUpLat,
      "lng": context.params.pickUpLng,
    },
    "destination": {
      "addressName": context.params.dropAddressName,
      "fullAddress": context.params.dropFullAddress,
      "shortAddress": context.params.dropShortAddress,
      "lat": context.params.dropLat,
      "lng": context.params.dropLng,
    },
    "estimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "bookedEstimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "waitingTime": {
      "waitingStartTime": null,
      "waitingEndTime": null,
    },
    "currencySymbol": serviceCategoryData.currencySymbol,
    "currencyCode": serviceCategoryData.currencyCode,
    "distanceUnit": serviceCategoryData.distanceType,
    "invoice": mappingUtil.calculateInvoiceObject(context.params),
    "serviceCategory": context.params.serviceCategory,
    "activity": {
      "noOfUsersInVehicle": null,
      "noOfSeatsAvailable": null,
      "isUserPickedUp": true,
      "isUserDropped": false,
      "waitingDuration": null,
      "actualBookingTime": new Date(),
      "bookingTime": new Date(),
      "denyOrExpireTime": null,
      "cancelTime": null,
      "acceptTime": new Date(),
      "arriveTime": new Date(),
      "pickUpTime": null,
      "dropTime": null,
      "rideStops": context.params.bookingLocations || [],
      "acceptLatLng": context.params.latLng,
      "arriveLatLng": context.params.latLng,
      "pickUpLatLng": context.params.latLng,
    },
    "requestSendProfessionals": {
      "professionalId": professionalData?._id?.toString(),
      "requestReceiveDateTime": new Date().toISOString(),
      "count": 1,
      "location.coordinates": professionalData?.location?.coordinates,
      "duration": 0,
      "distance": 0,
    },
    "rideStops": context.params.bookingLocations || [],
    "notes": context.params.notes || null,
    "payment": {
      "option": constantUtil.PAYMENTCASH,
      "card": {},
      "paid": true,
    },
    "isCancelToNearbyProfessionals": false,
    "isRestrictMaleBookingToFemaleProfessional":
      !!serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional,
    "isGenderAvailable": false,
    "childseatAvailable": false,
    "handicapAvailable": false,
    // Share Ride Start
    "isShareRide": context.params.isShareRide || false,
    "isPetAllowed": context.params.isPetAllowed || false,
    "isEnableLuggage": context.params?.isEnableLuggage || false,
    "totalPassengerCount": vehicleCategoryData?.seatCount || 0,
    "currentPassengerCount": context.params.passengerCount || 0,
    "passengerCount": context.params.passengerCount || 0,
    "parentShareRideId": null,
    "parentShareRideProfessional": context.meta.professionalId,
    // Share Ride End
    "pickupLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "dropLocation.coordinates": [
      [context.params.dropLng, context.params.dropLat],
    ],
    "upcomingLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "rideCoordinatesEncoded": rideCoordinatesEncoded,
  });

  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  bookingData = await this.getBookingDetailsById({
    "bookingId": bookingData._id.toString(),
  });
  switch (bookingData.serviceCategory?.toLowerCase()) {
    // case constantUtil.CONST_PACKAGES.toLowerCase():
    //   this.broker.emit("booking.updatePackageDetailsById", {
    //     "action": constantUtil.ADD,
    //     "uploadedBy": constantUtil.USER,
    //     "packageData": context.params.packageData || {},
    //     "bookingId": bookingData?._id?.toString(),
    //   });
    //   break;

    case constantUtil.CONST_SHARERIDE.toLowerCase():
      {
        const sharrideSearchRadius =
          generalSettings.data.shareRideMaxThershold / 1000 / 6371; // 6371 --> Default Earth Radious, 1000 --> KM Value ( Custom Meter / KM / Radious)
        //update Location
        if (
          // bookingData.location === null ||
          bookingData?.rideLocation?.coordinates?.length <= 1
        ) {
          await this.adapter.model.updateOne(
            {
              "_id": mongoose.Types.ObjectId(bookingData?._id),
            },
            {
              "rideLocation.coordinates": context.params?.rideLocation,
              "totalPassengerCount":
                bookingData?.professional?.vehicles[0]?.noOfSeats ||
                vehicleCategoryData?.seatCount ||
                0,
            }
          );
        }
        await this.broker.emit("professional.updateProfessionalShareRide", {
          "actionStatus": constantUtil.STATUS_UPCOMING,
          "bookingId": bookingData?._id?.toString(),
          "professionalId": context.meta.professionalId,
          // "pickUpLat": context.params.pickUpLat,
          // "pickUpLng": context.params.pickUpLng,
          // "dropLat": context.params.dropLat,
          // "dropLng": context.params.dropLng,
        });
        await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
          "actionStatus": constantUtil.ACCEPTED,
          "bookingId": bookingData?._id?.toString(), // If no Shortest path ride  then currnent accepted booking is Default
          "professionalId": context.meta.professionalId,
          "serviceCategory": bookingData.serviceCategory,
          "lastRidePassengerCount": 0,
          "lat": context.params?.latLng?.lat,
          "lng": context.params?.latLng?.lng,
          "parentShareRideId": bookingData?.parentShareRideId,
          // "passengerCount": 0,
        });
        // // check pickup
        // // const ridePickupData = await this.adapter.model.findOne({
        // //   "parentShareRideId": null,
        // //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //   "bookingStatus": { "$ne": constantUtil.ENDED },
        // //   "location": {
        // //     "$geoIntersects": {
        // //       "$geometry": {
        // //         "type": "Point",
        // //         "coordinates": [
        // //           context.params.pickUpLng,
        // //           context.params.pickUpLat,
        // //         ],
        // //       },
        // //     },
        // //     "$minDistance": 0,
        // //     "$maxDistance": 500, //need to change dynamic
        // //   },
        // // });
        // //------------ Test -------------
        // // const checkRidePickupData = await this.adapter.model
        // //   .findOne({
        // //     "_id": {
        // //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        // //     },
        // //     "parentShareRideId": null,
        // //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //     "bookingStatus": {
        // //       // "$ne": constantUtil.ENDED,
        // //       "$in": [
        // //         // constantUtil.AWAITING,
        // //         constantUtil.ACCEPTED,
        // //         constantUtil.ARRIVED,
        // //       ],
        // //     },
        // //     "rideLocation": {
        // //       "$geoIntersects": {
        // //         "$geometry": {
        // //           // "type": "Polygon",
        // //           "type": "MultiPoint",
        // //           "coordinates": [
        // //             // [
        // //             [context.params.pickUpLng, context.params.pickUpLat],
        // //             [context.params.dropLng, context.params.dropLat],
        // //             [context.params.dropLng, context.params.dropLat],
        // //             [context.params.pickUpLng, context.params.pickUpLat],
        // //             // ],
        // //           ],
        // //         },
        // //       },
        // //     },
        // //   })
        // //   .lean();
        // // const checkBoxRidePickupData = await this.adapter.model
        // //   .findOne({
        // //     "_id": {
        // //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        // //     },
        // //     "parentShareRideId": null,
        // //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //     "bookingStatus": {
        // //       // "$ne": constantUtil.ENDED,
        // //       "$in": [
        // //         // constantUtil.AWAITING,
        // //         constantUtil.ACCEPTED,
        // //         constantUtil.ARRIVED,
        // //       ],
        // //     },
        // //     "rideLocation.coordinates": {
        // //       "$geoWithin": {
        // //         "$box": [
        // //           [context.params.pickUpLng, context.params.pickUpLat],
        // //           [context.params.dropLng, context.params.dropLat],
        // //         ],
        // //       },
        // //     },
        // //   })
        // //   .lean();

        // // const checkCenterRidePickupData = await this.adapter.model
        // //   .findOne({
        // //     "_id": {
        // //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        // //     },
        // //     "parentShareRideId": null,
        // //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //     "bookingStatus": {
        // //       // "$ne": constantUtil.ENDED,
        // //       "$in": [
        // //         // constantUtil.AWAITING,
        // //         constantUtil.ACCEPTED,
        // //         constantUtil.ARRIVED,
        // //       ],
        // //     },
        // //     "rideLocation.coordinates": {
        // //       "$geoWithin": {
        // //         "$centerSphere": [
        // //           [context.params.pickUpLng, context.params.pickUpLat],
        // //           // [context.params.dropLng, context.params.dropLat],
        // //           searchRadius,
        // //         ],
        // //       },
        // //     },
        // //   })
        // //   .lean();
        // //----------- ridePickupData Old ---------
        // // const ridePickupData = await this.adapter.model
        // //   .findOne({
        // //     "_id": {
        // //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        // //     },
        // //     "parentShareRideId": null,
        // //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //     "bookingStatus": {
        // //       // "$ne": constantUtil.ENDED,
        // //       "$in": [
        // //         // constantUtil.AWAITING,
        // //         constantUtil.ACCEPTED,
        // //         constantUtil.ARRIVED,
        // //       ],
        // //     },
        // //     "rideLocation": {
        // //       // "$nearSphere": {
        // //       "$near": {
        // //         // "$geoIntersects": {
        // //         "$geometry": {
        // //           "type": "Point",
        // //           "coordinates": [
        // //             context.params.pickUpLng,
        // //             context.params.pickUpLat,
        // //           ],
        // //         },
        // //         "$minDistance": 0,
        // //         "$maxDistance": 2000, //need to change dynamic
        // //       },
        // //     },
        // //   })
        // //   .lean();
        // //----------- ridePickupData New ---------
        // const ridePickupData = await this.adapter.model
        //   .findOne({
        //     "_id": {
        //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        //     },
        //     "parentShareRideId": null,
        //     "isShareRide": true,
        //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        //     "bookingStatus": {
        //       // "$ne": constantUtil.ENDED,
        //       "$in": [
        //         // constantUtil.AWAITING,
        //         constantUtil.ACCEPTED,
        //         constantUtil.ARRIVED,
        //         constantUtil.STARTED,
        //       ],
        //     },
        //     "rideLocation.coordinates": {
        //       "$geoWithin": {
        //         "$centerSphere": [
        //           [context.params.pickUpLng, context.params.pickUpLat],
        //           sharrideSearchRadius,
        //         ],
        //       },
        //     },
        //     // "rideLocation": {
        //     //   // "$nearSphere": {
        //     //   "$near": {
        //     //     // "$geoIntersects": {
        //     //     "$geometry": {
        //     //       "type": "Point",
        //     //       "coordinates": [
        //     //         context.params.pickUpLng,
        //     //         context.params.pickUpLat,
        //     //       ],
        //     //     },
        //     //     "$minDistance": 0,
        //     //     "$maxDistance": generalSettings.data.shareRideMaxThershold, //need to change dynamic
        //     //   },
        //     // },
        //   })
        //   .lean();
        // // if (
        // //   ridePickupData ||
        // //   checkCenterRidePickupData ||
        // //   checkBoxRidePickupData
        // // )
        // //   throw new MoleculerError("Success", 500);
        // // else throw new MoleculerError("Success", 500);
        // // // check drop
        // // // const rideDropData = await this.adapter.model.findOne({
        // // //   "parentShareRideId": null,
        // // //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // // //   "bookingStatus": { "$ne": constantUtil.ENDED },
        // // //   "location": {
        // // //     "$geoIntersects": {
        // // //       "$geometry": {
        // // //         "type": "Point",
        // // //         "coordinates": [
        // // //           context.params.dropLng,
        // // //           context.params.dropLat,
        // // //         ],
        // // //       },
        // // //     },
        // // //     "$minDistance": 0,
        // // //     "$maxDistance": 500, //need to change dynamic
        // // //   },
        // // // });
        // //----------- rideDropData Old ---------
        // // const rideDropData = await this.adapter.model
        // //   .findOne({
        // //     "_id": {
        // //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        // //     },
        // //     "parentShareRideId": null,
        // //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //     "bookingStatus": {
        // //       // "$ne": constantUtil.ENDED,
        // //       "$in": [
        // //         // constantUtil.AWAITING,
        // //         constantUtil.ACCEPTED,
        // //         constantUtil.ARRIVED,
        // //       ],
        // //     },
        // //     "rideLocation": {
        // //       // "$nearSphere": {
        // //       "$near": {
        // //         // "$geoIntersects": {
        // //         "$geometry": {
        // //           "type": "Point",
        // //           "coordinates": [
        // //             context.params.dropLng,
        // //             context.params.dropLat,
        // //           ],
        // //         },
        // //         "$minDistance": 0,
        // //         "$maxDistance": 2000, //need to change dynamic
        // //       },
        // //     },
        // //   })
        // //   .lean();
        // //----------- rideDropData New ---------
        // const rideDropData = await this.adapter.model
        //   .findOne({
        //     "_id": {
        //       "$nin": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        //     },
        //     "parentShareRideId": null,
        //     "isShareRide": true,
        //     "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        //     "bookingStatus": {
        //       // "$ne": constantUtil.ENDED,
        //       "$in": [
        //         // constantUtil.AWAITING,
        //         constantUtil.ACCEPTED,
        //         constantUtil.ARRIVED,
        //         constantUtil.STARTED,
        //       ],
        //     },
        //     "rideLocation.coordinates": {
        //       "$geoWithin": {
        //         "$centerSphere": [
        //           [context.params.dropLng, context.params.dropLat],
        //           sharrideSearchRadius,
        //         ],
        //       },
        //     },
        //   })
        //   .lean();
        // // if (ridePickupData || rideDropData)
        // //   throw new MoleculerError("Success", 500);
        // // else throw new MoleculerError("Success", 500);
        // //---------- Sample code ------------
        // // await this.adapter.model.findOne({
        // //   "parentShareRideId": null,
        // //   "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
        // //   "bookingStatus": { "$ne": constantUtil.ENDED },
        // //   "location": {
        // //     // "$nearSphere": {
        // //     "$near": {
        // //       "$geometry": {
        // //         "type": "Point",
        // //         //"coordinates": [77.3266,8.2506],
        // //         "coordinates": [77.3124073, 8.248572],
        // //       },
        // //       "$minDistance": 0,
        // //       "$maxDistance": 2400, //need to change dynamic
        // //     },
        // //   },
        // // });
        // //-------
        // // -------------------------
        // const currentPassengerCount =
        //   parseInt(ridePickupData?.currentPassengerCount || 0) +
        //   parseInt(context.params?.passengerCount || 0);
        // if (
        //   ridePickupData &&
        //   rideDropData &&
        //   ridePickupData?._id?.toString() !== bookingData?._id?.toString() &&
        //   rideDropData?._id?.toString() !== bookingData?._id?.toString() &&
        //   currentPassengerCount <= parseInt(ridePickupData.totalPassengerCount)
        // ) {
        //   // //update currentPassengerCount in Parent Share Ride
        //   // await this.adapter.model.updateOne(
        //   //   {
        //   //     "_id": mongoose.Types.ObjectId(ridePickupData?._id),
        //   //     "parentShareRideId": null,
        //   //   },
        //   //   {
        //   //     "$inc": {
        //   //       "currentPassengerCount": parseInt(
        //   //         context.params?.passengerCount || 0
        //   //       ),
        //   //     },
        //   //   }
        //   // );
        //   //update Child Share Ride
        //   await this.adapter.model.updateOne(
        //     {
        //       "_id": mongoose.Types.ObjectId(bookingData?._id?.toString()),
        //     },
        //     {
        //       "parentShareRideId": ridePickupData?._id?.toString(),
        //       "parentShareRideProfessional":
        //         ridePickupData?.professional?.toString(),
        //       // "rideLocation.coordinates": [
        //       //   [context.params.pickUpLng, context.params.pickUpLat],
        //       //   [context.params.dropLng, context.params.dropLat],
        //       // ],
        //     }
        //   );
        //   // }
        // }
      }
      break;
  }
  // bookingData = bookingData.toJSON();
  // -------- Update Ongoing Booking Start ---------
  await this.broker.emit("professional.updateOnGoingBooking", {
    "professionalId": context.meta.professionalId,
    "bookingId": bookingData._id.toString(),
  });
  if (user?._id) {
    if (bookingData.user.bookingInfo.pendingReview) {
      await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(
            bookingData.user.bookingInfo.pendingReview?.toString()
          ),
          "bookingStatus": constantUtil.ENDED,
        },
        {
          "$set": {
            "bookingReview.userReview.rating": 5,
            "bookingReview.isUserReviewed": true,
          },
        }
      );
      await this.broker.emit("user.updateRating", {
        "userId": bookingData.user._id.toString(),
        "rating": 5,
      });
    }
    await this.broker.emit("user.updateOnGoingBooking", {
      "userId": user?._id.toString(),
      "bookingId": bookingData._id.toString(),
      "bookingType": bookingData.bookingType,
    });
  }
  // -------- Update Ongoing Booking End ---------
  // bookingData = await this.adapter.model
  //   .findById(mongoose.Types.ObjectId(bookingData._id.toString()))
  //   .populate("admin", { "data.accessToken": 0, "data.password": 0 })
  //   .populate("user", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("professional", {
  //     "password": 0,
  //     "bankDetails": 0,
  //     "cards": 0,
  //     "trustedContacts": 0,
  //   })
  //   .populate("category")
  //   .populate("security")
  //   .populate("officer", { "password": 0, "preferences": 0 })
  //   .lean();

  bookingData = await this.getBookingDetailsById({
    "bookingId": bookingData._id.toString(),
  });

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  bookingData.vehicle.isShowProfessionalList =
    vehicleCategoryData.isShowProfessionalList;

  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = serviceCategoryData.distanceType;
  bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
  bookingData["category"]["isNeedSecurityImageUpload"] =
    serviceCategoryData?.isNeedSecurityImageUpload || false;
  // Update Ongoing Ride for User
  if (bookingData?.user?._id) {
    const notificationObject = {
      "clientId": clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.USER,
        "action": constantUtil.ACTION_ACCEPTBOOKING,
        "timestamp": Date.now(),
        "message": BOOKING_ACCEPTED,
        "details": lzStringEncode(bookingObject(bookingData)),
      },
      "registrationTokens": [
        {
          "token": bookingData.user?.deviceInfo[0]?.deviceId,
          "id": bookingData?.user?._id?.toString(),
          "deviceType": bookingData.user?.deviceInfo[0]?.deviceType,
          "platform": bookingData.user?.deviceInfo[0]?.platform,
          "socketId": bookingData.user?.deviceInfo[0]?.socketId,
        },
      ],
    };

    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    // this.broker.emit('socket.statusChangeEvent', notificationObject)

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_ACCEPTED,
  };
};

bookingAction.updateTripWayData = async function (context) {
  const clientId = context.params.clientId;
  const { UPDATED, BOOKING_UPDATE_STATUS_FAILED, PROFESSIONAL_NEW_RIDE } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.professionalDetails?.languageCode
    );
  let updatedWayData = {};
  if (context.params.bookingStatus === constantUtil.ARRIVED) {
    updatedWayData = { "pickupWayData": context.params.pickupWayData };
  } else {
    updatedWayData = {
      "wayData": context.params.wayData,
      "manualWayData": context.params.manualWayData,
      "snapWayData": context.params.snapWayData,
      "manualDistance": context.params.manualDistance,
    };
  }
  const updatedBookingData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.bookingId),
      "professional": mongoose.Types.ObjectId(context.meta.professionalId),
      "bookingStatus": { "$in": [constantUtil.ARRIVED, constantUtil.STARTED] },
    },
    { "$set": { ...updatedWayData } }
  );
  if (!updatedBookingData.nModified) {
    throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
  } else {
    //------------- Start ---------------
    if (context.params.bookingStatus === constantUtil.ARRIVED) {
      const responseData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      });
      const locationData = {
        // "location": {
        //   "lng": responseData?.professional.location.coordinates[0],
        //   "lat": responseData?.professional.location.coordinates[1],
        // },
        "rideId": responseData._id?.toString(),
        "lat": responseData?.professional?.location?.coordinates?.[1],
        "lng": responseData?.professional?.location?.coordinates?.[0],
        "currentBearing": responseData?.professional?.currentBearing || null,
        "altitude": responseData?.professional?.altitude || null,
        "speed": responseData?.professional?.speed || null,
        "horizontalAccuracy":
          responseData?.professional?.horizontalAccuracy || null,
        "verticalAccuracy":
          responseData?.professional?.verticalAccuracy || null,
        "isLocationUpdated":
          responseData?.professional?.isLocationUpdated || null,
        "locationUpdatedTime":
          responseData?.professional?.locationUpdatedTime || null,
        "professionalName":
          (responseData?.professional?.firstName || "") +
          " " +
          (responseData?.professional?.lastName || ""),
        "professionalPhoneNo":
          (responseData?.professional?.phone?.code || "") +
          " " +
          (responseData?.professional?.phone?.number || ""),
        "rideCoordinatesEncoded": responseData?.rideCoordinatesEncoded || "",
        "pickupWayData": responseData?.pickupWayData || [],
        "manualWayData": responseData?.manualWayData || [],
      };
      //
      const adminNotificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": constantUtil.ACTION_BOOKINGREQUEST,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_NEW_RIDE,
          "details": locationData,
        },
        "registrationTokens": [{ "userType": constantUtil.ADMIN }],
      };
      this.broker.emit(
        "socket.sendAdminNotificationForRideWayData",
        adminNotificationObject
      );
    }
    //------------- End ---------------
    return {
      "code": 200,
      "data": {},
      "message": UPDATED,
    };
  }
};

bookingAction.coorperateBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  let responseData,
    errorCode = 200,
    responseMessage = "",
    languageCode;
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  try {
    // const bookingBy = context.params.bookingBy || constantUtil.COORPERATEOFFICE;

    //------------------ Notify Message Start -----------------
    const bookingBy = (context.params?.bookingBy ?? "").toUpperCase();
    const bookingFrom = (
      context.params?.bookingFrom || constantUtil.ADMIN
    ).toUpperCase();
    if (
      bookingBy === constantUtil.USER ||
      bookingBy === constantUtil.CORPORATEOFFICER //Officer / User
    ) {
      languageCode =
        context.params?.langCode || context.meta?.userDetails?.languageCode;
    } else if (
      bookingBy === constantUtil.COORPERATEOFFICE //Office  / Admin
    ) {
      languageCode =
        context.params?.langCode || context.meta?.adminDetails?.languageCode;
    }

    const {
      USER_NOT_FOUND,
      USER_WALLET_PAYMENT_INFO,
      VEHICLE_CATEGORY_NOT_FOUND,
      SERVICE_CATEGORY_NOT_FOUND,
      PROFESSIONAL_NOT_FOUND,
      INVALID_BOOKING,
      BOOKING_SCHEDULED_RIDE,
      PROFESSIONAL_NOTIFICATION,
      USER_HAVE_RIDE,
      PROFESSIONAL_NEW_RIDE,
      BOOKING_PAID,
    } = notifyMessage.setNotifyLanguage(languageCode);
    //------------------ Notify Message End -----------------

    // const notifyMsgNew = notifyMessage.getNotifyMessage(languageCode, "CREATED");
    switch (context.params.type.toUpperCase()) {
      case constantUtil.INSTANT: {
        context.params.bookingType = constantUtil.INSTANT;
        break;
      }
      case constantUtil.SCHEDULE: {
        context.params.bookingType = constantUtil.SCHEDULE;
        break;
      }
      default: {
        break;
      }
    }
    if (context.params.bookingType === constantUtil.INSTANT) {
      const userCheckData = await this.adapter.model
        .findOne({
          "bookingFor.phoneNumber": context.params.bookingFor.phoneNumber,
          "bookingStatus": {
            "$nin": [
              constantUtil.ENDED,
              constantUtil.EXPIRED,
              constantUtil.USERDENY,
              constantUtil.CANCELLED,
              constantUtil.USERCANCELLED,
              constantUtil.PROFESSIONALCANCELLED,
            ],
          },
          "bookingType": constantUtil.INSTANT,
        })
        .lean();
      if (userCheckData) {
        throw new MoleculerError(USER_HAVE_RIDE);
      }
    }
    let userData;
    if (context.params.userId) {
      userData = await this.broker.emit("user.getById", {
        "id": context.params.userId.toString(),
      });
      userData = userData && userData[0];
      if (!userData) {
        throw new MoleculerError(USER_NOT_FOUND);
      }
      if (context.params.paymentOption === constantUtil.PAYMENTWALLET) {
        /* @TODO CARD PAYMENT PENDING*/
        if (
          parseFloat(userData.wallet.availableAmount) <
          parseFloat(context.params.estimationAmount)
        ) {
          throw new MoleculerError(USER_WALLET_PAYMENT_INFO);
        }
      }
    }
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    let serviceCategoryData = await this.broker.emit(
      "category.getCategoryById",
      {
        "id": context.params.categoryId,
      }
    );
    serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
    if (!serviceCategoryData) {
      throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
    }
    serviceCategoryData.categoryData.vehicles.map((vehicle) => {
      if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
        vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
      }
    });
    const radius =
      parseInt(generalSettings.data.requestDistance) +
      parseInt(generalSettings.data.retryRequestDistance);
    let professionalRetryData = [];
    if (context.params.bookingType === constantUtil.INSTANT) {
      professionalRetryData = await this.getProfessionalByLocation(
        //clientId,
        context.params.pickUpLat,
        context.params.pickUpLng,
        context.params.dropLat,
        context.params.dropLng,
        context.params.vehicleCategoryId,
        constantUtil.BOOKING, // actionType
        context.params.rideType,
        parseInt(generalSettings.data.requestDistance), // radius,
        // context.params?.maxRadius ||
        //   radius + parseInt(generalSettings.data.requestDistance), // maxRadius
        true,
        true,
        false,
        vehicleCategoryData.isSubCategoryAvailable,
        vehicleCategoryData.isForceAppliedToProfessional,
        context.params.isGenderAvailable || false,
        context.params.childseatAvailable || false,
        context.params.handicapAvailable || false,
        !!(
          userData?.gender === constantUtil.MALE &&
          serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
        ),
        false, //isRentalSupported Need to Change
        context.params?.serviceAreaId,
        context.params?.isShareRide || false,
        context.params?.paymentOption || null,
        context.params?.languageCode || null,
        context.params?.isPetAllowed || false
      );
      professionalRetryData = professionalRetryData && professionalRetryData[0];
      if (!professionalRetryData?.length) {
        professionalRetryData = await this.getProfessionalByLocation(
          //clientId,
          context.params.pickUpLat,
          context.params.pickUpLng,
          context.params.dropLat,
          context.params.dropLng,
          context.params.vehicleCategoryId,
          constantUtil.BOOKING, // actionType
          context.params.rideType,
          context.params?.radius || radius,
          // context.params?.maxRadius ||
          //   radius + parseInt(generalSettings.data.requestDistance), // maxRadius
          false,
          true,
          generalSettings.data.isTailRideNeeded,
          vehicleCategoryData.isSubCategoryAvailable,
          vehicleCategoryData.isForceAppliedToProfessional,
          context.params.isGenderAvailable || false,
          context.params.childseatAvailable || false,
          context.params.handicapAvailable || false,
          !!(
            userData?.gender === constantUtil.MALE &&
            serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional
          ),
          false, //isRentalSupported Need to Change
          context.params?.serviceAreaId,
          context.params?.isShareRide || false,
          context.params?.paymentOption || null,
          context.params?.languageCode || null,
          context.params?.isPetAllowed || false
        );
        professionalRetryData =
          professionalRetryData && professionalRetryData[0];
      }

      // if (!professionalRetryData)
      //   throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
      // if (!professionalRetryData?.length) {
      //   return {
      //     "code": 503,
      //     "data": {},
      //     "message": PROFESSIONAL_NOT_FOUND,
      //   };
      // }
    }

    let newDate = helperUtil.toUTC(new Date().toISOString());
    let newRegionalDate = helperUtil.toRegionalUTC(new Date());
    //  vehicle Category Data Added to params
    context.params["vehicleCategoryData"] = vehicleCategoryData;

    if (context.params.bookingType === constantUtil.SCHEDULE) {
      newDate = helperUtil.toUTC(context.params.bookingDate);
      newRegionalDate = helperUtil.toRegionalUTC(context.params.bookingDate);
    }
    //#region algorithm part Start
    const requestSendProfessionals = [];
    if (
      professionalRetryData.length > 0 &&
      context.params.bookingType === constantUtil.INSTANT &&
      (generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
        generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST)
    ) {
      requestSendProfessionals.push({
        "professionalId": professionalRetryData[0]?._id?.toString(),
        "requestReceiveDateTime": new Date().toISOString(),
        "count": 1,
        "location.coordinates": professionalRetryData[0]?.location?.coordinates,
        "duration": professionalRetryData[0]?.duration,
        "distance": professionalRetryData[0]?.distance,
      });
      professionalRetryData = [professionalRetryData[0]];
    }
    //#endregion algorithm part End
    //--------------------------------------------------------------------
    // let encodedRideCoordinates = null;
    // if (context.params.rawRideCoordinates) {
    //   encodedRideCoordinates = cryptoAESEncode(
    //     context.params.rawRideCoordinates
    //   ); // From Admin Booking
    //   encodedRideCoordinates = encodedRideCoordinates?.toString();
    // }
    const rideCoordinatesEncoded =
      context.params.rideCoordinatesEncoded ||
      context.params.rawRideCoordinates;
    //--------------------------------
    let bookingData = await this.adapter.model.create({
      "clientId": clientId,
      "bookingId": `${generalSettings.data.bookingPrefix}-${customAlphabet(
        "1234567890",
        9
      )()}`,
      "bookingOTP": customAlphabet("1234567890", 4)().toString(),
      "bookingType": context.params.bookingType,
      "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
      "bookingDate": newDate,
      "regionalData": {
        "bookingDate": newRegionalDate,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
      },
      "bookingFor": context.params.bookingFor,
      "bookingBy": bookingBy,
      "bookingFrom": bookingFrom,
      "tripType": constantUtil.DAILYTRIP,
      "paymentSection": generalSettings.data.paymentSection,
      "admin": context.meta.adminId || null,
      "coorperate": context.params.corperateId || null,
      // "coorperate": context.params.corperateId
      //   ? context.params.corperateId
      //   : context.meta.adminId,
      "user": context.params.userId || null,
      "couponId": null,
      "guestType": context.params.userId
        ? constantUtil.USER
        : constantUtil.GUESTUSER,
      "category": context.params.categoryId,
      "isGenderAvailable": context.params.isGenderAvailable || false,
      "childseatAvailable": context.params.childseatAvailable || false,
      "handicapAvailable": context.params.handicapAvailable || false,
      "isShareRide": false,
      "isPetAllowed": context.params.isPetAllowed || false,
      "isEnableLuggage": context.params?.isEnableLuggage || false,
      "vehicle": {
        "vehicleCategoryId": context.params.vehicleCategoryId,
      },
      "origin": {
        "addressName": context.params.pickUpAddressName,
        "fullAddress": context.params.pickUpFullAddress,
        "shortAddress": context.params.pickUpShortAddress,
        "lat": context.params.pickUpLat,
        "lng": context.params.pickUpLng,
      },
      "destination": {
        "addressName": context.params.dropAddressName,
        "fullAddress": context.params.dropFullAddress,
        "shortAddress": context.params.dropShortAddress,
        "lat": context.params.dropLat,
        "lng": context.params.dropLng,
      },
      "estimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "bookedEstimation": {
        "distance": context.params.estimationDistance,
        "time": context.params.estimationTime,
        "pickupDistance": context.params.estimationPickupDistance,
        "pickupTime": context.params.estimationPickupTime,
        "dropDistance": context.params.estimationDropDistance,
        "dropTime": context.params.estimationDropTime,
      },
      "waitingTime": {
        "waitingStartTime": null,
        "waitingEndTime": null,
      },
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceUnit": serviceCategoryData.distanceType,
      "invoice": mappingUtil.calculateInvoiceObject(context.params),
      "serviceCategory": context.params.serviceCategory,
      "activity": {
        "noOfUsersInVehicle": null,
        "noOfSeatsAvailable": null,
        "isUserPickedUp": false,
        "isUserDropped": false,
        "waitingDuration": null,
        "actualBookingTime": new Date(),
        "bookingTime": new Date(),
        "denyOrExpireTime": null,
        "cancelTime": null,
        "acceptTime": null,
        "arriveTime": null,
        "pickUpTime": null,
        "dropTime": null,
        "rideStops": context.params.bookingLocations || [],
      },
      "rideStops": context.params.bookingLocations || [],
      "notes": context.params.notes?.toString("utf8") || null,
      "payment": {
        "option": constantUtil.PAYMENTCREDIT,
        "card": {},
        "paid": true,
      },
      // ---  for alogorithim Start --------
      "requestSendProfessionals": requestSendProfessionals,
      "requestRetryCount": 1,
      // ---  for alogorithim End --------
      "isRestrictMaleBookingToFemaleProfessional":
        serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional || false,
      "pickupLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "dropLocation.coordinates": [
        [context.params.dropLng, context.params.dropLat],
      ],
      "upcomingLocation.coordinates": [
        [context.params.pickUpLng, context.params.pickUpLat],
      ],
      "rideCoordinatesEncoded": rideCoordinatesEncoded,
    });
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    bookingData = await this.getBookingDetailsById({
      "bookingId": bookingData._id.toString(),
    });
    if (
      generalSettings?.data?.isEnableCorporateRideWalletPay &&
      bookingData.coorperate
    ) {
      const updateCorporateRideWalletPay = "";
      // let corporateData = await
      await this.broker.emit("admin.updateWalletAmountTripEnded", {
        "corporateId": bookingData.coorperate?._id?.toString(),
        "amount": parseFloat(bookingData?.invoice?.payableAmount || 0),
        "paymentType": constantUtil.DEBIT,
        "paymentMethod": bookingData.payment.option,
        "bookingId": bookingData._id?.toString(),
        "refBookingId": bookingData.bookingId,
        "transactionType": constantUtil.BOOKINGCHARGE,
        "transactionReason": BOOKING_PAID + " / " + bookingData.bookingId,
      });
    }
    // bookingData = bookingData.toJSON();
    if (context.params.userId) {
      bookingData.user = {
        "firstName": userData.firstName,
        "lastName": userData.lastName,
        "phone": userData.phone,
        "avatar": userData.avatar,
        "review": userData.review,
      };
    }
    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    bookingData.vehicle.isShowProfessionalList =
      vehicleCategoryData.isShowProfessionalList;

    bookingData.retryTime = generalSettings.data.driverRequestTimeout;
    bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
    // bookingData.distanceUnit = serviceCategoryData.distanceType;
    bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
    bookingData["category"]["isNeedSecurityImageUpload"] =
      serviceCategoryData?.isNeedSecurityImageUpload || false;
    bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
    bookingData["imageURLPath"] =
      generalSettings.data.spaces.spacesBaseUrl +
      "/" +
      generalSettings.data.spaces.spacesObjectName;

    responseData = bookingObject(bookingData); // Response Data
    if (context.params.bookingType === constantUtil.INSTANT) {
      if (professionalRetryData.length > 0) {
        if (context.params.isNotify === true) {
          const notificationObject = {
            "clientId": clientId,
            "data": {
              "type": constantUtil.NOTIFICATIONTYPE,
              "userType": constantUtil.PROFESSIONAL,
              "action": constantUtil.ACTION_BOOKINGREQUEST,
              "timestamp": Date.now(),
              "message": PROFESSIONAL_NEW_RIDE,
              "details": lzStringEncode(responseData),
            },
            "registrationTokens": professionalRetryData.map((professional) => {
              if (
                // parseFloat(professional.wallet.availableAmount) <
                // parseFloat(
                //   generalSettings.data?.minimumWalletAmountToOnline
                // ) &&
                bookingData.payment.option === constantUtil.WALLET ||
                bookingData.payment.option === constantUtil.PAYMENTCARD ||
                // bookingData.payment.option !== constantUtil.CASH &&
                // bookingData.payment.option !== constantUtil.CREDIT) ||
                parseFloat(professional.wallet.availableAmount) >=
                  parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
              ) {
                return {
                  "token": professional?.deviceInfo[0]?.deviceId || "",
                  "id": professional?._id?.toString(),
                  "deviceType": professional?.deviceInfo[0]?.deviceType || "",
                  "platform": professional?.deviceInfo[0]?.platform || "",
                  "socketId": professional?.deviceInfo[0]?.socketId || "",
                  "duration": professional?.duration,
                  "distance": professional?.distance,
                };
              }
            }),
          };

          /* SOCKET PUSH NOTIFICATION */
          this.broker.emit("socket.sendNotification", notificationObject);
          // this.broker.emit('socket.statusChangeEvent', notificationObject)

          /* FCM */
          this.broker.emit("admin.sendFCM", notificationObject);
        }
        responseMessage = PROFESSIONAL_NOTIFICATION;
      } else {
        if (
          parseInt(context.params.radius) < parseInt(context.params.maxRadius)
        ) {
          errorCode = 600; // 600 --> For Retry Booking Purpose
          responseMessage = PROFESSIONAL_NOTIFICATION;
        } else {
          errorCode = 500;
          responseMessage = PROFESSIONAL_NOT_FOUND;
        }
      }
      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": PROFESSIONAL_NOTIFICATION,
      // };
    } else if (context.params.bookingType === constantUtil.SCHEDULE) {
      let cachedBookingData = await storageUtil.read(constantUtil.SCHEDULE);
      if (!cachedBookingData) {
        cachedBookingData = [];
      }
      cachedBookingData.push({
        "_id": bookingData._id,
        "bookingId": bookingData.bookingId,
        "bookingDate": bookingData.bookingDate,
      });
      storageUtil.write(constantUtil.SCHEDULE, cachedBookingData);

      if (context.params.userId)
        this.broker.emit("user.updateScheduleRide", {
          "userId": context.params.userId,
          "bookingId": bookingData._id.toString(),
          "bookingDate": bookingData.bookingDate,
          "estimationTime": context.params.estimationTime,
        });
      responseMessage = BOOKING_SCHEDULED_RIDE;
      // return {
      //   "code": 200,
      //   "data": bookingObject(bookingData),
      //   "message": BOOKING_SCHEDULED_RIDE,
      // };
    }
    //#region SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE
    if (generalSettings.data?.notifyAdmin?.isEnableNewRideBook) {
      const adminNotificationObject = {
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": constantUtil.ACTION_BOOKINGREQUEST,
          "timestamp": Date.now(),
          "message": PROFESSIONAL_NEW_RIDE,
          "details": responseData,
        },
        "registrationTokens": [{ "userType": constantUtil.ADMIN }],
      };
      this.broker.emit(
        "socket.sendAdminNotificationForRide",
        adminNotificationObject
      );
    }
    //#endregion SOCKET PUSH NOTIFICATION TO ADMIN FOR NEW RIDE
    // return {
    //   "code": 200,
    //   "data": bookingObject(bookingData),
    //   "message": responseMessage,
    // };
  } catch (e) {
    errorCode = e.code || 400;
    responseMessage = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "message": responseMessage,
    "data": responseData,
  };
};

// UPDATE WAITING TIMES
bookingAction.updateWaitingTimes = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const {
    BOOKING_UPDATE_STATUS_FAILED,
    INVALID_BOOKING,
    VEHICLE_CATEGORY_NOT_FOUND,
    BOOKING_STATUS_IS,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  if (generalSettings.data.isWaitingEnable === true) {
    let bookingData = {},
      updatedBookingData;
    const updateData =
      context.params.type === constantUtil.STARTED
        ? {
            "waitingTime.waitingStartTime": new Date(),
          }
        : {
            "waitingTime.waitingEndTime": new Date(),
          };

    updatedBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId),
        "professional": mongoose.Types.ObjectId(context.meta.professionalId),
      },
      {
        "$set": updateData,
      }
    );

    if (!updatedBookingData.nModified) {
      throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED + "/WAITING TIME");
    }
    if (context.params.type === constantUtil.ENDED) {
      bookingData = await this.adapter.model
        .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
        .lean();
      if (!bookingData) {
        throw new MoleculerError(INVALID_BOOKING);
      }
      // bookingData = bookingData.toJSON();
      updatedBookingData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(context.params.bookingId),
          "professional": mongoose.Types.ObjectId(context.meta.professionalId),
        },
        {
          "$push": {
            "waitingTimesArray": bookingData.waitingTime,
          },
        }
      );
      if (!updatedBookingData.nModified) {
        throw new MoleculerError(
          BOOKING_UPDATE_STATUS_FAILED + "/WAITING TIME"
        );
      }
    }
    bookingData = await this.adapter.model
      .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
      })
      .populate("category")
      .populate("security")
      .populate("officer", { "password": 0, "preferences": 0 })
      .lean();
    if (!bookingData) {
      throw new MoleculerError(INVALID_BOOKING);
    }
    // bookingData = bookingData.toJSON();
    let vehicleCategoryData = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    if (!vehicleCategoryData) {
      throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
    }
    vehicleCategoryData =
      vehicleCategoryData[bookingData.vehicle.vehicleCategoryId.toString()];

    bookingData.vehicle.vehicleCategoryName =
      vehicleCategoryData.vehicleCategory;
    bookingData.vehicle.vehicleCategoryImage =
      vehicleCategoryData.categoryImage;
    bookingData.vehicle.vehicleCategoryMapImage =
      vehicleCategoryData.categoryMapImage;
    // bookingData.distanceUnit = bookingData.category.distanceType;

    return {
      "code": 200,
      "data": bookingObject(bookingData),
      "message": BOOKING_STATUS_IS + " " + bookingData.bookingStatus,
    };
  } else {
    throw new MoleculerError(
      BOOKING_UPDATE_STATUS_FAILED + "/WAITING TIME IS NOT AVAILABLE"
    );
  }
};

bookingAction.getOperatorBasedBooking = async function (context) {
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  let matchData = {};
  matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "createdAt": { "$lte": new Date() },
          // "coorperate": mongoose.Types.ObjectId(context.params.operatorId),
        }
      : {
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
          // "coorperate": mongoose.Types.ObjectId(context.params.operatorId),
        };

  switch (context.params.userType) {
    case constantUtil.COORPERATEOFFICE:
      matchData["coorperate"] = mongoose.Types.ObjectId(
        context.params.operatorId
      );
      break;

    case constantUtil.OPERATORS:
      matchData["admin"] = mongoose.Types.ObjectId(context.params.operatorId);
      break;
  }

  const query = [];
  query.push({
    "$match":
      context.params.city === ""
        ? matchData
        : {
            ...matchData,
            "category": mongoose.Types.ObjectId(context.params.city),
          },
  });
  query.push(
    {
      "$project": {
        "_id": 1,
        "createdAt": {
          "$dateToString": { "format": "%Y-%m-%d", "date": "$createdAt" },
        },
        "bookingDetails": {
          "bookingStatus": "$bookingStatus",
          "bookingType": "$bookingType",
        },
      },
    },
    {
      "$group": {
        "_id": "$createdAt",
        "bookingDetails": {
          "$push": "$bookingDetails",
        },
        "createdAt": { "$first": "$createdAt" },
      },
    }
  );
  const aggregateData = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  const responseJson = [];

  _.forEach(aggregateData, async (data) => {
    const finalData = {
      "date": data.createdAt,
      "data": {
        "totalCount": 0,
        "newCount": 0,
        "scheduleCount": 0,
        "userdenyCount": 0,
        "ongoingCount": 0,
        "expiredCount": 0,
        "cancelCount": 0,
        "endedCount": 0,
      },
    };
    _.map(data.bookingDetails, async (status) => {
      switch (status.bookingStatus) {
        case constantUtil.AWAITING:
          finalData.data.totalCount += 1;
          if (status.bookingType === constantUtil.SCHEDULE) {
            finalData.data.scheduleCount += 1;
          } else {
            finalData.data.newCount += 1;
          }
          break;
        case constantUtil.ACCEPTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERDENY:
          finalData.data.userdenyCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.PROFESSIONALCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.EXPIRED:
          finalData.data.expiredCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.STARTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.ENDED:
          finalData.data.endedCount += 1;
          finalData.data.totalCount += 1;
          break;
      }
    });
    responseJson.push(finalData);
  });

  return responseJson;
};

bookingAction.getOperatorBasedCategoryBooking = async function (context) {
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "createdAt": { "$lte": new Date() },
          "coorperate": mongoose.Types.ObjectId(context.params.operatorId),
        }
      : {
          "createdAt": {
            "$gt": new Date(date),
            "$lte": new Date(),
          },
          "coorperate": mongoose.Types.ObjectId(context.params.operatorId),
        };

  const query = [];
  query.push({
    "$match":
      context.params.city === ""
        ? matchData
        : {
            ...matchData,
            "category": mongoose.Types.ObjectId(context.params.city),
          },
  });
  query.push(
    {
      "$lookup": {
        "from": "admins",
        "localField": "vehicle.vehicleCategoryId",
        "foreignField": "_id",
        "as": "vehicles",
      },
    },
    {
      "$unwind": {
        "path": "$vehicles",
        "preserveNullAndEmptyArrays": true,
      },
    },
    {
      "$project": {
        "_id": 1,
        "vehicle": "$vehicles",
        "bookingDetails": {
          "bookingStatus": "$bookingStatus",
          "bookingType": "$bookingType",
        },
      },
    },
    {
      "$group": {
        "_id": "$vehicle._id",
        "bookingDetails": {
          "$push": "$bookingDetails",
        },
        "vehicleCategoryId": { "$first": "$vehicle._id" },
        "vehicleCategoryName": { "$first": "$vehicle.data.vehicleCategory" },
      },
    }
  );

  const responseJson = [];
  const aggregateData = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  _.forEach(aggregateData, async (data) => {
    const finalData = {
      "vehicleCategoryId": data.vehicleCategoryId,
      "vehicleCategoryName": data.vehicleCategoryName,
      "data": {
        "totalCount": 0,
        "newCount": 0,
        "scheduleCount": 0,
        "userdenyCount": 0,
        "ongoingCount": 0,
        "expiredCount": 0,
        "cancelCount": 0,
        "endedCount": 0,
      },
    };
    _.map(data.bookingDetails, async (status) => {
      switch (status.bookingStatus) {
        case constantUtil.AWAITING:
          finalData.data.totalCount += 1;
          if (status.bookingType === constantUtil.SCHEDULE) {
            finalData.data.scheduleCount += 1;
          } else {
            finalData.data.newCount += 1;
          }
          break;
        case constantUtil.ACCEPTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERDENY:
          finalData.data.userdenyCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.USERCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.PROFESSIONALCANCELLED:
          finalData.data.cancelCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.EXPIRED:
          finalData.data.expiredCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.STARTED:
          finalData.data.ongoingCount += 1;
          finalData.data.totalCount += 1;
          break;
        case constantUtil.ENDED:
          finalData.data.endedCount += 1;
          finalData.data.totalCount += 1;
          break;
      }
    });
    responseJson.push(finalData);
  });

  return responseJson;
};

bookingAction.getTotalFareData = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const fromTodayDate = new Date(
    new Date(generalSettings.data.lastCoorperateBilledDate).setHours(0, 0, 0, 0)
  );
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "bookingStatus": constantUtil.ENDED,
    "coorperate": mongoose.Types.ObjectId(context.params.operaterId),
  });

  if (context.params.billingId && context.params.billingId != "") {
    match["$and"].push({
      "billingId": mongoose.Types.ObjectId(context.params.billingId),
    });
  } else if (
    (context.params?.fromDate || "") !== "" &&
    (context.params?.toDate || "") !== ""
  ) {
    const fromDate = new Date(
      new Date(context.params.fromDate).setHours(0, 0, 0, 0)
    );
    const toDate = new Date(
      new Date(context.params.toDate).setHours(23, 59, 59, 999)
    );
    match["$and"].push({ "bookingDate": { "$gte": fromDate, "$lt": toDate } });
  } else {
    match["$and"].push({ "bookingDate": { "$gte": fromTodayDate } });
  }
  if (context.params.city && context.params.city != "") {
    match["$and"].push({
      "category": mongoose.Types.ObjectId(context.params.city),
    });
  }
  if (context.params.vehicleCategory && context.params.vehicleCategory != "") {
    match["$and"].push({
      "vehicle.vehicleCategoryId": mongoose.Types.ObjectId(
        context.params.vehicleCategory
      ),
    });
  }
  query.push({
    "$match": match,
  });
  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        {
          "$project": {
            "_id": "$_id",
            "invoice": "$invoice",
          },
        },
        {
          "$group": {
            "_id": null,
            "totalBookings": { "$sum": 1 },
            "totalFare": {
              "$sum": "$invoice.payableAmount",
            },
            "totalProfessionalEarnings": {
              "$sum": "$invoice.professionalCommision",
            },
            "totalSiteEarnings": {
              "$sum": "$invoice.siteCommission",
            },
          },
        },
      ],
    },
  });

  const responseJson = {};
  const jsonData1 = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  responseJson["dashboardData"] =
    jsonData1[0].response && jsonData1[0].response.length > 0
      ? jsonData1[0].response[0]
      : {
          "_id": "",
          "totalBookings": 0,
          "totalFare": 0,
          "totalProfessionalEarnings": 0,
          "totalSiteEarnings": 0,
        };

  return responseJson;
};

bookingAction.cooprateRideList = async function (context) {
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.phoneNumber": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingFor.name": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };

  const query = [
    {
      "$match": {
        "$and": [
          {
            "coorperate": mongoose.Types.ObjectId(context.params.id),
          },
        ],
      },
    },
    {
      "$facet": {
        "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
        "response": [
          { "$sort": { "_id": -1 } },
          {
            "$lookup": {
              "from": "professionals",
              "localField": "professional",
              "foreignField": "_id",
              "as": "professional",
            },
          },
          {
            "$unwind": {
              "path": "$professional",
              "preserveNullAndEmptyArrays": true,
            },
          },
          searchQuery,
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
          {
            "$project": {
              "_id": "$_id",
              "bookingId": "$bookingId",
              "bookingType": "$bookingType",
              "bookingStatus": "$bookingStatus",
              "bookingDate": "$bookingDate",
              "bookingFor": "$bookingFor",
              "bookingBy": "$bookingBy",
              "invoice": "$invoice",
              "origin": "$origin",
              "destination": "$destination",
              "vehicle": "$vehicle",
              "estimation": "$estimation",
              "user": {
                "_id": "$user._id",
                "firstName": "$user.firstName",
                "lastName": "$user.lastName",
                "email": "$user.email",
                "phone": "$user.phone",
              },
              "professional": {
                "_id": "$professional._id",
                "firstName": "$professional.firstName",
                "lastName": "$professional.lastName",
                "email": "$professional.email",
                "phone": "$professional.phone",
              },
              "isAssignAllowed": {
                "$cond": {
                  "if": {
                    "$lte": [
                      { "$subtract": ["$bookingDate", new Date()] },
                      25 * 60 * 1000,
                    ],
                  },
                  "then": true,
                  "else": false,
                },
              },
            },
          },
        ],
      },
    },
  ];
  if (!!context.params.filter === true) {
    query[0].$match.$and.push({ "bookingStatus": context.params.filter });
  }

  if (
    !!context.params?.fromDate === true &&
    !!context.params?.toDate === true
  ) {
    context.params.fromDate = new Date(context.params.fromDate);
    context.params.toDate = new Date(context.params.toDate).setDate(
      new Date(context.params.toDate).getDate() + 1
    );
    query[0].$match.$and.push({
      "bookingDate": {
        "$gte": context.params.fromDate,
        "$lt": context.params.toDate,
      },
    });
  } else if (!!context.params.recentFilter === true) {
    const { data } = await storageUtil.read(constantUtil.GENERALSETTING);

    query[0].$match.$and.push({
      "bookingDate": {
        "$gte": new Date(
          new Date(data.lastCoorperateBilledDate).setHours(0, 0, 0, 0)
        ),
      },
    });
  }

  if (!!context.params?.bookingByFilter === true) {
    query[0].$match.$and.push({ "bookingBy": context.params.bookingByFilter });
  }

  const [jsonData] = await this.adapter.model
    .aggregate(query)
    .allowDiskUse(true);

  return {
    "count": jsonData?.all?.[0]?.all || 0,
    "response": jsonData?.response?.length ? jsonData.response : [],
  };
};

bookingAction.getFareCalculation = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.find(
    {
      "_id": { "$in": ids },
      "bookingStatus": {
        "$in": [constantUtil.ENDED, constantUtil.USERCANCELLED],
      },
    },
    { "invoice": 1 }
  );
  let finalData = 0;
  await responseJson.map(async (data) => {
    finalData += data.invoice.payableAmount;
  });
  return {
    "amount": finalData,
  };
};
bookingAction.quickRideBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const {
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    USER_NOT_FOUND,
    USER_WALLET_PAYMENT_INFO,
    USER_PAYMENT_DECLINED,
    PROFESSIONAL_NOT_FOUND,
    INVALID_BOOKING,
    BOOKING_ACCEPTED,
    PROFESSIONAL_NEW_RIDE,
    PROFESSIONAL_ONLINE_NOTIFICATION,
    VEHICLE_NOT_AVAILABLE,
    INFO_PROFESSIONAL_HAVE_RIDE,
    INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  let paymentData,
    paymentInitId = null,
    paymentInitAmount = 0;
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "id": context.params.categoryId,
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
  if (!serviceCategoryData) {
    throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
  }
  serviceCategoryData.categoryData.vehicles.map((vehicle) => {
    if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
      vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
    }
  });
  let userData = await this.broker.emit("user.getById", {
    "id": context.meta.userId.toString(),
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  const bookingId =
    context.params?.bookingId ||
    `${generalSettings.data.bookingPrefix}-${customAlphabet(
      "1234567890",
      9
    )()}`;

  let professionalData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  if (professionalData.onlineStatus === false) {
    throw new MoleculerError(PROFESSIONAL_ONLINE_NOTIFICATION);
  }
  if (
    professionalData?.paymentMode?.includes(context.params.paymentOption) ===
    false
  ) {
    throw new MoleculerError(INFO_PROFESSIONAL_NOT_ACCEPT_SELECTED_PAYMENT);
  }
  if (professionalData.bookingInfo.ongoingBooking) {
    throw new MoleculerError(INFO_PROFESSIONAL_HAVE_RIDE, 422);
  }
  if (
    // parseFloat(professionalData.wallet.availableAmount) <
    // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
    context.params.paymentOption === constantUtil.WALLET ||
    context.params.paymentOption === constantUtil.PAYMENTCARD ||
    // context.params.paymentOption !== constantUtil.CASH &&
    // context.params.paymentOption !== constantUtil.CREDIT) ||
    parseFloat(professionalData.wallet.availableAmount) >=
      parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
  ) {
    console.log("continue booking....");
  } else {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  if (context.params.paymentOption === constantUtil.PAYMENTWALLET) {
    /* @TODO CARD PAYMENT PENDING*/
    if (
      parseFloat(userData.wallet.availableAmount) <
      parseFloat(context.params.estimationAmount)
    ) {
      throw new MoleculerError(USER_WALLET_PAYMENT_INFO);
    }
  }
  if (context.params.paymentOption === constantUtil.PAYMENTCARD) {
    let cardPayment = await this.broker.emit(
      "transaction.newBookingCardPayment",
      {
        ...context.params, // it contains all data
        "bookingId": bookingId,
        "userData": userData,
        "cardId": context.params.card.cardId,
        "estimationAmount": context.params?.estimationAmount,
      }
    );
    cardPayment = cardPayment && cardPayment[0];
    if (!cardPayment || cardPayment?.code > 200) {
      throw new MoleculerClientError(
        USER_PAYMENT_DECLINED,
        400,
        "CARD ERROR",
        cardPayment?.data
      );
    }
    paymentInitId =
      context.params?.paymentIntentId || cardPayment?.data?.paymentInitId;
    paymentInitAmount =
      context.params?.paymentIntentAmount || context.params.estimationAmount;
  }

  const vehicleDetails = {};
  professionalData.vehicles.forEach((vehicle) => {
    if (
      vehicle.status === constantUtil.ACTIVE &&
      vehicle.defaultVehicle === true
    ) {
      vehicleDetails["vehicleCategoryId"] = context.params.vehicleCategoryId;
      vehicleDetails["vinNumber"] = vehicle.vinNumber;
      vehicleDetails["type"] = vehicle.type;
      vehicleDetails["plateNumber"] = vehicle.plateNumber;
      vehicleDetails["model"] = vehicle.model;
      vehicleDetails["year"] = vehicle.year;
      vehicleDetails["color"] = vehicle.color;
      vehicleDetails["noOfDoors"] = vehicle.noOfDoors;
      vehicleDetails["noOfSeats"] = vehicle.noOfSeats;
    }
  });
  if (Object.keys(vehicleDetails).length === 0) {
    throw new MoleculerError(VEHICLE_NOT_AVAILABLE);
  }
  const newDate = helperUtil.toUTC(new Date().toISOString());
  const newRegionalDate = helperUtil.toRegionalUTC(new Date());
  context.params["vehicleCategoryData"] = vehicleCategoryData; // Vehicle Category Details
  const rideCoordinatesEncoded = context.params.rideCoordinatesEncoded;
  //------------------------------
  let bookingData = await this.adapter.model.create({
    "clientId": clientId,
    "bookingId": bookingId,
    "bookingOTP": customAlphabet("1234567890", 4)().toString(),
    "bookingType": constantUtil.INSTANT,
    "bookingSubType": context.params.bookingSubType || constantUtil.NORMAL,
    "guestType": constantUtil.USER,
    "bookingDate": newDate,
    "regionalData": {
      "bookingDate": newRegionalDate,
      "acceptTime": newRegionalDate,
      "arriveTime": newRegionalDate,
      "cancelTime": null,
      "pickUpTime": null,
      "dropTime": null,
    },
    "bookingFor": context.params.bookingFor,
    "bookingBy": constantUtil.USER,
    "bookingFrom": constantUtil.CONST_APP,
    "tripType": constantUtil.DAILYTRIP,
    "paymentSection": generalSettings.data.paymentSection,
    "bookingStatus": constantUtil.ARRIVED,
    "admin": null,
    "user": context.meta.userId,
    "professional": context.params.professionalId,
    "category": context.params.categoryId,
    "couponId": context.params.couponId || null,
    "isGenderAvailable": context.params.isGenderAvailable || false,
    "childseatAvailable": context.params.childseatAvailable || false,
    "handicapAvailable": context.params.handicapAvailable || false,
    "isShareRide": false,
    "isPetAllowed": context.params.isPetAllowed || false,
    "isEnableLuggage": context.params?.isEnableLuggage || false,
    "vehicle": vehicleDetails,
    "origin": {
      "addressName": context.params.pickUpAddressName,
      "fullAddress": context.params.pickUpFullAddress,
      "shortAddress": context.params.pickUpShortAddress,
      "lat": context.params.pickUpLat,
      "lng": context.params.pickUpLng,
    },
    "destination": {
      "addressName": context.params.dropAddressName,
      "fullAddress": context.params.dropFullAddress,
      "shortAddress": context.params.dropShortAddress,
      "lat": context.params.dropLat,
      "lng": context.params.dropLng,
    },
    "estimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "bookedEstimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "waitingTime": {
      "waitingStartTime": null,
      "waitingEndTime": null,
    },
    "currencySymbol": serviceCategoryData.currencySymbol,
    "currencyCode": serviceCategoryData.currencyCode,
    "distanceUnit": serviceCategoryData.distanceType,
    "invoice": mappingUtil.calculateInvoiceObject(context.params),
    "serviceCategory":
      context.params?.serviceCategory || CONST_RIDE.toLowerCase(),
    "activity": {
      "noOfUsersInVehicle": null,
      "noOfSeatsAvailable": null,
      "isUserPickedUp": false,
      "isUserDropped": false,
      "waitingDuration": null,
      "actualBookingTime": new Date(),
      "bookingTime": new Date(),
      "denyOrExpireTime": null,
      "cancelTime": null,
      "acceptTime": new Date(),
      "arriveTime": new Date(),
      "pickUpTime": null,
      "dropTime": null,
      "rideStops": context.params.bookingLocations || [],
      "acceptLatLng": context.params.latLng,
      "arriveLatLng": context.params.latLng,
      "pickUpLatLng": context.params.latLng,
    },
    "rideStops": context.params.bookingLocations || [],
    "notes": context.params.notes || null,
    "payment": {
      "option": context.params.paymentOption,
      "details": paymentData?.data || null,
      "card":
        context.params.paymentOption === constantUtil.PAYMENTCARD
          ? context.params.card
          : {},
      "paid": true,
    },
    "isRestrictMaleBookingToFemaleProfessional":
      !!serviceCategoryData?.isRestrictMaleBookingToFemaleProfessional,
    // for strip Payment wave
    "paymentInitId": paymentInitId || null,
    "paymentInitAmount": paymentInitAmount || 0,
    "pickupLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "dropLocation.coordinates": [
      [context.params.dropLng, context.params.dropLat],
    ],
    "upcomingLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "rideCoordinatesEncoded": rideCoordinatesEncoded,
  });
  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  }
  // bookingData = bookingData.toJSON();

  // if (context.params.couponApplied)
  //   this.broker.emit("admin.updateCouponCount", {
  //     "userId": context.meta.userId.toString(),
  //     "couponId": context.params.couponId.toString(),
  //   });

  await this.broker.emit("professional.updateOnGoingBooking", {
    "professionalId": context.params.professionalId.toString(),
    "bookingId": bookingData._id.toString(),
  });

  this.broker.emit("user.updateOnGoingBooking", {
    "userId": context.meta.userId.toString(),
    "bookingId": bookingData._id.toString(),
    "bookingType": bookingData.bookingType,
  });

  bookingData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(bookingData._id.toString()))
    .populate("admin", { "data.accessToken": 0, "data.password": 0 })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();

  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  bookingData.vehicle.isShowProfessionalList =
    vehicleCategoryData.isShowProfessionalList;

  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = serviceCategoryData.distanceType;
  bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
  bookingData["category"]["isNeedSecurityImageUpload"] =
    serviceCategoryData?.isNeedSecurityImageUpload || false;
  bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;

  const notificationObject = {
    "clientId": clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": constantUtil.PROFESSIONAL,
      "action": constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN,
      "timestamp": Date.now(),
      "message": PROFESSIONAL_NEW_RIDE,
      "details": lzStringEncode(bookingObject(bookingData)),
    },
    "registrationTokens": [
      {
        "token": professionalData?.deviceInfo[0]?.deviceId || "",
        "id": professionalData?._id?.toString(),
        "deviceType": professionalData?.deviceInfo[0]?.deviceType || "",
        "platform": professionalData?.deviceInfo[0]?.platform || "",
        "socketId": professionalData?.deviceInfo[0]?.socketId || "",
      },
    ],
  };

  /* SOCKET PUSH NOTIFICATION */
  this.broker.emit("socket.sendNotification", notificationObject);
  // this.broker.emit('socket.statusChangeEvent', notificationObject)

  /* FCM */
  this.broker.emit("admin.sendFCM", notificationObject);

  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": BOOKING_ACCEPTED,
  };
};

bookingAction.getCouponBasedRideList = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);
  const query = [];
  const match = {};
  match["$and"] = [];
  match["$and"].push({
    "couponId": mongoose.Types.ObjectId(context.params.id),
  });
  match["$and"].push({
    "bookingStatus": { "$nin": [constantUtil.USERDENY, constantUtil.EXPIRED] },
  });
  query.push({
    "$match": match,
  });
  const serchText = context.params.search?.trim();
  const searchQuery = {
    "$match": {
      "$or": [
        {
          "bookingId": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingDate": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingType": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "bookingStatus": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.firstName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.lastName": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.email": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
        {
          "professional.phone.number": {
            "$regex": serchText + ".*",
            "$options": "si",
          },
        },
      ],
    },
  };
  query.push({
    "$facet": {
      "all": [{ "$sort": { "_id": -1 } }, searchQuery, { "$count": "all" }],
      "response": [
        { "$sort": { "_id": -1 } },
        searchQuery,
        { "$skip": parseInt(skip) },
        { "$limit": parseInt(limit) },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "bookingId": "$bookingId",
            "bookingType": "$bookingType",
            "bookingStatus": "$bookingStatus",
            "bookingDate": "$bookingDate",
            "bookingFor": "$bookingFor",
            "origin": "$origin",
            "destination": "$destination",
            "vehicle": "$vehicle",
            "professional": {
              "_id": "$professional._id",
              "firstName": "$professional.firstName",
              "lastName": "$professional.lastName",
              "email": "$professional.email",
              "phone": "$professional.phone",
            },
          },
        },
      ],
    },
  });
  const responseJson = {};
  const jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

  responseJson["count"] = jsonData[0]?.all[0]?.all || 0;
  responseJson["response"] = jsonData[0]?.response || [];
  return responseJson;
};
//
bookingAction.trackBookingUsingLink = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const data = await this.adapter.model
    .findOne({
      "bookingId": context.params.trackId,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .lean();
  // data = data?.toJSON();
  if (!data) {
    throw new MoleculerClientError("NOT FOUND", 404, "", {
      "trackId": context.params.trackId,
    });
  }
  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  vehicleCategoryData = vehicleCategoryData[data.vehicle.vehicleCategoryId];
  if (!vehicleCategoryData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG PLEASE TRY AFTER SOME TIMES"
    );
  }
  return {
    "pickup": data?.origin?.fullAddress,
    "drop": data?.destination?.fullAddress,
    "pickupLat": data.origin.lat,
    "pickupLng": data.origin.lng,
    "destinationLat": data.destination.lat,
    "destinationLng": data.destination.lng,
    "vehicle": {
      "model": data?.professional?.vehicles[0].model,
      "plateNumber": data?.professional?.vehicles[0]?.plateNumber,
      "color": data?.professional?.vehicles[0]?.color,
      "categoryImage": vehicleCategoryData.categoryImage,
    },
    "professional": {
      "name":
        data?.professional?.firstName + " " + data?.professional?.lastName,
      "rating": data?.professional?.review?.avgRating,
      "phone": callSettings.data.isNeedCallMasking
        ? {
            "code": "",
            "number": generalSettings?.data?.callCenterPhone,
          }
        : data?.professional?.phone,
      "avatar": data?.professional?.avatar,
      "location": {
        "lng": data?.professional.location.coordinates[0],
        "lat": data?.professional.location.coordinates[1],
      },
    },
    "bookingStatus": data?.bookingStatus,
    "bookingId": data?.bookingId,
    "estimationTime": data?.estimation.time,
    "encodedPolyline": data?.encodedPolyline,
  };
};

bookingAction.trackCurrentLocationOfRide = async function (context) {
  const data = await this.adapter.model
    .findOne({ "bookingId": context.params.trackId }, { "professioanl": 1 })
    .populate("professional", {
      "location": 1,
    })
    .lean();
  if (!data) {
    throw new MoleculerError("INVALID REQUEST DONT HAVE PERMISSION");
  } else {
    return {
      "code": 200,
      "message": "Data fetched successfully",
      "data": {
        "location": {
          "lng": data?.professional.location.coordinates[0],
          "lat": data?.professional.location.coordinates[1],
        },
      },
    };
  }
};

bookingAction.trackRideCoordinatesUsingRideId = async function (context) {
  const clientId = context.params.clientId;
  const responseData = await this.adapter.model
    .findOne(
      { "_id": mongoose.Types.ObjectId(context.params.rideId) },
      {
        "_id": 1,
        "bookingStatus": 1,
        "professional": 1,
        "professional": 1,
        "rideCoordinatesEncoded": 1,
        "pickupWayData": 1,
        "manualWayData": 1,
      }
    )
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .lean();
  if (!responseData) {
    throw new MoleculerError("INVALID REQUEST DONT HAVE PERMISSION");
  } else {
    const locationData = {
      // "location": {
      //   "lng": responseData?.professional.location.coordinates[0],
      //   "lat": responseData?.professional.location.coordinates[1],
      // },
      "rideId": responseData._id.toString(),
      "bookingStatus": responseData?.bookingStatus,
      "lat": responseData?.professional?.location?.coordinates?.[1],
      "lng": responseData?.professional?.location?.coordinates?.[0],
      "currentBearing": responseData?.professional?.currentBearing || null,
      "altitude": responseData?.professional?.altitude || null,
      "speed": responseData?.professional?.speed || null,
      "horizontalAccuracy":
        responseData?.professional?.horizontalAccuracy || null,
      "verticalAccuracy": responseData?.professional?.verticalAccuracy || null,
      "isLocationUpdated":
        responseData?.professional?.isLocationUpdated || null,
      "locationUpdatedTime":
        responseData?.professional?.locationUpdatedTime || null,
      "professionalName":
        responseData?.professional?.firstName +
        " " +
        (responseData?.professional?.lastName || ""),
      "professionalPhoneNo":
        responseData?.professional?.phone?.code +
        " " +
        responseData?.professional?.phone?.number,
      "rideCoordinatesEncoded": responseData?.rideCoordinatesEncoded,
      "pickupWayData": responseData?.pickupWayData || [],
      "manualWayData": responseData?.manualWayData || [],
    };
    //
    if (context.params.action === constantUtil.CONST_GET) {
      const notificationObject = {
        "temprorySound": true,
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.PROFESSIONAL,
          "action": constantUtil.ACTION_UPDATE_WAYDATA,
          // "action": constantUtil.ACTION_BOOKINGREQUEST,
          "timestamp": Date.now(),
          "message": "Way Data",
          // "details": lzStringEncode({ "rideId": context.params.rideId }),
          "details": lzStringEncode(bookingObject(responseData)),
        },
        "registrationTokens": [
          {
            "token": responseData?.professional?.deviceInfo[0]?.deviceId || "",
            "id": responseData?.professional?._id?.toString(),
            "deviceType":
              responseData?.professional?.deviceInfo[0]?.deviceType || "",
            "platform":
              responseData?.professional?.deviceInfo[0]?.platform || "",
            "socketId":
              responseData?.professional?.deviceInfo[0]?.socketId || "",
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);
      // // /* FCM */
      // this.broker.emit("admin.sendFCM", notificationObject);
      // //------------- Start ---------------
      // // let responseData = await this.getBookingDetailsById({
      // //   "bookingId": context.params.rideId.toString(),
      // // });
      // locationData["rideId"] = context.params.rideId.toString();
      // const adminNotificationObject = {
      //   "clientId": clientId,
      //   "data": {
      //     "type": constantUtil.NOTIFICATIONTYPE,
      //     "userType": constantUtil.ADMIN,
      //     "action": constantUtil.ACTION_BOOKINGREQUEST,
      //     "timestamp": Date.now(),
      //     "message": "PROFESSIONAL_NEW_RIDE",
      //     "details": locationData,
      //   },
      //   "registrationTokens": [{ "userType": constantUtil.ADMIN }],
      // };
      // this.broker.emit(
      //   "socket.sendAdminNotificationForRideWayData",
      //   adminNotificationObject
      // );
      // //------------- End ---------------
    }
    // this.broker.emit("socket.getRideWayDataFromProfessional", {
    //   "socketId": responseData?.professional?.deviceInfo[0]?.socketId,
    //   "professionalId": responseData?.professional?._id?.toString(),
    //   "rideId": context.params.rideId,
    // });
    return {
      "code": 200,
      "message": "Data fetched successfully",
      "data": locationData,
    };
  }
};
//
bookingAction.professionalSelectAndRideBooking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const {
    VEHICLE_CATEGORY_NOT_FOUND,
    SERVICE_CATEGORY_NOT_FOUND,
    USER_NOT_FOUND,
    PROFESSIONAL_NOT_FOUND,
    PROFESSIONAL_ONLINE_NOTIFICATION,
    USER_WALLET_PAYMENT_INFO,
    USER_PAYMENT_DECLINED,
    INVALID_BOOKING,
    PROFESSIONAL_NEW_RIDE,
    PROFESSIONAL_NOTIFICATION,
  } = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );

  let vehicleCategoryData = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  vehicleCategoryData = vehicleCategoryData[context.params.vehicleCategoryId];

  if (!vehicleCategoryData) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  let serviceCategoryData = await this.broker.emit("category.getCategoryById", {
    "id": context.params.categoryId,
  });
  serviceCategoryData = serviceCategoryData && serviceCategoryData[0];
  if (!serviceCategoryData) {
    throw new MoleculerError(SERVICE_CATEGORY_NOT_FOUND);
  }
  serviceCategoryData.categoryData.vehicles.map((vehicle) => {
    if (vehicle.categoryId.toString() === context.params.vehicleCategoryId) {
      vehicleCategoryData = { ...vehicleCategoryData, ...vehicle };
    }
  });

  // switch (context.params.type.toUpperCase()) {
  //   case constantUtil.INSTANT: {
  //     context.params.bookingType = constantUtil.INSTANT;
  //     break;
  //   }
  //   // case constantUtil.SCHEDULE: {
  //   //   context.params.bookingType = constantUtil.SCHEDULE;
  //   //   break;
  //   // }
  //   // case constantUtil.SHARE: {
  //   //   context.params.bookingType = constantUtil.SHARE;
  //   //   break;
  //   // }
  //   default: {
  //     break;
  //   }
  // }
  let bookingData = {};
  let userData = await this.broker.emit("user.getById", {
    "id": context.meta.userId.toString(),
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  let professionalData = await this.broker.emit("professional.getById", {
    "id": context.params.professionalId.toString(),
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  if (professionalData.onlineStatus === false) {
    throw new MoleculerError(PROFESSIONAL_ONLINE_NOTIFICATION);
  }
  if (
    // parseFloat(professionalData.wallet.availableAmount) <
    // parseFloat(generalSettings.data?.minimumWalletAmountToOnline) &&
    context.params.paymentOption === constantUtil.WALLET ||
    context.params.paymentOption === constantUtil.PAYMENTCARD ||
    // context.params.paymentOption !== constantUtil.CASH &&
    // context.params.paymentOption !== constantUtil.CREDIT) ||
    parseFloat(professionalData.wallet.availableAmount) >=
      parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
  ) {
    console.log("continue booking....");
  } else {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  // if ((context?.params?.bookingId ?? "") === "") {
  const bookingId = `${generalSettings.data.bookingPrefix}-${customAlphabet(
    "1234567890",
    9
  )()}`;
  let paymentData;
  let paymentInitId;
  let paymentInitAmount;

  // let professionalRetryData = [[{}]];
  // if (context.params.bookingType === constantUtil.INSTANT) {
  //   professionalRetryData = await this.getProfessionalByLocation(
  //     context.params.pickUpLat,
  //     context.params.pickUpLng,
  //     context.params.dropLat,
  //     context.params.dropLng,
  //     context.params.vehicleCategoryId,
  //     generalSettings.data.requestDistance,
  //     true,
  //     true,
  //     false,
  //     vehicleCategoryData.isSubCategoryAvailable,
  //     vehicleCategoryData.isForceAppliedToProfessional,
  //     context.params.isGenderAvailable || false,
  //     context.params.childseatAvailable || false,
  //     context.params.handicapAvailable || false
  //   );
  //   if (professionalRetryData[0] && professionalRetryData[0].length === 0)
  //     professionalRetryData = await this.getProfessionalByLocation(
  //       context.params.pickUpLat,
  //       context.params.pickUpLng,
  //       context.params.dropLat,
  //       context.params.dropLng,
  //       context.params.vehicleCategoryId,
  //       generalSettings.data.retryRequestDistance,
  //       false,
  //       true,
  //       generalSettings.data.isTailRideNeeded,
  //       vehicleCategoryData.isSubCategoryAvailable,
  //       vehicleCategoryData.isForceAppliedToProfessional,
  //       context.params.isGenderAvailable || false,
  //       context.params.childseatAvailable || false,
  //       context.params.handicapAvailable || false
  //     );

  //   if (professionalRetryData[0] && professionalRetryData[0].length === 0)
  //     return {
  //       "code": 503,
  //       "data": {},
  //       "message": PROFESSIONAL_NOT_FOUND,
  //     };
  //   professionalRetryData =
  //     professionalRetryData[0] && professionalRetryData[0];
  // }

  // const vehicleDetails = {};
  // professionalData.vehicles.forEach((vehicle) => {
  //   if (
  //     vehicle.status === constantUtil.ACTIVE &&
  //     vehicle.defaultVehicle === true
  //   ) {
  //     vehicleDetails["vehicleCategoryId"] = context.params.vehicleCategoryId;
  //     vehicleDetails["vinNumber"] = vehicle.vinNumber;
  //     vehicleDetails["type"] = vehicle.type;
  //     vehicleDetails["plateNumber"] = vehicle.plateNumber;
  //     vehicleDetails["model"] = vehicle.model;
  //     vehicleDetails["year"] = vehicle.year;
  //     vehicleDetails["color"] = vehicle.color;
  //     vehicleDetails["noOfDoors"] = vehicle.noOfDoors;
  //     vehicleDetails["noOfSeats"] = vehicle.noOfSeats;
  //   }
  // });

  if (context.params.paymentOption === constantUtil.PAYMENTWALLET) {
    /* @TODO CARD PAYMENT PENDING*/
    if (
      parseFloat(userData.wallet.availableAmount) <
      parseFloat(context.params.estimationAmount)
    ) {
      throw new MoleculerError(USER_WALLET_PAYMENT_INFO);
    }
    // if (context.params.bookingType === constantUtil.SCHEDULE) {
    //   const updateValue =
    //     parseFloat(userData.wallet.availableAmount) -
    //     parseFloat(context.params.estimationAmount);
    //   const scheduleFreezedAmount =
    //     parseFloat(userData.wallet.scheduleFreezedAmount) +
    //     parseFloat(context.params.estimationAmount);
    //   this.broker.emit("user.scheduleWalletUpdate", {
    //     "userId": context.meta.userId,

    //     "finalAmount": updateValue,
    //     "amount": scheduleFreezedAmount,
    //   });
    // }
  }
  if (context.params.paymentOption === constantUtil.PAYMENTCARD) {
    let cardPayment = await this.broker.emit(
      "transaction.newBookingCardPayment",
      {
        ...context.params, // it contains all data
        "bookingId": bookingId,
        "userData": userData,
        "cardId": context.params.card.cardId,
        "estimationAmount": context.params?.estimationAmount,
      }
    );
    cardPayment = cardPayment && cardPayment[0];
    if (!cardPayment || cardPayment?.code > 200) {
      throw new MoleculerClientError(
        USER_PAYMENT_DECLINED,
        400,
        "CARD ERROR",
        cardPayment?.data
      );
    }
    paymentInitId = cardPayment?.data?.paymentInitId;
    paymentInitAmount = context.params.estimationAmount;
  }

  const newDate = helperUtil.toUTC(new Date().toISOString());
  const newRegionalDate = helperUtil.toRegionalUTC(new Date());
  //  vehicle Category Data Added to params
  context.params["vehicleCategoryData"] = vehicleCategoryData;
  const rideCoordinatesEncoded = context.params.rideCoordinatesEncoded;
  //-------------------------
  // // algortithim part
  // const requestSendProfessionals = [];
  // if (
  //   context.params.bookingType === constantUtil.INSTANT &&
  //   generalSettings.data.bookingAlgorithm === constantUtil.NEAREST
  // ) {
  //   requestSendProfessionals.push({
  //     "professionalId": professionalRetryData[0]._id.toString(),
  //     "count": 1,
  //   });
  //   professionalRetryData = [professionalRetryData[0]];
  // }

  bookingData = await this.adapter.model.create({
    "clientId": clientId,
    "bookingId": bookingId,
    "bookingOTP": customAlphabet("1234567890", 4)().toString(),
    "bookingType": constantUtil.INSTANT,
    "bookingSubType": constantUtil.PROFESSIONALSELECT,
    "guestType": constantUtil.USER,
    "bookingDate": newDate,
    "regionalData": {
      "bookingDate": newRegionalDate,
      "cancelTime": null,
      "acceptTime": null,
      "arriveTime": null,
      "pickUpTime": null,
      "dropTime": null,
    },
    "bookingFor": context.params.bookingFor,
    "bookingBy": constantUtil.USER,
    "bookingFrom": constantUtil.CONST_APP,
    "tripType": constantUtil.DAILYTRIP,
    "paymentSection": generalSettings.data.paymentSection,
    "admin": null,
    "user": context.meta.userId,
    "category": context.params.categoryId,
    "couponId": context.params.couponId || null,
    "isGenderAvailable": context.params.isGenderAvailable || false,
    "childseatAvailable": context.params.childseatAvailable || false,
    "handicapAvailable": context.params.handicapAvailable || false,
    "isShareRide": false,
    "isPetAllowed": context.params.isPetAllowed || false,
    "isEnableLuggage": context.params?.isEnableLuggage || false,
    "vehicle": {
      "vehicleCategoryId": context.params.vehicleCategoryId,
    },
    "origin": {
      "addressName": context.params.pickUpAddressName,
      "fullAddress": context.params.pickUpFullAddress,
      "shortAddress": context.params.pickUpShortAddress,
      "lat": context.params.pickUpLat,
      "lng": context.params.pickUpLng,
    },
    "destination": {
      "addressName": context.params.dropAddressName,
      "fullAddress": context.params.dropFullAddress,
      "shortAddress": context.params.dropShortAddress,
      "lat": context.params.dropLat,
      "lng": context.params.dropLng,
    },
    "estimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "bookedEstimation": {
      "distance": context.params.estimationDistance,
      "time": context.params.estimationTime,
      "pickupDistance": context.params.estimationPickupDistance,
      "pickupTime": context.params.estimationPickupTime,
      "dropDistance": context.params.estimationDropDistance,
      "dropTime": context.params.estimationDropTime,
    },
    "waitingTime": {
      "waitingStartTime": null,
      "waitingEndTime": null,
    },
    "currencySymbol": serviceCategoryData.currencySymbol,
    "currencyCode": serviceCategoryData.currencyCode,
    "distanceUnit": serviceCategoryData.distanceType,
    "invoice": mappingUtil.calculateInvoiceObject(context.params),
    "serviceCategory": context.params.serviceCategory,
    "activity": {
      "noOfUsersInVehicle": null,
      "noOfSeatsAvailable": null,
      "isUserPickedUp": false,
      "isUserDropped": false,
      "waitingDuration": null,
      "actualBookingTime": new Date(),
      "bookingTime": new Date(),
      "denyOrExpireTime": null,
      "cancelTime": null,
      "acceptTime": null,
      "arriveTime": null,
      "pickUpTime": null,
      "dropTime": null,
      "rideStops": context.params.bookingLocations || [],
      "acceptLatLng": context.params.latLng,
    },
    "rideStops": context.params.bookingLocations || [],
    "notes": context.params.notes || null,
    "payment": {
      "option": context.params.paymentOption,
      "details": paymentData?.data || null,
      "card":
        context.params.paymentOption === constantUtil.PAYMENTCARD
          ? context.params.card
          : {},
      "paid": true,
    },
    // for CARD Pre-Payment
    "paymentInitId": paymentInitId || null,
    "paymentInitAmount": paymentInitAmount || 0,
    // // for alogorithim
    // "requestSendProfessionals": requestSendProfessionals,
    "pickupLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "dropLocation.coordinates": [
      [context.params.dropLng, context.params.dropLat],
    ],
    "upcomingLocation.coordinates": [
      [context.params.pickUpLng, context.params.pickUpLat],
    ],
    "rideCoordinatesEncoded": rideCoordinatesEncoded,
  });

  if (!bookingData) {
    throw new MoleculerError(INVALID_BOOKING);
  } else {
    //#region Packages
    if (
      bookingData.serviceCategory === constantUtil.CONST_PACKAGES.toLowerCase()
    ) {
      this.broker.emit("booking.updatePackageDetailsById", {
        "action": constantUtil.ADD,
        "uploadedBy": constantUtil.USER,
        "packageData": context.params.packageData || {},
        "bookingId": bookingData?._id?.toString(),
      });
    }
    //#endregion Packages
    // Get booking Details
    bookingData = await this.getBookingDetailsById({
      "bookingId": bookingData?._id?.toString(),
    }); // Get booking Details
  }
  // bookingData = bookingData.toJSON();
  // } else {
  //   //  vehicle Category Data Added to params
  //   context.params["vehicleCategoryData"] = vehicleCategoryData;
  //   // update Booking data
  //   bookingData = await this.adapter.model.findOneAndUpdate(
  //     {
  //       "_id": mongoose.Types.ObjectId(context.params.bookingId),
  //     },
  //     {
  //       "vehicle": {
  //         "vehicleCategoryId": context.params.vehicleCategoryId,
  //       },
  //       "invoice": mappingUtil.calculateInvoiceObject(context),
  //     },
  //     { "new": true }
  //   );
  //   // .populate("user", {
  //   //   "password": 0,
  //   //   "bankDetailes": 0,
  //   //   "trustedContacts": 0,
  //   //   "cards": 0,
  //   // })
  //   // .populate("professional", {
  //   //   "password": 0,
  //   //   "cards": 0,
  //   //   "bankDetails": 0,
  //   //   "trustedContacts": 0,
  //   // })
  //   // .populate("category")
  //   // .populate("security")
  //   // .populate("officer", { "password": 0, "preferences": 0 });

  //   if (!bookingData) throw new MoleculerError(BOOKING_UPDATE_FAILED);
  //   bookingData = bookingData.toJSON();
  // }
  // track link

  const trackLink = `${generalSettings.data.redirectUrls.trackBooking}${bookingData.bookingId}`;

  bookingData.trackLink = trackLink;

  bookingData.user = {
    "firstName": context.meta.userDetails.firstName,
    "lastName": context.meta.userDetails.lastName,
    "phone": context.meta.userDetails.phone,
    "avatar": context.meta.userDetails.avatar,
    "review": context.meta.userDetails.review,
  };
  bookingData.vehicle.vehicleCategoryName = vehicleCategoryData.vehicleCategory;
  bookingData.vehicle.vehicleCategoryImage = vehicleCategoryData.categoryImage;
  bookingData.vehicle.vehicleCategoryMapImage =
    vehicleCategoryData.categoryMapImage;
  bookingData.vehicle.isShowProfessionalList =
    vehicleCategoryData.isShowProfessionalList;

  bookingData.retryTime = generalSettings.data.driverRequestTimeout;
  bookingData.totalRetryCount = generalSettings.data.bookingRetryCount;
  // bookingData.distanceUnit = serviceCategoryData.distanceType;
  bookingData.isNeedCallMasking = callSettings.data.isNeedCallMasking;
  bookingData["category"]["isNeedSecurityImageUpload"] =
    serviceCategoryData?.isNeedSecurityImageUpload || false;
  bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
  bookingData["imageURLPath"] =
    generalSettings.data.spaces.spacesBaseUrl +
    "/" +
    generalSettings.data.spaces.spacesObjectName;

  // if (context.params.bookingType === constantUtil.INSTANT) {
  const notificationObject = {
    "temprorySound": true,
    "clientId": clientId,
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": constantUtil.PROFESSIONAL,
      "action": constantUtil.ACTION_BOOKINGREQUEST,
      "timestamp": Date.now(),
      "message": PROFESSIONAL_NEW_RIDE,
      "details": lzStringEncode(bookingObject(bookingData)),
    },
    "registrationTokens": [
      {
        "token": professionalData?.deviceInfo[0]?.deviceId || "",
        "id": professionalData?._id?.toString(),
        "deviceType": professionalData?.deviceInfo[0]?.deviceType || "",
        "platform": professionalData?.deviceInfo[0]?.platform || "",
        "socketId": professionalData?.deviceInfo[0]?.socketId || "",
        "duration": professionalData?.duration,
        "distance": professionalData?.distance,
      },
    ],
  };

  /* SOCKET PUSH NOTIFICATION */
  this.broker.emit("socket.sendNotification", notificationObject);

  /* FCM */
  this.broker.emit("admin.sendFCM", notificationObject);
  return {
    "code": 200,
    "data": bookingObject(bookingData),
    "message": PROFESSIONAL_NOTIFICATION,
  };
  // }
};
bookingAction.makeCallUsingCallMasking = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const callSettings = await storageUtil.read(constantUtil.CALLSETTING);
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  const { USER_NOT_FOUND, PROFESSIONAL_NOT_FOUND } =
    await notifyMessage.setNotifyLanguage(
      context.meta?.userDetails?.languageCode
    );

  let responseBookingData = await this.adapter.model.find(
    {
      "_id": mongoose.Types.ObjectId(context.params.rideId.toString()),
      // mongoose.Types.ObjectId(context.params.bookingId.toString())
    },
    { "professional": 1, "user": 1 }
  );
  responseBookingData = responseBookingData && responseBookingData[0];
  // let bookingData = {};
  let userData = await this.broker.emit("user.getById", {
    "id": responseBookingData.user.toString(),
  });
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(USER_NOT_FOUND);
  }
  let professionalData = await this.broker.emit("professional.getById", {
    "id": responseBookingData.professional.toString(),
  });
  professionalData = professionalData && professionalData[0];
  if (!professionalData) {
    throw new MoleculerError(PROFESSIONAL_NOT_FOUND);
  }
  let callFrom = "",
    callTo = "";
  switch (context.params.requestFrom.toUpperCase()) {
    case constantUtil.USER:
      callFrom = userData.phone.number;
      callTo = professionalData.phone.number;
      break;

    case constantUtil.PROFESSIONAL:
      callFrom = professionalData.phone.number;
      callTo = userData.phone.number;
      break;
  }
  //---------- Tele CMI Call Masking Start ------------------
  const jsonInputData = {
    "appid": parseInt(callSettings.data.accountAccessKey), //2222295,
    "secret": callSettings.data.authToken, //"a698094b-d650-4533-a0d9-62ad70a4d9fb",
    "from": parseInt(
      callSettings.data.defaultCountryCode + callSettings.data.virtualNumber
    ), //918037222886, //Sender Number with Country Code
    "to": parseInt(callFrom), // From Call Number without Country Code
    "pcmo": [
      {
        "action": "bridge",
        "duration": 300,
        "timeout": 20,
        "from": parseInt(callSettings.data.virtualNumber), //8037222886, //Sender Number without Country Code
        "loop": 2,
        "connect": [
          {
            "type": "pstn",
            "number": parseInt(callTo), // To Call Number without Country Code
          },
        ],
      },
    ],
  };
  // const jsonInputData = {
  //   "appid": parseInt(callSettings.data.accountAccessKey), //2222295,
  //   "secret": callSettings.data.authToken, //"a698094b-d650-4533-a0d9-62ad70a4d9fb",
  //   "from": parseInt(callSettings.data.virtualNumber), //918037222886, //Sender no with Country Code
  //   "to": 9786160442, // From Call Number without Country Code
  //   "pcmo": [
  //     {
  //       "action": "bridge",
  //       "duration": 300,
  //       "timeout": 20,
  //       "from": 8037222886, //Sender no without Country Code
  //       "loop": 2,
  //       "connect": [
  //         {
  //           "type": "pstn",
  //           "number": 9342085455, // To Call Number without Country Code
  //         },
  //       ],
  //     },
  //   ],
  // };
  const config = {
    "method": "post",
    "url": "https://piopiy.telecmi.com/v1/pcmo_make_call",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": jsonInputData,
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data.message,
    };
  }
  //---------- Tele CMI Call Masking End ------------------
};
//----------------- Security Image Upload Start ------------
bookingAction.uploadSecurityImage = async function (context) {
  const userType = context?.params?.userType?.toUpperCase() || "";
  let languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { INFO_FILE_UPLOAD_SUCCESS, SOMETHING_WENT_WRONG } =
    await notifyMessage.setNotifyLanguage(languageCode);

  if (context?.params?.files?.length) {
    let imageResponse = await this.broker.emit(
      "admin.uploadSingleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": "rideId_" + `${context.params.rideId}` + "_",
        "requestFrom": constantUtil.BOOKING,
      }
    );
    imageResponse = imageResponse && imageResponse[0];
    if (!imageResponse) {
      throw new MoleculerError(SOMETHING_WENT_WRONG);
    }
    // update Booking data
    await this.adapter.model.findOneAndUpdate(
      {
        "_id": mongoose.Types.ObjectId(context.params.rideId),
      },
      { "securityImageName": imageResponse },
      { "new": true }
    );
    return {
      "code": 200,
      "data": { "securityImageName": imageResponse },
      "message": INFO_FILE_UPLOAD_SUCCESS,
    };
  }
};
//----------------- Security Image Upload End ------------
//----------------- Image Upload Start ------------
bookingAction.uploadSingleImage = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    languageCode;

  try {
    const userType = context?.params?.userType?.toUpperCase() || "";
    if (userType === constantUtil.USER) {
      languageCode = context.meta?.userDetails?.languageCode;
    } else if (userType === constantUtil.PROFESSIONAL) {
      languageCode = context.meta?.professionalDetails?.languageCode;
    }
    const { INFO_FILE_UPLOAD_SUCCESS, SOMETHING_WENT_WRONG } =
      notifyMessage.setNotifyLanguage(languageCode);
    let isValidFile = false;
    if (
      (context.params.action === constantUtil.ADD &&
        context.params?.files?.length) ||
      context.params.action === constantUtil.REMOVE
    ) {
      isValidFile = true;
    }

    if (isValidFile) {
      let imageResponse = await this.broker.emit(
        "admin.uploadSingleImageInSpaces",
        {
          "files": context.params?.files,
          "fileName":
            `${context.params.serviceCategory}` + "_" + `${Date.now()}` + "_",
          "requestFrom": context.params.requestFrom,
          "oldFileName": context.params.oldFileName,
          "action": context.params.action,
        }
      );
      imageResponse = imageResponse && imageResponse[0];
      if (!imageResponse) {
        throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
      }
      if (context.params.action === constantUtil.ADD) {
        responseData = {
          "requestFrom": context.params.requestFrom,
          "imageURL": imageResponse,
          "fileName": imageResponse.split("/").pop(),
        };
        message = INFO_FILE_UPLOAD_SUCCESS;
      }
    } else {
      errorCode = 400;
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
    responseData = {};
  }
  // return responseData;
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};
//----------------- Image Upload End ------------
// bookingAction.updateProfessionalBookingStatus

//#region Live Meter

bookingAction.rideLiveMeter = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context?.params?.userType?.toUpperCase() || "";
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    languageCode = "",
    responseData = {},
    bookingData = {};
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { INFO_SUCCESS, SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(context?.params?.langCode || languageCode);
  try {
    if (context.params.requestType === constantUtil.CONST_GET) {
      bookingData = await this.adapter.model
        .findOne(
          {
            "_id": mongoose.Types.ObjectId(context.params.rideId.toString()),
          },
          {
            "_id": 1,
            "currencySymbol": 1,
            "currencyCode": 1,
            "bookingDate": 1,
            "distanceUnit": 1,
            "estimation": 1,
            "invoice": 1,
            "bookingId": 1,
            "activity.acceptTime": 1,
            "activity.arriveTime": 1,
            "activity.pickUpTime": 1,
            "activity.acceptLatLng": 1,
            "activity.arriveLatLng": 1,
            "activity.pickUpLatLng": 1,
          }
        )
        .populate("category", {
          "_id": 0,
          "isPeakFareAvailable": 1,
          "isNightFareAvailable": 1,
        })
        .populate("professional", { "location": 1 })
        .lean();

      //  .populate("admin", { "data.accessToken": 0, "data.password": 0 })
      // .populate("user", {
      //   "password": 0,
      //   "bankDetails": 0,
      //   "cards": 0,
      //   "trustedContacts": 0,
      // })
      // .populate("professional", {
      //   "password": 0,
      //   "bankDetails": 0,
      //   "cards": 0,
      //   "trustedContacts": 0,
      // })
      // .populate("category")
      // .populate("security")
      // .populate("officer", { "password": 0, "preferences": 0 });
      context.params["bookingData"] = bookingData;
      context.params["afterRideWaitingMinsWithGracePeriod"] = 0;
      context.params["traveledDistance"] = 1;
      context.params["traveledTime"] = 1;

      // const distanceData = await googleApiUtil.distancematrix({
      //   "lat": bookingData.professional.location.coordinates[1],
      //   "lng": bookingData.professional.location.coordinates[0],
      // });
      // context.params["pickupDistance"] = distanceData?.distance || 0;
      //Pickup Distance
      // const pickupLatLngData = [];
      // pickupLatLngData.push(bookingData.activity.acceptLatLng);
      // pickupLatLngData.push(bookingData.activity.arriveLatLng);
      // const distanceData = await helperUtil.snapWayPath(pickupLatLngData);
      //Pickup Distance
      const mapDataPickup = await googleApiUtil.directions({
        "clientId": clientId,
        "from":
          bookingData.activity.acceptLatLng.lat +
          "," +
          bookingData.activity.acceptLatLng.lng,
        "to":
          bookingData.activity.arriveLatLng.lat +
          "," +
          bookingData.activity.arriveLatLng.lng,
      });
      // const distanceData = await helperUtil.snapWayPath([
      //   {
      //     "lat": -28.6717216,
      //     "lng": -49.3777503,
      //     // "timeStamp": 1642342745,
      //     // "isStop": false,
      //   },
      //   {
      //     "lat": -28.6717997,
      //     "lng": -49.377989,
      //     // "timeStamp": 1642344362,
      //     // "isStop": false,
      //   },
      // ]);
      // context.params["pickupDistance"] = distanceData?.distance || 0;
      context.params["pickupDistance"] =
        mapDataPickup.data?.routes[0]?.legs[0]?.distance?.value || 0;
      //Drop Distance
      const mapDataDrop = await googleApiUtil.directions({
        "clientId": clientId,
        "from":
          bookingData.activity.pickUpLatLng.lat +
          "," +
          bookingData.activity.pickUpLatLng.lng,
        "to": context.params.latLng.lat + "," + context.params.latLng.lng,
      });
      context.params["dropDistance"] =
        mapDataDrop.data?.routes[0]?.legs[0]?.distance?.value || 0;
      //Pickup Time
      let pickupTime = parseInt(
        (new Date(context.params.bookingData?.activity?.pickUpTime) -
          new Date(context.params.bookingData?.activity?.acceptTime)) /
          1000
      );
      if (pickupTime < 60) {
        pickupTime = 60;
      }
      context.params["pickupTime"] = pickupTime;
    } //else if (context.params.requestType === constantUtil.CONST_TRACK) {
    //Drop Time
    let dropTime = parseInt(
      (new Date() -
        new Date(context.params.bookingData?.activity?.acceptTime)) /
        1000
    );
    if (dropTime < 60) {
      dropTime = 60;
    }
    context.params["dropTime"] = dropTime;

    //#region Live Meter Calculation
    if (context.params.requestType === constantUtil.CONST_TRACK) {
      context.params.bookingData["invoice"][
        "afterRideWaitingMinsWithGracePeriod"
      ] = context.params.afterRideWaitingMinsWithGracePeriod; // For Waiting Charge (After ride)
      // context.params.bookingData["estimation"]["time"] = parseInt(
      //   context.params.traveledTime
      // );
      // context.params.bookingData["estimation"]["distance"] = parseInt(
      //   context.params.traveledDistance
      // );

      const traveledTime = parseInt(
        (new Date() -
          new Date(context.params.bookingData?.activity?.pickUpTime)) /
          1000
      );
      if (parseInt(context.params.traveledTime) <= parseInt(traveledTime)) {
        context.params["traveledTime"] = parseInt(context.params.traveledTime);
      } else {
        context.params["traveledTime"] = traveledTime;
      }
    }

    // responseData = await helperUtil.calculateEstimateAmountForLiveMeter({
    responseData = await helperUtil.calculateEstimateAmount({
      "clientId": clientId,
      "bookingData": context.params.bookingData,
      //
      "tollFareAmount": context.params?.tollFareAmount || 0,
      "airportFareAmount": context.params?.airportFareAmount || 0,
      //
      "estimationDistance": context.params.traveledDistance,
      "estimationTime": context.params.traveledTime,
      "calculatedPickupDistance": context.params.pickupDistance,
      "calculatedPickupTime": context.params.pickupTime,
      "calculatedDropDistance": context.params.dropDistance,
      "calculatedDropTime": context.params.dropTime,
      "beforeRideAssistanceCareProvidesMins": parseInt(
        context.params.beforeRideAssistanceCareProvidesMins || 0
      ),
      "afterRideAssistanceCareProvidesMins": parseInt(
        context.params.afterRideAssistanceCareProvidesMins || 0
      ),
    });
    //#endregion Live Meter Calculation

    if (responseData) {
      // responseData["invoice"] = {};
      // responseData["invoice"]["payableAmount"] = responseData.payableAmount;
      responseData["currencySymbol"] =
        context.params.bookingData?.currencySymbol;
      responseData["currencyCode"] = context.params.bookingData?.currencyCode;
      responseData["liveMeterAPIHitInterval"] =
        generalSettings.data?.liveMeterAPIHitInterval || 100;
      responseData["liveMeterTurfInterruptInterval"] =
        generalSettings.data?.liveMeterTurfInterruptInterval || 100;
      responseData["bookingData"] = context.params.bookingData;
      responseData["pickupDistance"] = context.params.pickupDistance || 0;
      responseData["pickupTime"] = context.params.pickupTime || 0;
      responseData["dropDistance"] = context.params.dropDistance || 0;
      responseData["dropTime"] = context.params.dropTime || 0;
    }
    // }
    message = INFO_SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": responseData,
  };
};

//#endregion Live Meter

//#region Dashboard
// Ride Graphs..........

bookingAction.getAllRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  // bookingData = {};
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              // "$gt": new Date(new Date().getTime() - 7 * 60 * 60 * 24 * 1000),
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              // "$gt": new Date(new Date().getTime() - 7 * 60 * 60 * 24 * 1000),
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
          },
        },
        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingStatus": "$bookingStatus",
            "userDenetCount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.USERDENY] },
                "then": { "$sum": 1 },
                "else": 0,
              },
            },
            "complitedCount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": 1,
                "else": 0,
              },
            },
            "ProfessionalEndedCount": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingStatus", constantUtil.PROFESSIONALCANCELLED],
                },
                "then": 1,
                "else": 0,
              },
            },
            "failedCount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.EXPIRED] },
                "then": 1,
                "else": 0,
              },
            },
            "CancledCount": {
              "$cond": {
                "if": {
                  "$in": [
                    "$bookingStatus",
                    [
                      constantUtil.USERCANCELLED,
                      constantUtil.PROFESSIONALCANCELLED,
                      constantUtil.USERDENY,
                    ],
                  ],
                },
                "then": 1,
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": { "bookingDate": "$bookingDate" },
            "complitedCount": { "$sum": "$complitedCount" },
            "failedCount": { "$sum": "$failedCount" },
            "CancledCount": { "$sum": "$CancledCount" },
            "totalCount": { "$sum": 1 },
          },
        },
        { "$sort": { "_id": 1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : notifyMessage.SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

bookingAction.getIncomeExpenceRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "createdAt": {
            //   "$gte": new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
            // },
            ...matchCondition,
            "bookingStatus": {
              "$in": [
                constantUtil.ENDED,
                constantUtil.PROFESSIONALCANCELLED,
                constantUtil.USERCANCELLED,
              ],
            },
            // "bookingStatus": { "$in":[constantUtil.ENDED]}
          },
        },
        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "ProfessionalCancelAmount": {
              "$cond": {
                "if": {
                  "$eq": ["$bookingStatus", constantUtil.PROFESSIONALCANCELLED],
                },
                "then": "$invoice.professionalCancellationAmount",
                "else": 0,
              },
            },
            "userCancelAmount": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.USERCANCELLED] },
                "then": "$invoice.cancellationAmount",
                "else": 0,
              },
            },
            "invoice": 1,
            "income": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": {
                  "$add": [
                    "$invoice.surchargeFee",
                    "$invoice.siteCommission",
                    "$invoice.serviceTax",
                  ],
                },
                "else": 0,
              },
            },

            "expence": {
              "$cond": {
                "if": { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                "then": "$invoice.professionalCommision",
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": { "bookingDate": "$bookingDate" },

            "revenue": {
              "$sum": {
                "$add": [
                  "$invoice.payableAmount",
                  "$userCancelAmount",
                  "$ProfessionalCancelAmount",
                ],
              },
            },
            //       "income": { "$sum": "$income" },
            "expence": {
              "$sum": { "$add": ["$expence", "$userCancelAmount"] },
            },
            //       "professionalCommision": { "$sum": "$invoice.professionalCommision" },
            //       "payableAmount": { "$sum": "$invoice.payableAmount" },
            "income": {
              "$sum": { "$add": ["$income", "$ProfessionalCancelAmount"] },
            },
            // "expence": {
            //   "$cond": {
            //     if: { "$eq": ["$bookingDate", "0"] },
            //     then: 0,
            //     else: { "$sum": { "$add": ["$expence", "$userCancelAmount"] } },
            //   },
            // },
            //       "ProfessionalCancelAmount":{"$sum":"$ProfessionalCancelAmount"},
            //       "userCancelAmount":{"$sum":"$cancellationAmount"}
          },
        },
        { "$sort": { "_id": 1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getProfessionalsRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "createdAt": {
            //   "$gt": new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
            // },
            ...matchCondition,
          },
        },

        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingStatus": "$bookingStatus",
            "bookingBy": "$bookingBy",

            "totalCount": {
              "$cond": {
                "if": { "$eq": ["$bookingBy", constantUtil.PROFESSIONAL] },
                "then": 1,
                "else": 0,
              },
            },
            "complitedCount": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                    { "$eq": ["$bookingBy", constantUtil.PROFESSIONAL] },
                  ],
                },
                "then": 1,
                "else": 0,
              },
            },
            "CancledCount": {
              "$cond": {
                "if": {
                  "$and": [
                    {
                      "$in": [
                        "$bookingStatus",
                        [
                          constantUtil.USERCANCELLED,
                          constantUtil.PROFESSIONALCANCELLED,
                          constantUtil.USERDENY,
                        ],
                      ],
                    },
                    { "$eq": ["$bookingBy", constantUtil.PROFESSIONAL] },
                  ],
                },
                "then": 1,
                "else": 0,
              },
            },
            "inProgress": {
              "$cond": {
                "if": {
                  "$and": [
                    {
                      "$in": [
                        "$bookingStatus",
                        [
                          constantUtil.ARRIVED,
                          constantUtil.STARTED,
                          constantUtil.ACCEPTED,
                        ],
                      ],
                    },
                    { "$eq": ["$bookingBy", constantUtil.PROFESSIONAL] },
                  ],
                },
                "then": 1,
                "else": 0,
              },
            },
            "failedCount": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$bookingStatus", constantUtil.EXPIRED] },
                    { "$eq": ["$bookingBy", constantUtil.PROFESSIONAL] },
                  ],
                },
                "then": 1,
                "else": 0,
              },
            },
          },
        },
        {
          "$group": {
            "_id": { "bookingDate": "$bookingDate" },
            "totalCount": { "$sum": "$totalCount" },
            "complitedCount": { "$sum": "$complitedCount" },
            "CancledCount": { "$sum": "$CancledCount" },
            "failedCount": { "$sum": "$failedCount" },
            "inProgress": { "$sum": "$inProgress" },
          },
        },
        { "$sort": { "_id": 1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getUsersRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };
    const query = [];

    query.push(
      {
        "$match": {
          // "createdAt": {
          //   "$gt": new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
          // },
          ...matchCondition,
        },
      },

      {
        "$project": {
          "_id": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingDate": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingStatus": "$bookingStatus",
          "bookingBy": "$bookingBy",

          "totalCount": {
            "$cond": {
              "if": { "$eq": ["$bookingBy", constantUtil.USER] },
              "then": 1,
              "else": 0,
            },
          },
          "complitedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                  { "$eq": ["$bookingBy", constantUtil.USER] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "CancledCount": {
            "$cond": {
              "if": {
                "$and": [
                  {
                    "$in": [
                      "$bookingStatus",
                      [
                        constantUtil.USERCANCELLED,
                        constantUtil.PROFESSIONALCANCELLED,
                        constantUtil.USERDENY,
                      ],
                    ],
                  },
                  { "$eq": ["$bookingBy", constantUtil.USER] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "failedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.EXPIRED] },
                  { "$eq": ["$bookingBy", constantUtil.USER] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": { "bookingDate": "$bookingDate" },
          "totalCount": { "$sum": "$totalCount" },
          "complitedCount": { "$sum": "$complitedCount" },
          "CancledCount": { "$sum": "$CancledCount" },
          "failedCount": { "$sum": "$failedCount" },
        },
      },
      { "$sort": { "_id": 1 } }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getCorporateRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };
    const query = [];

    query.push(
      {
        "$match": {
          // "createdAt": {
          //   "$gt": new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
          // },
          ...matchCondition,
        },
      },

      {
        "$project": {
          "_id": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingDate": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingStatus": "$bookingStatus",
          "bookingBy": "$bookingBy",

          "totalCount": {
            "$cond": {
              "if": { "$eq": ["$bookingBy", constantUtil.COORPERATEOFFICE] },
              "then": 1,
              "else": 0,
            },
          },
          "complitedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                  { "$eq": ["$bookingBy", constantUtil.COORPERATEOFFICE] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "CancledCount": {
            "$cond": {
              "if": {
                "$and": [
                  {
                    "$in": [
                      "$bookingStatus",
                      [
                        constantUtil.USERCANCELLED,
                        constantUtil.PROFESSIONALCANCELLED,
                        constantUtil.USERDENY,
                      ],
                    ],
                  },
                  { "$eq": ["$bookingBy", constantUtil.COORPERATEOFFICE] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "failedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.EXPIRED] },
                  { "$eq": ["$bookingBy", constantUtil.COORPERATEOFFICE] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": { "bookingDate": "$bookingDate" },
          "totalCount": { "$sum": "$totalCount" },
          "complitedCount": { "$sum": "$complitedCount" },
          "CancledCount": { "$sum": "$CancledCount" },
          "failedCount": { "$sum": "$failedCount" },
        },
      },
      { "$sort": { "_id": 1 } }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getAdminRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const matchCondition =
      context.params.city === ""
        ? {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
          }
        : {
            //"clientId": mongoose.Types.ObjectId(clientId),
            "createdAt": {
              "$gt": addDaysToRegionalUTC(new Date(), -7),
            },
            "category": mongoose.Types.ObjectId(context.params.city),
          };
    const query = [];

    query.push(
      {
        "$match": {
          // "createdAt": {
          //   "$gt": new Date(new Date() - 7 * 60 * 60 * 24 * 1000),
          // },
          ...matchCondition,
        },
      },

      {
        "$project": {
          "_id": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingDate": {
            "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
          },
          "bookingStatus": "$bookingStatus",
          "bookingBy": "$bookingBy",

          "totalCount": {
            "$cond": {
              "if": { "$eq": ["$bookingBy", constantUtil.ADMIN] },
              "then": 1,
              "else": 0,
            },
          },
          "complitedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.ENDED] },
                  { "$eq": ["$bookingBy", constantUtil.ADMIN] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "CancledCount": {
            "$cond": {
              "if": {
                "$and": [
                  {
                    "$in": [
                      "$bookingStatus",
                      [
                        constantUtil.USERCANCELLED,
                        constantUtil.PROFESSIONALCANCELLED,
                        constantUtil.USERDENY,
                      ],
                    ],
                  },
                  { "$eq": ["$bookingBy", constantUtil.ADMIN] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
          "failedCount": {
            "$cond": {
              "if": {
                "$and": [
                  { "$eq": ["$bookingStatus", constantUtil.EXPIRED] },
                  { "$eq": ["$bookingBy", constantUtil.ADMIN] },
                ],
              },
              "then": 1,
              "else": 0,
            },
          },
        },
      },
      {
        "$group": {
          "_id": { "bookingDate": "$bookingDate" },
          "totalCount": { "$sum": "$totalCount" },
          "complitedCount": { "$sum": "$complitedCount" },
          "CancledCount": { "$sum": "$CancledCount" },
          "failedCount": { "$sum": "$failedCount" },
        },
      },
      { "$sort": { "_id": 1 } }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
// ADMIN...........

// bookingAction.getTopDashboardAdminRides = async function (context) {
//   let errorCode = 200,
//     message = "",
//     errorMessage = "",
//     jsonData = {};
//   const type = context.params.type;
//   try {
//     const matchCondition = {};
//     if (type === "all") {
//       matchCondition["bookingBy"] = { "$in": [constantUtil.ADMIN] };
//     } else if (type === "instant") {
//       matchCondition["bookingBy"] = { "$in": [constantUtil.ADMIN] };
//       matchCondition["bookingType"] = constantUtil.INSTANT;
//     } else if (type === "schedule") {
//       matchCondition["bookingBy"] = { "$in": [constantUtil.ADMIN] };
//       matchCondition["bookingType"] = constantUtil.SCHEDULE;
//     } else if (type === "cancel") {
//       matchCondition["bookingBy"] = { "$in": [constantUtil.ADMIN] };
//       matchCondition["bookingType"] = constantUtil.USERCANCELLED;
//     }
//     if (context.params?.city !== "") {
//       matchCondition["category"] = {
//         "$in": [mongoose.Types.ObjectId(context.params.city)],
//       };
//     }
//     jsonData = await this.adapter.model.aggregate([
//       {
//         "$match": {
//           //  "bookingBy": { "$in": ["ADMIN"] },
//           ...matchCondition,
//           "coorperate": null,
//         },
//       },
//       {
//         "$lookup": {
//           "from": "admins",
//           "localField": "admin",
//           "foreignField": "_id",
//           "as": "admin",
//         },
//       },
//       { "$unwind": "$admin" },
//       {
//         "$project": {
//           // "_id": "$professional",
//           // "bookingDate": "$bookingDate",
//           //           "professional":"$professional",
//           "bookingBy": "$bookingBy",
//           //           "totalCount":{"$sum":1},
//           "admin": {
//             "$cond": {
//               "if": { "$eq": ["$admin", "$admin"] },
//               "then": "$admin",
//               "else": 0,
//             },
//           },
//           "adminName": "$admin.data.firstName",
//           // "gender": "$admin.data.gender",
//           "bookingType": "$bookingType",
//         },
//       },

//       {
//         "$group": {
//           "_id": "$admin._id",
//           "totalCount": { "$sum": 1 },
//           "adminName": { "$first": "$adminName" },
//           "bookingBy": { "$first": "$bookingBy" },
//           "bookingType": { "$first": "$bookingType" },
//         },
//       },
//       { "$sort": { "totalCount": -1 } },
//     ]);

//     return jsonData;
//   } catch (e) {
//     errorCode = 400;
//     errorMessage = e.message;
//     message = "SOMETHING_WENT_WRONG";
//   }
//   return {
//     "code": errorCode,
//     "error": errorMessage,
//     "message": message,
//     "data": jsonData,
//   };
// };
bookingAction.getTopDashboardAdminRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  // const type = context.params.type;
  try {
    const matchCondition = {};

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    matchCondition["bookingBy"] = constantUtil.ADMIN;

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": ["ADMIN"] },
            ...matchCondition,
            "coorperate": null,
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "admin",
            "foreignField": "_id",
            "as": "adminList",
          },
        },
        { "$unwind": "$adminList" },
        {
          "$project": {
            "_id": "$admin",

            "all": {
              "$cond": {
                "if": {
                  "$and": [{ "$eq": ["$admin", "$admin"] }],
                },

                "then": 1,
                "else": 0,
              },
            },
            "instant": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$admin", "$admin"] },
                    { "$eq": ["$bookingType", "INSTANT"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "schedule": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$admin", "$admin"] },
                    { "$eq": ["$bookingType", "SCHEDULE"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "Name": "$adminList.data.firstName",
            "bookingType": "$bookingType",
            "bookingBy": "$bookingBy",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "instantRides": { "$sum": "$instant" },
            "allRides": { "$sum": "$all" },
            "scheduleRides": { "$sum": "$schedule" },
            "Name": { "$first": "$Name" },
            "bookingBy": { "$first": "$bookingBy" },
            "bookingType": { "$first": "$bookingType" },
          },
        },
        { "$sort": { "allRides": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getTopDashboardAdminCategoryRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // const matchCondition =
    //   context.params.city === ""
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.ADMIN] },
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.ADMIN] },

    //         "category": mongoose.Types.ObjectId(context.params.city),
    //       };
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.ADMIN;

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            "coorperate": null,

            // "bookingBy": { "$in": ["ADMIN"] },
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "admin",
            "foreignField": "_id",
            "as": "adminList",
          },
        },
        { "$unwind": "$adminList" },
        {
          "$project": {
            "Name": "$adminList.data.firstName",
            "role": "$adminList.name",
            "_id": "$adminList._id",
            "gender": "$adminList.data.gender",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "name": { "$first": "$gender" },
            // "Name1": { "$first": "$Name" },
            // "role": { "$first": "$role" },
          },
        },
        {
          "$group": {
            "_id": "$Name",
            "totalCount": { "$sum": 1 },
            "gender": { "$first": "$name" },
          },
        },
        { "$sort": { "totalCount": -1 } },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

// Professionals........

// bookingAction.getTopProfessionalsPerformance = async function (context) {
//   let errorCode = 200,
//     message = "",
//     errorMessage = "",
//     jsonData = {};
//   const type = context.params.type;

//   try {
//     const matchCondition = {};
//     matchCondition["bookingBy"] = { "$in": [constantUtil.PROFESSIONAL] };

//     if (type === "instant") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
//     } else if (type === "schedule") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
//     } else if (type === "cancel") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
//     }
//     if (context.params?.city !== "") {
//       matchCondition["category"] = {
//         "$in": [mongoose.Types.ObjectId(context.params.city)],
//       };
//     }
//     jsonData = await this.adapter.model.aggregate([
//       {
//         "$match": {
//           // "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
//           ...matchCondition,
//         },
//       },
//       {
//         "$lookup": {
//           "from": "professionals",
//           "localField": "professional",
//           "foreignField": "_id",
//           "as": "professionalList",
//         },
//       },
//       { "$unwind": "$professionalList" },
//       {
//         "$project": {
//           //           "_id": "$professional",
//           // "bookingDate": "$bookingDate",
//           //           "professional":"$professional",
//           //           "totalCount":{"$sum":1},
//           "professional": {
//             "$cond": {
//               "if": { "$eq": ["$professional", "$professional"] },
//               "then": "$professional",
//               "else": 0,
//             },
//           },
//           "professionalName": "$professionalList.firstName",
//           // "gender": "$professional.gender",
//           "bookingBy": "$bookingBy",
//           "bookingType": "$bookingType",
//         },
//       },

//       {
//         "$group": {
//           "_id": "$professional",
//           "totalCount": { "$sum": 1 },
//           "professionalName": { "$first": "$professionalName" },
//           "bookingBy": { "$first": "$bookingBy" },
//           "bookingType": { "$first": "$bookingType" },
//         },
//       },
//       { "$sort": { "totalCount": -1 } },
//     ]);

//     return jsonData;
//   } catch (e) {
//     errorCode = 400;
//     errorMessage = e.message;
//     message = "SOMETHING_WENT_WRONG";
//   }
//   return {
//     "code": errorCode,
//     "error": errorMessage,
//     "message": message,
//     "data": jsonData,
//   };
// };
bookingAction.getTopProfessionalsRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};

  try {
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;

    // if (type === "instant") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
    // } else if (type === "schedule") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
    // } else if (type === "cancel") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
    // }
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": ["PROFESSIONAL"] },
            ...matchCondition,
            //                      "coorperate": null,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professionalList",
          },
        },
        { "$unwind": "$professionalList" },
        {
          "$project": {
            "_id": "$professional",

            "all": {
              "$cond": {
                "if": {
                  "$and": [{ "$eq": ["$professional", "$professional"] }],
                },

                "then": 1,
                "else": 0,
              },
            },
            "instant": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$professional", "$professional"] },
                    { "$eq": ["$bookingType", "INSTANT"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "schedule": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$professional", "$professional"] },
                    { "$eq": ["$bookingType", "SCHEDULE"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "Name": "$professionalList.firstName",
            "bookingType": "$bookingType",
            "bookingBy": "$bookingBy",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "instantRides": { "$sum": "$instant" },
            "allRides": { "$sum": "$all" },
            "scheduleRides": { "$sum": "$schedule" },
            //             "scheduleRides": {  "$cond": [ { "$eq": [ "$schedule", 1 ] }, 1, 0 ]},
            "Name": { "$first": "$Name" },

            "bookingBy": { "$first": "$bookingBy" },
            "bookingType": { "$first": "$bookingType" },
          },
        },

        { "$sort": { "allRides": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getTopProfessionalsVechileCategory = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // const matchCondition =
    //   context.params.city === ""
    //     ? {
    //         "bookingBy": constantUtil.PROFESSIONAL,
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },

    //         "category": mongoose.Types.ObjectId(context.params.city),
    //       };
    const matchCondition = {};

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    // toRegionalUTCDayStartHours(new Date());
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": constantUtil.PROFESSIONAL,
            //         "bookingBy" : constantUtil.USER,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "vehicle.vehicleCategoryId",
            "foreignField": "vehicles.vehicleCategoryId",
            "as": "professional",
          },
        },
        { "$unwind": "$professional" },
        {
          "$project": {
            //           "_id": "$professional",
            // "bookingDate": "$bookingDate",
            // //           "professional":"$professional",
            // "bookingBy": "$bookingBy",
            //           "totalCount":{"$sum":1},
            "vehicleCategoryId": {
              "$cond": {
                "if": {
                  "$eq": [
                    "$vehicle.vehicleCategoryId",
                    "$vehicle.vehicleCategoryId",
                  ],
                },
                "then": "$vehicle.vehicleCategoryId",
                "else": 0,
              },
            },

            //            "professional": {
            //                         "$cond": {
            //                           if: { $eq: ["$professionalId", "$professional._id"] },
            //                           then: "$professional.firstName",
            //                           else: 0,
            //                         },
            //                       },
            "vehiclesName": "$professional.vehicles.model",
          },
        },

        {
          "$group": {
            "_id": "$vehicleCategoryId",
            //           "totalCount": { "$sum": 1 },
            //               "professionalName":{ "$first": "$professionalName"},
            // "booking": { "$first": "$bookingBy" },
            "vehicles": { "$first": "$vehiclesName" },
          },
        },
        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },
            // "bookingBy": { "$first": "$booking" },
            "vehicles": { "$first": "$vehicles" },
          },
        },
        { "$sort": { "totalCount": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getTopProfessionalsCategoryRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // const matchCondition =
    //   context.params.city === ""
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },

    //         "category": mongoose.Types.ObjectId(context.params.city),
    //       };
    const matchCondition = {};

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            //  "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professionalList",
          },
        },
        { "$unwind": "$professionalList" },
        {
          "$project": {
            "Name": "$professionalList.firstName",
            "_id": "$professionalList._id",
            "gender": "$professionalList.gender",
          },
        },
        {
          "$group": {
            "_id": "$_id",
            "name": { "$first": "$gender" },
          },
        },
        {
          "$group": {
            "_id": "$professionalName",
            "totalCount": { "$sum": 1 },
            "gender": { "$first": "$name" },
          },
        },
        { "$sort": { "totalCount": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData || [];
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

// Users...........

// bookingAction.getTopUsersPerformance = async function (context) {
//   let errorCode = 200,
//     message = "",
//     errorMessage = "",
//     jsonData = {};
//   const type = context.params.type;

//   try {
//     const matchCondition = {};
//     matchCondition["bookingBy"] = { "$in": [constantUtil.USER] };

//     if (type === "instant") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
//     } else if (type === "schedule") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
//     } else if (type === "cancel") {
//       matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
//     }
//     if (context.params?.city !== "") {
//       matchCondition["category"] = {
//         "$in": [mongoose.Types.ObjectId(context.params.city)],
//       };
//     }
//     jsonData = await this.adapter.model.aggregate([
//       {
//         "$match": {
//           ...matchCondition,
//           // "bookingBy": constantUtil.USER,
//         },
//       },
//       {
//         "$lookup": {
//           "from": "users",
//           "localField": "user",
//           "foreignField": "_id",
//           "as": "users",
//         },
//       },
//       { "$unwind": "$users" },
//       {
//         "$project": {
//           //           "_id": "$user",
//           // "bookingDate": "$bookingDate",
//           // //           "professional":"$professional",
//           "bookingBy": "$bookingBy",
//           //           "totalCount":{"$sum":1},
//           "users": {
//             "$cond": {
//               "if": { "$eq": ["$user", "$user"] },
//               "then": "$user",
//               "else": 0,
//             },
//           },
//           "userName": "$users.firstName",
//           "bookingType": "$bookingType",
//         },
//       },

//       {
//         "$group": {
//           "_id": "$users",
//           "totalCount": { "$sum": 1 },
//           "userName": { "$first": "$userName" },
//           "bookingBy": { "$first": "$bookingBy" },
//           "bookingType": { "$first": "$bookingType" },
//         },
//       },
//       { "$sort": { "totalCount": -1 } },
//     ]);

//     return jsonData;
//   } catch (e) {
//     errorCode = 400;
//     errorMessage = e.message;
//     message = "SOMETHING_WENT_WRONG";
//   }
//   return {
//     "code": errorCode,
//     "error": errorMessage,
//     "message": message,
//     "data": jsonData,
//   };
// };
bookingAction.getTopUsersRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};

  try {
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.USER;

    // if (type === "instant") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
    // } else if (type === "schedule") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
    // } else if (type === "cancel") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
    // }
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": ["USER"] },
            ...matchCondition,
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "usersList",
          },
        },
        { "$unwind": "$usersList" },
        {
          "$project": {
            "_id": "$user",

            "all": {
              "$cond": {
                "if": {
                  "$and": [{ "$eq": ["$user", "$user"] }],
                },

                "then": 1,
                "else": 0,
              },
            },
            "instant": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "INSTANT"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "schedule": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "SCHEDULE"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "cancel": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "USERCANCELLED"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "Name": "$usersList.firstName",
            "bookingType": "$bookingType",
            "bookingBy": "$bookingBy",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "instantRides": { "$sum": "$instant" },
            "allRides": { "$sum": "$all" },
            "scheduleRides": { "$sum": "$schedule" },
            "cancelRides": { "$sum": "$cancel" },

            "Name": { "$first": "$Name" },

            "bookingBy": { "$first": "$bookingBy" },
            "bookingType": { "$first": "$bookingType" },
          },
        },

        { "$sort": { "allRides": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getTopUsersCategoryRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // const matchCondition =
    //   context.params.city === ""
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.USER] },
    //         // "bookingStatus": constantUtil.USERCANCELLED,
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.USER] },
    //         // "bookingStatus": constantUtil.USERCANCELLED,

    //         "category": mongoose.Types.ObjectId(context.params.city),
    //       };
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.USER;

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": constantUtil.USER,
            ...matchCondition,
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "usersList",
          },
        },
        { "$unwind": "$usersList" },
        {
          "$project": {
            "Name": "$usersList.firstName",
            "_id": "$usersList._id",
            "gender": "$usersList.gender",
          },
        },
        {
          "$group": {
            "_id": "$_id",
            "name": { "$first": "$gender" },
          },
        },
        {
          "$group": {
            "_id": "$name",
            "totalCount": { "$sum": 1 },
            "gender": { "$first": "$name" },
          },
        },
        { "$sort": { "totalCount": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

//#region  PopUp  ............

// Admin................
bookingAction.getPopUpAdminRidesDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    const matchCondition = {};
    matchCondition["admin"] = new mongoose.Types.ObjectId(context.params.id);

    // if (context.params.BookingType === constantUtil.SCHEDULE) {
    //   matchCondition["bookingType"] = constantUtil.SCHEDULE;
    // } else if (context.params.BookingType === constantUtil.INSTANT) {
    //   matchCondition["bookingType"] = constantUtil.INSTANT;
    // } else if (context.params.BookingType === constantUtil.CANCELLED) {
    //   matchCondition["bookingType"] = constantUtil.CANCELLED;
    // } else if (context.params.BookingType === constantUtil.ALL) {
    //   matchCondition["bookingBy"] = constantUtil.ADMIN;
    // }
    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.ADMIN;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.ADMIN;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    // const matchCondition =
    //   context.params.BookingType === null
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.ADMIN] },
    //         // "bookingType": {
    //         "$or": [
    //           { "bookingType": "INSTANT" },
    //           { "bookingType": "SCHEDULE" },
    //         ],
    //         // },
    //         "admin": new mongoose.Types.ObjectId(context.params.id),
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.ADMIN] },
    //         "bookingType": context.params.BookingType,
    //         "admin": new mongoose.Types.ObjectId(context.params.id),
    //       };

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": [constantUtil.ADMIN] },
            // "bookingType": context.params.BookingType,
            // "admin": new mongoose.Types.ObjectId(context.params.id),
            ...matchCondition,
            "coorperate": null,
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "admin",
            "foreignField": "_id",
            "as": "admin",
          },
        },
        { "$unwind": "$admin" },
        {
          "$project": {
            "bookingId": "$bookingId",
            // "bookingDate": "$bookingDate",
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            // "startAddress": { "$split": ["$rideStops.addressName", ","] },
            "startAddress": "$rideStops.addressName",
            "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },
            // "professionalName": { "$first": "$professionalName" },
            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },

            "startAddress": { "$first": "$startAddress" },
            "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpAdminDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "bookingBy": constantUtil.ADMIN,
            // "bookingType": context.params.BookingType,
            "admin": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "admin",
            "foreignField": "_id",
            "as": "admin",
          },
        },
        { "$unwind": "$admin" },
        {
          "$project": {
            "firstName": "$admin.data.firstName",
            "lastName": "$admin.data.lastName",
            "email": "$admin.data.email",
            "gender": "$admin.data.gender",
            "code": "$admin.data.phone.code",
            "phone": "$admin.data.phone.number",
            //
            "bookingId": "$bookingId",
          },
        },

        {
          "$group": {
            "_id": "$bookingId",

            "firstName": { "$first": "$firstName" },
            "lastName": { "$first": "$lastName" },
            "email": { "$first": "$email" },
            "gender": { "$first": "$gender" },
            "code": { "$first": "$code" },
            "phone": { "$first": "$phone" },
          },
        },
        { "$limit": 1 },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpAdminGraph = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["admin"] = new mongoose.Types.ObjectId(context.params.id);
    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.ADMIN;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.ADMIN;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.ADMIN] },

            // "admin": new mongoose.Types.ObjectId(context.params.id),
          },
        },

        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "bookingId": "$bookingId",
            //           "bookingDate": "$bookingDate",
          },
        },

        {
          "$group": {
            "_id": "$bookingDate",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

// Professionals popup........

bookingAction.getPopUpProfessionalRidesDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["professional"] = new mongoose.Types.ObjectId(
      context.params.id
    );

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    // const matchCondition =
    //   context.params.BookingType === null
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
    //         // "bookingType": {
    //         "$or": [
    //           { "bookingType": "INSTANT" },
    //           { "bookingType": "SCHEDULE" },
    //         ],
    //         // },
    //         "professional": new mongoose.Types.ObjectId(context.params.id),
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
    //         "bookingType": context.params.BookingType,
    //         "professional": new mongoose.Types.ObjectId(context.params.id),
    //       };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },
            // // "bookingType": { "$in": [constantUtil.INSTANT] },
            // "bookingType": context.params.BookingType,
            // "professional": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professionalDeatils",
          },
        },
        { "$unwind": "$professionalDeatils" },
        {
          "$project": {
            "bookingId": "$bookingId",
            // "bookingDate": "$bookingDate",
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            // "startAddress": { "$split": ["$rideStops.addressName", ","] },
            "startAddress": "$rideStops.addressName",
            "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },
            // "professionalName": { "$first": "$professionalName" },
            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },

            "startAddress": { "$first": "$startAddress" },
            "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpProfessionalDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "bookingBy": constantUtil.PROFESSIONAL,
            "professional": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professionalDeatils",
          },
        },
        { "$unwind": "$professionalDeatils" },
        {
          "$project": {
            "firstName": "$professionalDeatils.firstName",
            "lastName": "$professionalDeatils.lastName",
            "email": "$professionalDeatils.email",
            "gender": "$professionalDeatils.gender",
            "code": "$professionalDeatils.phone.code",
            "phone": "$professionalDeatils.phone.number",
          },
        },

        {
          "$group": {
            "_id": "$bookingId",

            "firstName": { "$first": "$firstName" },
            "lastName": { "$first": "$lastName" },
            "email": { "$first": "$email" },
            "gender": { "$first": "$gender" },
            "code": { "$first": "$code" },
            "phone": { "$first": "$phone" },
          },
        },
        { "$limit": 1 },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpProfessionalGraph = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["professional"] = new mongoose.Types.ObjectId(
      context.params.id
    );
    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.PROFESSIONAL;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.PROFESSIONAL] },

            // "professional": new mongoose.Types.ObjectId(context.params.id),
          },
        },

        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "bookingId": "$bookingId",
          },
        },

        {
          "$group": {
            "_id": "$bookingDate",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
// Users popup...........
bookingAction.getPopUpUsersRidesDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["user"] = new mongoose.Types.ObjectId(context.params.id);

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.USER;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.USER;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    // const matchCondition =
    //   context.params.BookingType === null
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.USER] },
    //         // "bookingType": {
    //         "$or": [
    //           { "bookingType": "INSTANT" },
    //           { "bookingType": "SCHEDULE" },
    //         ],
    //         // },
    //         "user": new mongoose.Types.ObjectId(context.params.id),
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.USER] },
    //         "bookingType": context.params.BookingType,
    //         "user": new mongoose.Types.ObjectId(context.params.id),
    //       };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.USER] },
            // "bookingType": context.params.BookingType,
            // "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "UsersDeatils",
          },
        },
        { "$unwind": "$UsersDeatils" },
        {
          "$project": {
            "bookingId": "$bookingId",
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "startAddress": "$rideStops.addressName",
            "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },

            "startAddress": { "$first": "$startAddress" },
            "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpUsersDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "bookingBy": constantUtil.USER,
            "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "UsersDeatils",
          },
        },
        { "$unwind": "$UsersDeatils" },
        {
          "$project": {
            "firstName": "$UsersDeatils.firstName",
            "lastName": "$UsersDeatils.lastName",
            "email": "$UsersDeatils.email",
            "gender": "$UsersDeatils.gender",
            "code": "$UsersDeatils.phone.code",
            "phone": "$UsersDeatils.phone.number",
          },
        },

        {
          "$group": {
            "_id": "$bookingId",
            "totalCount": { "$sum": 1 },
            "firstName": { "$first": "$firstName" },
            "lastName": { "$first": "$lastName" },
            "email": { "$first": "$email" },
            "gender": { "$first": "$gender" },
            "code": { "$first": "$code" },
            "phone": { "$first": "$phone" },
          },
        },
        { "$limit": 1 },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpUsersGraph = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["user"] = new mongoose.Types.ObjectId(context.params.id);

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.USER;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.USER;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.USER] },

            // "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },

        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "bookingId": "$bookingId",
          },
        },

        {
          "$group": {
            "_id": "$bookingDate",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
// Corporate User popup.................
bookingAction.getPopUpCorporateUsersRidesDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["user"] = new mongoose.Types.ObjectId(context.params.id);

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    // const matchCondition =
    //   context.params.BookingType === null
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
    //         // "bookingType": {
    //         "$or": [
    //           { "bookingType": "INSTANT" },
    //           { "bookingType": "SCHEDULE" },
    //         ],
    //         // },
    //         "user": new mongoose.Types.ObjectId(context.params.id),
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
    //         "bookingType": context.params.BookingType,
    //         "user": new mongoose.Types.ObjectId(context.params.id),
    //       };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
            // "bookingType": context.params.BookingType,
            // "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "corporateUser",
          },
        },
        { "$unwind": "$corporateUser" },
        {
          "$project": {
            "bookingId": "$bookingId",
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "startAddress": "$rideStops.addressName",
            "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
            "startAddress": { "$first": "$startAddress" },
            "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpCorporateUsersDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "bookingBy": constantUtil.CORPORATEOFFICER,
            "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "corporateUser",
          },
        },
        { "$unwind": "$corporateUser" },
        {
          "$project": {
            "firstName": "$corporateUser.firstName",
            "lastName": "$corporateUser.lastName",
            "email": "$corporateUser.email",
            "gender": "$corporateUser.gender",
            "code": "$corporateUser.phone.code",
            "phone": "$corporateUser.phone.number",
          },
        },

        {
          "$group": {
            "_id": "$firstName",
            "totalCount": { "$sum": 1 },
            "firstName": { "$first": "$firstName" },
            "lastName": { "$first": "$lastName" },
            "email": { "$first": "$email" },
            "gender": { "$first": "$gender" },
            "code": { "$first": "$code" },
            "phone": { "$first": "$phone" },
          },
        },
        { "$limit": 1 },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpCorporateUsersGraph = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["user"] = new mongoose.Types.ObjectId(context.params.id);

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },

            // "user": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "corporateUser",
          },
        },
        { "$unwind": "$corporateUser" },
        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "bookingId": "$bookingId",
          },
        },

        {
          "$group": {
            "_id": "$bookingDate",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
// Corporate Officer popup...........,
bookingAction.getPopUpCorporateOfficersRidesDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["coorperate"] = new mongoose.Types.ObjectId(
      context.params.id
    );

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.COORPERATEOFFICE;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.COORPERATEOFFICE;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    // const matchCondition =
    //   context.params.BookingType === null
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.COORPERATEOFFICE] },
    //         // "bookingType": {
    //         "$or": [
    //           { "bookingType": "INSTANT" },
    //           { "bookingType": "SCHEDULE" },
    //         ],
    //         // },
    //         "coorperate": new mongoose.Types.ObjectId(context.params.id),
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.COORPERATEOFFICE] },
    //         // "bookingType": context.params.BookingType,

    //         "coorperate": new mongoose.Types.ObjectId(context.params.id),

    //         // "bookingBy": { "$in": [constantUtil.ADMIN] },
    //         // "bookingType": context.params.BookingType,
    //         // "admin": new mongoose.Types.ObjectId(context.params.id),
    //       };
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.COORPERATEOFFICE] },
            // "coorperate": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "coorperate",
            "foreignField": "_id",
            "as": "corporate",
          },
        },
        { "$unwind": "$corporate" },
        {
          "$project": {
            "bookingId": "$bookingId",
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "startAddress": "$rideStops.addressName",
            "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$_id",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
            "startAddress": { "$first": "$startAddress" },
            "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpCorporateOfficersDeatils = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            "bookingBy": constantUtil.COORPERATEOFFICE,
            "coorperate": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "coorperate",
            "foreignField": "_id",
            "as": "corporate",
          },
        },
        { "$unwind": "$corporate" },
        {
          "$project": {
            "officeName": "$corporate.data.officeName",
            //           "lastName": "$corporate.data.lastName",
            "email": "$corporate.data.email",
            "city": "$corporate.data.locationAddress.city",
            "code": "$corporate.data.phone.code",
            "phone": "$corporate.data.phone.number",
          },
        },

        {
          "$group": {
            "_id": "$officeName",
            "totalCount": { "$sum": 1 },
            "firstName": { "$first": "$officeName" },
            //           "lastName": { "$first": "$lastName" },
            "email": { "$first": "$email" },
            "city": { "$first": "$city" },
            "code": { "$first": "$code" },
            "phone": { "$first": "$phone" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
bookingAction.getPopUpCorporateOfficersGraph = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);

    const matchCondition = {};
    matchCondition["coorperate"] = new mongoose.Types.ObjectId(
      context.params.id
    );

    if (context.params.BookingType === "ALL") {
      matchCondition["bookingBy"] = constantUtil.COORPERATEOFFICE;
    }
    if (context.params.BookingType !== "ALL") {
      matchCondition["bookingType"] = context.params.BookingType;
      matchCondition["bookingBy"] = constantUtil.COORPERATEOFFICE;
    }

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.COORPERATEOFFICE] },
            // // "createdAt": {
            // //   "$gte": new Date(new Date() - 20 * 60 * 60 * 24 * 1000),
            // // },
            // "coorperate": new mongoose.Types.ObjectId(context.params.id),
          },
        },
        // {
        //   "$lookup": {
        //     "from": "admins",
        //     "localField": "coorperate",
        //     "foreignField": "_id",
        //     "as": "corporate",
        //   },
        // },
        // { "$unwind": "$corporate" },
        {
          "$project": {
            "_id": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },
            "bookingDate": {
              "$dateToString": { "format": "%Y-%m-%d", "date": "$bookingDate" },
            },

            "bookingId": "$bookingId",
            //           "bookingDate": "$bookingDate",

            //           "startAddress": "$rideStops.addressName",
            //           "endAddress": "$rideStops.fullAddress",
          },
        },

        {
          "$group": {
            "_id": "$bookingDate",
            "totalCount": { "$sum": 1 },

            "bookingId": { "$first": "$bookingId" },
            "bookingDate": { "$first": "$bookingDate" },
            //           "startAddress": { "$first": "$startAddress" },
            //           "endAddress": { "$first": "$endAddress" },
          },
        },
      ])
      .allowDiskUse(true);
    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
//#endregion PopU.............

// Corporate User.........
bookingAction.getTopDashboardCorporateUserRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // if (type === "instant") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
    // } else if (type === "schedule") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
    // } else if (type === "cancel") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
    // }
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;

    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": ["CORPORATEOFFICER"] },
            ...matchCondition,
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "corporateUser",
          },
        },
        { "$unwind": "$corporateUser" },
        {
          "$project": {
            "_id": "$user",

            "all": {
              "$cond": {
                "if": {
                  "$and": [{ "$eq": ["$user", "$user"] }],
                },

                "then": 1,
                "else": 0,
              },
            },
            "instant": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "INSTANT"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "schedule": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "SCHEDULE"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "cancel": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$user", "$user"] },
                    { "$eq": ["$bookingType", "USERCANCELLED"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "Name": "$corporateUser.firstName",
            "bookingType": "$bookingType",
            "bookingBy": "$bookingBy",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "instantRides": { "$sum": "$instant" },
            "allRides": { "$sum": "$all" },
            "scheduleRides": { "$sum": "$schedule" },
            "cancelRides": { "$sum": "$cancel" },

            "Name": { "$first": "$Name" },

            "bookingBy": { "$first": "$bookingBy" },
            "bookingType": { "$first": "$bookingType" },
          },
        },

        { "$sort": { "allRides": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

bookingAction.getTopDashboardCorporateUserCategoryRides = async function (
  context
) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    // const matchCondition =
    //   context.params.city === ""
    //     ? {
    //         "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
    //         // "bookingStatus": constantUtil.USERCANCELLED,
    //       }
    //     : {
    //         "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
    //         // "bookingStatus": constantUtil.USERCANCELLED,

    //         "category": mongoose.Types.ObjectId(context.params.city),
    //       };
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.CORPORATEOFFICER;

    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }

    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }
    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            ...matchCondition,
            // "bookingBy": { "$in": [constantUtil.CORPORATEOFFICER] },
          },
        },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "corporateUserList",
          },
        },
        { "$unwind": "$corporateUserList" },
        {
          "$project": {
            "Name": "$corporateUserList.firstName",
            "_id": "$corporateUserList._id",
            "gender": "$corporateUserList.gender",
          },
        },
        {
          "$group": {
            "_id": "$_id",
            "gender": { "$first": "$gender" },
          },
        },
        {
          "$group": {
            "_id": "$name",
            "totalCount": { "$sum": 1 },
            "gender": { "$first": "$gender" },
          },
        },
        { "$sort": { "totalCount": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};
// Corporate Officer......

bookingAction.getTopDashboardCorporateOfficerRides = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const matchCondition = {};
    matchCondition["bookingBy"] = constantUtil.COORPERATEOFFICE;

    // if (type === "instant") {
    //   // matchCondition.push({ "bookingType": { "$in": [constantUtil.INSTANT] } });
    //   matchCondition["bookingType"] = { "$in": [constantUtil.INSTANT] };
    // } else if (type === "schedule") {
    //   matchCondition["bookingType"] = { "$in": [constantUtil.SCHEDULE] };
    // } else if (type === "cancel") {
    //   // matchCondition.push({
    //   //   "bookingType": { "$in": [constantUtil.USERCANCELLED] },
    //   // });
    //   matchCondition["bookingType"] = { "$in": [constantUtil.USERCANCELLED] };
    // }
    const date = new Date(
      new Date().setDate(
        new Date().getDate() - (parseInt(context.params.daysCount) - 1)
      )
    ).setHours(0, 0, 0, 0);
    if (context.params.type === constantUtil.LIFETIME) {
      matchCondition["createdAt"] = {
        "$lte": new Date(Date.now()),
      };
    } else {
      matchCondition["createdAt"] = {
        "$gt": new Date(date),
        // "$lte": Date.now(),
      };
    }
    if (context.params?.city !== "") {
      matchCondition["category"] = mongoose.Types.ObjectId(context.params.city);
    }

    jsonData = await this.adapter.model
      .aggregate([
        {
          "$match": {
            // "bookingBy": { "$in": ["COORPERATEOFFICE"] },
            ...matchCondition,
          },
        },
        {
          "$lookup": {
            "from": "admins",
            "localField": "coorperate",
            "foreignField": "_id",
            "as": "corporateList",
          },
        },
        { "$unwind": "$corporateList" },
        {
          "$project": {
            "_id": "$coorperate",

            "all": {
              "$cond": {
                "if": {
                  "$and": [{ "$eq": ["$coorperate", "$coorperate"] }],
                },

                "then": 1,
                "else": 0,
              },
            },
            "instant": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$coorperate", "$coorperate"] },
                    { "$eq": ["$bookingType", "INSTANT"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "schedule": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$coorperate", "$coorperate"] },
                    { "$eq": ["$bookingType", "SCHEDULE"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "cancel": {
              "$cond": {
                "if": {
                  "$and": [
                    { "$eq": ["$coorperate", "$coorperate"] },
                    { "$eq": ["$bookingType", "USERCANCELLED"] },
                  ],
                },

                "then": 1,
                "else": 0,
              },
            },
            "Name": "$corporateList.data.officeName",
            "bookingType": "$bookingType",
            "bookingBy": "$bookingBy",
          },
        },

        {
          "$group": {
            "_id": "$_id",

            "instantRides": { "$sum": "$instant" },
            "allRides": { "$sum": "$all" },
            "scheduleRides": { "$sum": "$schedule" },
            "cancelRides": { "$sum": "$cancel" },

            "Name": { "$first": "$Name" },

            "bookingBy": { "$first": "$bookingBy" },
            "bookingType": { "$first": "$bookingType" },
          },
        },

        { "$sort": { "allRides": -1 } },
      ])
      .allowDiskUse(true);

    return jsonData;
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

//#endregion Dashboard

bookingAction.trackProfessionalOngoingRide = async function (context) {
  const userType = context?.params?.userType?.toUpperCase() || "";
  let responseData = {},
    errorCode = 200,
    message = "";
  try {
    const bookingData = await this.adapter.model
      .findOne({
        "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
      })
      .populate("professional", {
        "password": 0,
        "bankDetails": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .lean();
    if (!bookingData) {
      throw new MoleculerError("INVALID BOOKING DETAILS", 500);
    }
    if (bookingData.professional?.bookingInfo?.ongoingBooking !== null) {
      const ongoingBookingData = await this.adapter.model
        .findOne(
          {
            "_id": mongoose.Types.ObjectId(
              bookingData.professional?.bookingInfo?.ongoingBooking?.toString()
            ),
          },
          { "bookingStatus": 1, "origin": 1, "destination": 1 }
        )
        .lean();
      if (!ongoingBookingData) {
        throw new MoleculerError("INVALID BOOKING DETAILS", 500);
      }
      let upcomingBookingData = [];
      if (bookingData.professional?._id) {
        // upcomingBookingData = await professionalModel
        upcomingBookingData = await this.schema.professionalModel.model
          .findOne(
            {
              "_id": mongoose.Types.ObjectId(
                bookingData.professional?._id?.toString()
              ),
            },
            { "bookingInfo": 1 }
          )
          .populate("bookingInfo.upcomingBooking.bookingId", {
            "bookingStatus": 1,
            "origin": 1,
            "destination": 1,
          })
          .lean();
      }

      responseData = {
        "serviceCategory": bookingData?.serviceCategory,
        "professionalUpcomingBooking":
          upcomingBookingData?.bookingInfo?.upcomingBooking || [],
        "professionalCurrentLocation": bookingData.professional?.location
          ?.coordinates || [0, 0],
        "originAddress": ongoingBookingData.origin,
        "destinationAddress": ongoingBookingData.destination,
        "professionalOngoingBooking": ongoingBookingData,
      };
    } else {
      throw new MoleculerError("INVALID BOOKING DETAILS", 500);
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};
bookingAction.radiusBasedGetData = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "";
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    // const query = [];
    // if (
    //   context.params.createdAtFrom &&
    //   context.params.createdAtFrom != "" &&
    //   context.params.createdAtTo &&
    //   context.params.createdAtTo != ""
    // ) {
    //   const fromDate = new Date(
    //     new Date(context.params.createdAtFrom).setHours(0, 0, 0, 0)
    //   );
    //   const toDate = new Date(
    //     new Date(context.params.createdAtTo).setHours(23, 59, 59, 999)
    //   );
    //   query.push({
    //     "$match": {
    //       // "category": mongoose.Types.ObjectId("629df7daf145971ef3e79784"),
    //       "regionalData.bookingDate": { "$gte": fromDate, "$lt": toDate },
    //     },
    //   });
    // }
    // // const professionalsData = await this.adapter.model.aggregate([
    // query.push(
    //   // {
    //   //   "$match": {
    //   //     "regionalData.bookingDate": { "$gte": ["COORPERATEOFFICE"] },
    //   //   },
    //   // },
    //   {
    //     "$lookup": {
    //       "from": "professionals",
    //       "localField": "professional",
    //       "foreignField": "_id",
    //       "as": "professional",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$professional",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "admins",
    //       "localField": "vehicle.vehicleCategoryId",
    //       "foreignField": "_id",
    //       "as": "vehicleCategory",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$vehicleCategory",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     "$lookup": {
    //       "from": "categories",
    //       "localField": "category",
    //       "foreignField": "_id",
    //       "as": "serviceArea",
    //     },
    //   },
    //   {
    //     "$unwind": {
    //       "path": "$serviceArea",
    //       "preserveNullAndEmptyArrays": true,
    //     },
    //   },
    //   {
    //     // "$facet": {
    //     //   "all": [{ "$sort": { "_id": -1 } }, { "$count": "all" }],
    //     //   "response": [
    //     //     { "$sort": { "_id": -1 } },
    //     //     { "$skip": 0 },
    //     //     { "$limit": 25 },
    //     //     {
    //     "$project": {
    //       "_id": "$_id",
    //       // "regionalData": "$regionalData",
    //       "bookingDate": "$activity.bookingTime",
    //       "scheduleTime": "$bookingDate",
    //       "createdAt": "$createdAt",
    //       "acceptTime": "$activity.acceptTime",
    //       "pickUpTime": "$activity.pickUpTime",
    //       "endDateTime": "$activity.dropTime",
    //       "arriveTime": "$activity.arriveTime",
    //       "cancelTime": "$activity.cancelTime",
    //       "denyOrExpireTime": "$activity.denyOrExpireTime",
    //       "bookingId": "$bookingId",
    //       //             "bookingFor": "$bookingFor",
    //       "userName": "$bookingFor.name",
    //       "userPhoneNumber": {
    //         "$concat": ["$bookingFor.phoneCode", "$bookingFor.phoneNumber"],
    //       },
    //       "professionalName": {
    //         "$concat": ["$professional.firstName", "$professional.lastName"],
    //       },
    //       "professionalNumber": {
    //         "$concat": [
    //           "$professional.phone.code",
    //           "$professional.phone.number",
    //         ],
    //       },
    //       "bookingType": "$bookingType",
    //       "bookingStatus": "$bookingStatus",
    //       "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
    //       "stops": "$activity.rideStops",
    //       "distance": "$estimation.distance",
    //       "travelTime": "$estimation.time",
    //       "payableAmount": "$invoice.payableAmount",
    //       "driverFare": "$invoice.professionalCommision",
    //       "rideCommission": "$invoice.siteCommission",
    //       "roundingAmount": "$invoice.roundingAmount",
    //       "couponAmount": "$invoice.couponAmount",
    //       "surchargeFee": "$invoice.surchargeFee",
    //       "tollFareAmount": "$invoice.tollFareAmount",
    //       "waitingCharge": "$invoice.waitingCharge",
    //       "professionalTolerenceAmount": "$invoice.professionalTolerenceAmount",
    //       "serviceArea": "$serviceArea.locationName",
    //     },
    //     // },
    //     //   ],
    //     // },
    //   }
    // );
    // ≥≥≥≥≥≥≥≥≥≥
    // ]);
    // >>>>>>>>>>>>>>>>>>>>>
    const coordinates = [context.params.lng, context.params.lat]; // lng, lat
    responseData = await this.adapter.model
      .find(
        {
          //"clientId": mongoose.Types.ObjectId(clientId),
          //         "origin" : {"type" : "Points" , "coordinates" : [ 8.307531, 77.221832 ]}
          "pickupLocation": {
            "$near": {
              "$geometry": {
                "type": "MultiPoint",
                // "coordinates": [77.221832, 8.307531], // lng,lat
                "coordinates": coordinates, // lng,lat
                // "coordinates": [7.1802124811404076, 4.3872393066393816],
              },
              //"$nearSphere": [ 8.307531, 77.221832 ],
              "$minDistance": 0,
              "$maxDistance": context.params.radius,
            },
          },
          "bookingDate": {
            // "$gte": new Date("2023-04-01T00:00:00.795Z"),
            "$gte": new Date(context.params.bookingFrom),
            "$lt": new Date(context.params.bookingTo).setHours(23, 59, 59, 999),
          },
        },
        {
          // "pickupLocation": 1,
          "activity.bookingTime": 1,
          "activity.acceptTime": 1,
          "activity.pickUpTime": 1,
          "activity.dropTime": 1,
          "activity.arriveTime": 1,
          "activity.cancelTime": 1,
          "activity.denyOrExpireTime": 1,
          "activity.rideStops": 1,
          "createdAt": 1,
          // "regionalData": 1,
          "bookingId": 1,
          "bookingFor": 1,
          "bookingType": 1,
          "bookingStatus": 1,
          "origin.fullAddress": 1,
          "destination.fullAddress": 1,
          "estimation.distance": 1,
          "estimation.time": 1,
          "invoice.payableAmount": 1,
          "invoice.professionalCommision": 1,
          "invoice.siteCommission": 1,
          "invoice.roundingAmount": 1,
          "invoice.couponAmount": 1,
          "invoice.waitingCharge": 1,
          "invoice.tollFareAmount": 1,
          "invoice.airportFareAmount": 1,
          "invoice.surchargeFee": 1,
          "invoice.professionalTolerenceAmount": 1,
          "invoice.beforeRideWaitingMins": 1,
          "invoice.beforeRideWaitingGracePeriod": 1,
          "invoice.afterRideWaitingMins": 1,
          "invoice.afterRideWaitingGracePeriod": 1,
          "bookingDate": 1,
          "distanceUnit": 1,
          "professional": 1,
          // "vehicle.vehicleCategoryId": 1,
          // "admins": 1,
        }
      )
      .populate("professional", {
        "firstName": 1,
        "lastName": 1,
        "phone": 1,
        // "vehicles": 1,
      })
      .sort({ "bookingDate": 1 })
      .lean();
    // .populate({
    //   "model": "vehicle.vehicleCategoryId",
    //   "path": "admins",
    // })
    // .populate({
    //   "path": "admins",
    //   "model": "professional.vehicles[0][0].vehicleCategoryId",
    //   // "$match": { "data.vehicleCategory": "vehicle.vehicleCategoryId" },
    //   "select": "data.vehicleCategory",
    // });
    // responseData = await this.adapter.model.aggregate(query);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};
bookingAction.totalRideReportData = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "";
  const clientId = context.params.clientId || context.meta.clientId;
  try {
    const query = [];
    if (
      context.params.bookingFrom &&
      context.params.bookingFrom != "" &&
      context.params.bookingTo &&
      context.params.bookingTo != ""
    ) {
      const fromDate = new Date(
        new Date(context.params.bookingFrom).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.bookingTo).setHours(23, 59, 59, 999)
      );
      query.push({
        "$match": {
          "bookingDate": { "$gte": fromDate, "$lt": toDate },
        },
      });
    }
    if (context.params.bookingId) {
      query.push({
        "$match": {
          "bookingId": {
            "$regex": context.params.bookingId?.trim(),
            "$options": "si",
          },
        },
      });
    }
    if (context.params.corporateId) {
      query.push({
        "$match": {
          "coorperate": mongoose.Types.ObjectId(context.params.corporateId),
        },
      });
    }
    if (
      context.params.bookingStatus &&
      context.params.bookingStatus !== "ALL"
    ) {
      query.push({
        "$match": {
          "bookingStatus": context.params.bookingStatus,
        },
      });
    }
    // const professionalsData = await this.adapter.model.aggregate([
    query.push(
      // {
      //   "$match": {
      //     "regionalData.bookingDate": { "$gte": ["COORPERATEOFFICE"] },
      //   },
      // },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professional",
        },
      },
      {
        "$unwind": {
          "path": "$professional",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "admins",
          "localField": "vehicle.vehicleCategoryId",
          "foreignField": "_id",
          "as": "vehicleCategory",
        },
      },
      {
        "$unwind": {
          "path": "$vehicleCategory",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "categories",
          "localField": "category",
          "foreignField": "_id",
          "as": "serviceArea",
        },
      },
      {
        "$unwind": {
          "path": "$serviceArea",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );
    if (
      context.params.vehicleCategory &&
      context.params.vehicleCategory !== "ALL"
    ) {
      query.push({
        "$match": {
          "vehicleCategory._id": mongoose.Types.ObjectId(
            context.params.vehicleCategory
          ),
        },
      });
    }
    if (context.params.serviceArea && context.params.serviceArea !== "ALL") {
      query.push({
        "$match": {
          "category": mongoose.Types.ObjectId(context.params.serviceArea),
        },
      });
    }
    query.push({
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          {
            "$project": {
              "_id": "$_id",
              // "regionalData": "$regionalData",
              "bookingDate": "$activity.bookingTime",
              "scheduleTime": "$bookingDate",
              "createdAt": "$createdAt",
              "acceptTime": "$activity.acceptTime",
              "pickUpTime": "$activity.pickUpTime",
              "endDateTime": "$activity.dropTime",
              "arriveTime": "$activity.arriveTime",
              "cancelTime": "$activity.cancelTime",
              "denyOrExpireTime": "$activity.denyOrExpireTime",
              "bookingId": "$bookingId",
              //             "bookingFor": "$bookingFor",
              "userName": "$bookingFor.name",
              "userPhoneNumber": {
                "$concat": [
                  "$bookingFor.phoneCode",
                  " ",
                  "$bookingFor.phoneNumber",
                ],
              },
              "professionalName": {
                "$concat": [
                  "$professional.firstName",
                  " ",
                  "$professional.lastName",
                ],
              },
              "professionalNumber": {
                "$concat": [
                  "$professional.phone.code",
                  " ",
                  "$professional.phone.number",
                ],
              },
              "bookingType": "$bookingType",
              "bookingStatus": "$bookingStatus",
              "vehicleCategory": "$vehicleCategory.data.vehicleCategory",
              "stops": "$activity.rideStops",
              "beforeRideWaitingMins": "$invoice.beforeRideWaitingMins",
              "beforeRideWaitingGracePeriod":
                "$invoice.beforeRideWaitingGracePeriod",
              "afterRideWaitingMins": "$invoice.afterRideWaitingMins",
              "afterRideWaitingGracePeriod":
                "$invoice.afterRideWaitingGracePeriod",
              "distance": "$estimation.distance",
              "travelTime": "$estimation.time",
              "payableAmount": "$invoice.payableAmount",
              "driverFare": "$invoice.professionalCommision",
              "rideCommission": "$invoice.siteCommission",
              "roundingAmount": "$invoice.roundingAmount",
              "couponAmount": "$invoice.couponAmount",
              "surchargeFee": "$invoice.surchargeFee",
              "tollFareAmount": "$invoice.tollFareAmount",
              "airportFareAmount": "$invoice.airportFareAmount",
              "waitingCharge": "$invoice.waitingCharge",
              "professionalTolerenceAmount":
                "$invoice.professionalTolerenceAmount",
              "serviceArea": "$serviceArea.locationName",
              "notes": "$notes",
            },
          },
          { "$sort": { "bookingDate": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
        ],
      },
    });

    responseData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};

bookingAction.changeBookingDate = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "";
  const clientId = context.params.clientId || context.meta.clientId;
  const { SOMETHING_WENT_WRONG } = notifyMessage.setNotifyLanguage(
    context.params.langCode
  );
  try {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
        //"clientId": mongoose.Types.ObjectId(clientId),
      },
      {
        "$set": {
          "bookingDate": context.params.bookingDate,
        },
      }
    );
    await this.broker.emit("booking.updateUpcommingScheduledRideInRedis");
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};

bookingAction.sendPromotion = async function (context) {
  let responseData = null;
  const clientId = context.params?.clientId || context.meta?.clientId;
  const requestFrom = context.params?.requestFrom;
  if (context.params.userType === constantUtil.USER) {
    responseData = await this.broker.emit("user.sendPromotion", {
      ...context.params,
      "requestFrom": requestFrom,
      "clientId": clientId,
    });
  } else if (context.params.userType === constantUtil.PROFESSIONAL) {
    responseData = await this.broker.emit("professional.sendPromotion", {
      ...context.params,
      "requestFrom": requestFrom,
      "clientId": clientId,
    });
  }
  responseData = responseData && responseData[0];
  if (responseData?.trustedContactsLog?.length > 0) {
    await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.rideId.toString()),
        //"clientId": mongoose.Types.ObjectId(clientId),
      },
      { "$push": { "trustedContactsLog": responseData.trustedContactsLog } }
    );
    // await this.adapter.model
    //   .findOneAndUpdate(
    //     {
    //       "_id": mongoose.Types.ObjectId(context.params.rideId.toString()),
    //       //"clientId": mongoose.Types.ObjectId(clientId),
    //     },
    //     { "$push": { "trustedContactsLog": responseData.trustedContactsLog } },
    //     { "new": true }
    //   )
    //   .lean();
  }
  return { "code": 200, "data": {}, "message": "requested successfully" };
};

//----------------------------------
module.exports = bookingAction;
