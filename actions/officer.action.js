const { customAlphabet } = require("nanoid");
const { MoleculerError } = require("moleculer").Errors;
const moment = require("moment");
const _ = require("lodash");
const mongoose = require("mongoose");

const cryptoUtils = require("../utils/crypto.util");
const constantUtil = require("../utils/constant.util");
const helperUtil = require("../utils/helper.util");
const storageUtil = require("../utils/storage.util");
const { officerObject } = require("../utils/mapping.util");

const notifyMessage = require("../mixins/notifyMessage.mixin");

const officerAction = {};

//#region Response Officer

officerAction.login = async function (context) {
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  context.params.otp = customAlphabet("1234567890", 4)();

  let responseData = await this.adapter.model.updateOne(
    {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
    },
    {
      "otp": cryptoUtils.hashSecret(context.params.otp),
    }
  );

  if (!responseData || (responseData.n === 0 && responseData.nModified === 0))
    responseData = await this.adapter.model.create({
      //"clientId": context.params.clientId,
      "phone.code": context.params.phoneCode,
      "phone.number": context.params.phoneNumber,
      "otp": cryptoUtils.hashSecret(context.params.otp),
    });
  if (!responseData) {
    throw new MoleculerError("INVALID PROFESSIONAL DATA");
  }
  this.broker.emit("admin.sendSMS", {
    "phoneNumber": `${context.params.phoneCode}${context.params.phoneNumber}`,
    "message": "",
    "notifyType": constantUtil.OTP,
    "otpReceiverAppName": constantUtil.SMS,
  });

  return {
    "code": 200,
    "message": "OTP sended successfully",
    "OTP":
      smsSetting.data.mode === constantUtil.DEVELOPMENT
        ? context.params.otp
        : "",
  };
};

officerAction.verifyOtp = async function (context) {
  const gerenalsetting = await storageUtil.read(constantUtil.GENERALSETTING);
  const checkData =
    gerenalsetting.data.smsType === constantUtil.TWILIO
      ? {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
          "otp": cryptoUtils.hashSecret(context.params.otp),
        }
      : {
          //"clientId": mongoose.Types.ObjectId(context.params.clientId),
          "phone.code": context.params.phoneCode,
          "phone.number": context.params.phoneNumber,
        };
  const responseData = await this.adapter.model.findOne(checkData).lean();
  if (!responseData) {
    throw new MoleculerError("Invalid user details", "401", "AUTH_FAILED");
  }
  const accessToken = cryptoUtils.jwtSign({
    "_id": responseData._id,
    "type": constantUtil.OFFICER,
    "deviceType": context.meta.userAgent.deviceType,
    "platform": context.meta.userAgent.platform,
  });

  let updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(responseData._id.toString()),
    {
      "otp": "",
      "deviceInfo": [
        {
          "accessToken": accessToken,
          "deviceId": context.meta.userAgent.deviceId,
          "deviceType": context.meta.userAgent.deviceType,
          "platform": context.meta.userAgent.platform,
        },
      ],
      "status": responseData.status || constantUtil.ACTIVE,
    }
  );
  if (!updateData) {
    throw new MoleculerError("Invalid data");
  } else {
    updateData = updateData.toJSON();
    updateData.newOfficer =
      updateData.email && updateData.firstName && updateData.lastName
        ? false
        : true;
    updateData.accessToken = accessToken;

    return { "code": 200, "data": officerObject(updateData) };
  }
};

officerAction.getProfile = async function (context) {
  const responseData = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.meta.officerId.toString()))
    .lean();
  responseData.newOfficer =
    responseData.email && responseData.firstName && responseData.lastName
      ? false
      : true;
  return { "code": 200, "data": officerObject(responseData) };
};

officerAction.updateProfile = async function (context) {
  const { INFO_INVALID_DATE_FORMAT, ALERT_DUPLICATE_EMAIL } =
    notifyMessage.setNotifyLanguage(context.params.langCode);
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const checkEmailCount = await this.adapter.model.countDocuments({
    "_id": {
      "$ne": mongoose.Types.ObjectId(context.params.id.toString()),
    },
    "email": context.params.email,
  });
  if (checkEmailCount > 0) {
    throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
  }
  if (
    !moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"], true).isValid()
  ) {
    throw new MoleculerError(INFO_INVALID_DATE_FORMAT);
  }
  const age =
    moment().year() -
    moment(context.params.dob, ["MM-DD-YYYY", "MM/DD/YYYY"]).year();
  if (age < parseInt(generalSettings.data.officerMinAge)) {
    throw new MoleculerError(
      "AGE MUST BE" +
        parseInt(generalSettings.data.officerMinAge) +
        " AND ABOVE TO PROCEED"
    );
  }
  context.params.uniqueCode = `${context.params.firstName.slice(
    0,
    4
  )}${helperUtil.randomString(6, "a")}`;

  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "files": context.params.files,
      "fileName": `avatar${context.params.uniqueCode}${constantUtil.OFFICER}`,
    });
    context.params.avatar = s3Image && s3Image[0];
  }
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.officerId.toString()),
    {
      "uniqueCode": context.params.uniqueCode,
      "firstName": context.params.firstName,
      "lastName": context.params.lastName,
      "email": context.params.email,
      "gender": context.params.gender,
      "avatar": context.params.avatar,
      "referredBy": context.params.referredBy
        ? context.params.referredBy
        : null,
      "dob": context.params.dob,
      "age": age,
      "currencyCode": generalSettings.data.currencyCode.toUpperCase(),
    }
  );
  if (!updateData) {
    throw new MoleculerError("Invalid data");
  } else {
    return { "code": 200, "data": officerObject(updateData) };
  }
};

officerAction.updateLiveLocation = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.officerId.toString()),
    {
      "location.coordinates": [context.params.lng, context.params.lat],
      "currentBearing": context.params.currentBearing,
      "altitude": context.params.altitude,
      "horizontalAccuracy": context.params.horizontalAccuracy,
      "verticalAccuracy": context.params.verticalAccuracy,
      "speed": context.params.speed,
      "isLocationUpdated": true,
      "locationUpdatedTime": new Date(),
    }
  );
  if (!updateData) {
    return { "code": 200, "data": {}, "message": "FAILED TO UPDATE LOCATION" };
  }
  return {
    "code": 200,
    "data": {
      "onlineStatus": updateData.onlineStatus,
      "bookingInfo": updateData.bookingInfo,
    },
    "message": "LOCATION UPDATED SUCCESSFULLY",
  };
};

officerAction.updatePermissionData = async function (context) {
  const updateData = {
    "osType": context.params.osType ? context.params.osType : null,
    "brand": context.params.brand ? context.params.brand : null,
    "model": context.params.model ? context.params.model : null,
    "osVersion": context.params.osVersion ? context.params.osVersion : null,
    "screenSize": context.params.screenSize ? context.params.screenSize : null,
    "internetConnectivityMedium": context.params.internetConnectivityMedium
      ? context.params.internetConnectivityMedium
      : null,
    "token": context.params.token ? context.params.token : null,
    "fcmId": context.params.fcmId ? context.params.fcmId : null,
    "deviceLocationStatus": context.params.deviceLocationStatus
      ? context.params.deviceLocationStatus
      : null,
    "appLocationStatus": context.params.appLocationStatus
      ? context.params.appLocationStatus
      : null,
    "baseUrl": context.params.baseUrl ? context.params.baseUrl : null,
    "appVersionInServer": context.params.appVersionInServer
      ? context.params.appVersionInServer
      : null,
    "appVersionInStore": context.params.appVersionInStore
      ? context.params.appVersionInStore
      : null,
    "updatedTime": new Date(),
    "deviceTime": context.params.deviceTime ? context.params.deviceTime : null,
    "timezoneInNumber": context.params.timezoneInNumber
      ? context.params.timezoneInNumber
      : null,
    "timezoneInformat": context.params.timezoneInformat
      ? context.params.timezoneInformat
      : null,
    "connectivityMediumName": context.params.connectivityMediumName
      ? context.params.connectivityMediumName
      : null,
    "socketConnetivity": context.params.socketConnetivity
      ? context.params.socketConnetivity
      : null,
  };
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.officerId.toString()),
    {
      "$set": {
        "permissionData": updateData,
        "location.coordinates": [
          context.params.currentLocation.lng,
          context.params.currentLocation.lat,
        ],
      },
    }
  );
  if (!responseJson) {
    throw new MoleculerError("ERROR IN UPDATE PERMISSION DATA");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "PERMISSION DATA IS UPDATED SUCCESSFULLY",
    };
  }
};

officerAction.updateOnlineStatus = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.officerId.toString()),
    {
      "onlineStatus": context.params.onlineStatus,
    }
  );
  if (!updateData) {
    throw new MoleculerError("FAILED TO UPDATE ONLINE STATUS");
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "STATUS UPDATED SUCCESSFULLY",
    };
  }
};

officerAction.logout = async function (context) {
  const updateData = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.meta.officerId.toString()),
    {
      "deviceInfo": [],
      "onlineStatus": false,
    }
  );
  if (!updateData) {
    throw new MoleculerError("Professional not found", "401", "AUTH_FAILED");
  } else {
    return { "code": 200, "message": "Device logout successfully" };
  }
};

officerAction.trackOfficerLocation = async function (context) {
  const jsonData = await this.adapter.model.findById(
    mongoose.Types.ObjectId(context.params.officerId.toString())
  );
  if (!jsonData) {
    throw new MoleculerError("ERROR IN LOCATION DETAILS");
  } else {
    const locationData = {
      "lat": jsonData.location
        ? jsonData.location.coordinates[1]
          ? jsonData.location.coordinates[1]
          : 0
        : 0,
      "lng": jsonData.location
        ? jsonData.location.coordinates[0]
          ? jsonData.location.coordinates[0]
          : 0
        : 0,
      "currentBearing": jsonData.currentBearing
        ? jsonData.currentBearing
        : null,
      "altitude": jsonData.altitude ? jsonData.altitude : null,
      "speed": jsonData.speed ? jsonData.speed : null,
      "horizontalAccuracy": jsonData.horizontalAccuracy
        ? jsonData.horizontalAccuracy
        : null,
      "verticalAccuracy": jsonData.verticalAccuracy
        ? jsonData.verticalAccuracy
        : null,
      "isLocationUpdated": jsonData.isLocationUpdated
        ? jsonData.isLocationUpdated
        : null,
      "locationUpdatedTime": jsonData.locationUpdatedTime
        ? jsonData.locationUpdatedTime
        : null,
    };
    return { "code": 200, "data": locationData, "message": "" };
  }
};

officerAction.getOfficerData = async function (context) {
  const todayCount = await this.adapter.count({
    "query": {
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "createdAt": { "$gt": new Date().setHours(0, 0, 0, 0) },
      // "category": mongoose.Types.ObjectId(context.params.city),
    },
  });
  const date = new Date(
    new Date().setDate(
      new Date().getDate() - (parseInt(context.params.daysCount) - 1)
    )
  ).setHours(0, 0, 0, 0);

  const matchData =
    context.params.type === constantUtil.LIFETIME
      ? {
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
            ],
          },
        }
      : {
          "status": {
            "$in": [
              constantUtil.ACTIVE,
              constantUtil.INACTIVE,
              constantUtil.ARCHIVE,
              constantUtil.INCOMPLETE,
            ],
          },
          "createdAt": {
            "$gt": date,
            "$lte": Date.now(),
          },
        };
  const data = await this.adapter.model.find(matchData, {
    "status": 1,
    "createdAt": 1,
  });
  let inactiveCount = 0,
    totalCount = 0,
    activeCount = 0,
    archiveCount = 0,
    triedCount = 0;

  _.map(data, async (data) => {
    switch (data.status) {
      case constantUtil.INACTIVE:
        inactiveCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ACTIVE:
        activeCount += 1;
        totalCount += 1;
        break;
      case constantUtil.ARCHIVE:
        archiveCount += 1;
        break;
      case constantUtil.INCOMPLETE:
        triedCount += 1;
        break;
    }
  });

  const dataSet = {};
  let graphData = await _.reduce(
    data,
    (set, value) => {
      const date = value.createdAt.toISOString().split("T")[0];
      if (!dataSet[date]) dataSet[date] = [];
      dataSet[date].push(value);
      return dataSet;
    },
    0
  );
  graphData = Object.keys(graphData).map((date) => {
    return {
      date,
      "count": graphData[date].length,
    };
  });

  const responseJson = {
    "totalCount": totalCount,
    "activeCount": activeCount,
    "inactiveCount": inactiveCount,
    "archiveCount": archiveCount,
    "triedCount": triedCount,
    "todayCount": todayCount,
    "graphData": graphData,
  };
  return responseJson;
};

officerAction.getRatings = async function (context) {
  let rideData = await this.broker.emit("securityEscort.getOfficerRatingList", {
    "officerId": context.meta.officerId.toString(),
    "skip": context.params.skip,
    "limit": context.params.limit,
  });
  rideData = rideData && rideData[0];
  if (!rideData) {
    throw new MoleculerError("ERROR IN USER RATING DETAILS");
  } else {
    return {
      "code": 200,
      "data": rideData,
    };
  }
};
//#endregion Response Officer

//#region  Corporate Officer

//--------- Corporate Ride Start ----------------
officerAction.getCorporateDetailsByUserId = async function (context) {
  // const userType = context.params.userType;
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};

  //------------ Notification Message Start ---------
  languageCode = context.meta?.userDetails?.languageCode;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  //------------ Notification Message End ---------
  try {
    responseJson = await this.broker.emit(
      "officer.getCorporateDetailsByUserId",
      {
        //"userId": context.meta.userId.toString(),
        "userId": context.params.userId.toString(),
      }
    );
    // ------------- Response Data Start ----------
    responseJson = responseJson && responseJson[0];
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};
//--------- Corporate Ride End ----------------

officerAction.getCorporateOfficerList = async function (context) {
  // const userType = context.params.userType;
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};

  //------------ Notification Message Start ---------
  languageCode = context.meta?.userDetails?.languageCode;
  const clientId = context.params.clientId || context.meta.clientId;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  //------------ Notification Message End ---------
  try {
    responseJson = await this.broker.emit("officer.getCorporateOfficerList", {
      "clientId": clientId,
      ...context.params,
    });
    // ------------- Response Data Start ----------
    responseJson = responseJson && responseJson[0];
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    ...responseJson, //To Pass value multiple Parameters
  };
};

officerAction.manageCorporateOfficer = async function (context) {
  // const userType = context.params.userType;
  let errorCode = 200,
    message = "",
    errorMessage = "",
    languageCode = "";
  // responseJson = {};

  //------------ Notification Message Start ---------
  languageCode = context.meta?.userDetails?.languageCode;
  const clientId = context.params.clientId;
  const {
    SOMETHING_WENT_WRONG,
    CREATED,
    UPDATED,
    INFO_OFFICER_ALREADY_EXISTS,
    INFO_INVALID_DETAILS,
    ALERT_DUPLICATE_EMAIL,
  } = notifyMessage.setNotifyLanguage(languageCode);

  //------------ Notification Message End ---------
  try {
    if (!context.params.corporate) {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
    const checkEmailCondition = context.params?.id
      ? {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "_id": { "$ne": mongoose.Types.ObjectId(context.params?.id) },
          "corporate": context.params.corporate,
          "email": context.params.email,
        }
      : {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "corporate": context.params.corporate,
          "email": context.params.email,
        };
    const checkEmailCount = await this.adapter.model.countDocuments(
      checkEmailCondition
    );
    if (checkEmailCount > 0) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    let userDetails = await this.broker.emit(
      "user.updateAndGetCororateOfficerByPhoneNumber",
      {
        "clientId": clientId,
        "phoneNumber": context.params.phoneNumber,
        "phoneCode": context.params.phoneCode,
        "email": context.params.email,
        "firstName": context.params.firstName,
        "lastName": context.params.lastName,
        "gender": context.params.gender,
        "languageCode": languageCode,
      }
    );
    userDetails = userDetails && userDetails[0];
    if (userDetails.code === 400) {
      throw new MoleculerError(ALERT_DUPLICATE_EMAIL);
    }
    const recordCount = await this.adapter.model.count({
      "userType": constantUtil.CORPORATEOFFICER,
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      "corporate": context.params.corporate,
      "phone.number": context.params.phoneNumber,
      "phone.code": context.params.phoneCode,
    });
    if (!context.params?.id) {
      if (recordCount === 0) {
        const updateData = await this.adapter.model.create({
          "userType": constantUtil.CORPORATEOFFICER,
          "clientId": clientId,
          "employeeId": context.params.employeeId,
          "email": context.params.email,
          "isEmailVerified": context.params.isEmailVerified || false,
          "corporate": context.params.corporate,
          "firstName": context.params.firstName,
          "lastName": context.params.lastName,
          "gender": context.params.gender,
          "phone.number": context.params.phoneNumber,
          "phone.code": context.params.phoneCode,
          "user": userDetails.userId,
          "status": constantUtil.ACTIVE,
          "isActive": true,
          "isDeleted": false,
          // "isActive": context.params.isActive,
        });
        message = CREATED;
      } else {
        errorCode = 202;
        message = INFO_OFFICER_ALREADY_EXISTS;
      }
    } else {
      if (recordCount <= 1) {
        const updateData = await this.adapter.model.updateOne(
          { "_id": mongoose.Types.ObjectId(context.params.id.toString()) },
          {
            "$set": {
              "employeeId": context.params.employeeId,
              "email": context.params.email,
              "isEmailVerified": context.params.isEmailVerified || false,
              "corporate": context.params.corporate,
              "firstName": context.params.firstName,
              "lastName": context.params.lastName,
              "phone.number": context.params.phoneNumber,
              "phone.code": context.params.phoneCode,
              "gender": context.params.gender,
              "status": constantUtil.ACTIVE,
              "isActive": context.params.isActive,
            },
          }
        );
        message = UPDATED;
      } else {
        errorCode = 202;
        message = INFO_OFFICER_ALREADY_EXISTS;
      }
    }
    // message = UPDATED;

    // // ------------- Response Data Start ----------
    // responseJson = responseJson && responseJson[0];
    // // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    errorMessage = e.message;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "error": errorMessage,
    "data": "",
  };
};

officerAction.getCorporateOfficerById = async function (context) {
  // const userType = context.params.userType;
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};

  //------------ Notification Message Start ---------
  languageCode =
    context.params.langCode || context.meta?.userDetails?.languageCode;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  //------------ Notification Message End ---------
  try {
    responseJson = await this.broker.emit("officer.getCorporateOfficerById", {
      //"userId": context.meta.userId.toString(),
      "id": context.params.id.toString(),
    });
    // ------------- Response Data Start ----------
    responseJson = responseJson && responseJson[0];
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

officerAction.checkCorporateOfficerByPhoneNumber = async function (context) {
  // const userType = context.params.userType;
  let errorCode = 200,
    message = "",
    languageCode = "",
    responseJson = {};

  //------------ Notification Message Start ---------
  languageCode =
    context.params.langCode || context.meta?.userDetails?.languageCode;
  const { SOMETHING_WENT_WRONG } =
    notifyMessage.setNotifyLanguage(languageCode);
  //------------ Notification Message End ---------
  try {
    responseJson = await this.broker.emit(
      "officer.checkCorporateOfficerByPhoneNumber",
      {
        "clientId": context.params.clientId,
        "corporateId": context.params.corporateId.toString(),
        "phoneCode": context.params.phoneCode,
        "phoneNumber": context.params.phoneNumber,
      }
    );
    // ------------- Response Data Start ----------
    responseJson = responseJson && responseJson[0];
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    // "count": responseJson?._id || 0,
    "data": responseJson || {},
  };
};
officerAction.changeCorporateOfficerData = async function (context) {
  const ids = context.params.ids.map((e) => mongoose.Types.ObjectId(e));
  const responseJson = await this.adapter.model.updateMany(
    {
      "_id": { "$in": ids },
      "userType": constantUtil.CORPORATEOFFICER,
      //"clientId": mongoose.Types.ObjectId(context.params.clientId),
    },
    {
      "status": context.params.status,
      // "isActive": context.params.isActive,
      // "isDeleted": context.params.isDeleted,
      // "data.accessToken": "",
    }
  );
  if (responseJson.nModified === 0) {
    throw new MoleculerError("Error in status change");
  } else {
    return { "code": 1, "response": "Updated Successfully" };
  }
};

officerAction.searchCorporateOfficerByName = async function (context) {
  let errorCode = 200,
    message = "",
    errorMessage = "",
    jsonData = {};
  try {
    const query = [];
    // FOR EXPORT DATE FILTER

    // USER STATUS
    query.push({
      "$match": {
        "status": constantUtil.ACTIVE,
        "corporate": mongoose.Types.ObjectId(
          context.params.corporateId?.toString()
        ),
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
    });
    // USER NAME
    if (context.params.name) {
      query.push(
        {
          "$addFields": {
            "fullName": {
              "$concat": ["$firstName", " ", "$lastName"],
            },
          },
        },
        {
          "$match": {
            "$or": [
              {
                "firstName": {
                  "$regex": context.params.name,
                  "$options": "si",
                },
              },
              {
                "lastName": { "$regex": context.params.name, "$options": "si" },
              },
              {
                "fullName": { "$regex": context.params.name, "$options": "si" },
              },
              {
                "phone.number": {
                  "$regex": context.params.name,
                  "$options": "si",
                },
              },
            ],
          },
        }
      );
    }
    query.push(
      // {
      //   "$lookup": {
      //     "from": "users",
      //     "localField": "user",
      //     "foreignField": "_id",
      //     "as": "userData",
      //   },
      // },
      // {
      //   "$unwind": {
      //     "path": "$userData",
      //     "preserveNullAndEmptyArrays": true,
      //   },
      // },
      // // {
      // //   "$lookup": {
      // //     "from": "categories",
      // //     "localField": "serviceAreaId",
      // //     "foreignField": "_id",
      // //     "as": "locationName",
      // //   },
      // // },
      // // {
      // //   "$unwind": {
      // //     "path": "$locationName",
      // //     "preserveNullAndEmptyArrays": true,
      // //   },
      // // },
      {
        "$facet": {
          "totalCount": [
            {
              "$count": "count",
            },
          ],
          "responseData": [
            {
              "$project": {
                "_id": "$_id",
                "firstName": "$firstName",
                "lastName": "$lastName",
                "email": "$email",
                // "phone": "$phone",
                "phoneCode": "$phone.code",
                "phoneNumber": "$phone.number",
                "status": "$status",
                "createdAt": "$createdAt",
                "updatedAt": "$updatedAt",
                // "locationName": "$locationName.locationName",
                // "serviceAreaId": "$serviceAreaId",

                // "walletAmount": "$wallet.availableAmount",
                // "rewardPoints": "$wallet.rewardPoints",
                // "gender": "$gender",
              },
            },
            { "$sort": { "createdAt": -1 } },
            { "$skip": 0 },
            { "$limit": 10 },
          ],
        },
      }
    );
    jsonData = await this.adapter.model.aggregate(query).allowDiskUse(true);
  } catch (e) {
    errorCode = 400;
    errorMessage = e.message;
    message = "SOMETHING_WENT_WRONG";
  }
  return {
    "code": errorCode,
    "error": errorMessage,
    "message": message,
    "data": jsonData,
  };
};

//#endregion Corporate Officer

module.exports = officerAction;
