const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { convertToFloat, convertToInt } = require("../utils/common.util");
const { toRegionalUTC } = require("../utils/helper.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const logAction = {};

logAction.getNotificationBasedOnUserType = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    responseData = {},
    matchCondition = {},
    userId,
    professionalId,
    languageCode;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
    userId = context.params?.userId;
    matchCondition = {
      "userId": mongoose.Types.ObjectId(userId?.toString()),
      "userType": userType,
    };
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
    professionalId = context.params?.professionalId;
    matchCondition = {
      "professionalId": mongoose.Types.ObjectId(professionalId?.toString()),
      "userType": userType,
    };
  }
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.langCode || languageCode);
  try {
    responseData = await this.broker.emit(
      //   "log.getNotificationBasedOnUserType",
      "admin.getNotificationBasedOnUserType", //Need to Remove
      {
        "userType": userType,
        "professionalId": professionalId,
        "userId": userId,
      }
    );
    // ------------- Response Data Start ----------
    responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseData,
  };
};

logAction.manageLog = async function (context) {
  const userType = (context.params?.userType ?? "").toUpperCase();
  let errorCode = 200,
    message = "",
    // addedUserData = {},
    languageCode;

  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
    // addedUserData = {
    //   "userId": context.params?.userId?.toString(),
    // };
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
    // addedUserData = {
    //   "professionalId": context.params?.professionalId?.toString(),
    // };
  }
  const { SOMETHING_WENT_WRONG, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params?.langCode || languageCode);
  try {
    await this.adapter.insert({
      "clientId": context.params.clientId || null,
      "name": context.params.name,
      "type": context.params.type || null,
      "userType": userType,
      "userId": context.params?.userId?.toString(),
      "professionalId": context.params?.professionalId?.toString(),
      "reason": context.params.reason,
      "trackData": context.params.trackData,
      "logDate": new Date(),
      "regionalLogDate": toRegionalUTC(new Date()),
    });
    //Delete Log History (30 Days older Records)
    await this.adapter.model.deleteMany({
      "name": context.params.name,
      "type": context.params.type || null,
      "logDate": { "$lt": new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) }, //30 Days older Records
      "userType": userType,
    });
    // ------------- Response Data Start ----------
    // responseData = responseData && responseData[0];
    message = INFO_SUCCESS;
    // ------------- Response Data End ----------
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    // responseData = {};
  }
  return {
    "code": errorCode,
    "message": message,
    "data": {},
  };
};

logAction.errorlogReportData = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    fromDate = null,
    toDate = null;
  const query = [];
  try {
    //---------------------------------
    if (context.params.logFrom && context.params.logFrom !== "") {
      fromDate = new Date(
        new Date(context.params.logFrom).setHours(0, 0, 0, 0)
      );
      toDate = new Date(
        new Date(context.params.logFrom).setHours(23, 59, 59, 999)
      );
    }
    if (context.params.logTo && context.params.logTo !== "") {
      if (!fromDate) {
        fromDate = new Date(
          new Date(context.params.logTo).setHours(0, 0, 0, 0)
        );
      }
      toDate = new Date(
        new Date(context.params.logTo).setHours(23, 59, 59, 999)
      );
    }
    if (!fromDate) {
      fromDate = new Date(new Date().setHours(0, 0, 0, 0));
    }
    if (!toDate) {
      toDate = new Date(new Date().setHours(23, 59, 59, 999));
    }

    //---------------------------------
    query.push({
      "$match": {
        "logDate": { "$gte": fromDate, "$lt": toDate },
      },
    });
    // if (context.params.errorType) {
    //   query.push({
    //     "$match": {
    //       "name": context.params.errorType,
    //     },
    //   });
    // }
    query.push(
      {
        "$match": {
          "name": constantUtil.ERRORDATA,
        },
      },
      {
        "$lookup": {
          "from": "users",
          "localField": "user",
          "foreignField": "_id",
          "as": "userData",
        },
      },
      {
        "$unwind": {
          "path": "$userData",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "professionals",
          "localField": "professional",
          "foreignField": "_id",
          "as": "professionalData",
        },
      },
      {
        "$unwind": {
          "path": "$professionalData",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );
    query.push({
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          {
            "$project": {
              "_id": "$_id",
              "name": "$name",
              "logDate": "$logDate",
              "userType": "$userType",
              "reason": "$reason",
              "trackData": "$trackData",
              // "userName": {
              //   "$concat": ["$userData.firstName", " ", "$userData.lastName"],
              // },
              // "userPhoneNumber": {
              //   "$concat": [
              //     "$userData.phone.code",
              //     " ",
              //     "$userData.phone.number",
              //   ],
              // },
            },
          },
          { "$sort": { "logDate": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
        ],
      },
    });
    responseData = await this.schema.adapter.model
      .aggregate(query)
      .allowDiskUse(true);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};

logAction.getLoginHistoryByloginId = async function (context) {
  const userType = context.params.userType.toUpperCase();
  let languageCode,
    responseData = [],
    errorCode = 200,
    message = "",
    count = 0;
  if (userType === constantUtil.USER) {
    languageCode = context.meta?.userDetails?.languageCode;
  } else if (userType === constantUtil.PROFESSIONAL) {
    languageCode = context.meta?.professionalDetails?.languageCode;
  }
  const { SOMETHING_WENT_WRONG } = await notifyMessage.setNotifyLanguage(
    languageCode
  );
  try {
    const query = {
      // "type": constantUtil.CONST_LOG,
      // "clientId": context.params.clientId,
      "name": constantUtil.LOGIN,
      "professional": mongoose.Types.ObjectId(context.params?.loginId),
      "isActive": true,
    };
    // FOR EXPORT DATE FILTER
    if (
      !!context.params?.fromDate === true &&
      !!context.params?.toDate === true
    ) {
      const fromDate = new Date(
        new Date(context.params.fromDate).setHours(0, 0, 0, 0)
      );
      const toDate = new Date(
        new Date(context.params.toDate).setHours(23, 59, 59, 999)
      );
      query["createdAt"] = { "$gte": fromDate, "$lt": toDate };
    }
    // const searchQuery = {
    //   "$match": {
    //     "$or": [
    //       {
    //         "data.name": {
    //           "$regex": context.params.search + ".*",
    //           "$options": "si",
    //         },
    //       },
    //     ],
    //   },
    // };
    const jsonData = await this.adapter.model.aggregate([
      {
        "$match": {
          ...query,
        },
      },
      {
        "$facet": {
          "all": [{ "$count": "all" }],
          "response": [
            {
              "$project": {
                "_id": "$_id",
                "type": "$type",
                "logDate": "$logDate",
                "logInTime": "$logInTime",
                "logOutTime": "$logOutTime",
                "onlineMinutes": "$onlineMinutes",
              },
            },
            { "$sort": { "_id": -1 } },
            { "$skip": parseInt(context.params?.skip || 0) },
            { "$limit": parseInt(context.params?.limit || 0) },
          ],
        },
      },
    ]);

    count = jsonData[0]?.all[0]?.all || 0;
    responseData = jsonData[0]?.response || [];
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
    responseData = [];
    count = 0;
  }
  return {
    "code": errorCode,
    "message": message,
    "count": count,
    "response": responseData,
  };
};
//-------------------------
module.exports = logAction;
