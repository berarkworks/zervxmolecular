const { customAlphabet } = require("nanoid");
const { MoleculerError, MoleculerClientError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const _ = require("lodash");
const axios = require("axios");
const turf = require("@turf/turf");
const currencyCodeUtil = require("../utils/currenyCode.util");
// const decodePolyline = require('decode-google-map-polyline')
const googleApiUtil = require("../utils/googleApi.util");

const constantUtil = require("../utils/constant.util");
const helperUtil = require("../utils/helper.util");
const storageUtil = require("../utils/storage.util");
// Collection Model
// const adminModel = require("../models/admin.model");
// const professionalModel = require("../models/professional.model");

const {
  bookingObject,
  bookingObjectDetails,
} = require("../utils/mapping.util");
const mappingUtil = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");
const {
  convertToInt,
  setDayStartHours,
  setDayEndHours,
} = require("../utils/common.util");
const {
  lzStringEncode,
  lzStringDecode,
  cryptoAESEncode,
  cryptoAESDecode,
} = require("../utils/crypto.util");
const {
  addDaysToRegionalUTC,
  getDbQueryString,
} = require("../utils/common.util");
const { CONST_RIDE } = require("../utils/constant.util");
const { parseBooleans } = require("xml2js/lib/processors");

const bookingV2 = {};

// BORKING STATUS CHANGE (ARRIVED, STARTED, ENDED)
bookingV2.updateProfessionalBookingStatus = async function (context) {
  const {
    OTP_INCORRECT,
    VEHICLE_CATEGORY_NOT_FOUND,
    INVALID_BOOKING,
    BOOKING_UPDATE_STATUS_FAILED,
    PROFESSIONAL_ARRIVED,
    PROFESSIONAL_STARTED,
    TRIP_END,
    INFO_FELLOW_PASSENGER,
    BOOKING_PAID,
    REFUND_TO_WALLET,
    EARNINGS,
    INFO_CARD_DECLINED,
    ALERT_FELLOW_PASSENGER_RIDE_ARRIVED,
    ALERT_FELLOW_PASSENGER_RIDE_STARTED,
    ALERT_FELLOW_PASSENGER_RIDE_ENDED,
  } = notifyMessage.setNotifyLanguage(
    context.meta?.professionalDetails?.languageCode
  );
  //#region Config Data
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const vehicleCategoryDataSet = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  if (!vehicleCategoryDataSet) {
    throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
  }
  //#endregion Config Data
  const clientId =
    context.params.clientId ||
    context.meta.clientId ||
    generalSettings.data.clientId;
  //
  let responseData = null,
    snapWayData = [],
    manualWayData = [],
    snapErrorData = {},
    finalWayDataSet = [],
    wayPointsSet = [],
    checkProfessionalRideCount = 0,
    checkUserRideCount = 0,
    isSuccessResult = false,
    vehicleCategoryData = null,
    bookingData = null,
    isSendWalletNotification = false,
    isCancelCapturePaymentIntents = false;

  const directionsData = {},
    finalEncodedPolyline = [],
    traveledDataSet = {};
  const professionalStatus = context.params.professionalStatus.toUpperCase();
  //
  const session = await mongoose.startSession();
  // const sessionSchema = await mongoose.startSession();
  try {
    const transactionOptions = {
      readPreference: "primary",
      readConcern: { level: "local" },
      writeConcern: { w: 1, j: false },
    };
    // session.startTransaction();
    // sessionSchema.startTransaction();
    await session.withTransaction(async () => {
      if (
        context.params.bookingOTP &&
        professionalStatus === constantUtil.STARTED
      ) {
        const checkJson = await this.adapter.model
          .findOne(
            {
              "_id": context.params.bookingId,
              "bookingOTP": context.params.bookingOTP.toString(),
            },
            { "_id": 1, "bookingOTP": 1 }
          )
          .lean();
        if (!checkJson) {
          throw new MoleculerError(OTP_INCORRECT);
        }
      }
      //#region Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      });
      if (!bookingData) {
        throw new MoleculerError(INVALID_BOOKING);
      }
      // Get Vehicle Details From Redish
      vehicleCategoryData =
        vehicleCategoryDataSet[
          bookingData.vehicle.vehicleCategoryId.toString()
        ];
      // Update Vehicle Details to Booking Model
      bookingData.vehicle["vehicleCategoryName"] =
        vehicleCategoryData.vehicleCategory;
      bookingData.vehicle["vehicleCategoryImage"] =
        vehicleCategoryData.categoryImage;
      bookingData.vehicle["vehicleCategoryMapImage"] =
        vehicleCategoryData.categoryMapImage;
      //
      // bookingData.distanceUnit = bookingData.category.distanceType;
      bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
        context.params?.afterRideWaitingMinsWithGracePeriod || 0;
      bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
      bookingData["imageURLPath"] =
        generalSettings.data.spaces.spacesBaseUrl +
        "/" +
        generalSettings.data.spaces.spacesObjectName;
      // bookingData.isNeedSecurityImageUpload =
      //   serviceCategoryData.isNeedSecurityImageUpload;
      //
      const userId = bookingData?.user?._id?.toString() || null;
      const professionalId = bookingData?.professional?._id?.toString() || null;
      const hubId =
        bookingData.professional?.vehicles?.[0]?.hub?.toString() || null;
      //
      const userData = await this.schema.userModel.model
        .findById(mongoose.Types.ObjectId(userId))
        .lean();
      const professionalData = await this.schema.professionalModel.model
        .findById(mongoose.Types.ObjectId(professionalId))
        .lean();
      const adminTransactionData = {
        "userType": constantUtil.ADMIN,
        "userId": null,
        "name": null,
        "avatar": null,
        "phoneNumber": null,
      };
      const userTransactionData = {
        "userType": constantUtil.USER,
        "userId": userData?._id?.toString(),
        "name": (userData?.firstName || "") + " " + (userData?.lastName || ""),
        "avatar": userData?.avatar || null,
        "phoneNumber": userData?.phone?.number || null,
      };
      const professionalTransactionData = {
        "userType": constantUtil.PROFESSIONAL,
        "userId": professionalData._id.toString(),
        "name":
          (professionalData.firstName || "") +
          " " +
          (professionalData.lastName || ""),
        "avatar": professionalData.avatar || null,
        "phoneNumber": professionalData.phone.number || null,
      };
      //#endregion Get booking Details
      if (
        (bookingData.bookingStatus === constantUtil.ACCEPTED &&
          professionalStatus === constantUtil.ARRIVED) ||
        (bookingData.bookingStatus === constantUtil.ARRIVED &&
          professionalStatus === constantUtil.STARTED) ||
        (bookingData.bookingStatus === constantUtil.STARTED &&
          professionalStatus === constantUtil.ENDED)
      ) {
        let mapLocationData,
          formatted_address = "";
        if (professionalStatus === constantUtil.ENDED) {
          mapLocationData = await googleApiUtil.geocode({
            "clientId": clientId,
            "lat": context.params.lat,
            "lng": context.params.lng,
          });
          formatted_address = await googleApiUtil.formattedAddress(
            mapLocationData.data.results
          );
          // const mapData = await googleApiUtil.directions({
          //   "from": `${bookingData.origin.lat},${bookingData.origin.lng}`,
          //   "to": `${context.params.lat},${context.params.lat}`,
          // });
        }
        bookingData.activity.rideStops = bookingData.activity.rideStops.map(
          (stop) => {
            if (
              professionalStatus === constantUtil.STARTED &&
              stop.type === constantUtil.ORIGIN
            ) {
              stop.lat = context.params.lat;
              stop.lng = context.params.lng;
              stop.status = constantUtil.ENDED;
            } else if (
              professionalStatus === constantUtil.ENDED &&
              stop.type === constantUtil.DESTINATION
            ) {
              const address = formatted_address;
              // mapLocationData.data.results[0].formatted_address || null;
              stop.lat = context.params.lat;
              stop.lng = context.params.lng;
              stop.status = constantUtil.ENDED;
              if (address) {
                stop.addressName = address;
                stop.fullAddress = address;
                stop.shortAddress = address;
              }
            }
            if (stop.type === constantUtil.ORIGIN) {
              directionsData["from1"] = [stop.lat + "," + stop.lng];
              directionsData["turfFrom"] = turf.point([
                parseFloat(stop.lat),
                parseFloat(stop.lng),
              ]);
            }
            if (stop.type === constantUtil.DESTINATION) {
              directionsData["to1"] = [stop.lat + "," + stop.lng];
              directionsData["turfTo"] = turf.point([
                parseFloat(stop.lat),
                parseFloat(stop.lng),
              ]);
            }
            return stop;
          }
        );
        let workedMins = 0,
          waitingMins = 0,
          timeData = null,
          trackData = {};
        switch (professionalStatus) {
          case constantUtil.ARRIVED:
            timeData = {
              "activity.arriveTime": new Date(),
              "regionalData.arriveTime": helperUtil.toRegionalUTC(new Date()),
              "activity.arriveLatLng": context.params.latLng,
            };
            break;

          case constantUtil.STARTED:
            timeData = {
              "activity.pickUpLatLng": context.params.latLng,
              "activity.pickUpTime": new Date(),
              "activity.isUserPickedUp": true,
              "regionalData.pickUpTime": helperUtil.toRegionalUTC(new Date()),
              "upcomingLocation.coordinates":
                bookingData?.dropLocation?.coordinates || [],
            };
            break;

          case constantUtil.ENDED:
            waitingMins = helperUtil.calculateWaitingMins(
              bookingData.waitingTimesArray
            );
            workedMins = parseInt(
              (new Date() - new Date(bookingData.activity.pickUpTime)) /
                (1000 * 60)
            );
            timeData = {
              "activity.dropLatLng": context.params.latLng,
              "activity.dropTime": new Date(),
              "activity.isUserDropped": true,
              "activity.workedMins": workedMins,
              "activity.waitingMins": waitingMins,
              "activity.endedBy": {
                "id": context.meta.professionalId.toString(),
                "userType": constantUtil.PROFESSIONAL,
              },
              "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
            };
            trackData = { "trackData": context.params?.trackData || {} };
            break;
        }

        // const updateBookingData = await this.adapter.model.updateOne(
        await this.schema.bookingModel.model.updateOne(
          {
            "_id": context.params.bookingId,
            "bookingStatus":
              professionalStatus === constantUtil.ARRIVED
                ? constantUtil.ACCEPTED
                : professionalStatus === constantUtil.STARTED
                ? constantUtil.ARRIVED
                : professionalStatus === constantUtil.ENDED
                ? constantUtil.STARTED
                : "",
          },
          {
            "$set": {
              "hub": hubId,
              "bookingStatus": professionalStatus,
              "wayData": context.params.wayData,
              "snapWayData": context.params.snapWayData,
              "manualWayData": context.params.manualWayData,
              "activity.rideStops": bookingData.activity.rideStops,
              "invoice.tollFareAmount": context.params?.tollFareAmount || 0,
              "invoice.airportFareAmount":
                context.params?.airportFareAmount || 0,
              ...timeData,
              // ...trackData,
            },
          },
          { session } // { "session": session }
        );
        // if (!updateBookingData.nModified) {
        //   throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
        // }
        //#region Update Coupon Details
        if (
          professionalStatus === constantUtil.STARTED &&
          bookingData.invoice.couponApplied === true &&
          bookingData?.user &&
          bookingData?.couponId
        ) {
          // await this.broker.emit("admin.updateCouponCount", {
          //   "userId": bookingData.user._id.toString(),
          //   "couponId": bookingData?.couponId?._id.toString(),
          //   "actionType": constantUtil.STARTED,
          // });
          const couponId = bookingData?.couponId?._id?.toString();
          // const coupon = await this.adapter.model
          const coupon = await this.schema.adminModel.model
            .findOne(
              {
                "_id": mongoose.Types.ObjectId(couponId),
                "name": constantUtil.COUPON,
              },
              {
                "_id": 1,
                "data.appliedCount": 1,
                "data.totalUsageCount": 1,
                "data.appliedDetails": 1,
              }
            )
            .lean();
          if (coupon.data.appliedCount < coupon.data.totalUsageCount) {
            let appliedCount;
            if (coupon.data?.appliedCount) {
              appliedCount = coupon.data?.appliedCount + 1;
            } else {
              appliedCount = 1;
            }
            let appliedDetails = [];
            let userFound = false;
            if (coupon.data?.appliedDetails) {
              appliedDetails = await coupon.data.appliedDetails.map(
                (userDataItem) => {
                  if (userDataItem.userId.toString() === userId) {
                    userDataItem.count += 1;
                    userFound = true;
                  }
                  return userDataItem;
                }
              );
            }
            if (!userFound) {
              if (appliedDetails.length === 0) {
                appliedDetails = [
                  {
                    "userId": userId,
                    "count": 1,
                  },
                ];
              } else {
                appliedDetails.push({
                  "userId": userId,
                  "count": 1,
                });
              }
            }
            let appliedCountData = {};
            if (context.params.actionType === constantUtil.STARTED) {
              appliedCountData = {
                "data.appliedCount": appliedCount,
              };
            }
            // await this.adapter.model.updateOne(
            await this.schema.adminModel.model.updateOne(
              {
                "_id": mongoose.Types.ObjectId(couponId),
                "name": constantUtil.COUPON,
              },
              {
                "$set": {
                  // "data.appliedCount": appliedCount,
                  ...appliedCountData,
                  "data.appliedDetails": appliedDetails,
                },
              },
              { session } // { "session": session }
            );
          }
        }
        //#endregion Update Coupon Details

        // // Get booking Details
        // bookingData = await this.getBookingDetailsById({
        //   "bookingId": context.params.bookingId.toString(),
        // }); // Get booking Details
        if (professionalStatus === constantUtil.ENDED) {
          const inputConstMeterReadingType =
            context.params?.meterReadingType || constantUtil.CONST_SNAPWAY;
          let meterReadingType = context.params.meterReadingType;
          const liveMeterMinInterruptThresholdTime =
            generalSettings.data.liveMeterMinInterruptThresholdTime;
          const liveMeterMaxInterruptThresholdTime =
            generalSettings.data.liveMeterMaxInterruptThresholdTime;
          const isEnableLiveMeter = generalSettings.data.isEnableLiveMeter;
          //#region intercity ride
          traveledDataSet["pickupDistance"] = 0;
          traveledDataSet["pickupTimeDuration"] = 0;
          traveledDataSet["dropDistance"] = 0;
          traveledDataSet["dropTimeDuration"] = 0;
          traveledDataSet["beforeRideAssistanceCareProvidesMins"] = parseInt(
            context.params.beforeRideAssistanceCareProvidesMins || 0
          );
          traveledDataSet["afterRideAssistanceCareProvidesMins"] = parseInt(
            context.params.afterRideAssistanceCareProvidesMins || 0
          );

          const distanceTypeUnitValue =
            (bookingData?.distanceUnit ?? constantUtil.KM).toUpperCase() ===
            constantUtil.KM
              ? constantUtil.KM_VALUE
              : constantUtil.MILES_VALUE;

          if (
            bookingData.serviceCategory ===
            constantUtil.CONST_INTERCITYRIDE.toLowerCase()
          ) {
            //---- Pickup Time
            traveledDataSet["pickupTimeDuration"] = parseInt(
              (new Date(bookingData?.activity?.pickUpTime) -
                new Date(bookingData?.activity?.acceptTime)) /
                1000
            );
            if (traveledDataSet["pickupTimeDuration"] < 60) {
              traveledDataSet["pickupTimeDuration"] = 60;
            }
            //---- Pickup Distance
            // const distanceData = await googleApiUtil.distancematrix({
            //   "lat": bookingData.professional.location.coordinates[1],
            //   "lng": bookingData.professional.location.coordinates[0],
            // });
            // traveledDataSet["pickupDistance"] = distanceData?.distance || 0;
            const pickupLatLngData = [];
            pickupLatLngData.push(bookingData.activity.acceptLatLng);
            pickupLatLngData.push(bookingData.activity.arriveLatLng);
            // const distanceData = await helperUtil.snapWayPath(pickupLatLngData);
            // const distanceData = await helperUtil.snapWayPath([
            //   {
            //     "lat": -28.6717216,
            //     "lng": -49.3777503,
            //     // "timeStamp": 1642342745,
            //     // "isStop": false,
            //   },
            //   {
            //     "lat": -28.6717997,
            //     "lng": -49.377989,
            //     // "timeStamp": 1642344362,
            //     // "isStop": false,
            //   },
            // ]);
            const mapDataPickup = await googleApiUtil.directions({
              "clientId": clientId,
              "from":
                bookingData.activity.acceptLatLng.lat +
                "," +
                bookingData.activity.acceptLatLng.lng,
              "to":
                bookingData.activity.pickUpLatLng.lat +
                "," +
                bookingData.activity.pickUpLatLng.lng,
            });
            traveledDataSet["pickupDistance"] =
              mapDataPickup.data?.routes[0]?.legs[0]?.distance?.value || 0;
            //-------- For RoundTrip Calculation Start --------------
            const mapDataDrop = await googleApiUtil.directions({
              "clientId": clientId,
              "from":
                bookingData.activity.acceptLatLng.lat +
                "," +
                bookingData.activity.acceptLatLng.lng,
              "to": context.params.latLng.lat + "," + context.params.latLng.lng,
            });
            traveledDataSet["dropDistance"] =
              mapDataDrop.data?.routes[0]?.legs[0]?.distance?.value || 0;
            //---- Pickup Time
            traveledDataSet["dropTimeDuration"] = parseInt(
              (new Date(bookingData?.activity?.pickUpTime) -
                new Date(bookingData?.activity?.dropTime)) /
                1000
            );
            // traveledDataSet["dropTimeDuration"] =
            //   parseInt(
            //     (
            //       (traveledDataSet["dropDistance"] / distanceTypeUnitValue) %
            //       50
            //     ).toFixed(2)
            //   ) * 60; //(time = distance ÷ speed), 50 --> Speed
            //-------- For RoundTrip Calculation End --------------;
          }
          //#endregion intercity ride

          //#region Traveled Time
          traveledDataSet["timeDuration"] = parseInt(
            (new Date(bookingData.activity.dropTime) -
              new Date(bookingData.activity.pickUpTime)) /
              1000
          );
          if (traveledDataSet["timeDuration"] < 60) {
            traveledDataSet["timeDuration"] = 60;
          }
          //#region Live Meter - Traveled Time
          if (
            isEnableLiveMeter &&
            inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
          ) {
            // bookingData["estimation"]["time"] = parseInt(
            //   context.params.traveledTime
            // );
            if (
              parseInt(context.params.traveledTime) >=
                parseInt(traveledDataSet["timeDuration"]) -
                  liveMeterMinInterruptThresholdTime &&
              parseInt(context.params.traveledTime) <=
                parseInt(traveledDataSet["timeDuration"]) +
                  liveMeterMaxInterruptThresholdTime
            ) {
              traveledDataSet["timeDuration"] = parseInt(
                context.params.traveledTime
              );
            } else {
              meterReadingType = constantUtil.CONST_TIMEINTERRUPT;
            }
          }
          //#endregion Live Meter - Traveled Time
          //#endregion Traveled Time

          let finalData = {};
          let calculatedData = {};
          const finalWayPolydata = [];
          snapWayData =
            context.params.snapWayData &&
            Array.isArray(context.params.snapWayData) &&
            context.params.snapWayData.length > 1
              ? context.params.snapWayData
              : [];
          manualWayData =
            context.params.manualWayData &&
            Array.isArray(context.params.manualWayData) &&
            context.params.manualWayData.length > 1
              ? context.params.manualWayData
              : [];

          //#region Traveled Distance
          traveledDataSet["km"] = 0;
          const liveMeterMinInterruptThresholdDistance =
            generalSettings.data.liveMeterMinInterruptThresholdDistance;
          const liveMeterMaxInterruptThresholdDistance =
            generalSettings.data.liveMeterMaxInterruptThresholdDistance;
          //
          let isNeedManualWayDistanceCalculate = false; // ManualWay Distance Calculation False
          if (
            generalSettings.data.distanceCalculateType === constantUtil.ROUTE
          ) {
            if (
              snapWayData &&
              snapWayData !== null &&
              snapWayData !== undefined &&
              Array.isArray(snapWayData) &&
              snapWayData.length > 0
            ) {
              const finalSnapWayData = await helperUtil.snapWayPath(
                snapWayData
              );
              snapErrorData = finalSnapWayData.snapErrorData;
              wayPointsSet = finalSnapWayData.finalSnapWayData;
              finalWayDataSet = finalSnapWayData.finalWayDataSet;
              traveledDataSet["km"] = finalSnapWayData.distance;
              // snapErrorData is empty We Need to Calculate the Distance Using ManualWay
              if (snapErrorData === {}) {
                isNeedManualWayDistanceCalculate = true;
              }
            } else {
              isNeedManualWayDistanceCalculate = true;
            }
          } else if (
            generalSettings.data.distanceCalculateType === constantUtil.MANUAL
          ) {
            isNeedManualWayDistanceCalculate = true;
          } else {
            isNeedManualWayDistanceCalculate = true;
          }
          //#region ManualWay Distance Calculation
          if (isNeedManualWayDistanceCalculate) {
            if (
              manualWayData &&
              manualWayData !== null &&
              manualWayData !== undefined &&
              Array.isArray(manualWayData) &&
              manualWayData.length > 1
            ) {
              const finalMaunalCalc =
                await helperUtil.manualWayDistanceCalculate(manualWayData);
              wayPointsSet = finalMaunalCalc.finalWayPoint;
              finalWayDataSet = [finalMaunalCalc.finalWayPoint];
              traveledDataSet["km"] = finalMaunalCalc.distance;
            } else {
              traveledDataSet["km"] =
                manualWayData.length == 1
                  ? context.params.manualDistance
                    ? context.params.manualDistance
                    : 0
                  : bookingData.estimation.distance;
            }
          }
          //#endregion ManualWay Distance Calculation

          //#region For Live Meter - Traveled Distance
          if (
            isEnableLiveMeter &&
            inputConstMeterReadingType === constantUtil.CONST_LIVEMETER
          ) {
            // bookingData["estimation"]["distance"] = parseInt(
            //   context.params.traveledDistance
            // );
            if (
              parseInt(context.params.traveledDistance) >=
                parseInt(traveledDataSet["km"]) -
                  liveMeterMinInterruptThresholdDistance &&
              parseInt(context.params.traveledDistance) <=
                parseInt(traveledDataSet["km"]) +
                  liveMeterMaxInterruptThresholdDistance
            ) {
              traveledDataSet["km"] = parseFloat(
                context.params.traveledDistance
              );
            } else {
              meterReadingType = constantUtil.CONST_DISTANCEINTERRUPT;
            }
          }
          //#endregion For Live Meter - Traveled Distance

          //#endregion Traveled Distance

          //#region Ride Fare Calculation
          calculatedData = await helperUtil.calculateEstimateAmount({
            "clientId": clientId,
            "bookingData": bookingData,
            //
            "tollFareAmount": context.params?.tollFareAmount || 0,
            "airportFareAmount": context.params?.airportFareAmount || 0,
            //
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
            "beforeRideAssistanceCareProvidesMins":
              traveledDataSet["beforeRideAssistanceCareProvidesMins"],
            "afterRideAssistanceCareProvidesMins":
              traveledDataSet["afterRideAssistanceCareProvidesMins"],
          });
          finalData = await helperUtil.endTripCalculation({
            "clientId": clientId,
            // "bookingData": bookingData,
            "paymentOption": bookingData.payment.option,
            "calculatedData": calculatedData,
            "estimationDistance": traveledDataSet["km"],
            "estimationTime": traveledDataSet["timeDuration"],
            "calculatedPickupDistance": traveledDataSet["pickupDistance"],
            "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
            "calculatedDropDistance": traveledDataSet["dropDistance"],
            "calculatedDropTime": traveledDataSet["dropTimeDuration"],
          });
          //#endregion Ride Fare Calculation

          let paidCalcType = constantUtil.EQUALTOPAID;
          if (finalData.payableAmount < bookingData.invoice.estimationAmount) {
            paidCalcType = constantUtil.LESSTHENPAID;
          } else if (
            finalData.payableAmount > bookingData.invoice.estimationAmount
          ) {
            paidCalcType = constantUtil.GREATERTHENPAID;
          }
          //#region Static Ride Image Processing
          const staticMapData = {
            "km": traveledDataSet["km"],
            "from": directionsData["from1"],
            "to": directionsData["to1"],
            "waypoints": "",
          };
          const wayValueSet = [];
          if (finalWayPolydata && finalWayPolydata.length > 0) {
            const wayPointsValue = await helperUtil.randomslice(
              finalWayPolydata,
              30
            );
            await wayPointsValue.forEach((w) => {
              wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
            });
          } else {
            const wayPointsValue = await helperUtil.randomslice(
              manualWayData,
              30
            );
            await wayPointsValue.forEach((w) => {
              wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
            });
          }
          staticMapData["waypoints"] = wayValueSet.join("");
          const value = await helperUtil.getStaticImage(staticMapData);
          const config = {
            "method": "get",
            "url": value,
            "headers": {},
            "responseType": "arraybuffer",
          };
          const imageData = await axios(config);
          let finalImageData = await this.broker.emit(
            "admin.uploadStaticMapImageInSpaces",
            {
              "files": imageData.data,
              "fileName": `${clientId}_static${directionsData["from1"]}Map${directionsData["to1"]}Image`,
            }
          );
          finalImageData = finalImageData && finalImageData[0];
          //#endregion Static Ride Image Processing
          await this.schema.bookingModel.model.updateOne(
            {
              "_id": context.params.bookingId,
              "bookingStatus": constantUtil.ENDED,
            },
            {
              "$set": {
                "hub": hubId,
                "finalWayData": finalWayDataSet,
                "manualDistance": context.params.manualDistance || 0,
                "manualWayData": context.params.manualWayData || [],
                "totalTollPassed": context.params.totalTollPassed || 0,
                "passedTollList": context.params.passedTollList || [],
                "invoice.serviceTax": finalData["serviceTax"],
                "invoice.tollFareAmount": finalData["tollFareAmount"],
                "invoice.airportFareAmount": finalData["airportFareAmount"],
                "invoice.amountInSite": finalData["amountInSite"],
                "invoice.amountInDriver": finalData["amountInDriver"],
                "invoice.travelCharge": finalData["travelCharge"],
                "invoice.distanceFare": finalData["distanceFare"],
                "invoice.timeFare": finalData["timeFare"],
                "invoice.siteCommissionWithoutTax":
                  finalData["siteCommissionWithoutTax"],
                "invoice.siteCommission": finalData["siteCommission"],
                "invoice.professionalCommision":
                  finalData["professionalCommision"],
                "invoice.professionalCommisionWithoutTips":
                  finalData["professionalCommision"],
                "invoice.estimationPayableAmount":
                  finalData["estimationAmount"],
                "invoice.payableAmount": finalData["payableAmount"],
                "invoice.couponAmount": finalData["couponAmount"],
                "invoice.professionalTolerenceAmount":
                  finalData["professionalTolerenceAmount"],
                //
                "estimation.distance": traveledDataSet["km"],
                "estimation.time": traveledDataSet["timeDuration"],
                "estimation.pickupDistance": traveledDataSet["pickupDistance"],
                "estimation.pickupTime": traveledDataSet["pickupTimeDuration"],
                "estimation.dropDistance": traveledDataSet["dropDistance"],
                "estimation.dropTime": traveledDataSet["dropTimeDuration"],
                //
                "bookedEstimation.distance": bookingData.estimation.distance,
                "bookedEstimation.time": bookingData.estimation.time,
                "bookedEstimation.pickupDistance":
                  bookingData.estimation.pickupDistance,
                "bookedEstimation.pickupTime":
                  bookingData.estimation.pickupTime,
                "rideMapRouteImage": finalImageData,
                "payment.paid": true,
                "encodedPolyline": finalEncodedPolyline,
                "mapErrorData": "",
                "snapErrorData": snapErrorData,
                "originalWayData": wayPointsSet,
                "distanceCalculateType":
                  generalSettings.data.distanceCalculateType,
                "invoice.peakFareCharge": finalData["peakFareCharge"],
                "invoice.nightFareCharge": finalData["nightFareCharge"],
                "invoice.waitingCharge": finalData["waitingCharge"],
                "invoice.beforeRideWaitingCharge":
                  finalData["beforeRideWaitingCharge"],
                "invoice.beforeRideWaitingMins":
                  finalData["beforeRideWaitingMins"],
                "invoice.afterRideWaitingCharge":
                  finalData["afterRideWaitingCharge"],
                "invoice.afterRideWaitingMins":
                  finalData["afterRideWaitingMins"],
                "meterReadingType": meterReadingType,
                "liveMeter": {
                  "isEnableLiveMeter": isEnableLiveMeter,
                  "traveledDistance": context.params.traveledDistance,
                  "traveledTime": context.params.traveledTime,
                },
                "invoice.intercityPickupTimeFare":
                  finalData["intercityPickupTimeFare"],
                "invoice.intercityPickupDistanceFare":
                  finalData["intercityPickupDistanceFare"],
                "invoice.intercityPickupCharge":
                  finalData["intercityPickupCharge"],
                "invoice.intercityDropCharge": finalData["intercityDropCharge"],
                "invoice.roundingAmount": finalData["roundingAmount"],
                "invoice.discountAmount": finalData["discountAmount"],
                //
                "invoice.beforeRideAssistanceCareProvidesMins":
                  traveledDataSet["beforeRideAssistanceCareProvidesMins"],
                "invoice.beforeRideAssistanceCareAmount":
                  finalData["beforeRideAssistanceCareAmount"],
                "invoice.afterRideAssistanceCareProvidesMins":
                  traveledDataSet["afterRideAssistanceCareProvidesMins"],
                //
                "invoice.afterRideAssistanceCareAmount":
                  finalData["afterRideAssistanceCareAmount"],
                "invoice.assistanceCareAmount":
                  finalData["assistanceCareAmount"],
              },
            },
            { session } //  { "session": session }
          );
          // this.broker.emit("professional.updatependingReview", {
          //   "professionalId": bookingData.professional._id.toString(),
          //   "bookingId": context.params.bookingId,
          //   "lastCurrencyCode": bookingData.currencyCode,
          // });
          await this.schema.professionalModel.model.updateOne(
            { "_id": mongoose.Types.ObjectId(professionalId) },
            {
              "$set": {
                "bookingInfo.ongoingBooking": null,
                "bookingInfo.pendingReview": context.params.bookingId
                  ? context.params.bookingId
                  : null,
              },
              // "lastCurrencyCode": bookingData.currencyCode,
            },
            { session } // { "session": session }
          );

          //#region Franchising (site commition to Hub)
          if (generalSettings.data.isEnableFranchising) {
            // let hubData = await this.broker.emit(
            //   "admin.updateHubSiteCommission",
            //   {
            //     // "professionalId": bookingData.professional?._id?.toString(),
            //     "bookingId": bookingData?._id?.toString(),
            //     "refBookingId": bookingData.bookingId,
            //     "hubId": bookingData.professional?.vehicles[0]?.hub?.toString(),
            //     "siteCommissionWithoutTax": finalData["siteCommissionWithoutTax"],
            //   }
            // );
            // hubData = hubData && hubData[0];
            // console.log(JSON.stringify(hubData));
            if (hubId) {
              let siteCommission = 0,
                siteCommissionPercentage = 0;
              const hubData = await this.schema.adminModel.model
                .findOne({
                  "_id": mongoose.Types.ObjectId(hubId),
                  "name": constantUtil.HUBS,
                  // "data.status": constantUtil.ACTIVE,
                })
                .lean();
              if (hubData) {
                siteCommissionPercentage =
                  hubData.data?.commissionPercentage || 0;
                siteCommission =
                  (parseFloat(finalData["siteCommissionWithoutTax"] || 0) /
                    100) *
                  parseFloat(siteCommissionPercentage);

                const hubResponse =
                  await this.schema.adminModel.model.updateOne(
                    {
                      "_id": mongoose.Types.ObjectId(hubId),
                      "name": constantUtil.HUBS,
                      // "data.status": constantUtil.ACTIVE,
                    },
                    { "$set": { "$inc": { "data.amount": siteCommission } } },
                    { session } //  { "session": session }
                  );
                if (hubResponse.nModified) {
                  //-------- Transaction Entry Start ---------------------
                  // this.broker.emit("transaction.hubSiteCommissionTransaction", {
                  //   "bookingId": context.params.bookingId?.toString(),
                  //   "refBookingId": context.params.refBookingId,
                  //   "hubId": context.params?.hubId?.toString(),
                  //   "hubData": hubData.data,
                  //   "amount": siteCommission,
                  //   "previousBalance": hubData?.data?.amount || 0,
                  //   "currentBalance":
                  //     parseFloat(hubData?.data?.amount || 0) +
                  //     parseFloat(siteCommission),
                  // });
                  const hubTransactionReason = `${
                    constantUtil.HUBSEARNINGS + " " + bookingData.bookingId
                  }`;
                  await this.schema.transactionModel.model.create(
                    [
                      {
                        "clientId": clientId,
                        "bookingId": context.params?.bookingId,
                        "transactionType": constantUtil.HUBSEARNINGS,
                        "transactionAmount": siteCommission,
                        "previousBalance": hubData?.data?.amount || 0,
                        "currentBalance":
                          parseFloat(hubData?.data?.amount || 0) +
                          parseFloat(siteCommission),
                        "paidTransactionId": null,
                        "transactionId": Date.now(),
                        "transactionDate": new Date(),
                        "transactionReason": hubTransactionReason,
                        "refBookingId": bookingData.bookingId,
                        "transactionStatus": constantUtil.SUCCESS,
                        "paymentType": constantUtil.CREDIT,
                        // "from": {
                        //   "userType": constantUtil.ADMIN,
                        //   "userId": null,
                        //   "name": null,
                        //   "avatar": null,
                        //   "phoneNumber": null,
                        // },
                        "from": adminTransactionData,
                        "to": {
                          "userType": constantUtil.HUBS,
                          "userId": hubId,
                          "name": hubData.data.hubsName || null,
                          "avatar": null,
                          "phoneNumber": hubData.data.phone.number || null,
                        },
                      },
                    ],
                    { session } // { "session": session }
                  );
                  //-------- Transaction Entry End ---------------------
                  await this.schema.bookingModel.model.updateOne(
                    {
                      "_id": mongoose.Types.ObjectId(context.params.bookingId),
                      "bookingStatus": constantUtil.ENDED,
                    },
                    {
                      "$set": {
                        "invoice.hubSiteCommission": siteCommission || 0,
                        "invoice.hubSiteCommissionPercentage":
                          siteCommissionPercentage || 0,
                      },
                    },
                    { session } // { "session": session }
                  );
                }
              }
            }
          }
          //#endregion Franchising (site commition to Hub)

          // if (bookingData.guestType === constantUtil.USER && bookingData.user) {
          if (bookingData.user) {
            // pending amount
            if (bookingData.invoice.pendingPaymentAmount > 0) {
              // await this.broker.emit("user.updateWalletForPendingAmount", {
              //   "userId": bookingData.user._id.toString(),
              // });
              await this.schema.userModel.model.updateOne(
                { "_id": mongoose.Types.ObjectId(userId) },
                { "$set": { "wallet.availableAmount": 0 } },
                { session } // { "session": session }
              );
            }
            // upate review
            // this.broker.emit("user.updatependingReview", {
            //   "userId": bookingData.user._id.toString(),
            //   "bookingId": context.params.bookingId,
            //   "lastCurrencyCode": bookingData.currencyCode,
            // });
            await this.schema.userModel.model.updateOne(
              { "_id": mongoose.Types.ObjectId(userId) },
              {
                "$set": {
                  "bookingInfo.ongoingBooking": null,
                  "bookingInfo.pendingReview": context.params.bookingId
                    ? context.params.bookingId
                    : null,
                  // "lastCurrencyCode": bookingData.currencyCode,
                },
              },
              { session } //  { "session": session }
            );
            if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
              // this.broker.emit("user.updateWalletAmountTripEnded", {
              //   "userId": bookingData.user._id.toString(),
              //   "amount": parseFloat(finalData.payableAmount.toFixed(2)),
              //   "transStatus": paidCalcType,
              //   "paymentMethod": bookingData.payment.option,
              // });

              let valueData;
              if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
                // valueData =
                //   context.params.transStatus === constantUtil.EQUALTOPAID
                //     ? {
                //         "availableAmount": parseFloat(
                //           userData.wallet.availableAmount.toFixed(2)
                //         ),
                //         "freezedAmount": 0,
                //       }
                //     : context.params.transStatus === constantUtil.LESSTHENPAID
                //     ? {
                //         "availableAmount": parseFloat(
                //           (
                //             parseFloat(userData.wallet.availableAmount) +
                //             (parseFloat(userData.wallet.freezedAmount) -
                //               parseFloat(finalData.payableAmount.toFixed(2)))
                //           ).toFixed(2)
                //         ),
                //         "freezedAmount": 0,
                //       }
                //     : context.params.transStatus === constantUtil.GREATERTHENPAID
                //     ? {
                //         "availableAmount": parseFloat(
                //           (
                //             parseFloat(userData.wallet.availableAmount) +
                //             (parseFloat(userData.wallet.freezedAmount) -
                //               parseFloat(finalData.payableAmount.toFixed(2)))
                //           ).toFixed(2)
                //         ),
                //         "freezedAmount": 0,
                //       }
                //     : {
                //         "availableAmount": parseFloat(
                //           userData.wallet.availableAmount.toFixed(2)
                //         ),
                //         "freezedAmount": parseFloat(
                //           userData.wallet.freezedAmount.toFixed(2)
                //         ),
                //       };
                // switch (context.params.transStatus) {
                switch (paidCalcType) {
                  case constantUtil.EQUALTOPAID:
                    valueData = {
                      "availableAmount": parseFloat(
                        userData.wallet.availableAmount.toFixed(2)
                      ),
                      "freezedAmount": 0,
                    };
                    break;

                  case constantUtil.LESSTHENPAID:
                    valueData = {
                      "availableAmount": parseFloat(
                        (
                          parseFloat(userData.wallet.availableAmount) +
                          (parseFloat(userData.wallet.freezedAmount) -
                            parseFloat(finalData.payableAmount.toFixed(2)))
                        ).toFixed(2)
                      ),
                      "freezedAmount": 0,
                    };
                    break;

                  case constantUtil.GREATERTHENPAID:
                    valueData = {
                      "availableAmount": parseFloat(
                        (
                          parseFloat(userData.wallet.availableAmount) +
                          (parseFloat(userData.wallet.freezedAmount) -
                            parseFloat(finalData.payableAmount.toFixed(2)))
                        ).toFixed(2)
                      ),
                      "freezedAmount": 0,
                    };
                    break;

                  default:
                    valueData = {
                      "availableAmount": parseFloat(
                        userData.wallet.availableAmount.toFixed(2)
                      ),
                      "freezedAmount": parseFloat(
                        userData.wallet.freezedAmount.toFixed(2)
                      ),
                    };
                }
              }
              // else if (
              //   bookingData.payment.option === constantUtil.PAYMENTCARD
              // ) {
              //   // valueData =
              //   //   context.params.transStatus === constantUtil.EQUALTOPAID
              //   //     ? {
              //   //         "availableAmount": parseFloat(
              //   //           userData.wallet.availableAmount.toFixed(2)
              //   //         ),
              //   //         "freezedAmount": 0,
              //   //       }
              //   //     : context.params.transStatus === constantUtil.LESSTHENPAID
              //   //     ? {
              //   //         "availableAmount": parseFloat(
              //   //           (
              //   //             parseFloat(userData.wallet.availableAmount) +
              //   //             (parseFloat(userData.wallet.freezedAmount) -
              //   //               parseFloat(finalData.payableAmount.toFixed(2)))
              //   //           ).toFixed(2)
              //   //         ),
              //   //         "freezedAmount": 0,
              //   //       }
              //   //     : context.params.transStatus === constantUtil.GREATERTHENPAID
              //   //     ? {
              //   //         "availableAmount": parseFloat(
              //   //           (
              //   //             parseFloat(userData.wallet.availableAmount) +
              //   //             (parseFloat(userData.wallet.freezedAmount) -
              //   //               parseFloat(finalData.payableAmount.toFixed(2)))
              //   //           ).toFixed(2)
              //   //         ),
              //   //         "freezedAmount": 0,
              //   //       }
              //   //     : {
              //   //         "availableAmount": parseFloat(
              //   //           userData.wallet.availableAmount.toFixed(2)
              //   //         ),
              //   //         "freezedAmount": parseFloat(
              //   //           userData.wallet.freezedAmount.toFixed(2)
              //   //         ),
              //   //       };
              //   switch (context.params.transStatus) {
              //     case constantUtil.EQUALTOPAID:
              //       valueData = {
              //         "availableAmount": parseFloat(
              //           userData.wallet.availableAmount.toFixed(2)
              //         ),
              //         "freezedAmount": 0,
              //       };
              //       break;

              //     case constantUtil.LESSTHENPAID:
              //       valueData = {
              //         "availableAmount": parseFloat(
              //           (
              //             parseFloat(userData.wallet.availableAmount) +
              //             (parseFloat(userData.wallet.freezedAmount) -
              //               parseFloat(finalData.payableAmount.toFixed(2)))
              //           ).toFixed(2)
              //         ),
              //         "freezedAmount": 0,
              //       };
              //       break;

              //     case constantUtil.GREATERTHENPAID:
              //       valueData = {
              //         "availableAmount": parseFloat(
              //           (
              //             parseFloat(userData.wallet.availableAmount) +
              //             (parseFloat(userData.wallet.freezedAmount) -
              //               parseFloat(finalData.payableAmount.toFixed(2)))
              //           ).toFixed(2)
              //         ),
              //         "freezedAmount": 0,
              //       };
              //       break;

              //     default:
              //       valueData = {
              //         "availableAmount": parseFloat(
              //           userData.wallet.availableAmount.toFixed(2)
              //         ),
              //         "freezedAmount": parseFloat(
              //           userData.wallet.freezedAmount.toFixed(2)
              //         ),
              //       };
              //       break;
              //   }
              // }
              const updateData = {
                "wallet.availableAmount": valueData.availableAmount,
                "wallet.freezedAmount": valueData.freezedAmount,
              };

              await this.schema.userModel.model.updateOne(
                { "_id": mongoose.Types.ObjectId(userId) },
                updateData,
                { session } //{ "session": session }
              );
              // if (!responseJson.nModified) return null;

              // const finalamount = Math.abs(
              //   parseFloat(userData.wallet.freezedAmount) -
              //     parseFloat(finalData.payableAmount.toFixed(2))
              // );
              // if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
              //   // // if (context.params.transStatus === constantUtil.GREATERTHENPAID) {
              //   // this.broker.emit("transaction.userLessAndGreaterEndTransaction", {
              //   //   "amount": finalamount,
              //   //   "previousBalance": userData.wallet.availableAmount,
              //   //   "currentBalance": valueData.availableAmount,
              //   //   "transType":
              //   //     paidCalcType === constantUtil.GREATERTHENPAID
              //   //       ? constantUtil.GREATERTHENPAID
              //   //       : constantUtil.LESSTHENPAID,
              //   //   "transactionType":
              //   //     paidCalcType === constantUtil.GREATERTHENPAID
              //   //       ? constantUtil.EXTRAAMOUNTDEBIT
              //   //       : constantUtil.CARDAMOUNTREFUND,
              //   //   "paymentType":
              //   //     paidCalcType === constantUtil.GREATERTHENPAID
              //   //       ? constantUtil.DEBIT
              //   //       : constantUtil.CREDIT,
              //   //   "userType": constantUtil.USER,
              //   //   "data": {
              //   //     "id": userData._id.toString(),
              //   //     "firstName": userData.firstName,
              //   //     "lastName": userData.lastName,
              //   //     "avatar": userData.avatar,
              //   //     "phone": {
              //   //       "code": userData.phone.code,
              //   //       "number": userData.phone.number,
              //   //     },
              //   //   },
              //   // });
              //   this.schema.transactionModel.model.create(
              //     [
              //       {
              //         "transactionType":
              //           paidCalcType === constantUtil.GREATERTHENPAID
              //             ? constantUtil.EXTRAAMOUNTDEBIT
              //             : constantUtil.CARDAMOUNTREFUND,
              //         "transactionAmount": finalamount,
              //         "previousBalance": userData.wallet.availableAmount,
              //         "currentBalance": valueData.availableAmount,
              //         "paidTransactionId": null,
              //         "transactionId": `${helperUtil.randomString(8, "a")}`,
              //         "transactionDate": new Date(),
              //         "paymentType":
              //           paidCalcType === constantUtil.GREATERTHENPAID
              //             ? constantUtil.DEBIT
              //             : constantUtil.CREDIT,
              //         "from":
              //           paidCalcType === constantUtil.GREATERTHENPAID
              //             ? userTransactionData
              //             : adminTransactionData,
              //         "to":
              //           paidCalcType === constantUtil.GREATERTHENPAID
              //             ? adminTransactionData
              //             : userTransactionData,
              //       },
              //     ],
              //     { "session": sessionSchema }
              //   );
              //   // } else if (
              //   //   context.params.transStatus === constantUtil.LESSTHENPAID
              //   // ) {
              //   //   this.broker.emit(
              //   //     "transaction.userLessAndGreaterEndTransaction",
              //   //     {
              //   //       "amount": finalamount,
              //   //       "previousBalance": userData.wallet.availableAmount,
              //   //       "currentBalance": valueData.availableAmount,
              //   //       "transType": constantUtil.LESSTHENPAID,
              //   //       "transactionType": constantUtil.CARDAMOUNTREFUND,
              //   //       "paymentType": constantUtil.CREDIT,
              //   //       "userType": constantUtil.USER,
              //   //       "data": {
              //   //         "id": userData._id.toString(),
              //   //         "firstName": userData.firstName,
              //   //         "lastName": userData.lastName,
              //   //         "avatar": userData.avatar,
              //   //         "phone": {
              //   //           "code": userData.phone.code,
              //   //           "number": userData.phone.number,
              //   //         },
              //   //       },
              //   //     }
              //   //   );
              //   // }
              // }
              //------------------
              const userAvailableAmount =
                parseFloat(bookingData?.user?.wallet?.availableAmount) +
                parseFloat(bookingData?.user?.wallet?.freezedAmount);
              // this.broker.emit("transaction.newBookingTransaction", {
              //   "userId": bookingData.user._id.toString(),
              //   "amount": finalData.payableAmount.toFixed(2),
              //   "previousBalance": userAvailableAmount,
              //   "currentBalance":
              //     parseFloat(userAvailableAmount) -
              //     parseFloat(finalData.payableAmount.toFixed(2)),
              //   "refBookingId": bookingData?.bookingId,
              // });
              await this.schema.transactionModel.model.create(
                [
                  {
                    "clientId": clientId,
                    "bookingId": context.params?.bookingId,
                    "transactionType": constantUtil.BOOKINGCHARGE,
                    "transactionAmount": finalData.payableAmount.toFixed(2),
                    "previousBalance": userAvailableAmount,
                    "currentBalance":
                      parseFloat(userAvailableAmount) -
                      parseFloat(finalData.payableAmount.toFixed(2)),
                    "paidTransactionId": null,
                    "transactionId": `${helperUtil.randomString(8, "a")}`,
                    "transactionDate": new Date(),
                    "transactionReason": `${
                      "New " + constantUtil.BOOKINGCHARGE
                    }`,
                    "refBookingId": bookingData?.bookingId,
                    "transactionStatus": constantUtil.SUCCESS,
                    "paymentType": constantUtil.DEBIT,
                    "from": userTransactionData,
                    "to": adminTransactionData,
                  },
                ],
                { session } //{ "session": session }
              );
            } else if (
              bookingData.payment.option === constantUtil.PAYMENTCARD
            ) {
              // take amount
              let finalPaymentResponse = await this.broker.emit(
                "transaction.tripEndCardCharge",
                {
                  "requestFrom": "bookingV2",
                  "userId": bookingData.user._id.toString(),
                  "amount": parseFloat(finalData.payableAmount.toFixed(2)),
                  "cardId": bookingData.payment.card.cardId.toString(),
                  "bookingId": bookingData._id.toString(),
                  "refBookingId": bookingData.bookingId,
                  "paymentInitId": bookingData.paymentInitId,
                  "paymentInitAmount": bookingData.paymentInitAmount,
                }
              );
              finalPaymentResponse =
                finalPaymentResponse && finalPaymentResponse[0];
              //--------------- db Insert Start------------
              const transaction =
                await this.schema.transactionModel.model.create(
                  [
                    {
                      "clientId": clientId,
                      "transactionAmount":
                        finalPaymentResponse.data.transactionAmount,
                      "previousBalance": null,
                      "currentBalance": null,
                      "transactionDate": new Date(),
                      "transactionReason":
                        finalPaymentResponse.data.transactionReason,
                      "refBookingId": bookingData.bookingId,
                      "from": userTransactionData,
                      "to": adminTransactionData,
                      "paymentType": constantUtil.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
                      "transactionType": constantUtil.BOOKINGCHARGECARD,
                      "transactionId": finalPaymentResponse.data.transactionId,
                      "bookingId": bookingData._id.toString(),
                      "gatewayName": finalPaymentResponse.data.paymentGateWay,
                      "paymentGateWay":
                        finalPaymentResponse.data.paymentGateWay,
                      "gatewayDocId": finalPaymentResponse.data.gatewayDocId,
                      "transactionStatus":
                        finalPaymentResponse.data.transactionStatus, // constantUtil.PROCESSING,
                      "cardDetailes": finalPaymentResponse.data.cardDetails,
                    },
                  ],
                  { session } // { "session": session }
                );
              //------------------- Below ---------------
              if (finalPaymentResponse?.data?.code === 200) {
                // const updateTransaction =
                await this.schema.bookingModel.model.updateOne(
                  {
                    "_id": mongoose.Types.ObjectId(
                      transaction?.[0]?._id?.toString()
                    ),
                  },
                  {
                    "$set": {
                      "transactionStatus":
                        finalPaymentResponse.data.transactionStatus, // constantUtil.PROCESSING,
                      "transactionId": finalPaymentResponse.data.transactionId,
                      "gatewayResponse":
                        finalPaymentResponse?.data?.data?.successResponse || {},
                    },
                  },
                  { session } //{ "session": session }
                );
                if (
                  parseFloat(finalData.payableAmount.toFixed(2)) >=
                  bookingData.paymentInitAmount
                ) {
                  // substract amount from user avilable amount
                  const remainCaptureAmount = parseFloat(
                    (
                      finalData.payableAmount - bookingData.paymentInitAmount
                    ).toFixed(2)
                  );
                  // notificationMessage = INFO_CARD_DECLINED; //Need to Change
                  availableAmount =
                    userData.wallet.availableAmount - remainCaptureAmount;

                  // let amountUpdateInUser = await this.broker.emit(
                  //   "user.updateWalletAmount",
                  //   {
                  //     "userId": userData._id.toString(),
                  //     "availableAmount": availableAmount,
                  //     "freezedAmount": userData.wallet.freezedAmount,
                  //     "scheduleFreezedAmount":
                  //       userData.wallet.scheduleFreezedAmount,
                  //     "isSendNotification": isSendNotification,
                  //     "notificationMessage": notificationMessage,
                  //   }
                  // );
                  // amountUpdateInUser =
                  //   amountUpdateInUser && amountUpdateInUser[0];
                  // if (!amountUpdateInUser) {
                  //   throw new MoleculerError(
                  //     SOMETHING_WENT_WRONG + " IN WALLET TRANSACTION",
                  //     500
                  //   );
                  // }
                  await this.schema.userModel.model.updateOne(
                    { "_id": mongoose.Types.ObjectId(userId) },
                    {
                      "$set": {
                        "wallet.availableAmount": availableAmount,
                        "wallet.freezedAmount": userData.wallet.freezedAmount,
                        "wallet.scheduleFreezedAmount":
                          userData.wallet.scheduleFreezedAmount,
                      },
                    },
                    { session } // { "session": session }
                  );
                  isSendWalletNotification = true;
                  //---------------------------------------
                  // transactionType = constantUtil.BOOKINGCHARGE;
                  const transaction =
                    await this.schema.transactionModel.model.create(
                      [
                        {
                          "clientId": clientId,
                          "transactionAmount": remainCaptureAmount,
                          "previousBalance": userData.wallet.availableAmount,
                          "currentBalance":
                            userData.wallet.availableAmount -
                            remainCaptureAmount,
                          "transactionReason":
                            finalPaymentResponse.data.transactionReason,
                          "refBookingId": bookingData.bookingId,
                          "from": userTransactionData,
                          "to": adminTransactionData,
                          "paymentType": constantUtil.DEBIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
                          "transactionType": constantUtil.BOOKINGCHARGE,
                          "transactionId":
                            finalPaymentResponse.data.transactionId,
                          "bookingId": bookingData._id.toString(),
                          "gatewayName": constantUtil.WALLET,
                          "paymentGateWay": constantUtil.WALLET,
                          "transactionStatus": constantUtil.SUCCESS,
                        },
                      ],
                      { session } //{ "session": session }
                    );
                  // session;
                  // if (!transaction) {
                  //   throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
                  // }
                  //--------------- db Insert Start------------
                }
              } else {
                // responseMessage = ERROR_CARD_PAYMENT_FAILED;
                // if (
                //   paymentType === constantUtil.CASH ||
                //   paymentType === constantUtil.CARD
                // ) {
                // responseMessage = INFO_CARD_DECLINED;
                // isSendNotification = true;
                // notificationMessage = INFO_CARD_DECLINED; //Need to Change
                availableAmount =
                  userData.wallet.availableAmount - finalData.payableAmount;
                // updateDb
                const updateTransaction =
                  await this.schema.transactionModel.model.updateOne(
                    {
                      "_id": mongoose.Types.ObjectId(
                        transaction?.[0]?._id?.toString()
                      ),
                    },
                    {
                      "$set": {
                        "transactionAmount": finalData.payableAmount,
                        "previousBalance": userData.wallet.availableAmount,
                        "currentBalance": availableAmount,
                        "transactionStatus": constantUtil.SUCCESS,
                        "gatewayName": constantUtil.WALLET, //If Card payment Failed
                        "paymentGateWay": constantUtil.WALLET,
                        "transactionType": constantUtil.BOOKINGCHARGE,
                      },
                    },
                    { session } // { "session": session }
                  );
                // if (!updateTransaction.nModified) {
                //   throw new MoleculerError(SOMETHING_WENT_WRONG, 500);
                // }
                //---------------- Payment Failed Start ------------------
                // substract amount from user avilable amount
                // let amountUpdateInUser = await this.broker.emit(
                //   "user.updateWalletAmount",
                //   {
                //     "userId": userData._id.toString(),
                //     "availableAmount": availableAmount,
                //     "freezedAmount": userData.wallet.freezedAmount,
                //     "scheduleFreezedAmount":
                //       userData.wallet.scheduleFreezedAmount,
                //     "isSendNotification": isSendNotification,
                //     "notificationMessage": notificationMessage,
                //   }
                // );
                // amountUpdateInUser =
                //   amountUpdateInUser && amountUpdateInUser[0];
                // if (!amountUpdateInUser) {
                //   throw new MoleculerError(
                //     SOMETHING_WENT_WRONG + " IN WALLET TRANSACTION",
                //     500
                //   );
                // }
                await this.schema.userModel.model.updateOne(
                  { "_id": mongoose.Types.ObjectId(userId) },
                  {
                    "$set": {
                      "wallet.availableAmount": availableAmount,
                      "wallet.freezedAmount": userData.wallet.freezedAmount,
                      "wallet.scheduleFreezedAmount":
                        userData.wallet.scheduleFreezedAmount,
                    },
                  },
                  { session } // { "session": session }
                );
                isSendWalletNotification = true;
                isCancelCapturePaymentIntents = true;
                //---------------- Payment Failed END ------------------
                // }
              }
              //------------------- Above ---------------
            }
          }
          // professsional transaction
          if (
            generalSettings.data.paymentSection === constantUtil.CASHANDWALLET
          ) {
            if (
              bookingData.payment.option === constantUtil.PAYMENTWALLET ||
              bookingData.payment.option === constantUtil.PAYMENTCARD
            ) {
              // this.broker.emit("professional.updateWalletAmountTripEnded", {
              //   "professionalId": bookingData.professional._id.toString(),
              //   "amount": parseFloat(finalData.professionalCommision.toFixed(2)),
              //   "paymentMethod": bookingData.payment.option,
              // });
              let onlineStatus = true,
                newAvailableAmount = 0;
              if (
                bookingData.payment.option === constantUtil.PAYMENTWALLET ||
                bookingData.payment.option === constantUtil.PAYMENTCARD ||
                (bookingData.payment.option === constantUtil.PAYMENTCREDIT && //  need to Revert //Zayride
                  generalSettings.data
                    .isProfessionalsCorporateSiteCommissionCreditToWallet)
              ) {
                newAvailableAmount =
                  parseFloat(professionalData.wallet.availableAmount) +
                  parseFloat(finalData.professionalCommision);
              } else {
                newAvailableAmount =
                  parseFloat(professionalData.wallet.availableAmount) -
                  parseFloat(finalData.professionalCommision);
              }
              if (
                parseFloat(generalSettings.data.minimumWalletAmountToOnline) >
                parseFloat(newAvailableAmount)
              ) {
                onlineStatus = false;
              }
              await this.schema.professionalModel.model.updateOne(
                { "_id": mongoose.Types.ObjectId(professionalId) },
                {
                  "$set": {
                    "wallet.availableAmount": newAvailableAmount,
                    "onlineStatus": onlineStatus,
                    "onlineStatusUpdatedAt": new Date(),
                  },
                },
                { session } // { "session": session }
              );
              // this.broker.emit("transaction.professionalBookingEndTransaction", {
              //   "professionalId": bookingData.professional._id.toString(),
              //   "amount": finalData.professionalCommision.toFixed(2),
              //   "bookingId": bookingData._id.toString(),
              //   "refBookingId": bookingData.bookingId,
              //   "previousBalance":
              //     bookingData?.professional?.wallet?.availableAmount,
              //   "currentBalance":
              //     parseFloat(bookingData?.professional?.wallet?.availableAmount) +
              //     parseFloat(finalData.professionalCommision.toFixed(2)),
              // });
              await this.schema.transactionModel.model.create(
                [
                  {
                    "clientId": clientId,
                    "bookingId": context.params?.bookingId,
                    "transactionType": constantUtil.PROFESSIONALEARNINGS,
                    "transactionAmount":
                      finalData.professionalCommision.toFixed(2),
                    "previousBalance": professionalData.wallet.availableAmount,
                    "currentBalance": newAvailableAmount,
                    "paidTransactionId": null,
                    "transactionId": `${helperUtil.randomString(8, "a")}`,
                    "transactionDate": new Date(),
                    "transactionReason": `${
                      EARNINGS + " " + bookingData.bookingId
                    }`,
                    "refBookingId": bookingData.bookingId,
                    "transactionStatus": constantUtil.SUCCESS,
                    "paymentType": constantUtil.CREDIT,
                    "from": adminTransactionData,
                    "to": professionalTransactionData,
                  },
                ],
                { session } // { "session": session }
              );
            }
            if (
              bookingData.payment.option === constantUtil.PAYMENTCASH ||
              bookingData.payment.option === constantUtil.CONST_PIX ||
              bookingData.payment.option === constantUtil.CONST_POS ||
              bookingData.payment.option === constantUtil.PAYMENTCREDIT //=>this for corporate beacuse in corpporate payment options use paymentcredit key word
            ) {
              // make transaction
              let paymentType = constantUtil.DEBIT;
              let newProfessionalCommision = (
                parseFloat(finalData.payableAmount) -
                parseFloat(finalData.professionalCommision)
              ).toFixed(2);
              if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
                //_____________________________ below ______________________________________________
                if (
                  generalSettings?.data?.isEnableCorporateRideWalletPay &&
                  bookingData.coorperate
                ) {
                  const updateCorporateRideWalletPay = "";
                  // let corporateData = await
                  let finalPayableAmount =
                    parseFloat(finalData.payableAmount.toFixed(2)) -
                    parseFloat(bookingData?.invoice?.payableAmount || 0);
                  if (finalPayableAmount > 0) {
                    await this.broker.emit(
                      "admin.updateWalletAmountTripEnded",
                      {
                        "corporateId": bookingData.coorperate._id.toString(),
                        "amount": finalPayableAmount,
                        "paymentType": constantUtil.DEBIT,
                        "paymentMethod": bookingData.payment.option,
                        "bookingId": bookingData._id?.toString(),
                        "refBookingId": bookingData.bookingId,
                        "transactionType": constantUtil.BOOKINGCHARGE,
                        "transactionReason":
                          BOOKING_PAID + " / " + bookingData.bookingId,
                      }
                    );
                  } else if (finalPayableAmount < 0) {
                    finalPayableAmount =
                      parseFloat(bookingData?.invoice?.payableAmount || 0) -
                      parseFloat(finalData.payableAmount.toFixed(2));
                    await this.broker.emit(
                      "admin.updateWalletAmountTripEnded",
                      {
                        "corporateId": bookingData.coorperate?._id?.toString(),
                        "amount": finalPayableAmount,
                        "paymentType": constantUtil.CREDIT,
                        "paymentMethod": bookingData.payment.option,
                        "bookingId": bookingData._id?.toString(),
                        "refBookingId": bookingData.bookingId,
                        "transactionType": constantUtil.BOOKINGCHARGE,
                        "transactionReason":
                          REFUND_TO_WALLET + " / " + bookingData.bookingId,
                      }
                    );
                  }
                  // corporateData = corporateData && corporateData[0];
                  // //-------- Transaction Entry Start ---------------------
                  // if (corporateData) {
                  //   this.broker.emit(
                  //     "transaction.corporateRideWalletPayTransaction",
                  //     {
                  //       "bookingId": context.params.bookingId?.toString(),
                  //       "refBookingId": bookingData.bookingId,
                  //       "corporateId": corporateData?._id?.toString(),
                  //       "corporateData": corporateData.data,
                  //       "amount": finalData.payableAmount,
                  //       "previousBalance": corporateData?.data?.amount || 0,
                  //       "currentBalance":
                  //         parseFloat(corporateData?.data?.amount || 0) -
                  //         parseFloat(finalData.payableAmount || 0),
                  //     }
                  //   );
                  // }
                  // //-------- Transaction Entry End ---------------------
                }
                //_____________________________ Above ______________________________________________
                if (
                  generalSettings.data
                    .isProfessionalsCorporateSiteCommissionCreditToWallet
                ) {
                  paymentType = constantUtil.CREDIT; //need to Revert
                  newProfessionalCommision =
                    finalData.professionalCommision.toFixed(2); //need to Revert
                } else {
                  // ----------- Zayride Start ------------
                  paymentType = constantUtil.DEBIT; //need to Revert
                  newProfessionalCommision = (
                    parseFloat(finalData.payableAmount) -
                    parseFloat(finalData.professionalCommision)
                  ).toFixed(2); //need to Revert
                  // ----------- Zayride End ------------
                }
              }
              if (finalData.payableAmount > finalData.professionalCommision) {
                // this.broker.emit("professional.updateWalletAmountTripEnded", {
                //   "professionalId": bookingData.professional._id.toString(),
                //   "amount": professionalCommision,
                //   "paymentMethod": bookingData.payment.option,
                // });
                let onlineStatus = true,
                  newAvailableAmount = 0;
                if (
                  bookingData.payment.option === constantUtil.PAYMENTWALLET ||
                  bookingData.payment.option === constantUtil.PAYMENTCARD ||
                  (bookingData.payment.option === constantUtil.PAYMENTCREDIT && //  need to Revert //Zayride
                    generalSettings.data
                      .isProfessionalsCorporateSiteCommissionCreditToWallet)
                ) {
                  newAvailableAmount =
                    parseFloat(professionalData.wallet.availableAmount) +
                    parseFloat(newProfessionalCommision);
                } else {
                  newAvailableAmount =
                    parseFloat(professionalData.wallet.availableAmount) -
                    parseFloat(newProfessionalCommision);
                }
                if (
                  parseFloat(generalSettings.data.minimumWalletAmountToOnline) >
                  parseFloat(newAvailableAmount)
                ) {
                  onlineStatus = false;
                }
                await this.schema.professionalModel.model.updateOne(
                  { "_id": mongoose.Types.ObjectId(professionalId) },
                  {
                    "$set": {
                      "wallet.availableAmount": newAvailableAmount,
                      "onlineStatus": onlineStatus,
                      "onlineStatusUpdatedAt": new Date(),
                    },
                  },
                  { session } //{ "session": session }
                );
                // this.broker.emit(
                //   "transaction.professionalSiteCommissionTransaction",
                //   {
                //     "professionalId": bookingData.professional._id.toString(),
                //     "amount": professionalCommision,
                //     "bookingId": bookingData._id.toString(),
                //     "refBookingId": bookingData.bookingId,
                //     "paymentType": paymentType,
                //     "previousBalance":
                //       bookingData?.professional?.wallet?.availableAmount,
                //     "currentBalance":
                //       parseFloat(
                //         bookingData?.professional?.wallet?.availableAmount
                //       ) + parseFloat(professionalCommision),
                //   }
                // );
                const transactionType =
                  paymentType === constantUtil.DEBIT
                    ? constantUtil.BOOKINGCHARGE
                    : constantUtil.PROFESSIONALEARNINGS;
                await this.schema.transactionModel.model.create(
                  [
                    {
                      "clientId": clientId,
                      "bookingId": context.params?.bookingId,
                      "transactionType": transactionType,
                      "transactionAmount": newProfessionalCommision,
                      "previousBalance":
                        professionalData.wallet.availableAmount,
                      "currentBalance": newAvailableAmount,
                      "paidTransactionId": null,
                      "transactionStatus": constantUtil.SUCCESS,
                      "transactionId": `${helperUtil.randomString(8, "a")}`,
                      "transactionReason": `${
                        transactionType + " " + bookingData.bookingId
                      }`,
                      "refBookingId": bookingData?.bookingId,
                      "transactionDate": new Date(),
                      "paymentType": paymentType, // CREDIT or DEBIT
                      "from": professionalTransactionData,
                      "to": adminTransactionData,
                    },
                  ],
                  { session } // { "session": session }
                );
              }

              if (finalData.payableAmount < finalData.professionalCommision) {
                // in cash payment user paied amount for a ride is lesser than professioanl commission so menas some coupons or some discount is applied so have reimpersed this differce amount

                // this.broker.emit("professional.updateWalletAmountTripEnded", {
                //   "professionalId": bookingData.professional._id.toString(),
                //   "amount": parseFloat(
                //     finalData.professionalCommision - finalData.payableAmount
                //   ),
                //   "paymentMethod": constantUtil.PAYMENTWALLET,
                // });
                let onlineStatus = true;
                const newAvailableAmount =
                  parseFloat(professionalData.wallet.availableAmount) +
                  parseFloat(
                    finalData.professionalCommision - finalData.payableAmount
                  );
                if (
                  parseFloat(generalSettings.data.minimumWalletAmountToOnline) >
                  parseFloat(newAvailableAmount)
                ) {
                  onlineStatus = false;
                }
                await this.schema.professionalModel.model.updateOne(
                  { "_id": mongoose.Types.ObjectId(professionalId) },
                  {
                    "$set": {
                      "wallet.availableAmount": newAvailableAmount,
                      "onlineStatus": onlineStatus,
                      "onlineStatusUpdatedAt": new Date(),
                    },
                  },
                  { session } // { "session": session }
                );
                // this.broker.emit(
                //   "transaction.professionalTolerenceAmountTransaction",
                //   {
                //     "professionalId": bookingData.professional._id.toString(),
                //     "amount": parseFloat(
                //       finalData.professionalCommision - finalData.payableAmount
                //     ).toFixed(2),
                //     "bookingId": bookingData._id.toString(),
                //     "refBookingId": bookingData.bookingId,
                //     "paymentType": paymentType,
                //     "previousBalance":
                //       bookingData?.professional?.wallet?.availableAmount,
                //     "currentBalance":
                //       parseFloat(
                //         bookingData?.professional?.wallet?.availableAmount
                //       ) +
                //       parseFloat(
                //         finalData.professionalCommision - finalData.payableAmount
                //       ),
                //   }
                // );
                await this.schema.transactionModel.model.create(
                  [
                    {
                      "clientId": clientId,
                      "bookingId": context.params?.bookingId,
                      "transactionType":
                        constantUtil.PROFESSIONALTOLERENCEAMOUNT,
                      "transactionAmount": parseFloat(
                        finalData.professionalCommision -
                          finalData.payableAmount
                      ).toFixed(2),
                      "previousBalance":
                        professionalData.wallet.availableAmount,
                      "currentBalance": newAvailableAmount,
                      "paidTransactionId": null,
                      "transactionId": `${helperUtil.randomString(8, "a")}`,
                      "transactionReason": `${
                        constantUtil.PROFESSIONALTOLERENCEAMOUNT +
                        " " +
                        bookingData?.bookingId
                      }`,
                      "refBookingId": bookingData?.bookingId,
                      "transactionStatus": constantUtil.SUCCESS,
                      "transactionDate": new Date(),
                      "paymentType": constantUtil.CREDIT,
                      "to": professionalTransactionData,
                      "from": adminTransactionData,
                    },
                  ],
                  { session } // { "session": session }
                );
              }
            }
          }
          //#region invite and earn
          const inviteAndEarnSettings = await storageUtil.read(
            constantUtil.INVITEANDEARN
          );
          if (inviteAndEarnSettings) {
            checkProfessionalRideCount = await this.adapter.model.count({
              "professional": mongoose.Types.ObjectId(professionalId),
              "bookingStatus": constantUtil.ENDED,
            });
            if (
              bookingData.guestType === constantUtil.USER &&
              bookingData.user
            ) {
              checkUserRideCount = await this.adapter.model.count({
                "user": mongoose.Types.ObjectId(userId),
                "bookingStatus": constantUtil.ENDED,
              });
            }
            // user
            if (userId && bookingData?.user?.referredBy) {
              let joiner = null,
                inviter = null;
              if (
                parseInt(checkUserRideCount) ===
                inviteAndEarnSettings?.data?.forUser?.rideCount
              ) {
                // this.broker.emit("user.inviteAndEarn", {
                //   "joinerId": bookingData.user._id.toString(),
                // });
                // joiner
                joiner = await this.schema.userModel.model
                  .findOne({ "_id": mongoose.Types.ObjectId(userId) })
                  .lean();
                // it means this user not referred by another user
                // if (!joiner?.referredBy && joiner?.referredBy === "") {
                //   return null;
                // }
                if (joiner?.referredBy) {
                  await this.schema.userModel.model.updateOne(
                    { "_id": mongoose.Types.ObjectId(userId) },
                    {
                      "$set": {
                        "wallet.availableAmount":
                          joiner.wallet.availableAmount +
                          inviteAndEarnSettings.data.forUser.amountToJoiner,
                        "joinAmount":
                          inviteAndEarnSettings.data.forUser.amountToJoiner,
                        "joinerRideCount":
                          (inviteAndEarnSettings?.data?.forUser?.rideCount ||
                            0) === 0
                            ? 0
                            : (joiner?.joinerRideCount || 0) + 1,
                      },
                    },
                    { session } //  { "session": session }
                  );
                  // if (joinerJson.nModified) {
                  //   this.broker.emit("transaction.joinCharge", {
                  //     "amount": inviteAndEarnSettings.data.forUser.amountToJoiner,
                  //     "userType": constantUtil.USER,
                  //     "data": {
                  //       "id": joiner._id.toString(),
                  //       "firstName": joiner.firstName,
                  //       "lastName": joiner.lastName,
                  //       "avatar": joiner.avatar,
                  //       "phone": {
                  //         "code": joiner.phone.code,
                  //         "number": joiner.phone.number,
                  //       },
                  //     },
                  //     "previousBalance": joiner.wallet.availableAmount,
                  //     "currentBalance":
                  //       joiner.wallet.availableAmount +
                  //       inviteAndEarnSettings.data.forUser.amountToJoiner,
                  //   });
                  // }
                  // const transId = `${helperUtil.randomString(8, "a")}`;
                  await this.schema.transactionModel.model.create(
                    [
                      {
                        "clientId": clientId,
                        "bookingId": context.params?.bookingId,
                        "transactionType": constantUtil.JOININGCHARGE,
                        "transactionAmount":
                          inviteAndEarnSettings.data.forUser.amountToJoiner,
                        "previousBalance": joiner.wallet.availableAmount,
                        "currentBalance":
                          joiner.wallet.availableAmount +
                          inviteAndEarnSettings.data.forUser.amountToJoiner,
                        "paidTransactionId": null,
                        "transactionId": Date.now(),
                        "transactionDate": new Date(),
                        "paymentType": constantUtil.CREDIT,
                        "transactionStatus": constantUtil.SUCCESS,
                        // "from": {
                        //   "userType": constantUtil.ADMIN,
                        //   "userId": null,
                        //   "name": null,
                        //   "avatar": null,
                        //   "phoneNumber": null,
                        // },
                        "from": adminTransactionData,
                        "to": {
                          "userType": constantUtil.USER,
                          "userId": joiner._id?.toString(),
                          "name":
                            (joiner.firstName || "") +
                            " " +
                            (joiner.lastName || ""),
                          "avatar": joiner.avatar || null,
                          "phoneNumber": joiner.phone.number || null,
                        },
                      },
                    ],
                    { session } // { "session": session }
                  );
                }
                // invitor
                if (joiner?.referredBy) {
                  inviter = await this.schema.userModel.model
                    .findOne({ "uniqueCode": joiner.referredBy.toString() })
                    .lean();
                }
                // if (!inviter) return null;
                if (inviter) {
                  const referrelTotalAmount =
                    (inviter?.referrelTotalAmount ?? 0) +
                    inviteAndEarnSettings.data.forUser.amountToInviter;

                  await this.schema.userModel.model.updateOne(
                    { "uniqueCode": joiner.referredBy.toString() },
                    {
                      "$set": {
                        "wallet.availableAmount":
                          inviter.wallet.availableAmount +
                          inviteAndEarnSettings.data.forUser.amountToInviter,
                        "referrelTotalAmount": referrelTotalAmount,
                        "joinCount": (inviter?.joinCount ?? 0) + 1,
                        "invitationCount":
                          (inviteAndEarnSettings?.data?.forProfessional
                            ?.rideCount || 0) === 0
                            ? 1
                            : inviter?.invitationCount,
                      },
                    },
                    { session } // { "session": session }
                  );
                  // if (inviterJson.nModified) {
                  //   this.broker.emit("transaction.inviteCharge", {
                  //     "amount": parseFloat(
                  //       inviteAndEarnSettings.data.forUser.amountToInviter.toFixed(
                  //         2
                  //       )
                  //     ),
                  //     "userType": constantUtil.USER,
                  //     "data": {
                  //       "id": inviter._id.toString(),
                  //       "firstName": inviter.firstName,
                  //       "lastName": inviter.lastName,
                  //       "avatar": inviter.avatar,
                  //       "phone": {
                  //         "code": inviter.phone.code,
                  //         "number": inviter.phone.number,
                  //       },
                  //     },
                  //     "previousBalance": inviter.wallet.availableAmount,
                  //     "currentBalance":
                  //       inviter.wallet.availableAmount +
                  //       inviteAndEarnSettings.data.forUser.amountToInviter,
                  //   });
                  // }
                  await this.schema.transactionModel.model.create(
                    [
                      {
                        "clientId": clientId,
                        "bookingId": context.params?.bookingId,
                        "transactionType": constantUtil.INVITECHARGE,
                        "transactionAmount": parseFloat(
                          inviteAndEarnSettings.data.forUser.amountToInviter?.toFixed(
                            2
                          )
                        ),
                        "previousBalance": inviter.wallet.availableAmount,
                        "currentBalance":
                          inviter.wallet.availableAmount +
                          inviteAndEarnSettings.data.forUser.amountToInviter,
                        "paidTransactionId": null,
                        "transactionId": Date.now(),
                        "transactionDate": new Date(),
                        "paymentType": constantUtil.CREDIT,
                        "transactionStatus": constantUtil.SUCCESS,
                        // "from": {
                        //   "userType": constantUtil.ADMIN,
                        //   "userId": null,
                        //   "name": null,
                        //   "avatar": null,
                        //   "phoneNumber": null,
                        // },
                        "from": adminTransactionData,
                        "to": {
                          "userType": constantUtil.USER,
                          "userId": inviter._id.toString(),
                          "name":
                            (inviter.firstName || "") +
                            " " +
                            (inviter.lastName || ""),
                          "avatar": inviter.avatar || null,
                          "phoneNumber": inviter.phone.number || null,
                        },
                      },
                    ],
                    { session } // { "session": session }
                  );
                }
              } else if (
                parseInt(checkUserRideCount) <
                inviteAndEarnSettings?.data?.forUser?.rideCount
              ) {
                // this.broker.emit("user.updateJoinerRideCount", {
                //   "joinerId": bookingData.user._id.toString(),
                // });
                try {
                  // joiner
                  joiner = await this.schema.userModel.model
                    .findOne(
                      { "_id": mongoose.Types.ObjectId(userId) },
                      { "_id": 1, "referredBy": 1, "joinerRideCount": 1 }
                    )
                    .lean();
                  // it means this user not referred by another user
                  // if (!joiner?.referredBy && joiner?.referredBy === "") {
                  //   return constantUtil.SUCCESS;
                  // }
                  if (joiner?.referredBy) {
                    await this.schema.userModel.model.updateOne(
                      { "_id": mongoose.Types.ObjectId(userId) },
                      {
                        "$set": {
                          "joinerRideCount": (joiner?.joinerRideCount || 0) + 1,
                        },
                      },
                      { session } // { "session": session }
                    );
                  }
                } catch (e) {}
              }
            }
            //professional
            if (professionalId && bookingData?.professional?.referredBy) {
              let joiner = null,
                inviter = null;
              if (
                parseInt(checkProfessionalRideCount) ===
                inviteAndEarnSettings?.data?.forProfessional?.rideCount
              ) {
                // this.broker.emit("professional.inviteAndEarn", {
                //   "joinerId": bookingData.professional._id.toString(),
                // });
                // joiner
                joiner = await this.schema.professionalModel.model
                  .findOne({ "_id": mongoose.Types.ObjectId(professionalId) })
                  .lean();
                // it means this professional not referred by another professional
                // if (!joiner?.referredBy && joiner?.referredBy === "") {
                //   // if joner not found we dont know who is reffered
                //   return null;
                // }
                if (joiner?.referredBy) {
                  const joinerJson =
                    await this.schema.professionalModel.model.updateOne(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      {
                        "$set": {
                          "$inc": {
                            "wallet.availableAmount":
                              inviteAndEarnSettings.data.forProfessional
                                .amountToJoiner,
                          },
                          "joinAmount":
                            inviteAndEarnSettings.data.forProfessional
                              .amountToJoiner,
                          "joinerRideCount":
                            (inviteAndEarnSettings?.data?.forProfessional
                              ?.rideCount || 0) === 0
                              ? 0
                              : (joiner?.joinerRideCount || 0) + 1,
                        },
                      },
                      { session } // { "session": session }
                    );
                  // if (joinerJson.nModified) {
                  //   this.broker.emit("transaction.joinCharge", {
                  //     "amount":
                  //       inviteAndEarnSettings.data.forProfessional.amountToJoiner,
                  //     "userType": constantUtil.PROFESSIONAL,
                  //     "data": {
                  //       "id": joiner._id.toString(),
                  //       "firstName": joiner.firstName,
                  //       "lastName": joiner.lastName,
                  //       "avatar": joiner.avatar,
                  //       "phone": {
                  //         "code": joiner.phone.code,
                  //         "number": joiner.phone.number,
                  //       },
                  //     },
                  //     "previousBalance": joiner.wallet.availableAmount,
                  //     "currentBalance":
                  //       joiner.wallet.availableAmount +
                  //       inviteAndEarnSettings.data.forProfessional.amountToJoiner,
                  //   });
                  // }
                  await this.schema.transactionModel.model.create(
                    [
                      {
                        "clientId": clientId,
                        "bookingId": context.params?.bookingId,
                        "transactionType": constantUtil.JOININGCHARGE,
                        "transactionAmount":
                          inviteAndEarnSettings.data.forProfessional
                            .amountToJoiner,
                        "previousBalance": joiner.wallet.availableAmount,
                        "currentBalance":
                          joiner.wallet.availableAmount +
                          inviteAndEarnSettings.data.forProfessional
                            .amountToJoiner,
                        "paidTransactionId": null,
                        "transactionId": Date.now(),
                        "transactionDate": new Date(),
                        "paymentType": constantUtil.CREDIT,
                        "transactionStatus": constantUtil.SUCCESS,
                        // "from": {
                        //   "userType": constantUtil.ADMIN,
                        //   "userId": null,
                        //   "name": null,
                        //   "avatar": null,
                        //   "phoneNumber": null,
                        // },
                        "from": adminTransactionData,
                        "to": {
                          "userType": constantUtil.PROFESSIONAL,
                          "userId": joiner._id.toString(),
                          "name":
                            (joiner.firstName || "") +
                            " " +
                            (joiner.lastName || ""),
                          "avatar": joiner.avatar || null,
                          "phoneNumber": joiner.phone.number || null,
                        },
                      },
                    ],
                    { session } //{ "session": session }
                  );
                }
                // inviter
                inviter = await this.schema.professionalModel.model
                  .findOne({ "uniqueCode": joiner.referredBy })
                  .lean();
                // if (!inviter) {
                //   return null;
                // }
                if (inviter) {
                  const inviterWalletAmount =
                    inviter.wallet.availableAmount +
                    inviteAndEarnSettings.data.forProfessional.amountToInviter;

                  const referrelTotalAmount =
                    (inviter?.referrelTotalAmount ?? 0) +
                    inviteAndEarnSettings.data.forProfessional.amountToInviter;

                  const inviterJson =
                    await this.schema.professionalModel.model.updateOne(
                      {
                        "_id": mongoose.Types.ObjectId(inviter._id.toString()),
                      },
                      {
                        "$set": {
                          "wallet.availableAmount": inviterWalletAmount,
                          "referrelTotalAmount": referrelTotalAmount,
                          "joinCount": (inviter?.joinCount ?? 0) + 1,
                          "invitationCount":
                            (inviteAndEarnSettings?.data?.forProfessional
                              ?.rideCount || 0) === 0
                              ? 1
                              : inviter?.invitationCount,
                        },
                      },
                      { session } //{ "session": session }
                    );
                  // if (inviterJson.nModified) {
                  //   this.broker.emit("transaction.inviteCharge", {
                  //     "amount":
                  //       inviteAndEarnSettings.data.forProfessional
                  //         .amountToInviter,
                  //     "userType": constantUtil.PROFESSIONAL,
                  //     "data": {
                  //       "id": inviter._id.toString(),
                  //       "firstName": inviter.firstName,
                  //       "lastName": inviter.lastName,
                  //       "avatar": inviter.avatar,
                  //       "phone": {
                  //         "code": inviter.phone.code,
                  //         "number": inviter.phone.number,
                  //       },
                  //     },
                  //     "previousBalance": inviter.wallet.availableAmount,
                  //     "currentBalance":
                  //       inviter.wallet.availableAmount +
                  //       inviteAndEarnSettings.data.forProfessional
                  //         .amountToInviter,
                  //   });
                  // }
                  await this.schema.transactionModel.model.create(
                    [
                      {
                        "clientId": clientId,
                        "bookingId": context.params?.bookingId,
                        "transactionType": constantUtil.INVITECHARGE,
                        "transactionAmount":
                          inviteAndEarnSettings.data.forProfessional
                            .amountToInviter,
                        "previousBalance": inviter.wallet.availableAmount,
                        "currentBalance":
                          inviter.wallet.availableAmount +
                          inviteAndEarnSettings.data.forProfessional
                            .amountToInviter,
                        "paidTransactionId": null,
                        "transactionId": Date.now(),
                        "transactionDate": new Date(),
                        "paymentType": constantUtil.CREDIT,
                        "transactionStatus": constantUtil.SUCCESS,
                        // "from": {
                        //   "userType": constantUtil.ADMIN,
                        //   "userId": null,
                        //   "name": null,
                        //   "avatar": null,
                        //   "phoneNumber": null,
                        // },
                        "from": adminTransactionData,
                        "to": {
                          "userType": constantUtil.PROFESSIONAL,
                          "userId": inviter._id?.toString(),
                          "name":
                            (inviter.firstName || "") +
                            " " +
                            (inviter.lastName || ""),
                          "avatar": inviter.avatar || null,
                          "phoneNumber": inviter.phone.number || null,
                        },
                      },
                    ],
                    { session } // { "session": session }
                  );
                }
              } else if (
                parseInt(checkProfessionalRideCount) <
                inviteAndEarnSettings?.data?.forProfessional?.rideCount
              ) {
                // this.broker.emit("professional.updateJoinerRideCount", {
                //   "joinerId": bookingData.professional._id.toString(),
                // });
                try {
                  // joiner
                  joiner = await this.schema.professionalModel.model
                    .findOne(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      { "_id": 1, "referredBy": 1, "joinerRideCount": 1 }
                    )
                    .lean();
                  // it means this professional not referred by another professional
                  // if (!joiner?.referredBy && joiner?.referredBy === "") {
                  //   // if joner not found we dont know who is reffered
                  //   return constantUtil.SUCCESS;
                  // }
                  if (joiner?.referredBy) {
                    await this.schema.professionalModel.model.updateOne(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      {
                        "$set": {
                          "joinerRideCount": (joiner?.joinerRideCount || 0) + 1,
                        },
                      },
                      { session } // { "session": session }
                    );
                  }
                } catch (e) {}
              }
            }
          }
          //#endregion invite and earn
        }
        switch (bookingData.serviceCategory?.toLowerCase()) {
          case constantUtil.CONST_SHARERIDE.toLowerCase():
            {
              // await this.broker.emit("professional.updateProfessionalShareRide", {
              //   "actionStatus": professionalStatus, // constantUtil.ENDED,
              //   "bookingId": bookingData?._id?.toString(),
              //   "professionalId": bookingData?.professional?._id?.toString(),
              // });
              switch (professionalStatus?.toUpperCase()) {
                case constantUtil.STATUS_UPCOMING:
                  await this.schema.professionalModel.model
                    .findOneAndUpdate(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      {
                        "$push": {
                          "bookingInfo.upcomingBooking": {
                            "bookingId": context.params.bookingId,
                            // "bookingDate": new Date(context.params.bookingDate),
                            // "estimationTime": context.params.estimationTime,
                          },
                        },
                      },
                      { session } //  { "session": session }
                      // { "new": true, "session": sessionSchema }
                    )
                    .lean();
                  break;

                case constantUtil.STATUS_ONGOING:
                  await this.schema.professionalModel.model
                    .findOneAndUpdate(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      {
                        "bookingInfo.ongoingBooking": context.params?.bookingId,
                        // "$pull": {
                        //   "bookingInfo.upcomingBooking": {
                        //     "bookingId": mongoose.Types.ObjectId(context.params.bookingId),
                        //     // "bookingDate": new Date(context.params.bookingDate),
                        //     // "estimationTime": context.params.estimationTime,
                        //   },
                        // },
                      },
                      { session } // { "session": session }
                      //  { "new": true, "session": sessionSchema }
                    )
                    .lean();
                  break;

                case constantUtil.ENDED:
                case constantUtil.CANCELLED:
                case constantUtil.USERCANCELLED:
                case constantUtil.PROFESSIONALCANCELLED:
                  await this.schema.professionalModel.model
                    .findOneAndUpdate(
                      { "_id": mongoose.Types.ObjectId(professionalId) },
                      {
                        "$pull": {
                          "bookingInfo.upcomingBooking": {
                            "bookingId": mongoose.Types.ObjectId(
                              context.params.bookingId
                            ),
                            // "bookingDate": new Date(context.params.bookingDate),
                            // "estimationTime": context.params.estimationTime,
                          },
                        },
                      },
                      { session } //  { "session": session }
                      //  { "new": true, "session": session }
                    )
                    .lean();
                  break;
              }
              const ongoingShareRideAction =
                professionalStatus === constantUtil.ENDED
                  ? constantUtil.CONST_DROP
                  : constantUtil.STATUS_UPCOMING;
              // await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
              //   "actionStatus": ongoingShareRideAction,
              //   "bookingId":
              //     ongoingShareRideAction === constantUtil.CONST_DROP
              //       ? null
              //       : bookingData?._id?.toString(),
              //   "professionalId": bookingData.professional._id.toString(),
              //   "serviceCategory": bookingData.serviceCategory,
              //   "lastRidePassengerCount":
              //     ongoingShareRideAction === constantUtil.CONST_DROP
              //       ? bookingData?.passengerCount
              //       : 0,
              //   "lat": context.params?.latLng?.lat,
              //   "lng": context.params?.latLng?.lng,
              //   "parentShareRideId": bookingData?.parentShareRideId,
              //   // "passengerCount":
              //   //   ongoingShareRideAction === constantUtil.CONST_DROP
              //   //     ? bookingData?.passengerCount
              //   //     : 0,
              // });
              let nextOngoingBookingId = null;
              // find next shortest path Child Share ride
              const nextOngoingChildShardRide = await this.adapter.model
                .findOne({
                  // "parentShareRideId": { "$nin": null },
                  "_id": {
                    "$nin": mongoose.Types.ObjectId(context.params.bookingId),
                  },
                  "isShareRide": true,
                  "professional": mongoose.Types.ObjectId(professionalId),
                  "serviceCategory": constantUtil.CONST_SHARERIDE.toLowerCase(),
                  // ...ridePickupCheckQuery,
                  "bookingStatus": {
                    "$in": [
                      constantUtil.ACCEPTED,
                      constantUtil.ARRIVED,
                      constantUtil.STARTED,
                    ],
                  },
                  "upcomingLocation": {
                    // "$nearSphere": {
                    "$near": {
                      "$geometry": {
                        "type": "Point",
                        // "coordinates": [context.params.lng, context.params.lat],
                        "coordinates": [
                          context.params?.latLng?.lng,
                          context.params?.latLng?.lat,
                        ],
                      },
                    },
                  },
                })
                .lean();
              // // find next shortest path Parent Share ride
              if (nextOngoingChildShardRide) {
                nextOngoingBookingId =
                  nextOngoingChildShardRide?._id?.toString(); // next ongoing Child Ride
                // } else if (
                //   nextOngoingChildShardRide === null &&
                //   nextOngoingParentShardRide
                // ) {
                //   bookingId = nextOngoingParentShardRide?._id?.toString(); // next ongoing Parent Ride
              }
              // else if (context.params.bookingId) {
              //   nextOngoingBookingId = context.params.bookingId;
              // }
              // Update current passanger count
              if (ongoingShareRideAction === constantUtil.CONST_DROP) {
                await this.schema.bookingModel.model.updateOne(
                  {
                    // "_id": mongoose.Types.ObjectId(bookingId?.toString()),
                    "_id": mongoose.Types.ObjectId(
                      bookingData?.parentShareRideId
                    ),
                    "parentShareRideId": null,
                    "isShareRide": true,
                    "professional": mongoose.Types.ObjectId(professionalId),
                    "serviceCategory":
                      constantUtil.CONST_SHARERIDE.toLowerCase(),
                  },
                  {
                    "$set": {
                      "$inc": {
                        "currentPassengerCount": -parseInt(
                          bookingData?.passengerCount || 0
                        ),
                      },
                    },
                  },
                  { session }
                );
              }
              // // if (bookingId) {
              // shareRideProfessional = await this.broker.emit(
              //   "professional.updateProfessionalShareRide",
              //   {
              //     "actionStatus": constantUtil.STATUS_ONGOING,
              //     "bookingId": bookingId, // next ongoing Ride
              //     "professionalId": professionalId,
              //     // "pickUpLat": null,
              //     // "pickUpLng": null,
              //     // "dropLat": null,
              //     // "dropLng": null,
              //   }
              // );
              await this.schema.professionalModel.model
                .updateOne(
                  { "_id": mongoose.Types.ObjectId(professionalId) },
                  {
                    "bookingInfo.ongoingBooking": nextOngoingBookingId,
                    // "$pull": {
                    //   "bookingInfo.upcomingBooking": {
                    //     "bookingId": mongoose.Types.ObjectId(context.params.bookingId),
                    //     // "bookingDate": new Date(context.params.bookingDate),
                    //     // "estimationTime": context.params.estimationTime,
                    //   },
                    // },
                  },
                  { session } //  { "session": session }
                )
                .lean();
            }
            break;
        }
        // // Get booking Details
        // bookingData = await this.getBookingDetailsById({
        //   "bookingId": context.params.bookingId.toString(),
        // }); // Get booking Details
      }
      //#region Packages
      if (
        bookingData.serviceCategory ===
        constantUtil.CONST_PACKAGES.toLowerCase()
      ) {
        // this.broker.emit("booking.updatePackageDetailsById", {
        //   "action": constantUtil.UPDATE,
        //   "uploadedBy": context.params.uploadedBy,
        //   "packageData": context.params.packageData || {},
        //   "bookingId": bookingData?._id?.toString(),
        // });
        // let isSuccess = true;
        try {
          let updateImageQuery = null;
          switch (context.params.uploadedBy?.toUpperCase()) {
            case constantUtil.ADMIN:
              break;

            case constantUtil.USER:
              updateImageQuery = {
                "$set": {
                  "packageData.userImage":
                    context.params.packageData.userImage || [],
                  "packageData.userImageUploadedAt": new Date(),
                },
              };
              break;

            case constantUtil.PROFESSIONAL:
              updateImageQuery = {
                "$set": {
                  "packageData.professionalImagePickup":
                    context.params.packageData.professionalImagePickup || [],
                  "packageData.professionalPickupedAt": new Date(),
                },
              };
              break;

            case constantUtil.PACKAGEDELIVERY:
              updateImageQuery = {
                "$set": {
                  "packageData.professionalImageDelivery":
                    context.params.packageData.professionalImageDelivery || [],
                  "packageData.professionaDeliveredAt": new Date(),
                  // "packageData.packageReceiver":
                  //   context.params.packageData.packageReceiver || [],
                  // "packageData.packageReceivedAt": new Date(),
                },
              };
              break;

            case constantUtil.PACKAGEDELIVERYANDRECEIVER:
              updateImageQuery = {
                "$set": {
                  "packageData.professionalImageDelivery":
                    context.params.packageData.professionalImageDelivery || [],
                  "packageData.professionaDeliveredAt": new Date(),
                  "packageData.packageReceiver":
                    context.params.packageData.packageReceiver || [],
                  "packageData.packageReceivedAt": new Date(),
                },
              };
              break;

            case constantUtil.PACKAGERECEIVER:
              updateImageQuery = {
                "$set": {
                  "packageData.packageReceiver":
                    context.params.packageData.packageReceiver || [],
                  "packageData.packageReceivedAt": new Date(),
                },
              };
              break;
          }
          if (context.params.action === constantUtil.ADD) {
            //Update Package Details
            // const updatedPackageData = await this.adapter.model.updateOne(
            await this.schema.bookingModel.model.updateOne(
              {
                "_id": mongoose.Types.ObjectId(
                  context.params.bookingId.toString()
                ),
              },
              {
                "$set": {
                  "packageData.recipientNo":
                    context.params?.packageData?.recipientNo || null,
                  "packageData.recipientPhoneCode":
                    context.params?.packageData?.recipientPhoneCode || null,
                  "packageData.recipientPhoneNo":
                    context.params?.packageData?.recipientPhoneNo || null,
                  "packageData.recipientName":
                    context.params?.packageData?.recipientName || null,
                  "packageData.isEnableOTPVerification":
                    context.params?.packageData?.isEnableOTPVerification ||
                    false,
                  "packageData.isEnableNoContactDelivery":
                    context.params?.packageData?.isEnableNoContactDelivery ||
                    false,
                  "packageData.isEnablePetsAtMyHome":
                    context.params?.packageData?.isEnablePetsAtMyHome || false,
                  "packageData.selectedPackageContent":
                    context.params?.packageData?.selectedPackageContent || null,
                  "packageData.instruction":
                    context.params?.packageData?.instruction || null,
                },
              },
              { session } //  { "session": session }
            );
          }
          //Update Image Details
          if (updateImageQuery) {
            // const updatedPackageImageData = await this.adapter.model.updateOne(
            await this.schema.bookingModel.model.updateOne(
              {
                "_id": mongoose.Types.ObjectId(
                  context.params.bookingId.toString()
                ),
              },
              { ...updateImageQuery },
              { session } //   { "session": session }
            );
          }
        } catch (e) {
          // isSuccess = false;
        }
        // return isSuccess;
      }
      const x = 10;
      x = 5;
      //#endregion Packages
      // await sessionSchema.commitTransaction();
      await session.commitTransaction();
      // Get booking Details
      bookingData = await this.getBookingDetailsById({
        "bookingId": context.params.bookingId.toString(),
      }); // Get booking Details
      isSuccessResult = true;
    }, transactionOptions);
  } catch (error) {
    console.log("error" + error.message);
    const responseData = {
      "errorMessage": error.message,
      "errorDetails": error.stack,
    };
    isSuccessResult = false;
    // await session.abortTransaction();
    // await sessionSchema.abortTransaction();
    this.broker.emit("log.manageLog", {
      "name": constantUtil.ERRORDATA,
      "type": constantUtil.ERRORDATA,
      "userType": constantUtil.ADMIN,
      "reason": error.message,
      "trackData": responseData,
    });
  } finally {
    session.endSession();
    // sessionSchema.endSession();
  }
  responseData = bookingObject(bookingData);
  //#region Send Notification
  if (isSuccessResult) {
    const bookingStatusAction =
      bookingData.bookingStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_PROFESSIONALARRIVED
        : bookingData.bookingStatus === constantUtil.STARTED
        ? constantUtil.ACTION_PROFESSIONALSTARTED
        : bookingData.bookingStatus === constantUtil.ENDED
        ? constantUtil.ACTION_BOOKINGENDED
        : "";
    //-------- Send SMS Alert Start --------------------------
    // if (
    //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
    //   bookingData.bookingBy === constantUtil.ADMIN ||
    //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
    //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
    // )
    {
      await this.broker.emit("booking.sendSMSAlertByRideId", {
        "rideId": context.params.bookingId.toString(),
        "bookingFrom": bookingData.bookingFrom,
        "notifyType": bookingData.bookingStatus,
        "notifyFor": constantUtil.USER,
      });
    }
    //-------- Send SMS Alert End --------------------------
    // if (bookingData.bookingBy === constantUtil.USER) {
    if (bookingData?.user?._id) {
      const notificationObject = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingStatusAction,
          "timestamp": Date.now(),
          "message":
            bookingStatusAction === constantUtil.ACTION_PROFESSIONALARRIVED
              ? PROFESSIONAL_ARRIVED
              : bookingStatusAction === constantUtil.ACTION_PROFESSIONALSTARTED
              ? PROFESSIONAL_STARTED
              : TRIP_END,
          "details": lzStringEncode(responseData),
        },
        "registrationTokens": [
          {
            "token": bookingData?.user?.deviceInfo[0]?.deviceId || "",
            "id": bookingData?.user?._id?.toString() || "",
            "deviceType": bookingData?.user?.deviceInfo[0]?.deviceType || "",
            "platform": bookingData?.user?.deviceInfo[0]?.platform || "",
            "socketId": bookingData?.user?.deviceInfo[0]?.socketId || "",
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      await this.broker.emit("socket.sendNotification", notificationObject);
      // this.broker.emit('socket.statusChangeEvent', notificationObject)
      /* FCM */
      await this.broker.emit("admin.sendFCM", notificationObject);
      /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
      // if (generalSettings.data.isEnableWebAppBooking) {
      this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
        "bookingId": bookingData._id,
        "bookingStatus": bookingData.bookingStatus,
        "dateTime": bookingData.updatedAt,
        "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
        "bookingInfo": bookingData?.user?.bookingInfo || {},
      });
      // }
    }
    // if (paymentType === constantUtil.CARD) {
    if (isCancelCapturePaymentIntents) {
      const gatewayData = {
        gateWay: constantUtil.STRIPE,
        isPreAuthRequired: false,
      }; // Need to Change
      const inputParams = {
        ...context.params,
        "paymentData": gatewayData,
        "userData": userData,
        "paymentInitId": context.params?.paymentInitId,
      };
      switch (gatewayData.gateWay) {
        case constantUtil.STRIPE:
          // if (gatewayData.gateWay === constantUtil.STRIPE) {
          //If isPreAuthRequired is True
          if (gatewayData.isPreAuthRequired === true) {
            await this.broker.emit(
              "transaction.stripe.cancelCapturePaymentIntents",
              {
                ...inputParams,
              }
            );
          }
          break;

        case CONST_MERCADOPAGO:
          await this.broker.emit(
            "transaction.mercadopago.cancelCapturePaymentIntents",
            {
              ...inputParams,
            }
          );
          break;
      }
    }
    // }
    if (bookingData?.user?._id && isSendWalletNotification) {
      const notificationObjectWallet = {
        "temprorySound": true,
        "clientId": clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": constantUtil.WALLET,
          "timestamp": Date.now(),
          "message": INFO_CARD_DECLINED || "",
          "details": lzStringEncode({
            "message": INFO_CARD_DECLINED || "",
          }),
        },
        "registrationTokens": [
          {
            "token": bookingData?.user?.deviceInfo[0]?.deviceId || "",
            "id": bookingData?.user?._id?.toString() || "",
            "deviceType": bookingData?.user?.deviceInfo[0]?.deviceType || "",
            "platform": bookingData?.user?.deviceInfo[0]?.platform || "",
            "socketId": bookingData?.user?.deviceInfo[0]?.socketId || "",
          },
        ],
      };
      /* SOCKET PUSH NOTIFICATION */
      await this.broker.emit(
        "socket.sendNotification",
        notificationObjectWallet
      );
      /* FCM */
      await this.broker.emit("admin.sendFCM", notificationObjectWallet);
    }
    switch (bookingData.serviceCategory?.toLowerCase()) {
      case constantUtil.CONST_SHARERIDE.toLowerCase():
        {
          this.broker.emit("booking.sendNotificationActionShareRideStatus", {
            "bookingId": context.params.bookingId.toString(),
            "parentShareRideId": bookingData.parentShareRideId?.toString(),
            "userType": constantUtil.USER,
            "actionStatus": bookingData.bookingStatus,
            "notificationMessage":
              bookingStatusAction === constantUtil.ACTION_PROFESSIONALARRIVED
                ? ALERT_FELLOW_PASSENGER_RIDE_ARRIVED
                : bookingStatusAction ===
                  constantUtil.ACTION_PROFESSIONALSTARTED
                ? ALERT_FELLOW_PASSENGER_RIDE_STARTED
                : ALERT_FELLOW_PASSENGER_RIDE_ENDED,
          });
        }
        break;
    }
    // send mail
    if (generalSettings.data.emailConfigurationEnable === true) {
      // SEND TO PROFESSIONAL
      if (bookingData.professional.email) {
        this.broker.emit("admin.sendServiceMail", {
          "templateName": "professionalInvoiceMailTemplate",
          ...mappingUtil.professionalInvoiceMailObject(bookingData),
        });
      }
      // SEND TO USER
      if (bookingData.user) {
        this.broker.emit("admin.sendServiceMail", {
          "templateName": "userInvoiceMailTemplate",
          ...mappingUtil.userInvoiceMailObject(bookingData),
        });
      }
      if (bookingData.coorperate?.data?.email) {
        // SEND TO CORPORATE
        this.broker.emit("admin.sendServiceMail", {
          "templateName": "corporateInvoiceMailTemplate",
          ...mappingUtil.corporateInvoiceMailObject(bookingData),
        });
      }
    }
  }
  //#endregion Send Notification
  if (isSuccessResult) {
    return { "code": 200, "data": responseData, "message": "" };
  } else {
    return {
      "code": 400,
      "data": {},
      "message": "Failed............",
    };
  }
};

// bookingV2.bookingStatusChange = async function (context) {
//   const professionalStatus = context.params.professionalStatus.toUpperCase();
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   //------------- Notification Msg Start ---------
//   const {
//     INVALID_BOOKING,
//     VEHICLE_CATEGORY_NOT_FOUND,
//     BOOKING_UPDATE_STATUS_FAILED,
//     PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION,
//     INFO_OPERATOR_STATUS_ARRIVED,
//     INFO_OPERATOR_STATUS_START,
//     PROFESSIONAL_RIDE_START_INFO,
//   } = notifyMessage.setNotifyLanguage(
//     context.meta?.professionalDetails?.languageCode
//   );
//   //------------- Notification Msg End ---------
//   let bookingData = await this.adapter.model
//     .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
//     .lean();

//   if (!bookingData) throw new MoleculerError(INVALID_BOOKING);
//   let vehicleCategoryData;
//   const vehicleCategoryDataSet = await storageUtil.read(
//     constantUtil.VEHICLECATEGORY
//   );
//   if (!vehicleCategoryDataSet)
//     throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);

//   if (
//     (bookingData.bookingStatus === constantUtil.ACCEPTED &&
//       professionalStatus === constantUtil.ARRIVED) ||
//     (bookingData.bookingStatus === constantUtil.ARRIVED &&
//       professionalStatus === constantUtil.STARTED)
//   ) {
//     bookingData.activity.rideStops = bookingData.activity.rideStops.map(
//       (stop) => {
//         if (
//           professionalStatus === constantUtil.STARTED &&
//           stop.type === constantUtil.ORIGIN
//         ) {
//           stop.status = constantUtil.ENDED;
//         }
//         return stop;
//       }
//     );
//     const timeData =
//       professionalStatus === constantUtil.ARRIVED
//         ? {
//             "activity.arriveTime": new Date(),
//             "regionalData.arriveTime": helperUtil.toRegionalUTC(new Date()),
//           }
//         : professionalStatus === constantUtil.STARTED
//         ? {
//             "activity.pickUpTime": new Date(),
//             "activity.isUserPickedUp": true,
//             "regionalData.pickUpTime": helperUtil.toRegionalUTC(new Date()),
//           }
//         : "";
//     const updateBookingData = await this.adapter.model.updateOne(
//       {
//         "_id": context.params.bookingId,
//         "bookingStatus":
//           professionalStatus === constantUtil.ARRIVED
//             ? constantUtil.ACCEPTED
//             : professionalStatus === constantUtil.STARTED
//             ? constantUtil.ARRIVED
//             : "",
//       },
//       {
//         "bookingStatus": professionalStatus,
//         "activity.rideStops": bookingData.activity.rideStops,
//         ...timeData,
//       }
//     );

//     if (!updateBookingData.nModified)
//       throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);

//     bookingData = await this.adapter.model
//       .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
//       .populate("user", {
//         "password": 0,
//         "bankDetails": 0,
//         "trustedContacts": 0,
//         "cards": 0,
//       })
//       .populate("professional", {
//         "password": 0,
//         "cards": 0,
//         "bankDetails": 0,
//         "trustedContacts": 0,
//       })
//       .populate("category")
//       .populate("security")
//       .populate("officer", { "password": 0, "preferences": 0 })
//       .lean();

//     // bookingData = bookingData.toJSON();

//     vehicleCategoryData =
//       vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

//     bookingData.vehicle.vehicleCategoryName =
//       vehicleCategoryData.vehicleCategory;
//     bookingData.vehicle.vehicleCategoryImage =
//       vehicleCategoryData.categoryImage;
//     bookingData.vehicle.vehicleCategoryMapImage =
//       vehicleCategoryData.categoryMapImage;
//     // bookingData.distanceUnit = bookingData.category.distanceType;
//     bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
//     bookingData["imageURLPath"] =
//       generalSettings.data.spaces.spacesBaseUrl +
//       "/" +
//       generalSettings.data.spaces.spacesObjectName;

//     const bookingendAction =
//       bookingData.bookingStatus === constantUtil.ARRIVED
//         ? constantUtil.ACTION_PROFESSIONALARRIVED
//         : bookingData.bookingStatus === constantUtil.STARTED
//         ? constantUtil.ACTION_PROFESSIONALSTARTED
//         : "";

//     // if (bookingData.bookingBy === constantUtil.USER) {
//     if (bookingData?.user?._id) {
//       const notificationObject = {
//         "data": {
//           "type": constantUtil.NOTIFICATIONTYPE,
//           "userType": constantUtil.USER,
//           "action": bookingendAction,
//           "timestamp": Date.now(),
//           "message":
//             bookingendAction === constantUtil.ACTION_PROFESSIONALARRIVED
//               ? PROFESSIONAL_RIDE_ARRIVED_NOTIFICATION
//               : PROFESSIONAL_RIDE_START_INFO,
//           "details": lzStringEncode(bookingObject(bookingData)),
//         },
//         "registrationTokens": [
//           {
//             "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
//             "id": bookingData.user?._id?.toString(),
//             "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
//             "platform": bookingData.user?.deviceInfo[0]?.platform || "",
//             "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
//           },
//         ],
//       };

//       /* SOCKET PUSH NOTIFICATION */
//       this.broker.emit("socket.sendNotification", notificationObject);
//       // this.broker.emit('socket.statusChangeEvent', notificationObject)

//       /* FCM */
//       this.broker.emit("admin.sendFCM", notificationObject);
//       /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
//       // if (generalSettings.data.isEnableWebAppBooking) {
//       this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
//         "bookingId": bookingData._id,
//         "bookingStatus": bookingData.bookingStatus,
//         "dateTime": bookingData.updatedAt,
//         "userAccessToken": bookingData.user?.deviceInfo[0]?.accessToken || "",
//         "bookingInfo": bookingData?.user?.bookingInfo || {},
//       });
//       // }
//     }
//     if (context.meta.adminId) {
//       const notificationObject = {
//         "data": {
//           "type": constantUtil.NOTIFICATIONTYPE,
//           "userType": constantUtil.PROFESSIONAL,
//           "action": constantUtil.ACTION_TRIPSTATUSCHANGEDBYADMIN,
//           "timestamp": Date.now(),
//           "message":
//             professionalStatus === constantUtil.ARRIVED
//               ? INFO_OPERATOR_STATUS_ARRIVED
//               : INFO_OPERATOR_STATUS_START,
//           "details": lzStringEncode(bookingObject(bookingData)),
//         },
//         "registrationTokens": [
//           {
//             "token": bookingData.professional?.deviceInfo[0]?.deviceId || "",
//             "id": bookingData.professional?._id?.toString(),
//             "deviceType":
//               bookingData.professional?.deviceInfo[0]?.deviceType || "",
//             "platform": bookingData.professional?.deviceInfo[0]?.platform || "",
//             "socketId": bookingData.professional?.deviceInfo[0]?.socketId || "",
//           },
//         ],
//       };
//       /* SOCKET PUSH NOTIFICATION */
//       this.broker.emit("socket.sendNotification", notificationObject);
//       // this.broker.emit('socket.statusChangeEvent', notificationObject)

//       /* FCM */
//       this.broker.emit("admin.sendFCM", notificationObject);
//     }
//   } else {
//     vehicleCategoryData =
//       vehicleCategoryDataSet[bookingData.vehicle.vehicleCategoryId.toString()];

//     bookingData.vehicle.vehicleCategoryName =
//       vehicleCategoryData.vehicleCategory;
//     bookingData.vehicle.vehicleCategoryImage =
//       vehicleCategoryData.categoryImage;
//     bookingData.vehicle.vehicleCategoryMapImage =
//       vehicleCategoryData.categoryMapImage;
//     // bookingData.distanceUnit = bookingData.category.distanceType;
//   }

//   return { "code": 200, "data": bookingData, "message": "" };
// };

// admin change professional status

// bookingV2.bookingStatusChangeEnded = async function (context) {
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   let errorCode = 200,
//     message = "",
//     errorMessage = "",
//     bookingData;
//   const clientId =
//     context.params.clientId ||
//     context.meta.clientId ||
//     generalSettings.data.clientId;
//   // const transactionOptions = {
//   //   "readConcern": { "level": "snapshot" },
//   //   "writeConcern": { "w": "majority" },
//   //   "readPreference": "primary",
//   // };
//   // const session = mongoose.startSession();

//   try {
//     // session.startTransaction();
//     const {
//       INVALID_BOOKING,
//       VEHICLE_CATEGORY_NOT_FOUND,
//       BOOKING_UPDATE_STATUS_FAILED,
//       BOOKING_PAID,
//       TRIP_END_PROFESSIONAL,
//       OPERATOR_STATUS_END,
//       REFUND_TO_WALLET,
//     } = await notifyMessage.setNotifyLanguage(
//       context.meta?.professionalDetails?.languageCode
//     );
//     let snapWayData = [],
//       manualWayData = [],
//       snapErrorData = {},
//       finalWayDataSet = [],
//       wayPointsSet = [],
//       checkProfessionalRideCount = 0,
//       checkUserRideCount = 0;

//     const directionsData = {},
//       finalEncodedPolyline = [],
//       traveledDataSet = {};

//     let bookingData = await this.adapter.model
//       .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
//       .populate("professional", { "location.coordinates": 1 })
//       .lean();
//     if (!bookingData) {
//       throw new MoleculerError(INVALID_BOOKING);
//     }
//     let vehicleCategoryData;
//     const vehicleCategoryDataSet = await storageUtil.read(
//       constantUtil.VEHICLECATEGORY
//     );
//     if (!vehicleCategoryDataSet) {
//       throw new MoleculerError(VEHICLE_CATEGORY_NOT_FOUND);
//     }
//     let mapLocationData,
//       locationData,
//       formatted_address = "";
//     if (context.params.isDistanceCalculate) {
//       // locationData = this.adapter.model.aggregate([
//       //   {
//       //     "$match": {
//       //       "_id": mongoose.Types.ObjectId(context.params.bookingId.toString()),
//       //     },
//       //   },
//       //   {
//       //     "$project": {
//       //       "_id": 0,
//       //       "snapWayData": { "$slice": ["$snapWayData", -1] },
//       //       "finalWayData": { "$slice": ["$finalWayData", -1] },
//       //     },
//       //   },
//       // ]);
//       locationData = {
//         "snapWayData":
//           bookingData.snapWayData[bookingData.snapWayData.length - 1],
//         "finalWayData":
//           bookingData.finalWayData[bookingData.finalWayData.length - 1],
//       };
//       mapLocationData = await googleApiUtil.geocode({
//         "lat":
//           locationData?.snapWayData?.lat || locationData?.finalWayData?.lat,
//         "lng":
//           locationData?.snapWayData?.lng || locationData?.finalWayData?.lng,
//       });
//       formatted_address = await googleApiUtil.formattedAddress(
//         mapLocationData.data.results
//       );
//     }
//     bookingData.activity.rideStops = bookingData.activity.rideStops.map(
//       (stop) => {
//         if (stop.type === constantUtil.DESTINATION) {
//           stop.status = constantUtil.ENDED;
//           if (context.params.isDistanceCalculate) {
//             stop.lat =
//               locationData?.snapWayData?.lat || locationData?.finalWayData?.lat;
//             stop.lng =
//               locationData?.snapWayData?.lng || locationData?.finalWayData?.lng;
//             const address = formatted_address;
//             // mapLocationData.data.results[0].formatted_address || null;
//             if (address) {
//               stop.addressName = address;
//               stop.fullAddress = address;
//               stop.shortAddress = address;
//             }
//           }
//         }
//         if (stop.type === constantUtil.ORIGIN) {
//           directionsData["from1"] = [stop.lat + "," + stop.lng];
//         }
//         if (stop.type === constantUtil.DESTINATION) {
//           directionsData["to1"] = [stop.lat + "," + stop.lng];
//         }
//         return stop;
//       }
//     );
//     if (bookingData.bookingStatus === constantUtil.STARTED) {
//       const workedMins = parseInt(
//         (new Date() - new Date(bookingData.activity.pickUpTime)) / (1000 * 60)
//       );
//       //----- check ------
//       // const updateBookingDataCheck = await this.adapter.model.updateOne(
//       //   {
//       //     "_id": context.params.bookingId,
//       //     "bookingStatus": constantUtil.STARTED,
//       //   },
//       //   {
//       //     "bookingStatus": constantUtil.ENDED,
//       //     "activity.rideStops": bookingData.activity.rideStops,
//       //     "activity.endedBy": {
//       //       "id": context.meta.adminId.toString(),
//       //       "userType": constantUtil.ADMIN,
//       //     },
//       //     "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
//       //   }
//       // );

//       const updateBookingData = await this.adapter.model.updateOne(
//         {
//           "_id": context.params.bookingId,
//           "bookingStatus": constantUtil.STARTED,
//         },
//         {
//           "bookingStatus": constantUtil.ENDED,
//           "activity.rideStops": bookingData.activity.rideStops,
//           "activity.dropLatLng": context.params.latLng,
//           "activity.dropTime": new Date(),
//           "activity.isUserDropped": true,
//           "activity.workedMins": workedMins,
//           "activity.endedBy": {
//             "id": context.meta.adminId.toString(),
//             "userType": constantUtil.ADMIN,
//           },
//           "regionalData.dropTime": helperUtil.toRegionalUTC(new Date()),
//         }
//       );

//       if (!updateBookingData.nModified)
//         throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);

//       checkProfessionalRideCount = await this.adapter.model.count({
//         "professional": mongoose.Types.ObjectId(
//           bookingData.professional._id.toString()
//         ),
//         "bookingStatus": constantUtil.ENDED,
//       });
//       if (bookingData.guestType === constantUtil.USER && bookingData.user) {
//         checkUserRideCount = await this.adapter.model.count({
//           "user": mongoose.Types.ObjectId(bookingData.user._id.toString()),
//           "bookingStatus": constantUtil.ENDED,
//         });
//       }
//       bookingData = await this.adapter.model
//         .findById(mongoose.Types.ObjectId(context.params.bookingId.toString()))
//         .populate("user", {
//           "password": 0,
//           "bankDetails": 0,
//           "trustedContacts": 0,
//           "cards": 0,
//         })
//         .populate("professional", {
//           "password": 0,
//           "cards": 0,
//           "bankDetails": 0,
//           "trustedContacts": 0,
//         })
//         .populate("category")
//         .populate("security")
//         .populate("officer", { "password": 0, "preferences": 0 })
//         .populate("coorperate")
//         .populate("couponId")
//         .lean();
//       // bookingData = bookingData.toJSON();

//       if (bookingData.bookingStatus === constantUtil.ENDED) {
//         traveledDataSet["timeDuration"] = parseInt(
//           (new Date(bookingData.activity.dropTime) -
//             new Date(bookingData.activity.pickUpTime)) /
//             1000
//         );
//         if (traveledDataSet["timeDuration"] < 60) {
//           traveledDataSet["timeDuration"] = 60;
//         }
//         let finalData = {};
//         let calculatedData = {};
//         const finalWayPolydata = [];
//         snapWayData =
//           bookingData && bookingData.snapWayData ? bookingData.snapWayData : [];
//         manualWayData =
//           bookingData && bookingData.manualWayData
//             ? bookingData.manualWayData
//             : [];
//         bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
//           context.params?.afterRideWaitingMinsWithGracePeriod || 0;
//         // if (
//         //   parseInt(dataSet['timeDuration']) <=
//         //   parseInt(generalSettings.data.timeDurationThershold)
//         // ) {
//         //   dataSet['km'] = 0
//         //   calculatedData = await helperUtil.calculateEstimateAmount({
//         //     'bookingData': bookingData,
//         //     'calcType': constantUtil.MINIMUMDISTANCE,
//         //     'estimationDistance': dataSet['km'],
//         //     'estimationTime': dataSet['timeDuration'],
//         //   })
//         //   finalData = await helperUtil.endTripCalculation({
//         //     'bookingData': bookingData,
//         //     'calculatedData': calculatedData,
//         //     'estimationDistance': dataSet['km'],
//         //     'estimationTime': dataSet['timeDuration'],
//         //   })
//         // } else {
//         traveledDataSet["km"] = 0;
//         if (generalSettings.data.distanceCalculateType === constantUtil.ROUTE) {
//           if (
//             snapWayData &&
//             snapWayData !== null &&
//             snapWayData !== undefined &&
//             Array.isArray(snapWayData) &&
//             snapWayData.length > 0
//           ) {
//             const finalSnapWayData = await helperUtil.snapWayPath(snapWayData);
//             snapErrorData = finalSnapWayData.snapErrorData;
//             wayPointsSet = finalSnapWayData.finalSnapWayData;
//             finalWayDataSet = finalSnapWayData.finalWayDataSet;
//             traveledDataSet["km"] = finalSnapWayData.distance;
//             // calculatedData = await helperUtil.calculateEstimateAmount({
//             //   "bookingData": bookingData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//             // finalData = await helperUtil.endTripCalculation({
//             //   "bookingData": bookingData,
//             //   "calculatedData": calculatedData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//           } else {
//             if (
//               manualWayData &&
//               manualWayData !== null &&
//               manualWayData !== undefined &&
//               Array.isArray(manualWayData) &&
//               manualWayData.length > 0
//             ) {
//               const finalMaunalCalc =
//                 await helperUtil.manualWayDistanceCalculate(manualWayData);
//               wayPointsSet = finalMaunalCalc.finalWayPoint;
//               finalWayDataSet = [finalMaunalCalc.finalWayPoint];
//               traveledDataSet["km"] = finalMaunalCalc.distance;
//               // calculatedData = await helperUtil.calculateEstimateAmount({
//               //   "bookingData": bookingData,
//               //   "estimationDistance": traveledDataSet["km"],
//               //   "estimationTime": traveledDataSet["timeDuration"],
//               //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//               //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//               //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//               //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//               // });
//               // finalData = await helperUtil.endTripCalculation({
//               //   "bookingData": bookingData,
//               //   "calculatedData": calculatedData,
//               //   "estimationDistance": traveledDataSet["km"],
//               //   "estimationTime": traveledDataSet["timeDuration"],
//               //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//               //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//               //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//               //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//               // });
//             } else {
//               traveledDataSet["km"] = bookingData.estimation.distance;
//               // calculatedData = await helperUtil.calculateEstimateAmount({
//               //   "bookingData": bookingData,
//               //   "estimationDistance": traveledDataSet["km"],
//               //   "estimationTime": traveledDataSet["timeDuration"],
//               //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//               //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//               //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//               //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//               // });
//               // finalData = await helperUtil.endTripCalculation({
//               //   "bookingData": bookingData,
//               //   "calculatedData": calculatedData,
//               //   "estimationDistance": traveledDataSet["km"],
//               //   "estimationTime": traveledDataSet["timeDuration"],
//               //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//               //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//               //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//               //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//               // });
//             }
//           }
//           calculatedData = await helperUtil.calculateEstimateAmount({
//             "bookingData": bookingData,
//             //
//             "tollFareAmount": context.params?.tollFareAmount || 0,
//             "airportFareAmount": context.params?.airportFareAmount || 0,
//             //
//             "estimationDistance": traveledDataSet["km"],
//             "estimationTime": traveledDataSet["timeDuration"],
//             "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             "calculatedDropDistance": traveledDataSet["dropDistance"],
//             "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             "beforeRideAssistanceCareProvidesMins": parseInt(
//               context.params.beforeRideAssistanceCareProvidesMins || 0
//             ),
//             "afterRideAssistanceCareProvidesMins": parseInt(
//               context.params.afterRideAssistanceCareProvidesMins || 0
//             ),
//           });
//           finalData = await helperUtil.endTripCalculation({
//             // "bookingData": bookingData,
//             "paymentOption": bookingData.payment.option,
//             "calculatedData": calculatedData,
//             "estimationDistance": traveledDataSet["km"],
//             "estimationTime": traveledDataSet["timeDuration"],
//             "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             "calculatedDropDistance": traveledDataSet["dropDistance"],
//             "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//           });
//         }
//         if (
//           generalSettings.data.distanceCalculateType === constantUtil.MANUAL
//         ) {
//           if (
//             manualWayData &&
//             manualWayData !== null &&
//             manualWayData !== undefined &&
//             Array.isArray(manualWayData) &&
//             manualWayData.length > 0
//           ) {
//             const finalMaunalCalc = await helperUtil.manualWayDistanceCalculate(
//               manualWayData
//             );
//             wayPointsSet = finalMaunalCalc.finalWayPoint;
//             finalWayDataSet = [finalMaunalCalc.finalWayPoint];
//             traveledDataSet["km"] = finalMaunalCalc.distance;
//             // calculatedData = await helperUtil.calculateEstimateAmount({
//             //   "bookingData": bookingData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//             // finalData = await helperUtil.endTripCalculation({
//             //   "bookingData": bookingData,
//             //   "calculatedData": calculatedData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//           } else {
//             traveledDataSet["km"] = bookingData.estimation.distance;
//             // calculatedData = await helperUtil.calculateEstimateAmount({
//             //   "bookingData": bookingData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//             // finalData = await helperUtil.endTripCalculation({
//             //   "bookingData": bookingData,
//             //   "calculatedData": calculatedData,
//             //   "estimationDistance": traveledDataSet["km"],
//             //   "estimationTime": traveledDataSet["timeDuration"],
//             //   "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             //   "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             //   "calculatedDropDistance": traveledDataSet["dropDistance"],
//             //   "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             // });
//           }
//           calculatedData = await helperUtil.calculateEstimateAmount({
//             "bookingData": bookingData,
//             //
//             "tollFareAmount": context.params?.tollFareAmount || 0,
//             "airportFareAmount": context.params?.airportFareAmount || 0,
//             //
//             "estimationDistance": traveledDataSet["km"],
//             "estimationTime": traveledDataSet["timeDuration"],
//             "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             "calculatedDropDistance": traveledDataSet["dropDistance"],
//             "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//             "beforeRideAssistanceCareProvidesMins": parseInt(
//               context.params.beforeRideAssistanceCareProvidesMins || 0
//             ),
//             "afterRideAssistanceCareProvidesMins": parseInt(
//               context.params.afterRideAssistanceCareProvidesMins || 0
//             ),
//           });
//           finalData = await helperUtil.endTripCalculation({
//             // "bookingData": bookingData,
//             "paymentOption": bookingData.payment.option,
//             "calculatedData": calculatedData,
//             "estimationDistance": traveledDataSet["km"],
//             "estimationTime": traveledDataSet["timeDuration"],
//             "calculatedPickupDistance": traveledDataSet["pickupDistance"],
//             "calculatedPickupTime": traveledDataSet["pickupTimeDuration"],
//             "calculatedDropDistance": traveledDataSet["dropDistance"],
//             "calculatedDropTime": traveledDataSet["dropTimeDuration"],
//           });
//         }

//         let paidCalcType = constantUtil.EQUALTOPAID;
//         if (finalData.payableAmount < bookingData.invoice.payableAmount) {
//           paidCalcType = constantUtil.LESSTHENPAID;
//         } else if (
//           finalData.payableAmount > bookingData.invoice.payableAmount
//         ) {
//           paidCalcType = constantUtil.GREATERTHENPAID;
//         }
//         const staticMapData = {
//           "km": traveledDataSet["km"],
//           "from": directionsData["from1"],
//           "to": directionsData["to1"],
//           "waypoints": "",
//         };

//         // const wayValueSet = [];
//         // if (finalWayPolydata && finalWayPolydata.length > 0) {
//         //   const wayPointsValue = await helperUtil.randomslice(
//         //     finalWayPolydata,
//         //     30
//         //   );
//         //   await wayPointsValue.forEach((w) => {
//         //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
//         //   });
//         //   staticMapData["waypoints"] = wayValueSet.join("");
//         // } else {
//         //   const wayPointsValue = await helperUtil.randomslice(
//         //     manualWayData,
//         //     30
//         //   );
//         //   await wayPointsValue.forEach((w) => {
//         //     wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
//         //   });
//         //   staticMapData["waypoints"] = wayValueSet.join("");
//         // }
//         const wayValueSet = [];
//         if (finalWayPolydata && finalWayPolydata.length > 0) {
//           const wayPointsValue = await helperUtil.randomslice(
//             finalWayPolydata,
//             30
//           );
//           await wayPointsValue.forEach((w) => {
//             wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
//           });
//           // staticMapData["waypoints"] = wayValueSet.join("");
//         } else {
//           const wayPointsValue = await helperUtil.randomslice(
//             manualWayData,
//             30
//           );
//           await wayPointsValue.forEach((w) => {
//             wayValueSet.push(`|${w.lat.toString()},${w.lng.toString()}`);
//           });
//           // staticMapData["waypoints"] = wayValueSet.join("");
//         }
//         staticMapData["waypoints"] = wayValueSet.join("");
//         const value = await helperUtil.getStaticImage(staticMapData);

//         const config = {
//           "method": "get",
//           "url": value,
//           "headers": {},
//           "responseType": "arraybuffer",
//         };

//         const imageData = await axios(config);

//         let finalImageData = await this.broker.emit(
//           "admin.uploadStaticMapImageInSpaces",
//           {
//             "files": imageData.data,
//             "fileName": `static${directionsData["from1"]}Map${directionsData["to1"]}Image`,
//           }
//         );
//         finalImageData = finalImageData && finalImageData[0];
//         const updateBookingData = await this.adapter.model.updateOne(
//           {
//             "_id": context.params.bookingId,
//             "bookingStatus": constantUtil.ENDED,
//           },
//           {
//             "hub":
//               bookingData.professional?.vehicles[0]?.hub?.toString() || null,
//             "finalWayData": finalWayDataSet,
//             "totalTollPassed": context.params.totalTollPassed || 0,
//             "passedTollList": context.params.passedTollList || [],
//             "invoice.serviceTax": finalData.serviceTax || 0,
//             "invoice.tollFareAmount": finalData.tollFareAmount || 0,
//             "invoice.airportFareAmount": finalData.airportFareAmount || 0,
//             "invoice.amountInSite": finalData.amountInSite || 0,
//             "invoice.amountInDriver": finalData.amountInDriver || 0,
//             "invoice.payableAmount": finalData.payableAmount || 0,
//             "invoice.siteCommission": finalData.siteCommission || 0,
//             "invoice.travelCharge": finalData.travelCharge || 0,
//             "invoice.distanceFare": finalData.distanceFare || 0,
//             "invoice.timeFare": finalData.timeFare || 0,
//             "invoice.siteCommissionWithoutTax":
//               finalData.siteCommissionWithoutTax || 0,
//             "invoice.professionalCommision":
//               finalData.professionalCommision || 0,
//             "invoice.professionalCommisionWithoutTips":
//               finalData.professionalCommision || 0,
//             "estimation.distance": traveledDataSet["km"] || 0,
//             "estimation.time": traveledDataSet["timeDuration"] || 0,
//             "estimation.pickupDistance": traveledDataSet["pickupDistance"] || 0,
//             "estimation.pickupTime": traveledDataSet["pickupTimeDuration"] || 0,
//             "estimation.dropDistance": traveledDataSet["dropDistance"],
//             "estimation.dropTime": traveledDataSet["dropTimeDuration"],
//             "bookedEstimation.distance": bookingData.estimation.distance,
//             "bookedEstimation.time": bookingData.estimation.acceptTime,
//             "bookedEstimation.pickupDistance":
//               bookingData.estimation.pickupDistance,
//             "bookedEstimation.pickupTime": bookingData.estimation.pickupTime,
//             "rideMapRouteImage": finalImageData,
//             "payment.paid": true,
//             "encodedPolyline": finalEncodedPolyline,
//             "mapErrorData": "",
//             "snapErrorData": snapErrorData,
//             "originalWayData": wayPointsSet,
//             "distanceCalculateType": generalSettings.data.distanceCalculateType,
//             "invoice.peakFareCharge": finalData["peakFareCharge"],
//             "invoice.nightFareCharge": finalData["nightFareCharge"],
//             "invoice.waitingCharge": finalData["waitingCharge"],
//             "invoice.beforeRideWaitingCharge":
//               finalData["beforeRideWaitingCharge"],
//             "invoice.beforeRideWaitingMins": finalData["beforeRideWaitingMins"],
//             "invoice.afterRideWaitingCharge":
//               finalData["afterRideWaitingCharge"],
//             "invoice.afterRideWaitingMins": finalData["afterRideWaitingMins"],
//             // "trackData": context.params?.trackData || {},
//             "invoice.intercityPickupTimeFare":
//               finalData["intercityPickupTimeFare"],
//             "invoice.intercityPickupDistanceFare":
//               finalData["intercityPickupDistanceFare"],
//             "invoice.intercityPickupCharge": finalData["intercityPickupCharge"],
//             "invoice.intercityDropCharge": finalData["intercityDropCharge"],
//             "invoice.roundingAmount": finalData["roundingAmount"],
//             "invoice.discountAmount": finalData["discountAmount"],
//             //
//             "invoice.beforeRideAssistanceCareProvidesMins": parseInt(
//               context.params.beforeRideAssistanceCareProvidesMins || 0
//             ),
//             "invoice.beforeRideAssistanceCareAmount":
//               finalData["beforeRideAssistanceCareAmount"],
//             "invoice.afterRideAssistanceCareProvidesMins": parseInt(
//               context.params.afterRideAssistanceCareProvidesMins || 0
//             ),
//             "invoice.afterRideAssistanceCareAmount":
//               finalData["afterRideAssistanceCareAmount"],
//             "invoice.assistanceCareAmount": finalData["assistanceCareAmount"],
//           }
//         );
//         if (!updateBookingData.nModified) {
//           throw new MoleculerError(BOOKING_UPDATE_STATUS_FAILED);
//         }
//         this.broker.emit("professional.updatependingReview", {
//           "professionalId": bookingData.professional._id.toString(),
//           "bookingId": context.params.bookingId,
//         });

//         //#region Franchising (site commition to Hub)
//         if (generalSettings.data.isEnableFranchising) {
//           let hubData = await this.broker.emit(
//             "admin.updateHubSiteCommission",
//             {
//               // "professionalId": bookingData.professional?._id?.toString(),
//               "bookingId": bookingData?._id?.toString(),
//               "refBookingId": bookingData.bookingId,
//               "hubId": bookingData.professional?.vehicles[0]?.hub?.toString(),
//               "siteCommissionWithoutTax": finalData["siteCommissionWithoutTax"],
//             }
//           );
//           hubData = hubData && hubData[0];
//           console.log(JSON.stringify(hubData));
//           await this.adapter.model.updateOne(
//             {
//               "_id": mongoose.Types.ObjectId(context.params.bookingId),
//               "bookingStatus": constantUtil.ENDED,
//             },
//             {
//               "invoice.hubSiteCommission": hubData?.hubSiteCommission || 0,
//               "invoice.hubSiteCommissionPercentage":
//                 hubData?.hubSiteCommissionPercentage || 0,
//             }
//           );
//         }
//         //#endregion Franchising (site commition to Hub)

//         // user payment
//         if (bookingData.guestType === constantUtil.USER && bookingData.user) {
//           this.broker.emit("user.updatependingReview", {
//             "userId": bookingData.user._id.toString(),
//             "bookingId": context.params.bookingId,
//           });

//           if (bookingData.payment.option === constantUtil.PAYMENTWALLET) {
//             this.broker.emit("user.updateWalletAmountTripEnded", {
//               "userId": bookingData.user._id.toString(),
//               "amount": parseFloat(finalData.payableAmount.toFixed(2)),
//               "transStatus": paidCalcType,
//               "paymentMethod": bookingData.payment.option,
//             });
//             const userAvailableAmount =
//               parseFloat(bookingData?.user?.wallet?.availableAmount) +
//               parseFloat(bookingData?.user?.wallet?.freezedAmount);
//             this.broker.emit("transaction.newBookingTransaction", {
//               "userId": bookingData.user._id.toString(),
//               "amount": finalData.payableAmount.toFixed(2),
//               "previousBalance": userAvailableAmount,
//               "currentBalance":
//                 parseFloat(userAvailableAmount) -
//                 parseFloat(finalData.payableAmount.toFixed(2)),
//               "refBookingId": bookingData?.bookingId,
//             });
//           }
//           if (bookingData.payment.option === constantUtil.PAYMENTCARD) {
//             // take amount
//             await this.broker.emit("transaction.tripEndCardCharge", {
//               "userId": bookingData.user._id.toString(),
//               "amount": parseFloat(finalData.payableAmount.toFixed(2)),
//               "cardId": bookingData.payment.card.cardId.toString(),
//               "bookingId": bookingData._id.toString(),
//               "refBookingId": bookingData.bookingId,
//               "paymentInitId": bookingData.paymentInitId,
//               "paymentInitAmount": bookingData.paymentInitAmount,
//             });
//           }
//         }

//         // professsional transaction
//         if (
//           generalSettings.data.paymentSection === constantUtil.CASHANDWALLET
//         ) {
//           if (
//             bookingData.payment.option === constantUtil.PAYMENTWALLET ||
//             bookingData.payment.option === constantUtil.PAYMENTCARD
//           ) {
//             this.broker.emit("professional.updateWalletAmountTripEnded", {
//               "professionalId": bookingData.professional._id.toString(),
//               "amount": parseFloat(finalData.professionalCommision.toFixed(2)),
//               "paymentMethod": bookingData.payment.option,
//             });
//             this.broker.emit("transaction.professionalBookingEndTransaction", {
//               "professionalId": bookingData.professional._id.toString(),
//               "amount": finalData.professionalCommision.toFixed(2),
//               "bookingId": bookingData._id.toString(),
//               "refBookingId": bookingData.bookingId,
//               "previousBalance":
//                 bookingData?.professional?.wallet?.availableAmount,
//               "currentBalance":
//                 parseFloat(bookingData?.professional?.wallet?.availableAmount) +
//                 parseFloat(finalData.professionalCommision.toFixed(2)),
//             });
//           }
//           if (
//             bookingData.payment.option === constantUtil.PAYMENTCASH ||
//             bookingData.payment.option === constantUtil.CONST_PIX ||
//             bookingData.payment.option === constantUtil.CONST_POS ||
//             bookingData.payment.option === constantUtil.PAYMENTCREDIT //=>this for corporate beacuse in corpporate payment options use paymentcredit key word
//           ) {
//             // make transaction
//             let paymentType = constantUtil.DEBIT;
//             let professionalCommision = (
//               parseFloat(finalData.payableAmount) -
//               parseFloat(finalData.professionalCommision)
//             ).toFixed(2);
//             if (bookingData.payment.option === constantUtil.PAYMENTCREDIT) {
//               if (
//                 generalSettings?.data?.isEnableCorporateRideWalletPay &&
//                 bookingData.coorperate
//               ) {
//                 const updateCorporateRideWalletPay = "";
//                 let finalPayableAmount =
//                   parseFloat(finalData.payableAmount.toFixed(2)) -
//                   parseFloat(bookingData?.invoice?.payableAmount || 0);
//                 if (finalPayableAmount > 0) {
//                   await this.broker.emit("admin.updateWalletAmountTripEnded", {
//                     "corporateId": bookingData.coorperate?._id?.toString(),
//                     "amount": finalPayableAmount,
//                     "paymentType": constantUtil.DEBIT,
//                     "paymentMethod": bookingData.payment.option,
//                     "bookingId": bookingData._id?.toString(),
//                     "refBookingId": bookingData.bookingId,
//                     "transactionType": constantUtil.BOOKINGCHARGE,
//                     "transactionReason":
//                       BOOKING_PAID + " / " + bookingData.bookingId,
//                   });
//                 } else if (finalPayableAmount < 0) {
//                   finalPayableAmount =
//                     parseFloat(bookingData?.invoice?.payableAmount || 0) -
//                     parseFloat(finalData.payableAmount.toFixed(2));
//                   await this.broker.emit("admin.updateWalletAmountTripEnded", {
//                     "corporateId": bookingData.coorperate?._id?.toString(),
//                     "amount": finalPayableAmount,
//                     "paymentType": constantUtil.CREDIT,
//                     "paymentMethod": bookingData.payment.option,
//                     "bookingId": bookingData._id?.toString(),
//                     "refBookingId": bookingData.bookingId,
//                     "transactionType": constantUtil.BOOKINGCHARGE,
//                     "transactionReason":
//                       REFUND_TO_WALLET + " / " + bookingData.bookingId,
//                   });
//                 }
//               }
//               if (
//                 generalSettings.data
//                   .isProfessionalsCorporateSiteCommissionCreditToWallet
//               ) {
//                 paymentType = constantUtil.CREDIT; //need to Revert
//                 professionalCommision =
//                   finalData.professionalCommision.toFixed(2); //need to Revert
//               } else {
//                 // ----------- Zayride Start ------------
//                 paymentType = constantUtil.DEBIT; //need to Revert
//                 professionalCommision = (
//                   parseFloat(finalData.payableAmount) -
//                   parseFloat(finalData.professionalCommision)
//                 ).toFixed(2); //need to Revert
//                 // ----------- Zayride End ------------
//               }
//             }
//             if (finalData.payableAmount > finalData.professionalCommision) {
//               this.broker.emit("professional.updateWalletAmountTripEnded", {
//                 "professionalId": bookingData.professional._id.toString(),
//                 "amount": professionalCommision,
//                 "paymentMethod": bookingData.payment.option,
//               });
//               this.broker.emit(
//                 "transaction.professionalSiteCommissionTransaction",
//                 {
//                   "professionalId": bookingData.professional._id.toString(),
//                   "amount": professionalCommision,
//                   "bookingId": bookingData._id.toString(),
//                   "refBookingId": bookingData.bookingId,
//                   "paymentType": paymentType,
//                   "previousBalance":
//                     bookingData?.professional?.wallet?.availableAmount,
//                   "currentBalance":
//                     parseFloat(
//                       bookingData?.professional?.wallet?.availableAmount
//                     ) + parseFloat(professionalCommision),
//                 }
//               );
//             }

//             if (finalData.payableAmount < finalData.professionalCommision) {
//               // in cash payment user paied amount for a ride is lesser than professioanl commission so menas some coupons or some discount is applied so have reimpersed this differce amount

//               await this.broker.emit(
//                 "professional.updateWalletAmountTripEnded",
//                 {
//                   "professionalId": bookingData.professional._id.toString(),
//                   "amount": parseFloat(
//                     finalData.professionalCommision - finalData.payableAmount
//                   ).toFixed(2),
//                   "paymentMethod": constantUtil.PAYMENTWALLET,
//                 }
//               );
//               await this.broker.emit(
//                 "transaction.professionalTolerenceAmountTransaction",
//                 {
//                   "professionalId": bookingData.professional._id.toString(),
//                   "amount": parseFloat(
//                     finalData.professionalCommision - finalData.payableAmount
//                   ).toFixed(2),
//                   "bookingId": bookingData._id?.toString(),
//                   "refBookingId": bookingData.bookingId,
//                   "paymentType": paymentType,
//                   "previousBalance":
//                     bookingData?.professional?.wallet?.availableAmount,
//                   "currentBalance":
//                     parseFloat(
//                       bookingData?.professional?.wallet?.availableAmount
//                     ) +
//                     parseFloat(
//                       finalData.professionalCommision - finalData.payableAmount
//                     ),
//                 }
//               );
//             }
//           }
//         }
//         switch (bookingData.serviceCategory?.toLowerCase()) {
//           case constantUtil.CONST_SHARERIDE.toLowerCase():
//             {
//               await this.broker.emit(
//                 "professional.updateProfessionalShareRide",
//                 {
//                   "actionStatus": constantUtil.ENDED,
//                   "bookingId": bookingData?._id?.toString(),
//                   "professionalId": bookingData?.professional?._id?.toString(),
//                   // "pickUpLat": context.params.pickUpLat,
//                   // "pickUpLng": context.params.pickUpLng,
//                   // "dropLat": context.params.dropLat,
//                   // "dropLng": context.params.dropLng,
//                 }
//               );
//               await this.broker.emit("booking.checkAndUpdateOngoingShareRide", {
//                 "actionStatus": constantUtil.CONST_DROP,
//                 "bookingId": null,
//                 "professionalId": bookingData.professional._id.toString(),
//                 "serviceCategory": bookingData.serviceCategory,
//                 "lastRidePassengerCount": bookingData?.passengerCount,
//                 "lat":
//                   context.params?.latLng?.lat ||
//                   bookingData?.professional.location.coordinates[1],
//                 "lng":
//                   context.params?.latLng?.lng ||
//                   bookingData?.professional.location.coordinates[0],
//                 "parentShareRideId": bookingData?.parentShareRideId,
//                 // "passengerCount":
//                 //   ongoingShareRideAction === constantUtil.CONST_DROP
//                 //     ? bookingData?.passengerCount
//                 //     : 0,
//               });
//             }
//             break;
//         }
//         // // invite and earning
//         // //xp
//         // bookingData = await this.adapter.model
//         //   .findById(
//         //     mongoose.Types.ObjectId(context.params.bookingId.toString())
//         //   )
//         //   .populate("user", {
//         //     "password": 0,
//         //     "bankDetails": 0,
//         //     "trustedContacts": 0,
//         //     "cards": 0,
//         //   })
//         //   .populate("professional", {
//         //     "password": 0,
//         //     "cards": 0,
//         //     "bankDetails": 0,
//         //     "trustedContacts": 0,
//         //   })
//         //   .populate("category")
//         //   .populate("security")
//         //   .populate("officer", { "password": 0, "preferences": 0 })
//         //   .populate("coorperate")
//         //   .populate("couponId")
//         //   .lean();

//         // Get booking Details
//         bookingData = await this.getBookingDetailsById({
//           "bookingId": context.params.bookingId.toString(),
//         }); // Get booking Details

//         // bookingData = bookingData.toJSON();
//         vehicleCategoryData =
//           vehicleCategoryDataSet[
//             bookingData.vehicle.vehicleCategoryId.toString()
//           ];

//         bookingData.vehicle.vehicleCategoryName =
//           vehicleCategoryData.vehicleCategory;
//         bookingData.vehicle.vehicleCategoryImage =
//           vehicleCategoryData.categoryImage;
//         bookingData.vehicle.vehicleCategoryMapImage =
//           vehicleCategoryData.categoryMapImage;
//         // bookingData.distanceUnit = bookingData.category.distanceType;
//         bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
//           context.params?.afterRideWaitingMinsWithGracePeriod || 0;
//         bookingData["isOtpNeeded"] = generalSettings.data.isOtpNeeded;
//         bookingData["imageURLPath"] =
//           generalSettings.data.spaces.spacesBaseUrl +
//           "/" +
//           generalSettings.data.spaces.spacesObjectName;

//         // invite and earn
//         const inviteAndEarnSettings = await storageUtil.read(
//           constantUtil.INVITEANDEARN
//         );
//         // user
//         if (bookingData?.user?.referredBy) {
//           if (
//             parseInt(checkUserRideCount) ===
//             inviteAndEarnSettings?.data?.forUser?.rideCount
//           ) {
//             this.broker.emit("user.inviteAndEarn", {
//               "joinerId": bookingData.user._id.toString(),
//             });
//           } else if (
//             parseInt(checkUserRideCount) <
//             inviteAndEarnSettings?.data?.forUser?.rideCount
//           ) {
//             this.broker.emit("user.updateJoinerRideCount", {
//               "joinerId": bookingData.user._id.toString(),
//             });
//           }
//         }

//         //professional
//         if (bookingData?.professional?.referredBy) {
//           if (
//             parseInt(checkProfessionalRideCount) ===
//             inviteAndEarnSettings?.data?.forProfessional?.rideCount
//           ) {
//             this.broker.emit("professional.inviteAndEarn", {
//               "joinerId": bookingData.professional._id.toString(),
//             });
//           } else if (
//             parseInt(checkProfessionalRideCount) <
//             inviteAndEarnSettings?.data?.forProfessional?.rideCount
//           ) {
//             this.broker.emit("professional.updateJoinerRideCount", {
//               "joinerId": bookingData.professional._id.toString(),
//             });
//           }
//         }
//         //-------- Send SMS Alert Start --------------------------
//         // if (
//         //   bookingData.bookingFrom === constantUtil.CONST_WEBAPP ||
//         //   bookingData.bookingBy === constantUtil.ADMIN ||
//         //   bookingData.bookingBy === constantUtil.COORPERATEOFFICE ||
//         //   bookingData.serviceCategory === constantUtil.CONST_PACKAGES
//         // )
//         {
//           this.broker.emit("booking.sendSMSAlertByRideId", {
//             "rideId": context.params.bookingId.toString(),
//             "bookingFrom": bookingData.bookingFrom,
//             "notifyType": constantUtil.ENDED,
//             "notifyFor": constantUtil.USER,
//           });
//         }
//         //-------- Send SMS Alert End --------------------------
//         // if (bookingData.bookingBy === constantUtil.USER) {
//         if (bookingData?.user?._id) {
//           const notificationObject = {
//             "data": {
//               "type": constantUtil.NOTIFICATIONTYPE,
//               "userType": constantUtil.USER,
//               "action": constantUtil.ACTION_BOOKINGENDED,
//               "timestamp": Date.now(),
//               "message": TRIP_END_PROFESSIONAL,
//               "details": lzStringEncode(bookingObject(bookingData)),
//             },
//             "registrationTokens": [
//               {
//                 "token": bookingData.user?.deviceInfo[0]?.deviceId || "",
//                 "id": bookingData.user?._id?.toString(),
//                 "deviceType": bookingData.user?.deviceInfo[0]?.deviceType || "",
//                 "platform": bookingData.user?.deviceInfo[0]?.platform || "",
//                 "socketId": bookingData.user?.deviceInfo[0]?.socketId || "",
//               },
//             ],
//           };

//           /* SOCKET PUSH NOTIFICATION */
//           this.broker.emit("socket.sendNotification", notificationObject);
//           // this.broker.emit('socket.statusChangeEvent', notificationObject)

//           /* FCM */
//           this.broker.emit("admin.sendFCM", notificationObject);
//           /* SOCKET PUSH NOTIFICATION FOR WEB-BOOKING*/
//           // if (generalSettings.data.isEnableWebAppBooking) {
//           this.broker.emit("socket.sendStatusNotificationToWebBookingApp", {
//             "bookingId": bookingData._id,
//             "bookingStatus": bookingData.bookingStatus,
//             "dateTime": bookingData.updatedAt,
//             "userAccessToken":
//               bookingData.user?.deviceInfo[0]?.accessToken || "",
//             "bookingInfo": bookingData?.user?.bookingInfo || {},
//           });
//           // }
//         }
//         if (bookingData.professional) {
//           const notificationObject = {
//             "data": {
//               "type": constantUtil.NOTIFICATIONTYPE,
//               "userType": constantUtil.PROFESSIONAL,
//               "action": constantUtil.ACTION_TRIPSTATUSCHANGEDBYADMIN,
//               "timestamp": Date.now(),
//               "message": OPERATOR_STATUS_END,
//               "details": lzStringEncode(bookingObject(bookingData)),
//             },
//             "registrationTokens": [
//               {
//                 "token":
//                   bookingData.professional?.deviceInfo[0]?.deviceId || "",
//                 "id": bookingData.professional?._id?.toString(),
//                 "deviceType":
//                   bookingData.professional?.deviceInfo[0]?.deviceType || "",
//                 "platform":
//                   bookingData.professional?.deviceInfo[0]?.platform || "",
//                 "socketId":
//                   bookingData.professional?.deviceInfo[0]?.socketId || "",
//               },
//             ],
//           };
//           /* SOCKET PUSH NOTIFICATION */
//           this.broker.emit("socket.sendNotification", notificationObject);
//           // this.broker.emit('socket.statusChangeEvent', notificationObject)

//           /* FCM */
//           this.broker.emit("admin.sendFCM", notificationObject);
//           // sendEamail

//           if (generalSettings.data.emailConfigurationEnable) {
//             // SEND TO PROFESSIONAL
//             if (bookingData.professional.email) {
//               this.broker.emit("admin.sendServiceMail", {
//                 "templateName": "professionalInvoiceMailTemplate",
//                 ...mappingUtil.professionalInvoiceMailObject(bookingData),
//               });
//             }
//             // SEND TO USER
//             if (bookingData.user && bookingData.user.email) {
//               this.broker.emit("admin.sendServiceMail", {
//                 "templateName": "userInvoiceMailTemplate",
//                 ...mappingUtil.userInvoiceMailObject(bookingData),
//               });
//             }
//             if (bookingData.coorperate?.data?.email) {
//               // SEND TO CORPORATE
//               this.broker.emit("admin.sendServiceMail", {
//                 "templateName": "corporateInvoiceMailTemplate",
//                 ...mappingUtil.corporateInvoiceMailObject(bookingData),
//               });
//             }
//             if (bookingData?.corporateEmailId) {
//               // SEND COPY INVOICE TO CORPORATE (For CORPORATE USER Ride)
//               this.broker.emit("admin.sendServiceMail", {
//                 "templateName": "corporateInvoiceMailTemplate",
//                 ...mappingUtil.corporateInvoiceMailObject(bookingData),
//               });
//             }
//           }
//         }
//       }
//     } else {
//       vehicleCategoryData =
//         vehicleCategoryDataSet[
//           bookingData.vehicle.vehicleCategoryId.toString()
//         ];

//       bookingData.vehicle.vehicleCategoryName =
//         vehicleCategoryData.vehicleCategory;
//       bookingData.vehicle.vehicleCategoryImage =
//         vehicleCategoryData.categoryImage;
//       bookingData.vehicle.vehicleCategoryMapImage =
//         vehicleCategoryData.categoryMapImage;
//       // bookingData.distanceUnit = bookingData.category.distanceType;
//       bookingData.invoice["afterRideWaitingMinsWithGracePeriod"] =
//         context.params?.afterRideWaitingMinsWithGracePeriod || 0;
//     }
//     // await session.commitTransaction();
//     return bookingData;
//     // return { "code": 200, "data": bookingData, "message": "" };
//   } catch (e) {
//     // await session.abortTransaction();
//     errorCode = e.code || 400;
//     errorMessage = e.message;
//     message = e.code ? e.message : "SOMETHING_WENT_WRONG";
//   }
//   // finally {
//   //   await session.endSession();
//   // }
//   return {
//     "code": errorCode,
//     "error": errorMessage,
//     "message": message,
//     "data": bookingData,
//   };
// };

bookingV2.checking = async function (context) {
  // //#region session Transaction Method 1
  // let responseData = null;
  // const session = await mongoose.startSession();
  // try {
  //   await session.withTransaction(async () => {
  //     await this.adapter.model.create(
  //       [
  //         {
  //           "invoice.afterRideWaitingGracePeriod": false,
  //           "invoice.afterRideWaitingMins": 0,
  //           "invoice.afterRideWaitingChargePerMin": 0,
  //           "invoice.isAfterRideWaitingCharge": false,
  //           "invoice.beforeRideWaitingGracePeriod": 0,
  //           "regionalData.bookingDate": new Date(),
  //           "bookingDate": new Date(),
  //           "paymentSection": "CASH",
  //           "tripType": "RIDE",
  //           "bookingBy": "USER",
  //           "bookingType": "INSTANT",
  //           "bookingOTP": "000000",
  //           "bookingId": "ZX-000000",
  //           "invoice.intercityBoundaryFareType": "FLAT",
  //           //
  //           "invoice.beforeRideWaitingMins": 0,
  //           "invoice.beforeRideWaitingChargePerMin": 0,
  //           "invoice.isBeforeRideWaitingCharge": false,
  //           "invoice.pendingPaymentAmount": 0,
  //           "invoice.professionalTolerenceAmount": 0,
  //           "invoice.airportFareAmount": 0,
  //           "invoice.tollFareAmount": 0,
  //           "invoice.waitingCharge": 0,
  //           "invoice.nightFareCharge": 0,
  //           "invoice.peakFareCharge": 0,
  //           "invoice.amountInSite": 0,
  //           "invoice.amountInDriver": 0,
  //           "invoice.siteCommissionWithoutTax": 0,
  //           "invoice.siteCommission": 0,
  //           "invoice.siteCommissionPercentage": 0,
  //           "invoice.professionalCommisionWithoutTips": 0,
  //           "invoice.professionalCommision": 0,
  //           "invoice.payableAmount": 0,
  //           "invoice.estimationPayableAmount": 0,
  //           "invoice.tipsAmount": 0,
  //           "invoice.timeFare": 0,
  //           "invoice.distanceFare": 0,
  //           "invoice.travelCharge": 0,
  //           "invoice.serviceTax": 0,
  //           "invoice.serviceTaxPercentage": 0,
  //           "invoice.couponAmount": 0,
  //           "invoice.couponValue": 0,
  //           "invoice.couponApplied": false,
  //           "invoice.actualEstimationAmount": 0,
  //           "invoice.estimationAmount": 0,
  //           "invoice.minimumChargeAmount": 0,
  //           "invoice.minimumChargeStatus": false,
  //           "invoice.professionalCancellationMinute": 0,
  //           "invoice.professionalCancellationAmount": 0,
  //           "invoice.professionalCancellationStatus": false,
  //           "invoice.cancellationMinute": 0,
  //           "invoice.cancellationAmount": 0,
  //           "invoice.cancellationStatus": false,
  //           "invoice.heatmapPeakFare": 0,
  //           "invoice.actualPeakFare": 0,
  //           "invoice.nightFare": 0,
  //           "invoice.peakFare": 0,
  //           "invoice.isNightTiming": false,
  //           "invoice.isPeakTiming": false,
  //           "invoice.fareSettings": {},
  //           "invoice.baseFare": 0,
  //           "payment.paid": true,
  //           "distanceUnit": 0,
  //           "currencyCode": "INR",
  //           "currencySymbol": "RS",
  //           "destination.lng": 72.3245,
  //           "destination.lat": 72.5334,
  //           "destination.shortAddress": `destination.shortAddress`,
  //           "destination.fullAddress": `destination.fullAddress`,
  //           "origin.lng": 75.1234,
  //           "origin.lat": 75.543,
  //           "origin.shortAddress": `origin.shortAddress`,
  //           "origin.fullAddress": `origin.fullAddress`,
  //           "category": "6724cacc3750a7ad7749c5c9",
  //           "bookingFor.phoneNumber": "000000",
  //           "bookingFor.phoneCode": "00",
  //           "invoice.beforeRideWaitingMins": 0,
  //           "invoice.beforeRideWaitingChargePerMin": 0,
  //           "invoice.isBeforeRideWaitingCharge": false,
  //           "invoice.pendingPaymentAmount": 0,
  //           "invoice.professionalTolerenceAmount": 0,
  //           "invoice.airportFareAmount": 0,
  //           "invoice.tollFareAmount": 0,
  //           "invoice.waitingCharge": 0,
  //           "invoice.nightFareCharge": 0,
  //           "invoice.peakFareCharge": 0,
  //           "invoice.amountInSite": 0,
  //           "invoice.amountInDriver": 0,
  //           "invoice.siteCommissionWithoutTax": 0,
  //           "invoice.siteCommission": 0,
  //           "invoice.siteCommissionPercentage": 0,
  //           "invoice.professionalCommisionWithoutTips": 0,
  //           "invoice.professionalCommision": 0,
  //           "invoice.payableAmount": 0,
  //         },
  //       ],
  //       { session }
  //     );
  //     responseData = await this.schema.professionalModel.model.create(
  //       [
  //         {
  //           "phone.code": "550",
  //           "phone.number": "6573456",
  //           "wallet.availableAmount": 0,
  //           "joinAmount": 0,
  //           "joinerRideCount": 0,
  //         },
  //       ],
  //       { session }
  //     );
  //     const professionalId = responseData?.[0]?._id?.toString();
  //     // responseData = JSON.parse(JSON.stringify(responseData));
  //     const isModeified = await this.schema.professionalModel.model.updateOne(
  //       { "_id": mongoose.Types.ObjectId(professionalId) },
  //       { "joinAmount": 0 },
  //       { session }
  //     );
  //     // //------------------------------
  //     if (isModeified.nModified) {
  //       await this.schema.userModel.model.create(
  //         [
  //           {
  //             "phone.code": "550",
  //             "phone.number": "657123456",
  //             "wallet.availableAmount": 0,
  //             "joinAmount": 0,
  //             "joinerRideCount": 0,
  //             "bookingInfo.ongoingBooking": professionalId,
  //           },
  //         ],
  //         { session }
  //       );
  //     }
  //     // //----------------
  //     await this.schema.transactionModel.model.create(
  //       [
  //         {
  //           "transactionType": "PENALITY",
  //           "transactionAmount": 0,
  //           "transactionReason": 550,
  //           "transactionId": Date.now(),
  //           "paymentType": "CASH",
  //           "from": { "userType": "USER" },
  //           "to": { "userType": "USER" },
  //         },
  //       ],
  //       { session }
  //     );
  //     const sss = 2345;
  //     // sss = 2345;
  //   });
  // } catch (error) {
  //   console.log(error.message);
  //   responseData = {
  //     "errorMessage": error.message,
  //     "errorDetails": error.stack,
  //   };
  //   // await session.abortTransaction();
  //   this.broker.emit("log.manageLog", {
  //     "name": "ERRORDATA",
  //     "userType": "USER",
  //     "reason": error.message,
  //     "trackData": responseData,
  //   });
  // } finally {
  //   session.endSession();
  // }
  // return responseData;
  // //#endregion session Transaction Method 1
  // //----------------------------------------------------------
  // #region session Transaction Method 2
  let responseData = null;
  const session = await mongoose.startSession();
  try {
    session.startTransaction();
    await this.adapter.model.create(
      [
        {
          "invoice.afterRideWaitingGracePeriod": false,
          "invoice.afterRideWaitingMins": 0,
          "invoice.afterRideWaitingChargePerMin": 0,
          "invoice.isAfterRideWaitingCharge": false,
          "invoice.beforeRideWaitingGracePeriod": 0,
          "regionalData.bookingDate": new Date(),
          "bookingDate": new Date(),
          "paymentSection": "CASH",
          "tripType": "RIDE",
          "bookingBy": "USER",
          "bookingType": "INSTANT",
          "bookingOTP": "000000",
          "bookingId": "ZX-000000",
          "invoice.intercityBoundaryFareType": "FLAT",
          //
          "invoice.beforeRideWaitingMins": 0,
          "invoice.beforeRideWaitingChargePerMin": 0,
          "invoice.isBeforeRideWaitingCharge": false,
          "invoice.pendingPaymentAmount": 0,
          "invoice.professionalTolerenceAmount": 0,
          "invoice.airportFareAmount": 0,
          "invoice.tollFareAmount": 0,
          "invoice.waitingCharge": 0,
          "invoice.nightFareCharge": 0,
          "invoice.peakFareCharge": 0,
          "invoice.amountInSite": 0,
          "invoice.amountInDriver": 0,
          "invoice.siteCommissionWithoutTax": 0,
          "invoice.siteCommission": 0,
          "invoice.siteCommissionPercentage": 0,
          "invoice.professionalCommisionWithoutTips": 0,
          "invoice.professionalCommision": 0,
          "invoice.payableAmount": 0,
          "invoice.estimationPayableAmount": 0,
          "invoice.tipsAmount": 0,
          "invoice.timeFare": 0,
          "invoice.distanceFare": 0,
          "invoice.travelCharge": 0,
          "invoice.serviceTax": 0,
          "invoice.serviceTaxPercentage": 0,
          "invoice.couponAmount": 0,
          "invoice.couponValue": 0,
          "invoice.couponApplied": false,
          "invoice.actualEstimationAmount": 0,
          "invoice.estimationAmount": 0,
          "invoice.minimumChargeAmount": 0,
          "invoice.minimumChargeStatus": false,
          "invoice.professionalCancellationMinute": 0,
          "invoice.professionalCancellationAmount": 0,
          "invoice.professionalCancellationStatus": false,
          "invoice.cancellationMinute": 0,
          "invoice.cancellationAmount": 0,
          "invoice.cancellationStatus": false,
          "invoice.heatmapPeakFare": 0,
          "invoice.actualPeakFare": 0,
          "invoice.nightFare": 0,
          "invoice.peakFare": 0,
          "invoice.isNightTiming": false,
          "invoice.isPeakTiming": false,
          "invoice.fareSettings": {},
          "invoice.baseFare": 0,
          "payment.paid": true,
          "distanceUnit": 0,
          "currencyCode": "INR",
          "currencySymbol": "RS",
          "destination.lng": 72.3245,
          "destination.lat": 72.5334,
          "destination.shortAddress": `destination.shortAddress`,
          "destination.fullAddress": `destination.fullAddress`,
          "origin.lng": 75.1234,
          "origin.lat": 75.543,
          "origin.shortAddress": `origin.shortAddress`,
          "origin.fullAddress": `origin.fullAddress`,
          "category": "6724cacc3750a7ad7749c5c9",
          "bookingFor.phoneNumber": "000000",
          "bookingFor.phoneCode": "00",
          "invoice.beforeRideWaitingMins": 0,
          "invoice.beforeRideWaitingChargePerMin": 0,
          "invoice.isBeforeRideWaitingCharge": false,
          "invoice.pendingPaymentAmount": 0,
          "invoice.professionalTolerenceAmount": 0,
          "invoice.airportFareAmount": 0,
          "invoice.tollFareAmount": 0,
          "invoice.waitingCharge": 0,
          "invoice.nightFareCharge": 0,
          "invoice.peakFareCharge": 0,
          "invoice.amountInSite": 0,
          "invoice.amountInDriver": 0,
          "invoice.siteCommissionWithoutTax": 0,
          "invoice.siteCommission": 0,
          "invoice.siteCommissionPercentage": 0,
          "invoice.professionalCommisionWithoutTips": 0,
          "invoice.professionalCommision": 0,
          "invoice.payableAmount": 0,
        },
      ],
      { session: session }
    );
    await this.adapter.model.updateOne(
      { "bookingOTP": "000000", "bookingId": "ZX-000000" },
      { "bookingOTP": "000000", "bookingId": "ZX-000000" },
      { session: session }
    );
    responseData = await this.schema.professionalModel.model.create(
      [
        {
          "phone.code": "550",
          "phone.number": "6573456",
          "wallet.availableAmount": 0,
          "joinAmount": 0,
          "joinerRideCount": 0,
        },
      ],
      { session: session }
    );
    const professionalId = responseData?.[0]?._id?.toString();
    // responseData = JSON.parse(JSON.stringify(responseData));
    const isModeified = await this.schema.professionalModel.model.updateOne(
      { "_id": mongoose.Types.ObjectId(professionalId) },
      { "joinAmount": 0 },
      { session: session }
    );
    // //------------------------------
    if (isModeified.nModified) {
      await this.schema.userModel.model.create(
        [
          {
            "phone.code": "550",
            "phone.number": "657123456",
            "wallet.availableAmount": 0,
            "joinAmount": 0,
            "joinerRideCount": 0,
            "bookingInfo.ongoingBooking": professionalId,
          },
        ],
        { session: session }
      );
    }
    // //----------------
    await this.schema.transactionModel.model.create(
      [
        {
          "transactionType": "PENALITY",
          "transactionAmount": 0,
          "transactionReason": 550,
          "transactionId": Date.now(),
          "paymentType": "CASH",
          "from": { "userType": "USER" },
          "to": { "userType": "USER" },
        },
      ],
      { session: session }
    );
    const sss = 2345;
    // sss = 2345;
    await session.commitTransaction();
  } catch (error) {
    console.log(error.message);
    responseData = {
      "errorMessage": error.message,
      "errorDetails": error.stack,
    };
    await session.abortTransaction();
    this.broker.emit("log.manageLog", {
      "name": constantUtil.ERRORDATA,
      "type": constantUtil.ERRORDATA,
      "userType": constantUtil.ADMIN,
      "reason": error.message,
      "trackData": responseData,
    });
  } finally {
    session.endSession();
  }
  return responseData;
  // #endregion session Transaction Method 2
};
//

//----------------------------------
module.exports = bookingV2;
