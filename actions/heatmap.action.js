const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");
const helperUtil = require("../utils/helper.util");

const heatmapAction = {};
//-------------------------------------
heatmapAction.manageHeatmap = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    heatmapDataResponse = {};
  const responseJson = {};
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const { SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    // const bookingDate = helperUtil.toUTC(context.params.bookingDate);
    // await this.adapter.model.create({
    //   "userId": context.params.userId,
    //   "bookingDate": bookingDate,
    //   "serviceAreaId": context.params.serviceAreaId,
    //   "locationTileId": context.params.locationTileId,
    //   "location.coordinates": [
    //     context.params.latLng?.lng,
    //     context.params.latLng?.lat,
    //   ],
    // });
    if (context.params.locationName && context.params.plusCodePrefix) {
      heatmapDataResponse = await this.broker.emit("heatmap.manageHeatmap", {
        ...context.params,
      });
      heatmapDataResponse = heatmapDataResponse && heatmapDataResponse[0];
    }
    if (heatmapDataResponse?.status === constantUtil.SUCCESS) {
      message = INFO_SUCCESS;
    } else {
      throw new MoleculerError(INFO_INVALID_DETAILS);
    }
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": responseJson,
  };
};

heatmapAction.manageManualPointHeatmap = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    heatmapSetupData = null;
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const { SOMETHING_WENT_WRONG, INFO_INVALID_DETAILS, INFO_SUCCESS } =
    notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------
  try {
    if (context.params.serviceAreaId) {
      heatmapSetupData = await this.schema.adminModel.model
        .findOne(
          {
            "name": constantUtil.HEATMAP,
            "isActive": true,
            "data.serviceAreaId": mongoose.Types.ObjectId(
              context.params.serviceAreaId
            ),
          },
          { "_id": 1 }
        )
        .lean();
    }
    const bookingDate = helperUtil.toUTC(context.params.bookingDate);
    const userCount = parseInt(context.params.userCount);
    for (let i = 0; i < userCount; i++) {
      await this.schema.adapter.model.create({
        "userId": context.params.userId,
        "bookingDate": bookingDate,
        "serviceAreaId": context.params.serviceAreaId,
        "heatmapId": heatmapSetupData?._id?.toString(),
        // "locationTileId": context.params.locationTileId,
        "plusCodePrefix": context.params.plusCodePrefix,
        "locationName": context.params.locationName,
        "location.coordinates": [
          context.params.latLng?.lng,
          context.params.latLng?.lat,
        ],
      });
    }
    //Delete Log History (30 Days older Records)
    await this.schema.adapter.model.deleteMany({
      "bookingDate": { "$lt": new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) }, //30 Days older Records
    });
    message = constantUtil.SUCCESS;
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : SOMETHING_WENT_WRONG;
  }
  return {
    "code": errorCode,
    "message": message,
    "data": {},
  };
};

heatmapAction.getHeatmapDataByLocation = async function (context) {
  let errorCode = 200,
    message = "",
    languageCode = "",
    professionalCurrentServiceAreaId = null;

  let responseJson = [];
  // const heatmapData = [];
  //------------ Notification Message Start ---------
  languageCode = context.meta?.professionalDetails?.languageCode;
  const {
    INFO_SUCCESS,
    INFO_FAILED,
    SOMETHING_WENT_WRONG,
    INFO_INVALID_DETAILS,
  } = notifyMessage.setNotifyLanguage(context.params.langCode || languageCode);
  //------------ Notification Message End ---------

  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

  if (generalSettings.data.isEnableHeatmap) {
    try {
      // let professionalCurrentServiceArea = null,
      //   heatmapRedisData = null;
      // //#region  professional Current Service Area
      // if (!context.params.serviceAreaId) {
      //   professionalCurrentServiceArea = await this.broker.emit(
      //     "category.getServieCategory",
      //     {
      //       "lat": context.params.lat,
      //       "lng": context.params.lng,
      //       "categoryName": constantUtil.CONST_RIDE.toLowerCase(), //Must be Lowercase
      //     }
      //   );
      //   professionalCurrentServiceArea =
      //     professionalCurrentServiceArea && professionalCurrentServiceArea[0];
      // }
      // //#endregion  professional Current Service Area
      // professionalCurrentServiceAreaId =
      //   context.params.serviceAreaId ||
      //   professionalCurrentServiceArea?._id?.toString() ||
      //   null;
      // const heatmapRedisDataList = await storageUtil.read(constantUtil.HEATMAP);
      // heatmapRedisDataList?.forEach((item) => {
      //   if (
      //     item.isActive === true &&
      //     item.data?.serviceAreaId?.toString() ===
      //       professionalCurrentServiceAreaId
      //   ) {
      //     heatmapRedisData = item;
      //   }
      // });
      // const maxSearchDistance = heatmapRedisData?.data?.searchRadius || 100; // Meter
      // const maxSearchTimeInterval = heatmapRedisData?.data?.timeInterval || 5; // Minutes
      // //-----
      // const professionaCount = await this.schema.professionalModel.model.count({
      //   "status": constantUtil.ACTIVE,
      //   "onlineStatus": true,
      //   "location": {
      //     // "$nearSphere": {
      //     "$near": {
      //       "$geometry": {
      //         "type": "Point",
      //         "coordinates": [
      //           parseFloat(context.params.lng),
      //           parseFloat(context.params.lat),
      //         ],
      //       },
      //       "$minDistance": 0,
      //       "$maxDistance": maxSearchDistance, //context.params.radius,
      //     },
      //   },
      // });
      // const fromDate = new Date(
      //   new Date().getTime() - 1000 * 60 * maxSearchTimeInterval
      // );
      // const toDate = new Date(new Date());
      // // const toDate = new Date(
      // //   new Date(context.params.toDate).setHours(23, 59, 59, 999)
      // // );
      // const bookingTriedData = await this.schema.adapter.model
      //   .find(
      //     {
      //       "location": {
      //         // "$nearSphere": {
      //         "$near": {
      //           "$geometry": {
      //             "type": "Point",
      //             "coordinates": [
      //               parseFloat(context.params.lng),
      //               parseFloat(context.params.lat),
      //             ],
      //           },
      //           "$minDistance": 0,
      //           "$maxDistance": maxSearchDistance, //context.params.radius,
      //         },
      //       },
      //       "bookingDate": { "$gte": fromDate, "$lt": toDate },
      //     },
      //     {
      //       "_id": 1,
      //       "location": 1,
      //       "serviceAreaId": 1,
      //       "plusCodePrefix": 1,
      //       "heatmapId": 1,
      //     }
      //   )
      //   .populate("heatmapId")
      //   .sort({ "plusCodePrefix": 1 })
      //   .lean();
      // //--------

      // //--------
      // let lastPlusCodePrefix = "";
      // // professionalPercentage = professionaCount / bookingTriedData.length;
      // await bookingTriedData.map(async (item) => {
      //   let peakFare = 0,
      //     density = 0,
      //     professionalPercentage = 0;
      //   const plusCodePrefixCount = await bookingTriedData.filter(
      //     (eachDoc) => eachDoc?.plusCodePrefix === item.plusCodePrefix
      //   ).length;
      //   professionalPercentage = professionaCount / plusCodePrefixCount;
      //   if (item.heatmapId?.data) {
      //     if (
      //       professionalPercentage >= item.heatmapId?.data?.fromValue &&
      //       professionalPercentage < item.heatmapId?.data?.toValue
      //     ) {
      //       peakFare = item.heatmapId?.data?.peakFare || 0;
      //     } else if (
      //       professionalPercentage >= item.heatmapId?.data?.fromValue1 &&
      //       professionalPercentage < item.heatmapId?.data?.toValue1
      //     ) {
      //       peakFare = item.heatmapId?.data?.peakFare1 || 0;
      //     } else if (
      //       professionalPercentage >= item.heatmapId?.data?.fromValue2 &&
      //       professionalPercentage < item.heatmapId?.data?.toValue2
      //     ) {
      //       peakFare = item.heatmapId?.data?.peakFare2 || 0;
      //     } else if (
      //       professionalPercentage >= item.heatmapId?.data?.fromValue3 &&
      //       professionalPercentage < item.heatmapId?.data?.toValue3
      //     ) {
      //       peakFare = item.heatmapId?.data?.peakFare3 || 0;
      //     } else if (
      //       professionalPercentage >= item.heatmapId?.data?.fromValue4 &&
      //       professionalPercentage <= item.heatmapId?.data?.toValue4
      //     ) {
      //       peakFare = item.heatmapId?.data?.peakFare4 || 0;
      //     }
      //   }
      //   density = (plusCodePrefixCount / bookingTriedData.length)?.toFixed(2);
      //   //---------------------------------
      //   const newRecordData = {
      //     "lat": item.location.coordinates[1],
      //     "lng": item.location.coordinates[0],
      //     "peakFare":
      //       peakFare > 0 && lastPlusCodePrefix !== item.plusCodePrefix
      //         ? peakFare.toString() + " X"
      //         : "",
      //     "plusCodePrefix": item.plusCodePrefix,
      //     "density": density,
      //   };
      //   if (!responseJson.includes(newRecordData)) {
      //     responseJson.push(newRecordData);
      //   }
      //   //---------------------------------
      //   lastPlusCodePrefix = item.plusCodePrefix;
      // });
      let responseData = await this.broker.emit(
        "heatmap.getHeatmapDataByLocation",
        {
          ...context.params,
        }
      );
      responseData = responseData && responseData[0];
      if (responseData.status === constantUtil.SUCCESS) {
        message = INFO_SUCCESS;
        professionalCurrentServiceAreaId = responseData.serviceAreaId;
        responseJson = responseData.response;
      } else {
        throw new MoleculerError(INFO_INVALID_DETAILS);
      }
    } catch (e) {
      errorCode = e.code || 400;
      message = e.code ? e.message : SOMETHING_WENT_WRONG;
    }
  }
  return {
    "code": errorCode,
    "message": message,
    "data": {
      "response": responseJson,
      "serviceAreaId": professionalCurrentServiceAreaId,
    },
  };
};
heatmapAction.heatmapReportData = async function (context) {
  let responseData = {},
    errorCode = 200,
    message = "",
    fromDate = null,
    toDate = null;
  const query = [];
  try {
    //---------------------------------
    if (context.params.timeInterval) {
      fromDate = new Date(
        new Date().getTime() - 1000 * 60 * parseInt(context.params.timeInterval)
      );
      toDate = new Date();
    } else {
      if (context.params.bookingFrom && context.params.bookingFrom !== "") {
        fromDate = new Date(
          new Date(context.params.bookingFrom).setHours(0, 0, 0, 0)
        );
        toDate = new Date(
          new Date(context.params.bookingFrom).setHours(23, 59, 59, 999)
        );
      }
      if (context.params.bookingTo && context.params.bookingTo !== "") {
        if (!fromDate) {
          fromDate = new Date(
            new Date(context.params.bookingTo).setHours(0, 0, 0, 0)
          );
        }
        toDate = new Date(
          new Date(context.params.bookingTo).setHours(23, 59, 59, 999)
        );
      }
      if (!fromDate) {
        fromDate = new Date(new Date().setHours(0, 0, 0, 0));
      }
      if (!toDate) {
        toDate = new Date(new Date().setHours(23, 59, 59, 999));
      }
    }
    //---------------------------------
    query.push({
      "$match": {
        "bookingDate": { "$gte": fromDate, "$lt": toDate },
        //"clientId": mongoose.Types.ObjectId(context.params.clientId),
      },
    });
    query.push(
      {
        "$lookup": {
          "from": "users",
          "localField": "userId",
          "foreignField": "_id",
          "as": "userData",
        },
      },
      {
        "$unwind": {
          "path": "$userData",
          "preserveNullAndEmptyArrays": true,
        },
      },
      {
        "$lookup": {
          "from": "categories",
          "localField": "serviceAreaId",
          "foreignField": "_id",
          "as": "serviceArea",
        },
      },
      {
        "$unwind": {
          "path": "$serviceArea",
          "preserveNullAndEmptyArrays": true,
        },
      }
    );
    if (context.params.serviceArea && context.params.serviceArea !== "ALL") {
      query.push({
        "$match": {
          "serviceAreaId": mongoose.Types.ObjectId(context.params.serviceArea),
        },
      });
    }
    query.push({
      "$facet": {
        "all": [{ "$count": "all" }],
        "response": [
          {
            "$project": {
              "_id": "$_id",
              "bookingDate": "$bookingDate",
              "locationName": "$locationName",
              "userName": {
                "$concat": ["$userData.firstName", " ", "$userData.lastName"],
              },
              "userPhoneNumber": {
                "$concat": [
                  "$userData.phone.code",
                  " ",
                  "$userData.phone.number",
                ],
              },
              "serviceArea": "$serviceArea.locationName",
            },
          },
          { "$sort": { "bookingDate": -1 } },
          { "$skip": context.params.skip },
          { "$limit": context.params.limit },
        ],
      },
    });
    responseData = await this.schema.adapter.model
      .aggregate(query)
      .allowDiskUse(true);
  } catch (e) {
    errorCode = e.code || 400;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
  }
  return {
    "code": errorCode,
    "data": responseData,
    "message": message,
  };
};
//-------------------------------
module.exports = heatmapAction;
