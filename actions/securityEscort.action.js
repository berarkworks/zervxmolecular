const { customAlphabet } = require("nanoid");
const { MoleculerError } = require("moleculer").Errors;
const mongoose = require("mongoose");
const _ = require("lodash");

const constantUtil = require("../utils/constant.util");
const helperUtil = require("../utils/helper.util");
const storageUtil = require("../utils/storage.util");
const {
  securityObject,
  bookingObject,
  securityBookingViewObject,
} = require("../utils/mapping.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const securityEscortAction = {};

securityEscortAction.getSosHelp = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context.params.userType.toUpperCase();
  let userData, professionalData, operatorData;
  if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("INVALID USER DETAIL");
    }
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.userId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  } else {
    professionalData = await this.broker.emit("professional.getById", {
      "id": context.meta.professionalId.toString(),
    });
    professionalData = professionalData[0];
    if (!professionalData) {
      throw new MoleculerError("INVALID PROFESSIONAL DETAIL");
    }
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.professionalId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  }
  let responseData;
  const securityData = await this.adapter.model.create({
    "securityThreadId": `${customAlphabet(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      8
    )()}`,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "operator": operatorData ? operatorData.operator : null,
    "officer": operatorData ? operatorData.officer : null,
    "status": operatorData ? operatorData.status : constantUtil.NEW,
    "priority": operatorData ? operatorData.priority : constantUtil.NOTASSIGNED,
    "ticketId": operatorData ? operatorData.ticketId : null,
    "userType": userType,
    "isUserSubscribe": context.params.isUserSubscribe,
    "booking": context.params.bookingId || null,
    "securityServiceName": context.params.securityServiceName,
    "securityServiceId": context.params.securityServiceId,
    "threadMessage": context.params.message,
    "alertType": context.params.alertType,
    "alertDate": helperUtil.toUTC(new Date()),
    "alertLocation": {
      "addressName": context.params.alertLocationAddressName,
      "fullAddress": context.params.alertLocationFullAddress,
      "shortAddress": context.params.alertLocationShortAddress,
      "lat": context.params.alertLocationLat,
      "lng": context.params.alertLocationLng,
    },
  });
  if (!securityData) {
    throw new MoleculerError("Invalid Security Escort data");
  }
  const userTrustedContact =
    userType === constantUtil.USER
      ? userData.trustedContacts
      : professionalData.trustedContacts;

  _.forEach(userTrustedContact, async (contacts) => {
    /* SMS */
    // this.broker.emit('admin.sendSMS', {
    //   'phoneNumber': `${contacts.phone.code}${contacts.phone.number}`,
    //   'message': '',
    // })
  });

  if (userType === constantUtil.USER) {
    await this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": context.meta.userId.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": false,
      "isEscortClosed": securityData.status === "CLOSED" ? false : true,
    });
  } else {
    await this.broker.emit("professional.updateOnGoingSecurityBooking", {
      "id": context.meta.professionalId.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": false,
      "isEscortClosed":
        securityData.status === constantUtil.CLOSED ? true : false,
    });
  }

  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(securityData._id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking")
    .populate("operator")
    .lean();

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (context.params.isUserSubscribe === true) {
    let officerRetryData = [{}];
    officerRetryData = await this.broker.emit(
      "officer.getOfficerByLocationRequest",
      {
        "lat": context.params.alertLocationLat,
        "lng": context.params.alertLocationLng,
        "radius": generalSettings.data.requestDistance,
      }
    );
    if (officerRetryData[0] && officerRetryData[0].length === 0)
      officerRetryData = await this.broker.emit(
        "officer.getOfficerByLocationRequest",
        {
          "lat": context.params.alertLocationLat,
          "lng": context.params.alertLocationLng,
          "radius": generalSettings.data.requestDistance,
        }
      );
    const updateSecurityData = this.adapter.model.updateOne(
      { "_id": securityData._id },
      { "isEscortApply": true }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    if (context.params.bookingId) {
      const bookingUpdate = await this.broker.emit(
        "booking.updateSecurityInfo",
        {
          "bookingId": securityData.booking.toString(),
          "securityId": securityData._id.toString(),
        }
      );
      if (bookingUpdate[0].code === 0) {
        throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
      }
      let bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      bookingData = bookingData && bookingData[0];

      responseData = bookingObject(bookingData.data);
    } else {
      responseData = securityBookingViewObject(responseJson);
    }
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.OFFICER,
        "action": constantUtil.ACTION_ESCORTREQUEST,
        "timestamp": Date.now(),
        "message": "NEED SOS HELP",
        "details": securityBookingViewObject(responseJson),
      },
      "registrationTokens": officerRetryData[0].map((officer) => {
        return {
          "token": officer.deviceInfo[0].deviceId,
          "id": officer._id.toString(),
          "deviceType": officer.deviceInfo[0].deviceType,
          "platform": officer.deviceInfo[0].platform,
          "socketId": officer.deviceInfo[0].socketId,
        };
      }),
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);
    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  } else {
    responseData = securityBookingViewObject(responseJson);
  }
  const adminNotificationObject = {
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_SECURITYNOTIFICATION,
      "timestamp": Date.now(),
      "message": "SECURITY NOTIFICATION",
      "details": responseData,
    },
  };
  if (securityData.status === constantUtil.NEW)
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit(
      "socket.sendSecurityNotificationToAdmin",
      adminNotificationObject
    );
  return {
    "code": 200,
    "data": responseData,
    "message": "NOTIFICATION SEND TO NEARBY OFFICERS",
  };
};

securityEscortAction.getSecurityHelpWithoutFile = async function (context) {
  const userType = context.params.userType.toUpperCase();
  let operatorData;
  if (userType === constantUtil.USER) {
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.userId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  } else {
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.professionalId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  }
  const securityData = await this.adapter.model.create({
    "securityThreadId": `${customAlphabet(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      8
    )()}`,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "operator": operatorData ? operatorData.operator : null,
    "officer": operatorData ? operatorData.officer : null,
    "status": operatorData ? operatorData.status : constantUtil.NEW,
    "priority": operatorData ? operatorData.priority : constantUtil.NOTASSIGNED,
    "ticketId": operatorData ? operatorData.ticketId : null,
    "userType": userType,
    "isUserSubscribe": context.params.isUserSubscribe,
    "booking": context.params.bookingId || null,
    "securityServiceName": context.params.securityServiceName,
    "securityServiceId": context.params.securityServiceId,
    "threadMessage": context.params.message,
    "alertType": context.params.alertType,
    "alertDate": helperUtil.toUTC(new Date()),
    "alertLocation": {
      "addressName": context.params.alertLocationAddressName,
      "fullAddress": context.params.alertLocationFullAddress,
      "shortAddress": context.params.alertLocationShortAddress,
      "lat": context.params.alertLocationLat,
      "lng": context.params.alertLocationLng,
    },
  });
  if (!securityData) {
    throw new MoleculerError("Invalid Security Escort data");
  }
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(securityData._id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking")
    .populate("operator");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  const adminNotificationObject = {
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_SECURITYNOTIFICATION,
      "timestamp": Date.now(),
      "message": "SECURITY NOTIFICATION",
      "details": securityBookingViewObject(responseJson),
    },
  };

  if (securityData.status === constantUtil.NEW)
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit(
      "socket.sendSecurityNotificationToAdmin",
      adminNotificationObject
    );

  return {
    "code": 200,
    "data": securityBookingViewObject(responseJson),
    "message": "",
  };
};

securityEscortAction.getSecurityHelpWithFile = async function (context) {
  const userType = context.params.userType.toUpperCase();
  let operatorData;
  if (userType === constantUtil.USER) {
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.userId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  } else {
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.professionalId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  }
  const uniqueCode = `${customAlphabet(
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    8
  )()}`;
  if (context.params.files && context.params.files.length > 0) {
    const s3Image = await this.broker.emit("admin.uploadSingleImageInSpaces", {
      "files": context.params.files,
      "fileName": `files${uniqueCode.toLowerCase()}${
        constantUtil.SECURITYSERVICE
      }`,
    });
    context.params.message = s3Image[0];
  }
  const securityData = await this.adapter.model.create({
    "securityThreadId": uniqueCode,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "operator": operatorData ? operatorData.operator : null,
    "officer": operatorData ? operatorData.officer : null,
    "status": operatorData ? operatorData.status : constantUtil.NEW,
    "priority": operatorData ? operatorData.priority : constantUtil.NOTASSIGNED,
    "ticketId": operatorData ? operatorData.ticketId : null,
    "userType": userType,
    "isUserSubscribe": context.params.isUserSubscribe,
    "booking": context.params.bookingId || null,
    "securityServiceName": context.params.securityServiceName,
    "securityServiceId": context.params.securityServiceId,
    "threadMessage": context.params.message || "",
    "alertType": context.params.alertType,
    "alertDate": helperUtil.toUTC(new Date()),
    "alertLocation": {
      "addressName": context.params.alertLocationAddressName,
      "fullAddress": context.params.alertLocationFullAddress,
      "shortAddress": context.params.alertLocationShortAddress,
      "lat": context.params.alertLocationLat,
      "lng": context.params.alertLocationLng,
    },
  });
  if (!securityData) {
    throw new MoleculerError("Invalid Security Escort data");
  }
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(securityData._id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking")
    .populate("operator");

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  const adminNotificationObject = {
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_SECURITYNOTIFICATION,
      "timestamp": Date.now(),
      "message": "SECURITY NOTIFICATION",
      "details": securityBookingViewObject(responseJson),
    },
  };
  if (securityData.status === constantUtil.NEW) {
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit(
      "socket.sendSecurityNotificationToAdmin",
      adminNotificationObject
    );
  }
  return {
    "code": 200,
    "data": securityBookingViewObject(responseJson),
    "message": "",
  };
};

securityEscortAction.acceptEscortBooking = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.escortStatus === constantUtil.AWAITING) {
    let notificationObject;
    let bookingAction;
    if (responseJson.userType === constantUtil.USER) {
      const updatedEscortData = await this.adapter.model.updateOne(
        {
          "_id": mongoose.Types.ObjectId(
            context.params.escortBookingId.toString()
          ),
          "user": responseJson.user._id,
          "status": { "$ne": constantUtil.CLOSED },
          "escortStatus": constantUtil.AWAITING,
        },
        {
          "officer": context.meta.officerId,
          "isEscortApply": true,
          "escortStatus": constantUtil.ACCEPTED,
          "activity.acceptTime": new Date(),
        }
      );
      if (updatedEscortData.nModified === 0)
        return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

      bookingAction = constantUtil.ACTION_ESCORTACCEPT;

      await this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": context.meta.officerId.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ACCEPTED,
      });

      await this.broker.emit("user.updateOnGoingSecurityBooking", {
        "id": responseJson.user._id.toString(),
        "escortBookingId": context.params.escortBookingId.toString(),
        "type": responseJson.securityServiceName,
        "isOfficerAssigned": true,
        "isEscortClosed":
          responseJson.status === constantUtil.CLOSED ? true : false,
      });

      responseJson = await this.adapter.model
        .findById(
          mongoose.Types.ObjectId(context.params.escortBookingId.toString())
        )
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 })
        .populate("booking")
        .populate("operator");
      if (!responseJson) {
        throw new MoleculerError("INVALID DETAILS");
      }
      if (responseJson.booking) {
        const bookingUpdate = await this.broker.emit(
          "booking.updateOfficerInfo",
          {
            "bookingId": responseJson.booking._id.toString(),
            "officer": context.meta.officerId.toString(),
          }
        );
        if (bookingUpdate[0].code === 0) {
          throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
        }
        let bookingData = await this.broker.emit("booking.viewRideDetails", {
          "bookingId": responseJson.booking._id.toString(),
        });
        bookingData = bookingData && bookingData[0];

        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": bookingAction,
            "timestamp": Date.now(),
            "message": "YOUR REQUEST IS ACCEPTED BY RESPONSE OFFICER",
            "details": bookingObject(bookingData.data),
          },
          "registrationTokens": [
            {
              "token": responseJson.user.deviceInfo[0].deviceId,
              "id": responseJson.user._id.toString(),
              "deviceType": responseJson.user.deviceInfo[0].deviceType,
              "platform": responseJson.user.deviceInfo[0].platform,
              "socketId": responseJson.user.deviceInfo[0].socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendSecurityAcceptNotify", {
          "id": context.params.escortBookingId,
        });

        if (responseJson.status !== constantUtil.CLOSED)
          this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
            "id": responseJson.user._id.toString(),
          });

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);

        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      } else {
        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": bookingAction,
            "timestamp": Date.now(),
            "message": "YOUR REQUEST IS ACCEPTED BY RESPONSE OFFICER",
            "details": securityBookingViewObject(responseJson),
          },
          "registrationTokens": [
            {
              "token": responseJson.user.deviceInfo[0].deviceId,
              "id": responseJson.user._id.toString(),
              "deviceType": responseJson.user.deviceInfo[0].deviceType,
              "platform": responseJson.user.deviceInfo[0].platform,
              "socketId": responseJson.user.deviceInfo[0].socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendSecurityAcceptNotify", {
          "id": context.params.escortBookingId,
        });

        if (responseJson.status !== constantUtil.CLOSED)
          this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
            "id": responseJson.user._id.toString(),
          });

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);

        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      }
    } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
      const updatedEscortData = await this.adapter.model.updateMany(
        {
          "professional": responseJson.professional._id,
          "status": { "$ne": constantUtil.CLOSED },
          "escortStatus": constantUtil.AWAITING,
        },
        {
          "officer": context.meta.officerId,
          "isEscortApply": true,
          "escortStatus": constantUtil.ACCEPTED,
          "activity.acceptTime": new Date(),
        }
      );
      if (updatedEscortData.nModified === 0)
        return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

      await this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": context.meta.officerId.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ACCEPTED,
      });

      await this.broker.emit("professional.updateOnGoingSecurityBooking", {
        "id": responseJson.user._id.toString(),
        "escortBookingId": context.params.escortBookingId.toString(),
        "type": responseJson.securityServiceName,
        "isOfficerAssigned": true,
        "isEscortClosed":
          responseJson.status === constantUtil.CLOSED ? true : false,
      });

      responseJson = await this.adapter.model
        .findById(
          mongoose.Types.ObjectId(context.params.escortBookingId.toString())
        )
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 })
        .populate("booking");

      if (!responseJson) {
        throw new MoleculerError("INVALID DETAILS");
      }
      bookingAction = constantUtil.ACTION_ESCORTACCEPT;

      if (responseJson.booking) {
        const bookingUpdate = await this.broker.emit(
          "booking.updateOfficerInfo",
          {
            "bookingId": responseJson.booking._id.toString(),
            "officer": context.meta.officerId.toString(),
          }
        );
        if (bookingUpdate[0].code === 0) {
          throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
        }
        let bookingData = await this.broker.emit("booking.viewRideDetails", {
          "bookingId": responseJson.booking._id.toString(),
        });
        bookingData = bookingData && bookingData[0];

        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": bookingAction,
            "timestamp": Date.now(),
            "message": "YOUR REQUEST IS ACCEPTED BY RESPONSE OFFICER",
            "details": bookingObject(bookingData.data),
          },
          "registrationTokens": [
            {
              "token": responseJson.professional.deviceInfo[0].deviceId,
              "id": responseJson.professional._id.toString(),
              "deviceType": responseJson.professional.deviceInfo[0].deviceType,
              "platform": responseJson.professional.deviceInfo[0].platform,
              "socketId": responseJson.professional.deviceInfo[0].socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendSecurityAcceptNotify", {
          "id": context.params.escortBookingId,
        });

        if (responseJson.status !== constantUtil.CLOSED)
          this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
            "id": responseJson.professional._id.toString(),
          });

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);

        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      } else {
        notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.USER,
            "action": bookingAction,
            "timestamp": Date.now(),
            "message": "YOUR REQUEST IS ACCEPTED BY RESPONSE OFFICER",
            "details": securityBookingViewObject(responseJson),
          },
          "registrationTokens": [
            {
              "token": responseJson.professional.deviceInfo[0].deviceId,
              "id": responseJson.professional._id.toString(),
              "deviceType": responseJson.professional.deviceInfo[0].deviceType,
              "platform": responseJson.professional.deviceInfo[0].platform,
              "socketId": responseJson.professional.deviceInfo[0].socketId,
            },
          ],
        };

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);

        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendSecurityAcceptNotify", {
          "id": context.params.escortBookingId,
        });

        if (responseJson.status !== constantUtil.CLOSED)
          this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
            "id": responseJson.professional._id.toString(),
          });

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);

        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      }
    }
  } else {
    return {
      "code": 200,
      "data": securityBookingViewObject(responseJson),
      "message": "",
    };
  }
};

securityEscortAction.escortStatusChange = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking");

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  let workedMins;
  if (context.params.escortStatus === constantUtil.ENDED) {
    workedMins = parseInt(
      (new Date() - new Date(responseJson.activity.arriveTime)) / (1000 * 60)
    );
  }

  const updateData =
    context.params.escortStatus === constantUtil.ARRIVED
      ? {
          "escortStatus": context.params.escortStatus,
          "activity.arriveTime": new Date(),
          "fromLocation.lat": context.params.lat,
          "fromLocation.lng": context.params.lng,
          "fromLocation.status": constantUtil.ENDED,
          "fromLocation.type": context.params.escortStatus,
        }
      : {
          "escortStatus": context.params.escortStatus,
          "activity.endTime": new Date(),
          "toLocation.lat": context.params.lat,
          "toLocation.lng": context.params.lng,
          "toLocation.status": constantUtil.ENDED,
          "toLocation.type": context.params.escortStatus,
          "activity.workedMins": workedMins,
        };
  if (responseJson.userType === constantUtil.USER) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "ticketId": responseJson.ticketId,
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ARRIVED
            : "",
      },
      updateData
    );
    if (updatedEscortData.nModified === 0) {
      throw new MoleculerError("ERROR IN SECURITY UPDATE");
    }
    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking")
      .populate("operator");

    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    let notificationObject;
    const bookingAction =
      responseJson.escortStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_ESCORTARRIVED
        : responseJson.escortStatus === constantUtil.ENDED
        ? constantUtil.ACTION_ESCORTENDED
        : "";

    if (responseJson.escortStatus === constantUtil.ENDED) {
      await this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson.officer._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ENDED,
      });
      await this.broker.emit("user.updateSecurityPendingReview", {
        "id": responseJson.user._id.toString(),
        "escortBookingId": context.params.escortBookingId.toString(),
        "type": responseJson.securityServiceName,
      });
    }
    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking")
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    if (responseJson.escortStatus === constantUtil.ENDED) {
      const { BOOKING_ACCEPTED } = await notifyMessage.setNotifyLanguage(
        context.meta?.userDetails?.languageCode
      );
      const adminNotificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": BOOKING_ACCEPTED,
          "details": securityBookingViewObject(responseJson),
        },
        "registrationTokens": [
          {
            "token":
              responseJson.operator &&
              responseJson.operator !== null &&
              responseJson.operator.data &&
              responseJson.operator.data.accessToken
                ? responseJson.operator.data.accessToken
                : "",
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendAdminNotification", adminNotificationObject);
    }

    if (responseJson.booking) {
      let bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      bookingData = bookingData && bookingData[0];
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(bookingData.data),
        },
        "registrationTokens": [
          {
            "token": responseJson.user.deviceInfo[0].deviceId,
            "id": responseJson.user._id.toString(),
            "deviceType": responseJson.user.deviceInfo[0].deviceType,
            "platform": responseJson.user.deviceInfo[0].platform,
            "socketId": responseJson.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.user._id.toString(),
        });

      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": securityBookingViewObject(responseJson),
        },
        "registrationTokens": [
          {
            "token": responseJson.user.deviceInfo[0].deviceId,
            "id": responseJson.user._id.toString(),
            "deviceType": responseJson.user.deviceInfo[0].deviceType,
            "platform": responseJson.user.deviceInfo[0].platform,
            "socketId": responseJson.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.user._id.toString(),
        });

      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    }
  } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ARRIVED
            : "",
      },
      updateData
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    if (responseJson.escortStatus === constantUtil.ENDED) {
      this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson.officer._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ENDED,
      });
      this.broker.emit("professional.updateSecurityPendingReview", {
        "id": responseJson.professional._id.toString(),
        "escortBookingId": context.params.escortBookingId.toString(),
        "type": responseJson.securityServiceName,
      });
    }
    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    let notificationObject;
    const bookingAction =
      responseJson.escortStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_ESCORTARRIVED
        : responseJson.escortStatus === constantUtil.ENDED
        ? constantUtil.ACTION_ESCORTENDED
        : "";

    if (responseJson.escortStatus === constantUtil.ENDED) {
      const { BOOKING_ACCEPTED } = await notifyMessage.setNotifyLanguage(
        context.meta?.professionalDetails?.languageCode
      );
      const adminNotificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.ADMIN,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": BOOKING_ACCEPTED,
          "details": securityBookingViewObject(responseJson),
        },
        "registrationTokens": [
          {
            "token":
              responseJson.operator &&
              responseJson.operator !== null &&
              responseJson.operator.data &&
              responseJson.operator.data.accessToken
                ? responseJson.operator.data.accessToken
                : "",
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendAdminNotification", adminNotificationObject);
    }

    if (responseJson.booking) {
      let bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      bookingData = bookingData && bookingData[0];
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(bookingData.data),
        },
        "registrationTokens": [
          {
            "token": responseJson.professional.deviceInfo[0].deviceId,
            "id": responseJson.professional._id.toString(),
            "deviceType": responseJson.professional.deviceInfo[0].deviceType,
            "platform": responseJson.professional.deviceInfo[0].platform,
            "socketId": responseJson.professional.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.professional._id.toString(),
        });

      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": securityBookingViewObject(responseJson),
        },
        "registrationTokens": [
          {
            "token": responseJson.professional.deviceInfo[0].deviceId,
            "id": responseJson.professional._id.toString(),
            "deviceType": responseJson.professional.deviceInfo[0].deviceType,
            "platform": responseJson.professional.deviceInfo[0].platform,
            "socketId": responseJson.professional.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.professional._id.toString(),
        });

      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    }
  }
};

securityEscortAction.acknowledgeEscortStatus = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.userType === constantUtil.USER) {
    const updateData =
      context.params.escortStatus === constantUtil.ARRIVED
        ? {
            "acknowledge.isArrived": true,
            "acknowledge.isArrivedStatus": context.params.acknowledgeStatus,
          }
        : {
            "acknowledge.isEnded": true,
            "acknowledge.isEndedStatus": context.params.acknowledgeStatus,
          };
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": { "$ne": constantUtil.CLOSED },
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ARRIVED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ENDED
            : "",
      },
      updateData
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN ACKNOWLEDGE" };

    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    }
  } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
    const updateData =
      context.params.escortStatus === constantUtil.ARRIVED
        ? {
            "acknowledge.isArrived": true,
            "acknowledge.isArrivedStatus": context.params.acknowledgeStatus,
          }
        : {
            "acknowledge.isEnded": true,
            "acknowledge.isEndedStatus": context.params.acknowledgeStatus,
          };
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ARRIVED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ENDED
            : "",
      },
      updateData
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN ACKNOWLEDGE" };

    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityBookingViewObject(responseJson),
        "message": "",
      };
    }
  }
};

securityEscortAction.viewSecurityRequest = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    responseJson = await this.adapter.model
      .find({
        "user": responseJson.user._id,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  } else if (responseJson.professional !== null) {
    responseJson = await this.adapter.model
      .find({
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  }
};

securityEscortAction.getAvaliableOfficer = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const responseJson = await this.adapter.model.findById(
    mongoose.Types.ObjectId(context.params.escortBookingId.toString())
  );
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  let officerRetryData = [{}];
  if (responseJson)
    officerRetryData = await this.broker.emit("officer.getOfficerByLocation", {
      "lat": responseJson.alertLocation.lat,
      "lng": responseJson.alertLocation.lng,
      "radius": generalSettings.data.requestDistance,
    });
  officerRetryData = officerRetryData && officerRetryData[0];
  if (!officerRetryData) {
    throw new MoleculerError("SORRY NO OFFICER AVAILABLE");
  } else {
    return officerRetryData;
  }
};

securityEscortAction.priorityChange = async function (context) {
  const responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.userType === constantUtil.USER) {
    await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": constantUtil.ATTENDED,
      },
      { "priority": context.params.priority }
    );

    if (responseJson.status !== constantUtil.CLOSED)
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.user._id.toString(),
      });
  } else if (responseJson.userType === constantUtil.PROFESSIONAL) {
    await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": constantUtil.ATTENDED,
      },
      {
        "status": context.params.priority,
      }
    );

    if (responseJson.status !== constantUtil.CLOSED)
      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.professional._id.toString(),
      });
  }
  if (responseJson.nModified === 0) return { "code": 0 };

  return { "code": 1, "response": "Updated Successfully" };
};

securityEscortAction.viewClosedSecurityRequest = async function (context) {
  let responseJson = await this.adapter.model
    .findOne({
      "ticketId": context.params.ticketId,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    responseJson = await this.adapter.model
      .find({
        "user": responseJson.user._id,
        "status": { "$eq": constantUtil.CLOSED },
        "ticketId": responseJson.ticketId,
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  } else if (responseJson.professional !== null) {
    responseJson = await this.adapter.model
      .find({
        "professional": responseJson.professional._id,
        "status": { "$eq": constantUtil.CLOSED },
        "ticketId": responseJson.ticketId,
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  }
};

securityEscortAction.viewSecurityRequestBasedOperator = async function (
  context
) {
  let responseJson = await this.adapter.model
    .findOne({
      "ticketId": context.params.ticketId,
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    responseJson = await this.adapter.model
      .find({
        "user": responseJson.user._id,
        "ticketId": context.params.ticketId,
        "operator": context.meta.adminId,
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  } else if (responseJson.professional !== null) {
    responseJson = await this.adapter.model
      .find({
        "professional": responseJson.professional._id,
        "ticketId": responseJson.ticketId,
        "operator": context.meta.adminId,
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = [];
    await responseJson.forEach((data) => {
      respoData.push(securityObject(data));
    });
    return respoData;
  }
};

securityEscortAction.sendOfficerRequest = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  let responseData, responseJsonSet;
  const securityData = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!securityData) {
    throw new MoleculerError("INVALID DETAILS");
  }
  let officerRetryData = [{}];
  officerRetryData = await this.broker.emit(
    "officer.getOfficerByLocationRequest",
    {
      "lat": securityData.alertLocation.lat,
      "lng": securityData.alertLocation.lng,
      "radius": generalSettings.data.requestDistance,
    }
  );
  officerRetryData = officerRetryData && officerRetryData[0];
  if (officerRetryData.length === 0) {
    officerRetryData = await this.broker.emit(
      "officer.getOfficerByLocationRequest",
      {
        "lat": securityData.alertLocation.lat,
        "lng": securityData.alertLocation.lng,
        "radius": generalSettings.data.requestDistance,
      }
    );
    officerRetryData = officerRetryData && officerRetryData[0];
  }
  if (officerRetryData.length === 0) {
    throw new MoleculerError("SORRY NO OFFICERS AVAILABLE");
  }
  if (securityData.user !== null) {
    const updateSecurityData = this.adapter.model.updateMany(
      {
        "user": securityData.user._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": securityData.ticketId,
      },
      { "isEscortApply": true }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    if (securityData.booking && securityData.booking._id) {
      const bookingUpdate = await this.broker.emit(
        "booking.updateSecurityInfo",
        {
          "bookingId": securityData.booking._id.toString(),
          "securityId": securityData._id.toString(),
        }
      );
      if (bookingUpdate[0].code === 0) {
        throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
      }
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": securityData.booking._id.toString(),
      });
      responseData = bookingObject(bookingData[0].data);
      responseJsonSet = bookingObject(bookingData[0].data);
    } else {
      const responseJson = await this.adapter.model
        .findById(mongoose.Types.ObjectId(securityData._id.toString()))
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 });
      if (!responseJson) {
        throw new MoleculerError("INVALID DETAILS");
      }
      responseData = securityObject(responseJson);
      responseJsonSet = securityObject(responseJson);
    }
    if (securityData.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": securityData.user._id.toString(),
      });
  } else if (securityData.professional !== null) {
    const updateSecurityData = this.adapter.model.updateMany(
      {
        "professional": securityData.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": securityData.ticketId,
      },
      {
        "isEscortApply": true,
      }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    if (securityData.booking._id) {
      const bookingUpdate = await this.broker.emit(
        "booking.updateSecurityInfo",
        {
          "bookingId": securityData.booking._id.toString(),
          "securityId": securityData._id.toString(),
        }
      );
      if (bookingUpdate[0].code === 0) {
        throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
      }
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": securityData.booking._id.toString(),
      });
      responseData = bookingObject(bookingData[0].data);
      responseJsonSet = bookingObject(bookingData[0].data);
    } else {
      const responseJson = await this.adapter.model
        .findById(mongoose.Types.ObjectId(securityData._id.toString()))
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 });
      if (!responseJson) {
        throw new MoleculerError("INVALID DETAILS");
      }
      responseData = securityObject(responseJson);
      responseJsonSet = securityObject(responseJson);
    }

    if (securityData.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": securityData.professional._id.toString(),
        "clientId": context.params.clientId,
      });
  }
  if (officerRetryData && officerRetryData.length > 0) {
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.OFFICER,
        "action": constantUtil.ACTION_ESCORTREQUEST,
        "timestamp": Date.now(),
        "message": "YOU GOT NEW SECURITY OPERATION",
        "details": responseData,
      },
      "registrationTokens": officerRetryData.map((officer) => {
        return {
          "token": officer.deviceInfo[0].deviceId,
          "id": officer._id.toString(),
          "deviceType": officer.deviceInfo[0].deviceType,
          "platform": officer.deviceInfo[0].platform,
          "socketId": officer.deviceInfo[0].socketId,
        };
      }),
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  return responseJsonSet;
};

securityEscortAction.assignOfficer = async function (context) {
  let notificationObject;
  let securityData = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!securityData) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (securityData.user !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": securityData.user._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": securityData.ticketId,
        "escortStatus": constantUtil.AWAITING,
      },
      {
        "officer": context.params.officerId,
        "isEscortApply": true,
        "isAdminForceAssign": true,
        "escortStatus": constantUtil.ACCEPTED,
      }
    );
    if (updatedEscortData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    securityData = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!securityData) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const bookingAction = constantUtil.ACTION_ESCORTACCEPT;

    this.broker.emit("officer.updateOnGoingBooking", {
      "officerId": context.params.officerId,
      "escortBookingId": context.params.escortBookingId,
      "type": constantUtil.ACCEPTED,
    });

    this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": securityData.user._id.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": true,
      "isEscortClosed":
        securityData.status === constantUtil.CLOSED ? true : false,
    });

    if (securityData.booking !== null) {
      const bookingUpdate = await this.broker.emit(
        "booking.updateOfficerInfo",
        {
          "bookingId": securityData.booking._id.toString(),
          "officer": context.params.officerId.toString(),
        }
      );
      if (bookingUpdate[0].code === 0) {
        throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
      }
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": securityData.booking._id.toString(),
        "clientId": context.params.clientId,
      });

      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": "RESPONSE OFFICER ACCECPT YOUR REQUEST",
          "details": bookingObject(bookingData),
        },
        "registrationTokens": [
          {
            "token":
              securityData.user &&
              securityData.user.deviceInfo &&
              securityData.user.deviceInfo[0].deviceId,
            "id":
              securityData.user &&
              securityData.user.deviceInfo &&
              securityData.user._id.toString(),
            "deviceType":
              securityData.user &&
              securityData.user.deviceInfo &&
              securityData.user.deviceInfo[0].deviceType,
            "platform":
              securityData.user &&
              securityData.user.deviceInfo &&
              securityData.user.deviceInfo[0].platform,
            "socketId":
              securityData.user &&
              securityData.user.deviceInfo &&
              securityData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (securityData.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": securityData.user._id.toString(),
          "clientId": context.params.clientId,
        });

      const notificationObject2 = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.OFFICER,
          "action": constantUtil.ACTION_ESCORTADMINASSIGN,
          "timestamp": Date.now(),
          "message": "ADMIN ASSIGN FOR SECURITY REQUEST",
          "details": securityObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.officer.deviceInfo[0].deviceId,
            "id": securityData.officer._id.toString(),
            "deviceType": securityData.officer.deviceInfo[0].deviceType,
            "platform": securityData.officer.deviceInfo[0].platform,
            "socketId": securityData.officer.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject2);

      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": "RESPONSE OFFICER ACCECPT YOUR REQUEST",
          "details": bookingObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.user.deviceInfo[0].deviceId,
            "id": securityData.user._id.toString(),
            "deviceType": securityData.user.deviceInfo[0].deviceType,
            "platform": securityData.user.deviceInfo[0].platform,
            "socketId": securityData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (securityData.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": securityData.user._id.toString(),
          "clientId": context.params.clientId,
        });

      const notificationObject2 = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.OFFICER,
          "action": constantUtil.ACTION_ESCORTADMINASSIGN,
          "timestamp": Date.now(),
          "message": "ADMIN ASSIGN FOR SECURITY REQUEST",
          "details": securityObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.officer.deviceInfo[0].deviceId,
            "id": securityData.officer._id.toString(),
            "deviceType": securityData.officer.deviceInfo[0].deviceType,
            "platform": securityData.officer.deviceInfo[0].platform,
            "socketId": securityData.officer.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject2);

      return {
        "code": 200,
        "data": securityObject(securityData),
        "message": "",
      };
    }
  } else if (securityData.professional !== null) {
    const updateSecurityData = this.adapter.model.updateMany(
      {
        "professional": securityData.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": securityData.ticketId,
        "escortStatus": constantUtil.AWAITING,
      },
      {
        "officer": context.params.officerId,
        "isEscortApply": true,
        "isAdminForceAssign": true,
        "escortStatus": constantUtil.ACCEPTED,
      }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    securityData = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!securityData) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const bookingAction = constantUtil.ACTION_ESCORTACCEPT;

    this.broker.emit("professional.updateOnGoingSecurityBooking", {
      "id": securityData.professional._id.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": true,
      "isEscortClosed":
        securityData.status === constantUtil.CLOSED ? true : false,
    });

    if (securityData.booking !== null) {
      const bookingUpdate = await this.broker.emit(
        "booking.updateOfficerInfo",
        {
          "bookingId": securityData.booking._id.toString(),
          "officer": context.meta.officerId.toString(),
        }
      );
      if (bookingUpdate[0].code === 0) {
        throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
      }
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": securityData.booking._id.toString(),
      });

      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": "RESPONSE OFFICER ACCECPT YOUR REQUEST",
          "details": bookingObject(bookingData),
        },
        "registrationTokens": [
          {
            "token": securityData.user.deviceInfo[0].deviceId,
            "id": securityData.user._id.toString(),
            "deviceType": securityData.user.deviceInfo[0].deviceType,
            "platform": securityData.user.deviceInfo[0].platform,
            "socketId": securityData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (securityData.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": securityData.professional._id.toString(),
        });

      const notificationObject2 = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.OFFICER,
          "action": constantUtil.ACTION_ESCORTADMINASSIGN,
          "timestamp": Date.now(),
          "message": "ADMIN ASSIGN FOR SECURITY REQUEST",
          "details": securityObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.officer.deviceInfo[0].deviceId,
            "id": securityData.officer._id.toString(),
            "deviceType": securityData.officer.deviceInfo[0].deviceType,
            "platform": securityData.officer.deviceInfo[0].platform,
            "socketId": securityData.officer.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject2);

      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message": "RESPONSE OFFICER ACCECPT YOUR REQUEST",
          "details": bookingObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.user.deviceInfo[0].deviceId,
            "id": securityData.user._id.toString(),
            "deviceType": securityData.user.deviceInfo[0].deviceType,
            "platform": securityData.user.deviceInfo[0].platform,
            "socketId": securityData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      if (securityData.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": securityData.professional._id.toString(),
        });

      const notificationObject2 = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.OFFICER,
          "action": constantUtil.ACTION_ESCORTADMINASSIGN,
          "timestamp": Date.now(),
          "message": "ADMIN ASSIGN FOR SECURITY REQUEST",
          "details": securityObject(securityData),
        },
        "registrationTokens": [
          {
            "token": securityData.officer.deviceInfo[0].deviceId,
            "id": securityData.officer._id.toString(),
            "deviceType": securityData.officer.deviceInfo[0].deviceType,
            "platform": securityData.officer.deviceInfo[0].platform,
            "socketId": securityData.officer.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject2);
      return {
        "code": 200,
        "data": securityObject(securityData),
        "message": "",
      };
    }
  }
};

securityEscortAction.sendTripWatch = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context.params.userType.toUpperCase();
  let userData, operatorData;
  if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("INVALID USER DETAIL");
    }
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.userId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  }
  let responseJson;
  const securityData = await this.adapter.model.create({
    "securityThreadId": `${customAlphabet(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      8
    )()}`,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "operator": operatorData ? operatorData.operator : null,
    "officer": operatorData ? operatorData.officer : null,
    "status": operatorData ? operatorData.status : constantUtil.NEW,
    "priority": operatorData ? operatorData.priority : constantUtil.NOTASSIGNED,
    "ticketId": operatorData ? operatorData.ticketId : null,
    "userType": userType,
    "isUserSubscribe": context.params.isUserSubscribe,
    "booking": context.params.bookingId,
    "securityServiceName": context.params.securityServiceName,
    "securityServiceId": context.params.securityServiceId,
    "threadMessage": context.params.message,
    "alertType": context.params.alertType,
    "alertDate": helperUtil.toUTC(new Date()),
    "alertLocation": {
      "addressName": context.params.alertLocationAddressName,
      "fullAddress": context.params.alertLocationFullAddress,
      "shortAddress": context.params.alertLocationShortAddress,
      "lat": context.params.alertLocationLat,
      "lng": context.params.alertLocationLng,
    },
  });
  if (!securityData) {
    throw new MoleculerError("Invalid Security Escort data");
  }
  if (context.params.isUserSubscribe === true) {
    let officerRetryData = [{}];
    officerRetryData = await this.broker.emit(
      "officer.getOfficerByLocationRequest",
      {
        "lat": context.params.alertLocationLat,
        "lng": context.params.alertLocationLng,
        "radius": generalSettings.data.requestDistance,
      }
    );

    if (officerRetryData[0] && officerRetryData[0].length === 0)
      officerRetryData = await this.broker.emit(
        "officer.getOfficerByLocationRequest",
        {
          "lat": context.params.alertLocationLat,
          "lng": context.params.alertLocationLng,
          "radius": generalSettings.data.requestDistance,
        }
      );
    if (officerRetryData && officerRetryData.length === 0) {
      throw new MoleculerError("Invalid Security Escort data");
    }
    const updateSecurityData = this.adapter.model.updateOne(
      { "_id": securityData._id },
      { "isEscortApply": true }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    await this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": context.meta.userId.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": false,
      "isEscortClosed":
        securityData.status === constantUtil.CLOSED ? true : false,
    });
    responseJson = await this.adapter.model
      .findById(mongoose.Types.ObjectId(securityData._id.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking")
      .populate("operator");
    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.OFFICER,
        "action": constantUtil.ACTION_ESCORTREQUEST,
        "timestamp": Date.now(),
        "message": "YOU GOT NEW SECURITY OPERATION",
        "details": securityBookingViewObject(responseJson),
      },
      "registrationTokens": officerRetryData[0].map((officer) => {
        return {
          "token": officer.deviceInfo[0].deviceId,
          "id": officer._id.toString(),
          "deviceType": officer.deviceInfo[0].deviceType,
          "platform": officer.deviceInfo[0].platform,
          "socketId": officer.deviceInfo[0].socketId,
        };
      }),
    };
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  const adminNotificationObject = {
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_SECURITYNOTIFICATION,
      "timestamp": Date.now(),
      "message": "SECURITY NOTIFICATION",
      "details": securityBookingViewObject(responseJson),
    },
  };
  if (securityData.status === constantUtil.NEW)
    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit(
      "socket.sendSecurityNotificationToAdmin",
      adminNotificationObject
    );
  if (context.params.bookingId) {
    const bookingUpdate = await this.broker.emit("booking.updateSecurityInfo", {
      "bookingId": context.params.bookingId,
      "securityId": securityData._id.toString(),
    });
    if (bookingUpdate[0].code === 0) {
      throw new MoleculerError("ERROR IN UPDATE BOOKING DETAILS");
    }
  }
  let bookingData = await this.broker.emit("booking.viewRideDetails", {
    "bookingId": context.params.bookingId,
  });
  bookingData = bookingData && bookingData[0];
  return {
    "code": 200,
    "data": bookingObject(bookingData.data),
    "message": "NOTIFICATION SEND TO NEARBY OFFICERS",
  };
};

securityEscortAction.sendPersonalWatch = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context.params.userType.toUpperCase();
  let userData, operatorData, responseJson;
  if (userType === constantUtil.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.meta.userId.toString(),
    });
    userData = userData && userData[0];
    if (!userData) {
      throw new MoleculerError("INVALID USER DETAIL");
    }
    operatorData = await this.adapter.model
      .findOne({
        "user": context.meta.userId,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .sort({ "_id": -1 });
  }
  const securityData = await this.adapter.model.create({
    "securityThreadId": `${customAlphabet(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      8
    )()}`,
    "user": context.meta.userId || null,
    "professional": context.meta.professionalId || null,
    "operator": operatorData ? operatorData.operator : null,
    "officer": operatorData ? operatorData.officer : null,
    "status": operatorData ? operatorData.status : constantUtil.NEW,
    "priority": operatorData ? operatorData.priority : constantUtil.NOTASSIGNED,
    "ticketId": operatorData ? operatorData.ticketId : null,
    "userType": userType,
    "isUserSubscribe": context.params.isUserSubscribe,
    "booking": context.params.bookingId || null,
    "securityServiceName": context.params.securityServiceName,
    "securityServiceId": context.params.securityServiceId,
    "threadMessage": context.params.message,
    "alertType": context.params.alertType,
    "alertDate": helperUtil.toUTC(new Date()),
    "alertLocation": {
      "addressName": context.params.alertLocationAddressName,
      "fullAddress": context.params.alertLocationFullAddress,
      "shortAddress": context.params.alertLocationShortAddress,
      "lat": context.params.alertLocationLat,
      "lng": context.params.alertLocationLng,
    },
    "fromLocation": {
      "addressName": context.params.fromLocationAddressName,
      "fullAddress": context.params.fromLocationFullAddress,
      "shortAddress": context.params.fromLocationShortAddress,
      "lat": context.params.fromLocationLat,
      "lng": context.params.fromLocationLng,
    },
    "toLocation": {
      "addressName": context.params.toLocationAddressName,
      "fullAddress": context.params.toLocationFullAddress,
      "shortAddress": context.params.toLocationShortAddress,
      "lat": context.params.toLocationLat,
      "lng": context.params.toLocationLng,
    },
  });
  if (!securityData) {
    throw new MoleculerError("Invalid Security Escort data");
  }
  if (context.params.isUserSubscribe === true) {
    let officerRetryData = [{}];
    officerRetryData = await this.broker.emit(
      "officer.getOfficerByLocationRequest",
      {
        "lat": context.params.fromLocationLat,
        "lng": context.params.fromLocationLng,
        "radius": generalSettings.data.requestDistance,
      }
    );

    if (officerRetryData[0] && officerRetryData[0].length === 0)
      officerRetryData = await this.broker.emit(
        "officer.getOfficerByLocationRequest",
        {
          "lat": context.params.fromLocationLat,
          "lng": context.params.fromLocationLng,
          "radius": generalSettings.data.requestDistance,
        }
      );

    if (officerRetryData && officerRetryData.length === 0) {
      throw new MoleculerError("Invalid Security Escort data");
    }
    const updateSecurityData = this.adapter.model.updateOne(
      { "_id": securityData._id },
      { "isEscortApply": true }
    );
    if (updateSecurityData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    }
    await this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": context.meta.userId.toString(),
      "escortBookingId": securityData._id.toString(),
      "type": securityData.securityServiceName,
      "isOfficerAssigned": false,
      "isEscortClosed":
        securityData.status === constantUtil.CLOSED ? true : false,
    });
    responseJson = await this.adapter.model
      .findById(mongoose.Types.ObjectId(securityData._id.toString()))
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking")
      .populate("operator");

    const notificationObject = {
      "clientId": context.params.clientId,
      "data": {
        "type": constantUtil.NOTIFICATIONTYPE,
        "userType": constantUtil.OFFICER,
        "action": constantUtil.ACTION_ESCORTREQUEST,
        "timestamp": Date.now(),
        "message": "YOU GOT NEW SECURITY OPERATION",
        "details": securityBookingViewObject(responseJson),
      },
      "registrationTokens": officerRetryData[0].map((officer) => {
        return {
          "token": officer.deviceInfo[0].deviceId,
          "id": officer._id.toString(),
          "deviceType": officer.deviceInfo[0].deviceType,
          "platform": officer.deviceInfo[0].platform,
          "socketId": officer.deviceInfo[0].socketId,
        };
      }),
    };

    /* SOCKET PUSH NOTIFICATION */
    this.broker.emit("socket.sendNotification", notificationObject);

    /* FCM */
    this.broker.emit("admin.sendFCM", notificationObject);
  }
  const adminNotificationObject = {
    "data": {
      "type": constantUtil.NOTIFICATIONTYPE,
      "userType": userType,
      "action": constantUtil.ACTION_SECURITYNOTIFICATION,
      "timestamp": Date.now(),
      "message": "SECURITY NOTIFICATION",
      "details": securityBookingViewObject(responseJson),
    },
  };

  if (securityData.status === constantUtil.NEW)
    this.broker.emit(
      "socket.sendSecurityNotificationToAdmin",
      adminNotificationObject
    );

  return {
    "code": 200,
    "data": securityBookingViewObject(responseJson),
    "message": "NOTIFICATION SEND TO NEARBY OFFICERS",
  };
};

securityEscortAction.cancelEscortBooking = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 });

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": { "$ne": constantUtil.CLOSED },
      },
      { "escortStatus": constantUtil.CANCELLED }
    );
    if (updatedEscortData.nModified === 0) {
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };
    }
    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("admin", { "data.accessToken": 0, "data.password": 0 });

    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.user._id.toString(),
      });
    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  } else if (responseJson.professional !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
      },
      {
        "escortStatus": constantUtil.CANCELLED,
      }
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 });

    if (responseJson.status !== constantUtil.CLOSED)
      this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
        "id": responseJson.professional._id.toString(),
      });

    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  }
};

securityEscortAction.escortAdminStatusChange = async function (context) {
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");

  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    responseJson = await this.adapter.model
      .find({
        "user": responseJson.user._id,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = await responseJson.map((data) => {
      return data._id;
    });
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "_id": { "$in": respoData },
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ARRIVED
            : "",
      },
      {
        "escortStatus": context.params.escortStatus,
      }
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    if (responseJson[0].escortStatus === constantUtil.ENDED) {
      this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson[0].officer._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ENDED,
      });
      this.broker.emit("user.updateSecurityPendingReview", {
        "id": responseJson[0].user._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": responseJson[0].securityServiceName,
      });
    }
    let notificationObject;
    const bookingAction =
      responseJson[0].escortStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_ESCORTARRIVED
        : responseJson[0].escortStatus === constantUtil.ENDED
        ? constantUtil.ACTION_ESCORTENDED
        : "";

    if (responseJson[0].booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson[0].booking.toString(),
        "clientId": context.params.clientId,
      });

      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(bookingData),
        },
        "registrationTokens": [
          {
            "token": bookingData.user.deviceInfo[0].deviceId,
            "id": bookingData.user._id.toString(),
            "deviceType": bookingData.user.deviceInfo[0].deviceType,
            "platform": bookingData.user.deviceInfo[0].platform,
            "socketId": bookingData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(responseJson),
        },
        "registrationTokens": [
          {
            "token": responseJson[0].user.deviceInfo[0].deviceId,
            "id": responseJson[0].user._id.toString(),
            "deviceType": responseJson[0].user.deviceInfo[0].deviceType,
            "platform": responseJson[0].user.deviceInfo[0].platform,
            "socketId": responseJson[0].user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  } else if (responseJson.professional !== null) {
    responseJson = await this.adapter.model
      .find({
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
      })
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("booking")
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("operator");
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
    const respoData = await responseJson.map((data) => {
      return data._id;
    });
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "_id": { "$in": respoData },
        "escortStatus":
          context.params.escortStatus === constantUtil.ARRIVED
            ? constantUtil.ACCEPTED
            : context.params.escortStatus === constantUtil.ENDED
            ? constantUtil.ARRIVED
            : "",
      },
      {
        "escortStatus": context.params.escortStatus,
      }
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    if (responseJson[0].escortStatus === constantUtil.ENDED) {
      this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson[0].officer._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": constantUtil.ENDED,
      });
      this.broker.emit("professional.updateSecurityPendingReview", {
        "id": responseJson[0].professional._id.toString(),
        "escortBookingId": context.params.escortBookingId,
        "type": responseJson[0].securityServiceName,
      });
    }
    let notificationObject;
    const bookingAction =
      responseJson[0].escortStatus === constantUtil.ARRIVED
        ? constantUtil.ACTION_ESCORTARRIVED
        : responseJson[0].escortStatus === constantUtil.ENDED
        ? constantUtil.ACTION_ESCORTENDED
        : "";

    if (responseJson[0].booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson[0].booking.toString(),
        "clientId": context.params.clientId,
      });

      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(bookingData),
        },
        "registrationTokens": [
          {
            "token": bookingData.user.deviceInfo[0].deviceId,
            "id": bookingData.user._id.toString(),
            "deviceType": bookingData.user.deviceInfo[0].deviceType,
            "platform": bookingData.user.deviceInfo[0].platform,
            "socketId": bookingData.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      notificationObject = {
        "clientId": context.params.clientId,
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": bookingAction,
          "timestamp": Date.now(),
          "message":
            bookingAction === constantUtil.ACTION_ESCORTARRIVED
              ? "RESPONSE OFFICER ARRIVED TO YOUR SPOT"
              : "RESPONSE OFFICER ENDED THE SECURITY OPERATION",
          "details": bookingObject(responseJson),
        },
        "registrationTokens": [
          {
            "token": responseJson[0].professional.deviceInfo[0].deviceId,
            "id": responseJson[0].professional._id.toString(),
            "deviceType": responseJson[0].professional.deviceInfo[0].deviceType,
            "platform": responseJson[0].professional.deviceInfo[0].platform,
            "socketId": responseJson[0].professional.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* FCM */
      this.broker.emit("admin.sendFCM", notificationObject);

      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  }
};

securityEscortAction.trackEscortDetails = async function (context) {
  const responseJson = await this.adapter.model
    .findOne({
      "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
    })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("booking");
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (
    responseJson.booking &&
    context.meta.officerId &&
    context.meta.officerId === ""
  ) {
    let bookingData = await this.broker.emit("booking.viewRideDetails", {
      "bookingId": responseJson.booking._id.toString(),
    });
    bookingData = bookingData[0];
    return {
      "code": 200,
      "data": bookingObject(bookingData.data),
      "message": "",
    };
  } else {
    return {
      "code": 200,
      "data": securityBookingViewObject(responseJson),
      "message": "",
    };
  }
};

securityEscortAction.viewAddressSave = async function (context) {
  const responseJson = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
  });
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  const updateData =
    context.params.type === constantUtil.ARRIVED
      ? {
          "fromLocation.addressName": context.params.addressName,
          "fromLocation.fullAddress": context.params.fullAddress,
          "fromLocation.shortAddress": context.params.shortAddress,
        }
      : {
          "toLocation.addressName": context.params.addressName,
          "toLocation.fullAddress": context.params.fullAddress,
          "toLocation.shortAddress": context.params.shortAddress,
        };
  if (responseJson.user !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": responseJson.user._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": responseJson.ticketId,
      },
      updateData
    );
    if (updatedEscortData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    } else {
      return {
        "code": 200,
        "response": {},
        "message": "UPDATED SUCCESSFULLY",
      };
    }
  } else if (responseJson.professional !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "professional": responseJson.professional._id,
        "status": { "$ne": constantUtil.CLOSED },
        "ticketId": responseJson.ticketId,
      },
      updateData
    );
    if (updatedEscortData.nModified === 0) {
      throw new MoleculerError("ERROR IN UPDATE SECURITY DATA");
    } else {
      return {
        "code": 200,
        "response": {},
        "message": "UPDATED SUCCESSFULLY",
      };
    }
  }
};

// SUBMIT ESCORT RATING FOR USER AND PROFESSIONAL
securityEscortAction.submitEscortRating = async function (context) {
  const userType = context.params.userType.toUpperCase();
  const responseJson = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
  });
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.escortStatus === constantUtil.ENDED) {
    const updateData =
      userType === constantUtil.USER
        ? {
            "escortReview.userReview.rating": context.params.rating,
            "escortReview.userReview.comment": context.params.comment,
            "escortReview.isUserReviewed": true,
          }
        : userType === constantUtil.PROFESSIONAL
        ? {
            "escortReview.professionalReview.rating": context.params.rating,
            "escortReview.professionalReview.comment": context.params.comment,
            "escortReview.isProfessionalReviewed": true,
          }
        : {
            "escortReview.officerReview.rating": context.params.rating,
            "escortReview.officerReview.comment": context.params.comment,
            "escortReview.isOfficerReviewed": true,
          };
    const updateBookingData = await this.adapter.model.updateOne(
      {
        "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
        "escortStatus": constantUtil.ENDED,
      },
      updateData
    );
    if (updateBookingData.nModified === 0) {
      throw new MoleculerError(`ERROR IN RATING UPDATE`);
    }
    if (userType === constantUtil.USER) {
      this.broker.emit("user.updateEscortRating", {
        "userId": context.meta.userId,
        "rating": context.params.rating,
      });
    } else if (userType === constantUtil.PROFESSIONAL) {
      this.broker.emit("professional.updateEscortRating", {
        "professionalId": context.meta.professionalId,
        "rating": context.params.rating,
      });
    } else {
      this.broker.emit("officer.updateEscortRating", {
        "officerId": context.meta.officerId,
        "rating": context.params.rating,
      });
    }
    const responseJson = await this.adapter.model.findOne({
      "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
    });
    if (!responseJson) {
      throw new MoleculerError("INVALID DETAILS");
    }
  }
  return {
    "code": 200,
    "data": securityBookingViewObject(responseJson),
    "message": "RATING SUBMITED SUCCESSFULLY",
  };
};

// SEND TO TRUSTED CONTACTS
// to send particular emergency message to their trusted contacts
securityEscortAction.sendEmergencyToTrustedContact = async function (context) {
  const responseJson = await this.adapter.model.findOne({
    "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
  });
  if (!responseJson) {
    throw new MoleculerError("SORRY ESCORT BOOKING ID IS INVALID");
  }
  let updateData;
  if (
    responseJson.sendedTrustedContact &&
    responseJson.sendedTrustedContact.length > 0
  ) {
    updateData = [
      ...responseJson.sendedTrustedContact,
      ...context.params.contacts,
    ];
  } else {
    updateData = context.params.contacts;
  }

  await _.forEach(context.params.contacts, async (contacts) => {
    /* SMS */
    // this.broker.emit('admin.sendSMS', {
    //   'phoneNumber': `${contacts.code}${contacts.number}`,
    //   'message': context.params.message,
    // })
  });
  const updateBookingData = await this.adapter.model.updateOne(
    {
      "_id": mongoose.Types.ObjectId(context.params.escortBookingId),
      "escortStatus": constantUtil.ENDED,
    },
    { "sendedTrustedContact": updateData }
  );
  if (updateBookingData.nModified === 0) {
    throw new MoleculerError(`ERROR IN RATING UPDATE`);
  } else {
    return {
      "code": 200,
      "data": {},
      "message": "SUCCESSFULLY SENDED TO TRUSTED CONTACTS",
    };
  }
};

// EXPIRY OR CANCEL FOR USER AND PROFESSIONAL
securityEscortAction.emergencyExpiryOrDeny = async function (context) {
  const userType = context.params.userType.toUpperCase();
  if (userType === constantUtil.USER) {
    if (
      !(
        context.meta.userId &&
        (context.params.denyType === constantUtil.EXPIRED ||
          context.params.denyType === constantUtil.USERCANCELLED)
      )
    ) {
      throw new MoleculerError("INVALID ACCESS");
    }
  } else {
    if (
      !(
        context.meta.professionalId &&
        (context.params.denyType === constantUtil.EXPIRED ||
          context.params.denyType === constantUtil.PROFESSIONALCANCELLED)
      )
    ) {
      throw new MoleculerError("INVALID ACCESS");
    }
  }

  const updateData =
    userType === constantUtil.USER
      ? context.params.denyType === constantUtil.EXPIRED
        ? {
            "escortStatus": constantUtil.EXPIRED,
            "activity.denyOrExpireTime": new Date(),
          }
        : {
            "escortStatus": constantUtil.USERDENY,
            "activity.denyOrExpireTime": new Date(),
          }
      : context.params.denyType === constantUtil.EXPIRED
      ? {
          "escortStatus": constantUtil.EXPIRED,
          "activity.denyOrExpireTime": new Date(),
        }
      : {
          "escortStatus": constantUtil.PROFESSIONALDENY,
          "activity.denyOrExpireTime": new Date(),
        };
  let responseJson = await this.adapter.model
    .findOne({ "_id": mongoose.Types.ObjectId(context.params.escortBookingId) })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("admin", { "data.accessToken": 0, "data.password": 0 });
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (responseJson.user !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "user": mongoose.Types.ObjectId(responseJson.user._id.toString()),
        "escortStatus": constantUtil.AWAITING,
      },
      updateData
    );
    if (updatedEscortData.nModified === 0) {
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };
    }
    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("admin", { "data.accessToken": 0, "data.password": 0 });

    await this.broker.emit("user.updateOnGoingSecurityBooking", {
      "id": responseJson.user._id.toString(),
      "isEscortClosed": false,
    });
    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  } else if (responseJson.professional !== null) {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "professional": mongoose.Types.ObjectId(
          responseJson.professional._id.toString()
        ),
        "escortStatus": constantUtil.AWAITING,
      },
      updateData
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("admin", { "data.accessToken": 0, "data.password": 0 });

    await this.broker.emit("professional.updateOnGoingSecurityBooking", {
      "id": responseJson.professional._id.toString(),
      "isEscortClosed": false,
    });

    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking.toString(),
      });
      return {
        "code": 200,
        "data": bookingObject(bookingData[0].data),
        "message": "",
      };
    } else {
      return {
        "code": 200,
        "data": securityObject(responseJson),
        "message": "",
      };
    }
  }
};

securityEscortAction.escortBookingCancel = async function (context) {
  const userType = context.params.userType.toUpperCase();
  let responseJson = await this.adapter.model
    .findById(
      mongoose.Types.ObjectId(context.params.escortBookingId.toString())
    )
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("officer", { "password": 0, "preferences": 0 });
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  }
  if (userType !== constantUtil.OFFICER) {
    if (responseJson.user !== null) {
      const updatedEscortData = await this.adapter.model.updateMany(
        {
          "user": responseJson.user._id,
          "status": { "$ne": constantUtil.CLOSED },
        },
        {
          "escortStatus": constantUtil.CANCELLED,
        }
      );

      if (updatedEscortData.nModified === 0)
        return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

      this.broker.emit("user.updateOnGoingSecurityBooking", {
        "id": responseJson.user._id.toString(),
        "isEscortClosed": false,
      });

      responseJson = await this.adapter.model
        .findById(
          mongoose.Types.ObjectId(context.params.escortBookingId.toString())
        )
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 })
        .populate("booking");

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.user._id.toString(),
          "clientId": context.params.clientId,
        });

      if (responseJson.officer) {
        this.broker.emit("officer.updateOnGoingBooking", {
          "officerId": responseJson.officer._id.toString(),
          "type": constantUtil.AWAITING,
          "clientId": context.params.clientId,
        });

        const notificationObject = {
          "clientId": context.params.clientId,
          "data": {
            "type": constantUtil.NOTIFICATIONTYPE,
            "userType": constantUtil.OFFICER,
            "action": constantUtil.ACTION_SECURITYCANCEL,
            "timestamp": Date.now(),
            "message": "BOOKING CANCELLED BY USER",
            "details": securityBookingViewObject(responseJson),
          },
          "registrationTokens": [
            {
              "token": responseJson.officer.deviceInfo[0].deviceId,
              "id": responseJson.officer._id.toString(),
              "deviceType": responseJson.officer.deviceInfo[0].deviceType,
              "platform": responseJson.officer.deviceInfo[0].platform,
              "socketId": responseJson.officer.deviceInfo[0].socketId,
            },
          ],
        };
        /* SOCKET PUSH NOTIFICATION */
        this.broker.emit("socket.sendNotification", notificationObject);

        /* FCM */
        this.broker.emit("admin.sendFCM", notificationObject);
      }

      if (responseJson.booking) {
        const bookingData = await this.broker.emit("booking.viewRideDetails", {
          "bookingId": responseJson.booking._id.toString(),
        });
        return {
          "code": 200,
          "data": bookingObject(bookingData[0].data),
          "message": "",
        };
      } else {
        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      }
    } else if (responseJson.professional !== null) {
      const updatedEscortData = await this.adapter.model.updateMany(
        {
          "professional": responseJson.professional._id,
          "status": { "$ne": constantUtil.CLOSED },
        },
        {
          "escortStatus": constantUtil.CANCELLED,
        }
      );
      if (updatedEscortData.nModified === 0)
        return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

      this.broker.emit("professional.updateOnGoingSecurityBooking", {
        "id": responseJson.professional._id.toString(),
        "isEscortClosed": false,
      });

      responseJson = await this.adapter.model
        .findById(
          mongoose.Types.ObjectId(context.params.escortBookingId.toString())
        )
        .populate("user", {
          "password": 0,
          "bankDetails": 0,
          "trustedContacts": 0,
          "cards": 0,
        })
        .populate("professional", {
          "password": 0,
          "cards": 0,
          "trustedContacts": 0,
        })
        .populate("officer", { "password": 0, "preferences": 0 })
        .populate("booking");

      if (responseJson.status !== constantUtil.CLOSED)
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.professional._id.toString(),
        });
      if (responseJson.officer)
        this.broker.emit("officer.updateOnGoingBooking", {
          "officerId": responseJson.officer._id.toString(),
          "type": constantUtil.AWAITING,
        });

      if (responseJson.booking) {
        const bookingData = await this.broker.emit("booking.viewRideDetails", {
          "bookingId": responseJson.booking._id.toString(),
        });
        return {
          "code": 200,
          "data": bookingObject(bookingData[0].data),
          "message": "",
        };
      } else {
        return {
          "code": 200,
          "data": securityBookingViewObject(responseJson),
          "message": "",
        };
      }
    }
  } else {
    const updatedEscortData = await this.adapter.model.updateMany(
      {
        "officer": responseJson.officer._id,
        "status": { "$ne": constantUtil.CLOSED },
      },
      {
        "officer": null,
        "escortStatus": constantUtil.AWAITING,
        "isEscortApply": false,
        "activity.acceptTime": null,
        "activity.arriveTime": null,
      }
    );
    if (updatedEscortData.nModified === 0)
      return { "code": 0, "data": {}, "message": "ERROR IN SECURITY UPDATE" };

    if (responseJson.userType === constantUtil.USER) {
      this.broker.emit("user.updateOnGoingSecurityBooking", {
        "id": responseJson.user._id.toString(),
        "escortBookingId": responseJson._id.toString(),
        "type": responseJson.securityServiceName,
        "isOfficerAssigned": false,
        "isEscortClosed":
          responseJson.status === constantUtil.CLOSED ? true : false,
      });
    } else {
      this.broker.emit("professional.updateOnGoingSecurityBooking", {
        "id": responseJson.professional._id.toString(),
        "escortBookingId": responseJson._id.toString(),
        "type": responseJson.securityServiceName,
        "isOfficerAssigned": false,
        "isEscortClosed":
          responseJson.status === constantUtil.CLOSED ? true : false,
      });
    }

    if (responseJson.officer)
      this.broker.emit("officer.updateOnGoingBooking", {
        "officerId": responseJson.officer._id.toString(),
        "type": constantUtil.AWAITING,
      });

    if (responseJson.status !== constantUtil.CLOSED) {
      if (responseJson.userType === constantUtil.USER) {
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.user._id.toString(),
        });
      } else {
        this.broker.emit("socket.sendSecurityUpdatesToAdmin", {
          "id": responseJson.professional._id.toString(),
        });
      }
    }

    responseJson = await this.adapter.model
      .findById(
        mongoose.Types.ObjectId(context.params.escortBookingId.toString())
      )
      .populate("user", {
        "password": 0,
        "bankDetails": 0,
        "trustedContacts": 0,
        "cards": 0,
      })
      .populate("professional", {
        "password": 0,
        "cards": 0,
        "trustedContacts": 0,
      })
      .populate("officer", { "password": 0, "preferences": 0 })
      .populate("booking");

    if (responseJson.booking) {
      const bookingData = await this.broker.emit("booking.viewRideDetails", {
        "bookingId": responseJson.booking._id.toString(),
      });
      const notificationObject = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": constantUtil.ACTION_ESCORTCANCEL,
          "timestamp": Date.now(),
          "message": "YOUR REQUEST IS CANCELLED BY RESPONSE OFFICER",
          "details": bookingObject(bookingData[0].data),
        },
        "registrationTokens": [
          {
            "token": responseJson.user.deviceInfo[0].deviceId,
            "id": responseJson.user._id.toString(),
            "deviceType": responseJson.user.deviceInfo[0].deviceType,
            "platform": responseJson.user.deviceInfo[0].platform,
            "socketId": responseJson.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendSecurityAcceptNotify", {
        "id": context.params.escortBookingId,
      });
    } else {
      const notificationObject = {
        "data": {
          "type": constantUtil.NOTIFICATIONTYPE,
          "userType": constantUtil.USER,
          "action": constantUtil.ACTION_ESCORTCANCEL,
          "timestamp": Date.now(),
          "message": "YOUR REQUEST IS CANCELLED BY RESPONSE OFFICER",
          "details": securityBookingViewObject(responseJson),
        },
        "registrationTokens": [
          {
            "token": responseJson.user.deviceInfo[0].deviceId,
            "id": responseJson.user._id.toString(),
            "deviceType": responseJson.user.deviceInfo[0].deviceType,
            "platform": responseJson.user.deviceInfo[0].platform,
            "socketId": responseJson.user.deviceInfo[0].socketId,
          },
        ],
      };

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendNotification", notificationObject);

      /* SOCKET PUSH NOTIFICATION */
      this.broker.emit("socket.sendSecurityAcceptNotify", {
        "id": context.params.escortBookingId,
      });
    }

    return {
      "code": 200,
      "data": securityBookingViewObject(responseJson),
      "message": "",
    };
  }
};

securityEscortAction.escortBookingMediaReport = async function (context) {
  if (context.params.files && context.params.files.length > 0) {
    context.params.documents = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `mediaReport${Date.now()}${context.params.mediaType}`,
      }
    );
  }
  const updateData =
    context.params.mediaType === constantUtil.MESSAGE
      ? {
          "$set": {
            "mediaType": constantUtil.MESSAGE,
            "mediaReportMessage": context.params.message,
          },
        }
      : context.params.mediaType === constantUtil.VIDEO
      ? {
          "$set": {
            "mediaType": constantUtil.VIDEO,
            "mediaReportVideo": context.params.documents[0][0],
          },
        }
      : context.params.mediaType === constantUtil.AUDIO
      ? {
          "$set": {
            "mediaType": constantUtil.AUDIO,
            "mediaReportAudio": context.params.documents[0][0],
          },
        }
      : {};
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.escortBookingId.toString()),
    updateData
  );
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  } else {
    return {
      "code": 200,
      "data": responseJson,
      "message": "",
    };
  }
};

securityEscortAction.escortBookingMediaReportImage = async function (context) {
  if (context.params.files && context.params.files.length > 0) {
    context.params.documents = await this.broker.emit(
      "admin.uploadMultipleImageInSpaces",
      {
        "files": context.params.files,
        "fileName": `mediaReport${Date.now()}${constantUtil.IMAGE}`,
      }
    );
  }
  const updateData = {
    "$push": {
      "mediaReportImage": context.params.documents[0][0],
    },
  };
  const responseJson = await this.adapter.updateById(
    mongoose.Types.ObjectId(context.params.escortBookingId.toString()),
    updateData
  );
  if (!responseJson) {
    throw new MoleculerError("INVALID DETAILS");
  } else {
    return {
      "code": 200,
      "data": responseJson,
      "message": "",
    };
  }
};

securityEscortAction.getOfficerHistory = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  const condition = {
    "officer": mongoose.Types.ObjectId(context.meta.officerId.toString()),
  };

  query.push({
    "$match": {
      "escortStatus": {
        "$in": [constantUtil.ENDED],
      },
      ...condition,
    },
  });

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$limit": parseInt(limit) },
        { "$skip": parseInt(skip) },
        {
          "$lookup": {
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user",
          },
        },
        {
          "$unwind": {
            "path": "$user",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$lookup": {
            "from": "professionals",
            "localField": "professional",
            "foreignField": "_id",
            "as": "professional",
          },
        },
        {
          "$unwind": {
            "path": "$professional",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "acknowledge": "$acknowledge",
            "escortStatus": "$escortStatus",
            "priority": "$priority",
            "alertDate": "$alertDate",
            "alertType": "$alertType",
            "alertLocation": "$alertLocation",
            "fromLocation": "$fromLocation",
            "toLocation": "$toLocation",
            "activity": "$activity",
            "escortReview": "$escortReview",
            "isAdminForceAssign": "$isAdminForceAssign",
            "securityThreadId": "$securityThreadId",
            "ticketId": "$ticketId",
            "userType": "$userType",
            "user": "$user",
            "professional": "$professional",
            "securityServiceName": "$securityServiceName",
            "securityServiceId": "$securityServiceId",
            "threadMessage": "$threadMessage",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query);
  return {
    "code": 200,
    "data": jsonData[0].response.map((data) => {
      return securityBookingViewObject(data);
    }),
    "message": "",
  };
};

securityEscortAction.getUserEscortHistory = async function (context) {
  const skip = parseInt(context.params.skip ?? 0);
  const limit = parseInt(context.params.limit ?? 20);

  const query = [];
  const condition = {
    "user": mongoose.Types.ObjectId(context.meta.userId.toString()),
    "booking": null,
    "isEscortApply": true,
  };

  query.push({
    "$match": {
      "escortStatus": {
        "$in": [constantUtil.ENDED],
      },
      ...condition,
    },
  });

  query.push({
    "$facet": {
      "response": [
        { "$sort": { "_id": -1 } },
        { "$limit": parseInt(limit) },
        { "$skip": parseInt(skip) },
        {
          "$lookup": {
            "from": "officers",
            "localField": "officer",
            "foreignField": "_id",
            "as": "officer",
          },
        },
        {
          "$unwind": {
            "path": "$officer",
            "preserveNullAndEmptyArrays": true,
          },
        },
        {
          "$project": {
            "_id": "$_id",
            "acknowledge": "$acknowledge",
            "escortStatus": "$escortStatus",
            "priority": "$priority",
            "alertDate": "$alertDate",
            "alertType": "$alertType",
            "alertLocation": "$alertLocation",
            "fromLocation": "$fromLocation",
            "toLocation": "$toLocation",
            "activity": "$activity",
            "escortReview": "$escortReview",
            "isAdminForceAssign": "$isAdminForceAssign",
            "securityThreadId": "$securityThreadId",
            "ticketId": "$ticketId",
            "userType": "$userType",
            "officer": "$officer",
            "securityServiceName": "$securityServiceName",
            "securityServiceId": "$securityServiceId",
            "threadMessage": "$threadMessage",
          },
        },
      ],
    },
  });
  const jsonData = await this.adapter.model.aggregate(query);
  return {
    "code": 200,
    "data": jsonData[0].response.map((data) => {
      return securityBookingViewObject(data);
    }),
    "message": "",
  };
};
securityEscortAction.viewEscortDetails = async function (context) {
  const responseJson = await this.adapter.model
    .findById(mongoose.Types.ObjectId(context.params.id.toString()))
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "trustedContacts": 0,
      "cards": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("booking")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("operator");
  if (!responseJson) {
    throw new MoleculerError("ERROR IN ESCORT BOOKING DETAILS");
  } else {
    return responseJson;
  }
};

module.exports = securityEscortAction;
