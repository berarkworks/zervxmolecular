const formula = require("formula");
const mongoose = require("mongoose");
const { MoleculerError } = require("moleculer").Errors;

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const formulaJSON = require("../data/formula.json");
const geolib = require("geolib");
const googleApiUtil = require("../utils/googleApi.util");
const {
  calculateDistanceAndTimeFare,
  professionalObject,
} = require("../utils/mapping.util");
const { getDayAndMonthNameOfDate } = require("../utils/common.util");
const { toRegionalUTC } = require("../utils/helper.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

const categoryAction = {};

categoryAction.userGetLocationBasedCategory = async function (context) {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const userType = context.params.userType ?? "";
  const clientId = context.params.clientId || context.meta.clientId;
  let responseData = {},
    errorCode = 200,
    statusCode = "",
    message = "",
    serviceCategoryData = null,
    pickupServiceCategoryData = null,
    dropServiceCategoryData = null,
    // categoryNameConditionQuery = {},
    // categoryCount = null,
    rideBookingCategory = "",
    isInterCityRide = false,
    rideInterCityCategory = "",
    rideBookingServiceCategory = "",
    categoryNameList = [],
    checkPickupData = [],
    checkDropData = [],
    heatmapPeakfare = 0;

  const checkPickupLngLatData = [],
    checkDropLngLatData = [];

  const {
    INFO_BASE_FARE,
    INFO_SERVICE_TAX,
    INFO_TRAVEL_CHARGE,
    INFO_MINIMUM_CHARGE,
    INFO_SURCHARGE_FEE,
    INFO_BOUNDARY_CHARGE,
    INFO_INTERCITY_PICKUP_CHARGE,
    INFO_SERVICE_NOT_PROVIDE,
  } = notifyMessage.setNotifyLanguage(
    context.params.langCode || context.meta?.userDetails?.languageCode
  );

  const serviceCategoryName = (
    context.params?.serviceCategoryName || constantUtil.CONST_RIDE
  ).toLowerCase();
  // const serviceCategoryName = constantUtil.CONST_SHARERIDE.toLowerCase(); //need to change
  const const_ride = constantUtil.CONST_RIDE.toLowerCase(); //Must be Lowercase
  const const_corporateride = constantUtil.CONST_CORPORATERIDE.toLowerCase(); //Must be Lowercase
  const const_intercityride = constantUtil.CONST_INTERCITYRIDE.toLowerCase(); //Must be Lowercase
  const const_pickupoutsideride =
    constantUtil.CONST_PICKUPOUTSIDERIDE.toLowerCase(); //Must be Lowercase
  const const_dropoutsideride =
    constantUtil.CONST_DROPOUTSIDERIDE.toLowerCase(); //Must be Lowercase
  const const_pickupanddropoutsideride =
    constantUtil.CONST_PICKUPANDDROPOUTSIDERIDE.toLowerCase(); //Must be Lowercase
  // const const_shareride = constantUtil.CONST_SHARERIDE.toLowerCase(); //Must be Lowercase
  const const_packages = constantUtil.CONST_PACKAGES.toLowerCase(); //Must be Lowercase

  try {
    //#region User Current Service Area
    let userCurrentServiceCategory = await this.broker.emit(
      "category.getServieCategory",
      {
        // "lat": context.params.lat,
        // "lng": context.params.lng,
        "clientId": clientId,
        "lat": context.params.pickupLat,
        "lng": context.params.pickupLng,
        "categoryName": const_ride, //const_intercityride,
      }
    );
    userCurrentServiceCategory =
      userCurrentServiceCategory && userCurrentServiceCategory[0];
    const userCurrentServiceAreaId =
      userCurrentServiceCategory?._id?.toString() || null;
    //#endregion User Current Service Area

    //#region Check Intrecity & Get Data
    const isCheckIntercityRidePickupPoint =
      generalSettings.data.isCheckIntercityRidePickupPoint;
    const isCheckIntercityRideDropPoint =
      generalSettings.data.isCheckIntercityRideDropPoint;

    categoryNameList.push(const_ride); //Default Assign Value
    rideBookingServiceCategory = const_ride; //Default Assign Value

    if (serviceCategoryName === const_ride) {
      // Pickup Lng & Lat
      checkPickupLngLatData.push({
        "$match": {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_ride,
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [
                  context.params.pickupLng,
                  context.params.pickupLat,
                ],
              },
            },
          },
        },
      });
      // Drop Lng & Lat
      checkDropLngLatData.push({
        "$match": {
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_ride,
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.dropLng, context.params.dropLat],
              },
            },
          },
        },
      });
      //--------------

      const checkInterCityData = await this.adapter.model
        .aggregate([...checkPickupLngLatData, ...checkDropLngLatData])
        .allowDiskUse(true);
      checkPickupData = await this.adapter.model
        .aggregate([...checkPickupLngLatData])
        .allowDiskUse(true);
      checkDropData = await this.adapter.model
        .aggregate([...checkDropLngLatData])
        .allowDiskUse(true);
      //------------
      // Check Pick & Drop Point Outside
      if (rideBookingServiceCategory === const_ride) {
        if (
          generalSettings.data.isEnableIntercityRide && // new Change
          checkInterCityData.length === 0 &&
          checkPickupData.length === 1 &&
          checkDropData.length === 1
        ) {
          categoryNameList.push(const_intercityride);
          rideBookingCategory = "InterCity";
          rideBookingServiceCategory = const_intercityride;
          isInterCityRide = generalSettings.data.isEnableIntercityRide
            ? false
            : true;
        } else if (
          generalSettings.data.isEnableIntercityRide && // new Change
          checkInterCityData.length === 0 &&
          checkPickupData.length === 0 &&
          checkDropData.length === 0
        ) {
          categoryNameList.push(const_pickupanddropoutsideride);
          rideBookingCategory = "InterCity";
          rideBookingServiceCategory = const_pickupanddropoutsideride;
          isInterCityRide = true;
        }
      }
      // Check Pick Point Outside
      if (
        isCheckIntercityRidePickupPoint &&
        generalSettings.data.isEnableIntercityRide // new Change
      ) {
        if (
          rideBookingServiceCategory === const_ride &&
          checkPickupData.length === 0
        ) {
          categoryNameList.push(const_pickupoutsideride);
          rideBookingCategory = "InterCity";
          rideBookingServiceCategory = const_pickupoutsideride;
          isInterCityRide = true;
        }
        //  else {
        //   categoryNameList.push(const_ride);
        //   rideBookingServiceCategory = const_ride;
        // }
      }
      // Drop Pick Point Outside
      if (
        isCheckIntercityRideDropPoint &&
        generalSettings.data.isEnableIntercityRide // new Change
      ) {
        if (
          rideBookingServiceCategory === const_ride &&
          checkDropData.length === 0
        ) {
          categoryNameList.push(const_dropoutsideride);
          rideBookingCategory = "InterCity";
          rideBookingServiceCategory = const_dropoutsideride;
          isInterCityRide = true;
        }
      }
      // Check Pick & Drop Point Outside
      if (
        generalSettings.data.isEnableIntercityRide &&
        rideBookingServiceCategory !== const_ride &&
        checkPickupData.length >= 1 &&
        checkDropData.length >= 1
      ) {
        categoryNameList = [const_ride, const_intercityride];
        rideBookingCategory = "InterCity";
        rideBookingServiceCategory = const_intercityride;
        isInterCityRide = false;
      }
      //-----------------
      // // if (
      // //   checkPickupLngLatData.length === 0 &&
      // //   checkDropLngLatData.length === 0
      // // ) {
      // //   rideBookingServiceCategory = const_intercityride;
      // //   categoryNameList.push(const_intercityride);
      // //   isInterCityRide = true;
      // // } else
      // if (checkPickupLngLatData.length > 0 || checkDropLngLatData.length > 0) {
      //   const checkInterCityData = await this.adapter.model.aggregate([
      //     ...checkPickupLngLatData,
      //     ...checkDropLngLatData,
      //   ]);
      //   // if (
      //   //   generalSettings.data.isEnableIntercityRide &&
      //   //   checkInterCityData.length === 0
      //   // ) {
      //   //   rideBookingCategory = "InterCity";
      //   //   rideInterCityCategory = "intercitypickup";
      //   //   rideBookingServiceCategory = const_intercityride;
      //   //   categoryNameList.push(const_intercityride);
      //   //   isInterCityRide = true;
      //   // } else {
      //   //   categoryNameList.push(const_ride);
      //   //   rideBookingServiceCategory = const_ride;
      //   // }
      //   if (generalSettings.data.isEnableIntercityRide) {
      //     if (checkInterCityData.length > 0) {
      //       categoryNameList.push(const_ride);
      //       rideBookingServiceCategory = const_ride;
      //       isInterCityRide = false;
      //     }
      //     //  else {
      //     //   rideBookingCategory = "InterCity";
      //     //   rideInterCityCategory = "intercitypickup";
      //     //   rideBookingServiceCategory = const_intercityride;
      //     //   categoryNameList.push(const_intercityride);
      //     //   isInterCityRide = true;
      //     // }
      //   }

      //   if (
      //     checkPickupData[0]?._id?.toString() !==
      //       checkDropData[0]?._id?.toString() &&
      //     rideBookingServiceCategory === const_ride
      //   ) {
      //     categoryNameList.push(const_intercityride);
      //     rideBookingServiceCategory = const_intercityride;
      //     isInterCityRide = false;
      //   }
      // } else {
      //   categoryNameList.push(const_pickupanddropoutsideride);
      //   rideBookingServiceCategory = const_pickupanddropoutsideride;
      //   isInterCityRide = true;
      // }
    }
    //#endregion Check Intrecity & Get Data

    //#region region Pickup Location
    if (serviceCategoryName === const_corporateride) {
      // categoryNameList.push(const_corporateride);
      rideBookingServiceCategory = const_corporateride;
      pickupServiceCategoryData = await this.adapter.model
        .find({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_corporateride,
          "corporateId": mongoose.Types.ObjectId(
            context.params?.corporateId?.toString()
          ),
          "status": constantUtil.ACTIVE,
        })
        .sort({ "recordType": 1 })
        .lean();

      if (pickupServiceCategoryData.length === 0) {
        rideBookingServiceCategory = const_ride;
      } else {
        const isValidInterCityRide = await this.adapter.model.count({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_corporateride,
          "corporateId": mongoose.Types.ObjectId(
            context.params?.corporateId?.toString()
          ),
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [
                  context.params.pickupLng,
                  context.params.pickupLat,
                ],
              },
            },
          },
        });
        if (isValidInterCityRide === 0) {
          isInterCityRide = true;
        }
      }
    } else if (serviceCategoryName === const_packages) {
      // categoryNameList.push(const_corporateride);
      rideBookingServiceCategory = const_packages;
      pickupServiceCategoryData = await this.adapter.model
        .find({
          "categoryName": const_packages,
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [
                  context.params.pickupLng,
                  context.params.pickupLat,
                ],
              },
            },
          },
        })
        .sort({ "recordType": 1 })
        .lean();
    }

    // if (serviceCategoryName === const_corporateride) {
    //   rideBookingServiceCategory = const_corporateride;
    //   categoryNameConditionQuery = {
    //     "$in": [
    //       const_ride,
    //       constantUtil.CONST_CORPORATERIDE.toLowerCase(),
    //     ],
    //   };
    // } else {
    //   categoryNameConditionQuery = {
    //     "$in": [
    //       const_ride,
    //       constantUtil.CONST_INTERCITYRIDE.toLowerCase(),
    //     ],
    //   };
    // }
    // if (!pickupServiceCategoryData) {
    //   rideBookingServiceCategory = const_ride;
    //   pickupServiceCategoryData = await this.adapter.model
    //     .findOne({
    //       "categoryName": const_ride,
    //       "status": constantUtil.ACTIVE,
    //       "location": {
    //         "$geoIntersects": {
    //           "$geometry": {
    //             "type": "Point",
    //             "coordinates": [
    //               context.params.pickupLng,
    //               context.params.pickupLat,
    //             ],
    //           },
    //         },
    //       },
    //     })
    //     .sort({ "recordTypeNumber": -1, "$near": 1 })
    //     .lean();
    // }
    // if (!pickupServiceCategoryData) {
    //   pickupServiceCategoryData = await this.adapter.model
    //     .findOne({
    //       "categoryName": const_intercityride,
    //       "status": constantUtil.ACTIVE,
    //       "location": {
    //         "$geoIntersects": {
    //           "$geometry": {
    //             "type": "Point",
    //             "coordinates": [
    //               context.params.pickupLng,
    //               context.params.pickupLat,
    //             ],
    //           },
    //         },
    //       },
    //     })
    //     .sort({ "recordTypeNumber": -1, "$near": 1 })
    //     .lean();
    //   if (pickupServiceCategoryData) {
    //     rideBookingCategory = "InterCity";
    //     rideInterCityCategory = "pickup";
    //     rideBookingServiceCategory = const_intercityride;
    //     isInterCityRide = true;
    //   }
    //   // return {
    //   //   "code": 200,
    //   //   "status": constantUtil.PICKUPLOCATIONOUT,
    //   //   "data": [],
    //   // };
    // }
    //----------
    if (!pickupServiceCategoryData || pickupServiceCategoryData.length === 0) {
      // rideBookingServiceCategory = const_ride;
      pickupServiceCategoryData = await this.adapter.model
        .find({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": {
            // "$in": [const_ride, const_intercityride],
            "$in": categoryNameList,
          },
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [
                  context.params.pickupLng,
                  context.params.pickupLat,
                ],
              },
            },
          },
        })
        .sort({ "recordType": 1 })
        .lean();

      // if (pickupServiceCategoryData[0]?.categoryName === const_intercityride) {
      //   if (
      //     generalSettings.data.isEnableIntercityRide &&
      //     pickupServiceCategoryData[0]?._id?.toString() !==
      //       userCurrentServiceAreaId //current Location
      //   ) {
      //     rideBookingCategory = "InterCity";
      //     rideInterCityCategory = "intercitypickup";
      //     rideBookingServiceCategory = const_intercityride;
      //     isInterCityRide = true;
      //   }
      // }
      // // pickupServiceCategoryData = pickupServiceCategoryData.filter(
      // //   (item) => item.categoryName === rideBookingServiceCategory
      // // )[0];
      // // return {
      // //   "code": 200,
      // //   "status": constantUtil.PICKUPLOCATIONOUT,
      // //   "data": [],
      // // };
    }
    pickupServiceCategoryData = pickupServiceCategoryData.filter(
      (item) => item.categoryName === rideBookingServiceCategory
    )[0]; //Get Fare Setup Based on pickup Location
    // if (userCurrentServiceAreaId) {
    //   if (
    //     // generalSettings.data.isEnableIntercityRide &&
    //     pickupServiceCategoryData?._id?.toString() !== userCurrentServiceAreaId //current Location
    //   ) {
    //     rideBookingCategory = "InterCity";
    //     rideBookingServiceCategory = const_intercityride;
    //     isInterCityRide = true;
    //   }
    // }
    //#endregion region Pickup Location

    //#region region Drop Location
    if (serviceCategoryName === const_corporateride) {
      // categoryNameList.push(const_corporateride);
      rideBookingServiceCategory = const_corporateride;
      // categoryCount = await this.adapter.model.count({
      dropServiceCategoryData = await this.adapter.model
        .find({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_corporateride,
          "corporateId": mongoose.Types.ObjectId(
            context.params?.corporateId?.toString()
          ),
          "status": constantUtil.ACTIVE,
        })
        .sort({ "recordType": 1 })
        .lean();

      if (dropServiceCategoryData.length === 0) {
        rideBookingServiceCategory = const_ride;
      } else {
        const isValidInterCityRide = await this.adapter.model.count({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": const_corporateride,
          "corporateId": mongoose.Types.ObjectId(
            context.params?.corporateId?.toString()
          ),
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.dropLng, context.params.dropLat],
              },
            },
          },
        });
        if (isValidInterCityRide === 0) {
          isInterCityRide = true;
        }
      }
    } else if (serviceCategoryName === const_packages) {
      // categoryNameList.push(const_corporateride);
      rideBookingServiceCategory = const_packages;
      // categoryCount = await this.adapter.model.count({
      dropServiceCategoryData = await this.adapter.model
        .find({
          "categoryName": const_packages,
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.dropLng, context.params.dropLat],
              },
            },
          },
        })
        .sort({ "recordType": 1 })
        .lean();
    }

    // if (!dropServiceCategoryData) {
    //   rideBookingServiceCategory = const_ride;
    //   // categoryCount = await this.adapter.model.count({
    //   dropServiceCategoryData = await this.adapter.model
    //     .findOne({
    //       "categoryName": const_ride,
    //       "status": constantUtil.ACTIVE,
    //       "location": {
    //         "$geoIntersects": {
    //           "$geometry": {
    //             "type": "Point",
    //             "coordinates": [context.params.dropLng, context.params.dropLat],
    //           },
    //         },
    //       },
    //     })
    //     .sort({ "recordTypeNumber": -1, "$near": 1 })
    //     .lean();
    // }
    // // if (categoryCount <= 0) {
    // if (!dropServiceCategoryData) {
    //   // categoryCount = await this.adapter.model.count({
    //   dropServiceCategoryData = await this.adapter.model
    //     .findOne({
    //       "categoryName": const_intercityride,
    //       "status": constantUtil.ACTIVE,
    //       "location": {
    //         "$geoIntersects": {
    //           "$geometry": {
    //             "type": "Point",
    //             "coordinates": [context.params.dropLng, context.params.dropLat],
    //           },
    //         },
    //       },
    //     })
    //     .sort({ "recordTypeNumber": -1, "$near": 1  })
    //     .lean();
    //   if (dropServiceCategoryData) {
    //     rideBookingCategory = "InterCity";
    //     rideInterCityCategory = "drop";
    //     rideBookingServiceCategory =
    //       const_intercityride;
    //     isInterCityRide = true;
    //   }
    //   // return { "code": 200, "status": constantUtil.DROPLOCATIONOUT, "data": [] };
    // }
    if (!dropServiceCategoryData || dropServiceCategoryData.length === 0) {
      // rideBookingServiceCategory = const_ride;
      dropServiceCategoryData = await this.adapter.model
        .find({
          //"clientId": mongoose.Types.ObjectId(clientId),
          "categoryName": {
            // "$in": [const_ride, const_intercityride],
            "$in": categoryNameList,
          },
          "status": constantUtil.ACTIVE,
          "location": {
            "$geoIntersects": {
              "$geometry": {
                "type": "Point",
                "coordinates": [context.params.dropLng, context.params.dropLat],
              },
            },
          },
        })
        .sort({ "recordType": 1 })
        .lean();

      // if (dropServiceCategoryData[0]?.categoryName === const_intercityride) {
      //   if (
      //     generalSettings.data.isEnableIntercityRide &&
      //     dropServiceCategoryData[0]?._id?.toString() !==
      //       userCurrentServiceAreaId //current Location
      //   ) {
      //     rideBookingCategory = "InterCity";
      //     rideInterCityCategory = "intercitydrop";
      //     rideBookingServiceCategory = const_intercityride;
      //     isInterCityRide = true;
      //   }
      //   // else {
      //   //   // dropServiceCategoryData = dropServiceCategoryData.filter(
      //   //   //   (item) => item.categoryName === const_ride
      //   //   // );
      //   //   // dropServiceCategoryData.shift(); //removes the first item of an array intercityride
      //   // }
      // }
    }
    dropServiceCategoryData = dropServiceCategoryData.filter(
      (item) => item.categoryName === rideBookingServiceCategory
    )[0];
    // }
    //#endregion region Drop Location

    // // serviceCategoryData = dropServiceCategoryData || pickupServiceCategoryData;
    // // if (!serviceCategoryData || categoryCount <= 0) {
    // if (!pickupServiceCategoryData || !dropServiceCategoryData) {
    //   // // statusCode =
    //   // //   generalSettings.data.isEnableIntercityRide === true
    //   // //     ? constantUtil.SERVICENOTAVAILABLE
    //   // //     : constantUtil.NOCATEGORYAVAILABLE;

    //   // switch (generalSettings.data?.isEnableIntercityRide) {
    //   //   case true:
    //   //     statusCode = constantUtil.SERVICENOTAVAILABLE;
    //   //     break;

    //   //   default:
    //   if (!pickupServiceCategoryData && !dropServiceCategoryData) {
    //     // statusCode = constantUtil.NOCATEGORYAVAILABLE;
    //     statusCode = constantUtil.PICKUPANDDROPLOCATIONOUT;
    //   } else if (!pickupServiceCategoryData) {
    //     statusCode = constantUtil.PICKUPLOCATIONOUT;
    //   } else if (!dropServiceCategoryData) {
    //     statusCode = constantUtil.DROPLOCATIONOUT;
    //   } else {
    //     statusCode = constantUtil.NOCATEGORYAVAILABLE;
    //   }
    //   //     break;
    //   // }

    //   //Need to Work --> statusCode SERVICENOTAVAILABLE Based workflow
    //   if (statusCode === constantUtil.SERVICENOTAVAILABLE) {
    //     // await this.broker.emit("booking.notifyScheduleRideToAdmin", {
    //     //   "bookingId": null,
    //     //   "detailsData": {},
    //     // });
    //     // notify to admin
    //     const notificationObject = {
    //      "clientId": clientId,
    //       "data": {
    //         "type": constantUtil.NOTIFICATIONTYPE,
    //         "userType": constantUtil.ADMIN,
    //         "action": constantUtil.ACTION_CANCELBOOKING,
    //         "timestamp": Date.now(),
    //         "message": "PROFESSIONAL_RIDE_CANCEL",
    //         "details": {},
    //       },
    //       "registrationTokens": [
    //         {
    //           "token": "",
    //           "id": context.params?.professionalId,
    //           "deviceType": "",
    //           "platform": "",
    //           "socketId": "",
    //         },
    //       ],
    //     };
    //     this.broker.emit(
    //       "socket.sendSecurityNotificationToAdmin",
    //       notificationObject
    //     );

    //     return {
    //       "code": 200,
    //       "status": statusCode,
    //       "data": [],
    //     };
    //   } else if (statusCode === constantUtil.PICKUPLOCATIONOUT) {
    //     serviceCategoryData = dropServiceCategoryData;
    //   } else if (statusCode === constantUtil.DROPLOCATIONOUT) {
    //     serviceCategoryData = pickupServiceCategoryData;
    //     // } else if (statusCode === constantUtil.NOCATEGORYAVAILABLE) { }
    //   } else if (statusCode === constantUtil.PICKUPANDDROPLOCATIONOUT) {
    //     if (serviceCategoryName === const_corporateride) {
    //       // rideBookingCategory = "InterCity";
    //       // categoryNameList.push(const_ride);
    //       rideBookingServiceCategory = const_corporateride;
    //       isInterCityRide = false;
    //     }
    //     //  else {
    //     //   rideBookingCategory = "InterCity";
    //     //   categoryNameList.push(const_pickupanddropoutsideride);
    //     //   rideBookingServiceCategory = const_pickupanddropoutsideride;
    //     //   isInterCityRide = true;
    //     //   // serviceCategoryData = await this.adapter.model
    //     //   //   .findOne({
    //     //   //     // .find({
    //     //   //     "categoryName": {
    //     //   //       // "$in": [const_ride, const_pickupanddropoutsideride],
    //     //   //       "$in": categoryNameList,
    //     //   //     },
    //     //   //     "status": constantUtil.ACTIVE,
    //     //   //     "location": {
    //     //   //       "$near": {
    //     //   //         "$geometry": {
    //     //   //           "type": "Point",
    //     //   //           "coordinates": [
    //     //   //             context.params.pickupLng,
    //     //   //             context.params.pickupLat,
    //     //   //           ],
    //     //   //         },
    //     //   //         // "$minDistance": 0,
    //     //   //         // "$maxDistance": 100000,
    //     //   //       },
    //     //   //     },
    //     //   //   })
    //     //   //   // .sort({ "recordType": 1 })
    //     //   //   .lean();
    //     // }
    //     serviceCategoryData = await this.adapter.model
    //       .findOne({
    //         // .find({
    //         "categoryName": const_ride,
    //         "status": constantUtil.ACTIVE,
    //         "location": {
    //           "$near": {
    //             "$geometry": {
    //               "type": "Point",
    //               "coordinates": [
    //                 context.params.pickupLng,
    //                 context.params.pickupLat,
    //               ],
    //             },
    //             // "$minDistance": 0,
    //             // "$maxDistance": 100000,
    //           },
    //         },
    //       })
    //       .lean();
    //   }
    // } else {
    //   statusCode = constantUtil.CATEGORYAVAILABLE;
    //   // serviceCategoryData =
    //   //   rideInterCityCategory === "intercitydrop"
    //   //     ? dropServiceCategoryData
    //   //     : pickupServiceCategoryData;

    //   serviceCategoryData = pickupServiceCategoryData;
    // }
    // serviceCategoryData = dropServiceCategoryData || pickupServiceCategoryData;
    // if (!serviceCategoryData || categoryCount <= 0) {
    if (!pickupServiceCategoryData || !dropServiceCategoryData) {
      if (
        serviceCategoryName === const_corporateride // || generalSettings.data.isEnableIntercityRide === false
      ) {
        rideBookingServiceCategory = const_ride;
        isInterCityRide = false;
      } else if (generalSettings.data.isEnableIntercityRide === false) {
        throw new MoleculerError(INFO_SERVICE_NOT_PROVIDE, 200); // new Change
      }

      if (pickupServiceCategoryData) {
        serviceCategoryData = pickupServiceCategoryData;
      } else {
        serviceCategoryData = await this.getNearByOneServiceLocation({
          "clientId": clientId,
          "pickupLat": context.params.pickupLat,
          "pickupLng": context.params.pickupLng,
          "serviceCategory": rideBookingServiceCategory,
        });
        // serviceCategoryData = await this.adapter.model
        //   .findOne({
        //     // .find({
        //     "categoryName": rideBookingServiceCategory,
        //     "status": constantUtil.ACTIVE,
        //     "location": {
        //       "$near": {
        //         "$geometry": {
        //           "type": "Point",
        //           "coordinates": [
        //             context.params.pickupLng,
        //             context.params.pickupLat,
        //           ],
        //         },
        //         // "$minDistance": 0,
        //         // "$maxDistance": 100000,
        //       },
        //     },
        //   })
        //   .lean();
      }
      if (!serviceCategoryData) {
        serviceCategoryData = await this.getNearByOneServiceLocation({
          "clientId": clientId,
          "pickupLat": context.params.pickupLat,
          "pickupLng": context.params.pickupLng,
          "serviceCategory": const_ride,
        });
        // serviceCategoryData = await this.adapter.model
        //   .findOne({
        //     // .find({
        //     "categoryName": const_ride,
        //     "status": constantUtil.ACTIVE,
        //     "location": {
        //       "$near": {
        //         "$geometry": {
        //           "type": "Point",
        //           "coordinates": [
        //             context.params.pickupLng,
        //             context.params.pickupLat,
        //           ],
        //         },
        //         // "$minDistance": 0,
        //         // "$maxDistance": 100000,
        //       },
        //     },
        //   })
        //   .lean();
      }
      // if (!pickupServiceCategoryData && !dropServiceCategoryData) {
      //   // statusCode = constantUtil.NOCATEGORYAVAILABLE;
      //   statusCode = constantUtil.PICKUPANDDROPLOCATIONOUT;
      // } else if (!pickupServiceCategoryData) {
      //   statusCode = constantUtil.PICKUPLOCATIONOUT;
      // } else if (!dropServiceCategoryData) {
      //   statusCode = constantUtil.DROPLOCATIONOUT;
      // } else {
      //   statusCode = constantUtil.NOCATEGORYAVAILABLE;
      // }
      // //     break;
      // // }

      // //Need to Work --> statusCode SERVICENOTAVAILABLE Based workflow
      // if (statusCode === constantUtil.SERVICENOTAVAILABLE) {
      //   // await this.broker.emit("booking.notifyScheduleRideToAdmin", {
      //   //   "bookingId": null,
      //   //   "detailsData": {},
      //   // });
      //   // notify to admin
      //   const notificationObject = {
      //    "clientId":  clientId,
      //     "data": {
      //       "type": constantUtil.NOTIFICATIONTYPE,
      //       "userType": constantUtil.ADMIN,
      //       "action": constantUtil.ACTION_CANCELBOOKING,
      //       "timestamp": Date.now(),
      //       "message": "PROFESSIONAL_RIDE_CANCEL",
      //       "details": {},
      //     },
      //     "registrationTokens": [
      //       {
      //         "token": "",
      //         "id": context.params?.professionalId,
      //         "deviceType": "",
      //         "platform": "",
      //         "socketId": "",
      //       },
      //     ],
      //   };
      //   this.broker.emit(
      //     "socket.sendSecurityNotificationToAdmin",
      //     notificationObject
      //   );

      //   return {
      //     "code": 200,
      //     "status": statusCode,
      //     "data": [],
      //   };
      // } else if (statusCode === constantUtil.PICKUPLOCATIONOUT) {
      //   serviceCategoryData = dropServiceCategoryData;
      // } else if (statusCode === constantUtil.DROPLOCATIONOUT) {
      //   serviceCategoryData = pickupServiceCategoryData;
      // } else if (statusCode === constantUtil.PICKUPANDDROPLOCATIONOUT) {
      //   if (serviceCategoryName === const_corporateride) {
      //     rideBookingServiceCategory = const_corporateride;
      //     isInterCityRide = false;
      //   }
      //   serviceCategoryData = await this.adapter.model
      //     .findOne({
      //       // .find({
      //       "categoryName": const_ride,
      //       "status": constantUtil.ACTIVE,
      //       "location": {
      //         "$near": {
      //           "$geometry": {
      //             "type": "Point",
      //             "coordinates": [
      //               context.params.pickupLng,
      //               context.params.pickupLat,
      //             ],
      //           },
      //           // "$minDistance": 0,
      //           // "$maxDistance": 100000,
      //         },
      //       },
      //     })
      //     .lean();
      // }
    } else {
      statusCode = constantUtil.CATEGORYAVAILABLE;

      serviceCategoryData = pickupServiceCategoryData;
    }

    const bookingDate = new Date(context.params.bookingDate);
    const bookingNextDate = new Date(
      new Date(
        new Date(new Date().setDate(new Date().getDate() + 1)).setHours(
          new Date(context.params.bookingDate).getHours()
        )
      ).setMinutes(new Date(context.params.bookingDate).getMinutes())
    );
    //
    const regionalBookingDate = toRegionalUTC(
      new Date(context.params.bookingDate)
    );
    const dayMonthData = await getDayAndMonthNameOfDate(bookingDate, "UPPER");
    const specialDaysData = await storageUtil.read(
      constantUtil.CONST_SPECIALDAYS
    );
    const checkSpecialDayPeakHours = await specialDaysData.filter((e) => {
      if (
        // new Date(regionalBookingDate).setHours(0, 0, 0, 0) ===
        //   new Date(e.data.specialDays.date).setHours(0, 0, 0, 0) &&
        // e.data.specialDays.status === true &&
        // Date.parse(regionalBookingDate) >=
        //   Date.parse(e.data.specialDays.startTime) &&
        // Date.parse(regionalBookingDate) <=
        //   Date.parse(e.data.specialDays.endTime)
        serviceCategoryData?._id?.toString() == e.data.serviceAreaId &&
        new Date(context.params.bookingDate).toISOString().substring(0, 10) ===
          new Date(e.data.specialDays.date).toISOString().substring(0, 10) &&
        e.data.specialDays.status === true &&
        Date.parse(bookingDate) >=
          Date.parse(new Date(e.data.specialDays.startTime)) &&
        Date.parse(bookingDate) <=
          Date.parse(new Date(e.data.specialDays.endTime))
      ) {
        return e.data.specialDays;
      }
    });
    //
    const checkPeakTiming = (data) => {
      let peakHoursData = null,
        isApplicablePeakHours = false;
      try {
        if (data.status) {
          const checkWeeklyPeakHours =
            data.weekDays &&
            data.weekDays.filter((e) => {
              if (e.day === dayMonthData.dayName && e.status === true) return e;
            });
          if (checkSpecialDayPeakHours.length > 0) {
            peakHoursData = checkSpecialDayPeakHours[0].data.specialDays;
            if (
              Date.parse(bookingDate) >=
                Date.parse(new Date(peakHoursData.startTime)) &&
              Date.parse(bookingDate) <=
                Date.parse(new Date(peakHoursData.endTime))
            ) {
              isApplicablePeakHours = true;
            }
          } else if (checkWeeklyPeakHours.length > 0) {
            peakHoursData = checkWeeklyPeakHours[0];
            // }
            // if (peakHoursData) {
            let startTime =
              parseFloat(new Date(peakHoursData.startTime).getHours()) +
              parseFloat(new Date(peakHoursData.startTime).getMinutes() / 60);
            let endTime =
              parseFloat(new Date(peakHoursData.endTime).getHours()) +
              parseFloat(new Date(peakHoursData.endTime).getMinutes() / 60);
            const startDate = new Date().setDate(new Date().getDate());
            const endDate = new Date().setDate(new Date().getDate() + 1);
            if (startTime > endTime) {
              startTime = new Date(
                new Date(
                  new Date(new Date(startDate)).setHours(
                    new Date(peakHoursData.startTime).getHours()
                  )
                ).setMinutes(new Date(peakHoursData.startTime).getMinutes())
              );
              endTime = new Date(
                new Date(
                  new Date(new Date(endDate)).setHours(
                    new Date(peakHoursData.endTime).getHours()
                  )
                ).setMinutes(new Date(peakHoursData.endTime).getMinutes())
              );
              if (
                Date.parse(bookingDate) >= Date.parse(startTime) ||
                Date.parse(bookingNextDate) <= Date.parse(endTime)
              ) {
                isApplicablePeakHours = true;
              }
            } else {
              startTime = new Date(
                new Date(
                  new Date(new Date()).setHours(
                    new Date(peakHoursData.startTime).getHours()
                  )
                ).setMinutes(new Date(peakHoursData.startTime).getMinutes())
              );
              endTime = new Date(
                new Date(
                  new Date(new Date()).setHours(
                    new Date(peakHoursData.endTime).getHours()
                  )
                ).setMinutes(new Date(peakHoursData.endTime).getMinutes())
              );
              if (
                Date.parse(bookingDate) >= Date.parse(startTime) &&
                Date.parse(bookingDate) <= Date.parse(endTime)
              ) {
                isApplicablePeakHours = true;
              }
            }
          }
        }
      } catch (e) {
        isApplicablePeakHours = false;
      }
      return isApplicablePeakHours;
    };

    const checkNightTiming = (data) => {
      let isApplicableNightHours = false;
      try {
        if (data.status) {
          let startTime =
            parseFloat(new Date(data.startTime).getHours()) +
            parseFloat(new Date(data.startTime).getMinutes() / 60);
          let endTime =
            parseFloat(new Date(data.endTime).getHours()) +
            parseFloat(new Date(data.endTime).getMinutes() / 60);
          // const startDate = new Date().setDate(new Date().getDate());
          // const endDate = new Date().setDate(new Date().getDate() + 1);
          const startDate = new Date().setDate(new Date(bookingDate).getDate());
          const endDate = new Date().setDate(
            new Date(bookingDate).getDate() + 1
          );
          if (startTime > endTime) {
            const nextDay = new Date(
              new Date(bookingDate).setDate(new Date(bookingDate).getDate() + 1)
            );
            startTime = new Date(
              new Date(
                new Date(new Date(startDate)).setHours(
                  new Date(data.startTime).getHours()
                )
              ).setMinutes(new Date(data.startTime).getMinutes())
            );
            endTime = new Date(
              new Date(
                new Date(new Date(endDate)).setHours(
                  new Date(data.endTime).getHours()
                )
              ).setMinutes(new Date(data.endTime).getMinutes())
            );
            // if (
            //   Date.parse(bookingDate) >= Date.parse(startTime) ||
            //   Date.parse(bookingNextDate) <= Date.parse(endTime)
            // )
            if (
              Date.parse(bookingDate) >= Date.parse(startTime) ||
              Date.parse(nextDay) <= Date.parse(endTime)
            ) {
              isApplicableNightHours = true;
            }
          } else {
            startTime = new Date(
              new Date(
                // new Date(new Date()).setHours(
                new Date(bookingDate).setHours(
                  new Date(data.startTime).getHours()
                )
              ).setMinutes(new Date(data.startTime).getMinutes())
            );
            endTime = new Date(
              new Date(
                // new Date(new Date())
                new Date(bookingDate).setHours(
                  new Date(data.endTime).getHours()
                )
              ).setMinutes(new Date(data.endTime).getMinutes())
            );
            if (
              Date.parse(bookingDate) >= Date.parse(startTime) &&
              Date.parse(bookingDate) <= Date.parse(endTime)
            ) {
              isApplicableNightHours = true;
            }
          }
        }
      } catch (e) {
        isApplicableNightHours = false;
      }
      return isApplicableNightHours;
    };

    const invoiceSetup = (data) => {
      const responseJson = {};
      responseJson["fareSummary"] = [];
      Object.keys(data).forEach((key, indx) => {
        const fareobj = {};
        switch (key) {
          case "baseFare":
            fareobj["title"] = INFO_BASE_FARE;
            fareobj["key"] = key;
            fareobj["value"] = parseFloat(data[key]);
            fareobj["currencyCode"] = data.currencyCode;
            fareobj["currencySymbol"] = data.currencySymbol;
            fareobj["isBold"] = false;
            fareobj["orderBy"] = indx;
            responseJson["fareSummary"].push(fareobj);
            break;
          case "serviceTax":
            if (parseFloat(data[key]) > 0) {
              fareobj["title"] = INFO_SERVICE_TAX;
              fareobj["key"] = key;
              fareobj["value"] = parseFloat(data[key]);
              fareobj["currencyCode"] = data.currencyCode;
              fareobj["currencySymbol"] = data.currencySymbol;
              fareobj["isBold"] = false;
              fareobj["orderBy"] = indx;
              responseJson["fareSummary"].push(fareobj);
            }
            break;
          case "travelCharge":
            fareobj["title"] = INFO_TRAVEL_CHARGE;
            fareobj["key"] = key;
            fareobj["value"] = parseFloat(data[key]);
            fareobj["currencyCode"] = data.currencyCode;
            fareobj["currencySymbol"] = data.currencySymbol;
            fareobj["isBold"] = false;
            fareobj["orderBy"] = indx;
            responseJson["fareSummary"].push(fareobj);
            break;
          case "minimumCharge":
            fareobj["title"] = INFO_MINIMUM_CHARGE;
            fareobj["key"] = key;
            fareobj["value"] = parseFloat(data[key]);
            fareobj["currencyCode"] = data.currencyCode;
            fareobj["currencySymbol"] = data.currencySymbol;
            fareobj["isBold"] = true;
            fareobj["orderBy"] = indx;
            responseJson["fareSummary"].push(fareobj);
            break;
          case "surchargeFee":
            if (parseFloat(data[key]) > 0) {
              fareobj["title"] = INFO_SURCHARGE_FEE;
              fareobj["key"] = key;
              fareobj["value"] = parseFloat(data[key]);
              fareobj["currencyCode"] = data.currencyCode;
              fareobj["currencySymbol"] = data.currencySymbol;
              fareobj["isBold"] = true;
              fareobj["orderBy"] = indx;
              responseJson["fareSummary"].push(fareobj);
            }
            break;
          case "intercityBoundaryFare":
            if (parseFloat(data[key]) > 0) {
              fareobj["title"] = INFO_BOUNDARY_CHARGE;
              fareobj["key"] = key;
              fareobj["value"] = parseFloat(data[key]);
              fareobj["currencyCode"] = data.currencyCode;
              fareobj["currencySymbol"] = data.currencySymbol;
              fareobj["isBold"] = true;
              fareobj["orderBy"] = indx;
              responseJson["fareSummary"].push(fareobj);
            }
            break;
          case "intercityPickupCharge":
            if (parseFloat(data[key]) > 0) {
              fareobj["title"] = INFO_INTERCITY_PICKUP_CHARGE;
              fareobj["key"] = key;
              fareobj["value"] = parseFloat(data[key]);
              fareobj["currencyCode"] = data.currencyCode;
              fareobj["currencySymbol"] = data.currencySymbol;
              fareobj["isBold"] = true;
              fareobj["orderBy"] = indx;
              responseJson["fareSummary"].push(fareobj);
            }
            break;
        }
      });
      responseJson["fareSummary"].sort(function (a, b) {
        return a.orderBy - b.orderBy;
      });
      return responseJson.fareSummary;
    };

    /* @TODO ALL VEICLE CATEGORY & CALCULATION */
    const vehicleCategoryJson = await storageUtil.read(
      constantUtil.VEHICLECATEGORY
    );
    // -------- Assign Distance Value Start---------------
    const distanceTypeUnitValue =
      (serviceCategoryData?.distanceType || constantUtil.KM).toUpperCase() ===
      constantUtil.KM
        ? constantUtil.KM_VALUE
        : constantUtil.MILES_VALUE;
    // -------- Assign Distance Value End---------------
    const categoryMappingJSON = {
      "serviceAreaID": serviceCategoryData._id,
      "serviceAreaName": serviceCategoryData.locationName,
      "serviceCategory":
        isInterCityRide === true ? const_intercityride : const_ride,
      "paymentMode": serviceCategoryData?.paymentMode || [constantUtil.CASH],
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceType": serviceCategoryData.distanceType,
      "distanceTypeUnitValue": distanceTypeUnitValue,
      "isShowProfessionalList":
        serviceCategoryData.isShowProfessionalList || false,
      "retryRequestDistance": generalSettings.data.retryRequestDistance,
      "maxRetryRequestDistance": serviceCategoryData.maxRetryRequestDistance,
      "roundingType":
        serviceCategoryData.roundingType || constantUtil.CONST_INTEGER,
      "roundingFactor": serviceCategoryData.roundingFactor,
      "professionalList": [],
      "couponList": [],
      "categories": [],
    };

    //#region Intercity Ride pickup Distance Calculation
    let estimationPickupDistance = 0,
      estimationPickupTime = 0,
      estimationDropDistance = 0,
      estimationDropTime = 0;

    // const isShareRide = serviceCategoryName === const_shareride ? true : false;

    if (isInterCityRide) {
      // const inputParametes = {
      //   "fromLat": context.params.pickupLat,
      //   "fromLng": context.params.pickupLng,
      //   "toLat": serviceCategoryData.location.coordinates[0][0][1],
      //   "toLng": serviceCategoryData.location.coordinates[0][0][0],
      // };
      // const rad = (x) => {
      //   return (x * Math.PI) / 180;
      // };
      // const getDistanceUsingCoordinates = (parametes) => {
      //   const R = 6378137; // Earth’s mean radius in meter
      //   const dLat = rad(parametes.toLat - parametes.fromLat);
      //   const dLong = rad(parametes.toLng - parametes.fromLng);
      //   const a =
      //     Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      //     Math.cos(rad(parametes.fromLat)) *
      //       Math.cos(rad(parametes.toLat)) *
      //       Math.sin(dLong / 2) *
      //       Math.sin(dLong / 2);
      //   const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      //   const d = R * c;
      //   return parseFloat(d.toFixed(2)); // returns the distance in meter
      // };
      // estimationPickupDistance = getDistanceUsingCoordinates(inputParametes);
      //  const ddd = geolib.findNearest(
      //    {
      //      "latitude": context.params.pickupLat,
      //      "longitude": context.params.pickupLng,
      //    },
      //    [
      //      { "latitude": 52.516272, "longitude": 13.377722 },
      //      { "latitude": 51.515, "longitude": 7.453619 },
      //      { "latitude": 51.503333, "longitude": -0.119722 },
      //      { "latitude": 55.751667, "longitude": 37.617778 },
      //      { "latitude": 48.8583, "longitude": 2.2945 },
      //      { "latitude": 59.3275, "longitude": 18.0675 },
      //      { "latitude": 50.516272, "longitude": 10.377722 },
      //      { "latitude": 59.916911, "longitude": 10.727567 },
      //    ]
      //  );

      // estimationPickupDistance = geolib.getDistance(
      //   {
      //     "latitude": context.params.pickupLat,
      //     "longitude": context.params.pickupLng,
      //   },
      //   {
      //     // "latitude": serviceCategoryData.location.coordinates[0][0][1],
      //     // "longitude": serviceCategoryData.location.coordinates[0][0][0],
      //     "latitude": coordinatesLngLat[1],
      //     "longitude": coordinatesLngLat[0],
      //   }
      // );
      const coordinatesLngLat = geolib.findNearest(
        {
          "latitude": context.params.pickupLat,
          "longitude": context.params.pickupLng,
        },
        [...serviceCategoryData.location.coordinates[0]]
      );
      const mapDataPickup = await googleApiUtil.directions({
        "clientId": clientId,
        "from": context.params.pickupLat + "," + context.params.pickupLng,
        "to": coordinatesLngLat[1] + "," + coordinatesLngLat[0],
      });
      estimationPickupDistance =
        mapDataPickup.data?.routes[0]?.legs[0]?.distance?.value || 0;
      // const centerPoint = geolib.getCenter([
      //   ...serviceCategoryData.location.coordinates[0],
      // ]);
      // estimationPickupDistance = geolib.getDistance(
      //   {
      //     "latitude": context.params.pickupLat,
      //     "longitude": context.params.pickupLng,
      //   },
      //   {
      //     // "latitude": serviceCategoryData.location.coordinates[0][0][1],
      //     // "longitude": serviceCategoryData.location.coordinates[0][0][0],
      //     "latitude": centerPoint.latitude,
      //     "longitude": centerPoint.longitude,
      //   }
      // );
      // const pickupLatLngData = [];
      // serviceCategoryData.location.coordinates[0].map((key) => {
      //   key.map((data) => {
      //     // console.log(Object.fromEntries(key));
      //     console.log(data);
      //     pickupLatLngData.push(data);
      //   });
      // });

      // const res = pickupLatLngData.reduce(
      //   (acc, curr) => ((acc[curr] = ""), acc),
      //   {}
      // );
      // console.log(res);

      estimationPickupTime =
        parseInt(
          ((estimationPickupDistance / distanceTypeUnitValue) % 50).toFixed(2)
        ) * 60; //(time = distance ÷ speed), 50 --> Speed
      if (estimationPickupTime < 60) {
        estimationPickupTime = 60;
      }
      //------- Assign estimationPickupDistance & estimationPickupTime -----------------
      context.params.estimationPickupDistance = estimationPickupDistance;
      context.params.estimationPickupTime = estimationPickupTime;
      //-------- For RoundTrip Calculation Start --------------
      const mapDataDrop = await googleApiUtil.directions({
        "clientId": clientId,
        "from": context.params.dropLat + "," + context.params.dropLng,
        "to": coordinatesLngLat[1] + "," + coordinatesLngLat[0],
      });
      estimationDropDistance =
        mapDataDrop.data?.routes[0]?.legs[0]?.distance?.value || 0;

      estimationDropTime =
        parseInt(
          ((estimationDropDistance / distanceTypeUnitValue) % 50).toFixed(2)
        ) * 60; //(time = distance ÷ speed), 50 --> Speed
      if (estimationDropTime < 60) {
        estimationDropTime = 60;
      }
      //-------- For RoundTrip Calculation End --------------
    }

    //#endregion Intercity Ride pickup Distance Calculation
    //#region professionalList
    let professionalList = await this.broker.emit(
      // "professional.getProfessionalByLocation",
      "booking.getOnlineProfessionals",
      {
        ...context.params,
        "clientId": clientId,
        "lat": context.params.pickupLat,
        "lng": context.params.pickupLng,
        "droplat": context.params.dropLat,
        "droplng": context.params.dropLng,
        "vehicleCategoryId": null,
        "professionalOnlineStatus": true,
        "actionType": constantUtil.CONST_ONLINE,
        "rideType": context.params.rideType,
        "radius": parseInt(
          serviceCategoryData.maxRetryRequestDistance ||
            generalSettings.data.requestDistance
        ),
        // "maxRadius": parseInt(
        //   serviceCategoryData.maxRetryRequestDistance ||
        //     generalSettings.data.requestDistance
        // ),
        "checkOngoingBooking": true,
        "checkLastPriority": true,
        "forTailRide": generalSettings.data.isTailRideNeeded,
        "isSubCategoryAvailable": true,
        "isForceAppliedToProfessional": true,
        "isGenderAvailable": false,
        "childseatAvailable": false,
        "handicapAvailable": false,
        "isRestrictMaleBookingToFemaleProfessional": false,
        "isRentalSupported": false,
        "serviceAreaId": null, // serviceCategoryData?._id?.toString(),
        "isShareRide": context.params.isShareRide ?? null, // null --> Default,
        "paymentOption": context.params?.paymentOption || null, // null --> Default,
        "languageCode": context.params?.languageCode || null, // null --> Default,
        "isPetAllowed": context.params?.isPetAllowed || false,
        "isEnableLuggage": context.params?.isEnableLuggage || false,
      }
    );
    professionalList = (professionalList && professionalList[0]) || [];
    if (professionalList.length > 0) {
      const vehicleCategoryJson = await storageUtil.read(
        constantUtil.VEHICLECATEGORY
      );
      categoryMappingJSON["professionalList"] = professionalList?.map(
        (professional) => {
          const defaultVehicle = professional.vehicles.filter((vehicle) => {
            vehicle = JSON.parse(JSON.stringify(vehicle));
            return (
              vehicle.defaultVehicle === true &&
              vehicle.status === constantUtil.ACTIVE
            );
          });
          // if (defaultVehicle[0]?.vehicleCategoryId) {
          if (
            defaultVehicle[0]?.vehicleCategoryId &&
            parseFloat(professional.wallet.availableAmount) >=
              parseFloat(generalSettings.data?.minimumWalletAmountToOnline)
          ) {
            const vehicalCategoryMetaData =
              vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];
            return {
              "currentBearing": professional.currentBearing,
              "driverId": professional._id,
              "serviceCategory":
                professional?.bookingInfo?.ongoingBooking?.serviceCategory ||
                null,
              "ongoingBooking":
                professional?.bookingInfo?.ongoingBooking?._id || null,
              "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
              "vehicleCategoryId": defaultVehicle[0].vehicleCategoryId,
              "isSubCategoryAvailable":
                defaultVehicle[0].isSubCategoryAvailable,
              "applySubCategory": defaultVehicle[0].applySubCategory,
              "subCategoryIds": defaultVehicle[0].subCategoryIds,
              "isGenderAvailable": defaultVehicle[0].isGenderAvailable,
              "childseatAvailable": defaultVehicle[0].childseatAvailable,
              "handicapAvailable": defaultVehicle[0].handicapAvailable,
              "isPetAllowed": defaultVehicle[0].isPetAllowed,
              "isEnableLuggage": defaultVehicle[0].isEnableLuggage,
              "isHasAC": defaultVehicle[0].isHasAC,
              "isHaveShare": vehicalCategoryMetaData?.mandatoryShare || false,
              "isTailRideBooking": professional?.isTailRideBooking || false,
              "vehicleCategoryMapImage":
                vehicalCategoryMetaData?.categoryMapImage || "",
              "lat": professional.location.coordinates[1],
              "lng": professional.location.coordinates[0],
              "serviceType": vehicalCategoryMetaData?.vehicleCategory || "",
              "fullName":
                professional?.firstName ??
                "" + " " + professional?.lastName ??
                "",
              "avatar": professional?.avatar ?? "",
              "reviewRating": professional?.review?.avgRating ?? 0,
              "paymentMode": professional?.paymentMode || [],
              // ...professional,
            };
          } else {
            return [];
          }
        }
      );
    }
    //#endregion professionalList
    //#region Heatmap Peakfare
    if (generalSettings.data.isEnableHeatmap) {
      let heatmapPeakfareData = await this.broker.emit(
        "heatmap.calculatePeakFareUsingHeatmap",
        {
          "clientId": clientId,
          "lat": context.params.pickupLat,
          "lng": context.params.pickupLng,
          "professionalCount": professionalList.length,
          "serviceAreaId": userCurrentServiceAreaId?.toString(),
        }
      );
      heatmapPeakfareData = heatmapPeakfareData && heatmapPeakfareData[0];
      if (heatmapPeakfareData?.status === constantUtil.SUCCESS) {
        heatmapPeakfare = heatmapPeakfareData.peakFare || 0;
      }
    }
    //#endregion Heatmap Peakfare
    //#region couponList
    if (context.meta.userId) {
      this.broker.emit("user.updateServiceAreaId", {
        "clientId": clientId,
        "userId":
          context.params.userId?.toString() || context.meta.userId?.toString(),
        "serviceAreaId": serviceCategoryData?._id?.toString(),
      });
      let couponList = await this.broker.emit("admin.getUserBasedCouponList", {
        "clientId": clientId,
        "lat": context.params.pickupLat,
        "lng": context.params.pickupLng,
        "userId":
          context.params.userId?.toString() || context.meta.userId?.toString(),
        "serviceAreaId": serviceCategoryData?._id?.toString(),
      });
      couponList = (couponList && couponList[0]) || [];
      if (couponList.length > 0) {
        categoryMappingJSON["couponList"] = couponList;
      }
    }
    //#endregion couponList

    categoryMappingJSON["categories"] =
      serviceCategoryData.categoryData.vehicles.map((vehicle) => {
        let serviceTax = 0,
          travelCharge = 0,
          minimumCharge = 0,
          travelChargeData = {},
          minimumChargeData = {},
          surChargeFee = 0,
          surChargeFeeData = {},
          siteCommision = 0,
          siteCommisionValue = 0,
          professionalCommision = 0,
          distanceFare = 0,
          timeFare = 0,
          peakFareCharge = 0,
          nightFareCharge = 0,
          intercityPickupTimeFare = 0,
          intercityPickupDistanceFare = 0,
          intercityPickupCharge = 0,
          intercityBoundaryFare = 0,
          interCityChargeData = {},
          totalWithoutServiceTax = 0,
          // serviceTaxPercentage = 0,
          intercityPickupFarePerDistance = 0,
          intercityPickupFarePerMinites = 0,
          intercityBoundaryFareType = "",
          isEnableRoundTrip = false,
          RoundTripFare = 0,
          RoundTripServiceTax = 0,
          discountAmount = 0,
          // Assistance Care
          beforeRideAssistanceCareAmount = 0,
          afterRideAssistanceCareAmount = 0,
          assistanceCareAmount = 0,
          isMinimumChargeApplied = false;

        vehicle["distanceTypeUnitValue"] = distanceTypeUnitValue;
        vehicle["estimationDistance"] = context.params.estimationDistance;
        vehicle["estimationTime"] = context.params.estimationTime;
        vehicle["fareSettings"] = null;
        const distanceAndTimeFareData = calculateDistanceAndTimeFare(vehicle);

        const serviceTaxPercentage = parseFloat(
          vehicle?.serviceTaxPercentage || 0
        );
        const baseFare = parseFloat(vehicle.baseFare || 0);
        //#region peakFare & nightFare
        const isPeakFareAvailable =
          serviceCategoryData?.isPeakFareAvailable?.status || false;
        const isNightFareAvailable =
          serviceCategoryData?.isNightFareAvailable?.status || false;
        //
        let isPeakFareTimeApplicable = checkPeakTiming(
          serviceCategoryData.isPeakFareAvailable
        );
        const isNightFareTimeApplicable = checkNightTiming(
          serviceCategoryData.isNightFareAvailable
        );
        //
        let actualPeakFare =
          isPeakFareAvailable === true
            ? checkSpecialDayPeakHours[0]?.data?.specialDays?.peakFare ||
              vehicle.peakFare
            : 0;
        let peakFareText = checkSpecialDayPeakHours?.[0]?.data?.specialDays
          ?.peakFare
          ? checkSpecialDayPeakHours?.[0]?.data?.specialDays?.notes
          : "";
        let nightFare = isNightFareAvailable === true ? vehicle.nightFare : 0;
        //#endregion peakFare & nightFare
        //#region Share Ride
        let sharePercentage = 0, // Default
          sharePercentageUser1 = 0, // Default
          sharePercentageUser2 = 0; // Default
        // shareRideAmountUser1 = 0, // Default
        // shareRideAmountUser2 = 0; // Default
        const passengerCount = context.params?.passengerCount || 1; // Default
        let isShareRide = vehicle?.isSupportShareRide || false;
        //
        if (
          generalSettings.data.isEnableShareRide &&
          isShareRide &&
          isInterCityRide === false
        ) {
          sharePercentageUser1 = parseFloat(vehicle?.sharePercentageUser1 || 0);
          sharePercentageUser2 = parseFloat(vehicle?.sharePercentageUser2 || 0);
          sharePercentage =
            passengerCount === 1
              ? sharePercentageUser1
              : sharePercentageUser1 + sharePercentageUser2;
        } else {
          isShareRide = false;
        }
        //#endregion Share Ride
        //#region Assistance Care
        const isEnableAssistanceCare =
          vehicle?.assistanceCareDetails?.isEnableAssistanceCare || false;
        // Before Ride Assistance Care
        const beforeRideAssistanceCareGracePeriod =
          vehicle?.assistanceCareDetails?.beforeRideAssistanceCareGracePeriod ||
          0;
        const beforeRideAssistanceCareNormalChargePerMin =
          vehicle?.assistanceCareDetails
            ?.beforeRideAssistanceCareNormalChargePerMin || 0;
        const beforeRideAssistanceCareRevisedChargePerMin =
          vehicle?.assistanceCareDetails
            ?.beforeRideAssistanceCareRevisedChargePerMin || 0;
        const beforeRideAssistanceCareNeededMins = parseInt(
          context.params.beforeRideAssistanceCareNeededMins || 0
        );
        const beforeRideAssistanceCareProvidesMins =
          context.params.beforeRideAssistanceCareProvidesMins ||
          beforeRideAssistanceCareNeededMins;
        const newBeforeRideAssistanceCareProvidesMins =
          beforeRideAssistanceCareProvidesMins -
          beforeRideAssistanceCareGracePeriod;
        if (
          isEnableAssistanceCare &&
          newBeforeRideAssistanceCareProvidesMins > 0
        ) {
          beforeRideAssistanceCareAmount =
            newBeforeRideAssistanceCareProvidesMins *
            beforeRideAssistanceCareNormalChargePerMin;
        }
        // After Ride Assistance Care
        const afterRideAssistanceCareGracePeriod =
          vehicle?.assistanceCareDetails?.afterRideAssistanceCareGracePeriod ||
          0;
        const afterRideAssistanceCareNormalChargePerMin =
          vehicle?.assistanceCareDetails
            ?.afterRideAssistanceCareNormalChargePerMin || 0;
        const afterRideAssistanceCareRevisedChargePerMin =
          vehicle?.assistanceCareDetails
            ?.afterRideAssistanceCareRevisedChargePerMin || 0;
        const afterRideAssistanceCareNeededMins = parseInt(
          context.params.afterRideAssistanceCareNeededMins || 0
        );
        const afterRideAssistanceCareProvidesMins =
          context.params.afterRideAssistanceCareProvidesMins ||
          afterRideAssistanceCareNeededMins;
        const newAfterRideAssistanceCareProvidesMins =
          afterRideAssistanceCareProvidesMins -
          afterRideAssistanceCareGracePeriod;
        if (
          isEnableAssistanceCare &&
          newAfterRideAssistanceCareProvidesMins > 0
        ) {
          afterRideAssistanceCareAmount =
            newAfterRideAssistanceCareProvidesMins *
            afterRideAssistanceCareNormalChargePerMin;
        }
        // Assistance Care Total Amount
        assistanceCareAmount =
          beforeRideAssistanceCareAmount + afterRideAssistanceCareAmount;
        //#endregion Assistance Care
        const calculateEstimateAmount = () => {
          // // DISTANCE CALCULATION BY FORMULA
          // const distanceKey = formulaJSON["RIDE"]["DISTANCEFARE"];
          // const distanceValue = {
          //   "FAREPERMETER": vehicle.farePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
          //   "TRAVELPERMETER": context.params.estimationDistance,
          // };
          // distanceFare = formula.run(distanceKey, distanceValue).toFixed(2);

          // // TIME CALCULATION BY FORMULA
          // const timeKey = formulaJSON["RIDE"]["TIMEFARE"];
          // const timeValue = {
          //   "FAREPERSEC": vehicle.farePerMinute / 60,
          //   "TRAVELPERSEC": context.params.estimationTime,
          // };
          // timeFare = formula.run(timeKey, timeValue).toFixed(2);
          // DISTANCE & TIME FARE
          distanceFare = distanceAndTimeFareData.distanceFare;
          timeFare = distanceAndTimeFareData.timeFare;

          //#region Intercity Ride Charge
          if (isInterCityRide) {
            intercityBoundaryFareType =
              vehicle?.intercityBoundaryFareType || "NONE";
            isEnableRoundTrip = vehicle?.isEnableRoundTrip || false;
            // DISTANCE CALCULATION BY FORMULA
            intercityPickupFarePerDistance =
              vehicle?.intercityPickupFarePerDistance || 0;
            const pickupDistanceKey =
              formulaJSON["RIDE"]["INTERCITYPICKUPDISTANCEFARE"];
            const pickupDistanceValue = {
              "PICKUPFAREPERMETER":
                intercityPickupFarePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
              "PICKUPDISTANCEPERMETER":
                context.params?.estimationPickupDistance ||
                distanceTypeUnitValue,
            };
            intercityPickupDistanceFare =
              formula.run(pickupDistanceKey, pickupDistanceValue)?.toFixed(2) ||
              0;

            // TIME CALCULATION BY FORMULA
            intercityPickupFarePerMinites =
              vehicle?.intercityPickupFarePerMinites || 0;
            const pickupTimeKey =
              formulaJSON["RIDE"]["INTERCITYPICKUPTIMEFARE"];
            const pickupTimeValue = {
              "PICKUPFAREPERSEC": intercityPickupFarePerMinites / 60,
              "PICKUPTIMEPERSEC": context.params?.estimationPickupTime || 60,
            };
            intercityPickupTimeFare =
              formula.run(pickupTimeKey, pickupTimeValue)?.toFixed(2) || 0;

            intercityPickupCharge =
              parseFloat(intercityPickupDistanceFare) +
              parseFloat(intercityPickupTimeFare);

            switch (intercityBoundaryFareType?.toUpperCase()) {
              case constantUtil.FLAT:
                intercityBoundaryFare = parseFloat(
                  vehicle?.intercityBoundaryFare?.toFixed(2) || 0
                );
                break;

              case constantUtil.PERCENTAGE:
                intercityBoundaryFare =
                  intercityPickupCharge *
                  parseFloat(vehicle?.intercityBoundaryFare?.toFixed(2) || 0);
                break;
            }

            interCityChargeData = {
              "intercityBoundaryFare": intercityBoundaryFare,
              "intercityPickupCharge": intercityPickupCharge,
            };
          }
          //#endregion Intercity Ride Charge
          //#region For RoundTrip Calculation
          if (isEnableRoundTrip) {
            // DISTANCE CALCULATION BY FORMULA
            intercityPickupFarePerDistance =
              vehicle?.intercityPickupFarePerDistance || 0;
            const dropDistanceKey =
              formulaJSON["RIDE"]["INTERCITYPICKUPDISTANCEFARE"];
            const dropDistanceValue = {
              "PICKUPFAREPERMETER":
                intercityPickupFarePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
              "PICKUPDISTANCEPERMETER":
                estimationDropDistance || distanceTypeUnitValue,
            };
            const intercityDropDistanceFare =
              formula.run(dropDistanceKey, dropDistanceValue)?.toFixed(2) || 0;

            // TIME CALCULATION BY FORMULA
            intercityPickupFarePerMinites =
              vehicle?.intercityPickupFarePerMinites || 0;
            const dropTimeKey = formulaJSON["RIDE"]["INTERCITYPICKUPTIMEFARE"];
            const dropTimeValue = {
              "PICKUPFAREPERSEC": intercityPickupFarePerMinites / 60,
              "PICKUPTIMEPERSEC": estimationDropTime || 60,
            };
            const intercityDropTimeFare =
              formula.run(dropTimeKey, dropTimeValue)?.toFixed(2) || 0;

            RoundTripFare =
              parseFloat(intercityDropDistanceFare) +
              parseFloat(intercityDropTimeFare);

            RoundTripServiceTax = RoundTripFare * (serviceTaxPercentage / 100); //Now Not Used
            // travelCharge =
            //   travelCharge +
            //   intercityPickupCharge +
            //   intercityBoundaryFare +
            //   RoundTripFare;

            // serviceTax = (
            //   parseFloat(serviceTax) + parseFloat(RoundTripServiceTax)
            // ).toFixed(2);
          }
          //#endregion For RoundTrip Calculation

          let finalKey;
          const finalValue = {
            "BASEFARE": baseFare,
            "TOTALDISTANCEFARE": parseFloat(distanceFare),
            "TOTALTIMEFARE": parseFloat(timeFare),
            "PEAKFEE": 0,
            "NIGHTFEE": 0,
          };
          if (
            isPeakFareAvailable && //  serviceCategoryData.isPeakFareAvailable.status &&
            isPeakFareTimeApplicable && //  checkPeakTiming(serviceCategoryData.isPeakFareAvailable) &&
            isNightFareAvailable && // serviceCategoryData.isNightFareAvailable.status &&
            isNightFareTimeApplicable //  checkNightTiming(serviceCategoryData.isNightFareAvailable)
          ) {
            finalKey = formulaJSON["RIDE"]["PEAKANDNIGHTFARE"];
            finalValue["PEAKFEE"] = actualPeakFare + heatmapPeakfare; // vehicle.peakFare;
            finalValue["NIGHTFEE"] = nightFare; // vehicle.nightFare;
          } else if (
            // serviceCategoryData.isNightFareAvailable.status && checkNightTiming(serviceCategoryData.isNightFareAvailable)
            isNightFareAvailable &&
            isNightFareTimeApplicable &&
            generalSettings.data.isEnableHeatmap &&
            heatmapPeakfare > 0
          ) {
            isPeakFareTimeApplicable = true;
            actualPeakFare = 0;
            finalKey = formulaJSON["RIDE"]["PEAKANDNIGHTFARE"];
            finalValue["PEAKFEE"] = heatmapPeakfare; // vehicle.peakFare;
            finalValue["NIGHTFEE"] = nightFare; // vehicle.nightFare;
          } else if (isNightFareAvailable && isNightFareTimeApplicable) {
            finalKey = formulaJSON["RIDE"]["ONLYNIGHTGFARE"];
            finalValue["NIGHTFEE"] = nightFare; // vehicle.nightFare;
          } else if (
            // serviceCategoryData.isPeakFareAvailable.status && checkPeakTiming(serviceCategoryData.isPeakFareAvailable)
            (isPeakFareAvailable && isPeakFareTimeApplicable) ||
            (generalSettings.data.isEnableHeatmap && heatmapPeakfare > 0)
          ) {
            nightFare = 0;
            finalKey = formulaJSON["RIDE"]["ONLYPEAKFARE"];
            if (isPeakFareAvailable && isPeakFareTimeApplicable) {
              finalValue["PEAKFEE"] = actualPeakFare + heatmapPeakfare; // vehicle.peakFare;
            } else {
              isPeakFareTimeApplicable = true;
              actualPeakFare = 0;
              finalValue["PEAKFEE"] = heatmapPeakfare;
            }
          } else {
            nightFare = 0;
            actualPeakFare = 0;
            finalKey = formulaJSON["RIDE"]["BASICFARE"];
          }
          //#region peakFare & nightFare
          if (isPeakFareTimeApplicable) {
            const peakFareKey = formulaJSON["RIDE"]["PEAKFARECHARGE"];
            peakFareCharge = formula.run(peakFareKey, finalValue);
          }
          if (isNightFareTimeApplicable) {
            const nightFareKey = formulaJSON["RIDE"]["NIGHTGFARECHARGE"];
            nightFareCharge = formula.run(nightFareKey, finalValue);
          }
          //#endregion peakFare & nightFare
          let deliveryCharge =
            formula.run(finalKey, finalValue)?.toFixed(2) || 0;
          //------------
          if (
            generalSettings.data.isEnableShareRide &&
            isShareRide &&
            isInterCityRide === false
          ) {
            const shareRidedeliveryCharge = (
              (parseFloat(deliveryCharge) / 100) *
              sharePercentage
            ).toFixed(2);
            discountAmount = (
              parseFloat(deliveryCharge) - parseFloat(shareRidedeliveryCharge)
            ).toFixed(2);
            deliveryCharge = shareRidedeliveryCharge;
          }
          //------------
          totalWithoutServiceTax = deliveryCharge;
          travelCharge =
            parseFloat(
              (
                parseFloat(deliveryCharge) +
                parseFloat(discountAmount) -
                baseFare
              )?.toFixed(2) || 0
            ) +
            parseFloat(intercityPickupCharge) +
            parseFloat(intercityBoundaryFare) +
            parseFloat(RoundTripFare); // +  parseFloat(assistanceCareAmount);

          let valueCheck = false;
          if (
            (vehicle?.minimumCharge?.status || false) === true &&
            deliveryCharge < parseFloat(vehicle.minimumCharge.amount)
          ) {
            minimumCharge = parseFloat(
              vehicle?.minimumCharge?.amount?.toFixed(2) || 0
            );
            valueCheck = true;
            isMinimumChargeApplied = true;
            deliveryCharge = parseFloat(minimumCharge);
            totalWithoutServiceTax = deliveryCharge;

            // minimumChargeData = { "minimumCharge": minimumCharge };
            travelCharge = minimumCharge - baseFare;
            travelChargeData = {
              "baseFare": baseFare,
              // "travelCharge": minimumCharge - vehicle.baseFare,
              "travelCharge": travelCharge,
            };
          } else {
            travelChargeData = {
              "baseFare": baseFare,
              "travelCharge": travelCharge,
            };
          }

          if (vehicle.isSurchargeEnable) {
            surChargeFee = parseFloat(vehicle.surchargeFee);
            surChargeFeeData = { "surchargeFee": surChargeFee };
          }

          serviceTax = (
            (parseFloat(deliveryCharge) +
              parseFloat(intercityBoundaryFare) +
              parseFloat(intercityPickupCharge) +
              parseFloat(RoundTripFare)) *
            (serviceTaxPercentage / 100)
          ).toFixed(2);

          if (userType === constantUtil.PROFESSIONAL) {
            siteCommision = vehicle?.siteCommissionManualMeter || 0; // For Professional Only (Manual Meter)
          } else {
            siteCommision = vehicle?.siteCommission || 0;
          }

          siteCommisionValue = (
            parseFloat(deliveryCharge) *
            (siteCommision / 100)
          ).toFixed(2);

          return (
            vehicle.minimumCharge &&
            vehicle.minimumCharge.status === true &&
            valueCheck
              ? //parseFloat(deliveryCharge) +
                parseFloat(minimumCharge) + parseFloat(serviceTax)
              : parseFloat(deliveryCharge) + parseFloat(serviceTax)
          ).toFixed(2);
        };

        const paymentAmount = parseFloat(calculateEstimateAmount());

        let vehicalCategoryMetaData = vehicleCategoryJson[vehicle.categoryId];
        vehicalCategoryMetaData = { ...vehicalCategoryMetaData, ...vehicle };
        const breakFareDetails = {
          "fareBreakUp": {
            "estimatefare": parseFloat(
              (
                paymentAmount +
                surChargeFee +
                intercityPickupCharge +
                intercityBoundaryFare +
                RoundTripFare +
                assistanceCareAmount
              ).toFixed(2)
            ),
            "message":
              vehicalCategoryMetaData.fareBreakUpDetail &&
              vehicalCategoryMetaData.fareBreakUpDetail.fareMessage
                ? vehicalCategoryMetaData.fareBreakUpDetail.fareMessage
                : "",
            "fareSummary": invoiceSetup({
              // "baseFare": baseFare,
              // "travelCharge": travelCharge,
              ...travelChargeData,
              ...minimumChargeData,
              ...surChargeFeeData, // Check need to Show
              // ...interCityChargeData, //For Only InterCity Ride
              "serviceTax": serviceTax,
              "payableAmount": parseFloat(
                (
                  paymentAmount +
                  surChargeFee +
                  intercityPickupCharge +
                  intercityBoundaryFare +
                  RoundTripFare +
                  assistanceCareAmount
                ).toFixed(2)
              ),
              "currencySymbol": serviceCategoryData.currencySymbol,
              "currencyCode": serviceCategoryData.currencyCode,
            }),
          },
        };
        const fareDataDetails = {
          ...vehicalCategoryMetaData.fareBreakUpDetail,
          ...breakFareDetails,
        };
        let pendingPaymentAmount = 0;
        let immediateReturnResponse = false;
        if (userType === constantUtil.COORPERATEOFFICE) {
          pendingPaymentAmount =
            context.meta?.userDetails?.wallet?.availableAmount < 0
              ? parseFloat(
                  Math.abs(
                    context.meta?.userDetails?.wallet?.availableAmount || 0
                  )
                )
              : 0;
          immediateReturnResponse = true;
        } else if (
          userType === constantUtil.ALL &&
          vehicle.forAdminAndMobile === true
        ) {
          pendingPaymentAmount = !context.meta.adminId
            ? context.meta?.[
                context.meta.userId
                  ? "userDetails"
                  : context.meta.professionalId && "professionalDetails"
              ].wallet?.availableAmount < 0
              ? parseFloat(
                  Math.abs(
                    context.meta?.userDetails?.wallet?.availableAmount || 0
                  )
                )
              : 0
            : false;
          immediateReturnResponse = true;
        } else if (userType === constantUtil.USER) {
          pendingPaymentAmount =
            context.meta?.userDetails?.wallet?.availableAmount < 0
              ? parseFloat(
                  Math.abs(
                    context.meta?.userDetails?.wallet?.availableAmount || 0
                  )
                )
              : 0;
          immediateReturnResponse = true;
        } else if (userType === constantUtil.PROFESSIONAL) {
          pendingPaymentAmount =
            context.meta?.professionalDetails?.wallet?.availableAmount < 0
              ? parseFloat(
                  Math.abs(
                    context.meta?.professionalDetails?.wallet
                      ?.availableAmount || 0
                  )
                )
              : 0;
          immediateReturnResponse = true;
        } else if (userType === constantUtil.GUESTUSER) {
          pendingPaymentAmount = 0;
          immediateReturnResponse = true;
        } else {
          pendingPaymentAmount = !context.meta.adminId
            ? context.meta?.[
                context.meta.userId
                  ? "userDetails"
                  : context.meta.professionalId && "professionalDetails"
              ].wallet?.availableAmount < 0
              ? parseFloat(
                  Math.abs(
                    context.meta?.userDetails?.wallet?.availableAmount || 0
                  )
                )
              : 0
            : false;
          immediateReturnResponse = true;
        }
        if (
          (generalSettings.data.isEnableShareRide === false && isShareRide) ||
          (generalSettings.data.isEnableShareRide &&
            isShareRide &&
            parseInt(context.params.estimationDistance || 0) <=
              parseInt(generalSettings.data.shareRideAllowMinDistance))
        ) {
          immediateReturnResponse = false;
        }
        // //#region professionalCommision
        // professionalCommision =
        //   paymentAmount +
        //   intercityPickupCharge +
        //   intercityBoundaryFare +
        //   RoundTripFare +
        //   assistanceCareAmount -
        //   (siteCommision + serviceTax);
        // //#endregion professionalCommision
        if (immediateReturnResponse) {
          return {
            "recordNo": vehicle?.recordNo || Date.now(),
            "categoryID": vehicle.categoryId,
            "isShowEstimationfareRange":
              vehicle?.isShowEstimationfareRange || false,
            "showEstimationfareRangeAmount":
              vehicle?.showEstimationfareRangeAmount || 0,
            "categoryName": vehicalCategoryMetaData.vehicleCategory,
            // "categoryNameLanguageKeys":
            //   vehicalCategoryMetaData.languageKeys || {},
            // + (isInterCityRide === true ? " - " + rideBookingCategory : ""),
            "categoryImage": vehicalCategoryMetaData.categoryImage,
            "categoryMapImage": vehicalCategoryMetaData?.categoryMapImage || "",
            "isPackageSupport": vehicalCategoryMetaData.isPackageSupport,
            "isScheduleSupport": vehicalCategoryMetaData.isScheduleSupport,
            "seatCount": vehicalCategoryMetaData.seatCount,
            "luggageCount": vehicalCategoryMetaData.luggageCount,
            "cost": parseFloat(
              (
                paymentAmount +
                intercityPickupCharge +
                intercityBoundaryFare +
                RoundTripFare +
                assistanceCareAmount
              ).toFixed(2)
            ),
            "estimationPayableAmount": parseFloat(
              (
                paymentAmount +
                surChargeFee +
                intercityPickupCharge +
                intercityBoundaryFare +
                RoundTripFare +
                assistanceCareAmount
              ).toFixed(2)
            ),
            "totalWithoutServiceTax": parseFloat(totalWithoutServiceTax),
            "serviceTaxPercentage": parseFloat(serviceTaxPercentage),
            "serviceTax": parseFloat(serviceTax),
            "surchargeFee": parseFloat(surChargeFee),
            "travelCharge": parseFloat(travelCharge),
            "minimumCharge": parseFloat(minimumCharge),
            "isMinimumChargeApplied": isMinimumChargeApplied,
            "distanceFare": parseFloat(distanceFare),
            "timeFare": parseFloat(timeFare),
            "pendingPaymentAmount": pendingPaymentAmount,
            "siteCommision": parseFloat(siteCommision),
            "siteCommisionValue": parseFloat(siteCommisionValue),
            // "professionalCommision": parseFloat(professionalCommision),
            "defaultCategory":
              vehicalCategoryMetaData?.isDefaultCategory || false,
            "isHaveShare": vehicalCategoryMetaData?.mandatoryShare || false,
            "fareDetails": fareDataDetails,
            "isSurge": isNightFareTimeApplicable || isPeakFareTimeApplicable,
            // checkNightTiming(serviceCategoryData.isNightFareAvailable) || // if  checkNightTiming send true
            // checkPeakTiming(serviceCategoryData.isPeakFareAvailable), //if checkPeakTiming  send true , else both are false send false
            "surge": {
              "isPeakTiming": isPeakFareTimeApplicable,
              "isNightTiming": isNightFareTimeApplicable,
              // "isPeakTiming": checkPeakTiming(
              //   serviceCategoryData.isPeakFareAvailable
              // ),
              // "isNightTiming": checkNightTiming(
              //   serviceCategoryData.isNightFareAvailable
              // ),
              "peakTimeFare": actualPeakFare + heatmapPeakfare, // vehicle.peakFare,
              "nightTimeFare": nightFare, // vehicle.nightFare,
              "peakFareText": peakFareText,
              "actualPeakFare": actualPeakFare,
              "heatmapPeakFare": heatmapPeakfare,
              "peakFareCharge": parseFloat(peakFareCharge),
              "nightFareCharge": parseFloat(nightFareCharge),
            },
            "estimationPickupDistance":
              context.params?.estimationPickupDistance || 0,
            "estimationPickupTime": context.params?.estimationPickupTime || 0,
            // Intercity Ride
            "isInterCityRide": isInterCityRide,
            "intercityPickupTimeFare": intercityPickupTimeFare,
            "intercityPickupDistanceFare": intercityPickupDistanceFare,
            "intercityBoundaryFare": intercityBoundaryFare,
            "intercityPickupCharge": intercityPickupCharge,
            "intercityPickupFarePerDistance": intercityPickupFarePerDistance,
            "intercityPickupFarePerMinites": intercityPickupFarePerMinites,
            // Share Ride
            "isShareRide": isShareRide,
            "passengerCount": passengerCount,
            "sharePercentageUser1": sharePercentageUser1,
            // "shareRideAmountUser1": shareRideAmountUser1,
            "sharePercentageUser2": sharePercentageUser2,
            // "shareRideAmountUser2": shareRideAmountUser2,
            "discountAmount": discountAmount,
            // Assistance Care
            "isEnableAssistanceCare": isEnableAssistanceCare,
            "beforeRideAssistanceCareNormalChargePerMin":
              beforeRideAssistanceCareNormalChargePerMin,
            "beforeRideAssistanceCareRevisedChargePerMin":
              beforeRideAssistanceCareRevisedChargePerMin,
            "beforeRideAssistanceCareGracePeriod":
              beforeRideAssistanceCareGracePeriod,
            "beforeRideAssistanceCareNeededMins":
              beforeRideAssistanceCareNeededMins,
            "beforeRideAssistanceCareProvidesMins":
              beforeRideAssistanceCareProvidesMins,
            "beforeRideAssistanceCareAmount": beforeRideAssistanceCareAmount,
            //
            "afterRideAssistanceCareNormalChargePerMin":
              afterRideAssistanceCareNormalChargePerMin,
            "afterRideAssistanceCareRevisedChargePerMin":
              afterRideAssistanceCareRevisedChargePerMin,
            "afterRideAssistanceCareGracePeriod":
              afterRideAssistanceCareGracePeriod,
            "afterRideAssistanceCareNeededMins":
              afterRideAssistanceCareNeededMins,
            "afterRideAssistanceCareProvidesMins":
              afterRideAssistanceCareProvidesMins,
            "afterRideAssistanceCareAmount": afterRideAssistanceCareAmount,
            "assistanceCareAmount": assistanceCareAmount,
          };
        }
      });
    categoryMappingJSON["categories"] = await categoryMappingJSON["categories"]
      .filter((each) => each !== undefined)
      .sort((a, b) => {
        return a.recordNo - b.recordNo;
      });
    categoryMappingJSON["professionalList"] = await categoryMappingJSON[
      "professionalList"
    ]?.filter((each) => {
      if (each?.vehicleCategoryId) {
        return each;
      }
    });
    responseData = categoryMappingJSON;
    statusCode = constantUtil.CATEGORYAVAILABLE; //Need to Change
  } catch (e) {
    errorCode = e.code || 200;
    message = e.code ? e.message : "SOMETHING WENT WRONG";
    responseData = {};
    statusCode = constantUtil.SERVICENOTAVAILABLE;
    console.log(
      "getCategoryIssue" + "__" + context.meta.userId + "__" + e.message
    );
  }
  return {
    "code": errorCode,
    "status": statusCode,
    "data": responseData,
    "errorMessage": message,
  };
};
//---------------------
module.exports = categoryAction;
