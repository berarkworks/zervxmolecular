const axios = require("axios");
const forge = require("node-forge");
const md5 = require("md5");
//
const flutterwave = {};

const axiosConfig = async (config) => {
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    return {
      "code": error.response.status,
      "data": {},
      "message": error.response.data.message,
    };
  }
};

// #region V1
//
flutterwave.clientEncryption = (body, sec_key) => {
  function getKey() {
    const keymd5 = md5(sec_key);
    const keymd5last12 = keymd5.substr(-12);

    const seckeyadjusted = sec_key.replace("FLWSECK-", "");
    const seckeyadjustedfirst12 = seckeyadjusted.substr(0, 12);

    return seckeyadjustedfirst12 + keymd5last12;
  }
  //
  body = JSON.stringify(body);
  const cipher = forge.cipher.createCipher(
    "3DES-ECB",
    forge.util.createBuffer(getKey())
  );
  cipher.start({ "iv": "" });
  cipher.update(forge.util.createBuffer(body, "utf-8"));
  cipher.finish();
  const encrypted = cipher.output;
  return forge.util.encode64(encrypted.getBytes());
};

flutterwave.charge = async (data) => {
  const str = JSON.stringify({
    "PBFPubKey": data.payload.PBFPubKey, //=>this is public key
    "client": flutterwave.clientEncryption(data.payload, data.secretkey),
    "alg": "3DES-24",
  });
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/charge",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": str,
  };
  return await axiosConfig(config);
};

flutterwave.refund = async (data) => {
  const dat = {
    "ref": data.ref,
    "seckey": data.seckey,
    "PBFPubKey": data.pubkey,
    "amount": data.amount,
  };
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/gpx/merchant/transactions/refund",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };
  return await axiosConfig(config);
};

flutterwave.validateCharge = async (data) => {
  const dat = JSON.stringify({
    "PBFPubKey": data.PBFPubKey,
    "transaction_reference": data.transaction_reference,
    "otp": data.otp,
  });
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };
  return await axiosConfig(config);
};

flutterwave.chargeVerify = async (data) => {
  const dat = JSON.stringify({
    "txref": data.txref,
    "SECKEY": data.SECKEY,
  });
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };
  return await axiosConfig(config);
};

flutterwave.tokenizedCharge = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data),
  };
  return await axiosConfig(config);
};

flutterwave.refundVerify = async (data) => {
  const config = {
    "method": "get",
    "url": `https://api.ravepay.co/v2/refunds/${data.id}?seckey=${data.seckey}`,
    "headers": {},
  };
  return await axiosConfig(config);
};

flutterwave.createTransfer = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/v2/gpx/transfers/create",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data),
  };
  return await axiosConfig(config);
};

flutterwave.bankAccountVerify = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.flutterwave.com/v3/accounts/resolve",
    "headers": {
      "Authorization": `Bearer ${data.secretKey}`,
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data.data),
  };
  return await axiosConfig(config);
};
// #endregion V1

// #region V3
//
flutterwave.clientEncryptionV3 = (body, sec_key) => {
  function getKey() {
    const keymd5 = md5(sec_key);
    const keymd5last12 = keymd5.substr(-12);

    const seckeyadjusted = sec_key.replace("FLWSECK-", "");
    const seckeyadjustedfirst12 = seckeyadjusted.substr(0, 12);

    return seckeyadjustedfirst12 + keymd5last12;
  }
  //
  body = JSON.stringify(body);
  const cipher = forge.cipher.createCipher(
    "3DES-ECB",
    forge.util.createBuffer(getKey())
  );
  cipher.start({ "iv": "" });
  cipher.update(forge.util.createBuffer(body, "utf-8"));
  cipher.finish();
  const encrypted = cipher.output;
  return forge.util.encode64(encrypted.getBytes());
};

flutterwave.chargeV3 = async (data) => {
  const str = JSON.stringify({
    "PBFPubKey": data.payload.PBFPubKey, //=>this is public key
    "client": flutterwave.clientEncryptionV3(data.payload, data.secretkey),
    "alg": "3DES-24",
  });

  const config = {
    "method": "post",
    // "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/charge",
    "url": "https://api.flutterwave.com/v3/charges?type=card",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": str,
  };
  return await axiosConfig(config);
};

flutterwave.refundV3 = async (data) => {
  const dat = {
    "ref": data.ref,
    "seckey": data.seckey,
    "PBFPubKey": data.pubkey,
    "amount": data.amount,
  };
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/gpx/merchant/transactions/refund",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };
  return await axiosConfig(config);
};

flutterwave.validateChargeV3 = async (data) => {
  const dat = JSON.stringify({
    "PBFPubKey": data.PBFPubKey,
    "transaction_reference": data.transaction_reference,
    "otp": data.otp,
  });

  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/validatecharge",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };

  return await axiosConfig(config);
};

flutterwave.chargeVerifyV3 = async (data) => {
  const dat = JSON.stringify({
    "txref": data.txref,
    "SECKEY": data.SECKEY,
  });

  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": dat,
  };

  return await axiosConfig(config);
};

flutterwave.tokenizedChargeV3 = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/flwv3-pug/getpaidx/api/tokenized/charge",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data),
  };
  return await axiosConfig(config);
};

flutterwave.refundVerifyV3 = async (data) => {
  const config = {
    "method": "get",
    "url": `https://api.ravepay.co/v2/refunds/${data.id}?seckey=${data.seckey}`,
    "headers": {},
  };

  return await axiosConfig(config);
};

flutterwave.createTransferV3 = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.ravepay.co/v2/gpx/transfers/create",
    "headers": {
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data),
  };
  return await axiosConfig(config);
};

flutterwave.bankAccountVerifyV3 = async (data) => {
  const config = {
    "method": "post",
    "url": "https://api.flutterwave.com/v3/accounts/resolve",
    "headers": {
      "Authorization": `Bearer ${data.secretKey}`,
      "Content-Type": "application/json",
    },
    "data": JSON.stringify(data.data),
  };
  return await axiosConfig(config);
};
// #endregion V3
//-----------------------------------

module.exports = flutterwave;
