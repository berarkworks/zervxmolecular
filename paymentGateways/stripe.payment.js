// if (paymentData.gateWay === 'STRIPE') {
//     let tokenJson = {}
//     let customerJson = {}
//     const responseData = {}
//     const keyData = {}
//     if (paymentData['mode'] === constantUtil.SANDBOX) {
//       keyData['secretKey'] = paymentData['testSecretKey']
//       keyData['publishableKey'] = paymentData['testPublishableKey']
//     } else {
//       keyData['secretKey'] = paymentData['secretKey']
//       keyData['publishableKey'] = paymentData['publishableKey']
//     }
//     tokenJson = {
//       ...keyData,
//       'data': {},
//     }
//     tokenJson['data']['card'] = {}
//     tokenJson['data']['card']['number'] = context.params.card.cardNumber
//     tokenJson['data']['card']['exp_month'] = context.params.card.expiryMonth
//     tokenJson['data']['card']['exp_year'] = context.params.card.expiryYear
//     tokenJson['data']['card']['cvc'] = context.params.card.cvv
//     finalPaymentData = await stripePaymentUtils.createTokenization(tokenJson)
//     if (finalPaymentData) {
//       responseData['cardToken'] = finalPaymentData.id
//       if (
//         !context.params.stripeCustomerId &&
//         context.params.stripeCustomerId == null
//       ) {
//         customerJson = {
//           ...keyData,
//           'data': {},
//         }
//         customerJson['data']['name'] = context.params.card.holderName
//         customerJson['data']['address'] = {}
//         customerJson['data']['address']['city'] = context.params.city
//           ? context.params.city
//           : 'chennai'
//         customerJson['data']['address']['line1'] = context.params.line1
//           ? context.params.line1
//           : '3k, vantage plaza'
//         customerJson['data']['address']['line2'] = context.params.line2
//           ? context.params.line2
//           : 'adayar'
//         customerJson['data']['address']['postal_code'] = context.params.zipCode
//           ? context.params.zipCode
//           : '600041'
//         customerJson['data']['address']['state'] = context.params.state
//           ? context.params.state
//           : 'Tamil Nadu'
//         customerJson['data']['address']['country'] = context.params.country
//           ? context.params.country
//           : 'IN'
//         customerJson['data']['description'] = context.params.description
//           ? context.params.description
//           : 'Create New Customer'
//         customerJson['data']['source'] = finalPaymentData.id
//         finalCustomerData = await stripePaymentUtils.createCustomer(
//           customerJson
//         )
//         if (finalCustomerData) {
//           responseData['customerId'] = finalCustomerData.id
//           return { 'code': 1, 'data': responseData, 'message': 'Valid Card' }
//         }
//       } else {
//         responseData['customerId'] = context.params.stripeCustomerId
//         return { 'code': 1, 'data': responseData, 'message': 'Valid Card' }
//       }
//     } else {
//       return { 'code': 0, 'data': {}, 'message': 'invalid Card' }
//     }
//   }
