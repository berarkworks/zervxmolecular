const mongoose = require("mongoose");

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");

const bookingMethods = {};
bookingMethods.getRentalLocationCategories = async function (params) {
  const serviceCategoryData = await this.adapter.model
    .findOne({
      //"clientId": mongoose.Types.ObjectId(params.clientId),
      "categoryName": "ride",
      "status": constantUtil.ACTIVE,
      "location": {
        "$geoIntersects": {
          "$geometry": {
            "type": "Point",
            "coordinates": [params.pickupLng, params.pickupLat],
          },
        },
      },
    })
    .lean();
  if (!serviceCategoryData) {
    return {
      "code": 204,
      "status": constantUtil.PICKUPLOCATIONOUT,
      "data": [],
    };
  }
  if (!serviceCategoryData?.rental?.isActive) {
    return {
      "code": 204,
      "status": "CATEGORY DOES NOT SUPPORT RENTALS",
      "data": [],
    };
  }
  const vehicleCategoriesDocs = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  const packages = [];

  for (
    let index = serviceCategoryData?.rental.timeRange.from;
    index <= serviceCategoryData?.rental.timeRange.to;
    index++
  ) {
    const data = {
      "name": `${index} ${serviceCategoryData?.rental.timeRange.type}`,
      "distance":
        index * serviceCategoryData?.rental.distanceRange.distanceForATimeRange,
      "duration": index,
      "distanceType": serviceCategoryData?.rental.distanceRange.type,
      "durationType": serviceCategoryData?.rental.timeRange.type,
      "exceedDurationFare": 0,
      "exceedDistanceFare": 0,
      "categories": [],
    };

    serviceCategoryData.categoryData.vehicles.forEach((eachVehicles, i) => {
      // add some data
      data.exceedDurationFare = eachVehicles.rental.exceed.timeFare;
      data.exceedDistanceFare = eachVehicles.rental.exceed.distanceFare;
      //
      if (eachVehicles.rental.isActive) {
        eachVehicles = {
          ...vehicleCategoriesDocs[eachVehicles.categoryId],
          ...eachVehicles,
        };

        const timeFare = index * eachVehicles.rental.estimate.farePerTime;

        const distanceFare =
          serviceCategoryData.rental.distanceRange.distanceForATimeRange *
          eachVehicles.rental.estimate.farePerDistance;

        const travelCharge = timeFare + distanceFare + eachVehicles.baseFare;
        const serviceTax =
          (eachVehicles.serviceTaxPercentage / travelCharge) * 100;
        const cost =
          serviceTax +
          travelCharge +
          (eachVehicles.isSurchargeEnable ? eachVehicles.surchargeFee : 0);

        data.categories.push({
          "categoryID": eachVehicles.categoryId,
          "categoryName": eachVehicles.vehicleCategory,
          "categoryImage": eachVehicles.categoryImage,
          "categoryMapImage": eachVehicles.categoryMapImage,
          "seatCount": eachVehicles.seatCount,
          "isPackageSupport": eachVehicles.isPackageSupport,
          "isScheduleSupport": eachVehicles.isScheduleSupport,
          "defaultCategory": eachVehicles.defaultCategory,
          "isHaveShare": eachVehicles?.mandatoryShare || false,
          "isSurge": false,
          "surge": {
            "isPeakTiming": false,
            "isNightTiming": false,
            "peakTimeFare": 0,
            "nightTimeFare": 0,
          },
          // for reference fares
          "siteCommisionValue":
            (eachVehicles.siteCommission / travelCharge) * 100,
          "serviceTaxPercentage": eachVehicles.serviceTaxPercentage,
          "pendingPaymentAmount":
            params.user?.userDetails.wallet.availableAmount < 0
              ? params.user?.userDetails.wallet.availableAmount
              : 0,
          // fare data
          timeFare,
          distanceFare,
          travelCharge,
          serviceTax,
          cost,
          "totalWithoutServiceTax": cost - serviceTax,
          "fareDetails": {
            "estimateFare": cost,
            "message": "",
            "fareSummary": [
              {
                "title": "Time Fare",
                "key": "timeFare",
                "value": timeFare,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": false,
                "orderBy": 1,
              },
              {
                "title": "Distance Fare",
                "key": "distanceFare",
                "value": distanceFare,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": false,
                "orderBy": 2,
              },
              {
                "title": "Base Fare",
                "key": "Base Fare",
                "value": eachVehicles.baseFare,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": false,
                "orderBy": 3,
              },
              {
                "title": "Service Tax",
                "key": "serviceTax",
                "value":
                  (eachVehicles.serviceTaxPercentage / travelCharge) * 100,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": false,
                "orderBy": 4,
              },
              {
                "title": eachVehicles.isSurchargeEnable
                  ? eachVehicles.surchargeTitle
                  : "Surcharge",
                "key": "surcharge",
                "value": eachVehicles.isSurchargeEnable
                  ? eachVehicles.surchargeFee
                  : 0,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": false,
                "orderBy": 5,
              },
              {
                "title": "Total Amount",
                "key": "cost",
                "value": cost,
                "currencyCode": serviceCategoryData.currencyCode,
                "currencySymbol": serviceCategoryData.currencySymbol,
                "isBold": true,
                "orderBy": 6,
              },
            ],
          },
        });
      }
    });
    packages.push(data);
  }
  return {
    "code": 200,
    "status": constantUtil.CATEGORYAVAILABLE,
    "data": {
      "serviceAreaID": serviceCategoryData._id,
      "serviceAreaName": serviceCategoryData.locationName,
      "currencySymbol": serviceCategoryData.currencySymbol,
      "currencyCode": serviceCategoryData.currencyCode,
      "distanceType": serviceCategoryData?.rental.distanceRange.type,
      "durationType": serviceCategoryData?.rental.timeRange.type,
      packages,
    },
  };
};
bookingMethods.getNearByOneServiceLocation = async function (inputParams) {
  return await this.adapter.model
    .findOne({
      // .find({
      //"clientId": mongoose.Types.ObjectId(inputParams.clientId),
      "categoryName": inputParams.serviceCategory,
      "status": constantUtil.ACTIVE,
      "location": {
        "$near": {
          "$geometry": {
            "type": "Point",
            "coordinates": [inputParams.pickupLng, inputParams.pickupLat],
          },
          // "$minDistance": 0,
          // "$maxDistance": 100000,
        },
      },
    })
    .lean();
};
//-------------------------
module.exports = bookingMethods;
