const Mongoose = require("mongoose");
const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { MoleculerError } = require("moleculer").Errors;

const bookingMethod = {};

bookingMethod.getProfessionalByLocation = async function (
  //clientId,
  lat,
  lng,
  droplat,
  droplng,
  vehicleCategoryId,
  actionType = constantUtil.CONST_ONLINE,
  rideType = constantUtil.CONST_RIDE,
  radius,
  // maxRadius,
  checkOngoingBooking,
  checkLastPriority,
  forTailRide,
  isSubCategoryAvailable,
  isForceAppliedToProfessional,
  isGenderAvailable,
  childseatAvailable,
  handicapAvailable,
  isRestrictMaleBookingToFemaleProfessional,
  isRentalSupported = false,
  serviceAreaId = null,
  isShareRide = null,
  paymentOption = null, // null --> Default
  languageCode = null, // null --> Default
  isPetAllowed = false,
  isEnableLuggage = false
) {
  return this.broker.emit("professional.getProfessionalByLocation", {
    //"clientId": clientId,
    "lat": lat,
    "lng": lng,
    "droplat": droplat,
    "droplng": droplng,
    "vehicleCategoryId": vehicleCategoryId,
    "professionalOnlineStatus": true,
    "actionType": actionType,
    "rideType": rideType,
    "radius": radius,
    // "maxRadius": maxRadius,
    "checkOngoingBooking": checkOngoingBooking,
    "checkLastPriority": checkLastPriority,
    "forTailRide": forTailRide,
    "isSubCategoryAvailable": isSubCategoryAvailable,
    "isForceAppliedToProfessional": isForceAppliedToProfessional,
    "isGenderAvailable": isGenderAvailable,
    "childseatAvailable": childseatAvailable,
    "handicapAvailable": handicapAvailable,
    "isRestrictMaleBookingToFemaleProfessional":
      !!isRestrictMaleBookingToFemaleProfessional,
    "isRentalSupported": isRentalSupported,
    "serviceAreaId": serviceAreaId,
    "isShareRide": isShareRide,
    "paymentOption": paymentOption,
    "languageCode": languageCode,
    "isPetAllowed": isPetAllowed,
    "isEnableLuggage": isEnableLuggage,
  });
};

bookingMethod.checkAndRemoveProfessionalAndUserDenyAndBlockList =
  async function (professionalData, bookingData, userData) {
    const finalProfessionals = [];
    if (
      !professionalData ||
      (professionalData && professionalData.length === 0)
    ) {
      return finalProfessionals; //"NO PROFESSIONAL FOUND";
    }
    // if (!bookingData) return "NO PROFESSIONAL FOUND";
    professionalData.map((professional) => {
      // if (
      //   bookingData.denyDriverList.includes(professional?._id?.toString()) !==
      //   true
      // ) {
      //   finalProfessionals.push(professional);
      // }
      // if (
      //   (bookingData?.denyDriverList
      //     ?.toString()
      //     ?.includes(professional?._id?.toString()) || false) === false &&
      //   (professional?.blockedUserList
      //     ?.toString()
      //     ?.includes(userData?._id?.toString()) || false) === false &&
      //   (userData?.blockedProfessionalList
      //     // (bookingData?.user?.blockedProfessionalList // need to Change
      //     ?.toString()
      //     ?.includes(professional?._id?.toString()) || false) === false
      // ) {
      //   finalProfessionals.push(professional);
      // }
      if (
        (bookingData?.denyDriverList
          ?.toString()
          ?.includes(professional?._id?.toString()) || false) === false &&
        (professional?.blockedUserList
          ?.toString()
          ?.includes(userData?._id?.toString()) || false) === false &&
        (userData?.blockedProfessionalList
          // (bookingData?.user?.blockedProfessionalList // need to Change
          ?.toString()
          ?.includes(professional?._id?.toString()) || false) === false
      ) {
        finalProfessionals.push(professional);
      }
    });
    if (!bookingData) {
      return finalProfessionals;
    } else {
      const generalSettings = await storageUtil.read(
        constantUtil.GENERALSETTING
      );
      if (
        generalSettings.data.bookingAlgorithm === constantUtil.NEAREST ||
        generalSettings.data.bookingAlgorithm === constantUtil.SHORTEST
      ) {
        const nearestProfessionals = [];
        finalProfessionals.filter((professional) => {
          const alredySendrequestProfessional =
            bookingData?.requestSendProfessionals?.find(
              (data) =>
                data?.professionalId?.toString() ===
                  professional?._id?.toString() &&
                data?.count >=
                  (generalSettings?.data?.nearestAlgorithm
                    ?.bookingRequestCountForProfessional || 2)
            );
          if (!alredySendrequestProfessional) {
            nearestProfessionals.push(professional);
          }
          // if (
          //   alredySendrequestProfessional?.count <
          //   (generalSettings?.data?.nearestAlgorithm
          //     ?.bookingRequestCountForProfessional || 2)
          // ) {
          //   nearestProfessionals.push(professional);
          // }
        });

        // if (!nearestProfessionals[0]) {
        //   if (
        //     generalSettings?.data?.nearestAlgorithm?.repeatProfessioalRequest ===
        //     true
        //   ) {
        //     // remove all driervers from requestSendProfessionals
        //     this.adapter.model.updateOne(
        //       {
        //         "_id": Mongoose.Types.ObjectId(bookingData._id.toString()),
        //       },
        //       {
        //         //need to empty arrays so just assign object
        //         "requestSendProfessionals": {
        //           "professionalId": finalProfessionals[0]?._id?.toString(),
        //           "count": 1,
        //         },
        //       }
        //     );
        //     // then send request to professionls from starting
        //     return finalProfessionals[0] ? [finalProfessionals[0]] : [];
        //   } else {
        //     // just return empty
        //     return [];
        //   }
        // }
        const sendRequestRetryCount = bookingData.requestRetryCount;
        const configBookingRetryCount =
          generalSettings?.data?.bookingRetryCount || 2;
        if (generalSettings?.data?.nearestAlgorithm?.repeatProfessioalRequest) {
          if (!nearestProfessionals[0]) {
            if (sendRequestRetryCount < configBookingRetryCount) {
              nearestProfessionals.push(finalProfessionals[0]); //  Assign Nearest Online Professional for Retry Notification
              const initialValrequestSendProfessionals = {
                "professionalId": nearestProfessionals[0]?._id?.toString(),
                "requestReceiveDateTime": new Date().toISOString(),
                "count": 0,
                "location.coordinates":
                  nearestProfessionals[0]?.location?.coordinates,
                "duration": nearestProfessionals[0]?.duration,
                "distance": nearestProfessionals[0]?.distance,
              };
              // bookingData.requestSendProfessionals =
              //   initialValrequestSendProfessionals;
              bookingData = await this.adapter.model.findOneAndUpdate(
                {
                  "_id": Mongoose.Types.ObjectId(bookingData._id.toString()),
                },
                {
                  //need to empty arrays so just assign object
                  "requestSendProfessionals":
                    initialValrequestSendProfessionals,
                  "requestRetryCount": bookingData.requestRetryCount + 1,
                },
                { "new": true }
              );
            }
          }
        }
        let query;
        const previousProfessional = bookingData.requestSendProfessionals.find(
          (professional) =>
            nearestProfessionals[0]?._id?.toString() ===
            professional?.professionalId?.toString()
        );
        if (previousProfessional) {
          query = [
            {
              "_id": Mongoose.Types.ObjectId(bookingData._id.toString()),
              "requestSendProfessionals.professionalId":
                previousProfessional?.professionalId?.toString(),
            },
            {
              "requestSendProfessionals.$.count":
                previousProfessional.count + 1,
              "requestSendProfessionals.$.requestReceiveDateTime":
                new Date().toISOString(),
            },
          ];
        } else {
          query = [
            {
              "_id": Mongoose.Types.ObjectId(bookingData._id.toString()),
            },
            {
              "$push": {
                "requestSendProfessionals": {
                  "professionalId": nearestProfessionals[0]?._id?.toString(),
                  "requestReceiveDateTime": new Date().toISOString(),
                  "count": 1,
                  "location.coordinates":
                    nearestProfessionals[0]?.location?.coordinates,
                  "duration": nearestProfessionals[0]?.duration,
                  "distance": nearestProfessionals[0]?.distance,
                },
              },
            },
          ];
        }

        const data = await this.adapter.model.updateOne(query[0], query[1]);
        if (!data.nModified) {
          throw new MoleculerError("cant update count");
        }
        return nearestProfessionals[0] ? [nearestProfessionals[0]] : [];
      }

      return finalProfessionals;
    }
  };

bookingMethod.findCurrentShareRideProfessional = async function (
  action,
  professionalData,
  currentShareRideProfessionalId
) {
  const finalProfessionals = [];
  if (!professionalData || (professionalData && professionalData.length === 0))
    return finalProfessionals; //"NO PROFESSIONAL FOUND";

  professionalData.map((professional) => {
    switch (action) {
      case constantUtil.ADD: // Search Ongoing Share Ride Professional
        if (
          professional?._id?.toString() ===
          currentShareRideProfessionalId?.toString()
        ) {
          finalProfessionals.push(professional);
        }
        break;

      case constantUtil.NEXT: // Search next Ongoing Share Ride Professional
        if (
          professional?._id?.toString() !==
            currentShareRideProfessionalId?.toString() &&
          (professional?.bookingInfo?.ongoingBooking?.isShareRide ?? false) ===
            true
        ) {
          finalProfessionals.push(professional);
        }
        break;

      case constantUtil.REMOVE: // Search new Share Ride Professional (Exclude Parent & Child Share Ride)
        if (
          professional?._id?.toString() !==
            currentShareRideProfessionalId?.toString() &&
          (professional?.bookingInfo?.ongoingBooking?.isShareRide ?? false) ===
            false
        ) {
          finalProfessionals.push(professional);
        }
        break;
    }
  });
  return finalProfessionals;
};

bookingMethod.getBookingDetailsById = async function (bookingData) {
  return await this.adapter.model
    .findById(Mongoose.Types.ObjectId(bookingData.bookingId.toString()))
    .populate("admin", { "data.accessToken": 0, "data.password": 0 })
    .populate("user", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("professional", {
      "password": 0,
      "bankDetails": 0,
      "cards": 0,
      "trustedContacts": 0,
    })
    .populate("category")
    .populate("security")
    .populate("officer", { "password": 0, "preferences": 0 })
    .populate("coorperate")
    .populate("couponId")
    .lean();
};

module.exports = bookingMethod;
