const twilio = require("twilio");
const AWS = require("aws-sdk");
// const fcmNotify = require("fcm-node");
// const { google } = require("googleapis");
// let fcmAccessToken = {};

// const fcmNotify = new FCM(serverKey);

const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { lzStringEncode, lzStringDecode } = require("../utils/crypto.util");
const adminMethod = {};

adminMethod.adminServiceInit = async function () {
  const json = {};
  const settingsJson = await this.adapter.model.find({}).lean();

  settingsJson.forEach((setting) => {
    if (!json[setting.name]) json[setting.name] = [];
    json[setting.name].push(setting);
  });
  // GENERAL
  storageUtil.write(
    constantUtil.GENERALSETTING,
    json[constantUtil.GENERALSETTING]
      ? json[constantUtil.GENERALSETTING][0]
      : {}
  );

  //#region MAP
  // const decodedData = JSON.parse(
  //   lzStringDecode(json[constantUtil.MAPSETTING][0]?.data?.encodedData)
  // );
  // const mapRawData = {};
  // mapRawData["id"] = decodedData.id;
  // mapRawData["name"] = constantUtil.MAPSETTING;
  // mapRawData["data"] = decodedData;
  // storageUtil.write(
  //   constantUtil.MAPSETTING,
  //   json[constantUtil.MAPSETTING] ? mapRawData : {}
  // );
  storageUtil.write(
    constantUtil.MAPSETTING,
    json[constantUtil.MAPSETTING] ? json[constantUtil.MAPSETTING][0] : {}
  );
  //#endregion MAP

  // PACKAGESETTING
  storageUtil.write(
    constantUtil.PACKAGESETTING,
    json[constantUtil.PACKAGESETTING]
      ? json[constantUtil.PACKAGESETTING][0]
      : {}
  );

  //BLOCKPROFESSIONALSETUP
  storageUtil.write(
    constantUtil.CONST_BLOCKPROFESSIONALSETUP,
    json[constantUtil.CONST_BLOCKPROFESSIONALSETUP]
      ? json[constantUtil.CONST_BLOCKPROFESSIONALSETUP][0]
      : {}
  );

  // AIRPORTTRANSFERSETTING
  storageUtil.write(
    constantUtil.CONST_AIRPORTTRANSFERSETTING,
    json[constantUtil.CONST_AIRPORTTRANSFERSETTING] || []
  );

  // SMS OTP
  storageUtil.write(
    constantUtil.SMSSETTING,
    json[constantUtil.SMSSETTING] ? json[constantUtil.SMSSETTING][0] : {}
  );
  // SMS NOTIFICATION
  storageUtil.write(
    constantUtil.NOTIFYSMSSETTING,
    json[constantUtil.NOTIFYSMSSETTING]
      ? json[constantUtil.NOTIFYSMSSETTING][0]
      : {}
  );
  // PROMOTION NOTIFICATION SETTING
  storageUtil.write(
    constantUtil.PROMOTIONSMSSETTING,
    json[constantUtil.PROMOTIONSMSSETTING]
      ? json[constantUtil.PROMOTIONSMSSETTING][0]
      : {}
  );
  // PROMOTION EMERGENCY SETTING
  storageUtil.write(
    constantUtil.EMERGENCYSMSSETTING,
    json[constantUtil.EMERGENCYSMSSETTING]
      ? json[constantUtil.EMERGENCYSMSSETTING][0]
      : {}
  );
  // SMS NOTIFICATION TEMPLATE
  storageUtil.write(
    constantUtil.NOTIFYSMSTEMPLATE,
    json[constantUtil.NOTIFYSMSTEMPLATE]
      ? json[constantUtil.NOTIFYSMSTEMPLATE]
      : []
  );
  // CALLCENTER
  storageUtil.write(
    constantUtil.CALLCENTER,
    json[constantUtil.CALLCENTER] ? json[constantUtil.CALLCENTER] : []
  );
  // HEATMAP
  storageUtil.write(
    constantUtil.HEATMAP,
    json[constantUtil.HEATMAP] ? json[constantUtil.HEATMAP] : []
  );
  // NOTIFY WHATSUPSETTING
  storageUtil.write(
    constantUtil.WHATSUPSETTING,
    json[constantUtil.WHATSUPSETTING]
      ? json[constantUtil.WHATSUPSETTING][0]
      : {}
  );
  // CALL
  storageUtil.write(
    constantUtil.CALLSETTING,
    json[constantUtil.CALLSETTING] ? json[constantUtil.CALLSETTING][0] : {}
  );
  // SMTP
  storageUtil.write(
    constantUtil.SMTPSETTING,
    json[constantUtil.SMTPSETTING] ? json[constantUtil.SMTPSETTING][0] : {}
  );
  // FCM
  storageUtil.write(
    constantUtil.FIREBASESETTING,
    json[constantUtil.FIREBASESETTING]
      ? json[constantUtil.FIREBASESETTING][0]
      : {}
  );
  // INVITE AND EARN
  storageUtil.write(
    constantUtil.INVITEANDEARN,
    json[constantUtil.INVITEANDEARN] ? json[constantUtil.INVITEANDEARN][0] : {}
  );
  // SPECIALDAYS
  try {
    storageUtil.write(
      constantUtil.CONST_SPECIALDAYS,
      json[constantUtil.CONST_SPECIALDAYS] || []
    );
  } catch (e) {
    console.log(e);
  }
  // GENDER
  try {
    storageUtil.write(
      constantUtil.CONST_GENDER,
      json[constantUtil.CONST_GENDER]?.filter((item) => item.isActive) || []
    );
  } catch (e) {
    console.log(e);
  }
  // VEHICLE CATEGORY JSON
  const vehicleObjectFn = () => {
    const vehicleCategoryObject = {};
    json[constantUtil.VEHICLECATEGORY]?.forEach((vehicle) => {
      vehicleCategoryObject[vehicle._id] = {
        "_id": vehicle._id,
        "id": vehicle._id,
        "name": constantUtil.VEHICLECATEGORY,
        "data": {
          "status": vehicle.data.status,
          "clientId": vehicle.data.clientId,
        },
        "recordNo": vehicle?.recordNo || 1,
        ...vehicle.data,
        "createdAt": vehicle.createdAt,
        "updatedAt": vehicle.updatedAt,
      };
    });
    return vehicleCategoryObject;
  };
  storageUtil.write(
    constantUtil.VEHICLECATEGORY,
    json[constantUtil.VEHICLECATEGORY] ? vehicleObjectFn() : {}
  );
  // PAYMENT GATEWAY JSON
  const paymentObjectFn = () => {
    const paymentsObject = {};
    json[constantUtil.PAYMENTGATEWAY].forEach((payments) => {
      paymentsObject[payments._id] = {
        "_id": payments._id,
        "id": payments._id,
        "type": constantUtil.PAYMENTGATEWAY,
        "data": {
          "status": payments.data.status,
          "clientId": payments.data.clientId,
        },
        ...payments.data,
        "createdAt": payments.createdAt,
        "updatedAt": payments.updatedAt,
      };
    });
    return paymentsObject;
  };
  storageUtil.write(
    constantUtil.PAYMENTGATEWAY,
    json[constantUtil.PAYMENTGATEWAY] ? paymentObjectFn() : {}
  );

  /* CANCELLATION REASON */
  const cancellationReasonObjectFn = () => {
    const cancellationReasonObject = {};
    json[constantUtil.CANCELLATIONREASON].forEach((reason) => {
      cancellationReasonObject[reason._id] = {
        "_id": reason._id,
        "id": reason._id,
        "name": constantUtil.CANCELLATIONREASON,
        "type": constantUtil.CANCELLATIONREASON,
        "data": {
          "status": reason.data.status,
          "clientId": reason.data.clientId,
        },
        ...reason.data,
        "createdAt": reason.createdAt,
        "updatedAt": reason.updatedAt,
      };
    });
    return cancellationReasonObject;
  };
  storageUtil.write(
    constantUtil.CANCELLATIONREASON,
    json[constantUtil.CANCELLATIONREASON] ? cancellationReasonObjectFn() : {}
  );
  // DOCUMENTS
  const documentsObjectFn = (data) => {
    const documentsObject = {};
    json[constantUtil.DOCUMENTS].forEach((documents) => {
      if (documents.data.docsFor === data) {
        documentsObject[documents._id] = {
          "id": documents._id,
          "_id": documents._id,
          "name": documents.data.docsFor,
          ...documents.data,
          "data": {
            "status": documents.data.status,
            "clientId": documents.data.clientId,
          },
        };
      }
    });
    return documentsObject;
  };
  storageUtil.write(
    constantUtil.PROFILEDOCUMENTS,
    json[constantUtil.DOCUMENTS]
      ? documentsObjectFn(constantUtil.PROFILEDOCUMENTS)
      : {}
  );
  storageUtil.write(
    constantUtil.DRIVERDOCUMENTS,
    json[constantUtil.DOCUMENTS]
      ? documentsObjectFn(constantUtil.DRIVERDOCUMENTS)
      : {}
  );
  // MEMBERSHIP PLANS
  const professionalMembership = {};
  const userMembership = {};
  const membershipPlansJson = () => {
    json[constantUtil.MEMBERSHIPPLAN]?.forEach((each) => {
      if (each.data.userType === constantUtil.PROFESSIONAL) {
        professionalMembership[each._id] = {
          "id": each._id,
          "_id": each._id,
          ...each.data,
          "name": constantUtil.MEMBERSHIPPLAN,
          "data": {
            "status": each.data.status,
            "clientId": each.data.clientId,
          },
        };
      }
      if (each.data.userType === constantUtil.USER) {
        userMembership[each._id] = {
          "id": each._id,
          "_id": each._id,
          ...each.data,
          "name": constantUtil.MEMBERSHIPPLAN,
          "data": {
            "status": each.data.status,
            "clientId": each.data.clientId,
          },
        };
      }
    });
  };
  membershipPlansJson();

  storageUtil.write(
    constantUtil.PROFESSIONALMEMBERSHIPPLAN,
    professionalMembership
  );
  storageUtil.write(constantUtil.USERMEMBERSHIPPLAN, userMembership);
};

adminMethod.smsInit = async function () {
  const { data } = await storageUtil.read(constantUtil.SMSSETTING);
  const notifiySMSData = await storageUtil.read(constantUtil.NOTIFYSMSSETTING);
  const promotionSMSSetting = await storageUtil.read(
    constantUtil.PROMOTIONSMSSETTING
  );
  const emergencySMSSetting = await storageUtil.read(
    constantUtil.EMERGENCYSMSSETTING
  );

  if (!data) {
    return this.logger.error("INVALID SMS CONFIG");
  }
  if (
    data.smsType === constantUtil.TWILIO &&
    data.accountSid &&
    data.authToken
  ) {
    this.twilio = twilio(data.accountSid, data.authToken);
  } else if (
    notifiySMSData?.data?.smsType === constantUtil.TWILIO &&
    notifiySMSData?.data?.accountSid &&
    notifiySMSData?.data?.authToken
  ) {
    this.twilio = twilio(
      notifiySMSData.data.accountSid,
      notifiySMSData.data.authToken
    );
  } else if (
    promotionSMSSetting?.data?.smsType === constantUtil.TWILIO &&
    promotionSMSSetting?.data?.accountSid &&
    promotionSMSSetting?.data?.authToken
  ) {
    this.twilio = twilio(
      promotionSMSSetting.data.accountSid,
      promotionSMSSetting.data.authToken
    );
  } else if (
    emergencySMSSetting?.data?.smsType === constantUtil.TWILIO &&
    emergencySMSSetting?.data?.accountSid &&
    emergencySMSSetting?.data?.authToken
  ) {
    this.twilio = twilio(
      emergencySMSSetting.data.accountSid,
      emergencySMSSetting.data.authToken
    );
  }
};

adminMethod.updatePrivilegesByRole = async function (context) {
  const responseJson = await this.adapter.updateMany(
    {
      "data.operatorRole": context.params.roleId,
    },
    {
      "data.extraPrivileges": {},
    }
  );
  return responseJson;
};

adminMethod.spacesS3Init = async function () {
  const { data } = await storageUtil.read(constantUtil.GENERALSETTING);
  if (
    !data ||
    !data.spaces ||
    !data.spaces.spacesSecret ||
    !data.spaces.spacesKey ||
    !data.spaces.spacesEndpoint
  )
    return this.logger.error("INVALID SPACES CONFIG");

  const spacesEndpoint = new AWS.Endpoint(data.spaces.spacesEndpoint);
  this.s3 = new AWS.S3({
    "endpoint": spacesEndpoint,
    "accessKeyId": data.spaces.spacesKey,
    "secretAccessKey": data.spaces.spacesSecret,
  });
};

adminMethod.fcmNotifyInit = async function () {
  const fcmSettings = await storageUtil.read(constantUtil.FIREBASESETTING);

  // // if (!fcmSettings.data.firebaseNotifyConfig) {
  // //   return this.logger.error("INVALID FIREBASE CONFIG");
  // // } else {
  // //   try {
  // //     this.fcmNotify = new fcmNotify(fcmSettings.data.firebaseNotifyConfig);
  // //   } catch (e) {
  // //     console.log(e.message);
  // //   }
  // // }
  // if (!fcmSettings.data.firebaseNotifyConfig) {
  //   return this.logger.error("INVALID FIREBASE CONFIG");
  // } else {
  //   try {
  //     const getAccessToken = () => {
  //       return new Promise(function (resolve, reject) {
  //         // const key = require("../placeholders/service-account.json");
  //         const jwtClient = new google.auth.JWT(
  //           fcmSettings.data.firebaseNotifyConfig.client_email,
  //           null,
  //           fcmSettings.data.firebaseNotifyConfig.private_key,
  //           ["https://www.googleapis.com/auth/firebase.messaging"],
  //           null
  //         );
  //         jwtClient.authorize(function (err, tokens) {
  //           if (err) {
  //             reject(err);
  //             return;
  //           }
  //           resolve(tokens.access_token);
  //         });
  //       });
  //     };
  //     const accessToken = await getAccessToken();
  //     const projectId = fcmSettings.data.firebaseNotifyConfig.project_id;
  //     this.fcmAccessToken = new fcmAccessToken({
  //       "accessToken": accessToken,
  //       "projectId": projectId,
  //     });
  //     //-----------------------------
  //   } catch (e) {
  //     console.log(e.message);
  //   }
  // }
};
//-----------------------------
module.exports = adminMethod;
