"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const zmapValidation = require("../validations/zmap.validation");
const zmapAction = require("../actions/zmap.action");
const zmapEvent = require("../events/zmap.event");

module.exports = {
  "name": "zmap",
  "mixins": [DBMixin(constantUtil.DBSCHEMA_ZMAP)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "getAutocomplete": {
      // "auth": constantUtil.REQUIRED,
      // "access": [
      //   constantUtil.ADMIN,
      //   constantUtil.USER,
      //   constantUtil.PROFESSIONAL,
      // ],
      // "params": zmapValidation.getAutocomplete,
      "handler": zmapAction.getAutocomplete,
    },
    "getDirections": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": zmapValidation.getDirections,
      "handler": zmapAction.getDirections,
    },
    "getGeocode": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": zmapValidation.getGeocode,
      "handler": zmapAction.getGeocode,
    },
    "getDistanceMatrix": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": zmapValidation.getDistanceMatrix,
      "handler": zmapAction.getDistanceMatrix,
    },
    "getSnapway": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": zmapValidation.getSnapway,
      "handler": zmapAction.getSnapway,
    },
  },
  "events": {
    "zmap.manageData": {
      "handler": zmapEvent.manageData,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
