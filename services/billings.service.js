"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const billingsEvent = require("../events/billings.event");
// const billingsAction = require('../actions/billings.action')
// const billingsValidation = require('../validations/billings.validation')

module.exports = {
  "name": "billings",
  "mixins": [DBMixin(constantUtil.DBSCHEMABILLINGS)],
  "settings": {},
  "dependencies": [],
  "actions": {},
  "events": {
    "billings.saveBillingCycleCronJob": {
      "handler": billingsEvent.saveBillingCycleCronJob,
    },
    "billings.fetchBillingCycle": {
      "handler": billingsEvent.fetchBillingCycle,
    },
    "billings.fetchCoorperateBillingCycle": {
      "handler": billingsEvent.fetchCoorperateBillingCycle,
    },
    "billings.getBillingIds": {
      "handler": billingsEvent.getBillingIds,
    },
    "billings.getAdminBillingCycle": {
      "handler": billingsEvent.getAdminBillingCycle,
    },
    "billings.getAdminCoorperateBillingCycle": {
      "handler": billingsEvent.getAdminCoorperateBillingCycle,
    },
    "billings.saveCoorperateBillingCycleCronJob": {
      "handler": billingsEvent.saveCoorperateBillingCycleCronJob,
    },
    "billings.getCoorperateOfficeBillingList": {
      "handler": billingsEvent.getCoorperateOfficeBillingList,
    },
    "billings.changeCoorperatePaidStatus": {
      "handler": billingsEvent.changeCoorperatePaidStatus,
    },
    "billings.updatePdfImage": {
      "handler": billingsEvent.updatePdfImage,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
