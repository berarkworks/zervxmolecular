"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const professionalValidations = require("../validations/professional.validation");
const professionalAction = require("../actions/professional.action");
const professionalEvent = require("../events/professional.event");

module.exports = {
  "name": "professional",
  "mixins": [DBMixin(constantUtil.DBSCHEMAPROFESSIONAL)],
  "userModel": DBMixin(constantUtil.DBSCHEMAUSERS),
  "adminModel": DBMixin(constantUtil.DBSCHEMAADMIN),
  "settings": {},
  "dependencies": [],
  "actions": {
    // "validateLoginProfessional": {
    //   "params": professionalValidations.login,
    //   "handler": professionalAction.validateLoginProfessional,
    // },
    "login": {
      "params": professionalValidations.login,
      "handler": professionalAction.login,
    },
    "loginWithPhoneNo": {
      "params": professionalValidations.loginWithPhoneNo,
      "handler": professionalAction.loginWithPhoneNo,
    },
    "verifyOtp": {
      "params": professionalValidations.verifyOtp,
      "handler": professionalAction.verifyOtp,
    },
    "verifyEmail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.verifyEmail,
      "handler": professionalAction.verifyEmail,
    },
    "profile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.profile,
    },
    "updateProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateProfile,
      "handler": professionalAction.updateProfile,
    },
    "updateProfileImage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateProfileImage,
      "handler": professionalAction.updateProfileImage,
    },
    "updateProfileAndVehicleDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateProfileAndVehicleDetails,
      "handler": professionalAction.updateProfileAndVehicleDetails,
    },
    "updateProfileCommonDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.updateProfileCommonDetails,
    },
    "manageServiceType": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": {
        "professionalId": { "type": "string", "min": "1" },
        "serviceTypeId": { "type": "string", "min": "1" },
        "serviceType": { "type": "string", "min": "1" },
      },
      "handler": professionalAction.manageServiceType,
    },
    "activateAndInactivateAccount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.activateAndInactivateAccount,
      "handler": professionalAction.activateAndInactivateAccount,
    },
    "changePhoneNumber": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.changePhoneNumber,
      "handler": professionalAction.changePhoneNumber,
    },
    "updateOnlineStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateOnlineStatus,
      "handler": professionalAction.updateOnlineStatus,
    },
    "updatePaymentMode": {
      "params": {
        "professionalId": { "type": "string", "min": 8 },
        "paymentMode": { "type": "array" },
      },
      "handler": professionalAction.updatePaymentMode,
    },
    "getDocumentListForProfileVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getDocumentListForProfileVerification,
    },
    "updateProfileVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateProfileVerification,
      "handler": professionalAction.updateProfileVerification,
    },
    "removeProfileVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.removeProfileVerification,
      "handler": professionalAction.removeProfileVerification,
    },
    "getDocumentListForVehicleVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getDocumentListForVehicleVerification,
    },
    "getServiceLocationList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.ADMIN],
      "handler": professionalAction.getServiceLocationList,
    },
    "getHubList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getHubList,
    },
    "updateVehicleDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateVehicleDetails,
      "handler": professionalAction.updateVehicleDetails,
    },
    "updateVehicleDocuments": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateVehicleDocuments,
      "handler": professionalAction.updateVehicleDocuments,
    },
    "removeVehicleDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.removeVehicleDetails,
      "handler": professionalAction.removeVehicleDetails,
    },
    "setDefaultVehicle": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN, constantUtil.PROFESSIONAL],
      "params": {
        // "professionalId": { "type": "string", "min": 4 },
        "id": { "type": "string", "min": 4 }, // Vehicle's Id
      },
      "handler": professionalAction.setDefaultVehicle,
    },
    "updateVehicleHubDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateVehicleHubDetails,
      "handler": professionalAction.updateVehicleHubDetails,
    },
    "getDocumentListForHub": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.getDocumentListForHub,
      "handler": professionalAction.getDocumentListForHub,
    },
    "logout": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.logout,
      "handler": professionalAction.logout,
    },
    "updateLiveLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateLiveLocation,
      "handler": professionalAction.updateLiveLocation,
    },
    "trackProfessionalLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": { "professionalId": { "type": "string", "min": "1" } },
      "handler": professionalAction.trackProfessionalLocation,
    },
    "deleteProfessionalUsingProfessionalId": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": {
        "action": { "type": "string", "optional": true },
        "id": { "type": "string", "min": "10", "optional": true },
        "ids": {
          "type": "array",
          "items": { "type": "string" },
          "optional": true,
        },
      },
      "handler": professionalAction.deleteProfessionalUsingProfessionalId,
    },
    "addBankDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      //'params': professionalValidations.addBankDetails,
      "handler": professionalAction.addBankDetails,
    },
    "getBankDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getBankDetails,
    },
    "manageCard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.manageCard,
      "handler": professionalAction.manageCard,
    },
    "getCardsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getCardsList,
    },
    "getProfessionalViewDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getProfessionalViewDetails,
    },
    "getRatings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.getRatings,
      "handler": professionalAction.getRatings,
    },
    "checkProfessionalAvail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.checkProfessionalAvail,
      "handler": professionalAction.checkProfessionalAvail,
    },
    "checkUserAndProfessionalAvailableUsingPhoneNo": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.checkProfessionalAvail,
      "handler":
        professionalAction.checkUserAndProfessionalAvailableUsingPhoneNo,
    },
    "checkContacts": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.checkContacts,
      "handler": professionalAction.checkContacts,
    },
    // DASHBOARD
    "getProfessionalData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.getProfessionalData,
      "handler": professionalAction.getProfessionalData,
    },
    "getVehiclesData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.getVehiclesData,
      "handler": professionalAction.getVehiclesData,
    },
    "getVehiclesDataBasedVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.getVehiclesDataBasedVehicleCategory,
      "handler": professionalAction.getVehiclesDataBasedVehicleCategory,
    },
    "getUploadedList": {
      "auth": constantUtil.REQUIRED,
      // 'access': [constantUtil.ADMIN],
      "params": professionalValidations.getList,
      "handler": professionalAction.getUploadedList,
    },
    "getExpiredProfessionalList": {
      "auth": constantUtil.REQUIRED,
      // 'access': [constantUtil.ADMIN],
      "params": professionalValidations.getList,
      "handler": professionalAction.getExpiredProfessionalList,
    },
    "fetchBillingCycle": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.fetchBillingCycle,
    },
    "getDriverBillingList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.getDriverBillingList,
      "handler": professionalAction.getDriverBillingList,
    },
    "updateSubCategoryOption": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateSubCategoryOption,
      "handler": professionalAction.updateSubCategoryOption,
    },
    "addWalletAmount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.addWalletAmount,
      "handler": professionalAction.addWalletAmount,
    },
    "updatePermissionData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updatePermissionData,
      "handler": professionalAction.updatePermissionData,
    },
    // INVITE AND EARN
    "getInviteAndEarnList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.getInviteAndEarnList,
      "handler": professionalAction.getInviteAndEarnList,
    },
    "downloadInviteAndEarnList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": professionalValidations.getInviteAndEarnList,
      "handler": professionalAction.downloadInviteAndEarnList,
    },
    "downloadProfessionalJoiningList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": professionalValidations.getInviteAndEarnList,
      "handler": professionalAction.downloadProfessionalJoiningList,
    },
    "getUsersJoinerList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.getUsersJoinerList,
      "handler": professionalAction.getUsersJoinerList,
    },
    "getInviteAndEarnDetail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getInviteAndEarnDetail,
    },
    "getInviteAndEarnHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.getInviteAndEarnHistory,
      "handler": professionalAction.getInviteAndEarnHistory,
    },
    "updatePasswordBypass": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": professionalValidations.updatePasswordBypass,
      "handler": professionalAction.updatePasswordBypass,
    },
    "updateLastPriorityAddress": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateLastPriorityAddress,
      "handler": professionalAction.updateLastPriorityAddress,
    },
    "deleteLastPriorityAddress": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.deleteLastPriorityAddress,
    },
    "updateLastPriorityStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.updateLastPriorityStatus,
      "handler": professionalAction.updateLastPriorityStatus,
    },
    "getLastPriorityDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getLastPriorityDetails,
    },
    "getQrcode": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": professionalAction.getQrcode,
    },
    "getProfessionalDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": professionalAction.getProfessionalDetails,
    },
    //PAYMENT ,CARD,BANK ACCOUNT RELATED
    "verifyCard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": professionalValidations.verifyCard,
      "handler": professionalAction.verifyCard,
    },
    "activateMembershipPlan": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.ADMIN],
      "params": professionalValidations.activateMembershipPlan,
      "handler": professionalAction.activateMembershipPlan,
    },
    //--------- Redeem Rewards Point Start ----------------
    "redeemRewardsPoint": {
      "auth": constantUtil.REQUIRED,
      // "access": [
      //   constantUtil.USER,
      //   constantUtil.PROFESSIONAL,
      //   constantUtil.ADMIN,
      // ],
      "params": {
        "lng": { "type": "number", "convert": true },
        "lat": { "type": "number", "convert": true },
        "redeemPoint": { "type": "number", "convert": true },
      },
      "handler": professionalAction.redeemRewardsPoint,
    },
    // Professional Active Inactive..........
    "getProfessionalActiveInactive": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.ADMIN],
      // "params": professionalValidations.activateMembershipPlan,
      "handler": professionalAction.getProfessionalActiveInactive,
    },
    //
    "manageBlockedUserList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": {
        "professionalId": { "type": "string", "convert": true },
        "blockedUserId": { "type": "string", "convert": true },
        "action": { "type": "string", "convert": true },
      },
      "handler": professionalAction.manageBlockedUserList,
    },
    "manageBlockedProfessional": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "convert": true },
        "action": { "type": "string", "convert": true },
        // "blockDuration": { "type": "number", "convert": true },
        "blockDate": { "type": "string", "convert": true, "optional": true },
      },
      "handler": professionalAction.manageBlockedProfessional,
    },

    //--------- Redeem Rewards Point End ----------------
    "getOnlineOfflineList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": professionalAction.getOnlineOfflineList,
    },
    "manageTrustedContact": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": professionalAction.manageTrustedContact,
    },
  },
  "events": {
    "professional.verifyToken": {
      "handler": professionalEvent.verifyToken,
    },
    "professional.getProfessionalByLocation": {
      "params": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "droplat": { "type": "number", "optional": true, "convert": true },
        "droplng": { "type": "number", "optional": true, "convert": true },
        "vehicleCategoryId": { "type": "string", "min": 1, "optional": true },
        "professionalOnlineStatus": { "type": "boolean", "convert": true },
        // "rideType": { "type": "string", "optional": true, "default": "ride" },
        "actionType": {
          "type": "enum",
          "values": [constantUtil.CONST_ONLINE, constantUtil.BOOKING],
          "optional": true,
          "default": constantUtil.CONST_ONLINE,
          "uppercase": true, // Must be uppercase
        },
        "rideType": {
          "type": "enum",
          "values": [constantUtil.CONST_RIDE, constantUtil.CONST_DELIVERY],
          "optional": true,
          "default": constantUtil.CONST_RIDE,
          "uppercase": true, // Must be uppercase
        },
        "radius": { "type": "number", "convert": true },
        // "maxRadius": { "type": "number", "convert": true },
        "checkOngoingBooking": { "type": "boolean", "convert": true },
        "checkLastPriority": { "type": "boolean", "convert": true },
        "forTailRide": { "type": "boolean", "convert": true },
        "isSubCategoryAvailable": { "type": "boolean", "convert": true },
        "isForceAppliedToProfessional": { "type": "boolean", "convert": true },
        "isRestrictMaleBookingToFemaleProfessional": {
          "type": "boolean",
          "convert": true,
        },
        "isRentalSupported": {
          "type": "boolean",
          "convert": true,
          "default": false,
        },
        "serviceAreaId": {
          "type": "string",
          "convert": true,
          "optional": true,
        },
        "isShareRide": {
          "type": "boolean",
          "convert": true,
          "optional": true,
          "default": null,
        },
      },
      "handler": professionalEvent.getProfessionalByLocation,
    },
    "professional.getUnregisteredProfessionalList": {
      "handler": professionalEvent.getUnregisteredProfessionalList,
    },
    "professional.closeUnregisteredProfessional": {
      "handler": professionalEvent.closeUnregisteredProfessional,
    },
    "professional.addUnregisteredProfessionalNotes": {
      "handler": professionalEvent.addUnregisteredProfessionalNotes,
    },
    "professional.addUnregisteredProfessionalStatusChange": {
      "handler": professionalEvent.addUnregisteredProfessionalStatusChange,
    },
    "professional.getProfessionalList": {
      "handler": professionalEvent.getProfessionalList,
    },
    "professional.getEditProfessional": {
      "handler": professionalEvent.getEditProfessional,
    },
    "professional.updateProfessionalStep1": {
      "handler": professionalEvent.updateProfessionalStep1,
    },
    "professional.getAllDocumentForUpload": {
      "handler": professionalEvent.getAllDocumentForUpload,
    },
    "professional.updateProfessionalStep2": {
      "handler": professionalEvent.updateProfessionalStep2,
    },
    "professional.updateProfessionalStep3": {
      "handler": professionalEvent.updateProfessionalStep3,
    },
    "professional.updateProfessionalStep4": {
      "handler": professionalEvent.updateProfessionalStep4,
    },
    "professional.updateAllVehiclesShareableDetailsUsingCategoryId": {
      "handler":
        professionalEvent.updateAllVehiclesShareableDetailsUsingCategoryId,
    },
    "professional.updateBankDetailsProfessional": {
      "handler": professionalEvent.updateBankDetailsProfessional,
    },
    "professional.changeProfessionalsStatus": {
      "handler": professionalEvent.changeProfessionalsStatus,
    },
    "professional.updateLiveLocation": {
      "params": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "currentBearing": { "type": "number", "convert": true },
        "speed": { "type": "number", "convert": true },
        "verticalAccuracy": { "type": "number", "convert": true },
        "horizontalAccuracy": { "type": "number", "convert": true },
        "altitude": { "type": "number", "convert": true },
      },
      "handler": professionalEvent.updateLiveLocation,
    },
    "professional.getById": {
      "params": { "id": { "type": "string", "min": 1 } },
      "handler": professionalEvent.getById,
    },
    "professional.updateWalletAmountTripEnded": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "paymentMethod": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updateWalletAmountTripEnded,
    },
    "professional.updateOnGoingBooking": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updateOnGoingBooking,
    },
    "professional.updateOnGoingAndTailBooking": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updateOnGoingAndTailBooking,
    },
    "professional.updateTailOnGoingBooking": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updateTailOnGoingBooking,
    },
    "professional.updatependingReview": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "optional": true },
        "lastCurrencyCode": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updatependingReview,
    },
    "professional.updateRating": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "rating": { "type": "number", "max": 5 },
      },
      "handler": professionalEvent.updateRating,
    },
    "professional.updateEscortRating": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "rating": { "type": "number", "max": 5 },
      },
      "handler": professionalEvent.updateEscortRating,
    },
    "professional.changeVehicleStatus": {
      "handler": professionalEvent.changeVehicleStatus,
    },
    "professional.professionalProfileDocumentVerification": {
      "handler": professionalEvent.professionalProfileDocumentVerification,
    },
    "professional.professionalVehicleDocumentVerification": {
      "handler": professionalEvent.professionalVehicleDocumentVerification,
    },
    "professional.deleteProfessionalProfileDocument": {
      "handler": professionalEvent.deleteProfessionalProfileDocument,
    },
    "professional.deleteProfessionalVehicleDocument": {
      "handler": professionalEvent.deleteProfessionalVehicleDocument,
    },
    "professional.addProfessionalNotes": {
      "handler": professionalEvent.addProfessionalNotes,
    },
    "professional.addVehicleNotes": {
      "handler": professionalEvent.addVehicleNotes,
    },
    "professional.professionalDocumentExpiryNotify": {
      "handler": professionalEvent.professionalDocumentExpiryNotify,
    },
    "professional.getVehicleList": {
      "handler": professionalEvent.getVehicleList,
    },
    "professional.editVehicleDetails": {
      "handler": professionalEvent.editVehicleDetails,
    },
    "professional.professionalVerification": {
      "handler": professionalEvent.professionalVerification,
    },
    "professional.getUnverifiedProfessionalList": {
      "handler": professionalEvent.getUnverifiedProfessionalList,
    },
    "professional.getExpiredProfessionalList": {
      "handler": professionalEvent.getExpiredProfessionalList,
    },
    "professional.updateTrustedContact": {
      "handler": professionalEvent.updateTrustedContact,
    },
    "professional.changeTrustedContactReason": {
      "handler": professionalEvent.changeTrustedContactReason,
    },
    "professional.getTrustedContactsList": {
      "handler": professionalEvent.getTrustedContactsList,
    },
    "professional.updateSubscriptionPlan": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "planId": { "type": "string", "optional": true },
        "subscribedAt": { "type": "string", "optional": true },
        "subscribedEndAt": { "type": "string", "optional": true },
        "subscribedAtTimestamp": { "type": "number", "optional": true },
        "subscribedEndAtTimestamp": { "type": "number", "optional": true },
        "price": { "type": "number", "optional": true },
      },
      "handler": professionalEvent.updateSubscriptionPlan,
    },
    "professional.getProfessionalListForNotification": {
      "handler": professionalEvent.getProfessionalListForNotification,
    },
    "professional.getProfessionalByAdmin": {
      "handler": professionalEvent.getProfessionalByAdmin,
    },
    "professional.updateAllOnlineProfessionalStatus": {
      "handler": professionalEvent.updateAllOnlineProfessionalStatus,
    },
    "professional.sendMinimumAmountWarningNotification": {
      "handler": professionalEvent.sendMinimumAmountWarningNotification,
    },
    // "professional.isProfessionalVerified": {
    //   "handler": professionalEvent.isProfessionalVerified,
    // },
    "professional.cronCheckProfileDocumentsExpire": {
      "handler": professionalEvent.cronCheckProfileDocumentsExpire,
    },
    "professional.cronCheckVehicleDocumentsExpire": {
      "handler": professionalEvent.cronCheckVehicleDocumentsExpire,
    },
    "professional.getWishedProfessionalList": {
      "handler": professionalEvent.getWishedProfessionalList,
    },
    "professional.updateWalletRechargeOrWithdraw": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "transStatus": { "type": "string", "min": 1 },
      },
      "handler": professionalEvent.updateWalletRechargeOrWithdraw,
    },
    "professional.updateAllProfessionals": {
      "handler": professionalEvent.updateAllProfessionals,
    },
    "professional.blockedTheProfessionalsUsingCron": {
      "handler": professionalEvent.blockedTheProfessionalsUsingCron,
    },
    "professional.unBlockedTheProfessionalsUsingCron": {
      "handler": professionalEvent.unBlockedTheProfessionalsUsingCron,
    },
    // "professional.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId":
    //   {
    //     "params": {
    //       "serviceTypeId": { "type": "string", "min": 1 },
    //     },
    //     "handler":
    //       professionalEvent.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId,
    //   },
    "professional.getAllProfessionals": {
      "handler": professionalEvent.getAllProfessionals,
    },
    "professional.getAvailableWalletAmount": {
      "handler": professionalEvent.getAvailableWalletAmount,
    },
    "professional.getWalletAmountList": {
      "handler": professionalEvent.getWalletAmountList,
    },
    "professional.categoryChanges": {
      "handler": professionalEvent.categoryChanges,
    },
    "professional.updateOnGoingSecurityBooking": {
      "params": {
        "id": { "type": "string", "min": 4 },
        "escortBookingId": { "type": "string", "optional": true },
        "type": { "type": "string", "optional": true },
        "isOfficerAssigned": { "type": "boolean", "optional": true },
      },
      "handler": professionalEvent.updateOnGoingSecurityBooking,
    },
    "professional.updateSecurityPendingReview": {
      "params": {
        "id": { "type": "string", "min": 4 },
        "escortBookingId": { "type": "string", "optional": true },
        "type": { "type": "string", "min": 1 },
      },
      "handler": professionalEvent.updateSecurityPendingReview,
    },
    "professional.inviteAndEarn": {
      "params": {
        "joinerId": { "type": "string", "min": 4 },
      },
      "handler": professionalEvent.inviteAndEarn,
    },
    "professional.updateJoinerRideCount": {
      "params": {
        "joinerId": { "type": "string", "min": 4 },
      },
      "handler": professionalEvent.updateJoinerRideCount,
    },
    "professional.inviterRideEarnings": {
      "params": {
        "inviterUniqueCode": { "type": "string", "min": 4 },
        "amount": { "type": "number", "convert": true },
        "bookingId": { "type": "string" },
        "refBookingId": { "type": "string" },
        "transactionReason": { "type": "string" },
      },
      "handler": professionalEvent.inviterRideEarnings,
    },
    "professional.updateWalletAmountInBooking": {
      "params": {
        "id": { "type": "string", "min": 1 },
        "finalAmount": { "type": "number", "convert": true },
      },
      "handler": professionalEvent.updateWalletAmountInBooking,
    },
    "professional.updateOnlineStatus": {
      "params": {
        "id": { "type": "string", "min": 1 },
        "status": { "type": "boolean" },
      },
      "handler": professionalEvent.updateOnlineStatus,
    },
    "professional.getCategoryList": {
      "handler": professionalEvent.getCategoryList,
    },
    // 'professional.addCard': {
    //   'handler': professionalEvent.addCard,
    // },
    "professional.addVerifiedCard": {
      "params": professionalValidations.addVerifiedCard,
      "handler": professionalEvent.addVerifiedCard,
    },
    "professional.sendMoneyToFriends": {
      // 'params': {
      //   'professionlId': { 'type': 'string', 'min': 1, 'convert': true },
      //   'amount': { 'type': 'number', 'convert': true },
      //   'phoneNumber': { 'type': 'string', 'min': 1 },
      //   'phoneCode': { 'type': 'string', 'min': 1 },
      // },
      "handler": professionalEvent.sendMoneyToFriends,
    },
    // professionl doucemnts expiry
    "professional.expireDocumentsProfessionlUnverified": {
      "handler": professionalEvent.expireDocumentsProfessionlUnverified,
    },
    // DB ACCESS
    "professional.removePrefix0": {
      "handler": professionalEvent.removePrefix0,
    },
    "professional.unverifiedDocProfessional": {
      "handler": professionalAction.getUploadedList, // just reused check care fully
    },
    "professional.forceLogout": {
      "handler": professionalEvent.forceLogout,
    },
    // Get Card Details -- Added by Ajith
    "professional.getCardDetailsById": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "cardId": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.getCardDetailsById,
    },
    // Get Card Details -- Added by Ajith
    "professional.updateDefaultCardStatusInActiveActive": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "cardId": { "type": "string", "optional": true },
      },
      "handler": professionalEvent.updateDefaultCardStatusInActiveActive,
    },
    // wallet update
    "professional.updateWalletAmount": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "availableAmount": { "type": "number" },
        "freezedAmount": { "type": "number" },
        "isSendNotification": {
          "type": "boolean",
          "convert": true,
          "optional": true,
        },
        "notificationMessage": {
          "type": "string",
          "optional": true,
        },
        "requestFrom": {
          "type": "enum",
          "values": [constantUtil.WALLETWITHDRAWAL],
          "optional": true,
        },
      },
      "handler": professionalEvent.updateWalletAmount,
    },
    "professional.membershipExpiring": {
      "handler": professionalEvent.membershipExpiring,
    },
    "professional.checkProfessionalDocumentList": {
      "handler": professionalEvent.checkProfessionalDocumentList,
    },
    "professional.membershipPlanExpiring": {
      "handler": professionalEvent.membershipPlanExpiring,
    },
    "professional.getMembershipList": {
      "handler": professionalEvent.getMembershipList,
    },
    "professional.getProfessionalDetails": {
      "params": {
        "phoneNumber": { "type": "string", "min": 1 },
      },
      "handler": professionalEvent.getProfessionalDetails,
    },
    "professional.updateWalletRewardPoint": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "rewardPoints": { "type": "number", "convert": true },
      },
      "handler": professionalEvent.updateWalletRewardPoint,
    },
    "professional.checkServiceAreaUsingId": {
      "params": {
        "serviceAreaId": { "type": "string", "convert": true, "min": 12 }, //serviceCategory
      },
      "handler": professionalEvent.checkServiceAreaUsingId,
    },
    // #region DataMigration Script
    "professional.professionalProfileDocumentsDataMigrationScript": {
      "handler":
        professionalEvent.professionalProfileDocumentsDataMigrationScript,
    },
    // #endregion DataMigration Script
    "professional.sendPromotion": {
      "handler": professionalEvent.sendPromotion,
    },
    "professional.updateProfessionalShareRide": {
      "params": {
        "actionStatus": { "type": "string", "convert": true },
        "professionalId": { "type": "string", "convert": true },
        "bookingId": { "type": "string", "optional": true },
        // "pickUpLat": { "type": "number", "optional": true },
        // "pickUpLng": { "type": "number", "optional": true },
        // "dropLat": { "type": "number", "optional": true },
        // "dropLng": { "type": "number", "optional": true },
      },
      "handler": professionalEvent.updateProfessionalShareRide,
    },
    "professional.getEndRideIssueList": {
      "handler": professionalEvent.getEndRideIssueList,
    },
    "professional.updateBookingInfoIds": {
      "handler": professionalEvent.updateBookingInfoIds,
    },
    "professional.calculateAndUpdateIncentiveAmount": {
      "handler": professionalEvent.calculateAndUpdateIncentiveAmount,
    },
    "professional.getProfessionalListByServiceAreaId": {
      "params": {
        "serviceAreaId": { "type": "string", "convert": true, "min": 12 },
      },
      "handler": professionalEvent.getProfessionalListByServiceAreaId,
    },
    "professional.checkProfessionalAvail": {
      "handler": professionalEvent.checkProfessionalAvail,
    },
    "professional.checkUniqueReferralCode": {
      "handler": professionalEvent.checkUniqueReferralCode,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
