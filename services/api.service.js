"use strict";

const ApiGateway = require("moleculer-web");

const routes = require("../routes");
const cryptoUtils = require("../utils/crypto.util");

const constantUtil = require("../utils/constant.util");
// const apiActions = require("../actions/api.action");

module.exports = {
  "name": "api",
  "mixins": [ApiGateway],
  "settings": {
    "port": process.env.PORT || 8080,
    "ip": "0.0.0.0",
    "use": [],
    // FOR DEVELOPMENT CORS
    "cors": {
      "origin": "*",
      "methods": ["GET", "POST"],
      "Access-Control-Allow-Origin": "*",
      "credentials": true,
    },
    // FOR DEVELOPMENT CORS
    "routes": routes,
    "log4XXResponses": false,
    "logRequestParams": null,
    "logResponseData": null,
    "assets": {
      "folder": "static",
      "options": {},
    },
    "actions": {},
  },

  "methods": {
    async authenticate(context, route, req) {
      const clientIp =
        req.headers["x-forwarded-for"] ||
        req.connection?.remoteAddress ||
        req.socket?.remoteAddress ||
        req.connection?.socket?.remoteAddress ||
        req.headers["phoneipaddress"];
      context.meta["clientIp"] = clientIp; //ip Address
      console.log(
        "myip_forward" +
          req.headers["x-forwarded-for"] +
          "remoteAddress_COnn" +
          req.connection?.remoteAddress +
          "socket_remoteAddress" +
          req.socket?.remoteAddress +
          "socket_remote_2" +
          req.connection?.socket?.remoteAddress +
          "phoneipaddress" +
          req.headers["phoneipaddress"]
      );
      //client Id
      const clientId = req.headers["clientid"] || req.headers["clientId"];
      context.meta["clientId"] = clientId; //client Id
      //language Code
      const langCode = req.headers["langcode"] || req.headers["langCode"];
      context.meta["langCode"] = langCode;
      //
      if (req.$action.auth === constantUtil.REQUIRED) {
        const auth = req.headers["authorization"];

        if (auth && auth.startsWith("Bearer")) {
          const token = auth.slice(7).trim();
          let jwt = null;
          try {
            jwt = cryptoUtils.jwtVerify(token, {
              "deviceType": context.meta.userAgent.deviceType,
              "platform": context.meta.userAgent.platform,
            });
          } catch (e) {
            const payload = {
              "type": constantUtil.EXTERNAL,
            };
            jwt = { "auth": true, "payload": payload };
          }

          if (jwt.auth) {
            context.meta.userType = jwt.payload.type;

            let validDevice = [];
            if (jwt.payload.type === constantUtil.USER) {
              context.meta.userId = jwt.payload._id;
              validDevice = await this.broker.emit("user.verifyToken", {
                "userId": jwt.payload._id,
                "accessToken": token,
                "langCode": langCode,
              });
              if (validDevice[0]) {
                context.meta.userDetails = {
                  "firstName": validDevice[0].firstName,
                  "lastName": validDevice[0].lastName,
                  "phone": validDevice[0].phone,
                  "avatar": validDevice[0].avatar,
                  "review": validDevice[0].review,
                  "wallet": validDevice[0].wallet,
                  "stripeCustomerId": validDevice[0].stripeCustomerId,
                  "languageCode": validDevice[0].languageCode || langCode,
                };
              }
            }
            if (jwt.payload.type === constantUtil.PROFESSIONAL) {
              context.meta.professionalId = jwt.payload._id;
              validDevice = await this.broker.emit("professional.verifyToken", {
                "professionalId": jwt.payload._id,
                "accessToken": token,
                "langCode": langCode,
              });
              if (validDevice[0]) {
                context.meta.professionalDetails = {
                  "firstName": validDevice[0].firstName,
                  "lastName": validDevice[0].lastName,
                  "phone": validDevice[0].phone,
                  "avatar": validDevice[0].avatar,
                  "review": validDevice[0].review,
                  "wallet": validDevice[0].wallet,
                  "stripeCustomerId": validDevice[0].stripeCustomerId,
                  "languageCode": validDevice[0].languageCode || langCode,
                };
              }
            }
            if (jwt.payload.type === constantUtil.ADMIN) {
              context.meta.adminId = jwt.payload._id;
              validDevice = await this.broker.emit("admin.verifyToken", {
                "adminId": jwt.payload._id,
                "accessToken": token,
              });
              if (validDevice[0]) {
                if (validDevice[0].name === constantUtil.HUBS) {
                  context.meta.adminDetails = {
                    "firstName": validDevice[0].data.hubsName,
                    "lastName": validDevice[0].data.lastName || "",
                    "phone": validDevice[0].data.phone,
                    "avatar": validDevice[0].data.avatar || "",
                    "languageCode":
                      validDevice[0].data.languageCode ||
                      constantUtil.DEFAULT_LANGUAGE,
                  };
                } else {
                  context.meta.adminDetails = {
                    "firstName": validDevice[0].data.firstName,
                    "lastName": validDevice[0].data.lastName,
                    "phone": validDevice[0].data.phone,
                    "avatar": validDevice[0].data.avatar,
                    "languageCode": validDevice[0].data.languageCode,
                  };
                }
              }
            }
            // console.log(context.meta);
            if (jwt.payload.type === constantUtil.OFFICER) {
              context.meta.officerId = jwt.payload._id;
              validDevice = await this.broker.emit("officer.verifyToken", {
                "officerId": jwt.payload._id,
                "accessToken": token,
              });
              if (validDevice[0]) {
                context.meta.userDetails = {
                  "firstName": validDevice[0].firstName,
                  "lastName": validDevice[0].lastName,
                  "phone": validDevice[0].phone,
                  "avatar": validDevice[0].avatar,
                  "review": validDevice[0].review,
                  "languageCode": validDevice[0].languageCode,
                };
              }
            }
            if (jwt.payload.type === constantUtil.EXTERNAL) {
              const storageUtil = require("../utils/storage.util");
              const generalSettings = await storageUtil.read(
                constantUtil.GENERALSETTING
              );
              if (generalSettings.data.authToken === token) {
                const dfgsd = token;
                validDevice.push({ "dgfsd": 1 });
              }
            }
            if (!validDevice[0])
              throw new ApiGateway.Errors.UnAuthorizedError(
                ApiGateway.Errors.ERR_INVALID_TOKEN
              );
            return jwt.payload;
          } else {
            throw new ApiGateway.Errors.UnAuthorizedError(
              ApiGateway.Errors.ERR_INVALID_TOKEN
            );
          }
        } else {
          throw new ApiGateway.Errors.UnAuthorizedError(
            ApiGateway.Errors.ERR_NO_TOKEN
          );
        }
      }
    },
    async authorize(context, route, req) {
      if (
        req.$action.auth == constantUtil.REQUIRED &&
        req.$action.access &&
        !req.$action.access.includes(context.meta.userType)
      ) {
        throw new ApiGateway.Errors.UnAuthorizedError("NO_RIGHTS");
      }
      return true;
    },
  },
};
