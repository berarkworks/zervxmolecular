const Cron = require("moleculer-cron");
const moment = require("moment");

const exec = require("child_process").exec;
const fs = require("fs");

const storageUtil = require("../utils/storage.util");
const constantUtil = require("../utils/constant.util");

module.exports = {
  "name": "cronJob",
  "mixins": [Cron],
  "crons": [
    {
      "name": "minuteCronJob",
      "cronTime": "* * * * *", // Original Time
      // "cronTime": "0 16 * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        const cachedBookingData = await storageUtil.read(constantUtil.SCHEDULE);
        if (cachedBookingData?.length) {
          for (const element of cachedBookingData) {
            if (!element.bookingDate) continue;

            // const bookingDate = moment.utc(
            //   new Date(element.bookingDate).toISOString()
            // );
            // const beforeOneHourBooking = bookingDate.subtract(50, "minutes");
            // const currentData = moment(new Date().toISOString()).utc();
            // const currentDataNew = moment.utc(new Date().toISOString());
            // const differenceInMinutes = Math.round(
            //   moment
            //     .duration(beforeOneHourBooking.diff(currentData))
            //     .asMinutes()
            // );
            const differenceInMinutes = Math.round(
              Math.round(new Date() - new Date(element.bookingDate)) / 1000 / 60
            );
            // BOOKING 1HR BEFORE FIRST 45 MINUTES and BEFORE FIRST 2 MINUTES
            // if (differenceInMinutes === -45 || differenceInMinutes === -3) {
            if (differenceInMinutes === -20 || differenceInMinutes === -3) {
              await this.emit("booking.notifyChecklistEnabledProfessional", {
                "bookingId": element._id,
                "isWish": true,
              });
            }
            // BOOKING 1HR BEFORE FIRST 40 MINUTES and BEFORE FIRST 1 MINUTES
            if (
              // differenceInMinutes === -40 ||
              // differenceInMinutes === -30 ||
              // differenceInMinutes === -25 ||
              // differenceInMinutes === -20 ||
              // // differenceInMinutes === -15 ||
              // // differenceInMinutes === -10 ||
              // // differenceInMinutes === -2 ||
              differenceInMinutes >= -15 &&
              differenceInMinutes <=
                parseInt(gerenalsetting.data.adminBookingExpiredThreshold)
            ) {
              await this.emit("booking.notifyChecklistEnabledProfessional", {
                "bookingId": element._id,
                "isWish": false,
                "radius": gerenalsetting.data.requestDistance, // FROM GENERAL SETTNGS
              });
            }
            // BOOKING 1HR BEFORE FIRST 35 MINUTES and BEFORE FIRST 3 MINUTES
            if (
              // differenceInMinutes === -35 ||
              // differenceInMinutes === -5 ||
              // differenceInMinutes === -1
              differenceInMinutes === -12
            ) {
              await this.emit("booking.notifyScheduleRideToAdmin", {
                "bookingId": element._id,
              });
            }
          }
        }
        // this.emit("socket.sendStatusNotificationToWebBookingApp", {
        //   "bookingId": "bookingData._id",
        //   "bookingStatus": "bookingData.bookingStatus",
        //   "dateTime": "bookingData.updatedAt",
        // });
        this.emit("booking.bookingTimeOut", {});
        this.emit("professional.updateAllOnlineProfessionalStatus", {});
        this.emit("officer.updateAllOnlineOfficerStatus", {});
        await this.emit("professional.membershipPlanExpiring", {});
        // #region Data Migration
        //Booking Data Migration
        // await this.emit("booking.bookingDataMigrationScript", {
        //   "requestFrom": constantUtil.LOGIN,
        // });
        // //professional Profile & Vehicle Document DataMigration
        // await this.emit(
        //   "professional.professionalProfileDocumentsDataMigrationScript"
        // );
        // #endregion Data Migration
        // #region Update Professional Document & TimeStamp
        if (
          gerenalsetting?.data?.registrationSetupUpdateTime &&
          gerenalsetting?.data?.registrationSetupRedisUpdateTime &&
          gerenalsetting?.data?.registrationSetupUpdateTime ===
            gerenalsetting?.data?.registrationSetupRedisUpdateTime
        ) {
          await this.emit("admin.updateGeneralSettingTimestampInDBAndRedis", {
            "type": constantUtil.CONST_REGISTRATIONSETTING_REDIS,
          });
          this.emit("professional.updateAllProfessionals", {}); // UPDATE ALL PROFESSIONALS
        }
        // #region Update Professional Document & TimeStamp
      },
      "manualStart": false,
    },
    {
      "name": "scheduledRide_Billing_Membership",
      "cronTime": "0 0 * * *", // At 00:00 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        // const serviceTypeList = [
        //   constantUtil.ALL,
        //   constantUtil.CONST_RIDE,
        //   constantUtil.CONST_DELIVERY,
        // ];
        // serviceTypeList.forEach(async (serviceType) => {
        //   await this.emit("professional.cronCheckProfileDocumentsExpire", {
        //     "serviceType": serviceType?.toUpperCase(),
        //   });
        // });
        // await this.emit("professional.cronCheckVehicleDocumentsExpire", {
        //   "serviceType": constantUtil.ALL,
        // });
        // await this.emit("professional.cronCheckVehicleDocumentsExpire", {
        //   "serviceType": constantUtil.CONST_RIDE,
        // });
        // await this.emit("professional.cronCheckVehicleDocumentsExpire", {
        //   "serviceType": constantUtil.CONST_DELIVERY,
        // });
        // serviceTypeList.forEach(async (serviceType) => {
        //   await this.emit("professional.cronCheckVehicleDocumentsExpire", {
        //     "serviceType": serviceType?.toUpperCase(),
        //   });
        // });
        await this.emit("booking.updateUpcommingScheduledRideInRedis", {});
        // await this.emit("professional.cronCheckDocumentsExpire", {
        //   "serviceType": constantUtil.CONST_RIDE,
        // });
        // await this.emit("professional.cronCheckDocumentsExpire", {
        //   "serviceType": constantUtil.CONST_DELIVERY,
        // });
        // await this.emit("professional.cronCheckDocumentsExpire", {
        //   "serviceType": constantUtil.ALL,
        // });
        await this.emit("billings.saveBillingCycleCronJob", {});
        await this.emit("professional.membershipExpiring", {});
        await this.emit("professional.membershipPlanExpiring", {});
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckVehicleDocuments_All",
      "cronTime": "10 0 * * *", // At 0:10 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckVehicleDocumentsExpire", {
            "serviceType": constantUtil.ALL,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckVehicleDocuments_Ride",
      "cronTime": "15 0 * * *", // At 00:15 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckVehicleDocumentsExpire", {
            "serviceType": constantUtil.CONST_RIDE,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckVehicleDocuments_Delivery",
      "cronTime": "20 0 * * *", // At 00:20 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckVehicleDocumentsExpire", {
            "serviceType": constantUtil.CONST_DELIVERY,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckProfileDocuments_All",
      "cronTime": "25 0 * * *", // At 00:25 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        // const serviceTypeList = [
        //   constantUtil.ALL,
        //   constantUtil.CONST_RIDE,
        //   constantUtil.CONST_DELIVERY,
        // ];
        // serviceTypeList.forEach(async (serviceType) => {
        //   await this.emit("professional.cronCheckProfileDocumentsExpire", {
        //     "serviceType": serviceType?.toUpperCase(),
        //   });
        // });
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckProfileDocumentsExpire", {
            "serviceType": constantUtil.ALL,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckProfileDocuments_Ride",
      "cronTime": "30 0 * * *", // At 00:30 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckProfileDocumentsExpire", {
            "serviceType": constantUtil.CONST_RIDE,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "documentsExpireCheckProfileDocuments_Delivery",
      "cronTime": "35 0 * * *", // At 00:35 AM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting.data?.isEnableCronJob) {
          await this.emit("professional.cronCheckProfileDocumentsExpire", {
            "serviceType": constantUtil.CONST_DELIVERY,
          });
        }
      },
      "manualStart": false,
    },
    {
      "name": "DBBackup",
      "cronTime": "0 23 * * *", // At 23.00 --> 11 PM // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        let cmd = null;
        const dbName = process.env.CLIENT_KEY ? process.env.CLIENT_KEY : "rays";
        //--------------------------------
        try {
          const today = new Date();
          const dir =
            "/root/dump/" +
            today.toLocaleDateString("en-US", { "weekday": "long" });
          if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
          }
          if (gerenalsetting.data?.dbBackupType === constantUtil.NONE) {
            console.log("No database backup.");
          } else if (
            gerenalsetting.data?.dbBackupType === constantUtil.CONST_DUMP
          ) {
            cmd =
              "mongodump --host 0.0.0.0 --port 27017 --db " +
              dbName +
              " --out " +
              dir; // Command for mongodb dump process
          } else {
            cmd =
              "mongodump --host 0.0.0.0 --port 27017 --db " +
              dbName +
              " --out " +
              dir +
              " --gzip"; // Command for mongodb dump process
          }
          exec(cmd, function (error, stdout, stderr) {
            if (error)
              console.log("Successfully backedup the database" + error.message);
            //once successful or error
            console.log("Successfully backedup the database");
          });
          //--------------------------------
          let before5Day = new Date();
          before5Day = new Date(before5Day.setDate(before5Day.getDate() - 5));
          const before5DayDir =
            "/root/dump/" +
            before5Day.toLocaleDateString("en-US", { "weekday": "long" });
          if (fs.existsSync(before5DayDir)) {
            fs.rmdirSync(before5DayDir, { "recursive": true, "force": true });
          }
        } catch (e) {
          console.log("Error in database backedup.");
        }
        //--------------------------------
        this.emit("professional.unBlockedTheProfessionalsUsingCron", {
          "langCode": gerenalsetting.data.languageCode,
        });
      },
      "manualStart": false,
    },
    {
      "name": "IncentiveProgram_UpdateSignup",
      "cronTime": "40 23 * * *", // At 23.40 --> 11:40 PM // Original Time
      // "cronTime": "00 16 * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting?.data?.isEnableIncentive) {
          await this.emit("log.updateSignupLogByCronjob", {});
        }
      },
      "manualStart": false,
    },
    {
      "name": "IncentiveProgram_UpdateIncentiveAmountToUser",
      "cronTime": "40 23 * * *", // At 23.40 --> 11:40 PM // Original Time
      // "cronTime": "09 16 * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting?.data?.isEnableIncentive) {
          await this.emit("admin.runIncentiveProgramToUserByCronjob", {});
        }
      },
      "manualStart": false,
    },
    {
      "name": "IncentiveProgram_UpdateIncentiveAmountToProfessional",
      "cronTime": "50 23 * * *", // At 23.50 --> 11:50 PM // Original Time
      // "cronTime": "09 16 * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        if (gerenalsetting?.data?.isEnableIncentive) {
          await this.emit(
            "admin.runIncentiveProgramToProfessionalByCronjob",
            {}
          );
        }
      },
      "manualStart": false,
    },
    {
      "name": "coorperateBillingCycle",
      "cronTime": "0 0 1 * *", // At Every Month
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        // Is Corporate Ride Wallet Pay Disabled then Corporate Billing Cycle is Automatically Enable (Cron Job Run)
        if (!gerenalsetting?.data?.isEnableCorporateRideWalletPay) {
          await this.emit("billings.saveCoorperateBillingCycleCronJob", {});
        }
      },
      "manualStart": false,
    },
    {
      "name": "20MinuteCronJob_AutoBlockProfessionalBasedOnRanking",
      "cronTime": "*/20 * * * *", // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        const gerenalsetting = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        this.emit("professional.blockedTheProfessionalsUsingCron", {
          "langCode": gerenalsetting.data.languageCode,
        });
        this.emit("professional.sendMinimumAmountWarningNotification", {
          "langCode": gerenalsetting.data.languageCode,
        });
      },
      "manualStart": false,
    },
    {
      "name": "30MinuteCron",
      "cronTime": "*/30 * * * *", // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        await this.emit("booking.adminBookingTimeOut", {});
      },
      "manualStart": false,
    },
    {
      "name": "50MinuteCron",
      "cronTime": "*/50 * * * *", // Original Time
      // "cronTime": "* * * * *", // For Testing
      // 'timeZone': gerenalsetting.data.cronTimeZone,
      "onTick": async function () {
        await this.emit("booking.updateFCMAccessToken", {
          "clientId": "clientId",
        }); // FCM Access Token
      },
      "manualStart": false,
    },
  ],
  "actions": {},
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  async created() {},
  async started() {},
  async stopped() {},
};
