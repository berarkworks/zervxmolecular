"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const categoryValidation = require("../validations/category.validation");
const categoryAction = require("../actions/category.action");
const categoryEvent = require("../events/category.event");
//
const bookingMethods = require("../methods/category.methods");

module.exports = {
  "name": "category",
  "mixins": [DBMixin("categories")],
  "settings": {},
  "dependencies": [],
  "actions": {
    "userGetLocationBasedCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": categoryValidation.userGetLocationBasedCategory,
      "handler": categoryAction.userGetLocationBasedCategory,
    },
    "userGetLocationBasedCategoryForGustUser": {
      "access": [constantUtil.GUESTUSER],
      "params": categoryValidation.userGetLocationBasedCategory,
      "handler": categoryAction.userGetLocationBasedCategory,
    },
  },
  "events": {
    "category.updateServiceCategory": {
      "handler": categoryEvent.updateServiceCategory,
    },
    "category.updateVehicleShareRideConfigDetails": {
      "handler": categoryEvent.updateVehicleShareRideConfigDetails,
    },
    "category.getAllServiceCategory": {
      "handler": categoryEvent.getAllServiceCategory,
    },
    "category.getAllServiceActiveInactiveCategory": {
      "handler": categoryEvent.getAllServiceActiveInactiveCategory,
    },
    "category.getServiceCategoryList": {
      "handler": categoryEvent.getServiceCategoryList,
    },
    "category.getServiceCategoryById": {
      "handler": categoryEvent.getServiceCategoryById,
    },
    "category.getServiceLocationList": {
      "handler": categoryEvent.getServiceLocationList,
    },
    "category.getAllServiceMultiPoint": {
      "handler": categoryEvent.getAllServiceMultiPoint,
    },
    "category.changeServiceCategoryStatus": {
      "handler": categoryEvent.changeServiceCategoryStatus,
    },
    "category.getCategoryById": {
      "params": { "id": "string" },
      "handler": categoryEvent.getCategoryById,
    },
    "category.getServiceBasedLocation": {
      "params": { "categoryName": "string" },
      "handler": categoryEvent.getServiceBasedLocation,
    },
    "category.getEagleServiceCategory": {
      "params": { "categoryName": "string" },
      "handler": categoryEvent.getEagleServiceCategory,
    },
    "category.updateCategoryFareBreakup": {
      "params": categoryValidation.updateCategoryFareBreakup,
      "handler": categoryEvent.updateCategoryFareBreakup,
    },
    "category.getCategoryFareBreakup": {
      "params": categoryValidation.getCategoryFareBreakup,
      "handler": categoryEvent.getCategoryFareBreakup,
    },
    "category.getServiceBasedCategoryData": {
      "params": { "categoryName": "string" },
      "handler": categoryEvent.getServiceBasedCategoryData,
    },
    "category.getPopularServiceCategory": {
      "handler": categoryEvent.getPopularServiceCategory,
    },
    "category.getServieCategory": {
      "handler": categoryEvent.getServieCategory,
    },

    // third party booking service
    "category.getCategoriesForThirdPartyBookingService": {
      "handler": categoryEvent.getCategoriesForThirdPartyBookingService,
    },

    // RIDE
    // RENTAL
    // CATEGORIES
    "category.getBookingLocationCategories": {
      "params": {
        "pickupLat": {
          "type": "number",
        },
        "pickupLng": {
          "type": "number",
        },
        "rideType": {
          "type": "string",
          "uppercase": true,
        },
        "user": {
          "type": "object",
        },
      },
      "handler": categoryEvent.getBookingLocationCategories,
    },
    "category.updateServiceArea": {
      "handler": categoryEvent.updateServiceArea,
    },
  },
  "methods": {
    "getRentalLocationCategories": bookingMethods.getRentalLocationCategories,
    "getNearByOneServiceLocation": bookingMethods.getNearByOneServiceLocation,
  },
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
