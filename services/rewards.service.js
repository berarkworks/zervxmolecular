"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
// const rewardsValidation = require("../validations/rewards.validation");
const rewardsAction = require("../actions/rewards.action");
const rewardsEvent = require("../events/rewards.event");

module.exports = {
  "name": "rewards",
  "mixins": [DBMixin(constantUtil.DBSCHEMAREWARDS)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "getRewardsListByUserType": {
      "auth": constantUtil.REQUIRED,
      // "access": [
      //   constantUtil.USER,
      //   constantUtil.PROFESSIONAL,
      //   constantUtil.ADMIN,
      // ],
      "params": {
        "skip": { "type": "number", "convert": true },
        "limit": { "type": "number", "convert": true },
        "phoneNumber": { "type": "string", "convert": true },
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
      },
      "handler": rewardsAction.getRewardsListByUserType,
    },
    "getRewardsForRideByUserType": {
      "auth": constantUtil.REQUIRED,
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "rideId": { "type": "string", "min": 12 },
        "lng": { "type": "number", "convert": true },
        "lat": { "type": "number", "convert": true },
      },
      "handler": rewardsAction.getRewardsForRideByUserType,
    },
    "scratchRewardsById": {
      "auth": constantUtil.REQUIRED,
      // "access": [
      //   constantUtil.USER,
      //   constantUtil.PROFESSIONAL,
      //   constantUtil.ADMIN,
      // ],
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "rewardId": { "type": "string", "min": 12 },
      },
      "handler": rewardsAction.scratchRewardsById,
    },
    "getRewordHistory": {
      "auth": constantUtil.REQUIRED,
      "params": {
        // "userType": {
        //   "type": "enum",
        //   "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        // },
        // "rewardId": { "type": "string", "min": 12 },
        // "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": rewardsAction.getRewordHistory,
    },
    "getRewordHistoryPopUp": {
      "auth": constantUtil.REQUIRED,
      "params": {
        "id": { "type": "string", "min": 12 },
      },
      "handler": rewardsAction.getRewordHistoryPopUp,
    },
    "getRewardHistoryByUserId": {
      "auth": constantUtil.REQUIRED,
      "params": {
        "userId": { "type": "string", "min": 12 },
      },
      "handler": rewardsAction.getRewardHistoryByUserId,
    },
  },
  "events": {
    "rewards.getRewardListCountByUserType": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": rewardsEvent.getRewardListCountByUserType,
    },
    "rewards.insertRewardByUserType": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "couponId": { "type": "string", "min": 12 },
        "rideId": { "type": "string", "min": 12 },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": rewardsEvent.insertRewardByUserType,
    },
    "rewards.insertRewardByUserTypeFromAdmin": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "createdId": { "type": "string", "min": 12 },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": rewardsEvent.insertRewardByUserTypeFromAdmin,
    },
    "rewards.getRewardsForRideByUserType": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "rideId": { "type": "string", "min": 12 },
        "lng": { "type": "number", "convert": true },
        "lat": { "type": "number", "convert": true },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
        "rewardType": {
          "type": "enum",
          "values": [constantUtil.POINTS, constantUtil.PARTNERDEALS],
        },
      },
      "handler": rewardsEvent.getRewardsForRideByUserType,
    },
    "rewards.scratchRewardsById": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "rewardId": { "type": "string", "min": 12 },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": rewardsEvent.scratchRewardsById,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
