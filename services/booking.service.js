"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const bookingMethod = require("../methods/booking.method");
const bookingEvent = require("../events/booking.event");
const bookingAction = require("../actions/booking.action");
const bookingValidation = require("../validations/booking.validation");
const smsAndCallUtil = require("../utils/smsAndCall.util");

module.exports = {
  "name": "booking",
  "mixins": [DBMixin(constantUtil.DBSCHEMABOOKINGS)],
  "professionalModel": DBMixin(constantUtil.DBSCHEMAPROFESSIONAL),
  "settings": {},
  "dependencies": [],
  "actions": {
    "userGetPaymentOptions": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.userGetPaymentOptions,
      "handler": bookingAction.userGetPaymentOptions,
    },
    "userRideBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.userRideBooking,
      "handler": bookingAction.userRideBooking,
    },
    "userRetryBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.ADMIN],
      "params": bookingValidation.userRetryBooking,
      "handler": bookingAction.userRetryBooking,
    },
    "expiredRetryBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.expiredRetryBooking,
      "handler": bookingAction.expiredRetryBooking,
    },
    "trackBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.trackBooking,
      "handler": bookingAction.trackBooking,
    },
    "bookingExpiryOrCancel": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.bookingExpiryOrCancel,
      "handler": bookingAction.bookingExpiryOrCancel2,
    },
    "getCancellationReason": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.getCancellationReason,
      "handler": bookingAction.getCancellationReason,
    },
    "cancelBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.cancelBooking,
      "handler": bookingAction.cancelBooking,
    },
    "professionalAcceptBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.professionalAcceptBooking,
      "handler": bookingAction.professionalAcceptBooking,
    },
    "updateProfessionalBookingStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.updateProfessionalBookingStatus,
      "handler": bookingAction.updateProfessionalBookingStatus,
    },
    "updatedProfessionalBookingStopStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.updatedProfessionalBookingStopStatus,
      "handler": bookingAction.updatedProfessionalBookingStopStatus,
    },
    "submitRating": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.submitRating,
      "handler": bookingAction.submitRating,
    },
    "professionalGetScheduleList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.professionalGetScheduleList,
      "handler": bookingAction.professionalGetScheduleList,
    },
    "professionalAddWishList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.professionalAddWishList,
      "handler": bookingAction.professionalAddWishList,
    },
    "professionalGetWishList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.professionalGetWishList,
      "handler": bookingAction.professionalGetWishList,
    },
    "userGetScheduleList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.userGetScheduleList,
      "handler": bookingAction.userGetScheduleList,
    },
    "getOrderAndBookingHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.getOrderAndBookingHistory,
      "handler": bookingAction.getOrderAndBookingHistory,
    },
    "changeRideLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.changeRideLocation,
      "handler": bookingAction.changeRideLocation,
    },
    "getBookingDetail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": bookingAction.getBookingDetail,
    },
    "getBookingDetailById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": bookingAction.getBookingDetailById,
    },
    "trackBookingDetailsByIdFromWebBookingApp": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": bookingAction.trackBookingDetailsByIdFromWebBookingApp,
    },
    "trackBookingDetailsByIdForDebug": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": bookingAction.trackBookingDetailsByIdForDebug,
    },
    "trackRideCoordinatesUsingRideId": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "rideId": { "type": "string", "min": 10 },
        "action": {
          "type": "enum",
          "values": [constantUtil.CONST_GET, constantUtil.CONST_TRACK],
          "default": constantUtil.CONST_TRACK,
        },
      },
      "handler": bookingAction.trackRideCoordinatesUsingRideId,
    },
    // TRUSTED CONTACTS
    "getTrustedContactsReasons": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": bookingAction.getTrustedContactsReasons,
    },
    "addAndRemoveTrustedContacts": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.addAndRemoveTrustedContacts,
      "handler": bookingAction.addAndRemoveTrustedContacts,
    },
    "getTrustedContactsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": bookingAction.getTrustedContactsList,
    },
    "shareTripToTrustedContact": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.shareTripToTrustedContact,
      "handler": bookingAction.shareTripToTrustedContact,
    },
    // VIEW BOOKING RECIPT
    "viewRecipt": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": { "bookingId": { "type": "string" } },
      "handler": bookingAction.viewRecipt,
    },
    // SCHEDULE BOOKING CATEGORY CHANGE
    "scheduleBookingCategoryChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": { "bookingId": { "type": "string", "min": "1" } },
      "handler": bookingAction.scheduleBookingCategoryChange,
    },
    // DASHBOARD
    "getCityBasedRidesData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getCityBasedRidesData,
      "handler": bookingAction.getCityBasedRidesData,
    },
    "getGraphBookingData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getGraphBookingData,
      "handler": bookingAction.getGraphBookingData,
    },
    "getAvailableProfessionals": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getAvailableProfessionals,
      "handler": bookingAction.getAvailableProfessionals,
    },
    "getAvailableProfessionalsForGuestUser": {
      // "access": [constantUtil.GUESTUSER],
      "params": bookingValidation.getAvailableProfessionals,
      "handler": bookingAction.getAvailableProfessionals,
    },
    "getOnlineProfessionals": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.getOnlineProfessionals,
      "handler": bookingAction.getOnlineProfessionals,
    },
    "newRideBookingFromAdmin": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.newRideBookingFromAdmin,
      "handler": bookingAction.newRideBookingFromAdmin,
    },
    "getOperatorBasedBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getOperatorBasedBooking,
      "handler": bookingAction.getOperatorBasedBooking,
    },
    "getOperatorBasedCategoryBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getOperatorBasedCategoryBooking,
      "handler": bookingAction.getOperatorBasedCategoryBooking,
    },
    "getTotalFareData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getTotalFareData,
      "handler": bookingAction.getTotalFareData,
    },
    "bookingStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.bookingStatusChange,
      "handler": bookingAction.bookingStatusChange,
    },
    "bookingStatusChangeEnded": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.bookingStatusChangeEnded,
      "handler": bookingAction.bookingStatusChangeEnded,
    },
    "adminCancelBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.adminCancelBooking,
      "handler": bookingAction.adminCancelBooking,
    },
    "getSiteEarnings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getSiteEarnings,
      "handler": bookingAction.getSiteEarnings,
    },
    "getSiteEarningsSummary": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getSiteEarningsSummary,
      "handler": bookingAction.getSiteEarningsSummary,
    },
    "adminBookingExpiryOrCancel": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.adminBookingExpiryOrCancel,
      "handler": bookingAction.adminBookingExpiryOrCancel,
    },
    "giveTips": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.giveTips,
      "handler": bookingAction.giveTips,
    },
    "getCommonConfig": {
      "auth": constantUtil.NOTREQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": bookingAction.getCommonConfig,
    },
    "getDriversBillingEarnings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.getDriversBillingEarnings,
      "handler": bookingAction.getDriversBillingEarnings,
    },
    "getProfessionalInstantPaymentEarnings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.getProfessionalInstantPaymentEarnings,
      "handler": bookingAction.getProfessionalInstantPaymentEarnings,
    },
    "getDayEarnings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.getDayEarnings,
      "handler": bookingAction.getDayEarnings,
    },
    "getProfessionalDashboard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "handler": bookingAction.getProfessionalDashboard,
    },
    "adminRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.adminRideList,
      "handler": bookingAction.adminRideList,
    },
    "cooprateRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.cooprateRideList,
      "handler": bookingAction.cooprateRideList,
    },
    "getUserRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getUserRideList,
      "handler": bookingAction.getUserRideList,
    },
    "getUserRideReportList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getUserRideReportList,
      "handler": bookingAction.getUserRideReportList,
    },
    "getProfessionalRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getProfessionalRideList,
      "handler": bookingAction.getProfessionalRideList,
    },
    "getProfessionalRideReportList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getProfessionalRideReportList,
      "handler": bookingAction.getProfessionalRideReportList,
    },

    "getAppUsageList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getAppUsageList,
      "handler": bookingAction.getAppUsageList,
    },
    "getProfessionalCommissionReport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": bookingValidation.getProfessionalCommissionReport,
      "handler": bookingAction.getProfessionalCommissionReport,
    },
    "getProfessionalRankingReport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": bookingValidation.getProfessionalRankingReport,
      "handler": bookingAction.getProfessionalRankingReport,
    },
    "getRideReviewRatingReport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getRideReviewRatingReport,
    },
    "downloadRideHistoryData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.downloadRideHistoryData,
    },
    "getRidesReport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": bookingValidation.getRideReport,
      "handler": bookingAction.getRidesReport,
    },

    "getHubsRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getHubsRideList,
      "handler": bookingAction.getHubsRideList,
    },
    "getCouponBasedRideList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getCouponBasedRideList,
      "handler": bookingAction.getCouponBasedRideList,
    },
    "manualMeterBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.manualMeterBooking,
      "handler": bookingAction.manualMeterBooking,
    },
    "updateTripWayData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.updateTripWayData,
      "handler": bookingAction.updateTripWayData,
    },
    "coorperateBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN, constantUtil.USER],
      "params": bookingValidation.coorperateBooking,
      "handler": bookingAction.coorperateBooking,
    },
    "updateWaitingTimes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.updateWaitingTimes,
      "handler": bookingAction.updateWaitingTimes,
    },
    "getFareCalculation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getFareCalculation,
    },
    "quickRideBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.quickRideBooking,
      "handler": bookingAction.quickRideBooking,
    },
    //  booking track using a link
    "trackBookingUsingLink": {
      "params": {
        "trackId": { "type": "string", "min": 2 },
      },
      "handler": bookingAction.trackBookingUsingLink,
    },
    "trackCurrentLocationOfRide": {
      "params": {
        "trackId": { "type": "string", "min": 2 },
      },
      "handler": bookingAction.trackCurrentLocationOfRide,
    },
    "professionalSelectAndRideBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": bookingValidation.professionalSelectAndRideBooking,
      "handler": bookingAction.professionalSelectAndRideBooking,
    },
    "makeCallUsingCallMasking": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": bookingValidation.makeCallUsingCallMasking,
      "handler": bookingAction.makeCallUsingCallMasking,
    },
    "uploadSecurityImage": {
      "auth": constantUtil.REQUIRED,
      "handler": bookingAction.uploadSecurityImage,
    },
    "uploadSingleImage": {
      "auth": constantUtil.REQUIRED,
      "params": {
        "action": {
          "type": "enum",
          "values": [constantUtil.ADD, constantUtil.REMOVE],
        },
        "userType": {
          "type": "enum",
          "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
        },
        "serviceCategory": {
          "type": "enum",
          "lowercase": true, // Must be lowercase
          "convert": true,
          "values": [
            constantUtil.CONST_RIDE.toLowerCase(), // Must Lowercase Type
            constantUtil.CONST_PACKAGES.toLowerCase(), // Must Lowercase Type
          ],
          "default": constantUtil.CONST_RIDE.toLowerCase(), // Must Lowercase Type
        },
        "requestFrom": {
          "type": "enum",
          "values": [constantUtil.BOOKING, constantUtil.CONST_PACKAGES],
        },
        "oldFileName": { "type": "string", "optional": true },
      },
      "handler": bookingAction.uploadSingleImage,
    },
    "rideLiveMeter": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN, constantUtil.PROFESSIONAL],
      // "params": bookingValidation.rideLiveMeter,
      "handler": bookingAction.rideLiveMeter,
    },
    //#region Dashboar

    // Ride Graphs..........

    "getAdminDashboardAllRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": bookingValidation.getAdminDashboardAllRides, // getAdminDashboardAllRides
      "handler": bookingAction.getAdminDashboardAllRides,
    },
    "getAllRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getAllRides,
    },
    "getIncomeExpenceRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getIncomeExpenceRides,
    },
    "getProfessionalsRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getProfessionalsRides,
    },
    "getUsersRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getUsersRides,
    },
    "getCorporateRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getCorporateRides,
    },
    "getAdminRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getAdminRides,
    },

    // Admin..............
    "getTopDashboardAdminRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopDashboardAdminRides,
    },

    // "getTopDashboardAdminRides": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "handler": bookingAction.getTopDashboardAdminRides,
    // },

    "getTopDashboardAdminCategoryRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopDashboardAdminCategoryRides,
    },

    // Professionals........

    "getTopProfessionalsRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopProfessionalsRides,
    },

    "getTopProfessionalsVechileCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopProfessionalsVechileCategory,
    },
    "getTopProfessionalsCategoryRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopProfessionalsCategoryRides,
    },
    // Users...........

    "getTopUsersRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopUsersRides,
    },

    "getTopUsersCategoryRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopUsersCategoryRides,
    },

    // Corporate User.........

    "getTopDashboardCorporateUserRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopDashboardCorporateUserRides,
    },

    "getTopDashboardCorporateUserCategoryRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopDashboardCorporateUserCategoryRides,
    },

    // Corporate Officer......
    "getTopDashboardCorporateOfficerRides": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getTopDashboardCorporateOfficerRides,
    },

    //#endregion Dashboard

    //#region start PopUp  ............

    // Admin.........
    "getPopUpAdminRidesDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpAdminRidesDeatils,
    },
    "getPopUpAdminDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpAdminDeatils,
    },
    "getPopUpAdminGraph": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpAdminGraph,
    },
    // Professionals popup........
    "getPopUpProfessionalRidesDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpProfessionalRidesDeatils,
    },
    "getPopUpProfessionalDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpProfessionalDeatils,
    },
    "getPopUpProfessionalGraph": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpProfessionalGraph,
    },

    // Users popup...........
    "getPopUpUsersRidesDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpUsersRidesDeatils,
    },
    "getPopUpUsersDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpUsersDeatils,
    },
    "getPopUpUsersGraph": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpUsersGraph,
    },
    // Corporate User popup.................
    "getPopUpCorporateUsersRidesDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateUsersRidesDeatils,
    },
    "getPopUpCorporateUsersDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateUsersDeatils,
    },
    "getPopUpCorporateUsersGraph": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateUsersGraph,
    },
    // Corporate Officer popup...........,
    "getPopUpCorporateOfficersRidesDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateOfficersRidesDeatils,
    },
    "getPopUpCorporateOfficersDeatils": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateOfficersDeatils,
    },
    "getPopUpCorporateOfficersGraph": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.getPopUpCorporateOfficersGraph,
    },
    //#endregion popup.......

    "trackProfessionalOngoingRide": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "userId": { "type": "string", "optional": true, "convert": true },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
        "bookingId": { "type": "string", "convert": true, "min": 12 },
      },
      "handler": bookingAction.trackProfessionalOngoingRide,
    },
    "radiusBasedGetData": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": bookingAction.radiusBasedGetData,
    },
    "totalRideReportData": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": bookingAction.totalRideReportData,
    },
    "changeBookingDate": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": bookingAction.changeBookingDate,
    },
    "sendPromotion": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": bookingValidation.sendPromotion,
      "handler": bookingAction.sendPromotion,
    },
  },
  "events": {
    "booking.notifyChecklistEnabledProfessional": {
      "handler": bookingEvent.notifyChecklistEnabledProfessional,
    },
    "booking.notifyScheduleRideToAdmin": {
      "handler": bookingEvent.notifyScheduleRideToAdmin,
    },

    // ADMIN RIDE MODULES API
    "booking.viewRideDetails": {
      "handler": bookingEvent.viewRideDetails,
    },
    "booking.adminBookingTimeOut": {
      "handler": bookingEvent.adminBookingTimeOut,
    },
    "booking.updateFCMAccessToken": {
      "handler": bookingEvent.updateFCMAccessToken,
    },
    "booking.getNewRidesList": {
      "handler": bookingEvent.getNewRidesList,
    },
    "booking.getGuestRidesList": {
      "handler": bookingEvent.getGuestRidesList,
    },
    "booking.getManualMeterRidesList": {
      "handler": bookingEvent.getManualMeterRidesList,
    },
    "booking.getCoorperateRidesList": {
      "handler": bookingEvent.getCoorperateRidesList,
    },
    "booking.getScheduleRidesList": {
      "handler": bookingEvent.getScheduleRidesList,
    },
    "booking.getOngoingRidesList": {
      "handler": bookingEvent.getOngoingRidesList,
    },
    "booking.getEndedRidesList": {
      "handler": bookingEvent.getEndedRidesList,
    },
    "booking.getExpiredRidesList": {
      "handler": bookingEvent.getExpiredRidesList,
    },
    "booking.getUserCancelledRidesList": {
      "handler": bookingEvent.getUserCancelledRidesList,
    },
    "booking.getProfessionalCancelledRidesList": {
      "handler": bookingEvent.getProfessionalCancelledRidesList,
    },
    "booking.paymentFailedIssueList": {
      "handler": bookingEvent.paymentFailedIssueList,
    },
    "booking.getSearchRidesList": {
      "handler": bookingEvent.getSearchRidesList,
    },
    "booking.getAssignProfessionalList": {
      "handler": bookingEvent.getAssignProfessionalList,
    },
    "booking.sendRequstToProfessional": {
      "handler": bookingEvent.sendRequstToProfessional,
    },
    "booking.updateSecurityInfo": {
      "params": {
        "bookingId": { "type": "string", "min": 1 },
        "securityId": { "type": "string", "min": 1 },
      },
      "handler": bookingEvent.updateSecurityInfo,
    },
    "booking.updateOfficerInfo": {
      "params": {
        "bookingId": { "type": "string", "min": 1 },
        "officer": { "type": "string", "min": 1 },
      },
      "handler": bookingEvent.updateOfficerInfo,
    },
    "booking.getAllRidesList": {
      "handler": bookingEvent.getAllRidesList,
    },
    "booking.bookingTimeOut": {
      "handler": bookingEvent.bookingTimeOut,
    },
    "booking.getUserRideDetails": {
      "params": {
        "userId": { "type": "string", "min": 1 },
      },
      "handler": bookingEvent.getUserRideDetails,
    },
    "booking.getProfessionalRideDetails": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
      },
      "handler": bookingEvent.getProfessionalRideDetails,
    },
    "booking.getUserRatingList": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "skip": { "type": "number", "convert": true },
        "limit": { "type": "number", "convert": true },
      },
      "handler": bookingEvent.getUserRatingList,
    },
    "booking.getProfessionalRatingList": {
      "params": {
        "professionalId": { "type": "string", "min": 1 },
        "skip": { "type": "number", "convert": true },
        "limit": { "type": "number", "convert": true },
      },
      "handler": bookingEvent.getProfessionalRatingList,
    },
    "booking.getAllBillingBookings": {
      "handler": bookingEvent.getAllBillingBookings,
    },
    "booking.getAllCoorperateBillingBookings": {
      "handler": bookingEvent.getAllCoorperateBillingBookings,
    },
    "booking.updateBillingId": {
      "handler": bookingEvent.updateBillingId,
    },
    "booking.getDriverBillingList": {
      "handler": bookingEvent.getDriverBillingList,
    },
    "booking.getNotifyRidesList": {
      "handler": bookingEvent.getNotifyRidesList,
    },
    "booking.professionalCancelRideList": {
      "handler": bookingEvent.professionalCancelRideList,
    },
    "booking.generateInvoiceMail": {
      "handler": bookingEvent.generateInvoiceMail,
    },
    "booking.checkBookingCountforCoupon": {
      "handler": bookingEvent.checkBookingCountforCoupon,
    },
    // cancel booking helping events
    "booking.adminCannotAssignProfessionalCanceledRide": {
      "params": bookingValidation.adminCannotAssignProfessionalCanceledRide,
      "handler": bookingEvent.adminCannotAssignProfessionalCanceledRide,
    },
    "booking.nextProfessionalBuffer": {
      //=> this api is used for notify any professional not accept ride when cancel booking
      "params": {
        "professionalId": { "type": "string", "min": 1, "convert": true },
        "bookingId": { "type": "string", "min": 1, "convert": true },
        "cancelledTime": { "type": "date", "convert": true },
        "cancellationReason": { "type": "string" },
        "cancellationAmount": { "type": "number", "convert": true },
        "cancellationPenalityStatus": { "type": "boolean", "convert": true },
        "bookingDate": { "type": "date", "convert": true },
      },
      "handler": bookingEvent.nextProfessionalBuffer,
    },
    "booking.getBookingDetailsById": {
      "params": {
        "bookingId": { "type": "string", "min": 1, "convert": true },
      },
      "handler": bookingEvent.getBookingDetailsById,
    },
    // THIRD PARTY BOOKING SERVICE

    "booking.newRideBookingForThirdParty": {
      "handler": bookingEvent.newRideBookingForThirdParty,
    },

    // request send to professional event
    "booking.sendBookingRequestToProfessionals": {
      "params": {
        "bookingId": { "type": "string", "min": 12 },
      },
      "handler": bookingEvent.sendBookingRequestToProfessionals,
    },
    //  send  SMS Alert
    "booking.sendSMSAlertByRideId": {
      "params": {
        "rideId": { "type": "string", "min": 12 },
        "bookingFrom": {
          "type": "enum",
          "values": [
            constantUtil.CONST_WEBAPP,
            constantUtil.CONST_APP,
            constantUtil.ADMIN,
            constantUtil.COORPERATEOFFICE,
          ],
        },
        "notifyType": {
          "type": "enum",
          "values": [
            constantUtil.ACCEPTED,
            constantUtil.ARRIVED,
            constantUtil.STARTED,
            constantUtil.ENDED,
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.FORCEASSIGNBYADMIN,
          ],
        },
        "notifyFor": {
          "type": "enum",
          "values": [
            constantUtil.USER,
            constantUtil.PROFESSIONAL,
            constantUtil.PACKAGERECEIVER,
          ],
        },
      },
      "handler": smsAndCallUtil.sendSMSAlertByRideId,
    },
    "booking.sendSMSAlertForPromotions": {
      "params": {
        "notifyFor": {
          "type": "enum",
          "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
        },
        "phoneNumber": { "type": "string" },
        "promotionMessage": { "type": "string" },
      },
      "handler": smsAndCallUtil.sendSMSAlertForPromotions,
    },
    // for external
    "booking.getAllRideDetailsExternal": {
      "handler": bookingEvent.getAllRideDetailsExternal,
    },
    "booking.updateUpcommingScheduledRideInRedis": {
      "handler": bookingEvent.updateUpcommingScheduledRideInRedis,
    },
    "booking.bookingDataMigrationScript": {
      "handler": bookingEvent.bookingDataMigrationScript,
    },
    "booking.getOnlineProfessionals": {
      // "params": bookingValidation.getOnlineProfessionals,
      "handler": bookingEvent.getOnlineProfessionals,
    },
    "booking.updatePackageDetailsById": {
      "handler": bookingEvent.updatePackageDetailsById,
    },
    "booking.checkAndUpdateOngoingShareRide": {
      "params": {
        "actionStatus": {
          "type": "enum",
          "values": [
            constantUtil.ACCEPTED,
            constantUtil.CONST_PICKUP,
            constantUtil.STARTED,
            constantUtil.CONST_DROP,
            constantUtil.STATUS_UPCOMING,
          ],
          "default": constantUtil.CONST_PICKUP,
        },
        "professionalId": { "type": "string", "convert": true },
        "bookingId": { "type": "string", "convert": true, "optional": true },
        "lastRidePassengerCount": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "parentShareRideId": {
          "type": "string",
          "convert": true,
          "optional": true,
        },
        // "passengerCount": { "type": "number", "convert": true, "default": 0 },
      },
      "handler": bookingEvent.checkAndUpdateOngoingShareRide,
    },
    "booking.sendNotificationActionShareRideStatus": {
      "params": {
        "parentShareRideId": {
          "type": "string",
          "convert": true,
          "optional": true,
        },
        "bookingId": { "type": "string", "convert": true, "optional": true },
        "userType": {
          "type": "enum",
          "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
        },
        "actionStatus": {
          "type": "enum",
          "values": [
            constantUtil.ACCEPTED,
            constantUtil.ARRIVED,
            constantUtil.STARTED,
            constantUtil.ENDED,
            constantUtil.USERCANCELLED,
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.ACTION_CANCELBOOKING,
          ],
        },
        "notificationMessage": {
          "type": "string",
          "convert": true,
        },
      },
      "handler": bookingEvent.sendNotificationActionShareRideStatus,
    },
    "booking.getRideListByProfessionalId": {
      "params": {
        "professionalId": { "type": "string", "min": 12 },
        "minimumReviewRating": { "type": "number", "convert": true },
        "startDate": { "type": "date", "convert": true },
        "endDate": { "type": "date", "convert": true },
      },
      "handler": bookingEvent.getRideListByProfessionalId,
    },
    "booking.getRideListByUserId": {
      "params": {
        "userId": { "type": "string", "min": 12 },
        "minimumReviewRating": { "type": "number", "convert": true },
        "startDate": { "type": "date", "convert": true },
        "endDate": { "type": "date", "convert": true },
      },
      "handler": bookingEvent.getRideListByUserId,
    },
    "booking.getProfessionalRankingReport": {
      // "params": bookingValidation.getProfessionalRankingReport,
      "handler": bookingEvent.getProfessionalRankingReport,
    },
    "booking.getRideReviewRatingReport": {
      "handler": bookingEvent.getRideReviewRatingReport,
    },
    "booking.rideBookingDetails": {
      "handler": bookingEvent.rideBookingDetails,
    },
  },
  "methods": {
    "getProfessionalByLocation": bookingMethod.getProfessionalByLocation,
    // "checkProfessionalDenyList": bookingMethod.checkProfessionalDenyList,
    "checkAndRemoveProfessionalAndUserDenyAndBlockList":
      bookingMethod.checkAndRemoveProfessionalAndUserDenyAndBlockList,
    "getBookingDetailsById": bookingMethod.getBookingDetailsById,
    "findCurrentShareRideProfessional":
      bookingMethod.findCurrentShareRideProfessional,
  },
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
