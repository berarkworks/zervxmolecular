"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const logValidation = require("../validations/log.validation");
const logAction = require("../actions/log.action");
const logEvent = require("../events/log.event");

module.exports = {
  "name": "log",
  "mixins": [DBMixin(constantUtil.DBSCHEMALOG)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "getNotificationBasedOnUserType": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      // "params": logValidation.getNotificationBasedOnUserType,
      "handler": logAction.getNotificationBasedOnUserType,
    },
    "manageLog": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": logValidation.manageLog,
      "handler": logAction.manageLog,
    },
    "errorlogReportData": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": logAction.errorlogReportData,
    },
    "getLoginHistoryByloginId": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": logValidation.getLoginHistoryByloginId,
      "handler": logAction.getLoginHistoryByloginId,
    },
  },
  "events": {
    "log.getNotificationBasedOnUserType": {
      "handler": logEvent.getNotificationBasedOnUserType,
    },
    "log.insertSignupLog": {
      "handler": logEvent.insertSignupLog,
    },
    "log.updateSignupLog": {
      "handler": logEvent.updateSignupLog,
    },
    "log.updateSignupLogByCronjob": {
      "handler": logEvent.updateSignupLogByCronjob,
    },
    "log.manageLog": {
      "handler": logEvent.manageLog,
    },
    "log.calculateOnlineMinutes": {
      "params": {
        "userType": {
          "type": "enum",
          "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
        },
        "professionalId": {
          "type": "string",
          "convert": true,
          "optional": true,
        },
        "userId": { "type": "string", "convert": true, "optional": true },
      },
      "handler": logEvent.calculateOnlineMinutes,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
