const { MoleculerError } = require("moleculer").Errors;

const SocketIO = require("../mixins/socket.mixin");

const storageUtil = require("../utils/storage.util");
const constantUtil = require("../utils/constant.util");
const cryptoUtils = require("../utils/crypto.util");
const notifyMessage = require("../mixins/notifyMessage.mixin");

module.exports = {
  "name": "socketIO",
  "mixins": [SocketIO],
  "settings": {
    "port": process.env.SOCKET_PORT ? process.env.SOCKET_PORT : 5000,
    "options": {
      //	Socket.io options
      "transports": ["websocket"],
      // 'transports': ['polling'],
      "cors": true,
      "origins": ["*"],
      "cookie": false,
    },
  },
  "actions": {},
  "namespaces": {
    "/user": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [constantUtil.USER]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit("connection", "CONNECTED TO /user NAMESPACE");
          }
        },
      },
      "getNearbyDrivers": {
        "params": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
        "handler": async function (context) {
          const clientId = context.params.clientId || context.meta?.clientId;
          const generalSettings = await storageUtil.read(
            constantUtil.GENERALSETTING
          );
          const radius =
            parseInt(generalSettings.data?.requestDistance || 100) +
            parseInt(generalSettings.data?.retryRequestDistance || 100);
          const professionalByLocation = await this.broker.emit(
            "professional.getProfessionalByLocation",
            {
              ...context.params,
              //"clientId": clientId,
              "vehicleCategoryId": null,
              "professionalOnlineStatus": true,
              "actionType": constantUtil.CONST_ONLINE,
              "rideType": context.params?.rideType,
              "radius": context.params?.radius || radius,
              //"maxRadius": context.params.maxRadius,
              "checkOngoingBooking": false,
              "checkLastPriority": false,
              "forTailRide": generalSettings.data?.isTailRideNeeded || false,
              "isSubCategoryAvailable": true,
              "isForceAppliedToProfessional": true,
              "isGenderAvailable": false,
              "childseatAvailable": false,
              "handicapAvailable": false,
              "isRestrictMaleBookingToFemaleProfessional": false,
              "isRentalSupported": false,
              "serviceAreaId": context.params?.serviceAreaId || null,
              "isShareRide": context.params?.isShareRide || null, // null --> Default
              "paymentOption": context.params?.paymentOption || null, // null --> Default For get All PaymentMode Professional
              "languageCode": context.params?.languageCode || null, // null --> Default For get All language Known Professional
              "isPetAllowed": context.params?.isPetAllowed || false,
              "isEnableLuggage": context.params?.isEnableLuggage || false,
            }
          );
          if (professionalByLocation.length === 0) {
            context.socket.emit("getNearbyDrivers", null);
            return;
          }

          const vehicleCategoryJson = await storageUtil.read(
            constantUtil.VEHICLECATEGORY
          );
          if (professionalByLocation && professionalByLocation.length > 0) {
            const professionalJSON = professionalByLocation[0]?.map(
              (professional) => {
                const defaultVehicle = professional.vehicles.filter(
                  (vehicle) => {
                    vehicle = JSON.parse(JSON.stringify(vehicle));
                    return (
                      vehicle.defaultVehicle === true &&
                      vehicle.status === constantUtil.ACTIVE
                    );
                  }
                );
                if (
                  defaultVehicle[0]?.vehicleCategoryId &&
                  parseFloat(professional.wallet.availableAmount) >=
                    parseFloat(
                      generalSettings.data?.minimumWalletAmountToOnline
                    )
                ) {
                  const vehicalCategoryMetaData =
                    vehicleCategoryJson[defaultVehicle[0].vehicleCategoryId];
                  return {
                    "currentBearing": professional.currentBearing,
                    "driverId": professional._id,
                    "gender": professional.gender,
                    "serviceCategory":
                      professional?.bookingInfo?.ongoingBooking
                        ?.serviceCategory || null,
                    "ongoingBooking":
                      professional?.bookingInfo?.ongoingBooking?._id || null,
                    "vehcileCategoryId": defaultVehicle[0].vehicleCategoryId,
                    "isSubCategoryAvailable":
                      defaultVehicle[0].isSubCategoryAvailable,
                    "applySubCategory": defaultVehicle[0].applySubCategory,
                    "subCategoryIds": defaultVehicle[0].subCategoryIds,
                    "isGenderAvailable": defaultVehicle[0].isGenderAvailable,
                    "handicapAvailable": defaultVehicle[0].handicapAvailable,
                    "childseatAvailable": defaultVehicle[0].childseatAvailable,
                    "isCompanyVehicle": defaultVehicle[0].isCompanyVehicle,
                    "isPetAllowed": defaultVehicle[0].isPetAllowed,

                    "isHaveShare":
                      vehicalCategoryMetaData?.mandatoryShare || false,
                    "isTailRideBooking":
                      professional?.isTailRideBooking || false,
                    "vehicleCategoryMapImage":
                      vehicalCategoryMetaData?.categoryMapImage || "",
                    "lat": professional.location.coordinates[1],
                    "lng": professional.location.coordinates[0],
                    "serviceType": vehicalCategoryMetaData?.vehicleCategory,
                    "fullName":
                      professional?.firstName ??
                      "" + " " + professional?.lastName ??
                      "",
                    "avatar": professional?.avatar ?? "",
                    "reviewRating": professional?.review?.avgRating ?? 0,
                    "paymentMode": professional?.paymentMode || [],
                  };
                }
              }
            );
            return professionalJSON;
          }
        },
      },
      "getLocationBasedCategory": {
        "params": {
          "userType": { "type": "string", "optional": true },
          "pickupLat": { "type": "number", "convert": true },
          "pickupLng": { "type": "number", "convert": true },
          "dropLat": { "type": "number", "convert": true },
          "dropLng": { "type": "number", "convert": true },
          "estimationDistance": { "type": "number", "convert": true },
          "estimationTime": { "type": "number", "convert": true },
          "categoryName": { "type": "string" },
        },
        "handler": async function (context) {
          const categoryData = await this.broker.call(
            "category.userGetLocationBasedCategory",
            context.params
          );
          return categoryData;
        },
      },
      "updateLiveLocation": {
        "params": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
          "currentBearing": { "type": "number", "convert": true },
          "speed": { "type": "number", "convert": true },
          "verticalAccuracy": { "type": "number", "convert": true },
          "horizontalAccuracy": { "type": "number", "convert": true },
          "altitude": { "type": "number", "convert": true },
        },
        "handler": async function (context) {
          const data = await this.broker.emit("user.updateLiveLocation", {
            ...context.params,
            "userId": context.socket.userId,
          });
          return data[0];
        },
      },
      "disconnect": {
        "handler": async function () {
          this.logger.info("DISCONNECTED FROM /user NAMESPACE");
        },
      },
    },
    "/professional": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [
              constantUtil.PROFESSIONAL,
            ]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit(
              "connection",
              "CONNECTED TO /professional NAMESPACE"
            );
          }
        },
      },
      "updateLiveLocation": {
        "params": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
          "currentBearing": { "type": "number", "convert": true },
          "speed": { "type": "number", "convert": true },
          "verticalAccuracy": { "type": "number", "convert": true },
          "horizontalAccuracy": { "type": "number", "convert": true },
          "altitude": { "type": "number", "convert": true },
          "onlineStatus": {
            "type": "boolean",
            "optional": true,
            "convert": true,
          },
        },
        "handler": async function (context) {
          const data = await this.broker.emit(
            "professional.updateLiveLocation",
            {
              ...context.params,
              "professionalId":
                context.params?.professionalId ||
                context?.socket?.professionalId ||
                context.socket.userId,
            }
          );
          console.log("-------------------------------");
          console.log(JSON.stringify(context.params));
          console.log(
            context.socket.userId +
              "_" +
              context.socket.professionalId +
              "_" +
              context.params?.professionalId
          );
          console.log("-------------------------------");
          console.log(JSON.stringify(data[0]));
          console.log("-------------------------------");
          return data[0];
        },
      },
      "getHeatmapDataByLocation": {
        "params": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
          "serviceAreaId": {
            "type": "string",
            "convert": true,
            "optional": true,
          },
        },
        "handler": async function (context) {
          const responseData = await this.broker.call(
            "heatmap.getHeatmapDataByLocation", // Call the Action method name
            context.params
          );
          return responseData;
        },
      },
      "disconnect": {
        "handler": async function () {
          this.logger.info("DISCONNECTED FROM /professional NAMESPACE");
        },
      },
    },
    "/officer": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [constantUtil.OFFICER]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit(
              "connection",
              "CONNECTED TO /officer NAMESPACE"
            );
          }
        },
      },
      "updateLiveLocation": {
        "params": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
          "currentBearing": { "type": "number", "convert": true },
          "speed": { "type": "number", "convert": true },
          "verticalAccuracy": { "type": "number", "convert": true },
          "horizontalAccuracy": { "type": "number", "convert": true },
          "altitude": { "type": "number", "convert": true },
        },
        "handler": async function (context) {
          const data = await this.broker.emit("officer.updateLiveLocation", {
            ...context.params,
            "officerId": context.socket.userId,
          });
          return data[0];
        },
      },
      "disconnect": {
        "handler": async function () {
          // this.logger.info('DISCONNECTED FROM /officer NAMESPACE')
        },
      },
    },
    "/notification": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [
              constantUtil.USER,
              constantUtil.PROFESSIONAL,
              constantUtil.OFFICER,
              constantUtil.ADMIN,
            ]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit(
              "connection",
              "CONNECTED TO /notification NAMESPACE"
            );
          }
        },
      },
      "trackUserLocation": {
        "params": {
          "userId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "user.trackUserLocation",
            context.params
          );
          return locationData;
        },
      },
      "trackProfessionalLocation": {
        "params": {
          "professionalId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "professional.trackProfessionalLocation",
            context.params
          );
          return locationData;
        },
      },
      "trackOfficerLocation": {
        "params": {
          "officerId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "officer.trackOfficerLocation",
            context.params
          );
          return locationData;
        },
      },
      "disconnect": {
        "handler": async function () {
          // this.logger.info('DISCONNECTED FROM /notification NAMESPACE')
        },
      },
    },
    "/webBooking": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [
              constantUtil.USER,
              // constantUtil.PROFESSIONAL,
              // constantUtil.OFFICER,
              constantUtil.ADMIN,
            ]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit(
              "connection",
              "CONNECTED TO /notification NAMESPACE"
            );
          }
        },
      },
      "trackUserLocation": {
        "params": {
          "userId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "user.trackUserLocation",
            context.params
          );
          return locationData;
        },
      },
      "trackProfessionalLocation": {
        "params": {
          "professionalId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "professional.trackProfessionalLocation",
            context.params
          );
          return locationData;
        },
      },
      "trackBookingDetailsByIdFromWebBookingApp": {
        "params": {
          "bookingId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "booking.trackBookingDetailsByIdFromWebBookingApp",
            context.params
          );
          return locationData;
        },
      },
      "disconnect": {
        "handler": async function () {
          // this.logger.info('DISCONNECTED FROM /notification NAMESPACE')
        },
      },
    },
    "/admin": {
      "connection": {
        "middlewares": [
          async function (context) {
            await this.socketAuthentication(context, [constantUtil.ADMIN]);
          },
        ],
        "handler": async function (context) {
          if (context.socket && context.socket.emit) {
            context.socket.emit("connection", "CONNECTED TO /admin NAMESPACE");
          }
        },
      },
      "trackProfessionalLocation": {
        "params": {
          "professionalId": { "type": "string", "min": "1" },
        },
        "handler": async function (context) {
          const locationData = await this.broker.call(
            "professional.trackProfessionalLocation",
            context.params
          );
          return locationData;
        },
      },
      "disconnect": {
        "handler": async function () {
          // this.logger.info('DISCONNECTED FROM /admin NAMESPACE')
        },
      },
    },
  },
  "events": {
    "socket.sendNotification": {
      "params": {
        "temprorySound": { "type": "boolean", "optional": true },
        "clientId": { "type": "string", "optional": true },
        "data": { "type": "object" },
        "registrationTokens": {
          "type": "array",
          "items": {
            "type": "object",
            "strict": true,
            "props": {
              "id": { "type": "string" },
              "token": { "type": "string" },
              "deviceType": { "type": "string" },
              "platform": { "type": "string" },
              "socketId": { "type": "string" },
              "duration": {
                "type": "number",
                "convert": true,
                "default": 0,
                "optional": true,
              },
              "distance": {
                "type": "number",
                "convert": true,
                "default": 0,
                "optional": true,
              },
            },
          },
        },
      },
      "handler": async function (context) {
        context.params.registrationTokens.forEach((data) => {
          // console.log(
          //   'sendNotification',
          //   context.params.data.userType,
          //   context.params.data.action,
          //   data.socketId
          // )
          this.io
            .of("/notification")
            .to(data.socketId)
            // .emit(data.token, context.params.data)
            .emit(data.token, {
              ...context.params.data,
              "duration": data.duration,
              "distance": data.distance,
            });
        });
      },
    },
    "socket.sendAdminNotification": {
      "params": {
        "temprorySound": { "type": "boolean", "optional": true },
        "clientId": { "type": "string", "optional": true },
        "data": { "type": "object" },
        "registrationTokens": {
          "type": "array",
          "items": {
            "type": "object",
            "strict": true,
            "props": {
              "id": { "type": "string", "optional": true },
              "token": { "type": "string", "optional": true },
              "deviceType": { "type": "string", "optional": true },
              "platform": { "type": "string", "optional": true },
              "socketId": { "type": "string", "optional": true },
            },
          },
        },
      },
      "handler": async function (context) {
        context.params.registrationTokens.forEach((data) => {
          this.io.of("/admin").emit(data.token, context.params.data);
        });
      },
    },
    "socket.sendAdminNotificationForRide": {
      "params": {
        "temprorySound": { "type": "boolean", "optional": true },
        "clientId": { "type": "string", "optional": true },
        "data": { "type": "object" },
        "registrationTokens": {
          "type": "array",
          "items": {
            "type": "object",
            "strict": true,
            "props": {
              "id": { "type": "string", "optional": true },
              "token": { "type": "string", "optional": true },
              "deviceType": { "type": "string", "optional": true },
              "platform": { "type": "string", "optional": true },
              "socketId": { "type": "string", "optional": true },
              "userType": { "type": "string", "optional": true },
            },
          },
        },
      },
      "handler": async function (context) {
        context.params.registrationTokens.forEach((data) => {
          this.io.of("/admin").emit(data.userType, context.params.data);
        });
      },
    },
    // "socket.getRideWayDataFromProfessional": {
    //   "handler": async function (context) {
    //     // this.io
    //     //   .of("/professional")
    //     //   .to(data.socketId)
    //     //   .emit("notifyProfessionalForRideWayData", context.params);

    //     // this.io
    //     //   .of("/notification")
    //     //   .to(data.socketId)
    //     //   .emit(data.token, {
    //     //     ...context.params.data,
    //     //     "duration": data.duration,
    //     //     "distance": data.distance,
    //     //   });

    //     this.io
    //       .of("/professional")
    //       .to(context.params.socketId)
    //       .emit("notifyProfessionalForRideWayData", {
    //         ...context.params,
    //       });
    //   },
    // },
    "socket.sendAdminNotificationForRideWayData": {
      "handler": async function (context) {
        this.io
          .of("/admin")
          .emit("notifyAdminForRideWayData", context.params.data);
      },
    },
    "socket.sendSecurityNotificationToAdmin": {
      "handler": async function (context) {
        this.io.of("/admin").emit("notifyAdmin", context.params.data);
      },
    },
    "socket.sendProfessionalStatusNotificationToAdminSatelightView": {
      "handler": async function (context) {
        this.io
          .of("/admin")
          .emit("notifyAdminForProfessionalStatus", context.params.data);
      },
    },
    "socket.sendStatusNotificationToWebBookingApp": {
      "handler": async function (context) {
        this.io
          .of("/webBooking")
          .emit("trackBookingStatusForWebBookingApp", context.params);
      },
    },
    "socket.sendOperatorAcceptedNotification": {
      "params": {
        "id": { "type": "string" },
      },
      "handler": async function (context) {
        this.io.of("/admin").emit("operatorAcceptNotify", context.params);
      },
    },
    "socket.sendSecurityAcceptNotify": {
      "params": {
        "id": { "type": "string" },
      },
      "handler": async function (context) {
        this.io.of("/admin").emit("securityAcceptNotify", context.params);
      },
    },
    "socket.sendSecurityUpdatesToAdmin": {
      "params": {
        "id": { "type": "string" },
      },
      "handler": async function (context) {
        this.io.of("/admin").emit("securityUpdates", context.params);
      },
    },
  },
  "methods": {
    "socketAuthentication": async function (context, access) {
      if (
        context.socket?.handshake?.query &&
        context.socket?.handshake?.query?.accessToken &&
        context.socket?.handshake?.query?.deviceId
      ) {
        try {
          context.socket.handshake.query.accessToken =
            context.socket.handshake.query.accessToken.toString();
          context.socket.handshake.query.deviceId =
            context.socket.handshake.query.deviceId.toString();

          const jwt = cryptoUtils.jwtVerify(
            context.socket.handshake.query.accessToken
          );
          if (jwt.auth) {
            context.socket.userId = jwt.payload._id;
            context.socket.userType = jwt.payload.type;

            this.logger.info(
              `${context.socket.userId} ${context.socket.userType} CONNECTED`
            );
            if (!access.includes(jwt.payload.type))
              context.socket.disconnect(true);
            let validDevice;
            if (jwt.payload.type === constantUtil.USER) {
              validDevice = await this.broker.emit("user.verifyToken", {
                "userId": jwt.payload._id,
                "accessToken": context.socket.handshake.query.accessToken,
                "deviceId": context.socket.handshake.query.deviceId,
                "socketId":
                  context.socket.adapter.nsp.name.toString() === "/notification"
                    ? context.socket.id
                    : "",
              });
            }
            if (jwt.payload.type === constantUtil.PROFESSIONAL) {
              validDevice = await this.broker.emit("professional.verifyToken", {
                "professionalId": jwt.payload._id,
                "accessToken": context.socket.handshake.query.accessToken,
                "deviceId": context.socket.handshake.query.deviceId,
                "socketId":
                  context.socket.adapter.nsp.name.toString() === "/notification"
                    ? context.socket.id
                    : "",
              });
            }
            if (jwt.payload.type === constantUtil.OFFICER) {
              validDevice = await this.broker.emit("officer.verifyToken", {
                "officerId": jwt.payload._id,
                "accessToken": context.socket.handshake.query.accessToken,
                "deviceId": context.socket.handshake.query.deviceId,
                "socketId":
                  context.socket.adapter.nsp.name.toString() === "/notification"
                    ? context.socket.id
                    : "",
              });
            }
            if (jwt.payload.type === constantUtil.ADMIN) {
              validDevice = await this.broker.emit("admin.verifyToken", {
                "adminId": jwt.payload._id,
                "accessToken": context.socket.handshake.query.accessToken,
                "socketId":
                  context.socket.adapter.nsp.name.toString() === "/notification"
                    ? context.socket.id
                    : "",
              });
            }
            if (!validDevice[0]) context.socket.disconnect(true);
          } else {
            context.socket.disconnect(true);
          }
        } catch (error) {
          context.socket.disconnect(true);
        }
      } else {
        context.socket.disconnect(true);
      }
    },
  },
  /* LIFECYCLE METHODS */
  created() {},
  async started() {},
  async stopped() {},
};
