"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const userValidations = require("../validations/user.validation");
const userAction = require("../actions/user.action");
const userEvent = require("../events/user.event");

module.exports = {
  "name": "user",
  "mixins": [DBMixin(constantUtil.DBSCHEMAUSERS)],
  "professionalModel": DBMixin(constantUtil.DBSCHEMAPROFESSIONAL),
  "settings": {},
  "dependencies": [],
  "actions": {
    // "validateLoginUser": {
    //   "params": userValidations.login,
    //   "handler": userAction.validateLoginUser,
    // },
    "login": {
      "params": userValidations.login,
      "handler": userAction.login,
    },
    "verifyOtp": {
      "params": userValidations.verifyOtp,
      "handler": userAction.verifyOtp,
    },
    "verifyEmail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.verifyEmail,
      "handler": userAction.verifyEmail,
    },
    "profile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.profile,
    },
    "updateProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.updateProfile,
      "handler": userAction.updateProfile,
    },
    "activateAndInactivateAccount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.activateAndInactivateAccount,
      "handler": userAction.activateAndInactivateAccount,
    },
    "changePhoneNumber": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.changePhoneNumber,
      "handler": userAction.changePhoneNumber,
    },
    "manageAddress": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.manageAddress,
      "handler": userAction.manageAddress,
    },
    "manageCard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.manageCard,
      "handler": userAction.manageCard,
    },
    "setDefaultPaymentOption": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.setDefaultPaymentOption,
      "handler": userAction.setDefaultPaymentOption,
    },
    "setAppliedCoupons": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.setAppliedCoupons,
      "handler": userAction.setAppliedCoupons,
    },
    "logout": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.logout,
    },
    "updateLiveLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.updateLiveLocation,
      "handler": userAction.updateLiveLocation,
    },
    "trackUserLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": { "userId": { "type": "string", "min": "1" } },
      "handler": userAction.trackUserLocation,
    },
    "getUserData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.getUserData,
      "handler": userAction.getUserData,
    },
    "addBankDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      // 'params': userValidations.addBankDetails,
      "handler": userAction.addBankDetails,
    },
    "removeBankDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.removeBankDetails,
      "handler": userAction.removeBankDetails,
    },
    "getBankDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.getBankDetails,
    },
    "checkUserAvail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.checkUserAvail,
      "handler": userAction.checkUserAvail,
    },
    "checkUserAndProfessionalAvailableUsingPhoneNo": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.checkUserAvail,
      "handler": userAction.checkUserAndProfessionalAvailableUsingPhoneNo,
    },
    "adminCheckUserAvail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.checkUserAvail,
      "handler": userAction.adminCheckUserAvail,
    },
    "registerUserByBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.registerUserByBooking,
      "handler": userAction.registerUserByBooking,
    },
    "viewDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.viewDetails,
    },
    "adminViewDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": userAction.adminViewDetails,
    },
    "getRatings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.getRatings,
      "handler": userAction.getRatings,
    },
    "checkContacts": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.checkContacts,
      "handler": userAction.checkContacts,
    },
    "addWalletAmount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.addWalletAmount,
      "handler": userAction.addWalletAmount,
    },
    "updatePermissionData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.updatePermissionData,
      "handler": userAction.updatePermissionData,
    },
    "getInviteAndEarnDetail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.getInviteAndEarnDetail,
    },
    "getInviteAndEarnHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.getInviteAndEarnHistory,
      "handler": userAction.getInviteAndEarnHistory,
    },
    "getInviteAndEarnList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.getInviteAndEarnList,
      "handler": userAction.getInviteAndEarnList,
    },
    "downloadInviteAndEarnList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": userValidations.getInviteAndEarnList,
      "handler": userAction.downloadInviteAndEarnList,
    },
    "downloadUserJoiningList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": userValidations.getInviteAndEarnList,
      "handler": userAction.downloadUserJoiningList,
    },
    "searchUserByName": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": userAction.searchUserByName,
    },
    "getUsersJoinerList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.getUsersJoinerList,
      "handler": userAction.getUsersJoinerList,
    },
    "updatePasswordBypass": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": userValidations.updatePasswordBypass,
      "handler": userAction.updatePasswordBypass,
    },
    "getUserId": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.getUserId,
      "handler": userAction.getUserId,
    },
    "getQrcode": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": userAction.getQrcode,
    },
    //PAYMENT ,CARD,BANK ACCOUNT RELATED
    "verifyCard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": userValidations.verifyCard,
      "handler": userAction.verifyCard,
    },
    "dbMigration": {
      "handler": userAction.dbMigration,
    },
    //--------- Redeem Rewards Point Start ----------------
    "redeemRewardsPoint": {
      "auth": constantUtil.REQUIRED,
      // "access": [
      //   constantUtil.USER,
      //   constantUtil.PROFESSIONAL,
      //   constantUtil.ADMIN,
      // ],
      "params": {
        "lng": { "type": "number", "convert": true },
        "lat": { "type": "number", "convert": true },
        "redeemPoint": { "type": "number", "convert": true },
      },
      "handler": userAction.redeemRewardsPoint,
    },
    "getBookingLocationCategories": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": {
        "pickupLat": {
          "type": "number",
          "convert": true,
        },
        "pickupLng": {
          "type": "number",
          "convert": true,
        },
        "rideType": {
          "type": "enum",
          "values": ["rental"],
        },
      },
      "handler": userAction.getBookingLocationCategories,
    },

    //--------- Redeem Rewards Point End ----------------

    // UsersActiveInactive......

    "getUsersActiveInactive": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      // "params": userValidations.verifyCard,
      "handler": userAction.getUsersActiveInactive,
    },
    "manageBlockedProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": {
        "userId": { "type": "string", "convert": true },
        "blockedProfessionalId": { "type": "string", "convert": true },
        "action": { "type": "string", "convert": true },
      },
      "handler": userAction.manageBlockedProfessionalList,
    },
    "manageTrustedContact": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": userAction.manageTrustedContact,
    },
    "updateProfileNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "id": { "type": "string", "convert": true },
      },
      "handler": userAction.updateProfileNotes,
    },
  },
  "events": {
    "user.verifyToken": {
      "handler": userEvent.verifyToken,
    },
    "user.getUsersList": {
      "handler": userEvent.getUsersList,
    },
    "user.getEditUser": {
      "handler": userEvent.getEditUser,
    },
    "user.updateUser": {
      "handler": userEvent.updateUser,
    },
    "user.getUnregisteredUserList": {
      "handler": userEvent.getUnregisteredUserList,
    },
    "user.closeUnregisteredUser": {
      "handler": userEvent.closeUnregisteredUser,
    },
    "user.addUnregisteredUserNotes": {
      "handler": userEvent.addUnregisteredUserNotes,
    },
    "user.changeUnregisteredUsersStatus": {
      "handler": userEvent.changeUnregisteredUsersStatus,
    },
    "user.getById": {
      "params": { "id": { "type": "string", "min": 1 } },
      "handler": userEvent.getById,
    },
    "user.changeUsersStatus": {
      "handler": userEvent.changeUsersStatus,
    },
    "user.updateScheduleRide": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "convert": true },
        "bookingDate": { "type": "string", "convert": true },
        "estimationTime": { "type": "number", "convert": true },
      },
      "handler": userEvent.updateScheduleRide,
    },
    "user.updateOnGoingBooking": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "convert": true, "optional": true },
        "bookingType": { "type": "string", "convert": true, "optional": true },
      },
      "handler": userEvent.updateOnGoingBooking,
    },
    "user.removeOngoingBooking": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": userEvent.removeOngoingBooking,
    },
    "user.removeScheduleBooking": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "convert": true, "optional": true },
        "bookingType": { "type": "string", "convert": true, "optional": true },
      },
      "handler": userEvent.removeScheduleBooking,
    },
    "user.updateRating": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "rating": { "type": "number", "max": 5 },
      },
      "handler": userEvent.updateRating,
    },
    "user.updateEscortRating": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "rating": { "type": "number", "max": 5 },
      },
      "handler": userEvent.updateEscortRating,
    },
    "user.updateTrustedContact": {
      "handler": userEvent.updateTrustedContact,
    },
    "user.changeTrustedContactReason": {
      "handler": userEvent.changeTrustedContactReason,
    },
    "user.getTrustedContactsList": {
      "handler": userEvent.getTrustedContactsList,
    },
    "user.updateLiveLocation": {
      "handler": userEvent.updateLiveLocation,
    },
    "user.updatependingReview": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "optional": true },
        "lastCurrencyCode": { "type": "string", "optional": true },
      },
      "handler": userEvent.updatependingReview,
    },
    "user.updateSubscriptionPlan": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "planId": { "type": "string", "optional": true },
        "subscribedAt": { "type": "string", "optional": true },
        "subscribedEndAt": { "type": "string", "optional": true },
        "subscribedAtTimestamp": { "type": "number", "optional": true },
        "subscribedEndAtTimestamp": { "type": "number", "optional": true },
        "price": { "type": "number", "optional": true },
      },
      "handler": userEvent.updateSubscriptionPlan,
    },
    "user.updateWalletAmountInBooking": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "finalAmount": { "type": "number", "convert": true },
      },
      "handler": userEvent.updateWalletAmountInBooking,
    },
    "user.updateCancelFeeInWallet": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "finalAmount": { "type": "number", "convert": true },
      },
      "handler": userEvent.updateCancelFeeInWallet,
    },
    "user.updateWalletAmountTripEnded": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "transStatus": { "type": "string", "min": 1 },
        "paymentMethod": { "type": "string", "min": 1 },
      },
      "handler": userEvent.updateWalletAmountTripEnded,
    },
    "user.updateWalletForPendingAmount": {
      "params": {
        "userId": { "type": "string", "min": 1 },
      },
      "handler": userEvent.updateWalletForPendingAmount,
    },
    "user.updateWalletRechargeOrWithdraw": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "transStatus": { "type": "string", "min": 1 },
      },
      "handler": userEvent.updateWalletRechargeOrWithdraw,
    },
    "user.sendMoneyToFriends": {
      "params": {
        "userId": { "type": "string", "min": 1, "convert": true },
        "amount": { "type": "number", "convert": true },
        "phoneNumber": { "type": "string", "min": 1 },
        "phoneCode": { "type": "string", "min": 1 },
      },
      "handler": userEvent.sendMoneyToFriends,
    },
    "user.scheduleWalletUpdate": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "finalAmount": { "type": "number", "convert": true },
      },
      "handler": userEvent.scheduleWalletUpdate,
    },
    "user.updateWalletAmountInScheduleBooking": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "finalAmount": { "type": "number", "convert": true },
      },
      "handler": userEvent.updateWalletAmountInScheduleBooking,
    },
    "user.getAvailableWalletAmount": {
      "handler": userEvent.getAvailableWalletAmount,
    },
    "user.getWalletAmountList": {
      "handler": userEvent.getWalletAmountList,
    },
    "user.checkUserAvailable": {
      "handler": userEvent.checkUserAvailable,
    },
    "user.getUsers": {
      "handler": userEvent.getUsers,
    },
    "user.getUserDetails": {
      "handler": userEvent.getUserDetails,
    },
    "user.updateOnGoingSecurityBooking": {
      "params": {
        "id": { "type": "string", "min": 4 },
        "escortBookingId": { "type": "string", "optional": true },
        "type": { "type": "string", "min": 1, "optional": true },
        "isOfficerAssigned": { "type": "boolean", "optional": true },
      },
      "handler": userEvent.updateOnGoingSecurityBooking,
    },
    "user.updateSecurityPendingReview": {
      "params": {
        "id": { "type": "string", "min": 4 },
        "escortBookingId": { "type": "string", "min": 4 },
        "type": { "type": "string", "min": 1 },
      },
      "handler": userEvent.updateSecurityPendingReview,
    },
    "user.inviteAndEarn": {
      "params": {
        "joinerId": { "type": "string", "min": 4 },
      },
      "handler": userEvent.inviteAndEarn,
    },
    "user.updateJoinerRideCount": {
      "params": {
        "joinerId": { "type": "string", "min": 4 },
      },
      "handler": userEvent.updateJoinerRideCount,
    },
    "user.inviterRideEarnings": {
      "params": {
        "inviterUniqueCode": { "type": "string", "min": 4 },
        "amount": { "type": "number", "convert": true },
        "bookingId": { "type": "string" },
        "refBookingId": { "type": "string" },
        "transactionReason": { "type": "string" },
      },
      "handler": userEvent.inviterRideEarnings,
    },
    "user.addVerifiedCard": {
      "params": userValidations.addVerifiedCard,
      "handler": userEvent.addVerifiedCard,
    },
    "user.getCategoryList": {
      "handler": userEvent.getCategoryList,
    },
    "user.addCard": {
      "handler": userEvent.addCard,
    },
    // for booking

    // DB ACCESS
    "user.removePrefix0": {
      "handler": userEvent.removePrefix0,
    },
    "user.forceLogout": {
      "handler": userEvent.forceLogout,
    },
    // USER WALLET
    "user.updateWalletAmount": {
      "params": {
        "userId": { "type": "string", "convert": true, "min": 1 },
        "availableAmount": { "type": "number" },
        "freezedAmount": { "type": "number" },
        "scheduleFreezedAmount": { "type": "number" },
        "isSendNotification": {
          "type": "boolean",
          "convert": true,
          "optional": true,
        },
        "notificationMessage": {
          "type": "string",
          "optional": true,
        },
        "requestFrom": {
          "type": "enum",
          "values": [constantUtil.WALLETWITHDRAWAL],
          "optional": true,
        },
      },
      "handler": userEvent.updateWalletAmount,
    },
    // refund detailes to admin
    "user.getRefundDetails": {
      "handler": userEvent.getRefundDetails,
    },
    // Get Card Details -- Added by Ajith
    "user.getCardDetailsById": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "cardId": { "type": "string", "min": 1 },
      },
      "handler": userEvent.getCardDetailsById,
    },
    "user.getUserListForNotification": {
      "handler": userEvent.getUserListForNotification,
    },
    // Get Card Details -- Added by Ajith
    "user.updateDefaultCardStatusInActiveActive": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "cardId": { "type": "string", "optional": true },
      },
      "handler": userEvent.updateDefaultCardStatusInActiveActive,
    },
    "user.changeCardDefaultStatus": {
      "params": {
        "userId": { "type": "string", "min": 1, "convert": true },
        "cardId": { "type": "string", "min": 1, "convert": true },
        "status": { "type": "boolean", "convert": true },
      },
      "handler": userEvent.changeCardDefaultStatus,
    },
    // FOR PROMOTION CAMPAIGN APIS
    "user.getUsersDataforPromtionCampaign": {
      "params": {
        "category": { "type": "string" },
        "filter": { "type": "string" },
        "type": { "type": "string" },
      },
      "handler": userEvent.getUsersDataforPromtionCampaign,
    },
    "user.updateWalletRewardPoint": {
      "params": {
        "userId": { "type": "string", "min": 1 },
        "rewardPoints": { "type": "number", "convert": true },
      },
      "handler": userEvent.updateWalletRewardPoint,
    },

    //#region Cororate Officer
    "user.updateAndGetCororateOfficerByPhoneNumber": {
      "params": {
        "clientId": { "type": "string", "min": 1, "convert": true },
        "phoneNumber": { "type": "string", "min": 1, "convert": true },
        "phoneCode": { "type": "string", "min": 1, "convert": true },
        "nationalIdNo": { "type": "string", "trim": true, "optional": true },
        "email": { "type": "string", "min": 1, "convert": true },
        "firstName": { "type": "string", "min": 1, "convert": true },
        "lastName": { "type": "string", "min": 1, "convert": true },
        "gender": { "type": "string", "min": 1, "convert": true },
      },
      "handler": userEvent.updateAndGetCororateOfficerByPhoneNumber,
    },
    //#endregion Cororate Officer
    "user.sendPromotion": {
      "handler": userEvent.sendPromotion,
    },
    "user.updateServiceAreaId": {
      "handler": userEvent.updateServiceAreaId,
    },
    "user.getEndRideIssueList": {
      "handler": userEvent.getEndRideIssueList,
    },
    "user.updateBookingInfoIds": {
      "handler": userEvent.updateBookingInfoIds,
    },
    "user.calculateAndUpdateIncentiveAmount": {
      "handler": userEvent.calculateAndUpdateIncentiveAmount,
    },
    "user.getUserListByServiceAreaId": {
      "params": {
        "serviceAreaId": { "type": "string", "convert": true, "min": 12 },
      },
      "handler": userEvent.getUserListByServiceAreaId,
    },
    "user.checkUserAvail": {
      "handler": userEvent.checkUserAvail,
    },
    "user.checkUniqueReferralCode": {
      "handler": userEvent.checkUniqueReferralCode,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
