"use strict";

const DBMixin = require("../mixins/db.mixin");
// const DBMixin = require("../mixins/replicaSetDBConn.mixin");
const constantUtil = require("../utils/constant.util");
const bookingMethod = require("../methods/booking.method");
// const bookingEvent = require("../events/booking.event");
const bookingV2Action = require("../actions/bookingV2.action");
const bookingValidation = require("../validations/booking.validation");
// const smsAndCallUtil = require("../utils/smsAndCall.util");

module.exports = {
  "name": "bookingV2",
  "mixins": [DBMixin(constantUtil.DBSCHEMABOOKINGS)],
  "bookingModel": DBMixin(constantUtil.DBSCHEMABOOKINGS),
  "adminModel": DBMixin(constantUtil.DBSCHEMAADMIN),
  "professionalModel": DBMixin(constantUtil.DBSCHEMAPROFESSIONAL),
  "userModel": DBMixin(constantUtil.DBSCHEMAUSERS),
  "transactionModel": DBMixin(constantUtil.DBSCHEMATRANSACTION),
  "settings": {},
  "dependencies": [],
  "actions": {
    "updateProfessionalBookingStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": bookingValidation.updateProfessionalBookingStatus,
      "handler": bookingV2Action.updateProfessionalBookingStatus,
    },
    // "bookingStatusChange": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": bookingValidation.bookingStatusChange,
    //   "handler": bookingV2Action.bookingStatusChange,
    // },
    // "bookingStatusChangeEnded": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": bookingValidation.bookingStatusChangeEnded,
    //   "handler": bookingV2Action.bookingStatusChangeEnded,
    // },
    "checking": {
      "handler": bookingV2Action.checking,
    },
  },
  "events": {},
  "methods": {
    // "getProfessionalByLocation": bookingMethod.getProfessionalByLocation,
    "checkAndRemoveProfessionalAndUserDenyAndBlockList":
      bookingMethod.checkAndRemoveProfessionalAndUserDenyAndBlockList,
    "getBookingDetailsById": bookingMethod.getBookingDetailsById,
    // "findCurrentShareRideProfessional":
    //   bookingMethod.findCurrentShareRideProfessional,
  },
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
