"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const securityEscortAction = require("../actions/securityEscort.action");
const securityEscortEvent = require("../events/securityEscort.event");
const securityEscortValidation = require("../validations/securityEscort.validation");

module.exports = {
  "name": "securityEscort",
  "mixins": [DBMixin(constantUtil.DBSCHEMASECURITYESCORT)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "getSosHelp": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": securityEscortValidation.getHelp,
      "handler": securityEscortAction.getSosHelp,
    },
    "getSecurityHelpWithoutFile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": securityEscortValidation.getHelp,
      "handler": securityEscortAction.getSecurityHelpWithoutFile,
    },
    "getSecurityHelpWithFile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": securityEscortValidation.getHelp,
      "handler": securityEscortAction.getSecurityHelpWithFile,
    },
    "acceptEscortBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": securityEscortValidation.acceptEscortBooking,
      "handler": securityEscortAction.acceptEscortBooking,
    },
    "escortStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": securityEscortValidation.escortStatusChange,
      "handler": securityEscortAction.escortStatusChange,
    },
    "acknowledgeEscortStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": securityEscortValidation.acknowledgeEscortStatus,
      "handler": securityEscortAction.acknowledgeEscortStatus,
    },
    "viewSecurityRequest": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.viewSecurityRequest,
      "handler": securityEscortAction.viewSecurityRequest,
    },
    "viewClosedSecurityRequest": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.viewClosedSecurityRequest,
      "handler": securityEscortAction.viewClosedSecurityRequest,
    },
    "viewSecurityRequestBasedOperator": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.viewSecurityRequestBasedOperator,
      "handler": securityEscortAction.viewSecurityRequestBasedOperator,
    },
    "getAvaliableOfficer": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.getAvaliableOfficer,
      "handler": securityEscortAction.getAvaliableOfficer,
    },
    "sendOfficerRequest": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.sendOfficerRequest,
      "handler": securityEscortAction.sendOfficerRequest,
    },
    "assignOfficer": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.assignOfficer,
      "handler": securityEscortAction.assignOfficer,
    },
    "priorityChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.priorityChange,
      "handler": securityEscortAction.priorityChange,
    },
    "sendTripWatch": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": securityEscortValidation.getHelp,
      "handler": securityEscortAction.sendTripWatch,
    },
    "sendPersonalWatch": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": securityEscortValidation.sendPersonalWatch,
      "handler": securityEscortAction.sendPersonalWatch,
    },
    "cancelEscortBooking": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.cancelEscortBooking,
      "handler": securityEscortAction.cancelEscortBooking,
    },
    "escortBookingCancel": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": securityEscortValidation.escortBookingCancel,
      "handler": securityEscortAction.escortBookingCancel,
    },
    "escortAdminStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.escortAdminStatusChange,
      "handler": securityEscortAction.escortAdminStatusChange,
    },
    "trackEscortDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": securityEscortValidation.trackEscortDetails,
      "handler": securityEscortAction.trackEscortDetails,
    },
    "viewAddressSave": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.viewAddressSave,
      "handler": securityEscortAction.viewAddressSave,
    },
    "submitEscortRating": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": securityEscortValidation.submitEscortRating,
      "handler": securityEscortAction.submitEscortRating,
    },
    "sendEmergencyToTrustedContact": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": securityEscortValidation.sendEmergencyToTrustedContact,
      "handler": securityEscortAction.sendEmergencyToTrustedContact,
    },
    "emergencyExpiryOrDeny": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      // 'params': securityEscortValidation.emergencyExpiryOrDeny,
      "handler": securityEscortAction.emergencyExpiryOrDeny,
    },
    "escortBookingMediaReport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": securityEscortValidation.escortBookingMediaReport,
      "handler": securityEscortAction.escortBookingMediaReport,
    },
    "escortBookingMediaReportImage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": securityEscortValidation.escortBookingMediaReportImage,
      "handler": securityEscortAction.escortBookingMediaReportImage,
    },
    "getOfficerHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": securityEscortValidation.getOfficerHistory,
      "handler": securityEscortAction.getOfficerHistory,
    },
    "getUserEscortHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": securityEscortValidation.getUserEscortHistory,
      "handler": securityEscortAction.getUserEscortHistory,
    },
    "viewEscortDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": securityEscortValidation.viewEscortDetails,
      "handler": securityEscortAction.viewEscortDetails,
    },
  },
  "events": {
    "securityEscort.getUnViewedData": {
      "handler": securityEscortEvent.getUnViewedData,
    },
    "securityEscort.getSecurityList": {
      "handler": securityEscortEvent.getSecurityList,
    },
    "securityEscort.getSecurityClosedList": {
      "handler": securityEscortEvent.getSecurityClosedList,
    },
    "securityEscort.getSecurityOperatorList": {
      "handler": securityEscortEvent.getSecurityOperatorList,
    },
    "securityEscort.changeSecurityStatus": {
      "handler": securityEscortEvent.changeSecurityStatus,
    },
    "securityEscort.addSecurityNotes": {
      "handler": securityEscortEvent.addSecurityNotes,
    },
    "securityEscort.operatorAcceptSecurity": {
      "handler": securityEscortEvent.operatorAcceptSecurity,
    },
    "securityEscort.getOfficerRatingList": {
      "handler": securityEscortEvent.getOfficerRatingList,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
