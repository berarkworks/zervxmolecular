"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const heatmapValidations = require("../validations/heatmap.validation");
const heatmapAction = require("../actions/heatmap.action");
const heatmapEvent = require("../events/heatmap.event");

module.exports = {
  "name": "heatmap",
  "mixins": [DBMixin(constantUtil.DBSCHEMA_HEATMAP)],
  "professionalModel": DBMixin(constantUtil.DBSCHEMAPROFESSIONAL),
  // "categoryModel": DBMixin(constantUtil.DBSCHEMACATEGORIES),
  "adminModel": DBMixin(constantUtil.DBSCHEMAADMIN),
  "settings": {},
  "dependencies": [],
  "actions": {
    "manageHeatmap": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": heatmapValidations.manageHeatmap,
      "handler": heatmapAction.manageHeatmap,
    },
    "manageManualPointHeatmap": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": heatmapValidations.manageHeatmap,
      "handler": heatmapAction.manageManualPointHeatmap,
    },
    "getHeatmapDataByLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": heatmapValidations.getHeatmapDataByLocation,
      "handler": heatmapAction.getHeatmapDataByLocation,
    },
    "heatmapReportData": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],

      "handler": heatmapAction.heatmapReportData,
    },
  },
  "events": {
    "heatmap.manageHeatmap": {
      "params": heatmapValidations.manageHeatmap,
      "handler": heatmapEvent.manageHeatmap,
    },
    "heatmap.getHeatmapDataByLocation": {
      "params": heatmapValidations.getHeatmapDataByLocation,
      "handler": heatmapEvent.getHeatmapDataByLocation,
    },
    "heatmap.calculatePeakFareUsingHeatmap": {
      "params": heatmapValidations.calculatePeakFareUsingHeatmap,
      "handler": heatmapEvent.calculatePeakFareUsingHeatmap,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
