"use strict";

const DBMixin = require("../mixins/db.mixin");
const officerValidation = require("../validations/officer.validation");
const officerAction = require("../actions/officer.action");
const officerEvent = require("../events/officer.event");
const constantUtil = require("../utils/constant.util");

module.exports = {
  "name": "officer",
  "mixins": [DBMixin("officers")],
  "settings": {},
  "dependencies": [],
  "actions": {
    //#region Response Officer
    "login": {
      "params": officerValidation.login,
      "handler": officerAction.login,
    },
    "verifyOtp": {
      "params": officerValidation.verifyOtp,
      "handler": officerAction.verifyOtp,
    },
    "getProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "handler": officerAction.getProfile,
    },
    "updateProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": officerValidation.updateProfile,
      "handler": officerAction.updateProfile,
    },
    "updateLiveLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": officerValidation.updateLiveLocation,
      "handler": officerAction.updateLiveLocation,
    },
    "updateOnlineStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": officerValidation.updateOnlineStatus,
      "handler": officerAction.updateOnlineStatus,
    },
    "logout": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "handler": officerAction.logout,
    },
    "trackOfficerLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.OFFICER,
      ],
      "params": { "officerId": { "type": "string", "min": "1" } },
      "handler": officerAction.trackOfficerLocation,
    },
    "getOfficerData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": officerValidation.getOfficerData,
      "handler": officerAction.getOfficerData,
    },
    "getRatings": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": officerValidation.getRatings,
      "handler": officerAction.getRatings,
    },
    "updatePermissionData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.OFFICER],
      "params": officerValidation.updatePermissionData,
      "handler": officerAction.updatePermissionData,
    },
    //#endregion Officers

    //#region Corporate User

    "getCorporateDetailsByUserId": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": {
        "userType": {
          "type": "string",
          "convert": true,
          "enum": [constantUtil.CORPORATEOFFICER, constantUtil.RESPONSEOFFICER],
        },
        "userId": { "type": "string", "convert": true },
      },
      "handler": officerAction.getCorporateDetailsByUserId,
    },

    "getCorporateOfficerList": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": officerValidation.getCorporateOfficerList,
      "handler": officerAction.getCorporateOfficerList,
    },

    "manageCorporateOfficer": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": officerValidation.manageCorporateOfficer,
      "handler": officerAction.manageCorporateOfficer,
    },

    "getCorporateOfficerById": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": {
        // "userType": {
        //   "type": "string",
        //   "convert": true,
        //   "enum": [constantUtil.CORPORATEOFFICER, constantUtil.RESPONSEOFFICER],
        // },
        "id": { "type": "string", "convert": true },
      },
      "handler": officerAction.getCorporateOfficerById,
    },
    "checkCorporateOfficerByPhoneNumber": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "params": {
        // "userType": {
        //   "type": "string",
        //   "convert": true,
        //   "enum": [constantUtil.CORPORATEOFFICER, constantUtil.RESPONSEOFFICER],
        // },
        "corporateId": { "type": "string", "convert": true },
        "phoneCode": { "type": "string", "convert": true },
        "phoneNumber": { "type": "string", "convert": true },
      },
      "handler": officerAction.checkCorporateOfficerByPhoneNumber,
    },
    "changeCorporateOfficerData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": officerValidation.changeCorporateOfficerData,
      "handler": officerAction.changeCorporateOfficerData,
    },
    "searchCorporateOfficerByName": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": officerAction.searchCorporateOfficerByName,
    },
    //#endregion Corporate User
  },
  "events": {
    //#region Officers
    "officer.verifyToken": {
      "handler": officerEvent.verifyToken,
    },
    //ADMIN MODULE APIS
    "officer.getOfficerList": {
      "handler": officerEvent.getOfficerList,
    },
    "officer.getEditOfficer": {
      "handler": officerEvent.getEditOfficer,
    },
    "officer.changeOfficerStatus": {
      "handler": officerEvent.changeOfficerStatus,
    },
    "officer.updateOfficer": {
      "handler": officerEvent.updateOfficer,
    },
    "officer.updateLiveLocation": {
      "handler": officerEvent.updateLiveLocation,
    },
    "officer.getOfficerByLocation": {
      "handler": officerEvent.getOfficerByLocation,
    },
    "officer.getOfficerByLocationRequest": {
      "handler": officerEvent.getOfficerByLocationRequest,
    },
    "officer.updateOnGoingBooking": {
      "handler": officerEvent.updateOnGoingBooking,
    },
    "officer.updateEscortRating": {
      "params": {
        "officerId": { "type": "string", "min": 1 },
        "rating": { "type": "number", "max": 5 },
      },
      "handler": officerEvent.updateEscortRating,
    },
    "officer.updateAllOnlineOfficerStatus": {
      "handler": officerEvent.updateAllOnlineOfficerStatus,
    },
    //#endregion Response Officer

    //#region Corporate Officer

    "officer.getCorporateDetailsByUserId": {
      "params": {
        "userId": { "type": "string", "convert": true },
      },
      "handler": officerEvent.getCorporateDetailsByUserId,
    },

    "officer.getCorporateOfficerList": {
      "params": officerValidation.getCorporateOfficerList,
      "handler": officerEvent.getCorporateOfficerList,
    },
    "officer.getCorporateOfficerById": {
      "params": {
        "id": { "type": "string", "convert": true },
      },
      "handler": officerEvent.getCorporateOfficerById,
    },

    "officer.checkCorporateOfficerByPhoneNumber": {
      "params": {
        "corporateId": { "type": "string", "convert": true },
        "phoneCode": { "type": "string", "convert": true },
        "phoneNumber": { "type": "string", "convert": true },
      },
      "handler": officerEvent.checkCorporateOfficerByPhoneNumber,
    },

    //#endregion Corporate Officer
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
