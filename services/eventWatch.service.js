"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const eventWatchValidation = require("../validations/eventWatch.validation");
const eventWatchAction = require("../actions/eventWatch.action");
const eventWatchEvent = require("../events/eventWatch.event");

module.exports = {
  "name": "eventWatch",
  "mixins": [DBMixin(constantUtil.DBSCHEMAEVENTSWATCH)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "createEvent": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.createEvent,
      "handler": eventWatchAction.createEvent,
    },
    "addPraticipants": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.addPraticipants,
      "handler": eventWatchAction.addPraticipants,
    },
    "praticipantsStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.praticipantsStatusChange,
      "handler": eventWatchAction.praticipantsStatusChange,
    },
    "eventStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.eventStatusChange,
      "handler": eventWatchAction.eventStatusChange,
    },
    "editEvent": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.editEvent,
      "handler": eventWatchAction.editEvent,
    },
    "viewDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.viewDetails,
      "handler": eventWatchAction.viewDetails,
    },
    "getYourEventList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.getYourEventList,
      "handler": eventWatchAction.getYourEventList,
    },
    "getParticipatedEventList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": eventWatchValidation.getParticipatedEventList,
      "handler": eventWatchAction.getParticipatedEventList,
    },
  },
  "events": {},
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
