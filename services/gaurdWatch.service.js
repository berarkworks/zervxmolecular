"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const gaurdWatchValidation = require("../validations/gaurdWatch.validation");
const gaurdWatchAction = require("../actions/gaurdWatch.action");
const gaurdWatchEvent = require("../events/gaurdWatch.event");

module.exports = {
  "name": "gaurdWatch",
  "mixins": [DBMixin(constantUtil.DBSCHEMAGAURDWATCH)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "createSiteLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.createSiteLocation,
      "handler": gaurdWatchAction.createSiteLocation,
    },
    "changeSiteLocationStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.changeSiteLocationStatus,
      "handler": gaurdWatchAction.changeSiteLocationStatus,
    },
    "deleteSiteLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.deleteSiteLocation,
      "handler": gaurdWatchAction.deleteSiteLocation,
    },
    "getSiteLocationList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": gaurdWatchAction.getSiteLocationList,
    },
    "sendQrToMail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.sendQrToMail,
      "handler": gaurdWatchAction.sendQrToMail,
    },
    "addGaurd": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.addGaurd,
      "handler": gaurdWatchAction.addGaurd,
    },
    "getGaurdList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.getGaurdList,
      "handler": gaurdWatchAction.getGaurdList,
    },
    "viewSiteLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.viewSiteLocation,
      "handler": gaurdWatchAction.viewSiteLocation,
    },
    "loginAndLogout": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.loginAndLogout,
      "handler": gaurdWatchAction.loginAndLogout,
    },
    "breakStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.breakStatusChange,
      "handler": gaurdWatchAction.breakStatusChange,
    },
    "getGaurdDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": gaurdWatchAction.getGaurdDetails,
    },
    "scanAndUpdate": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.scanAndUpdate,
      "handler": gaurdWatchAction.scanAndUpdate,
    },
    "getGaurdHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.getGaurdHistory,
      "handler": gaurdWatchAction.getGaurdHistory,
    },
    "getMyHistory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.getMyHistory,
      "handler": gaurdWatchAction.getMyHistory,
    },
    "removeGaurd": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": gaurdWatchValidation.removeGaurd,
      "handler": gaurdWatchAction.removeGaurd,
    },
    "getSiteLocationBasedGuard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": gaurdWatchAction.getSiteLocationBasedGuard,
    },
  },
  "events": {},
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
