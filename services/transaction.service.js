"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");

const transactionEvent = require("../events/transaction.event");
const transactionValidation = require("../validations/transaction.validation");
const transactionAction = require("../actions/transaction.action");
// gateway specified events
const transactionFlutterWaveEvents = require("../paymentevents/flutterwave.payment.event");
const transactionStripeEvents = require("../paymentevents/stripe.payment.event");
const transactionRazorpayEvents = require("../paymentevents/razorpay.payment.event");
const transactionTigoEvents = require("../paymentevents/tigo.payment.event");
const transactionTeleportEvents = require("../paymentevents/teleport.payment.event");
const transactionCyberSourceEvents = require("../paymentevents/cybersource.payment.event");
const transactionZcreditapiaryEvents = require("../paymentevents/zcreditapiary.payment.event");
const transactionPeachEvents = require("../paymentevents/peach.payment.event");
const transactionMercadopagoEvents = require("../paymentevents/mercadopago.payment.event");
const transactionTeyaEvents = require("../paymentevents/teya.payment.event");

const transactionservice = {
  "name": "transaction",
  "mixins": [DBMixin(constantUtil.DBSCHEMATRANSACTION)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "newRechargeWalletTransaction": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": transactionValidation.newRechargeWalletTransaction,
      "handler": transactionAction.newRechargeWalletTransaction,
    },
    "newWithdrawalWalletTransaction": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": transactionValidation.newWithdrawalWalletTransaction,
      "handler": transactionAction.newWithdrawalWalletTransaction,
    },
    "sendMoneyToFriends": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": transactionValidation.sendMoneyToFriends,
      "handler": transactionAction.sendMoneyToFriends,
    },
    "getTransactionDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": transactionValidation.getTransactionDetails,
      "handler": transactionAction.getTransactionDetails,
    },
    "getTransactionDetailsById": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": transactionValidation.getTransactionDetailsById,
      "handler": transactionAction.getTransactionDetailsById,
    },
    //ADMIN PANEL WALLET MANAGEMENT
    "getWalletTransactionList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": transactionValidation.getWalletTransactionList,
      "handler": transactionAction.getWalletTransactionList,
    },
    "getParticularTransactionList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": transactionValidation.getParticularTransactionList,
      "handler": transactionAction.getParticularTransactionList,
    },
    "getTransactionHistoryByType": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": transactionValidation.getTransactionHistoryByType,
      "handler": transactionAction.getTransactionHistoryByType,
    },

    // redirect payment response

    // MOBILE SIDE PAYMENT
    "redirectPayment": {
      "handler": transactionAction.redirectPayment,
    },
    "verifyBankAccount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.USER],
      // 'params': transactionValidation.verifyBankAccount,
      "handler": transactionAction.verifyBankAccount,
    },

    // flutter wave verify
    "validateCharge": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.USER],
      "params": transactionValidation.validateCharge,
      "handler": transactionAction.validateCharge,
    },
    "refund": {
      "handler": transactionAction.refund,
    },

    // webhook
    "webhook": {
      "handler": transactionAction.webhook,
    },
    // Bank Details
    "getBankInputData": {
      // 'params': {
      //   'paymentGatway': { 'type': 'string', 'min': 1 },
      //   'paymentType': { 'type': 'string', 'min': 1 },
      //   // 'userType': { 'type': 'string', 'min': 1 },
      // },
      "handler": transactionAction.getBankInputData,
    },
    //#region  Payment Gatway
    "verifyCardByPaymentGatway": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.USER],
      "params": {
        "paymentGatway": { "type": "string", "min": 1 },
        "userType": { "type": "string", "min": 1 },
      },
      "handler": transactionAction.verifyCardByPaymentGatway,
    },
    "newBookingCardPayment": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.USER],
      "params": {
        "paymentGatway": { "type": "string", "min": 1 },
        "userType": { "type": "string", "min": 1 },
      },
      "handler": transactionAction.newBookingCardPayment,
    },
    "newRechargeWalletCardPayment": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL, constantUtil.USER],
      "params": {
        "paymentGatway": { "type": "string", "min": 1 },
        "userType": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
      },
      "handler": transactionAction.newRechargeWalletCardPayment,
    },
    "newWithdrawalWalletCardPayment": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.PROFESSIONAL],
      "params": {
        "paymentGatway": { "type": "string", "min": 1 },
        "userType": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
      },
      "handler": transactionAction.newBookingCardPayment,
    },
    "webhookingStripePixKeyPayment": {
      "handler": transactionAction.webhookingStripePixKeyPayment,
    },
    "checkCardTransactionStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": {
        "userType": { "type": "string", "convert": "true", "min": 1 },
        "userId": { "type": "string", "optional": true, "convert": "true" },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": "true",
        },
        "transactionId": { "type": "string", "convert": true },
      },
      "handler": transactionAction.checkCardTransactionStatus,
    },
    "getTransactionReport": {
      "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "access": [constantUtil.ADMIN],
      // "params": {
      //   "userType": { "type": "string", "convert": "true", "min": 1 },

      // },
      "handler": transactionAction.getTransactionReport,
    },

    "validateCardRegistrationStatus": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      // "params": {
      //   "userType": { "type": "string", "convert": "true", "min": 1 },
      //   "userId": { "type": "string", "optional": true, "convert": "true" },
      //   "professionalId": {
      //     "type": "string",
      //     "optional": true,
      //     "convert": "true",
      //   },
      //   "cardTokenId": { "type": "string", "convert": true },
      // },
      "handler": transactionAction.validateCardRegistrationStatus,
    },
    "webhookingMercadopagoPixKeyPayment": {
      "handler": transactionAction.webhookingMercadopagoPixKeyPayment,
    },
    "verifyCVVByPaymentGatway": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      // "params": {
      //   "userType": { "type": "string", "convert": "true", "min": 1 },
      //   "userId": { "type": "string", "optional": true, "convert": "true" },
      //   "professionalId": {
      //     "type": "string",
      //     "optional": true,
      //     "convert": "true",
      //   },
      //   "cardTokenId": { "type": "string", "convert": true },
      // },
      "handler": transactionAction.verifyCVVByPaymentGatway,
    },
    "webhookingDelbankPixKeyPayout": {
      "handler": transactionAction.webhookingDelbankPixKeyPayout,
    },
    //#endregion  Payment Gatway
  },
  "events": {
    "transaction.newBookingTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userId": { "type": "string", "min": 1 },
      },
      "handler": transactionEvent.newBookingTransaction,
    },
    "transaction.professionalBookingEndTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "min": 1, "convert": true },
        "refBookingId": { "type": "string", "min": 1 },
      },
      "handler": transactionEvent.professionalBookingEndTransaction,
    },
    "transaction.professionalPayoutTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "professionalId": { "type": "string", "min": 1 },
      },
      "handler": transactionEvent.professionalPayoutTransaction,
    },
    "transaction.professionalSiteCommissionTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "min": 1, "convert": true },
        "refBookingId": { "type": "string", "min": 1 },
        "paymentType": {
          "type": "enum",
          "values": [constantUtil.CREDIT, constantUtil.DEBIT],
        },
      },
      "handler": transactionEvent.professionalSiteCommissionTransaction,
    },
    "transaction.cancelCharge": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userId": { "type": "string", "min": 1 },
        "bookingId": {
          "type": "string",
          "min": 1,
          "convert": true,
          "optional": true,
        },
      },
      "handler": transactionEvent.cancelCharge,
    },
    "transaction.profesionalCancelCharge": {
      "params": {
        "id": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "bookingId": {
          "type": "string",
          "min": 1,
          "convert": true,
          "optional": true,
        },
        "refBookingId": { "type": "string" },
      },
      "handler": transactionEvent.profesionalCancelCharge,
    },
    "transaction.checkCardVaild": {
      "params": transactionValidation.checkCardVaild,
      "handler": transactionEvent.checkCardVaild,
    },
    // "transaction.newBookingCharges": {
    //   "params": {
    //     "card": {
    //       "type": "object",
    //       "strict": true,
    //     },
    //     "currencyCode": { "type": "string", "min": 1 },
    //     "amount": { "type": "string", "min": 1 },
    //     "userId": { "type": "string", "min": 1 },
    //   },
    //   "handler": transactionEvent.newBookingCharges,
    // },
    "transaction.newTipsCharge": {
      "params": {
        "card": {
          "type": "object",
          "strict": true,
          "props": {
            "type": { "type": "string" },
            "cardNumber": { "type": "string" },
            "expiryYear": { "type": "number" },
            "expiryMonth": { "type": "number" },
            "holderName": { "type": "string" },
            "cvv": { "type": "string" },
          },
        },
        "currencyCode": { "type": "string", "min": 1 },
        "amount": { "type": "number" },
        "userId": { "type": "string", "min": 8, "optional": true },
        "professionalId": { "type": "string", "min": 8, "optional": true },
      },
      "handler": transactionEvent.newTipsCharge,
    },
    "transaction.updateWalletAdminRecharge": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "isReimbursement": {
          "type": "boolean",
          "convert": true,
          "optional": true,
        },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string", "optional": true },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
        "admin": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string", "optional": true },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.updateWalletAdminRecharge,
    },
    "transaction.updateWalletAdminDebit": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string", "optional": true },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
        "admin": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string", "optional": true },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.updateWalletAdminDebit,
    },
    "transaction.subscriptionTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string" },
            "avatar": { "type": "string" },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.subscriptionTransaction,
    },
    "transaction.inviteCharge": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string" },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.inviteCharge,
    },
    "transaction.joinCharge": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string" },
            "avatar": { "type": "string", "optional": true },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.joinCharge,
    },
    "transaction.newBookingCancelRefund": {
      "params": {
        "paymentTokenId": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
        "currencyCode": { "type": "string", "min": 1 },
        "amount": { "type": "string", "min": 1, "convert": true },
        "userId": { "type": "string", "min": 1 },
        "paymentInitId": { "type": "string", "convert": true },
        "cardId": { "type": "string", "optional": true, "convert": true },
        "refBookingId": { "type": "string", "optional": true, "convert": true },
        "bookingId": { "type": "string", "optional": true, "convert": true },
        "transactionType": {
          "type": "string",
          "optional": true,
          "convert": true,
        },
      },
      "handler": transactionEvent.newBookingCancelRefund,
    },
    "transaction.userLessAndGreaterEndTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "userType": { "type": "string", "min": 1 },
        "transType": { "type": "string", "min": 1 },
        "transactionType": { "type": "string", "min": 1 },
        "paymentType": { "type": "string", "min": 1 },
        "data": {
          "type": "object",
          "strict": true,
          "props": {
            "id": { "type": "string" },
            "firstName": { "type": "string" },
            "lastName": { "type": "string" },
            "avatar": { "type": "string" },
            "phone": {
              "type": "object",
              "strict": true,
              "props": {
                "code": { "type": "string" },
                "number": { "type": "string" },
              },
            },
          },
        },
      },
      "handler": transactionEvent.userLessAndGreaterEndTransaction,
    },
    "transaction.professionalTolerenceAmountTransaction": {
      "params": {
        "amount": { "type": "number", "convert": true },
        "professionalId": { "type": "string", "min": 1 },
        "bookingId": { "type": "string", "min": 1, "convert": true },
        "refBookingId": { "type": "string", "min": 1, "convert": true },
      },
      "handler": transactionEvent.professionalTolerenceAmountTransaction,
    },
    // "transaction.addBankDetailsInStripeAccount": {
    //   "handler": transactionEvent.addBankDetailsInStripeAccount,
    // },
    "transaction.verifyCard": {
      "params": transactionValidation.verifyCard,
      "handler": transactionEvent.verifyCard,
    },
    "transaction.refund": {
      "params": transactionValidation.refund,
      "handler": transactionEvent.refund,
    },
    // booking events using cards
    "transaction.tripEndCardCharge": {
      "handler": transactionEvent.tripEndCardCharge,
    },

    //TO  ADMIN
    // refund detailes to admin
    "transaction.getRefundDetails": {
      "handler": transactionEvent.getRefundDetails,
    },
    "transaction.adminGetWithDrawDetails": {
      "handler": transactionEvent.adminGetWithDrawDetails,
    },
    "transaction.adminWalletWithManualTransfer": {
      "handler": transactionEvent.adminWalletWithManualTransfer,
    },
    "transaction.getTransactionById": {
      "params": { "transactionId": { "type": "string" } },
      "handler": transactionEvent.getTransactionById,
    },
    "transaction.changeTransactionStatus": {
      "handler": transactionEvent.changeTransactionStatus,
    },
    //  booking Events
    "transaction.newBookingCardPayment": {
      "handler": transactionEvent.newBookingCardPayment,
    },

    //#region Payment Gatway

    //
    // in this section some changes is happend ,watch carefully
    // GATEWAY BASED EVENTS
    // FLUTTERWAVE
    //#region FLUTTERWAVE
    // V1
    "transaction.flutterwave.checkValidCard": {
      "params": transactionValidation.flutterWave.checkValidCard,
      "handler": transactionFlutterWaveEvents.checkValidCard,
    },
    "transaction.flutterwave.walletRecharge": {
      "handler": transactionFlutterWaveEvents.walletRecharge,
    },
    "transaction.flutterwave.refund": {
      "params": {
        "transactionId": { "type": "string", "convert": true },
        "amount": { "type": "string", "convert": true },
      },
      "handler": transactionFlutterWaveEvents.refund,
    },
    "transaction.flutterwave.instantBooking": {
      "handler": transactionFlutterWaveEvents.instantBooking,
    },
    "transaction.flutterwave.tripEndCardCharge": {
      "handler": transactionFlutterWaveEvents.tripEndCardCharge,
    },
    "transaction.flutterwave.withdraw": {
      "params": {
        "userType": { "type": "string", "convert": "true", "min": 1 },
        "userId": { "type": "string", "min": 1 },
        "amount": { "type": "string", "convert": true, "min": "1" },
      },
      "handler": transactionFlutterWaveEvents.withdraw,
    },
    "transaction.flutterwave.verifyBankAccount": {
      "handler": transactionFlutterWaveEvents.verifyBankAccount,
    },
    "transaction.flutterwave.adminRefundAction": {
      "handler": transactionFlutterWaveEvents.adminRefundAction,
    },
    "transaction.flutterwave.adminTransfer": {
      "handler": transactionFlutterWaveEvents.adminTransfer,
    },
    // webhook
    "transaction.flutterwave.webhooking": {
      "handler": transactionFlutterWaveEvents.webhooking,
    },
    // // V2
    "transaction.flutterwave.checkValidCardV3": {
      "params": transactionValidation.flutterWave.checkValidCard,
      "handler": transactionFlutterWaveEvents.checkValidCardV3,
    },
    "transaction.flutterwave.walletRechargeV3": {
      "handler": transactionFlutterWaveEvents.walletRechargeV3,
    },
    "transaction.flutterwave.refundV3": {
      "params": {
        "transactionId": { "type": "string", "convert": true },
        "amount": { "type": "string", "convert": true },
      },
      "handler": transactionFlutterWaveEvents.refundV3,
    },
    // "transaction.flutterwave.instantBooking": {
    //   "handler": transactionFlutterWaveEvents.instantBooking,
    // },
    // "transaction.flutterwave.tripEndCardCharge": {
    //   "handler": transactionFlutterWaveEvents.tripEndCardCharge,
    // },
    // "transaction.flutterwave.withdraw": {
    //   "params": {
    //     "userType": { "type": "string", "convert": "true", "min": 1 },
    //     "userId": { "type": "string", "min": 1 },
    //     "amount": { "type": "string", "convert": true, "min": "1" },
    //   },
    //   "handler": transactionFlutterWaveEvents.withdraw,
    // },
    // "transaction.flutterwave.verifyBankAccount": {
    //   "handler": transactionFlutterWaveEvents.verifyBankAccount,
    // },
    // "transaction.flutterwave.adminRefundAction": {
    //   "handler": transactionFlutterWaveEvents.adminRefundAction,
    // },
    // "transaction.flutterwave.adminTransfer": {
    //   "handler": transactionFlutterWaveEvents.adminTransfer,
    // },
    // webhook
    "transaction.flutterwave.webhookingV3": {
      "handler": transactionFlutterWaveEvents.webhookingV3,
    },
    //#endregion FLUTTERWAVE
    //#region STRIPE Payment
    "transaction.stripe.checkValidCard": {
      "handler": transactionStripeEvents.checkValidCard,
    },
    "transaction.stripe.refundsAmount": {
      "handler": transactionStripeEvents.refundsAmount,
    },
    "transaction.stripe.newRechargeWalletTransaction": {
      "handler": transactionStripeEvents.newRechargeWalletTransaction,
    },
    "transaction.stripe.newRechargeWalletTransactionUsingPixkey": {
      "handler":
        transactionStripeEvents.newRechargeWalletTransactionUsingPixkey,
    },
    "transaction.stripe.newWithdrawalWalletTransaction": {
      "handler": transactionStripeEvents.newWithdrawalWalletTransaction,
    },
    "transaction.stripe.tripEndCardCharge": {
      "handler": transactionStripeEvents.tripEndCardCharge,
    },
    "transaction.stripe.verifyBankAccount": {
      "handler": transactionStripeEvents.verifyBankAccount,
    },
    // webhook
    "transaction.stripe.webhooking": {
      "handler": transactionStripeEvents.webhooking,
    },
    "transaction.stripe.createPaymentIntents": {
      "handler": transactionStripeEvents.createPaymentIntents,
    },
    "transaction.stripe.capturePaymentIntentsTransaction": {
      "handler": transactionStripeEvents.capturePaymentIntentsTransaction,
    },
    "transaction.stripe.cancelCapturePaymentIntents": {
      "handler": transactionStripeEvents.cancelCapturePaymentIntents,
    },
    "transaction.stripe.cancelCaptureAndCreatePaymentTransaction": {
      "handler":
        transactionStripeEvents.cancelCaptureAndCreatePaymentTransaction,
    },
    "transaction.stripe.updateCaptureAmountAndCapturePaymentTransaction": {
      "handler":
        transactionStripeEvents.updateCaptureAmountAndCapturePaymentTransaction,
    },
    //#endregion  STRIPE Payment
    //razorpay
    "transaction.razorpay.checkValidCard": {
      "handler": transactionRazorpayEvents.checkValidCard,
    },
    "transaction.razorpay.refundsAmount": {
      "handler": transactionRazorpayEvents.refundsAmount,
    },
    "transaction.razorpay.newRechargeWalletTransaction": {
      "handler": transactionRazorpayEvents.newRechargeWalletTransaction,
    },
    "transaction.razorpay.createOrderPaymentForBooking": {
      "handler": transactionRazorpayEvents.createOrderPaymentForBooking,
    },
    "transaction.razorpay.capturePaymentIntentsTransaction": {
      "handler": transactionRazorpayEvents.capturePaymentIntentsTransaction,
    },
    "transaction.razorpay.capturePaymentIntentsAndRefund": {
      "handler": transactionRazorpayEvents.capturePaymentIntentsAndRefund,
    },
    //Tigo
    "transaction.tigo.newRechargeWalletTransaction": {
      "handler": transactionTigoEvents.newRechargeWalletTransaction,
    },
    "transaction.tigoWebhooking": {
      "params": {
        "userType": { "type": "string", "convert": "true", "min": 1 },
        "userId": { "type": "string", "optional": true, "convert": "true" },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": "true",
        },
        "transactionAmount": { "type": "number", "convert": "true" },
        "transactionStatus": { "type": "string", "convert": "true", "min": 1 },
        "transactionId": { "type": "string", "convert": true },
      },
      "handler": transactionEvent.tigoWebhooking,
    },
    //#region CyberSource
    "transaction.cybersource.newRechargeWalletTransaction": {
      "handler": transactionCyberSourceEvents.newRechargeWalletTransaction,
    },
    "transaction.cyberSource.webhooking": {
      "params": {
        "userType": { "type": "string", "convert": "true", "min": 1 },
        "userId": { "type": "string", "optional": true, "convert": "true" },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": "true",
        },
        "transactionAmount": { "type": "number", "convert": "true" },
        "transactionStatus": { "type": "string", "convert": "true", "min": 1 },
        "transactionId": { "type": "string", "convert": true },
      },
      "handler": transactionCyberSourceEvents.cyberSourceWebhooking,
    },
    "transaction.cyberSource.checkTransactionStatus": {
      "params": {
        "userType": { "type": "string", "convert": "true", "min": 1 },
        "userId": { "type": "string", "optional": true, "convert": "true" },
        "professionalId": {
          "type": "string",
          "optional": true,
          "convert": "true",
        },
        "transactionId": { "type": "string", "convert": true },
      },
      "handler":
        transactionCyberSourceEvents.checkCyberSourceTransactionStatusUsingTransactionId,
    },
    //#endregion CyberSource
    //#region Zcreditapiary
    "transaction.zcreditapiary.checkValidCard": {
      "handler": transactionZcreditapiaryEvents.checkValidCard,
    },
    "transaction.zcreditapiary.newRechargeWalletTransaction": {
      "handler": transactionZcreditapiaryEvents.newRechargeWalletTransaction,
    },
    "transaction.zcreditapiary.createPaymentIntents": {
      "handler": transactionZcreditapiaryEvents.createPaymentIntents,
    },
    "transaction.zcreditapiary.capturePaymentIntentsTransaction": {
      "handler":
        transactionZcreditapiaryEvents.capturePaymentIntentsTransaction,
    },
    "transaction.zcreditapiary.cancelCapturePaymentIntents": {
      "handler": transactionZcreditapiaryEvents.cancelCapturePaymentIntents,
    },
    "transaction.zcreditapiary.tripEndCardCharge": {
      "handler": transactionZcreditapiaryEvents.tripEndCardCharge,
    },
    //#endregion Zcreditapiary
    //#region Peach
    "transaction.peach.checkValidCard": {
      "handler": transactionPeachEvents.checkValidCard,
    },
    "transaction.peach.refundsAmount": {
      "handler": transactionPeachEvents.refundsAmount,
    },
    "transaction.peach.newRechargeWalletTransaction": {
      "handler": transactionPeachEvents.newRechargeWalletTransaction,
    },
    "transaction.peach.createPaymentIntents": {
      "handler": transactionPeachEvents.createPaymentIntents,
    },
    // "transaction.peach.createOrderPaymentForBooking": {
    //   "handler": transactionPeachEvents.createOrderPaymentForBooking,
    // },
    "transaction.peach.tripEndCardCharge": {
      "handler": transactionPeachEvents.tripEndCardCharge,
    },
    "transaction.peach.cancelCapturePaymentIntents": {
      "handler": transactionPeachEvents.cancelCapturePaymentIntents,
    },
    "transaction.peach.capturePaymentIntentsAndRefund": {
      "handler": transactionPeachEvents.capturePaymentIntentsAndRefund,
    },
    "transaction.peach.validateCardRegistrationStatus": {
      // "params": {
      //   "userType": { "type": "string", "convert": "true", "min": 1 },
      //   "userId": { "type": "string", "optional": true, "convert": "true" },
      //   "professionalId": {
      //     "type": "string",
      //     "optional": true,
      //     "convert": "true",
      //   },
      //   "cardTokenId": { "type": "string", "convert": true },
      // },
      "handler": transactionPeachEvents.validateCardRegistrationStatus,
    },
    "transaction.peach.newWithdrawalWalletTransaction": {
      "handler": transactionPeachEvents.newWithdrawalWalletTransaction,
    },
    //#endregion Peach
    //#region mercadopago
    "transaction.mercadopago.checkValidCard": {
      "handler": transactionMercadopagoEvents.checkValidCard,
    },
    "transaction.mercadopago.updateCardToken": {
      "handler": transactionMercadopagoEvents.updateCardToken,
    },
    "transaction.mercadopago.refundsAmount": {
      "handler": transactionMercadopagoEvents.refundsAmount,
    },
    "transaction.mercadopago.newRechargeWalletTransaction": {
      "handler": transactionMercadopagoEvents.newRechargeWalletTransaction,
    },
    "transaction.mercadopago.createPaymentIntents": {
      "handler": transactionMercadopagoEvents.createPaymentIntents,
    },
    // // "transaction.peach.createOrderPaymentForBooking": {
    // //   "handler": transactionPeachEvents.createOrderPaymentForBooking,
    // // },
    "transaction.mercadopago.tripEndCardCharge": {
      "handler": transactionMercadopagoEvents.tripEndCardCharge,
    },
    "transaction.mercadopago.cancelCapturePaymentIntents": {
      "handler": transactionMercadopagoEvents.cancelCapturePaymentIntents,
    },
    "transaction.mercadopago.capturePaymentIntentsTransaction": {
      "handler": transactionMercadopagoEvents.capturePaymentIntentsTransaction,
    },
    // "transaction.peach.capturePaymentIntentsAndRefund": {
    //   "handler": transactionPeachEvents.capturePaymentIntentsAndRefund,
    // },
    "transaction.mercadopago.verifyCVVByPaymentGatway": {
      // "params": {
      //   "userType": { "type": "string", "convert": "true", "min": 1 },
      //   "userId": { "type": "string", "optional": true, "convert": "true" },
      //   "professionalId": {
      //     "type": "string",
      //     "optional": true,
      //     "convert": "true",
      //   },
      //   "cardTokenId": { "type": "string", "convert": true },
      // },
      "handler": transactionMercadopagoEvents.verifyCVVByPaymentGatway,
    },
    "transaction.mercadopago.newWithdrawalWalletTransaction": {
      "handler": transactionMercadopagoEvents.newWithdrawalWalletTransaction,
    },
    "transaction.mercadopago.verifyBankAccount": {
      "handler": transactionMercadopagoEvents.verifyBankAccount,
    },
    //#endregion mercadopago
    //#endregion Payment Gatway
    //#region Teya Payment
    "transaction.teya.checkValidCard": {
      "handler": transactionTeyaEvents.checkValidCard,
    },
    // "transaction.teya.refundsAmount": {
    //   "handler": transactionTeyaEvents.refundsAmount,
    // },
    "transaction.teya.newRechargeWalletTransaction": {
      "handler": transactionTeyaEvents.newRechargeWalletTransaction,
    },
    // "transaction.teya.newRechargeWalletTransactionUsingPixkey": {
    //   "handler": transactionTeyaEvents.newRechargeWalletTransactionUsingPixkey,
    // },
    // "transaction.teya.newWithdrawalWalletTransaction": {
    //   "handler": transactionTeyaEvents.newWithdrawalWalletTransaction,
    // },
    "transaction.teya.tripEndCardCharge": {
      "handler": transactionTeyaEvents.tripEndCardCharge,
    },
    // "transaction.teya.verifyBankAccount": {
    //   "handler": transactionTeyaEvents.verifyBankAccount,
    // },
    // // webhook
    // "transaction.teya.webhooking": {
    //   "handler": transactionTeyaEvents.webhooking,
    // },
    "transaction.teya.createPaymentIntents": {
      "handler": transactionTeyaEvents.createPaymentIntents,
    },
    // "transaction.teya.capturePaymentIntentsTransaction": {
    //   "handler": transactionTeyaEvents.capturePaymentIntentsTransaction,
    // },
    "transaction.teya.captureAndRefundPaymentIntentsTransaction": {
      "handler":
        transactionTeyaEvents.captureAndRefundPaymentIntentsTransaction,
    },
    "transaction.teya.cancelCapturePaymentIntents": {
      "handler": transactionTeyaEvents.cancelCapturePaymentIntents,
    },
    // "transaction.teya.cancelCaptureAndCreatePaymentTransaction": {
    //   "handler": transactionTeyaEvents.cancelCaptureAndCreatePaymentTransaction,
    // },
    // "transaction.teya.updateCaptureAmountAndCapturePaymentTransaction": {
    //   "handler":
    //     transactionTeyaEvents.updateCaptureAmountAndCapturePaymentTransaction,
    // },
    //#endregion Teya Payment
    //#region Point Redeem
    "transaction.teleport.redeemRewardsPointToTeleportAirtime": {
      "params": {
        "redeemRewardsPointValue": { "type": "number", "convert": "true" },
        "phoneNumber": { "type": "string", "convert": "true", "min": 1 },
        "transactionReason": { "type": "string", "convert": "true", "min": 1 },
      },
      "handler": transactionTeleportEvents.redeemRewardsPointToTeleportAirtime,
    },
    //#endregion Point Redeem

    // membership transaction
    "transaction.membershipAddingTransaction": {
      "params": {
        "transactionAmount": { "type": "string", "convert": true },
        "from": {
          "type": "object",
          "props": {
            "userType": {
              "type": "enum",
              "values": [
                constantUtil.USER,
                constantUtil.PROFESSIONAL,
                constantUtil.ADMIN,
              ],
            },
            "userId": {
              "type": "string",
              "min": 12,
              "convert": true,
            },
          },
        },
        "to": {
          "type": "object",
          "props": {
            "userType": {
              "type": "enum",
              "values": [
                constantUtil.USER,
                constantUtil.PROFESSIONAL,
                constantUtil.ADMIN,
              ],
            },
            "userId": {
              "type": "string",
              "min": 12,
              "convert": true,
            },
          },
        },
        "paymentType": {
          "type": "enum",
          "values": [
            constantUtil.CREDIT,
            constantUtil.DEBIT,
            constantUtil.PAYMENTGATEWAY,
            constantUtil.CASH,
            constantUtil.WALLET,
          ],
        },
        "gatewayName": {
          "type": "enum",
          "values": [constantUtil.FLUTTERWAVE, constantUtil.STRIPE],
          "optional": true,
        },
        "transactionType": {
          "type": "string",
          "values": [
            constantUtil.WALLETRECHARGE,
            constantUtil.WALLETWITHDRAWAL,
            constantUtil.SENDMONEYTOFRIEND,
            constantUtil.BOOKINGCHARGE,
            constantUtil.ADMINRECHARGE,
            constantUtil.ADMINDEBIT,
            constantUtil.SUBSCRIPTIONCHARGE,
            constantUtil.CANCELLATIONCHARGE,
            constantUtil.PROFESSIONALEARNINGS,
            constantUtil.JOININGCHARGE,
            constantUtil.INVITECHARGE,
            constantUtil.CARDAMOUNTREFUND,
            constantUtil.EXTRAAMOUNTDEBIT,
            constantUtil.PROFESSIONALTOLERENCEAMOUNT,
            constantUtil.VERIFYCARD,
            constantUtil.MEMBERSHIPPLAN,
          ],
          "optional": true,
        },
        "gatewayDocId": {
          "type": "string",
          "min": 12,
          "convert": true,
          "optional": true,
        },
        "membershipPlanId": {
          "type": "string",
          "min": 12,
          "convert": true,
        },
      },
      "handler": transactionEvent.membershipAddingTransaction,
    },
    "transaction.insertTransactionBasedOnUserType": {
      // "params": {
      //   "transactionType": { "type": "string", "min": 1 },
      //   "transactionAmount": { "type": "number", "convert": "true" },
      //   "transactionReason": { "type": "string", "convert": "true" },
      //   "transactionStatus": { "type": "string", "convert": "true", "min": 1 },
      //   "transactionId": { "type": "string", "optional": true },
      //   "paymentType": {
      //     "type": "enum",
      //     "values": [constantUtil.CREDIT, constantUtil.DEBIT],
      //   },
      //   "paymentGateWay": {
      //     "type": "string",
      //     "convert": "true",
      //     "optional": true,
      //   },
      //   "gatewayResponse": { "type": Object },
      //   "userType": {
      //     "type": "enum",
      //     "values": [
      //       constantUtil.USER,
      //       constantUtil.PROFESSIONAL,
      //       constantUtil.ADMIN,
      //     ],
      //   },
      //   "userId": {
      //     "type": "string",
      //     "min": 12,
      //     "convert": true,
      //     "optional": true,
      //   },
      //   "professionalId": {
      //     "type": "string",
      //     "min": 12,
      //     "convert": true,
      //     "optional": true,
      //   },
      //   "paymentToUserType": constantUtil.ADMIN,
      //   "paymentToUserId": {
      //     "type": "string",
      //     "min": 12,
      //     "convert": true,
      //     "optional": true,
      //   },
      // },
      "handler": transactionEvent.insertTransactionBasedOnUserType,
    },
    "transaction.hubSiteCommissionTransaction": {
      "params": {
        "bookingId": { "type": "string", "convert": true },
        "refBookingId": { "type": "string", "convert": true },
        "hubId": { "type": "string", "convert": true },
        "hubData": { "type": "object" },
        "amount": { "type": "number", "convert": true },
      },
      "handler": transactionEvent.hubSiteCommissionTransaction,
    },
    "transaction.corporateRideWalletPayTransaction": {
      "params": {
        "bookingId": { "type": "string", "convert": true },
        "refBookingId": { "type": "string", "convert": true },
        "corporateId": { "type": "string", "convert": true },
        "corporateData": { "type": "object" },
        "amount": { "type": "number", "convert": true },
      },
      "handler": transactionEvent.corporateRideWalletPayTransaction,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};

// // only testing purpose
// transactionservice.actions.ENDBOOKINGCHARGE =
//   transactionEvent.tripEndCardCharge;
// export

module.exports = transactionservice;
