"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const subscriptionValidation = require("../validations/subscription.validation");
const subscriptionAction = require("../actions/subscription.action");
const subscriptionEvent = require("../events/subscription.event");

module.exports = {
  "name": "subscription",
  "mixins": [DBMixin(constantUtil.DBSCHEMASUBSCRIPTION)],
  "settings": {},
  "dependencies": [],
  "actions": {
    "getAllSubscription": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": subscriptionAction.getAllSubscription,
    },
    "addSubscriptionPlan": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": subscriptionValidation.addSubscriptionPlan,
      "handler": subscriptionAction.addSubscriptionPlan,
    },
    "getSubscriptionPlanDetail": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": subscriptionValidation.addSubscriptionPlan,
      "handler": subscriptionAction.getSubscriptionPlanDetail,
    },
    "getAllServiceList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": subscriptionAction.getAllServiceList,
    },
  },
  "events": {
    "subscription.getSubscriptionList": {
      "handler": subscriptionEvent.getSubscriptionList,
    },
    "subscription.getEditSubscription": {
      "handler": subscriptionEvent.getEditSubscription,
    },
    "subscription.changeSubscriptionStatus": {
      "handler": subscriptionEvent.changeSubscriptionStatus,
    },
    "subscription.updateSubscription": {
      "handler": subscriptionEvent.updateSubscription,
    },
  },
  "methods": {},
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {},
  async stopped() {},
};
