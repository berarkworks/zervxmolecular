"use strict";

const DBMixin = require("../mixins/db.mixin");
const constantUtil = require("../utils/constant.util");
const adminEvent = require("../events/admin.event");
const adminMethod = require("../methods/admin.method");
const adminValidation = require("../validations/admin.validation");
const adminAction = require("../actions/admin.action");

const { sendWhatsupAlert } = require("../utils/smsAndCall.util");

module.exports = {
  "name": "admin",
  "mixins": [DBMixin(constantUtil.DBSCHEMAADMIN)],
  //"userModel": DBMixin(constantUtil.DBSCHEMAUSERS),
  "settings": {},
  "dependencies": [],

  "actions": {
    // "NEW": {
    //   "handler": async function (context) {
    //     console.log(context);
    //     const data = this.adapter.model.create({
    //       "title": "ai",
    //       "decription": "djdj",
    //     });
    //     console.log(data);
    //   },
    // },
    "reactConfig": {
      "access": [constantUtil.ADMIN],
      "handler": adminAction.reactConfig,
    },
    "reactConfigWebbooking": {
      "access": [constantUtil.ADMIN],
      "params": { "clientSecretKey": { "type": "string", "optional": true } },
      "handler": adminAction.reactConfigWebbooking,
    },
    "getCommonConfig": {
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": adminAction.getCommonConfig,
    },
    "getLanguageKeys": {
      "access": [constantUtil.ADMIN],
      "params": adminValidation.languageCodeValidation,
      "handler": adminAction.getLanguageKeys,
    },
    "setUserBasedLanguage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.languageCodeValidation,
      "handler": adminAction.setUserBasedLanguage,
    },

    "loginValidation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.loginValidation,
    },
    "loginWithOtp": {
      "params": adminValidation.loginWithOtp,
      "handler": adminAction.loginWithOtp,
    },
    "verifyOtp": {
      "params": adminValidation.verifyOtp,
      "handler": adminAction.verifyOtp,
    },
    "getProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getProfile,
    },
    "updateProfile": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateProfile,
      "handler": adminAction.updateProfile,
    },
    "changePassword": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "id": { "type": "string", "min": 8 },
        "oldPassword": { "type": "string", "min": 6 },
        "newPassword": { "type": "string", "min": 6 },
      },
      "handler": adminAction.changePassword,
    },
    "getGenerelConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": adminAction.getGenerelConfig,
    },
    "getServiceAreaConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "handler": adminAction.getServiceAreaConfig,
    },
    "getMapConfig": {
      // "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": {
        "clientId": { "type": "string", "min": 8 },
      },
      "handler": adminAction.getMapConfig,
    },
    "getZervxServices": {
      // "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getZervxServices,
    },
    "getServiceTypeById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getServiceTypeById,
    },
    "manageClientZervxServices": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.manageClientZervxServices,
    },
    "uploadGeneralImages": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.uploadGeneralImages,
    },
    "updateGenerelConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateGenerelConfig,
      "handler": adminAction.updateGenerelConfig,
    },
    "updateAppConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateAppConfig,
      "handler": adminAction.updateAppConfig,
    },
    "updateMapConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateMapConfig,
      "handler": adminAction.updateMapConfig,
    },
    "updatePackageConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePackageConfig,
      "handler": adminAction.updatePackageConfig,
    },
    "updateFirebaseConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateFirebaseConfig,
      "handler": adminAction.updateFirebaseConfig,
    },
    "updateBlockProfessionalConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateBlockProfessionalConfig,
      "handler": adminAction.updateBlockProfessionalConfig,
    },
    //SMS
    "getSMSConfigList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getSMSConfigList,
    },
    "getSMSConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": { "id": { "type": "string", "optional": true, "min": 8 } },
      "handler": adminAction.getSMSConfig,
    },
    "updateSMSConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateSMSConfig,
      "handler": adminAction.updateSMSConfig,
    },
    "changeSMSConfigActiveStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "id": { "type": "string", "optional": true, "min": 8 },
        "status": { "type": "string", "optional": true },
      },
      "handler": adminAction.changeSMSConfigActiveStatus,
    },
    "getSMTPConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getSMTPConfig,
    },
    "updateSMTPConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateSMTPConfig,
      "handler": adminAction.updateSMTPConfig,
    },
    //VEHICLE CATEGORY SERVICE APIS
    "getVehicleCategoryList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getVehicleCategoryList,
    },
    "updateVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateVehicleCategory,
      "handler": adminAction.updateVehicleCategory,
    },
    "editVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.editVehicleCategory,
    },
    "changeVehicleCategoryStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeVehicleCategoryStatus,
    },
    "getAllVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllVehicleCategory,
    },
    "getEagleVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getEagleVehicleCategory,
    },
    "getCityBasedVehicleCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getCityBasedVehicleCategory,
      "handler": adminAction.getCityBasedVehicleCategory,
    },
    "getActiveServiceCategoryBasedOnServiceAreaId": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "city": { "type": "string", "min": 1 },
      },
      "handler": adminAction.getActiveServiceCategoryBasedOnServiceAreaId,
    },
    //LOCATION SERVICE APIS
    "updateServiceCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateServiceCategory,
      "handler": adminAction.updateServiceCategory,
    },
    "manageSpecialDays": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.manageSpecialDays,
      "handler": adminAction.manageSpecialDays,
    },
    "getAllServiceMultiPoint": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllServiceMultiPoint,
    },
    "getAllServiceCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllServiceCategory,
    },
    "getAllServiceActiveInactiveCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllServiceActiveInactiveCategory,
    },
    "getServiceCategoryList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getServiceCategoryList,
    },
    "getServiceCategoryById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getServiceCategoryById,
    },
    "changeServiceCategoryStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeServiceCategoryStatus,
    },
    "getServiceBasedLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getServiceBasedLocation,
    },
    "getEagleServiceCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getEagleServiceCategory,
    },
    "updateCategoryFareBreakup": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.updateCategoryFareBreakup,
    },
    "getCategoryFareBreakup": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCategoryFareBreakup,
    },
    "getCategoryFareBreakupImages": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCategoryFareBreakupImages,
    },
    //PAYMENT GATEWAY SERVICE APIS
    "getAllPaymentGateway": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getAllPaymentGateway,
    },
    "updatePaymentGateway": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePaymentGateway,
      "handler": adminAction.updatePaymentGateway,
    },
    "getEditPaymentGateway": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditPaymentGateway,
    },
    "changePaymentGatewayStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changePaymentGatewayStatus,
    },
    "userGetAvailableCurrencies": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": adminValidation.userGetAvailableCurrencies,
      "handler": adminAction.userGetAvailableCurrencies,
    },
    //OPERATORS AND OPERATOR ROLES SERVICE APIS
    "getOperatorsRoleList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getOperatorsRoleList,
    },
    "getAllOperatorsRoles": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllOperatorsRoles,
    },
    "updateOperatorsRole": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateOperatorsRole,
      "handler": adminAction.updateOperatorsRole,
    },
    "getOperatorRole": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getOperatorRole,
    },
    "changeOperatorsRoleStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeOperatorsRoleStatus,
    },
    "getOperatorsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getOperatorsList,
    },
    "updateOperator": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateOperator,
      "handler": adminAction.updateOperator,
    },
    "getOperator": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getOperator,
    },
    "changeOperatorStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeOperatorStatus,
    },
    //RESPONSE OFFICE SERVICE APIS
    "updateResponseOfficePrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePrivileges,
      "handler": adminAction.updateResponseOfficePrivileges,
    },
    "getResponseOfficePrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getResponseOfficePrivileges,
    },
    "getResponseOfficeList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getResponseOfficeList,
    },
    "updateResponseOffice": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateResponseOffice,
      "handler": adminAction.updateResponseOffice,
    },
    "getResponseOffice": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getResponseOffice,
    },
    "changeResponseOfficeStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeResponseOfficeStatus,
    },
    "getAllResponseOffices": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllResponseOffices,
    },
    //DOCUMENTS SERVICE APIS
    "getDocumentsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getDocumentsList,
    },
    "getEditDocument": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditDocument,
    },
    "updateDocument": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateDocument,
      "handler": adminAction.updateDocument,
    },
    "changeDocumentStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeDocumentStatus,
    },
    // PROFESSIONAL DOCUMENT VERIFY
    "expireDocumentsProfessionlUnverified": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.expireDocumentsProfessionlUnverified,
    },
    //CANCELLATION REASONS SERVICE APIS
    "getCancellationReasonsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getCancellationReasonsList,
    },
    // "getEditCancellationReason": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": adminValidation.getEdit,
    //   "handler": adminAction.getEditCancellationReason,
    // },
    "updateCancellationReason": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateCancellationReason,
      "handler": adminAction.updateCancellationReason,
    },
    "changeCancellationReasonStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeCancellationReasonStatus,
    },
    //HUBS SERVICE APIS
    "updateHubsPrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePrivileges,
      "handler": adminAction.updateHubsPrivileges,
    },
    "getHubsPrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getHubsPrivileges,
    },
    "getHubsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getHubsList,
    },
    "getEditHub": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditHub,
    },
    "updateHubs": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateHubs,
      "handler": adminAction.updateHubs,
    },
    "changeHubsStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeHubsStatus,
    },
    "addWalletAmount": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.addWalletAmount,
      "handler": adminAction.addWalletAmount,
    },
    "getHubEmployeeList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getHubEmployeeList,
      "handler": adminAction.getHubEmployeeList,
    },
    "updateHubsEmployee": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateHubsEmployee,
      "handler": adminAction.updateHubsEmployee,
    },
    "getEditHubEmployees": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEditHubEmployees,
      "handler": adminAction.getEditHubEmployees,
    },
    "changeHubEmployeesStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeHubEmployeesStatus,
    },
    // GET DATA USING ONLY ID
    "getDataById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getDataById,
    },
    //VERIFICATION DOCUMENTS SERVICE APIS
    "getVerificationDocumentList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getVerificationDocumentList,
    },
    "getEditVerificationDocument": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditVerificationDocument,
    },
    "updateVerificationDocument": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateVerificationDocument,
      "handler": adminAction.updateVerificationDocument,
    },
    "changeVerificationDocumentStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeVerificationDocumentStatus,
    },
    //USERS SERVICE APIS
    "getUsersList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getUsersList,
    },
    "getEditUser": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditUser,
    },
    "updateUser": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateUser,
      "handler": adminAction.updateUser,
    },
    "getUnregisteredUserList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getUnregisteredUserList,
    },
    "closeUnregisteredUser": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.closeUnregisteredUser,
    },
    "addUnregisteredUserNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.addUnregisteredUserNotes,
    },
    "changeUsersStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeUsersStatus,
    },
    "changeUnregisteredUsersStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeUnregisteredUsersStatus,
    },
    //PROFESSIONAL SERVICE APIS
    "getUnregisteredProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getUnregisteredProfessionalList,
    },
    "closeUnregisteredProfessional": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.closeUnregisteredProfessional,
    },
    "addUnregisteredProfessionalNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.addUnregisteredProfessionalNotes,
    },
    "addUnregisteredProfessionalStatusChange": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.addUnregisteredProfessionalStatusChange,
    },
    "getProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getProfessionalList,
    },
    "getEditProfessional": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditProfessional,
    },
    "updateProfessionalStep1": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateProfessionalStep1,
      "handler": adminAction.updateProfessionalStep1,
    },
    "getAllDocumentForUpload": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllDocumentForUpload,
    },
    "updateProfessionalStep2": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateProfessionalStep2,
      "handler": adminAction.updateProfessionalStep2,
    },
    "getHubListByLocation": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": { "serviceArea": { "type": "string", "min": 8 } },
      "handler": adminAction.getHubListByLocation,
    },
    "updateProfessionalStep3": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateProfessionalStep3,
      "handler": adminAction.updateProfessionalStep3,
    },
    "updateProfessionalStep4": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateProfessionalStep4,
      "handler": adminAction.updateProfessionalStep4,
    },
    "changeProfessionalsStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeProfessionalsStatus,
    },
    "updateBankDetailsProfessional": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateBankDetailsProfessional,
      "handler": adminAction.updateBankDetailsProfessional,
    },
    "changeVehicleStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeVehicleStatus,
      "handler": adminAction.changeVehicleStatus,
    },
    "professionalProfileDocumentVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.professionalProfileDocumentVerification,
      "handler": adminAction.professionalProfileDocumentVerification,
    },
    "deleteProfessionalDocument": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
        "documentId": { "type": "string", "min": 8 },
        "documentType": {
          "type": "enum",
          "values": [
            constantUtil.PROFILEDOCUMENTS,
            constantUtil.DRIVERDOCUMENTS,
          ],
        },
      },
      "handler": adminAction.deleteProfessionalDocument,
    },
    "professionalVehicleDocumentVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.professionalVehicleDocumentVerification,
      "handler": adminAction.professionalVehicleDocumentVerification,
    },
    "addProfessionalNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
        "notes": { "type": "string", "min": 2 },
      },
      "handler": adminAction.addProfessionalNotes,
    },
    "addVehicleNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
        "notes": { "type": "string", "min": 2 },
      },
      "handler": adminAction.addVehicleNotes,
    },
    "professionalDocumentExpiryNotify": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
      },
      "handler": adminAction.professionalDocumentExpiryNotify,
    },
    "getVehicleList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getVehicleList,
      "handler": adminAction.getVehicleList,
    },
    "editVehicleDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.editVehicleDetails,
      "handler": adminAction.editVehicleDetails,
    },
    "professionalVerification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
      },
      "handler": adminAction.professionalVerification,
    },
    "getUnverifiedProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getUnverifiedProfessionalList,
    },
    "getExpiredProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getExpiredProfessionalList,
    },

    // USER AND PROFESSIONAL COMPAINED APIS
    "getCategoryList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getCategoryList,
      "handler": adminAction.getCategoryList,
    },
    //LANGUAGES SERVICE APIS
    "getLanguagesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getLanguagesList,
    },
    "getAllLanguages": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getAllLanguages,
    },
    "getEditLanguage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditLanguage,
    },
    "updateLanguage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateLanguage,
      "handler": adminAction.updateLanguage,
    },
    "changeLanguageStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeLanguageStatus,
    },
    "getDefaultLanguage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getDefaultLanguage,
    },
    "updateTranslateKeys": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateTranslateKeys,
      "handler": adminAction.updateTranslateKeys,
    },
    "setDefaultLanguage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.setDefaultLanguage,
    },
    //SECURITY SERVICE APIS
    "getSecurityServiceList": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
        constantUtil.ADMIN,
      ],
      "handler": adminAction.getSecurityServiceList,
    },
    "updateSecurityServiceList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateSecurityServiceList,
      "handler": adminAction.updateSecurityServiceList,
    },
    //RIDE SERVICE APIS
    "viewRideDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": { "bookingId": { "type": "string" } },
      "handler": adminAction.viewRideDetails,
    },
    "getNewRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getNewRidesList,
      "handler": adminAction.getNewRidesList,
    },
    "getGuestRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getGuestRidesList,
    },
    "getManualMeterRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getManualMeterRidesList,
    },
    "getCoorperateRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getCoorperateRidesList,
      "handler": adminAction.getCoorperateRidesList,
    },
    "getScheduleRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getScheduleRidesList,
    },
    "getOngoingRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getOngoingRidesList,
    },
    "getEndedRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getEndedRidesList,
    },
    "getEndRideIssueList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getEndRideIssueList,
    },
    "updateBookingInfo": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.updateBookingInfo,
    },

    "getExpiredRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getExpiredRidesList,
    },
    "getUserCancelledRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getUserCancelledRidesList,
    },
    "getProfessionalCancelledRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getProfessionalCancelledRidesList,
    },
    "paymentFailedIssueList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.paymentFailedIssueList,
    },
    "getSearchRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getSearchRidesList,
    },
    "getAssignProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
      },
      "handler": adminAction.getAssignProfessionalList,
    },
    "sendRequstToProfessional": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "bookingId": { "type": "string", "min": 1 },
        "professionalId": { "type": "string", "min": 1 },
        "actionType": {
          "type": "enum",
          "values": [
            constantUtil.ACTION_ASSIGN,
            constantUtil.ACTION_REASSIGN,
            constantUtil.ACTION_FORCEASSIGN,
          ],
        },
      },
      "handler": adminAction.sendRequstToProfessional,
    },
    //RESPONSE OFFICERS SERVICE APIS
    "getOfficerList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getOfficerList,
    },
    "getEditOfficer": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditOfficer,
    },
    "changeOfficerStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeOfficerStatus,
    },
    "updateOfficer": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateOfficer,
      "handler": adminAction.updateOfficer,
    },
    //SUBSCRIPTION SERVICE APIS
    "getSubscriptionList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getSubscriptionList,
    },
    "getEditSubscription": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditSubscription,
    },
    "changeSubscriptionStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeSubscriptionStatus,
    },
    "updateSubscription": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateSubscription,
      "handler": adminAction.updateSubscription,
    },
    // DASHBOARD
    "getOperatorData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getOperatorData,
      "handler": adminAction.getOperatorData,
    },
    "getHubsData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getHubsData,
      "handler": adminAction.getHubsData,
    },
    "getHubsBasedCityData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getHubsBasedCityData,
      "handler": adminAction.getHubsBasedCityData,
    },
    "getNotificationData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getNotificationData,
    },
    // GODS VIEW
    "getLocationProfessionalList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getLocationProfessionalList,
      "handler": adminAction.getLocationProfessionalList,
    },
    "getAllRidesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getAllRidesList,
      "handler": adminAction.getAllRidesList,
    },
    "getProfessionalsAndServiceArea": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getProfessionalsAndServiceArea,
      "handler": adminAction.getProfessionalsAndServiceArea,
    },
    // EARNINGS MANAGEMENT
    "fetchBillingCycle": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.fetchBillingCycle,
    },
    "fetchCoorperateBillingCycle": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.fetchCoorperateBillingCycle,
    },
    // WALLET AMOUNT MANAGEMENT
    "getWalletAmountList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getWalletAmountList,
      "handler": adminAction.getWalletAmountList,
    },
    // payment gateway refund
    "getRefundDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getRefundDetails,
      "handler": adminAction.getRefundDetails,
    },
    "refundAction": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.refundAction,
      "handler": adminAction.refundAction,
    },
    // payout
    "getWithDrawDetails": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getWithDrawDetails,
      "handler": adminAction.getWithDrawDetails,
    },
    "walletWithDrawManualTransfer": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.walletWithDrawManualTransfer,
      "handler": adminAction.walletWithDrawManualTransfer,
    },

    // TRANSACTION
    "adminGetTransaction": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.adminGetTransaction,
      "handler": adminAction.adminGetTransaction,
    },
    "changeTransactionStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeTransactionStatus,
      "handler": adminAction.changeTransactionStatus,
    },

    //
    // REPORTS MANAGEMENT
    "addReports": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "params": adminValidation.addReports,
      "handler": adminAction.addReports,
    },
    "getReportsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getReportsList,
    },
    "reportChangeStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.reportChangeStatus,
    },
    "addReportsNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.addReportsNotes,
    },
    "viewReports": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.viewReports,
    },
    // SECURITY MANAGEMENT
    "getSecurityList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getSecurityList,
      "handler": adminAction.getSecurityList,
    },
    "getSecurityClosedList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getSecurityClosedList,
      "handler": adminAction.getSecurityClosedList,
    },
    "getSecurityOperatorList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getSecurityOperatorList,
      "handler": adminAction.getSecurityOperatorList,
    },
    "changeSecurityStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.changeSecurityStatus,
    },
    "addSecurityNotes": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.addSecurityNotes,
    },
    "operatorAcceptSecurity": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.operatorAcceptSecurity,
    },
    //INVITEANDEARN
    "updateInviteAndEarn": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateInviteAndEarn,
      "handler": adminAction.updateInviteAndEarn,
    },
    "updateInviteAndEarnImage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.updateInviteAndEarnImage,
    },
    "getInviteAndEarnConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getInviteAndEarnConfig,
    },
    "saveErrorData": {
      "handler": adminAction.saveErrorData,
    },
    //COORPERATE OFFICE SERVICE APIS
    "getCoorperateOfficeList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getCoorperateOfficeList,
    },
    "updateCoorperateOffice": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateCoorperateOffice,
      "handler": adminAction.updateCoorperateOffice,
    },
    "getCoorperateOffice": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getCoorperateOffice,
    },
    "changeCoorperateOfficeStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeCoorperateOfficeStatus,
    },
    "getAllCoorperateOffices": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "requestFrom": { "type": "string", "optional": true },
      },
      // "params": adminValidation.changeStatus,
      "handler": adminAction.getAllCoorperateOffices,
    },
    "updateCoorperateOfficePrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePrivileges,
      "handler": adminAction.updateCoorperateOfficePrivileges,
    },
    "getCoorperateOfficePrivileges": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCoorperateOfficePrivileges,
    },
    "getCoorperateOfficeBillingList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getCoorperateOfficeBillingList,
      "handler": adminAction.getCoorperateOfficeBillingList,
    },
    "changeCoorperatePaidStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeCoorperatePaidStatus,
      "handler": adminAction.changeCoorperatePaidStatus,
    },
    // EMAIL TEMPLATE MANAGEMENT
    "getEmailTemplateList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getEmailTemplateList,
    },
    "updateEmailTemplate": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateEmailTemplate,
      "handler": adminAction.updateEmailTemplate,
    },
    "editEmailTemplate": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.editEmailTemplate,
    },
    "changeEmailTemplateStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeEmailTemplateStatus,
    },
    // POPULAR PLACES MANAGEMENT
    "getPopularPlacesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getListWithCityFilter,
      "handler": adminAction.getPopularPlacesList,
    },
    "updatePopularPlace": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePopularPlace,
      "handler": adminAction.updatePopularPlace,
    },
    "uploadPopularPlaceImage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.uploadPopularPlaceImage,
    },
    "getEditPopularPlace": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditPopularPlace,
    },
    "changePopularPlaceStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changePopularPlaceStatus,
    },
    "getPopularPlaces": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": adminValidation.getPopularPlaces,
      "handler": adminAction.getPopularPlaces,
    },
    "getAllPopularPlacesData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "handler": adminAction.getAllPopularPlacesData,
    },
    // AIRPORT MANAGEMENT
    "getAirportList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getListWithCityFilter,
      "handler": adminAction.getAirportList,
    },
    "updateAirport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateAirport,
      "handler": adminAction.updateAirport,
    },
    "uploadAirportImage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.uploadAirportImage,
    },
    "getEditAirport": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditAirport,
    },
    "changeAirportStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeAirportStatus,
    },
    "getAirportStopList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getAirportStopList,
      "handler": adminAction.getAirportStopList,
    },
    "updateAirportStops": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateAirportStops,
      "handler": adminAction.updateAirportStops,
    },
    "getEditAirportStop": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEditAirportStop,
      "handler": adminAction.getEditAirportStop,
    },
    "changeAirportStopStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeAirportStopStatus,
      "handler": adminAction.changeAirportStopStatus,
    },
    "getAirports": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": adminValidation.getAirports,
      "handler": adminAction.getAirports,
    },
    "getAllAirportData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": adminAction.getAllAirportData,
    },
    "getAllAirportDataForWebBookingApp": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.USER],
      "params": {
        "secretKey": { "type": "string", "convert": true },
      },
      "handler": adminAction.getAllAirportDataForWebBookingApp,
    },
    // AIRPORT TRANSFER SETTINGS

    "getAirportTransferConfig": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      "params": {
        "secretKey": { "type": "string", "convert": true },
      },
      "handler": adminAction.getAirportTransferConfig,
    },
    "updateAirportTransferConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateAirportTransferConfig,
      "handler": adminAction.updateAirportTransferConfig,
    },
    // TOLL CONCEPT
    "getTollList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getListWithCityFilter,
      "handler": adminAction.getTollList,
    },
    "updateToll": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateToll,
      "handler": adminAction.updateToll,
    },
    "getEditToll": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditToll,
    },
    "changeTollStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeTollStatus,
    },
    "getAllTollData": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": adminAction.getAllTollData,
    },
    // COUPON CODE MANAGEMENT
    "getCouponList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getListWithCityFilter,
      "handler": adminAction.getCouponList,
    },
    "updateCoupon": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateCoupon,
      "handler": adminAction.updateCoupon,
    },
    "getEditCoupon": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditCoupon,
    },
    "changeCouponStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeCouponStatus,
    },
    "getUserBasedCouponList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": adminValidation.getUserBasedCouponList,
      "handler": adminAction.getUserBasedCouponList,
    },
    "checkAndApplyManualCoupon": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER],
      "params": adminValidation.checkAndApplyManualCoupon,
      "handler": adminAction.checkAndApplyManualCoupon,
    },
    "checkUniqueReferralCode": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.checkUniqueReferralCode,
      "handler": adminAction.checkUniqueReferralCode,
    },
    // NOTIFICATION MANAGEMENT
    "getNotificationTemplatesList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getNotificationTemplatesList,
      "handler": adminAction.getNotificationTemplatesList,
    },
    "updateNotificationTemplates": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateNotificationTemplates,
      "handler": adminAction.updateNotificationTemplates,
    },
    "getNotificationTemplates": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getNotificationTemplates,
      "handler": adminAction.getNotificationTemplates,
    },
    "changeNotificationTemplateStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeNotificationTemplateStatus,
      "handler": adminAction.changeNotificationTemplateStatus,
    },
    "deleteNotificationTemplateById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": { "id": { "type": "string" } },
      "handler": adminAction.deleteNotificationTemplateById,
    },
    "sendPromotion": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.sendPromotion,
      "handler": adminAction.sendPromotion,
    },
    "getUserListForNotification": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getUserListForNotification,
      "handler": adminAction.getUserListForNotification,
    },
    // POPULAR DESTINATION MANAGEMENT
    "getPopularDestinationList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getPopularDestinationList,
    },
    "updatePopularDestination": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updatePopularDestination,
      "handler": adminAction.updatePopularDestination,
    },
    "getEditPopularDestination": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getEditPopularDestination,
    },
    "changePopularDestinationStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changePopularDestinationStatus,
    },
    // FOR DB ACCESS NO MORE NEEDED
    // "remove0": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "handler": adminAction.remove0,
    // },
    "forceLogout": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.forceLogout,
    },
    // promtion  notificaton
    "getUpdateEmailEditableFields": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getUpdateEmailEditableFields,
      "handler": adminAction.getUpdateEmailEditableFields,
    },
    // developer testing apis
    "fcmTesting": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.fcmTesting,
    },
    // Send Message Using FCm
    "sendMessageToFCM": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.USER, constantUtil.PROFESSIONAL],
      "handler": adminAction.sendMessageToFCM,
    },
    "getFCMAccessTokenUsingSecretKey": {
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      // "params": {
      //   "secretKey": { "type": "string", "convert": true },
      // },
      "handler": adminAction.getFCMAccessTokenUsingSecretKey,
    },

    // BOOKING SERVICE MANAGEMENT
    "getServiceCategoryForThirdPartyBooking": {
      "params": adminValidation.getServiceCategoryForThirdPartyBooking,

      "handler": adminAction.getServiceCategoryForThirdPartyBooking,
    },
    "newRideBookingForThirdParty": {
      "params": adminValidation.newRideBookingForThirdParty,
      "handler": adminAction.newRideBookingForThirdParty,
    },
    "createBookingServiceManagement": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.createBookingServiceManagement,
      "handler": adminAction.createBookingServiceManagement,
    },
    "bookingServiceManagementsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.bookingServiceManagementsList,
    },
    "getBookingServiceMangement": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getBookingServiceMangement,
    },
    "editBookingServiceManagement": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.editBookingServiceManagement,
      "handler": adminAction.editBookingServiceManagement,
    },
    "changeBookingServiceManagementStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.changeStatus,
      "handler": adminAction.changeBookingServiceManagementStatus,
    },
    // CLIENT RELATED APIS
    "createClient": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.createClient,
      "handler": adminAction.createClient,
    },
    "getclients": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getclients,
    },
    "editClient": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.editClient,
      "handler": adminAction.editClient,
    },
    "getAllActiveInActiveClients": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      // "params": adminValidation.getList,
      "handler": adminAction.getAllActiveInActiveClients,
    },
    "getPlansList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getPlansList,
    },
    "createAndUpdatePlans": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.createAndEditPlans,
      "handler": adminAction.createAndEditPlans,
    },
    "getPlans": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getPlanById,
      "handler": adminAction.getPlanById,
    },
    "getAllPlans": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.getPlanById,
      "handler": adminAction.getAllPlans,
    },
    // pending payment for clients
    "clientPaymentDetails": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      "params": {
        "key": { "type": "string", "min": 2, "trim": true },
      },
      "handler": adminAction.clientPaymentDetails,
    },
    "changeClientActiveStatus": {
      "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      // "params": adminValidation.createPromotionCampaign,
      "handler": adminAction.changeClientActiveStatus,
    },
    "clientPayBill": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      "params": {
        "clientId": { "type": "string", "min": 12 },
        "days": { "type": "number", "convert": true, "default": 0 },
      },
      "handler": adminAction.clientPayBill,
    },
    "createPromotionCampaign": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.createPromotionCampaign,
      "handler": adminAction.createPromotionCampaign,
    },
    "getPartnerDealsRewardsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getPartnerDealsRewardsList,
      "handler": adminAction.getRewardsList,
    },
    "getPointsRewardsList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getPointsRewardsList,
      "handler": adminAction.getRewardsList,
    },
    "managePartnerDealsRewards": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.managePartnerDealsReward,
      "handler": adminAction.managePartnerDealsReward,
    },
    "managePointsRewards": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.managePointsRewards,
      "handler": adminAction.managePointsRewards,
    },
    // Call Settings
    "getCALLConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCALLConfig,
    },
    "updateCALLConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateCALLConfig,
      "handler": adminAction.updateCALLConfig,
    },
    //#region Call Center
    "getCallCenterList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCallCenterList,
    },
    "manageCallCenter": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.updateCALLConfig,
      "handler": adminAction.manageCallCenter,
    },
    //#endregion Call Center
    //#region Heat Map
    "getHeatMapList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getHeatMapList,
    },
    "manageHeatMap": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.updateCALLConfig,
      "handler": adminAction.manageHeatMap,
    },
    //#endregion Heat Map
    //#region Service Type
    "getServiceTypeList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getServiceTypeList,
    },
    "manageServiceType": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.updateCALLConfig,
      "handler": adminAction.manageServiceType,
    },
    //#endregion Service Type
    //#region Common Category
    "getCommonCategoryDropDownList": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": {
        "type": { "type": "string", "min": 2 },
        "category": { "type": "string", "min": 2 },
      },
      "handler": adminAction.getCommonCategoryDropDownList,
    },
    "getCommonCategoryList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getCommonCategoryList,
    },
    "manageCommonCategory": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.updateCALLConfig,
      "handler": adminAction.manageCommonCategory,
    },
    //#endregion Common Category
    //#region Landing Page
    "getLandingPageList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getLandingPageList,
    },
    "manageLandingPage": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.updateCALLConfig,
      "handler": adminAction.manageLandingPage,
    },
    "getLandingPageTrendingSliderList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "handler": adminAction.getLandingPageTrendingSliderList,
    },
    //#endregion Landing Page
    // Membership Plan
    "getMembershipPlanList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getList,
      "handler": adminAction.getMembershipPlanList,
    },
    // "getAllLanguages": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "handler": adminAction.getAllLanguages,
    // },
    "getMembershipPlanById": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getEdit,
      "handler": adminAction.getMembershipPlanById,
    },
    "updateMembershipPlan": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.updateMembershipPlan,
      "handler": adminAction.updateMembershipPlan,
    },
    // "changeLanguageStatus": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": adminValidation.changeStatus,
    //   "handler": adminAction.changeLanguageStatus,
    // },
    // "getDefaultLanguage": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "handler": adminAction.getDefaultLanguage,
    // },
    // "updateTranslateKeys": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": adminValidation.updateTranslateKeys,
    //   "handler": adminAction.updateTranslateKeys,
    // },
    // "setDefaultLanguage": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   "params": adminValidation.getEdit,
    //   "handler": adminAction.setDefaultLanguage,
    // },

    // for phone call telecmi
    "telecmiCallWebhook": {
      handler(context) {
        this.adapter.model.create({
          "name": "TELECMIWEBHOOK",
          "data.response": context.params,
        });
      },
    },
    //
    "getMembershipListByUserType": {
      "auth": [constantUtil.PROFESSIONAL],
      "params": adminValidation.getMembershipListByUserType,
      "handler": adminAction.getMembershipListByUserType,
    },
    "getRewardPointOfCity": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getRewardPointOfCity,
      "handler": adminAction.getRewardPointOfCity,
    },

    // EXTERNAL API FOR CHECKING ALL
    "getAllRideDetailsExternal": {
      "auth": constantUtil.REQUIRED,
      "params": adminValidation.getAllRideDetailsExternal,
      "handler": adminAction.getAllRideDetailsExternal,
    },

    //------ Rewards Start ----------
    "redeemRewardsPointConfig": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.PROFESSIONAL,
        constantUtil.USER,
        constantUtil.ADMIN,
      ],
      "params": {
        "lng": { "type": "string", "convert": true },
        "lat": { "type": "string", "convert": true },
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
      },
      "handler": adminAction.redeemRewardsPointConfig,
    },
    //------ Rewards End ----------
    //#region Admin Common API's
    "getAllList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "name": { "type": "string", "convert": true },
      },
      "handler": adminAction.getAllList,
    },
    "changeStatus": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        "name": { "type": "string", "convert": true },
      },
      "handler": adminAction.changeStatus,
    },
    //#endregion
    "administratorsCreateAndUpdate": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": {
        // "name": { "type": "string", "convert": true },
      },
      "handler": adminAction.administratorsCreateAndUpdate,
    },
    //#region Registration Setup
    "manageRegistrationSetup": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.manageRegistrationSetup,
      "handler": adminAction.manageRegistrationSetup,
    },
    // "updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId": {
    //   "auth": constantUtil.REQUIRED,
    //   "access": [constantUtil.ADMIN],
    //   // "params": {
    //   //   "serviceTypeId": { "type": "string", "min": 1 },
    //   // },
    //   "handler":
    //     adminAction.updateAllProfessionalsRegistrationDocumentBasedOnServiceTypeId,
    // },
    "getRegistrationSetupList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      // "params": adminValidation.getRegistrationSetupList,
      "handler": adminAction.getRegistrationSetupList,
    },
    "getRegistrationSetupByServiceAreaId": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": {
        "serviceAreaId": { "type": "string" },
      },
      "handler": adminAction.getRegistrationSetupByServiceAreaId,
    },
    "getRegistrationDocumentIdUsingServiceAreaIdAndServiceType": {
      "auth": constantUtil.REQUIRED,
      "access": [
        constantUtil.ADMIN,
        constantUtil.USER,
        constantUtil.PROFESSIONAL,
      ],
      "params": {
        "serviceAreaId": { "type": "string" },
        "serviceType": { "type": "string" },
        "serviceTypeId": { "type": "string" },
      },
      "handler":
        adminAction.getRegistrationDocumentIdUsingServiceAreaIdAndServiceType,
    },
    //#endregion Registration Setup
    //#region Incentive Setup
    "getIncentiveList": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.getIncentiveList,
      "handler": adminAction.getIncentiveList,
    },
    "manageIncentiveSetup": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN],
      "params": adminValidation.manageIncentiveSetup,
      "handler": adminAction.manageIncentiveSetup,
    },
    "getProfessionalIncentiveDashboard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN, constantUtil.PROFESSIONAL],
      "params": {
        "professionalId": { "type": "string", "min": 8 },
        "serviceAreaId": { "type": "string", "min": 8 },
      },
      "handler": adminAction.getProfessionalIncentiveDashboard,
    },
    "getUserIncentiveDashboard": {
      "auth": constantUtil.REQUIRED,
      "access": [constantUtil.ADMIN, constantUtil.USER],
      "params": {
        "userId": { "type": "string", "min": 8 },
      },
      "handler": adminAction.getUserIncentiveDashboard,
    },
    //#endregion Incentive Setup
    //#region ThirdParty API Authorization
    "getThirdPartyAPIAuthorization": {
      // "auth": constantUtil.REQUIRED,
      // "access": [constantUtil.ADMIN],
      // "params": {
      //   "client_id": { "type": "string" },
      //   "client_secret": { "type": "string" },
      // },
      "handler": adminAction.getThirdPartyAPIAuthorization,
    },
    //#endregion ThirdParty API Authorization
  },
  "events": {
    "admin.sendSMS": {
      "params": {
        "phoneNumber": { "type": "string" },
        "message": { "type": "string" },
        "notifyType": {
          "type": "enum",
          "values": [
            constantUtil.OTP,
            constantUtil.PROFESSIONALACTIVATED,
            constantUtil.ACCEPTED,
            constantUtil.ARRIVED,
            constantUtil.STARTED,
            constantUtil.ENDED,
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.FORCEASSIGNBYADMIN,
            constantUtil.PROMOTION,
            constantUtil.EMERGENCY,
          ],
        },
        "otpReceiverAppName": {
          "type": "enum",
          "values": [constantUtil.SMS, constantUtil.WHATSUP],
        },
      },
      "handler": adminEvent.sendSMS,
    },
    //  send  Whatsup Alert
    "admin.sendWhatsupAlert": {
      "params": {
        "rideId": { "type": "string", "optional": true },
        "bookingFrom": {
          "type": "enum",
          "values": [
            constantUtil.CONST_WEBAPP,
            constantUtil.CONST_APP,
            constantUtil.ADMIN,
            constantUtil.COORPERATEOFFICE,
          ],
        },
        "notifyType": {
          "type": "enum",
          "values": [
            constantUtil.NONE,
            constantUtil.ACCEPTED,
            constantUtil.ARRIVED,
            constantUtil.STARTED,
            constantUtil.ENDED,
            constantUtil.PROFESSIONALCANCELLED,
            constantUtil.FORCEASSIGNBYADMIN,
          ],
        },
        "notifyFor": {
          "type": "enum",
          "values": [
            constantUtil.ALL,
            constantUtil.USER,
            constantUtil.PROFESSIONAL,
          ],
        },
        "alertType": {
          "type": "enum",
          "optional": true,
          "values": [constantUtil.SMS, constantUtil.OTP],
        },
      },
      "handler": sendWhatsupAlert,
    },
    "admin.sendMail": {
      "params": adminValidation.sendMail,
      "handler": adminEvent.sendMail,
    },
    "admin.sendFCM": {
      "params": {
        "temprorySound": { "type": "boolean", "optional": true },
        "clientId": { "type": "string", "optional": true },
        "data": { "type": "object" },
        "registrationTokens": {
          "type": "array",
          "items": {
            "type": "object",
            "strict": true,
            "props": {
              "id": { "type": "string" },
              "token": { "type": "string" },
              "deviceType": { "type": "string" },
              "platform": { "type": "string" },
              "socketId": { "type": "string" },
              "duration": {
                "type": "number",
                "convert": true,
                "default": 0,
                "optional": true,
              },
              "distance": {
                "type": "number",
                "convert": true,
                "default": 0,
                "optional": true,
              },
            },
          },
        },
      },
      "handler": adminEvent.sendFCM,
    },
    "admin.verifyToken": {
      "handler": adminEvent.verifyToken,
    },
    "admin.getProfassionalDocument": {
      "handler": adminEvent.getProfassionalDocument,
    },
    "admin.getHubListByLocation": {
      "params": {
        "serviceArea": { "type": "string" },
      },
      "handler": adminEvent.getHubListByLocation,
    },
    "admin.getDocumentListForHub": {
      "handler": adminEvent.getDocumentListForHub,
    },
    "admin.rolesForceUpdate": {
      "handler": adminEvent.rolesForceUpdate,
    },
    "admin.rolesChangeInactive": {
      "handler": adminEvent.rolesChangeInactive,
    },
    "admin.getTrustedContactsReasons": {
      "handler": adminEvent.getTrustedContactsReasons,
    },
    "admin.getReasonData": {
      "handler": adminEvent.getReasonData,
    },
    "admin.updateSettingBillingTime": {
      "handler": adminEvent.updateSettingBillingTime,
    },
    "admin.updateSettingCoorperateBillingTime": {
      "handler": adminEvent.updateSettingCoorperateBillingTime,
    },
    "admin.uploadSingleImageInSpaces": {
      "handler": adminEvent.uploadSingleImageInSpaces,
    },
    "admin.uploadStaticMapImageInSpaces": {
      "handler": adminEvent.uploadStaticMapImageInSpaces,
    },
    "admin.uploadPdfInSpaces": {
      "handler": adminEvent.uploadPdfInSpaces,
    },
    "admin.uploadMultipleImageInSpaces": {
      "handler": adminEvent.uploadMultipleImageInSpaces,
    },
    "admin.getAllCoorperateOffices": {
      "handler": adminEvent.getAllCoorperateOffices,
    },
    "admin.updateCoorperateBillData": {
      "handler": adminEvent.updateCoorperateBillData,
    },
    "admin.mailContentLink": {
      "handler": adminEvent.mailContentLink,
    },
    "admin.promationMailContentLink": {
      "handler": adminEvent.promationMailContentLink,
    },
    "admin.updateCouponCount": {
      "handler": adminEvent.updateCouponCount,
    },
    "admin.sendServiceMail": {
      "handler": adminEvent.sendServiceMail,
    },
    // DB ACCESS
    "admin.removePrefix0": {
      "handler": adminEvent.removePrefix0,
    },
    // notification
    "admin.sendPromotionMail": {
      "handler": adminEvent.sendPromotionMail,
    },
    "admin.getBankInputData": {
      "handler": adminEvent.getBankInputData,
    },
    // 'admin.getPaymentOptionList': {
    //   'handler': adminEvent.getPaymentOptionList,
    // },
    "admin.getBankFieldDefaultDisplayInApp": {
      "handler": adminEvent.getBankFieldDefaultDisplayInApp,
    },
    "admin.testData": {
      "handler": adminEvent.testData,
    },
    //
    "admin.getById": {
      "params": {
        "id": { "type": "string", "min": 12, "trim": true, "convert": true },
      },
      "handler": adminAction.getDataById,
    },
    "admin.getSubscriptionPlansData": {
      "params": {
        "isDefault": {
          "type": "boolean",
          "convert": true,
          "default": false,
        },
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
      },
      "handler": adminEvent.getSubscriptionPlansData,
    },
    "admin.sendBulkServiceMail": {
      "params": {
        "templateName": { "type": "string" },
        "receiversData": { "type": "array", "min": 1 },
      },
      "handler": adminEvent.sendBulkServiceMail,
    },
    // ----------- Loyalty Coupon / Rewards Start --------
    // "admin.getLoyaltyCouponById": {
    //   "params": {
    //     "couponId": {
    //       "type": "string",
    //       "min": 12,
    //       "trim": true,
    //       "convert": true,
    //     },
    //     "userType": {
    //       "type": "enum",
    //       "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
    //     },
    //   },
    //   "handler": adminEvent.getLoyaltyCouponById,
    // },
    // "admin.updateLoyaltyCouponAppliedCount": {
    //   "params": {
    //     "couponId": {
    //       "type": "string",
    //       "min": 12,
    //       "trim": true,
    //       "convert": true,
    //     },
    //     "userType": {
    //       "type": "enum",
    //       "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
    //     },
    //   },
    //   "handler": adminEvent.updateLoyaltyCouponAppliedCount,
    // },
    "admin.getActiveRewardsListBasedOnCityAndUserType": {
      "params": {
        "lng": { "type": "string", "convert": true },
        "lat": { "type": "string", "convert": true },
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "rewardType": {
          "type": "enum",
          "values": [constantUtil.POINTS, constantUtil.PARTNERDEALS],
        },
      },
      "handler": adminEvent.getActiveRewardsListBasedOnCityAndUserType,
    },
    "admin.updateRewardUsedCount": {
      "params": {
        "couponId": { "type": "string", "min": 12 },
      },
      "handler": adminEvent.updateRewardUsedCount,
    },
    // ----------- Loyalty Coupon / Rewards End --------
    //------ Rewards Start ----------
    "admin.redeemRewardsPointConfig": {
      "params": {
        "lng": { "type": "string", "convert": true },
        "lat": { "type": "string", "convert": true },
        "userType": {
          "type": "enum",
          "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
        },
        "languageCode": { "type": "string", "optional": true },
      },
      "handler": adminEvent.redeemRewardsPointConfig,
    },
    //------ Rewards End ----------
    "admin.getNotificationBasedOnUserType": {
      "handler": adminEvent.getNotificationBasedOnUserType, // Temprory Need to remove All
    },
    "admin.getUserBasedCouponList": {
      "params": adminValidation.getUserBasedCouponList,
      "handler": adminEvent.getUserBasedCouponList,
    },
    "admin.getEmailNotificationTemplate": {
      "handler": adminEvent.getEmailNotificationTemplate,
    },
    "admin.updateHubSiteCommission": {
      "params": {
        "bookingId": { "type": "string", "convert": true },
        "refBookingId": { "type": "string", "convert": true },
        "hubId": { "type": "string", "convert": true },
        "siteCommissionWithoutTax": { "type": "number", "convert": true },
      },
      "handler": adminEvent.updateHubSiteCommission,
    },
    "admin.updateRegistrationDocuments": {
      "handler": adminEvent.updateRegistrationDocuments,
    },
    "admin.updateGeneralSettingTimestampInDBAndRedis": {
      "handler": adminEvent.updateGeneralSettingTimestampInDBAndRedis,
    },
    "admin.updateDataInRedis": {
      "params": {
        "type": { "type": "string", "min": 1 },
        "category": { "type": "string", "optional": true },
      },
      "handler": adminEvent.updateDataInRedis,
    },
    "admin.getRegistrationDocumentUsingServiceTypeId": {
      "handler": adminEvent.getRegistrationDocumentUsingServiceTypeId,
    },
    "admin.updateWalletAmountTripEnded": {
      "params": {
        "corporateId": { "type": "string", "min": 1 },
        "amount": { "type": "number", "convert": true },
        "paymentMethod": { "type": "string", "optional": true },
      },
      "handler": adminEvent.updateWalletAmountTripEnded,
    },
    "admin.runIncentiveProgramToProfessionalByCronjob": {
      "handler": adminEvent.runIncentiveProgramToProfessionalByCronjob,
    },
    "admin.runIncentiveProgramToUserByCronjob": {
      "handler": adminEvent.runIncentiveProgramToUserByCronjob,
    },
    "admin.checkAndValidateIncentiveRuleByServiceAreaId": {
      "handler": adminEvent.checkAndValidateIncentiveRuleByServiceAreaId,
    },
    "admin.manageClientsBaseData": {
      "params": {
        "clientId": { "type": "string", "min": 1 },
      },
      "handler": adminEvent.manageClientsBaseData,
    },
    "admin.getClientsBaseDataFromRedisUsingTypeAndClientId": {
      "params": {
        "type": {
          "type": "string",
          "uppercase": true,
          "enum": [
            constantUtil.GENERALSETTING,
            constantUtil.VEHICLECATEGORY,
            constantUtil.MAPSETTING,
            constantUtil.SMSSETTING,
            constantUtil.CALLSETTING,
            constantUtil.INVITEANDEARN,
            constantUtil.PAYMENTGATEWAY,
            constantUtil.SMTPSETTING,
            constantUtil.PACKAGESETTING,
            constantUtil.CONST_SPECIALDAYS,
            constantUtil.FIREBASESETTING,
            constantUtil.FCMACCESSTOKEN,
            constantUtil.CANCELLATIONREASON,
            constantUtil.CONST_AIRPORTTRANSFERSETTING,
          ],
        },
        "clientId": { "type": "string", "min": 1 },
      },
      "handler": adminEvent.getClientsBaseDataFromRedisUsingTypeAndClientId,
    },
    "admin.updateClientsBaseDataFromRedisUsingType": {
      "params": {
        "action": {
          "type": "string",
          "uppercase": true,
          "enum": [constantUtil.UPDATE],
        },
        "name": {
          "type": "string",
          "uppercase": true,
          "enum": [
            constantUtil.GENERALSETTING,
            constantUtil.VEHICLECATEGORY,
            constantUtil.MAPSETTING,
            constantUtil.PACKAGESETTING,
            constantUtil.SMSSETTING,
            constantUtil.CALLSETTING,
            constantUtil.INVITEANDEARN,
            constantUtil.AIRPORT,
            constantUtil.TOLL,
            constantUtil.POPULARPLACE,
            constantUtil.COUPON,
          ],
        },
        "name": { "type": "string", "min": 1 },
        "clientId": { "type": "string", "optional": true },
        "updateKeyName": {
          "type": "string",
          "uppercase": true,
          "optional": true,
          "enum": [
            constantUtil.MAPSETTING,
            constantUtil.PACKAGESETTING,
            constantUtil.AIRPORT,
            constantUtil.TOLL,
            constantUtil.POPULARPLACE,
            constantUtil.COUPON,
          ],
        },
      },
      "handler": adminEvent.updateClientsBaseDataFromRedisUsingType,
    },
    "admin.getAllAirportData": {
      "handler": adminEvent.getAllAirportData,
    },
  },
  "methods": {
    "adminServiceInit": adminMethod.adminServiceInit,
    "smsInit": adminMethod.smsInit,
    "updatePrivilegesByRole": adminMethod.updatePrivilegesByRole,
    "spacesS3Init": adminMethod.spacesS3Init,
    "fcmNotifyInit": adminMethod.fcmNotifyInit,
  },
  /**
   * SERVICE LIFECYCLE METHODS
   */
  created() {},
  async started() {
    await this.adminServiceInit();
    await this.smsInit();
    await this.spacesS3Init();
    await this.fcmNotifyInit();
  },
  async stopped() {},
};
