module.exports = {
  "root": true,
  "env": {
    "node": true,
    "commonjs": true,
    "es6": true,
    "jquery": false,
  },
  "plugins": ["prettier"],
  "extends": ["eslint:recommended", "prettier"],
  "parserOptions": {
    "sourceType": "module",
    "ecmaVersion": "2020",
  },
  "rules": {
    "prettier/prettier": [
      "error",
      {
        "singleQuote": false,
        "semi": true,
        "tabWidth": 2,
      },
    ],
    "no-var": "error",
    "no-unused-vars": "warn",
    "no-unused-expressions": "warn",
    "no-unused-labels": "warn",
    "prefer-const": "error",
    "no-use-before-define": "error",
    "quote-props": "error",
  },
};
