const constantUtil = require("../utils/constant.util");
const constantUtill = require("../utils/constant.util");

const heatmapValidations = {};
//-----------------------------------------
heatmapValidations.manageHeatmap = {
  "userId": { "type": "string", "optional": true },
  // "bookingDate": { "type": "string", "min": 1 },
  // "locationTileId": { "type": "string", "min": 1 },
  "plusCodePrefix": { "type": "string", "min": 1 },
  "serviceAreaId": { "type": "string", "optional": true },
  "locationName": { "type": "string", "min": 1 },
  "latLng": {
    "type": "object",
    "props": {
      "lat": {
        "type": "number",
        "convert": true,
      },
      "lng": {
        "type": "number",
        "convert": true,
      },
    },
  },
};
heatmapValidations.getHeatmapDataByLocation = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "serviceAreaId": { "type": "string", "convert": true, "optional": true },
};
heatmapValidations.calculatePeakFareUsingHeatmap = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "professionalCount": { "type": "number", "convert": true },
  "serviceAreaId": { "type": "string", "convert": true },
};
//-----------------------------------------
module.exports = heatmapValidations;
