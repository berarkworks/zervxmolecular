const constantUtills = require("../utils/constant.util");

const logValidation = {};

logValidation.getAutocomplete = {
  "userType": {
    "optional": true,
    "type": "enum",
    "values": [constantUtills.USER, constantUtills.PROFESSIONAL],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
};

logValidation.getDirections = {
  "userType": {
    "optional": true,
    "type": "enum",
    "values": [constantUtills.USER, constantUtills.PROFESSIONAL],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
};

logValidation.getGeocode = {
  "userType": {
    "optional": true,
    "type": "enum",
    "values": [constantUtills.USER, constantUtills.PROFESSIONAL],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
};

logValidation.getDistanceMatrix = {
  "userType": {
    "optional": true,
    "type": "enum",
    "values": [constantUtills.USER, constantUtills.PROFESSIONAL],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
};

logValidation.getSnapway = {
  "userType": {
    "optional": true,
    "type": "enum",
    "values": [constantUtills.USER, constantUtills.PROFESSIONAL],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
};
module.exports = logValidation;
