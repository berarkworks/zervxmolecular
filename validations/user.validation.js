const constantUtil = require("../utils/constant.util");
const constantUtill = require("../utils/constant.util");

const userValidations = {};

userValidations.login = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
  "password": { "type": "string", "optional": true },
  "bookingFrom": { "type": "string", "min": 1, "optional": true }, // CONST_WEBAPP, CONST_APP
  "type": { "type": "string", "min": 1, "optional": true }, // Must Pass // --> CONST_ANDROID_OS, CONST_IOS, CONST_WEB_OS
  "userName": { "type": "string", "min": 1, "optional": true }, // Must Pass // cryptoJs-AES Encoded Data, Keys--> phoneCode + phoneNumber
};

userValidations.verifyOtp = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
  "otp": { "type": "string", "optional": true },
};

userValidations.verifyEmail = {
  "userId": { "type": "string", "min": 4, "convert": true },
  "action": {
    "type": "enum",
    "values": [constantUtill.OTP, constantUtill.CONST_VERIFY],
  },
  "email": { "type": "string", "optional": true },
  "otp": { "type": "string", "optional": true },
};

userValidations.updateProfile = {
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email", "optional": true },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtill.MALE, constantUtill.FEMALE, constantUtill.OTHER],
  //   "optional": true,
  // },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
  "dob": {
    "type": "string",
    "pattern": "^\\d{2}-\\d{2}-\\d{4}|\\d{2}/\\d{2}/\\d{4}$",
    "optional": true,
  },
  "gender": { "type": "string", "optional": true },
  "avatar": { "type": "string", "optional": true },
  "referredBy": { "type": "string", "optional": true, "trim": true },
  "uniqueCode": { "type": "string", "optional": true, "trim": true },
  "status": {
    "type": "enum",
    "values": [
      constantUtill.INCOMPLETE,
      constantUtill.ACTIVE,
      constantUtill.INACTIVE,
      constantUtill.ARCHIVE,
    ],
  },
};

userValidations.activateAndInactivateAccount = {
  "userId": { "type": "string", "min": 4, "convert": true },
  "action": {
    "type": "enum",
    "values": [constantUtill.ACTIVE, constantUtill.INACTIVE],
  },
};

userValidations.changePhoneNumber = {
  ...userValidations.verifyOtp,
  "otp": { ...userValidations.verifyOtp.otp, "optional": true },
};

userValidations.manageAddress = {
  "addressId": { "type": "string", "optional": true },
  "placeId": { "type": "string" },
  "addressName": { "type": "string" },
  "addressType": {
    "type": "enum",
    "values": [
      constantUtill.ADDRESSTYPE_HOME,
      constantUtill.ADDRESSTYPE_WORK,
      constantUtill.ADDRESSTYPE_OTHER,
    ],
  },
  "shortAddress": { "type": "string" },
  "fullAddress": { "type": "string" },
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};

userValidations.manageCard = {
  "cardId": { "type": "string", "min": 1, "optional": true, "trim": true },
  "type": { "type": "string", "trim": true, "optional": true },
  "cardNumber": { "type": "string", "min": 1, "trim": true },
  "expiryYear": { "type": "string", "min": 1, "trim": true },
  "expiryMonth": { "type": "string", "min": 1, "trim": true },
  "holderName": { "type": "string", "min": 1, "trim": true },
  "currencyCode": { "type": "string", "optional": true, "trim": true },
  "pin": { "type": "number", "optional": true, "convert": true, "trim": true },
  "isDefault": { "type": "boolean", "convert": false, "optional": true },
  "securityCode": { "type": "string", "optional": true },
  "paymentGateWayCustomerId": {
    "type": "string",
    "convert": false,
    "optional": true,
    "trim": true,
  },
};

userValidations.setDefaultPaymentOption = {
  "defaultPayment": {
    "type": "enum",
    "values": [
      constantUtill.PAYMENTCASH,
      constantUtill.PAYMENTWALLET,
      constantUtill.PAYMENTCARD,
      constantUtill.PAYMENTCREDIT,
      constantUtill.CONST_POS,
      constantUtill.CONST_PIX,
    ],
  },
};

userValidations.setAppliedCoupons = {
  "couponId": { "type": "string", "optional": true },
};

userValidations.updateLiveLocation = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "currentBearing": { "type": "number", "convert": true },
  "speed": { "type": "number", "convert": true },
  "verticalAccuracy": { "type": "number", "convert": true },
  "horizontalAccuracy": { "type": "number", "convert": true },
  "altitude": { "type": "number", "convert": true },
};

userValidations.getUserData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtill.LIFETIME, constantUtill.FILTER],
  },
};

userValidations.addBankDetails = {
  "accountName": { "type": "string", "min": 2 },
  "accountNumber": { "type": "string", "min": 6 },
  "bankCode": { "type": "string", "min": 1, "trim": true },
};
userValidations.removeBankDetails = {
  "id": { "type": "string", "min": 1 },
};
userValidations.checkUserAvail = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
};
userValidations.registerUserByBooking = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
  "nationalIdNo": { "type": "string", "optional": true },
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtill.MALE, constantUtill.FEMALE, constantUtill.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
};

userValidations.getRatings = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
userValidations.checkContacts = {
  "contacts": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "code": { "type": "string" },
        "number": { "type": "string" },
      },
    },
  },
};
userValidations.addWalletAmount = {
  "id": { "type": "string" },
  "amount": { "type": "number", "convert": true },
  "reason": { "type": "string", "optional": true },
  "isReimbursement": { "type": "boolean", "optional": true },
  "type": {
    "type": "enum",
    "values": [
      constantUtill.CREDIT,
      constantUtill.DEBIT,
      constantUtill.REWARDPOINT,
      constantUtill.REDEEMREWARDPOINT,
    ],
  },
};
userValidations.updatePermissionData = {
  "osType": {
    "type": "enum",
    "values": [constantUtill.IOS, constantUtill.ANDROID],
    "optional": true,
  },
  "brand": { "type": "string", "optional": true },
  "model": { "type": "string", "optional": true },
  "osVersion": { "type": "string", "optional": true },
  "screenSize": { "type": "string", "optional": true },
  "internetConnectivityMedium": {
    "type": "string",
    "optional": true,
  },
  "token": { "type": "string", "optional": true },
  "fcmId": { "type": "string", "optional": true },
  "deviceLocationStatus": { "type": "boolean", "optional": true },
  "appLocationStatus": { "type": "boolean", "optional": true },
  "baseUrl": { "type": "string", "optional": true },
  "appVersionInServer": { "type": "string", "optional": true },
  "appVersionInStore": { "type": "string", "optional": true },
  "deviceTime": { "type": "string", "optional": true },
  "timezoneInNumber": { "type": "string", "optional": true },
  "timezoneInformat": { "type": "string", "optional": true },
  "connectivityMediumName": { "type": "string", "optional": true },
  "currentLocation": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
    "optional": true,
  },
  "socketConnetivity": { "type": "boolean", "optional": true },
};
userValidations.getInviteAndEarnHistory = {
  "inviteCode": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
userValidations.getInviteAndEarnList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "isHasInviteCount": { "type": "boolean", "convert": true, "optional": true },
};
userValidations.getUsersJoinerList = {
  "id": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
userValidations.updatePasswordBypass = {
  "id": { "type": "string", "min": 1 },
  "isOtpBypass": { "type": "boolean" },
};
userValidations.getUserId = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
};
// payment Related card ,bank
userValidations.verifyCard = {
  "transactionId": {
    "type": "string",
    "min": 1,
    "convert": true,
    "trim": "true",
  },
  "otp": { "type": "number", "convert": true, "trim": true },
  "mode": {
    "uppercase": true,
    "type": "enum",
    "values": [constantUtil.OTP, constantUtil.PIN],
  }, //router action
  "cardId": { "type": "string", "min": 1, "convert": true, "trim": true },
};
userValidations.addVerifiedCard = {
  "card": { "type": "object" },
  "userId": {
    "type": "string",
    "convert": true,
    "trim": true,
    "min": 1,
  },
};
module.exports = userValidations;
