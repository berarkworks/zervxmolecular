const constantUtils = require("../utils/constant.util");

const bookingValidation = {};

bookingValidation.userGetPaymentOptions = {};

const commonSearchFields = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};

bookingValidation.userRideBooking = {
  "bookingBy": {
    "type": "enum",
    "values": [
      constantUtils.CORPORATEOFFICER, //Officer
      constantUtils.USER, //User
    ],
    "default": constantUtils.USER, //default -  User
    "optional": true,
  },
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "min": 1 },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "tripType": {
    "type": "enum",
    "values": [constantUtils.DAILYTRIP, constantUtils.RENTAL],
    "default": constantUtils.DAILYTRIP,
  },
  "rentalPackage": { "type": "number", "convert": true, "optional": true },
  "bookingDate": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "couponId": { "type": "string", "optional": true },
  "couponAmount": { "type": "number", "convert": true, "optional": true },
  "couponValue": { "type": "number", "convert": true, "optional": true },
  "couponType": { "type": "string", "optional": true },
  "couponApplied": { "type": "boolean", "optional": true },
  "isProfessionalTolerance": { "type": "boolean", "optional": true },
  "actualEstimationAmount": { "type": "number", "convert": true },

  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "pendingPaymentAmount": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
  "paymentOption": {
    "type": "enum",
    "values": [
      constantUtils.PAYMENTCASH,
      constantUtils.PAYMENTWALLET,
      constantUtils.PAYMENTCARD,
      constantUtils.PAYMENTCREDIT,
      constantUtils.CONST_PIX,
      constantUtils.CONST_POS,
    ],
  },
  "card": {
    "type": "object",
    "optional": true,
    "strict": true,
    "props": {
      "type": { "type": "string", "optional": true },
      "cardNumber": { "type": "string", "optional": true },
      "expiryYear": { "type": "string", "optional": true },
      "expiryMonth": { "type": "string", "optional": true },
      "holderName": { "type": "string", "optional": true },
      "cvv": { "type": "string", "optional": true },
      "cardId": { "type": "string", "optional": true },
    },
  },
  "isGenderAvailable": { "type": "boolean", "convert": true, "optional": true },
  "childseatAvailable": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "handicapAvailable": { "type": "boolean", "convert": true, "optional": true },
  "bookingId": { "type": "string", "optional": true },
  "paymentIntentId": { "type": "string", "optional": true },
};

bookingValidation.userRetryBooking = {
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.expiredRetryBooking = {
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.trackBooking = {
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.bookingExpiryOrCancel = {
  "userType": {
    "type": "enum",
    "values": [
      constantUtils.USER.toLowerCase(),
      constantUtils.PROFESSIONAL.toLowerCase(),
    ],
  },
  "denyType": {
    "type": "enum",
    "values": [
      constantUtils.EXPIRED,
      constantUtils.USERCANCELLED,
      constantUtils.PROFESSIONALCANCELLED,
    ],
  },
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.getCancellationReason = {
  "userType": {
    "type": "enum",
    "values": [
      constantUtils.USER.toLowerCase(),
      constantUtils.PROFESSIONAL.toLowerCase(),
    ],
  },
};

bookingValidation.cancelBooking = {
  "bookingId": { "type": "string", "min": 1 },
  "bookingStatus": {
    "type": "enum",
    "values": [
      constantUtils.AWAITING,
      constantUtils.ARRIVED,
      constantUtils.ACCEPTED,
      constantUtils.REJECTED,
      constantUtils.USERDENY,
      constantUtils.USERCANCELLED,
      constantUtils.PROFESSIONALCANCELLED,
      constantUtils.EXPIRED,
      constantUtils.STARTED,
    ],
    "default": constantUtils.AWAITING,
  },
  "userType": {
    "type": "enum",
    "values": [
      constantUtils.USER.toLowerCase(),
      constantUtils.PROFESSIONAL.toLowerCase(),
    ],
  },
  "cancellationReason": { "type": "string", "optional": true },
  "cancelReasonDescription": { "type": "string", "optional": true },
};

bookingValidation.professionalAcceptBooking = {
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.updateProfessionalBookingStatus = {
  "bookingId": { "type": "string", "min": 1 },
  "bookingOTP": { "type": "string", "optional": true },
  "tollFareAmount": { "type": "number", "optional": true, "convert": true },
  "totalTollPassed": { "type": "number", "optional": true, "convert": true },
  "passedTollList": { "type": "array", "optional": true },
  "professionalStatus": {
    "type": "enum",
    "values": [
      constantUtils.ARRIVED.toLowerCase(),
      constantUtils.STARTED.toLowerCase(),
      constantUtils.ENDED.toLowerCase(),
    ],
  },
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "wayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "manualDistance": { "type": "number", "optional": true, "convert": true },
  "manualWayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "snapWayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "traveledDistance": { "type": "number", "convert": true, "optional": true },
  "traveledTime": { "type": "number", "convert": true, "optional": true },
  "afterRideWaitingGracePeriod": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
  "meterReadingType": {
    // "type": String,
    "optional": true,
    "type": "enum",
    "values": [
      constantUtils.CONST_LIVEMETER,
      constantUtils.CONST_SNAPWAY,
      constantUtils.CONST_INTERRUPT,
      constantUtils.CONST_TIMEINTERRUPT,
      constantUtils.CONST_DISTANCEINTERRUPT,
      constantUtils.CONST_MANUAL,
    ],
  },
  "trackData": { "type": "object", "optional": true },
};

bookingValidation.updatedProfessionalBookingStopStatus = {
  "bookingId": { "type": "string", "min": 1 },
  "stopId": { "type": "string", "min": 1 },
  "stopStatus": {
    "type": "enum",
    "values": [
      constantUtils.ARRIVED.toLowerCase(),
      constantUtils.ENDED.toLowerCase(),
    ],
  },
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};

bookingValidation.professionalAddWishList = {
  "action": {
    "type": "enum",
    "values": [
      constantUtils.ADD.toLowerCase(),
      constantUtils.REMOVE.toLowerCase(),
    ],
  },
  "bookingId": { "type": "string", "min": 1 },
};

bookingValidation.professionalGetWishList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "isAdminAssign": { "type": "boolean", "convert": true, "default": false },
};

bookingValidation.submitRating = {
  "userType": {
    "type": "enum",
    "values": [
      constantUtils.USER.toLowerCase(),
      constantUtils.PROFESSIONAL.toLowerCase(),
    ],
  },
  "rating": { "type": "number", "convert": true },
  "comment": { "type": "string", "convert": true },
  "lng": { "type": "number", "convert": true, "optional": true },
  "lat": { "type": "number", "convert": true, "optional": true },
};

bookingValidation.professionalGetScheduleList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "categoryId": {
    "type": "array",
    "items": { "type": "string", "min": 1 },
  },
  "lng": { "type": "number", "convert": true, "optional": true },
  "lat": { "type": "number", "convert": true, "optional": true },
  "serviceCategoryId": { "type": "string", "optional": true },
};

bookingValidation.userGetScheduleList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

bookingValidation.getOrderAndBookingHistory = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
bookingValidation.addAndRemoveTrustedContacts = {
  "id": { "type": "string", "min": "1", "optional": true },
  // "clientId": { "type": "string", "min": 8 },
  "reasonId": { "type": "string", "min": 1 },
  "name": { "type": "string", "min": "1" },
  "phoneCode": { "type": "string", "max": "20" },
  "phoneNumber": { "type": "string", "min": "6" },
};
bookingValidation.shareTripToTrustedContact = {
  "bookingId": { "type": "string", "min": 1, "optional": true },
  "contacts": {
    "type": "array",
    "min": 1,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "_id": { "type": "string", "min": 1 },
        "phoneCode": { "type": "string", "min": 1 },
        "phoneNumber": { "type": "string", "min": 1 },
      },
    },
  },
};
bookingValidation.changeRideLocation = {
  "bookingId": { "type": "string", "min": 1 },
  "userType": {
    "type": "enum",
    "values": [constantUtils.USER, constantUtils.PROFESSIONAL],
    "default": constantUtils.USER,
  },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
        "status": { "type": "string", "optional": true },
        "_id": { "type": "string", "optional": true },
      },
    },
  },
};
bookingValidation.getCityBasedRidesData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtils.LIFETIME, constantUtils.FILTER],
  },
};
bookingValidation.getGraphBookingData = {
  "daysCount": { "type": "number", "convert": true },
  "city": { "type": "string", "optional": true },
  "type": {
    "type": "enum",
    "values": [constantUtils.LIFETIME, constantUtils.FILTER],
  },
};
//#region Dashboard

bookingValidation.getAdminDashboardAllRides = {
  "daysCount": { "type": "number", "convert": true },
  "city": { "type": "string", "optional": true },
  "type": {
    "type": "enum",
    "values": [constantUtils.LIFETIME, constantUtils.FILTER],
  },
};
// bookingValidation.getAllRides = {
//   "time": { "type": "number", "convert": true },
//   "cityId": { "type": "string", "optional": true },
// };
//#endregion Dashboard
bookingValidation.getOperatorBasedBooking = {
  "daysCount": { "type": "number", "convert": true },
  "city": { "type": "string", "optional": true },
  "operatorId": { "type": "string" },
  "type": {
    "type": "enum",
    "values": [constantUtils.LIFETIME, constantUtils.FILTER],
  },
};
bookingValidation.getOperatorBasedCategoryBooking = {
  "daysCount": { "type": "number", "convert": true },
  "city": { "type": "string", "optional": true },
  "operatorId": { "type": "string" },
  "type": {
    "type": "enum",
    "values": [constantUtils.LIFETIME, constantUtils.FILTER],
  },
};
bookingValidation.getAvailableProfessionals = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};
bookingValidation.newRideBookingFromAdmin = {
  "bookingBy": {
    "type": "enum",
    "values": [
      constantUtils.COORPERATEOFFICE, // Office / Admin
      constantUtils.CORPORATEOFFICER, // Officer / User
      constantUtils.ADMIN, //Office  / Admin
    ],
    "default": constantUtils.ADMIN, //default - Office  / Admin
    "optional": true,
  },
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "optional": true },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "userId": { "type": "string", "optional": true },
  "bookingDate": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
  "paymentOption": {
    "type": "enum",
    "values": [
      constantUtils.PAYMENTCASH,
      constantUtils.PAYMENTWALLET,
      constantUtils.PAYMENTCREDIT,
      constantUtils.CONST_PIX,
      constantUtils.CONST_POS,
    ],
  },
  "isNotify": { "type": "boolean", "convert": true },
  "isGenderAvailable": { "type": "boolean", "convert": true, "optional": true },
  "childseatAvailable": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "handicapAvailable": { "type": "boolean", "convert": true, "optional": true },
};
bookingValidation.bookingStatusChange = {
  "bookingId": { "type": "string", "min": 1 },
  "professionalStatus": {
    "type": "enum",
    "values": [
      constantUtils.ARRIVED.toLowerCase(),
      constantUtils.STARTED.toLowerCase(),
    ],
  },
};
bookingValidation.bookingStatusChangeEnded = {
  "bookingId": { "type": "string", "min": 1 },
  "isDistanceCalculate": { "type": "boolean", "convert": true },
  "afterRideWaitingMins": { "type": "string", "optional": true },
};
bookingValidation.adminCancelBooking = {
  "bookingId": { "type": "string", "min": 1 },
};
bookingValidation.getSiteEarnings = {
  // 'skip': { 'type': 'number', 'convert': true },
  // 'limit': { 'type': 'number', 'convert': true },
  "search": { "type": "string", "optional": true },
  "billingId": { "type": "string", "optional": true },
  "city": { "type": "string", "optional": true },
  "vehicleCategory": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "paymentSectionFilter": { "type": "string" },
};
bookingValidation.getSiteEarningsSummary = {
  "professionalId": { "type": "string", "min": 4 },
  // 'skip': { 'type': 'number', 'convert': true },
  // 'limit': { 'type': 'number', 'convert': true },
  "search": { "type": "string", "optional": true },
  "billingId": { "type": "string", "optional": true },
  "city": { "type": "string", "optional": true },
  "vehicleCategory": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "paymentSectionFilter": { "type": "string" },
};
bookingValidation.adminBookingExpiryOrCancel = {
  "bookingId": { "type": "string", "min": 1 },
};
bookingValidation.giveTips = {
  "bookingId": { "type": "string", "min": 8 },
  "amount": { "type": "number", "convert": true },
  "paymentType": {
    "type": "enum",
    "values": [
      constantUtils.PAYMENTCASH,
      constantUtils.PAYMENTWALLET,
      constantUtils.PAYMENTCARD,
    ],
  },
  "transactionId": { "type": "string", "optional": true, "default": null },
  "gatewayResponse": { "type": "object", "optional": true, "default": null },
  "card": {
    "type": "object",
    "optional": true,
    "strict": true,
    "props": {
      "type": { "type": "string", "optional": true },
      "cardNumber": { "type": "string", "optional": true },
      "expiryYear": { "type": "number", "optional": true },
      "expiryMonth": { "type": "number", "optional": true },
      "holderName": { "type": "string", "optional": true },
      "cvv": { "type": "string", "optional": true },
    },
  },
};
bookingValidation.getDriversBillingEarnings = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
bookingValidation.getDayEarnings = {
  "userType": {
    "type": "enum",
    "values": [constantUtils.USER, constantUtils.PROFESSIONAL],
    "default": constantUtils.PROFESSIONAL,
  },
  "userId": { "type": "string", "min": 10, "optional": true },
  "professionalId": { "type": "string", "min": 10, "optional": true },
  "billedDate": { "type": "date", "convert": true },
  "actionType": { "type": "string", "optional": true },
  "startDate": { "type": "date", "convert": true, "optional": true }, //Future Use
  "endDate": { "type": "date", "convert": true, "optional": true },
  "rideType": { "type": "string", "optional": true },
  "minimumReviewRating": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
  "minimumEligibleAmount": {
    "type": "number",
    "convert": true,
    "optional": true,
    "default": 0,
  },
};
bookingValidation.adminRideList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": {
    "type": "enum",
    "values": [
      constantUtils.AWAITING,
      constantUtils.ARRIVED,
      constantUtils.ACCEPTED,
      constantUtils.USERCANCELLED,
      constantUtils.USERDENY,
      constantUtils.PROFESSIONALCANCELLED,
      constantUtils.EXPIRED,
      constantUtils.CANCELLED,
      constantUtils.STARTED,
      constantUtils.ENDED,
      constantUtils.ALL,
    ],
  },
  "search": { "type": "string" },
};
bookingValidation.cooprateRideList = {
  "id": { "type": "string", "min": 12, "convert": true },
  "skip": { "type": "number", "convert": true, "optional": true, "default": 0 },
  "limit": {
    "type": "number",
    "convert": true,
    "optional": true,
    "default": 100000000,
  },
  "filter": {
    "type": "enum",
    "values": [
      constantUtils.AWAITING,
      constantUtils.ARRIVED,
      constantUtils.ACCEPTED,
      constantUtils.USERDENY,
      constantUtils.USERCANCELLED,
      constantUtils.CANCELLED,
      constantUtils.PROFESSIONALCANCELLED,
      constantUtils.EXPIRED,
      constantUtils.STARTED,
      constantUtils.ENDED,
    ],
    "optional": true,
  },
  "recentFilter": { "type": "boolean", "convert": true, "default": false },
  "search": { "type": "string", "optional": true, "default": "" },
  "fromDate": {
    "type": "string",
    "convert": true,
    "optional": true,
    "default": "",
  },
  "toDate": {
    "type": "string",
    "convert": true,
    "optional": true,
    "default": "",
  },
};
bookingValidation.getUserRideList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};
bookingValidation.getUserRideReportList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};

bookingValidation.getProfessionalRideList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};
bookingValidation.getProfessionalRideReportList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};

bookingValidation.getAppUsageList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};
// bookingValidation.getProfessionalCommissionReport = {
//   ...commonSearchFields,
// };
bookingValidation.getRideReport = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};

bookingValidation.getHubsRideList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};
bookingValidation.getCouponBasedRideList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string" },
};
bookingValidation.manualMeterBooking = {
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "min": 1 },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "bookingDate": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
};
bookingValidation.updateTripWayData = {
  "bookingId": { "type": "string", "min": 1 },
  "bookingStatus": { "type": "string", "optional": true },
  "manualDistance": { "type": "number", "optional": true, "convert": true },
  "pickupWayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "wayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "manualWayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
  "snapWayData": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "timeStamp": { "type": "number", "convert": true },
        "isStop": { "type": "boolean", "optional": true },
        "isInterrupt": { "type": "boolean", "optional": true },
        "horizontalAccuracy": {
          "type": "number",
          "convert": true,
          "optional": true,
        },
      },
    },
  },
};
bookingValidation.coorperateBooking = {
  "bookingBy": {
    "type": "enum",
    "values": [
      constantUtils.CORPORATEOFFICER, //Officer / User
      constantUtils.COORPERATEOFFICE, //Office  / Admin
      constantUtils.ADMIN, //Office  / Admin
    ],
    "default": constantUtils.COORPERATEOFFICE, //default - Office  / Admin
    "optional": true,
  },
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "optional": true },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "userId": { "type": "string", "optional": true },
  "corperateId": { "type": "string", "optional": true },
  "bookingDate": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
  "paymentOption": {
    "type": "enum",
    "values": [constantUtils.PAYMENTCREDIT],
  },
  "isNotify": { "type": "boolean", "convert": true },
  "isGenderAvailable": { "type": "boolean", "convert": true, "optional": true },
  "childseatAvailable": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "handicapAvailable": { "type": "boolean", "convert": true, "optional": true },
};
bookingValidation.updateWaitingTimes = {
  "bookingId": { "type": "string", "min": 1 },
  "type": {
    "type": "enum",
    "values": [constantUtils.STARTED, constantUtils.ENDED],
  },
};
bookingValidation.getTotalFareData = {
  "operaterId": { "type": "string", "min": 1 },
  "billingId": { "type": "string", "optional": true },
  "city": { "type": "string", "optional": true },
  "vehicleCategory": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
bookingValidation.quickRideBooking = {
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "min": 1 },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "bookingDate": { "type": "string", "min": 1 },
  "professionalId": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "couponId": { "type": "string", "optional": true },
  "couponAmount": { "type": "number", "convert": true, "optional": true },
  "couponValue": { "type": "number", "convert": true, "optional": true },
  "couponType": { "type": "string", "optional": true },
  "couponApplied": { "type": "boolean", "optional": true },
  "isProfessionalTolerance": { "type": "boolean", "optional": true },
  "actualEstimationAmount": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
  "paymentOption": {
    "type": "enum",
    "values": [
      constantUtils.PAYMENTCASH,
      constantUtils.PAYMENTWALLET,
      constantUtils.PAYMENTCARD,
      constantUtils.PAYMENTCREDIT,
      constantUtils.CONST_PIX,
      constantUtils.CONST_POS,
    ],
  },
  "card": {
    "type": "object",
    "optional": true,
    "strict": true,
    "props": {
      "type": { "type": "string", "optional": true },
      "cardNumber": { "type": "string", "optional": true },
      "expiryYear": { "type": "string", "optional": true },
      "expiryMonth": { "type": "string", "optional": true },
      "holderName": { "type": "string", "optional": true },
      "cvv": { "type": "string", "optional": true },
      "cardId": { "type": "string", "optional": true },
    },
  },
  "isGenderAvailable": { "type": "boolean", "convert": true, "optional": true },
  "childseatAvailable": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "handicapAvailable": { "type": "boolean", "convert": true, "optional": true },
};
bookingValidation.adminCannotAssignProfessionalCanceledRide = {
  "cancellationReason": { "type": "string", "min": 1, "convert": true },
  "professsionalId": { "type": "string", "min": 1, "convert": true },
  "cancelledTime": { "type": "date", "convert": true },
  "bookingId": { "type": "string", "min": 1, "convert": true },
  "professionalCancellationAmount": { "type": "number", "convert": true },
  "isCancelToNearbyProfessionals": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
};
bookingValidation.professionalSelectAndRideBooking = {
  // "bookingId": { "type": "string", "optional": true },
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "min": 1 },
      "phoneCode": { "type": "string", "min": 1 },
      "phoneNumber": { "type": "string", "min": 1 },
    },
  },
  "bookingDate": { "type": "string", "min": 1 },
  "professionalId": { "type": "string", "min": 1 },
  "categoryId": { "type": "string", "min": 1 },
  "vehicleCategoryId": { "type": "string", "min": 1 },
  "pickUpAddressName": { "type": "string", "min": 1, "optional": true },
  "pickUpFullAddress": { "type": "string", "min": 1 },
  "pickUpShortAddress": { "type": "string", "min": 1 },
  "pickUpLat": { "type": "number", "convert": true },
  "pickUpLng": { "type": "number", "convert": true },
  "dropAddressName": { "type": "string", "min": 1, "optional": true },
  "dropFullAddress": { "type": "string", "min": 1 },
  "dropShortAddress": { "type": "string", "min": 1 },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "bookingLocations": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "addressName": { "type": "string", "min": 1, "optional": true },
        "fullAddress": { "type": "string", "min": 1 },
        "shortAddress": { "type": "string", "min": 1 },
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "type": {
          "type": "enum",
          "values": [
            constantUtils.ORIGIN,
            constantUtils.DESTINATION,
            constantUtils.STOP,
          ],
        },
      },
    },
  },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "estimationAmount": { "type": "number", "convert": true },
  "couponId": { "type": "string", "optional": true },
  "couponAmount": { "type": "number", "convert": true, "optional": true },
  "couponValue": { "type": "number", "convert": true, "optional": true },
  "couponType": { "type": "string", "optional": true },
  "couponApplied": { "type": "boolean", "optional": true },
  "isProfessionalTolerance": { "type": "boolean", "optional": true },
  "actualEstimationAmount": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
  "serviceTax": { "type": "number", "convert": true },
  "travelCharge": { "type": "number", "convert": true },
  "distanceFare": { "type": "number", "convert": true },
  "timeFare": { "type": "number", "convert": true },
  "siteCommisionValue": { "type": "number", "convert": true },
  "paymentOption": {
    "type": "enum",
    "values": [
      constantUtils.PAYMENTCASH,
      constantUtils.PAYMENTWALLET,
      constantUtils.PAYMENTCARD,
      constantUtils.PAYMENTCREDIT,
      constantUtils.CONST_PIX,
      constantUtils.CONST_POS,
    ],
  },
  "card": {
    "type": "object",
    "optional": true,
    "strict": true,
    "props": {
      "type": { "type": "string", "optional": true },
      "cardNumber": { "type": "string", "optional": true },
      "expiryYear": { "type": "string", "optional": true },
      "expiryMonth": { "type": "string", "optional": true },
      "holderName": { "type": "string", "optional": true },
      "cvv": { "type": "string", "optional": true },
      "cardId": { "type": "string", "optional": true },
    },
  },
  "isGenderAvailable": { "type": "boolean", "convert": true, "optional": true },
  "childseatAvailable": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "handicapAvailable": { "type": "boolean", "convert": true, "optional": true },
};
bookingValidation.makeCallUsingCallMasking = {
  "rideId": { "type": "string", "min": 4, "convert": true },
  "bookingId": { "type": "string", "min": 4, "convert": true },
  "requestFrom": { "type": "string", "min": 4, "convert": true },
};
bookingValidation.getProfessionalInstantPaymentEarnings = {
  "skip": { "type": "number", "optional": true, "convert": true, "default": 0 },
  "limit": {
    "type": "number",
    "optional": true,
    "convert": true,
    "default": 10000,
  },
};
bookingValidation.rideLiveMeter = {
  "rideId": { "type": "string", "min": 1 },
  "requestType": {
    "type": "enum",
    "values": [constantUtils.CONST_GET, constantUtils.CONST_TRACK],
  },
  "bookingData": { "type": "object", "optional": true },
  "traveledDistance": { "type": "number", "convert": true, "optional": true },
  "traveledTime": { "type": "number", "convert": true, "optional": true },
  "afterRideWaitingMinsWithGracePeriod": {
    "type": "number",
    "convert": true,
    "optional": true,
  },
};
bookingValidation.sendPromotion = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "requestFrom": {
    "type": "enum",
    "values": [constantUtils.CONST_APP],
  },
  "type": {
    "type": "enum",
    "values": [
      constantUtils.SMS,
      constantUtils.EMAIL,
      constantUtils.PUSH,
      constantUtils.EMERGENCY,
    ],
  },
  "userType": {
    "type": "enum",
    "values": [constantUtils.USER, constantUtils.PROFESSIONAL],
  },
  // "template": {
  //   "type": "object",
  //   // "strict": true,
  //   "props": {
  //     "title": { "type": "string" },
  //     "content": { "type": "string" },
  //     "image": { "type": "string", "optional": true },
  //     "subject": { "type": "string", "optional": true },
  //   },
  // },
};
//-------------------------------------
module.exports = bookingValidation;
