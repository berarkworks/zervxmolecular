const constantUtil = require("../utils/constant.util");
const Vaildator = require("fastest-validator");
const mongoose = require("mongoose");

// // custom function
// const JsonArrayValidator = newValidator({
//   "messages": {
//     // Register our new error message text
//     "evenNumber":
//       "The '{field}' field must be an JSON ARRAY but Actual: {actual}",
//   },
// });
// JsonArrayValidator.add(
//   "jsonArray",
//   function ({ schema, messages }, path, context) {
//     return {
//       "source": {},
//     };
//   }
// );

// //

const adminValidation = {};

adminValidation.languageCodeValidation = {
  "languageCode": { "type": "string" },
};
adminValidation.loginWithOtp = {
  "action": { "type": "string", "trim": true, "optional": true },
  "userType": { "type": "string", "trim": true, "optional": true },
  "phoneCode": { "type": "string", "max": 6, "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
};
adminValidation.verifyOtp = {
  ...adminValidation.loginWithOtp,
  "otp": { "type": "string", "optional": true },
};
adminValidation.changeStatus = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "status": { "type": "string" },
};
adminValidation.addWalletAmount = {
  "id": { "type": "string" },
  "amount": { "type": "number", "convert": true },
  "reason": { "type": "string", "optional": true },
  "isReimbursement": { "type": "boolean", "optional": true },
  "type": {
    "type": "enum",
    "values": [constantUtil.CREDIT, constantUtil.DEBIT],
  },
};
adminValidation.getEdit = {
  "id": { "type": "string", "min": 4 },
  "action": { "type": "string", "optional": true },
};
adminValidation.getList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateProfile = {
  "firstName": { "type": "string", "min": 2, "optional": true },
  "lastName": { "type": "string", "min": 1, "optional": true },
  "officeName": { "type": "string", "min": 1, "max": 16, "optional": true },
  "hubsName": { "type": "string", "min": 1, "max": 16, "optional": true },
  "email": { "type": "email" },
  // "gender": {
  //   "type": "enum",
  //   "optional": true,
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "trim": true },
  "avatar": { "type": "string", "optional": true },
  "password": { "type": "string", "optional": true },
};
adminValidation.updateGenerelConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "siteTitle": { "type": "string", "min": 3 },
  "siteUrl": { "type": "string", "min": 6 },
  "bookingPrefix": { "type": "string", "min": 1 },
  "emailAddress": { "type": "email" },
  "currencySymbol": { "type": "string" },
  "currencyCode": { "type": "string" },
  "cronTimeZone": { "type": "string" },
  "driverRequestTimeout": {
    "type": "number",
    "positive": true,
    "convert": true,
  },
  "driverMinAge": { "type": "number", "positive": true, "convert": true },
  "bookingRetryCount": { "type": "number", "positive": true, "convert": true },
  "tipsMinAmount": { "type": "number", "positive": true, "convert": true },
  "tipsMaxAmount": { "type": "number", "positive": true, "convert": true },
  "responceOfficerMinAge": {
    "type": "number",
    "positive": true,
    "convert": true,
  },
  "requestDistance": { "type": "number", "positive": true, "convert": true },
  "approxCalculationDistance": {
    "type": "number",
    "positive": true,
    "convert": true,
  },
  "approxCalculationTime": {
    "type": "number",
    "positive": true,
    "convert": true,
  },
  "minimumWalletAmountToOnline": { "type": "number", "convert": true },
  "onlineStatusThreshold": { "type": "number", "convert": true },

  "adminBookingExpiredThreshold": { "type": "number", "convert": true },
  "retryRequestDistance": {
    "type": "number",
    "positive": true,
    "convert": true,
  },
  "billingDays": { "type": "number", "positive": true, "convert": true },
  "arrayData": {
    "type": "object",
    "strict": true,
    "props": {
      "pageViewLimits": { "type": "array", "items": { "type": "string" } },
    },
  },
  "firebaseAdmin": {
    "type": "object",
    "strict": true,
    "props": {
      "androidUser": { "type": "string", "min": 6 },
      "androidDriver": { "type": "string", "min": 6 },
      "iosUser": { "type": "string", "min": 6 },
      "iosDriver": { "type": "string", "min": 6 },
    },
  },
  // "mapApi": {
  //   "type": "object",
  //   "strict": true,
  //   "props": {
  //     "web": { "type": "string", "min": 6 },
  //     "api": { "type": "string", "min": 6 },
  //     "android": { "type": "string", "min": 6 },
  //     "ios": { "type": "string", "min": 6 },
  //   },
  // },
  // "mapApiArray": {
  //   "type": "object",
  // },
  "spaces": {
    "type": "object",
    "strict": true,
    "props": {
      "spacesKey": { "type": "string", "min": 1 },
      "spacesSecret": { "type": "string", "min": 1 },
      "spacesEndpoint": { "type": "string", "min": 1 },
      "spacesBaseUrl": { "type": "string", "min": 1 },
      "spacesObjectName": { "type": "string", "min": 1 },
      "spacesBucketName": { "type": "string", "min": 1 },
    },
  },
  "tipStatus": { "type": "boolean", "convert": true },
  "driverCancelFeeStatus": { "type": "boolean", "convert": true },
  "isTailRideNeeded": { "type": "boolean", "convert": true },
  "isAddressChangeEnable": { "type": "boolean", "convert": true },
  "isTollNeeded": { "type": "boolean", "convert": true },
  "isOtpNeeded": { "type": "boolean", "convert": true },
  "isProduction": { "type": "boolean", "convert": true },
  "isTollManual": { "type": "boolean", "convert": true },
  "isTollToDriver": { "type": "boolean", "convert": true },
  "isEnableIntercityBoundaryFareToProfessional": {
    "type": "boolean",
    "convert": true,
  },
  "isReferralNeeded": { "type": "boolean", "convert": true },
  "isCancelToNextProfessional": { "type": "boolean", "convert": true },
  "lightLogo": { "type": "string" },
  "darkLogo": { "type": "string" },
  "mobileLogo": { "type": "string" },
  "favicon": { "type": "string" },
  "paymentSection": {
    "type": "enum",
    "values": [constantUtil.CASHANDWALLET, constantUtil.ALL],
  },
  "isCountryCodeNeeded": { "type": "boolean", "convert": true },
  "isManualMeterNeeded": { "type": "boolean", "convert": true },
  "isSocketPolling": { "type": "boolean", "convert": true },
  "defaultDialCode": { "type": "string", "min": 1 },
  "distanceCalculateType": { "type": "string", "min": 1 },

  "lastPriorityStatus": { "type": "boolean" },
  "lastPriorityCount": { "type": "number" },
  "timeDurationThershold": { "type": "number" },
  "turfDistanceThershold": { "type": "number" },
  "androidVersionKey": { "type": "number", "convert": true },
  "androidUserVersionKey": { "type": "number", "convert": true },
  "androidDriverVersionKey": { "type": "number", "convert": true },
  "iosVersionKey": { "type": "number", "convert": true },
  "iosUserVersionKey": { "type": "number", "convert": true },
  "iosDriverVersionKey": { "type": "number", "convert": true },
  "stopLimit": { "type": "number", "convert": true },
  // "appUpdateStatus": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.NONE,
  //     constantUtil.OPTIONAL,
  //     constantUtil.FORCEUPDATE,
  //   ],
  // },
  // "androidUserAppUpdateStatus": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.NONE,
  //     constantUtil.OPTIONAL,
  //     constantUtil.FORCEUPDATE,
  //   ],
  // },
  // "androidDriverAppUpdateStatus": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.NONE,
  //     constantUtil.OPTIONAL,
  //     constantUtil.FORCEUPDATE,
  //   ],
  // },
  // "iosDriverAppUpdateStatus": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.NONE,
  //     constantUtil.OPTIONAL,
  //     constantUtil.FORCEUPDATE,
  //   ],
  // },
  // "iosUserAppUpdateStatus": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.NONE,
  //     constantUtil.OPTIONAL,
  //     constantUtil.FORCEUPDATE,
  //   ],
  // },
  "siteStatus": { "type": "boolean" },
  "isRerouteEnable": { "type": "boolean" },
  "isWaitingEnable": { "type": "boolean" },
  "siteStatusMessage": { "type": "string" },
  "smsType": {
    "type": "enum",
    "values": [constantUtil.NONE, constantUtil.FIREBASE, constantUtil.TWILIO],
  },
  "redirectUrls": {
    "type": "object",
    "strict": true,
    "props": {
      "userPlayStorePackagename": { "type": "string", "min": 1 },
      "userPlayStore": { "type": "string", "min": 1 },
      "driverPlayStorePackagename": { "type": "string", "min": 1 },
      "driverPlayStore": { "type": "string", "min": 1 },
      "userAppstoreBundleId": { "type": "string", "min": 1 },
      "userAppstore": { "type": "string", "min": 1 },
      "driverAppstoreBundleId": { "type": "string", "min": 1 },
      "driverAppstore": { "type": "string", "min": 1 },
      "userAppleId": { "type": "string", "min": 1 },
      "driverAppleId": { "type": "string", "min": 1 },
      "faqUrl": { "type": "string", "min": 1 },
      "termsAndConditionUrl": { "type": "string", "min": 1 },
      "privacyUrl": { "type": "string", "min": 1 },
      "aboutUrl": { "type": "string", "min": 1 },
      "paymentGatewayUrl": { "type": "url" },
      "paymentRedirectApiUrl": { "type": "url" },
      "trackBooking": { "type": "url" },
      "websiteUrl": { "type": "url" },
    },
  },
  "isQuickTripNeeded": { "type": "boolean", "convert": true },
  "isDailyTripNeeded": { "type": "boolean", "convert": true },
  "isOutstationNeeded": { "type": "boolean", "convert": true },
  "isRentalNeeded": { "type": "boolean", "convert": true },
  "isAirportTripNeeded": { "type": "boolean", "convert": true },
  "isCarpoolTripNeeded": { "type": "boolean", "convert": true },
  "isAmbulanceNeeded": { "type": "boolean", "convert": true },
  "isSmallPackageNeeded": { "type": "boolean", "convert": true },
  "isHandicapNeeded": { "type": "boolean", "convert": true },
  "isChildseatNeeded": { "type": "boolean", "convert": true },
  "isgenderTripNeeded": { "type": "boolean", "convert": true },
  "walletSendMinAmount": { "type": "number", "convert": true },
  "walletSendMaxAmount": { "type": "number", "convert": true },
  "walletMinAmountThreshold": { "type": "number", "convert": true },
  "walletRechargeMaxAmount": { "type": "number", "convert": true },
  "walletRechargeMinAmount": { "type": "number", "convert": true },
  "withDrawMinAmount": { "type": "number", "convert": true },
  "emailConfigurationEnable": { "type": "boolean", "convert": true },
  "smsConfigurationEnable": { "type": "boolean", "convert": true },
  "callCenterPhone": { "type": "string" },
  "defaultCountryCode": { "type": "string" },
  "sosEmergencyNumber": { "type": "string" },
  "isVinNeeded": { "type": "boolean" },
  "documentExpireThreshold": { "type": "number" },
  "nextProfessionalThreshold": { "type": "number", "convert": true },
  "adminAssignThreshold": { "type": "number", "convert": true },
  "isMapOptimized": { "type": "boolean", "convert": true },
  "isUserReroute": { "type": "boolean", "convert": true },
  "isDriverReroute": { "type": "boolean", "convert": true },
  "isHubNeeded": {
    "type": "boolean",
    "convert": true,
  },
  "payoutOptions": {
    "type": "enum",
    "values": [
      constantUtil.AUTOMATED,
      constantUtil.MANUAL,
      constantUtil.DISPLAYINFO,
    ],
  },
  // algorithm Type
  "bookingAlgorithm": {
    "type": "enum",
    "values": [
      constantUtil.COMPETITIVE,
      constantUtil.NEAREST,
      constantUtil.SHORTEST,
    ],
  },
  "nearestAlgorithm": {
    "type": "object",
    "props": {
      "bookingRequestCountForProfessional": {
        "type": "number",
        "convert": true,
      },
      "repeatProfessioalRequest": {
        "type": "boolean",
        "convert": true,
      },
    },
  },
  "isProfessionalSubscriptionModel": {
    "type": "boolean",
    "covert": true,
  },
  "brandImage": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "bookingAppImage": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "bookingAppHomePageMessage": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "companyAddress": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "companyRegNumber": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "companyCopyRight": {
    "type": "string",
    "optional": true,
    "convert": true,
    "default": "",
  },
  "defaultPayment": {
    "type": "string",
    "optional": true,
    "convert": true,
  },
  "location": {
    "type": "object",
    "props": {
      "lat": {
        "type": "number",
        "convert": true,
      },
      "lng": {
        "type": "number",
        "convert": true,
      },
    },
  },
};
adminValidation.updateAppConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  // "cronTimeZone": { "type": "string" },
  // "driverRequestTimeout": {
  //   "type": "number",
  //   "positive": true,
  //   "convert": true,
  // },
  // "bookingRetryCount": { "type": "number", "positive": true, "convert": true },
  // "requestDistance": { "type": "number", "positive": true, "convert": true },
  // "approxCalculationDistance": {
  //   "type": "number",
  //   "positive": true,
  //   "convert": true,
  // },
  // "approxCalculationTime": {
  //   "type": "number",
  //   "positive": true,
  //   "convert": true,
  // },
  // "minimumWalletAmountToOnline": {
  //   "type": "number",
  //   "convert": true,
  // },
  // "onlineStatusThreshold": {
  //   "type": "number",
  //   "convert": true,
  // },
  "billingDays": { "type": "number", "positive": true, "convert": true },
  "firebaseAdmin": {
    "type": "object",
    "strict": true,
    "props": {
      "androidUser": { "type": "string", "min": 6 },
      "androidDriver": { "type": "string", "min": 6 },
      "iosUser": { "type": "string", "min": 6 },
      "iosDriver": { "type": "string", "min": 6 },
    },
  },
  "mapApi": {
    "type": "object",
    "strict": true,
    "props": {
      "web": { "type": "string", "min": 6 },
      "api": { "type": "string", "min": 6 },
      "android": { "type": "string", "min": 6 },
      "androidUser": { "type": "string", "min": 6 },
      "androidProfessional": { "type": "string", "min": 6 },
      "ios": { "type": "string", "min": 6 },
      "iosUser": { "type": "string", "min": 6 },
      "iosProfessional": { "type": "string", "min": 6 },
    },
  },
  "mapApiArray": {
    "type": "object",
  },
  "spaces": {
    "type": "object",
    "strict": true,
    "props": {
      "spacesKey": { "type": "string", "min": 1 },
      "spacesSecret": { "type": "string", "min": 1 },
      "spacesEndpoint": { "type": "string", "min": 1 },
      "spacesBaseUrl": { "type": "string", "min": 1 },
      "spacesObjectName": { "type": "string", "min": 1 },
      "spacesBucketName": { "type": "string", "min": 1 },
    },
  },
  "tipStatus": { "type": "boolean", "convert": true },
  "driverCancelFeeStatus": { "type": "boolean", "convert": true },
  "isTailRideNeeded": { "type": "boolean", "convert": true },
  // "isAddressChangeEnable": { "type": "boolean", "convert": true },
  "isTollNeeded": { "type": "boolean", "convert": true },
  "isOtpNeeded": { "type": "boolean", "convert": true },
  "isProduction": { "type": "boolean", "convert": true },
  "isTollManual": { "type": "boolean", "convert": true },
  "isTollToDriver": { "type": "boolean", "convert": true },
  "isReferralNeeded": { "type": "boolean", "convert": true },
  // "isCancelToNextProfessional": { "type": "boolean", "convert": true },
  "isCountryCodeNeeded": { "type": "boolean", "convert": true },
  "isManualMeterNeeded": { "type": "boolean", "convert": true },
  "isSocketPolling": { "type": "boolean", "convert": true },
  "defaultDialCode": { "type": "string", "min": 1 },

  "lastPriorityStatus": { "type": "boolean" },
  "lastPriorityCount": { "type": "number" },
  "timeDurationThershold": { "type": "number" },
  "turfDistanceThershold": { "type": "number" },
  "androidVersionKey": { "type": "number", "convert": true },
  "androidUserVersionKey": { "type": "number", "convert": true },
  "androidDriverVersionKey": { "type": "number", "convert": true },
  "iosVersionKey": { "type": "number", "convert": true },
  "iosUserVersionKey": { "type": "number", "convert": true },
  "iosDriverVersionKey": { "type": "number", "convert": true },
  "stopLimit": { "type": "number", "convert": true },
  "appUpdateStatus": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.OPTIONAL,
      constantUtil.FORCEUPDATE,
    ],
  },
  "androidUserAppUpdateStatus": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.OPTIONAL,
      constantUtil.FORCEUPDATE,
    ],
  },
  "androidDriverAppUpdateStatus": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.OPTIONAL,
      constantUtil.FORCEUPDATE,
    ],
  },
  "iosDriverAppUpdateStatus": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.OPTIONAL,
      constantUtil.FORCEUPDATE,
    ],
  },
  "iosUserAppUpdateStatus": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.OPTIONAL,
      constantUtil.FORCEUPDATE,
    ],
  },
  // "siteStatus": { "type": "boolean" },
  "isRerouteEnable": { "type": "boolean" },
  "isWaitingEnable": { "type": "boolean" },
  // "siteStatusMessage": { "type": "string" },
  "smsType": {
    "type": "enum",
    "values": [constantUtil.NONE, constantUtil.FIREBASE, constantUtil.TWILIO],
  },
  "isQuickTripNeeded": { "type": "boolean", "convert": true },
  "isDailyTripNeeded": { "type": "boolean", "convert": true },
  "isOutstationNeeded": { "type": "boolean", "convert": true },
  "isRentalNeeded": { "type": "boolean", "convert": true },
  "isAirportTripNeeded": { "type": "boolean", "convert": true },
  "isCarpoolTripNeeded": { "type": "boolean", "convert": true },
  "isAmbulanceNeeded": { "type": "boolean", "convert": true },
  "isSmallPackageNeeded": { "type": "boolean", "convert": true },
  "isHandicapNeeded": { "type": "boolean", "convert": true },
  "isChildseatNeeded": { "type": "boolean", "convert": true },
  "isPetAllowed": { "type": "boolean", "convert": true },
  "isgenderTripNeeded": { "type": "boolean", "convert": true },
  "emailConfigurationEnable": { "type": "boolean", "convert": true },
  "smsConfigurationEnable": { "type": "boolean", "convert": true },
  "callCenterPhone": { "type": "string" },
  "defaultCountryCode": { "type": "string" },
  "sosEmergencyNumber": { "type": "string" },
  "isVinNeeded": { "type": "boolean" },
  // "documentExpireThreshold": { "type": "number" },
  // "nextProfessionalThreshold": { "type": "number", "convert": true },
  // "adminAssignThreshold": { "type": "number", "convert": true },
  "isMapOptimized": { "type": "boolean", "convert": true },
  "isUserReroute": { "type": "boolean", "convert": true },
  "isDriverReroute": { "type": "boolean", "convert": true },
  "isHubNeeded": {
    "type": "boolean",
    "convert": true,
  },
  "payoutOptions": {
    "type": "enum",
    "values": [
      constantUtil.AUTOMATED,
      constantUtil.MANUAL,
      constantUtil.DISPLAYINFO,
    ],
  },
  // algorithm Type
  "bookingAlgorithm": {
    "type": "enum",
    "values": [
      constantUtil.COMPETITIVE,
      constantUtil.NEAREST,
      constantUtil.SHORTEST,
    ],
  },
  "nearestAlgorithm": {
    "type": "object",
    "props": {
      "bookingRequestCountForProfessional": {
        "type": "number",
        "convert": true,
      },
      "repeatProfessioalRequest": { "type": "boolean", "convert": true },
    },
  },
  "isProfessionalSubscriptionModel": { "type": "boolean", "covert": true },
  // "brandImage": {
  //   "type": "string",
  //   "optional": true,
  //   "convert": true,
  //   "default": "",
  // },
  "dbBackupType": {
    "type": "enum",
    "values": [
      constantUtil.NONE,
      constantUtil.CONST_DUMP,
      constantUtil.CONST_GZIP,
    ],
  },
  "nationalIdCheck": {
    "type": "object",
    "props": {
      "type": { "type": "string", "convert": true },
      "accessToken": { "type": "string", "convert": true },
    },
  },
};
adminValidation.updateMapConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  // "mapApi": {
  //   "type": "object",
  //   "strict": true,
  //   "props": {
  //     "web": { "type": "string", "min": 6 },
  //     "api": { "type": "string", "min": 6 },
  //     "android": { "type": "string", "min": 6 },
  //     "ios": { "type": "string", "min": 6 },
  //   },
  // },
  // "mapApiArray": {
  //   "type": "object",
  // },
};
adminValidation.updatePackageConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "isEnableOTPVerification": { "type": "boolean", "convert": true },
  "isEnableNoContactDelivery": { "type": "boolean", "convert": true },
  "isEnablePetsAtMyHome": { "type": "boolean", "convert": true },

  "userImage": {
    "type": "object",
    "strict": true,
    "props": {
      "isEnableImageUpload": { "type": "boolean", "convert": true },
      "isImageUploadMandatory": { "type": "boolean", "convert": true },
      "uploadImageLimit": { "type": "number", "convert": true },
    },
  },
  "professionalImagePickup": {
    "type": "object",
    "strict": true,
    "props": {
      "isEnableImageUpload": { "type": "boolean", "convert": true },
      "isImageUploadMandatory": { "type": "boolean", "convert": true },
      "uploadImageLimit": { "type": "number", "convert": true },
    },
  },
  "professionalImageDelivery": {
    "type": "object",
    "strict": true,
    "props": {
      "isEnableImageUpload": { "type": "boolean", "convert": true },
      "isImageUploadMandatory": { "type": "boolean", "convert": true },
      "uploadImageLimit": { "type": "number", "convert": true },
    },
  },
  "packageReceiver": {
    "type": "object",
    "strict": true,
    "props": {
      "isEnableImageUpload": { "type": "boolean", "convert": true },
      "isImageUploadMandatory": { "type": "boolean", "convert": true },
    },
  },
  // "services": [{
  //   "type": "object",
  //   "props": {
  //     "id": {
  //       "type": mongoose.Schema.Types.ObjectId,
  //       // "default": mongoose.Schema.Types.ObjectId,
  //     },
  //     "value": { "type": "String" },
  //     "isActive": { "type": "boolean", "convert": true },
  //   },
  // }],
};
adminValidation.updateFirebaseConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "firebaseConfig": {
    "type": "object",
    "strict": true,
    "props": {
      "apiKey": { "type": "string", "min": 6 },
      "authDomain": { "type": "string", "min": 6 },
      "databaseURL": { "type": "string", "min": 6 },
      "projectId": { "type": "string", "min": 6 },
      "storageBucket": { "type": "string", "min": 6 },
      "messagingSenderId": { "type": "string", "min": 6 },
      "appId": { "type": "string", "min": 6 },
      "measurementId": { "type": "string", "min": 6 },
    },
  },
  "firebaseNotifyConfig": {
    "type": "object",
    "strict": true,
    "props": {
      "type": { "type": "string", "min": 6 },
      "project_id": { "type": "string", "min": 6 },
      "private_key_id": { "type": "string", "min": 6 },
      "private_key": { "type": "string", "min": 6 },
      "client_email": { "type": "string", "min": 6 },
      "client_id": { "type": "string", "min": 6 },
      "auth_uri": { "type": "string", "min": 6 },
      "token_uri": { "type": "string", "min": 6 },
      "auth_provider_x509_cert_url": { "type": "string", "min": 6 },
      "client_x509_cert_url": { "type": "string", "min": 6 },
      "universe_domain": { "type": "string", "min": 6 },
    },
  },
};
adminValidation.updateBlockProfessionalConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },

  "expiryDays": { "type": "number", "convert": true },
  "thresholdValue": { "type": "number", "convert": true },

  "fromValue": { "type": "number", "convert": true },
  "toValue": { "type": "number", "convert": true },
  "blockDays": { "type": "number", "convert": true },

  "fromValue1": { "type": "number", "convert": true },
  "toValue2": { "type": "number", "convert": true },
  "blockDays1": { "type": "number", "convert": true },

  "fromValue2": { "type": "number", "convert": true },
  "toValue2": { "type": "number", "convert": true },
  "blockDays2": { "type": "number", "convert": true },

  "fromValue3": { "type": "number", "convert": true },
  "toValue3": { "type": "number", "convert": true },
  "blockDays3": { "type": "number", "convert": true },

  "acceptanceRate": { "type": "number", "convert": true },
  "rejectionRate": { "type": "number", "convert": true },
  "cancellationRate": { "type": "number", "convert": true },

  "isEnableNotification": { "type": "boolean", "convert": true },
};

adminValidation.updateSMSConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "bookingFrom": {
        "type": "enum",
        "values": [
          constantUtil.COMMON,
          constantUtil.CONST_WEBAPP,
          constantUtil.CONST_APP,
          constantUtil.ADMIN,
          constantUtil.COORPERATEOFFICE,
        ],
      },
      "notifyType": {
        "type": "enum",
        "values": [
          constantUtil.OTP,
          constantUtil.SMS,
          constantUtil.PROMOTION,
          constantUtil.EMERGENCY,
          constantUtil.PROFESSIONALACTIVATED,
          constantUtil.ACCEPTED,
          constantUtil.ARRIVED,
          constantUtil.STARTED,
          constantUtil.ENDED,
          constantUtil.PROFESSIONALCANCELLED,
          constantUtil.FORCEASSIGNBYADMIN,
        ],
      },
      "accountSid": { "type": "string", "min": 3 },
      "authToken": { "type": "string", "min": 3 },
      "accountNumber": { "type": "string", "min": 3 },
      "mode": { "type": "string" },
      "isEnableSMSGatwayOTP": { "type": "boolean", "convert": true },
      "isEnableWhatsupOTP": { "type": "boolean", "convert": true },
      "isEnableDeviceNetworkOTP": { "type": "boolean", "convert": true },
      "emailConfigurationEnable": { "type": "boolean", "convert": true },
      "isEnablePushNotify": { "type": "boolean", "convert": true },
      "smsType": {
        "type": "enum",
        "values": [
          constantUtil.FIREBASE,
          constantUtil.TWILIO,
          constantUtil.WHATSUP,
          constantUtil.SENDIFY,
          constantUtil.AFRO,
          constantUtil.NONE,
          constantUtil.TUNISIESMS,
        ],
      },
      "smsNotificationsEnable": { "type": "boolean", "convert": true },
      "smsTypeNotifications": {
        "type": "enum",
        "values": [
          constantUtil.FIREBASE,
          constantUtil.AFRO,
          constantUtil.TWILIO,
          constantUtil.WHATSUP,
          constantUtil.SENDIFY,
          constantUtil.NONE,
          constantUtil.TUNISIESMS,
        ],
      },
      "senderName": { "type": "string" },
      "senderID": { "type": "string" },
      "accessToken": { "type": "string" },
      "notifyMessage": { "type": "string" },
      "privileges": { "type": "object", "optional": true },
      "isEnableSandboxMode": { "type": "boolean" },
      "sandboxPhone": { "type": "object" },
      "templateFields": {
        "type": "array",
        "items": { "type": "string" },
        "optional": true,
      },
      "description": { "type": "string", "optional": true },
    },
  },
};
adminValidation.updateCALLConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "callType": {
        "type": "enum",
        "values": [
          constantUtil.TELECMI,
          constantUtil.TWILIO,
          constantUtil.SINCH,
          constantUtil.SENDBIRD,
          constantUtil.NONE,
        ],
      },
      "accountAccessKey": { "type": "string", "min": 6 },
      "authToken": { "type": "string", "min": 6 },
      "defaultCountryCode": { "type": "string" },
      "virtualNumber": { "type": "string", "min": 6 },
      "mode": { "type": "string" },
      "isNeedCallMasking": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "senderID": { "type": "string" },
      "isEnableInAppCalling": {
        "type": "boolean",
        "convert": true,
      },
    },
  },
};
adminValidation.updateSMTPConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "host": { "type": "string", "min": 4 },
      "port": { "type": "string" },
      "client": { "type": "string", "min": 4 },
      "secret": { "type": "string", "min": 4 },
      "mode": { "type": "string" },
    },
  },
};
adminValidation.updateVehicleCategory = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "clientId": { "type": "string", "optional": true, "min": 8 },
  "vehicleCategory": { "type": "string", "min": 2 },
  "seatCount": { "type": "number", "convert": true, "min": 1, "max": 100 },
  // "childSeatCount": { "type": "number", "convert": true, "min": 1, "max": 20 },
  "sharePercent": { "type": "number", "convert": true, "min": 0, "max": 100 },
  "mandatoryShare": { "type": "boolean", "convert": true },
  "shareStatus": { "type": "boolean", "convert": true },
  "status": { "type": "string" },
  "categoryImage": { "type": "string", "optional": true },
  "categoryMapImage": { "type": "string", "optional": true },
};
adminValidation.updateServiceCategory = {
  "_id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "categoryName": { "type": "string", "min": 4 },
      "locationName": { "type": "string", "min": 4 },
      "address": { "type": "string", "convert": true, "optional": true },
      "corporateId": { "type": "string", "convert": true, "optional": true },
      "serviceAreaId": { "type": "string", "convert": true, "optional": true },
      "recordType": { "type": "string" },

      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "type": { "type": "string" },
          "coordinates": {
            "type": "array",
            "items": {
              "type": "array",
              "items": { "type": "array", "items": "number" },
            },
          },
        },
      },
      "isPeakFareAvailable": {
        "type": "object",
        "strict": true,
        "props": {
          "status": { "type": "boolean", "convert": true },
          "startTime": { "type": "string", "optional": true },
          "endTime": { "type": "string", "optional": true },
          "weekDays": {
            "type": "array",
            // "strict": true,
            "props": {
              "days": { "type": "string", "convert": true },
              "status": { "type": "boolean", "convert": true },
              "startTime": { "type": "string", "optional": true },
              "endTime": { "type": "string", "optional": true },
              "startTime1": { "type": "string", "optional": true },
              "endTime1": { "type": "string", "optional": true },
              "startTime2": { "type": "string", "optional": true },
              "endTime2": { "type": "string", "optional": true },
            },
          },
        },
      },
      "isNightFareAvailable": {
        "type": "object",
        "strict": true,
        "props": {
          "status": { "type": "boolean", "convert": true },
          "startTime": { "type": "string", "optional": true },
          "endTime": { "type": "string", "optional": true },
        },
      },

      "rental": {
        "type": "object",
        "props": {
          "isActive": { "type": "boolean", "convert": true },
          "timeRange": {
            "type": "object",
            "props": {
              "from": { "type": "number", "convert": true },
              "to": { "type": "number", "convert": true },
              "type": { "type": "enum", "values": ["HR", "MIN"] },
            },
          },
          "distanceRange": {
            "type": "object",
            "props": {
              "distanceForATimeRange": { "type": "number", "convert": true },
              "type": { "type": "enum", "values": ["KM", "MILES"] },
            },
          },
        },
      },
      "maxRetryRequestDistance": { "type": "number", "convert": true },
      "categoryData": {
        "type": "object",
        "strict": true,
        "props": {
          "vehicles": {
            "type": "array",
            "items": {
              "type": "object",
              "strict": true,
              "props": {
                "recordNo": { "type": "number", "convert": true },
                "categoryId": { "type": "string" },
                "isDefaultCategory": { "type": "boolean" },
                "isSubCategoryAvailable": { "type": "boolean" },
                "isForceAppliedToProfessional": { "type": "boolean" },
                "isSharerideForceAppliedToProfessional": { "type": "boolean" },
                "forAdminAndMobile": { "type": "boolean" },
                "subCategoryIds": {
                  "type": "array",
                  "items": { "type": "string" },
                },
                "baseFare": { "type": "number", "convert": true },
                "isEnableDynamicTravelCharge": {
                  "type": "boolean",
                  "convert": true,
                },
                "fromDistance": { "type": "number", "convert": true },
                "toDistance": { "type": "number", "convert": true },
                "farePerDistance": { "type": "number", "convert": true },
                "farePerMinute": { "type": "number", "convert": true },

                "fromDistance1": { "type": "number", "convert": true },
                "toDistance1": { "type": "number", "convert": true },
                "farePerDistance1": { "type": "number", "convert": true },
                "farePerMinute1": { "type": "number", "convert": true },

                "fromDistance2": { "type": "number", "convert": true },
                "toDistance2": { "type": "number", "convert": true },
                "farePerDistance2": { "type": "number", "convert": true },
                "farePerMinute2": { "type": "number", "convert": true },

                "fromDistance3": { "type": "number", "convert": true },
                "toDistance3": { "type": "number", "convert": true },
                "farePerDistance3": { "type": "number", "convert": true },
                "farePerMinute3": { "type": "number", "convert": true },

                "fromDistance4": { "type": "number", "convert": true },
                "toDistance4": { "type": "number", "convert": true },
                "farePerDistance4": { "type": "number", "convert": true },
                "farePerMinute4": { "type": "number", "convert": true },

                "siteCommission": { "type": "number", "convert": true },
                "siteCommissionManualMeter": {
                  "type": "number",
                  "convert": true,
                },
                "serviceTaxPercentage": { "type": "number", "convert": true },
                "peakFare": {
                  "type": "number",
                  // "min": "1",
                  "convert": true,
                },
                "nightFare": {
                  "type": "number",
                  // "min": "1",
                  "convert": true,
                },
                "multiStopCharge": { "type": "number", "convert": true },
                "isEnableRoundTrip": { "type": "boolean", "convert": true },
                "intercityBoundaryFareType": {
                  "type": "enum",
                  "values": [
                    constantUtil.FLAT,
                    constantUtil.PERCENTAGE,
                    constantUtil.NONE,
                  ],
                },
                "intercityBoundaryFare": { "type": "number", "convert": true },
                "intercityPickupFarePerDistance": {
                  "type": "number",
                  "convert": true,
                },
                "intercityPickupFarePerMinites": {
                  "type": "number",
                  "convert": true,
                },
                "isShowEstimationfareRange": {
                  "type": "boolean",
                  "convert": true,
                },
                "showEstimationfareRangeAmount": { "type": "number" },
                "isSupportShareRide": { "type": "boolean", "convert": true },
                "sharePercentageUser1": { "type": "number" },
                "sharePercentageUser2": { "type": "number" },

                // * surcharge
                "surchargeFee": { "type": "number", "convert": true },
                "isSurchargeEnable": { "type": "boolean", "convert": true },
                "surchargeTitle": { "type": "string", "convert": true },

                "isScheduleSupport": { "type": "boolean", "convert": true },
                "isPackageSupport": { "type": "boolean", "convert": true },
                "cancellation": {
                  "type": "object",
                  "strict": true,
                  "props": {
                    "status": { "type": "boolean", "convert": true },
                    "amount": { "type": "number", "convert": true },
                    "thresholdMinute": { "type": "number", "convert": true },
                  },
                },
                "professionalCancellation": {
                  "type": "object",
                  "strict": true,
                  "props": {
                    "status": { "type": "boolean", "convert": true },
                    "amount": { "type": "number", "convert": true },
                    "thresholdMinute": { "type": "number", "convert": true },
                  },
                },
                "minimumCharge": {
                  "type": "object",
                  "strict": true,
                  "props": {
                    "status": { "type": "boolean", "convert": true },
                    "amount": { "type": "number", "convert": true },
                  },
                },
                "fareBreakUpDetail": {
                  "type": "object",
                  "props": {
                    "title": { "type": "string" },
                    "description": { "type": "string" },
                    "fareMessage": { "type": "string" },
                    "amenities": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "strict": true,
                        "props": {
                          "title": { "type": "string" },
                          "image": { "type": "string" },
                        },
                      },
                    },
                    "fleets": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "strict": true,
                        "props": {
                          "title": { "type": "string" },
                          "image": { "type": "string" },
                        },
                      },
                    },
                    "guidelines": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "strict": true,
                        "props": {
                          "title": { "type": "string" },
                          "points": {
                            "type": "array",
                            "items": { "type": "string" },
                          },
                        },
                      },
                    },
                  },
                },
                "waitingChargeDetails": {
                  "type": "object",
                  "strict": true,
                  "props": {
                    "isBeforeRideWaitingCharge": {
                      "type": "boolean",
                      "convert": true,
                    },
                    "beforeRideWaitingChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                    "beforeRideWaitingGracePeriod": {
                      "type": "number",
                      "convert": true,
                    },
                    "isAfterRideWaitingCharge": {
                      "type": "boolean",
                      "convert": true,
                    },
                    "afterRideWaitingChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                    "afterRideWaitingGracePeriod": {
                      "type": "number",
                      "convert": true,
                    },
                  },
                },
                "assistanceCareDetails": {
                  "type": "object",
                  "strict": true,
                  "props": {
                    "isEnableAssistanceCare": {
                      "type": "boolean",
                      "convert": true,
                    },
                    "beforeRideAssistanceCareGracePeriod": {
                      "type": "number",
                      "convert": true,
                    },
                    "beforeRideAssistanceCareNormalChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                    "beforeRideAssistanceCareRevisedChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                    "afterRideAssistanceCareGracePeriod": {
                      "type": "number",
                      "convert": true,
                    },
                    "afterRideAssistanceCareNormalChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                    "afterRideAssistanceCareRevisedChargePerMin": {
                      "type": "number",
                      "convert": true,
                    },
                  },
                },
                "rental": {
                  "type": "object",
                  "props": {
                    "isActive": { "type": "boolean" },
                    "estimate": {
                      "type": "object",
                      "props": {
                        "farePerDistance": {
                          "type": "number",
                          "convert": true,
                        },
                        "farePerTime": {
                          "type": "number",
                          "convert": true,
                        },
                      },
                    },
                    "exceed": {
                      "type": "object",
                      "props": {
                        "farePerDistance": {
                          "type": "number",
                          "convert": true,
                        },
                        "farePerTime": {
                          "type": "number",
                          "convert": true,
                        },
                      },
                    },
                  },
                },
                "cancellationFeeCreditToUserPrecentage": {
                  "type": "number",
                  "convert": true,
                },
                "cancellationFeeCreditToProfessionalPrecentage": {
                  "type": "number",
                  "convert": true,
                },
              },
            },
          },
        },
      },
      "currencyCode": { "type": "string" },
      // 'country': {
      //   'type': 'object',
      //   'props': {
      //     'name': { 'type': 'string' },
      //     'code': { 'type': 'string' },
      //   },
      // },

      "paymentMode": {
        "type": "array",
        "items": { "type": "string" },
      },
      "payoutOptions": {
        "type": "enum",
        "values": [
          constantUtil.AUTOMATED,
          constantUtil.MANUAL,
          constantUtil.DISPLAYINFO,
        ],
        "optional": true,
      },
      "currencySymbol": { "type": "string" },
      "distanceType": {
        "type": "enum",
        "values": [constantUtil.KM, constantUtil.MILES],
      },
      "status": { "type": "string" },
      "isShowProfessionalList": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "isRestrictMaleBookingToFemaleProfessional": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "isNeedSecurityImageUpload": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "roundingType": {
        "type": "enum",
        "values": [constantUtil.CONST_DECIMAL, constantUtil.CONST_INTEGER],
        "default": constantUtil.CONST_INTEGER,
      },
      "roundingFactor": { "type": "number", "convert": true, "default": false },
    },
  },
};
adminValidation.updatePaymentGateway = {
  "_id": { "type": "string", "min": 8 },
  "data": {
    "type": "object",
    "strict": false,
    "props": {
      "mode": {
        "type": "enum",
        "values": [constantUtil.LIVE, constantUtil.SANDBOX],
      },
      "paymentType": {
        "type": "enum",
        "values": [
          constantUtil.CARD,
          constantUtil.CASH,
          constantUtil.WALLET,
          constantUtil.REDEEM,
        ],
      },
      "status": {
        "type": "enum",
        "values": [constantUtil.ACTIVE, constantUtil.INACTIVE],
      },
      "name": { "type": "string", "min": 2, "max": 20 },
      "gateWay": { "type": "string", "min": 2, "max": 20 },
      "payoutGateWay": { "type": "string", "min": 2, "max": 20 },
      "isEditNeeded": { "type": "boolean" },
      "availableCurrencies": { "type": "array", "optional": true },
      //
      "testSecretKey": { "type": "string", "optional": true },
      "testPublicKey": { "type": "string", "optional": true },
      "testPrivateKey": { "type": "string", "optional": true },
      "testEncryptionKey": { "type": "string", "optional": true },
      //
      "liveSecretKey": { "type": "string", "optional": true },
      "livePublicKey": { "type": "string", "optional": true },
      "livePrivateKey": { "type": "string", "optional": true },
      "liveEncryptionKey": { "type": "string", "optional": true },
    },
  },
};
adminValidation.userGetAvailableCurrencies = {};
adminValidation.updateOperatorsRole = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "role": { "type": "string" },
      "status": { "type": "string" },
      "privileges": { "type": "object", "optional": true },
      "forceUpdate": { "type": "boolean", "convert": true },
    },
  },
};
adminValidation.updateOperator = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "firstName": { "type": "string", "min": 2 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string" },
  "status": { "type": "string" },
  "operatorRole": { "type": "string" },
  "avatar": { "type": "string", "optional": true },
  "extraPrivileges": { "type": "string", "optional": true },
  "password": { "type": "string", "optional": true },
};
adminValidation.updateResponseOffice = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "email": { "type": "email" },
      "officeName": { "type": "string" },
      "officeId": { "type": "string" },
      "serviceArea": { "type": "string" },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "line1": { "type": "string" },
          "city": { "type": "string" },
          "state": { "type": "string" },
          "country": { "type": "string" },
          "zipcode": { "type": "string", "optional": true },
          "fulladdress": { "type": "string" },
        },
      },
      "phone": {
        "type": "object",
        "strict": true,
        "props": {
          "code": { "type": "string" },
          "number": { "type": "string" },
        },
      },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "showAddress": { "type": "string" },
      "status": { "type": "string" },
      "password": { "type": "string", "optional": true },
    },
  },
};
adminValidation.updateDocument = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "docsName": { "type": "string", "min": 4 },
  "docsFor": {
    "type": "enum",
    "values": [constantUtil.PROFILEDOCUMENTS, constantUtil.DRIVERDOCUMENTS],
  },
  "docsDetail": { "type": "string", "optional": true, "min": 4 },
  "docsExpiry": { "type": "boolean", "convert": true },
  "docsMandatory": { "type": "boolean", "convert": true },
  "status": { "type": "string" },
};
adminValidation.updateCancellationReason = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "reason": { "type": "string", "min": 4 },
      "reasonFor": {
        "type": "enum",
        "values": [
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.ADMIN,
        ],
      },
      "bookingStatus": {
        "type": "enum",
        "values": [
          constantUtil.COMMON,
          constantUtil.AWAITING,
          constantUtil.ARRIVED,
          constantUtil.ACCEPTED,
          constantUtil.STARTED,
        ],
      },
      "isCancelToNextProfessional": { "type": "boolean", "convert": true },
      "isHasPenalty": { "type": "boolean", "convert": true },
      "penaltyChargeFrom": {
        "type": "enum",
        "values": [
          constantUtil.NONE,
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.ADMIN,
        ],
      },
      "penaltyCreditTo": {
        "type": "enum",
        "values": [
          constantUtil.NONE,
          constantUtil.USER,
          constantUtil.PROFESSIONAL,
          constantUtil.ADMIN,
        ],
      },
      // "isUserRideCancellationPenalty": {
      //   "type": "boolean",
      //   "convert": true,
      // },
      // "isProfessionalRideCancellationPenalty": {
      //   "type": "boolean",
      //   "convert": true,
      // },
      "status": { "type": "string" },
      "languageKeys": {
        "type": "array",
        "props": {
          "key": { "type": "string", "convert": true },
          "values": { "type": "string", "convert": true },
        },
      },
    },
  },
};
adminValidation.updatePrivileges = {
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "privileges": { "type": "object", "optional": true },
    },
  },
};
adminValidation.updateHubs = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "email": { "type": "email" },
      "hubsName": { "type": "string" },
      "serviceArea": { "type": "string" },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "line1": { "type": "string" },
          "city": { "type": "string" },
          "state": { "type": "string" },
          "country": { "type": "string" },
          "zipcode": { "type": "string", "optional": true },
          "fulladdress": { "type": "string" },
        },
      },
      "phone": {
        "type": "object",
        "strict": true,
        "props": {
          "code": { "type": "string" },
          "number": { "type": "string" },
        },
      },
      "showAddress": { "type": "string" },
      "timeSettings": {
        "type": "object",
        "strict": true,
        "props": {
          "startTime": { "type": "string", "min": 1 },
          "endTime": { "type": "string", "min": 1 },
        },
      },
      "arrayData": {
        "type": "object",
        "strict": true,
        "props": {
          "holidayDates": { "type": "array", "items": { "type": "string" } },
          "employeeList": { "type": "array", "optional": true },
        },
      },
      "status": { "type": "string" },
      "password": { "type": "string", "optional": true },
      "commissionPercentage": { "type": "number", "convert": true },
      "amount": { "type": "number", "convert": true, "optional": true },
    },
  },
};
adminValidation.getHubEmployeeList = {
  "hubsId": { "type": "string", "min": 8 },
  "id": { "type": "string", "optional": true, "min": 8 },
  "status": { "type": "string", "optional": true },
  "filter": { "type": "string", "optional": true },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateHubsEmployee = {
  "hubsId": { "type": "string", "min": 8 },
  "id": { "type": "string", "optional": true, "min": 8 },
  "firstName": { "type": "string", "min": 2 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "avatar": { "type": "string", "optional": true },
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string" },
  "status": { "type": "string" },
  "password": { "type": "string", "optional": true },
};
adminValidation.getEditHubEmployees = {
  "hubsId": { "type": "string", "min": 8 },
  "id": { "type": "string", "min": 8 },
};
adminValidation.updateVerificationDocument = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "docName": { "type": "string", "min": 2 },
      "docDescription": { "type": "string", "min": 2 },
      "status": { "type": "string" },
    },
  },
};
adminValidation.updateUser = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "firstName": { "type": "string", "min": 2 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "dob": { "type": "string", "optional": true },
  "avatar": { "type": "string", "optional": true },
  "status": { "type": "string" },
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string" },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
};
adminValidation.updateProfessionalStep1 = {
  "professionalId": { "type": "string", "optional": true, "min": 8 },
  "firstName": { "type": "string", "min": 2 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string" },
  "nationalIdNo": { "type": "string", "optional": true },
  "status": { "type": "string", "default": constantUtil.UNVERIFIED },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "avatar": { "type": "string", "optional": true },
  "dob": {
    "type": "string",
    "pattern": "^\\d{2}-\\d{2}-\\d{4}|\\d{2}/\\d{2}/\\d{4}$",
  },
  "line1": { "type": "string", "min": 1 },
  "city": { "type": "string", "min": 1 },
  "state": { "type": "string", "min": 1 },
  "country": { "type": "string", "min": 1 },
  "zipcode": { "type": "string", "optional": true },
};
adminValidation.updateProfessionalStep2 = {
  "professionalId": { "type": "string", "min": 8 },
  "profileVerificationId": { "type": "string", "optional": true },
  "expiryDate": { "type": "string", "optional": true },
  "documentName": { "type": "string", "min": 1 },
  "files": { "type": "array", "optional": true, "convert": "true" },
};
adminValidation.updateProfessionalStep3 = {
  "apiFrom": { "type": "string", "optional": true, "min": "1" },
  "isAddOnPlan": {
    "type": "boolean",
    "convert": true,
    "optional": true,
  },
  "addOnPlanId": {
    "type": "string",
    "convert": true,
    "optional": true,
  },
  "id": { "type": "string", "optional": true, "min": 8 },
  "professionalId": { "type": "string", "min": 8 },
  "vehicleCategoryId": { "type": "string" },
  "vinNumber": { "type": "string", "optional": true },
  "defaultVehicle": { "type": "boolean", "optional": true, "convert": true },
  "plateNumber": { "type": "string" },
  // "type": {
  //   "type": "enum",
  //   "values": [
  //     constantUtil.PETROL,
  //     constantUtil.DIESEL,
  //     constantUtil.CNG,
  //     constantUtil.ELECTRIC,
  //     constantUtil.OTHER,
  //   ],
  // },
  "type": { "type": "string" },
  "maker": { "type": "string" },
  "model": { "type": "string" },
  "year": { "type": "string" },
  "color": { "type": "string" },
  "noOfDoors": { "type": "string" },
  "noOfSeats": { "type": "string" },
  "hub": { "type": "string" },
  "serviceCategory": { "type": "string" },
  "scheduleDate": { "type": "string" },
  "status": {
    "type": "enum",
    "values": [constantUtil.VERIFIED, constantUtil.UNVERIFIED],
  },
  "frontImage": { "type": "string", "optional": true },
  "backImage": { "type": "string", "optional": true },
  "leftImage": { "type": "string", "optional": true },
  "rightImage": { "type": "string", "optional": true },
  "handicapAvailable": { "type": "boolean", "convert": true },
  "childseatAvailable": { "type": "boolean", "convert": true },
  "isCompanyVehicle": { "type": "boolean", "convert": true },
  "isRentalSupported": { "type": "boolean", "convert": true, "optional": true },
  "isHasUSBChargePort": { "type": "boolean", "convert": true },
  "isHasWifi": { "type": "boolean", "convert": true },
  "isPetAllowed": { "type": "boolean", "convert": true },
  "isEnableLuggage": { "type": "boolean", "convert": true },
  "isHasAC": { "type": "boolean", "convert": true, "default": false },
};
adminValidation.updateProfessionalStep4 = {
  "id": { "type": "string", "min": 8, "optional": true },
  "vehicleId": { "type": "string", "min": 8 },
  "professionalId": { "type": "string", "min": 8 },
  "expiryDate": { "type": "string", "optional": true },
  "documentName": { "type": "string", "min": 1 },
  "image": { "type": "string", "optional": true },
  "files": { "type": "array", "optional": true, "convert": true },
  "action": {
    "type": "string",
    "optional": true,
    "convert": true,
    "lowercase": true,
  },
};
adminValidation.updateBankDetailsProfessional = {
  "id": { "type": "string", "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    // "props": {
    //   "accountName": { "type": "string", "min": 2 },
    //   "accountNumber": { "type": "string", "min": 6 },
    //   "bankName": { "type": "string", "min": 2 },
    //   "routingNumber": { "type": "string", "min": 4 },
    //   "branchCode": { "type": "string", "min": 2 },
    //   "country": { "type": "string", "min": 1 },
    //   "currency": { "type": "string", "min": 1 },
    // },
  },
};
adminValidation.changeVehicleStatus = {
  "professionalId": { "type": "string", "min": 8 },
  "ids": { "type": "array", "items": { "type": "string" } },
  "status": {
    "type": "enum",
    "values": [constantUtil.ACTIVE, constantUtil.INACTIVE],
  },
};
adminValidation.professionalProfileDocumentVerification = {
  "professionalId": { "type": "string", "min": 8 },
  "documentName": { "type": "string", "min": 2 },
  "expiryDate": { "type": "string", "optional": true },
  "reason": { "type": "string", "optional": true },
  "status": {
    "type": "enum",
    "values": [
      constantUtil.VERIFIED,
      constantUtil.UNVERIFIED,
      constantUtil.REJECTED,
    ],
  },
};
adminValidation.professionalVehicleDocumentVerification = {
  "professionalId": { "type": "string", "min": 8 },
  "id": { "type": "string", "min": 8 },
  "documentName": { "type": "string", "min": 2 },
  "expiryDate": { "type": "string", "optional": true },
  "reason": { "type": "string", "optional": true },
  "status": {
    "type": "enum",
    "values": [
      constantUtil.VERIFIED,
      constantUtil.UNVERIFIED,
      constantUtil.REJECTED,
    ],
  },
};
adminValidation.getVehicleList = {
  "professionalId": { "type": "string", "min": 8 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.editVehicleDetails = {
  "professionalId": { "type": "string", "min": 8 },
  "id": { "type": "string", "min": 8 },
};
adminValidation.updateLanguage = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "languageName": { "type": "string", "min": 2 },
      "languageCode": { "type": "string", "min": 2 },
      "languageDefault": { "type": "boolean", "convert": true },
      "languageDirection": {
        "type": "enum",
        "values": [constantUtil.RTL, constantUtil.LTR],
      },
      "status": { "type": "string", "default": constantUtil.ACTIVE },
      "languageKeys": { "type": "object" },
    },
  },
};
adminValidation.updateTranslateKeys = {
  "id": { "type": "string", "min": 8 },
  "languageKeys": { "type": "object", "strict": true },
};
adminValidation.updateSecurityServiceList = {
  "id": { "type": "string", "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "emergency": {
        "type": "array",
        "items": {
          "type": "object",
          "strict": true,
          "props": {
            "_id": { "type": "string" },
            "code": { "type": "string" },
            "name": { "type": "string" },
            "status": { "type": "boolean" },
          },
        },
      },
      "tracking": {
        "type": "array",
        "items": {
          "type": "object",
          "strict": true,
          "props": {
            "_id": { "type": "string" },
            "code": { "type": "string" },
            "name": { "type": "string" },
            "status": { "type": "boolean" },
          },
        },
      },
      "sos": {
        "type": "array",
        "items": {
          "type": "object",
          "strict": true,
          "props": {
            "_id": { "type": "string" },
            "code": { "type": "string" },
            "name": { "type": "string" },
            "status": { "type": "boolean" },
          },
        },
      },
    },
  },
};
// RESPONSE OFFICER VALIDATION
adminValidation.updateOfficer = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "firstName": { "type": "string", "min": 2 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string" },
  "status": { "type": "string", "default": constantUtil.ACTIVE },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtil.MALE, constantUtil.FEMALE, constantUtil.OTHER],
  // },
  "gender": { "type": "string", "min": 2 },
  "avatar": { "type": "string", "optional": true },
  "dob": {
    "type": "string",
    "pattern": "^\\d{2}-\\d{2}-\\d{4}|\\d{2}/\\d{2}/\\d{4}$",
  },
};
// SUBSCRIPTION VALIDATION
adminValidation.updateSubscription = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "name": { "type": "string", "min": 2, "max": 16 },
  "hint": { "type": "string", "min": 1 },
  "description": { "type": "string", "min": 1 },
  "expireInText": { "type": "string", "min": 1 },
  "currencyCode": { "type": "string", "min": 1 },
  "currencySymbol": { "type": "string", "min": 1 },
  "price": { "type": "number", "convert": true },
  "expireDays": { "type": "number", "convert": true },
  "markAsPopular": { "type": "boolean", "convert": true },
  "status": { "type": "string", "default": constantUtil.ACTIVE },
  "services": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "_id": { "type": "string" },
        "code": { "type": "string" },
        "name": { "type": "string" },
        "status": { "type": "boolean" },
        "isCommonService": { "type": "boolean" },
        "callNumber": { "type": "string", "optional": true },
      },
    },
  },
};
adminValidation.getOperatorData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtil.LIFETIME, constantUtil.FILTER],
  },
};
adminValidation.getHubsData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtil.LIFETIME, constantUtil.FILTER],
  },
};
adminValidation.getHubsBasedCityData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtil.LIFETIME, constantUtil.FILTER],
  },
  "city": { "type": "string", "optional": true },
};

adminValidation.getLocationProfessionalList = {
  "city": { "type": "string", "min": 4 },
};
adminValidation.getAllRidesList = {
  "time": { "type": "number", "convert": true },
  "cityId": { "type": "string", "optional": true },
};
adminValidation.getProfessionalsAndServiceArea = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "bookingType": {
    "type": "enum",
    "values": [constantUtil.INSTANT, constantUtil.SCHEDULE],
  },
};
adminValidation.getCityBasedVehicleCategory = {
  "city": { "type": "string", "min": 1 },
};
adminValidation.getWalletAmountList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "userType": { "type": "string", "min": 1 },
  "search": { "type": "string", "optional": true },
  "filter": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.addReports = {
  "userType": { "type": "string", "min": 1 },
  "comment": { "type": "string", "optional": true },
};
adminValidation.getSecurityList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "userTypefilter": { "type": "string" },
  "priorityFilter": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.getSecurityClosedList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "userTypefilter": { "type": "string" },
  "priorityFilter": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.getSecurityOperatorList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "userTypefilter": { "type": "string" },
  "priorityFilter": { "type": "string" },
  "statusfilter": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateInviteAndEarn = {
  "id": { "type": "string", "min": 8 },
  "forUser": {
    "type": "object",
    "strict": true,
    "props": {
      "showText": { "type": "string" },
      "amountToInviter": { "type": "number" },
      "amountToJoiner": { "type": "number" },
      "image": { "type": "string" },
      "description": { "type": "string" },
      "messageText": { "type": "string" },
      "rideCount": { "type": "number" },
      "isEnableReferralRideEarning": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "referralRideEarningPercentage": {
        "type": "number",
        "convert": true,
        "default": 0,
      },
      "languageKeys": { "type": "object", "optional": true },
    },
  },
  "forProfessional": {
    "type": "object",
    "strict": true,
    "props": {
      "showText": { "type": "string" },
      "amountToInviter": { "type": "number" },
      "amountToJoiner": { "type": "number" },
      "image": { "type": "string" },
      "description": { "type": "string" },
      "messageText": { "type": "string" },
      "rideCount": { "type": "number" },
      "isEnableReferralRideEarning": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "referralRideEarningPercentage": {
        "type": "number",
        "convert": true,
        "default": 0,
      },
      "languageKeys": { "type": "object", "optional": true },
    },
  },
};
adminValidation.updateCoorperateOffice = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "email": { "type": "email" },
      "isEmailVerified": {
        "type": "boolean",
        "convert": true,
        "default": false,
      },
      "officeName": { "type": "string" },
      "officeId": { "type": "string" },
      "serviceArea": { "type": "string" },
      "serviceAreaId": { "type": "string" },
      "categoryIds": { "type": "array", "items": { "type": "string" } },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "line1": { "type": "string" },
          "city": { "type": "string" },
          "state": { "type": "string" },
          "country": { "type": "string" },
          "zipcode": { "type": "string", "optional": true },
          "fulladdress": { "type": "string" },
        },
      },
      "phone": {
        "type": "object",
        "strict": true,
        "props": {
          "code": { "type": "string" },
          "number": { "type": "string" },
        },
      },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "showAddress": { "type": "string" },
      "contactPersonName": { "type": "string" },
      "financePersonName": { "type": "string" },
      "status": { "type": "string" },
      "password": { "type": "string", "optional": true },
      "isEnableFixedChangeRide": { "type": "boolean" },
      "amount": { "type": "number", "convert": true },
      "freezedAmount": { "type": "number", "convert": true },
      "guidelines": { "type": "string", "optional": true, "covert": true },
    },
  },
};
adminValidation.getCoorperateOfficeBillingList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "id": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.changeCoorperatePaidStatus = {
  "billingId": { "type": "string" },
  "coorperateId": { "type": "string" },
  "comments": { "type": "string" },
  "status": { "type": "boolean" },
};
adminValidation.updateEmailTemplate = {
  "id": { "type": "string", "optional": true },
  "emailTemplateEditableFields": {
    "type": "array",
    "items": { "type": "string" },
  },
  "name": { "type": "string" },
  "emailSubject": { "type": "string" },
  "senderName": { "type": "string" },
  "senderEmail": { "type": "string" },
  "emailContent": { "type": "string" },
  "status": { "type": "string" },
};
adminValidation.updatePopularPlace = {
  "id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true },
      "serviceArea": { "type": "string" },
      "placeName": { "type": "string" },
      "placeDescription": { "type": "string" },
      "placeImage": { "type": "string" },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "placeId": { "type": "string" },
          "addressType": { "type": "string" },
          "addressName": { "type": "string" },
          "fullAddress": { "type": "string" },
          "shortAddress": { "type": "string" },
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "locationCoordinates": {
        "type": "object",
        "optional": true,
        "props": {
          "type": { "type": "string" },
          "coordinates": {
            "type": "array",
            "items": {
              "type": "array",
              "items": { "type": "array", "items": "number" },
            },
          },
        },
      },
      "status": { "type": "string" },
    },
  },
};
adminValidation.getPopularPlaces = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};
adminValidation.updateAirport = {
  "id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true },
      "serviceArea": { "type": "string" },
      "airportShortName": { "type": "string" },
      "airportLongName": { "type": "string" },
      "airportDescription": { "type": "string" },
      "airportImage": { "type": "string" },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "placeId": { "type": "string" },
          "addressType": { "type": "string" },
          "addressName": { "type": "string" },
          "fullAddress": { "type": "string" },
          "shortAddress": { "type": "string" },
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "locationCoordinates": {
        "type": "object",
        "props": {
          "type": { "type": "string" },
          "coordinates": {
            "type": "array",
            "items": {
              "type": "array",
              "items": { "type": "array", "items": "number" },
            },
          },
        },
      },
      "stops": { "type": "array" },
      "status": { "type": "string" },
      "tollCategorys": {
        "type": "array",
        "items": {
          "type": "object",
          "strict": true,
          "props": {
            "categoryId": { "type": "string" },
            "amount": { "type": "number", "convert": true },
          },
        },
      },
    },
  },
};
adminValidation.getAirportStopList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateAirportStops = {
  "airportId": { "type": "string" },
  "id": { "type": "string", "optional": true },
  "name": { "type": "string" },
  "location": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
  "status": {
    "type": "enum",
    "values": [constantUtil.ACTIVE, constantUtil.INACTIVE],
  },
};
adminValidation.getEditAirportStop = {
  "airportId": { "type": "string" },
  "id": { "type": "string" },
};
adminValidation.changeAirportStopStatus = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "status": {
    "type": "enum",
    "values": [constantUtil.ACTIVE, constantUtil.INACTIVE],
  },
};
adminValidation.getAirports = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};
adminValidation.updateAirportTransferConfig = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "clientId": { "type": "string", "optional": true, "min": 8 },
  // "data": {
  //   "type": "object",
  //   "strict": true,
  //   "props": {
  "isEnableBookingPassengerConfig": { "type": "boolean", "convert": true },
  "isEnableBookingLuggageConfig": { "type": "boolean", "convert": true },
  "bookingPassengerConfig": {
    "type": "array",
    "strict": true,
    "props": {
      "categoryId": { "type": "string", "convert": true },
      "categoryName": { "type": "string", "convert": true },
      "count": { "type": "number", "convert": true },
    },
  },
  "bookingLuggageConfig": {
    "type": "array",
    "strict": true,
    "props": {
      "categoryId": { "type": "string", "convert": true },
      "categoryName": { "type": "string", "convert": true },
      "count": { "type": "number", "convert": true },
    },
  },
  "isEnableFlightNumber": { "type": "boolean", "convert": true },
  "isEnableWelcomeSignboard": { "type": "boolean", "convert": true },
  "isEnableReturnTransfer": { "type": "boolean", "convert": true },
  "isEnableReturningPassengerConfig": { "type": "boolean", "convert": true },
  "isEnableReturningLuggageConfig": { "type": "boolean", "convert": true },
  "returningPassengerConfig": {
    "type": "array",
    "strict": true,
    "props": {
      "categoryId": { "type": "string", "convert": true },
      "categoryName": { "type": "string", "convert": true },
      "count": { "type": "number", "convert": true },
    },
  },
  "returningLuggageConfig": {
    "type": "array",
    "strict": true,
    "props": {
      "categoryId": { "type": "string", "convert": true },
      "categoryName": { "type": "string", "convert": true },
      "count": { "type": "number", "convert": true },
    },
    //   },
    // },
  },
};
adminValidation.getListWithCityFilter = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "city": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateToll = {
  "id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true },
      "serviceArea": { "type": "string" },
      "tollName": { "type": "string" },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "placeId": { "type": "string" },
          "addressType": { "type": "string" },
          "addressName": { "type": "string" },
          "fullAddress": { "type": "string" },
          "shortAddress": { "type": "string" },
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "locationCoordinates": {
        "type": "object",
        "props": {
          "type": { "type": "string" },
          "coordinates": {
            "type": "array",
            "items": {
              "type": "array",
              "items": { "type": "array", "items": "number" },
            },
          },
        },
      },
      "tollCategorys": {
        "type": "array",
        "items": {
          "type": "object",
          "strict": true,
          "props": {
            "categoryId": { "type": "string" },
            "tollAmount": { "type": "number", "convert": true },
          },
        },
      },
      "status": { "type": "string" },
    },
  },
};
adminValidation.updateCoupon = {
  "id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true },
      "serviceArea": { "type": "string" },
      "serviceAreaId": { "type": "string" },
      "categoryType": {
        "type": "enum",
        "values": [
          constantUtil.DAILYTRIP,
          constantUtil.OUTSTATION,
          constantUtil.RENTAL,
          constantUtil.CARPOOL,
          constantUtil.AMBULANCE,
          constantUtil.PACKAGEDELIVERY,
        ],
      },
      "categoryIds": { "type": "array", "items": { "type": "string" } },
      "couponTitle": { "type": "string" },
      "couponDescription": { "type": "string" },
      "couponCode": { "type": "string" },
      "couponType": {
        "type": "enum",
        "values": [constantUtil.FLAT, constantUtil.PERCENTAGE],
      },
      "couponAmount": { "type": "number" },
      "couponMaxAmount": { "type": "number", "convert": true },
      "totalUsageCount": { "type": "number" },
      "usageCountPerPerson": { "type": "number" },
      "vaildFrom": { "type": "string", "optional": true },
      "vaildTo": { "type": "string", "optional": true },
      "consumersType": {
        "type": "enum",
        "values": [
          constantUtil.FIRSTUSER,
          constantUtil.EVERYONE,
          constantUtil.PROMOTION,
        ],
      },
      "isAutomatic": { "type": "boolean", "convert": true },
      "isProfessionalTolerance": { "type": "boolean", "convert": true },
      "isDisplayInApp": { "type": "boolean", "convert": true },
      "couponCodeMessage": { "type": "string", "optional": true },
      "status": { "type": "string" },
      "couponGuideLines": {
        "type": "array",
        "items": {
          "type": "object",
          "props": {
            "title": { "type": "string" },
            "points": {
              "type": "array",
              "items": { "type": "string" },
            },
          },
        },
        "optional": true,
      },
      "couponPaymentType": {
        "type": "array",
        "items": {
          "type": "enum",
          "values": [constantUtil.WALLET, constantUtil.CASH, constantUtil.CARD],
        },
      },
      "isCouponCheckWithMaxTripAmount": { "type": "boolean", "convert": true },
    },
  },
};
adminValidation.getUserBasedCouponList = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "userId": { "type": "string", "optional": true },
  "serviceAreaId": { "type": "string", "optional": true },
};
adminValidation.checkAndApplyManualCoupon = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "userId": { "type": "string", "optional": true },
  "serviceAreaId": { "type": "string", "optional": true },
  "couponCode": { "type": "string", "convert": true, "trim": true },
};
adminValidation.checkUniqueReferralCode = {
  "userType": { "type": "string" },
  "userId": { "type": "string", "optional": true },
  "professionalId": { "type": "string", "optional": true },
  "uniqueCode": { "type": "string", "convert": true, "trim": true },
};
adminValidation.getNotificationTemplatesList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
  "type": {
    "type": "enum",
    "values": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
  },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
};
adminValidation.updateNotificationTemplates = {
  "id": { "type": "string", "optional": true },
  "type": {
    "type": "enum",
    "values": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
  },
  "title": { "type": "string" },
  "content": { "type": "string" },
  "image": { "type": "string", "optional": true },
  "subject": { "type": "string", "optional": true },
  "contentEditableFilelds": {
    "type": "array",
    "items": { "type": "string" },
    "optional": true,
  },
  "status": { "type": "string" },
};
adminValidation.getNotificationTemplates = {
  "id": { "type": "string", "min": 4 },
  "type": {
    "type": "enum",
    "values": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
  },
};
adminValidation.changeNotificationTemplateStatus = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "type": {
    "type": "enum",
    "values": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
  },
  "status": { "type": "string" },
};
adminValidation.getUserListForNotification = {
  "category": { "type": "string", "min": 1, "trim": true },
  "filter": { "type": "string" },
  "skip": { "type": "number" },
  "limit": { "type": "number" },
  "search": { "type": "string" },
  "userType": {
    "type": "enum",
    "values": [
      constantUtil.PROFESSIONAL.toLocaleLowerCase(),
      constantUtil.USER.toLocaleLowerCase(),
    ],
  },
};
adminValidation.sendPromotion = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "type": {
    "type": "enum",
    "values": [constantUtil.SMS, constantUtil.EMAIL, constantUtil.PUSH],
  },
  "userType": {
    "type": "enum",
    "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
  },
  "phone": {
    "type": "object",
    // "strict": true,
    "optional": true,
    "props": {
      "code": { "type": "string" },
      "number": { "type": "string" },
    },
  },
  "template": {
    "type": "object",
    "strict": true,
    "props": {
      "title": { "type": "string", "optional": true },
      "content": { "type": "string", "optional": true },
      "image": { "type": "string", "optional": true },
      "subject": { "type": "string", "optional": true },
    },
  },
};
adminValidation.updatePopularDestination = {
  "id": { "type": "string", "optional": true },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true },
      "placeName": { "type": "string" },
      "placeDescription": { "type": "string" },
      "placeImage": { "type": "string" },
      "locationAddress": {
        "type": "object",
        "strict": true,
        "props": {
          "placeId": { "type": "string" },
          "addressType": { "type": "string" },
          "addressName": { "type": "string" },
          "fullAddress": { "type": "string" },
          "shortAddress": { "type": "string" },
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "location": {
        "type": "object",
        "strict": true,
        "props": {
          "lat": { "type": "number", "convert": true },
          "lng": { "type": "number", "convert": true },
        },
      },
      "status": { "type": "string" },
    },
  },
};
// events validations
adminValidation.sendMail = {
  "from": { "type": "string" },
  "to": { "type": "string" },
  "subject": { "type": "string" },
  "html": { "type": "string" },
};
// promotion email tempaltes
adminValidation.getUpdateEmailEditableFields = {
  "id": { "type": "string", "min": 1, "optional": true },
  "emailTemplateEditableFields": { "type": "array", "optional": true },
};

// WALLET refund

adminValidation.getRefundDetails = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string", "optional": true },
  "filter": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "userType": {
    "type": "enum",
    "values": ["", "user", "professional"],
  },
};
adminValidation.refundAction = {
  "action": { "type": "enum", "values": ["verify", "retry"] },
  "transactionId": { "type": "string", "min": 1, "convert": true },
};

// payout
adminValidation.getWithDrawDetails = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "search": { "type": "string", "optional": true },
  "filter": { "type": "string", "optional": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "userType": {
    "type": "enum",
    "values": ["", "user", "professional"],
  },
};

adminValidation.walletWithDrawManualTransfer = {
  "transactionId": { "type": "string", "min": 2, "convert": true },
  "action": {
    "type": "enum",
    "values": ["transfer", "transferretry", "verify"],
  },
};
adminValidation.adminGetTransaction = {
  "transactionId": { "type": "string" },
};
adminValidation.changeTransactionStatus = {
  "ids": {
    "type": "array",
    "items": {
      "type": "string",
      "min": 12,
    },
  },
  "status": {
    "type": "enum",
    "values": [
      constantUtil.SUCCESS,
      constantUtil.PENDING,
      constantUtil.PROCESSING,
      constantUtil.FAILED,
    ],
  },
};

// THIRD MAN BOOKING SERVICE
adminValidation.getServiceCategoryForThirdPartyBooking = {
  "origin": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
  "destination": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
};

adminValidation.newRideBookingForThirdParty = {
  "bookingFor": {
    "type": "object",
    "strict": true,
    "props": {
      "name": { "type": "string", "min": 1 },
      "phoneCode": { "type": "string", "min": 1, "convert": true },
      "phoneNumber": { "type": "string", "min": 1, "convert": true },
    },
  },
  "serviceCategoryId": { "type": "string", "min": 12, "convert": true },
  "vehicleCategoryId": { "type": "string", "min": 12, "convert": true },
  "origin": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
  "destination": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
};

adminValidation.createBookingServiceManagement = {
  "name": { "type": "string", "convert": true },
  "email": { "type": "email", "trim": true },
  "phone": {
    "type": "object",
    "props": {
      "code": { "type": "string" },
      "number": { "type": "string" },
    },
  },
  "password": { "type": "string", "trim": true, "convert": true },
  "contactPersonName": { "type": "string", "convert": true },
  "financePersonName": { "type": "string", "convert": true },
};
adminValidation.editBookingServiceManagement = {
  "id": { "type": "string", "trim": true, "min": 12 },
  ...adminValidation.createBookingServiceManagement,
};
adminValidation.createClient = {
  "key": {
    "type": "string",
    "trim": true,
    "min": 2,
    "convert": true,
    "lowercase": true,
  },
  "name": { "type": "string", "convert": true, "trim": true, "min": 2 },
  "status": { "type": "boolean", "convert": true },
  "email": { "type": "email" },
  "plan": { "type": "string", "min": 2 },
  "billingDate": { "type": "date", "convert": true, "trim": true },
  "expiryDays": { "type": "number", "convert": true, "trim": true },
  "notifyMessage": {
    "type": "string",
    "convert": true,
    "trim": true,
    "min": 2,
  },
};
adminValidation.editClient = {
  "id": { "type": "string", "trim": true, "min": 12 },
  ...adminValidation.createClient,
};

adminValidation.createAndEditPlans = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "role": { "type": "string" },
      "name": { "type": "string" },
      "status": { "type": "string" },
      "privileges": { "type": "object", "optional": true },
      "forceUpdate": { "type": "boolean", "convert": true },
    },
  },
};
adminValidation.getPlanById = {
  "id": { "type": "string", "min": 12 },
};
adminValidation.getCategoryList = {
  "userType": {
    "type": "enum",
    "values": [
      constantUtil.USER.toLocaleLowerCase(),
      constantUtil.PROFESSIONAL.toLocaleLowerCase(),
    ],
  },
};

// PROMOTION CAMPAIGN
adminValidation.createPromotionCampaign = {
  "type": {
    "type": "enum",
    "values": [constantUtil.EMAIL, constantUtil.PUSH, constantUtil.SMS],
  },
  "userType": {
    "type": "enum",
    "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
  },
  "category": {
    "type": "enum",
    "values": [constantUtil.CITY, constantUtil.ALPHABETICALORDER],
  },
  "filter": { "type": "string", "min": 1, "convert": true, "optional": true },
  "template": { "type": "objectID", "convert": true },
};

// loyalty coupon

adminValidation.managePartnerDealsReward = {
  // for url and api for queies  checkout admin.routes.js
  "requestType": { "type": "enum", "values": ["add", "update", "edit"] },
  "id": { "type": "string", "convert": true, "optional": true },
  "isActive": { "type": "boolean", "convert": true },
  "serviceArea": { "type": "string", "convert": true },
  "title": { "type": "string", "min": 1, "convert": true },
  "description": { "type": "string", "min": 1, "convert": true },

  "totalUsageCount": { "type": "number", "convert": true },
  "minimumEligibleAmount": { "type": "number", "convert": true },
  "couponCode": { "type": "string", "convert": true },
  "userType": {
    "type": "enum",
    "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
  },
  "validFrom": { "type": "date", "convert": true },
  "validTo": { "type": "date", "convert": true },
  "expiryDate": { "type": "date", "convert": true },
  "termsAndCondition": { "type": "string", "convert": true },
  "sponsorName": { "type": "string", "convert": true },
  "sponsorLink": { "type": "string", "convert": true },
  "guidelines": { "type": "string", "covert": true },
  "imageUrl": { "type": "string", "min": 1, "optional": true },
  // bel
  "files": { "type": "array", "optional": true, "max": 1 },
};

const sameValidationOfPointsReward = {
  "rewardDocId": { "type": "string", "optional": true },
  "isActive": { "type": "boolean", "convert": true },
  "title": { "type": "string", "convert": true },
  "description": { "type": "string", "convert": true },
  "pointsType": {
    "type": "enum",
    "values": [constantUtil.PERCENTAGE, constantUtil.FLAT],
  },
  "rewardPoints": { "type": "number", "convert": true },
  "userType": { "type": "string", "default": constantUtil.PROFESSIONAL },
  "redeemLimitMin": { "type": "number", "convert": true },
  "redeemLimitMax": { "type": "number", "convert": true },
  "redeemPointValue": { "type": "number", "convert": true },
  "termsAndCondition": { "type": "string", "convert": true },
  "guidelines": {
    "type": "array",
    "values": {
      "type": "object",
      "props": {
        "title": "string",
        "points": {
          "type": "array",
          "values": {
            "type": "string",
          },
        },
      },
    },
  },
};
adminValidation.managePointsRewards = {
  "requestType": { "type": "enum", "values": ["add", "update", "edit"] },
  "serviceArea": { "type": "string", "convert": true },
  "professional": { "type": "object", "props": sameValidationOfPointsReward },
  "user": { "type": "object", "props": sameValidationOfPointsReward },
};
adminValidation.updateMembershipPlan = {
  "id": { "type": "string", "optional": true, "min": 8 },
  "data": {
    "type": "object",
    "strict": true,
    "props": {
      "clientId": { "type": "string", "optional": true, "min": 8 },
      "name": { "type": "string", "min": 2 },
      "userType": {
        "type": "enum",
        "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
      },
      "planType": {
        "type": "enum",
        "values": [
          constantUtil.WEEKLY,
          constantUtil.MONTHLY,
          constantUtil.YEARLY,
        ],
      },
      "validityDays": { "type": "number", "convert": true },
      "totalNoOfRides": { "type": "number", "convert": true },
      "isDefault": { "type": "boolean", "convert": true },
      "status": { "type": "string", "default": constantUtil.ACTIVE },
    },
  },
};

adminValidation.getMembershipListByUserType = {
  "userType": { "type": "enum", "values": ["user", "professional"] },
  "limit": { "type": "number", "convert": true },
  "skip": { "type": "number", "convert": true },
  "filter": {
    "type": "enum",
    "values": [constantUtil.ALL, constantUtil.EXPIRED],
  },
  "search": { "type": "string", "convert": true, "default": "" },
};
adminValidation.getPartnerDealsRewardsList = {
  "rewardType": { "type": "string", "default": constantUtil.PARTNERDEALS },
  "userType": { "type": "enum", "values": ["user", "professional"] },
  "limit": { "type": "number", "convert": true, "default": 0 },
  "skip": { "type": "number", "convert": true, "default": 0 },
  "filter": {
    "type": "enum",
    "values": [constantUtil.ALL, constantUtil.EXPIRED],
  },
  "search": { "type": "string", "convert": true, "default": "" },
  "fromDate": { "type": "string", "convert": true, "default": "" },
  "toDate": { "type": "string", "covert": true, "default": "" },
};

adminValidation.getPointsRewardsList = {
  "rewardType": { "type": "string", "default": constantUtil.POINTS },
};

adminValidation.getRewardPointOfCity = {
  "serviceAreaId": { "type": "string", "convert": true },
};
adminValidation.getAllRideDetailsExternal = {
  "fromDate": {
    "type": "date",
    "convert": true,
    "custom": (value, errors, schema, name, parent, context) => {
      if (new Date(context.data.fromDate).getTime() > new Date().getTime()) {
        errors.push({ "type": "fromDate_greater_than_now" });
      }
      return value;
    },
  },
  "toDate": {
    "type": "date",
    "convert": true,
    "custom": (value, errors, schema, name, parent, context) => {
      if (
        new Date(context.data.fromDate).getTime() >
        new Date(context.data.toDate).getTime()
      ) {
        errors.push({ "type": "toDate_greater_than_fromDate" });
      }
      // if (new Date(context.data.toDate).getTime() > new Date().getTime()) {
      //   errors.push({ "type": "toDate_greater_than_now" });
      // }
      return value;
    },
  },
};
adminValidation.getCoorperateRidesList = {
  "fromDate": { "type": "string", "optional": true, "default": "" },
  "toDate": { "type": "string", "optional": true, "default": "" },

  "search": { "type": "string", "optional": true, "default": "" },
  "limit": {
    "type": "number",
    "convert": true,
    "optional": true,
    "default": 100000000,
  },
  "skip": { "type": "number", "convert": true, "optional": true, "default": 0 },
  "filter": {
    "type": "enum",
    "values": [
      constantUtil.AWAITING,
      constantUtil.ARRIVED,
      constantUtil.ACCEPTED,
      constantUtil.USERDENY,
      constantUtil.USERCANCELLED,
      constantUtil.CANCELLED,
      constantUtil.PROFESSIONALCANCELLED,
      constantUtil.EXPIRED,
      constantUtil.STARTED,
      constantUtil.ENDED,
    ],
    "optional": true,
  },
  "bookingByFilter": {
    "type": "enum",
    "values": [
      constantUtil.USER,
      constantUtil.PROFESSIONAL,
      constantUtil.COORPERATEOFFICE,
      "",
    ],
    "optional": true,
    "default": "",
  },
};
//#region Incentive Setup
adminValidation.getIncentiveList = {
  "rewardType": { "type": "string", "default": constantUtil.PARTNERDEALS },
  // "userType": { "type": "enum", "values": ["user", "professional"] },
  "limit": { "type": "number", "convert": true, "default": 0 },
  "skip": { "type": "number", "convert": true, "default": 0 },
  // "filter": {
  //   "type": "enum",
  //   "values": [constantUtil.ALL, constantUtil.EXPIRED],
  // },
  "search": { "type": "string", "convert": true, "default": "" },
  "fromDate": { "type": "string", "convert": true, "default": "" },
  "toDate": { "type": "string", "covert": true, "default": "" },
};

adminValidation.manageIncentiveSetup = {
  "serviceAreaId": { "type": "string", "convert": true, "min": 1 },
  "name": { "type": "string" },
  "title": { "type": "string" },
  "description": { "type": "string" },
  "userType": {
    "type": "enum",
    "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
  },
  "serviceType": { "type": "string" },
  "campaignType": { "type": "string" },
  "totalNoOfRides": { "type": "number", "convert": true },
  "minimumReviewRating": { "type": "number", "convert": true },
  "rideAcceptanceRate": { "type": "number", "convert": true },
  "days": { "type": "string", "convert": true },
  // "days": {
  //   "type": "array",
  //   "items": { "type": "string" },
  // },
  "onlineTime": { "type": "number", "convert": true, "default": 0 },
  "minimumEligibleAmount": { "type": "number", "convert": true, "default": 0 },
  "validFrom": { "type": "date", "convert": true },
  "validTo": { "type": "date", "convert": true },
  "startTime": { "type": "date", "convert": true },
  "startTime1": { "type": "date", "convert": true },
  "startTime2": { "type": "date", "convert": true },
  "endTime": { "type": "date", "convert": true },
  "endTime1": { "type": "date", "convert": true },
  "endTime2": { "type": "date", "convert": true },
  "amount": { "type": "number", "convert": true },
  "incentiveType": { "type": "string", "convert": true },
};
//#endregion Incentive Setup
//------------------------------------

module.exports = adminValidation;
