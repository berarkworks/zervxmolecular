const constantUtil = require("../utils/constant.util");

const transactionValidation = {};

transactionValidation.newRechargeWalletTransaction = {
  // 'card': {
  //   'type': 'object',
  //   'strict': true,
  //   'props': {
  //     'type': { 'type': 'string' },
  //     'stripeCardToken': { 'type': 'string', 'optional': true },
  //     'cardNumber': { 'type': 'string' },
  //     'expiryYear': { 'type': 'string' },
  //     'expiryMonth': { 'type': 'string' },
  //     'holderName': { 'type': 'string' },
  //     'cvv': { 'type': 'string' },
  //   },
  // },
  // 'address': {
  //   'type': 'object',
  //   'strict': true,
  //   'optional': true,
  //   'props': {
  //     'line1': { 'type': 'string' },
  //     'line2': { 'type': 'string' },
  //     'city': { 'type': 'string' },
  //     'zipCode': { 'type': 'string' },
  //     'state': { 'type': 'string' },
  //     'country': { 'type': 'string' },
  //   },
  // },
  // 'description': { 'type': 'string', 'optional': true },
  // 'currencyCode': { 'type': 'string', 'optional': true },
  // 'amount': { 'type': 'number', 'min': 1, 'convert': true },
  // 'reason': { 'type': 'string', 'optional': true },
  "userType": { "type": "string", "min": 1 },
  "cardId": { "type": "string", "min": 1 },
  "amount": { "type": "number", "min": 1, "convert": true },
  "cvv": { "type": "string", "optional": true },
  "reason": { "type": "string", "optional": true, "convert": true },
};

transactionValidation.newWithdrawalWalletTransaction = {
  // 'account': {
  //   'type': 'object',
  //   'props': {
  //     'accountName': { 'type': 'string', 'min': 2 },
  //     'accountNumber': { 'type': 'string', 'min': 6 },
  //     'bankName': { 'type': 'string', 'min': 2 },
  //     'branchCode': { 'type': 'string', 'min': 2 },
  //     'routingNumber': { 'type': 'string', 'min': 4 },
  //     'country': { 'type': 'string', 'min': 1 },
  //     'currency': { 'type': 'string', 'min': 1 },
  //   },
  // },
  // 'address': {
  //   'type': 'object',
  //   'strict': true,
  //   'optional': true,
  //   'props': {
  //     'line1': { 'type': 'string' },
  //     'line2': { 'type': 'string' },
  //     'city': { 'type': 'string' },
  //     'zipCode': { 'type': 'string' },
  //     'state': { 'type': 'string' },
  //     'country': { 'type': 'string' },
  //   },
  // },
  // 'description': { 'type': 'string', 'optional': true },

  // 'reason': { 'type': 'string', 'optional': true },
  // 'userType': { 'type': 'string', 'min': 1 },
  //
  "amount": { "type": "string", "min": 1, "convert": true, "trim": true },
  "userType": {
    "type": "string",
    "min": 1,
    "convert": true,
    "trim": true,
    "uppercase": true,
  },
  "bankId": { "type": "string", "optional": true }, //its used only for user
  "reason": { "type": "string", "convert": true, "optional": true },
};
transactionValidation.sendMoneyToFriends = {
  "amount": { "type": "number", "convert": true },
  "reason": { "type": "string", "optional": true },
  // "phoneNumber": { "type": "string", "min": 1 },
  // "phoneCode": { "type": "string", "min": 1 },
  "userType": { "type": "string", "min": 1 },
  "userId": { "type": "string", "optional": true },
  "professionalId": { "type": "string", "optional": true },
  "receiverType": { "type": "string" },
  "receiverPhoneCode": { "type": "string" },
  "receiverPhoneNumber": { "type": "string" },
};
transactionValidation.getTransactionDetailsById = {
  "id": { "type": "string", "min": 1 },
};
transactionValidation.getTransactionDetails = {
  "userType": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
transactionValidation.getWalletTransactionList = {
  "userType": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "filter": { "type": "string", "optional": true },
  "reimbursefilter": { "type": "boolean" },
  "search": { "type": "string", "optional": true },
};
transactionValidation.getParticularTransactionList = {
  "userType": { "type": "string", "min": 1 },
  "id": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
transactionValidation.getTransactionHistoryByType = {
  "userType": { "type": "string", "min": 1 },
  "id": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

// CARD REALATED
transactionValidation.verifyCard = {
  "otp": { "type": "number" },
  "transactionId": {
    "type": "string",
    "min": 1,
    "convert": true,
    "trim": true,
  },
};
transactionValidation.checkCardVaild = {
  "cardNumber": { "type": "string", "min": 1, "convert": true },
  "expiryYear": { "type": "string", "min": 1, "convert": true },
  "expiryMonth": { "type": "string", "min": 1, "convert": true },
  "cvv": { "type": "string", "convert": true },
  "userId": { "type": "string", "convert": true, "min": 1 },
  "holderName": { "type": "string" },
  "userType": {
    "type": "enum",
    "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
  },
  // 'pin': { 'type': 'string', 'min': 1, 'convert': true },
};
transactionValidation.refund = {
  "transactionId": {
    "type": "string",
    "min": 1,
    "trim": true,
    "convert": true,
  },
};
transactionValidation.rechargeVerify = {
  "userType": { "type": "string", "uppercase": true, "trim": true },

  "cardId": { "type": "string", "min": 1, "convert": true, "trim": true },
  "pin": { "type": "number", "convert": true, "min": 1, "trim": true },
  "cvv": { "type": "number", "convert": true, "trim": true },
};
transactionValidation.initiateTransaction = {
  "userType": {
    "type": "string",
    "values": [constantUtil.USER, constantUtil.PROFESSIONAL],
    "uppercase": true,
    "min": 1,
  },
  "amount": { "type": "number", "convert": true },
  "transactionType": {
    "type": "enum",
    "values": [constantUtil.WALLETRECHARGE, constantUtil.BOOKINGCHARGE],
  },
};
transactionValidation.verifyBankAccount = {
  "bankCode": { "type": "string", "convert": true, "min": 2 },
  "accountNumber": { "type": "string", "convert": true, "min": 2 },
  "bankName": { "type": "string", "convert": true },
};

// GATEWAY BASED VALIDATION

transactionValidation.flutterWave = {
  "checkValidCard": {
    "cardNumber": { "type": "string", "min": 1, "convert": true },
    "expiryYear": { "type": "string", "min": 1, "convert": true },
    "expiryMonth": { "type": "string", "min": 1, "convert": true },
    "cvv": { "type": "string", "convert": true, "min": 3 },
    // 'pin': { 'type': 'string', 'min': 1, 'convert': true },
    "userId": { "type": "string", "convert": true, "min": 1 },
    "userType": {
      "type": "enum",
      "values": [constantUtil.PROFESSIONAL, constantUtil.USER],
    },
  },
};
// validation
transactionValidation.validateCharge = {
  "validateType": {
    "type": "enum",
    "values": ["OTP", "PIN", constantUtil.ADDRESS],
  },
  "address": {
    "type": "object",
    "optional": true,
    "props": {
      "billingzip": { "type": "string", "min": "1" },
      "billingcity": { "type": "string", "min": "1" },
      "billingaddress": { "type": "string", "min": "1" },
      "billingstate": { "type": "string", "min": "1" },
      "billingcountry": { "type": "string", "min": "1" },
    },
  },
  "pin": { "type": "string", "convert": true, "optional": true },
  "otp": { "type": "string", "convert": true, "optional": true },
  "transactionId": { "type": "string", "convert": true },
  "payload": { "type": "object", "optional": true },
};

module.exports = transactionValidation;
