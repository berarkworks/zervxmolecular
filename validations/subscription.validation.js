const subscriptionValidation = {};

subscriptionValidation.addSubscriptionPlan = {
  "subscriptionId": { "type": "string", "min": 1 },
};

module.exports = subscriptionValidation;
