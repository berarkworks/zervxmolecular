const constantUtil = require("../utils/constant.util");

const eventWatchValidation = {};

eventWatchValidation.createEvent = {
  "eventName": { "type": "string", "min": 1 },
  "description": { "type": "string", "min": 1 },
  "eventLocationAddressName": { "type": "string", "optional": true },
  "eventLocationFullAddress": { "type": "string", "optional": true },
  "eventLocationShortAddress": { "type": "string", "optional": true },
  "eventLocationLat": { "type": "number", "convert": true },
  "eventLocationLng": { "type": "number", "convert": true },
  "status": {
    "type": "enum",
    "values": [constantUtil.ACTIVE, constantUtil.INACTIVE],
  },
  "eventStatus": {
    "type": "enum",
    "values": [
      constantUtil.ACTIVE,
      constantUtil.INACTIVE,
      constantUtil.STARTED,
      constantUtil.ENDED,
    ],
  },
  "eventType": {
    "type": "enum",
    "values": [constantUtil.PUBLIC, constantUtil.PRIVATE],
  },
  "participants": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "number": { "type": "string" },
        "code": { "type": "string" },
        "status": {
          "type": "enum",
          "values": [
            constantUtil.NOTASSIGNED,
            constantUtil.NOTINTERESTED,
            constantUtil.INTERESTED,
            constantUtil.MAYBE,
            constantUtil.STARTED,
            constantUtil.REACHED,
          ],
        },
      },
    },
    "optional": true,
  },
};

eventWatchValidation.addPraticipants = {
  "eventId": { "type": "string", "min": 1 },
  "status": {
    "type": "enum",
    "values": [constantUtil.INTERESTED, constantUtil.MAYBE],
  },
};

eventWatchValidation.praticipantsStatusChange = {
  "eventId": { "type": "string", "min": 1 },
  "status": { "type": "string", "min": 1 },
};

eventWatchValidation.eventStatusChange = {
  "eventId": { "type": "string", "min": 1 },
  "status": { "type": "string", "min": 1 },
};

eventWatchValidation.editEvent = {
  "eventId": { "type": "string", "min": 1 },
  "status": { "type": "string", "min": 1 },
  "eventStatus": { "type": "string", "min": 1 },
  "eventName": { "type": "string", "min": 1 },
  "eventType": { "type": "string", "min": 1 },
  "description": { "type": "string", "min": 1 },
  "eventDate": { "type": "string", "min": 1 },
  "eventLocationAddressName": { "type": "string", "min": 1 },
  "eventLocationFullAddress": { "type": "string", "min": 1 },
  "eventLocationShortAddress": { "type": "string", "min": 1 },
  "eventLocationLat": { "type": "number", "min": 1 },
  "eventLocationLng": { "type": "number", "min": 1 },
};

eventWatchValidation.viewDetails = {
  "eventId": { "type": "string", "min": 1 },
};

eventWatchValidation.getYourEventList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

eventWatchValidation.getParticipatedEventList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

module.exports = eventWatchValidation;
