const officerValidations = {};

const constantUtill = require("../utils/constant.util");

//#region Response Officer

officerValidations.updateProfile = {
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtill.MALE, constantUtill.FEMALE, constantUtill.OTHER],
  // },
  "gender": { "type": "string" },
  "status": { "type": "string" },
  "avatar": { "type": "string", "min": 1, "optional": true },
  "referredBy": { "type": "string", "optional": true },
  "uniqueCode": { "type": "string", "optional": true },
  "dob": {
    "type": "string",
    "pattern": "^\\d{2}-\\d{2}-\\d{4}|\\d{2}/\\d{2}/\\d{4}$",
  },
};

officerValidations.updateLiveLocation = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "currentBearing": { "type": "number", "convert": true },
  "speed": { "type": "number", "convert": true },
  "verticalAccuracy": { "type": "number", "convert": true },
  "horizontalAccuracy": { "type": "number", "convert": true },
  "altitude": { "type": "number", "convert": true },
};

officerValidations.updateOnlineStatus = {
  "onlineStatus": { "type": "boolean", "convert": true },
};

officerValidations.getOfficerData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtill.LIFETIME, constantUtill.FILTER],
  },
};

officerValidations.getRatings = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

officerValidations.updatePermissionData = {
  "osType": {
    "type": "enum",
    "values": [constantUtill.IOS, constantUtill.ANDROID],
    "optional": true,
  },
  "brand": { "type": "string", "optional": true },
  "model": { "type": "string", "optional": true },
  "osVersion": { "type": "string", "optional": true },
  "screenSize": { "type": "string", "optional": true },
  "internetConnectivityMedium": {
    "type": "string",
    "optional": true,
  },
  "token": { "type": "string", "optional": true },
  "fcmId": { "type": "string", "optional": true },
  "deviceLocationStatus": { "type": "boolean", "optional": true },
  "appLocationStatus": { "type": "boolean", "optional": true },
  "baseUrl": { "type": "string", "optional": true },
  "appVersionInServer": { "type": "string", "optional": true },
  "appVersionInStore": { "type": "string", "optional": true },
  "deviceTime": { "type": "string", "optional": true },
  "timezoneInNumber": { "type": "string", "optional": true },
  "timezoneInformat": { "type": "string", "optional": true },
  "connectivityMediumName": { "type": "string", "optional": true },
  "currentLocation": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
    "optional": true,
  },
  "socketConnetivity": { "type": "boolean", "optional": true },
};

//#endregion Response Officer

//#region Corporate Officer

officerValidations.getCorporateOfficerList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

officerValidations.manageCorporateOfficer = {
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email" },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtill.MALE, constantUtill.FEMALE, constantUtill.OTHER],
  // },
  "gender": { "type": "string" },
  // "status": {
  //   "type": "string",
  // },
  "corporate": { "type": "string" },
};
officerValidations.checkCorporateOfficerByPhoneNumber = {
  "phoneCode": { "type": "string" },
  "phoneNumber": { "type": "string", "max": 15 },
  "corporateId": { "type": "string" },
};
officerValidations.changeCorporateOfficerData = {
  "ids": { "type": "array", "items": { "type": "string" } },
  "status": { "type": "string" },
  // "isActive": { "type": "boolean" },
  // "isDeleted": { "type": "boolean" },
};

//#endregion Corporate Officer

module.exports = officerValidations;
