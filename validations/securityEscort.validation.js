const constantUtil = require("../utils/constant.util");

const securityEscortValidation = {};

securityEscortValidation.getHelp = {
  "isUserSubscribe": { "type": "boolean", "convert": true },
  "bookingId": { "type": "string", "optional": true },
  "alertLocationAddressName": { "type": "string", "optional": true },
  "alertLocationFullAddress": { "type": "string", "optional": true },
  "alertLocationShortAddress": { "type": "string", "optional": true },
  "alertLocationLat": { "type": "number", "convert": true },
  "alertLocationLng": { "type": "number", "convert": true },
  "alertType": {
    "type": "enum",
    "values": [constantUtil.SOS, constantUtil.EMERGENCY, constantUtil.TRACKING],
  },
  "securityServiceName": { "type": "string", "min": 1 },
  "securityServiceId": { "type": "string", "min": 1 },
  "message": { "type": "string", "optional": true },
};
securityEscortValidation.sendPersonalWatch = {
  "isUserSubscribe": { "type": "boolean", "convert": true },
  "bookingId": { "type": "string", "optional": true },

  "alertLocationAddressName": { "type": "string", "optional": true },
  "alertLocationFullAddress": { "type": "string", "optional": true },
  "alertLocationShortAddress": { "type": "string", "optional": true },
  "alertLocationLat": { "type": "number", "convert": true },
  "alertLocationLng": { "type": "number", "convert": true },

  "fromLocationAddressName": { "type": "string", "optional": true },
  "fromLocationFullAddress": { "type": "string", "optional": true },
  "fromLocationShortAddress": { "type": "string", "optional": true },
  "fromLocationLat": { "type": "number", "convert": true },
  "fromLocationLng": { "type": "number", "convert": true },

  "toLocationAddressName": { "type": "string", "optional": true },
  "toLocationFullAddress": { "type": "string", "optional": true },
  "toLocationShortAddress": { "type": "string", "optional": true },
  "toLocationLat": { "type": "number", "convert": true },
  "toLocationLng": { "type": "number", "convert": true },

  "alertType": {
    "type": "enum",
    "values": [constantUtil.SOS, constantUtil.EMERGENCY, constantUtil.TRACKING],
  },
  "securityServiceName": { "type": "string", "min": 1 },
  "securityServiceId": { "type": "string", "min": 1 },
  "message": { "type": "string", "optional": true },
};

securityEscortValidation.acceptEscortBooking = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.escortStatusChange = {
  "escortBookingId": { "type": "string", "min": 1 },
  "escortStatus": { "type": "string", "min": 1 },
  "lat": { "type": "number" },
  "lng": { "type": "number" },
};

securityEscortValidation.acknowledgeEscortStatus = {
  "escortBookingId": { "type": "string", "min": 1 },
  "acknowledgeStatus": { "type": "string", "min": 1 },
  "escortStatus": { "type": "string", "min": 1 },
};

securityEscortValidation.viewSecurityRequest = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.viewClosedSecurityRequest = {
  "ticketId": { "type": "string", "min": 1 },
};

securityEscortValidation.viewSecurityRequestBasedOperator = {
  "ticketId": { "type": "string", "min": 1 },
};

securityEscortValidation.getAvaliableOfficer = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.sendOfficerRequest = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.cancelEscortBooking = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.escortAdminStatusChange = {
  "escortBookingId": { "type": "string", "min": 1 },
  "escortStatus": { "type": "string", "min": 1 },
};

securityEscortValidation.trackEscortDetails = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.viewAddressSave = {
  "escortBookingId": { "type": "string", "min": 1 },
  "addressName": { "type": "string", "min": 1 },
  "fullAddress": { "type": "string", "min": 1 },
  "shortAddress": { "type": "string", "min": 1 },
  "type": {
    "type": "enum",
    "values": [constantUtil.ARRIVED, constantUtil.ENDED],
  },
};

securityEscortValidation.assignOfficer = {
  "escortBookingId": { "type": "string", "min": 1 },
  "officerId": { "type": "string", "min": 1 },
};

securityEscortValidation.priorityChange = {
  "escortBookingId": { "type": "string", "min": 1 },
  "priority": {
    "type": "enum",
    "values": [
      constantUtil.MEDIUM,
      constantUtil.MINIMAL,
      constantUtil.CRITICAL,
    ],
  },
};

securityEscortValidation.submitEscortRating = {
  "userType": {
    "type": "enum",
    "values": [
      constantUtil.USER.toLowerCase(),
      constantUtil.PROFESSIONAL.toLowerCase(),
      constantUtil.OFFICER.toLowerCase(),
    ],
  },
  "rating": { "type": "number", "convert": true },
  "comment": { "type": "string", "optional": true },
};

securityEscortValidation.sendEmergencyToTrustedContact = {
  "escortBookingId": { "type": "string", "min": 4 },
  "contacts": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "code": { "type": "string", "max": 6 },
        "number": { "type": "string", "max": 14 },
        "message": { "type": "string" },
      },
    },
  },
};

securityEscortValidation.sendEmergencyToTrustedContact = {
  "escortBookingId": { "type": "string", "min": 4 },
  "denyType": {
    "type": "enum",
    "values": [constantUtil.EXPIRED, constantUtil.USERDENY],
  },
  "userType": {
    "type": "enum",
    "values": [
      constantUtil.USER.toLowerCase(),
      constantUtil.PROFESSIONAL.toLowerCase(),
    ],
  },
};

securityEscortValidation.escortBookingCancel = {
  "escortBookingId": { "type": "string", "min": 1 },
  "userType": {
    "type": "enum",
    "values": [
      constantUtil.USER.toLowerCase(),
      constantUtil.PROFESSIONAL.toLowerCase(),
      constantUtil.OFFICER.toLowerCase(),
    ],
  },
};

securityEscortValidation.escortBookingMediaReport = {
  "escortBookingId": { "type": "string", "min": 1 },
  "mediaType": {
    "type": "enum",
    "values": [
      constantUtil.MESSAGE,
      constantUtil.IMAGE,
      constantUtil.VIDEO,
      constantUtil.AUDIO,
    ],
  },
  "message": { "type": "string", "optional": true },
};

securityEscortValidation.escortBookingMediaReportImage = {
  "escortBookingId": { "type": "string", "min": 1 },
};

securityEscortValidation.getOfficerHistory = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

securityEscortValidation.getUserEscortHistory = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

securityEscortValidation.viewEscortDetails = {
  "id": { "type": "string", "min": 1 },
};

module.exports = securityEscortValidation;
