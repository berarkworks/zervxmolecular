const constantUtills = require("../utils/constant.util");

const logValidation = {};

logValidation.manageLog = {
  "name": {
    "type": "enum",
    "values": [
      constantUtills.LOGIN,
      constantUtills.LOGOUT,
      constantUtills.ERRORDATA,
    ],
  },
  "userType": {
    "type": "enum",
    "values": [
      constantUtills.ADMIN,
      constantUtills.DEVELOPER,
      constantUtills.OPERATORS,
      constantUtills.COORPERATEOFFICE,
      constantUtills.USER,
      constantUtills.PROFESSIONAL,
    ],
  },
  "professionalId": { "type": "string", "convert": true, "optional": true },
  "userId": { "type": "string", "convert": true, "optional": true },
  "reason": { "type": "string", "convert": true, "optional": true },
};

logValidation.getLoginHistoryByloginId = {
  "userType": { "type": "string", "min": 1 },
  "loginId": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
//---------------------
module.exports = logValidation;
