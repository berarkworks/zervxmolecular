const constantUtil = require("../utils/constant.util");
const constantUtill = require("../utils/constant.util");

const professionalValidations = {};

professionalValidations.login = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
  "password": { "type": "string", "min": 1, "optional": true },
  "type": { "type": "string", "min": 1, "optional": true }, // Must Pass --> CONST_ANDROID_OS, CONST_IOS, CONST_WEB_OS
  "userName": { "type": "string", "min": 1, "optional": true }, // Must Pass --> cryptoJs-AES Encoded Data, Keys--> phoneCode + phoneNumber
};
professionalValidations.loginWithPhoneNo = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
  "password": { "type": "string", "min": 1, "optional": true },
  "type": { "type": "string", "min": 1, "optional": true }, // Must Pass --> CONST_ANDROID_OS, CONST_IOS, CONST_WEB_OS
  "userName": { "type": "string", "min": 1, "optional": true }, // Must Pass --> cryptoJs-AES Encoded Data, Keys--> phoneCode + phoneNumber
  "otpReceiverAppName": {
    "type": "enum",
    "values": [constantUtill.WHATSUP, constantUtill.SMS],
    "optional": true,
  },
};
professionalValidations.verifyOtp = {
  ...professionalValidations.login,
  "otp": { "type": "string", "optional": true },
};

professionalValidations.verifyEmail = {
  "professionalId": { "type": "string", "min": 4, "convert": true },
  "action": {
    "type": "enum",
    "values": [constantUtill.OTP, constantUtill.CONST_VERIFY],
  },
  "email": { "type": "string", "optional": true },
  "otp": { "type": "string", "optional": true },
};

professionalValidations.checkProfessionalAvail = {
  "phoneCode": { "type": "string", "trim": true },
  "phoneNumber": { "type": "string", "max": 15, "trim": true },
};

professionalValidations.updateProfile = {
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "email": { "type": "email", "optional": true },
  "isEmailVerified": { "type": "boolean", "convert": true, "default": false },
  // "gender": {
  //   "type": "enum",
  //   "values": [constantUtill.MALE, constantUtill.FEMALE, constantUtill.OTHER],
  // },
  "nationalIdNo": { "type": "string", "optional": true, "trim": true },
  "nationalInsuranceNo": { "type": "string", "optional": true, "trim": true },
  "gender": { "type": "string", "optional": true },
  "avatar": { "type": "string", "optional": true },
  "referredBy": { "type": "string", "optional": true, "trim": true },
  "uniqueCode": { "type": "string", "optional": true, "trim": true },
  "dob": {
    "type": "string",
    "pattern": "^\\d{2}-\\d{2}-\\d{4}|\\d{2}/\\d{2}/\\d{4}$",
    "optional": true,
  },
  "address": { "type": "string", "min": 1, "optional": true },
  "city": { "type": "string", "min": 1, "optional": true },
  "state": { "type": "string", "optional": true },
  "country": { "type": "string", "optional": true },
  "zipcode": { "type": "string", "optional": true },
};

professionalValidations.updateProfileImage = {
  "professionalId": { "type": "string", "min": 4, "convert": true },
  "avatar": { "type": "string", "optional": true },
};
professionalValidations.updateProfileAndVehicleDetails = {
  "firstName": { "type": "string", "min": 1 },
  "lastName": { "type": "string", "min": 1 },
  "serviceCategory": { "type": "string", "min": 1 },
  "hubId": { "type": "string", "min": 1, "optional": true },
  "referredBy": { "type": "string", "optional": true, "trim": true },
  // "uniqueCode": { "type": "string", "optional": true },
  // "address": { "type": "string", "min": 1, "optional": true },
  // "city": { "type": "string", "min": 1, "optional": true },
  // "state": { "type": "string", "optional": true },
  // "country": { "type": "string", "optional": true },
  // "zipcode": { "type": "string", "optional": true },
};
professionalValidations.activateAndInactivateAccount = {
  "professionalId": { "type": "string", "min": 4, "convert": true },
  "action": {
    "type": "enum",
    "values": [constantUtill.ACTIVE, constantUtill.INACTIVE],
  },
};

professionalValidations.changePhoneNumber = {
  ...professionalValidations.verifyOtp,
  "otp": { ...professionalValidations.verifyOtp.otp, "optional": true },
};

professionalValidations.updateOnlineStatus = {
  "onlineStatus": { "type": "boolean", "convert": true },
  "lat": { "type": "number", "convert": true, "optional": true },
  "lng": { "type": "number", "convert": true, "optional": true },
  "currentBearing": { "type": "number", "convert": true, "optional": true },
  "speed": { "type": "number", "convert": true, "optional": true },
  "verticalAccuracy": { "type": "number", "convert": true, "optional": true },
  "horizontalAccuracy": { "type": "number", "convert": true, "optional": true },
  "altitude": { "type": "number", "convert": true, "optional": true },
};

professionalValidations.updateProfileVerification = {
  "id": { "type": "string", "optional": true },
  "expiryDate": { "type": "string", "optional": true },
  "documentType": { "type": "string", "min": 1 },
  "documentName": { "type": "string", "min": 1 },
  "documents": { "type": "string", "optional": true },
  "files": { "type": "array", "optional": true },
  "isMandatory": { "type": "boolean", "convert": true, "default": false },
};

professionalValidations.removeProfileVerification = {
  "id": { "type": "string", "min": 4 },
};

professionalValidations.updateVehicleDetails = {
  "vehicleCategoryId": { "type": "string", "optional": true },
  "vinNumber": { "type": "string", "optional": true },
  // "type": {
  //   "type": "enum",
  //   "values": [
  //     constantUtill.PETROL,
  //     constantUtill.DIESEL,
  //     constantUtill.CNG,
  //     constantUtill.ELECTRIC,
  //     constantUtill.OTHER,
  //   ],
  // },
  "type": { "type": "string", "optional": true },
  "plateNumber": { "type": "string", "optional": true },
  "maker": { "type": "string", "optional": true },
  "model": { "type": "string", "optional": true },
  "year": { "type": "string", "optional": true },
  "color": { "type": "string", "optional": true },
  "noOfDoors": { "type": "string", "optional": true },
  "noOfSeats": { "type": "string", "optional": true },
  "default": { "type": "boolean", "optional": true, "convert": true },
};

professionalValidations.updateVehicleDocuments = {
  "id": { "type": "string", "min": 1 },
  "expiryDate": { "type": "string", "optional": true },
  "documentType": { "type": "string", "min": 1 },
  "documentName": { "type": "string", "min": 1 },
  "documents": { "type": "string", "optional": true },
  "files": { "type": "array", "optional": true },
};

professionalValidations.removeVehicleDetails = {
  "id": { "type": "string", "min": 4 },
};

professionalValidations.updateVehicleHubDetails = {
  "id": { "type": "string", "min": 12 },
  "hubId": { "type": "string" },
  "scheduleDate": { "type": "string" },
  "serviceCategory": { "type": "string", "min": 12 },
};

professionalValidations.getDocumentListForHub = {
  "hubId": { "type": "string" },
};
professionalValidations.updateLiveLocation = {
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
  "currentBearing": { "type": "number", "convert": true, "default": 0 },
  "speed": { "type": "number", "convert": true, "default": 0 },
  "verticalAccuracy": { "type": "number", "convert": true, "default": 0 },
  "horizontalAccuracy": { "type": "number", "convert": true, "default": 0 },
  "altitude": { "type": "number", "convert": true, "default": 0 },
  "onlineStatus": { "type": "boolean", "optional": true, "convert": true },
};

professionalValidations.logout = {};

professionalValidations.getProfessionalData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtill.LIFETIME, constantUtill.FILTER],
  },
};

professionalValidations.getVehiclesData = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtill.LIFETIME, constantUtill.FILTER],
  },
};

professionalValidations.getVehiclesDataBasedVehicleCategory = {
  "daysCount": { "type": "number", "convert": true },
  "type": {
    "type": "enum",
    "values": [constantUtill.LIFETIME, constantUtill.FILTER],
  },
};

professionalValidations.addBankDetails = {
  "accountName": { "type": "string", "min": 2 },
  "accountNumber": { "type": "string", "min": 6 },
  "bankCode": { "type": "string", "min": 1, "trim": true },
  // 'bankName': { 'type': 'string', 'min': 2 },
  // 'routingNumber': { 'type': 'string', 'min': 4 },
  // 'branchCode': { 'type': 'string', 'min': 2 },
  // 'country': { 'type': 'string', 'min': 1 },
  // 'currency': { 'type': 'string', 'min': 1 },
};

professionalValidations.manageCard = {
  "cardId": {
    "type": "string",
    "min": 1,
    "optional": true,
  },
  "type": { "type": "string", "trim": true, "optional": true },
  "cardNumber": { "type": "string", "min": 1, "trim": true },
  "expiryYear": { "type": "string", "min": 1, "trim": true },
  "expiryMonth": { "type": "string", "min": 1, "trim": true },
  "holderName": { "type": "string", "min": 1, "trim": true },
  "currencyCode": { "type": "string", "optional": true, "trim": true },
  "pin": { "type": "number", "optional": true, "convert": true, "trim": true },
  "isDefault": { "type": "boolean", "optional": true, "convert": true },
  "cvv": { "type": "string", "optional": true, "convert": true },
  "securityCode": { "type": "string", "optional": true },
  "paymentGateWayCustomerId": {
    "type": "string",
    "convert": false,
    "optional": true,
    "trim": true,
  },
};

professionalValidations.getRatings = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
professionalValidations.getList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "filter": { "type": "string" },
  "search": { "type": "string" },
};
professionalValidations.getDriverBillingList = {
  "billingId": { "type": "string" },
};
professionalValidations.updateSubCategoryOption = {
  "optionStatus": { "type": "boolean" },
  "isEnableShareRide": { "type": "boolean", "convert": true, "optional": true },
};
professionalValidations.addWalletAmount = {
  "id": { "type": "string" },
  "amount": { "type": "number", "convert": true },
  "reason": { "type": "string", "optional": true },
  "isReimbursement": { "type": "boolean", "optional": true },
  "type": {
    "type": "enum",
    "values": [
      constantUtill.CREDIT,
      constantUtill.DEBIT,
      constantUtill.REWARDPOINT,
      constantUtill.REDEEMREWARDPOINT,
    ],
  },
};
professionalValidations.updatePermissionData = {
  "osType": {
    "type": "enum",
    "values": [constantUtill.IOS, constantUtill.ANDROID],
    "optional": true,
  },
  "brand": { "type": "string", "optional": true },
  "model": { "type": "string", "optional": true },
  "osVersion": { "type": "string", "optional": true },
  "screenSize": { "type": "string", "optional": true },
  "internetConnectivityMedium": {
    "type": "string",
    "optional": true,
  },
  "token": { "type": "string", "optional": true },
  "fcmId": { "type": "string", "optional": true },
  "deviceLocationStatus": { "type": "boolean", "optional": true },
  "appLocationStatus": { "type": "boolean", "optional": true },
  "baseUrl": { "type": "string", "optional": true },
  "appVersionInServer": { "type": "string", "optional": true },
  "appVersionInStore": { "type": "string", "optional": true },
  "deviceTime": { "type": "string", "optional": true },
  "timezoneInNumber": { "type": "string", "optional": true },
  "timezoneInformat": { "type": "string", "optional": true },
  "connectivityMediumName": { "type": "string", "optional": true },
  "currentLocation": {
    "type": "object",
    "strict": true,
    "props": {
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
    "optional": true,
  },
  "socketConnetivity": { "type": "boolean", "optional": true },
};
professionalValidations.getInviteAndEarnHistory = {
  "inviteCode": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
professionalValidations.getInviteAndEarnList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "fromDate": { "type": "string", "optional": true },
  "toDate": { "type": "string", "optional": true },
  "isHasInviteCount": { "type": "boolean", "convert": true, "optional": true },
};
professionalValidations.getUsersJoinerList = {
  "id": { "type": "string", "min": 1 },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};
professionalValidations.updatePasswordBypass = {
  "id": { "type": "string", "min": 1 },
  "isOtpBypass": { "type": "boolean" },
};
professionalValidations.updateLastPriorityAddress = {
  "isPriorityStatus": { "type": "boolean" },
  "isPriorityLocation": {
    "type": "object",
    "props": {
      "placeId": { "type": "string", "min": 1 },
      "addressType": { "type": "string", "min": 1 },
      "addressName": { "type": "string", "min": 1 },
      "fullAddress": { "type": "string", "min": 1 },
      "shortAddress": { "type": "string", "min": 1 },
      "lat": { "type": "number", "convert": true },
      "lng": { "type": "number", "convert": true },
    },
  },
};
professionalValidations.updateLastPriorityStatus = {
  "isPriorityStatus": { "type": "boolean" },
};
professionalValidations.checkContacts = {
  "contacts": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "code": { "type": "string" },
        "number": { "type": "string" },
      },
    },
  },
};
// payment Related card ,bank
professionalValidations.verifyCard = {
  "transactionId": {
    "type": "string",
    "min": 1,
    "convert": true,
    "trim": "true",
  },
  "otp": { "type": "number", "convert": true, "trim": true },
  "mode": {
    "uppercase": true,
    "type": "enum",
    "values": [constantUtil.OTP, constantUtil.PIN],
  }, //router action
  "cardId": { "type": "string", "min": 1, "convert": true, "trim": true },
};
professionalValidations.addVerifiedCard = {
  "card": { "type": "object" },
  "professionalId": {
    "type": "string",
    "convert": true,
    "trim": true,
    "min": 1,
  },
};
professionalValidations.activateMembershipPlan = {
  "membershipPlanId": { "type": "string", "min": 12 },
  "professionalId": { "type": "string", "min": 12, "optional": true },
};
module.exports = professionalValidations;
