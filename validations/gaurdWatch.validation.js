const gaurdWatchValidation = {};

gaurdWatchValidation.createSiteLocation = {
  "placeId": { "type": "string" },
  "addressName": { "type": "string" },
  "shortAddress": { "type": "string" },
  "fullAddress": { "type": "string" },
  "lat": { "type": "number", "convert": true },
  "lng": { "type": "number", "convert": true },
};
gaurdWatchValidation.changeSiteLocationStatus = {
  "locationId": { "type": "string" },
  "status": { "type": "string" },
};
gaurdWatchValidation.deleteSiteLocation = {
  "locationId": { "type": "string" },
};
gaurdWatchValidation.sendQrToMail = {
  "locationId": { "type": "string" },
  "email": { "type": "string" },
};
gaurdWatchValidation.addGaurd = {
  "locationId": { "type": "string" },
  "code": { "type": "string" },
  "number": { "type": "string" },
  "fromTime": { "type": "string" },
  "workingHrs": { "type": "string" },
};

gaurdWatchValidation.getGaurdList = {
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "locationId": { "type": "string" },
};

gaurdWatchValidation.viewSiteLocation = {
  "locationId": { "type": "string" },
};

gaurdWatchValidation.loginAndLogout = {
  "locationId": { "type": "string" },
  "type": { "type": "string" },
};

gaurdWatchValidation.breakStatusChange = {
  "locationId": { "type": "string" },
  "type": { "type": "string" },
};

gaurdWatchValidation.scanAndUpdate = {
  "locationId": { "type": "string" },
  "locationData": {
    "type": "array",
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "lat": { "type": "number", "convert": true },
        "lng": { "type": "number", "convert": true },
        "date": { "type": "number", "convert": true },
      },
    },
  },
};

gaurdWatchValidation.getGaurdHistory = {
  "locationId": { "type": "string" },
  "gaurdId": { "type": "string" },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
};

gaurdWatchValidation.getMyHistory = {
  "locationId": { "type": "string" },
  "skip": { "type": "number", "convert": true },
  "limit": { "type": "number", "convert": true },
  "isCurrentSite": { "type": "boolean", "convert": true },
};

gaurdWatchValidation.removeGaurd = {
  "locationId": { "type": "string" },
  "gaurdId": { "type": "string" },
};

module.exports = gaurdWatchValidation;
