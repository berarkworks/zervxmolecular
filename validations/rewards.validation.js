const constantUtill = require("../utils/constant.util");

const rewardsValidation = {};

rewardsValidation.getRewardsById = {
  "cityId": { "type": "string", "min": 1 },
  "id": { "type": "string", "min": 1 },
};

module.exports = rewardsValidation;
