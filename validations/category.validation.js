const constantUtill = require("../utils/constant.util");

const categoryValidation = {};

categoryValidation.userGetLocationBasedCategory = {
  "userType": { "type": "string", "optional": true },
  "pickupLat": { "type": "number", "convert": true },
  "pickupLng": { "type": "number", "convert": true },
  "dropLat": { "type": "number", "convert": true },
  "dropLng": { "type": "number", "convert": true },
  "estimationDistance": { "type": "number", "convert": true },
  "estimationTime": { "type": "number", "convert": true },
  "bookingDate": { "type": "string", "min": 1 },
  "bookingType": {
    "type": "enum",
    "values": [constantUtill.INSTANT, constantUtill.SCHEDULE],
  },
};
categoryValidation.updateCategoryFareBreakup = {
  "cityId": { "type": "string", "min": 1 },
  "id": { "type": "string", "min": 1 },
  "title": { "type": "string", "optional": true },
  "description": { "type": "string", "optional": true },
  "fareMessage": { "type": "string", "optional": true },
  "amenities": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "title": { "type": "string" },
        "image": { "type": "string" },
      },
    },
  },
  "fleets": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "title": { "type": "string" },
        "image": { "type": "string" },
      },
    },
  },
  "guidelines": {
    "type": "array",
    "optional": true,
    "items": {
      "type": "object",
      "strict": true,
      "props": {
        "title": { "type": "string" },
        "points": { "type": "array", "items": { "type": "string" } },
      },
    },
  },
};
categoryValidation.getCategoryFareBreakup = {
  "cityId": { "type": "string", "min": 1 },
  "id": { "type": "string", "min": 1 },
};

module.exports = categoryValidation;
