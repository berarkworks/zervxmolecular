const paymentUtil = {};

// For card tokenization
paymentUtil.createTokenization = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.tokens.create(params.data);
  return data;
};
// For create Charge
paymentUtil.createCharges = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.charges.create(params.data);
  return data;
};
// For create Refund
paymentUtil.createRefund = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.refunds.create(params.data);
  return data;
};
// For capture Charge
paymentUtil.captureCharges = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.charges.capture(params.data);
  return data;
};
// For create customer
paymentUtil.createCustomer = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.customers.create(params.data);
  return data;
};
// For create Bank Account
paymentUtil.createBankAccount = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.customers.createSource(
    params.customer,
    params.data
  );
  return data;
};
// For Create Payout
paymentUtil.createPayout = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.payouts.create(params.data);
  return data;
};
// For Create Bank Source
paymentUtil.createBankSource = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.customers.createSource(
    params.customer,
    params.data
  );
  return data;
};
// For Transfer Amount
paymentUtil.createTransfer = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.transfers.create(params.data);
  return data;
};
// For create Customer Stripe Account
paymentUtil.createCustomerStripeAccount = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.accounts.create(params.data);
  return data;
};
// For create Customer Stripe Account
paymentUtil.createExternalAccount = async (params) => {
  const stripe = require("stripe")(params.secretKey);
  const data = await stripe.accounts.createExternalAccount(
    params.customer,
    params.data
  );
  return data;
};

module.exports = paymentUtil;
