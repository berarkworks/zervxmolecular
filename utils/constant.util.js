const constantUtil = {};

/* Error Code CONSTANTS */
/* ----------> Success <-----------*/
// 1   --> Success --> For Admin Panel
// 200 --> Success
// 422 --> Success --> Parameter Validation / show Notification Message
// 600 --> Success --> For Retry Booking Purpose Only

/* ----------> Faild <-----------*/
// 0   --> Faild --> For Admin Panel
// 500 --> Faild --> Default MoleculerError
// 500 --> Faild --> Professional Not Found
// 503 --> Faild --> Professional Not Found

/* API ROUTE CONSTANTS */
constantUtil.DEFAULT_PASSWORD = "123456";
constantUtil.DEFAULT_LANGUAGE = "en";
constantUtil.DEFAULT_AVATAR = "/images/user.default.jpg";
//
constantUtil.BASEDATA = "BASEDATA";
constantUtil.CLIENTDATA = "CLIENTDATA";
//
constantUtil.CONST_PARENT = "PARENT";
constantUtil.CONST_CHILD = "CHILD";
//
constantUtil.COUNTRYCODE_BRAZIL = "+55";
//
constantUtil.REQUIRED = "REQUIRED";
constantUtil.NOTREQUIRED = "NOT REQUIRED";
constantUtil.COMMON = "COMMON";
constantUtil.USER = "USER";
constantUtil.PROFESSIONAL = "PROFESSIONAL";
constantUtil.DRIVER = "DRIVER";
constantUtil.ADMINISTRATORS = "ADMINISTRATORS";
constantUtil.ADMIN = "ADMIN";
constantUtil.DEVELOPER = "DEVELOPER";
constantUtil.OPERATORS = "OPERATORS";
constantUtil.HUBS = "HUBS";
constantUtil.HUBSEMPLOYEE = "HUBSEMPLOYEE";
constantUtil.OFFICER = "OFFICER";
constantUtil.RESPONSEOFFICE = "RESPONSEOFFICE";
constantUtil.REPORTS = "REPORTS";
constantUtil.GUESTUSER = "GUESTUSER";
constantUtil.PROFESSIONALGUEST = "PROFESSIONALGUEST";
constantUtil.INVITEANDEARN = "INVITEANDEARN";
constantUtil.ERRORDATA = "ERRORDATA";
constantUtil.COORPERATEOFFICE = "COORPERATEOFFICE";
constantUtil.BOOKING = "BOOKING";
constantUtil.CORPORATEOFFICER = "CORPORATEOFFICER";
constantUtil.RESPONSEOFFICER = "RESPONSEOFFICER";
constantUtil.EXTERNAL = "EXTERNAL";
//
constantUtil.CONST_RIDE = "RIDE";
constantUtil.CONST_INTERCITYRIDE = "INTERCITYRIDE";
constantUtil.CONST_CORPORATERIDE = "CORPORATERIDE";
constantUtil.CONST_PICKUPOUTSIDERIDE = "PICKUPOUTSIDERIDE";
constantUtil.CONST_DROPOUTSIDERIDE = "DROPOUTSIDERIDE";
constantUtil.CONST_PICKUPANDDROPOUTSIDERIDE = "PICKUPANDDROPOUTSIDERIDE";
constantUtil.CONST_PACKAGES = "PACKAGES";
constantUtil.CONST_SHARERIDE = "SHARERIDE";
constantUtil.CONST_PICKUP = "PICKUP";
constantUtil.CONST_DROP = "DROP";
constantUtil.CONST_DELIVERY = "DELIVERY";
//
constantUtil.CONST_GOOGLE = "GOOGLE";
constantUtil.CONST_BING = "BING";
constantUtil.CONST_TOMTOM = "TOMTOM";
constantUtil.CONST_GEOAPIFY = "GEOAPIFY";
constantUtil.CONST_LOCATIONIQ = "LOCATIONIQ";
constantUtil.CONST_GBETA = "GBETA";
constantUtil.CONST_MAPBOX = "MAPBOX";
//
constantUtil.ACTION_ADD = "add"; // Don't Change Leter Case Type
constantUtil.ACTION_EDIT = "edit"; // Don't Change Leter Case Type
constantUtil.ACTION_VIEW = "view"; // Don't Change Leter Case Type
constantUtil.ACTION_DELETE = "delete"; // Don't Change Leter Case Type
//
constantUtil.CONST_AUTOCOMPLETE = "AUTOCOMPLETE";
constantUtil.CONST_DIRECTION = "DIRECTION";
constantUtil.CONST_LOCATION = "LOCATION";
constantUtil.CONST_PLACE = "PLACE";
constantUtil.CONST_SNAP = "SNAP";
//
constantUtil.CONST_GENDER = "GENDER";

/* ADMIN CONSTANTS */
constantUtil.GENERALSETTING = "GENERALSETTING";
constantUtil.MAPSETTING = "MAPSETTING";
constantUtil.SMSSETTING = "SMSSETTING";
constantUtil.NOTIFYSMSSETTING = "NOTIFYSMSSETTING";
constantUtil.PROMOTIONSMSSETTING = "PROMOTIONSMSSETTING";
constantUtil.EMERGENCYSMSSETTING = "EMERGENCYSMSSETTING";
constantUtil.NOTIFYSMSTEMPLATE = "NOTIFYSMSTEMPLATE";
constantUtil.WHATSUPSETTING = "WHATSUPSETTING";
constantUtil.CALLSETTING = "CALLSETTING";
constantUtil.CALLCENTER = "CALLCENTER";
constantUtil.HEATMAP = "HEATMAP";
constantUtil.SMTPSETTING = "SMTPSETTING";
constantUtil.FIREBASESETTING = "FIREBASESETTING";
constantUtil.PACKAGESETTING = "PACKAGESETTING";
//
constantUtil.CONST_REGISTRATIONSETTING = "REGISTRATIONSETTING";
constantUtil.CONST_AIRPORTTRANSFERSETTING = "AIRPORTTRANSFERSETTING";
constantUtil.CONST_REGISTRATIONSETTING_REDIS = "REGISTRATIONSETTINGREDIS";
constantUtil.CONST_BLOCKPROFESSIONALSETUP = "BLOCKPROFESSIONALSETUP";
//
constantUtil.VEHICLECATEGORY = "VEHICLECATEGORY";
constantUtil.PAYMENTGATEWAY = "PAYMENTGATEWAY";
constantUtil.PEACH = "PEACH";
constantUtil.OPERATORSROLE = "OPERATORSROLE";
constantUtil.CANCELLATIONREASON = "CANCELLATIONREASON";
constantUtil.DOCUMENTS = "DOCUMENTS";
constantUtil.PROFILEDOCUMENTS = "PROFILEDOCUMENTS";
constantUtil.DRIVERDOCUMENTS = "DRIVERDOCUMENTS";
constantUtil.VERIFICATIONDOCUMENT = "VERIFICATIONDOCUMENT";
constantUtil.HUBSPRIVILEGES = "HUBSPRIVILEGES";
constantUtil.HUBSEMPLOYEEPRIVILEGES = "HUBSEMPLOYEEPRIVILEGES";
constantUtil.OFFICEPRIVILEGES = "OFFICEPRIVILEGES";
constantUtil.COORPERATEPRIVILEGES = "COORPERATEPRIVILEGES";
constantUtil.LANGUAGES = "LANGUAGES";
constantUtil.SECURITYSERVICE = "SECURITYSERVICE";
constantUtil.TRUSTEDREASONS = "TRUSTEDREASONS";
constantUtil.VEHICLEDETAILS = "VEHICLEDETAILS";
constantUtil.EMAILTEMPLATE = "EMAILTEMPLATE";
constantUtil.POPULARPLACE = "POPULARPLACE";
constantUtil.POPULARDESTINATION = "POPULARDESTINATION";
constantUtil.AIRPORT = "AIRPORT";
constantUtil.TOLL = "TOLL";
constantUtil.COUPON = "COUPON";
constantUtil.NOTIFICATIONTEMPLATES = "NOTIFICATIONTEMPLATES";
constantUtil.MEMBERSHIPPLAN = "MEMBERSHIPPLAN";
constantUtil.PROFESSIONALSELECT = "PROFESSIONALSELECT";
constantUtil.WEEKLY = "WEEKLY";
constantUtil.MONTHLY = "MONTHLY";
constantUtil.YEARLY = "YEARLY";
constantUtil.PROFESSIONALMEMBERSHIPPLAN = "PROFESSIONALMEMBERSHIPPLAN";
constantUtil.USERMEMBERSHIPPLAN = "USERMEMBERSHIPPLAN";
constantUtil.PROFESSIONALACTIVATED = "PROFESSIONALACTIVATED";

/* USER & PROFESSIONAL ACCOUNT STATUS */
constantUtil.INCOMPLETE = "INCOMPLETE";
constantUtil.UNVERIFIED = "UNVERIFIED";
constantUtil.VERIFIED = "VERIFIED";
constantUtil.APPROVED = "APPROVED";
constantUtil.ACTIVE = "ACTIVE";
constantUtil.OFFLINE = "OFFLINE";
constantUtil.DEACTIVE = "DEACTIVE";
constantUtil.BLOCKED = "BLOCKED";
constantUtil.UNBLOCKED = "UNBLOCKED";
constantUtil.ATTENDED = "ATTENDED";
constantUtil.NEW = "NEW";
constantUtil.CLOSED = "CLOSED";
constantUtil.CRITICAL = "CRITICAL";
constantUtil.MINIMAL = "MINIMAL";
constantUtil.MEDIUM = "MEDIUM";
constantUtil.NOTASSIGNED = "NOTASSIGNED";
constantUtil.PETROL = "PETROL";
constantUtil.DIESEL = "DIESEL";
constantUtil.CNG = "CNG";
constantUtil.CONST_NON_ELECTRIC = "NONELECTRIC";
constantUtil.ELECTRIC = "ELECTRIC";
constantUtil.OTHER = "OTHER";
constantUtil.REGISTRATIONSTEP_PROFILE = "PROFILE";
constantUtil.REGISTRATIONSTEP_PROFILEDOCUMENTS = "PROFILEDOCUMENTS";
constantUtil.REGISTRATIONSTEP_VEHICLEDETAILS = "VEHICLEDETAILS";
constantUtil.REGISTRATIONSTEP_VEHICLEDOCUMENTS = "VEHICLEDOCUMENTS";
constantUtil.REGISTRATIONSTEP_HUBDETAILS = "HUBDETAILS";
constantUtil.REGISTRATIONSTEP_PENDINGVEHICLEVERIFICATION =
  "PENDINGVEHICLEVERIFICATION";
constantUtil.REGISTRATIONCOMPLETED = "HOME";
constantUtil.ADDRESSTYPE_HOME = "HOME";
constantUtil.ADDRESSTYPE_WORK = "WORK";
constantUtil.ADDRESSTYPE_OTHER = "OTHER";
constantUtil.PUBLIC = "PUBLIC";
constantUtil.PRIVATE = "PRIVATE";
constantUtil.INTERESTED = "INTERESTED";
constantUtil.NOTINTERESTED = "NOTINTERESTED";
constantUtil.MAYBE = "MAYBE";
constantUtil.REACHED = "REACHED";
constantUtil.MESSAGE = "MESSAGE";
constantUtil.IMAGE = "IMAGE";
constantUtil.VIDEO = "VIDEO";
constantUtil.AUDIO = "AUDIO";
constantUtil.STATUS_ONGOING = "ONGOING";
constantUtil.STATUS_UPCOMING = "UPCOMING";

/* BOOKING */
constantUtil.BOOKINGSERVICEMANAGEMET = "BOOKINGSERVICEMANAGEMET";
constantUtil.INSTANT = "INSTANT";
constantUtil.SCHEDULE = "SCHEDULE";
constantUtil.SHARE = "SHARE";
constantUtil.AWAITING = "AWAITING";
constantUtil.AWAITINGNEXTPROFESSIONAL = "AWAITINGNEXTPROFESSIONAL";
constantUtil.ARRIVED = "ARRIVED";
constantUtil.ACCEPTED = "ACCEPTED";
constantUtil.REJECTED = "REJECTED";
constantUtil.USERDENY = "USERDENY";
constantUtil.USERCANCELLED = "USERCANCELLED";
constantUtil.CANCELLED = "CANCELLED";
constantUtil.PROFESSIONALCANCELLED = "PROFESSIONALCANCELLED";
constantUtil.PROFESSIONALDENY = "PROFESSIONALDENY";
constantUtil.EXPIRED = "EXPIRED";
constantUtil.STARTED = "STARTED";
constantUtil.FORCEASSIGNBYADMIN = "FORCEASSIGNBYADMIN";
constantUtil.PAID = "PAID";
constantUtil.ENDED = "ENDED";
constantUtil.PAYMENTCREDIT = "CREDIT";
constantUtil.PAYMENTCASH = "CASH";
constantUtil.PAYMENTWALLET = "WALLET";
constantUtil.WALLET = "WALLET";
constantUtil.PAYMENTCARD = "CARD";
constantUtil.PAYMENTCOMMISSION = "COMMISSION";
constantUtil.ORIGIN = "ORIGIN";
constantUtil.STOP = "STOP";
constantUtil.DESTINATION = "DESTINATION";
constantUtil.EQUALTOPAID = "EQUALTOPAID";
constantUtil.LESSTHENPAID = "LESSTHENPAID";
constantUtil.GREATERTHENPAID = "GREATERTHENPAID";

/* COMMON CONSTANTS */
constantUtil.INACTIVE = "INACTIVE";
constantUtil.ARCHIVE = "ARCHIVE";
constantUtil.LTR = "LTR";
constantUtil.RTL = "RTL";
constantUtil.MILES = "MILES";
constantUtil.MILES_VALUE = 1610;
constantUtil.KM = "KM";
constantUtil.KM_VALUE = 1000;
constantUtil.AVATAR = "AVATAR";
constantUtil.DEVELOPMENT = "DEVELOPMENT";
constantUtil.PRODUCTION = "PRODUCTION";
constantUtil.DEFAULT = "DEFAULT";

/* PAYMENTS CONSTANTS */
constantUtil.LIVE = "LIVE";
constantUtil.SANDBOX = "SANDBOX";
constantUtil.CARD = "CARD";
constantUtil.CASH = "CASH";
constantUtil.CREDIT = "CREDIT";
constantUtil.DEBIT = "DEBIT";
constantUtil.SUCCESS = "SUCCESS";
constantUtil.FAILED = "FAILED";
constantUtil.PENDING = "PENDING";
constantUtil.PROCESSING = "PROCESSING";
constantUtil.REDEEM = "REDEEM";
constantUtil.COMPLETE = "COMPLETE";

/* TRANSACTION CONSTANTS */
constantUtil.CANCELCAPTURE = "CANCELCAPTURE";
constantUtil.PENALITY = "PENALITY";

/* DB CONSTANTS */
constantUtil.DBSCHEMAADMIN = "admin";
constantUtil.DBSCHEMAUSERS = "users";
constantUtil.DBSCHEMAPROFESSIONAL = "professionals";
constantUtil.DBSCHEMACATEGORIES = "categories";
constantUtil.DBSCHEMABOOKINGS = "bookings";
constantUtil.DBSCHEMATRANSACTION = "transactions";
constantUtil.DBSCHEMAOFFICER = "officers";
constantUtil.DBSCHEMASUBSCRIPTION = "subscription";
constantUtil.DBSCHEMASECURITYESCORT = "securityescorts";
constantUtil.DBSCHEMABILLINGS = "billings";
constantUtil.DBSCHEMAEVENTSWATCH = "eventWatch";
constantUtil.DBSCHEMAGAURDWATCH = "gaurdWatch";

constantUtil.DBSCHEMAREWARDS = "rewards";
constantUtil.DBSCHEMALOG = "log";
constantUtil.DBSCHEMA_ZMAP = "zmap";
constantUtil.DBSCHEMA_HEATMAP = "heatmap";

/* OTHER */
constantUtil.MALE = "MALE";
constantUtil.FEMALE = "FEMALE";
constantUtil.OTHER = "OTHER";
constantUtil.DEFAULTAVATAR = "/images/user.default.jpg";
constantUtil.NOTIFICATIONTYPE = "NOTIFICATION";
constantUtil.ADD = "ADD";
constantUtil.UPDATE = "UPDATE";
constantUtil.REMOVE = "REMOVE";
constantUtil.NEXT = "NEXT";
constantUtil.YES = "YES";
constantUtil.NO = "NO";
constantUtil.PICKUPLOCATIONOUT = "PICKUPLOCATIONOUT";
constantUtil.DROPLOCATIONOUT = "DROPLOCATIONOUT";
constantUtil.PICKUPANDDROPLOCATIONOUT = "PICKUPANDDROPLOCATIONOUT";
constantUtil.NOCATEGORYAVAILABLE = "NOCATEGORYAVAILABLE";
constantUtil.SERVICENOTAVAILABLE = "SERVICENOTAVAILABLE";
constantUtil.CATEGORYAVAILABLE = "CATEGORYAVAILABLE";
constantUtil.VALID = "VALID";
constantUtil.ISVALID = "ISVALID";
constantUtil.GOINGTOINVALID = "GOINGTOINVALID";
constantUtil.WHENBOOKING = "WHENBOOKING";
constantUtil.WHENCOMPLETE = "WHENCOMPLETE";
constantUtil.ANDROID = "ANDROID";
constantUtil.IOS = "IOS";
constantUtil.UPLOADED = "UPLOADED";
constantUtil.DUEAMOUNT = "DUEAMOUNT";
constantUtil.FREEZEDAMOUNT = "FREEZEDAMOUNT";
constantUtil.LOGIN = "LOGIN";
constantUtil.LOGOUT = "LOGOUT";
constantUtil.BREAKSTART = "BREAKSTART";
constantUtil.BREAKEND = "BREAKEND";
constantUtil.ALL = "ALL";
constantUtil.CASHANDWALLET = "CASHANDWALLET";
constantUtil.INSTANTPAYMENT = "INSTANTPAYMENT";
constantUtil.BILLINGCYCLE = "BILLINGCYCLE";
constantUtil.NONE = "NONE";
constantUtil.OPTIONAL = "OPTIONAL";
constantUtil.FORCEUPDATE = "FORCEUPDATE";
//
constantUtil.WHATSUP = "WHATSUP";
constantUtil.TWILIO = "TWILIO";
constantUtil.FIREBASE = "FIREBASE";
constantUtil.TELECMI = "TELECMI";
constantUtil.SINCH = "SINCH";
constantUtil.SENDBIRD = "SENDBIRD";
constantUtil.AFRO = "AFRO";
constantUtil.SENDIFY = "SENDIFY";
constantUtil.TUNISIESMS = "TUNISIESMS";
//
constantUtil.FCMACCESSTOKEN = "FCMACCESSTOKEN";
constantUtil.NORMAL = "NORMAL";
constantUtil.MINIMUMDISTANCE = "MINIMUMDISTANCE";
constantUtil.MANUAL = "MANUAL";
constantUtil.ROUTE = "ROUTE";
constantUtil.FLAT = "FLAT";
constantUtil.PERCENTAGE = "PERCENTAGE";
constantUtil.FIRSTUSER = "FIRSTUSER";
constantUtil.EVERYONE = "EVERYONE";
constantUtil.DAILYTRIP = "DAILYTRIP";
constantUtil.OUTSTATION = "OUTSTATION";
constantUtil.RENTAL = "RENTAL";
constantUtil.CARPOOL = "CARPOOL";
constantUtil.AMBULANCE = "AMBULANCE";
constantUtil.PACKAGEDELIVERY = "PACKAGEDELIVERY";
constantUtil.PACKAGERECEIVER = "PACKAGERECEIVER";
constantUtil.PACKAGEDELIVERYANDRECEIVER = "PACKAGEDELIVERYANDRECEIVER";
constantUtil.SMS = "SMS";
constantUtil.EMAIL = "EMAIL";
constantUtil.PUSH = "PUSH";
constantUtil.PROMOTION = "PROMOTION";

/* SECURITY SERVICES */
constantUtil.SOS = "SOS";
constantUtil.EMERGENCY = "EMERGENCY";
constantUtil.TRACKING = "TRACKING";

/* DASHBOARD FILTER*/
constantUtil.LIFETIME = "LIFETIME";
constantUtil.FILTER = "FILTER";

/* NOTIFICATION ACTION */
constantUtil.ACTION_HOME = "HOME";
constantUtil.ACTION_BOOKINGREQUEST = "BOOKINGREQUEST";
constantUtil.ACTION_CANCELBOOKING = "CANCELBOOKING";
constantUtil.ACTION_ACCEPTBOOKING = "ACCEPTBOOKING";
constantUtil.ACTION_PROFESSIONALARRIVED = "PROFESSIONALARRIVED";
constantUtil.ACTION_PROFESSIONALSTARTED = "PROFESSIONALSTARTED";
constantUtil.ACTION_BOOKINGENDED = "BOOKINGENDED";
constantUtil.ACTION_BOOKINGSTOPARRIVED = "BOOKINGSTOPARRIVED";
constantUtil.ACTION_BOOKINGSTOPENDED = "BOOKINGSTOPENDED";
constantUtil.ACTION_BOOKINGLOCATIONCHANGE = "BOOKINGLOCATIONCHANGE";
constantUtil.ACTION_BOOKINGFORCEASSIGNBYADMIN = "BOOKINGFORCEASSIGNBYADMIN";
constantUtil.ACTION_ESCORTREQUEST = "ESCORTREQUEST";
constantUtil.ACTION_ESCORTACCEPT = "ESCORTACCEPT";
constantUtil.ACTION_ESCORTARRIVED = "ESCORTARRIVED";
constantUtil.ACTION_ESCORTENDED = "ESCORTENDED";
constantUtil.ACTION_DOCUMENTEXPIRED = "DOCUMENTEXPIRED";
constantUtil.ACTION_REPORTSNOTIFICATION = "REPORTSNOTIFICATION";
constantUtil.ACTION_SECURITYNOTIFICATION = "SECURITYNOTIFICATION";
constantUtil.ACTION_SECURITYCANCEL = "SECURITYCANCEL";
constantUtil.ACTION_ESCORTCANCEL = "ESCORTCANCEL";
constantUtil.ACTION_ESCORTADMINASSIGN = "ESCORTADMINASSIGN";
constantUtil.ACTION_BOOKINGCANCELCHECKINGFORANOTHERDRIVER =
  "BOOKINGCANCELCHECKINGFORANOTHERDRIVER";
constantUtil.ACTION_PROMOTION = "PROMOTION";
constantUtil.ACTION_EMERGENCY = "EMERGENCY";
constantUtil.ACTION_WALLETTRANSACTIONUPDATE = "WALLETTRANSACTIONUPDATE";
constantUtil.ACTION_TRIPSTATUSCHANGEDBYADMIN = "TRIPSTATUSCHANGEDBYADMIN";
constantUtil.ACTION_ASSIGN = "ASSIGN";
constantUtil.ACTION_REASSIGN = "REASSIGN";
constantUtil.ACTION_FORCEASSIGN = "FORCEASSIGN";
constantUtil.ACTION_TRUSTEDREASONSANDCONTACTANDLOG =
  "TRUSTEDREASONSANDCONTACTANDLOG";
constantUtil.ACTION_UPDATE_WAYDATA = "UPDATEWAYDATA";

/* EMAIL CONSTANTS */
constantUtil.EMAIL_CONST_OTPFORVERIFICATION = "OtpForVerification";
constantUtil.EMAIL_CONST_INVOICE = "Invoice";
constantUtil.EMAIL_CONST_WELCOMETOOPERATOR = "WelcomeToOperator";
constantUtil.EMAIL_CONST_WELCOMETOHUBS = "WelcomeToHubs";
constantUtil.EMAIL_CONST_WELCOMETOPROFESSIONAL = "WelcomeToProfessional";
constantUtil.EMAIL_CONST_USERWELCOMEMAIL = "userWelcomeMail";
constantUtil.EMAIL_CONST_ORDERINVOICEFORUSER = "OrderInvoiceForUser";
constantUtil.EMAIL_CONST_ORDERINVOICEFORPROFESSIONAL =
  "OrderInvoiceForProfessional";
constantUtil.EMAIL_CONST_PROFESSIONALWELCOMEMAIL = "professionalWelcomeMail";
constantUtil.EMAIL_CONST_USERINVOICEMAILTEMPLATE = "userInvoiceMailTemplate";
constantUtil.EMAIL_CONST_PROFESSIONALINVOICEMAILTEMPLATE =
  "professionalInvoiceMailTemplate";
constantUtil.EMAIL_CONST_CORPORATEINVOICEMAILTEMPLATE =
  "corporateInvoiceMailTemplate";
constantUtil.EMAIL_CONST_RIDEEXPIREDEMAIL = "rideExpiredEmail";
constantUtil.EMAIL_CONST_PROFESSIONALCANCELLATIONEMAILWITHCANCELLATIONAMOUNT =
  "professionalCancellationEmailWithCancellationAmount";
constantUtil.EMAIL_CONST_USERCANCELLATIONEMAILWITHCANCELAMOUNT =
  "userCancellationEmailWithCancelAmount";
constantUtil.EMAIL_CONST_CLIENTWELCOMEMAIL = "clientWelcomeMail";
constantUtil.EMAIL_CONST_EMAILVERIFICATION = "emailVerification";
constantUtil.EMAIL_CONST_DOCUMENTEXPIRED = "documentExpired";
constantUtil.EMAIL_CONST_DOCUMENTREJECTED = "documentRejected";
constantUtil.EMAIL_CONST_EMERGENCYNOTIFICATION = "emergencyNotification";

/* TRANSACTON CONSTANTS */
constantUtil.WALLETRECHARGE = "WALLETRECHARGE";
constantUtil.WALLETWITHDRAWAL = "WALLETWITHDRAWAL";
constantUtil.ADMINRECHARGE = "ADMINRECHARGE";
constantUtil.SUBSCRIPTIONCHARGE = "SUBSCRIPTIONCHARGE";
constantUtil.SENDMONEYTOFRIEND = "SENDMONEYTOFRIEND";
constantUtil.BOOKINGCHARGE = "BOOKINGCHARGE";
constantUtil.BOOKINGCHARGEFREEZED = "BOOKINGCHARGEFREEZED";
constantUtil.TIPSCHARGE = "TIPSCHARGE";
constantUtil.HUBSEARNINGS = "HUBSEARNINGS";
constantUtil.PROFESSIONALEARNINGS = "PROFESSIONALEARNINGS";
constantUtil.PROFESSIONALTOLERENCEAMOUNT = "PROFESSIONALTOLERENCEAMOUNT";
constantUtil.ADMINDEBIT = "ADMINDEBIT";
constantUtil.CANCELLATIONCHARGE = "CANCELLATIONCHARGE";
constantUtil.INVITECHARGE = "INVITECHARGE";
constantUtil.JOININGCHARGE = "JOININGCHARGE";
constantUtil.CARDAMOUNTREFUND = "CARDAMOUNTREFUND";
constantUtil.EXTRAAMOUNTDEBIT = "EXTRAAMOUNTDEBIT";
constantUtil.VERIFYCARD = "VERIFYCARD";
constantUtil.REWARDPOINT = "REWARDPOINT";
constantUtil.REDEEMREWARDPOINT = "REDEEMREWARDPOINT";
constantUtil.REDEEMREWARDAIRTIME = "REDEEMREWARDAIRTIME";
constantUtil.BOOKINGCHARGECARD = "BOOKINGCHARGECARD";
constantUtil.CANCELLATIONCREDIT = "CANCELLATIONCREDIT";

constantUtil.MSG_WALLETRECHARGE = "Amount Added To Wallet";
constantUtil.WALLETWITHDRAWAL_ = "Amount WithDraw from Wallet";
constantUtil.ADMINRECHARGE_ = "Wallet Amount Loaded";
constantUtil.ADMINDEBIT_ = "Wallet Amount Debited";
constantUtil.SUBSCRIPTIONCHARGE_ = "Paid For Subscription";
constantUtil.SENDMONEYTOFRIEND_ = "Transfered To Friend";
constantUtil.BOOKINGCHARGE_ = "Paid For Booking";
constantUtil.BOOKINGCHARGEFREEZED_ = "Freezed Amount For Booking";
constantUtil.TIPSCHARGE_ = "You gave Tips";
constantUtil.PROFESSIONALEARNINGS_ = "Booking Earnings Received";
constantUtil.PROFESSIONALTOLERENCEAMOUNT_ = "Refund For Discount Applied";
constantUtil.CANCELLATIONCHARGE_ = "Ride cancel amount";
constantUtil.INVITECHARGE_ = "Referrel Amount";
constantUtil.JOININGCHARGE_ = "Joining Amount";
constantUtil.CARDAMOUNTREFUND_ = "Remaining Payment Amount Loaded";
constantUtil.EXTRAAMOUNTDEBIT_ = "Remaining Payment Amount Debited";

// PAYMENT GATEWAYS
constantUtil.PEACH = "PEACH";
constantUtil.FLUTTERWAVE = "FLUTTERWAVE";
constantUtil.STRIPE = "STRIPE";
constantUtil.RAZORPAY = "RAZORPAY";
constantUtil.CONST_TIGO = "TIGO";
constantUtil.CONST_TELEPORT = "TELEPORT";
constantUtil.CONST_CYBERSOURCE = "CYBERSOURCE";
constantUtil.CONST_ZCREDITAPIARY = "ZCREDITAPIARY";
constantUtil.CONST_MERCADOPAGO = "MERCADOPAGO";
constantUtil.CONST_TEYA = "TEYA";

// CARD VALIDATION TYPE
constantUtil.PIN = "PIN";
constantUtil.OTP = "OTP";
constantUtil.REDIRECT = "REDIRECT";
constantUtil.NOAUTH = "NOAUTH";
constantUtil.ADDRESS = "ADDRESS";
constantUtil.DISPATCH = "DISPATCH";
constantUtil.CONST_VERIFY = "VERIFY";
//

//payout
constantUtil.AUTOMATED = "AUTOMATED";
// constantUtil.MANUAL = "MANUAL";
constantUtil.DISPLAYINFO = "DISPLAYINFO";
constantUtil.TRANSFERRETRY = "TRANSFERRETRY";

// CLIENT RELATED
constantUtil.BRSCLIENTS = "BRSCLIENTS";
constantUtil.PLANS = "PLANS";

// PROMOTION CAMPIGN CATEGORY TYPES

// for user
constantUtil.ALPHABETICALORDER = "ALPHABETICALORDER";
constantUtil.CITY = "CITY";
constantUtil.CONST_ONLINE = "ONLINE";
constantUtil.CONST_OFFLINE = "OFFLINE";
constantUtil.CONST_CUSTOM = "CUSTOM";

// for professionals

// Ride Booking Algorithem

constantUtil.COMPETITIVE = "COMPETITIVE";
constantUtil.NEAREST = "NEAREST";
constantUtil.SHORTEST = "SHORTEST";
constantUtil.CONST_DUMP = "DUMP";
constantUtil.CONST_GZIP = "GZIP";

//LOYALTY COUPONS
constantUtil.LOYALTYCOUPON = "LOYALTYCOUPON"; //old
constantUtil.REWARDS = "REWARDS";
constantUtil.POINTS = "POINTS";
constantUtil.PARTNERDEALS = "PARTNERDEALS";
//
constantUtil.CONST_NOTFOUND = "Not Found.";
constantUtil.CORPORATECREDIT = "CORPORATE CREDIT";
//
constantUtil.CONST_GET = "GET";
constantUtil.CONST_TRACK = "TRACK";
//
constantUtil.CONST_LIVEMETER = "LIVEMETER";
constantUtil.CONST_SNAPWAY = "SNAPWAY";
constantUtil.CONST_MANUALWAY = "MANUALWAY";
constantUtil.CONST_INTERRUPT = "INTERRUPT";
constantUtil.CONST_TIMEINTERRUPT = "TIMEINTERRUPT";
constantUtil.CONST_DISTANCEINTERRUPT = "DISTANCEINTERRUPT";
constantUtil.CONST_MANUAL = "MANUAL";
//
constantUtil.CONST_ANDROID_OS = "A0S";
constantUtil.CONST_IOS = "I0S";
constantUtil.CONST_WEB_OS = "W0S";
//
constantUtil.CONST_POS = "POS";
constantUtil.CONST_PIX = "PIX";
constantUtil.CONST_SPECIALDAYS = "SPECIALDAYS";
//
constantUtil.CONST_DECIMAL = "DECIMAL";
constantUtil.CONST_INTEGER = "INTEGER";
//
constantUtil.CONST_APP = "APP";
constantUtil.CONST_WEBAPP = "WEBAPP";
//
constantUtil.CONST_SERVICETYPE = "SERVICETYPE";
constantUtil.CONST_ZERVXSERVICES = "ZERVXSERVICES";
constantUtil.CONST_LANDINGPAGE = "LANDINGPAGE";
//
constantUtil.CONST_LOG = "LOG";
constantUtil.CONST_INCENTIVE = "INCENTIVE";
constantUtil.CONST_GUARANTEE = "GUARANTEE";
constantUtil.CONST_CAMPAIGN = "CAMPAIGN";
constantUtil.CONST_WEEKDAYS = "WEEKDAYS";
constantUtil.CONST_PASSENGERTYPE = "PASSENGERTYPE";
constantUtil.CONST_LUGGAGETYPE = "LUGGAGETYPE";
constantUtil.CONST_LANGUAGE = "LANGUAGE";
constantUtil.CONST_REVIEWCOMMENTS = "REVIEWCOMMENTS";
constantUtil.CONST_WATERBOTTLETYPE = "WATERBOTTLETYPE";
constantUtil.CONST_FUELTYPE = "FUELTYPE";
//
constantUtil.CONST_V1 = "V1";
constantUtil.CONST_V2 = "V2";
constantUtil.CONST_V3 = "V3";
//
constantUtil.CONST_MANDATORY = "MANDATORY";
constantUtil.CONST_SKIP = "SKIP";
//----------------------------------------
module.exports = constantUtil;
