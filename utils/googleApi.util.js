const axios = require("axios");

const storageUtil = require("./storage.util");
const constantUtil = require("./constant.util");

const googleApiUtil = {};

googleApiUtil.geocode = async (ctx) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const url =
    "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
    ctx.lat +
    "," +
    ctx.lng +
    "&sensor=false" +
    "&key=" +
    generalSettings.data.mapApi.api;
  const response = await axios(url);
  return response;
};

googleApiUtil.directions = async (ctx) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const url =
    "https://maps.googleapis.com/maps/api/directions/json?origin=" +
    ctx.from +
    "&destination=" +
    ctx.to +
    "&key=" +
    generalSettings.data.mapApi.api +
    "&sensor=false&mode=driving";
  const response = await axios(url);
  return response;
};

googleApiUtil.distancematrix = async (ctx) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const url =
    "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
    ctx.lat +
    "," +
    ctx.lng +
    "&key=" +
    generalSettings.data.mapApi.api +
    "&sensor=false&mode=driving";
  const response = await axios(url);
  return response;
};

googleApiUtil.waypoints = async (ctx) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const url =
    "https://maps.googleapis.com/maps/api/directions/json?" +
    "&origin=" +
    ctx.from +
    "&destination=" +
    ctx.to +
    "&waypoints=optimize:true" + // optimize:true for optimize path
    ctx.waypoints +
    "&key=" +
    generalSettings.data.mapApi.api +
    "&sensor=false&mode=driving";
  const response = await axios(url);
  return response;
};

googleApiUtil.snapPoints = async (ctx) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const url =
    "https://roads.googleapis.com/v1/snapToRoads?path=" +
    ctx.path +
    "&interpolate=true" + // interpolate true = large value || false = less value
    "&key=" +
    generalSettings.data.mapApi.api;
  const response = await axios(url);
  return response;
};

googleApiUtil.formattedAddress = async (arrayAddress) => {
  let arrayValueCount = 0,
    formatted_address = "";
  for (let i = 0; i < arrayAddress.length; i++) {
    const mapData = arrayAddress[i].formatted_address.split(",");
    // mapData = mapData.split(",");
    if (arrayValueCount < mapData.length) {
      arrayValueCount = mapData.length;
      if (mapData.toString().indexOf("+", -1) !== -1) {
        mapData.shift();
      }
      // mapData = mapData.split(",");
      formatted_address = mapData?.toString("utf8");
    }
  }
  return formatted_address;
};

module.exports = googleApiUtil;
