const { utc } = require("moment-timezone");
const formula = require("formula");
const bcrypt = require("bcrypt-nodejs");
const geolib = require("geolib");
const storageutil = require("../utils/storage.util");

const formulaJSON = require("../data/formula.json");
const constantUtil = require("./constant.util");
const storageUtil = require("./storage.util");
const googleApiUtil = require("./googleApi.util");
const { calculateDistanceAndTimeFare } = require("../utils/mapping.util");
const {
  getDayAndMonthNameOfDate,
  toRegionalUTC,
  roundingAmountCalculation,
} = require("../utils/common.util");

const helperUtil = {};
helperUtil.getMinutesDiff = (greaterTime, smallTime) => {
  return Math.round(
    (((new Date(greaterTime) - new Date(smallTime)) % 86400000) % 3600000) /
      60000
  );
};
helperUtil.toUTC = (timestamp) => {
  return utc(timestamp).format();
};
helperUtil.toRegionalUTC = (date) => {
  let newLocalUTCDate = date;
  try {
    newLocalUTCDate = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).toISOString();
  } catch (e) {
    newLocalUTCDate = new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60000
    ).toISOString();
  }
  return newLocalUTCDate;
};

const getOffset = (timeZone = "UTC", date = new Date()) => {
  const utcDate = new Date(date.toLocaleString("en-US", { "timeZone": "UTC" }));
  const tzDate = new Date(date.toLocaleString("en-US", { timeZone }));
  return (tzDate.getTime() - utcDate.getTime()) / 6e4;
};

helperUtil.validPassword = (password, passwordb) => {
  return bcrypt.compareSync(password, passwordb);
};

helperUtil.getDaysToMilliSeconds = (daysCount) => {
  return (daysCount || 1) * (1000 * 60 * 60 * 24);
};

helperUtil.pickArrayWaypoint = (ar) => {
  const newArray = ar;
  const finalArray = [];
  const finalWayPoint = [];
  const finalWayDataSet = [];
  for (let index = 0; 21 <= parseInt(newArray.length); index++) {
    const array = newArray.splice(0, 21, newArray[20]);
    finalWayPoint.push(...array);
    finalWayDataSet.push(array);
    const wayValue = [];
    array.forEach((w) => {
      if (w?.lat && w?.lng) {
        wayValue.push(`|${w.lat.toString()},${w.lng.toString()}`);
      }
    });
    finalArray.push(wayValue.join(""));
  }
  if (21 > newArray.length) {
    const wayValue = [];
    finalWayPoint.push(...newArray);
    finalWayDataSet.push(newArray);
    newArray.forEach((w) => {
      if (w?.lat && w?.lng) {
        wayValue.push(`|${w.lat.toString()},${w.lng.toString()}`);
      }
    });
    finalArray.push(wayValue.join(""));
  }
  return {
    "finalArray": finalArray,
    "finalWayPoint": finalWayPoint,
    "finalWayDataSet": finalWayDataSet,
  };
};

helperUtil.snapWayPath = async (ar) => {
  const newArray = ar,
    finalArray = [],
    finalWayPoint = [],
    finalWayDataSet = [],
    finalSnap = [],
    finalCheckSnap = [],
    lengthData = [];

  let isSnap = false,
    snapErrorData = {},
    finalSnapWayData = [],
    length = 0;
  try {
    for (let index = 0; 20 <= parseInt(newArray.length); index++) {
      const array = newArray.splice(0, 20, newArray[19]);
      finalWayPoint.push(...array);
      const wayValue = [];
      array.forEach((w, i) => {
        if (w?.lat && w?.lng) {
          if (i === 0) {
            wayValue.push(`${w.lat.toString()},${w.lng.toString()}`);
          } else {
            wayValue.push(`|${w.lat.toString()},${w.lng.toString()}`);
          }
        }
      });
      finalArray.push(wayValue.join(""));
    }
    if (20 > newArray.length) {
      const wayValue = [];
      finalWayPoint.push(...newArray);
      newArray.forEach((w, i) => {
        if (w?.lat && w?.lng) {
          if (i === 0) {
            wayValue.push(`${w.lat.toString()},${w.lng.toString()}`);
          } else {
            wayValue.push(`|${w.lat.toString()},${w.lng.toString()}`);
          }
        }
      });
      finalArray.push(wayValue.join(""));
    }
    if (finalArray && finalArray.length > 0) {
      for (const data of finalArray) {
        try {
          const snapData = await googleApiUtil.snapPoints({ "path": data });
          if (snapData.status === 200 && snapData.statusText === "OK") {
            if (snapData?.data?.snappedPoints) {
              finalSnap.push(...snapData.data.snappedPoints);
              finalWayDataSet.push(snapData.data.snappedPoints);
              isSnap = true;
            }
          } else {
            snapErrorData = {
              "status": snapData.status,
              "statusText": snapData.statusText,
              "data": snapData.data,
              "headers": snapData.headers,
              "config": snapData.config,
            };
          }
        } catch (e) {
          console.log(e.message);
        }
      }
    }
    if (isSnap) {
      finalSnap.forEach((e) => {
        finalSnapWayData.push({
          "lat": e.location.latitude,
          "lng": e.location.longitude,
          "timeStamp":
            finalWayPoint[e.originalIndex] &&
            finalWayPoint[e.originalIndex].timeStamp
              ? finalWayPoint[e.originalIndex].timeStamp
              : finalWayPoint[0].timeStamp,
          "isStop":
            finalWayPoint[e.originalIndex] &&
            finalWayPoint[e.originalIndex].isStop
              ? finalWayPoint[e.originalIndex].isStop
              : finalWayPoint[0].isStop,
        });
        lengthData.push({
          "latitude": e.location.latitude,
          "longitude": e.location.longitude,
        });
      });
      finalWayDataSet.forEach((each) => {
        for (const e of each) {
          finalCheckSnap.push({
            "lat": e.location.latitude,
            "lng": e.location.longitude,
            "timeStamp":
              finalWayPoint[e.originalIndex] &&
              finalWayPoint[e.originalIndex].timeStamp
                ? finalWayPoint[e.originalIndex].timeStamp
                : finalWayPoint[0].timeStamp,
            "isStop":
              finalWayPoint[e.originalIndex] &&
              finalWayPoint[e.originalIndex].isStop
                ? finalWayPoint[e.originalIndex].isStop
                : finalWayPoint[0].isStop,
          });
        }
      });
    } else {
      finalSnapWayData = finalWayPoint;
    }
    length =
      lengthData && lengthData.length > 0
        ? geolib.getPathLength(lengthData)
        : 0;
  } catch (e) {
    console.log(e.message);
  }
  return {
    "snapErrorData": snapErrorData,
    "finalSnapWayData": finalSnapWayData,
    "finalWayDataSet": finalCheckSnap,
    "distance": length,
  };
};

helperUtil.manualWayDistanceCalculate = (ar) => {
  const lengthData = [];
  for (const e of ar) {
    lengthData.push({ "latitude": e.lat, "longitude": e.lng });
  }
  const length =
    lengthData && lengthData.length > 0 ? geolib.getPathLength(lengthData) : 0;

  return {
    "finalWayPoint": ar,
    "distance": length,
  };
};

helperUtil.randomString = (length, chars) => {
  let mask = "";
  if (chars.indexOf("a") > -1) mask += "abcdefghijklmnopqrstuvwxyz";
  if (chars.indexOf("A") > -1) mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  if (chars.indexOf("#") > -1) mask += "0123456789";
  let result = "";
  for (let i = length; i > 0; --i) {
    result += mask[Math.floor(Math.random() * mask.length)];
  }
  return result;
};

const getUniqueCodeRandomString = async (length, chars) => {
  let mask = "abcdefghijklmnopqrstuvwxyz0123456789";
  // if (chars.indexOf("a") > -1) mask += "abcdefghijklmnopqrstuvwxyz";
  // if (chars.indexOf("A") > -1) mask += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  // if (chars.indexOf("#") > -1) mask += "0123456789";
  let result = "";
  for (let i = length; i > 0; --i) {
    result += mask[Math.floor(Math.random() * mask.length)];
  }
  return result;
};

helperUtil.getDynamicUniqueCode = async (firstName, clientId) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const uniqueCodeLength = generalSettings.data?.uniqueCodeLength || 10;
  let dynamicUniqueCode = "",
    randomStringLength = 4;
  const sliceFirstName = firstName.slice(0, 4);
  const companyCode = generalSettings.data.siteTitle.slice(0, 4);

  const strLength = (sliceFirstName + companyCode).length;

  randomStringLength = uniqueCodeLength - strLength;

  const randomString = await getUniqueCodeRandomString(randomStringLength);
  dynamicUniqueCode = sliceFirstName + randomString + companyCode;
  return dynamicUniqueCode.toUpperCase();
};

// helperUtil.randomslice = async (ar, size) => {
//   const new_ar = [...ar];
//   new_ar.splice(Math.floor(Math.random() * ar.length), 1);
//   return ar.length <= size + 1 ? new_ar : helperUtil.randomslice(new_ar, size);
// };

helperUtil.randomslice = async (ar, size) => {
  // const randomsliceString = function async(data) {
  //   try {
  //     const new_Data = [...data];
  //     new_Data.splice(Math.floor(Math.random() * new_Data.length), 1);
  //     return new_Data;
  //   } catch (e) {
  //     const xxx = e.message;
  //   }
  // };
  try {
    const new_ar = [...ar];
    ar.forEach((element) => {
      // for (let i = 0; i < ar.length; i++) {
      if (size < new_ar.length) {
        // new_ar = await randomsliceString(new_ar);
        new_ar.splice(Math.floor(Math.random() * new_ar.length), 1);
      }
    });
    return new_ar;
  } catch (e) {
    return ar?.[0] || [];
  }
};

// END TRIP CALCULATION
helperUtil.calculateEstimateAmount = async (estimationData) => {
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);

  const couponCalc = function (data) {
    const couponMaxAmount =
      estimationData?.bookingData?.couponId?.data?.couponMaxAmount || 0;
    const isCouponCheckWithMaxTripAmount =
      estimationData?.bookingData?.couponId?.data
        ?.isCouponCheckWithMaxTripAmount || false;
    let amount =
      estimationData.bookingData.invoice.couponType === constantUtil.PERCENTAGE
        ? parseFloat(
            parseFloat(
              parseFloat(data.deliveryCharge) *
                ((estimationData.bookingData.invoice.couponValue || 0) / 100)
            ).toFixed(2)
          )
        : parseFloat(estimationData.bookingData.invoice.couponValue.toFixed(2));
    // if amount is more than specified amount so set max amount in coupon
    if (isCouponCheckWithMaxTripAmount) {
      if (
        // parseFloat(data.deliveryCharge) - parseFloat(data.amount) > couponMaxAmount
        parseFloat(data.deliveryCharge) >= couponMaxAmount
      ) {
        amount = 0;
      }
      //} else {
      //   if (amount > couponMaxAmount) {
      //     amount = couponMaxAmount;
      //   }
    } else {
      if (amount > couponMaxAmount) {
        amount = couponMaxAmount;
      }
    }
    return amount;
  };

  let serviceTax = 0,
    distanceFare = 0,
    timeFare = 0,
    peakFareCharge = 0,
    nightFareCharge = 0,
    multiStopCharge = 0,
    waitingCharge = 0,
    couponAmount = 0,
    siteCommissionWithoutTax = 0,
    siteCommission = 0,
    professionalCommision = 0,
    estimationAmount = 0,
    payableAmount = 0,
    professionalTolerenceAmount = 0,
    pendingPaymentAmount = 0,
    beforeRideWaitingCharge = 0,
    afterRideWaitingCharge = 0,
    //
    beforeRideWaitingMins = 0,
    beforeRideWaitingMinsWithGracePeriod = 0,
    afterRideWaitingMins = 0,
    afterRideWaitingMinsWithGracePeriod = 0,
    //------- InterCity Ride --------
    isInterCityRide = false,
    intercityPickupTimeFare = 0,
    intercityPickupDistanceFare = 0,
    intercityPickupCharge = 0,
    intercityBoundaryFare = 0,
    isEnableRoundTrip = false,
    intercityDropCharge = 0,
    intercityDropDistanceFare = 0,
    intercityDropTimeFare = 0,
    //-------------------------------
    surchargeFee = 0,
    deliveryCharge = 0,
    deliveryChargeWithCoupon = 0,
    travelCharge = 0,
    roundingAmount = 0,
    discountAmount = 0,
    assistanceCareAmount = 0,
    isMinimumChargeApplied = false;
  //---------------------------------------
  // const tollFareAmount = parseFloat(
  //   (estimationData?.bookingData?.invoice?.tollFareAmount || 0).toFixed(2)
  // );
  // const airportFareAmount = parseFloat(
  //   (estimationData?.bookingData?.invoice?.airportFareAmount || 0).toFixed(2)
  // );
  const tollFareAmount = parseFloat(
    (estimationData?.tollFareAmount || 0).toFixed(2)
  );
  const airportFareAmount = parseFloat(
    (estimationData?.airportFareAmount || 0).toFixed(2)
  );
  const isSurChargeEnable =
    estimationData?.bookingData?.invoice?.isSurChargeEnable || false;
  isInterCityRide =
    estimationData?.bookingData?.invoice?.isInterCityRide || false;
  const baseFare = parseFloat(
    estimationData?.bookingData?.invoice?.baseFare || 0
  );
  isEnableRoundTrip =
    estimationData?.bookingData?.invoice?.isEnableRoundTrip || false;
  const siteCommissionPercentage = parseFloat(
    (estimationData.bookingData.invoice.siteCommissionPercentage || 0) / 100
  );
  multiStopCharge =
    (await estimationData.bookingData.invoice?.multiStopChargeList
      ?.map((item) => item.amount)
      ?.reduce((prevAmt, currAmt) => prevAmt + currAmt, 0)) || 0;
  //#region For Share Ride Start
  const isShareRide = estimationData?.bookingData?.isShareRide || false;
  // const passengerCount = parseInt(
  //   estimationData?.bookingData?.passengerCount || 0
  // );
  // const sharePercentageUser1 = parseFloat(
  //   estimationData?.bookingData?.invoice?.sharePercentageUser1 || 0
  // );
  // const sharePercentageUser2 = parseFloat(
  //   estimationData?.bookingData?.invoice?.sharePercentageUser2 || 0
  // );
  // let sharePercentage = 0; // Default
  // if (
  //   generalSettings.data.isEnableShareRide &&
  //   isShareRide &&
  //   isInterCityRide === false
  // ) {
  //   sharePercentage =
  //     passengerCount === 1
  //       ? sharePercentageUser1
  //       : sharePercentageUser1 + sharePercentageUser2;
  // }
  //#endregion For Share Ride End
  //---------------------------------------

  // //#region InterCity Ride
  // if (isInterCityRide) {
  //   intercityPickupTimeFare =
  //     estimationData.bookingData?.invoice?.intercityPickupTimeFare || 0;
  //   intercityPickupDistanceFare =
  //     estimationData.bookingData?.invoice?.intercityPickupDistanceFare || 0;
  //   intercityBoundaryCharge =
  //     estimationData.bookingData?.invoice?.intercityBoundaryCharge || 0;
  // }
  // //#endregion InterCity Ride

  //#region For Fixed Payable Amount Ride/Booking Start
  if (estimationData.bookingData.invoice.isFixedPayableAmount) {
    travelCharge = estimationData.bookingData.invoice.travelCharge;
    payableAmount = estimationData.bookingData.invoice.payableAmount;
    estimationAmount = estimationData.bookingData.invoice.estimationAmount;
    professionalCommision =
      estimationData.bookingData.invoice.professionalCommision;
    siteCommission = estimationData.bookingData.invoice.siteCommission;
    siteCommissionWithoutTax =
      estimationData.bookingData.invoice.siteCommissionWithoutTax;
    return {
      "deliveryCharge": 0,
      "deliveryChargeWithCoupon": 0,
      "travelCharge": travelCharge,
      "serviceTax": 0,
      "timeFare": 0,
      "distanceFare": 0,
      "peakFareCharge": 0,
      "nightFareCharge": 0,
      "multiStopCharge": 0,
      "waitingCharge": 0,
      "beforeRideWaitingCharge": 0,
      "beforeRideWaitingMins": 0,
      "afterRideWaitingCharge": 0,
      "afterRideWaitingMins": 0,
      "tollFareAmount": 0,
      "airportFareAmount": 0,
      "couponAmount": 0,
      "siteCommissionWithoutTax": siteCommissionWithoutTax,
      "professionalCommision": parseFloat(professionalCommision),
      "siteCommission": siteCommission,
      "estimationAmount": parseFloat(estimationAmount),
      "payableAmount": payableAmount,
      "professionalTolerenceAmount": 0,
      "isInterCityRide": isInterCityRide,
      "intercityPickupTimeFare": 0,
      "intercityPickupDistanceFare": 0,
      "intercityBoundaryCharge": 0,
      "roundingAmount": 0,
      "discountAmount": discountAmount,
    };
  }
  //#endregion For Fixed Payable Amount Ride/Booking End
  //#region For Shar Ride Start
  if (isShareRide) {
    return {
      "deliveryCharge": estimationData.bookingData.invoice.deliveryCharge,
      "deliveryChargeWithCoupon":
        estimationData.bookingData.invoice.deliveryChargeWithCoupon,
      "travelCharge": estimationData.bookingData.invoice.travelCharge,
      "serviceTax": estimationData.bookingData.invoice.serviceTax || 0,
      "timeFare": estimationData.bookingData.invoice.timeFare || 0,
      "distanceFare": estimationData.bookingData.invoice.distanceFare || 0,
      "peakFareCharge": peakFareCharge,
      "nightFareCharge": nightFareCharge,
      "multiStopCharge": multiStopCharge,
      "waitingCharge": estimationData.bookingData.invoice.waitingCharge || 0,
      "beforeRideWaitingCharge":
        estimationData.bookingData.invoice.beforeRideWaitingCharge || 0,
      "beforeRideWaitingMins":
        estimationData.bookingData.invoice.beforeRideWaitingMins || 0,
      "afterRideWaitingCharge":
        estimationData.bookingData.invoice.afterRideWaitingCharge || 0,
      "afterRideWaitingMins":
        estimationData.bookingData.invoice.afterRideWaitingMins || 0,
      "tollFareAmount": tollFareAmount,
      "airportFareAmount": airportFareAmount,
      "couponAmount": estimationData.bookingData.invoice.couponAmount || 0,
      "siteCommissionWithoutTax":
        estimationData.bookingData.invoice.siteCommissionWithoutTax || 0,
      "professionalCommision":
        estimationData.bookingData.invoice.professionalCommision || 0,
      "siteCommission": estimationData.bookingData.invoice.siteCommission || 0,
      "estimationAmount":
        estimationData.bookingData.invoice.estimationAmount || 0,
      "payableAmount": estimationData.bookingData.invoice.payableAmount || 0,
      "professionalTolerenceAmount":
        estimationData.bookingData.invoice.professionalTolerenceAmount || 0,
      "isInterCityRide": estimationData.bookingData.invoice.isInterCityRide,
      "intercityPickupTimeFare":
        estimationData.bookingData.invoice.intercityPickupTimeFare || 0,
      "intercityPickupDistanceFare":
        estimationData.bookingData.invoice.intercityPickupDistanceFare || 0,
      "intercityBoundaryCharge":
        estimationData.bookingData.invoice.intercityBoundaryCharge || 0,
      "roundingAmount": estimationData.bookingData.invoice.roundingAmount || 0,
      "discountAmount": estimationData.bookingData.invoice.discountAmount || 0,
    };
  }
  //#endregion For Shar Ride End

  // if (
  //   estimationData?.bookingData?.invoice?.isSurChargeEnable ||
  //   (!!estimationData?.bookingData?.invoice?.isSurChargeEnable == false &&
  //     !!estimationData?.bookingData?.invoice?.surchargeFee === true)
  // ) {
  //   surchargeFee = parseFloat(
  //     estimationData?.bookingData?.invoice?.surchargeFee?.toFixed(2) || 0
  //   );
  // }
  if (isSurChargeEnable) {
    surchargeFee = parseFloat(
      (estimationData?.bookingData?.invoice?.surchargeFee || 0).toFixed(2)
    );
  }

  const bookingDate = new Date(estimationData.bookingData.bookingDate);
  const bookingNextDate = new Date(
    new Date(
      new Date(new Date().setDate(new Date().getDate() + 1)).setHours(
        new Date(estimationData.bookingData.bookingDate).getHours()
      )
    ).setMinutes(new Date(estimationData.bookingData.bookingDate).getMinutes())
  );
  //
  const regionalBookingDate = toRegionalUTC(
    new Date(estimationData.bookingData.bookingDate)
  );
  const dayMonthData = await getDayAndMonthNameOfDate(bookingDate, "UPPER");
  const specialDaysData = await storageUtil.read(
    constantUtil.CONST_SPECIALDAYS
  );
  const checkSpecialDayPeakHours = await specialDaysData.filter((e) => {
    if (
      // new Date(regionalBookingDate).setHours(0, 0, 0, 0) ===
      //   new Date(e.data.specialDays.date).setHours(0, 0, 0, 0) &&
      // e.data.specialDays.status === true &&
      // Date.parse(regionalBookingDate) >=
      //   Date.parse(e.data.specialDays.startTime) &&
      // Date.parse(regionalBookingDate) <= Date.parse(e.data.specialDays.endTime)

      // new Date(estimationData.bookingData.bookingDate).setHours(0, 0, 0, 0) ===
      //   new Date(e.data.specialDays.date).setHours(0, 0, 0, 0) &&
      estimationData.bookingData?.category?.toString() ==
        e.data.serviceAreaId &&
      new Date(bookingDate).toISOString().substring(0, 10) ===
        new Date(e.data.specialDays.date).toISOString().substring(0, 10) &&
      e.data.specialDays.status === true &&
      Date.parse(bookingDate) >=
        Date.parse(new Date(e.data.specialDays.startTime)) &&
      Date.parse(bookingDate) <=
        Date.parse(new Date(e.data.specialDays.endTime))
    )
      return e.data.specialDays;
  });
  //
  // const checkPeakTiming = (data) => {
  //   let peakHoursData = null,
  //     isApplicablePeakHours = false;
  //   try {
  //     if (data.status) {
  //       const checkWeeklyPeakHours =
  //         data.weekDays &&
  //         data.weekDays.filter((e) => {
  //           if (e.day === dayMonthData.dayName && e.status === true) return e;
  //         });
  //       if (checkSpecialDayPeakHours.length > 0) {
  //         peakHoursData = checkSpecialDayPeakHours[0].data.specialDays;
  //         if (
  //           Date.parse(bookingDate) >=
  //             Date.parse(new Date(peakHoursData.startTime)) &&
  //           Date.parse(bookingDate) <=
  //             Date.parse(new Date(peakHoursData.endTime))
  //         ) {
  //           isApplicablePeakHours = true;
  //         }
  //       } else if (checkWeeklyPeakHours.length > 0) {
  //         peakHoursData = checkWeeklyPeakHours[0];
  //         // }
  //         // if (peakHoursData) {
  //         let startTime =
  //           parseFloat(new Date(peakHoursData.startTime).getHours()) +
  //           parseFloat(new Date(peakHoursData.startTime).getMinutes() / 60);
  //         let endTime =
  //           parseFloat(new Date(peakHoursData.endTime).getHours()) +
  //           parseFloat(new Date(peakHoursData.endTime).getMinutes() / 60);
  //         const startDate = new Date().setDate(new Date().getDate());
  //         const endDate = new Date().setDate(new Date().getDate() + 1);
  //         if (startTime > endTime) {
  //           startTime = new Date(
  //             new Date(
  //               new Date(new Date(startDate)).setHours(
  //                 new Date(peakHoursData.startTime).getHours()
  //               )
  //             ).setMinutes(new Date(peakHoursData.startTime).getMinutes())
  //           );
  //           endTime = new Date(
  //             new Date(
  //               new Date(new Date(endDate)).setHours(
  //                 new Date(peakHoursData.endTime).getHours()
  //               )
  //             ).setMinutes(new Date(peakHoursData.endTime).getMinutes())
  //           );
  //           if (
  //             Date.parse(bookingDate) >= Date.parse(startTime) ||
  //             Date.parse(bookingNextDate) <= Date.parse(endTime)
  //           )
  //             isApplicablePeakHours = true;
  //         } else {
  //           startTime = new Date(
  //             new Date(
  //               new Date(new Date()).setHours(
  //                 new Date(peakHoursData.startTime).getHours()
  //               )
  //             ).setMinutes(new Date(peakHoursData.startTime).getMinutes())
  //           );
  //           endTime = new Date(
  //             new Date(
  //               new Date(new Date()).setHours(
  //                 new Date(peakHoursData.endTime).getHours()
  //               )
  //             ).setMinutes(new Date(peakHoursData.endTime).getMinutes())
  //           );
  //           if (
  //             Date.parse(bookingDate) >= Date.parse(startTime) &&
  //             Date.parse(bookingDate) <= Date.parse(endTime)
  //           )
  //             isApplicablePeakHours = true;
  //         }
  //       }
  //     }
  //   } catch (e) {
  //     isApplicablePeakHours = false;
  //   }
  //   return isApplicablePeakHours;
  // };

  // const checkNightTiming = (data) => {
  //   let isApplicableNightHours = false;
  //   try {
  //     if (data.status) {
  //       let startTime =
  //         parseFloat(new Date(data.startTime).getHours()) +
  //         parseFloat(new Date(data.startTime).getMinutes() / 60);
  //       let endTime =
  //         parseFloat(new Date(data.endTime).getHours()) +
  //         parseFloat(new Date(data.endTime).getMinutes() / 60);
  //       // const startDate = new Date().setDate(new Date().getDate());
  //       // const endDate = new Date().setDate(new Date().getDate() + 1);
  //       const startDate = new Date().setDate(new Date(bookingDate).getDate());
  //       const endDate = new Date().setDate(new Date(bookingDate).getDate() + 1);
  //       if (startTime > endTime) {
  //         const nextDay = new Date(
  //           new Date(bookingDate).setDate(new Date(bookingDate).getDate() + 1)
  //         );
  //         startTime = new Date(
  //           new Date(
  //             new Date(new Date(startDate)).setHours(
  //               new Date(data.startTime).getHours()
  //             )
  //           ).setMinutes(new Date(data.startTime).getMinutes())
  //         );
  //         endTime = new Date(
  //           new Date(
  //             new Date(new Date(endDate)).setHours(
  //               new Date(data.endTime).getHours()
  //             )
  //           ).setMinutes(new Date(data.endTime).getMinutes())
  //         );
  //         // if (
  //         //   Date.parse(bookingDate) >= Date.parse(startTime) ||
  //         //   Date.parse(bookingNextDate) <= Date.parse(endTime)
  //         // )
  //         if (
  //           Date.parse(bookingDate) >= Date.parse(startTime) ||
  //           Date.parse(nextDay) <= Date.parse(endTime)
  //         )
  //           isApplicableNightHours = true;
  //       } else {
  //         startTime = new Date(
  //           new Date(
  //             // new Date(new Date()).setHours(new Date(data.startTime).getHours())
  //             new Date(bookingDate).setHours(
  //               new Date(data.startTime).getHours()
  //             )
  //           ).setMinutes(new Date(data.startTime).getMinutes())
  //         );
  //         endTime = new Date(
  //           new Date(
  //             // new Date(new Date()).setHours(new Date(data.endTime).getHours())
  //             new Date(bookingDate).setHours(new Date(data.endTime).getHours())
  //           ).setMinutes(new Date(data.endTime).getMinutes())
  //         );
  //         if (
  //           Date.parse(bookingDate) >= Date.parse(startTime) &&
  //           Date.parse(bookingDate) <= Date.parse(endTime)
  //         )
  //           isApplicableNightHours = true;
  //       }
  //     }
  //   } catch (e) {
  //     isApplicableNightHours = false;
  //   }
  //   return isApplicableNightHours;
  // };
  // DISTANCE CALCULATION BY FORMULA
  // if (
  //   Math.abs(
  //     estimationData.bookingData.estimation.distance -
  //       estimationData.estimationDistance
  //   ) <= parseInt(generalSettings.data.approxCalculationDistance)
  // ) {
  //   distanceFare = estimationData.bookingData.invoice.distanceFare.toFixed(2)
  // } else {

  // }
  // -------- Assign Distance Value Start---------------
  const distanceTypeUnitValue =
    (
      estimationData.bookingData?.distanceUnit ?? constantUtil.KM
    ).toUpperCase() === constantUtil.KM
      ? constantUtil.KM_VALUE
      : constantUtil.MILES_VALUE;
  // -------- Assign Distance Value End---------------
  //#region Time Fare & Distance Fare
  // const distanceKey = formulaJSON["RIDE"]["DISTANCEFARE"];
  // const distanceValue = {
  //   "FAREPERMETER":
  //     estimationData.bookingData.invoice.farePerDistance /
  //     distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
  //   "TRAVELPERMETER": estimationData.estimationDistance,
  // };
  // distanceFare = await formula.run(distanceKey, distanceValue).toFixed(2);
  // // TIME CALCULATION BY FORMULA
  // // if (
  // //   Math.abs(
  // //     estimationData.bookingData.estimation.time - estimationData.estimationTime
  // //   ) <=
  // //   parseInt(generalSettings.data.approxCalculationTime) * 60
  // // ) {
  // //   timeFare = estimationData.bookingData.invoice.timeFare.toFixed(2)
  // // } else {

  // // }

  // const timeKey = formulaJSON["RIDE"]["TIMEFARE"];
  // const timeValue = {
  //   "FAREPERSEC": estimationData.bookingData.invoice.farePerMinute / 60,
  //   "TRAVELPERSEC": estimationData.estimationTime,
  // };
  // timeFare = await formula.run(timeKey, timeValue).toFixed(2);
  estimationData.bookingData.invoice["distanceTypeUnitValue"] =
    distanceTypeUnitValue;
  estimationData.bookingData.invoice["estimationDistance"] =
    estimationData.estimationDistance;
  estimationData.bookingData.invoice["estimationTime"] =
    estimationData.estimationTime;
  const distanceAndTimeFareData = calculateDistanceAndTimeFare({
    ...estimationData.bookingData.invoice,
  }); // To Pass Invoice Data
  // DISTANCE & TIME FARE
  distanceFare = distanceAndTimeFareData.distanceFare;
  timeFare = distanceAndTimeFareData.timeFare;
  //#endregion Time Fare & Distance Fare

  //#region intercity ride Fare
  if (
    estimationData.bookingData.serviceCategory ===
    constantUtil.CONST_INTERCITYRIDE.toLowerCase()
  ) {
    // DISTANCE CALCULATION BY FORMULA
    const intercityPickupFarePerDistance = parseFloat(
      estimationData.bookingData.invoice.intercityPickupFarePerDistance || 0
    );
    const pickupDistanceKey =
      formulaJSON["RIDE"]["INTERCITYPICKUPDISTANCEFARE"];
    const pickupDistanceValue = {
      "PICKUPFAREPERMETER":
        intercityPickupFarePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
      "PICKUPDISTANCEPERMETER": estimationData.calculatedPickupDistance || 0,
    };
    intercityPickupDistanceFare = parseFloat(
      formula.run(pickupDistanceKey, pickupDistanceValue)?.toFixed(2) || 0
    );

    // TIME CALCULATION BY FORMULA
    const intercityPickupFarePerMinites = parseFloat(
      estimationData.bookingData.invoice.intercityPickupFarePerMinites || 0
    );
    const pickupTimeKey = formulaJSON["RIDE"]["INTERCITYPICKUPTIMEFARE"];
    const pickupTimeValue = {
      "PICKUPFAREPERSEC": intercityPickupFarePerMinites / 60,
      "PICKUPTIMEPERSEC": estimationData.calculatedPickupTime || 0,
    };
    intercityPickupTimeFare = parseFloat(
      formula.run(pickupTimeKey, pickupTimeValue)?.toFixed(2) || 0
    );

    intercityPickupCharge = parseFloat(
      (intercityPickupDistanceFare + intercityPickupTimeFare).toFixed(2)
    );

    intercityBoundaryFare = parseFloat(
      estimationData?.bookingData?.invoice?.intercityBoundaryFare || 0
    );

    if (isEnableRoundTrip) {
      // // DISTANCE CALCULATION BY FORMULA
      // const intercityPickupFarePerDistance = parseFloat(
      //   estimationData.bookingData.invoice.intercityPickupFarePerDistance || 0
      // );
      const dropDistanceKey =
        formulaJSON["RIDE"]["INTERCITYPICKUPDISTANCEFARE"];
      const dropDistanceValue = {
        "PICKUPFAREPERMETER":
          intercityPickupFarePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
        "PICKUPDISTANCEPERMETER": estimationData.calculatedDropDistance || 0,
      };
      intercityDropDistanceFare = parseFloat(
        formula.run(dropDistanceKey, dropDistanceValue)?.toFixed(2) || 0
      );

      // // TIME CALCULATION BY FORMULA
      // const intercityPickupFarePerMinites = parseFloat(
      //   estimationData.bookingData.invoice.intercityPickupFarePerMinites || 0
      // );
      const dropTimeKey = formulaJSON["RIDE"]["INTERCITYPICKUPTIMEFARE"];
      const dropTimeValue = {
        "PICKUPFAREPERSEC": intercityPickupFarePerMinites / 60,
        "PICKUPTIMEPERSEC": estimationData.calculatedDropTime || 0,
      };
      intercityDropTimeFare = parseFloat(
        formula.run(dropTimeKey, dropTimeValue)?.toFixed(2) || 0
      );

      intercityDropCharge = parseFloat(
        (intercityDropDistanceFare + intercityDropTimeFare).toFixed(2)
      );
    }
  }
  //#endregion intercity ride Fare

  //#region  Waiting Charge Before & After Ride End
  //---------Before Ride Start ------
  if (estimationData.bookingData?.invoice?.isBeforeRideWaitingCharge) {
    beforeRideWaitingMinsWithGracePeriod = Math.ceil(
      parseInt(
        (new Date(estimationData.bookingData.activity.pickUpTime) -
          new Date(estimationData.bookingData.activity.arriveTime)) /
          1000
      ) / 60
    );
    beforeRideWaitingMins =
      beforeRideWaitingMinsWithGracePeriod -
      parseFloat(
        estimationData.bookingData.invoice.beforeRideWaitingGracePeriod
      );
    if (beforeRideWaitingMins > 0) {
      beforeRideWaitingCharge =
        beforeRideWaitingMins *
        parseFloat(
          estimationData.bookingData.invoice.beforeRideWaitingChargePerMin
        );
    }
  }
  //---------After Ride Start ------
  if (estimationData.bookingData?.invoice?.isAfterRideWaitingCharge) {
    afterRideWaitingMinsWithGracePeriod = parseFloat(
      estimationData.bookingData?.invoice
        ?.afterRideWaitingMinsWithGracePeriod || 0
    );
    afterRideWaitingMins =
      parseFloat(afterRideWaitingMinsWithGracePeriod) -
      parseFloat(
        estimationData.bookingData.invoice.afterRideWaitingGracePeriod
      );
    if (afterRideWaitingMins > 0) {
      afterRideWaitingCharge =
        afterRideWaitingMins *
        parseFloat(
          estimationData.bookingData.invoice.afterRideWaitingChargePerMin
        );
    }
  }
  //#endregion  Waiting Charge Before & After Ride End

  //#region Assistance Care
  let beforeRideAssistanceCareAmount = 0,
    afterRideAssistanceCareAmount = 0;
  const isEnableAssistanceCare =
    estimationData.bookingData?.invoice?.isEnableAssistanceCare || false;
  // Before Ride Assistance Care
  const beforeRideAssistanceCareGracePeriod =
    estimationData.bookingData?.invoice?.beforeRideAssistanceCareGracePeriod ||
    0;
  const beforeRideAssistanceCareNormalChargePerMin =
    estimationData.bookingData?.invoice
      ?.beforeRideAssistanceCareNormalChargePerMin || 0;
  const beforeRideAssistanceCareRevisedChargePerMin =
    estimationData.bookingData?.invoice
      ?.beforeRideAssistanceCareRevisedChargePerMin || 0;
  const beforeRideAssistanceCareNeededMins =
    estimationData.bookingData?.invoice?.beforeRideAssistanceCareNeededMins ||
    0;
  const beforeRideAssistanceCareProvidesMins =
    estimationData.beforeRideAssistanceCareProvidesMins ||
    beforeRideAssistanceCareNeededMins;
  // const beforeRideAssistanceCareProvidesMins = 10;
  const newBeforeRideAssistanceCareProvidesMins =
    beforeRideAssistanceCareProvidesMins - beforeRideAssistanceCareGracePeriod;
  if (isEnableAssistanceCare && newBeforeRideAssistanceCareProvidesMins > 0) {
    if (
      beforeRideAssistanceCareProvidesMins <= beforeRideAssistanceCareNeededMins
    ) {
      beforeRideAssistanceCareAmount =
        parseInt(newBeforeRideAssistanceCareProvidesMins) *
        parseFloat(beforeRideAssistanceCareNormalChargePerMin);
    } else {
      beforeRideAssistanceCareAmount =
        parseInt(newBeforeRideAssistanceCareProvidesMins) *
        parseFloat(beforeRideAssistanceCareRevisedChargePerMin);
    }
  }
  // After Ride Assistance Care
  const afterRideAssistanceCareGracePeriod =
    estimationData.bookingData?.invoice?.afterRideAssistanceCareGracePeriod ||
    0;
  const afterRideAssistanceCareNormalChargePerMin =
    estimationData.bookingData?.invoice
      ?.afterRideAssistanceCareNormalChargePerMin || 0;
  const afterRideAssistanceCareRevisedChargePerMin =
    estimationData.bookingData?.invoice
      ?.afterRideAssistanceCareRevisedChargePerMin || 0;
  const afterRideAssistanceCareNeededMins =
    estimationData.bookingData?.invoice?.afterRideAssistanceCareNeededMins || 0;
  const afterRideAssistanceCareProvidesMins =
    estimationData.afterRideAssistanceCareProvidesMins ||
    afterRideAssistanceCareNeededMins;
  // const afterRideAssistanceCareProvidesMins = 10;
  const newAfterRideAssistanceCareProvidesMins =
    afterRideAssistanceCareProvidesMins - afterRideAssistanceCareGracePeriod;
  if (isEnableAssistanceCare && newAfterRideAssistanceCareProvidesMins > 0) {
    if (
      afterRideAssistanceCareProvidesMins <= afterRideAssistanceCareNeededMins
    ) {
      afterRideAssistanceCareAmount =
        parseInt(newAfterRideAssistanceCareProvidesMins) *
        parseFloat(afterRideAssistanceCareNormalChargePerMin);
    } else {
      afterRideAssistanceCareAmount =
        parseInt(newAfterRideAssistanceCareProvidesMins) *
        parseFloat(afterRideAssistanceCareRevisedChargePerMin);
    }
  }
  // Assistance Care Total Amount
  assistanceCareAmount =
    beforeRideAssistanceCareAmount + afterRideAssistanceCareAmount;
  //#endregion Assistance Care
  if (
    Math.abs(
      estimationData.bookingData.estimation.distance -
        estimationData.estimationDistance
    ) <= parseInt(generalSettings.data.approxCalculationDistance) &&
    Math.abs(
      estimationData.bookingData.estimation.time - estimationData.estimationTime
    ) <=
      parseInt(generalSettings.data.approxCalculationTime) * 60
  ) {
    timeFare = estimationData.bookingData.invoice.timeFare.toFixed(2);
    distanceFare = estimationData.bookingData.invoice.distanceFare.toFixed(2);

    // //-------- Waiting Charge Before & After Ride Start ---------------
    // //---------Before Ride Start ------
    // if (estimationData.bookingData?.invoice?.isBeforeRideWaitingCharge) {
    //   beforeRideWaitingMinsWithGracePeriod = Math.ceil(
    //     parseInt(
    //       (new Date(estimationData.bookingData.activity.pickUpTime) -
    //         new Date(estimationData.bookingData.activity.arriveTime)) /
    //         1000
    //     ) / 60
    //   );
    //   beforeRideWaitingMins =
    //     beforeRideWaitingMinsWithGracePeriod -
    //     parseFloat(
    //       estimationData.bookingData.invoice.beforeRideWaitingGracePeriod
    //     );
    //   if (beforeRideWaitingMins > 0) {
    //     beforeRideWaitingCharge =
    //       beforeRideWaitingMins *
    //       parseFloat(
    //         estimationData.bookingData.invoice.beforeRideWaitingChargePerMin
    //       );
    //   }
    // }
    // //---------After Ride Start ------
    // if (estimationData.bookingData.invoice.isAfterRideWaitingCharge === true) {
    //   afterRideWaitingMinsWithGracePeriod = parseFloat(
    //     estimationData.bookingData?.invoice
    //       ?.afterRideWaitingMinsWithGracePeriod || 0
    //   );
    //   afterRideWaitingMins =
    //     parseFloat(afterRideWaitingMinsWithGracePeriod) -
    //     parseFloat(
    //       estimationData.bookingData.invoice.afterRideWaitingGracePeriod
    //     );
    //   if (afterRideWaitingMins > 0) {
    //     afterRideWaitingCharge =
    //       afterRideWaitingMins *
    //       parseFloat(
    //         estimationData.bookingData.invoice.afterRideWaitingChargePerMin
    //       );
    //   }
    // }
    // //-------- Waiting Charge Before & After Ride End ---------------

    // deliveryCharge = (
    //   estimationData.bookingData.invoice.travelCharge +
    //   baseFare +
    //   beforeRideWaitingCharge +
    //   afterRideWaitingCharge +
    //   intercityPickupCharge +
    //   intercityDropCharge
    // ).toFixed(2);
    deliveryCharge = (
      estimationData.bookingData.invoice.travelCharge + baseFare
    ).toFixed(2);

    // if (
    //   generalSettings.data.isEnableShareRide &&
    //   isShareRide &&
    //   isInterCityRide === false
    // ) {
    //   const shareRidedeliveryCharge = (
    //     (parseFloat(deliveryCharge) / 100) *
    //     sharePercentage
    //   ).toFixed(2);
    //   discountAmount = parseFloat(deliveryCharge) - shareRidedeliveryCharge;
    //   deliveryCharge = shareRidedeliveryCharge;
    // }

    deliveryCharge = (
      parseFloat(deliveryCharge) +
      beforeRideWaitingCharge +
      afterRideWaitingCharge +
      intercityPickupCharge +
      intercityDropCharge +
      multiStopCharge
    ).toFixed(2);

    if (
      estimationData.bookingData.invoice.minimumChargeStatus === true &&
      parseFloat(deliveryCharge) <
        estimationData.bookingData.invoice.minimumChargeAmount
    ) {
      deliveryCharge =
        estimationData.bookingData.invoice.minimumChargeAmount.toFixed(2);
      isMinimumChargeApplied = true;
    }

    // waitingCharge = (
    //   parseFloat(estimationData.bookingData.activity.waitingMins) *
    //   parseFloat(estimationData.bookingData.invoice.waitingChargePerMin)
    // ).toFixed(2);
    waitingCharge = 0;

    serviceTax = (
      parseFloat(deliveryCharge) *
      ((estimationData.bookingData.invoice.serviceTaxPercentage || 0) / 100)
    ).toFixed(2);

    siteCommissionWithoutTax = (
      parseFloat(deliveryCharge) * siteCommissionPercentage
    ).toFixed(2);
    pendingPaymentAmount =
      estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
    siteCommission = (
      parseFloat(siteCommissionWithoutTax) +
      parseFloat(serviceTax) +
      parseFloat(pendingPaymentAmount)
    ).toFixed(2);

    professionalCommision = (
      parseFloat(deliveryCharge) - //+
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) -
      parseFloat(siteCommissionWithoutTax)
    ).toFixed(2);

    estimationAmount = (
      parseFloat(deliveryCharge) +
      parseFloat(serviceTax) +
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount) +
      surchargeFee +
      parseFloat(pendingPaymentAmount) +
      parseFloat(intercityBoundaryFare) +
      parseFloat(assistanceCareAmount)
    ).toFixed(2);

    payableAmount = (
      parseFloat(deliveryCharge) +
      parseFloat(serviceTax) +
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount) +
      surchargeFee +
      parseFloat(pendingPaymentAmount) +
      parseFloat(intercityBoundaryFare) +
      parseFloat(assistanceCareAmount)
    ).toFixed(2);

    if (estimationData.bookingData.invoice.couponApplied === true) {
      couponAmount = couponCalc({
        "deliveryCharge": deliveryCharge,
      });
      deliveryChargeWithCoupon =
        parseFloat(deliveryCharge) - parseFloat(couponAmount);
      if (deliveryChargeWithCoupon < 0) {
        deliveryChargeWithCoupon = 0;
      }
      serviceTax = (
        parseFloat(deliveryChargeWithCoupon) *
        ((estimationData.bookingData.invoice.serviceTaxPercentage || 0) / 100)
      ).toFixed(2);

      if (estimationData.bookingData.invoice.isProfessionalTolerance === true) {
        siteCommissionWithoutTax = (
          parseFloat(deliveryChargeWithCoupon) * siteCommissionPercentage
        ).toFixed(2);
        pendingPaymentAmount =
          estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
        siteCommission = (
          parseFloat(siteCommissionWithoutTax) +
          parseFloat(serviceTax) +
          parseFloat(pendingPaymentAmount)
        ).toFixed(2);

        professionalCommision = (
          parseFloat(deliveryChargeWithCoupon) - //+
          // parseFloat(waitingCharge) +
          // parseFloat(beforeRideWaitingCharge) +
          // parseFloat(afterRideWaitingCharge) -
          parseFloat(siteCommissionWithoutTax)
        ).toFixed(2);
      } else {
        siteCommissionWithoutTax = (
          parseFloat(deliveryCharge) * siteCommissionPercentage
        ).toFixed(2);
        pendingPaymentAmount =
          estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
        siteCommission = (
          parseFloat(siteCommissionWithoutTax) +
          parseFloat(serviceTax) +
          parseFloat(pendingPaymentAmount)
        ).toFixed(2);

        professionalCommision = (
          parseFloat(deliveryCharge) - //+
          // parseFloat(waitingCharge) +
          // parseFloat(beforeRideWaitingCharge) +
          // parseFloat(afterRideWaitingCharge) -
          parseFloat(siteCommissionWithoutTax)
        ).toFixed(2);
      }

      payableAmount = (
        parseFloat(deliveryChargeWithCoupon) +
        parseFloat(serviceTax) +
        // parseFloat(waitingCharge) +
        // parseFloat(beforeRideWaitingCharge) +
        // parseFloat(afterRideWaitingCharge) +
        parseFloat(tollFareAmount) +
        parseFloat(airportFareAmount) +
        surchargeFee +
        parseFloat(pendingPaymentAmount) +
        parseFloat(intercityBoundaryFare) +
        parseFloat(assistanceCareAmount)
      ).toFixed(2);
    }

    // // for toll
    // if (generalSettings.data.isTollToDriver === true) {
    //   professionalCommision =
    //     parseFloat(professionalCommision) + parseFloat(tollFareAmount);
    // } else {
    //   siteCommission = parseFloat(siteCommission) + parseFloat(tollFareAmount);
    // }
    // //
    // if (professionalCommision > payableAmount) {
    //   professionalTolerenceAmount =
    //     parseFloat(professionalCommision) - parseFloat(payableAmount);
    // }
    travelCharge = parseFloat(
      estimationData?.bookingData?.invoice?.travelCharge || 0
    );
    const inputData = {
      "amount": payableAmount,
      "type": estimationData?.bookingData?.invoice?.roundingType,
      "factor": estimationData?.bookingData?.invoice?.roundingFactor,
    };
    roundingAmount = await roundingAmountCalculation(inputData);
    if (estimationData?.bookingData?.invoice?.roundingFactor > 0) {
      payableAmount = parseFloat(payableAmount) + parseFloat(roundingAmount);
    }
    // return {
    //   "deliveryCharge": parseFloat(deliveryCharge),
    //   "deliveryChargeWithCoupon": parseFloat(deliveryChargeWithCoupon || 0),
    //   "travelCharge": parseFloat(
    //     estimationData.bookingData.invoice.travelCharge
    //   ),
    //   "serviceTax": parseFloat(serviceTax),
    //   "timeFare": parseFloat(timeFare),
    //   "distanceFare": parseFloat(distanceFare),
    //   "waitingCharge": parseFloat(waitingCharge),
    //   "beforeRideWaitingCharge": parseFloat(beforeRideWaitingCharge),
    //   "beforeRideWaitingMins": beforeRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. beforeRideWaitingMins + beforeRideWaitingGracePeriod)
    //   "afterRideWaitingCharge": parseFloat(afterRideWaitingCharge),
    //   "afterRideWaitingMins": afterRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. afterRideWaitingMins + afterRideWaitingGracePeriod)
    //   "tollFareAmount": parseFloat(tollFareAmount),
    //   "couponAmount": parseFloat(couponAmount),
    //   "siteCommissionWithoutTax": parseFloat(siteCommissionWithoutTax),
    //   "professionalCommision": parseFloat(professionalCommision),
    //   "siteCommission": parseFloat(siteCommission),
    //   "estimationAmount": parseFloat(estimationAmount),
    //   "payableAmount": parseFloat(payableAmount),
    //   "professionalTolerenceAmount": parseFloat(professionalTolerenceAmount),
    //   "isInterCityRide": isInterCityRide,
    //   "intercityPickupTimeFare": parseFloat(intercityPickupTimeFare),
    //   "intercityPickupDistanceFare": parseFloat(intercityPickupDistanceFare),
    //   "intercityBoundaryCharge": parseFloat(intercityBoundaryCharge),
    // };
  } else {
    //#region peakFare & nightFare
    // const isPeakFareAvailable =
    //   estimationData?.bookingData?.category?.isPeakFareAvailable?.status ||
    //   false;
    // const isPeakFareTimeApplicable = checkPeakTiming(
    //   estimationData.bookingData.category.isPeakFareAvailable
    // );
    // const isNightFareAvailable =
    //   estimationData?.bookingData?.category?.isNightFareAvailable?.status ||
    //   false;
    // const isNightFareTimeApplicable = checkNightTiming(
    //   estimationData.bookingData.category.isNightFareAvailable
    // );
    //#endregion peakFare & nightFare
    const isPeakTiming = estimationData.bookingData.invoice.isPeakTiming;
    const isNightTiming = estimationData.bookingData.invoice.isNightTiming;
    const invoiceNightFare = estimationData.bookingData.invoice.nightFare;
    const invoicePeakFare = estimationData.bookingData.invoice.peakFare;
    let finalKey;
    const finalValue = {
      "BASEFARE": baseFare,
      "TOTALDISTANCEFARE": parseFloat(distanceFare),
      "TOTALTIMEFARE": parseFloat(timeFare),
      "PEAKFEE": 0,
      "NIGHTFEE": 0,
    };
    if (
      // isPeakFareAvailable &&
      // isNightFareAvailable &&
      // isPeakFareTimeApplicable &&
      // isNightFareTimeApplicable
      isPeakTiming &&
      isNightTiming
    ) {
      finalKey = formulaJSON["RIDE"]["PEAKANDNIGHTFARE"];
      finalValue["PEAKFEE"] = invoicePeakFare;
      finalValue["NIGHTFEE"] = invoiceNightFare;
      // } else if (isNightFareAvailable && isNightFareTimeApplicable) {
    } else if (isNightTiming) {
      finalKey = formulaJSON["RIDE"]["ONLYNIGHTGFARE"];
      finalValue["NIGHTFEE"] = invoiceNightFare;
    } else if (
      // (isPeakFareAvailable && isPeakFareTimeApplicable) ||
      // generalSettings.data.isEnableHeatmap
      isPeakTiming
    ) {
      finalKey = formulaJSON["RIDE"]["ONLYPEAKFARE"];
      finalValue["PEAKFEE"] = invoicePeakFare;
    } else {
      finalKey = formulaJSON["RIDE"]["BASICFARE"];
    }
    //#region peakFare & nightFare
    if (isPeakTiming) {
      const peakFareKey = formulaJSON["RIDE"]["PEAKFARECHARGE"];
      peakFareCharge = await formula.run(peakFareKey, finalValue);
    }
    if (isNightTiming) {
      const nightFareKey = formulaJSON["RIDE"]["NIGHTGFARECHARGE"];
      nightFareCharge = await formula.run(nightFareKey, finalValue);
    }
    //#endregion peakFare & nightFare

    // //-------- Waiting Charge Before & After Ride Start ---------------
    // //---------Before Ride Start ------
    // if (estimationData.bookingData?.invoice?.isBeforeRideWaitingCharge) {
    //   beforeRideWaitingMinsWithGracePeriod = Math.ceil(
    //     parseInt(
    //       (new Date(estimationData.bookingData.activity.pickUpTime) -
    //         new Date(estimationData.bookingData.activity.arriveTime)) /
    //         1000
    //     ) / 60
    //   );
    //   beforeRideWaitingMins =
    //     beforeRideWaitingMinsWithGracePeriod -
    //     parseFloat(
    //       estimationData.bookingData.invoice.beforeRideWaitingGracePeriod
    //     );
    //   if (beforeRideWaitingMins > 0) {
    //     beforeRideWaitingCharge =
    //       beforeRideWaitingMins *
    //       parseFloat(
    //         estimationData.bookingData.invoice.beforeRideWaitingChargePerMin
    //       );
    //   }
    // }
    // //---------After Ride Start ------
    // if (estimationData.bookingData.invoice.isAfterRideWaitingCharge === true) {
    //   afterRideWaitingMinsWithGracePeriod = parseFloat(
    //     estimationData.bookingData?.invoice
    //       ?.afterRideWaitingMinsWithGracePeriod || 0
    //   );
    //   afterRideWaitingMins =
    //     parseFloat(afterRideWaitingMinsWithGracePeriod) -
    //     parseFloat(
    //       estimationData.bookingData.invoice.afterRideWaitingGracePeriod
    //     );
    //   if (afterRideWaitingMins > 0) {
    //     afterRideWaitingCharge =
    //       afterRideWaitingMins *
    //       parseFloat(
    //         estimationData.bookingData.invoice.afterRideWaitingChargePerMin
    //       );
    //   }
    // }
    // //-------- Waiting Charge Before & After Ride End ---------------
    let deliveryChargeWithCoupon;
    let deliveryCharge = await formula.run(finalKey, finalValue);
    let valueCheck = false;
    // if (
    //   generalSettings.data.isEnableShareRide &&
    //   isShareRide &&
    //   isInterCityRide === false
    // ) {
    //   const shareRidedeliveryCharge =
    //     (parseFloat(deliveryCharge) / 100) * sharePercentage;
    //   discountAmount = parseFloat(deliveryCharge) - shareRidedeliveryCharge;
    //   deliveryCharge = shareRidedeliveryCharge;
    // }
    deliveryCharge =
      deliveryCharge +
      beforeRideWaitingCharge +
      afterRideWaitingCharge +
      // intercityBoundaryFare + //Boundary Base Fare
      intercityPickupCharge +
      intercityDropCharge +
      multiStopCharge;

    if (
      estimationData.bookingData.invoice.minimumChargeStatus === true &&
      parseFloat(deliveryCharge) <
        estimationData.bookingData.invoice.minimumChargeAmount
    ) {
      valueCheck = true;
      deliveryCharge =
        estimationData.bookingData.invoice.minimumChargeAmount.toFixed(2);
      isMinimumChargeApplied = true;
    }
    // waitingCharge = (
    //   parseFloat(estimationData.bookingData.activity.waitingMins) *
    //   parseFloat(estimationData.bookingData.invoice.waitingChargePerMin)
    // ).toFixed(2);
    waitingCharge = 0;

    const surchargeFee = parseFloat(
      estimationData?.bookingData?.invoice?.surchargeFee?.toFixed(2) || 0
    );

    serviceTax = (
      parseFloat(deliveryCharge) *
      ((estimationData.bookingData.invoice.serviceTaxPercentage || 0) / 100)
    ).toFixed(2);

    siteCommissionWithoutTax = (
      parseFloat(deliveryCharge) * siteCommissionPercentage
    ).toFixed(2);

    pendingPaymentAmount =
      estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
    siteCommission = (
      parseFloat(siteCommissionWithoutTax) +
      parseFloat(serviceTax) +
      parseFloat(pendingPaymentAmount)
    ).toFixed(2);

    professionalCommision = (
      parseFloat(deliveryCharge) - //+
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) -
      parseFloat(siteCommissionWithoutTax)
    ).toFixed(2);

    estimationAmount = (
      parseFloat(deliveryCharge) +
      parseFloat(serviceTax) +
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount) +
      parseFloat(surchargeFee) +
      parseFloat(pendingPaymentAmount) +
      parseFloat(intercityBoundaryFare) +
      parseFloat(assistanceCareAmount)
    ).toFixed(2);

    payableAmount = (
      parseFloat(deliveryCharge) +
      parseFloat(serviceTax) +
      // parseFloat(waitingCharge) +
      // parseFloat(beforeRideWaitingCharge) +
      // parseFloat(afterRideWaitingCharge) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount) +
      parseFloat(surchargeFee) +
      parseFloat(pendingPaymentAmount) +
      parseFloat(intercityBoundaryFare) +
      parseFloat(assistanceCareAmount)
    ).toFixed(2);

    if (estimationData.bookingData.invoice.couponApplied === true) {
      couponAmount = couponCalc({
        "deliveryCharge": deliveryCharge,
      });
      deliveryChargeWithCoupon = parseFloat(deliveryCharge) - couponAmount;
      if (deliveryChargeWithCoupon < 0) {
        deliveryChargeWithCoupon = 0;
      }
      serviceTax = (
        parseFloat(deliveryChargeWithCoupon) *
        ((estimationData.bookingData.invoice.serviceTaxPercentage || 0) / 100)
      ).toFixed(2);

      if (estimationData.bookingData.invoice.isProfessionalTolerance === true) {
        siteCommissionWithoutTax = (
          parseFloat(deliveryChargeWithCoupon) * siteCommissionPercentage
        ).toFixed(2);

        pendingPaymentAmount =
          estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
        siteCommission = (
          parseFloat(siteCommissionWithoutTax) +
          parseFloat(serviceTax) +
          parseFloat(pendingPaymentAmount)
        ).toFixed(2);

        professionalCommision = (
          parseFloat(deliveryChargeWithCoupon) - // +
          // parseFloat(waitingCharge) +
          // parseFloat(beforeRideWaitingCharge) +
          // parseFloat(afterRideWaitingCharge) -
          parseFloat(siteCommissionWithoutTax)
        ).toFixed(2);
      } else {
        siteCommissionWithoutTax = (
          parseFloat(deliveryCharge) * siteCommissionPercentage
        ).toFixed(2);

        pendingPaymentAmount =
          estimationData.bookingData.invoice.pendingPaymentAmount.toFixed(2);
        siteCommission = (
          parseFloat(siteCommissionWithoutTax) +
          parseFloat(serviceTax) +
          parseFloat(pendingPaymentAmount)
        ).toFixed(2);

        professionalCommision = (
          parseFloat(deliveryCharge) - //+
          // parseFloat(waitingCharge) +
          // parseFloat(beforeRideWaitingCharge) +
          // parseFloat(afterRideWaitingCharge) -
          parseFloat(siteCommissionWithoutTax)
        ).toFixed(2);
      }

      payableAmount = (
        parseFloat(deliveryChargeWithCoupon) +
        parseFloat(serviceTax) +
        // parseFloat(waitingCharge) +
        // parseFloat(beforeRideWaitingCharge) +
        // parseFloat(afterRideWaitingCharge) +
        parseFloat(tollFareAmount) +
        parseFloat(airportFareAmount) +
        parseFloat(surchargeFee) +
        parseFloat(pendingPaymentAmount) +
        parseFloat(intercityBoundaryFare) +
        parseFloat(assistanceCareAmount)
      ).toFixed(2);
    }
    // // toll
    // if (generalSettings.data.isTollToDriver === true) {
    //   professionalCommision =
    //     parseFloat(professionalCommision) + parseFloat(tollFareAmount);
    // } else {
    //   siteCommission = parseFloat(siteCommission) + parseFloat(tollFareAmount);
    // }
    // //
    // if (professionalCommision > payableAmount) {
    //   professionalTolerenceAmount =
    //     parseFloat(professionalCommision) - parseFloat(payableAmount);
    // }
    // if (isShareRide) {
    travelCharge =
      parseFloat(deliveryCharge) + parseFloat(discountAmount) - baseFare;
    // } else {
    //   travelCharge = parseFloat(deliveryCharge) - baseFare;
    // }
    const inputData = {
      "amount": payableAmount,
      "type": estimationData?.bookingData?.invoice?.roundingType,
      "factor": estimationData?.bookingData?.invoice?.roundingFactor,
    };
    roundingAmount = await roundingAmountCalculation(inputData);
    if (estimationData?.bookingData?.invoice?.roundingFactor > 0) {
      payableAmount = parseFloat(payableAmount) + parseFloat(roundingAmount);
    }
    // return {
    //   "deliveryCharge": parseFloat(deliveryCharge),
    //   "deliveryChargeWithCoupon": parseFloat(deliveryChargeWithCoupon),
    //   "travelCharge":
    //     parseFloat(deliveryCharge) -
    //     parseFloat(estimationData.bookingData.invoice.baseFare),
    //   "serviceTax": parseFloat(serviceTax),
    //   "timeFare": parseFloat(timeFare),
    //   "distanceFare": parseFloat(distanceFare),
    //   "waitingCharge": parseFloat(waitingCharge),
    //   "beforeRideWaitingCharge": parseFloat(beforeRideWaitingCharge),
    //   "beforeRideWaitingMins": beforeRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. beforeRideWaitingMins + beforeRideWaitingGracePeriod)
    //   "afterRideWaitingCharge": parseFloat(afterRideWaitingCharge),
    //   "afterRideWaitingMins": afterRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. afterRideWaitingMins + afterRideWaitingGracePeriod)
    //   "tollFareAmount": parseFloat(tollFareAmount),
    //   "couponAmount": parseFloat(couponAmount),
    //   "siteCommissionWithoutTax": parseFloat(siteCommissionWithoutTax),
    //   "professionalCommision": parseFloat(professionalCommision),
    //   "siteCommission": parseFloat(siteCommission),
    //   "estimationAmount": parseFloat(estimationAmount),
    //   "payableAmount": parseFloat(payableAmount),
    //   "professionalTolerenceAmount": parseFloat(professionalTolerenceAmount),
    //   "isInterCityRide": isInterCityRide,
    //   "intercityPickupTimeFare": parseFloat(intercityPickupTimeFare),
    //   "intercityPickupDistanceFare": parseFloat(intercityPickupDistanceFare),
    //   "intercityBoundaryCharge": parseFloat(intercityBoundaryCharge),
    // };
  }

  //#region professionalCommision
  // toll
  if (generalSettings.data.isTollToDriver === true) {
    professionalCommision =
      parseFloat(professionalCommision) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount);
  } else {
    siteCommission =
      parseFloat(siteCommission) +
      parseFloat(tollFareAmount) +
      parseFloat(airportFareAmount);
  }
  //For Professional Tolerance
  if (
    estimationData.bookingData.invoice.isProfessionalTolerance === false &&
    professionalCommision > payableAmount
  ) {
    professionalTolerenceAmount =
      parseFloat(professionalCommision) - parseFloat(payableAmount);
  }
  //For Professional Intercity BoundaryFare
  if (generalSettings.data.isEnableIntercityBoundaryFareToProfessional) {
    professionalCommision =
      parseFloat(professionalCommision) + parseFloat(intercityBoundaryFare);
  }
  // For Assistance Care Amount
  if (estimationData.bookingData?.invoice?.isEnableAssistanceCare) {
    professionalCommision =
      parseFloat(professionalCommision) + parseFloat(assistanceCareAmount);
  }
  //#endregion professionalCommision

  return {
    "isMinimumChargeApplied": isMinimumChargeApplied,
    "deliveryCharge": parseFloat(deliveryCharge),
    "deliveryChargeWithCoupon": parseFloat(deliveryChargeWithCoupon || 0),
    "travelCharge": travelCharge,
    "serviceTax": parseFloat(serviceTax),
    "timeFare": parseFloat(timeFare),
    "distanceFare": parseFloat(distanceFare),
    "peakFareCharge": parseFloat(peakFareCharge),
    "nightFareCharge": parseFloat(nightFareCharge),
    "multiStopCharge": parseFloat(multiStopCharge),
    "waitingCharge": parseFloat(waitingCharge),
    "beforeRideWaitingCharge": parseFloat(beforeRideWaitingCharge),
    "beforeRideWaitingMins": beforeRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. beforeRideWaitingMins + beforeRideWaitingGracePeriod)
    "afterRideWaitingCharge": parseFloat(afterRideWaitingCharge),
    "afterRideWaitingMins": afterRideWaitingMinsWithGracePeriod, // Including Grace Period, (i,e. afterRideWaitingMins + afterRideWaitingGracePeriod)
    "tollFareAmount": parseFloat(tollFareAmount),
    "airportFareAmount": parseFloat(airportFareAmount),
    "couponAmount": parseFloat(couponAmount),
    "siteCommissionWithoutTax": parseFloat(siteCommissionWithoutTax),
    "professionalCommision": parseFloat(professionalCommision),
    "siteCommission": parseFloat(siteCommission),
    "estimationAmount": parseFloat(estimationAmount),
    "payableAmount": parseFloat(payableAmount),
    "professionalTolerenceAmount": parseFloat(professionalTolerenceAmount),
    "isInterCityRide": isInterCityRide,
    "intercityPickupTimeFare": parseFloat(intercityPickupTimeFare),
    "intercityPickupDistanceFare": parseFloat(intercityPickupDistanceFare),
    "intercityBoundaryFare": parseFloat(intercityBoundaryFare),
    "intercityPickupCharge": parseFloat(intercityPickupCharge),
    "intercityDropCharge": parseFloat(intercityDropCharge),
    "roundingAmount": roundingAmount,
    "discountAmount": discountAmount,
    "beforeRideAssistanceCareAmount": beforeRideAssistanceCareAmount,
    "afterRideAssistanceCareAmount": afterRideAssistanceCareAmount,
    "assistanceCareAmount": assistanceCareAmount,
  };
  // }
};

//FINAL CALCULATION DATA
helperUtil.endTripCalculation = async (calculationData) => {
  const responseJson = {};
  responseJson["isMinimumChargeApplied"] =
    calculationData?.calculatedData?.isMinimumChargeApplied;
  responseJson["deliveryCharge"] = parseFloat(
    (calculationData?.calculatedData?.deliveryCharge || 0).toFixed(2)
  );
  responseJson["deliveryChargeWithCoupon"] = parseFloat(
    (calculationData?.calculatedData?.deliveryChargeWithCoupon || 0).toFixed(2)
  );
  responseJson["travelCharge"] = parseFloat(
    (calculationData?.calculatedData?.travelCharge || 0).toFixed(2)
  );
  responseJson["tollFareAmount"] = parseFloat(
    (calculationData?.calculatedData?.tollFareAmount || 0).toFixed(2)
  );
  responseJson["airportFareAmount"] = parseFloat(
    (calculationData?.calculatedData?.airportFareAmount || 0).toFixed(2)
  );
  responseJson["couponAmount"] = parseFloat(
    (calculationData?.calculatedData?.couponAmount || 0).toFixed(2)
  );
  responseJson["peakFareCharge"] = parseFloat(
    (calculationData?.calculatedData?.peakFareCharge || 0).toFixed(2)
  );
  responseJson["nightFareCharge"] = parseFloat(
    (calculationData?.calculatedData?.nightFareCharge || 0).toFixed(2)
  );
  responseJson["multiStopCharge"] = parseFloat(
    (calculationData?.calculatedData?.multiStopCharge || 0).toFixed(2)
  );
  responseJson["waitingCharge"] = parseFloat(
    (calculationData?.calculatedData?.waitingCharge || 0).toFixed(2)
  );
  //-------- Waiting Charge Before & After Ride Start ---------------
  responseJson["beforeRideWaitingCharge"] = parseFloat(
    (calculationData?.calculatedData?.beforeRideWaitingCharge || 0).toFixed(2)
  );
  responseJson["beforeRideWaitingMins"] = parseFloat(
    (calculationData?.calculatedData?.beforeRideWaitingMins || 0).toFixed(2)
  );
  responseJson["afterRideWaitingCharge"] = parseFloat(
    (calculationData?.calculatedData?.afterRideWaitingCharge || 0).toFixed(2)
  );
  responseJson["afterRideWaitingMins"] = parseFloat(
    (calculationData?.calculatedData?.afterRideWaitingMins || 0).toFixed(2)
  );
  //-------- Waiting Charge Before & After Ride End ---------------
  responseJson["surchargeFee"] = parseFloat(
    (calculationData?.calculatedData?.surchargeFee || 0).toFixed(2)
  );
  responseJson["distanceFare"] = parseFloat(
    (calculationData?.calculatedData?.distanceFare || 0).toFixed(2)
  );
  responseJson["timeFare"] = parseFloat(
    (calculationData?.calculatedData?.timeFare || 0).toFixed(2)
  );
  responseJson["serviceTax"] = parseFloat(
    (calculationData?.calculatedData?.serviceTax || 0).toFixed(2)
  );
  responseJson["siteCommissionWithoutTax"] = parseFloat(
    (calculationData?.calculatedData?.siteCommissionWithoutTax || 0).toFixed(2)
  );
  responseJson["siteCommission"] = parseFloat(
    (calculationData?.calculatedData?.siteCommission || 0).toFixed(2)
  );
  responseJson["professionalCommision"] = parseFloat(
    (calculationData?.calculatedData?.professionalCommision || 0).toFixed(2)
  );
  responseJson["estimationAmount"] = parseFloat(
    (calculationData?.calculatedData?.estimationAmount || 0).toFixed(2)
  );
  responseJson["payableAmount"] = parseFloat(
    (calculationData?.calculatedData?.payableAmount || 0).toFixed(2)
  );
  responseJson["roundingAmount"] = parseFloat(
    (calculationData?.calculatedData?.roundingAmount || 0).toFixed(2)
  );
  responseJson["discountAmount"] = parseFloat(
    (calculationData?.calculatedData?.discountAmount || 0).toFixed(2)
  );
  responseJson["professionalTolerenceAmount"] = parseFloat(
    (calculationData?.calculatedData?.professionalTolerenceAmount || 0).toFixed(
      2
    )
  );
  responseJson["intercityPickupTimeFare"] = parseFloat(
    (calculationData?.calculatedData?.intercityPickupTimeFare || 0).toFixed(2)
  );
  responseJson["intercityPickupDistanceFare"] = parseFloat(
    (calculationData?.calculatedData?.intercityPickupDistanceFare || 0).toFixed(
      2
    )
  );
  responseJson["intercityPickupCharge"] = parseFloat(
    (calculationData?.calculatedData?.intercityPickupCharge || 0).toFixed(2)
  );
  responseJson["intercityDropCharge"] = parseFloat(
    (calculationData?.calculatedData?.intercityDropCharge || 0).toFixed(2)
  );
  responseJson["beforeRideAssistanceCareAmount"] = parseFloat(
    (
      calculationData?.calculatedData?.beforeRideAssistanceCareAmount || 0
    ).toFixed(2)
  );
  responseJson["afterRideAssistanceCareAmount"] = parseFloat(
    (
      calculationData?.calculatedData?.afterRideAssistanceCareAmount || 0
    ).toFixed(2)
  );
  responseJson["assistanceCareAmount"] = parseFloat(
    (calculationData?.calculatedData?.assistanceCareAmount || 0).toFixed(2)
  );

  responseJson["amountInDriver"] = 0;
  responseJson["amountInSite"] = responseJson["payableAmount"];
  // if (calculationData.bookingData.payment.option === constantUtil.CASH) {
  if (calculationData.paymentOption === constantUtil.CASH) {
    responseJson["amountInSite"] = 0;
    responseJson["amountInDriver"] = responseJson["payableAmount"];
  }
  responseJson["paidStatus"] = true;

  return responseJson;
};

helperUtil.getStaticImage = async (directionsData) => {
  const zoomValue = directionsData.km >= 20000 ? 11 : 13;
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const staticImage =
    "https://maps.googleapis.com/maps/api/staticmap?zoom=" +
    zoomValue +
    "&size=670x334&format=jpg&maptype=roadmap&key=" +
    generalSettings.data.mapApi.api;
  const pickup =
    "&markers=color:0x00ff00|size:mid|label:P|" + directionsData.from;
  const drop = "&markers=color:0x0000ff|size:mid|label:D|" + directionsData.to;
  let imagePath = "";
  if (directionsData["waypoints"] !== "") {
    imagePath = "&path=color:0xff0000|weight:2" + directionsData["waypoints"];
  }
  return staticImage + pickup + drop + imagePath;
};

helperUtil.calculateWaitingMins = (ar) => {
  let totalMins = 0;
  if (ar && ar.length > 0)
    for (const e of ar) {
      totalMins += parseInt(
        (new Date(e.waitingStartTime) - new Date(e.waitingEndTime)) /
          (1000 * 60)
      );
    }
  return totalMins;
};
helperUtil._valueFinder = (obj, key) => {
  if (
    obj[key] &&
    !!obj[key] !== false &&
    typeof obj[key] !== "object" &&
    !Array.isArray(obj[key])
  ) {
    return obj[key];
  }
  // else
  for (const props in obj) {
    if (
      typeof obj[props] === "object" &&
      !Array.isArray(obj[props]) &&
      !!obj !== false
    ) {
      const value = helperUtil._valueFinder(obj[props], key);
      if (value) return value;
    }
  }
  return;
};
// bookingObject for ThirdParty booking Service

helperUtil.thirdPartyBookingObjet = (bookingObject) => {
  return {
    "_id": bookingObject._id.toString(),
    "bookingId": bookingObject.bookingId,
    "bookingTime": bookingObject.activity.bookingTime,
    "bookingFor": bookingObject.bookingFor,
    "bookinOtp": bookingObject.bookinOtp,
    "vehicle": {
      "vehicleCategoryId": bookingObject.vehicle.vehicleCategoryId,
    },
    "serviceCategoryId": bookingObject.category,
    "origin": bookingObject.origin,
    "destination": bookingObject.destination,
    "estimation": bookingObject.estimation,
    "currencySymbol": bookingObject.currencySymbol,
    "currencyCode": bookingObject.currencyCode,
    "invoice": {
      "baseFare": bookingObject.invoice.baseFare,
      "distanceFare": bookingObject.invoice.distanceFare,
      "timeFare": bookingObject.invoice.timeFare,
      "travelCharge": bookingObject.travelCharge,
      "serviceTax": bookingObject.invoice.serviceTax,
      "surchargeFee": bookingObject.invoice.surchargeFee,
      "professionalCommision": bookingObject.invoice.professionalCommision,
      "estimationAmount": bookingObject.invoice.estimationAmount,
    },
  };
};

// // Get Registration Document
// helperUtil.getRegistrationDocumentFromRedis = async (serviceType) => {
//   //#region Profile Documents
//   const profileDocumentList = await storageutil.read(
//     constantUtil.PROFILEDOCUMENTS
//   );
//   if (!profileDocumentList)
//     return {
//       "code": 0,
//       "data": {},
//       "message": "ERROR IN PROFILE DOCUMENTS",
//     };

//   const profileDocumentRideList = Object.keys(profileDocumentList)
//     .map((documentId) => ({
//       ...profileDocumentList[documentId],
//       // "id": documentId,
//     }))
//     .filter(function (el) {
//       const docsServiceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       if (el.status === constantUtil.ACTIVE && docsServiceType === serviceType)
//         return el;
//     });
//   //#endregion Profile Documents
//   const driverDocumentList = await storageutil.read(
//     constantUtil.DRIVERDOCUMENTS
//   );
//   if (!driverDocumentList)
//     return {
//       "code": 0,
//       "data": {},
//       "message": "ERROR IN DRIVER DOCUMENTS",
//     };
//   //#region Vehicle & Driver Documents
//   const driverDocumentRideList = await Object.keys(driverDocumentList)
//     .map((documentId) => ({
//       ...driverDocumentList[documentId],
//       // "id": documentId,
//     }))
//     .filter(function (el) {
//       const docsServiceType = el.docsServiceType || constantUtil.CONST_RIDE;
//       if (el.status === constantUtil.ACTIVE && docsServiceType === serviceType)
//         return el;
//     });
//   //#endregion Vehicle & Driver Documents
//   return {
//     "profileDocumentRideList": profileDocumentRideList,
//     "driverDocumentRideList": driverDocumentRideList,
//   };
// };

// Get Registration Document
helperUtil.getRegistrationDocumentFromRedisBasedOnServiceTypeId = async (
  serviceTypeId
) => {
  //#region Profile Documents
  const profileDocumentList = await storageutil.read(
    constantUtil.PROFILEDOCUMENTS
  );
  if (!profileDocumentList)
    return {
      "code": 0,
      "data": {},
      "message": "ERROR IN PROFILE DOCUMENTS",
    };

  const profileDocumentRideList = Object.keys(profileDocumentList)
    .map((documentId) => ({
      ...profileDocumentList[documentId],
      // "id": documentId,
    }))
    .filter(function (el) {
      if (
        el.status === constantUtil.ACTIVE &&
        el.docsServiceTypeId?.toString() === serviceTypeId?.toString()
      )
        return el;
    });
  //#endregion Profile Documents
  const driverDocumentList = await storageutil.read(
    constantUtil.DRIVERDOCUMENTS
  );
  if (!driverDocumentList)
    return {
      "code": 0,
      "data": {},
      "message": "ERROR IN DRIVER DOCUMENTS",
    };
  //#region Vehicle & Driver Documents
  const driverDocumentRideList = await Object.keys(driverDocumentList)
    .map((documentId) => ({
      ...driverDocumentList[documentId],
      // "id": documentId,
    }))
    .filter(function (el) {
      if (
        el.status === constantUtil.ACTIVE &&
        el.docsServiceTypeId?.toString() === serviceTypeId?.toString()
      )
        return el;
    });
  //#endregion Vehicle & Driver Documents
  return {
    "profileDocumentRideList": profileDocumentRideList,
    "driverDocumentRideList": driverDocumentRideList,
  };
};
module.exports = helperUtil;
