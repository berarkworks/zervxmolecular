const CryptoJS = require("crypto-js");
const LZString = require("lz-string");

const base64Encode = (rawString) => {
  const wordArray = CryptoJS.enc.Utf8.parse(rawString);
  return CryptoJS.enc.Base64.stringify(wordArray)
    .replace("=", "")
    .replace("=", "");
};

const base64Decode = (base64String) => {
  const wordArray = CryptoJS.enc.Base64.parse(base64String);
  return CryptoJS.enc.Utf8.stringify(wordArray);
};

const hasher = (secret) => {
  return CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(secret));
};

const secretKeyText = "my-32-character-ultra-secure-and-ultra-long-secret";
const secret = hasher(secretKeyText);

//#region LZ String compress/decompress

const lzCompress = (rawString) => {
  return LZString.compressToBase64(rawString);
};

const lzDecompress = (compressString) => {
  return LZString.decompressFromBase64(compressString);
};
//#endregion LZ String compress/decompress

module.exports = {
  "hashSecret": hasher,
  "jwtSign": (payload = {}) => {
    payload.createdAt = new Date().toISOString();
    const base64Payload = base64Encode(JSON.stringify(payload));
    const base64Signature = base64Encode(
      CryptoJS.HmacSHA256(base64Payload, secret)
    );
    return base64Encode(`${base64Signature}:${base64Payload}`);
  },

  "jwtVerify": (token = null) => {
    token = base64Decode(token);
    const [signature, payload] = token.split(":").filter((d) => d.length > 0);
    const base64Signature = base64Encode(CryptoJS.HmacSHA256(payload, secret));

    if (signature === base64Signature) {
      const _payload = JSON.parse(base64Decode(payload));

      return {
        "auth": true,
        "payload": _payload,
      };
    }
    return { "auth": false, "payload": null };
  },
  // "passwordEncode": (password) => {
  //   return CryptoJS.AES.encrypt(password, secret);
  // },
  "passwordEncode": (password) => {
    CryptoJS.AES.encrypt(password, secret);
  },
  // "passwordDecode": (encrypted) => CryptoJS.AES.decrypt(encrypted, secret),
  "passwordDecode": (encrypted) => {
    const bytes = CryptoJS.AES.decrypt(encrypted, secret);
    return bytes.toString(CryptoJS.enc.Utf8);
  },
  "cryptoAESEncode": (data) => {
    return CryptoJS.AES.encrypt(data, secretKeyText);
  },
  "cryptoAESDecode": (encrypted) => {
    const bytes = CryptoJS.AES.decrypt(encrypted, secretKeyText);
    return bytes.toString(CryptoJS.enc.Utf8);
  },
  "base64Encode": (rawString) => base64Encode(rawString),

  "base64Decode": (base64String) => base64Decode(base64String),

  //#region LZ String compress/decompress

  "lzStringEncode": (data) => lzCompress(JSON.stringify(data)),

  "lzStringDecode": (data) => lzDecompress(data),

  //#endregion LZ String compress/decompress
};
