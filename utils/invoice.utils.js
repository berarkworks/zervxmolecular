const formulaJSON = require("../data/formula.json");
const formula = require("formula");
const constantUtil = require("./constant.util");

const invoiceUtils = {};
invoiceUtils.calculateEstimationAmount = (data) => {
  const { vehicleCategoryData } = data;
  // DISTANCE CALCULATION BY FORMULA
  const distanceFare = formula.run(formulaJSON["RIDE"]["DISTANCEFARE"], {
    "FAREPERMETER":
      vehicleCategoryData.farePerDistance / data.distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile //=>this fare data converts according to meters
    "TRAVELPERMETER": data.estimationDistance, // => in  meters
  });

  // time fare CALCULATION BY FORMULA
  const timeFare = formula.run(formulaJSON["RIDE"]["TIMEFARE"], {
    "FAREPERSEC": vehicleCategoryData.farePerMinute / 60, //=>this fare data converts according to seconds
    "TRAVELPERSEC": data.estimationTime, // => in  seconds
  });

  //
  let finalKey;
  const finalValue = {
    "BASEFARE": vehicleCategoryData.baseFare,
    "TOTALDISTANCEFARE": parseFloat(distanceFare),
    "TOTALTIMEFARE": parseFloat(timeFare),
    "PEAKFEE": 0,
    "NIGHTFEE": 0,
  };
  //
  if (data.peakTiming && data.nightTiming) {
    finalKey = formulaJSON["RIDE"]["PEAKANDNIGHTFARE"];
    finalValue["PEAKFEE"] = vehicleCategoryData.peakFare;
    finalValue["NIGHTFEE"] = vehicleCategoryData.nightFare;
  } else if (data.peakTiming) {
    finalKey = formulaJSON["RIDE"]["ONLYPEAKFARE"];
    finalValue["PEAKFEE"] = vehicleCategoryData.peakFare;
  } else if (data.nightTiming) {
    finalKey = formulaJSON["RIDE"]["ONLYNIGHTGFARE"];
    finalValue["NIGHTFEE"] = vehicleCategoryData.nightFare;
  } else {
    finalKey = formulaJSON["RIDE"]["BASICFARE"];
  }

  let estimationAmount = parseFloat(formula.run(finalKey, finalValue));

  if (
    vehicleCategoryData.minimumCharge?.status &&
    parseFloat(estimationAmount) <
      (vehicleCategoryData.minimumCharge?.amount || 0)
  ) {
    estimationAmount = vehicleCategoryData.minimumCharge.amount;
  }
  //
  const serviceTax =
    (estimationAmount * vehicleCategoryData.serviceTaxPercentage) / 100;
  const siteCommission =
    (estimationAmount * vehicleCategoryData.siteCommission) / 100;
  //  below return function parsefolat two times because tofixed returns string
  return {
    "estimationAmount": parseFloat(
      parseFloat(estimationAmount + serviceTax).toFixed(2)
    ),
    "distanceFare": parseFloat(parseFloat(distanceFare).toFixed(2)),
    "timeFare": parseFloat(parseFloat(timeFare).toFixed(2)),
    "travelCharge": parseFloat(parseFloat(timeFare + distanceFare).toFixed(2)),
    "serviceTax": parseFloat(parseFloat(serviceTax).toFixed(2)),
    "siteCommission": parseFloat(parseFloat(siteCommission).toFixed(2)),
    "siteCommissionWithoutTax": parseFloat(parseFloat(serviceTax).toFixed(2)),
    "professionalCommision": parseFloat(parseFloat(siteCommission).toFixed(2)),
    "professionalCommisionWithoutTips": parseFloat(
      parseFloat(siteCommission).toFixed(2)
    ),
  };
};
invoiceUtils.calculateInvoiceforThirdPartyBookingService = function (data) {
  const { vehicleCategoryData } = data;
  const {
    estimationAmount,
    travelCharge,
    distanceFare,
    timeFare,
    serviceTax,
    siteCommission,
    siteCommissionWithoutTax,
    professionalCommision,
    professionalCommisionWithoutTips,
  } = this.calculateEstimationAmount(data);

  return {
    "baseFare": vehicleCategoryData.baseFare,
    "farePerDistance": vehicleCategoryData.farePerDistance,
    "farePerMinute": vehicleCategoryData.farePerMinute,
    "peakFare": vehicleCategoryData.peakFare,
    "nightFare": vehicleCategoryData.nightFare,
    // "sharePercent": vehicleCategoryData.sharePercent,
    "cancellationStatus": false, // cancellation key is changed for only cancellation applicable in cancelled a ride and also following cancellations
    "cancellationAmount": 0,
    "cancellationMinute": 0,
    "professionalCancellationStatus": false,
    "professionalCancellationAmount": 0,
    "professionalCancellationMinute": 0,
    "minimumChargeStatus": vehicleCategoryData?.minimumCharge?.status ?? false,
    "minimumChargeAmount": vehicleCategoryData?.minimumCharge?.status
      ? vehicleCategoryData?.minimumCharge?.amount ?? 0
      : 0,
    // Surcharge
    "surchargeFee": vehicleCategoryData.surchargeFee || 0,
    "isSurchargeEnable": vehicleCategoryData?.isSurchargeEnable ?? true,
    "surchargeTitle": vehicleCategoryData?.surchargeTitle ?? "Surcharge",
    //

    "waitingCharge": 0,
    "estimationAmount": estimationAmount,
    "travelCharge": travelCharge,
    "distanceFare": distanceFare,
    "timeFare": timeFare,
    "tipsAmount": 0,
    "serviceTaxPercentage": vehicleCategoryData.serviceTaxPercentage || 0,
    "serviceTax": serviceTax,
    "payableAmount": estimationAmount,
    // "siteCommissionPercentage": vehicleCategoryData.siteCommission || 0,
    "sitecommissionPercentage": vehicleCategoryData.siteCommission || 0, //from professional
    "siteCommission": siteCommission,
    "siteCommissionWithoutTax": siteCommissionWithoutTax,
    "professionalCommision": professionalCommision,
    "professionalCommisionWithoutTips": professionalCommisionWithoutTips,
    "amountInDriver": 0,
    "amountInSite": 0,
    "tollFareAmount": 0,
    "airportFareAmount": 0,
    "passedTollList": [],
    "totalTollPassed": 0,
    "professionalTolerenceAmount": 0,
    "pendingPaymentAmount": 0,
    "estimationPayableAmount": estimationAmount,
    "actualEstimationAmount": estimationAmount,
    "couponApplied": false,
    "couponValue": 0,
    "couponType": "",
    "couponAmount": 0,
    "isProfessionalTolerance": false,
    "isBeforeRideWaitingCharge":
      vehicleCategoryData?.waitingChargeDetails?.isBeforeRideWaitingCharge ||
      false,
    "beforeRideWaitingChargePerMin":
      vehicleCategoryData.waitingChargeDetails &&
      vehicleCategoryData.waitingChargeDetails.isBeforeRideWaitingCharge
        ? vehicleCategoryData.waitingChargeDetails.beforeRideWaitingChargePerMin
        : 0,
    "beforeRideWaitingMins": 0,
    "beforeRideWaitingGracePeriod":
      vehicleCategoryData?.waitingChargeDetails?.beforeRideWaitingGracePeriod ||
      0,
    "beforeRideWaitingCharge": 0,
    "isAfterRideWaitingCharge":
      vehicleCategoryData?.waitingChargeDetails?.isAfterRideWaitingCharge ||
      false,
    "afterRideWaitingChargePerMin":
      vehicleCategoryData.waitingChargeDetails &&
      vehicleCategoryData.waitingChargeDetails.isAfterRideWaitingCharge
        ? vehicleCategoryData.waitingChargeDetails.afterRideWaitingChargePerMin
        : 0,
    "afterRideWaitingMins": 0,
    "afterRideWaitingGracePeriod":
      vehicleCategoryData?.waitingChargeDetails?.afterRideWaitingGracePeriod ||
      0,
    "afterRideWaitingCharge": 0,
  };
};

invoiceUtils.checkPeakTiming = async (data) => {
  if (!data.status) {
    return false;
  }
  const bookingDate = new Date(data.bookingDate);
  const bookingNextDate = new Date(
    new Date(
      new Date(new Date().setDate(new Date().getDate() + 1)).setHours(
        new Date(data.bookingDate).getHours()
      )
    ).setMinutes(new Date(data.bookingDate).getMinutes())
  );
  const dayMonthData = await constantUtil.getDayAndMonthNameOfDate(
    bookingDate,
    "UPPER"
  );

  let startTime =
    parseFloat(new Date(data.startTime).getHours()) +
    parseFloat(new Date(data.startTime).getMinutes() / 60);
  let endTime =
    parseFloat(new Date(data.endTime).getHours()) +
    parseFloat(new Date(data.endTime).getMinutes() / 60);
  const startDate = new Date().setDate(new Date().getDate());
  const endDate = new Date().setDate(new Date().getDate() + 1);
  if (startTime > endTime) {
    startTime = new Date(
      new Date(
        new Date(new Date(startDate)).setHours(
          new Date(data.startTime).getHours()
        )
      ).setMinutes(new Date(data.startTime).getMinutes())
    );
    endTime = new Date(
      new Date(
        new Date(new Date(endDate)).setHours(new Date(data.endTime).getHours())
      ).setMinutes(new Date(data.endTime).getMinutes())
    );
    if (
      Date.parse(bookingDate) >= Date.parse(startTime) ||
      Date.parse(bookingNextDate) <= Date.parse(endTime)
    )
      return true;
  } else {
    startTime = new Date(
      new Date(
        new Date(new Date()).setHours(new Date(data.startTime).getHours())
      ).setMinutes(new Date(data.startTime).getMinutes())
    );
    endTime = new Date(
      new Date(
        new Date(new Date()).setHours(new Date(data.endTime).getHours())
      ).setMinutes(new Date(data.endTime).getMinutes())
    );
    if (
      Date.parse(bookingDate) >= Date.parse(startTime) &&
      Date.parse(bookingDate) <= Date.parse(endTime)
    )
      return true;
  }
  return false;
};

invoiceUtils.checkNightTiming = (data) => {
  if (!data.status) {
    return false;
  }
  const bookingDate = new Date(data.bookingDate);
  const bookingNextDate = new Date(
    new Date(
      new Date(new Date().setDate(new Date().getDate() + 1)).setHours(
        new Date(data.bookingDate).getHours()
      )
    ).setMinutes(new Date(data.bookingDate).getMinutes())
  );

  let startTime =
    parseFloat(new Date(data.startTime).getHours()) +
    parseFloat(new Date(data.startTime).getMinutes() / 60);
  let endTime =
    parseFloat(new Date(data.endTime).getHours()) +
    parseFloat(new Date(data.endTime).getMinutes() / 60);
  const startDate = new Date().setDate(new Date().getDate());
  const endDate = new Date().setDate(new Date().getDate() + 1);
  if (startTime > endTime) {
    startTime = new Date(
      new Date(
        new Date(new Date(startDate)).setHours(
          new Date(data.startTime).getHours()
        )
      ).setMinutes(new Date(data.startTime).getMinutes())
    );
    endTime = new Date(
      new Date(
        new Date(new Date(endDate)).setHours(new Date(data.endTime).getHours())
      ).setMinutes(new Date(data.endTime).getMinutes())
    );
    if (
      Date.parse(bookingDate) >= Date.parse(startTime) ||
      Date.parse(bookingNextDate) <= Date.parse(endTime)
    )
      return true;
  } else {
    startTime = new Date(
      new Date(
        new Date(new Date()).setHours(new Date(data.startTime).getHours())
      ).setMinutes(new Date(data.startTime).getMinutes())
    );
    endTime = new Date(
      new Date(
        new Date(new Date()).setHours(new Date(data.endTime).getHours())
      ).setMinutes(new Date(data.endTime).getMinutes())
    );
    if (
      Date.parse(bookingDate) >= Date.parse(startTime) &&
      Date.parse(bookingDate) <= Date.parse(endTime)
    )
      return true;
  }

  return false;
};
//-----------------------
module.exports = invoiceUtils;
