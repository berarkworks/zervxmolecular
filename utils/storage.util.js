const Redis = require("ioredis");
const redis = process.env.REDIS ? new Redis(process.env.REDIS) : new Redis();

const storageUtil = {};

storageUtil.read = async (key) => {
  if (typeof key === "string") {
    const jsonString = await redis.get(key);
    const parsedJson = JSON.parse(jsonString);
    return parsedJson;
  }
  return null;
};

storageUtil.write = async (key, json) => {
  if (typeof key === "string" && typeof json === "object") {
    const jsonString = JSON.stringify(json);
    await redis.set(key, jsonString);
    return json;
  }
  return null;
};

module.exports = storageUtil;
