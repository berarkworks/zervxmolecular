const { utc } = require("moment-timezone");
const { CONST_DECIMAL, CONST_INTEGER } = require("./constant.util");

const commonUtil = {};

commonUtil.getMinutesDiff = (greaterTime, smallTime) => {
  return Math.round(
    (((new Date(greaterTime) - new Date(smallTime)) % 86400000) % 3600000) /
      60000
  );
};
commonUtil.toUTC = (timestamp) => {
  return utc(timestamp).format();
};

commonUtil.getDaysToMilliSeconds = (daysCount) => {
  return (daysCount || 1) * (1000 * 60 * 60 * 24);
};

commonUtil.convertToFloat = (value) => {
  let responseValue = 0;
  try {
    responseValue = parseFloat(value || 0);
  } catch (e) {
    responseValue = 0;
  }
  return responseValue;
};

commonUtil.convertToInt = (value) => {
  let responseValue = 0;
  try {
    responseValue = parseInt(value || 0);
  } catch (e) {
    responseValue = 0;
  }
  return responseValue;
};

// commonUtil.roundToNearestAmount = async (amount, factor = 0) => {
//   const type = "integer";
//   let result = amount,
//     quotient = 0;

//   if (type === CONST_DECIMAL) {
//     if (factor > 0) {
//       quotient = amount / factor;
//       // console.log(quotient);
//       result = Math.round(quotient) * factor;
//       if (result < amount) {
//         // console.log(res + "__" + amount + "_" + factor);
//         result = Math.round(result + factor);
//       }
//     }
//   } else {
//     if (factor < 0) {
//       quotient = amount / factor;
//       result = Math.round(quotient) * factor;
//     }
//   }
//   // console.log(quotient + "_|_" + res);
//   return result;
// };

commonUtil.roundingAmountCalculation = async (inputData) => {
  const type = inputData?.type?.toUpperCase() || CONST_INTEGER,
    amount = parseFloat(inputData.amount);
  //
  let calculatedAmount = parseFloat(inputData.amount),
    quotient = 0,
    result = 0;

  // if (type === CONST_INTEGER) {
  if (inputData.factor > 0) {
    quotient = amount / inputData.factor;
    // console.log(quotient);
    calculatedAmount = Math.round(quotient) * inputData.factor;
    if (calculatedAmount < amount) {
      // console.log(res + "__" + amount + "_" + factor);
      calculatedAmount = Math.round(calculatedAmount + inputData.factor);
    }
  }
  // } else if (type === CONST_DECIMAL) {
  //   if (inputData.factor > 0) {
  //     // quotient = amount / inputData.factor;
  //     // calculatedAmount = Math.round(quotient) * inputData.factor;
  //     calculatedAmount =
  //       Math.round(amount + "e" + inputData.factor) + "e-" + inputData.factor;
  //   }
  // }
  // console.log(quotient + "_|_" + res);
  result = calculatedAmount - amount;
  return result;
};

commonUtil.initialCaps = (value) => {
  return value.charAt(0).toUpperCase() + value.slice(1);
};

commonUtil.convertToBoolean = (value) => {
  let responseValue = 0;
  try {
    responseValue = parseInt(value || 0);
  } catch (e) {
    responseValue = 0;
  }
  return responseValue;
};

commonUtil.setDayStartHours = (value) => {
  let responseValue = new Date(new Date().setHours(0, 0, 0, 0));
  try {
    responseValue = new Date(new Date(value).setHours(0, 0, 0, 0));
  } catch (e) {
    responseValue = new Date(new Date().setHours(0, 0, 0, 0));
  }
  return responseValue;
};

commonUtil.setDayEndHours = (value) => {
  let responseValue = new Date(new Date().setHours(23, 59, 59, 999));
  try {
    responseValue = new Date(new Date(value).setHours(23, 59, 59, 999));
  } catch (e) {
    responseValue = new Date(new Date().setHours(23, 59, 59, 999));
  }
  return responseValue;
};

commonUtil.toRegionalUTC = (date) => {
  let newLocalUTCDate = date;
  try {
    newLocalUTCDate = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).toISOString();
  } catch (e) {
    newLocalUTCDate = new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60000
    ).toISOString();
  }
  return newLocalUTCDate;
};

commonUtil.toRegionalUTCDayStartHours = (date) => {
  let newLocalUTCDate = date;
  try {
    newLocalUTCDate = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).toISOString();
  } catch (e) {
    newLocalUTCDate = new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60000
    ).toISOString();
  }
  return new Date(newLocalUTCDate.substring(0, 10));
};

// commonUtil.addDaysToRegionalUTC = (date, daysCount = 0) => {
//   let newLocalUTCDate = date;
//   try {
//     // new Date(
//     //   new Date(date.getTime() - date.getTimezoneOffset() * 60000) +
//     //     parseInt(daysCount) * 60 * 60 * 24 * 1000
//     // );
//     newLocalUTCDate = new Date(
//       new Date(date.getTime() - date.getTimezoneOffset() * 60000).valueOf() +
//         daysCount * 60 * 60 * 24 * 1000
//     )
//       .toString()
//       .substring(0, 10);

//     // new Date(
//     //   date.getTime() - date.getTimezoneOffset() * 60000
//     // ).toISOString();
//     newLocalUTCDate = new Date(Date.now() + daysCount * 60 * 60 * 24 * 1000);
//     newLocalUTCDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000);
//   } catch (e) {
//     newLocalUTCDate = new Date(
//       new Date(date.getTime() - date.getTimezoneOffset() * 60000).valueOf() +
//         daysCount * 60 * 60 * 24 * 1000
//     )
//       .toString()
//       .substring(4, 15);
//   }
//   // return new Date(newLocalUTCDate.substring(0, 10));
//   return new Date(newLocalUTCDate);
// };

commonUtil.addDaysToRegionalUTC = (date, daysCount = 0) => {
  let newLocalUTCDate = date;
  try {
    newLocalUTCDate = new Date(
      new Date(date.getTime() - date.getTimezoneOffset() * 60000).valueOf() +
        daysCount * 60 * 60 * 24 * 1000
    ).toISOString();
  } catch (e) {
    newLocalUTCDate = new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60000
    ).toISOString();
  }

  return new Date(new Date(newLocalUTCDate.substring(0, 10)).toISOString());
};
// commonUtil.toRegionalUTCDayStartHours = (date) => {
//   let newLocalUTCDate = date;
//   try {
//     newLocalUTCDate = new Date(
//       date.getTime() - date.getTimezoneOffset() * 60000
//     ).setHours(0, 0, 0, 0);
//     newLocalUTCDate = new Date(newLocalUTCDate).toISOString();
//   } catch (e) {
//     newLocalUTCDate = new Date(
//       new Date().getTime() - new Date().getTimezoneOffset() * 60000
//     ).setHours(0, 0, 0, 0);
//     newLocalUTCDate = new Date(newLocalUTCDate).toISOString();
//   }
//   return newLocalUTCDate;
// };

commonUtil.toRegionalUTCDayEnd = (date) => {
  let newLocalUTCDate = date;
  try {
    newLocalUTCDate = new Date(
      date.getTime() - date.getTimezoneOffset() * 60000
    ).setHours(23, 59, 59, 999);
    newLocalUTCDate = newLocalUTCDate.toISOString();
  } catch (e) {
    newLocalUTCDate = new Date(
      new Date().getTime() - new Date().getTimezoneOffset() * 60000
    ).setHours(23, 59, 59, 999);
    newLocalUTCDate = newLocalUTCDate.toISOString();
  }
  return newLocalUTCDate;
};

commonUtil.getDbQueryString = (inputQuery) => {
  return JSON.stringify(inputQuery);
};

commonUtil.trimPrefixZero = (phoneNumber) => {
  if (phoneNumber.charAt(0) === "0") {
    phoneNumber = phoneNumber.substring(1);
  }
  return phoneNumber;
};

commonUtil.getDayAndMonthNameOfDate = async (date, type) => {
  const dayNameList = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const monthNameList = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const dayName = dayNameList[date.getDay()];
  const monthName = monthNameList[date.getMonth()];
  if (type === "UPPER" || type === "upper") {
    return {
      "dayName": dayName.toUpperCase(),
      "monthName": monthName.toUpperCase(),
    };
  } else if (type === "LOWER" || type === "lower") {
    return {
      "dayName": dayName.toLowerCase(),
      "monthName": monthName.toLowerCase(),
    };
  } else {
    return { "dayName": dayName, "monthName": monthName };
  }
};

module.exports = commonUtil;
