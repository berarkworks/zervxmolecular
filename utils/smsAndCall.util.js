const axios = require("axios");
const { MoleculerError } = require("moleculer").Errors;
const moment = require("moment");

const notifyMessage = require("../mixins/notifyMessage.mixin");
const constantUtil = require("../utils/constant.util");
const storageUtil = require("../utils/storage.util");
const { initialCaps } = require("../utils/common.util");

const smsAndCallUtil = {};

//----------------- SMS Alert Start ------------
smsAndCallUtil.sendSMSAlertByRideId = async function (context) {
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const notifySMSSetting = await storageUtil.read(
    constantUtil.NOTIFYSMSSETTING
  );
  const notifyMsg = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  let professionalName = "",
    userName = "",
    bookingDate = "",
    message = "",
    pickupAddress = "",
    dropAddress = "",
    cancelReason = "",
    url = "",
    headers = {},
    config = {},
    jsonInputData = {};
  try {
    if (
      smsSetting.data.mode === constantUtil.PRODUCTION &&
      notifySMSSetting.data?.smsNotificationsEnable
    ) {
      //Get data from Redis
      const notifySMSTemplate = await storageUtil.read(
        constantUtil.NOTIFYSMSTEMPLATE
      );
      const generalSettings = await storageUtil.read(
        constantUtil.GENERALSETTING
      );
      //
      let notifySMSData = null;
      Object.values(notifySMSTemplate)
        .filter((option) => option.isActive === true)
        .forEach((option) => {
          if (
            option.data.notifyType === context.params.notifyType &&
            option.data.bookingFrom === context.params.bookingFrom
          ) {
            notifySMSData = option;
          }
        });

      if (notifySMSData?.data?.smsNotificationsEnable) {
        //  Booking data
        let bookingDetails = await this.broker.emit(
          "booking.getBookingDetailsById",
          {
            "bookingId": context.params?.rideId,
          }
        );
        bookingDetails = bookingDetails && bookingDetails[0];
        if (!bookingDetails) {
          throw new MoleculerError(notifyMsg.INVALID_BOOKING);
        }
        const formUrlEncoded = (inputData) =>
          Object.keys(inputData).reduce(
            (queryParmaData, item) =>
              queryParmaData +
              `&${item}=${encodeURIComponent(inputData[item])}`,
            ""
          );
        //#region Message Start
        professionalName =
          bookingDetails?.professional?.firstName?.trim() || "";
        userName = bookingDetails?.user?.firstName?.trim() || "";
        // bookingDate = bookingDetails.bookingDate.toLocaleString("en-GB", {
        //   "hour12": false,
        // });
        bookingDate = bookingDetails.bookingDate.toLocaleDateString(
          constantUtil.DEFAULT_LANGUAGE,
          {
            "year": "numeric",
            "month": "short",
            "day": "numeric",
            "hour": "numeric",
            "minute": "2-digit",
          }
        );
        pickupAddress = bookingDetails?.origin?.fullAddress?.trim() || "";
        dropAddress = bookingDetails?.destination?.fullAddress?.trim() || "";
        cancelReason = bookingDetails?.cancellationReason || "";

        // switch (context.params.notifyType) {
        //   case constantUtil.ACCEPTED:
        //     if (!smsSetting?.data?.privileges?.isSendToAcceptedBooking) {
        //       throw new MoleculerError(notifyMsg.INVALID_ACCESS);
        //     } else {
        //       message = smsSetting.data.notifyMessageAccepted;
        //     }
        //     break;

        //   case constantUtil.ARRIVED:
        //     if (!smsSetting?.data?.privileges?.isSendToArrivedBooking) {
        //       throw new MoleculerError(notifyMsg.INVALID_ACCESS);
        //     } else {
        //       message = smsSetting.data.notifyMessageArrived;
        //     }
        //     break;

        //   case constantUtil.STARTED:
        //     if (!smsSetting?.data?.privileges?.isSendToStartedBooking) {
        //       throw new MoleculerError(notifyMsg.INVALID_ACCESS);
        //     } else {
        //       message = smsSetting.data.notifyMessageStarted;
        //     }
        //     break;

        //   case constantUtil.ENDED:
        //     if (!smsSetting?.data?.privileges?.isSendToEndedBooking) {
        //       throw new MoleculerError(notifyMsg.INVALID_ACCESS);
        //     } else {
        //       message = smsSetting.data.notifyMessageEnded;
        //     }
        //     break;
        // }
        message = notifySMSData.data.notifyMessage;
        //----------- Message Start ------------------
        message = message.replace(
          /{{professionalName}}/g,
          initialCaps(professionalName)
        );
        message = message.replace(/{{userName}}/g, initialCaps(userName));
        message = message.replace(
          /{{plateNumber}}/g,
          (
            bookingDetails?.professional?.vehicles[0]?.plateNumber || ""
          ).toUpperCase()
        );
        message = message.replace(
          /{{carModel}}/g,
          initialCaps(bookingDetails?.professional?.vehicles[0]?.maker || "") +
            " " +
            initialCaps(bookingDetails?.professional?.vehicles[0]?.model || "")
        );
        message = message.replace(
          /{{carColor}}/g,
          initialCaps(bookingDetails?.professional?.vehicles[0]?.color || "")
        );
        message = message.replace(
          /{{bookingOTP}}/g,
          bookingDetails?.bookingOTP?.toString()
        );
        message = message.replace(
          /{{phoneNumber}}/g,
          bookingDetails.professional.phone.code.toString() +
            bookingDetails.professional.phone.number.toString()
        );
        message = message.replace(
          /{{trackLink}}/g,
          `${generalSettings.data.redirectUrls.trackBooking}${bookingDetails.bookingId}`
        );
        message = message.replace(/{{bookingDate}}/g, bookingDate);
        message = message.replace(/{{pickupAddress}}/g, pickupAddress);
        message = message.replace(/{{dropAddress}}/g, dropAddress);
        message = message.replace(/{{cancelReason}}/g, cancelReason);
        //#endregion Message End
        let phoneCode = "",
          phoneNumber = "";

        if (context.params.notifyFor === constantUtil.PROFESSIONAL) {
          phoneCode = bookingDetails.professional.phone.code.toString();
          phoneNumber = bookingDetails.professional.phone.number.toString();
        } else if (context.params.notifyFor === constantUtil.PACKAGERECEIVER) {
          phoneCode = "";
          phoneNumber = bookingDetails.packageData.recipientNo.toString();
          // phoneCode = bookingDetails.packageData.recipientPhoneCode.toString();
          // phoneNumber = bookingDetails.packageData.recipientPhoneNo.toString();
        } else {
          phoneCode = bookingDetails.bookingFor.phoneCode.toString();
          phoneNumber = bookingDetails.bookingFor.phoneNumber.toString();
        }
        switch (notifySMSSetting.data.smsType) {
          case constantUtil.AFRO:
            jsonInputData = {
              "from": notifySMSSetting.data.senderID,
              "sender": notifySMSSetting.data.senderName,
              "to": phoneCode + phoneNumber,
              // "+251911237975",
              "message": message,
            };
            url = "https://api.afromessage.com/api/send";
            headers = {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + notifySMSSetting.data.accessToken,
            };
            break;

          case constantUtil.TWILIO:
            await this.broker.emit("admin.sendSMS", {
              "phoneNumber": `${phoneCode}${phoneNumber}`,
              "message": message,
              "notifyType": context.params.notifyType,
              "otpReceiverAppName": constantUtil.SMS,
            });
            break;

          case constantUtil.SENDIFY:
            headers = {
              "Content-Type": "application/x-www-form-urlencoded",
            };
            //#region Send SMS Using WhatsUp
            if (notifySMSSetting.data.isEnableWhatsupOTP) {
              jsonInputData = {
                "secret": notifySMSSetting.data.authToken,
                "type": "whatsapp",
                "message": message,
                "phone": phoneCode + phoneNumber,
                "expire": "300",
                "account": notifySMSSetting.data.accountSid,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/otp", // OTP
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using WhatsUp
            //#region Send SMS Using Device Sim Network
            if (notifySMSSetting.data.isEnableDeviceNetworkOTP) {
              jsonInputData = {
                "secret": notifySMSSetting.data.authToken,
                "type": "sms",
                "message": message,
                "phone": context.params.phoneNumber,
                "expire": "300",
                "mode": "devices",
                "priority": 1,
                "sim": notifySMSSetting.data.senderID,
                "device": notifySMSSetting.data.accountNumber,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/sms", // SMS
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using Device Sim Network
            //#region Send SMS Using Gatway
            if (notifySMSSetting.data.isEnableSMSGatwayOTP) {
              jsonInputData = {
                "secret": notifySMSSetting.data.authToken,
                "type": "sms",
                "message": message,
                "phone": phoneCode + phoneNumber,
                "expire": "300",
                "mode": "credits",
                "gateway": notifySMSSetting.data.accessToken,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/sms", // SMS
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using Gatway
            break;
        }

        switch (notifySMSSetting.data.smsType) {
          case constantUtil.AFRO:
            config = {
              "method": "post",
              "url": url,
              "headers": headers,
              "data": jsonInputData,
            };
            try {
              const response = await axios(config);
              return response.data;
            } catch (e) {
              return "error";
            }
            break;
        }
      }
    }
  } catch (e) {
    return constantUtil.FAILED;
  }
  return constantUtil.SUCCESS;
};
//----------------- SMS Alert End ------------
smsAndCallUtil.sendWhatsupAlert = async function (context) {
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  const whatsupData = await storageUtil.read(constantUtil.WHATSUPSETTING);
  const notifySMSSetting = await storageUtil.read(
    constantUtil.NOTIFYSMSSETTING
  );
  const notifyMsg = await notifyMessage.setNotifyLanguage(
    context.meta?.userDetails?.languageCode
  );
  let professionalName = "",
    userName = "",
    bookingDate = "",
    message = "",
    pickupAddress = "",
    dropAddress = "",
    cancelReason = "",
    url = "",
    headers = {},
    config = {},
    jsonInputData = {},
    phoneCode = "",
    phoneNumber = "";
  try {
    if (context.params.alertType === constantUtil.SMS) {
      if (
        smsSetting.data.mode === constantUtil.PRODUCTION &&
        notifySMSSetting.data?.smsNotificationsEnable
      ) {
        //Get data from Redis
        const notifySMSTemplate = await storageUtil.read(
          constantUtil.NOTIFYSMSTEMPLATE
        );
        const generalSettings = await storageUtil.read(
          constantUtil.GENERALSETTING
        );
        //
        let notifySMSData = null;
        Object.values(notifySMSTemplate)
          .filter((option) => option.isActive === true)
          .forEach((option) => {
            if (
              option.data.notifyType === context.params.notifyType &&
              option.data.bookingFrom === context.params.bookingFrom
            ) {
              notifySMSData = option;
            }
          });

        if (notifySMSData?.data?.smsNotificationsEnable) {
          //  Booking data
          let bookingDetails = await this.broker.emit(
            "booking.getBookingDetailsById",
            {
              "bookingId": context.params?.rideId,
            }
          );
          bookingDetails = bookingDetails && bookingDetails[0];
          if (!bookingDetails)
            throw new MoleculerError(notifyMsg.INVALID_BOOKING);

          const formUrlEncoded = (inputData) =>
            Object.keys(inputData).reduce(
              (queryParmaData, item) =>
                queryParmaData +
                `&${item}=${encodeURIComponent(inputData[item])}`,
              ""
            );
          //#region Message Start
          professionalName =
            bookingDetails?.professional?.firstName?.trim() || "";
          userName = bookingDetails?.user?.firstName?.trim() || "";
          // bookingDate = bookingDetails.bookingDate.toLocaleString("en-GB", {
          //   "hour12": false,
          // });
          bookingDate = bookingDetails.bookingDate.toLocaleDateString(
            constantUtil.DEFAULT_LANGUAGE,
            {
              "year": "numeric",
              "month": "short",
              "day": "numeric",
              "hour": "numeric",
              "minute": "2-digit",
            }
          );
          pickupAddress = bookingDetails?.origin?.fullAddress?.trim() || "";
          dropAddress = bookingDetails?.destination?.fullAddress?.trim() || "";
          cancelReason = bookingDetails?.cancellationReason || "";
          //----------- Message Start ------------------
          message = message.replace(
            /{{professionalName}}/g,
            initialCaps(professionalName)
          );
          message = message.replace(/{{userName}}/g, initialCaps(userName));
          message = message.replace(
            /{{plateNumber}}/g,
            (
              bookingDetails?.professional?.vehicles[0]?.plateNumber || ""
            ).toUpperCase()
          );
          message = message.replace(
            /{{carModel}}/g,
            initialCaps(
              bookingDetails?.professional?.vehicles[0]?.maker || ""
            ) +
              " " +
              initialCaps(
                bookingDetails?.professional?.vehicles[0]?.model || ""
              )
          );
          message = message.replace(
            /{{carColor}}/g,
            initialCaps(bookingDetails?.professional?.vehicles[0]?.color || "")
          );
          message = message.replace(
            /{{bookingOTP}}/g,
            bookingDetails?.bookingOTP?.toString()
          );
          message = message.replace(
            /{{phoneNumber}}/g,
            bookingDetails.professional.phone.code.toString() +
              bookingDetails.professional.phone.number.toString()
          );
          message = message.replace(
            /{{trackLink}}/g,
            `${generalSettings.data.redirectUrls.trackBooking}${bookingDetails.bookingId}`
          );
          message = message.replace(/{{bookingDate}}/g, bookingDate);
          message = message.replace(/{{pickupAddress}}/g, pickupAddress);
          message = message.replace(/{{dropAddress}}/g, dropAddress);
          message = message.replace(/{{cancelReason}}/g, cancelReason);
          //#endregion Message End

          if (context.params.notifyFor === constantUtil.PROFESSIONAL) {
            phoneCode = bookingDetails.professional.phone.code.toString();
            phoneNumber = bookingDetails.professional.phone.number.toString();
          } else if (
            context.params.notifyFor === constantUtil.PACKAGERECEIVER
          ) {
            phoneCode = "";
            phoneNumber = bookingDetails.packageData.recipientNo.toString();
            // phoneCode = bookingDetails.packageData.recipientPhoneCode.toString();
            // phoneNumber = bookingDetails.packageData.recipientPhoneNo.toString();
          } else {
            phoneCode = bookingDetails.bookingFor.phoneCode.toString();
            phoneNumber = bookingDetails.bookingFor.phoneNumber.toString();
          }
        }
      }
    } else {
      message = context.params?.template?.content || context.params.message;
    }
    {
      try {
        jsonInputData = {
          // "from": smsSetting.data.senderID,
          // "sender": smsSetting.data.senderName,
          // "to":
          //   bookingDetails.bookingFor.phoneCode.toString() +
          //   bookingDetails.bookingFor.phoneNumber.toString(),
          // // "+251911237975",
          // "message": message,

          "messaging_product": "whatsapp",
          "recipient_type": "individual",
          "to": phoneCode + phoneNumber,
          // "to": "+917373598575",
          // // "+251911237975",
          "type": "text",
          "text": {
            "preview_url": false,
            "body": message,
          },
        };
        url =
          "https://graph.facebook.com/v16.0/" +
          whatsupData.data.accountNumber + // Phone Account id
          "/messages"; //Original
        // url = "https://graph.facebook.com/v16.0/115441128234449/messages"; //need to Change url
        headers = {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + whatsupData.data.authToken,
          // "Authorization":
          //   "Bearer EAAKzCDUdLqsBAFHQmYkYLhM02QJoaELL6CtrppqqpOp8Vs9qLnDXNkoBbeUi97Jcb1Vg2G7tkWOYtw6NsBDCZCZBN7cyy5MtDKVW5dpzQHJsz2cDWrvxFVpTxJ0DuUJW4X5uZCH9ZBLwHvw7OuzoFJZCXzTKZAGE8aggZBzZB4pUvINFpf8h0ZC6nuvcrgtm2z7ZBA9TbGpDQ169xAl6V31M1I",
        };
        config = {
          "method": "post",
          "url": url,
          "headers": headers,
          // "data": jsonInputData,
          "data": {
            "messaging_product": "whatsapp",
            "recipient_type": "individual",
            "to": phoneCode + phoneNumber,
            // "to": "+917373598575",
            // // "+251911237975",
            "type": "text",
            "text": {
              "preview_url": false,
              "body": message,
            },
          },
        };
        const response = await axios(config);
      } catch (error) {
        console.log("error from whatsup message", error);
      }
    }
  } catch (e) {
    return constantUtil.FAILED;
  }
  return constantUtil.SUCCESS;
};
smsAndCallUtil.sendSMSAlertForPromotions = async function (context) {
  const smsSetting = await storageUtil.read(constantUtil.SMSSETTING);
  let promotionSMSSetting = null;
  if (context.params.notifyType === constantUtil.EMERGENCY) {
    promotionSMSSetting = await storageUtil.read(
      constantUtil.EMERGENCYSMSSETTING
    );
  } else {
    promotionSMSSetting = await storageUtil.read(
      constantUtil.PROMOTIONSMSSETTING
    );
  }
  //
  let url = "",
    headers = {},
    config = {},
    jsonInputData = {};
  try {
    if (
      smsSetting.data.mode === constantUtil.PRODUCTION &&
      (context.params.notifyType === constantUtil.EMERGENCY ||
        promotionSMSSetting.data?.smsNotificationsEnable)
    ) {
      if (context.params.promotionMessage) {
        const formUrlEncoded = (inputData) =>
          Object.keys(inputData).reduce(
            (queryParmaData, item) =>
              queryParmaData +
              `&${item}=${encodeURIComponent(inputData[item])}`,
            ""
          );
        //----------- Message Start ------------------
        switch (promotionSMSSetting.data.smsType) {
          case constantUtil.AFRO:
            jsonInputData = {
              "from": promotionSMSSetting.data.senderID,
              "sender": promotionSMSSetting.data.senderName,
              "to": context.params.phoneNumber,
              // "+251911237975",
              "message": context.params.promotionMessage,
            };
            url = "https://api.afromessage.com/api/send";
            headers = {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + promotionSMSSetting.data.accessToken,
            };
            break;

          case constantUtil.TWILIO:
            await this.broker.emit("admin.sendSMS", {
              "phoneNumber": context.params.phoneNumber,
              "message": context.params.promotionMessage,
              "notifyType": context.params.notifyType, // EMERGENCY / PROMOTION,
              "otpReceiverAppName": constantUtil.SMS,
            });
            break;

          case constantUtil.TUNISIESMS:
            // headers = {
            //   "Content-Type": "application/json",
            // };
            //#region Send SMS Using Gatway
            if (promotionSMSSetting.data.isEnableSMSGatwayOTP) {
              jsonInputData = {
                "fct": "sms",
                "key": promotionSMSSetting.data.authToken,
                "mobile": context.params.phoneNumber,
                "sms": context.params.promotionMessage,
                "sender": promotionSMSSetting.data.senderName,
                "date": moment(new Date()).format("DD/MM/YYYY"),
                "heure": moment(new Date()).format("h:mm"),
              };
              const urlLink =
                "https://api.l2t.io/tn/v0/api/api.aspx?fct=sms&key=" +
                jsonInputData.key +
                "&mobile=" +
                jsonInputData.mobile +
                "&sms=" +
                jsonInputData.sms +
                "&sender=" +
                jsonInputData.sender +
                "&date=" +
                jsonInputData.date +
                "&heure=" +
                jsonInputData.heure +
                "";
              config = {
                "method": "GET",
                "url": urlLink,
                // "port": 443,
                "headers": {},
              };
              try {
                const response = await axios(config);
                responseData = response?.data;
              } catch (e) {
                responseData = null;
              }
            }
            //#endregion Send SMS Using Gatway
            break;

          case constantUtil.SENDIFY:
            headers = {
              "Content-Type": "application/x-www-form-urlencoded",
            };
            //#region Send SMS Using WhatsUp
            if (promotionSMSSetting.data.isEnableWhatsupOTP) {
              jsonInputData = {
                "secret": promotionSMSSetting.data.authToken,
                "type": "whatsapp",
                "message": context.params.promotionMessage,
                "phone": context.params.phoneNumber,
                "expire": "300",
                "account": promotionSMSSetting.data.accountSid,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/otp", // OTP
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using WhatsUp
            //#region Send SMS Using Device Sim Network
            if (promotionSMSSetting.data.isEnableDeviceNetworkOTP) {
              jsonInputData = {
                "secret": promotionSMSSetting.data.authToken,
                "type": "sms",
                "message": context.params.promotionMessage,
                "phone": context.params.phoneNumber,
                "expire": "300",
                "mode": "devices",
                "priority": 1,
                "sim": promotionSMSSetting.data.senderID,
                "device": promotionSMSSetting.data.accountNumber,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/sms", // SMS
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using Device Sim Network
            //#region Send SMS Using Gatway
            if (promotionSMSSetting.data.isEnableSMSGatwayOTP) {
              jsonInputData = {
                "secret": promotionSMSSetting.data.authToken,
                "type": "sms",
                "message": context.params.promotionMessage,
                "phone": context.params.phoneNumber,
                "expire": "300",
                "mode": "credits",
                "gateway": promotionSMSSetting.data.accessToken,
              };
              config = {
                "method": "post",
                "url": "https://sendify.com.br/api/send/sms", // SMS
                // "port": 443,
                "headers": headers,
                "data": formUrlEncoded(jsonInputData),
              };
              try {
                const response = await axios(config);
              } catch (e) {
                console.log(e.message);
              }
            }
            //#endregion Send SMS Using Gatway
            break;
        }

        switch (promotionSMSSetting.data.smsType) {
          case constantUtil.AFRO:
            config = {
              "method": "post",
              "url": url,
              "headers": headers,
              "data": jsonInputData,
            };
            try {
              const response = await axios(config);
              return response.data;
            } catch (e) {
              return "error";
            }
            break;
        }
      }
    }
  } catch (e) {
    return constantUtil.FAILED;
  }
  return constantUtil.SUCCESS;
};
//------------------------------
module.exports = smsAndCallUtil;
