const { MoleculerError } = require("moleculer").Errors;
const storageUtils = require("./storage.util");
const constantUtils = require("./constant.util");
const flutterwave = require("../paymentGateways/fultterwave.payment");
const Mongoose = require("mongoose");
const { customAlphabet } = require("nanoid");

// export json
const transactionFlutterWaveEvents = {};
//
transactionFlutterWaveEvents.checkValidCard = async function (context) {
  const local = `http://192.168.1.20:8080/api/transaction/redirectPayment/${context.transactionId.toString()}`;
  const live = `https://api.${context.generalSettings.data.siteTitle.toLowerCase()}.com/api/transaction/redirectPayment/${context.transactionId.toString()}`;
  const payload = {
    "PBFPubKey": context.paymentData.testPublicKey,
    "cardno": context.cardNumber,
    "cvv": context.cvv,
    "expirymonth": context.expiryMonth,
    "expiryyear": context.expiryYear,
    "currency": "NGN",
    "country": "NG",
    "amount": "100", // its hard coded need to chage we have to work
    "email": context.userData.email,
    "phonenumber": context.userData.phone.number,
    "firstname": context.userData.firstName,
    "lastname": context.userData.lastName,
    "txRef": `card adding into ${context.generalSettings.data.siteTitle.toLowerCase()} ${Date.now()}`,
    "redirect_url": local,
  };
  const charge = await flutterwave.charge({
    "payload": payload,
    "secretkey": context.paymentData.testSecretKey,
  });
  if (charge.status === "success") {
    if (
      charge.data.authModelUsed &&
      charge.data.authModelUsed === "VBVSECURECODE"
    ) {
      return {
        "code": 200,
        "message": charge.message,
        "data": {
          "authType": "REDIRECT",
          "authModelUsed": charge.data.authModelUsed,
          "authurl": charge.data.authurl,
          "transactionId": context.transactionId,
          "redirectUrl": charge.data.redirectUrl,
          "payload": payload,
          "successResponse": charge.data,
        },
      };
    }
    if (charge.data.suggested_auth === "PIN") {
      return {
        "code": 200,
        "message": "PLEASE ENTER YOUR PIN",
        "data": {
          "authType": "PIN",
          "transactionId": context.transactionId,
          "payload": payload,
          "successresponse": charge.data,
        },
      };
    }

    // this means no condition matches only for development purpose
    return charge;
  }
  if (charge.status === "error") {
    return {
      "code": 400,
      "data": charge.data,
      "message": charge.message,
    };
  }
};
transactionFlutterWaveEvents.refund = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG REDIS ERROR CANT FIND PAYMENT"
    );
  }
  let gatewayData;
  for (const prop in paymentOptionList) {
    if (paymentOptionList[prop].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[prop];
      break;
    }
  }
  if (!gatewayData) {
    throw new MoleculerError(
      "SOMETHING WENT WRONG FLUTTERWAVE PAYEMENT GATEWAY DATE NOT FOUND OR STATUS OF PLEASE CHECK CAREFULLY "
    );
  }
  // GET TRANSACTION DETAILES
  const transaction = await this.adapter.model.findOne({
    "_id": context.params.transactionId,
  });
  if (!transaction) {
    throw new MoleculerError(
      `SOMETHING WENT WRONG CANT FIND ONE TRANSACTION , ID IS ${context.params._id} BUT NEED TO REFUND IT ,THIS IS PAYEMNT CHECK CAREFULLY`
    );
  }

  const refund = await flutterwave.refund({
    "ref": transaction.data.flwRef,
    "seckey": gatewayData.testSecretKey,
    "amount": transaction.transactionAmount,
  });
  if (refund.status === "error") {
    // need to set code inform to ADMIN
  }
  if (refund.status === "success") {
    if (refund.data.status === "completed") {
      // update db
      const updateTransaction = await this.adapter.model.updateOne(
        {
          "_id": Mongoose.Types.ObjectId(transaction._id.toString()),
        },
        {
          "refundResponse": refund.data,
        }
      );
      // verify the refund
      const reufundVerify = await flutterwave.reufundVerify();
    }
    return {
      "code": 200,
      "data": refund.data,
      "message": refund.message,
    };
  }
};
transactionFlutterWaveEvents.walletRecharge = async function (context) {
  const paymentOptionList = await storageUtils.read(
    constantUtils.PAYMENTGATEWAY
  );
  if (!paymentOptionList) throw new MoleculerError("SOMETHING WENT WRONG");
  let gatewayData;
  for (const props in paymentOptionList) {
    if (paymentOptionList[props].gateWay === constantUtils.FLUTTERWAVE) {
      gatewayData = paymentOptionList[props];
    }
  }
  if (!gatewayData) {
    throw new MoleculerError("NO ACTIVE PAYMENTGATEWAY");
  }
  // GET USERDATA
  let userData;
  if (context.params.userType === constantUtils.PROFESSIONAL) {
    userData = await this.broker.emit("professional.getById", {
      "id": context.params.userId,
    });
  }
  if (context.params.userType === constantUtils.USER) {
    userData = await this.broker.emit("user.getById", {
      "id": context.params.userId,
    });
  }
  userData = userData && userData[0];
  if (!userData) {
    throw new MoleculerError(
      "CANT FIND USER SOMETHING WENT WRONG IN FLUTTERWAVE WALLET AMOUNT RECHARGE"
    );
  }
  // card detailes
  const cardDetailes = userData.cards.find(
    (card) => card._id.toString() === context.params.cardId
  );
  if (!!cardDetailes === false) {
    return {
      "code": 400,
      "message": "GIVEN CARD  NOT FOUND SOMETHING WENT WRONG",
      "data": {},
    };
  }
  // need generalSettings
  const generalSettings = await storageUtils.read(constantUtils.GENERALSETTING);

  // txt reference
  let txRef;
  if (!!context.params.reason === true) {
    txRef = `${generalSettings.data.siteTitle}/${
      constantUtils.WALLETRECHARGE
    }/${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}/${
      context.params.reason
    }`;
  } else {
    txRef = `${generalSettings.data.siteTitle}/${
      constantUtils.WALLETRECHARGE
    }/${customAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 8)()}/`;
  }
  //

  // IF EMBED TOKEN FOUND ONLY OTP PROCESS
  if (cardDetailes.embedtoken) {
    const payload = {
      "currency": "NGN",
      "SECKEY": gatewayData.testSecretKey,
      "token": cardDetailes.embedtoken,
      "country": "NG",
      "amount": context.params.amount,
      "email": userData.email,
      "firstname": userData.firstName,
      "lastname": userData.lastName,
      "txRef": txRef,
    };
    const charge = await flutterwave.tokenizedCharge(payload);

    if (charge.status === "success") {
      if (charge.data.status === "successful") {
        const transaction = await this.adapter.model.create({
          "transactionAmount": charge.data.amount,
          "transactionReason": context.params.reason,
          "from": {
            "userType": context.params.userType,
            "userId": context.params.userId,
            "name": userData.firstName + userData.lastName,
            "phoneNumber": userData.phoneNumber,
          },
          "to": {
            "userType": context.params.userType,
            "name": constantUtils.ADMIN,
          },
          "paymentType": constantUtils.CREDIT, // TO SHOW TO USER WALLET TRANASCTION PAGE
          "gatewayName": constantUtils.FLUTTERWAVE,
          "transactionType": constantUtils.WALLETRECHARGE,
          "transactionId": `FL-${customAlphabet("1234567890", 9)()}`,
          "gatewayDocId": gatewayData._id,
          "transactionStatus": constantUtils.PENDING,
          "cardDetailes": cardDetailes,
          "gatewayResponse": charge.data,
        });
        if (charge.data.authModelUsed === "noauth") {
          // auth type checking
          // find need verify
          if (charge.data.chargeResponseCode === "00") {
            // verify the charge transaction
            const verify = await flutterwave.chargeVerify({
              "txref": charge.data.txRef,
              "SECKEY": gatewayData.testSecretKey,
            });
            // verify success
            if (verify.status === "success") {
              if (verify.data.status === "successful") {
                if (verify.data.chargecode === "00") {
                  // updateDb
                  const updateTransaction = await this.adapter.model.updateOne(
                    {
                      "_id": Mongoose.Types.ObjectId(
                        transaction._id.toString()
                      ),
                    },
                    {
                      "refundResponse": verify.data,
                      "transactionStatus": constantUtils.SUCCESS,
                    }
                  );
                  if (!updateTransaction.nModified) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN DB UPDATION"
                    );
                  }
                  // add amount to userData
                  let amountUpdateInUser;
                  if (context.params.userType === constantUtils.PROFESSIONAL) {
                    amountUpdateInUser = await this.broker.emit(
                      "professional.updateWalletAmount",
                      {
                        "professionalId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                      }
                    );
                  }
                  if (context.params.userType === constantUtils.USER) {
                    amountUpdateInUser = await this.broker.emit(
                      "user.updateWalletAmount",
                      {
                        "userId": userData._id.toString(),
                        "availableAmount":
                          userData.wallet.availableAmount +
                          context.params.amount,
                        "freezedAmount": userData.wallet.freezedAmount,
                        "scheduleFreezedAmount":
                          userData.wallet.scheduleFreezedAmount,
                      }
                    );
                  }
                  amountUpdateInUser =
                    amountUpdateInUser && amountUpdateInUser[0];
                  if (!amountUpdateInUser) {
                    throw new MoleculerError(
                      "SOMETHING WENT WRONG IN WALLET UPDATE IN WALLET RECHARE NO AUTH"
                    );
                  }
                  return {
                    "code": 200,
                    "message": "AMOUNT IS ADDED TO YOUR WALLET SUCCESSFULLY",
                    "data": {
                      "availableAmount":
                        userData.wallet.availableAmount + context.params.amount,
                    },
                  };
                }
                return {
                  "code": 400,
                  "message": verify.message,
                  "data": verify.data,
                };
                // then dont need some times need to check
              }
              return {
                "code": 400,
                "message": verify.message,
                "data": verify.data,
              };
            }
            //  SOMTHIG HAPPEND WRONG NEED TO INFORM ADMIN

            throw new MoleculerError(
              "SOME THING WENT WRONG IN WALLET RECHARGE "
            );
          }
          // it means dont know what happens need to know for developers
          return {
            "code": 400,
            "message": charge.message,
            "data": charge.data,
          };
        }
      }
      // charge not approved
      return {
        "code": 400,
        "message": charge.data.vbvrespmessage,
        "data": charge.data,
      };
    }
    // it handles error
    return {
      "code": 400,
      "message": charge.message,
      "data": charge.data,
    };
  }

  return null;
};
module.exports = transactionFlutterWaveEvents;

// if (paymentData.gateWay === constantUtils.FLUTTERWAVE) {
//   //
//   let transaction = await this.adapter.model.findOne({
//     '_id': context.params.transactionId,
//   })
//   if (!transaction)
//     throw new MoleculerError(
//       'CANT FIND TRANSACTION ID IS' + context.params.id
//     )

//   //
//   const Flutterwave = require('flutterwave-node-v3')
//   const flw = new Flutterwave(
//     paymentData.testPublicKey,
//     paymentData.testSecretKey
//   )
//   if (context.params.authMode === constantUtils.OTP) {
//     const cardValidate = await flw.Charge.validate({
//       'otp': context.params.otp,
//       //'flw_ref': transaction.gatewayResponse.data.flw_ref,//Ajith
//       // 'flw_ref': gatewayResponse.data.flw_ref,
//       'flw_ref': transaction.transactionId,
//     })
//     if (cardValidate.status === 'error') {
//       return { 'code': 500, 'data': null, 'message': cardValidate.message }
//     }
//     const updateTransaction = await this.adapter.model.updateOne(
//       {
//         '_id': Mongoose.Types.ObjectId(transaction._id.toString()),
//       },
//       {
//         'gatewayResponse': cardValidate,
//       }
//     )
//     if (!updateTransaction.nModified)
//       throw new MoleculerError(
//         `SOMETHING WENT WRONG IN CARD VALIDATE SUCESS REPONSE UPDATING IN DB AND ID IS ${transaction._id}`
//       )

//     //
//   }
//   if (context.params.authMode === 'REDIRECT') {
//     // IT MEANS API AMOUNT IS SUCCESS
//   }
//   const checkTransactionCount = await this.adapter.model.count({
//     'transactionId': transaction.transactionId,
//   })
//   if (!checkTransactionCount)
//     throw new MoleculerError(
//       'CANT FIND TRANSACTION ID IS' + transaction.transactionId
//     )
//   if (checkTransactionCount === 0) {
//     //-------------- Ajith --------------
//     const updateTrasactionData = this.adapter.insert({
//       'transactionType': constantUtils.WALLETRECHARGE,
//       'transactionAmount': transaction.transactionAmount,
//       'transactionReason': transaction.transactionReason || '',
//       'paidTransactionId': finalPaymentData.id,
//       'transactionId': transaction.transactionId,
//       'transactionDate': new Date(),
//       'transactionStatus': constantUtils.SUCCESS,
//       'paymentType': constantUtils.CREDIT,
//       'from': {
//         'userType': context.params.userType,
//         'userId': id,
//         'name': userData.firstName + ' ' + userData.lastName || null,
//         'avatar': userData.avatar || null,
//         'phoneNumber': userData.phone.number || null,
//       },
//       'to': {
//         'userType': constantUtils.ADMIN,
//         'userId': null,
//         'name': userData.firstName + ' ' + userData.lastName || null,
//         'avatar': userData.avatar || null,
//         'phoneNumber': userData.phone.number || null,
//       },
//       // 'isActive': 1,
//     })
//     if (!updateTrasactionData)
//       throw new MoleculerError('ERROR IN WALLET REACHARGE')
//     //----------------------------
//   } else {
//     const updateTransaction = await this.adapter.model.updateOne(
//       {
//         'transactionId': transaction.transactionId,
//       },
//       {
//         'transactionStatus': constantUtils.SUCCESS,
//         'transactionType': constantUtils.WALLETRECHARGE,
//       }
//     )
//     if (!updateTransaction.nModified)
//       throw new MoleculerError(
//         `SOMETHING WENT WRONG IN CARD VALIDATE SUCESS REPONSE UPDATING IN DB AND ID IS ${transaction._id}`
//       )
//   }
//   transaction = await this.adapter.model.findOne({
//     '_id': context.params.transactionId,
//   })
//   if (!transaction)
//     throw new MoleculerError(
//       'CANT FIND TRANSACTION ID IS' + context.params.id
//     )
//   // updateAmount
//   if (context.params.userType === constantUtils.USER) {
//     this.broker.emit('user.updateWalletAmount', {
//       'userId': userData._id.toString(),
//       'availableAmount':
//         userData.wallet.availableAmount +
//         parseFloat(transaction.gatewayResponse.data.amount),
//       'freezedAmount': userData.wallet.freezedAmount,
//       'scheduleFreezedAmount': userData.wallet.scheduleFreezedAmount,
//     })
//   }
//   if (context.params.userType === constantUtils.PROFESSIONAL) {
//     this.broker.emit('professional.updateWalletAmount', {
//       'professionalId': userData._id.toString(),
//       'availableAmount':
//         userData.wallet.availableAmount +
//         parseFloat(transaction.gatewayResponse.data.amount),
//       'freezedAmount': userData.wallet.freezedAmount,
//     })
//   }

//   return {
//     'code': 200,
//     'data': '',
//     'message': `AMOUNT ADDED SUCCESSFULLY`,
//   }
// }
