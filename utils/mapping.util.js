const { MoleculerError } = require("moleculer").Errors;

const constantUtil = require("./constant.util");
const storageUtil = require("./storage.util");
const formula = require("formula");
const formulaJSON = require("../data/formula.json");

const notifyMessage = require("../mixins/notifyMessage.mixin");
const { CONST_MERCADOPAGO, SANDBOX, CARD } = require("./constant.util");

const mappingUtils = {};

mappingUtils.userObject = async function (userData) {
  const { INFO_PAYMENT_OPTIONS } = notifyMessage.setNotifyLanguage(
    constantUtil.DEFAULT_LANGUAGE
  );
  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  const clientId = userData?.clientId?.toString();
  if (!paymentOptionList) {
    throw new MoleculerError(INFO_PAYMENT_OPTIONS);
  }
  let paymentGateWay = {};
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentGateWay = paymentOptionList[prop];
      paymentGateWay = {
        "mode": paymentGateWay.mode,
        "gateWay": paymentGateWay.gateWay,
        "testSecretKey": paymentGateWay.testSecretKey,
        "testPublicKey": paymentGateWay.testPublicKey,
        "livePublicKey": paymentGateWay.livePublicKey,
        "liveSecretKey": paymentGateWay.liveSecretKey,
        "paymentType": paymentGateWay.paymentType,
        "isCVVRequired": paymentGateWay.isCVVRequired,
        "isPreAuthRequired": paymentGateWay.isPreAuthRequired,
        "isEnablePixkeyPayment": paymentGateWay.isEnablePixkeyPayment,
        "redirectUrl": null,
        "redirectMethodType": null,
        "redirectType": null,
      };
      break;
    }
  }
  if (paymentGateWay.gateWay === CONST_MERCADOPAGO) {
    const baseURL =
      generalSettings.data?.redirectUrls?.paymentGatewayUrl ||
      "https://walrus-app-vfyf9.ondigitalocean.app";
    let publicKey = null;
    if (paymentGateWay.mode.toUpperCase() === SANDBOX) {
      publicKey = paymentGateWay["testPublicKey"];
    } else {
      publicKey = paymentGateWay["livePublicKey"];
    }
    //
    const redirectUrl =
      `${baseURL}/card?publicKey=` +
        publicKey +
        "&languageCode=" +
        userData?.languageCode || "en";
    //
    paymentGateWay["redirectUrl"] = redirectUrl;
    paymentGateWay["redirectMethodType"] = "GET";
    paymentGateWay["redirectType"] = CARD;
  }
  return {
    "clientId": clientId,
    "userId": userData?._id?.toString(),
    "newUser": userData.newUser,
    "accessToken": userData.accessToken,
    "avatar": userData.avatar?.toString("utf8"),
    "uniqueCode": userData.uniqueCode,
    "languageCode": userData.languageCode,
    "currencyCode": userData.currencyCode,
    "lastCurrencyCode": userData.lastCurrencyCode || "USD",
    "status": userData.status,
    "firstName": userData.firstName,
    "lastName": userData.lastName,
    "email": userData.email,
    "isEmailVerified": userData.isEmailVerified || false,
    "gender": userData.gender,
    "phone": {
      "code": userData.phone.code,
      "number": userData.phone.number,
    },
    "nationalIdNo": userData.nationalIdNo,
    "dob": userData.dob,
    "addressList": userData.addressList,
    "bookingInfo": userData.bookingInfo,
    "escortInfo": userData.escortInfo,
    "createdAt": userData.createdAt,
    "updatedAt": userData.updatedAt,
    "defaultPayment":
      userData.defaultPayment || generalSettings.data.defaultPayment,
    "appliedCoupon": userData.appliedCoupon,
    "trustedContacts": userData.trustedContacts,
    "review": userData.review,
    "wallet": userData.wallet,
    "subscriptionInfo": userData.subscriptionInfo,
    "inActivatedAt": userData.inActivatedAt || null,
    "inActivatedBy": userData.inActivatedBy,
    "paymentGateWay": paymentGateWay,
    "paymentGateWayCustomerId": userData.paymentGateWayCustomerId,
    "isEnableUserBlockList": generalSettings.data.isEnableUserBlockList,
    "cardList": userData.cards || [],
    "scanQr": userData.scanAndPayQR,
  };
};

mappingUtils.userObjectForAdmin = (userData) => {
  return {
    "_id": userData._id.toString(),
    "newUser": userData.newUser,
    "accessToken": userData.accessToken,
    "avatar": userData.avatar?.toString("utf8"),
    "uniqueCode": userData.uniqueCode,
    "languageCode": userData.languageCode,
    "currencyCode": userData.currencyCode,
    "lastCurrencyCode": userData.lastCurrencyCode || "USD",
    "status": userData.status,
    "firstName": userData.firstName,
    "lastName": userData.lastName,
    "email": userData.email,
    "gender": userData.gender,
    "phone": {
      "code": userData.phone.code,
      "number": userData.phone.number,
    },
    "nationalIdNo": userData.nationalIdNo,
    "addressList": userData.addressList,
    "bookingInfo": userData.bookingInfo,
    "escortInfo": userData.escortInfo,
    "createdAt": userData.createdAt,
    "updatedAt": userData.updatedAt,
    "defaultPayment": userData.defaultPayment,
    "trustedContacts": userData.trustedContacts,
    "review": userData.review,
    "wallet": userData.wallet,
    "subscriptionInfo": userData.subscriptionInfo,
  };
};

mappingUtils.professionalObject = async function (professionalData) {
  const { INFO_PAYMENT_OPTIONS } = notifyMessage.setNotifyLanguage("en");

  const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
  const paymentOptionList = await storageUtil.read(constantUtil.PAYMENTGATEWAY);
  const vehicleCategoryList = await storageUtil.read(
    constantUtil.VEHICLECATEGORY
  );
  const clientId = professionalData?.clientId?.toString();
  if (!paymentOptionList) {
    throw new MoleculerError(INFO_PAYMENT_OPTIONS);
  }
  let paymentGateWay = {};
  for (const prop in paymentOptionList) {
    if (
      paymentOptionList[prop].status === constantUtil.ACTIVE &&
      paymentOptionList[prop].paymentType === constantUtil.PAYMENTCARD
    ) {
      paymentGateWay = paymentOptionList[prop];
      paymentGateWay = {
        "mode": paymentGateWay.mode,
        "gateWay": paymentGateWay.gateWay,
        "testSecretKey": paymentGateWay.testSecretKey,
        "testPublicKey": paymentGateWay.testPublicKey,
        "livePublicKey": paymentGateWay.livePublicKey,
        "liveSecretKey": paymentGateWay.liveSecretKey,
        "paymentType": paymentGateWay.paymentType,
        "isCVVRequired": paymentGateWay.isCVVRequired,
        "isPreAuthRequired": paymentGateWay.isPreAuthRequired,
        "isEnablePixkeyPayment": paymentGateWay.isEnablePixkeyPayment,
        "redirectUrl": null,
        "redirectMethodType": null,
        "redirectType": null,
      };
      break;
    }
  }
  if (paymentGateWay.gateWay === CONST_MERCADOPAGO) {
    const baseURL =
      generalSettings.data?.redirectUrls?.paymentGatewayUrl ||
      "https://walrus-app-vfyf9.ondigitalocean.app";
    let publicKey = null;
    if (paymentGateWay.mode.toUpperCase() === SANDBOX) {
      publicKey = paymentGateWay["testPublicKey"];
    } else {
      publicKey = paymentGateWay["livePublicKey"];
    }
    //
    const redirectUrl =
      `${baseURL}/card?publicKey=` +
        publicKey +
        "&languageCode=" +
        professionalData?.languageCode || "en";
    //
    paymentGateWay["redirectUrl"] = redirectUrl;
    paymentGateWay["redirectMethodType"] = "GET";
    paymentGateWay["redirectType"] = CARD;
  }
  await professionalData?.vehicles?.map((vehicle) => {
    if (vehicle.vehicleCategoryId) {
      vehicle["vehicleCategoryName"] =
        vehicleCategoryList[
          vehicle.vehicleCategoryId?.toString()
        ]?.vehicleCategory;
    } else {
      vehicle["vehicleCategoryName"] = "";
    }
  });
  return {
    "clientId": clientId,
    "registerDate": professionalData.createdAt,
    "professionalId": professionalData?._id?.toString(),
    "newProfessional": professionalData?.newProfessional ?? false,
    "registrationNextStep": professionalData.registrationNextStep,
    "unverifiedVehicleIds": professionalData?.unverifiedVehicleIds || [],
    "accessToken": professionalData.accessToken,
    "avatar": professionalData.avatar?.toString("utf8"),
    "languageCode": professionalData.languageCode,
    "status": professionalData.status,
    "firstName": professionalData.firstName,
    "lastName": professionalData.lastName,
    "email": professionalData.email,
    "isEmailVerified": professionalData.isEmailVerified || false,
    "gender": professionalData.gender,
    "phone": {
      "code": professionalData.phone.code,
      "number": professionalData.phone.number,
    },
    "nationalIdNo": professionalData.nationalIdNo,
    "nationalInsuranceNo": professionalData.nationalInsuranceNo,
    "tin": professionalData.tin,
    "dob": professionalData.dob,
    "age": professionalData.age,
    "address": professionalData.address,
    "profileVerification": professionalData.profileVerification,
    "bankDetails": professionalData.bankDetails || {},
    "vehicles": professionalData.vehicles,
    "serviceTypeId": professionalData?.serviceTypeId || null,
    "serviceType": professionalData?.serviceType || constantUtil.CONST_RIDE,
    "bookingInfo": professionalData.bookingInfo,
    "escortInfo": professionalData.escortInfo,
    "createdAt": professionalData.createdAt,
    "updatedAt": professionalData.updatedAt,
    "onlineStatus": professionalData.onlineStatus,
    "trustedContacts": professionalData.trustedContacts,
    "review": professionalData.review,
    "wallet": professionalData.wallet,
    "subscriptionInfo": professionalData.subscriptionInfo,
    "scanQr": professionalData.scanQr,
    "isPriorityStatus": professionalData.isPriorityStatus,
    "currencyCode": professionalData.currencyCode,
    "lastCurrencyCode": professionalData.lastCurrencyCode || "USD",
    "membershipDetails": professionalData.membershipDetails || [],
    "inActivatedAt": professionalData.inActivatedAt || null,
    "inActivatedBy": professionalData.inActivatedBy,
    "licenceNo": professionalData.licenceNo,
    "paymentGateWay": paymentGateWay,
    "paymentGateWayCustomerId": professionalData.paymentGateWayCustomerId,
    "isEnableUserBlockList": generalSettings.data.isEnableProfessionalBlockList,
    "serviceAreaId":
      professionalData.serviceAreaId ||
      professionalData.vehicles?.[0]?.serviceCategory,
    "blockedTill": professionalData.blockedTill,
    "paymentMode": professionalData.paymentMode || [],
  };
};

mappingUtils.officerObject = (officerData) => {
  return {
    "newOfficer": officerData.newOfficer,
    "accessToken": officerData.accessToken,
    "avatar": officerData.avatar?.toString("utf8"),
    "languageCode": officerData.languageCode,
    "status": officerData.status,
    "firstName": officerData.firstName,
    "lastName": officerData.lastName,
    "email": officerData.email,
    "gender": officerData.gender,
    "phone": {
      "code": officerData.phone.code,
      "number": officerData.phone.number,
    },
    "dob": officerData.dob,
    "age": officerData.age,
    "bookingInfo": officerData.bookingInfo,
    "escortInfo": officerData.escortInfo,
    "onlineStatus": officerData.onlineStatus,
  };
};

mappingUtils.adminObject = (adminData) => {
  return {
    "_id": adminData._id,
    "clientId": adminData?.data?.clientId?.toString(),
    "userType": adminData.name,
    "isSuperAdmin": adminData.data?.isSuperAdmin || false,
    "firstName": adminData.data.firstName,
    "lastName": adminData.data.lastName,
    "hubsName": adminData.data.hubsName,
    "officeName": adminData.data.officeName,
    "email": adminData.data.email,
    "gender": adminData.data.gender,
    "phone": {
      "code": adminData.data.phone.code,
      "number": adminData.data.phone.number,
    },
    "avatar": adminData.data.avatar?.toString("utf8"),
    "walletAmount": adminData.data.amount || 0,
    "accessToken": adminData.data.accessToken,
    "languageCode": adminData.data.languageCode,
    "status": adminData.data.status,
    "billingData": adminData.data.billingData,
    "defaultPrivileges": adminData.data.defaultPrivileges,
    "privileges": adminData.data.privileges,
    "extraPrivileges": adminData.data.extraPrivileges,
    "createdAt": adminData.data.createdAt,
    "updatedAt": adminData.data.updatedAt,
    "isEnableFixedChangeRideForCorporate":
      adminData.data?.isEnableFixedChangeRide || false,
  };
};

// const removeImageURLPath = async (imageNameWithPath) => {
//   const generalSettings = await storageUtil.read(constantUtil.GENERALSETTING);
//   const imageURL =
//     generalSettings.data.spaces.spacesBaseUrl +
//     "/" +
//     generalSettings.data.spaces.spacesObjectName;
//   return imageNameWithPath.replace(imageURL, "");
// };

mappingUtils.bookingObject = (bookingData) => {
  // const imageURL =
  //   generalSettings.data.spaces.spacesBaseUrl +
  //   "/" +
  //   generalSettings.data.spaces.spacesObjectName;
  const imageURL = bookingData.imageURLPath || "";
  //-------- rideStops Start --------
  // const rideStops = [];
  // const rideStopsList = bookingData?.activity || [];
  // console.log(rideStopsList);
  // console.log(bookingData?.activity.rideStops);
  // rideStopsList.rideStops.forEach((rideStops) => {
  //   const jsonOutput = {
  //     "sts": rideStops.status, //"status"
  //     "typ": rideStops.type, //"type"
  //     "_id": rideStops._id,
  //     "aN": rideStops.addressName, //"addressName"
  //     "fA": rideStops.fullAddress, //"fullAddress"
  //     "sA": rideStops.shortAddress, //"shortAddress"
  //     "la": rideStops.lat, //"lat"
  //     "ln": rideStops.lng, //"lng"
  //   };
  //   rideStops.push(jsonOutput);
  // });
  const rideStops = bookingData?.activity?.rideStops;
  // const paymentOption =
  //   (bookingData?.payment?.option ?? "") === constantUtil.CREDIT
  //     ? constantUtil.CORPORATECREDIT
  //     : bookingData?.payment?.option ?? "";
  const paymentOption = bookingData?.payment?.option ?? "";
  const isPrepaidRide = bookingData?.payment?.isPrepaidRide || false;
  // console.log(rideStops);
  //-----------------------------
  const payableAmount =
    bookingData.bookingStatus === constantUtil.AWAITING
      ? bookingData?.invoice?.actualEstimationAmount || 0
      : bookingData?.invoice?.payableAmount || 0; //"payableAmount"//Need to Check
  // "couponApplied": bookingData?.invoice?.couponApplied ?? 0,
  //-------- rideStops End --------
  return {
    "_id": bookingData?._id ?? "",
    "bI": bookingData?.bookingId ?? "", //"bookingId"
    "bO":
      bookingData.isOtpNeeded === false ||
      (bookingData.serviceCategory ===
        constantUtil.CONST_PACKAGES.toLowerCase() &&
        (bookingData?.packageData?.isEnableOTPVerification || false) === false)
        ? ""
        : bookingData?.bookingOTP, //"bookingOTP"
    "bT": bookingData?.bookingType ?? "", //"bookingType"
    "bD": bookingData?.bookingDate ?? "", //"bookingDate"
    "sC": bookingData?.serviceCategory ?? "", //"serviceCategory"
    "bF": bookingData?.bookingFor
      ? {
          "nA": bookingData?.bookingFor.name, //"name"
          "pC": bookingData?.bookingFor.phoneCode, //"phoneCode"
          "pNo": bookingData?.bookingFor.phoneNumber, //"phoneNumber"
        }
      : {}, //"bookingFor"
    "bB": bookingData?.bookingBy || "USER", //"bookingBy"
    // "isSecurityAvail": bookingData?.isSecurityAvail ?? "", // Need to Check
    "us": bookingData.user //"user"
      ? {
          "_id": bookingData?.user?._id ?? "",
          "fN": bookingData?.user?.firstName || "No Name", //"firstName"
          // "lastName": bookingData?.user?.lastName ?? "",
          "ph": bookingData?.user?.phone
            ? {
                "cod": bookingData?.user?.phone.code, // "code"
                "no": bookingData?.user?.phone.number, //"number"
              }
            : {}, //"phone"
          "av": (bookingData?.user?.avatar || "")
            .replace(imageURL, "")
            ?.toString("utf8"), //"avatar"
          // "escortInfo": bookingData?.user?.escortInfo ?? {},  // Need to Check
          "bIn": bookingData?.user?.bookingInfo
            ? {
                "onB": bookingData?.user?.bookingInfo.ongoingBooking, // "ongoingBooking"
                "pRw": bookingData?.user?.bookingInfo.pendingReview, //"pendingReview"
                "sB": bookingData?.user?.bookingInfo.scheduledBooking ?? [], //"scheduledBooking"
              }
            : {}, //"bookingInfo"
          "wa": bookingData?.user?.wallet
            ? {
                "aA": bookingData?.user?.wallet.availableAmount, //"availableAmount"
                "fzA": bookingData?.user?.wallet.freezedAmount, //"freezedAmount"
                "dA": bookingData?.user?.wallet.dueAmount, //"dueAmount"
                "sFA": bookingData?.user?.wallet.scheduleFreezedAmount, //"dueAmount"
              }
            : {}, //"wallet"
          "aR": bookingData?.user?.review?.avgRating ?? 0, //"avgRating"
        }
      : {},
    "iAFA": bookingData?.isAdminForceAssign ?? false, //"isAdminForceAssign"
    // "iAA": bookingData.adminAssignedProfessional ? true : false, //isAdminAssigned
    "rA": bookingData.adminAssignedProfessional
      ? constantUtil.ACTION_ASSIGN
      : bookingData?.isAdminForceAssign
      ? constantUtil.FORCEASSIGNBYADMIN
      : constantUtil.NONE, //rideAction
    "iAOC": bookingData.isAcceptOtherCategory ?? false, //"isAcceptOtherCategory"
    "pr": bookingData.professional //"professional"
      ? {
          "_id": bookingData?.professional?._id ?? "",
          "fN": bookingData?.professional?.firstName ?? "", //"firstName"
          // "lastName": bookingData?.professional?.lastName ?? "",
          "ph": bookingData?.professional?.phone
            ? {
                "cod": bookingData?.professional?.phone.code, // "code"
                "no": bookingData?.professional?.phone.number, //"number"
              }
            : {}, //"phone"
          "av": (bookingData?.professional?.avatar || "")
            .replace(imageURL, "")
            ?.toString("utf8"), //"avatar"
          "aR": bookingData?.professional?.review?.avgRating ?? 0, // "avgRating"
          "la": bookingData?.professional?.location?.coordinates[1] ?? 0, //"lat"
          "ln": bookingData?.professional?.location?.coordinates[0] ?? 0, //"lng"
          "wa": bookingData?.professional?.wallet
            ? {
                "aA": bookingData?.professional?.wallet.availableAmount, //"availableAmount"
                "fzA": bookingData?.professional?.wallet.freezedAmount, //"freezedAmount"
                "dA": bookingData?.professional?.wallet.dueAmount, //"dueAmount"
              }
            : {}, //"wallet"
          "bIn": bookingData?.professional?.bookingInfo
            ? {
                "onB": bookingData?.professional?.bookingInfo.ongoingBooking, // "ongoingBooking"
                "pRw": bookingData?.professional?.bookingInfo.pendingReview, //"pendingReview"
                "oTB":
                  bookingData?.professional?.bookingInfo.ongoingTailBooking, // "ongoingTailBooking"
              }
            : {}, //"bookingInfo"
          "oS": bookingData?.professional?.onlineStatus ?? true, //"onlineStatus"
        }
      : {},
    "of": bookingData.officer //"officer"
      ? {
          "_id": bookingData?.officer?._id ?? "",
          "fN": bookingData?.officer?.firstName ?? "", // "firstName"
          // "lastName": bookingData?.officer?.lastName ?? "",
          "ph": bookingData?.officer?.phone
            ? {
                "cod": bookingData?.officer?.phone.code, // "code"
                "no": bookingData?.officer?.phone.number, //"number"
              }
            : {}, //"phone"
          "av": (bookingData?.officer?.avatar ?? "")
            .replace(imageURL, "")
            ?.toString("utf8"), //"avatar"
          "aR": bookingData?.officer?.review?.avgRating ?? 0, //"avgRating"
          "la": bookingData?.officer?.location?.coordinates[1] ?? 0, //"lat"
          "ln": bookingData?.officer?.location?.coordinates[0] ?? 0, //"lng"
        }
      : {},
    "se": bookingData.security //"security"
      ? {
          "_id": bookingData?.security?._id ?? "",
          "ack": bookingData?.security?.acknowledge ?? "", //"acknowledge"
          "iEA": bookingData?.security?.isEscortApply ?? "", // "isEscortApply"
          "eS": bookingData?.security?.escortStatus || {}, //"escortStatus"
          "eR": bookingData?.security?.escortReview || {}, //"escortReview"
          "iAFA": bookingData?.security?.isAdminForceAssign ?? "", //"isAdminForceAssign"
          "uT": bookingData?.security?.userType ?? "", //"userType"
          "sSN": bookingData?.security?.securityServiceName ?? "", //"securityServiceName"
          "sSI": bookingData?.security?.securityServiceId ?? "", //"securityServiceId"
          "tM": bookingData?.security?.threadMessage ?? "", //"threadMessage"
          "aT": bookingData?.security?.alertType ?? "", //"alertType"
          "aD": bookingData?.security?.alertDate ?? "", //"alertDate"
          "aL": bookingData?.security?.alertLocation ?? "", //"alertLocation"
          "sTI": bookingData?.security?.securityThreadId ?? "", //"securityThreadId"
        }
      : {},
    "ve": bookingData.vehicle //"vehicle"
      ? {
          // "vehicleCategoryId": bookingData?.vehicle?.vehicleCategoryId ?? "",
          "vCN": bookingData.vehicle?.vehicleCategoryName ?? "", //"vehicleCategoryName"
          "vCI": (bookingData.vehicle?.vehicleCategoryImage ?? "").replace(
            imageURL,
            ""
          ), //"vehicleCategoryImage"
          "mo": bookingData.vehicle?.model ?? "", //"model"
          "pN": bookingData.vehicle?.plateNumber ?? "", //"plateNumber"
          "co": bookingData.vehicle?.color ?? "", //"color"
          "vCMI": (bookingData.vehicle?.vehicleCategoryMapImage ?? "").replace(
            imageURL,
            ""
          ), // "vehicleCategoryMapImage"
        }
      : {},
    "bR": bookingData?.bookingReview
      ? {
          "userReview": {
            "rat": bookingData?.bookingReview?.userReview?.rating || 0, // "rating"
          },
          "professionalReview": {
            //"professionalReview"
            "rat": bookingData?.bookingReview?.professionalReview?.rating || 0, // "rating"
          },
        }
      : {}, //"bookingReview"
    "mD": bookingData?.manualDistance ?? "", //"manualDistance"
    "estimation": bookingData?.estimation
      ? {
          "distance": parseInt(bookingData?.estimation?.distance || 0), //  "distance" //Need to Remove Condition
          "time": parseInt(bookingData?.estimation?.time || 60), //"time" //Need to Remove Condition
        }
      : {}, //"estimation"
    // "bookedEstimation": bookingData?.bookedEstimation ?? {},
    "cS": bookingData?.currencySymbol ?? "$", // "currencySymbol"
    "cC": bookingData?.currencyCode ?? "USD", //"currencyCode"
    "pa": bookingData.payment //"payment"
      ? {
          // "op": isPrepaidRide === true ? constantUtil.PAID : paymentOption, //"option"
          "op": paymentOption, //"option"
          "pd": bookingData?.payment?.paid ?? "", //"paid"
          "pI": bookingData?.payment?.paymentId ?? "", //"paymentId"
        }
      : {},
    "inv": bookingData.invoice //"invoice"
      ? {
          "paA": payableAmount >= 0 ? payableAmount : 0, //"payableAmount"//Need Io Check
          // "couponApplied": bookingData?.invoice?.couponApplied ?? 0,
          // "couponAmount": bookingData?.invoice?.couponAmount ?? 0,
          // "pendingPaymentAmount":
          //   bookingData?.invoice?.pendingPaymentAmount ?? 0,
          "aEA": bookingData?.invoice?.actualEstimationAmount ?? 0,
          "pC": bookingData?.invoice?.professionalCommision || 0,
          "iARWC": bookingData?.invoice?.isAfterRideWaitingCharge ?? false, // "isAfterRideWaitingCharge"
          "iEAC": bookingData?.invoice?.isEnableAssistanceCare ?? false, // "isEnableAssistanceCare"
        }
      : {},
    "ac": bookingData.activity //"activity"
      ? {
          "rSU": bookingData.activity.rideStopsUpdated || false,
          "pUT": bookingData.activity.pickUpTime, //"pickUpTime"
          "dT": bookingData.activity.dropTime, // "dropTime"
          "aT": bookingData.activity.arriveTime, // "arriveTime"
          "rS": rideStops, // "rideStops"
        }
      : {},
    // "cancellationReason": bookingData?.cancellationReason ?? "",
    "cRD": bookingData?.cancelReasonDescription ?? "", //"cancelReasonDescription"
    "bS": bookingData?.bookingStatus ?? "", //"bookingStatus"
    "dU": bookingData?.distanceUnit ?? constantUtil.KM, //"distanceUnit"
    "tRC": bookingData.totalRetryCount ?? 2, //"totalRetryCount"
    "rT": bookingData?.retryTime ?? 10, //"retryTime"
    "iW": bookingData?.isWished ?? false, //"isWished"
    "rMRI": bookingData?.rideMapRouteImage ?? "", //"rideMapRouteImage"
    "sAI": parseInt(60 * 60), //"scheduleAcceptInterval"
    "tL": bookingData?.trackLink ?? "", //"trackLink"
    // "isRestrictMaleBookingToFemaleProfessional":
    //   !!bookingData?.isRestrictMaleBookingToFemaleProfessional,
    // "isNeedCallMasking": bookingData?.isNeedCallMasking ?? false, // Need to Check
    "iNSIU": bookingData?.category?.isNeedSecurityImageUpload ?? false, //"isNeedSecurityImageUpload"
  };
  // });
};

//-----------
mappingUtils.bookingObjectDetails = (bookingData) => {
  // const imageURL =
  //   generalSettings.data.spaces.spacesBaseUrl +
  //   "/" +
  //   generalSettings.data.spaces.spacesObjectName;
  const imageURL = bookingData.imageURLPath || "";
  //-------- rideStops Start --------
  // const rideStops = [];
  // const rideStopsList = bookingData?.activity || [];
  // console.log(rideStopsList);
  // console.log(bookingData?.activity.rideStops);
  // rideStopsList.rideStops.forEach((rideStops) => {
  //   const jsonOutput = {
  //     "sts": rideStops.status, //"status"
  //     "typ": rideStops.type, //"type"
  //     "_id": rideStops._id,
  //     "aN": rideStops.addressName, //"addressName"
  //     "fA": rideStops.fullAddress, //"fullAddress"
  //     "sA": rideStops.shortAddress, //"shortAddress"
  //     "la": rideStops.lat, //"lat"
  //     "ln": rideStops.lng, //"lng"
  //   };
  //   rideStops.push(jsonOutput);
  // });
  const rideStops = bookingData?.activity?.rideStops;
  const paymentOption =
    (bookingData?.payment?.option ?? "") === constantUtil.CREDIT
      ? constantUtil.CORPORATECREDIT
      : bookingData?.payment?.option ?? "";
  const isPrepaidRide = bookingData?.payment?.isPrepaidRide || false;
  // console.log(rideStops);
  //-----------------------------
  const payableAmount =
    bookingData.bookingStatus === constantUtil.AWAITING
      ? bookingData?.invoice?.actualEstimationAmount || 0
      : bookingData?.invoice?.payableAmount || 0; //"payableAmount"//Need to Check
  // "couponApplied": bookingData?.invoice?.couponApplied ?? 0,
  //-------- rideStops End --------
  return {
    "_id": bookingData?._id ?? "",
    "bookingId": bookingData?.bookingId ?? "",
    // "bookingOTP": bookingData?.bookingOTP ?? "",
    "bookingOTP":
      bookingData.isOtpNeeded === false ||
      (bookingData.serviceCategory ===
        constantUtil.CONST_PACKAGES.toLowerCase() &&
        (bookingData?.packageData?.isEnableOTPVerification || false) === false)
        ? ""
        : bookingData?.bookingOTP, //"bookingOTP"
    "bookingType": bookingData?.bookingType ?? "",
    "bookingDate": bookingData?.bookingDate ?? "",
    "bookingFor": bookingData?.bookingFor || {},
    "bookingBy": bookingData?.bookingBy || "USER",
    "serviceCategory": bookingData?.serviceCategory || "",
    // "isSecurityAvail": bookingData?.isSecurityAvail ?? "", // Need to Check
    "user": bookingData.user
      ? {
          "_id": bookingData?.user?._id ?? "",
          "firstName": bookingData?.user?.firstName || "No Name",
          "lastName": bookingData?.user?.lastName || "",
          "phone": bookingData?.user?.phone || {},
          "avatar": (bookingData?.user?.avatar || "")
            .replace(imageURL, "")
            ?.toString("utf8"),
          // "escortInfo": bookingData?.user?.escortInfo ?? {},  // Need to Check
          "bookingInfo": bookingData?.user?.bookingInfo || {},
          "wallet": bookingData?.user?.wallet || {},
          "avgRating": bookingData?.user?.review?.avgRating ?? null,
          "nationalIdNo": bookingData?.user?.nationalIdNo ?? null,
        }
      : {},
    "isAdminForceAssign": bookingData?.isAdminForceAssign ?? false,
    // "isAdminAssigned": bookingData.adminAssignedProfessional ? true : false,
    "rideAction": bookingData.adminAssignedProfessional
      ? constantUtil.ACTION_ASSIGN
      : bookingData?.isAdminForceAssign
      ? constantUtil.FORCEASSIGNBYADMIN
      : constantUtil.NONE,
    "isAcceptOtherCategory": bookingData.isAcceptOtherCategory ?? false,
    "professional": bookingData.professional
      ? {
          "_id": bookingData?.professional?._id ?? "",
          "firstName": bookingData?.professional?.firstName ?? "",
          "lastName": bookingData?.professional?.lastName ?? "",
          "phone": bookingData?.professional?.phone || {},
          "avatar": (bookingData?.professional?.avatar || "")
            .replace(imageURL, "")
            ?.toString("utf8"),
          "avgRating": bookingData?.professional?.review?.avgRating ?? null,
          "lat": bookingData?.professional?.location?.coordinates[1] ?? null,
          "lng": bookingData?.professional?.location?.coordinates[0] ?? null,
          "wallet": bookingData?.professional.wallet || {},
          "bookingInfo": bookingData?.professional?.bookingInfo || {},
          "onlineStatus": bookingData?.professional?.onlineStatus || true,
          "nationalIdNo": bookingData?.professional?.nationalIdNo ?? null,
        }
      : {},
    "officer": bookingData.officer
      ? {
          "_id": bookingData?.officer?._id ?? "",
          "firstName": bookingData?.officer?.firstName ?? "",
          "lastName": bookingData?.officer?.lastName ?? "",
          "phone": bookingData?.officer?.phone ?? {},
          "avatar": (bookingData?.officer?.avatar ?? "")
            .replace(imageURL, "")
            ?.toString("utf8"),
          "avgRating": bookingData?.officer?.review?.avgRating ?? null,
          "lat": bookingData?.officer?.location?.coordinates[1] ?? null,
          "lng": bookingData?.officer?.location?.coordinates[0] ?? null,
        }
      : {},
    "security": bookingData.security
      ? {
          "_id": bookingData?.security?._id ?? "",
          "acknowledge": bookingData?.security?.acknowledge ?? "",
          "isEscortApply": bookingData?.security?.isEscortApply ?? "",
          "escortStatus": bookingData?.security?.escortStatus || {},
          "escortReview": bookingData?.security?.escortReview || {},
          "isAdminForceAssign": bookingData?.security?.isAdminForceAssign ?? "",
          "userType": bookingData?.security?.userType ?? "",
          "securityServiceName":
            bookingData?.security?.securityServiceName ?? "",
          "securityServiceId": bookingData?.security?.securityServiceId ?? "",
          "threadMessage": bookingData?.security?.threadMessage ?? "",
          "alertType": bookingData?.security?.alertType ?? "",
          "alertDate": bookingData?.security?.alertDate ?? "",
          "alertLocation": bookingData?.security?.alertLocation ?? "",
          "securityThreadId": bookingData?.security?.securityThreadId ?? null,
        }
      : {},
    "vehicle": bookingData.vehicle
      ? {
          "vehicleCategoryId": bookingData?.vehicle?.vehicleCategoryId ?? "",
          "vehicleCategoryName": bookingData.vehicle?.vehicleCategoryName ?? "",
          "vehicleCategoryImage": (
            bookingData.vehicle?.vehicleCategoryImage ?? ""
          ).replace(imageURL, ""),
          "model": bookingData.vehicle?.model ?? "",
          "plateNumber": bookingData.vehicle?.plateNumber ?? "",
          "color": bookingData.vehicle?.color ?? null,
          "vehicleCategoryMapImage": (
            bookingData.vehicle?.vehicleCategoryMapImage ?? ""
          ).replace(imageURL, ""),
        }
      : {},
    "bookingReview": bookingData?.bookingReview || {},
    "manualDistance": bookingData?.manualDistance || null,
    "estimation": bookingData?.estimation || {},
    // "bookedEstimation": bookingData?.bookedEstimation ?? {},
    "currencySymbol": bookingData?.currencySymbol ?? "$",
    "currencyCode": bookingData?.currencyCode || "USD",
    "payment": bookingData.payment
      ? {
          "option": isPrepaidRide === true ? constantUtil.PAID : paymentOption,
          "paid": bookingData?.payment?.paid ?? "",
          "paymentId": bookingData?.payment?.paymentId ?? "",
        }
      : {},
    "invoice": bookingData.invoice
      ? {
          "payableAmount": payableAmount >= 0 ? payableAmount : 0, //bookingData?.invoice?.payableAmount ?? 0,
          // "couponApplied": bookingData?.invoice?.couponApplied ?? 0,
          // "couponAmount": bookingData?.invoice?.couponAmount ?? 0,
          // "pendingPaymentAmount":
          //   bookingData?.invoice?.pendingPaymentAmount ?? 0,
          "actualEstimationAmount":
            bookingData?.invoice?.actualEstimationAmount ?? 0,
          "professionalCommision":
            bookingData?.invoice?.professionalCommision || 0,
          "isAfterRideWaitingCharge":
            bookingData?.invoice?.isAfterRideWaitingCharge ?? false,
          "isEnableAssistanceCare":
            bookingData?.invoice?.isEnableAssistanceCare ?? false,
          "beforeRideAssistanceCareNeededMins":
            bookingData?.invoice?.beforeRideAssistanceCareNeededMins || 0,
          "afterRideAssistanceCareNeededMins":
            bookingData?.invoice?.afterRideAssistanceCareNeededMins || 0,
          "assistanceCareAmount":
            bookingData?.invoice?.assistanceCareAmount || 0,
          "assistanceCareNotes": bookingData?.invoice?.assistanceCareNotes,
          "fareSettings": bookingData?.invoice?.fareSettings || [],
          "peakFareText": bookingData?.invoice?.peakFareText || "",
          "peakFare": bookingData?.invoice?.peakFare || 0,
          "nightFare": bookingData?.invoice?.nightFare || 0,
          "multiStopCharge": bookingData?.invoice?.multiStopCharge || 0,
          "tipsAmount": bookingData?.invoice?.tipsAmount || 0,
          "airportFareAmount": bookingData?.invoice?.airportFareAmount || 0,
          "isFixedPayableAmount":
            bookingData?.invoice?.isFixedPayableAmount || false,
        }
      : {},
    "activity": bookingData.activity
      ? {
          "rideStopsUpdated": bookingData.activity.rideStopsUpdated || false,
          "pickUpTime": bookingData.activity.pickUpTime,
          "dropTime": bookingData.activity.dropTime,
          "arriveTime": bookingData.activity.arriveTime,
          "rideStops": rideStops, //bookingData.activity.rideStops,
        }
      : {},
    // "cancellationReason": bookingData?.cancellationReason ?? "",
    "cancelReasonDescription": bookingData?.cancelReasonDescription ?? "",
    "bookingStatus": bookingData?.bookingStatus ?? "",
    "distanceUnit": bookingData?.distanceUnit || constantUtil.KM,
    "totalRetryCount": bookingData.totalRetryCount ?? 2,
    "retryTime": bookingData?.retryTime ?? 10,
    "isWished": bookingData?.isWished ?? false,
    "rideMapRouteImage": bookingData?.rideMapRouteImage ?? "",
    "scheduleAcceptInterval": parseInt(60 * 60),
    "trackLink": bookingData?.trackLink ?? undefined,
    // "isRestrictMaleBookingToFemaleProfessional":
    //   !!bookingData?.isRestrictMaleBookingToFemaleProfessional,
    // "isNeedCallMasking": bookingData?.isNeedCallMasking ?? false, // Need to Check
    "isNeedSecurityImageUpload":
      bookingData?.category?.isNeedSecurityImageUpload || false,
    "packageData": bookingData?.packageData || {},
    "rideCoordinatesEncoded": bookingData?.rideCoordinatesEncoded,
    "notes": bookingData?.notes,
  };
  // });
};
//--------

mappingUtils.securityObject = (securityData) => {
  return {
    "_id": securityData._id,
    "user": {
      "_id": securityData?.user?._id || "",
      "firstName": securityData?.user?.firstName || "",
      "lastName": securityData?.user?.lastName || "",
      "phone": securityData?.user?.phone || {},
      "avatar": (securityData?.user?.avatar || "")?.toString("utf8"),
      "avgRating": securityData?.user?.review?.avgRating || null,
    },
    "professional": {
      "_id": securityData?.professional?._id || "",
      "firstName": securityData.professional.firstName || "",
      "lastName": securityData.professional.lastName || "",
      "phone": securityData?.professional?.phone || {},
      "avatar": (securityData?.professional?.avatar || "")?.toString("utf8"),
      "avgRating": securityData?.professional?.review?.avgRating || null,
    },
    "officer": {
      "_id": securityData.officer._id || "",
      "firstName": securityData.officer.firstName || "",
      "lastName": securityData.officer.lastName || "",
      "phone": securityData.officer.phone || {},
      "avatar": (securityData.officer.avatar || "")?.toString("utf8"),
      "avgRating": securityData.officer.review.avgRating || null,
    },
    "operator": {
      "_id": securityData?.operator?._id || "",
      "firstName": securityData?.operator?.data?.firstName || "",
      "lastName": securityData?.operator?.data?.lastName || "",
      "phone": securityData?.operator?.data?.phone || {},
      "avatar": (securityData.operator.data.avatar || "")?.toString("utf8"),
    },
    "security": {
      "_id": securityData?._id || "",
      "acknowledge": securityData?.acknowledge || {},
      "isEscortApply": securityData?.isEscortApply || false,
      "escortStatus": securityData?.escortStatus || "",
      "activity": securityData?.activity || {},
      "fromLocation": securityData?.fromLocation || {},
      "toLocation": securityData?.toLocation || {},
      "isAdminForceAssign": securityData?.isAdminForceAssign || false,
      "userType": securityData?.userType || "",
      "priority": securityData?.priority || "",
      "status": securityData?.status || "",
      "adminNotes": securityData?.adminNotes || "",
      "securityServiceName": securityData.securityServiceName || "",
      "securityServiceId": securityData?.securityServiceId || "",
      "threadMessage": securityData?.threadMessage || "",
      "alertType": securityData?.alertType || "",
      "alertDate": securityData?.alertDate || "",
      "alertLocation": securityData?.alertLocation || {},
      "securityThreadId": securityData?.securityThreadId || null,
      "ticketId": securityData?.ticketId || "",
      "mediaType": securityData?.mediaType || "",
      "mediaReportMessage": securityData?.mediaReportMessage || null,
      "mediaReportImage": securityData?.mediaReportImage || [],
      "mediaReportVideo": securityData?.mediaReportVideo || null,
      "mediaReportAudio": securityData?.mediaReportAudio || null,
    },
    "booking": {
      "_id": securityData?.booking?._id || "",
      "bookingDate": securityData?.booking?.bookingDate || "",
      "bookingId": securityData?.booking?.bookingId || "",
      "bookingOTP": securityData?.booking?.bookingOTP || "",
      "bookingType": securityData?.booking?.bookingType || "",
      "bookingFor": securityData?.booking?.bookingFor || {},
      "bookingBy": securityData?.booking?.bookingBy || "",
      "vehicle": securityData?.booking?.vehicle || {},
      "origin": securityData?.booking?.origin || {},
      "destination": securityData?.booking?.destination || {},
      "estimation": securityData?.booking?.estimation || {},
      "currencySymbol": securityData?.booking?.currencySymbol || "",
      "currencyCode": securityData?.booking?.currencyCode || "",
      "payment": securityData?.booking?.payment || {},
      "invoice": securityData?.booking?.invoice || {},
      "activity": securityData?.booking?.activity || {},
      "cancellationReason": securityData?.booking?.cancellationReason || "",
      "bookingStatus": securityData?.booking?.bookingStatus || "",
      "distanceUnit": securityData?.booking?.distanceUnit || constantUtil.KM,
      "totalRetryCount": securityData?.booking?.totalRetryCount || 2,
      "retryTime": securityData?.booking?.retryTime || 10,
      "isWished": securityData?.booking?.isWished || false,
      "rideMapRouteImage": securityData?.booking?.rideMapRouteImage || "",
      "scheduleAcceptInterval": parseInt(60 * 60),
    },
  };
};

mappingUtils.securityBookingViewObject = (securityData) => {
  return {
    "_id": securityData._id,
    "user": securityData.user
      ? {
          "_id": securityData?.user?._id || "",
          "bookingInfo": securityData?.user?.bookingInfo || "",
          "escortInfo": securityData?.user?.escortInfo || "",
          "firstName": securityData?.user?.firstName || "",
          "lastName": securityData?.user?.lastName || "",
          "phone": securityData?.user?.phone || {},
          "avatar": (securityData.user.avatar || "")?.toString("utf8"),
          "avgRating":
            securityData?.manageHeatmap?.user?.review?.avgRating || null,
        }
      : {},
    "professional": securityData.professional
      ? {
          "_id": securityData?.professional?._id || "",
          "bookingInfo": securityData?.professional?.bookingInfo || "",
          "escortInfo": securityData?.professional?.escortInfo || "",
          "firstName": securityData?.professional?.firstName || "",
          "lastName": securityData?.professional?.lastName || "",
          "phone": securityData?.professional?.phone || {},
          "avatar": (securityData?.professional?.avatar || "")?.toString(
            "utf8"
          ),
          "avgRating": securityData?.professional?.review?.avgRating || null,
          "wallet": securityData?.professional?.wallet || "",
        }
      : {},
    "officer": securityData.officer
      ? {
          "_id": securityData?.officer?._id || "",
          "firstName": securityData.officer.firstName || "",
          "lastName": securityData?.officer?.lastName || "",
          "phone": securityData?.officer?.phone || {},
          "avatar": (securityData?.officer.avatar || "")?.toString("utf8"),
          "bookingInfo": securityData?.officer?.bookingInfo || "",
          "avgRating": securityData?.officer?.review?.avgRating || null,
          "lat": securityData?.officer?.location?.coordinates?.[1] || null,
          "lng": securityData?.officer?.location?.coordinates?.[0] || null,
        }
      : {},
    "operator": securityData.operator
      ? {
          "_id": securityData?.operator?._id || "",
          "firstName": securityData?.operator?.data?.firstName || "",
          "lastName": securityData?.operator?.data?.lastName || "",
          "phone": securityData?.operator?.data?.phone || {},
          "avatar": (securityData?.operator?.data?.avatar || "")?.toString(
            "utf8"
          ),
        }
      : {},
    "security": securityData
      ? {
          "_id": securityData?._id || "",
          "acknowledge": securityData?.acknowledge || "",
          "isEscortApply": securityData?.isEscortApply || false,
          "escortStatus": securityData?.escortStatus || {},
          "escortReview": securityData?.escortReview || {},
          "activity": securityData?.activity || {},
          "fromLocation": securityData?.fromLocation || {},
          "toLocation": securityData?.toLocation || {},
          "isAdminForceAssign": securityData?.isAdminForceAssign || false,
          "userType": securityData?.userType || "",
          "priority": securityData?.priority || "",
          "status": securityData?.status || "",
          "adminNotes": securityData?.adminNotes || "",
          "securityServiceName": securityData?.securityServiceName || "",
          "securityServiceId": securityData?.securityServiceId || "",
          "threadMessage": securityData?.threadMessage || "",
          "alertType": securityData?.alertType || "",
          "alertDate": securityData?.alertDate || "",
          "alertLocation": securityData?.alertLocation || "",
          "securityThreadId": securityData?.securityThreadId || null,
          "ticketId": securityData?.ticketId || null,
          "mediaType": securityData.mediaType || "",
          "mediaReportMessage": securityData?.mediaReportMessage || "",
          "mediaReportImage": securityData?.mediaReportImage || [],
          "mediaReportVideo": securityData?.mediaReportVideo || "",
          "mediaReportAudio": securityData?.mediaReportAudio || "",
          "retryTime": securityData?.retryTime || 20,
        }
      : {},
  };
};

mappingUtils.reportObject = (reportData) => {
  return {
    "status": reportData.data.status,
    "firstName": reportData.data.firstName,
    "lastName": reportData.data.lastName,
    "phone": {
      "code": reportData.data.phone.code,
      "number": reportData.data.phone.number,
    },
    "comment": reportData.data.comment,
    "adminNotes": reportData.data.adminNotes,
    "commentDate": reportData.createdAt,
  };
};
mappingUtils.otpMailObject = (obj) => {
  return {
    "phoneNumber": obj.phone.number,
    "phoneCode": obj.phone.code,
    "walletAvailableAmount": obj.wallet.availableAmount,
    "firstName": obj.firstName,
    "lastName": obj.lastName,
    "email": obj.email,
    "otp": obj.otp,
    "emailOTP": obj.emailOTP,
  };
};
mappingUtils.userWelcomeMailObject = (obj) => {
  return {
    "phone": obj.phone.code + " " + obj.phone.number,
    "walletAvailableAmount": obj.wallet.availableAmount,
    "firstName": obj.firstName,
    "lastName": obj.lastName,
    "email": obj.email,
  };
};
mappingUtils.professionalWelcomeMailObject = (obj) => {
  return {
    "phone": obj.phone.code + "" + obj.phone.number,
    "walletAvailableAmount": obj.wallet.availableAmount,
    "firstName": obj.firstName,
    "lastName": obj.lastName,
    "email": obj.email,
    "address": `${obj.address.line} ${obj.address.city} ${obj.address.state} ${obj.address.country}`,
    "age": obj.age,
    "dob": obj.dob,
  };
};
mappingUtils.professionalDocumentExpiredMailObject = (obj) => {
  return {
    "phone": obj.phone.code + "" + obj.phone.number,
    "firstName": obj.firstName,
    "lastName": obj.lastName,
    "email": obj.email,
    "documentName": obj.documentName,
    "documentType": obj.documentType,
    "reason": obj.reason,
  };
};

mappingUtils.adminOtpMailObject = (admin) => {
  return {
    "clientId": admin.clientId,
    "phoneNumber": admin.phone.number,
    "phoneCode": admin.phone.code,
    "firstName": admin.firstName,
    "lastName": admin.lastName,
    "email": admin.email,
    "otp": admin.otp,
    "emailOTP": admin.emailOTP,
  };
};
mappingUtils.userCancelBookingEmailObjectWithCancellationAmount = (
  bookingData
) => {
  return {
    "bookingId": bookingData.bookingId,
    "cancelTime": bookingData.activity.cancelTime,
    "bookingForName": bookingData.bookingFor.name,
    "bookingForNumber": bookingData.bookingFor.number,
    "userName": bookingData.user.firstName + " " + bookingData.user.lastName,
    "email": bookingData.user.email,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "estimationAmount": bookingData.invoice.estimationAmount,
    "cancellationAmount": bookingData.cancellationAmount,
    "cancellationMinute": bookingData.invoice.cancellationMinute,
    "cancellationReason": bookingData.cancellationReason,
    "cancelledBy":
      bookingData.activity?.cancelledBy?.userType === constantUtil.USER
        ? "YOU"
        : bookingData.activity?.cancelledBy?.userType || "",
  };
};
mappingUtils.professionalCancelBookingEmailObjectWithCancelAmount = (
  bookingData
) => {
  return {
    "bookingId": bookingData.bookingId,
    "name":
      bookingData.professional.firstName +
      " " +
      bookingData.professional.lastName,
    "cancelTime": bookingData.activity.cancelTime,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "estimationAmount": bookingData.invoice.estimationAmount,
    "cancellationAmount": bookingData.invoice.professionalCancellationAmount,
    "cancelledBy":
      bookingData.activity?.cancelledBy?.userType === constantUtil.PROFESSIONAL
        ? "YOU"
        : bookingData?.activity?.cancelledBy?.userType || "",
    "cancellationMinute": bookingData.invoice.professionalCancellationMinute,
    "cancellationReason": bookingData.cancellationReason,
    "email": bookingData.professional.email,
  };
};
mappingUtils.professionalInvoiceMailObject = (bookingData) => {
  return {
    "bookingId": bookingData.bookingId,
    "baseFare": bookingData.invoice.baseFare,
    "farePerDistance": bookingData.invoice.farePerDistance,
    "farePerMinute": bookingData.invoice.farePerMinute,
    "peakFare": bookingData.invoice.peakFare,
    "nightFare": bookingData.invoice.nightFare,
    "multiStopCharge": bookingData.invoice.multiStopCharge,
    "surchargeFee": bookingData.invoice.surchargeFee || "0",
    "waitingChargePerMin": bookingData.invoice.waitingChargePerMin || "0",
    "waitingCharge": bookingData.invoice.waitingCharge || "0",
    "estimationAmount": bookingData.invoice.estimationAmount || "0",
    "travelCharge": bookingData.invoice.travelCharge,
    "distanceFare": bookingData.invoice.distanceFare,
    "paidAmount": bookingData.invoice.payableAmount,
    "serviceTax": bookingData.invoice.serviceTax,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "name":
      (bookingData?.professional?.firstName || "") +
      " " +
      (bookingData?.professional?.lastName || ""),
    "userName":
      bookingData.guestType === constantUtil.USER
        ? (bookingData?.user?.firstName || "") +
          " " +
          (bookingData?.user?.lastName || "")
        : bookingData.bookingFor.name,
    "email": bookingData?.professional?.email || "",
    "bookingDate": bookingData.bookingDate,
  };
};
mappingUtils.userInvoiceMailObject = (bookingData) => {
  return {
    "bookingId": bookingData.bookingId,
    "baseFare": bookingData.invoice.baseFare,
    "farePerDistance": bookingData.invoice.farePerDistance,
    "farePerMinute": bookingData.invoice.farePerMinute,
    "peakFare": bookingData.invoice.peakFare,
    "nightFare": bookingData.invoice.nightFare,
    "multiStopCharge": bookingData.invoice.multiStopCharge,
    "surchargeFee": bookingData.invoice.surchargeFee || "0",
    "waitingChargePerMin": bookingData.invoice.waitingChargePerMin || "0",
    "waitingCharge": bookingData.invoice.waitingCharge || "0",
    "estimationAmount": bookingData.invoice.estimationAmount || "0",
    "travelCharge": bookingData.invoice.travelCharge || "0",
    "distanceFare": bookingData.invoice.distanceFare,
    "payableAmount": bookingData.invoice.payableAmount || "0",
    "serviceTax": bookingData.invoice.serviceTax,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "name":
      (bookingData?.user?.firstName || "") +
      " " +
      (bookingData?.user?.lastName || ""),
    "professionalName":
      (bookingData?.professional?.firstName || "") +
      " " +
      (bookingData?.professional?.lastName || ""),
    "email": bookingData?.user?.email || "",
    "bookingDate": bookingData.bookingDate,
  };
};
mappingUtils.corporateInvoiceMailObject = (bookingData) => {
  return {
    "bookingId": bookingData.bookingId,
    "baseFare": bookingData.invoice.baseFare,
    "farePerDistance": bookingData.invoice.farePerDistance,
    "farePerMinute": bookingData.invoice.farePerMinute,
    "peakFare": bookingData.invoice.peakFare,
    "nightFare": bookingData.invoice.nightFare,
    "multiStopCharge": bookingData.invoice.multiStopCharge,
    "surchargeFee": bookingData.invoice.surchargeFee || "0",
    "waitingChargePerMin": bookingData.invoice.waitingChargePerMin || "0",
    "waitingCharge": bookingData.invoice.waitingCharge || "0",
    "estimationAmount": bookingData.invoice.estimationAmount || "0",
    "travelCharge": bookingData.invoice.travelCharge || "0",
    "distanceFare": bookingData.invoice.distanceFare,
    "payableAmount": bookingData.invoice.payableAmount || "0",
    "serviceTax": bookingData.invoice.serviceTax,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "bookingFor": bookingData.bookingFor.name,
    "professionalName":
      (bookingData?.professional?.firstName || "") +
      " " +
      (bookingData?.professional?.lastName || ""),
    "email":
      bookingData?.coorperate?.data?.email || bookingData?.corporateEmailId,
    "corporateOfficeName": bookingData.coorperate.data.officeName,
    "corporateOfficeId": bookingData.coorperate.data.officeId,
    "bookingDate": bookingData.bookingDate,
  };
};
mappingUtils.promtionMailObject = async function (userData) {
  return {
    "name": userData.firstName + " " + userData.lastName,
    "fullName": userData.firstName + " " + userData.lastName,
    "email": userData.email,
    "phone": userData.phone.code + " " + userData.phone.number,
    "walletAmount": userData.wallet.availableAmount,
    "trackUrl": userData.trackUrl,
    "siteTitle": userData.siteTitle,
    "reciverName": userData.reciverName,
    "senderName": userData.senderName,
  };
};
mappingUtils.rideExpiredEmailObject = async function (bookingData) {
  return {
    "name": bookingData.user.firstName + " " + bookingData.user.lastName,
    "email": bookingData.user.email,
    "bookingId": bookingData.bookingId,
    "baseFare": bookingData.invoice.baseFare,
    "farePerDistance": bookingData.invoice.farePerDistance,
    "farePerMinute": bookingData.invoice.farePerMinute,
    "peakFare": bookingData.invoice.peakFare,
    "nightFare": bookingData.invoice.nightFare,
    "surchargeFee": bookingData.invoice.surchargeFee || "0",
    "waitingChargePerMin": bookingData.invoice.waitingChargePerMin || "0",
    "waitingCharge": bookingData.invoice.waitingCharge || "0",
    "estimationAmount": bookingData.invoice.estimationAmount || "0",
    "travelCharge": bookingData.invoice.travelCharge || "0",
    "distanceFare": bookingData.invoice.distanceFare,
    "payableAmount": bookingData.invoice.payableAmount || "0",
    "serviceTax": bookingData.invoice.serviceTax,
    "originAddress": bookingData.origin.fullAddress,
    "destinationAddress": bookingData.destination.fullAddress,
    "expiredTime": bookingData.activity.denyOrExpireTime,
  };
};

// functions mapping
mappingUtils.mapObject = (data) => {
  const final = [];
  for (const key in data) {
    final.push({
      "title": key,
      "value": data[key],
    });
  }
  return final;
};

mappingUtils.calculateInvoiceObject = (bookingData) => {
  let pendingPaymentAmount = 0,
    actualEstimationAmount = 0,
    estimationPayableAmount = 0,
    couponApplied = false,
    couponValue = 0,
    couponType = "",
    couponAmount = 0,
    isProfessionalTolerance = true;

  //#region Default Values
  let isEnableDynamicTravelCharge =
    bookingData.vehicleCategoryData?.isEnableDynamicTravelCharge || false;
  let baseFare = bookingData.vehicleCategoryData.baseFare;
  //farePerDistance
  let farePerDistance = bookingData.vehicleCategoryData?.farePerDistance || 0;
  let farePerDistance1 = bookingData.vehicleCategoryData?.farePerDistance1 || 0;
  let farePerDistance2 = bookingData.vehicleCategoryData?.farePerDistance2 || 0;
  let farePerDistance3 = bookingData.vehicleCategoryData?.farePerDistance3 || 0;
  let farePerDistance4 = bookingData.vehicleCategoryData?.farePerDistance4 || 0;
  //farePerMinute
  let farePerMinute = bookingData.vehicleCategoryData?.farePerMinute || 0;
  let farePerMinute1 = bookingData.vehicleCategoryData?.farePerMinute1 || 0;
  let farePerMinute2 = bookingData.vehicleCategoryData?.farePerMinute2 || 0;
  let farePerMinute3 = bookingData.vehicleCategoryData?.farePerMinute3 || 0;
  let farePerMinute4 = bookingData.vehicleCategoryData?.farePerMinute4 || 0;
  //fromDistance
  let fromDistance = bookingData.vehicleCategoryData?.fromDistance || 0;
  let fromDistance1 = bookingData.vehicleCategoryData?.fromDistance1 || 0;
  let fromDistance2 = bookingData.vehicleCategoryData?.fromDistance2 || 0;
  let fromDistance3 = bookingData.vehicleCategoryData?.fromDistance3 || 0;
  let fromDistance4 = bookingData.vehicleCategoryData?.fromDistance4 || 0;
  //toDistance
  let toDistance = bookingData.vehicleCategoryData?.toDistance || 0;
  let toDistance1 = bookingData.vehicleCategoryData?.toDistance1 || 0;
  let toDistance2 = bookingData.vehicleCategoryData?.toDistance2 || 0;
  let toDistance3 = bookingData.vehicleCategoryData?.toDistance3 || 0;
  let toDistance4 = bookingData.vehicleCategoryData?.toDistance4 || 0;
  //
  const isPeakTiming = bookingData?.surge?.isPeakTiming || false;
  const isNightTiming = bookingData?.surge?.isNightTiming || false;
  const actualPeakFare = bookingData?.surge?.actualPeakFare || 0;
  const heatmapPeakFare = bookingData?.surge?.heatmapPeakFare || 0;
  // let peakFare =
  //   bookingData?.surge?.peakTimeFare ||
  //   bookingData.vehicleCategoryData.peakFare;
  // let nightFare =
  //   bookingData?.surge?.nightTimeFare ||
  //   bookingData.vehicleCategoryData.nightFare;
  let peakFare = bookingData?.surge?.peakTimeFare || 0;
  let nightFare = bookingData?.surge?.nightTimeFare || 0;
  //
  // let sharePercent = bookingData.vehicleCategoryData.sharePercent;
  let minimumChargeStatus =
    bookingData.vehicleCategoryData?.minimumCharge?.status || false;
  let minimumChargeAmount = minimumChargeStatus
    ? bookingData.vehicleCategoryData.minimumCharge.amount
    : 0;
  let isMinimumChargeApplied = bookingData.isMinimumChargeApplied || false;
  let isSurchargeEnable =
    bookingData.vehicleCategoryData?.isSurchargeEnable || false;
  let surchargeFee = isSurchargeEnable
    ? bookingData.vehicleCategoryData.surchargeFee
    : 0;
  let surchargeTitle =
    bookingData.vehicleCategoryData?.surchargeTitle || "Surcharge";
  // let waitingChargePerMin =
  //   bookingData.vehicleCategoryData.waitingChargePerMin || 0;
  let estimationAmount = bookingData.estimationAmount;
  let travelCharge = bookingData.travelCharge || 0;
  let distanceFare = bookingData.distanceFare || 0;
  let timeFare = bookingData.timeFare || 0;
  //----------- Intercity Start ------------------
  let intercityBoundaryFareType =
    bookingData.intercityBoundaryFareType || "NONE";
  let isEnableRoundTrip = bookingData.isEnableRoundTrip || false;
  let intercityBoundaryFare = bookingData.intercityBoundaryFare || 0;
  let intercityPickupFarePerDistance =
    bookingData.intercityPickupFarePerDistance || 0;
  let intercityPickupFarePerMinites =
    bookingData.intercityPickupFarePerMinites || 0;
  let intercityPickupDistanceFare =
    bookingData.intercityPickupDistanceFare || 0;
  let intercityPickupTimeFare = bookingData.intercityPickupTimeFare || 0;
  let intercityPickupCharge = bookingData.intercityPickupCharge || 0;
  //----------- Intercity Start ------------------
  let serviceTaxPercentage =
    bookingData.vehicleCategoryData.serviceTaxPercentage || 0;
  let serviceTax = bookingData.serviceTax;
  let payableAmount = bookingData.estimationAmount;
  let siteCommissionPercentage =
    bookingData.vehicleCategoryData.siteCommission || 0;
  let siteCommission = bookingData.siteCommisionValue + bookingData.serviceTax;
  let siteCommissionWithoutTax = bookingData.siteCommisionValue;
  let professionalCommision =
    bookingData.estimationAmount -
    (bookingData.siteCommisionValue + bookingData.serviceTax);
  let professionalCommisionWithoutTips =
    bookingData.estimationAmount -
    (bookingData.siteCommisionValue + bookingData.serviceTax);
  const airportFareAmount = bookingData.airportFareAmount || 0;
  const stopCharge = bookingData.vehicleCategoryData.multiStopCharge || 0; //Charge Per Stop
  //--------- before Ride Waiting Charge Start --------------------
  let isBeforeRideWaitingCharge =
    bookingData.vehicleCategoryData?.waitingChargeDetails
      ?.isBeforeRideWaitingCharge || false;
  let beforeRideWaitingChargePerMin = isBeforeRideWaitingCharge
    ? bookingData.vehicleCategoryData.waitingChargeDetails
        .beforeRideWaitingChargePerMin
    : 0;
  let beforeRideWaitingGracePeriod = isBeforeRideWaitingCharge
    ? bookingData.vehicleCategoryData?.waitingChargeDetails
        ?.beforeRideWaitingGracePeriod
    : 0;
  //--------- before Ride Waiting Charge end --------------------
  //--------- after Ride Waiting Charge Start --------------------
  let isAfterRideWaitingCharge =
    bookingData.vehicleCategoryData?.waitingChargeDetails
      ?.isAfterRideWaitingCharge || false;
  let afterRideWaitingChargePerMin = isAfterRideWaitingCharge
    ? bookingData.vehicleCategoryData.waitingChargeDetails
        .afterRideWaitingChargePerMin
    : 0;
  let afterRideWaitingGracePeriod = isAfterRideWaitingCharge
    ? bookingData.vehicleCategoryData?.waitingChargeDetails
        ?.afterRideWaitingGracePeriod
    : 0;
  //--------- after Ride Waiting Charge End --------------------
  const isFixedPayableAmount = bookingData.isFixedPayableAmount || false;
  //
  let cancellationFeeCreditToUserPrecentage =
    bookingData.vehicleCategoryData?.cancellationFeeCreditToUserPrecentage || 0;
  let cancellationFeeCreditToProfessionalPrecentage =
    bookingData.vehicleCategoryData
      ?.cancellationFeeCreditToProfessionalPrecentage || 0;
  const roundingType = bookingData?.roundingType || constantUtil.CONST_INTEGER;
  const roundingFactor = bookingData?.roundingFactor || 0;
  let sharePercentageUser1 = bookingData?.sharePercentageUser1 || 0;
  let sharePercentageUser2 = bookingData?.sharePercentageUser2 || 0;
  let shareRideAmountUser1 = bookingData?.shareRideAmountUser1 || 0;
  let shareRideAmountUser2 = bookingData?.shareRideAmountUser2 || 0;
  let discountAmount = bookingData?.discountAmount || 0;
  let roundingAmount = bookingData?.roundingAmount || 0;
  // ---- Assistance Care Start ------
  let isEnableAssistanceCare = bookingData?.isEnableAssistanceCare ?? false; // Pass From User
  let beforeRideAssistanceCareGracePeriod =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.beforeRideAssistanceCareGracePeriod || 0;
  let beforeRideAssistanceCareNormalChargePerMin =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.beforeRideAssistanceCareNormalChargePerMin || 0;
  let beforeRideAssistanceCareRevisedChargePerMin =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.beforeRideAssistanceCareRevisedChargePerMin || 0;
  let beforeRideAssistanceCareNeededMins = parseInt(
    bookingData.beforeRideAssistanceCareNeededMins || 0
  ); // Pass From User
  let beforeRideAssistanceCareProvidesMins = parseInt(
    bookingData.beforeRideAssistanceCareProvidesMins ||
      beforeRideAssistanceCareNeededMins
  ); // Pass From User
  let beforeRideAssistanceCareAmount =
    bookingData.beforeRideAssistanceCareAmount || 0; // Pass From User
  //
  let afterRideAssistanceCareGracePeriod =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.afterRideAssistanceCareGracePeriod || 0;
  let afterRideAssistanceCareNormalChargePerMin =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.afterRideAssistanceCareNormalChargePerMin || 0;
  let afterRideAssistanceCareRevisedChargePerMin =
    bookingData.vehicleCategoryData?.assistanceCareDetails
      ?.afterRideAssistanceCareRevisedChargePerMin || 0;
  let afterRideAssistanceCareNeededMins = parseInt(
    bookingData.afterRideAssistanceCareNeededMins || 0
  ); // Pass From User
  let afterRideAssistanceCareProvidesMins = parseInt(
    bookingData.afterRideAssistanceCareProvidesMins ||
      afterRideAssistanceCareNeededMins
  ); // Pass From User
  let afterRideAssistanceCareAmount =
    bookingData.afterRideAssistanceCareAmount || 0; // Pass From User
  //
  let assistanceCareAmount = bookingData.assistanceCareAmount || 0; // Pass From User
  const assistanceCareNotes = bookingData.assistanceCareNotes || ""; // Pass From User
  // ---- Assistance Care End ------
  const isEnableAddOn = bookingData.isEnableAddOn || false;
  const addOnDetails = bookingData.addOnDetails || [];
  const addOnAmount = bookingData.addOnAmount || 0;
  //#endregion Default Values

  const cancellationStatus = false,
    cancellationAmount = 0,
    cancellationMinute = 0,
    professionalCancellationStatus = false,
    professionalCancellationAmount = 0,
    professionalCancellationMinute = 0;

  //For Fixed Payable Amount Ride/Booking
  if (isFixedPayableAmount) {
    isEnableDynamicTravelCharge = false;
    //------ Default Values Start -----------
    baseFare = 0;
    //farePerDistance
    farePerDistance = 0;
    farePerDistance1 = 0;
    farePerDistance2 = 0;
    farePerDistance3 = 0;
    farePerDistance4 = 0;
    //farePerDistance
    farePerMinute = 0;
    farePerMinute1 = 0;
    farePerMinute2 = 0;
    farePerMinute3 = 0;
    farePerMinute4 = 0;
    //fromDistance
    fromDistance = 0;
    fromDistance1 = 0;
    fromDistance2 = 0;
    fromDistance3 = 0;
    fromDistance4 = 0;
    //toDistance
    toDistance = Date.now();
    toDistance1 = 0;
    toDistance2 = 0;
    toDistance3 = 0;
    toDistance4 = 0;
    //
    intercityBoundaryFareType = "FLAT";
    isEnableRoundTrip = false;
    intercityBoundaryFare = 0;
    intercityPickupFarePerDistance = 0;
    intercityPickupFarePerMinites = 0;
    peakFare = 0;
    nightFare = 0;
    // sharePercent = 0;
    minimumChargeStatus = false;
    minimumChargeAmount = 0;
    isMinimumChargeApplied = false;
    surchargeFee = 0;
    isSurchargeEnable = false;
    surchargeTitle = "Surcharge";
    // waitingChargePerMin = 0;
    estimationAmount = bookingData.payableAmount; //For isFixedPayableAmount
    actualEstimationAmount = bookingData.payableAmount; //For isFixedPayableAmount
    travelCharge = bookingData.payableAmount; //For isFixedPayableAmount
    distanceFare = 0;
    timeFare = 0;
    intercityPickupDistanceFare = 0;
    intercityPickupTimeFare = 0;
    intercityPickupCharge = 0;
    serviceTaxPercentage = 0;
    serviceTax = 0;
    payableAmount = bookingData.payableAmount; //For isFixedPayableAmount
    siteCommissionPercentage =
      bookingData.vehicleCategoryData.siteCommission || 0; //For isFixedPayableAmount
    siteCommission =
      (bookingData.payableAmount / 100) * siteCommissionPercentage; //For isFixedPayableAmount
    siteCommissionWithoutTax =
      (bookingData.payableAmount / 100) * siteCommissionPercentage; //For isFixedPayableAmount
    professionalCommision = payableAmount - siteCommission;
    professionalCommisionWithoutTips = payableAmount - siteCommission;
    //--------- before Ride Waiting Charge Start --------------------
    isBeforeRideWaitingCharge = false;
    beforeRideWaitingChargePerMin = 0;
    beforeRideWaitingGracePeriod = 0;
    //--------- before Ride Waiting Charge end --------------------
    //--------- after Ride Waiting Charge Start --------------------
    isAfterRideWaitingCharge = false;
    afterRideWaitingChargePerMin = 0;
    afterRideWaitingGracePeriod = 0;
    //--------- after Ride Waiting Charge End --------------------
    cancellationFeeCreditToUserPrecentage = 0;
    cancellationFeeCreditToProfessionalPrecentage = 0;
    roundingAmount = 0;
    sharePercentageUser1 = 0;
    sharePercentageUser2 = 0;
    shareRideAmountUser1 = 0;
    shareRideAmountUser2 = 0;
    discountAmount = 0;
    // ---- Assistance Care Start ------
    isEnableAssistanceCare = false;
    beforeRideAssistanceCareGracePeriod = 0;
    beforeRideAssistanceCareNormalChargePerMin = 0;
    beforeRideAssistanceCareRevisedChargePerMin = 0;
    beforeRideAssistanceCareNeededMins = 0;
    beforeRideAssistanceCareProvidesMins = 0;
    beforeRideAssistanceCareAmount = 0;
    //
    afterRideAssistanceCareGracePeriod = 0;
    afterRideAssistanceCareNormalChargePerMin = 0;
    afterRideAssistanceCareRevisedChargePerMin = 0;
    afterRideAssistanceCareNeededMins = 0;
    afterRideAssistanceCareProvidesMins = 0;
    afterRideAssistanceCareAmount = 0;
    //
    assistanceCareAmount = 0;
    // ---- Assistance Care End ------
    //------ Default Values End -----------
  } else {
    switch ((bookingData?.bookingBy ?? "").toUpperCase()) {
      case constantUtil.ADMIN: // Admin
      case constantUtil.COORPERATEOFFICE: //Office
        estimationPayableAmount = bookingData.estimationAmount;
        actualEstimationAmount = bookingData.estimationAmount;
        break;

      case constantUtil.USER: // User
        pendingPaymentAmount = bookingData.pendingPaymentAmount || 0;
        estimationPayableAmount = bookingData.actualEstimationAmount;
        actualEstimationAmount = bookingData.actualEstimationAmount;
        couponApplied = bookingData.couponApplied || false;
        couponValue = bookingData.couponValue || 0;
        couponType = bookingData.couponType || "";
        couponAmount = bookingData.couponAmount || 0;
        isProfessionalTolerance = bookingData.isProfessionalTolerance;
        break;

      case constantUtil.CORPORATEOFFICER: //Officer (or) Office Employee
        pendingPaymentAmount = bookingData.pendingPaymentAmount || 0;
        estimationPayableAmount = bookingData.actualEstimationAmount;
        actualEstimationAmount = bookingData.actualEstimationAmount;
        isProfessionalTolerance = bookingData.isProfessionalTolerance;
        break;

      case constantUtil.PROFESSIONAL: // Professional
        estimationPayableAmount = bookingData.actualEstimationAmount;
        actualEstimationAmount = bookingData.actualEstimationAmount;
        break;
    }
  }
  //#region fareSettings
  const fareSettings = [];
  if (isEnableDynamicTravelCharge) {
    fareSettings.push({
      "startKM": fromDistance,
      "endKM": toDistance,
      "farePerMinute": farePerMinute,
      "farePerDistance": farePerDistance,
    });
    fareSettings.push({
      "startKM": fromDistance1,
      "endKM": toDistance1,
      "farePerMinute": farePerMinute1,
      "farePerDistance": farePerDistance1,
    });
    fareSettings.push({
      "startKM": fromDistance2,
      "endKM": toDistance2,
      "farePerMinute": farePerMinute2,
      "farePerDistance": farePerDistance2,
    });
    fareSettings.push({
      "startKM": fromDistance3,
      "endKM": toDistance3,
      "farePerMinute": farePerMinute3,
      "farePerDistance": farePerDistance3,
    });
    fareSettings.push({
      "startKM": fromDistance4,
      "endKM": Date.now(), //Unlimited Distance
      "farePerMinute": farePerMinute4,
      "farePerDistance": farePerDistance4,
    });
  } else {
    fareSettings.push({
      "startKM": 0,
      "endKM": Date.now(), //Unlimited Distance
      "farePerMinute": farePerMinute,
      "farePerDistance": farePerDistance,
    });
  }
  //#endregion fareSettings

  return {
    "isEnableDynamicTravelCharge": isEnableDynamicTravelCharge,
    "baseFare": baseFare,
    "fareSettings": fareSettings,
    // //farePerDistance
    "farePerDistance": farePerDistance,
    // "farePerDistance1": farePerDistance1,
    // "farePerDistance2": farePerDistance2,
    // "farePerDistance3": farePerDistance3,
    // "farePerDistance4": farePerDistance4,
    // //farePerMinute
    "farePerMinute": farePerMinute,
    // "farePerMinute1": farePerMinute1,
    // "farePerMinute2": farePerMinute2,
    // "farePerMinute3": farePerMinute3,
    // "farePerMinute4": farePerMinute4,
    // //
    "isPeakTiming": isPeakTiming,
    "isNightTiming": isNightTiming,
    "peakFareText": bookingData?.surge?.peakFareText || "",
    "peakFare": peakFare,
    "nightFare": nightFare,
    "stopCharge": stopCharge, //Per Stop
    "multiStopChargeList": [],
    "multiStopCharge": 0,
    "actualPeakFare": actualPeakFare,
    "heatmapPeakFare": heatmapPeakFare,
    // "sharePercent": sharePercent,
    "cancellationStatus": cancellationStatus,
    "cancellationAmount": cancellationAmount,
    "cancellationMinute": cancellationMinute,
    "professionalCancellationStatus": professionalCancellationStatus,
    "professionalCancellationAmount": professionalCancellationAmount,
    "professionalCancellationMinute": professionalCancellationMinute,
    "minimumChargeStatus": minimumChargeStatus,
    "minimumChargeAmount": minimumChargeAmount,
    "isMinimumChargeApplied": isMinimumChargeApplied,
    //---------- Surcharge Start ---------
    "surchargeFee": surchargeFee,
    "isSurchargeEnable": isSurchargeEnable,
    "surchargeTitle": surchargeTitle,
    //---------- Surcharge End ---------
    // "waitingChargePerMin": waitingChargePerMin,
    "peakFareCharge": 0,
    "nightFareCharge": 0,
    "waitingCharge": 0,
    "estimationAmount": estimationAmount,
    "travelCharge": travelCharge,
    "distanceFare": distanceFare,
    "timeFare": timeFare,
    //-------- InterCity Start ------------
    "intercityBoundaryFareType": intercityBoundaryFareType,
    "isEnableRoundTrip": isEnableRoundTrip,
    "intercityBoundaryFare": intercityBoundaryFare,
    "intercityPickupFarePerDistance": intercityPickupFarePerDistance,
    "intercityPickupFarePerMinites": intercityPickupFarePerMinites,
    "intercityPickupDistanceFare": intercityPickupDistanceFare,
    "intercityPickupTimeFare": intercityPickupTimeFare,
    "intercityPickupCharge": intercityPickupCharge,
    //-------- InterCity End ------------
    "tipsAmount": 0,
    "serviceTaxPercentage": serviceTaxPercentage,
    "serviceTax": serviceTax,
    "payableAmount": payableAmount,
    "siteCommissionPercentage": siteCommissionPercentage,
    "siteCommission": siteCommission,
    "siteCommissionWithoutTax": siteCommissionWithoutTax,
    "professionalCommision": professionalCommision,
    "professionalCommisionWithoutTips": professionalCommisionWithoutTips,
    "amountInDriver": 0,
    "amountInSite": 0,
    "tollFareAmount": 0,
    "airportFareAmount": airportFareAmount,
    "passedTollList": [],
    "totalTollPassed": 0,
    "professionalTolerenceAmount": 0,
    "pendingPaymentAmount": pendingPaymentAmount,
    "estimationPayableAmount": estimationPayableAmount,
    "actualEstimationAmount": actualEstimationAmount,
    "couponApplied": couponApplied,
    "couponValue": couponValue,
    "couponType": couponType,
    "couponAmount": couponAmount,
    "isProfessionalTolerance": isProfessionalTolerance,
    //---------- Waiting Charge Start ---------
    "isBeforeRideWaitingCharge": isBeforeRideWaitingCharge,
    "beforeRideWaitingChargePerMin": beforeRideWaitingChargePerMin,
    "beforeRideWaitingMins": 0,
    "beforeRideWaitingGracePeriod": beforeRideWaitingGracePeriod,
    "beforeRideWaitingCharge": 0,
    "isAfterRideWaitingCharge": isAfterRideWaitingCharge,
    "afterRideWaitingChargePerMin": afterRideWaitingChargePerMin,
    "afterRideWaitingMins": 0,
    "afterRideWaitingGracePeriod": afterRideWaitingGracePeriod,
    "afterRideWaitingCharge": 0,
    "isFixedPayableAmount": isFixedPayableAmount,
    //---------- Waiting Charge End ---------
    "cancellationFeeCreditToUserPrecentage":
      cancellationFeeCreditToUserPrecentage,
    "cancellationFeeCreditToProfessionalPrecentage":
      cancellationFeeCreditToProfessionalPrecentage,
    "roundingType": roundingType,
    "roundingFactor": roundingFactor,
    "roundingAmount": roundingAmount,
    "sharePercentageUser1": sharePercentageUser1,
    "sharePercentageUser2": sharePercentageUser2,
    "shareRideAmountUser1": shareRideAmountUser1,
    "shareRideAmountUser2": shareRideAmountUser2,
    "discountAmount": discountAmount,
    //---------- Assistance Care ---------
    "isEnableAssistanceCare": isEnableAssistanceCare,
    "beforeRideAssistanceCareGracePeriod": beforeRideAssistanceCareGracePeriod,
    "beforeRideAssistanceCareNormalChargePerMin":
      beforeRideAssistanceCareNormalChargePerMin,
    "beforeRideAssistanceCareRevisedChargePerMin":
      beforeRideAssistanceCareRevisedChargePerMin,
    "beforeRideAssistanceCareNeededMins": beforeRideAssistanceCareNeededMins,
    "beforeRideAssistanceCareProvidesMins":
      beforeRideAssistanceCareProvidesMins,
    "beforeRideAssistanceCareAmount": beforeRideAssistanceCareAmount,
    //
    "afterRideAssistanceCareGracePeriod": afterRideAssistanceCareGracePeriod,
    "afterRideAssistanceCareNormalChargePerMin":
      afterRideAssistanceCareNormalChargePerMin,
    "afterRideAssistanceCareRevisedChargePerMin":
      afterRideAssistanceCareRevisedChargePerMin,
    "afterRideAssistanceCareNeededMins": afterRideAssistanceCareNeededMins,
    "afterRideAssistanceCareProvidesMins": afterRideAssistanceCareProvidesMins,
    "afterRideAssistanceCareAmount": afterRideAssistanceCareAmount,
    //
    "assistanceCareAmount": assistanceCareAmount,
    "assistanceCareNotes": assistanceCareNotes,
    //
    "isEnableAddOn": isEnableAddOn,
    "addOnDetails": addOnDetails,
    "addOnAmount": addOnAmount,
  };
};

mappingUtils.calculateDistanceAndTimeFare = (invoice) => {
  let invoiceData = [];
  let farePerMinute = 0,
    distanceFare = 0,
    timeFare = 0;

  if (!invoice.fareSettings) {
    if (invoice.isEnableDynamicTravelCharge) {
      invoiceData.push({
        "startKM": invoice.fromDistance,
        "endKM": invoice.toDistance,
        "farePerMinute": invoice.farePerMinute,
        "farePerDistance": invoice.farePerDistance,
      });
      invoiceData.push({
        "startKM": invoice.fromDistance1,
        "endKM": invoice.toDistance1,
        "farePerMinute": invoice.farePerMinute1,
        "farePerDistance": invoice.farePerDistance1,
      });
      invoiceData.push({
        "startKM": invoice.fromDistance2,
        "endKM": invoice.toDistance2,
        "farePerMinute": invoice.farePerMinute2,
        "farePerDistance": invoice.farePerDistance2,
      });
      invoiceData.push({
        "startKM": invoice.fromDistance3,
        "endKM": invoice.toDistance3,
        "farePerMinute": invoice.farePerMinute3,
        "farePerDistance": invoice.farePerDistance3,
      });
      invoiceData.push({
        "startKM": invoice.fromDistance4,
        "endKM": Date.now(), //Unlimited Distance
        "farePerMinute": invoice.farePerMinute4,
        "farePerDistance": invoice.farePerDistance4,
      });
    } else {
      invoiceData.push({
        "startKM": 0,
        "endKM": Date.now(), //Unlimited Distance
        "farePerMinute": invoice.farePerMinute,
        "farePerDistance": invoice.farePerDistance,
      });
    }
  } else {
    invoiceData = invoice.fareSettings;
  }
  const distanceTypeUnitValue = invoice.distanceTypeUnitValue || 0;
  invoiceData.map((fareSettings) => {
    const startKM = fareSettings.startKM * distanceTypeUnitValue;
    const endKM = fareSettings.endKM * distanceTypeUnitValue;
    if (invoice.estimationDistance >= startKM) {
      // rr = rr + (Math.min(d, x.endKM) - x.startKM) * x.farePerMinute;
      const traveledDistance =
        Math.min(invoice.estimationDistance, endKM) - startKM;
      // DISTANCE CALCULATION BY FORMULA
      const distanceKey = formulaJSON["RIDE"]["DISTANCEFARE"];
      const distanceValue = {
        "FAREPERMETER": fareSettings.farePerDistance / distanceTypeUnitValue, //1000 Meter --> 1 KM, 1610 meter --> 1 Mile
        "TRAVELPERMETER": traveledDistance,
      };
      const traveledDistanceFare = parseFloat(
        formula.run(distanceKey, distanceValue)?.toFixed(2) || 0
      );
      distanceFare = distanceFare + parseFloat(traveledDistanceFare);
      //FOR TIME CALCULATION
      farePerMinute = fareSettings.farePerMinute;
    }
  });

  // TIME CALCULATION BY FORMULA
  const timeKey = formulaJSON["RIDE"]["TIMEFARE"];
  const timeValue = {
    "FAREPERSEC": farePerMinute / 60,
    "TRAVELPERSEC": invoice.estimationTime || 0,
  };
  timeFare = parseFloat(formula.run(timeKey, timeValue)?.toFixed(2) || 0);

  return { "distanceFare": distanceFare, "timeFare": timeFare };
};
//------------------
module.exports = mappingUtils;
