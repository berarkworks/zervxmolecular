const axios = require("axios");
const qs = require("qs");
const FormData = require("form-data");
const { DOMParser } = require("xmldom");

const paymentUtil = {};

// For card tokenization
paymentUtil.createTokenization = async (params) => {
  // const url = `${params.baseUrl}v1/registrations`;
  const url = `${params.baseUrl}v1/payments`;
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    // console.log(config);
    const response = await axios(config);
    return response.data;
  } catch (error) {
    console.log(error);
    return null;
  }

  // const url = `${params.baseUrl}v1/payments`; // params.id -- tokenization Id
  // params.data["paymentType"] = "DB"; // "DB" --> Debit
  // const config = {
  //   "method": "post",
  //   "url": url,
  //   "headers": {
  //     "Authorization": `Bearer ${params.token}`,
  //     "Content-Type": "application/x-www-form-urlencoded",
  //   },
  //   "data": qs.stringify(params.data),
  // };
  // try {
  //   const response = await axios(config);
  //   return response.data;
  // } catch (error) {
  //   // console.log(error)
  //   return null;
  // }
};

// For Validate card tokenization Status
paymentUtil.validateCardTokenizationStatus = async (params) => {
  // const url =
  //   `${params.baseUrl}v1/registrations/${params.cardTokenId}?entityId=` +
  //   params.data.entityId; // params.id -- tokenization Id
  // const url =
  //   `${params.baseUrl}v1/payments/${params.transactionId}?entityId=` +
  //   params.data.entityId; // params.id -- tokenization Id
  const url =
    `${params.baseUrl}v1/payments/${params.paymentIntentId}?entityId=` +
    params.data.entityId; // params.id -- tokenization Id
  const config = {
    // "method": "post",
    "method": "get",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      // "Content-Type": "application/x-www-form-urlencoded",
    },
    // "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error);
    return null;
  }
};

// For card delete tokenization
paymentUtil.deleteTokenization = async (params) => {
  const url = `${params.baseUrl}v1/registrations/${params.id}?entityId=${params.entityId}`; // params._id -- tokenization Id
  const config = {
    "method": "delete",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
    },
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Transactions
paymentUtil.preAuthTransaction = async (params) => {
  const url = `${params.baseUrl}v1/registrations/${params.cardTokenId}/payments`; // params._id -- tokenization Id
  params.data["paymentType"] = "PA"; // "PA" --> PreAuth
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Instant Card Payment using CardToken
paymentUtil.instantCardPayment = async (params) => {
  const url = `${params.baseUrl}v1/registrations/${params.cardTokenId}/payments`; // params.id -- tokenization Id
  params.data["paymentType"] = "DB"; // "DB" --> Debit
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Instant Cash Deposit to Card using CardToken
paymentUtil.instantCashDeposit = async (params) => {
  const url = `${params.baseUrl}v1/registrations/${params.cardTokenId}/payments`; // params.id -- tokenization Id
  params.data["paymentType"] = "CD"; // "CD" --> CASH DEPOSIT
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Capture
paymentUtil.capturePayment = async (params) => {
  const url = `${params.baseUrl}v1/payments/${params.paymentInitId}`; // params.id -- tokenization Id
  params.data["paymentType"] = "CP"; // "CP" --> Capture
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Cancel Capture
paymentUtil.cancelCapturePayment = async (params) => {
  const url = `${params.baseUrl}v1/payments/${params.paymentIntentId}`; // params.id -- tokenization Id
  params.data["paymentType"] = "RV"; // "RV" --> Reversal / CancelCapture
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Refund
paymentUtil.refundPayment = async (params) => {
  const url = `${params.baseUrl}v1/payments/${params.id}`; // params.id -- tokenization Id
  params.data["paymentType"] = "RF"; // "RF" --> Refund
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Transactions
paymentUtil.allTransactions = async (params) => {
  const url = `${params.baseUrl}v1/payments/${params._id}`; // params._id -- tokenization Id
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Get Transaction
paymentUtil.getTransaction = async (params) => {
  const url = `${params.baseUrl}v1/registrations/${params.transactionId}?entityId=${params.entityId}`; // params.transaction_id -- tokenization Id
  const config = {
    "method": "get",
    "url": url,
    "port": 443,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
    },
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};

// For Debit Transactions
paymentUtil.debitTransactions = async (params) => {
  const url = `${params.baseUrl}v1/payments`;
  const config = {
    "port": 443,
    "method": "POST", //"post",
    "url": url,
    "headers": {
      "Authorization": `Bearer ${params.token}`,
      "Content-Length": qs.stringify(params.data).length,
      "Content-Type": "application/x-www-form-urlencoded",
    },
    "data": qs.stringify(params.data),
  };
  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    // console.log(error)
    return null;
  }
};
// For Debit Wallet Transactions
paymentUtil.debitWalletTransactions = async (params) => {
  const data = new FormData();
  const url = `${params.baseUrl}/API/Payments?key=${params.key}`;
  //---------------------
  const clientCode = params.clientCode;
  const companyName = params.companyName;
  const referenceId = Date.now();
  const dueDate = new Date().toLocaleDateString("en-CA");
  const callbackUrl = "https://www.example.com/";
  const externalLinkURL = "https://www.google.com/";
  //------------------

  // <Reference>${companyName}_PeachPayments_Payout_at_${dueDate}</Reference>

  params.data = `<APIPaymentsRequest>
    <Header>
      <PsVer>2.0.1</PsVer>
      <Client>${clientCode}</Client>
      <Service>Creditors</Service>
      <ServiceType>SDV</ServiceType>
      <DueDate>${dueDate.replace(/-/g, "")}</DueDate>
      <CallbackUrl>${callbackUrl}</CallbackUrl>
      <Reference>${referenceId}</Reference>
    </Header>
    <Payments>
      <FileContents>
        <Initial>${params.data.initial || ""}</Initial>
        <FirstName>${params.data.firstName || ""}</FirstName>
        <Surname>${params.data.lastName || ""}</Surname>
        <BranchCode>${params.data.branchCode}</BranchCode>
        <AccountNumber>${params.data.accountNumber}</AccountNumber>
        <FileAmount>${params.data.amount}</FileAmount>
        <AccountType>0</AccountType>
        <AmountMultiplier>1</AmountMultiplier>
        <Reference>${params.data.transactionReason}</Reference>
        <ExternalLinks>
          <ExternalLink>
            <Label>Invoice</Label>
            <URL>${externalLinkURL}</URL>
          </ExternalLink>
        </ExternalLinks>
      </FileContents>
    </Payments>
    <Totals>
      <Records>1</Records>
      <Amount>${params.data.amount}</Amount>
      <BranchHash>${params.data.branchCode}</BranchHash>
      <AccountHash>${params.data.accountNumber}</AccountHash>
    </Totals>
  </APIPaymentsRequest>`;
  //---------------------
  data.append("request", params.data);
  const config = {
    "method": "post",
    "url": url,
    "port": 443,
    "headers": {
      ...data.getHeaders(),
    },
    "data": data,
  };
  try {
    let response = await axios(config);
    //-----------------------------
    var parser = new DOMParser();
    var xmlDoc = parser.parseFromString(response.data, "text/xml");
    // var tagObj = xmlDoc.getElementsByTagName("CDVResults");
    var status = xmlDoc
      .getElementsByTagName("Result")[0]
      .childNodes[0].nodeValue?.toUpperCase();
    var transactionStatus = (
      status === "ERROR"
        ? "invalid"
        : xmlDoc.getElementsByTagName("Result")?.[2]?.childNodes?.[0]?.nodeValue
    )?.toUpperCase();
    var message =
      xmlDoc.getElementsByTagName("ResultMessage")?.[0]?.childNodes?.[0]
        ?.nodeValue;
    //-----------------------------
    // const successData = {
    //   "data": response,
    //   "config": config,
    //   "formData": params.data,
    // };
    // await axios({
    //   "method": "GET",
    //   "port": 443,
    //   "url": `https://webhook.site/74044c86-4159-4ac7-a980-6e53a0eb2988`,
    //   "headers": {
    //     "Content-Type": "application/json",
    //   },
    //   "data": { "APIStatus": "success", "gatwayResponse": successData },
    // });
    //-----------------------------
    // return response.data;
    return {
      "id": referenceId,
      "referenceId": referenceId,
      "xmlResponse": response.data,
      "transactionStatus": transactionStatus,
      "status": status,
      "message": message,
    };
  } catch (error) {
    console.log(error);
    // const errorData = {
    //   "data": JSON.stringify(error || {}),
    //   "config": config,
    //   "formData": params.data,
    // };
    // await axios({
    //   "method": "GET",
    //   "port": 443,
    //   "url": `https://webhook.site/74044c86-4159-4ac7-a980-6e53a0eb2988`,
    //   "headers": {
    //     "Content-Type": "application/json",
    //   },
    //   "data": { "APIStatus": "failed", "gatwayResponse": errorData },
    // });
    return null;
  }
};
//-----------------------------
module.exports = paymentUtil;
